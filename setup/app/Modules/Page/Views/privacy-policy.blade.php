@extends('Page::layout.one-column-page')
@section('page_title')
Privacy Policy - Aquantuo
@endsection
@section('content')

  <div class="container">
  @if(Session::get('UserId') == '')
   <br/><br/>
  @endif
    <div class="heading2">
      Privacy Policy
    </div>
      <?php echo $content['Content']; ?>
  </div>

@endsection


