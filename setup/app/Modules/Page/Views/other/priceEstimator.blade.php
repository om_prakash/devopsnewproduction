@extends('Page::layout.one-column-page')
@push('css')
{!! Html::style('theme/web/css/parsley.css') !!}
@endpush
@section('content')

@section('page_title')
Shipping Calculator - Aquantuo
@endsection
<!-- 
	* Total forms 4
	* 1. online (I am buying online myself) -> formID = online
	* 2. buy for me (I need Aquantuo to buy for me) -> formID = buyforme_form
	* 3. send package (I need to move a package from one country to another) -> formID = delivery_form
	* 4. Local Delivery -> formID = local_delivery_form
 -->

<div class="container section-custom price">
	<div class="row">
		<div class="price_estimator" id="price_estimator">
			<div class=" clearfix">
				<div class="col-sm-8 col-xs-12 mob-right">
					<h3 class="modal-title text-left" style="padding-left:5px;" id="myModalLabel">Price Estimator </h3>
				</div>
				<!-- form box -->
				<div class="col-sm-8 col-xs-12 mob-right no-gutter ">
					<div class="m-scroll" style="overflow-x:auto;overflow-y:hidden;">
						<div class="col-md-4" id="first_div" onclick="return set_form('air')" >
							<div class="air_div_css block-space loader-box" id="first_sec">
								<h4>Air/Air Freight <span class="pull-right"><i class="fa fa-plane fa_size" style="margin-left:10px" ></i></span></h4>
								<!-- <p>Get it in 13 days or less</p> -->
								<p>5-10 business days</p>
								<span class="loader" style="display:none;">
									<div class="wrap">
										<div class="bar1"></div>
										<div class="bar2"></div>
										<div class="bar3"></div>
										<div class="bar4"></div>
										<div class="bar5"></div>
										<div class="bar6"></div>
										<div class="bar7"></div>
										<div class="bar8"></div>
										<div class="bar7"></div>
										<div class="bar8"></div>
										<div class="bar9"></div>
										<div class="bar10"></div>
									</div>
								</span>
							</div>
						</div>
						<div class="col-md-4" id="secound_div" onclick="return set_form('sea')" >
							<div style="" class="sea_div_css block-space loader-box" id="second_sec">
								<h4>Ocean/Sea Freight <span class="pull-right"><i class="fa fa-ship fa_size" style="margin-left:10px"></i></span></h4>
								<p>Get it in 6 to 8 weeks</p>
								<span class="loader" style="display:none;">
									<div class="wrap">
										<div class="bar1"></div>
										<div class="bar2"></div>
										<div class="bar3"></div>
										<div class="bar4"></div>
										<div class="bar5"></div>
										<div class="bar6"></div>
										<div class="bar7"></div>
										<div class="bar8"></div>
										<div class="bar7"></div>
										<div class="bar8"></div>
										<div class="bar9"></div>
										<div class="bar10"></div>
									</div>
								</span>
							</div>
						</div>

						<div class="col-md-4" id="third_div" onclick="return set_form('delivery')" >
							<div style="" class="delivery_div_css block-space loader-box" id="third_sec">
								<h4>Local Delivery <span class="pull-right"><i class="fa fa-motorcycle fa_size" style="margin-left:10px" aria-hidden="true"></i></span></h4>
								<p>Pick up and deliver within Ghana</p>
								<span class="loader" style="display:none;">
									<div class="wrap">
										<div class="bar1"></div>
										<div class="bar2"></div>
										<div class="bar3"></div>
										<div class="bar4"></div>
										<div class="bar5"></div>
										<div class="bar6"></div>
										<div class="bar7"></div>
										<div class="bar8"></div>
										<div class="bar7"></div>
										<div class="bar8"></div>
										<div class="bar9"></div>
										<div class="bar10"></div>
									</div>
								</span>
							</div>
						</div> 
					</div>
					<!-- tabe content -->
					<div class="col-sm-12 col-xs-12 text-center" id="default_text"><br />
						<h3  class=" bottom-space">Please choose an option from above that best fits your delivery need.</h3>
					</div>
					<div class="col-sm-12 col-xs-12 ">
						<!-- Nav tabs menu -->
						<div id="tabmenu" style="display: none;">
							<ul class="nav nav-tabs my-tabs" role="tablist">
								<li role="presentation"  class="active li-loader-box" id="menu1">
									<a href="#home" aria-controls="home" role="tab" data-toggle="tab" id="menu32" onclick="return request_type('op');"> 
										<div class="list-icon-price">
											<i class="fa fa-shopping-cart"></i>
										</div>
										I am shopping online myself or shipping my package to Aquantuo
									</a>
								<span class="loader" style="display:none;">
									<div class="wrap">
										<div class="bar1"></div>
										<div class="bar2"></div>
										<div class="bar3"></div>
										<div class="bar4"></div>
										<div class="bar5"></div>
										<div class="bar6"></div>
										<div class="bar7"></div>
										<div class="bar8"></div>
										<div class="bar7"></div>
										<div class="bar8"></div>
										<div class="bar9"></div>
										<div class="bar10"></div>
									</div>
								</span>
							</li>
							<li role="presentation" class="li-loader-box" id="menu2">
								<a href="#profile" aria-controls="profile" role="tab" data-toggle="tab" onclick="return request_type('bfm');">
									<div class="list-icon-price"><i class="fa fa-shopping-bag"></i></div>
									I need Aquantuo to buy for me
								 </a>
							<span class="loader" style="display:none;">
								<div class="wrap">
									<div class="bar1"></div>
									<div class="bar2"></div>
									<div class="bar3"></div>
									<div class="bar4"></div>
									<div class="bar5"></div>
									<div class="bar6"></div>
									<div class="bar7"></div>
									<div class="bar8"></div>
									<div class="bar7"></div>
									<div class="bar8"></div>
									<div class="bar9"></div>
									<div class="bar10"></div>
								</div>
							</span>
						</li>
						<li role="presentation" class="li-loader-box" id="menu3">
							<a href="#messages" aria-controls="messages" role="tab" data-toggle="tab" id="menu33" onclick="return request_type('delivery');">
								<div class="list-icon-price"><i class="fa fa-file"></i> </div>
								Pick up my package from one country to another
								</a>
						<span class="loader" style="display:none;">
							<div class="wrap">
								<div class="bar1"></div>
								<div class="bar2"></div>
								<div class="bar3"></div>
								<div class="bar4"></div>
								<div class="bar5"></div>
								<div class="bar6"></div>
								<div class="bar7"></div>
								<div class="bar8"></div>
								<div class="bar7"></div>
								<div class="bar8"></div>
								<div class="bar9"></div>
								<div class="bar10"></div>
							</div>
						</span>
					</li>
				</ul>
			</div>
			<!-- end Nav tabs menu -->
			<!-- Tab panes -->
			<div class="tab-content" id="tabcontent" style="display: none;">
				<div role="tabpanel" class="tab-pane active" id="home">
					<div class="row">
						<form name="online" id="online" class="custom-select">
							<input type="hidden" name="request_type" value="online">
							<input type="hidden" name="travelMode" value="" id="online_travelMode">
							<div class="col-sm-12 col-xs-12">
								<br>
								<label class="control-label">Destination Country </label>
							</div>
							<div class="row" style="margin: 0;">
								<div class="col-sm-4 col-xs-12">
									<select name="country" class="form-control required" id="pp_pickup_country10" onchange="get_state2('pp_pickup_country10','pp_pickup_state10','pp_pickup_city10','pp_pickup_state10','','','','10','')">
										<option value="">Select Country</option>
										@foreach($country as $key)
										<option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}","state_available":"{{$key->state_available}}"}'>{{$key->Content}}</option>
										@endforeach
									</select>
									
								</div>
								<div class="col-sm-4 col-xs-12 select-chosen">
									<span id="ap_id10">
									<!-- //chosen-select -->
									<select name="state" class="form-control required left-disabled chosen-select" id="pp_pickup_state10" onchange="get_city('pp_pickup_state','pp_pickup_city','pp_pickup_city','')">
										<option value="">Select State/Region</option>
										
									</select>
									</span>
								</div>
								<div class="col-sm-4 col-xs-12">
									<select  name="city" class="form-control required chosen-select" id="pp_pickup_city10">
										<option value="">Select City</option>
									</select>
								</div>
							</div>

							<div class="row" style="margin: 0;">
								<div class="col-sm-6 col-xs-12 col-md-6">
									<div class="form-group">
										<label class="control-label">Select Package Category</label>
										<span class="airCategoryDropDown">
											
										</span>
										
									</div>
								</div>

								

								<div class="col-sm-6 col-xs-12 col-md-6" >
									<div class="form-group">
										<label class="control-label">Total cost of all items($)<span class="red-star"> *</span></label>
										<br>
										<div class="custom-input">
											<input class="form-control required float usename-#product cost#" name="pe_product_cost"  >
										</div>
									</div>
								</div>

								
							</div>

							

							<div class="col-sm-12 col-xs-12">
				               <div class="form-group">
				                  <div class="checkbox curtomlabel">
				                     <label>
				                     <input type="checkbox" name="consolidate_check" value="on" @if(Session::get('consolidate_item') == 'on') checked="checked" @endif >
				                     <p >Consolidate my items and ship them together when possible.</p>
				                     </label>
				                  </div>
				               </div>
				            </div>
							<div id="online_weight_div">
								<div class="col-sm-6 col-xs-12">
									<label class="control-label">
										<br>
										Total weight of item/all items&nbsp;
										<a data-toggle="popover" data-trigger="focus"  data-container="body" data-placement="top" type="button" data-html="true" href="javascript:void(0)" id="login1" class="info_icon" style="margin-top:0;color:black;"><img alt="Total weight of item" src="theme/web/images/info_icon.png">
										</a> </label>
										<div id="popover-content-login1" class="hide">
											<h4>Suggested item weights</h4>
											<small>These are generic weights and may not reflect the actual weight of your item.</small>
											<table class="table table-bordered table-striped">
												<thead>
													<tr>
														<th width="150">Item Name</th>
														<th width="60">Weight/Unit</th>
													</tr>
												</thead>
												<tbody>
													@if(count($item) > 0)
													@foreach($item as $key)
													<tr>
														<td>{{ucfirst($key->item_name)}} </td>
														<td >{{number_format($key->lbsWeight,2)}} Lbs / {{number_format($key->kgWeight,2)}} Kg</td>
													</tr>
													@endforeach
													@endif
													@if($item_count > 5)
													<tr>
														<td colspan="2"><a href="javascript::void(0)" class="label label-info" onclick="return show_hide('more_items');">More</a></td>
													</tr>
													@endif
												</tbody>
											</table>
										</div>
										<div  class="custom-group">
											<div class="custom-input">
												<input  type="numeric" name="weight" class="form-control required float usename-#weight#" placeholder="Total Weight of all Item(s)">
											</div>
											<select name="weight_unit" style="margin-top:10px;" class="form-control required usename-#weight_unit#">
												<option value="lbs" >in lbs</option>
												<option value="kg" >in kg</option>
											</select>
										</div>
									</div>
									<div class="col-sm-6 col-xs-3 m-padd-0">
										<label class="control-label" ></label>
										<br><br>
									</div>
								</div>
								<div class="col-sm-12 col-xs-12">
									<br>
									<div class="form-group clearfix">
										<button class="custom-btn1 btn" id="Calculate1" onclick="return addResClass();">Calculate Now</button>
										<a class="custom-btn1 btn" id="Calculate1" htef="javascript:void(0)" onclick="return form_reset('online')">Reset</a>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="profile">
						<!-- Buy for Me -->
						<div class="row">
							<form name="buyforme_form" id="buyforme_form" class="custom-select">
								<input type="hidden" name="request_type" value="buyforme">
								<input type="hidden" name="travelMode" value="" id="buyforme_travelMode">
								<!-- This is for future use -->
								<!-- <div class="col-sm-12 col-xs-12 col-md-12">
								</div> -->
								<div class="col-sm-12 col-xs-12">
									<br>
									<label class="control-label">Destination Address</label>
								</div>
								<div class="">
									<div class="col-sm-4 col-xs-12">
										<select name="country" class="form-control required " id="pp_pickup_country1" onchange="get_state2('pp_pickup_country1','pp_pickup_state1','pp_pickup_city1','pp_pickup_state1','','','','1','')">

											<option value="">Select Country</option>
											@foreach($country as $key)
											<option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}","state_available":"{{$key->state_available}}"}'>{{$key->Content}}</option>
											@endforeach
										</select>
									</div>
									<div class="col-sm-4 col-xs-12">
										<span id="ap_id1">
										<select name="state" class="form-control required left-disabled chosen-select" id="pp_pickup_state1" onchange="get_city('pp_pickup_state1','pp_pickup_city1','pp_pickup_city1','')">
											<option value="">Select State/Region</option>
										</select>
										</span>
									</div>
									<div class="col-sm-4 col-xs-12">
										<select  name="city" class="form-control required chosen-select" id="pp_pickup_city1">
											<option value="">Select City</option>
										</select>
									</div>
								</div>
								<div class="col-sm-12 col-xs-12">
									<br />
									<div class="form-group">
										<label class="control-label">Select Package Category</label>
										<span class="airCategoryDropDown2">
											
										</span>
									</div>
								</div>
								<div class="col-sm-6 col-xs-12 col-md-12" >
									<div class="form-group">
										<label class="control-label">Total cost of all items($)<span class="red-star"> *</span></label>
										<br>
										<div class="custom-input">
											<input class="form-control required float usename-#product cost#" name="pe_product_cost" id="pe_product_cost" >
										</div>
									</div>
								</div>
								<div class="col-sm-12 col-xs-12">
					               <div class="form-group">
					                  <div class="checkbox curtomlabel">
					                     <label>
					                     <input type="checkbox" name="consolidate_check" value="on" @if(Session::get('consolidate_item') == 'on') checked="checked" @endif >
					                     <p >Consolidate my items and ship them together when possible.</p>
					                     </label>
					                  </div>
					               </div>
					            </div>
								<div id="buy_weight_div">
									<div class="col-sm-6 col-xs-12">
										<label class="control-label">Total weight of item/all items&nbsp;
											<a data-toggle="popover" data-trigger="focus"  data-container="body" data-placement="top" type="button" data-html="true" href="javascript:void(0)" id="login" class="info_icon" style="margin-top:0;color:black;"><img alt="Total weight of item" src="theme/web/images/info_icon.png">
											</a> </label>
											<div id="popover-content-login" class="hide">
												<h4>Suggested item weights</h4>
												<small>These are generic weights and may not reflect the actual weight of your item.</small>
												<table class="table table-bordered table-striped">
													<thead>
														<tr>
															<th width="150">Item Name</th>
															<th width="60">Weight/Unit</th>
														</tr>
													</thead>
													<tbody>
														@if(count($item) > 0)
														@foreach($item as $key)
														<tr>
															<td>{{ucfirst($key->item_name)}} </td>
															<td >{{number_format($key->lbsWeight,2)}} Lbs / {{number_format($key->kgWeight,2)}} Kg</td>
														</tr>
														@endforeach
														@endif
														@if($item_count > 5)
														<tr>
															<td colspan="2"><a href="javascript::void(0)" class="label label-info" onclick="return show_hide('more_items');">More</a></td>
														</tr>
														@endif
													</tbody>
												</table>
											</div>
											<br>
											<div  class="custom-group">
												<div class="custom-input">
													<input  type="numeric" name="weight" class="form-control required float usename-#weight#" placeholder="Total Weight of all Item(s)">
												</div>
												<select name="weight_unit" style="margin-top:10px;" class="form-control required usename-#weight_unit#">
													<option value="lbs" >in lbs</option>
													<option value="kg" >in kg</option>
												</select>
											</div>
										</div>
									</div>
									<div class="col-sm-12 col-xs-12">
										<br>
										<div class="form-group clearfix">
											<button type="submit" class="custom-btn1 btn" id="Calculate2">Calculate Now</button>
											<a class="custom-btn1 btn" id="Calculate1" htef="javascript:void(0)" onclick="return form_reset('buyforme_form')">Reset</a>
										</div>
									</div>
								</form>
							</div>
							<!--End Buy for Me -->
						</div>
						<!-- Send package -->
						<div role="tabpanel" class="tab-pane" id="messages">
							<div class="row">
								<form name="delivery_form" id="delivery_form" class="custom-select">
									<input type="hidden" name="travelMode" value="" id="delivery_travelMode">
									
									<div class="col-sm-12 col-xs-12">
										<br>
										<label class="control-label">Pickup Address</label>
									</div>
									<div class="">
										<div class="col-sm-3 col-xs-12">
											<select name="country2" class="form-control required usename-#country#" id="pp_pickup_country4" onchange="get_state2('pp_pickup_country4','pp_pickup_state4','pp_pickup_city4','pp_pickup_state4','','','','4','')">


												<option value="">Select Country</option>
												@foreach($country as $key)
												<option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}","state_available":"{{$key->state_available}}"}'>{{$key->Content}}</option>
												@endforeach
											</select>
										</div>
										<div class="col-sm-3 col-xs-12">
											<span id = "ap_id4">
											<select name="state2" class="form-control required chosen-select left-disabled usename-#state#" id="pp_pickup_state4" onchange="get_city('pp_pickup_state4','pp_pickup_city4','pp_pickup_city4','')">
												<option value="">Select State/Region</option>
											</select>
											</span>
										</div>
										<div class="col-sm-3 col-xs-12">
											<select  name="city2" class="form-control required chosen-select usename-#city#" id="pp_pickup_city4">
												<option value="">Select City</option>
											</select>
										</div>
										<div id="pickup_postal_code_div" class="col-sm-3 col-xs-12 custom-input">
											<input type="text" placeholder="Enter Postal Code"  class="form-control required usename-#Postal Code#" name="pickup_postal_code" />
										</div>
									</div>
									<div class="col-sm-12 col-xs-12">
										<br>
										<label class="control-label">Destination Address</label>
									</div>
									<div class="">
										<div class="col-sm-3 col-xs-12">
											<select name="country" class="form-control required" id="pp_pickup_country3" onchange="get_state2('pp_pickup_country3','pp_pickup_state3','pp_pickup_city3','pp_pickup_state3','','','','3','')">

												<option value="">Select Country</option>
												@foreach($country as $key)
												<option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}","state_available":"{{$key->state_available}}"}'>{{$key->Content}}</option>
												@endforeach
											</select>
										</div>
										<div class="col-sm-3 col-xs-12">
											<span id="ap_id3">
											<select name="state" class="form-control required left-disabled chosen-select" id="pp_pickup_state3" onchange="get_city('pp_pickup_state3','pp_pickup_city3','pp_pickup_city3','')">
												<option value="">Select State/Region</option>
											</select>
											</span>
										</div>
										<div class="col-sm-3 col-xs-12">
											<select  name="city" class="form-control required chosen-select" id="pp_pickup_city3">
												<option value="">Select City</option>
											</select>
										</div>
										<div id="destination_postal_code_div"  class="col-sm-3 col-xs-12 custom-input">
											<input type="text" placeholder="Enter Postal Code" class="form-control required usename-#Postal Code#" name="destination_postal_code" />
										</div>
									</div>

									<div class="col-sm-12 col-xs-12">
										<br />
										<div class="form-group">
											<label class="control-label">Select Package Category</label>
											<span class="airCategoryDropDown3">
											</span>
										</div>
									</div>

									<div class="col-sm-12 col-xs-12 col-md-12" >
										<div class="form-group">
											<label class="control-label">Total cost of all items($)</label><span class="red-star"> *</span></label>
											<br>
											<div class="custom-input">
												<input class="form-control required float usename-#product cost#" name="pe_product_cost"  >
											</div>
										</div>
									</div>

									<div class="form-group clearfix">
										<div class="col-sm-12 col-xs-12">
											<br>
											<label class="control-label">Measurement Unit</label>
											<br>
											<label class="radio-inline " >
												<span class="pull-left"><input type="radio" name="measurement_unit" value="cm_kg" ></span><span class="Insurance_check">Metric (cm/kg)</span></label>
												<label class="radio-inline m0device">
													<span class="pull-left">
														<input type="radio" name="measurement_unit" value="inches_lbs" checked="checked">
														</span> <span class="Insurance_check">Imperial (inches/lbs) </span></label>
													</div>
												</div>
												<div class="">
													<br>
													<div class="col-sm-3 col-xs-12">
														<div class="form-group clearfix">
															<label class="control-label">Item Weight</label>
															<br>
															<div class="custom-input">
																<input class="form-control required float usename-#weight#" placeholder="Enter weight" name="weight" id="pe_weight" >
															</div>
														</div>
													</div>
													<div class="col-sm-3 col-xs-12">
														<div class="form-group clearfix">
															<label class="control-label">Length</label>
															<br>
															<div class="custom-input">
																<input class="form-control required float usename-#length#" placeholder="Enter length" name="length" id="pe_length" >
															</div>
														</div>
													</div>
													<div class="col-sm-3 col-xs-12">
														<div class="form-group clearfix">
															<label class="control-label">Height</label>
															<br>
															<div class="custom-input">
																<input class="form-control required float usename-#height#" placeholder="Enter height" name="height" id="pe_height">
															</div>
														</div>
													</div>
													<div class="col-sm-3 col-xs-12">
														<div class="form-group clearfix">
															<label class="control-label">Width</label>
															<br>
															<div class="custom-input">
																<input class="form-control required float usename-#width#" placeholder="Enter width" name="width" id="pe_width">
															</div>
														</div>
													</div>
												</div>
												<div class="col-sm-12 col-xs-12">
									               <div class="form-group">
									                  <div class="checkbox curtomlabel">
									                     <label>
									                     <input type="checkbox" name="consolidate_check" value="on" @if(Session::get('consolidate_item') == 'on') checked="checked" @endif >
									                     <p >Consolidate my items and ship them together when possible.</p>
									                     </label>
									                  </div>
									               </div>
									            </div>
												<div class="col-sm-12 col-xs-12">
													<div class="form-group clearfix">
														<button type="submit" class="custom-btn1 btn" id="Calculate3">Calculate Now</button>
														<a class="custom-btn1 btn" id="Calculate1" htef="javascript:void(0)" onclick="return form_reset('delivery_form')">Reset</a>
													</div>
												</div>
											</form>
										</div>
									</div>
									<!-- End send package -->
								</div>
								<!-- end Tab panes -->
								<!-- local delivery form -->
								<div class="row" id="local_delivery" style="display: none;">
									<form name="local_delivery_form" id="local_delivery_form" class="custom-select">
										<!-- <input type="hidden" name="travelMode" value="" id="delivery_travelMode"> -->
										<div class="col-sm-12 col-xs-12">
											<br>
											<label class="control-label"> Drop off Address</label>
										</div>
										<div class="col-sm-12 col-xs-12">
											<div class="row">
												<br>
												<input type="hidden" value='{"id": "56bdbfb4cf32079714ee89ca", "name": "Ghana", "state_available": "1"}' id="pp_pickup_country5" name="country">
												<div class="col-sm-4 col-xs-12 custom-input">
													<input type="text" name="pickup_address" placeholder="Enter drop off address" class="form-control required usename-#pickup address#">
												</div>
												<div class="col-sm-4 col-xs-12">
													<select name="state" class="form-control required left-disabled" id="pp_pickup_state5" onchange="get_city('pp_pickup_state5','pp_pickup_city5','pp_pickup_city5','')">
														<option value="">Select Region</option>
													</select>
												</div>
												<div class="col-sm-4 col-xs-12">
													<select  name="city" class="form-control required chosen-select" id="pp_pickup_city5">
														<option value="">Select City</option>
													</select>
												</div>
											</div>
										</div>
										<div class="col-sm-12 col-xs-12">
											<br>
											<label class="control-label">Pickup Address</label>
										</div>
										<div class="col-sm-12 col-xs-12">
											<div class="row">
												<br>
												<input type="hidden" value='{"id": "56bdbfb4cf32079714ee89ca", "name": "Ghana", "state_available": "1"}' id="pp_pickup_country6" name="country2">
												<div class="col-sm-4 col-xs-12 custom-input">
													<input type="text" name="dropoff_address" class="form-control required usename-#pickup address#" placeholder="Enter pickup address">
												</div>
												<div class="col-sm-4 col-xs-12">
													<select name="state2" class="form-control required left-disabled" id="pp_pickup_state6" onchange="get_city('pp_pickup_state6','pp_pickup_city6','pp_pickup_city6','')">
														<option value="">Select State/Region</option>
													</select>
												</div>
												<div class="col-sm-4 col-xs-12 ">
													<select  name="city2" class="form-control required chosen-select" id="pp_pickup_city6">
														<option value="">Select City</option>
													</select>
												</div>
											</div>
										</div>

										<div class="col-sm-12 col-xs-12">
											<br />
											<div class="form-group">
												<label class="control-label">Select Package Category</label>
												<select name="category" id="" class="form-control required">
													<option value="">Select Category</option>
													<?php foreach ($category_local as $key) {?>
													<option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}"}' class="">{{$key->Content}}</option>
													<?php }?>
												</select>
											</div>
											<br />
										</div>

										<div class="col-sm-6 col-xs-12">
											<label class="control-label">Item Value<small>&nbsp;</small><span class="red-star"> *</span></label>
											
											<div class="custom-input" style="margin-top: 6px;">
												<input type="numeric" name="pe_product_cost" class="form-control required usename-#item value#" placeholder="Item Value">
											</div>
										</div>

										<!-- <div id="localdelivery_weight_div">
											<br>
											<div class="col-sm-6 col-xs-12">
												<label class="control-label">Total weight of item/all items</label>
												<br>
												<div class="custom-input">
													<input type="numeric" name="weight" class="form-control required usename-#weight#">
												</div>
											</div>
											<div class="col-sm-6 col-xs-12">
												<label class="control-label">Weight unit of all Item(s)</label>
												<br>
												<select name="weight_unit" class="form-control required usename-#weight_unit#">

													<option value="kg" selected="selected" >in kg</option>
													<option value="lbs" >in lbs</option>
												</select>
											</div>
										</div> -->

										<!-- ch -->
										<div class="col-sm-6 col-xs-12">
											<label class="control-label">
												
												Total weight of item/all items&nbsp;
												<a data-toggle="popover" data-trigger="focus"  data-container="body" data-placement="top" type="button" data-html="true" href="javascript:void(0)" id="login1" class="info_icon" style="margin-top:0;color:black;"><img alt="Total weight of item" src="theme/web/images/info_icon.png">
												</a> </label>
												<div id="popover-content-login1" class="hide">
													<h4>Suggested item weights</h4>
													<small>These are generic weights and may not reflect the actual weight of your item</small>
													<table class="table table-bordered table-striped">
														<thead>
															<tr>
																<th width="150">Item Name</th>
																<th width="60">Weight/Unit</th>
															</tr>
														</thead>
														<tbody>
															@if(count($item) > 0)
															@foreach($item as $key)
															<tr>
																<td>{{ucfirst($key->item_name)}} </td>
																<td >{{number_format($key->lbsWeight,2)}} Lbs / {{number_format($key->kgWeight,2)}} Kg</td>
															</tr>
															@endforeach
															@endif
															@if($item_count > 5)
															<tr>
																<td colspan="2"><a href="javascript::void(0)" class="label label-info" onclick="return show_hide('more_items');">More</a></td>
															</tr>
															@endif
														</tbody>
													</table>
												</div>
												<div  class="custom-group">
													<div class="custom-input">
														<input  type="numeric" name="weight" class="form-control required float usename-#weight#" placeholder="Total Weight of all Item(s)">
													</div>
													<select name="weight_unit" style="margin-top:10px;" class="form-control required usename-#weight_unit#">
														<option value="lbs" >in lbs</option>
														<option value="kg" selected="selected" >in kg</option>
													</select>
												</div>
											</div>
											
										<!-- ech end -->


										<div class="col-sm-12 col-xs-12">
											<br>
											<div class="form-group clearfix">
												<button type="submit" class="custom-btn1 btn" id="Calculate2">Calculate Now</button>
												<a class="custom-btn1 btn" id="Calculate1" htef="javascript:void(0)" onclick="return form_reset('local_delivery_form')">Reset</a>
											</div>
										</div>









									</form>
								</div>
								<!-- end local delivery form -->
							</div>
							<!-- end content -->
						</div>
						<!-- form box end-->
						<!-- calculation box-->
						<div class="col-sm-4 col-xs-12 calcu1" >
							<div class="calcu" id="rescal">
								<div class="blue-bg text-center color-white">
<!--
									<h4 id="estimated_res">Estimated Shipping Cost</h4>
-->
									<h4 id="estimated_res"> Estimated Total Cost </h4>
									<h1 class="large_price">$<span id="pe_calculated_total_amount">--</span></h1>
									<p class="color-white" id="pe_calculated_total_amount_in_ghana"></p>
									<!-- <p class="color-white" id="pe_calculated_total_amount_in_peso"></p> -->
									<p class="color-white" id="pe_calculated_total_amount_in_uk"></p>
									<p class="color-white" id="pe_calculated_total_amount_in_cad"></p>
									<p class="color-white" id="pe_calculated_total_amount_in_kenya"></p>
								</div>
								<div class="">
									<!-- <div class="col-xs-12">
										<br />
										<div class="pull-left">
											<label class="control-label">Distance</label>
										</div>
										<div class="pull-right">
											<label class="control-label"><span id="pe_calculated_distance">-- </span>&nbsp;km</label>
										</div>
									</div> -->
									<div class="col-xs-12">
										<p style="color: white;">Below are included in the quote above</p>
									</div>
									<div id= "bfm_res" style="display: none;">
										<div class="col-xs-12">
											<br />
											<div class="pull-left">
												<label class="control-label">Item Price</label>
											</div>
											<div class="pull-right">
												<label class="control-label">$<span id="pe_item_price">-- </span></label>
											</div>
										</div>

										<div class="col-xs-12">

											<div class="pull-left">
												<label class="control-label">Fees and Delivery</label>
											</div>
											<div class="pull-right">
												<label class="control-label">$<span id="pe_fess">-- </span></label>
											</div>
										</div>
									</div>
									
									<div id="intial_shipping_div" style="display: none;">
										<div class="col-xs-12">
											<div class="pull-left">
												<label class="control-label">Int’l Shipping</label>
											</div>
											<div class="pull-right">
												<label class="control-label">$<span id="intial_shipping">-- </span></label>
											</div>
										</div>
									</div>
									
									<div id="custom_duty_div" style="display: none;">
										<div class="col-xs-12">
											<div class="pull-left">
												<label class="control-label">Customs Duty</label>
											</div>
											<div class="pull-right">
												<label class="control-label">$<span id="custom_duty">-- </span></label>
											</div>
										</div>
									</div>
									
									<div id="insurance_div" style="display: none;">
										<div class="col-xs-12">
											<div class="pull-left">
												<label class="control-label">Insurance</label>
											</div>
											<div class="pull-right">
												<label class="control-label">$<span id="insurance">-- </span></label>
											</div>
										</div>
									</div>
									<div id="ups_rate_div" style="display: none;">
										<div class="col-xs-12">
											<div class="pull-left">
												<label class="control-label">Shipping within USA</label>
												
											</div>
											<div class="pull-right">
												<label class="control-label">$<span id="ups_rate">-- </span></label>
											</div>
										</div>
									</div>
									
									

									<div class="col-xs-12">
										<div class="pull-left">
											<label class="control-label">Total Weight</label>
										</div>
										<div class="pull-right">
											<label class="control-label"><span id="pe_calculated_item_weight">--</span>&nbsp;lbs</label>
										</div>
									</div>
									
									
									
									
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="clearfix"><br></div>
							<div id="requestBtn" style="display: none" >
								<input type="hidden" value="" id="requestData">
								<input type="submit" id="createRequest"  class="custom-btn1 btn btn-success" value="Create Request">
							</div>
						</div>
						<!-- calculation box end-->
					</div> </div>
					<!-- more items -->
					<div  id="more_items">
						<div class="col-sm-12 col-xs-12 mob-right">
							<h3 class="modal-title text-left" style="padding-left:5px;color:#31C3F3" id="myModalLabel">Suggest Item weights<span class="pull-right"><a onclick="return show_hide('price_estimator')" href="javascript::void(0)"  class="btn btn-default blue-btn" style="color:#FFFFFF;"><i class="fa fa-arrow-left" aria-hidden="true" ></i>&nbsp;&nbsp;Back</a>&nbsp;&nbsp; </span></h3>
							<p>&nbsp;These are generic weights and may not reflect the actual weight of your item.</p>
							<br>
							<table class="custom-table table table-bordered">
								<thead>
									<tr>
										<th>Item Name</th>
										<th>Weight/Lbs</th>
										<th>Weight/Kg</th>
									</tr>
								</thead>
								<?php $no = 0;?>
								@if(count($items) > 0)
								@foreach($items as $key)
								<?php $no = $no + 1;?>
								<tbody>
									<tr >
										<td>
											{{ucfirst($key->item_name)}}
										</td>
										<td>
											@if($key->lbsWeight != '')
											{{number_format($key->lbsWeight,2)}}
											@endif
										</td>
										<td>
											@if($key->lbsWeight != '')
											{{number_format($key->kgWeight,2)}}
											@endif
										</td>
									</tr>
								</tbody>
								@endforeach
								@endif
							</table>
						</div>
					</div>
					<!-- end more items -->
				</div>
				<!-- Modal -->
				<div id="myModal" class="modal fade" role="dialog">
					<div class="modal-dialog">
						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-body login-bg" id="signUpForm" style="margin-top: 0px !important;margin-bottom:0px !important">
								<div class="heading-blue">Join</div>
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<form method="POST" action="{{url('/signup') }}" accept-charset="UTF-8" id="signup" class="form-vertical label-less business" enctype="multipart/form-data" data-parsley-validate="">
									<input name="_token" value="{{ csrf_token() }}" type="hidden">
									<div class="col-sm-6" id="first_name">
										<div class="form-group">
											<input class="form-control" placeholder="First Name" maxlength="60" name="firstName" value="" data-parsley-required-message="Please enter first name" required="" type="text">
											<p class="help-block red" id='er_firstName' style="color:red">
												@if($errors->has('firstName')){{ $errors->first('firstName') }}@endif
											</p>
										</div>
									</div>
									<div class="col-sm-6" id="last_name">
										<div class="form-group">
											<input class="form-control" placeholder="Last Name" maxlength="60" name="lastName" value="" data-parsley-required-message="Please enter last name" required="" type="text">
											<p class="help-block red" id='er_lastName' style="color:red">
												@if($errors->has('lastName')){{ $errors->first('lastName') }}@endif
											</p>
										</div>
									</div>
									<div class="col-sm-12 col-xs-12">
										<div class="form-group">
											<input class="form-control" placeholder="Email" id="email" maxlength="60" name="email" value="" data-parsley-required-message="Please enter email" required="" data-parsley-type="email" data-parsley-email-message="Must be a valid email address" type="text">
											<p class="help-block red" id='er_email' style="color:red">
												@if($errors->has('email')){{ $errors->first('email') }}@endif
											</p>
										</div>
									</div>
									<div class="col-sm-12 col-xs-12">
										<div class="form-group">
											<input class="form-control" placeholder="Password" name="password" id="password" data-parsley-required-message="Please enter password" required="" type="password">
											<p class="help-block red" id='er_password' style="color:red">
												@if($errors->has('password')){{ $errors->first('password') }}@endif
											</p>
										</div>
									</div>
									<div class="col-sm-12 col-xs-12">
										<div class="form-group">
											<input class="form-control" placeholder="Confirm Password" maxlength="80" name="confirmPassword" required="" data-parsley-required-message="Please enter confirm password" data-parsley-equalto="#password" data-parsley-equalto-message="Password and confirm password mismatch" type="password">
											<p class="help-block red" id='er_confirmPassword' style="color:red">
												@if($errors->has('confirmPassword')){{ $errors->first('confirmPassword') }}@endif
											</p>
										</div>
									</div>
									<div class="col-sm-12 col-xs-12">
										<input name="usertype" value="requester" type="hidden">
									</div>
									<div class="col-sm-12 col-xs-12">
										<p class="color-white text-left">By signing up, you agree to Aquantuo’s <a href="{{ url('/terms-and-conditions') }}" target="_blank">Terms and
											Conditions</a> and <a href="{{ url('/privacy-policy') }}" target="_blank">Privacy Policy</a>
										</p>
									</div>
									<div class="col-sm-12 col-xs-12">
										<div class="form-group">
											<button class="custom-btn1 btn-block" style="margin-bottom:0;">
											Signup
											<div class="custom-btn-h"></div>
											</button><div class="error-msg"></div>
										</div>
										<div class="clearfix"></div>
										<p class="text-center" style="color:white;margin-top:10px; margin-bottom:0px;" align="center">Already have an account? <a id="signin" href="javascript::void()">Sign in</a></p>
									</div>
									<div class="clearfix"></div>
								</form>
							</div>
							<div class="login-bg" id="signInForm" style="margin-top: 0px !important; margin-bottom: 0px !important; display: none;">
								<div class="heading-blue">
									Sign in
								</div>
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<form method="POST" action="{{ url('/login') }}" accept-charset="UTF-8" name="ServicesForm" id="ServicesForm" class="form-vertical" data-parsley-validate="">
									<input name="_token" value="{{ csrf_token() }}" type="hidden">
									<input name="price_estimator" value="price_estimator" type="hidden">
									<input name="price_estimator_form" value="online" type="hidden" id="price_estimator_form">

									<div class="col-sm-12">
										<div class="form-group">
											<label for="Email" class="control-label">Email</label>
											<div class="input-img-hldr">
												<input class="form-control" placeholder="Email" id="email" required="" name="email" type="text" data-parsley-required-message="Please enter email" data-parsley-type="email" data-parsley-email-message="Must be a valid email address">
												<span><i class="fa fa-envelope input-img" aria-hidden="true"></i></span>
												<p class="help-block red" id='er_email' style="color:red">
													@if($errors->has('email')){{ $errors->first('email') }}@endif
												</p>
											</div>
										</div>
									</div>
									<div class="col-sm-12">
										<div class="form-group">
											<label for="password" class="control-label">Password</label>
											<div class="input-img-hldr">
												<input class="form-control" required="" placeholder="Password" id="password" maxlength="80" value="" name="password" data-parsley-required-message="Please enter password" type="password">
												<span><i class="fa fa-lock input-img" aria-hidden="true"></i></span>
												<p class="help-block red" id='er_password' style="color:red">
													@if ($errors->has('password')){{ $errors->first('password') }}@endif
												</p>
											</div>
										</div>
									</div>
									<input name="session_lat" value="0" id="session_lat" type="hidden">
									<div class="col-xs-12 col-sm-12">
										<div class="form-group">
											<div class="checkbox">
												<label><input tabindex="4" class="field login-checkbox" name="remember" id="Field" type="checkbox">&nbsp;Remember me</label>
												<p class="color-blue pull-right"><a href="{{ url('/forgot-password') }}">Forgot Password?</a></p>
											</div>
										</div>
									</div>
									<div class="col-sm-12">
										<div class="form-group">
											<button class="custom-btn1 btn-block">
											Login
											<div class="custom-btn-h"></div>
											</button>
										</div>
									</div>
									<div class="clearfix"></div>
									<div class="col-sm-12">
										<div class="form-group">
											<label align="center">New to Aquantuo? <a href="javascript::void()" id="signUp">Sign Up</a></label>
										</div>
									</div>
								</form>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
				</div>
			</div>




@endsection
@section('script')
@parent
{!! Html::script('theme/web/js/utility.js') !!}
{!! Html::script('theme/web/js/validation.js') !!}
{!! Html::script('theme/web/js/parsley.min.js') !!}
{!! Html::style('theme/admin/choose-multiple/bootstrap-chosen.css') !!}
{!! Html::script('theme/admin/choose-multiple/chosen.jquery.js') !!}




<script>
function addResClass(){
	$('.calcu').addClass('bounceIn');
}
function get_postcode(id,search){
	get_state2('pp_pickup_country'+id,'pp_pickup_state'+id,'pp_pickup_city'+id,'pp_pickup_state'+id,'','','',id,search);
}

jQuery(document).ready(function ($) {
	$(".chosen-select").chosen({no_results_text: "Oops, nothing found!"});
	$('#online').trigger('reset');
	$('#buyforme_form').trigger('reset');
	$('#delivery_form').trigger('reset');
});


/*function set_drop_down()
{
	$(".chosen-select").chosen({no_results_text: "Oops, nothing found!"})
}*/






$('.loader-box').click(function () {
	$(this).addClass('load').siblings().removeClass('load');
setTimeout( function(){
		$('.loader-box').removeClass('load').siblings().removeClass('load');
},500);
});
$('.li-loader-box').click(function () {
	$(this).addClass('load').siblings().removeClass('load');
setTimeout( function(){
		$('.li-loader-box').removeClass('load').siblings().removeClass('load');
},700);
});
function show_hide(sec)
{
	if(sec == 'more_items'){
		$('#more_items').show();
		$('#price_estimator').hide();
	}else{
		$('#price_estimator').show();
		$('#more_items').hide();
	}
}
show_hide('price_estimator');
get_state('pp_pickup_country5','pp_pickup_state5','pp_pickup_city5','pp_pickup_state5','','','yes');
get_state('pp_pickup_country6','pp_pickup_state6','pp_pickup_city6','pp_pickup_state6','','','yes');
	$('#first_div').click(function(){
		$('#secound_div').removeClass('translateXC');
	$('#first_div').removeClass('translateXC');
	$('#third_div').removeClass('translateXC');
	$('#secound_div').removeClass('translateXA');
	$('#first_div').removeClass('translateXA');
	$('#third_div').removeClass('translateXA');
	});
$('#secound_div').click(function(){
	$(this).removeClass('translateXC');
	$('#first_div').removeClass('translateXC');
	$('#third_div').removeClass('translateXC');
	if($(this).width() > 20){
		$(this).addClass('translateXA');
		$('#first_div').addClass('translateXA');
		$('#third_div').addClass('translateXA');
	}
	else{
		$(this).removeClass('translateXA');
		$('#first_div').removeClass('translateXA');
		$('#third_div').removeClass('translateXA');
	}
});
$('#third_div').click(function(){
	if($(this).width() > 20){
		$(this).addClass('translateXC');
		$('#first_div').addClass('translateXC');
		$('#secound_div').addClass('translateXC');
	}
	else{
		$(this).removeClass('translateXC');
		$('#first_div').removeClass('translateXC');
		$('#secound_div').removeClass('translateXC');
	}
});
// tooltip --------//
$("[data-toggle=popover]").each(function(i, obj) {
$(this).popover({
html: true,
content: function() {
var id = $(this).attr('id')
return $('#popover-content-' + id).html();
}
});
});
$(document).ready(function(){
	$("#createRequest").click(function(){
		form_submit($("#requestData").val());
	});
	$("#signin").click(function(){
		$("#signInForm").css('display','block');
		$("#signUpForm").css('display','none');
	});
	$("#signUp").click(function(){
		$("#signInForm").css('display','none');
		$("#signUpForm").css('display','block');
	});
});
function form_submit(type){
$.ajax({
url: SITEURL+'post-form',
data: $('#'+type).serialize()+'&type='+type,
method: 'post',
dataType: 'json',
success: function(res){
	if(res.success == 1){

		if(res.result.type == 'online'){
			window.location.href = SITEURL+'on-line-purchases';
		}else if(res.result.type == 'buyforme_form'){
			window.location.href = SITEURL+'buy-for-me';
		}else{
			window.location.href = SITEURL+'new-prepare-request';
		}
	}else{
		$("#myModal").modal('show');
	}
	}
});
}
var error = '<?php echo $errors->has(); ?>';
$(document).ready(function(){
if(error){
$("#myModal").modal('show');
}
});
function show_request(request){
if(request == 'buy'){
$("#bt2").trigger("click");
}else{
$("#bt").trigger("click");
}
}
new Validate({
FormName : 'online',
ErrorLevel : 1,
validateHidden: false,
callback : function(){
price_estimatory('online');
}
});
new Validate({
FormName : 'buyforme_form',
ErrorLevel : 1,
validateHidden: false,
callback : function(){
price_estimatory('buyforme_form');
}
});
new Validate({
FormName : 'delivery_form',
ErrorLevel : 1,
validateHidden: false,
callback : function(){
price_estimatory('delivery_form');
}
});
new Validate({
FormName : 'local_delivery_form',
ErrorLevel : 1,
validateHidden: false,
callback : function(){
price_estimatory('local_delivery_form');
}
});
function price_estimatory(type){
if(type == 'delivery_form'){
var ajaxurl ="delivery-price-estimator";
}else if(type == 'local_delivery_form'){
var ajaxurl ="localdelivery-price-estimator";
}else{
var ajaxurl ="online-buy-price-estimator";
}
$("#requestData").val(type);
$("#custom_duty_div").hide(); 
	$("#insurance_div").hide(); 
$.ajax({
url: SITEURL+ajaxurl,
data: $('#'+type).serialize(),
method: 'post',
dataType: 'json',
success: function(res){
if(res.success == 1){
	$('#pe_calculated_total_amount').html(parseFloat(res.total_amount).toFixed(2));
	$('#pe_calculated_total_amount_in_ghana').html(res.ghana_total_amount);
	$('#pe_calculated_total_amount_in_peso').html(res.philipins_cost);
	$('#pe_calculated_total_amount_in_uk').html(res.uk_cost);
	$('#pe_calculated_total_amount_in_cad').html(res.canadian_cost);
	$('#pe_calculated_total_amount_in_kenya').html(res.kenya_cost);
	$('#pe_calculated_item_volume').html(res.volume+' Cu. '+"Cm");
	$('#pe_calculated_item_weight').html(parseFloat(res.total_weight).toFixed(2));
	$('#pe_calculated_distance').html(parseFloat(res.total_distance).toFixed(2));
	if(ajaxurl == 'online-buy-price-estimator' && res.item_cost > 0){
		$("#pe_item_price").html(parseFloat(res.item_cost).toFixed(2));

		$("#pe_fess").html(parseFloat(res.ProcessingFees).toFixed(2))
		request_type('bfm');
	}
	$("#requestBtn").show();
	$("#custom_duty_div").hide(); 
	$("#insurance_div").hide(); 
	$("#ups_rate_div").hide(); 
	$("#custom_duty_div").hide();
	$('#intial_shipping_div').hide();
	
	if (type == 'delivery_form') {
		if (res.calculationinfo.shippingcost > 0) {
			$('#intial_shipping_div').show();
			$('#intial_shipping').html(parseFloat(res.calculationinfo.shippingcost).toFixed(2));
		}
	}
	
	if (res.shipping_cost > 0) {
		$('#intial_shipping_div').show();
		$('#intial_shipping').html(parseFloat(res.shipping_cost).toFixed(2));
	}
	 
	if(res.custom_duty>0){
		$("#custom_duty_div").show(); 
		$('#custom_duty').html(parseFloat(res.custom_duty).toFixed(2));
	}
	if(res.insurance>0){
		$("#insurance_div").show(); 
		$('#insurance').html(parseFloat(res.insurance).toFixed(2));
	}
	
	if(res.ups_rate>0){
		$("#ups_rate_div").show(); 
		$('#ups_rate').html(parseFloat(res.ups_rate).toFixed(2));
	}
	$('html, body').animate({ scrollTop: 0 }, 'slow');



}else{
alert(res.msg);
}
}
});
}
function check_insurance(type)
{
var value =document.getElementById(type+'_insurance').checked;
if(value == true) {
$('#'+type+'_product_section').show();
}else {
$('#'+type+'_product_section').hide();
}
}
check_insurance('buy');
check_insurance('online');
function set_form(type)
{
	var categoryData = '<?php echo $category ?>';
	categoryData = JSON.parse(categoryData);
	console.log('categoryData',categoryData);
	if(type == 'air' || type == 'sea'){
		
	var checkType = type;
	if(checkType=='sea'){
		checkType  = 'ship';
	}
	// online 
	var airCaregoryHtml = '<select name="category" id="package_category" class="form-control required">';
		airCaregoryHtml += '<option value="">Select Category</option>';
		categoryData.forEach(function(entry) {
			if(entry.TravelMode==checkType){
				airCaregoryHtml += '<option value={"id":"'+entry._id+'"} class="travel-mode-'+entry.TravelMode+'">'+entry.Content+'</option>';
				
			}
		});
		airCaregoryHtml += '</select>';
	  $(".airCategoryDropDown").html(airCaregoryHtml);
	
	// buy4 me
	var airCaregoryHtml = '<select name="category" id="package_category2" class="form-control required">';
		airCaregoryHtml += '<option value="">Select Category</option>';
		categoryData.forEach(function(entry) {
			
			if(entry.TravelMode==checkType){
				airCaregoryHtml += '<option value={"id":"'+entry._id+'"} class="travel-mode-'+entry.TravelMode+'">'+entry.Content+'</option>';
				
			}
		});
		airCaregoryHtml += '</select>';
	 $(".airCategoryDropDown2").html(airCaregoryHtml);
	 
	// send a package
	var airCaregoryHtml = '<select name="category" id="package_category3" class="form-control required">';
		airCaregoryHtml += '<option value="">Select Category</option>';
		categoryData.forEach(function(entry) {
			
			if(entry.TravelMode==checkType){
				airCaregoryHtml += '<option value={"id":"'+entry._id+'"} class="travel-mode-'+entry.TravelMode+'">'+entry.Content+'</option>';
				
			}
		});
		airCaregoryHtml += '</select>';
	 $(".airCategoryDropDown3").html(airCaregoryHtml);
	}
	
	if(type == 'air'){
		
		$('#tabcontent').show();
		$('#tabmenu').show();
		$('#default_text').hide();
		$('#first_sec').addClass('active');
		$('#second_sec').removeClass('active');
		$('#third_sec').removeClass('active');
		$('#online_travelMode').val('air');
		$('#buyforme_travelMode').val('air');
		$('#delivery_travelMode').val('air');
		$('#online_weight_div').show();
		$('#buy_weight_div').show();
		$('#local_delivery').hide();
		toggle_category('.travel-mode-air','.travel-mode-ship','#package_category');
		toggle_category('.travel-mode-air','.travel-mode-ship','#package_category2');
		toggle_category('.travel-mode-air','.travel-mode-ship','#package_category3');
	}else if(type == 'sea'){
		
		$('#tabcontent').show();
		$('#tabmenu').show();
		$('#default_text').hide();
		$('#first_sec').addClass('active');
		$('#second_sec').addClass('active');
		$('#first_sec').removeClass('active');
		$('#third_sec').removeClass('active');
		$('#online_travelMode').val('ship');
		$('#buyforme_travelMode').val('ship');
		$('#delivery_travelMode').val('ship');
		//$('#online_weight_div').hide();
		//$('#buy_weight_div').hide();
		$('#local_delivery').hide();
		toggle_category('.travel-mode-ship','.travel-mode-air','#package_category');
		toggle_category('.travel-mode-ship','.travel-mode-air','#package_category2');
		toggle_category('.travel-mode-ship','.travel-mode-air','#package_category3');
	}else if(type == 'delivery'){
		$('#tabcontent').hide();
		$('#tabmenu').hide();
		$('#default_text').hide();
		$('#third_sec').addClass('active');
		$('#first_sec').removeClass('active');
		$('#second_sec').removeClass('active');
		$('#local_delivery').show();
		$('#estimated_res').html('Estimated Total Cost');
		$("#bfm_res").hide();
		$("#price_estimator_form").val('local');

		}
}
//set_form('air');
function toggle_category(showid,hideid,id){
$(id).val('');
$(showid).attr('disabled',false);
$(showid).show();
$(hideid).attr('disabled',true);
$(hideid).hide();
}
function form_reset(formid){
$("#"+formid).trigger('reset');
}

function request_type(type)
{
	if(type == 'bfm'){
		$('#estimated_res').html('Estimated Purchase and Delivery cost');
		$("#bfm_res").show();

	}else{
		$('#estimated_res').html('Estimated Total Cost');
		$("#bfm_res").hide();
	}

	if(type == 'bfm'){
		$("#price_estimator_form").val('buyforme');
	}else if(type == 'op'){
		$("#price_estimator_form").val('online');
	}else if(type == 'delivery'){
		$("#price_estimator_form").val('delivery');
	}


}

</script>
@endsection
			<!-- 5a5e0205360d5c21217a8770 -->

