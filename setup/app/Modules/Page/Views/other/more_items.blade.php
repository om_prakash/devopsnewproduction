@extends('Page::layout.one-column-page')
@section('content')
<div class="container">
  <div class="row">
  	<div class="col-md-12">
      <span>
  		<h4 style="color: #31B0D5;">Total Weight of all Item(s) <span class="pull-right"><a href="{{url('price-estimator')}}"  class="btn btn-default blue-btn" style="color:#FFFFFF;"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;&nbsp;Back</a>&nbsp;&nbsp; </span></h4>
      
          
            
         

          <br>
        <table class="custom-table table table-bordered">
                    <thead>
                        <tr>
                           <th>Item Name</th>
                           <th>Weight/Lbs</th>
                           <th>Weight/Kg</th>
                        </tr>
                    </thead>
                    <?php $no = 0 ;?>
                    @if(count($items) > 0)
                    	@foreach($items as $key)
                    	<?php $no = $no+1; ;?>
                    <tbody>
                    	<tr >
                    		
                    		<td>
                    			{{ucfirst($key->item_name)}}
                    		</td>
                    		<td>
                        @if($key->lbsWeight != '')
                    			{{number_format($key->lbsWeight,2)}} 
                        @endif
                    		</td>
                        <td>
                        @if($key->lbsWeight != '')
                        {{number_format($key->kgWeight,2)}} 
                        @endif
                        </td>
            			</tr>
                    </tbody>
                    @endforeach
				@endif
        </table>
    </div>
  </div>
</div>
@endsection