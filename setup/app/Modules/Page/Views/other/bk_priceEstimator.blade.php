@extends('Page::layout.one-column-page')
@section('content')

<div class="container section-custom price">
	<div class="row">

    	<div class="price_estimator" id="price_estimator">
    			<div class=" clearfix">
    				<div class="col-sm-8 col-xs-12 mob-right">
    					<h3 class="modal-title text-left" style="padding-left:5px;" id="myModalLabel">Price Estimator</h3>
    				</div>
    			<!-- form box -->
                <div class="col-sm-8 col-xs-12 mob-right no-gutter ">
                
                	<div class="m-scroll">
	                	<div class="col-md-4" id="first_div" onclick="return set_form('air')" >
	                		<div class="air_div_css block-space loader-box" id="first_sec">
								<h4>Air/Air Freight <span class="pull-right"><i class="fa fa-plane fa_size" aria-hidden="true" ></i></span></h4>
								<p>Get it in 13 days or less</p>
								<span class="loader" style="display:none;">
									<div class="wrap"> 
										<div class="bar1"></div> 
										<div class="bar2"></div> 
										<div class="bar3"></div> 
										<div class="bar4"></div> 
										<div class="bar5"></div> 
										<div class="bar6"></div> 
										<div class="bar7"></div> 
										<div class="bar8"></div>
										<div class="bar7"></div> 
										<div class="bar8"></div>
										<div class="bar9"></div> 
										<div class="bar10"></div>
									</div>
								</span>
	                		</div>
	                	</div>

	                	<div class="col-md-4" id="secound_div" onclick="return set_form('sea')" >
	                		<div style="" class="sea_div_css block-space loader-box" id="second_sec">
								<h4>Ocean/Sea Freight <span class="pull-right"><i class="fa fa-ship fa_size" aria-hidden="true"></i></span></h4>
								<p>Get it in 6 to 8 weeks</p>
								<span class="loader" style="display:none;">
									<div class="wrap"> 
										<div class="bar1"></div> 
										<div class="bar2"></div> 
										<div class="bar3"></div> 
										<div class="bar4"></div> 
										<div class="bar5"></div> 
										<div class="bar6"></div> 
										<div class="bar7"></div> 
										<div class="bar8"></div>
										<div class="bar7"></div> 
										<div class="bar8"></div>
										<div class="bar9"></div> 
										<div class="bar10"></div>
									</div>
								</span>
	                		</div>
	                	</div>

	                	<div class="col-md-4" id="third_div" onclick="return set_form('delivery')" >
	                		<div style="" class="delivery_div_css block-space loader-box" id="third_sec">
							<h4>Local Delivery <span class="pull-right"><i class="fa fa-motorcycle fa_size" style="margin-left:40px" aria-hidden="true"></i></span></h4>
								<p>Pick up and deliver within Ghana</p>
								<span class="loader" style="display:none;">
									<div class="wrap"> 
										<div class="bar1"></div> 
										<div class="bar2"></div> 
										<div class="bar3"></div> 
										<div class="bar4"></div> 
										<div class="bar5"></div> 
										<div class="bar6"></div> 
										<div class="bar7"></div> 
										<div class="bar8"></div>
										<div class="bar7"></div> 
										<div class="bar8"></div>
										<div class="bar9"></div> 
										<div class="bar10"></div>
									</div>
								</span>
	                		</div>
	                	</div>
	                </div>

                	<!-- tabe content -->
                	<div class="col-sm-12 col-xs-12 text-center" id="default_text"><br />
                	<h3  class=" bottom-space">Please choose an option from above that best fits your delivery need.</h3>
                	</div>
                	
                	<div class="col-sm-12 col-xs-12 ">
					  <!-- Nav tabs menu -->
					  	<div id="tabmenu" style="display: none;">
						  <ul class="nav nav-tabs my-tabs" role="tablist">
						  	
								<li role="presentation"  class="active li-loader-box" id="menu1"><a href="#home" aria-controls="home" role="tab" data-toggle="tab" id="menu32"> <i class="fa fa-shopping-cart"></i>I am buying online myself</a>
								<span class="loader" style="display:none;">
									<div class="wrap"> 
										<div class="bar1"></div> 
										<div class="bar2"></div> 
										<div class="bar3"></div> 
										<div class="bar4"></div> 
										<div class="bar5"></div> 
										<div class="bar6"></div> 
										<div class="bar7"></div> 
										<div class="bar8"></div>
										<div class="bar7"></div> 
										<div class="bar8"></div>
										<div class="bar9"></div> 
										<div class="bar10"></div>
									</div>
								</span>
							</li>
						    

						    
								<li role="presentation" class="li-loader-box" id="menu2"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab"><i class="fa fa-shopping-bag"></i> I need Aquantuo to buy for me</a>
								<span class="loader" style="display:none;">
									<div class="wrap"> 
										<div class="bar1"></div> 
										<div class="bar2"></div> 
										<div class="bar3"></div> 
										<div class="bar4"></div> 
										<div class="bar5"></div> 
										<div class="bar6"></div> 
										<div class="bar7"></div> 
										<div class="bar8"></div>
										<div class="bar7"></div> 
										<div class="bar8"></div>
										<div class="bar9"></div> 
										<div class="bar10"></div>
									</div>
								</span>
							</li>
						    
						    
								<li role="presentation" class="li-loader-box" id="menu3"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab" id="menu33"><i class="fa fa-file"></i> I need to move a package from one country to another</a>
								<span class="loader" style="display:none;">
									<div class="wrap"> 
										<div class="bar1"></div> 
										<div class="bar2"></div> 
										<div class="bar3"></div> 
										<div class="bar4"></div> 
										<div class="bar5"></div> 
										<div class="bar6"></div> 
										<div class="bar7"></div> 
										<div class="bar8"></div>
										<div class="bar7"></div> 
										<div class="bar8"></div>
										<div class="bar9"></div> 
										<div class="bar10"></div>
									</div>
								</span>
							</li>
						  </ul>
						</div>
						<!-- end Nav tabs menu -->

						

					  <!-- Tab panes -->
						<div class="tab-content" id="tabcontent" style="display: none;">
						    <div role="tabpanel" class="tab-pane active" id="home">
						    	<div class="row">
			                		<form name="online" id="online" class="custom-select">

			                		<input type="hidden" name="request_type" value="online">
			                		<input type="hidden" name="travelMode" value="" id="online_travelMode">
				                	<div class="col-sm-12 col-xs-12">
				                		<br>
				                		<label class="control-label">Destination Country</label>
				                	</div>

				                	<div >
				                		<div class="col-sm-4 col-xs-12">
					                		<select name="country" class="form-control required" id="pp_pickup_country" onchange="get_state('pp_pickup_country','pp_pickup_state','pp_pickup_city','pp_pickup_state','')">
						                            <option value="">Select Country</option>
						                            @foreach($country as $key)
						                             <option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}","state_available":"{{$key->state_available}}"}'>{{$key->Content}}</option>
						                            @endforeach
						                    </select>
						                </div>
						                <div class="col-sm-4 col-xs-12">
							                <select name="state" class="form-control required left-disabled" id="pp_pickup_state" onchange="get_city('pp_pickup_state','pp_pickup_city','pp_pickup_city','')">
							                        <option value="">Select State</option>
							                </select>
						                </div>
						                <div class="col-sm-4 col-xs-12">
							                <select  name="city" class="form-control required" id="pp_pickup_city">
						                            <option value="">Select City</option>
						                    </select>
						                </div>
				                	</div>

				                	<div class="col-sm-12 col-xs-12">
				                        <br />
				                       <div class="form-group">
				                          	<label class="control-label">Select Package Category</label>
				                          	<select name="category" id="package_category" class="form-control required">
				                             <option value="">Select Category</option>
				                             <?php foreach ($category as $key) {?>
				                              <option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}"}' class="travel-mode-{{$key->TravelMode}}">{{$key->Content}}</option>
				                             <?php }?>
				                            </select>
				                       </div>
				                    </div>

				                	
				                  	<div id="online_weight_div">	

				                  	

				                  		<div class="col-sm-6 col-xs-12">
				                    		<label class="control-label">
				                    		<br>
				                    		Total weight of item/all items&nbsp;
				                    		<a data-toggle="popover" data-trigger="focus"  data-container="body" data-placement="top" type="button" data-html="true" href="javascript:void(0)" id="login1" class="info_icon" style="margin-top:0;color:black;"><img src="theme/web/images/info_icon.png">
											</a> </label>
											
											<div id="popover-content-login1" class="hide">
												<h4>Suggested item weights</h4>
												<small>These are suggested weights and may not reflect the actual weight of your item</small>
												<table class="table table-bordered table-striped">
													<thead>
														<tr>
															<th width="150">Item Name</th>
															<th width="60">Weight/Unit</th>
											 			</tr>
													 </thead>
													 <tbody>
													 	@if(count($item) > 0)

													 		@foreach($item as $key)
															<tr>
																<td>{{ucfirst($key->item_name)}} </td>
																<td >{{number_format($key->lbsWeight,2)}} Lbs / {{number_format($key->kgWeight,2)}} Kg</td>
															 </tr>
																@endforeach
													 		@endif
															 
															@if($item_count > 5)
															 <tr>
																<td colspan="2"><a href="javascript::void(0)" class="label label-info" onclick="return show_hide('more_items');">More</a></td>
												 			</tr>
												 			@endif
												 			
											 		</tbody>
											 </table>
											</div>
											<div  class="custom-group">
		                						<div class="custom-input">
					                    			<input  type="numeric" name="weight" class="form-control required usename-#weight#" placeholder="Total Weight of all Item(s)">
					                    		</div>
					                    		<select name="weight_unit" style="margin-top:10px;" class="form-control required usename-#weight_unit#">
					                    			<option value="lbs" >in lbs</option>
					                    			<option value="kg" >in kg</option>
					                    		</select>
				                    		</div>
				                  		</div>

				                  		<div class="col-sm-6 col-xs-3 m-padd-0">
				                    		<label class="control-label" ></label>
				                    		<br><br>
				                    		
				                   			
				                  		</div>
				                  	</div>
				                  	
					                <div class="col-sm-12 col-xs-12">
					                <br>
					                        <div class="form-group clearfix">
					                            <button class="custom-btn1 btn" id="Calculate1">Calculate Now</button>
					                            <a class="custom-btn1 btn" id="Calculate1" htef="javascript:void(0)" onclick="return form_reset('online')">Reset</a>
					                        </div>

					                        
					                </div>


					                </form>
				                </div>
						    </div>
						    <div role="tabpanel" class="tab-pane" id="profile">
						    	<!-- Buy for Me -->
			                	<div class="row">
			                		<form name="buyforme_form" id="buyforme_form" class="custom-select">
			                		<input type="hidden" name="request_type" value="buyforme">
			                		<input type="hidden" name="travelMode" value="" id="buyforme_travelMode">
			                		
			                		<!-- This is for future use -->
				                	<!-- <div class="col-sm-12 col-xs-12 col-md-12">
				                    	<br />
				                        <div class="form-group">
				                            <label class="control-label">Country Purchase</label>
				                            <br>
				                           <div class="custom-input">
				                                <input class="form-control required usename-#country purchase#" name="country_purchase" id="country_purchase">
				                           </div>
				                        </div>
				                    </div> -->


				                	<div class="col-sm-12 col-xs-12">
				                		<br>
				                		<label class="control-label">Destination Address</label>
				                	</div>

				                	<div class="">
				                		<div class="col-sm-4 col-xs-12">
					                		<select name="country" class="form-control required" id="pp_pickup_country1" onchange="get_state('pp_pickup_country1','pp_pickup_state1','pp_pickup_city1','pp_pickup_state1','')">
						                            <option value="">Select Country</option>
						                            @foreach($country as $key)
						                             <option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}","state_available":"{{$key->state_available}}"}'>{{$key->Content}}</option>
						                            @endforeach
						                    </select>
						                </div>
						                <div class="col-sm-4 col-xs-12">
							                <select name="state" class="form-control required left-disabled" id="pp_pickup_state1" onchange="get_city('pp_pickup_state1','pp_pickup_city1','pp_pickup_city1','')">
							                        <option value="">Select State</option>
							                </select>
						                </div>
						                <div class="col-sm-4 col-xs-12">
							                <select  name="city" class="form-control required" id="pp_pickup_city1">
						                            <option value="">Select City</option>
						                    </select>
						                </div>
				                	</div>

				                	<div class="col-sm-12 col-xs-12">
				                        <br />
				                       <div class="form-group">
				                          	<label class="control-label">Select Package Category</label>
				                          	<select name="category" id="package_category" class="form-control required">
				                             <option value="">Select Category</option>
				                             <?php foreach ($category as $key) {?>
				                              <option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}"}' class="travel-mode-{{$key->TravelMode}}">{{$key->Content}}</option>
				                             <?php }?>
				                            </select>
				                       </div>
				                    </div>

				                	<div class="col-sm-6 col-xs-12 col-md-12" >
				                	
			                        	<div class="form-group">
			                            	<label class="control-label">Total cost of all items($)</label>
			                            	<br>
				                           	<div class="custom-input">
				                                <input class="form-control required numeric usename-#product cost#" name="pe_product_cost" id="pe_product_cost" >
				                           	</div>
			                        	</div>
			                    	</div>

			                    	

				                    <div id="buy_weight_div">
				                    	<div class="col-sm-6 col-xs-12">
				                    		<label class="control-label">Total weight of item/all items&nbsp;
				                    		<a data-toggle="popover" data-trigger="focus"  data-container="body" data-placement="top" type="button" data-html="true" href="javascript:void(0)" id="login" class="info_icon" style="margin-top:0;color:black;"><img src="theme/web/images/info_icon.png">
											</a> </label>
											
											<div id="popover-content-login" class="hide">
												<h4>Suggested item weights</h4>
												<small>These are suggested weights and may not reflect the actual weight of your item</small>
												<table class="table table-bordered table-striped">
													<thead>
														<tr>
															<th width="150">Item Name</th>
															<th width="60">Weight/Unit</th>
											 			</tr>
													 </thead>
													 <tbody>
													 	@if(count($item) > 0)

													 		@foreach($item as $key)
															<tr>
																<td>{{ucfirst($key->item_name)}} </td>

																<td >{{number_format($key->lbsWeight,2)}} Lbs / {{number_format($key->kgWeight,2)}} Kg</td>
															 </tr>
																@endforeach
													 		@endif
															 
															@if($item_count > 5)
															 <tr>
																<td colspan="2"><a href="javascript::void(0)" class="label label-info" onclick="return show_hide('more_items');">More</a></td>
												 			</tr>
												 			@endif
												 			
											 		</tbody>
											 </table>
											</div>






				                    		<br>

				                    		<div  class="custom-group">
		                						<div class="custom-input">
					                    			<input  type="numeric" name="weight" class="form-control required usename-#weight#" placeholder="Total Weight of all Item(s)">
					                    		</div>
					                    		<select name="weight_unit" style="margin-top:10px;" class="form-control required usename-#weight_unit#">
					                    			<option value="lbs" >in lbs</option>
					                    			<option value="kg" >in kg</option>
					                    		</select>
				                    		</div>


				                  		</div>

				                  		
				                  	</div>

					                <div class="col-sm-12 col-xs-12">
					                <br>
					                        <div class="form-group clearfix">
					                            <button type="submit" class="custom-btn1 btn" id="Calculate2">Calculate Now</button>
					                            <a class="custom-btn1 btn" id="Calculate1" htef="javascript:void(0)" onclick="return form_reset('buyforme_form')">Reset</a>
					                        </div>
					                </div>
					                </form>
				                </div>
	                			<!--End Buy for Me -->
						    </div>

						    <!-- Send package -->
						    <div role="tabpanel" class="tab-pane" id="messages">
						    	<div class="row">

			                		<form name="delivery_form" id="delivery_form" class="custom-select">
			                		<input type="hidden" name="travelMode" value="" id="delivery_travelMode">
			                		<div class="col-sm-12 col-xs-12">
				                		<br>
				                		<label class="control-label">Destination Address</label>
				                	</div>
			                		<div class="">
				                		<div class="col-sm-4 col-xs-12">
					                		<select name="country" class="form-control required" id="pp_pickup_country3" onchange="get_state('pp_pickup_country3','pp_pickup_state3','pp_pickup_city3','pp_pickup_state3','')">
						                            <option value="">Select Country</option>
						                            @foreach($country as $key)
						                             <option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}","state_available":"{{$key->state_available}}"}'>{{$key->Content}}</option>
						                            @endforeach
						                    </select>
						                </div>
						                <div class="col-sm-4 col-xs-12">
							                <select name="state" class="form-control required left-disabled" id="pp_pickup_state3" onchange="get_city('pp_pickup_state3','pp_pickup_city3','pp_pickup_city3','')">
							                        <option value="">Select State</option>
							                </select>
						                </div>
						                <div class="col-sm-4 col-xs-12">
							                <select  name="city" class="form-control required" id="pp_pickup_city3">
						                            <option value="">Select City</option>
						                    </select>
						                </div>
				                	</div>
				                	<div class="col-sm-12 col-xs-12">
				                		<br>
				                		<label class="control-label">Pickup Address</label>
				                	</div>
				                	<div class="">
				                		<div class="col-sm-4 col-xs-12">
					                		<select name="country2" class="form-control required usename-#country#" id="pp_pickup_country4" onchange="get_state('pp_pickup_country4','pp_pickup_state4','pp_pickup_city4','pp_pickup_state4','')">
						                            <option value="">Select Country</option>
						                            @foreach($country as $key)
						                             <option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}","state_available":"{{$key->state_available}}"}'>{{$key->Content}}</option>
						                            @endforeach
						                    </select>
						                </div>
						                <div class="col-sm-4 col-xs-12">
							                <select name="state2" class="form-control required left-disabled usename-#state#" id="pp_pickup_state4" onchange="get_city('pp_pickup_state4','pp_pickup_city4','pp_pickup_city4','')">
							                        <option value="">Select State</option>
							                </select>
						                </div>
						                <div class="col-sm-4 col-xs-12">
							                <select  name="city2" class="form-control required usename-#city#" id="pp_pickup_city4">
						                            <option value="">Select City</option>
						                    </select>
						                </div>
				                	</div>

				                    <div class="col-sm-12 col-xs-12">
				                    <br />
				                       <div class="form-group">
				                          <label class="control-label">Select Package Category</label>
				                          <select name="category" id="package_category" class="form-control required">
				                             <option value="">Select Category</option>
				                             <?php foreach ($category as $key) {?>
				                              <option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}"}' class="travel-mode-{{$key->TravelMode}}">{{$key->Content}}</option>
				                             <?php }?>
				                             </select>
				                       </div>
				                    </div>

				                    <div class="form-group clearfix">
				                  		<div class="col-sm-12 col-xs-12">
											  <br>
				                    		<label class="control-label">Measurement Unit</label>
				                    		<br>
				                    		<label class="radio-inline " >
				                    			<span class="pull-left"><input type="radio" name="measurement_unit" value="cm_kg" ></span><span class="Insurance_check">Metric (cm/kg)</span></label>
				                    		<label class="radio-inline m0device">
						                    <span class="pull-left">
						                        <input type="radio" name="measurement_unit" value="inches_lbs" checked="checked">
						                        </span> <span class="Insurance_check">Imperial (inches/lbs) </span></label>
						                </div>
				                	</div>

				                	<div class="">
				                  		<br>
				                  		<div class="col-sm-3 col-xs-12">

				                  			<div class="form-group clearfix">
					                            <label class="control-label">Item Weight</label>
					                            <br>
					                           <div class="custom-input">
					                                <input class="form-control required float usename-#weight#" placeholder="Enter weight" name="weight" id="pe_weight" >
					                           </div>
					                        </div>
				                  		</div>
					                    <div class="col-sm-3 col-xs-12">
					                        <div class="form-group clearfix">
					                            <label class="control-label">Length</label>
					                            <br>
					                           <div class="custom-input">
					                                <input class="form-control required float usename-#length#" placeholder="Enter length" name="length" id="pe_length" >
					                           </div>
					                        </div>
					                    </div>
					                    <div class="col-sm-3 col-xs-12">
					                        <div class="form-group clearfix">
					                            <label class="control-label">Height</label>
					                            <br>
					                           <div class="custom-input">
					                                <input class="form-control required float usename-#height#" placeholder="Enter height" name="height" id="pe_height">
					                           </div>
					                        </div>
					                    </div>
					                    <div class="col-sm-3 col-xs-12">
					                        <div class="form-group clearfix">
					                            <label class="control-label">Width</label>
					                            <br>
					                           <div class="custom-input">
					                                <input class="form-control required float usename-#width#" placeholder="Enter width" name="width" id="pe_width">
					                           </div>
					                        </div>
					                    </div>
				                	</div>

				                	
				                  	

					                <div class="col-sm-12 col-xs-12">
					                        <div class="form-group clearfix">
					                            <button type="submit" class="custom-btn1 btn" id="Calculate3">Calculate Now</button>
					                            <a class="custom-btn1 btn" id="Calculate1" htef="javascript:void(0)" onclick="return form_reset('delivery_form')">Reset</a>
					                        </div>
					                </div>



			                		</form>


			                	</div>
						    </div>
						    <!-- End send package -->
						    
						</div>
						<!-- end Tab panes -->

						<!-- local delivery form -->
						<div class="row" id="local_delivery" style="display: none;">

							<form name="local_delivery_form" id="local_delivery_form" class="custom-select">
	                		<!-- <input type="hidden" name="travelMode" value="" id="delivery_travelMode"> -->
	                		
	                		 <div class="col-sm-12 col-xs-12">
	                			<br>
	                			<label class="control-label">Pickup Address</label>
		                	</div>

		                	<div class="col-sm-12 col-xs-12">
								<div class="row">
		                		<br>
		                		<input type="hidden" value='{"id": "56bdbfb4cf32079714ee89ca", "name": "Ghana", "state_available": "1"}' id="pp_pickup_country5" name="country">

		                		<div class="col-sm-4 col-xs-12 custom-input">
		                			<input type="text" name="pickup_address" placeholder="Enter pickup address" class="form-control required usename-#pickup address#">
		                		</div>


				                <div class="col-sm-4 col-xs-12">
					                <select name="state" class="form-control required left-disabled" id="pp_pickup_state5" onchange="get_city('pp_pickup_state5','pp_pickup_city5','pp_pickup_city5','')">
					                        <option value="">Select Region</option>
					                </select>
								</div>
								
				                <div class="col-sm-4 col-xs-12">
					                <select  name="city" class="form-control required" id="pp_pickup_city5">
				                            <option value="">Select City</option>
				                    </select>
				                </div>
							</div>
							 </div>
		                	
	                		<div class="col-sm-12 col-xs-12">
	                			<br>
		                		<label class="control-label">Drop off Address</label>
		                		
		                	</div>

		                	<div class="col-sm-12 col-xs-12">
								<div class="row">
									<br>
									 <input type="hidden" value='{"id": "56bdbfb4cf32079714ee89ca", "name": "Ghana", "state_available": "1"}' id="pp_pickup_country6" name="country2">
									<div class="col-sm-4 col-xs-12 custom-input">
										<input type="text" name="dropoff_address" class="form-control required usename-#pickup address#" placeholder="Enter drop off address">
										
									</div>
									
									<div class="col-sm-4 col-xs-12">
										<select name="state2" class="form-control required left-disabled" id="pp_pickup_state6" onchange="get_city('pp_pickup_state6','pp_pickup_city6','pp_pickup_city6','')">
												<option value="">Select State</option>
										</select>
									</div>
									<div class="col-sm-4 col-xs-12 ">
										<select  name="city2" class="form-control required" id="pp_pickup_city6">
												<option value="">Select City</option>
										</select>
									</div>
								</div>
		                	</div>

							 

		                	<div class="col-sm-12 col-xs-12">
		                    	<br />
		                       <div class="form-group">
		                          <label class="control-label">Select Package Category</label>
		                          <select name="category" id="" class="form-control required">
		                             <option value="">Select Category</option>
		                             <?php foreach ($category_local as $key) {?>
		                              <option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}"}' class="">{{$key->Content}}</option>
		                             <?php }?>
		                             </select>
		                       </div>
		                    </div>

		                    <div id="localdelivery_weight_div">
								<br>
		                    	<div class="col-sm-6 col-xs-12">
		                    		<label class="control-label">Total weight of item/all items</label>
		                    		<br>
		                    		<div class="custom-input">
		                    		<input type="numeric" name="weight" class="form-control required usename-#weight#">
		                  			</div>
		                  		</div>

		                  		<div class="col-sm-6 col-xs-12">
								  <label class="control-label">Weight unit of all Item(s)</label>
		                    		<br>
		                    		<select name="weight_unit" class="form-control required usename-#weight_unit#">
		                    			<option value="lbs" >in lbs</option>
		                    			<option value="kg" >in kg</option>
		                    		</select>
		                   			
		                  		</div>

		                  	</div>



		                  	<div class="col-sm-12 col-xs-12">
			                	<br>
		                        <div class="form-group clearfix">
		                            <button type="submit" class="custom-btn1 btn" id="Calculate2">Calculate Now</button>
		                            <a class="custom-btn1 btn" id="Calculate1" htef="javascript:void(0)" onclick="return form_reset('local_delivery_form')">Reset</a>
		                        </div>
			                </div>
		                	</form>
		                </div>
		                <!-- end local delivery form -->

					</div>
                	<!-- end content -->
                	
                </div>



                <!-- form box end-->

                <!-- calculation box-->
                <div class="col-sm-4 col-xs-12 calcu1" >
		            <div class="calcu" >
		              	<div class="blue-bg text-center color-white">
		                  <h4>Estimated Shipping Cost</h4>
		                    <h1 class="large_price">$<span id="pe_calculated_total_amount">--</span></h1>
		                    <p class="color-white" id="pe_calculated_total_amount_in_ghana"></p>
		                    <p class="color-white" id="pe_calculated_total_amount_in_peso"></p> 
		                    <p class="color-white" id="pe_calculated_total_amount_in_uk"></p>
		                    <p class="color-white" id="pe_calculated_total_amount_in_cad"></p>
		                </div>
		                <div class="">

		                	<div class="col-xs-12">
		                		<br />

		                		<div class="pull-left">
		                    		<label class="control-label">Distance</label>
		                    	</div>
		                    	<div class="pull-right">
		                    		<label class="control-label"><span id="pe_calculated_distance">-- </span>&nbsp;km</label>
		                    	</div>
		                  		
		              		</div>
		                	<div class="col-xs-12">
		                  		<div class="pull-left">
		                    		<label class="control-label">Total Weight</label>
		                    	</div>
		                    	<div class="pull-right">
		                    		<label class="control-label"><span id="pe_calculated_item_weight">--</span>&nbsp;lbs</label>
		                    	</div>
		              		</div>
		              		<div class="clearfix"></div>
		            	</div>
		          	</div>
		        </div>
		        <!-- calculation box end-->
    	</div> </div>

    	<!-- more items -->
    	<div  id="more_items">



    		<div class="col-sm-12 col-xs-12 mob-right">
    			<h3 class="modal-title text-left" style="padding-left:5px;color:#31C3F3" id="myModalLabel">Suggest Item weights<span class="pull-right"><a onclick="return show_hide('price_estimator')" href="javascript::void(0)"  class="btn btn-default blue-btn" style="color:#FFFFFF;"><i class="fa fa-arrow-left" aria-hidden="true" ></i>&nbsp;&nbsp;Back</a>&nbsp;&nbsp; </span></h3>
    			<p>&nbsp;These are suggested weights and may not reflect the actual weight of your item.</p>
    			<br>
    		<table class="custom-table table table-bordered">
                <thead>
                    <tr>
                       <th>Item Name</th>
                       <th>Weight/Lbs</th>
                       <th>Weight/Kg</th>
                    </tr>
                </thead>
                    <?php $no = 0 ;?>
                    @if(count($items) > 0)
                    	@foreach($items as $key)
                    	<?php $no = $no+1; ;?>
                    <tbody>
                    	<tr >
                    		
                    		<td>
                    			{{ucfirst($key->item_name)}}
                    		</td>
                    		<td>
                        @if($key->lbsWeight != '')
                    			{{number_format($key->lbsWeight,2)}} 
                        @endif
                    		</td>
                        <td>
                        @if($key->lbsWeight != '')
                        {{number_format($key->kgWeight,2)}} 
                        @endif
                        </td>
            			</tr>
                    </tbody>
                    @endforeach
				@endif
    		</table>
    		
    		</div>
    	</div>
    	<!-- end more items -->

    </div>
</div>







@endsection

@section('script')
@parent
{!! Html::script('theme/web/js/utility.js') !!}
{!! Html::script('theme/web/js/validation.js') !!}


<script>
$('.loader-box').click(function () {
	$(this).addClass('load').siblings().removeClass('load');
    setTimeout( function(){
		$('.loader-box').removeClass('load').siblings().removeClass('load');
    },500);
});
$('.li-loader-box').click(function () {
	$(this).addClass('load').siblings().removeClass('load');
    setTimeout( function(){
		$('.li-loader-box').removeClass('load').siblings().removeClass('load');
    },700);
});

function show_hide(sec)
{
	if(sec == 'more_items'){
		$('#more_items').show();
		$('#price_estimator').hide();
	}else{
		$('#price_estimator').show();
		$('#more_items').hide();
	}
}

show_hide('price_estimator');



get_state('pp_pickup_country5','pp_pickup_state5','pp_pickup_city5','pp_pickup_state5','','','yes');
get_state('pp_pickup_country6','pp_pickup_state6','pp_pickup_city6','pp_pickup_state6','','','yes');

	$('#first_div').click(function(){
		$('#secound_div').removeClass('translateXC');
	$('#first_div').removeClass('translateXC');
	$('#third_div').removeClass('translateXC');
	$('#secound_div').removeClass('translateXA');
	$('#first_div').removeClass('translateXA');
	$('#third_div').removeClass('translateXA');
	});
$('#secound_div').click(function(){
	$(this).removeClass('translateXC');
	$('#first_div').removeClass('translateXC');
	$('#third_div').removeClass('translateXC');

	if($(this).width() > 20){
		$(this).addClass('translateXA');
		$('#first_div').addClass('translateXA');
		$('#third_div').addClass('translateXA');
	}
	else{
		$(this).removeClass('translateXA');
		$('#first_div').removeClass('translateXA');
		$('#third_div').removeClass('translateXA');
	}
});	

$('#third_div').click(function(){
	if($(this).width() > 20){
		$(this).addClass('translateXC');
		$('#first_div').addClass('translateXC');
		$('#secound_div').addClass('translateXC');
	}
	else{
		$(this).removeClass('translateXC');
		$('#first_div').removeClass('translateXC');
		$('#secound_div').removeClass('translateXC');
	}
});	
// tooltip --------//
$("[data-toggle=popover]").each(function(i, obj) {
$(this).popover({
  html: true,
  content: function() {
    var id = $(this).attr('id')
    return $('#popover-content-' + id).html();
  }
});

});



function show_request(request){
	if(request == 'buy'){
		$("#bt2").trigger("click");
	}else{
		$("#bt").trigger("click");
	}
}

new Validate({
    FormName : 'online',
    ErrorLevel : 1,
    validateHidden: false,
    callback : function(){
      price_estimatory('online');
    }

});

new Validate({
    FormName : 'buyforme_form',
    ErrorLevel : 1,
    validateHidden: false,
    callback : function(){
      price_estimatory('buyforme_form');
    }

});

new Validate({
    FormName : 'delivery_form',
    ErrorLevel : 1,
    validateHidden: false,
    callback : function(){
      price_estimatory('delivery_form');
    }

});

new Validate({
    FormName : 'local_delivery_form',
    ErrorLevel : 1,
    validateHidden: false,
    callback : function(){
      price_estimatory('local_delivery_form');
    }

});




function price_estimatory(type){
	if(type == 'delivery_form'){
		var ajaxurl ="delivery-price-estimator";
	}else if(type == 'local_delivery_form'){
		var ajaxurl ="localdelivery-price-estimator";
	}else{
		var ajaxurl ="online-buy-price-estimator";
	}

	

	$.ajax({
        url: SITEURL+ajaxurl,
        data: $('#'+type).serialize(),
        method: 'post',
        dataType: 'json',
        success: function(res){
        	if(res.success == 1){
        		$('#pe_calculated_total_amount').html(parseFloat(res.total_amount).toFixed(2));
	            $('#pe_calculated_total_amount_in_ghana').html(res.ghana_total_amount);
	            $('#pe_calculated_total_amount_in_peso').html(res.philipins_cost);
	            $('#pe_calculated_total_amount_in_uk').html(res.uk_cost);
	            $('#pe_calculated_total_amount_in_cad').html(res.canadian_cost);


	            


	           	$('#pe_calculated_item_volume').html(res.volume+' Cu. '+"Cm");
	            $('#pe_calculated_item_weight').html(parseFloat(res.total_weight).toFixed(2));
	            $('#pe_calculated_distance').html(parseFloat(res.total_distance).toFixed(2));
	            $('html, body').animate({ scrollTop: 0 }, 'slow');
        	}else{
        		alert(res.msg);
        	}
        	
        }
    });
}


function check_insurance(type)
{
	var value =document.getElementById(type+'_insurance').checked;
	if(value == true) {
	   $('#'+type+'_product_section').show();
	}else {
		$('#'+type+'_product_section').hide();
	}
}
check_insurance('buy');
check_insurance('online');


function set_form(type)
{
	if(type == 'air'){
		$('#tabcontent').show();
		$('#tabmenu').show();
		$('#default_text').hide();
		$('#first_sec').addClass('active');
		$('#second_sec').removeClass('active');
		$('#third_sec').removeClass('active');
		$('#online_travelMode').val('air');
		$('#buyforme_travelMode').val('air');
		$('#delivery_travelMode').val('air');
		$('#online_weight_div').show();
		$('#buy_weight_div').show();
		$('#local_delivery').hide();
		toggle_category('.travel-mode-air','.travel-mode-ship','#package_category');
		
		
	}else if(type == 'sea'){
		$('#tabcontent').show();
		$('#tabmenu').show();
		$('#default_text').hide();
		$('#first_sec').addClass('active');
		$('#second_sec').addClass('active');
		$('#first_sec').removeClass('active');
		$('#third_sec').removeClass('active');
		$('#online_travelMode').val('ship');
		$('#buyforme_travelMode').val('ship');
		$('#delivery_travelMode').val('ship');
		//$('#online_weight_div').hide();
		//$('#buy_weight_div').hide();
		$('#local_delivery').hide();
		toggle_category('.travel-mode-ship','.travel-mode-air','#package_category');
		
		
	}else if(type == 'delivery'){
		$('#tabcontent').hide();
		$('#tabmenu').hide();
		$('#default_text').hide();
		$('#third_sec').addClass('active');
		$('#first_sec').removeClass('active');
		$('#second_sec').removeClass('active');
		$('#local_delivery').show();
	}
}

//set_form('air');

function toggle_category(showid,hideid,id){
  $(id).val('');
  $(showid).attr('disabled',false);
  $(showid).show();
  $(hideid).attr('disabled',true);
  $(hideid).hide();
}

function form_reset(formid){
	$("#"+formid).trigger('reset');
}




  



</script>
@endsection

<!-- 5a5e0205360d5c21217a8770 -->