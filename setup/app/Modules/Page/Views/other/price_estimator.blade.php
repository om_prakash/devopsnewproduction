@extends('Page::layout.one-column-page')
@section('content')

<!-- Modal -->
@if(Session::get('UserId') == '')
<br/><br/><br/><br/>
@endif
<div class="container section-custom price">
   <div class="row">
    <div class="price_estimator">
        <form name="price_estimatory_form" id="price_estimatory_form">
            <div class=" clearfix">

                <div class="col-sm-8 col-xs-12 mob-right">

              <h3 class="modal-title text-left col-sm-12 col-xs-12" id="myModalLabel">Price Estimator</h3>
              <div class="form-group clearfix">
                    <div class="col-sm-12 col-xs-12">
                    <label class="control-label">Delivery Address</label>
                    <br />
                    </div>
                    <div class="clearfix"></div>
                    <div class="dotted-line"></div>
                    <div class="address-bar-custom">
                    <div class="col-sm-6 col-xs-12" style="margin-bottom: 0;">
                        <div class="from clearfix">
                          <div class="point"></div>
                            <div class="custom-input">
                            <input class="required usename-#source address#" placeholder="From" name="pe_source_address" >
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12" style="margin-bottom: 0;">
                        <div class="to clearfix">
                          <div class="point"></div>
                          <div class="custom-input">
                            <input class="required usename-#destination address#" placeholder="To" name="pe_destination_address" >
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="form-group clearfix">
                    <div class="col-sm-12 col-xs-12">
                       <div class="form-group">
                          <label class="control-label">Shipping Mode</label>
                          <div class="clearfix">
                             <label class="radio-inline">
                                 <span class="pull-left"><input type="radio" name="travel_mode" id="travel_mode" value="air" onclick="toggle_category('.travel-mode-air','.travel-mode-ship','#package_category')" checked="checked"></span>
                                    <span class="Insurance_check">By Air </span>
                             </label>
                             <label class="radio-inline">
                                  <span class="pull-left"><input type="radio" name="travel_mode" id="travel_mode" value="ship" onclick="toggle_category('.travel-mode-ship','.travel-mode-air','#package_category')"></span>
                                     <span class="Insurance_check">By Sea</span>
                             </label>
                          </div>
                       </div>
                    </div>
                    <div class="col-sm-12 col-xs-12">
                    <br />
                       <div class="form-group">
                          <label class="control-label">Select Package Category</label>
                          <select name="category" id="package_category" class="form-control required">
                             <option value="">Select Category</option>
                             <?php foreach ($category as $key) {?>
                              <option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}"}' class="travel-mode-{{$key->TravelMode}}">{{$key->Content}}</option>
                             <?php }?>
                             </select>
                       </div>
                    </div>
                </div>

                <div class="form-group clearfix">
                  <div class="col-sm-12 col-xs-12">
                    <label class="control-label">Measurement Unit</label>
                    <br>
                    <label class="radio-inline " >
                    <span class="pull-left"><input type="radio" name="measurement_unit" value="cm_kg" ></span><span class="Insurance_check">Metric (cm/kg)</span></label>
                    <label class="radio-inline m0device">
                    <span class="pull-left">
                        <input type="radio" name="measurement_unit" value="inches_lbs" checked="checked">
                        </span> <span class="Insurance_check">Imperial (inches/lbs) </span></label>
                    </div>
                </div>

                <div class="">
                <br>
                  <div class="col-sm-3 col-xs-12">
                    <label class="control-label">Item Weight</label>
                    <br>
                   <div class="custom-input">
                        <input class="required float usename-#weight#" name="weight" value="">
                   </div>
                  </div>
                    <div class="col-sm-3 col-xs-12">
                        <div class="form-group clearfix">
                            <label class="control-label">Length</label>
                            <br>
                           <div class="custom-input">
                                <input class="required float usename-#length#" name="length" id="pe_length" >
                           </div>
                        </div>
                    </div>
                    <div class="col-sm-3 col-xs-12">
                        <div class="form-group clearfix">
                            <label class="control-label">Height</label>
                            <br>
                           <div class="custom-input">
                                <input class="required float usename-#height#" name="height" id="pe_height">
                           </div>
                        </div>
                    </div>
                    <div class="col-sm-3 col-xs-12">
                        <div class="form-group clearfix">
                            <label class="control-label">Width</label>
                            <br>
                           <div class="custom-input">
                                <input class="required float usename-#width#" name="width" id="pe_width">
                           </div>
                        </div>
                    </div>
                </div>
                <style type="text/css">
                  .Insurance_check{float: left; margin-top: 3px; margin-left: 5px;}

                </style>
                  <div class="col-xs-12 form-group">
                     <span class="pull-left">
                     <input type="checkbox" name="pe_insurance" id="pe_insurance" value="yes" onclick="check_insurance()" >
                     </span>
                      <span class="Insurance_check"> Insurance</span>
                  </div>

                <br />
                <div class="clearfix"></div>
                <div class="" style="dispaly:none;" id="product_section">
                <div class=""><br />
                    <div class="col-xs-6">
                        <div class="form-group clearfix">
                            <label class="control-label">Product Cost</label>
                            <br>
                           <div class="custom-input">
                                <input class="required numeric usename-#product cost#" name="pe_product_cost" id="pe_product_cost" >
                           </div>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group clearfix">
                            <label class="control-label">Product Quantity</label>
                            <br>
                           <div class="custom-input">
                                <input class="required numeric usename-#product quantity#" name="pe_product_qty" id="pe_product_qty">
                           </div>
                        </div>
                    </div>
                 </div>
                </div>
                 <div class="clearfix"></div>
                <div class="col-sm-12 col-xs-12">
                        <div class="form-group clearfix">
                            <button class="custom-btn1 btn" id="Calculate">Calculate Now</button>
                        </div>
                </div>
            </div>
            <div class="col-sm-4 col-xs-12 calcu1">
            <div class="calcu">
              <div class="blue-bg text-center color-white">
                  <h4>Estimated Shipping Cost</h4>
                    <h1 class="large_price">$<span id="pe_calculated_total_amount">00.00</span></h1>
                    <p class="color-white" id="pe_calculated_total_amount_in_ghana"></p>
                </div>
                <div class="">

                <div class="col-xs-12">
                <br />
                  <div class="pull-left">
                    <label class="control-label">Item Weight-</label>
                    </div>
                    <div class="pull-right">
                    <label class="control-label"><span id="pe_calculated_item_weight">00.00</span>&nbsp;Lbs</label>
                    </div>
              </div>
                <div class="col-xs-12">
                  <div class="pull-left">
                    <label class="control-label">Volume-</label>
                    </div>
                    <div class="pull-right">
                    <label class="control-label"><span id="pe_calculated_item_volume">00.00 Cu. Cm</span></label>
                    </div>
              </div>
              <div class="clearfix"></div>
</div>
            </div>

            </div>
            </div>
        </form>
    </div>
  </div>
</div>
@endsection
@section('script')
@parent

{!! Html::script('theme/web/js/validation.js') !!}

@endsection

@section('inline-script')
@parent
<script>

new Validate({
    FormName : 'price_estimatory_form',
    ErrorLevel : 1,
    validateHidden: false,
    callback : function(){
      price_estimatory();
    }

  });


function toggle_category(showid,hideid,id){
  $(id).val('');
  $(showid).attr('disabled',false);
  $(showid).show();
  $(hideid).attr('disabled',true);
  $(hideid).hide();
}


function price_estimatory()
{

    try
    {
      $("#Calculate").addClass("spinning");
      $.ajax({
        url: SITEURL+'price_estimator',
        data: $('#price_estimatory_form').serialize(),
        method: 'post',
        dataType: 'json',
        success: function(res){
          $("#Calculate").removeClass("spinning");
          $('#pe_calculated_distance').html('00.00');
          $('#pe_calculated_total_amount').html('00.00');
          $('#pe_calculated_total_amount_in_ghana').html('');
          $('#pe_calculated_item_volume').html('00.00 Cu. Cm');
          $('#pe_calculated_item_weight').html('00.00 Kg');

          if(res.success == 1)
          {
            $('#pe_calculated_distance').html(parseFloat(res.calculationinfo.distance).toFixed(2));
            $('#pe_calculated_total_amount').html(parseFloat(res.total_amount).toFixed(2));
            $('#pe_calculated_total_amount_in_ghana').html(res.ghana_total_amount);
            //$('#pe_calculated_item_volume').html(res.calculationinfo.showVolume+' Cu. '+res.calculationinfo.weightUnit);
            $('#pe_calculated_item_volume').html(res.calculationinfo.showVolume+' Cu. '+"Cm");
            $('#pe_calculated_item_weight').html(parseFloat(res.calculationinfo.weight).toFixed(2));
            $('html, body').animate({ scrollTop: 0 }, 'slow');

          }
          else
          {
            alert(res.msg);
          }
        }
      });
    }catch(e){
      console.log(e);
    }

    return false;
}

function check_insurance()
{

  if(document.getElementById('pe_insurance').checked == true) {
    $('#product_section').show();
  } else {
    $('#product_section').hide();
  }
}
check_insurance();
toggle_category('.travel-mode-air','.travel-mode-ship','#package_category');



</script>
@endsection