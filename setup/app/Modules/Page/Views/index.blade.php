<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>	Aquantuo Delivery App Coming Soon </title>
	<meta name="description" content="Kenya, price estimator and quote is showing the international shipping price for Ghana" >
	<meta name="author" content="Jewel Theme">

	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
	<!-- Bootstrap  -->
	{!! HTML::style('theme/web/css/bootstrap.min.css') !!}
	
	<!-- icon fonts font Awesome -->
	{!! HTML::style('theme/web/css/font-awesome.min.css') !!}
	
	<!-- Custom Styles -->
	{!! HTML::style('theme/web/css/style.css') !!}

	<!--Color Style -->
	{!! HTML::style('theme/web/css/colors/default.css') !!}

	<!--[if lt IE 9]>
	<script src="assets/js/html5shiv.js"></script>
	<![endif]-->

</head>
<body>


	<!-- Preloader -->
	<div id="preloader">
		<div id="loader">
			<div class="dot"></div>
			<div class="dot"></div>
			<div class="dot"></div>
			<div class="dot"></div>
			<div class="dot"></div>
			<div class="dot"></div>
			<div class="dot"></div>
			<div class="dot"></div>
			<div class="lading"></div>
		</div>
	</div><!-- /#preloader -->
	<!-- Preloader End-->



	<!-- Page Top Section -->
	<section id="page-top" class="section-style" data-background-image="theme/web/images/page-top.jpg">
		<div class="pattern height-resize">
			<div class="container">
				<h1 class="site-title">
					Aquantuo					
				</h1><!-- /.site-title -->
				<h3 class="section-name">
					<span >
					www.aquantuo.com
					</span>
				</h3><!-- /.section-name -->
				<h2 class="section-title">
					Coming Soon
				</h2><!-- /.Section-title  -->
				<div id="time_countdown" class="time-count-container">

					<div class="col-sm-3">
						<div class="time-box">
							<div class="time-box-inner dash days_dash animated" data-animation="rollIn" data-animation-delay="300">
								<span class="time-number">
									<span class="digit">0</span>
									<span class="digit">0</span>
								</span>
								<span class="time-name">Days</span>
							</div>
						</div>
					</div>

					<div class="col-sm-3">
						<div class="time-box">
							<div class="time-box-inner dash hours_dash animated" data-animation="rollIn" data-animation-delay="600">
								<span class="time-number">
									<span class="digit">0</span>
									<span class="digit">0</span>	
								</span>
								<span class="time-name">Hours</span>
							</div>
						</div>
					</div>

					<div class="col-sm-3">
						<div class="time-box">
							<div class="time-box-inner dash minutes_dash animated" data-animation="rollIn" data-animation-delay="900">
								<span class="time-number">
									<span class="digit">0</span>
									<span class="digit">0</span>
									<span class="digit">0</span>
								</span>
								<span class="time-name">Minutes</span>
							</div>
						</div>
					</div>

					<div class="col-sm-3">
						<div class="time-box">
							<div class="time-box-inner dash seconds_dash animated" data-animation="rollIn" data-animation-delay="1200">
								<span class="time-number">
									<span class="digit">0</span>
									<span class="digit">0</span>
								</span>
								<span class="time-name">Seconds</span>
							</div>
						</div>
					</div>
					
				</div><!-- /.time-count-container -->

			
				
			</div><!-- /.container -->
		</div><!-- /.pattern -->		
	</section><!-- /#page-top -->
	<!-- Page Top Section  End -->



	<!-- About Us Section End -->



	<!-- Subscribe Section -->

		<!-- Subscribe Section End -->

		<!-- jQuery Library -->
		{!! HTML::script('theme/web/js/jquery-2.1.0.min.js') !!}
		<!-- Modernizr js -->
		{!! HTML::script('theme/web/js/modernizr-2.8.0.min.js') !!}
		<!-- Plugins -->
		{!! HTML::script('theme/web/js/plugins.js') !!}
		<!-- Custom JavaScript Functions -->
		{!! HTML::script('theme/web/js/functions.js') !!}
		<!-- Color Style Switcher -->
		{!! HTML::script('theme/web/js/switcher.js') !!}
		<!-- Custom JavaScript Functions -->
		{!! HTML::script('theme/web/js/jquery.ajaxchimp.min.js') !!}
	</body>
	</html>
