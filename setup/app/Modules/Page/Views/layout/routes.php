<?php

Route::group(array('module' => 'Page', 'namespace' => 'App\Modules\Page\Controllers'), function () {

    Route::get('page/reset_password/{id}/{token}', 'PageController@reset_password');
    Route::post('page/reset_password/{id}/{token}', 'PageController@post_reset_password');
    Route::get('page/success', 'PageController@success');
    Route::get('page/testuser', 'PageController@testuser');
    Route::get('page/account-security-instructions', 'PageController@account_security_instructions');

    Route::get('/', 'PageController@promo');
    Route::get('terms-and-conditions', 'PageController@terms_and_conditions');
    Route::get('privacy-policy', 'PageController@privacy_policy');
    Route::any('ajax/contact-us', 'PageController@contact_us');
    Route::any('ajax/subscribe', 'PageController@subscribe');
    Route::any('ajax/country', 'PageController@country');
    Route::any('ajax/place-an-order', 'PageController@place_an_order');

    Route::get('login', 'LoginController@login');
    Route::post('login', 'LoginController@login_post');
    Route::get('signup', 'PageController@signup');
    Route::post('signup', 'PageController@registration');
    Route::get('transporter-signup', 'PageController@signup');
    Route::post('signup/transporter', 'PageController@registration');
    Route::get('become-a-transporter', 'PageController@become_a_transporter');
    Route::get('near-transporter', 'PageController@near_transporter');
    Route::get('trip-post-by-transporter', 'PageController@trip_list');
    Route::get('forgot-password', 'PageController@forgot_password');
    Route::post('forgot-password', 'PageController@makeForgotPassword');

    //  new route
    Route::post('dashboard', 'PageController@dashboard');
    //Route::any('dashboard','PageController@login');

    Route::post('state-list', 'AjaxController@state_list');
    Route::post('city-list', 'AjaxController@city_list');

    Route::post('price_estimator', 'AjaxController@post_price_estimator');
    Route::get('price-estimator', 'AjaxController@price_estimator');

    Route::get('set_location', 'AjaxController@set_location');
    Route::get('set-datetime', 'LoginController@setDatetime');

});
