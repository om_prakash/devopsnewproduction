<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1 user-scalable=no">
    <meta name="description" content="Have items delivered to your home or your business in Ghana from the US. Send packages to the US from Ghana. Get paid with your spare luggage space. Shop international stores and receive them at your door. Get help shopping international store.">
    <meta name="keywords" content="shipping ghana, shipping to ghana, shipping to ghana from usa, shipping from ghana to usa, ghana shipping companies, air cargo companies in ghana, air freight to ghana, freight logistics companies, dhl ghana prices, online stores in ghana, online shopping in ghana, send parcel to usa, door to door shipping to ghana from usa, package forwarding, international car shipping, international shipping companies, door to door shipping company, shipping internationally, online shopping ghana, online shopping usa, shipping rates, shipping cost, shipping calculator, local delivery company, shipping a car, air freight, sea freight, express shipping">


    <meta name="author" content="Aquantuo.com">
    <meta http-equiv="refresh" content="3000">
    <meta name="_token" content="{!! csrf_token() !!}"/>
    <meta name="csrf-token" content="{{{ Session::token() }}}">

    <link rel="icon" type="image/png" href="{{ url('theme/web/promo/images/favicon.png')}}"/>
	@if (trim($__env->yieldContent('page_title')))
		<title>@yield('page_title')</title>
    @else<title>Shipping Internationally - Air Freight and Sea Freight - Aquantuo</title>
    @endif
    
    <link href="theme/web/promo/images/favicon.png" type="image/png" rel="icon">
    {!! Html::script('theme/web/js/jquery.js') !!}
    {!! Html::script('theme/web/promo/js/bootstrap.min.js') !!}
    {!! Html::script('theme/fancybox/source/mouse_wheel.js') !!}
    <!-- Bootstrap -->
    {!! Html::style('theme/web/promo/css/bootstrap.min.css') !!}
    <!-- {!! Html::style('theme/web/promo/css/style.css') !!} -->
    {!! Html::style('theme/web/promo/css/style.min.css') !!}
    {!! Html::style('theme/web/promo/css/font-awesome.css') !!}
    {!! Html::style('theme/fancybox/source/jquery.fancybox.css') !!}

    {!! Html::script('theme/fancybox/source/jquery.fancybox.js') !!}
    <script type="text/javascript">

      var SITEURL = '{{URL::to("")}}/';
      var USERID = '{{Session::get("UserId")}}',
          USERIMAGE = '{{Session::get("Image")}}',
          USERNAME = '{{Session::get("Name")}}';


      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                              })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
      ga('create', 'UA-80596505-1', 'auto');
      ga('send', 'pageview');
    </script>

    <style type="text/css">
      input{background-color:white !important;}
      @media only screen and (max-width: 600px) and (min-width: 400px)  {
        #bs-example-navbar-collapse-2 > div > ul > li:nth-child(1) {
          margin-top: 35%;
        }
        body > div.container-fluid > div:nth-child(1) > nav > div > div.navbar-header > div:nth-child(3) > button {
          float: right !important;
        }
        .navbar-nav {
          margin: 0px;
          margin-top: -24%;
          margin-right: 30%;
        }
        .avatar::after {
          content: "\f0d7";
          font-family: "FontAwesome";
          position: absolute;
          right: 1px;
          top: 13px;
        }
        .storeName {
          display: none !important;
        }
      }
    </style>

 <!-- Piwik -->
<script type="text/javascript">
  var _paq = _paq || [];
  /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u="//piwik.aquantuo.com/";
    _paq.push(['setTrackerUrl', u+'piwik.php']);
    _paq.push(['setSiteId', '1']);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
  })();
</script>
<!-- End Piwik Code -->




<!-- Facebook Pixel Code -->
<script>

!function(f,b,e,v,n,t,s)

{if(f.fbq)return;n=f.fbq=function(){n.callMethod?

n.callMethod.apply(n,arguments):n.queue.push(arguments)};

if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';

n.queue=[];t=b.createElement(e);t.async=!0;

t.src=v;s=b.getElementsByTagName(e)[0];

s.parentNode.insertBefore(t,s)}(window,document,'script',

'https://connect.facebook.net/en_US/fbevents.js');

fbq('init', '523058054800599'); 

fbq('track', 'PageView');

</script>

<noscript>
<img height="1" width="1" src="https://www.facebook.com/tr?id=523058054800599&ev=PageView&noscript=1"/>
</noscript>

<!-- End Facebook Pixel Code -->



  </head>
<body>
<div class="modal fade" id="exampleModal45465" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel"></h5>
            <a href="javascript::void(0)" class="close" data-dismiss="modal" aria-label="Close" onclick="alertClose()"> <span aria-hidden="true">&times;</span>
            </a>
          </div>
          <input type="hidden" value="{{session()->get('alert_status')}}" id="alertMsg">
          <div class="modal-body">{{session()->get('alert_content')}}</div>

      </div>
    </div>
</div>
<script>
$(document).ready(function() {
    var aMsg = $("#alertMsg").val();
    if(aMsg == "on"){
      $('#exampleModal45465').modal({
          backdrop: 'static',
          keyboard: false
      })
      $('#exampleModal45465').modal('show');
    }
  });

  function alertClose(){

    ajaxUrl = SITEURL + "/alert-close";
      $.ajax({
      type: "POST",
      dataType: 'json',
      url: ajaxUrl,
      data: "",
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      success: function(res) {
      }
    });

  }
</script>
    <!--pop up open -->

@if (!Session::has('tzo') && !in_array(Request::segment(1),['page']))
<script type="text/javascript">
   if (navigator.cookieEnabled) {
     document.location.href = SITEURL+'set-datetime?tzo=' + (- new Date().getTimezoneOffset()) + '&href=' + document.location.href;
   }
   if (!navigator.cookieEnabled) alert("Cookies must be enabled!");
</script>
@endif



    <!-- pop close-->
    <div class="container-fluid">
    <div class="row">
      <!--parallax 1 -->
      <nav class="navbar navbar-default nav-bg navbar-fixed-top responsive first_menu" style="">
        <div class="container">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <div class="navbar-right">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
            </div>

            <div class="cetner-m">
              <a class="navbar-brand " href="#"><img alt="Aquantuo" src="{{asset('theme/web/promo/images/logo.png')}}">
              </a>
            </div>
              <?php if (Session::get('UserId') != '') {?>
               <div class="navbar-right">
              <button type="button" class="navbar-toggle collapsed pull-left" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
            </div>
            <?php }?>
          </div>
          <!-- Collect the nav links, forms, and other content for toggling -->
           <?php if (Session::get('UserId') != '') {?>

             <ul class="nav navbar-nav pull-right dropdown downarrow downarrow-inner pull-right">
                <li class="nav-item avatar" id="dropdownMobileMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                     <?php if (Session::get('Image') != '') {?>
                          <img src="{{ImageUrl.Session::get('Image')}}" class="img-fluid"  width="40" height="40">
                        <?php } else {?>
                          <img src="{{ImageUrl}}/user-no-image.jpg" class="img-fluid"  width="40" height="40">
                      <?php }?>

                  </li>
                @if(in_array(Request::segment(1),['terms-and-conditions','privacy-policy']))
                <li><a href="{{ url('my-request') }}" style="padding-top:15px;"> My Dashboard </a></li>
                @else

                <li class="nav-item storeName  waves-effect waves-light" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    {{Session::get('ShowName')}}
                  </li>
                <div class="dropdown-menu dropdown-info logoutbar dropdown-menu-right" aria-labelledby="dropdownMenu3" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                    <a class="dropdown-item waves-effect waves-light" href="{{url('logout')}}">Log Out</a>
                </div>
                <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="menu1">
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="{{url('my-profile')}}">My Profile</a></li>
                  <li role="presentation"><a role="menuitem" tabindex="-1"  href="{{url('statistics')}}">Statistics</a></li>

                  <li role="presentation"><a role="menuitem" tabindex="-1" href="{{url('change-password')}}">Change Password</a>
                  </li>
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="{{url('support')}}" >Support/FAQ</a>
                  </li>
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="{{url('invite-friend')}}">Invite Friend</a>
                  </li>
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="{{url('logout')}}">Logout</a>
                  </li>
                </ul>
              @endif
        	</ul>
        	<?php }?>
          <!-- /.navbar-collapse -->
          <?php if (Session::get('UserId') != '') {?>
          <?php }?>

          <div class="collapse navbar-collapse pull-right" id="bs-example-navbar-collapse-1">
            <div class="row">

              <ul class="nav navbar-nav navbar-right">
                <li id="add-class" class="@if(in_array(Request::segment(1),['dashboard']))  active @endif" ><a href="{{url('')}}#home" onclick="add_class()">Home</a></li>
                @if (Session::get('UserId') != '')
                @else
                <li id="add-class" class="@if(in_array(Request::segment(1),['#aboutapp']))  active @endif" ><a href="{{url('')}}#aboutapp">How it works </a></li>
                @endif
                <li id="add-class" class="@if(in_array(Request::segment(1),['price-estimator']))  active @endif"><a href="{{url('price-estimator')}}" >Price Estimator</a></li>

                <?php if (Session::get('UserId') != '') {?>
                <li class="@if(in_array(Request::segment(1),['my-request','trip-detail','prepare-request','near-by-transporter','delivery-details','posts-by-transporters','buy-for-me-detail','online-request-detail','payment-by-for-me','on-line-purchases','online-payment','buy-for-me','process-card-list','post-by-transporter','edit-online-purchase','buyforme-payment']))  active @endif "><a href="{{url('my-request')}}">Requester</a></li>
                @if(in_array(Session::get( 'Usertype'), ['transporter','both'] ))
                <li class="@if(in_array(Request::segment(1),['transporter']))  active @endif" ><a href="{{url('transporter')}}">Transporter </a></li>
                @endif
                <li class="visible-md-lg  @if(in_array(Request::segment(1),['my-profile','change-password','address','faq-list','reward','edit-transporter-profile','setting','edit-profile','my-aquantuo-address','card-list','support','become-transporter','statistics','pending-request','transporter-pending-request','bank-information','edit-bank-information']))  active @endif

                 dropdown">



                  </li>


                  <li class="visible-sm-xs">
                  <!-- for responsive -->
                  <a class="custom-drp-tggl">Account<span class="caret"></span></a>
                  <ul class="custom-drp-d" style="display:none;">
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="{{url('my-profile')}}">My Profile</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="{{url('change-password')}}">Change Password</a></li>
                  </ul>
                </li>
                <?php } else {?>

                  <!-- <li id="add-class" class="visible-md-lg dropdown @if(in_array(Request::segment(1),['signup','login'])) active @endif">
                      <a href="{{url('signup')}}" >Sign In/Sign Up</a>
                  </li> -->


                  <li role="presentation" class="dropdown">
                    <a href="#" class="dropdown-toggle @if(in_array(Request::segment(1),['signup','login'])) active @endif" id="drop4" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Sign In/Sign Up <span class="caret"></span> </a>
                    <ul class="dropdown-menu" id="menu1" aria-labelledby="drop4">
                      <li ><a href="{{url('login')}}">Sign In</a></li>
                      <li ><a href="{{url('signup')}}">Sign Up</a></li>
                    </ul>
                  </li>


                  <li class="visible-sm-xs">
                  <!-- for responsive -->
                  <a class="custom-drp-tggl">Account<span class="caret"></span></a>
                  <ul class="custom-drp-d {{ (Request::is('signup') ? 'active' : '') }} {{ (Request::is('login') ? 'active' : '') }}" style="display:none;">
                    <li ><a href="{{url('signup')}}">Sign up</a>
                    </li>
                    <li ><a href="{{url('login')}}">Sign in</a>
                    </li>
                  </ul>
                  </li>
				  <!-- for responsive -->

                  <li class="{{ (Request::is('transporter-signup') ? 'active' : '') }}" ><a href="{{url('transporter-signup')}}?type=transporter">Become a Transporter</a></li>


                <?php }?>

                <li><a href="http://support.aquantuo.com/" class="nav-scroll" target="_blank">FAQs </a></li>
                   <li class="{{ (Request::is('Contact-signup') ? 'active' : '') }}" >
                  <!--  <a href="{{url('/')}}#contact_section">Contact</a> -->
                   <a href="http://support.aquantuo.com/contact" target="_blank">Contact</a>
                   </li>


              </ul>

            </div>
          </div>

        </div>

        <!-- /.container-fluid -->
      </nav>
      <!-- Button trigger modal -->
    </div>
    @if(Session::get('UserId') != '')
    <div class="row">
      <nav class="navbar navbar-default second-menu">
        <div class="container">
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
            <div class="row">
              <ul class="nav navbar-nav">

                @if(Request::segment(1) == 'transporter')
                <li class="@if(in_array(Request::segment(2),['','details']))  active @endif" ><a  href="{{url('transporter')}}">New Requests</a></li>
                <li class="@if(in_array(Request::segment(2),['my-deliveries','delivery-details','online_detail','buy_for_me_detail']))  active @endif"><a  href="{{url('transporter/my-deliveries')}}">My Deliveries</a></li>
                <li class="@if(in_array(Request::segment(2),['post-a-trip']))  active @endif"><a  href="{{url('transporter/post-a-trip')}}">Post a Trip</a></li>
                <li class="visible-md-lg  @if(in_array(Request::segment(2),['bussiness-trips','individual-trips','edit_individual','view-request','edit-trips']))  active @endif dropdown">
                  <a class="dropdown-toggle" id="menu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">My Trips<span class="caret"></span></a>
                  <ul class="dropdown-menu" role="menu" aria-labelledby="menu2">
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="{{url('transporter/individual-trips')}}">Individual</a></li>
                    @if(Session()->get('TransporterType') == 'business')
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="{{url('transporter/bussiness-trips')}}">Business</a></li>
                    @endif
                  </ul>
                  </li>
                  <li class="visible-sm-xs">
                   <!-- for responsive -->
                   <a class="custom-drp-tggl" id="menu2">My Trips<span class="caret"></span></a>
                  <ul class="custom-drp-d" style="display:none;">
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="{{url('transporter/individual-trips')}}">Individual</a></li>
                    @if(Session()->get('TransporterType') == 'business')
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="{{url('transporter/bussiness-trips')}}">Business</a></li>
                    @endif
                  </ul>
                   <!-- for responsive -->
                </li>


                @else
                <li class="@if(in_array(Request::segment(1),['prepare-request','buy-for-me','process-card-list','on-line-purchases','buyforme-payment','local-new-prepare-request'])) active @endif"><a data-toggle="modal" id="Edit" title="Edit" data-whatever="@mdo" href="#popup"  onclick="$('.animat').removeClass('animat');" href="#" class="create-request-btn">Create Request</a></li>

                <li class="@if(in_array(Request::segment(1),['my-request','delivery-details','buy-for-me-detail','edit-request','edit-buy-for-me','online-request-detail','payment-by-for-me','online-payment','edit-online-purchase','local-delivery-detail','send-package-detail','edit-local-delivery'])) active @endif"  ><a  href="{{url('my-request')}}">My Requests</a></li>

                <li class="@if(in_array(Request::segment(1),['track-order'])) active @endif"><a  href="{{url('track-order')}}">Track My Order</a></li>

                <li class="@if(in_array(Request::segment(1),['price-estimator'])) active @endif "    class="@if(in_array(Request::segment(4),[''])) active @endif"><a  href="{{url('price-estimator')}}">Price Estimator</a></li>

                <!-- <li class="@if(in_array(Request::segment(1),['near-by-transporter','trip-detail'])) active @endif "    class="@if(in_array(Request::segment(4),[''])) active @endif"><a  href="{{url('near-by-transporter')}}">Nearby Transporters</a></li>
                <li class="@if(in_array(Request::segment(1),['post-by-transporter'])) active @endif"><a  href="{{url('post-by-transporter')}}">Posts by Transporters</a></li> -->


              @endif

              <li class="@if(in_array(Request::segment(1),['notification'])) active @endif"><a  href="{{url('notification')}}">Notifications</a></li>


             <!--  <div class="profile-side-menu">
                  <li><a href="{{url('my-profile')}}"><i aria-hidden="true" class="fa fa-user"></i>General Information</a></li>

                  <li><a href="{{url('my-aquantuo-address')}}" ><i aria-hidden="true" class="fa fa-map-marker"></i>My Aquantuo Address</a></li>
                  <li><a href="{{url('card-list')}}"><i aria-hidden="true" class="fa fa-credit-card"></i>Cards</a></li>
                  <li><a href="{{url('bank-information')}}"><i aria-hidden="true" class="fa fa-bank"></i>Bank Information</a></li>

                  <li class="dropdown visible-md-lg">
                  <a class="dropdown-toggle" id="setting1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i aria-hidden="true" class="fa fa-cog"></i>Setting<span class="glyphicon glyphicon-chevron-right pull-right"></span></a>
                    <ul class="dropdown-menu nav nav-pills nav-stacked new" role="menu" aria-labelledby="setting1">
                      <li><a href="#">General Setting</a></li>
                      <li><a href="#">Shipping Setting</a></li>
                      <li><a href="#">Saved Address</a></li>
                      <li><a href="#">Buy for me / Shopping Assistant</a></li>
                      <li><a href="#">Payments</a></li>
                      <li><a href="#">Order History</a></li>
                    </ul>
                   </li>
                    <li class="visible-sm-xs">
                    <a class="custom-drp-tggl"><i aria-hidden="true" class="fa fa-cog"></i>Setting<span class="glyphicon glyphicon-chevron-right pull-right"></span></a>
                    <ul class="custom-drp-d" style="display:none;">
                      <li><a href="#">General Setting</a></li>
                      <li><a href="#">Shipping Setting</a></li>
                      <li><a href="#">Saved Address</a></li>
                      <li><a href="#">Buy for me / Shopping Assistant</a></li>
                      <li><a href="#">Payments</a></li>
                      <li><a href="#">Order History</a></li>
                    </ul>

                  </li>
                </li>

              </div> -->
            </ul>
          </div>
        </div>
      </div>
    </nav>
    </div>
    @endif
    
    <script>
    $(document).ready(function(e) {
      $(".custom-drp-tggl").click(function(){
        $(this).siblings().toggleClass("menu-open")
      });
    });
    </script>





    <div class="clearfix"></div>
    <div class="margin-sec"></div>
    <div class="clearfix"></div>

    <?php
if (Session::get('UserId') != '') {

	?>
    <div class="wrapper clearfix row" style="margin-top: 20px !important;">
    <?php

} else {
	?>
      <div class="wrapper clearfix row" style="margin-top: 51px !important;">
       <?php

}

?>




    <div class="@yield('bg-color')">
    	@foreach (['danger', 'warning', 'success', 'info'] as $msg)

        <div class="container">
          <div class="row">
            <div class="flash-message text-left" id="error_msg_section">
                @if(Session::has($msg))
                  <p class="alert alert-{{ $msg }}"><?php print_r(Session()->get($msg));?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  </p>
                @endif

            </div>
          </div>
        </div>
        @endforeach
      @yield('content')
    </div>
    </div>
    <div class="row">
    <div class="container-fluid section-custom footer">
      <div class="container">
        <div class="col-md-12 col-sm-12 customf">
          <div class="row">
            <div class="col-md-6 col-sm-6">
              {!! HTML::image('theme/web/promo/images/white_logo.png','Aquantuo',array('width' =>'50%','class'=>'footer-logo' )); !!}
              <p class="color-white">We provide customized end to end air freight and sea freight services from the US to Ghana and from Ghana to the US for individuals, small and large businesses.</p>
            </div>
            <div class="col-md-3 col-sm-3 sitemap-style">
              <ul style="margin-top:0px;" class="footer-list">
                <li style="margin-bottom:0px;"><a href="{{url('/')}}#home">- Home</a></li>
                <li style="margin-bottom:0px;"><a href="{{url('/')}}#aboutapp">- How it works</a></li>
                <!-- <li style="margin-bottom:0px;"><a href="{{url('/')}}#features">- Features </a></li> -->
                <li style="margin-bottom:0px;"><a href="{{url('/')}}#download">- Download</a></li>
                <li style="margin-bottom:0px;" ><a href="http://support.aquantuo.com/" >- Help & Support </a></li>
                <li style="margin-bottom:0px;"><a href="http://support.aquantuo.com/contact" target="_blank">- Contact</a></li>
              </ul>
            </div>
            <div class="col-md-3 col-sm-3">
              <div class="social-media clearfix">
                <div class="col-md-4 col-sm-4 col-xs-4">
                  <a href="https://www.facebook.com/Aquantuo" target="_blank">
                    <div class="social-icon-facebook">
                      <i class="fa fa-facebook"></i>
                    </div>
                  </a>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4">
                  <a href="https://twitter.com/Aquantuo" target="_blank">
                    <div class="social-icon-twitter">
                      <i class="fa fa-twitter"></i>
                    </div>
                  </a>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4">
                  <a href="https://www.instagram.com/Aquantuo/" target="_blank">
                    <div class="social-icon-instagram">
                      <i class="fa fa-instagram"></i>
                    </div>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="second-f clearfix row">
        <div class="col-xs-6">
          Copyright&copy; {{date('Y')}} Aquantuo
        </div>
        <div class="col-xs-6 text-right">
          <a href="{{url('terms-and-conditions')}}" target="_blank">Terms and Conditions</a> &nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;
          <a href="{{url('privacy-policy')}}" target="_blank">Privacy Policy</a>
        </div>
      </div>
    </div>
    </div>
    </div>
    </div>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-80596505-1"></script>
    <script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-80596505-1');
</script>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    {!! Html::script('theme/web/promo/js/waypoints.min.js') !!}


    @section('script')

    @show

    @yield('inline-script')

    <script>
    $('body')
    .off('click.dropdown touchstart.dropdown.data-api', '.dropdown')
    .on('click.dropdown touchstart.dropdown.data-api', '.dropdown form', function (e) { e.stopPropagation() })
    window.jQuery || document.write('<script src="js/jquery-1.9.1.min.js"><\/script>')

    function myFunction() {
      document.getElementById("#mypopup1").removeClass("animat");
    }

    $(".custom-table").wrap('<div class="table-responsive">');
    $('.required').each(function(){
      if($(this).hasClass('required'))
      {
        var myhtml = "<span class='red-star'> *</span>"
        $(this).prev('label').append(myhtml);
      }
    });
    $(document).ready(function(e) {

      $(".close-button").click(function() {
        $(".custom-popup").addClass("animat");
      });

    });


    $(document).ready(function(){
      $('#year').html(new Date().getFullYear());
    });




    </script>

        <script type='text/javascript' data-cfasync='false'>
    window.purechatApi = {
        l: [],
        t: [],
        on: function() {
            this.l.push(arguments);
        }
    };
    (function() {
        var done = false;
        var script = document.createElement('script');
        script.async = true;
        script.type = 'text/javascript';
        script.src = 'https://app.purechat.com/VisitorWidget/WidgetScript';
        document.getElementsByTagName('HEAD').item(0).appendChild(script);
        script.onreadystatechange = script.onload = function(e) {
            if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) {
                var w = new PCWidget({
                    c: '8d16e161-79d5-4422-9578-91cf910edaad',
                    f: true
                });
                done = true;
            }
        };
    })();
</script>
    @if(!session::has('latitude') && Session::has('UserId'))
      @include('Page::layout.share_location')
    @endif

    @if(!empty(session()->get('UserId')) && !Request::ajax())
      	{!! Html::script('theme/web/js/user.js')!!}


<!--
        {!! Html::script('service/new_services/node_modules/socket.io/node_modules/socket.io-client/dist/socket.io.js') !!}
-->
{!! Html::script('service/chat_services/node_modules/socket.io/node_modules/socket.io-client/dist/socket.io.js') !!}


        <!-- <script src="https://52.5.191.55/service/new_services/node_modules/socket.io/node_modules/socket.io-client/dist/socket.io.js"></script> -->

        <script >


          //var socket = io.connect('https://aquantuo.com:8443',{secure: true});
        // var socket = io('https://aquantuo.com:8443');
        </script>

        {!! Html::script('theme/web/chat/moment.js') !!}
       {!! Html::script('theme/web/chat/chat.js')!!}
      	<!-- @include('Page::layout.chat') -->
    @endif

</body>
</html>
<div class="modal fade" id="popup" data-backdrop="false" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div id="mypopup1" class="custom-popup animat">
      <div class="times"><a href="#" data-dismiss="modal" class="close-button"></a></div>
      <div class="">

        <a href="{{url('on-line-purchases')}}">
          <div class="row-opt clearfix">
            <div class="col-md-2 col-xs-2">
              <img class="row-opt-img" alt="Online purchase" src="{{asset('theme/web/promo/images/online_purchase.png')}}">
            </div>
            <div class="col-md-10 col-xs-10 row">
              <div class="options">
                <div>Your Online Purchases/Mail a Package to us</div>
                <p style="text-align: justify;">Shop online, pay for it and have it shipped to your unique Aquantuo address OR Mail a package in your possession to Aquantuo. We will then have it sent to an address of your choosing.</p>

              </div>
            </div>
            <div class="clearfix"></div>
          </div>
        </a>


        <a href="{{url('buy-for-me')}}">
          <div class="row-opt clearfix">
            <div class="col-md-2 col-xs-2">
              <img class="row-opt-img" alt="Buy for me" src="{{asset('theme/web/promo/images/buy_for_me.png')}}">
            </div>
            <div class="col-md-10 col-xs-10 row">
              <div class="options">
                <div>Buy For Me</div>
                <p>Tell us what you need help purchasing. We will buy it and have it sent to you.</p>
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
        </a>

        <a href="{{url('new-prepare-request')}}">
          <div class="row-opt clearfix">
            <div class="col-md-2 col-xs-2">
              <img class="row-opt-img" alt="Send a package" src="{{asset('theme/web/promo/images/preapre_rqst.png')}}">
            </div>
            <div class="col-md-10 col-xs-10 row">
              <div class="options">
                <div>Send a Package</div>
                <p>Request for a package to be picked up and delivered to an international address.</p>
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
        </a>

        <a href="{{url('local-new-prepare-request')}}">
          <div class="row-opt clearfix">
            <div class="col-md-2 col-xs-2">
              <img class="row-opt-img" alt="Local delivery" src="{{asset('theme/web/promo/images/delivery_rqst.png')}}">
            </div>
            <div class="col-md-10 col-xs-10 row">
              <div class="options">
                <div>Local Delivery</div>
                <p>Within Ghana only. Have a package moved from one place to another.</p>

              </div>
            </div>
            <div class="clearfix"></div>
          </div>
        </a>

        <!-- <a href="{{url('prepare-request')}}">
          <div class="row-opt clearfix">
            <div class="col-md-2 col-xs-2">
              <img class="row-opt-img" src="{{asset('theme/web/promo/images/preapre_rqst.png')}}">
            </div>
            <div class="col-md-10 col-xs-10 row">
              <div class="options">
                <div>Send A Package</div>
                <p>Request for a package to be moved from one place to another.</p>
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
        </a> -->

      </div>
    </div>
    </div>
</div>
</div>
<div class="col-sm-12">
@include('User::chat_box')
</div>

<div class="backdrop"></div>

<style>
 .backdropo{display:none;}
.backdrop.active{position:fixed; top:0; left:0; height:100%; width:100%; height:100%; background:rgba(0,0,0,0.5); z-index:3; display:block;}
</style>
<script>
 //~ var jData = JSON.stringify({
                //~ "lat": "31.7860603",
                //~ "long": "-132.0853276",
                //~ "name": "deepak",
                //~ "address": "bhopal",

            //~ });

           //~ socket.emit("login", jData, function(response) {

                //~ });
</script>
