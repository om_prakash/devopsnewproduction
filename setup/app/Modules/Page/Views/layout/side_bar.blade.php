<style type="text/css">
	@media screen and (max-width: 767px) {
		.nav.nav-pills.nav-stacked li a {
		    color: #020101 !important;
		    padding: 6px 25px;
		}
	}
</style>



	<div class="col-md-3 col-sm-4 col-xs-12 first">
	
						<div class="box-shadow ">

							<div class="col-xs-12">
							<h4><b>My Account</b></h4>
							<ul class="nav nav-pills nav-stacked">
								<li class="">
									<a href="{{url('my-profile')}}"><i aria-hidden="true" class="fa fa-user"></i>
										<span class="">&nbsp; &nbsp; General Information</span>
									</a>
								</li>
								<li class="">
									<a href="{{url('my-aquantuo-address')}}" >
										<i aria-hidden="true" class="fa fa-map-marker"></i>
										<span class="">&nbsp; &nbsp; My Aquantuo Address</span>
									</a>
								</li>
								<li class="">
									<a href="{{url('card-list')}}">
										<i aria-hidden="true" class="fa fa-credit-card"></i>
										<span class="">&nbsp; Cards</span>
									</a>
								</li>
								
								@if(Session::get('Usertype') != 'requester')
								<li class="">
									<a href="{{url('bank-information')}}">
										<i aria-hidden="true" class="fa fa-bank"></i>
										<span class="">&nbsp; Bank Information</span>
									</a>
								</li>
								@endif
								<li class="">
									<a href="{{url('reward')}}">
										<i class="fa fa-gift" aria-hidden="true"></i>
										<span class="">&nbsp; Reward</span>
									</a>
								</li>
                                <li class="">
									<a href="{{url('currency')}}">
										<i class="fa fa-eur" aria-hidden="true"></i>
										<span class="">&nbsp; Currency</span>
									</a>
								</li>
								<li class="collapse in">
									<a href="{{url('address')}}">
									<i class="fa fa-check" aria-hidden="true"></i>
									Saved Addresses
									</a>
								</li>

								<li class="collapse in">
									<a href="{{url('setting')}}">
									<i aria-hidden="true" class="fa fa-cog"></i> &nbsp;General Settings
									</a>
								</li>
								
								

								<!-- <li class="settings">
                                	<a href="#collapseExample" class="collapsed" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="collapseExample" style="background:#eee;">
									
										<i aria-hidden="true" class="fa fa-cog"></i>
										<span class="">&nbsp; Settings</span>
										<span class="glyphicon glyphicon-chevron-right pull-right"></span>
									</a>
									@if(in_array(Request::segment(1),['setting','address'])) 
										<div class="collapsed in" id="collapseExample" aria-expanded="false">
									@else
										<div class="collapse" id="collapseExample" aria-expanded="false" style="@if(!in_array(Request::segment(1),['setting'])) height:0px; @endif">
									@endif
									<ul class="nav nav-pills nav-stacked new ">
										<li class="collapse">
											<a href="{{url('setting')}}">General</a>
										</li>
                                          @if(in_array(Session::get( 'Usertype'), ['requester'] )) 
										<li class="">
											<a href="{{url('become-transporter')}}">Become a Transporter</a>
										</li>
                                          @endif  
										<li class="collapse in">
											<a href="{{url('address')}}">Saved Addresses</a>
										</li>

										<li class="">
											<a href="{{url('faq-list')}}">FAQ</a>
										</li>
										<li class="">
											<a href="{{url('terms-and-conditions')}}" target="_blank">Terms of Service</a>
										</li>
										<li class="">
											<a href="{{url('privacy-policy')}}" target="_blank">Privacy Policy</a>
										</li>

									</ul>
                                    </div>
								</li> -->
							</ul>
						</div>
				   </div> 
				</div>
