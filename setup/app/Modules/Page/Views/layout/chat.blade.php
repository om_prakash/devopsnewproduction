{!! Html::style('theme/web/chat/css/converse.min.css') !!}
{!! Html::script('theme/web/chat/converse.js') !!}
<style type="text/css">
    #conversejs { height: 10px !important; }
</style>
<script>
 require(['converse'], function (converse) {
  (function () {
    /* XXX: This function initializes jquery.easing for the https://conversejs.org
    * website. This code is only useful in the context of the converse.js
    * website and converse.js itself is NOT dependent on it.
    */
    var $ = converse.env.jQuery;
    $.extend( $.easing, {
        easeInOutExpo: function (x, t, b, c, d) {
            if (t==0) return b;
            if (t==d) return b+c;
            if ((t/=d/2) < 1) return c/2 * Math.pow(2, 10 * (t - 1)) + b;
            return c/2 * (-Math.pow(2, -10 * --t) + 2) + b;
        },
    });

    $(window).scroll(function() {
        if ($(".navbar").offset().top > 50) {
            $(".navbar-fixed-top").addClass("top-nav-collapse");
        } else {
            $(".navbar-fixed-top").removeClass("top-nav-collapse");
        }
    });
    //jQuery for page scrolling feature - requires jQuery Easing plugin
    $('.page-scroll a').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 700, 'easeInOutExpo');
        event.preventDefault();
    });
  })(); //onProviderConnect
converse.initialize({
    providers_link : "http://aquantuo.com",
    //bosh_service_url: "https://aquantuo.com:7070/http-bind/", // Please use this connection manager only for testing purposes
    //prebind: true,
    bosh_service_url: 'https://aquantuo.com:7443/http-bind/',
    keepalive: true,
    message_carbons: true,
    play_sounds: true,
    roster_groups: false,
    show_controlbox_by_default: false,
    allow_contact_removal : false,
    allow_contact_requests : false,
    allow_muc : false,
    allow_registration : false,
    auto_login : true,
    auto_reconnect : true,
    auto_subscribe : true,
    jid: '{{session()->get("UserId")}}@aquantuo.com',
    password: '{{session()->get("UserId")}}',
    allow_logout: false,
    xhr_user_search_url: false,
    xhr_user_search: false,
    allow_otr : false,
    locked_domain: 'aquantuo.com',
    visible_toolbar_buttons : {
        call: false,
        clear: false,
        emoticons: false,
        toggle_occupants: true
    },
    notification_icon: "{{  asset('theme/web/promo/images/preloader.gif') }}"
  });
});



function open_chat_box(userjid,name,loaderid) {
	var jid = userjid+'@aquantuo.com';
	$('#'+loaderid).addClass('spinning');

    $.ajax({
        url : SITEURL+'user/check-chat-user',
        method: 'POST',
        data : {'chat_to_id' : userjid},
        success : function(res){
            var obj = JSON.parse(res);
            if(obj.success == 1) {
                try {
                	$('#'+loaderid).removeClass('spinning');
                    converse.chats.open(jid);
                } catch(err) {
                    try
                    {
                    	$('#'+loaderid).addClass('spinning');
                        add_to_contact(userjid,name,function(id){
                        	setTimeout(function(){
                        		$('#'+loaderid).removeClass('spinning');
	                        	console.log(jid);
	                        	converse.chats.open(userjid);
                        	}, 2000);
                        });
                    } catch(err2) {   $('#'+loaderid).removeClass('spinning');  	}
                }
            } else {
				$('#'+loaderid).removeClass('spinning');
                alert('Oops! You are not authorized.');
            }
        }
    });
 }

function add_to_contact(userjid,name,callback) {
	var jid = userjid+'@aquantuo.com';
	converse.contacts.add(jid,name,function(){

    });
	callback(jid);
}

</script>
