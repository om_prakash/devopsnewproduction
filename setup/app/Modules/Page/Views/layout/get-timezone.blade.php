<?php
echo <<<EOE
 <script type="text/javascript">
   if (navigator.cookieEnabled)
     document.cookie = "tzo="+ (- new Date().getTimezoneOffset());
 </script>
EOE;
if (!isset($_COOKIE['tzo'])) {
  echo <<<EOE
    <script type="text/javascript">
      if (navigator.cookieEnabled) window.location.reload();
      else alert("Cookies must be enabled!");
    </script>
EOE;
}
?>