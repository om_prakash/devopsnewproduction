@if(count($state) >0)
    <option value="">Select Region</option>
    @foreach($state as $key)
        <option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}"}' @if($oldtext == strtolower($key->Content)) selected="selected" @endif>{{$key->Content}}</option>
    @endforeach
@else
    <option value="">State not found</option>
@endif
