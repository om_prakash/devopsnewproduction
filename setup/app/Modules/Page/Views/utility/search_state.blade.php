<select name="state{{$append_id}}" class="form-control required left-disabled chosen-select" id="pp_pickup_state{{$append_id}}" onchange="get_city('pp_pickup_state{{$append_id}}','pp_pickup_city{{$append_id}}','pp_pickup_city{{$append_id}}','')"> 

@if(count($state) >0)
<!-- chosen-select -->
	@if($country == 'UK')				
    <option value="">Select Postal Code</option>
    @else
    <option value="">Select State/Region</option>
    @endif
    @foreach($state as $key)
        <option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}", "short_name": "{{ $key->short_name }}"}' @if($oldtext == strtolower($key->Content)) selected="selected" @endif>{{$key->Content}}</option>
    @endforeach
@else
    <option value="">State not found</option>
@endif
 </select>
<script>

jQuery(document).ready(function ($) {
	$(".chosen-select").chosen({no_results_text: "Oops, nothing found!"})
		});
	$(".chosen-select").chosen().change(function() {
        //alert(+$(this).val());
    });

    var obs = $('.chosen-select').chosen({width: '100%'}).data('chosen');
    obs.search_field.on('keyup', function(e){
        if($(this).val().length > 3){
            get_postcode('{{$append_id}}',$(this).val());
        }
    });
</script>
{!! Html::script('theme/web/js/utility.js') !!}

