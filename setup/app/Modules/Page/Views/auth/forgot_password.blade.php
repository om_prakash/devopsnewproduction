@extends('Page::layout.one-column-page')
@section('page_title')
Forgot Password- Aquantuo
@endsection
@section('bg-color')

@endsection
@section('content')

<style>
.wrapper {
  background: rgba(0, 0, 0, 0) url("theme/web/promo/images/slider_img_01.jpg") repeat fixed 0 0 / cover ;
}
.wrapper.clearfix {
  padding-top: 80px;
  margin-top:0px;
}
@media screen and (max-width: 768px) {
    .wrapper.clearfix {
        padding-top: 50px;
        margin-top:15px;
}
}
.forgot {
    margin:40px auto;
    width: 400px;
}
@media screen and (max-width: 450px) {
    .forgot {
        width: 290px !important;
    }
}
</style>

<div class="row">
  <div id="" class="container">
     <div class="col-md-12 col-xs-12">
     	<div class="login-bg forgot">
        <div class="heading-blue">
           Forgot Password
        </div>
        <div class="col-md-12 text-center"><label>It happens to all of us, we can help…</label></div>
        {!! Form::open(array('url' => 'forgot-password', 'class' => 'form-horizontal','id'=>'ServicesForm' )) !!}
            <div class="col-sm-12 col-xs-12">
               <div class="form-group">
                  {!! Form::label('Email', 'Email',['class'=>'control-label']) !!}
                 <div class="input-img-hldr"> {!! Form::text('email', '',['class'=>'form-control', 'placeholder'=> 'Email','maxlength' =>'60'] ) !!}
                  <span><i class="fa fa-envelope input-img"  aria-hidden="true"></i></span> </div>
                  <p class="help-block red" id='er_email' style="color:red">
                     @if($errors->has('email')){{ $errors->first('email') }}@endif
                  </p>
               </div>
            </div>
            <div class="col-sm-12 col-xs-12">
               <div class="form-group">
                    <button type="submit" class="btn btn-primary" name="save" onclick = "return check_password()" value="Submit">Submit</button>
                    <a href = "{{url('login')}}" class = 'btn btn-primary'>Go Back</a>
                </div>
            </div>

        {!! Form::close() !!}
        <div class="clearfix"></div>
		</div>
        <div class="clearfix"></div>
     </div>
     <!--div class="col-md-7 col-xs-12 hidden-md hidden-lg">
        <h2 class="color-blue">What is new in Aquantuo</h2>
        <ul class="features-ul">
           <li>-   Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
           <li>-   or sit amet, consg elit.</li>
           <li>-   Lorem ipsumt amet, ctetur adipiscing elit.</li>
           <li>-    ipsum dolor sit amet, consectetur adipiscing elit.</li>
           <li>-   Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
           <li>-   or sit amet, consg elit.</li>
           <li>-   Lorem ipsumt amet, ctetur adipiscing elit.</li>
           <li>-    ipsum dolor sit amet, consectetur adipiscing elit.</li>
           <li>-   Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
           <li>-   or sit amet, consg elit.</li>
           <li>-   Lorem ipsumt amet, ctetur adipiscing elit.</li>
           <li>-    ipsum dolor sit amet, consectetur adipiscing elit.</li>
        </ul>
     </div-->
  </div>
</div>

@endsection
