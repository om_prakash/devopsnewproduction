@extends('Page::layout.one-column-page')
@section('bg-color')

@endsection
@section('page_title')
Sign Up for International Shipping- Aquantuo
@endsection

@section('content')
{!! Html::script('theme/web/js/validations.js') !!}
<style>
.wrapper {
  background: rgba(0, 0, 0, 0) url("theme/web/promo/images/slider_img_01.jpg") repeat fixed 0 0 / cover ;
}
.wrapper.clearfix {
  padding-top: 80px;
  margin-top:0px;
}
@media screen and (max-width: 768px) {
	.wrapper.clearfix {
		padding-top: 50px;
		margin-top:15px;
}
}
</style>
<div class="row">
      <div id="" class="container">
         <div class="col-sm-7 col-xs-12 hidden-sm hidden-xs">
            {{-- <h2 class="color-blue">What is new in Aquantuo</h2> --}}
            <br /><br />
            @if(Request::segment(1) == 'transporter-signup')
              <p class="color-white">
                <strong>Our affiliate program allows us to partner with other trusted businesses that are into sea freight in areas that we are not operational. It also allows us to engage with individuals who are traveling internationally by air that would like to earn some extra cash with spare or excess luggage.</strong><br />
              </p>

              <p class="color-white">
                <strong>Partnering with other reliable sea freight companies allows us to serve a larger market while reducing transportation costs for our clients.</strong><br />
              </p>

              <p class="color-white">
                <strong>Ready to be a transporter? Sign up, we will reach out promptly and get the process started.</strong><br />
              </p>
              
            @endif

              @if(Request::segment(1) != 'transporter-signup')
              <ul class="features-ul dashed-ul font-size13">
                <li>The days of asking: “Who is traveling to…?” or “How can I get this package to…?” are over. <br /> Aquantuo takes care of that! </li>
               <li>Shop any store online and we will get the item sent to you internationally. </li>
               <li>Tell us what you want to buy, we will help get it and have it sent to you.</li>
               <li>Need to send any item from one country to the other? We will help you.</li>
               <li>Aquantuo is affordable, faster and safer.</li>
              </ul>
              @endif


         </div>
         <div class="col-md-5 col-xs-12">
         	<div class=" login-bg">
            <div class="heading-blue">
              Join
            </div>

            {!! Form::open(array('url' => 'signup', 'files' => 'true', 'id' => 'signup' , 'class' => 'form-vertical label-less business','onsubmit'=>"return check_aggrement()") ) !!}
            <?php if (Input::get('type') == 'transporter') {?>
               <div class="col-sm-12 col-xs-12">
                  <div class="form-group clearfix">
                     <div class="my-tab clearfix">
                        <div class="sliding-div" id="ac_type_bg"></div>
                        <div class="tab1" id="individual" onclick ="transporter_type('individual')" style="color:#fff;" >Individual<span> Transporter</span></div>
                        <div class="tab2" id="transporter" onclick="transporter_type('business')" style="color:#fff;">Business<span> Transporter</span></div>
                        <input type="hidden" name ='transportertype' value='individual' id='transportertype' >
                     </div>
                  </div>
               </div>
            <?php }?>
            <div class="col-sm-6" id="first_name">
               <div class="form-group">
                  {!! Form::text('firstName', '',['class'=>'form-control', 'placeholder'=> 'First Name' ,'maxlength' => 60]) !!}
                  <p class="help-block red" id='er_firstName' style="color:red">
                     @if($errors->has('firstName')){{ $errors->first('firstName') }}@endif
                  </p>
               </div>
            </div>
            <div class="col-sm-6" id="last_name">
               <div class="form-group">
                  {!! Form::text('lastName', '',['class'=>'form-control', 'placeholder'=> 'Last Name','maxlength' => 60]) !!}
                  <p class="help-block red" id='er_lastName' style="color:red">
                     @if($errors->has('lastName')){{ $errors->first('lastName') }}@endif
                  </p>
               </div>
            </div>
             @if(Request::segment(1) != 'signup')
            <div class="col-sm-12 col-xs-12" id="business_name" style="display:none;" >
               <div class="form-group">
                  {!! Form::text('business_name', '',['class'=>'form-control', 'placeholder'=> 'Business Name','maxlength' => 60]) !!}
                  <p class="help-block red" id='er_business_name' style="color:red">
                     @if($errors->has('business_name')){{ $errors->first('business_name') }}@endif
                  </p>
               </div>
            </div>
           @endif

            <div class="col-sm-12 col-xs-12">
               <div class="form-group">
                  {!! Form::text('email', '',['class'=>'form-control', 'placeholder'=> 'Email','id'=>'email','maxlength' =>60]) !!}
                  <p class="help-block red" id='er_email' style="color:red">
                     @if($errors->has('email')){{ $errors->first('email') }}@endif
                  </p>
               </div>
            </div>
            <div class="col-sm-12 col-xs-12">
               <div class="form-group">
                  {!! Form::password('password',['class'=>'form-control', 'placeholder'=> 'Password','maxlength' =>80]) !!}
                  <p class="help-block red" id='er_password' style="color:red">
                     @if($errors->has('password')){{ $errors->first('password') }}@endif
                  </p>
               </div>
            </div>
            <div class="col-sm-12 col-xs-12">
               <div class="form-group">
                  {!! Form::password('confirmPassword',['class'=>'form-control', 'placeholder'=> 'Confirm Password','maxlength' =>80]) !!}
                  <p class="help-block red" id='er_confirmPassword' style="color:red">
                     @if($errors->has('confirmPassword')){{ $errors->first('confirmPassword') }}@endif
                  </p>
               </div>
            </div>
            <div class="col-sm-12 col-xs-12">
               <input type="hidden"  name="usertype"   value="requester">
            </div>
            <div class="col-sm-12 col-xs-12">
               <div class="form-group">
                  <div class="checkbox">
                     @if(Request::segment(1) == 'transporter-signup')
                      <label>
                        <input type="checkbox" value="yes" name="become_transporter" class="mt1" onclick="return accept_condition();" id="become_transporter">&nbsp;&nbsp;
                           <span id="tp-checkbox-msg">Signing up to move packages when I travel internationally or I am a business that wants to ship items by sea for Aquantuo</span>
                        
                        </label>
                    @endif
                  </div>

                  


               </div>
            </div>
            <div class="col-sm-12 col-xs-12">
               <p class="color-white text-left">By signing up, you agree to Aquantuo’s <a href="{{url('terms-and-conditions')}}" target="_blank">Terms and
                  Conditions</a> and <a href="{{url('privacy-policy')}}" target="_blank">Privacy Policy</a>
               </p>
            </div>
            <div class="col-sm-12 col-xs-12">
               <div class="form-group">
                  <button class="custom-btn1 btn-block "  style="margin-bottom:0;">
                     Signup
                     <div class="custom-btn-h"></div>
                  </button>
               </div>
               <div class="clearfix"></div>
               <!-- <p align="center" class="text-center" style="margin-top:10px; margin-bottom:0px;"><a href="{{url('login')}}"><strong>Back to Login</strong></a></p> -->

              <p align="center" class="text-center" style="color:white;margin-top:10px; margin-bottom:0px;">Already have an account? <a href="{{url('login')}}">Sign in</a></p>


            </div>
            <div class="clearfix"></div>
            {!! Form::close() !!}
            </div>
         </div>
         <div class="col-md-7 col-xs-12 hidden-md hidden-lg">
          @if(Request::segment(1) == 'transporter-signup')
              <p class="color-white">
                <strong>Travelling now or in the future? Make money with your spare luggage space.<br />
                Join the global community of peer-to-peer package delivery</strong><br /><br />
                <strong>Here's how...</strong>
              </p>
              <ul class="features-ul dashed-ul font-size13" style="margin-left: 10px;">
                </li>
                <li>At sign up, choose to move items as your travel</li>
                <li>Aquantuo verifies your identity</li>
                <li>Post the details of your next trip </li>
                <li>Find and accept items from the pool of users intended for your destination country or let Aquantuo assign a package to you</li>
                <li>Arrange a meet to pick up the items or have the items mailed to you</li>
                <li>Arrive at the destination country and hand the package over to Aquantuo or the recepient</li>
                <li>Get PAID!</li>
              </ul>
            @endif

        @if(Request::segment(1) != 'transporter-signup')
          <ul class="features-ul dashed-ul font-size13">
            <li>The days of asking: “Who is traveling to…?” or “How can I get this package to…?” are over. <br /> Aquantuo takes care of that! </li>
           <li>Shop any store online and we will get the item sent to you internationally. </li>
           <li>Tell us what you want to buy, we will help get it and have it sent to you.</li>
           <li>Need to send any item from one country to the other? We will help you.</li>
           <li>Aquantuo is affordable, faster and safer.</li>
          </ul>
          @endif



     </div>
      </div>
</div>
<style>

</style>
<script type="text/javascript">

  function check_aggrement()
  {
    var flage = true;
    var checked =$('#become_transporter').prop('checked');
    if("<?php echo Input::get('type')?>" == 'transporter'){
      if(checked == 0){
        alert("Please accept aggrement.");
        flage= false;
      }else{
        flage= true;
      }
    }
    return flage;
  }

  function accept_condition()
  {
      flage= false;
      checked =$('#become_transporter').prop('checked');

      if(checked == 0){
        flage = true;
        
      }else{
        if(confirm('You are also electing to move items as you travel as an individual or as a business that ships items, additional information would be required.')){
          flage = true;
        }
      }
      return flage;
  }


   function transporter_type(type)
   {
   	if(type == 'individual') {

   		$('#ac_type_bg').css('right','50%');
   		$('#transportertype').val('individual');
      $('#tp-checkbox-msg').html('Signing up to move packages as I travel.');
   	} else {
      $('#tp-checkbox-msg').html('Signing up to move packages as a Business or as I travel.');
   		$('#ac_type_bg').css('right','0%');
   		$('#transportertype').val('business');
   	}

   }
   @if(Input::has('tytype'))
      transporter_type('{{ Input::get('tytype') }}');
   @endif
   $('#individual').click(function(){

 //   $('#first_name').show();
  //  $('#last_name').show();
    $('#business_name').hide();
   // $('#VatTaxNo').hide();

  });

   $('#transporter').click(function(){

   // $('#first_name').hide();
   // $('#last_name').hide();
    $('#business_name').show();
  //  $('#VatTaxNo').show();

  });



new Validate({
    FormName :  'signup',
    ErrorLevel : 1,

   });

</script>
@endsection
