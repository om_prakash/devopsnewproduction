@extends('Page::layout.one-column-page')
@section('bg-color')

@endsection
@section('page_title')
Login to my shipping account -  Aquantuo
@endsection

@section('content')

<style>
.wrapper {
  background: rgba(0, 0, 0, 0) url("theme/web/promo/images/slider_img_01.jpg") repeat fixed 0 0 / cover ;
}
.wrapper.clearfix {
  padding-top: 80px;
  margin-top:0px;
}
@media screen and (max-width: 768px) {
	.wrapper.clearfix {
		padding-top: 50px;
		margin-top:15px;
}
}
</style>

<div class="row">
  <div id="" class="container">
<p id="demo"></p>
     <div class="col-md-7 col-sm-7 col-xs-12 hidden-sm hidden-xs">
        {{-- <h2 class="color-blue">What is new in Aquantuo</h2> --}}
        <br/><br/>
        <ul class="features-ul dashed-ul font-size13">
           <li>The days of asking: “Who is traveling to…?” or “How can I get this package to…?” are over. <br /> Aquantuo takes care of that! </li>
           {{-- <li>Shop online or place your listing today and we will help ensure you get your package. It's affordable, faster and safer.</li>
           <li>On our platform, you will find Individual and Business Transporters local to your community and much farther who will help get your package(s) to its destination.</li> --}}
           <li>Shop any store online and we will get the item sent to you internationally. </li>
           <li>Tell us what you want to buy, we will help get it and have it sent to you.</li>
           <li>Need to send any item from one country to the other? We will help you.</li>
           <li>Aquantuo is affordable, faster and safer.</li>

        </ul>
     </div>
     <div class="col-md-5  col-sm-5 col-xs-12">
     	<div class=" login-bg">
        <div class="heading-blue">
           Sign in
        </div>

        {!! Form::open(['name'=>'ServicesForm','id' => 'ServicesForm','url'=> 'login', 'class' => 'form-vertical','method' => 'POST']) !!}
        <input type="hidden" name="session_lat" value="0" id="session_lat">
        <input type="hidden" name="session_lng" value="0" id="session_lng">
        <div class="col-sm-12">
           <div class="form-group">
              {!! Form::label('Email', 'Email',['class'=>'control-label']) !!}
              <?php $email = isset($_COOKIE['Email']) ? $_COOKIE['Email'] : '';
$password = isset($_COOKIE['password']) ? $_COOKIE['password'] : '';
?>
             <div class="input-img-hldr"> {!! Form::text('email', $email,['class'=>'form-control', 'placeholder'=> 'Email','id'=>'email','maxlength' =>60]) !!}
              <span><i class="fa fa-envelope input-img"  aria-hidden="true"></i></span> </div>
              <p class="help-block red" id='er_email' style="color:red">
                 @if($errors->has('email')){{ $errors->first('email') }}@endif
              </p>
           </div>
        </div>
        <div class="col-sm-12">
           <div class="form-group">
              {!! Form::label('password', 'Password',['class'=>'control-label']) !!}
              <div class="input-img-hldr">
                <input type="password" class="form-control" placeholder='Password' id='password' maxlength="80" value="{{$password}}" name="password">
              <span><i class="fa fa-lock input-img" aria-hidden="true"></i></span>   </div>
              <p class="help-block red" id='er_password' style="color:red">
                 @if ($errors->has('password')){{ $errors->first('password') }}@endif
              </p>
           </div>
        </div>
        <div class="col-sm-12">
           <div class="form-group">
              <div class="checkbox">
                 <label><input type="checkbox" tabindex="4" class="field login-checkbox" name="remember" id="Field" @if(isset($_COOKIE['Email'])) checked @endif >&nbsp;Remember me</label>
                  <p class="color-blue pull-right" ><a href="{{url('forgot-password')}}">Forgot Password?</a></p>
              </div>
           </div>
        </div>

        <div class="col-sm-12">
           <div class="form-group">
              <button class="custom-btn1 btn-block">
                 Login
                 <div class="custom-btn-h"></div>
              </button>
           </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-sm-12">
           <div class="form-group">
        <label align="center">New to Aquantuo? <a href="{{url('signup')}}">Sign Up</a></label>
        </div>
        </div>

        {!! Form::close() !!}
        <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
     </div>
     <div class="col-md-7 col-sm-7 col-xs-12 hidden-md hidden-lg">
        <h2 class="color-blue">What is new in Aquantuo</h2>
       <ul class="features-ul dashed-ul font-size13">
           <li>The days of asking: “Who is traveling to…?” or “How can I get this package to…?” are over. <br /> Aquantuo takes care of that! </li>
           {{-- <li>Shop online or place your listing today and we will help ensure you get your package. It's affordable, faster and safer.</li>
           <li>On our platform, you will find Individual and Business Transporters local to your community and much farther who will help get your package(s) to its destination.</li> --}}
           <li>Shop any store online and we will get the item sent to you internationally. </li>
           <li>Tell us what you want to buy, we will help get it and have it sent to you.</li>
           <li>Need to send any item from one country to the other? We will help you.</li>
           <li>Aquantuo is affordable, faster and safer.</li>

        </ul>
     </div>
  </div>
</div>
<script>
var x = document.getElementById("demo");
$(document).ready(function() {


    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else {
        x.innerHTML = "Geolocation is not supported by this browser.";
    }

});


function showPosition(position) {
  $("#session_lat").val(position.coords.latitude);
  $("#session_lng").val(position.coords.longitude);
    //x.innerHTML = "Latitude: " + position.coords.latitude +
    //"<br>Longitude: " + position.coords.longitude;
}

</script>

@endsection
