@extends('Page::layout.one-column-page')
@section('page_title')
Term and Condition - Aquantuo
@endsection
@section('content')

  <div class="container">
  @if(Session::get('UserId') == '')
   <br/><br/>
  @endif
    <div class="heading2">
     <h1> Terms and Conditions</h1>
    </div>
      <?php echo $content['Content']; ?>
  </div>

@endsection

