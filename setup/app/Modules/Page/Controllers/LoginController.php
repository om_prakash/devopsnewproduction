<?php namespace App\Modules\Page\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Models\User;
use App\Library\OpenFireRestApi;
use App\Library\Utility;
use Illuminate\Support\Facades\Validator;
use Input;
use Redirect;
use Session;
use App\Http\Models\Setting;

class LoginController extends Controller
{

    public function __construct()
    {
        if (session()->has('UserId')) {
            return Redirect::to('dashboard')->send();
        }
    }

    /* Developer Name : Aakash tejwal
     * Date : 17-08-2016
     * About Function : This function is use for display login from
     * Method : Post
     * Lest Update :
     * Update by :
     *
     */
    public function login()
    {
        return view('Page::auth.login');
    }

    /* Developer Name : Aakash tejwal
     * Date : 17-08-2016
     * About Function : This function is use for login (transporter,requester)
     * Method : Post
     * Lest Update :
     * Update by :
     *
     */
    public function login_post()
    {
        $v = Validator::make(Input::get(), ['email' => 'required|email', 'password' => 'required']);

        if ($v->fails()) {
            return Redirect::to('login')->withErrors($v)->withInput();
        } else {

            $result = User::where(['Email' => strtolower(Input::get('email')), 'Password' => md5(trim(Input::get('password')))])
            ->where('delete_status','!=','yes')
            ->first();
			
            if (count($result) > 0) {

                if ($result->delete_status == 'yes') {
                    return Redirect::to('login')->with('danger', "Fail! The email address or password that you've entered doesn't match.");
                }

                if (in_array('active', [$result->TransporterStatus, $result->RequesterStatus])) {

                    /*(new OpenFireRestApi())->addUser($result->_id, (String) $result->_id, $result->Name, $result->Email, array('Group 1'));*/

                    LoginController::set_user_session($result);
                   
                    if (Input::get('remember') == 'on') {
                        setcookie('Email', strtolower(Input::get('email')));
                        setcookie('password', Input::get('password'));
                    } else {
                        setcookie("Email", "", time() - 3600);
                        setcookie("password", "", time() - 3600);
                    }

                    if ($result->temprary) {
                        $result->temprary = '';
                        $result->save();
                        
                        return Redirect::to($result->detail_url)->withSuccess('Success! You are logged in successfully.');
                    } else {
                        $result->DeviceType = 'website';
                        if(Input::get('session_lat') != '' && Input::get('session_lng') != ''){
                           
                            $result->CurrentLocation= [floatval(Input::get('session_lng')),floatval(Input::get('session_lat'))];
                            
                        }
                        $result->save();
                        
                        if (strtolower(@$result->packagePayment) == 'yes') {
                            $users = User::where(['_id' => $result->_id])->update(['packagePayment' => 'no']);
                            return redirect()->away($result->link);
                            //return redirect($result->link);
                        }
                        if (@$result->Street1 == '' || @$result->Country == '' || @$result->State == '' || @$result->City == '' || @$result->PhoneNo == ''){
                            return Redirect::to('edit-profile')->withSuccess('Please update profile !!.');;
                        }

                        return Redirect::to('dashboard')->withSuccess('Success! You are logged in successfully.');
                    }

                } else {
                   
                    return Redirect::to('login')->with('danger', "Fail! Your account is inactive, please contact support.");
                }
            }
            return Redirect::to('login')->with('danger', "Fail! The email address or password that you've entered doesn't match.");
        }
    }

    public static function set_user_session($result)
    {
        $alertMsg = Setting::where('_id', '=', '5c57fefccab743651e132659')->select()->first();
        Session()->put(array(
            'UserId' => $result['_id'],
            'Image' => $result['Image'],
            'Usertype' => $result['UserType'],
            'ShowName' => ucfirst($result['Name']),
            'Name' => ucfirst($result['Name']),
            'FirstName' => ucfirst($result['FirstName']),
            'LastName' => ucfirst($result['LastName']),
            'TransporterType' => $result['TransporterType'],
            'Email' => true,
            'Profile_Status' => $result['ProfileStatus'],
            'AqLatLong' => $result['AqLatLong'],
            'AqLat' => isset($result['AqLatLong'][1]) ? $result['AqLatLong'][1] : '',
            'AqLong' => isset($result['AqLatLong'][0]) ? $result['AqLatLong'][0] : '',
            'AqAddress' => Utility::formated_address([
                $result['AqAddress'],
                'Unit#' . $result['UniqueNo'],
                $result['AqCity'],
                $result['AqState'],
                $result['AqZipcode'],
                $result['AqCountry'],
            ]),
            'Aqphone'=>$result['Aqphone'],
            'Aqcc'=>$result['Aqcc'],
            "Default_Currency" => $result['Default_Currency'],
            "TPVerified" => $result['TransporterStatus'],
            'consolidate_item' => $result['consolidate_item'],
            'CountryCode' => $result['CountryCode'],
            'PhoneNo' => $result['PhoneNo'],
            'lastnotify' => date('d-m-Y H:i:s'),
            'alert_status' => @$alertMsg->status,
            'alert_content' => @$alertMsg->content,
        ));
        if ($result['TransporterType'] == 'business') {
            if (!empty(trim($result['BusinessName']))) {
                Session()->put('ShowName', $result['BusinessName']);
            }
        }
    }
    public function setDatetime()
    {
        Session::put('tzo', Input::get('tzo'));
        return Redirect::to(Input::get('href'));
    }
}
