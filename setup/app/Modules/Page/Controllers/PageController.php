<?php namespace App\Modules\Page\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Models\Setting;
use App\Http\Models\Subscription;
use App\Http\Models\User;
use App\Http\Models\Userextrainfo;
use App\Http\Models\Webcontent;
use App\Library\OpenFireRestApi;
use App\Library\Utility;
use App\Modules\Admin\Models\City_State_Country\City_State_Country;
use Illuminate\Http\Request;
use Input;
use MongoDate;
use Redirect;
use Validator;
use App\Http\Models\SendMail;
use App\Library\Notification\Pushnotification;
use App\Http\Models\SilderImages;
use App\Http\Models\Client;
use App\Http\Models\Category;
use App\Library\NewEmail;
use App\Library\NewEmail2;

use App\Library\Notify;
use App\Events\TestEvent;
use App\Http\Models\Mobilemoney;
use App\Http\Models\Notification;


class PageController extends Controller
{
    public function index()
    {
        return view('Page::index');
    }
    public function signup()
    {
        if (session()->has('UserId')) {
            return Redirect::to('dashboard')->send();
        }
        return view('Page::auth.signup');
    }

   public function check_status()
    {
        $rec = Mobilemoney::where(['country_code'=>'233'])->get();
        //$rec2 = Mobilemoney::where(['country_code'=>'+233'])->count();
        //print_r($rec); die;
        foreach ($rec as $key ) {
           Mobilemoney::where(['_id'=> $key->_id,'country_code'=>'233'])->update(['country_code'=>'+233']);
        }
        echo 'done';

        /*$te = new NewEmail();
            //send_mail_test  $te->send_mail2
        $te->send_mail('5a7d7d4ce4b06005c659a45e', [
           "to" => "kapil@idealittechno.com",
            //"to" => "kapil.idealittechno@gmail.com",
            "replace" => [
                "[USERNAME]" => "ajay queue 56-7-2018".date("Y-m-d H:i:s"),
                
            ],
        ]);
    */
        //echo date("Y-m-d H:i A");
       // echo phpinfo();

    }

    public function check_event(Request $request)
    {
        $input_array = [
            'title'=>'Login Alert',
            'message'=>'Someone wants to login with this account.',
            'type'=> 'Check',
            'notification_id'=>"APA91bERE8WPT0Erq6zTbld7Bf5Q4auKGpkLeEKAbzd4iblCPs0lYZGvAIvO_RhdzE-aoh87Q-g_uJEaPJnRvMx2dMhIZxW0AESo6im5Etz-7sR8kjqgEgA",
            'deviceType'=>"android",
            'locationkey'=>''
        ];
        event(new TestEvent($input_array));
        echo 'sent'; die;

    }

    /* Developer Name : Aakash tejwal
     * Date : 16-08-2016
     * About Function : This function is use for registration (transporter,requester)
     * Method : Post
     * Lest Update :
     * Update by :
     *
     */
    public function registration(Request $request)
    {
        if (session()->has('UserId')) {
            return Redirect::to('dashboard')->send();
        }
        $validation = Validator::make($request->all(), [
            'firstName' => 'required|Max:20|regex:/^[\pL\s\-]+$/u',
            'lastName' => 'required|Max:20|regex:/^[\pL\s\-]+$/u',
            'business_name' => (Input::get('transportertype') == 'business') ? 'required|Min:3' : '',
            'email' => 'required|email|unique:users,Email,yes,delete_status',
            'password' => 'required|Min:6|Max:20',
            'confirmPassword' => 'required|same:password',

        ]);

        if ($validation->fails()) {
            if (!empty(trim(Input::get('transportertype')))) {
                $tptype = 'individual';
                if (Input::get('transportertype') == 'business') {
                    $tptype = 'business';
                }
                return Redirect::to("transporter-signup?type=transporter&tytype=$tptype")->withErrors($validation)->withInput();
            } else {
                return Redirect::to("signup")->withErrors($validation)->withInput();
            }
        } else {
            $UniqueNo = Input::get('firstName')[0] . Input::get('lastName')[0];
            $insert = [
                'FirstName' => ucfirst(Input::get('firstName')),
                'LastName' => ucfirst(Input::get('lastName')),
                'Name' => ucfirst(Input::get('firstName')) . " " . ucfirst(Input::get('lastName')),
                'UniqueNo' => '',
            ];

            $insert['Email'] = strtolower(trim(Input::get('email')));
            $insert['Password'] = md5(trim(Input::get('password')));
            $insert['TransporterType'] = '';
            $insert['UserType'] = 'requester';
            $insert['ChatName'] = "aquantuo" . Utility::sequence('user');
            $insert['CountryCode'] = "";
            $insert['PhoneNo'] = '';
            $insert['AlternateCCode'] = '';
            $insert['AlternatePhoneNo'] = '';
            $insert['RequesterStatus'] = 'active';
            $insert['BusinessName'] = '';
            if (!empty(trim(Input::get('business_name')))) {
                $insert['BusinessName'] = ucfirst(Input::get('business_name'));
            }
            $insert['Age'] = "";
            $insert['SSN'] = "";
            $insert['Street1'] = '';
            $insert['Street2'] = '';
            $insert['Country'] = '';
            $insert['delete_status'] = 'no';
            $insert['State'] = '';
            $insert['City'] = '';
            $insert['ZipCode'] = '';
            $insert['BankName'] = '';
            $insert['AccountHolderName'] = '';
            $insert['BankAccountNo'] = '';
            $insert['RoutingNo'] = '';
            $insert['StripeId'] = '';
            $insert['VatTaxNo'] = '';
            $insert['TransporterStatus'] = "not_registred";
            $insert['UserStatus'] = "active";
            $insert['Image'] = "";
            $insert['IDProof'] = '';
            $insert['LicenceId'] = '';
            $insert['type_of_id'] = '';
            $insert['TPTrackLocation'] = "on";
            $insert['EmailStatus'] = "on";
            $insert['consolidate_item'] = "on";
            $insert['NoficationStatus'] = "on";
            $insert['TPSetting'] = "on";
            $insert['SoundStatus'] = "on";
            $insert['VibrationStatus'] = "on";
            $insert['EnterOn'] = new MongoDate();
            $insert['UpdateOn'] = new MongoDate();
            $insert['RatingCount'] = 0;
            $insert['RatingByCount'] = 0;
            $insert['CurrentLocation'] = array();
            $insert['DeliveryAreaCountry'] = array();
            $insert['DeliveryAreaState'] = array();
            $insert['DeliveryAreaCities'] = array();
            $insert['ProfileStatus'] = 'step-one';
            $insert['StripeBankId'] = "";
            $insert['NotificationId'] = '';
            $insert['DeviceId'] = '';
            $insert['DeviceType'] = 'web';
            $insert['UniqueNo'] = strtoupper($UniqueNo) . Utility::sequence('user_unique');
            $insert['bank_info'] = [];
            $insert['AqAddress'] = '';
            $insert['AqCity'] = '';
            $insert['AqState'] = '';
            $insert['AqCountry'] = '';
            $insert['AqZipcode'] = '';
            $insert['AqLatLong'] = [];
            $insert['Aqphone'] = "";
            $insert['Aqcc'] = "";
            $insert['Default_Currency'] = 'GHS';

            if (Input::get('become_transporter') == 'yes') {
                $insert['UserType'] = 'both';
                $insert["TransporterStatus"] = "not_verify";
                $insert["TransporterType"] = "individual";
            }
            if (in_array(Input::get('transportertype'), ['individual', 'business'])) {
                $insert['UserType'] = 'both';
                $insert["TransporterType"] = Input::get('transportertype');
                $insert["TransporterStatus"] = "not_verify";
            }
            // Get aquantuo addres
            $supportemail = Setting::find('563b0e31e4b03271a097e1ca');
            //    echo "<pre>";  print_r($supportemail);  die;
            if (count($supportemail) > 0) {
                $insert['AqAddress'] = $supportemail->AqAddress;
                $insert['AqCity'] = $supportemail->AqCity;
                $insert['AqState'] = $supportemail->AqState;
                $insert['AqZipcode'] = $supportemail->AqZipcode;
                $insert['AqCountry'] = $supportemail->AqCountry;
                $insert['AqLatLong'] = $supportemail->AqLatlong;
                $insert['Aqphone'] = $supportemail->Aqphone;
                $insert['Aqcc'] = $supportemail->Aqcc;
            }

            $insert['_id'] = (string) User::insertGetId($insert);

            if ($insert['_id']) {

                /*(new OpenFireRestApi())->addUser($insert['_id'], (String) $insert['_id'], $insert['Name'], $insert['Email'], array('Group 1'));*/

                // email to user
                send_mail('55d5a0be6734c4fb378b4567', [
                    'to' => trim(Input::get('email')),
                    'replace' => [
                        '[USERNAME]' => $insert['Name'],
                        '[ADDRESS]' => $insert['AqAddress'].', Unit # '.$insert['UniqueNo'].', '.$insert['AqCity'].' '.$insert['AqState'].' '.$insert['AqZipcode'].', '.$insert['AqCountry'],
                    ],
                ]);

                // email to admin

                if (count($supportemail) > 0) {
                    send_mail('565fd3e3e4b076e7a2a176f5', [
                        'to' => $supportemail->RegEmail,
                        'replace' => [
                            '[USERNAME]' => $insert['Name'],
                            '[EMAIL]' => $insert['Email'],
                            '[USERTYPE]' => (($insert['UserType'] == 'both') ? "Transporter({Input::get('transportertype')})" : 'Requester'),
                        ],
                    ]);
                }

                Notification::insert([
                    "NotificationTitle" => "New User Registered.",
                    "NotificationMessage" => sprintf('%s has registered.', ucfirst($insert['Name'])),
                    "NotificationUserId" => [],
                    "NotificationReadStatus" => 0,
                    "Date" => new MongoDate(),
                    "GroupTo" => "Admin",
                ]);

                LoginController::set_user_session($insert);

                $response = array("success" => 1, "msg" => "Registered successfully.");
            }

            if ($insert['UserType'] == 'requester') {
                if (input::get('become_transporter') == 'yes') {
                    return Redirect::to('become-transporter');
                } else {
                    return Redirect::to('edit-profile')
                        ->withSuccess('Success! You have registered successfully');
                }

            } else {
                return Redirect::to('become-transporter')
                    ->withSuccess('Success! You have registered successfully');
            }
        }
    }

    public function forgot_password()
    {
        if (session()->has('UserId')) {
            return Redirect::to('dashboard')->send();
        }
        return view('Page::auth.forgot_password');
    }

    public function makeForgotPassword(Request $request)
    {
        if (session()->has('UserId')) {
            return Redirect::to('dashboard')->send();
        }
        $validation = Validator::make($request->all(), [
            'email' => 'required|email',
        ]);
        if ($validation->fails()) {
            return Redirect::to("forgot-password")->withErrors($validation)->withInput();
        } else {
            $where = array('Email' => strtolower(Input::get('email')));
            $getInfo = User::where($where)->get(array('_id', 'email', 'Name'));

            if (count($getInfo) > 0) {
                $pin = md5(mt_rand(10000, 99999));

                $getInfo[0]->_id = (String) $getInfo[0]->_id;

                $link = url("page/reset_password/" . $getInfo[0]->_id . "/$pin");
                $emailtemplate = array(
                    'to' => Input::get('email'),
                    'replace' => array(
                        '[USERNAME]' => $getInfo[0]->Name,
                        '[LINK]' => "<a href='$link' >$link</a>",
                    ),
                );

                send_mail('55d5a0be6734c4fb378b4568', $emailtemplate);
                User::where($where)->update(array('Token' => $pin, 'TokenCreateAt' => new MongoDate()));

                return Redirect::to('login')->withSuccess('Please check your email and follow the instructions to reset your password.');

            } else {
                return Redirect::to('forgot-password')->with('danger', 'You are not registered with us.');
            }
        }
    }

    public function success()
    {
        return view("Page::other/password_success");
    }

    public function account_security_instructions()
    {
        $data['info'] = Webcontent::find('56682927133078a051c8e501');
        return view('page::other.account-security-instruction', $data);
    }

    public function promo()
    {
        $data['client'] = Client::where('Status', '=', 'active')->get();
        $data['images'] = SilderImages::where('Status', '=', 'active')->get();
        return view("Page::promo",$data);
    }

    public function terms_and_conditions()
    {
        $data['content'] = Webcontent::find('5667cb81780ed01e85586c05');
        return view('Page::terms-and-conditions', $data);
    }

    public function privacy_policy()
    {

        $data['content'] = Webcontent::find('5667cb74780ed01e85586c02');

        return view('Page::privacy-policy', $data);
    }
    public function contact_us____()
    {
        $respose = array('success' => 0, 'msg' => "Oops! We are unable to send your email. Please try again.");
        if (Input::get('email') != "") {
            //========mail to user=====//
            $array = array(
                'to' => Input::get('email'),
                'replace' => array(
                    '[NAME]' => ucfirst(Input::get('fullname')),
                    '[EMAIL]' => ucfirst(Input::get('email')),
                    '[SUBJECT]' => ucfirst(Input::get('subject')),
                    '[MESSAGE]' => ucfirst(Input::get('message')),
                ),
            );
            send_mail('56bc34dd5509251cd67773f6', $array);
            // Mail to admin
            $array = array(
                'to' => 'admin@aquantuo.com',
                'replace' => array(
                    '[NAME]' => ucfirst(Input::get('fullname')),
                    '[EMAIL]' => ucfirst(Input::get('email')),
                    '[SUBJECT]' => ucfirst(Input::get('subject')),
                    '[MESSAGE]' => ucfirst(Input::get('message')),
                ),
            );

            $supportemail = Setting::find('563b0e31e4b03271a097e1ca');
            if (count($supportemail) > 0) {
                $array['to'] = $supportemail['SupportEmail'];
            }
            send_mail('56bc35545509251cd67773f7', $array);

            //=========mail setting====//*/
            $respose = array('success' => 1, 'msg' => "Thank you for contacting Aquantuo, we will get in touch with you soon!!");
        }

        echo json_encode($respose);
    }

    public function contact_us()
    {
       
            
        $respose = array('success' => 0, 'msg' => "Oops! We are unable to send your email. Please try again.");
        if (Input::get('email') != "") {
            //========mail to user=====//
            $array = array(
                'to' => Input::get('email'),
                'replace' => array(
                    '[NAME]' => ucfirst(Input::get('fullname')),
                    '[EMAIL]' => ucfirst(Input::get('email')),
                    '[SUBJECT]' => ucfirst(Input::get('subject')),
                    '[MESSAGE]' => ucfirst(Input::get('message')),
                ),
            );
            send_mail('56bc34dd5509251cd67773f6', $array);
            // Mail to admin
            $ph='';
            if(!Input::get('phone_number') == '' && !Input::get('country_code') == ''){
                $ph = Input::get('country_code')."-".Input::get('phone_number');
            }

            $replace_array =[
                '[NAME]' => ucfirst(Input::get('fullname')),
                '[EMAIL]' => ucfirst(Input::get('email')),
                '[SUBJECT]' => ucfirst(Input::get('subject')),
                '[MESSAGE]' => ucfirst(Input::get('message')),
                '[PHONENO]'=>$ph
            ];

            

            $array = array(
                'to' => 'admin@aquantuo.com',
                'replace' => $replace_array,
            );

            $supportemail = Setting::find('563b0e31e4b03271a097e1ca');
            if (count($supportemail) > 0) {
                $array['to'] = $supportemail['SupportEmail'];
            }
            send_mail('56bc35545509251cd67773f7', $array);

            //=========mail setting====//*/
            $respose = array('success' => 1, 'msg' => "Thank you for contacting Aquantuo, we will get in touch with you soon!!");
        }

        echo json_encode($respose);
    }

    public function subscribe()
    {

        $response = array("success" => 0);
        if (Input::get('subscribe_email') != "") {

            $data = array(
                "Name" => Input::get('subscribe_name'),
                "Email" => Input::get('subscribe_email'),
                "Country" => Input::get('subscribe_country'),
                "State" => Input::get('subscribe_state'),
                "City" => Input::get('subscribe_city'),
                "Message" => Input::get('subscribe_message'),
            );

            $supportemail = Setting::find('563b0e31e4b03271a097e1ca');
            if (count($supportemail) > 0) {

                //Email to Admin
                $array = array(
                    'to' => $supportemail['SupportEmail'],
                    'replace' => array(
                        '[NAME]' => ucfirst(Input::get('subscribe_name')),
                        '[EMAIL]' => ucfirst(Input::get('subscribe_email')),
                        '[COUNTRY]' => ucfirst(Input::get('subscribe_country')),
                        '[STATE]' => ucfirst(Input::get('subscribe_state')),
                        '[CITY]' => ucfirst(Input::get('subscribe_city')),
                        '[MESSAGE]' => ucfirst(Input::get('subscribe_message')),
                    ),
                );

                send_mail('56bdd7d45509251cd67773f9', $array);
                //End Email to Admin
            }

            //Email to User
            $array = array(
                'to' => strtolower(Input::get('subscribe_email')),
                'replace' => array(
                    '[NAME]' => ucfirst(Input::get('subscribe_name')),
                    '[EMAIL]' => ucfirst(Input::get('subscribe_email')),
                    '[COUNTRY]' => ucfirst(Input::get('subscribe_country')),
                    '[STATE]' => ucfirst(Input::get('subscribe_state')),
                    '[CITY]' => ucfirst(Input::get('subscribe_city')),
                    '[MESSAGE]' => ucfirst(Input::get('subscribe_message')),
                ),
            );
            send_mail('56bdd7605509251cd67773f8', $array);
            //End Email to Users
            Subscription::insert($data);
            $response = array("success" => 1);
        }
        echo json_encode($response);
    }

    public function country()
    {
        $data['info'] = City_State_Country::where(array('type' => 'Country', 'Status' => 'Active'))->get(array('Content'));
        foreach ($data['info'] as $key) {
            $dropbox[] = "<option value=''>Select Country</option>";
            $dropbox[] = "<option value='$key->Content'>$key->Content</option>";
        }
        $response['html'] = implode(' ', $dropbox);
        echo json_encode($response);
    }

    public function place_an_order()
    {
        $respose = array('success' => 0, 'msg' => "Oops! We are unable to send your email. Please try again.");

        if (Input::get('email_id') != "") {
            //========mail to user=====//
            $array = array(
                'to' => Input::get('email_id'),
                'replace' => array(
                    '[ITEMURL]' => ucfirst(Input::get('item_url')),
                    '[QUANTITY]' => ucfirst(Input::get('quantity')),
                    '[EMAIL]' => ucfirst(Input::get('email_id')),
                    '[PHONENO]' => ucfirst(Input::get('phone_no')),
                    '[NAME]' => ucfirst(Input::get('name')),
                    '[COMMENTS]' => ucfirst(Input::get('comments')),
                ),
            );
            send_mail('579b2022e4b0d2166bc35a94', $array);
            // Mail to admin
            $array = array(
                'to' => 'order@aquantuo.com',
                'replace' => array(
                    '[ITEMURL]' => ucfirst(Input::get('item_url')),
                    '[QUANTITY]' => ucfirst(Input::get('quantity')),
                    '[EMAIL]' => ucfirst(Input::get('email_id')),
                    '[PHONENO]' => ucfirst(Input::get('phone_no')),
                    '[NAME]' => ucfirst(Input::get('name')),
                    '[COMMENTS]' => ucfirst(Input::get('comments')),
                ),
            );

            send_mail('579b2305e4b0d2166bc35a96', $array);
            //=========mail setting====//*/
            $respose = array('success' => 1, 'msg' => "Thank you for placing an order, Aquantuo team will get in touch with you soon!!");
        }
        echo json_encode($respose);
    }

    public function reset_password($id, $token)
    {
        $tokenCreateAt = new MongoDate(strtotime("-6 hours"));

        $Auth = User::where(array('_id' => $id, 'Token' => $token, 'TokenCreateAt' => array('$gt' => $tokenCreateAt)))->get();
        if (count($Auth) > 0) {
            return view("Page::other/reset_password");
        } else {
            return view("Page::other/reset_password_expire");
        }
    }
    public function post_reset_password($id, $token, Request $request)
    {
        $v = Validator::make($request->all(), array('new_password' => 'required', 'confirm_password' => 'required|same:new_password'));
        if ($v->fails()) {
            return Redirect::to("page/reset_password/$id/$token")->withErrors($v)->withInput();
        } else {
            $save = User::find($id);
            if (count($save) > 0) {
                if ($save->Password != md5(Input::get('new_password'))) {
                    $oldPasswordinfo = Userextrainfo::where(array('UserId' => $id, 'OldPassword' => md5(Input::get('new_password'))))->get();

                    if (count($oldPasswordinfo) > 0) {
                        return Redirect::to("page/reset_password/$id/$token")->with('fail', ' You have used this password before.');
                    } else {
                        $oldpassword = $save->Password;
                        $token = md5(rand(10000, 99999999));
                        $save->Password = md5(trim(Input::get('new_password')));
                        $save->Token = $token;
                        $save->save();

                        //========mail setting=====//
                        $link = url("page/reset_password/") . '/' . ((string) $save->_id) . "/$token";
                        $array = array(
                            'to' => $save->Email,
                            'replace' => array(
                                '[USERNAME]' => $save->Name,
                                '[EMAIL]' => $save->Email,
                                '[RESETLINK]' => "<a href='$link'  target='_blank' > Here</a>",
                                '[SECURE_INFO_LINK]' => "<a href='http://support.aquantuo.com/help-support/36' target='_blank' >secured</a>",
                            ),
                        );
                        send_mail('566fff48e4b050325ee2fcce', $array);

                        //=========mail setting====//
                        // Store old password
                        Userextrainfo::insert(array(
                            'UserId' => $id,
                            'OldPassword' => $oldpassword,
                        ));

                        return Redirect::to("page/success");
                    }
                } else {
                    return Redirect::to("page/reset_password/$id/$token")->with('fail', 'Your current password and new password should not be same.');
                }
            } else {
                return Redirect::to("page/reset_password/$id/$token");
            }
        }
    }
    public function become_a_transporter()
    {
        return view('page::auth.become-a-transporter');
    }
    public function near_transporter()
    {
        return view('page::list.near-transporter');
    }

    public function trip_list()
    {
        return view('page::list.trip-list');
    }

    public function get_send_mail()
    {
        
        return view('Page::send_mail');

    }


    public function send_mail()
    {


        send_mail('5a0fc5a9360d5c0ce7138b2b', [
            'to' => trim(Input::get('email')),
            'replace' => [
                '[USERNAME]' => "",
                '[ADDRESS]' => "",
            ],
        ]);

        return Redirect::back()->withSuccess('Success! You have registered successfully');;

    }

    public function email_through_cron()
    {

        
        $mail =SendMail::where(['status'=>'ready'])->limit(1)->orderBy('_id', 'desc')->get();
        $response=['success'=>0];
        //$Notification = new Pushnotification();
        $Notification = new Pushnotification();
        $new_mail = new NewEmail();
        if($mail){
        //   echo '<pre>';
        //   print_r($mail); die;
            foreach ($mail as $key) {

                if($key->by_mean == 'notification'){
                    $Notification->setValue('title',@$key->title);
                    $Notification->setValue('message',@$key->message);
                    $Notification->setValue('type', @$key->type);
                    $Notification->setValue('location', @$key->location);

                    $Notification->setValue('locationkey',@$key->locationkey);
                    $Notification->setValue('itemId',@$key->itemId);
                    $Notification->add_user($key->NotificationId, $key->DeviceType);
                    $Notification->fire();
                }else if($key->by_mean == 'email_2'){
                    $new_mail->send_mail($key->email_id, [
                        "to" => $key->email,
                        "replace" => [
                            "[USERNAME]" => @$key->USERNAME,
                            "[PACKAGETITLE]" => @$key->PACKAGETITLE,
                            "[PACKAGEID]" => @$key->PACKAGEID,
                            "[SOURCE]" => @$key->SOURCE,
                            "[DESTINATION]" => @$key->DESTINATION,
                            "[DROPOFFADDRESS]" => @$key->DROPOFFADDRESS,
                            "[PICKUPOFFADDRESS]" => @$key->PICKUPOFFADDRESS,
                            "[PACKAGENUMBER]" => @$key->PACKAGENUMBER,
                            "[SHIPPINGCOST]" => @$key->SHIPPINGCOST,
                            "[TOTALCOST]" => @$key->TOTALCOST,
                            "[CC]" => @$key->CC,
                            "[MNO]" => @$key->MNO,
                            "[PRFEE]" => @$key->PRFEE,
                        ],
                    ]
                    );
                    SendMail::where(['_id'=>$key->_id])->update(['status'=>'done']);

                }else{

                    $new_mail->send_mail($key->email_id, [
                        "to" => $key->email,
                        "replace" => [
                            "[USERNAME]" => @$key->USERNAME,
                            "[PACKAGETITLE]" => @$key->PACKAGETITLE,
                            "[PACKAGEID]" => @$key->PACKAGEID,
                            "[SOURCE]" => @$key->SOURCE,
                            "[DESTINATION]" => @$key->DESTINATION,
                            "[DROPOFFADDRESS]" => @$key->DROPOFFADDRESS,
                            "[PICKUPOFFADDRESS]" => @$key->PICKUPOFFADDRESS,
                            "[PACKAGENUMBER]" => @$key->PACKAGENUMBER,
                            "[SHIPPINGCOST]" => @$key->SHIPPINGCOST,
                            "[TOTALCOST]" => @$key->TOTALCOST,
                            "[TRANSPORTER]" => @$key->TRANSPORTER,
                            '[PRICE]' => @$key->PRICE,
                            '[ITEM_PRICE]'=> @$key->ITEM_PRICE,
                            '[SHIPPINGCOST2]' => @$key->SHIPPINGCOST2, 
                            '[INSURANCE]' => @$key->INSURANCE,
                            '[REQUESTER]'=> @$key->REQUESTER,
                            '[URL]' => @$key->URL,
                            '[TITLE]'=> @$key->TITLE,
                            '[DATE]'=> @$key->DATE,
                            //'[EMAIL]'=> @$key->EMAIL,
                            //'[USERTYPE]'=> @$key->USERTYPE,
                            //'[DEVICETYPE]'=> @$key->DEVICETYPE,
                        ],
                    ]
                    );
                   
                }

                SendMail::where(['_id'=>$key->_id])->update(['status'=>'done']);
                
            }
            $response=['success'=>1];
        }

        echo json_encode($response);
    }

    public function alertClose() {
        Session()->put(array('alert_status' => 'off'));
        $response['status'] = 1;
        return response()->json($response);
    }

}
