<?php namespace App\Modules\Page\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Models\Category;
use App\Http\Models\CityStateCountry;
use App\Http\Models\Configuration;
use App\Library\Requesthelper;
use App\Library\NewRequesthelper;
use App\Library\Utility;
use App\Library\upsRate;
use Input;
use Redirect;
use Session;
use App\Http\Models\Item;
use App\Http\Models\Extraregion;
use App\Modules\User\Controllers\Calculation;
use App\Modules\User\Controllers\NewCalculation;
use App\Http\Models\Currency;
use App\Http\Models\Activitylog;
use App\Http\Models\Deliveryrequest;
use App\Http\Models\Distance;

class AjaxController extends Controller {
    public function state_list() {

        $data['oldtext'] = strtolower(Input::get('oldtext'));

        $country = (Object) json_decode(Input::get('data'));
        $data['state'] = CityStateCountry::where(['type' => 'State', 'Status' => 'Active'])->orderby('Content', 'asc')
            ->where(['CountryId' => @$country->id])->get(['_id', 'Content']);

        return view('Page::utility.state', $data);
    }

    public function localDeliveryPriceEstimator() {

        $response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
        $category = json_decode(Input::get('category'));
        $distance = 0;
        $weight = Input::get('weight');
        $weight_unit = Input::get('weight_unit');

        $city = json_decode(Input::get('city'));
        $state = json_decode(Input::get('state'));
        $country = json_decode(Input::get('country'));
        $city2 = json_decode(Input::get('city2'));
        $state2 = json_decode(Input::get('state2'));
        $country2 = json_decode(Input::get('country2'));

        //print_r($state2); die;
        $after_decimal = strlen(substr(strrchr($weight, "."), 1));
        if($after_decimal > 3){
            $response['msg'] = 'Invalid weight.';
            return json_encode($response);die;
        }

        $dest_add = $this->latlongapi2(Input::get('dropoff_address') . ' ' . $city->name . ' ,' . $state->name . ' ,' . $country->name);

        if ($dest_add != false) {
            $souce_add = $this->latlongapi2(Input::get('pickup_address') . ' ' . $city2->name . ' ,' . $state2->name . ' ,' . $country2->name);
            if ($souce_add != false) {
                $distance = Utility::getDistanceBetweenPointsNew($souce_add['lat'], $souce_add['lng'], $dest_add['lat'], $dest_add['lng']);
                if ($distance == false) {
                    $response['msg'] = 'Oops! We are unable to calculate distance.';
                    return json_encode($response);
                    die;
                }
            } else {
                $response['msg'] = 'Oops! We are unable to find your source address, please correct.';
                return json_encode($response);
                die;
            }
        } else {
            $response['msg'] = 'Oops! We are unable to find your destination address, please correct.';
            return json_encode($response);
            die;
        }

        //Category calculation
        if ($category->id) {
            $match_category = Category::where(['_id' => $category->id, 'Status' => 'Active', 'type' => 'localCategory'])
                ->select('price')
                ->first();

            if ($match_category) {
                $response['success'] = 1;
                $response['category_price'] = $match_category->price;
            } else {
                $response = ['success' => 0, 'msg' => 'The shipping category you have specified is unsupported.'];
            }
        }
        //Kumasi-Sunyani Rd .. Brong ahafo..sunyani
        //Kumasi City Mall..ashanti..kumasi

        //Item value calculation
        if (Input::get('pe_product_cost') != '' && $response['success'] == 1) {

            $match_value = Distance::Where('from', '<=', (float) trim(Input::get('pe_product_cost')))->where('to', '>=', (float) trim(Input::get('pe_product_cost')))->where(['Status' => 'Active', 'country_id' => $country->id, "type" => "item-value"])
                ->select('price')
                ->first();

            if ($match_value) {
                $response['success'] = 1;
                $response['item_price'] = $match_value->price;
            } else {
                $response = ['success' => 0, 'msg' => 'The item value you have specified is unsupported.'];
            }
        }

        //Distance calculation
        if ($distance >  0 && $distance != '' && $response['success'] == 1) {
            //Distance in to miles

            $distance = $distance * 0.000621371;
            $match_distance = Distance::Where('from', '<=', (float) $distance)->where('to', '>=', (float) $distance)
                ->where(['Status' => 'Active', 'country_id' => $country->id, "type" => "distance"])
                ->select('price')
                ->first();

            if ($match_distance) {
                $response['success'] = 1;
                $response['distance_price'] = $match_distance->price;
            } else {
                $response = ['success' => 0, 'msg' => 'The shipping address/distance you have specified is unsupported.'];

            }
        }

        if ($response['success'] == 1) {
            //Resion Charges
            $response['resionCharges'] = $this->resionCharges(['name' => $state->name, 'id' => $state->id]);
            $response['total_amount'] = $response['distance_price'] + $response['category_price'] + $response['item_price'] + $response['resionCharges'];

            //print_r($response['resionCharges']); die;
            $currency_conversion = $this->currency_conversion($response['total_amount']);
            $response['msg'] = "Calculation";
            $response['total_weight'] = $this->get_weight(Input::get('weight'), $weight_unit);
            $response['ghana_total_amount'] = $currency_conversion['ghanian_cost'];
            $response['canadian_cost'] = $currency_conversion['canadian_cost'];
            $response['philipins_cost'] = $currency_conversion['philipins_cost'];
            $response['uk_cost'] = $currency_conversion['uk_cost'];
        }
        return json_encode($response);
    }

    public function state_list2() {
        $data['oldtext'] = strtolower(Input::get('oldtext'));
        $country = (Object) json_decode(Input::get('data'));
        $data['country'] = $country->name;
        $data['append_id'] = Input::get('append_id');

        $query = CityStateCountry::query();
        $query->where(['type' => 'State', 'Status' => 'Active']);
        if(Input::get('search') != ''){
            $query->where('Content','like',"%".Input::get('search')."%");
        }

        $query->where(['CountryId' => @$country->id])->orderby('Content', 'asc');
        $data['state'] =  $query->get(['_id', 'Content', 'short_name']);


        //$data['state'] = CityStateCountry::where(['type' => 'State', 'Status' => 'Active'])->orderby('Content', 'asc')
            //->where(['CountryId' => @$country->id])->get(['_id', 'Content']);
        return view('Page::utility.search_state', $data);
    }
    public function city_list() {
        $data['oldtext'] = strtolower(Input::get('oldtext'));
        $state = (Object) json_decode(Input::get('data'));
        $data['state'] = CityStateCountry::where(['type' => 'city', 'Status' => 'Active'])->orderby('Content', 'asc')
            ->where(['StateId' => @$state->id])->get(['_id', 'Content']);
        return view('Page::utility.city', $data);
    }

    public function prepare_request_calculation() {
        return view('Page::request.prepare-request-calculation');
    }

    public function post_price_estimator() {
        $response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];

        //lhwunit == length/height/width unit
        $lhwunit = 'cm';
        $weightunit = 'kg';
        if (Input::get('measurement_unit') == 'inches_lbs') {
            $lhwunit = 'inches';
            $weightunit = 'lbs';
        }

        $distance = 0;
        $dest_add = Utility::getLatLong(Input::get('pe_destination_address'));
        if ($dest_add != false) {
            $souce_add = Utility::getLatLong(Input::get('pe_source_address'));

            if ($souce_add != false) {
                $distance = Utility::getDistanceBetweenPointsNew($souce_add['lat'], $souce_add['lng'], $dest_add['lat'], $dest_add['lng']);
                if ($distance == false) {
                    $response['msg'] = 'Oops! We are unable to calculate distance.';
                    return json_encode($response);
                    die;
                }
            } else {
                $response['msg'] = 'Oops! We are unable to find your source address, please correct.';
                return json_encode($response);
                die;
            }
        } else {
            $response['msg'] = 'Oops! We are unable to find your destination address, please correct.';
            return json_encode($response);
            die;
        }

        $category = json_decode(Input::get('category'));

        $requesthelper = new NewRequesthelper(
            [
                "needInsurance" => (Input::get('pe_insurance') == 'yes') ? true : false,
                "productQty" => (Input::get('pe_product_qty') < 1) ? 1 : Input::get('pe_product_qty'),
                "productCost" => Input::get('pe_product_cost'),
                "productWidth" => Input::get('width'),
                "productWidthUnit" => $lhwunit,
                "productHeight" => Input::get('height'),
                "productHeightUnit" => $lhwunit,
                "productLength" => Input::get('length'),
                "productLengthUnit" => $lhwunit,
                "productWeight" => Input::get('weight'),
                "productWeightUnit" => $weightunit,
                "productCategory" => @$category->name,
                "productCategoryId" => @$category->id,
                "distance" => $distance,
                "travelMode" => Input::get('travel_mode'),
                "currency" => trim(Session::get('Default_Currency')),
            ]
        );

        $requesthelper->calculate();
        $response['calculationinfo'] = $requesthelper->get_information();

        $response['total_amount'] = 0;
        $response['ghana_total_amount'] = 0;

        if (!isset($response['calculationinfo']->error)) {

            $response['success'] = 1;
            $response['total_amount'] = $response['calculationinfo']->shippingcost + $response['calculationinfo']->insurance;
            $response['ghana_total_amount'] = $response['calculationinfo']->formated_currency;
        } else {
            $response['msg'] = $response['calculationinfo']->error;
        }

        return json_encode($response);
    }

    public function price_estimator__() {
        $data['category'] = Category::where(['Status' => 'Active'])->get();
        return view('Page::other.price_estimator', $data);
    }


    public function set_location() {
        Session::set('latitude', Input::get('latitude'));
        Session::set('longitude', Input::get('longitude'));
        return Redirect::to(Input::get('href'));
    }


    public function price_estimator() {
        $data['category'] = Category::where(['Status' => 'Active', 'type' => 'Category'])->get();
        $data['category_local'] = Category::where(['Status' => 'Active', 'type' => 'localCategory'])->get();
        $data['country'] = CityStateCountry::where(['type' => 'Country', 'Status' => 'Active'])
            ->orderBy('Content', 'Asc')
            ->get(['_id', 'Content', 'state_available']);

        $data['item'] = Item::Where(['Status' => 'Active'])->limit(5)->get();
        $data['items'] = Item::Where(['Status' => 'Active'])->get();
        $data['item_count'] = Item::Where(['Status' => 'Active'])->count();
        return view('Page::other.priceEstimator', $data);
    }

    public function NewonlineAndBuyformePricEestimator() {
        $response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
        $data = ['shippingcost' => 0, 'insurance' => 0, 'ghana_total_amount' => 0, 'total_amount' => 0, 'error' => [], 'formated_text' => '', 'distance_in_mile' => 0, 'type' => 'online', 'item_cost' => 0, 'shipping_cost_by_user' => 0, 'product_count' => 0, 'ProcessingFees' => 0];
        $data['consolidate_check'] = trim(Input::get('consolidate_check'));
        $data['is_customs_duty_applied'] = false;
        $weight = Input::get('weight'); // Input::get('weight_unit') === 'lbs' ? number_format(((Input::get('weight') * 0.453592)), 2, '.', '') : Input::get('weight');
       
        $weight_unit = Input::get('weight_unit');
        $data['weight_unit'] = $weight_unit;
        $data['weight'] = $weight;
        $category = json_decode(Input::get('category'));
        $after_decimal = strlen(substr(strrchr($weight, "."), 1));
        if($after_decimal > 3){
            $response['msg'] = 'Invalid weight.';
            return json_encode($response);die;
        }
        $city = null;
        if (Input::get('city') !== '') {
            $city = json_decode(Input::get('city'));
        }
        if (Input::get('request_type') == 'buyforme') {
            $state = json_decode(Input::get('state1'));
        }else{
            $state = json_decode(Input::get('state10'));
        }
        $country = json_decode(Input::get('country'));
        $destination_address = $this->latlongapi2($city !== null ? $city->name : '' . ',' . $state->name . ',' . $country->name);
        if ($destination_address != false) {
            $aqlat = 39.2625832;
            $aqlong = -75.60483959999999;
            //$data['distance'] = Utility::GetDrivingDistance($aqlat, $aqlong, $destination_address['lat'], $destination_address['lng']);
            $data['distance'] = Utility::getDistanceBetweenPointsNew($aqlat, $aqlong, $destination_address['lat'], $destination_address['lng']);
            $data['distance'] = (Object) ['distance' => $data['distance']];
            if ($data['distance'] == false) {
                $response['msg'] = 'Oops! We are unable to calculate distance.';
                return json_encode($response);die;
            }
        }
        if (Input::get('request_type') == 'buyforme') {
            $data['item_cost'] = Input::get('pe_product_cost');
            $data['type'] = 'buyforme';
        }
        if (!empty($category)) {
            if ($category->id == '') {
                $response['msg'] = 'The category you have selected is not found.';
                return json_encode($response);die;
            }
        }
        if($country->name!="USA" && $country->name!="Kenya"){
			$data['is_customs_duty_applied'] = true;
		}
        
        $product[] = [
            'request_type'      => Input::get('request_type'),
           // 'insurance_status'  => "no",
            'insurance_status'  => "yes",
            'travelMode'        => Input::get('travelMode'),
            'qty'               => 1,
            'categoryid'        => @$category->id,
            'category'          => @$category->name,
            "weight_unit"       => $weight_unit,
            "length"            => 1,
            "width"             => $weight,
            "height"            => 1,
            "weight"            => ($weight == '') ? 0 : $weight,
            'shipping_cost_by_user' => 0,
            'price'             => floatval(Input::get('pe_product_cost')),
            'shippingCost'     =>0.0,
            'insurance'    =>0.0,
        ];
       // echo Input::get('request_type');
        
        $userinfo = ['Default_Currency' => 'USD'];
        $calculate = new NewCalculation();
        $information = $calculate->GetRatePE($product, $data, $userinfo, $country->name, $state);
        
        if ($information['success'] == 1) {


            $information['total_amount'] = $information['total_amount'];
            $response['res'] = $information['res'];
            $information['shipping_cost'] = $information['shippingCost'];
            //Resion Charges
            $response['resionCharges'] = $this->resionCharges(['name' => $state->name, 'id' => $state->id]);
            $information['total_amount'] = $information['total_amount'] + $response['resionCharges'];
            $response['ProcessingFees'] = $information['ProcessingFees'] + $response['resionCharges'];
            //end

           

            $currency_conversion = $this->currency_conversion($information['total_amount']);
            $response['success'] = 1;
            $response['msg'] = "Calculation";
            if (Input::get('request_type') == 'buyforme') {
                $information['total_amount'] = $information['total_amount'];
            }
            $response['total_amount'] = $information['total_amount'];
            $response['item_cost'] = $data['item_cost'];
            $response['fess'] = $response['total_amount'] - $data['item_cost'];

            $response['ghana_total_amount'] = $currency_conversion['ghanian_cost'];
            $response['canadian_cost'] = $currency_conversion['canadian_cost'];
            $response['philipins_cost'] = $currency_conversion['philipins_cost'];
            $response['uk_cost'] = $currency_conversion['uk_cost'];
            $response['kenya_cost'] = $currency_conversion['kenya_cost'];
            

            $response['total_weight'] = $information['total_weight'];
            $response['custom_duty'] = $information['DutyAndCustom'];
            $response['insurance'] = $information['insurance'];
            $response['shipping_cost'] = $information['shippingCost'];
            
            if (Input::get('travelMode') == 'ship') {
                $response['total_weight'] = $this->get_weight($weight, $weight_unit);
            }
            
            // $response['total_amount_txt'] = $information['total_amount_txt'];
            $response['total_distance'] = ($data['distance']->distance > 0) ? $data['distance']->distance / 1000 : $data['distance']->distance;
        } else {
            $response['msg'] = $information['msg'];
        }
        $response['formdata'] = $data;
        
        return json_encode($response);

    }

    public function onlineAndBuyformePricEestimator() {
    
        $response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
        $data = ['shippingcost' => 0, 'insurance' => 0, 'ghana_total_amount' => 0,
            'total_amount' => 0, 'error' => [], 'formated_text' => '', 'distance_in_mile' => 0, 'type' => 'online', 'item_cost' => 0, 'shipping_cost_by_user' => 0, 'product_count' => 0, 'ProcessingFees' => 0];
        $data['consolidate_check'] = trim(Input::get('consolidate_check'));

        $weight = Input::get('weight');
        $weight_unit = Input::get('weight_unit');
        $category = json_decode(Input::get('category'));

        $after_decimal = strlen(substr(strrchr($weight, "."), 1));
        if($after_decimal > 3){
            $response['msg'] = 'Invalid weight.';
            return json_encode($response);die;
        }


        $city = json_decode(Input::get('city'));
        if (Input::get('request_type') == 'buyforme') {
            $state = json_decode(Input::get('state1'));
        }else{
            $state = json_decode(Input::get('state10'));
        }
        
        $country = json_decode(Input::get('country'));

        $destination_address = $this->latlongapi2($city->name . ',' . $state->name . ',' . $country->name);

        if ($destination_address != false) {
            $aqlat = 39.2625832;
            $aqlong = -75.60483959999999;
            //$data['distance'] = Utility::GetDrivingDistance($aqlat, $aqlong, $destination_address['lat'], $destination_address['lng']);
            $data['distance'] = Utility::getDistanceBetweenPointsNew($aqlat, $aqlong, $destination_address['lat'], $destination_address['lng']);
            $data['distance'] = (Object) ['distance' => $data['distance']];
            if ($data['distance'] == false) {
                $response['msg'] = 'Oops! We are unable to calculate distance.';
                return json_encode($response);die;
            }
        }

        if (Input::get('request_type') == 'buyforme') {
            $data['item_cost'] = Input::get('pe_product_cost');
            $data['type'] = 'buyforme';
        }
        if (!empty($category)) {
            if ($category->id == '') {
                $response['msg'] = 'The category you have selected is not found.';
                return json_encode($response);die;

            }
        }


        $product[] = [
            'request_type' => Input::get('request_type'),
            'insurance_status' => "no",
            'travelMode' => Input::get('travelMode'),
            'qty' => 1,
            'categoryid' => @$category->id,
            'category' => @$category->name,
            "weight_unit" => $weight_unit,
            "length" => 1,
            "width" => $weight,
            "height" => 1,
            "weight" => ($weight == '') ? 0 : $weight,
            'shipping_cost_by_user' => 0,
            'price' => (Input::get('request_type') == 'buyforme') ? Input::get('price') : 1,
        ];
        $userinfo = ['Default_Currency' => 'USD'];
        
        $calculate = new Calculation();
        $information = $calculate->getRate2($product, $data, $userinfo, $country->name);


        if ($information['success'] == 1) {

            $information['total_amount'] = $information['data']['total_amount'];
            $information['shipping_cost'] = $information['data']['shipping_cost'];
            //Resion Charges
            $response['resionCharges'] = $this->resionCharges(['name' => $state->name, 'id' => $state->id]);
            $information['total_amount'] = $information['total_amount'] + $response['resionCharges'];
            //end
            $currency_conversion = $this->currency_conversion($information['total_amount']);
            $response['success'] = 1;
            $response['msg'] = "Calculation";
            if (Input::get('request_type') == 'buyforme') {
                
                $information['total_amount'] = $information['total_amount'] + $information['data']['ProcessingFees'];
            }
            $response['total_amount'] = $information['total_amount'];
            $response['item_cost'] = $data['item_cost'];
            $response['fess'] = $response['total_amount'] - $data['item_cost'];

            $response['ghana_total_amount'] = $currency_conversion['ghanian_cost'];
            $response['canadian_cost'] = $currency_conversion['canadian_cost'];
            $response['philipins_cost'] = $currency_conversion['philipins_cost'];
            $response['uk_cost'] = $currency_conversion['uk_cost'];

            $response['total_weight'] = $information['total_weight'];
            if (Input::get('travelMode') == 'ship') {
                $response['total_weight'] = $this->get_weight(Input::get('weight'), $weight_unit);
            }

            $response['total_distance'] = ($data['distance']->distance > 0) ? $data['distance']->distance / 1000 : $data['distance']->distance;
        } else {
            $response['msg'] = $information['msg'];
        }
        return json_encode($response);
    }

    public function resionCharges($state) {
        //print_r($state['id']); die;
        $charges = Extraregion::where(['state_id' => $state['id'], 'status' => 'Active'])->select('amount')->first();
        if ($charges) {
            return $charges->amount;
        } else {
            return 0.00;
        }
    }

    public function latlongapi($address) {

        $flag = false;

        while ($flag == false) {
            $res = Utility::getLatLong($address);
            if ($res != false) {
                $flag = true;
            }
        }
        return $res;
    }

    public function latlongapi2($address) {

        $flag = false;
        $i = 0;
        while ($flag == false && $i < 19) {
            $i = $i + 1;
            $res = Utility::getLatLong($address);
            if ($res != false) {
                $flag = true;
                break;
            }
        }
        return $res;
    }

    public function kgToLb ($val) {
        return $val * 2.20462;
    }

    public function sendPackagePriceEstimator() {
        $response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
        $after_decimal = strlen(substr(strrchr(Input::get('weight'), "."), 1));

        if ($after_decimal > 3) {
            $response['msg'] = 'Invalid weight.';
            return json_encode($response);die;
        }

        $lhwunit = 'cm';
        $weightunit = 'kg';

        if (Input::get('measurement_unit') == 'inches_lbs') {
            $lhwunit = 'inches';
            $weightunit = 'lbs';
        }

        $category = json_decode(Input::get('category'));
        $distance = 0;
        $city = json_decode(Input::get('city'));    // destination.
        $state = json_decode(Input::get('state3'));
        $country = json_decode(Input::get('country'));  // destination.
        $city2 = json_decode(Input::get('city2'));  // Pickup.
        $state2 = json_decode(Input::get('state4'));
        $country2 = json_decode(Input::get('country2'));    // Pickup.
        $length = Input::get('length');
        $width = Input::get('width');
        $height = Input::get('height');
        $weight = Input::get('weight');
        $destination_postal_code = Input::get("destination_postal_code");
        $pickup_postal_code = Input::get("pickup_postal_code");
        $ups_rate = 0;
        $warehouse_transit_fee = 0;
        $is_ups_rate_applied = false;
        $is_customs_duty_applied = false;
		
        if ($country->name != $country2->name) {
			$is_customs_duty_applied = true;

            if (Input::get("measurement_unit") == "cm_kg") {
                $weight = $this->kgToLb($weight);
            }
            
            if ($country->name == "USA") {  // destination country is US.
                $itemCost = Input::get('pe_product_cost');

                if ($itemCost <= 2000) {
                    $is_customs_duty_applied = false;
                }

                $params = array(
                    "width" => $width,
                    "height" => $height,
                    "length" => $length,
                    "weight" => $weight,
                    "pickupPostalCode" => "19977",
                    "pickupStateProvinceCode" => "DE",
                    "pickupCity" => "Smyrna",
                    "pickupCountryCode" => "US",
                    "destinationPostalCode" => $destination_postal_code,
                    "service"=> "03"
                );
                //$ups_calculated_rate = Utility::calculate_ups($params);
                //$ups_rate = $ups_calculated_rate->RatedShipment[0]->TotalCharges->MonetaryValue;
                $ups = new upsRate();
				$ups->setCredentials();
                $ups_rate =  $ups->getRate($params);
                $is_ups_rate_applied = true;
            } else if ($country2->name == "USA") {
                $params = array(
                    "width" => $width,
                    "height" => $height,
                    "length" => $length,
                    "weight" => $weight,
                    "pickupPostalCode" => $pickup_postal_code,
                    "pickupStateProvinceCode" => $state2->short_name,
                    "pickupCity" => $city2->name,
                    "pickupCountryCode" => "US",
                    "destinationPostalCode" => "19977",
                    "service"=> "03"
                );
                //$ups_calculated_rate = Utility::calculate_ups($params);
                //$ups_rate = $ups_calculated_rate->RatedShipment[0]->TotalCharges->MonetaryValue;
                $ups = new upsRate();
				$ups->setCredentials();
                $ups_rate =  $ups->getRate($params);
                $is_ups_rate_applied = true;
            }
        }

        // UPS charge also applied in USA if state is diffrent
        if ($country->name == "USA" && $country2->name == "USA") {
			if($state->name != $state2->name){
				
				if (Input::get("measurement_unit") == "cm_kg") {
					$weight = $this->kgToLb($weight);
				}
                $params = array(
                    "width" => $width,
                    "height" => $height,
                    "length" => $length,
                    "weight" => $weight,
                    "pickupPostalCode" => "19977",
                    "pickupStateProvinceCode" => "DE",
                    "pickupCity" => "Smyrna",
                    "pickupCountryCode" => "US",
                    "destinationPostalCode" => $destination_postal_code,
                    "service"=> "03"
                );
				$ups = new upsRate();
				$ups->setCredentials();
                $ups_rate =  $ups->getRate($params);
                $is_ups_rate_applied = true;
                $is_customs_duty_applied = true;
			}
		}
		
        $dest_add = $this->latlongapi2($city->name . ',' . $state->name . ',' . $country->name);

        if ($dest_add != false) {
            $souce_add = $this->latlongapi2($city2->name . ',' . $state2->name . ',' . $country2->name);
            if ($souce_add != false) {
                $distance = Utility::getDistanceBetweenPointsNew($souce_add['lat'], $souce_add['lng'], $dest_add['lat'], $dest_add['lng']);
                if ($distance == false) {
                    $response['msg'] = 'Oops! We are unable to calculate distance.';
                    return json_encode($response);
                    die;
                }
            } else {
                $response['msg'] = 'Oops! We are unable to find your destination address, please correct.';
                return json_encode($response);
                die;
            }
        } else {
            $response['msg'] = 'Oops! We are unable to find your source address, please correct.';
            return json_encode($response);
            die;
        }
        
        // Get Volumetric Weight Calculator
        $VolumetricInfo = ["length"=>$length,"height"=>$height,"width"=>$width,"type"=>$weightunit];
        $VolumetricWeight =  Utility::calculate_volumetric_weight($VolumetricInfo);
        $volumetricWeightInLbs = $VolumetricWeight * 2.20462;

		if ($volumetricWeightInLbs > $weight) {
			$weight = $volumetricWeightInLbs;
		}
		
        // If weight or volumetric weight will exceed 160lbs, show the following message -
        if ($weight > 160 && $is_ups_rate_applied) {
            $response['msg'] = 'Please contact us at support@aquantuo.com for pricing.';
            return json_encode($response);
            die;
        }
        
        $requesthelper = new NewRequesthelper(
            [
                "needInsurance" => (Input::get('pe_insurance') == 'yes') ? true : true,
                "productQty" => (Input::get('pe_product_qty') < 1) ? 1 : Input::get('pe_product_qty'),
                "productCost" => Input::get('pe_product_cost'),
                "productWidth" => Input::get('width'),
                "productWidthUnit" => $lhwunit,
                "productHeight" => Input::get('height'),
                "productHeightUnit" => $lhwunit,
                "productLength" => Input::get('length'),
                "productLengthUnit" => $lhwunit,
                "productWeight" => $weight,
                "productWeightUnit" => $weightunit,
                "productCategory" => @$category->name,
                "productCategoryId" => @$category->id,
                "distance" => $distance,
                "travelMode" => Input::get('travelMode'),
                "currency" => trim(Session::get('Default_Currency')),
                'consolidate_check'=>trim(Input::get('consolidate_check')),
                'is_ups_rate_applied'=>$is_ups_rate_applied,
                'is_customs_duty_applied'=>$is_customs_duty_applied
            ]
        );
		
        $requesthelper->calculate();
        $response['calculationinfo'] = $requesthelper->get_information();

        $response['total_amount'] = 0;
        $response['ghana_total_amount'] = 0;
        if (!isset($response['calculationinfo']->error)) {
            $response['success'] = 1;
            
            if($is_ups_rate_applied){
				$warehouse_transit_fee = $response['calculationinfo']->warehouse_transit_fee;
			}
			$custom_duty = 0;
			if($country->name !== "Kenya" && $is_customs_duty_applied){
				$custom_duty = $response['calculationinfo']->DutyAndCustom;
            }
            
            $response['total_weight'] = $this->get_weight(Input::get('weight'), $weightunit);
            $config = Configuration::find(CongigId);
            if ($country->name === 'Kenya') {
                $extra = 0;
                if (Input::get('pe_product_qty') > 100) {
                    $extra = (Input::get('pe_product_qty') * $config->kenya_per_item_value)/100;
                    $extra = number_format($extra, 2, '.', '');
                }
                // if ($weightunit === 'kg') {
                //     $ratePerKg = number_format(($weight * $config->kenya_rate_per_kilogram), 2, '.', '');
                // } else {
                    $ratePerKg = number_format(($response['total_weight'] * 0.453592 * $config->kenya_rate_per_kilogram), 2, '.', '');
                // }
                $ratePerKg = $ratePerKg < $config->kenya_rate_per_kilogram ? $config->kenya_rate_per_kilogram : $ratePerKg;
                $response['calculationinfo']->shippingcost = $ratePerKg;
                // echo $ratePerKg;
                $response['total_amount'] = $ratePerKg + $extra;
                
                if ($response['total_amount'] < $config->kenya_rate_per_kilogram) {
                    $response['total_amount'] = $config->kenya_rate_per_kilogram;
                }
            } else {
                $response['total_amount'] = $response['calculationinfo']->shippingcost + $response['calculationinfo']->insurance + $custom_duty + $response['calculationinfo']->Tax + $ups_rate + $warehouse_transit_fee;
            }
            //Resion Charges
            $response['resionCharges'] = $this->resionCharges(['name' => $state->name, 'id' => $state->id]);
            $response['total_amount'] = $response['total_amount'] + $response['resionCharges'];

            $currency_conversion = $this->currency_conversion($response['total_amount']);

            $response['msg'] = "Calculation";
            $response['total_distance'] = ($distance > 0) ? $distance / 1000 : $distance;

            $response['ghana_total_amount'] = $currency_conversion['ghanian_cost'];
            $response['canadian_cost'] = $currency_conversion['canadian_cost'];
            $response['philipins_cost'] = $currency_conversion['philipins_cost'];
            $response['uk_cost'] = $currency_conversion['uk_cost'];
            $response['kenya_cost'] = $currency_conversion['kenya_cost'];
            
            $response['ups_rate'] = $ups_rate;
            $response['insurance'] = $response['calculationinfo']->insurance;
            $response['custom_duty'] = $custom_duty;
            $response['warehouse_transit_fee'] = $warehouse_transit_fee;
            
        } else {
            $response['msg'] = $response['calculationinfo']->error;
        }
        return json_encode($response);
    }

    public function currency_conversion($shipcost) {

        $data = ['cost' => 0, 'ghanian_cost' => 0, 'philipins_cost' => 0, 'uk_cost' => 0, 'canadian_cost' => 0, 'kenya_cost' => 0];
        $currency = Currency::where(['Status' => 'Active'])->select('CurrencyRate', 'FormatedText', 'CurrencyCode')->get();

        if ($currency) {
            foreach ($currency as $key) {
                if ($key->CurrencyCode == 'GHS') {
                    $data['ghanian_cost'] = str_replace('[AMT]', number_format($shipcost * $key->CurrencyRate, 2), $key->FormatedText);
                } else if ($key->CurrencyCode == 'CAD') {
                    $data['canadian_cost'] = str_replace('[AMT]', number_format($shipcost * $key->CurrencyRate, 2), $key->FormatedText);
                } else if ($key->CurrencyCode == 'PHP') {
                    $data['philipins_cost'] = str_replace('[AMT]', number_format($shipcost * $key->CurrencyRate, 2), $key->FormatedText);
                } else if ($key->CurrencyCode == 'GBP') {
                    $data['uk_cost'] = str_replace('[AMT]', number_format($shipcost * $key->CurrencyRate, 2), $key->FormatedText);
                } else if ($key->CurrencyCode == 'KES') {
                    $data['kenya_cost'] = str_replace('[AMT]', number_format($shipcost * $key->CurrencyRate, 2), $key->FormatedText);
                }
            }
        }
        return $data;
    }

    public function get_weight($weight, $type) {
        $type = strtolower($type);
        if ($type == 'kg') {
            return (((float) $weight) * 2.20462);
        } else if ($type == 'gram') {
            return (((float) $weight) * 0.00220462);
        } else {
            return $weight;
        }
    }
    public function post_form() {

        Session()->put([
            'type' => (!empty($_POST['type'])) ? $_POST['type'] : '',
            'travelMode' => (!empty($_POST['travelMode'])) ? $_POST['travelMode'] : '',
            'country' => (!empty($_POST['country'])) ? $_POST['country'] : '',
            'state' => (!empty($_POST['state'])) ? $_POST['state'] : '',
            'city' => (!empty($_POST['city'])) ? $_POST['city'] : '',
            'country2' => (!empty($_POST['country2'])) ? $_POST['country2'] : '',
            'state2' => (!empty($_POST['state2'])) ? $_POST['state2'] : '',
            'city2' => (!empty($_POST['city2'])) ? $_POST['city2'] : '',
            'category' => (!empty($_POST['category'])) ? $_POST['category'] : '',
            'weight' => (!empty($_POST['weight'])) ? $_POST['weight'] : '',
            'weight_unit' => (!empty($_POST['weight_unit'])) ? $_POST['weight_unit'] : '',
            'measurement_unit' => (!empty($_POST['measurement_unit'])) ? $_POST['measurement_unit'] : '',
            'length' => (!empty($_POST['length'])) ? $_POST['length'] : '',
            'height' => (!empty($_POST['height'])) ? $_POST['height'] : '',
            'width' => (!empty($_POST['width'])) ? $_POST['width'] : '',
            'pe_product_cost' => (!empty($_POST['pe_product_cost'])) ? $_POST['pe_product_cost'] : '',
            'consolidate_item'=> (!empty($_POST['consolidate_check'])) ? $_POST['consolidate_check'] : '',
        ]);
        if (session()->has('UserId')) {
            $response['success'] = 1;
        } else {
            $response['success'] = 0;
        }
        $response['result'] = $_POST;
        return json_encode($response);
    }

    public function track_ordder() {

        $query = Activitylog::query();
        $searchvalue = trim(Input::get('package_id'));

        if ($searchvalue != "") {
            if (Input::get('package_readio') == 'package_id') {
                $where = ['PackageNumber' => $searchvalue];
                $data['package'] = Activitylog::where($where)->orderby('EnterOn', 'desc', 'request_type')->get();
            } else {
                $where = ['package_id' => $searchvalue];
                $data['package'] = Activitylog::where($where)->orderby('EnterOn', 'desc', 'request_type')->get();
            }

            // created and Expected date for use
            if (isset($data['package'][0])) {
                $where2 = array("ProductList" => array('$elemMatch' => array('package_id' => $searchvalue)));
                $data['date'] = Deliveryrequest::where($where2)->select('_id', 'EnterOn', 'UpdateOn', 'ExpectedDate', 'Status', 'updated_at', 'RequestType', 'ProductList.$')->first();
                // current status date
                if(isset($data['package'][0]['request_id'])){
                    $data['current_status'] = Deliveryrequest::where(['_id'=>$data['package'][0]['request_id']])->select('Status','RequestType')->first();
                }

                $data['activity'] = Activitylog::where('package_id', '=', $searchvalue)
                    ->where('status', '=', $data['date']['ProductList'][0]['status'])
                    ->select('EnterOn', '_id', 'status')
                    ->get();
            }
        } else {
            $data['package'] = [];
            $data['date'] = '';
            $data['activity'] = '';
        }
        return view('User::list.track_order', $data);
    }

    public function post_track_ordder() {

        $query = Deliveryrequest::Query();

        if (Input::get('package_readio') == 'PackageNumber') {

            $query->where('PackageNumber', '=', Input::get('package'));
            $data['result'] = $query->first();
            $data['product'] = 'multiple';

        } else {
            $query->where('ProductList.package_id', '=', Input::get('package'));
            $res = $query->first();

            if (count($res) > 0) {
                for ($i = 0; $i < count($res->ProductList); $i++) {
                    if ($res->ProductList[$i]['package_id'] == Input::get('package')) {
                        $data['result'] = $res->ProductList[$i];
                        $data['result']['EnterOn'] = $res->EnterOn;
                        $data['result']['DeliveryDate'] = $res->DeliveryDate;
                        $data['result']['distance'] = $res->distance;
                        $data['result']['shippingCost'] = $res->shippingCost;
                        break;
                    }
                }
            } else { $data['result'] = array();}
            $data['product'] = '';
        }

        $data['package'] = Input::get('package');
        $data['package_readio'] = Input::get('package_readio');
        return view('User::list.track_order', $data);
    }

}
