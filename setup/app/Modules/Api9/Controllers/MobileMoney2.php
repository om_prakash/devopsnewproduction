<?php namespace App\Modules\Api9\Controllers;

use App;
use App\Http\Controllers\Controller;
use App\Http\Models\CityStateCountry;
use App\Http\Models\Deliveryrequest;
use App\Http\Models\Mobilemoney;
use App\Http\Models\Transaction;
use Input;
use MongoDate;
use MongoId;
use Validator;

class MobileMoney2 extends Controller {
	/*
		 	 * Author: Aakash Tejwal
		 	* Created Date : 28-02-2018
	*/

	public function update_mobile_number()
    {
        $response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
        $validate = Validator::make(Input::get(), [
            'userId' => 'required',
            'country_code' => 'required',
            'mobile_number' => 'required',
            'mobile_type' => 'required',
            'number_id' => 'required',

        ]);
        if ($validate->fails()) {
            $validate->errors()->add('field', trans('Oops! Something went wrong'));
            return $validate->errors();
        } else {
            
                $mobile = Mobilemoney::where(['_id'=> Input::get('number_id')])->where('user_id','=',Input::get('userId'))->first();
                if(count($mobile) > 0){
                   $mobile->country_code =  Input::get('country_code');
                   $mobile->mobile_number =  Input::get('mobile_number');
                   $mobile->alternet_moblie =  substr(Input::get('mobile_number'), -4);
                   $mobile->mobile_type =  Input::get('mobile_type');
                   if(Input::get('voucher_code') != ''){
                        $mobile->voucher_code =  Input::get('voucher_code');
                   }
                   
                   if($mobile->save()){
                        $response = ['success' =>1, 'msg' => 'Mobile number has been updated successfully.'];
                        
                   }
                }
            
            return response()->json($response);
        }

    }

	public function addmobilenumber() {
		$response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		$validate = Validator::make(Input::get(), [
			'userId' => 'required',
			'country_code' => 'required',
			'mobile_number' => 'required',
			'mobile_type' => 'required',
		]);
		if ($validate->fails()) {
			$validate->errors()->add('field', trans('Oops! Something went wrong'));
			return $validate->errors();
		} else {

			$insertData = [
				'user_id' => Input::get('userId'),
				'country_code' => Input::get('country_code'),
				'mobile_number' => Input::get('mobile_number'),
				'alternet_moblie' => substr(Input::get('mobile_number'), -4),
				'mobile_type' => Input::get('mobile_type'),
				'voucher_code' => Input::get('voucher_code'),
				'EnterOn' => new MongoDate(),
			];
			$data = Mobilemoney::insert($insertData);
			if (count($data) > 0) {
				$response = [
					'success' => 1,
					'msg' => 'Mobile number has been added successfully.',
				];
			} else {
				$response = [
					'success' => 0,
					'msg' => 'Oops! Something went wrong.',
				];
			}

		}
		return response()->json($response);
	}

	public function mobile_money_responce() {
		$response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		$validate = Validator::make(Input::get(), [
			'userId' => 'required',
			'package_id' => 'required',
			'country_code' => 'required',
			'mobile_number' => 'required',
			'mobile_moneyId' => 'required',
			'amount' => 'required',
			'mobile_type' => 'required',
		]);
		if ($validate->fails()) {
			$validate->errors()->add('field', trans('Oops! Something went wrong'));
			return $validate->errors();
		} else {

			$userData = Deliveryrequest::where('_id', '=', Input::get('package_id'))->first();
			$mobileMoney = Mobilemoney::where(['user_id' => Input::get('userId'), 'mobile_number' => Input::get('mobile_number')])->select('_id', 'country_code', 'mobile_type', 'alternet_moblie')->first();
			$CityStateCountry = CityStateCountry::where(['type' => 'Country', 'Content' => 'Ghana'])->select('Content', 'CurrencyCode', 'CurrencySymbol', 'CurrencyRate')->first();

			if (Input::get('country_code') == '+233') {
				$CurrencyRate = $CityStateCountry['CurrencyRate'];
			} else {
				$CurrencyRate = 1;
			}

			if (Input::get('mobile_type') == 'vodafone') {
				$reqstatus = 'momo_initiated';
				$mobilestatus = 'awaiting_payment';
			} else {
				$reqstatus = 'momo_initiated';
				$mobilestatus = 'awaiting_payment';
			}

			$mobilemoney[] = [
				'_id' => (string) new MongoId(),
				'country_code' => $mobileMoney['country_code'],
				'mobile_number_money' => Input::get('mobile_number'),
				'mobile_money_status' => $mobilestatus,
				'CurrencyRate' => $CurrencyRate,
				'mobile_money_amount' => Input::get('amount'),
				'alternet_moblie' => $mobileMoney['alternet_moblie'],
				'mobile_moneyId' => (int) Input::get('mobile_moneyId'),
				'mobile_type' => Input::get('mobile_type'),
			];

			if (isset($userData->mobilemoney)) {

				$mobilemoney2 = [
					'_id' => (string) new MongoId(),
					'country_code' => $mobileMoney['country_code'],
					'mobile_number_money' => Input::get('mobile_number'),
					'mobile_money_status' => $mobilestatus,
					'CurrencyRate' => $CurrencyRate,
					'mobile_money_amount' => Input::get('amount'),
					'alternet_moblie' => $mobileMoney['alternet_moblie'],
					'mobile_moneyId' => (int) Input::get('mobile_moneyId'),
					'mobile_type' => Input::get('mobile_type'),
				];

				$finalresult = Deliveryrequest::where(['_id' => trim(Input::get('package_id'))])->push(['mobilemoney' => $mobilemoney2]);
				if ($userData['Status'] == 'pending') {
                    Deliveryrequest::where('_id', '=', Input::get('package_id'))->update([
                        'PaymentStatus' => 'capture',
                        'Status' => $reqstatus,
                        'PaymentDate' => new MongoDate(),
                    ]);
                }
			} else {
				if ($userData['Status'] == 'pending') {
					$updateData = Deliveryrequest::where('_id', '=', Input::get('package_id'))->update([
						'PaymentStatus' => 'capture',
						'Status' => $reqstatus,
						'PaymentDate' => new MongoDate(),
						'mobilemoney' => $mobilemoney,
					]);

					if (isset($userData->ProductList)) {
						$p_array = $userData->ProductList;
						foreach ($p_array as $key => $value) {
							$p_array[$key]['status'] = $reqstatus;
							$p_array[$key]['PaymentStatus'] = 'yes';
						}
						$update['ProductList'] = $p_array;
					}
					$finalresult = Deliveryrequest::where('_id', '=', Input::get('package_id'))->update($update);
				}
			}

			$up = Deliveryrequest::where('_id', '=', Input::get('package_id'))->update(['need_to_pay' => 0]);
			Transaction::insert(array(
				"CurrencyRate" => @$CurrencyRate,
				"momo_id" => (int) Input::get('mobile_moneyId'),
				"SendById" => (String) Input::get('userId'),
				"SendByName" => @$userData['RequesterName'],
				"SendToId" => "aquantuo",
				"RecieveByName" => "Aquantuo",
				"Description" => "Amount deposited for Mobile Money product PackageId: {$userData['PackageNumber']}",
				"Credit" => floatval(Input::get('amount')),
				"Debit" => "",
				"Status" => "pending",
				"TransactionType" => $userData['RequestType'],
				"request_id"=> Input::get('package_id'),
				"EnterOn" => new MongoDate(),
			));
			
			
				

			if (count($finalresult) > 0) {
				$response = [
					'success' => 1,
					//'msg' => 'Transaction has been completed successfully. Please verify transaction request on your phone to effect payment.',
					'msg' => "Payment has been requested. Please check your mobile device to approve/confirm payment.",
				];
			} else {
				$response = [
					'success' => 0,
					'msg' => 'Oops! Something went wrong.',
				];
			}

		}
		return response()->json($response);
	}

	public function delete_mobile_no() {
		$response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		$validate = Validator::make(Input::get(), [
			'userId' => 'required',
			'mobile_no' => 'required',
		]);
		if ($validate->fails()) {
			$validate->errors()->add('field', trans('Oops! Something went wrong'));
			return $validate->errors();
		} else {

			$data = Mobilemoney::where(['user_id' => Input::get('userId'),
				'mobile_number' => Input::get('mobile_no'),
			])->delete();

			if (count($data) > 0) {
				$response = ['success' => 1,
					'msg' => 'Delete has been successfully.',
				];

			} else {
				$response = ['success' => 0,
					'msg' => 'Oops! Something went wrong.'];
			}

		}
		return response()->json($response);
	}

}
