<?php namespace App\Modules\Api9\Controllers;

use App;
use App\Http\Controllers\Controller;
use App\Http\Models\Activitylog;
use App\Http\Models\Category;
use App\Http\Models\Configuration;
use App\Http\Models\Currency;
use App\Http\Models\Deliveryrequest;
use App\Http\Models\Distance;
use App\Http\Models\Extraregion;
use App\Http\Models\Notification;
use App\Http\Models\RequestCard;
use App\Http\Models\Setting;
use App\Http\Models\Transaction;
use App\Http\Models\User;
use App\Library\QueueEmail;
use App\Library\Reqhelper;
use App\Library\Utility;
use App\Modules\User\Controllers\Payment;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use MongoDate;
use MongoId;
use App\Http\Models\Itemhistory;

class LocalDelivery extends Controller {

	public function check_mail() {
		//echo phpinfo(); die;
		$Email = new QueueEmail();
		$emailinfo = array(
			"to" => "kapil@idealittechno.com",
			"replace" => array(
				"[USERNAME]" => "check",
			),
		);
		$checkmail = $Email->send("560f774ce4b0e8a48085522c", $emailinfo);
	}

	public function calculation() {
		$response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		$validate = Validator::make(Input::get(), [
			//'category_id'=>'required',
			'distance' => 'required',
			//'item_value'=>'required',
			'products_list' => 'required',
			'country_id' => 'required',
			'state_id' => 'required',
			'state_name' => 'required',
			'user_id' => 'required',
			'Default_Currency' => 'required',
		]);

		if ($validate->fails()) {
			$validate->errors()->add('field', trans('Oops! Something went wrong'));
			return $validate->errors();
		} else {
			$data['item_price'] = 0;
			$data['category_price'] = 0;
			$data['distance_price'] = 0;
			$data['AreaCharges'] = 0;
			$data['insurance'] = 0;
			$item_total_price = 0;
			$distance = Input::get('distance') * 0.000621371;
			$match_distance = Distance::Where('from', '<=', (float) $distance)->where('to', '>=', (float) $distance)
				->where(['Status' => 'Active', 'country_id' => Input::get('country_id'), "type" => "distance"])
				->select('price')
				->first();
			if (count($match_distance) == 0) {
				$response = ['success' => 0, 'msg' => 'Sorry! Shipping condition does not match according to choose addresses.'];
				return response()->json($response);
			}

			$match_value = Distance::where(['Status' => 'Active', 'country_id' => Input::get('country_id'), "type" => "item-value"])
				->select('price', 'from', 'to')->get();
			if (count($match_distance) == 0) {
				return response()->json($response);
			}

			$volume = $weight = $p= 0;
			$item = json_decode(Input::get('products_list'), 1);
			$return_array = $item;

			foreach ($return_array as $key => $value) {
				$return_array[$key]['shippingCost'] = 0;
				$return_array[$key]['InsuranceCost'] = 0;
			}

			foreach ($item as $key => $value) {
				$return_array[$key]['shippingCost'] = 0;
				$item_total_price += ($value['productCost'] * $value['productQty']);
				//Weight
				$weight += $this->get_weight($value['productWeight'], $value['productWeightUnit']);
				//Volume
				$volume += $this->getVolumeInLbs(array(
					"width" => $value['productWidth'],
					"widthunit" => $value['productHeightUnit'],
					"height" => $value['productHeight'],
					"heightunit" => $value['productHeightUnit'],
					"length" => $value['productLength'],
					"lengthunit" => $value['productHeightUnit'],
				));

				//Insurance
				if ($value['InsuranceStatus'] == 'yes') {
					$return_array[$key]['InsuranceCost'] = $this->get_insurance($value['productCost'], $value['productQty']);
					$data['insurance'] += $return_array[$key]['InsuranceCost'];
				}

				//Price Item category
				$match_category = Category::where(['_id' => $value['productCategoryId'], 'Status' => 'Active', 'type' => 'localCategory', 'country_id' => Input::get('country_id')])->select('price')
					->first();
				if ($match_category) {
					$data['category_price'] += $match_category->price;
					//$return_array[$key]['shippingCost'] = $match_category->price;
				} else {
					$response = ['success' => 0, 'msg' => 'Category not found.'];
					return response()->json($response);
				}
				//Item value price
				$temp = 0;
				foreach ($match_value as $key2) {

					if ($key2->from <= (float) trim($value['productCost']) && $key2->to >= (float) trim($value['productCost'])) {
						$data['item_price'] += $key2->price;
						$p = $key2->price;
						$temp = $p;
						//$return_array[$key]['shippingCost'] += $match_category->price;
					} 
				}

				if($temp == 0){
					$response = ['success' => 0, 'msg' => "The item value you have specified is unsupported for product '" . $value['item_name'] . '.'];
					return response()->json($response);
				}
				//Distance price
				$data['distance_price'] += $match_distance->price;
				$return_array[$key]['shippingCost'] = $match_category->price + $p + $match_distance->price;
			}

			//extraa charge of out side accra
			$data['AreaCharges'] = $this->resionCharges(['name' => Input::get('state_name'), 'id' => Input::get('state_id')]);
			//end accra charge
			$data['shippingCost'] = $data['category_price'] + $data['item_price'] + $data['distance_price'];
			$data['totalCost'] = $data['category_price'] + $data['item_price'] + $data['distance_price'] + $data['AreaCharges'] + $data['insurance'];

			//Miles to Km
			$distance = $distance * 1.60934;
			//Lbs to kg
			//$weight = $weight * 0.453592;

			$data['distance'] = $distance;
			$data['distanceShow'] = number_format($distance, 2) . " Km";
			
			$data['volume'] = $volume;
			$data['weight'] = $weight;

			$data['shippingCost'] = $data['shippingCost'];
			$data['totalCost'] = $data['totalCost'];
			$data['insurance'] = $data['insurance'];

			$currency_conversion = $this->currency_conversion($data['totalCost']);

			if (input::get('currency') == 'GHS') {
				$data['GhanaTotalCost'] = $currency_conversion['ghanian_cost'];
			} else if (Session()->get('Default_Currency') == 'CAD') {
				$data['GhanaTotalCost'] = $currency_conversion['canadian_cost'];
			} else if (Session()->get('Default_Currency') == 'PHP') {
				$data['GhanaTotalCost'] = $currency_conversion['philipins_cost'];
			} else if (Session()->get('Default_Currency') == 'GBP') {
				$data['GhanaTotalCost'] = $currency_conversion['uk_cost'];
			} else {
				$data['GhanaTotalCost'] = $currency_conversion['ghanian_cost'];
			}
			$currencies = array(array('label'=>'Ghana - GHS','symbol'=>'GHS','value'=>$currency_conversion['GHSRate']),
						);

			$data['shipping_cost_in_ghs'] = $this->currency_conversion_without_format_text($data['shippingCost']);
			$data['regionCharges_in_ghs'] = $this->currency_conversion_without_format_text($data['AreaCharges']);
			
			$data['insurance_price_in_ghs'] = $this->currency_conversion_without_format_text($data['insurance']);
			$data['total_amount_in_ghs'] = $this->currency_conversion_without_format_text($data['totalCost']);
			
			$data['products_list'] = $return_array;
			$data['note_html'] = "";
			$data['currencies'] = $currencies;
			$data['total_no_of_item'] = count($item);
			$data['item_total_price'] = $item_total_price;
			
			

			$response = ['success' => 1, 'msg' => 'Calculation', 'result' => $data];
			return response()->json($response);
		}
	}

	public function currency_conversion_without_format_text($shipcost,$currency="GHS") {
		$currency = Currency::where(['Status' => 'Active','CurrencyCode'=>'GHS'])->select('CurrencyRate','CurrencyCode')->first();
		$cost = 0.0;
		if ($currency) {
			$cost =  number_format($shipcost * $currency->CurrencyRate, 2);
		}
		return $cost;
	}

	public function currency_conversion($shipcost) {

		$data = ['cost' => 0, 'ghanian_cost' => 0, 'philipins_cost' => 0, 'uk_cost' => 0, 'canadian_cost' => 0];
		$currency = Currency::where(['Status' => 'Active'])->select('CurrencyRate', 'FormatedText', 'CurrencyCode')->get();

		if ($currency) {
			foreach ($currency as $key) {
				if ($key->CurrencyCode == 'GHS') {
					$data['GHSRate'] = $key['CurrencyRate'];
					$data['ghanian_cost'] = str_replace('[AMT]', number_format($shipcost * $key->CurrencyRate, 2), $key->FormatedText);
				} else if ($key->CurrencyCode == 'CAD') {
					$data['canadian_cost'] = str_replace('[AMT]', number_format($shipcost * $key->CurrencyRate, 2), $key->FormatedText);
				} else if ($key->CurrencyCode == 'PHP') {
					$data['philipins_cost'] = str_replace('[AMT]', number_format($shipcost * $key->CurrencyRate, 2), $key->FormatedText);
				} else if ($key->CurrencyCode == 'GBP') {
					$data['uk_cost'] = str_replace('[AMT]', number_format($shipcost * $key->CurrencyRate, 2), $key->FormatedText);
				}
			}
		}
		return $data;
	}

	public function resionCharges($state) {
		$charges = Extraregion::where(['state_id' => $state['id'], 'status' => 'Active'])->select('amount')->first();
		if ($charges) {
			return $charges->amount;
		} else {
			return 0.00;
		}
	}

	public function get_weight($weight, $type) {
		$type = strtolower($type);
		if ($type == 'kg') {
			return (((float) $weight) * 2.20462);
		} else if ($type == 'gram') {
			return (((float) $weight) * 0.00220462);
		} else {
			return $weight;
		}
	}

	public function get_volume($array) {
		return ($this->get_size_in_cm($array['length'], $array['lengthunit']) * $this->get_size_in_cm($array['width'], $array['widthunit']) * $this->get_size_in_cm($array['height'], $array['heightunit']));
	}

	public function get_size_in_cm($height, $unit) {
		$in_cm = (float) $height;
		$unit = strtolower($unit);
		if ($unit == 'inches') {
			$in_cm = $in_cm * 2.54;
		}
		return $in_cm;
	}
	
	public function getVolumeInLbs($array) {
		
		return ($this->get_size_in_inch($array['length'], $array['lengthunit']) * $this->get_size_in_inch($array['width'], $array['widthunit']) * $this->get_size_in_inch($array['height'], $array['heightunit']));
	}
	public function get_size_in_inch($height, $unit) {
		$in_lbs = (float) $height;
		$unit = strtolower($unit);
		if ($unit == 'cm') {
			$in_lbs = $in_lbs / 2.54;
		}
		return $in_lbs;
	}

	public function get_insurance($cost, $quantity) {
		$data = Configuration::where(array('_id' => '5673e33e6734c4f874685c84'))
			->select('Insurance')->first();
		$value = 0;
		if (count($data) > 0) {
			if (is_array($data->Insurance)) {
				foreach ($data->Insurance as $key) {
					if ($key['MinPrice'] <= (float) $cost && $key['MaxPrice'] >= (float) $cost) {
						$value = $key['Rate'] * (int) $quantity;
					}
				}
			}
		}
		return $value;
	}

	/*
		     * function for: Create local delivery
		     * function created by: Deepak Pateriya
		     * Created At: 28 feb 2018
		     * Updated By:
		     * Updated At:
	*/
	public function create_local_delivery_request() {
		$response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		$validate = Validator::make(Input::get(), [
			'user_id' => 'required',
			'products_list' => 'required',
		]);

		if ($validate->fails()) {
			$validate->errors()->add('field', trans('Oops! Something went wrong'));
			return $validate->errors();
		} else {

			$userInfo = User::where(['_id' => Input::get('user_id')])->first();
			if (count($userInfo) == 0) {

				return response()->json($response);die;
			}

			$city = trim(Input::get('city'));
			$state = trim(Input::get('state'));
			$country = trim(Input::get('country'));

			$drop_off_city = trim(Input::get('drop_off_city'));
			$drop_off_state = trim(Input::get('drop_off_state'));
			$drop_off_country = trim(Input::get('drop_off_country'));

			$return_city = trim(Input::get('return_city'));
			$return_state = trim(Input::get('return_state'));
			$return_country = trim(Input::get('return_country'));

			$nd_return_city = trim(Input::get('nd_return_city'));
			$nd_return_state = trim(Input::get('nd_return_state'));
			$nd_return_country = trim(Input::get('nd_return_country'));

			$calculated_distance = floatval(Input::get('calculated_distance'));
			if ($calculated_distance > 0) {
				$calculated_distance = $calculated_distance * 0.000621371;
			} else {
				$calculated_distance = Utility::getDistanceBetweenPointsNew(
					floatval(Input::get('PickupLat')),
					floatval(Input::get('PickupLong')),
					floatval(Input::get('DeliveryLat')),
					floatval(Input::get('DeliveryLong'))
				);
			}

			$ProductImage = $pickUpDate = $deliveryDate = '';
			if (Input::get('pickup_date') != '') {
				$pickUpDate = new MongoDate(strtotime(Input::get('pickup_date')));
			}

			if (Input::get('drop_off_date') != '') {
				$deliveryDate = new MongoDate(strtotime(Input::get('drop_off_date')));
			}

			if (Input::get('RequestId') != '') {
				$req_data = Deliveryrequest::where(['_id' => trim(Input::get('RequestId')), 'Status' => 'pending'])->first();
				if (count($req_data) == 0) {

					return response()->json($response);
				}
				$PackageId = $req_data->PackageId;
				$Pn = $req_data->PackageNumber;
			} else {
				$PackageId = Utility::sequence('Request');
				$Pn = $PackageId . time();
			}

			$insertArray = [
				'RequesterId' => new MongoId(Input::get('user_id')),
				'RequesterName' => ucfirst($userInfo->Name),
				'ProductTitle' => '',
				'RequestType' => "local_delivery",
				'Status' => 'pending',
				'ReceiverIsDifferent'=> (Input::get('ReceiverIsDifferent') == 'on')?'yes':'no', 
				'ReceiverName'=> (Input::get('ReceiverIsDifferent') == 'on')? ucfirst(Input::get('ReceiverName')):'' ,
				'PackageId' => $PackageId,
				'device_version' => Input::get('device_version'),
				'app_version' => Input::get('app_version'),
				'device_type' => Input::get('device_type'),
				"PackageNumber" => $Pn,
				"PickupFullAddress" => $this->formated_address([
					(trim(Input::get('address_line_1')) != '') ? trim(Input::get('address_line_1')) : '',
					(trim(Input::get('address_line_2') != '')) ? trim(Input::get('address_line_2')) : '',
					@$city,
					@$state,
					@$country,
				], Input::get('zipcode')),

				'PickupAddress' => trim(Input::get('address_line_1')),
				'PickupAddress2' => trim(Input::get('address_line_2')),
				'PickupCity' => @$city,
				'PickupState' => @$state,
				'PickupCountry' => @$country,
				'PickupPinCode' => trim(Input::get('zipcode')),
				'PickupLatLong' => [floatval(Input::get('PickupLong')), floatval(Input::get('PickupLat'))],
				'PickupDate' => $pickUpDate,
				'DeliveryFullAddress' => $this->formated_address([
					(trim(Input::get('drop_off_address_line_1')) != '') ? trim(Input::get('drop_off_address_line_1')) : '',
					(trim(Input::get('drop_off_address_line_2')) != '') ? trim(Input::get('drop_off_address_line_2')) : '',
					@$drop_off_city,
					@$drop_off_state,
					@$drop_off_country,
				], Input::get('drop_off_zipcode')),
				'DeliveryAddress' => trim(Input::get('drop_off_address_line_1')),
				'DeliveryAddress2' => trim(Input::get('drop_off_address_line_2')),
				'DeliveryCity' => @$drop_off_city,
				'DeliveryState' => @$drop_off_state,
				'DeliveryCountry' => @$drop_off_country,
				'DeliveryPincode' => trim(Input::get('drop_off_zipcode')),
				"DeliveryLatLong" => [floatval(Input::get('DeliveryLong')), floatval(Input::get('DeliveryLat'))],
				'DeliveryDate' => $deliveryDate,

				"ReturnFullAddress" => $this->formated_address([
					Input::get('return_address_line_1'),
					Input::get('return_address_line_2'),
					@$return_city,
					@$return_state,
					@$return_country,
				], Input::get('return_zipcode')),
				"ReturnAddressSamePickup" => Input::get('ReturnAddressSamePickup'),
				"ReturnAddress" => trim(Input::get('return_address_line_1')),
				"ReturnAddress2" => trim(Input::get('return_address_line_2')),
				"ReturnCityTitle" => @$return_city,
				"ReturnStateTitle" => @$return_state,
				'ReturnCountry' => @$return_country,
				'ReturnPincode' => trim(Input::get('return_zipcode')),
				"FlexibleDeliveryDate" => (Input::get('is_delivery_date_flexible') == 'yes') ? 'yes' : 'no',
				"JournyType" => (Input::get('journey_type') == 'return') ? 'return' : 'one_way',
				'NotDelReturnFullAddress' => $this->formated_address([
					trim(Input::get('nd_return_address_line_1')),
					trim(Input::get('nd_return_address_line_2')),
					@$nd_return_city,
					@$nd_return_state,
					@$nd_return_country,
				], Input::get('nd_return_zipcode')),
				"InCaseNotDelReturnSamePickup" => Input::get('InCaseNotDelReturnSamePickup'),
				"InCaseNotDelReturnAddress" => trim(Input::get('nd_return_address_line_1')),
				"InCaseNotDelReturnAddress2" => trim(Input::get('nd_return_address_line_2')),
				"InCaseNotDelReturnCity" => @$nd_return_city,
				"InCaseNotDelReturnState" => @$nd_return_state,
				"InCaseNotDelReturnCountry" => @$nd_return_country,
				"InCaseNotDelReturnPincode" => trim(Input::get('nd_return_zipcode')),
				"PackageMaterial" => (Input::get('need_package_material') == 'yes') ? 'yes' : 'no',
				"PackageMaterialShipped" => '',
				"PublicPlace" => (Input::get('public_palce') == 'yes') ? 'yes' : 'no',
				'ReceiverCountrycode' => trim(Input::get('country_code')),
				'ReceiverMobileNo' => trim(Input::get('phone_number')),
				'SenderMobileNo' => Input::get('SenderMobileNo'),
				'SenderCountrycode'=>Input::get('SenderCountrycode'),
				'FlexibleDeliveryDate' => Input::get('is_delivery_date_flexible'),
				"Distance" => $calculated_distance,
				"StripeChargeId" => '',
				"PaymentDate" => "",
				"PaymentStatus" => 'pending',
				'promocode' => trim(Input::get('promocode')),
				'itemCount' => 0,
				'ShippingCost' => 0,
				'InsuranceCost' => 0,
				'Discount' => trim(Input::get('Discount')),
				'AreaCharges' => 0,
				'TotalCost' => 0,
				'after_update_difference' => 0,
				"EnterOn" => new MongoDate(),
				"UpdateOn" => new MongoDate(),
				'drop_off_state_id' => Input::get('drop_off_state_id'),
				'country_id' => Input::get('country_id'),
				'ProductList' => [],
			];

			$products_list = json_decode(Input::get('products_list'), 1);

			$k = 0;
			foreach ($products_list as $key) {

				$k = $k + 1;
				$insertArray['ShippingCost'] += @$key['shippingCost'];
				$insertArray['InsuranceCost'] += @$key['InsuranceCost'];
				$insertArray['TotalCost'] += @$key['shippingCost']+@$key['InsuranceCost'];

				if (!empty(trim($insertArray["ProductTitle"]))) {
					$insertArray["ProductTitle"] = "{$insertArray['ProductTitle']},";
				}

				$insertArray["ProductTitle"] .= @$key['item_name'];
				$insertArray['ProductList'][] = [
					"_id" => (string) new MongoId(),
					"product_name" => @$key['item_name'],
					
					'package_id' => (@$key['package_id'] != '') ? $key['package_id'] : $Pn . $k,

					"status" => 'pending',
					"productWidth" => $key['productWidth'],
					"productHeight" => $key['productHeight'],
					"productLength" => $key['productLength'],
					"productCost" => $key['productCost'],
					"productWeight" => $key['productWeight'],
					"productHeightUnit" => $key['productHeightUnit'],
					"ProductWeightUnit" => $key['productWeightUnit'],
					"ProductLengthUnit" => $key['productLengthUnit'],
					"productCategory" => $key['productCategory'],
					"productCategoryId" => $key['productCategoryId'],
					//"travelMode" => $key['travelMode'],
					"InsuranceStatus" => $key['InsuranceStatus'],
					"productQty" => $key['productQty'],
					"ProductImage" => $key['ProductImage'],
					"QuantityStatus" => $key['QuantityStatus'],
					"BoxQuantity" => $key['productQty'],
					"Description" => $key['description'],
					"PackageMaterial" => $key['package_material_needed'],
					"shippingCost" => @$key['shippingCost'],
					'InsuranceCost' => @$key['InsuranceCost'],
					"DeliveryVerifyCode" => (Input::get('request_id') == '') ? rand(1000, 9999) : $key['DeliveryVerifyCode'],
					'tpid' => '',
					'tpName' => '',
					'inform_mail_sent' => 'no',
					'PaymentStatus' => 'no',
					'total_cost' => @$key['shippingCost']+@$key['InsuranceCost'],
					'after_update' => @$key['shippingCost']+@$key['InsuranceCost'],
					'RequesterRating' => 0,
					'RequesterFeedbcak' => '',
					'TransporterFeedbcak' => '',
					'TransporterRating' => 0,
					'aq_fees'=>0,
				];

			}

			$insertArray['itemCount'] = $k;
			$insertArray['TotalCost'] = $insertArray['TotalCost'] - trim(Input::get('Discount'));
			//  $insertArray['AquantuoFees'] = $this->get_aquantuo_fees($insertArray['TotalCost']);
			$insertArray['TotalCost'] = $insertArray['TotalCost'];

			//Region Charges
			$insertArray['AreaCharges'] = trim(Input::get('region_charge'));
			$insertArray['TotalCost'] = $insertArray['TotalCost'] + $insertArray['AreaCharges'];

		}

		//print_r($insertArray);die;
		$currency_conversion = $this->currency_conversion($insertArray['TotalCost']);

		
		$user_currency = $currency_conversion['ghanian_cost'];
		

		if (Input::get('RequestId') == '') {
			$record = (String) Deliveryrequest::insertGetId($insertArray);
			if ($record) {
				$response = ['success' => 1, 'result' => ['local_delivery_request_id' => $record], 'msg' => 'Request has been created successfully.','TotalCost'=> $insertArray['TotalCost'],'user_currency'=>$user_currency];

				$ActivityLog = DeliveryRequest::where('_id', '=', $record)->select('_id', 'PackageNumber', 'ProductList', 'RequesterId')->first();

				$array = $ActivityLog->ProductList;

				/* Activity Log  Create Local delivery */
				foreach ($array as $key) {
					$insertactivity = [
						'request_id' => $ActivityLog->_id,
						'request_type' => 'local_delivery',
						'PackageNumber' => $ActivityLog->PackageNumber,
						'item_id' => $key['_id'],
						'package_id' => $key['package_id'],
						'item_name' => $key['product_name'],
						'log_type' => 'request',
						'message' => 'Package has been created.',
						'status' => 'pending',
						'EnterOn' => new MongoDate(),
					];

					Activitylog::insert($insertactivity);
				}
			}
		} else {
			$update = Deliveryrequest::where(['_id' => Input::get('RequestId')])->update($insertArray);
			if ($update) {
				$response = ['success' => 1, 'result' => ['local_delivery_request_id' => Input::get('RequestId')],'TotalCost'=> $insertArray['TotalCost'],'user_currency'=>$user_currency, 'msg' => 'Request has been updated successfully.'];
			}

		}

		return response()->json($response);die;}

	/*
		* function for: Edit local delivery
		* function created by: Deepak Pateriya
		* Created At: 01 march 2018
		* Updated By:
		* Updated At:
	*/
	public function local_delivery_detail() {
		$response = ['success' => 0, 'msg' => 'Record not found.'];
		$result = Deliveryrequest::where(['_id' => Input::get('local_delivery_request_id'), 'RequestType' => 'local_delivery'])->first();
		if (!$result) {
			return response()->json($response);die;
		}
		$result['PickupDate'] = (String) @$result->PickupDate->sec;
		$result['DeliveryDate'] = (String) @$result->DeliveryDate->sec;
		$result['JournyType'] = ucwords(str_replace('_', ' ', $result->JournyType));
		$return_array = $result['ProductList'];

		$cards = RequestCard::where(['request_id' => Input::get('local_delivery_request_id')])->select('last4', 'brand', 'payment')->get();
		$itemHistory = Itemhistory::where(['request_id'=>Input::get('local_delivery_request_id')])->get();

		foreach ($return_array as $key => $value) {
			$return_array[$key]['show_status'] = $this->get_status_title($value['status'], 'local_delivery');
			$return_array[$key]['ChatUserName'] = "";
			$return_array[$key]['ChatUserImage'] = "";
			$return_array[$key]['ChatUserId'] = $value['tpid'];
			$return_array[$key]['UserRating'] = 0;
			if ($value['tpid'] != '') {
				$transporter = User::where(['_id' => $value['tpid']])->select('Name', 'Image', 'RatingByCount', 'RatingCount')->first();
				if ($transporter) {
					$return_array[$key]['ChatUserName'] = $transporter->Name;
					$return_array[$key]['ChatUserImage'] = $transporter->Image;
				}

				if (@$transporter->RatingCount > 0 && @$transporter->RatingByCount > 0) {
					$return_array[$key]['UserRating'] = $transporter->RatingCount / $transporter->RatingByCount;
				}

			}
			$return_array[$key]['PickupLat'] = @$result->PickupLatLong[1];
			$return_array[$key]['PickupLong'] = @$result->PickupLatLong[0];
			$return_array[$key]['DeliveryLat'] = @$result->DeliveryLatLong[1];
			$return_array[$key]['DeliveryLong'] = @$result->DeliveryLatLong[0];

		}
		$result['ProductList'] = $return_array;
		$result['region_charge'] = $result['AreaCharges'];

		$array = [];

		// /$itemHistory
		//$result['result']['updated_data'] = [];
		$updated_data = [];
		if (count($itemHistory) > 0 && isset($result['need_to_pay'])) {
			foreach ($itemHistory as $key) {
				$updated_data[] = ['msg' => "After reviewing your listing, '" . $key['product_name'] . "', the cost was increased by $" . number_format(@$key['update_difference'], 2) . "."];

			}
			$result['updated_data'] = $updated_data;
		}

		$result['need_to_pay_currency'] = '';
		if(isset($result['need_to_pay'])){

			$currency_conversion = $this->currency_conversion($result['need_to_pay']);
			$result['need_to_pay_currency'] = $currency_conversion['ghanian_cost'];
		}

		$result['total_cost_currency'] =  $this->currency_conversion($result['TotalCost'])['ghanian_cost'];
		$result['TotalCost'] = $result['TotalCost'] + $result['after_update_difference'];

		

			

		if (count($cards) > 0) {
			foreach ($cards as $key) {
				$array[] = [
					'last4' => $key['last4'],
					'brand' => $key['brand'],
					'amount' => $key['payment'],
				];
			}
		}
		$result['cards'] = $array;
		$result['RequestDate'] = (String) @$result->EnterOn->sec;

		$response = ['success' => 1, 'result' => $result, 'msg' => 'Local delivery data', 'editResponse' => $result];
		return response()->json($response);die;
	}

	function get_status_title($status, $requesttype = '') {
		switch ($status) {
		case 'ready':
			return 'Ready';
			break;
		case 'trip_pending':
			return 'Pending';
			break;
		case 'trip_ready':
			return 'Ready';
			break;
		case 'out_for_delivery':
			return 'Out for Delivery';
			break;
		case 'out_for_pickup':
			return 'Out for Pickup';
			break;
		case 'accepted':
			return 'Accepted';
			break;
		case 'delivered':
			return 'Delivered';
			break;
		case 'cancel':
			return 'Canceled';
			break;
		case 'cancel_by_admin':
			return 'Cancelled by Admin';
			break;
		case 'purchased':
			return ($requesttype == 'online') ? 'Item Received' : 'Item Purchased';
			break;
		case 'assign':
			return 'Transporter Assigned';
			break;
		case 'paid':
			return ($requesttype == 'online') ? 'Waiting for Item to be Received' : 'Pending Purchase';
			break;
			break;
		case 'reviewed':
			return 'Reviewed';
			break;
		case 'not_purchased':
			return 'Waiting for your payment';
			break;
		case 'item_received':
			return 'Item Received';
			break;
		default:
			return 'Pending';

		}
	}

	/*
     * function for: Update local delivery
     * function created by: Deepak Pateriya
     * Created At: 01 march 2018
     * Updated By:
     * Updated At:
	*/
	public function update_local_delivery_request() {
		$response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		$validate = Validator::make(Input::get(), [
			'user_id' => 'required',
			//'product_list' => 'required',
		]);

		if ($validate->fails()) {
			$validate->errors()->add('field', trans('Oops! Something went wrong'));
			return $validate->errors();
		} else {

			$userInfo = User::where(['_id' => Input::get('user_id')])->first();
			if (count($userInfo) == 0) {
				return response()->json($response);die;
			}

			//$PackageId = $this->get_packageno();
			$city = Input::get('city');
			$state = Input::get('state');
			$country = Input::get('country');

			$drop_off_city = Input::get('drop_off_city');
			$drop_off_state = Input::get('drop_off_state');
			$drop_off_country = Input::get('drop_off_country');

			$return_city = Input::get('return_city');
			$return_state = Input::get('return_state');
			$return_country = Input::get('return_country');

			$nd_return_city = Input::get('nd_return_city');
			$nd_return_state = Input::get('nd_return_state');
			$nd_return_country = Input::get('nd_return_country');

			$calculated_distance = floatval(Input::get('calculated_distance'));
			if ($calculated_distance > 0) {
				$calculated_distance = $calculated_distance * 0.000621371;
			} else {
				$calculated_distance = Utility::getDistanceBetweenPointsNew(
					floatval(Input::get('PickupLat')),
					floatval(Input::get('PickupLong')),
					floatval(Input::get('DeliveryLat')),
					floatval(Input::get('DeliveryLong'))
				);
			}

			$PackageId = Utility::sequence('Request') . time();

			$updateArray = [
				'RequesterId' => new MongoId(Input::get('user_id')),
				'RequesterName' => ucfirst($userInfo->Name),
				'ProductTitle' => '',
				'RequestType' => "local_delivery",
				'Status' => 'pending',
				'PackageId' => $PackageId,
				'device_version' => Input::get('device_version'),
				'app_version' => Input::get('app_version'),
				'device_type' => Input::get('device_type'),
				"PackageNumber" => $PackageId . time(),
				"PickupFullAddress" => $this->formated_address([
					(Input::get('address_line_1') != '') ? Input::get('address_line_1') : '',
					(Input::get('address_line_2') != '') ? Input::get('address_line_2') : '',
					@$city,
					@$state,
					@$country,
				], Input::get('zipcode')),
				'PickupAddress' => Input::get('address_line_1'),
				'PickupAddress2' => Input::get('address_line_2'),
				'PickupCity' => @$city,
				'PickupState' => @$state,
				'PickupCountry' => @$country,
				'PickupPinCode' => Input::get('zipcode'),
				'PickupLatLong' => [floatval(Input::get('PickupLong')), floatval(Input::get('PickupLat'))],
				'PickupDate' => new MongoDate(strtotime(Input::get('pickup_date'))),
				'DeliveryFullAddress' => $this->formated_address([
					(Input::get('drop_off_address_line_1') != '') ? Input::get('drop_off_address_line_1') : '',
					(Input::get('drop_off_address_line_2') != '') ? Input::get('drop_off_address_line_2') : '',
					@$drop_off_city,
					@$drop_off_state,
					@$drop_off_country,
				], Input::get('drop_off_zipcode')),
				'DeliveryAddress' => Input::get('drop_off_address_line_1'),
				'DeliveryAddress2' => Input::get('drop_off_address_line_2'),
				'DeliveryCity' => @$drop_off_city,
				'DeliveryState' => @$drop_off_state,
				'DeliveryCountry' => @$drop_off_country,
				'DeliveryPincode' => Input::get('drop_off_zipcode'),
				"DeliveryLatLong" => [floatval(Input::get('DeliveryLong')), floatval(Input::get('DeliveryLat'))],
				'DeliveryDate' => new MongoDate(strtotime(Input::get('drop_off_date'))),

				"ReturnFullAddress" => $this->formated_address([
					Input::get('return_address_line_1'),
					Input::get('return_address_line_2'),
					@$return_city,
					@$return_state,
					@$return_country,
				], Input::get('return_zipcode')),
				"ReturnAddress" => Input::get('return_address_line_1'),
				"ReturnAddress2" => Input::get('return_address_line_2'),
				"ReturnCityTitle" => @$return_city,
				"ReturnStateTitle" => @$return_state,
				'ReturnCountry' => @$return_country,
				'ReturnPincode' => Input::get('return_zipcode'),
				"FlexibleDeliveryDate" => (Input::get('is_delivery_date_flexible') == 'yes') ? 'yes' : 'no',
				"JournyType" => (Input::get('journey_type') == 'return') ? 'return' : 'one_way',
				'NotDelReturnFullAddress' => $this->formated_address([
					Input::get('nd_return_address_line_1'),
					Input::get('nd_return_address_line_2'),
					@$nd_return_city,
					@$nd_return_state,
					@$nd_return_country,
				], Input::get('nd_return_zipcode')),
				"InCaseNotDelReturnAddress" => Input::get('nd_return_address_line_1'),
				"InCaseNotDelReturnAddress2" => Input::get('nd_return_address_line_2'),
				"InCaseNotDelReturnCity" => @$nd_return_city,
				"InCaseNotDelReturnState" => @$nd_return_state,
				"InCaseNotDelReturnCountry" => @$nd_return_country,
				"InCaseNotDelReturnPincode" => Input::get('nd_return_zipcode'),
				"PackageMaterial" => (Input::get('need_package_material') == 'yes') ? 'yes' : 'no',
				"PackageMaterialShipped" => '',
				"PublicPlace" => (Input::get('public_palce') == 'yes') ? 'yes' : 'no',
				'ReceiverCountrycode' => Input::get('country_code'),
				'ReceiverMobileNo' => Input::get('phone_number'),
				'FlexibleDeliveryDate' => Input::get('is_delivery_date_flexible'),
				"Distance" => $calculated_distance,
				'ShippingCost' => 0,
				'InsuranceCost' => 0,
				'Discount' => 0,
				'TotalCost' => 0,
				'needToPay' => 0,
				'promocode' => Input::get('promocode'),
				'ProductList' => [],
			];

			// if(input::get('product_list')!='')
			//  if (count($res->product) > 0) {

			if (Input::get('products_list') != '') {

				$product = json_decode(Input::get('products_list'));

				$k = 0;
				foreach ($product as $key) {

					$k++;
					$updateArray["ProductTitle"] .= @$key->productName;
					$updateArray['ShippingCost'] += @$key->shippingCost;
					$updateArray['InsuranceCost'] += @$key->InsuranceCost;
					$updateArray['TotalCost'] += @$key->shippingCost+@$key->InsuranceCost;

					$updateArray['ProductList'][] = [
						'_id' => (Input::get('request_id') == '') ? (string) new MongoId() : $key->_id,
						"product_name" => @$key->product_name,
						'package_id' => (Input::get('request_id') == '') ? $updateArray['PackageNumber'] . $k : $key->PackageNumber,
						'status' => (Input::get('request_id') == '') ? 'pending' : $key->status,
						"productWidth" => $key->productWidth,
						"productHeight" => $key->productHeight,
						"productLength" => $key->productLength,
						"productCost" => $key->productCost,
						"productWeight" => $key->productWeight,
						"productHeightUnit" => $key->productHeightUnit,
						"ProductWeightUnit" => $key->productWeightUnit,
						"ProductLengthUnit" => $key->productLengthUnit,
						"productCategory" => $key->category,
						"productCategoryId" => $key->productCategoryId,
						"travelMode" => $key->travelMode,
						"InsuranceStatus" => $key->InsuranceStatus,
						"productQty" => $key->productQty,
						"ProductImage" => $key->ProductImage,
						//"OtherImage" => $key->OtherImage,
						"QuantityStatus" => $key->quantity,
						"BoxQuantity" => $key->productQty,
						"Description" => $key->description,
						"PackageMaterial" => $key->package_material_needed,
						//"PackageMaterialShipped" => $key->PackageMaterialShipped,
						"shippingCost" => @$key->shippingCost,
						'InsuranceCost' => @$key->InsuranceCost,
						"DeliveryVerifyCode" => (Input::get('request_id') == '') ? rand(1000, 9999) : $key->DeliveryVerifyCode,
						'tpid' => '',
						'tpName' => '',
						'inform_mail_sent' => 'no',
						'PaymentStatus' => 'no',
						'total_cost' => @$key->shippingCost+@$key->InsuranceCost,
					];

				}

			}

			$result = (String) Deliveryrequest::where(['_id' => Input::get('local_delivery_request_id')])->update($updateArray);
			if (!$result) {
				$response = ['success' => 0, 'msg' => 'Oops! Something went wrong. during updation'];
				return response()->json($response);die;
			}

			$response = ['success' => 1, 'msg' => 'Local delivery request updated successfully.'];

			return response()->json($response);die;

		}
	}

	/*
		* function for: Common function for get formated address between two address.
		* function created by: Deepak Pateriya
		* Created At: 28 feb 2018
		* Updated By:
		* Updated At:
	*/
	public function formated_address($array, $zip) {
		$address = '';
		foreach ($array as $ky => $val) {
			if (trim($val) != '') {
				if (trim($address) != '') {$address = "$address, ";}
				$address .= $val;
			}
		}
		if (trim($zip) != '') {$address .= "- $zip";}
		return $address;
	}

	public function package_payment() {
		$response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		$validate = Validator::make(Input::get(), [
			'user_id' => 'required',
			'packageId' => 'required',
			'creditCardId' => 'required',
		]);

		if ($validate->fails()) {
			$validate->errors()->add('field', trans('Oops! Something went wrong'));
			return $validate->errors();
		} else {
			$update = [
				'StripeChargeId' => '',
				'Status' => 'ready',
			];
			$where = ['_id' => Input::get('packageId'), 'RequesterId' => new MongoId(Input::get('user_id')), 'Status' => 'pending'];
			$req_data = Deliveryrequest::where($where)->first();

			$user_data = User::where(['_id' => Input::get('user_id')])->first();
			if (count($req_data) == 0 || count($user_data) == 0) {
				
				return response()->json($response);die;
			}

			$res = Payment::capture($user_data, $req_data->TotalCost, trim(Input::get('creditCardId')), true);

			if (isset($res['id'])) {
				$update['StripeChargeId'] = $res['id'];
				Transaction::insert(array(
					"SendById" => (String) $user_data->_id,
					"SendByName" => @$user_data->RequesterName,
					"SendToId" => "aquantuo",
					"RecieveByName" => "Aquantuo",
					"Description" => "Amount deposited against delivery request for {$req_data->ProductTitle}, PackageId: {$req_data->PackageNumber} from {$req_data->PickupFullAddress} to {$req_data->DeliveryFullAddress}",
					"Credit" => floatval($req_data->TotalCost),
					"Debit" => "",
					"Status" => "complete",
					"TransactionType" => "delivery_request",
					"EnterOn" => new MongoDate(),
				));

				$p_array = $req_data->ProductList;
				foreach ($p_array as $key => $value) {
					$p_array[$key]['status'] = 'ready';
					$p_array[$key]['PaymentStatus'] = 'yes';
				}
				$update['ProductList'] = $p_array;

				if (Deliveryrequest::where($where)->update($update)) {
					Notification::Insert([
						array(
							"NotificationTitle" => trans('lang.SEND_CREATE_TITLE'),
							"NotificationMessage" => sprintf(trans('lang.SEND_CREATE_MSG'), $req_data->ProductTitle, $req_data->PackageNumber),
							"NotificationUserId" => array(),
							"NotificationReadStatus" => 0,
							"location" => "request_detail",
							"locationkey" => (string) Input::get('packageId'),
							"Date" => new MongoDate(),
							"GroupTo" => "Admin",
						),
					]);

					RequestCard::Insert([
						'request_id' => Input::get('packageId'),
						'item_id' => '',
						'card_id' => trim(Input::get('creditCardId')),
						'type' => 'request',
						'payment' => $req_data->TotalCost,
						'brand' => @$res['source']['brand'],
						'last4' => @$res['source']['last4'],
						'description' => "Payment done for request Title: '" . $req_data->ProductTitle . "' Package Id:" . $req_data->PackageNumber . ".",
						"EnterOn" => new MongoDate(),
					]);

					if ($user_data->EmailStatus == "on") {
						send_mail('5694a10f5509251cd67773eb', [
							"to" => $user_data->Email,
							"replace" => [
								"[USERNAME]" => $user_data->Name,
								"[PACKAGETITLE]" => ucfirst($req_data->ProductTitle),
								"[PACKAGEID]" => $req_data->PackageId,
								"[SOURCE]" => $req_data->PickupFullAddress,
								"[DESTINATION]" => $req_data->DeliveryFullAddress,
								"[PACKAGENUMBER]" => $req_data->PackageNumber,
								"[SHIPPINGCOST]" => $req_data->ShippingCost,
								"[TOTALCOST]" => $req_data->TotalCost,
							],
						]);
					}

					$setting = Setting::find('563b0e31e4b03271a097e1ca');
					if (count($setting) > 0) {

						send_mail('56cd3e1f5509251cd677740c', [
							"to" => trim($setting->SupportEmail),
							"replace" => [
								"[USERNAME]" => $user_data->Name,
								"[PACKAGETITLE]" => ucfirst($req_data->ProductTitle),
								"[PACKAGEID]" => $req_data->PackageId,
								"[SOURCE]" => $req_data->PickupFullAddress,
								"[DESTINATION]" => $req_data->DeliveryFullAddress,
								"[PACKAGENUMBER]" => $req_data->PackageNumber,
								"[SHIPPINGCOST]" => $req_data->ShippingCost,
								"[TOTALCOST]" => $req_data->TotalCost,
								"[REQUEST]" => 'Delivery',
							],
						]);

					}

					Reqhelper::update_status2($req_data->_id);

					/*  Activity Log for local delivery payment */
					$productList = $req_data->ProductList;
					foreach ($productList as $key => $val) {
						$insertactivity = [
							'request_id' => $req_data->_id,
							'request_type' => 'local_delivery',
							'PackageNumber' => $req_data->PackageNumber,
							'item_id' => $val['_id'],
							'package_id' => $val['package_id'],
							'item_name' => $val['product_name'],
							'log_type' => 'request',
							'message' => 'Payment successful.',
							'status' => 'ready',
							'EnterOn' => new MongoDate(),
						];
						Activitylog::insert($insertactivity);
					}
					$response = ['success' => 1, 'msg' => 'Payment has been successfully completed.'];
				}
			} else {
				$response = ['success' => 0, 'msg' => $res];
			}
			return response()->json($response);die;
		}
	}
	/*
	 * After review payment local payment 
	 * 08 Feb 2020
	 */
	 public function remaining_payment() {
		$response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		$validate = Validator::make(Input::get(), [
			'user_id' => 'required',
			'packageId' => 'required',
			'creditCardId' => 'required',
		]);

		if ($validate->fails()) {
			$validate->errors()->add('field', trans('Oops! Something went wrong'));
			return $validate->errors();
		} else {
			$update = [
				'StripeChargeId' => '',
				'Status' => 'ready',
			];
			$where = ['_id' => Input::get('packageId'), 'RequesterId' => new MongoId(Input::get('user_id'))];
			$req_data = Deliveryrequest::where($where)->first();

			$user_data = User::where(['_id' => Input::get('user_id')])->first();
			if (count($req_data) == 0 || count($user_data) == 0) {
				
				return response()->json($response);die;
			}
			$res = Payment::capture($user_data, $req_data->need_to_pay, trim(Input::get('creditCardId')), true);

			if (isset($res['id'])) {
				$update['StripeChargeId'] = $res['id'];
				Transaction::insert(array(
					"SendById" => (String) $user_data->_id,
					"SendByName" => @$user_data->RequesterName,
					"SendToId" => "aquantuo",
					"RecieveByName" => "Aquantuo",
					"Description" => "Amount deposited against delivery request for {$req_data->ProductTitle}, PackageId: {$req_data->PackageNumber} from {$req_data->PickupFullAddress} to {$req_data->DeliveryFullAddress}",
					"Credit" => floatval($req_data->need_to_pay),
					"Debit" => "",
					"Status" => "complete",
					"TransactionType" => "delivery_request",
					"EnterOn" => new MongoDate(),
				));

				
				$update['need_to_pay'] = 0;

				if (Deliveryrequest::where($where)->update($update)) {
					Notification::Insert([
						array(
							"NotificationTitle" => trans('lang.SEND_CREATE_TITLE'),
							"NotificationMessage" => sprintf(trans('lang.SEND_CREATE_MSG'), $req_data->ProductTitle, $req_data->PackageNumber),
							"NotificationUserId" => array(),
							"NotificationReadStatus" => 0,
							"location" => "request_detail",
							"locationkey" => (string) Input::get('packageId'),
							"Date" => new MongoDate(),
							"GroupTo" => "Admin",
						),
					]);

					RequestCard::Insert([
						'request_id' => Input::get('packageId'),
						'item_id' => '',
						'card_id' => trim(Input::get('creditCardId')),
						'type' => 'request',
						'payment' => $req_data->need_to_pay,
						'brand' => @$res['source']['brand'],
						'last4' => @$res['source']['last4'],
						'description' => "Payment done for request Title: '" . $req_data->ProductTitle . "' Package Id:" . $req_data->PackageNumber . ".",
						"EnterOn" => new MongoDate(),
					]);

					

					

					/*  Activity Log for local delivery payment */
					$productList = $req_data->ProductList;
					foreach ($productList as $key => $val) {
						$insertactivity = [
							'request_id' => $req_data->_id,
							'request_type' => 'local_delivery',
							'PackageNumber' => $req_data->PackageNumber,
							'item_id' => $val['_id'],
							'package_id' => $val['package_id'],
							'item_name' => $val['product_name'],
							'log_type' => 'request',
							'message' => 'Payment successful.',
							'status' => 'ready',
							'EnterOn' => new MongoDate(),
						];
						Activitylog::insert($insertactivity);
					}
					$response = ['success' => 1, 'msg' => 'Payment has been successfully completed.'];
				}
			} else {
				$response = ['success' => 0, 'msg' => $res];
			}
			return response()->json($response);die;
		}
	}

	public function myDeliveries() {
		$response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		$validate = Validator::make(Input::get(), [
			'user_id' => 'required',
			'type' => 'required',
			'pageno' => 'required',
		]);

		if ($validate->fails()) {
			$validate->errors()->add('field', trans('Oops! Something went wrong'));
			return $validate->errors();
		} else {
			$query = Deliveryrequest::query();
			if (Input::get('type') == 'current') {
				$status = ['ready', 'accepted', 'out_for_pickup', 'out_for_delivery', 'assign'];
			} else {
				$status = ['delivered', 'cancel'];
			}

			$query->where('ProductList.tpid', Input::get('user_id'))
				->orwhere('TransporterId', new MongoId(Input::get('user_id')));

			$limit = 20;
			$start = (Input::get('pageno') > 1) ? ($limit * (Input::get('pageno') - 1)) : 0;
			$query->skip($start)->limit($limit);
			$result = $query->orderby('_id', 'desc')->get();

			foreach ($result as $val) {
				$RequesterId[] = $val['RequesterId'];
			}

			if (isset($RequesterId)) {
				$Requesterinfo = User::where(['_id' => ['$in' => $RequesterId]])->get();
				if ($Requesterinfo->count() > 0) {
					foreach ($Requesterinfo as $key) {
						$Requesterdata[(string) $key['_id']] = (Object) $key;
					}
				}
			}

			$return_array = [];

			foreach ($result as $key) {
				$data = $this->userInfoFunc($Requesterinfo, $key['RequesterId']);
				$ChatName = $data['Name'];
				$ChatUsrImage = $data['image'];

				if (isset($key->ProductList)) {
					foreach ($key->ProductList as $value) {
						$image = "";
						if ($value['tpid'] == Input::get('user_id')) {
							if (isset($value['image'])) {
								$image = $value['image'];
							} else {
								if (isset($value['ProductImage'])) {
									if ($value['ProductImage'] != '') {
										$image = $value['ProductImage'];
									}
								} 
							}

							$temp_array = [
								'request_id' => $key->_id,
								'_id' => $value['_id'],
								'Itemid' => $value['_id'],
								'package_id' => $value['package_id'],
								'status' => $value['status'],
								'show_status' => get_status_title($value['status'], $key['RequestType'])['status'],
								'tpid' => $value['tpid'],
								'RequestLocation' => $key['PickupCity'] . " to " . $key['DeliveryCity'],
								'tpName' => $value['tpName'],
								'product_name' => $value['product_name'],
								'image' => $image,
								'TripId' => "",
								'package_type' => $key['RequestType'],
								'item_cost' => @$value['productCost'],
								'PickupLat' => @$key['PickupLatLong'][1],
								'PickupLong' => @$key['PickupLatLong'][0],
								'DeliveryLat' => @$key['DeliveryLatLong'][1],
								'DeliveryLong' => @$key['DeliveryLatLong'][0],
								'ChatUserId' => (String) $key->RequesterId,
								'ChatUserName' => $ChatName,
								'ChatUsrImage' => @$ChatUsrImage,
								'Date' => $key['EnterOn']->sec,
								'request_version'=> @$key['request_version'],
							];
							
							if (Input::get('type') == 'current') {
								if (in_array($value['status'], ['ready', 'accepted', 'out_for_pickup', 'out_for_delivery', 'assign'])) {
									$return_array[] = $temp_array;
								}
							} else {
								if (in_array($value['status'], ['delivered', 'cancel'])) {
									$return_array[] = $temp_array;
								}
							}
						}
					}
				} else {
					$temp_array = [
						'request_id' => $key->_id,
						'_id' => $key['_id'],
						'package_id' => $key['PackageNumber'],
						'status' => $key['Status'],
						'show_status' => get_status_title($key['Status'], $key['RequestType'])['status'],
						'tpid' => $key['TransporterId'],
						'RequestLocation' => $key['PickupCity'] . " to " . $key['DeliveryCity'],
						'tpName' => $key['TransporterName'],
						'product_name' => $key['ProductTitle'],
						'image' => @$key['ProductImage'],
						'TripId' => "",
						'package_type' => $key['RequestType'],
						'request_id' => $key->_id,
						'Date' => $key['EnterOn']->sec,
						'PickupLat' => @$key['PickupLatLong'][1],
						'PickupLong' => @$key['PickupLatLong'][0],
						'DeliveryLat' => @$key['DeliveryLatLong'][1],
						'DeliveryLong' => @$key['DeliveryLatLong'][0],
						'ChatUserId' => (String) $key->RequesterId,
						'ChatUserName' => $ChatName,
						'ChatUsrImage' => @$ChatUsrImage,
						'request_version'=> @$key['request_version'],
					];

					if (Input::get('type') == 'current') {
						if (in_array($key['Status'], ['ready', 'accepted', 'out_for_pickup', 'out_for_delivery', 'assign'])) {
							$return_array[] = $temp_array;
						}
					} else {
						if (in_array($key['Status'], ['delivered', 'cancel'])) {
							$return_array[] = $temp_array;
						}
					}
				}
			}
			
			if (count($return_array) > 0) {
				$response = [ 'success' => 1, 'msg' => 'Deliveries list', 'result' => $return_array ];
			} else {
				$response = [ 'success' => 0, 'msg' => 'Record not found.', 'result' => $return_array ];
			}
			
			return response()->json($response);die;
		}
	}

	public function userInfoFunc($array, $userid) {
		$data = ['Name' => '', 'image' => ''];
		foreach ($array as $key) {
			if ($key['_id'] == $userid) {
				$data['Name'] = $key['Name'];
				$data['image'] = $key['Image'];
			}
		}
		return $data;
	}

	public function demo() {
		Deliveryrequest::where('_id', '=', Input::get('request_id'))->get();
	}

}
