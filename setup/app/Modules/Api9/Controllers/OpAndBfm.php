<?php namespace App\Modules\Api9\Controllers;

use App;
use App\Http\Controllers\Controller;
use App\Http\Models\Activitylog;
use App\Http\Models\Category;
use App\Http\Models\Configuration;
use App\Http\Models\Currency;
use App\Http\Models\Deliveryrequest;
use App\Http\Models\Distance;
use App\Http\Models\Extraregion;
use App\Http\Models\Notification;
use App\Http\Models\RequestCard;
use App\Http\Models\Setting;
use App\Http\Models\Transaction;
use App\Http\Models\User;
use App\Library\QueueEmail;
use App\Library\Reqhelper;
use App\Library\Utility;
use App\Modules\User\Controllers\Payment;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use MongoDate;
use MongoId;
use App\Http\Models\Itemhistory;

class OpAndBfm extends Controller {

	public function online_purchase_calculation() {
        $response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
        $validate = Validator::make(Input::get(), [
			'products_list' => 'required',
			'distance' => 'required',
			'drop_state_id' => 'required',
		]);
		if ($validate->fails()) {
			$validate->errors()->add('field', trans('Oops! Something went wrong'));
			return $validate->errors();
		} else {
			
		}
        
        return json_encode($response);

    }

}