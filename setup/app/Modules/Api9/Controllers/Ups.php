<?php namespace App\Modules\Api9\Controllers;

use App;
use App\Http\Controllers\Controller;
use App\Http\Models\Activitylog;
use App\Http\Models\Category;
use App\Http\Models\Configuration;
use App\Http\Models\Currency;
use App\Http\Models\Deliveryrequest;
use App\Http\Models\Distance;
use App\Http\Models\Extraregion;
use App\Http\Models\Notification;
use App\Http\Models\RequestCard;
use App\Http\Models\Setting;
use App\Http\Models\Transaction;
use App\Http\Models\User;
use App\Library\QueueEmail;
use App\Library\Reqhelper;
use App\Library\Utility;
use App\Modules\User\Controllers\Payment;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use MongoDate;
use MongoId;
use App\Http\Models\Itemhistory;
use UpsApi;

class Ups extends Controller {

	public function testUps(){
		$address = new \Ups\Entity\Address();
		$rate = new \Ups\Rate(
			$accessKey, //accessKey
			"kapilideal", //userId
			"ideal@Indore123" //password
		);
		try {
		    $shipment = new \Ups\Entity\Shipment();

		    $shipperAddress = $shipment->getShipper()->getAddress();
		    $shipperAddress->setPostalCode('99205');

		    $address = new \Ups\Entity\Address();
		    $address->setPostalCode('99205');
		    $shipFrom = new \Ups\Entity\ShipFrom();
		    $shipFrom->setAddress($address);

		    $shipment->setShipFrom($shipFrom);

		    $shipTo = $shipment->getShipTo();
		    $shipTo->setCompanyName('Test Ship To');
		    $shipToAddress = $shipTo->getAddress();
		    $shipToAddress->setPostalCode('99205');

		    $package = new \Ups\Entity\Package();
		    $package->getPackagingType()->setCode(\Ups\Entity\PackagingType::PT_PACKAGE);
		    $package->getPackageWeight()->setWeight(10);
		    
		    // if you need this (depends of the shipper country)
		    $weightUnit = new \Ups\Entity\UnitOfMeasurement;
		    $weightUnit->setCode(\Ups\Entity\UnitOfMeasurement::UOM_KGS);
		    $package->getPackageWeight()->setUnitOfMeasurement($weightUnit);

		    $dimensions = new \Ups\Entity\Dimensions();
		    $dimensions->setHeight(10);
		    $dimensions->setWidth(10);
		    $dimensions->setLength(10);

		    $unit = new \Ups\Entity\UnitOfMeasurement;
		    $unit->setCode(\Ups\Entity\UnitOfMeasurement::UOM_IN);

		    $dimensions->setUnitOfMeasurement($unit);
		    $package->setDimensions($dimensions);

		    $shipment->addPackage($package);

		    var_dump($rate->getRate($shipment));
		} catch (Exception $e) {
		    var_dump($e);
		}

	}

}