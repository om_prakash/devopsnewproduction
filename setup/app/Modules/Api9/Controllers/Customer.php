<?php namespace App\Modules\Api9\Controllers;

use App;
use App\Http\Controllers\Controller;
use App\Http\Models\Deliveryrequest;
use App\Http\Models\RequestCard;

use App\Http\Models\Configuration;
use App\Http\Models\Transaction;
use App\Http\Models\User;
use App\Library\Reqhelper;
use App\Library\Utility;
use App\Modules\User\Controllers\Payment;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Mail,Request;
use MongoDate;
use MongoId;
use App\Http\Models\Setting;
use App\Modules\User\Controllers\NewCalculation;
use App\Http\Models\Extraregion;
use App\Http\Models\Currency;
use App\Library\NewRequesthelper;
use App\Http\Models\Category;
use App\Http\Models\Distance;
use App\Http\Models\Address;

use App\Library\upsRate;
use App\Http\Models\CityStateCountry;

class Customer extends Controller
{

    function testFunction(Request $request) {
        $response['success'] = 0;
        $supportemail = Setting::find('563b0e31e4b03271a097e1ca');

        $insert['AqAddress'] = $supportemail->AqAddress;
        $insert['AqCity'] = $supportemail->AqCity;
        $insert['AqState'] = $supportemail->AqState;
        $insert['AqZipcode'] = $supportemail->AqZipcode;
        $insert['AqCountry'] = $supportemail->AqCountry;
        $insert['AqLatLong'] = $supportemail->AqLatlong;
        $insert['Aqphone'] = $supportemail->Aqphone;
        $insert['Aqcc'] = $supportemail->Aqcc;

        $users = User::get();
        if(count($users) > 0){
            foreach ($users as $key) {
                User::where(['_id'=>$key->_id])->update($insert);
            }
        }
                
        return response()->json($response);
    }

    public function create_send_package()
    {
        $response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
        $data['cards'] = Payment::card_list('cus_CKhCqcqz0hK19B');
        return response()->json(['result' => $data['cards']]);
    }

    public function item_payment()
    {
        $response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
        $validate = Validator::make(Input::get(), [
            'requestid' => 'required',
            'card_id' => 'required',
            'type' => 'required',
            'user_id' => 'required',
        ]);
        if ($validate->fails()) {
            $validate->errors()->add('field', trans('Oops! Something went wrong'));
            return $validate->errors();
        } else {
            $userinfo = User::find(Input::get('user_id'));
            $where = [
                '_id' => trim(Input::get('requestid')),
                'RequesterId' => new MongoId(Input::get('user_id')),
                'RequestType' => trim(Input::get('type')),
            ];
            $req_data = Deliveryrequest::where($where)->first();
            if (count($req_data) > 0 && count($userinfo) > 0) {
                $productName = $req_data->ProductTitle;
                $package_id = $req_data->PackageNumber; 
                $cost = $req_data->need_to_pay;
                $res = Payment::capture($userinfo, (($cost)), trim(Input::get('card_id')), true);

                if (isset($res['id'])) {
                    $up = Deliveryrequest::where($where)->update(['need_to_pay' => 0]);
                    Transaction::insert(array(
                        "SendById" => (String) @$userinfo->_id,
                        "SendByName" => @$userinfo->Name,
                        "SendToId" => "aquantuo",
                        "RecieveByName" => "Aquantuo",
                        "Description" => "Amount deposited against product '{$productName}', PackageId: {$package_id}",
                        "Credit" => floatval($cost),
                        "Debit" => "",
                        "Status" => "",
                        "TransactionType" => $req_data->RequestType,
                        'request_id'=> trim(Input::get('requestid')),
                        'item_id'=> trim(Input::get('itemid')),
                        "EnterOn" => new MongoDate(),
                    ));

                    RequestCard::Insert([
                        'request_id' => trim(Input::get('requestid')),
                        'item_id' => trim(Input::get('itemid')),
                        'card_id' => trim(Input::get('card_id')),
                        'type' => 'request',
                        'payment' => $cost,
                        'brand' => @$res['source']['brand'],
                        'last4' => @$res['source']['last4'],
                        'description' => "Payment done for item Title: '" . $productName . "' Item Id:" . $package_id . ".",
                        "EnterOn" => new MongoDate(),
                    ]);

                    //$req_data->RequestType
                    if($req_data->RequestType == 'local_delivery'){
                        Reqhelper::update_status2(trim(Input::get('requestid')));
                    }else{
                        Reqhelper::update_status(trim(Input::get('requestid')));
                    }

                    
                    //$status_update = Deliveryrequest::where($where)->update(['Status' => $res]);
                    $response = ['success' => 1, 'msg' => 'Payment has been successful.'];
                    $response['msg'] = "Success! Payment for package '" .$productName."' has been successfully completed.Aquantuo will proceed with next steps immediately. ";
                } else {
                    $response['msg'] = $res;
                }
            }
        }
        return response()->json($response);
    }


    /*public function online_payment()
    {
        $response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
        $validate = Validator::make(Input::get(), [
            'packageId' => 'required',
            'creditCardId' => 'required',
            'user_id' => 'required',
        ]);

        if ($validate->fails()) {
            $validate->errors()->add('field', trans('Oops! Something went wrong'));
            return $validate->errors();
        }else{
            $userinfo = User::find(Input::get('user_id'));
            $where = [
                '_id' => trim(Input::get('packageId')),
                'RequesterId' => new MongoId(Input::get('user_id')),
                'RequestType' => 'online',
                'Status'=>'pending'
                
            ];
            $req_data = Deliveryrequest::where($where)->first(); 
            if (count($req_data) > 0 && count($userinfo) > 0) {

            }
        } 
    }*/

    public function test_mail()
    {
        $time_start = microtime(true);
        $data['name'] = "<b>Hello</b>Ajay,<br/>Test this queue-19 " . date('Y-m-d h:i A');
        $message = "hello";
        Mail::send('email.welcome', $data, function ($message) {
            $message->subject("hello");
            $message->to('ajay@idealittechno.com');
        });
        $time_end = microtime(true);
        $time = $time_end - $time_start;
        echo "Process Time: {$time}";
    }
    /*
     * function for: Create local delivery
     * function created by: Deepak Pateriya
     * Created At: 28 feb 2018
     * Updated By:
     * Updated At:
     */
    public function create_local_delivery_request(){
        $response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
        $validate = Validator::make(Input::get(), [
            'user_id' => 'required',
            'product_list' => 'required',
        ]);

        if ($validate->fails()) {
            $validate->errors()->add('field', trans('Oops! Something went wrong'));
            return $validate->errors();
        } else {

            $userInfo = User::where(['_id' => Input::get('user_id')])->first();
            if (count($userInfo) == 0) {
                return response()->json($response);die;
            }

            //$PackageId = $this->get_packageno();
            $city = trim(Input::get('city'));
            $state = trim(Input::get('state'));
            $country = trim(Input::get('country'));

            $drop_off_city = trim(Input::get('drop_off_city'));
            $drop_off_state = trim(Input::get('drop_off_state'));
            $drop_off_country = trim(Input::get('drop_off_country'));

            $return_city = trim(Input::get('return_city'));
            $return_state = trim(Input::get('return_state'));
            $return_country = trim(Input::get('return_country'));

            $nd_return_city = trim(Input::get('nd_return_city'));
            $nd_return_state = trim(Input::get('nd_return_state'));
            $nd_return_country = trim(Input::get('nd_return_country'));

            $calculated_distance = floatval(Input::get('calculated_distance'));
            if ($calculated_distance > 0) {
                $calculated_distance = $calculated_distance * 0.000621371;
            } else {
                $calculated_distance = Utility::getDistanceBetweenPointsNew(
                    floatval(Input::get('PickupLat')),
                    floatval(Input::get('PickupLong')),
                    floatval(Input::get('DeliveryLat')),
                    floatval(Input::get('DeliveryLong'))
                );
            }

            $PackageId = Utility::sequence('Request') . time();

            $insertArray = [
                'RequesterId' => new MongoId(Input::get('user_id')),
                'RequesterName' => ucfirst($userInfo->Name),
                'ProductTitle' => '',
                'RequestType' => "local_delivery",
                'Status' => 'pending',
                'PackageId' => $PackageId,
                'device_version' => Input::get('device_version'),
                'app_version' => Input::get('app_version'),
                'device_type' => Input::get('device_type'),
                "PackageNumber" => $PackageId . time(),
                "PickupFullAddress" => $this->formated_address([
                    (trim(Input::get('address_line_1')) != '') ? trim(Input::get('address_line_1')) : '',
                    (trim(Input::get('address_line_2') != '')) ? trim(Input::get('address_line_2')) : '',
                    @$city,
                    @$state,
                    @$country,
                ], Input::get('zipcode')),
                'PickupAddress' => trim(Input::get('address_line_1')),
                'PickupAddress2' => trim(Input::get('address_line_2')),
                'PickupCity' => @$city,
                'PickupState' => @$state,
                'PickupCountry' => @$country,
                'PickupPinCode' => trim(Input::get('zipcode')),
                'PickupLatLong' => [floatval(Input::get('PickupLong')), floatval(Input::get('PickupLat'))],
                'PickupDate' => new MongoDate(strtotime(Input::get('pickup_date'))),
                'DeliveryFullAddress' => $this->formated_address([
                    (trim(Input::get('drop_off_address_line_1')) != '') ? trim(Input::get('drop_off_address_line_1')) : '',
                    (trim(Input::get('drop_off_address_line_2')) != '') ? trim(Input::get('drop_off_address_line_2')) : '',
                    @$drop_off_city,
                    @$drop_off_state,
                    @$drop_off_country,
                ], Input::get('drop_off_zipcode')),
                'DeliveryAddress' => trim(Input::get('drop_off_address_line_1')),
                'DeliveryAddress2' => trim(Input::get('drop_off_address_line_2')),
                'DeliveryCity' => @$drop_off_city,
                'DeliveryState' => @$drop_off_state,
                'DeliveryCountry' => @$drop_off_country,
                'DeliveryPincode' => trim(Input::get('drop_off_zipcode')),
                "DeliveryLatLong" => [floatval(Input::get('DeliveryLong')), floatval(Input::get('DeliveryLat'))],
                'DeliveryDate' => new MongoDate(strtotime(Input::get('drop_off_date'))),

                "ReturnFullAddress" => $this->formated_address([
                    Input::get('return_address_line_1'),
                    Input::get('return_address_line_2'),
                    @$return_city,
                    @$return_state,
                    @$return_country,
                ], Input::get('return_zipcode')),
                "ReturnAddress" => trim(Input::get('return_address_line_1')),
                "ReturnAddress2" => trim(Input::get('return_address_line_2')),
                "ReturnCityTitle" => @$return_city,
                "ReturnStateTitle" => @$return_state,
                'ReturnCountry' => @$return_country,
                'ReturnPincode' => trim(Input::get('return_zipcode')),
                "FlexibleDeliveryDate" => (Input::get('is_delivery_date_flexible') == 'yes') ? 'yes' : 'no',
                "JournyType" => (Input::get('journey_type') == 'return') ? 'return' : 'one_way',
                'NotDelReturnFullAddress' => $this->formated_address([
                    trim(Input::get('nd_return_address_line_1')),
                    trim(Input::get('nd_return_address_line_2')),
                    @$nd_return_city,
                    @$nd_return_state,
                    @$nd_return_country,
                ], Input::get('nd_return_zipcode')),
                "InCaseNotDelReturnAddress" => trim(Input::get('nd_return_address_line_1')),
                "InCaseNotDelReturnAddress2" => trim(Input::get('nd_return_address_line_2')),
                "InCaseNotDelReturnCity" => @$nd_return_city,
                "InCaseNotDelReturnState" => @$nd_return_state,
                "InCaseNotDelReturnCountry" => @$nd_return_country,
                "InCaseNotDelReturnPincode" => trim(Input::get('nd_return_zipcode')),
                "PackageMaterial" => (Input::get('need_package_material') == 'yes') ? 'yes' : 'no',
                "PackageMaterialShipped" => '',
                "PublicPlace" => (Input::get('public_palce') == 'yes') ? 'yes' : 'no',
                'ReceiverCountrycode' => trim(Input::get('country_code')),
                'ReceiverMobileNo' => trim(Input::get('phone_number')),
                'FlexibleDeliveryDate' => Input::get('is_delivery_date_flexible'),
                "Distance" => $calculated_distance,
                'ShippingCost' => trim(Input::get('ShippingCost')),
                'InsuranceCost' => trim(Input::get('InsuranceCost')),
                'TotalCost' => trim(Input::get('TotalCost')),
                'needToPay' => trim(Input::get('needToPay')),
                'promocode' => trim(Input::get('promocode')),
                'Discount' => trim(Input::get('Discount')),
                'region_charge' => trim(Input::get('RegionCost')),
                'ProductList' => [],
            ];

            // if(input::get('product_list')!='')
            //  if (count($res->product) > 0) {

            if (Input::get('product_list') != '') {

                $product = json_decode(Input::get('product_list'));

                $k = 0;
                foreach ($product as $key) {

                    $k++;
                    $insertArray["ProductTitle"] .= @$key->productName;
                    $insertArray['ShippingCost'] += @$key->shippingCost;
                    $insertArray['InsuranceCost'] += @$key->InsuranceCost;
                    $insertArray['TotalCost'] += @$key->shippingCost+@$key->InsuranceCost;

                    $insertArray['ProductList'][] = [
                        '_id' => (Input::get('request_id') == '') ? (string) new MongoId() : $key->_id,
                        "productName" => @$key->productName,
                        'package_id' => (Input::get('request_id') == '') ? $insertArray['PackageNumber'] . $k : $key->PackageNumber,
                        'status' => (Input::get('request_id') == '') ? 'pending' : $key->status,
                        "productWidth" => $key->productWidth,
                        "productHeight" => $key->productHeight,
                        "productLength" => $key->productLength,
                        "productCost" => $key->productCost,
                        "productWeight" => $key->productWeight,
                        "productHeightUnit" => $key->productHeightUnit,
                        "ProductWeightUnit" => $key->productWeightUnit,
                        "ProductLengthUnit" => $key->productLengthUnit,
                        "productCategory" => $key->category,
                        "productCategoryId" => $key->productCategoryId,
                        "travelMode" => $key->travelMode,
                        "InsuranceStatus" => $key->InsuranceStatus,
                        "productQty" => $key->productQty,
                        "ProductImage" => $key->ProductImage,
                        //"OtherImage" => $key->OtherImage,
                        "QuantityStatus" => $key->quantity,
                        "BoxQuantity" => $key->productQty,
                        "Description" => $key->description,
                        "PackageMaterial" => $key->package_material_needed,
                        //"PackageMaterialShipped" => $key->PackageMaterialShipped,
                        "shippingCost" => @$key->shippingCost,
                        'InsuranceCost' => @$key->InsuranceCost,
                        "DeliveryVerifyCode" => (Input::get('request_id') == '') ? rand(1000, 9999) : $key->DeliveryVerifyCode,
                        'tpid' => '',
                        'tpName' => '',
                        'inform_mail_sent' => 'no',
                        'PaymentStatus' => 'no',
                        'total_cost' => @$key->shippingCost+@$key->InsuranceCost,
                    ];

                }

            }

            //print_r($insertArray);die;

            $record = (String) Deliveryrequest::insertGetId($insertArray);
            if (!$record) {
                $response = ['success' => 0, 'msg' => 'Oops! Something went wrong. during insertion'];
                return response()->json($response);die;
            }

            $response = ['success' => 1, 'result' => ['local_delivery_request_id' => $record], 'msg' => 'Local delivery request created successfully.'];

            return response()->json($response);die;

        }
    }
    /*
     * function for: Edit local delivery
     * function created by: Deepak Pateriya
     * Created At: 01 march 2018
     * Updated By:
     * Updated At:
    */
    public function edit_local_delivery_request(){
        $response = ['success' => 0, 'msg' => 'Record not found.'];

        $result = Deliveryrequest::where(['_id' => Input::get('local_delivery_request_id'), 
            'RequestType' => 'local_delivery'])->first();
        if (!$result) {
            return response()->json($response);die;
        }
        $response = ['success' => 1, 'result' => $result, 'msg' => 'Local delivery data'];
        return response()->json($response);die;
    }
    /*
    * function for: Update local delivery
    * function created by: Deepak Pateriya
    * Created At: 01 march 2018
    * Updated By:
    * Updated At:
    */
    public function update_local_delivery_request(){
        $response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
        $validate = Validator::make(Input::get(), [
            'user_id' => 'required',
            //'product_list' => 'required',
        ]);

        if ($validate->fails()) {
            $validate->errors()->add('field', trans('Oops! Something went wrong'));
            return $validate->errors();
        } else {

            $userInfo = User::where(['_id' => Input::get('user_id')])->first();
            if (count($userInfo) == 0) {
                return response()->json($response);die;
            }

            //$PackageId = $this->get_packageno();
            $city = Input::get('city');
            $state = Input::get('state');
            $country = Input::get('country');

            $drop_off_city = Input::get('drop_off_city');
            $drop_off_state = Input::get('drop_off_state');
            $drop_off_country = Input::get('drop_off_country');

            $return_city = Input::get('return_city');
            $return_state = Input::get('return_state');
            $return_country = Input::get('return_country');

            $nd_return_city = Input::get('nd_return_city');
            $nd_return_state = Input::get('nd_return_state');
            $nd_return_country = Input::get('nd_return_country');

            $calculated_distance = floatval(Input::get('calculated_distance'));
            if ($calculated_distance > 0) {
                $calculated_distance = $calculated_distance * 0.000621371;
            } else {
                $calculated_distance = Utility::getDistanceBetweenPointsNew(
                    floatval(Input::get('PickupLat')),
                    floatval(Input::get('PickupLong')),
                    floatval(Input::get('DeliveryLat')),
                    floatval(Input::get('DeliveryLong'))
                );
            }

            $PackageId = Utility::sequence('Request') . time();

            $updateArray = [
                'RequesterId' => new MongoId(Input::get('user_id')),
                'RequesterName' => ucfirst($userInfo->Name),
                'ProductTitle' => '',
                'RequestType' => "local_delivery",
                'Status' => 'pending',
                'PackageId' => $PackageId,
                'device_version' => Input::get('device_version'),
                'app_version' => Input::get('app_version'),
                'device_type' => Input::get('device_type'),
                "PackageNumber" => $PackageId . time(),
                "PickupFullAddress" => $this->formated_address([
                    (Input::get('address_line_1') != '') ? Input::get('address_line_1') : '',
                    (Input::get('address_line_2') != '') ? Input::get('address_line_2') : '',
                    @$city,
                    @$state,
                    @$country,
                ], Input::get('zipcode')),
                'PickupAddress' => Input::get('address_line_1'),
                'PickupAddress2' => Input::get('address_line_2'),
                'PickupCity' => @$city,
                'PickupState' => @$state,
                'PickupCountry' => @$country,
                'PickupPinCode' => Input::get('zipcode'),
                'PickupLatLong' => [floatval(Input::get('PickupLong')), floatval(Input::get('PickupLat'))],
                'PickupDate' => new MongoDate(strtotime(Input::get('pickup_date'))),
                'DeliveryFullAddress' => $this->formated_address([
                    (Input::get('drop_off_address_line_1') != '') ? Input::get('drop_off_address_line_1') : '',
                    (Input::get('drop_off_address_line_2') != '') ? Input::get('drop_off_address_line_2') : '',
                    @$drop_off_city,
                    @$drop_off_state,
                    @$drop_off_country,
                ], Input::get('drop_off_zipcode')),
                'DeliveryAddress' => Input::get('drop_off_address_line_1'),
                'DeliveryAddress2' => Input::get('drop_off_address_line_2'),
                'DeliveryCity' => @$drop_off_city,
                'DeliveryState' => @$drop_off_state,
                'DeliveryCountry' => @$drop_off_country,
                'DeliveryPincode' => Input::get('drop_off_zipcode'),
                "DeliveryLatLong" => [floatval(Input::get('DeliveryLong')), floatval(Input::get('DeliveryLat'))],
                'DeliveryDate' => new MongoDate(strtotime(Input::get('drop_off_date'))),

                "ReturnFullAddress" => $this->formated_address([
                    Input::get('return_address_line_1'),
                    Input::get('return_address_line_2'),
                    @$return_city,
                    @$return_state,
                    @$return_country,
                ], Input::get('return_zipcode')),
                "ReturnAddress" => Input::get('return_address_line_1'),
                "ReturnAddress2" => Input::get('return_address_line_2'),
                "ReturnCityTitle" => @$return_city,
                "ReturnStateTitle" => @$return_state,
                'ReturnCountry' => @$return_country,
                'ReturnPincode' => Input::get('return_zipcode'),
                "FlexibleDeliveryDate" => (Input::get('is_delivery_date_flexible') == 'yes') ? 'yes' : 'no',
                "JournyType" => (Input::get('journey_type') == 'return') ? 'return' : 'one_way',
                'NotDelReturnFullAddress' => $this->formated_address([
                    Input::get('nd_return_address_line_1'),
                    Input::get('nd_return_address_line_2'),
                    @$nd_return_city,
                    @$nd_return_state,
                    @$nd_return_country,
                ], Input::get('nd_return_zipcode')),
                "InCaseNotDelReturnAddress" => Input::get('nd_return_address_line_1'),
                "InCaseNotDelReturnAddress2" => Input::get('nd_return_address_line_2'),
                "InCaseNotDelReturnCity" => @$nd_return_city,
                "InCaseNotDelReturnState" => @$nd_return_state,
                "InCaseNotDelReturnCountry" => @$nd_return_country,
                "InCaseNotDelReturnPincode" => Input::get('nd_return_zipcode'),
                "PackageMaterial" => (Input::get('need_package_material') == 'yes') ? 'yes' : 'no',
                "PackageMaterialShipped" => '',
                "PublicPlace" => (Input::get('public_palce') == 'yes') ? 'yes' : 'no',
                'ReceiverCountrycode' => Input::get('country_code'),
                'ReceiverMobileNo' => Input::get('phone_number'),
                'FlexibleDeliveryDate' => Input::get('is_delivery_date_flexible'),
                "Distance" => $calculated_distance,
                'ShippingCost' => 0,
                'InsuranceCost' => 0,
                'Discount' => 0,
                'TotalCost' => 0,
                'needToPay' => 0,
                'promocode' => Input::get('promocode'),
                'ProductList' => [],
            ];

            // if(input::get('product_list')!='')
            //  if (count($res->product) > 0) {

            if (Input::get('product_list') != '') {

                $product = json_decode(Input::get('product_list'));

                $k = 0;
                foreach ($product as $key) {

                    $k++;
                    $insertArray["ProductTitle"] .= @$key->productName;
                    $insertArray['ShippingCost'] += @$key->shippingCost;
                    $insertArray['InsuranceCost'] += @$key->InsuranceCost;
                    $insertArray['TotalCost'] += @$key->shippingCost+@$key->InsuranceCost;

                    $insertArray['ProductList'][] = [
                        '_id' => (Input::get('request_id') == '') ? (string) new MongoId() : $key->_id,
                        "productName" => @$key->productName,
                        'package_id' => (Input::get('request_id') == '') ? $insertArray['PackageNumber'] . $k : $key->PackageNumber,
                        'status' => (Input::get('request_id') == '') ? 'pending' : $key->status,
                        "productWidth" => $key->productWidth,
                        "productHeight" => $key->productHeight,
                        "productLength" => $key->productLength,
                        "productCost" => $key->productCost,
                        "productWeight" => $key->productWeight,
                        "productHeightUnit" => $key->productHeightUnit,
                        "ProductWeightUnit" => $key->productWeightUnit,
                        "ProductLengthUnit" => $key->productLengthUnit,
                        "productCategory" => $key->category,
                        "productCategoryId" => $key->productCategoryId,
                        "travelMode" => $key->travelMode,
                        "InsuranceStatus" => $key->InsuranceStatus,
                        "productQty" => $key->productQty,
                        "ProductImage" => $key->ProductImage,
                        //"OtherImage" => $key->OtherImage,
                        "QuantityStatus" => $key->quantity,
                        "BoxQuantity" => $key->productQty,
                        "Description" => $key->description,
                        "PackageMaterial" => $key->package_material_needed,
                        //"PackageMaterialShipped" => $key->PackageMaterialShipped,
                        "shippingCost" => @$key->shippingCost,
                        'InsuranceCost' => @$key->InsuranceCost,
                        "DeliveryVerifyCode" => (Input::get('request_id') == '') ? rand(1000, 9999) : $key->DeliveryVerifyCode,
                        'tpid' => '',
                        'tpName' => '',
                        'inform_mail_sent' => 'no',
                        'PaymentStatus' => 'no',
                        'total_cost' => @$key->shippingCost+@$key->InsuranceCost,
                    ];

                }

            }

            $result = (String) Deliveryrequest::where(['_id' => Input::get('local_delivery_request_id')])->update($updateArray);
            if (!$result) {
                $response = ['success' => 0, 'msg' => 'Oops! Something went wrong. during updation'];
                return response()->json($response);die;
            }

            $response = ['success' => 1, 'msg' => 'Local delivery request updated successfully.'];

            return response()->json($response);die;

        }
    }
    /*
    * function for: Common function for get formated address between two address.
    * function created by: Deepak Pateriya
    * Created At: 28 feb 2018
    * Updated By:
    * Updated At:
    */
    public function formated_address($array, $zip){
        $address = '';
        foreach ($array as $ky => $val) {
            if (trim($val) != '') {
                if (trim($address) != '') {$address = "$address, ";}
                $address .= $val;
            }
        }
        if (trim($zip) != '') {$address .= "- $zip";}
        return $address;
    }

    public function retrievePayment(){   
        $merchant_key = '3f3f665c-f170-11e7-9871-f23c9170642f';
        if(Input::get('invoice_id') != ''){
            $requests = Deliveryrequest::where(array("mobilemoney" => array('$elemMatch' => array('mobile_moneyId' =>(int) trim(Input::get('invoice_id'))))))->first();
            
            if(count($requests) > 0){
                foreach ($requests->mobilemoney as $key2) {
                    if($key2['mobile_moneyId'] == trim(Input::get('invoice_id'))){
                        $url = "https://community.ipaygh.com/v1/gateway/status_chk?invoice_id={$key2['mobile_moneyId']}&merchant_key={$merchant_key}";
                            
                        $ch = curl_init($url);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        $output = curl_exec($ch);
                        if (strpos($output, 'paid') !== false) {
                            $this->updateRequest($requests->_id,trim(Input::get('invoice_id')));
                            
                            Transaction::where(['momo_id'=> (int) trim(Input::get('invoice_id'))])->update(['Status'=>'completed']);
                            send_mail('5b167b41360d5c10b3a24048', [
                            "to" => "support@aquantuo.com",
                                "replace" => [
                                    "[PACKAGETITLE]" => ucfirst($requests->ProductTitle),
                                    "[PACKAGEID]" => $requests->PackageNumber,
                                    "[DESTINATION]" => $requests->DeliveryFullAddress,
                                    "[PACKAGENUMBER]" => $requests->PackageNumber,
                                    "[USER]" => $requests->RequesterName,
                                    "[AMOUNT]" => number_format($requests->TotalCost,2)
                                ],
                            ]);
						}

                        return $output;

                    }
                    
                }
            }
        }

        return 'Done';
    }

    public function retrievePayment2(){   
        $merchant_key = '3f3f665c-f170-11e7-9871-f23c9170642f';
        if(Input::get('invoice_id') != ''){
            $requests = Deliveryrequest::where(array("mobilemoney" => array('$elemMatch' => array('mobile_moneyId' =>(int) trim(Input::get('invoice_id'))))))->first();
            
            if(count($requests) > 0){
                foreach ($requests->mobilemoney as $key2) {
                    if($key2['mobile_moneyId'] == trim(Input::get('invoice_id'))){
                        $url = "https://community.ipaygh.com/v1/gateway/status_chk?invoice_id={$key2['mobile_moneyId']}&merchant_key={$merchant_key}";
                            
                        $ch = curl_init($url);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        $output = curl_exec($ch);
                        if (strpos($output, 'paid') !== false) {
                        $this->updateRequest($requests->_id,trim(Input::get('invoice_id')));
                        Transaction::where(['momo_id'=> (int) trim(Input::get('invoice_id'))])->update(['Status'=>'completed']);
                        //$ch = Transaction::where(['momo_id'=> (int) trim(Input::get('invoice_id'))])->first();
                        //print_r($ch); die;
                            /*send_mail('5b167b41360d5c10b3a24048', [
                            "to" => "support@aquantuo.com",
                                "replace" => [
                                    "[PACKAGETITLE]" => ucfirst($requests->ProductTitle),
                                    "[PACKAGEID]" => $requests->PackageNumber,
                                    "[DESTINATION]" => $requests->DeliveryFullAddress,
                                    "[PACKAGENUMBER]" => $requests->PackageNumber,
                                    "[USER]" => $requests->RequesterName,
                                    "[AMOUNT]" => number_format($requests->TotalCost,2)
                                ],
                            ]);*/


                        }

                        return $output;

                    }
                    
                }
            }
        }

        return 'Done';
    }

    public function updateRequest($id,$invoice){
        $data =  Deliveryrequest::where(['_id'=> $id])->first();
        $ProductList = $data->ProductList;
        $mobilemoney = $data->mobilemoney;
        if($data->Status == 'momo_initiated'){
            $data->Status = 'ready';
            foreach ($data->ProductList as $key => $value ) {
               $ProductList[$key]['status'] = 'ready';
            }
        }
        foreach ($data->mobilemoney as $key => $value) {
            if(!in_array($value['mobile_money_status'],['cancelled','expired']) ){
                if($invoice == $value['mobile_moneyId']){
                   // $key['mobile_money_status'] = 'paid';
                    $mobilemoney[$key]['mobile_money_status'] = 'paid';
                }
            }
        }
        $data->ProductList = $ProductList;
        $data->mobilemoney = $mobilemoney;
        $data->save();
    }

    public function kgToLb ($val) {
        return $val * 2.20462;
    }

    public function priceEstimator() {
        $response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
        $validate = Validator::make(Input::get(), [
            'travelMode' => 'required',
            'request_type' => 'required',
            'drop_off_lat' => 'required',
            'drop_off_long' => 'required',
            'category_id' => 'required',
            'category_name' => 'required',
            'drop_off_state_id' => 'required',
            'product_cost'=> 'required'
        ]);

        if ($validate->fails()) {
            $validate->errors()->add('field', trans('Oops! Something went wrong'));
            return $validate->errors();
        } else {
            $data = [
                'shippingcost' => 0,
                'insurance' => 0, 
                'ghana_total_amount' => 0,
                'total_amount' => 0, 
                'error' => [], 
                'formated_text' => '', 
                'distance_in_mile' => 0, 
                'type' => 'online', 
                'item_cost' => 0,
                "weight_unit"  => Input::get('weight_unit'),
                "weight"  => Input::get('weight'),
                'shipping_cost_by_user' => 0, 
                'product_count' => 0, 
                'ProcessingFees' => 0,
                'consolidate_check'=> (Input::get('consolidate_item') != '')? Input::get('consolidate_item'):'off', 
                'is_customs_duty_applied'=>false
            ];
            $destination_address=[
                'lat'=> Input::get('drop_off_lat'),
                'lng'=> Input::get('drop_off_long')
            ];
            $aqlat  = 39.2625832;
            $aqlong = -75.60483959999999;

            $weight = Input::get('weight');
            $weight_unit = Input::get('weight_unit');

            $data['distance']  = Utility::getDistanceBetweenPointsNew($aqlat, $aqlong, $destination_address['lat'], $destination_address['lng']);
            //print_r($data['distance']);die;
            $data['distance'] = (Object) ['distance' => $data['distance']];

            if (Input::get('request_type') == 'buyforme') {
                $data['item_cost'] = floatval(Input::get('product_cost'));
                $data['type'] = 'buyforme';
            }

            if(Input::get('drop_off_country_id')!="56bda94bcf3207510bee89c9"){
				$data['is_customs_duty_applied'] = true;
			}
        
            if(in_array(Input::get('request_type'), ['online','buyforme'])){

                $product[] = [
                    'request_type'          => Input::get('request_type'),
                    'insurance_status'      => "yes",
                    //'insurance_status'    => "yes",
                    'travelMode'            => Input::get('travelMode'),
                    'qty'                   => 1,
                    'categoryid'            => Input::get('category_id'),
                    'category'              => Input::get('category_name'),
                    "weight_unit"           => $weight_unit,
                    "length"                => 1,
                    "width"                 => $weight,
                    "height"                => 1,
                    "weight"                => ($weight == '') ? 0 : $weight,
                    'shipping_cost_by_user' => 0,
                    'price'                 => floatval(Input::get('product_cost')),
                    'shipping_cost'         => 0.0,
                    'insurance_cost'        => 0.0,
                ];
                $userinfo = ['Default_Currency' => 'USD'];
                $exists = CityStateCountry::where(array('_id' => Input::get('drop_off_country_id')))->get();
                $countryName = '';
                if (count($exists) === 0) {
                    $countryName = '';
                } else {
                    $countryName = $exists[0]['Content'];
                }
                $calculate = new NewCalculation();
                $information = $calculate->GetRate($product, $data, $userinfo, $countryName);
				
                if ($information['success'] == 1) {
                    $response['success'] = 1;
                    $response['msg']     = "Calculation";

                    $response['resionCharges'] = $this->resionCharges(['name' =>'','id'=> Input::get('drop_off_state_id')]);
                    $response['total_amount'] = $information['total_amount'] + $response['resionCharges'];

                    $currency_conversion = $this->currency_conversion($response['total_amount']);
                    $response['result']['ghana_total_amount'] = $currency_conversion['ghanian_cost'];
                    $response['result']['canadian_cost'] = $currency_conversion['canadian_cost'];
                    $response['result']['philipins_cost'] = $currency_conversion['philipins_cost'];
                    $response['result']['uk_cost'] = $currency_conversion['uk_cost'];
                    $response['result']['kenya_cost'] = $currency_conversion['kenya_cost'];
                    $response['result']['total_weight'] = floatval($information['total_weight']);

                    $response['result']['item_cost'] = floatval($information['total_item_cost']);
                    $response['result']['fees'] = $information['total_amount'] - $information['total_item_cost'];
                    $response['result']['fees'] = floatval($information['ProcessingFees']);
                    $response['result']['custom_duty'] = $information['DutyAndCustom'];
					$response['result']['insurance'] = $information['insurance'];
					$response['result']['shipping_cost'] = $information['shippingCost'];

                    if (Input::get('travelMode') == 'ship') {
						$response['result']['total_weight'] = $this->get_weight(Input::get('weight'), $weight_unit);
					}

                    $response['result']['total_amount'] = floatval($response['total_amount']);
                } else {
                    $response['msg'] = $information['msg'];
                }
            }else if(in_array(Input::get('request_type'), ['delivery'])){
                $ups_rate = 0;
                $is_customs_duty_applied = false;
				$warehouse_transit_fee = 0;
				$is_ups_rate_applied = false;

                // Calculate UPS rate.
                $pickup_country = Input::get("pickup_country");   // Pickup.
                $drop_off_country = Input::get("drop_off_country");    // Drop off.
                $drop_off_zipcode = Input::get("drop_off_zipcode");
                $pickup_postal_code = Input::get("pickup_zipcode");
                $pickup_state = Input::get("pickup_state");
                $pickup_state_short_name = Input::get("pickup_state_short_name");
                $pickup_city = Input::get("pickup_city");
                $dropoff_state = Input::get("drop_off_state");
                $width = Input::get("width");
                $height = Input::get("height");
                $length = Input::get("length");
                $weight = Input::get("weight");

                if ($pickup_country != $drop_off_country) {
                    $is_customs_duty_applied = true;
                    
                    if (Input::get("measurement_unit") == "cm_kg") {
                        $weight = $this->kgToLb($weight);
                    }

                    if ($pickup_country == "USA") { // 56bda94bcf3207510bee89c9
                        $params = array(
                            "width" => $width,
                            "height" => $height,
                            "length" => $length,
                            "weight" => $weight,
                            "pickupPostalCode" => $pickup_postal_code,
                            "pickupStateProvinceCode" => $pickup_state_short_name,
                            "pickupCity" => $pickup_city,
                            "pickupCountryCode" => "US",
                            "destinationPostalCode" => "19977",
                            "service"=> "03"
                        );

                        $ups = new upsRate();
                        $ups->setCredentials();
                        $ups_rate = $ups->getRate($params);
                        $is_ups_rate_applied = true;
                    } else if ($drop_off_country == "USA") { // 56bda94bcf3207510bee89c9
                        if (Input::get('product_cost') <= 2000) {
                            $is_customs_duty_applied = false;
                        }

                        $params = array(
                            "width" => $width,
                            "height" => $height,
                            "length" => $length,
                            "weight" => $weight,
                            "pickupPostalCode" => "19977",
                            "pickupStateProvinceCode" => "DE",
                            "pickupCity" => "Smyrna",
                            "pickupCountryCode" => "US",
                            "destinationPostalCode" => $drop_off_zipcode,
                            "service"=> "03"
                        );

                        $ups = new upsRate();
                        $ups->setCredentials();
                        $ups_rate = $ups->getRate($params);
                        $is_ups_rate_applied = true;
                    }
                }
                // End.
                
                // UPS charge also applied in USA if state is diffrent
				if ($pickup_country == "USA" && $drop_off_country == "USA") {
					if ($pickup_state != $dropoff_state) {

						if (Input::get("measurement_unit") == "cm_kg") {
							$weight = $this->kgToLb($weight);
						}

						$params = array(
							"width" => $width,
							"height" => $height,
							"length" => $length,
							"weight" => $weight,
							"pickupPostalCode" => "19977",
							"pickupStateProvinceCode" => "DE",
							"pickupCity" => "Smyrna",
							"pickupCountryCode" => "US",
							"destinationPostalCode" => $destination_postal_code,
							"service"=> "03"
						);
						$ups = new upsRate();
						$ups->setCredentials();
						$ups_rate =  $ups->getRate($params);
						$is_ups_rate_applied = true;
                        $is_customs_duty_applied = true;
					}
				}

                // Calculate volumetric weight of product.
                $VolumetricInfo = ["length" => $length, "height" => $height, "width" => $width, "type" => $weight_unit];
                $VolumetricWeight = Utility::calculate_volumetric_weight($VolumetricInfo);
                $volumetricWeightInLbs = $VolumetricWeight * 2.20462;
                
                if ($volumetricWeightInLbs > $weight) {
                    $weight = $volumetricWeightInLbs;
                }

                // If weight or volumetric weight will exceed 160lbs, show the following message -
                if ($weight > 160 && $is_ups_rate_applied) {
                    $response['msg'] = 'Please contact us at support@aquantuo.com for pricing.';
                    return response()->json($response);
                    die();
                }

                if (Input::get('pickup_lat') == '' | Input::get('pickup_long') == '') {
                    $response['msg']="pickup_lat and pickup_long is required.";
                    return response()->json($response);die;
                }

                $distance = Utility::getDistanceBetweenPointsNew(Input::get('pickup_lat'), Input::get('pickup_long'),Input::get('drop_off_lat'),Input::get('drop_off_long'));
                $lhwunit = 'cm';
                $weightunit = 'kg';
                
                if (Input::get('measurement_unit')  == 'inches_lbs') {
                    $lhwunit = 'inches';
                    $weightunit = 'lbs';
                }
                
                $requesthelper = new NewRequesthelper(
                    [
                        "needInsurance" => (Input::get('pe_insurance') == 'yes') ? true : true,
                        "productQty" => (Input::get('productQty') < 1) ? 1 : Input::get('productQty'),
                        //"productCost" => Input::get('pe_product_cost'),
                        "productCost" => Input::get('product_cost'),
                        "productWidth" => Input::get('width'),
                        "productWidthUnit" => $lhwunit,
                        "productHeight" => Input::get('height'),
                        "productHeightUnit" => $lhwunit,
                        "productLength" => Input::get('length'),
                        "productLengthUnit" => $lhwunit,
                        "productWeight" => Input::get('weight'),
                        "productWeightUnit" => $weightunit,
                        "productCategory" => Input::get('category_name'),
                        "productCategoryId" => Input::get('category_id'),
                        "distance" => $distance,
                        "travelMode" => (Input::get('request_type') == 'local_ghana')? "ship": Input::get('travelMode'),
                        "currency" => 'GHS',
                        'consolidate_check'=>(Input::get('consolidate_check') != '')? Input::get('consolidate_check'):'off',
                    ]
                );

                $requesthelper->calculate();
                $resp['calculationinfo'] = $requesthelper->get_information();
                if (!isset($resp['calculationinfo']->error)) {
                    $response['success'] = 1;

					if($is_ups_rate_applied){
						$warehouse_transit_fee = $resp['calculationinfo']->warehouse_transit_fee;
					}

					$custom_duty = 0;
					if ($is_customs_duty_applied) {
						$custom_duty = $resp['calculationinfo']->DutyAndCustom;
                    }
                    
                    $response['result']['shipping_cost'] = $resp['calculationinfo']->shippingcost;
                    $config = Configuration::find(CongigId);
                    if ($drop_off_country === 'Kenya') {
                        $custom_duty = 0;
                        $extra = 0;
                        if (Input::get('product_cost') > 100) {
                            $extra = (Input::get('product_cost') * $config->kenya_per_item_value)/100;
                            $extra = number_format($extra, 2, '.', '');
                        }
                        // if ($weightunit === 'kg') {
                        //     $ratePerKg = number_format(($weight * $config->kenya_rate_per_kilogram), 2, '.', '');
                        // } else {
                            $ratePerKg = number_format(($weight * 0.453592 * $config->kenya_rate_per_kilogram), 2, '.', '');
                        // }
                        $ratePerKg = $ratePerKg < $config->kenya_rate_per_kilogram ? $config->kenya_rate_per_kilogram : $ratePerKg;
                        $response['result']['shipping_cost'] = $ratePerKg;
                        // echo $ratePerKg;
                        $resp['total_amount'] = $ratePerKg + $extra;
                        
                        if ($resp['total_amount'] < $config->kenya_rate_per_kilogram) {
                            $resp['total_amount'] = $config->kenya_rate_per_kilogram;
                        }
                    }
                    $resp['total_amount'] = $response['result']['shipping_cost'] + $resp['calculationinfo']->insurance + $ups_rate + $custom_duty + $resp['calculationinfo']->Tax + $warehouse_transit_fee;
                    
                    //Resion Charges
                    $resp['resionCharges'] = $this->resionCharges(['name' =>'','id'=> Input::get('state_id')]);
                    $resp['total_amount'] = $resp['resionCharges'] + $resp['total_amount'];

                    $currency_conversion = $this->currency_conversion($resp['total_amount']);
                    $response['msg'] = "Calculation";
                    $response['result']['ghana_total_amount'] = $currency_conversion['ghanian_cost'];
                    $response['result']['canadian_cost'] = $currency_conversion['canadian_cost'];
                    $response['result']['philipins_cost'] = $currency_conversion['philipins_cost'];
                    $response['result']['uk_cost'] = $currency_conversion['uk_cost'];
                    $response['result']['kenya_cost'] = $currency_conversion['kenya_cost'];
                    $response['result']["total_weight"] = $weight;
                    $response['result']["item_cost"] = Input::get('product_cost');
                    $response['result']["fees"] = 0;
                    $response['result']["custom_duty"] = $custom_duty;
                    $response['result']['insurance'] = $resp['calculationinfo']->insurance;
                    $response['result']['total_amount']= floatval($resp['total_amount']);
                    $response['result']['total_distance'] = floatval(($distance > 0)? $distance /1000:$distance);
                    $response['result']['ups_rate'] = $ups_rate;
                    $response['result']['resion_charges'] = $resp['resionCharges'];
                }else{
                    $response['msg'] = $response['calculationinfo']->error;
                }
            }else if(in_array(Input::get('request_type'), ['local_ghana'])){
                if(Input::get('pickup_lat') == '' | Input::get('pickup_long') == ''){
                    $response['msg']="pickup_lat and pickup_long is required.";
                    return response()->json($response);die;
                }
                $distance = Utility::getDistanceBetweenPointsNew(Input::get('pickup_lat'), Input::get('pickup_long'),Input::get('drop_off_lat'),Input::get('drop_off_long'));
                $lhwunit = 'cm';
                $weightunit = 'kg';
                if (Input::get('measurement_unit')  == 'inches_lbs') {
                    $lhwunit = 'inches';
                    $weightunit = 'lbs';
                }


                if(Input::get('category_id')){

                    $match_category = Category::where(['_id' => Input::get('category_id'), 'Status' => 'Active', 'type' => 'localCategory','country_id'=> Input::get('drop_off_country_id')])
                    ->select('price')
                    ->first();

                    if(count($match_category) > 0){
                        $response['success']=1;
                        $response["result"]['category_price'] = $match_category->price;
                    }else{
                        $response = ['success' => 0, 'msg' => 'The shipping category you have specified is unsupported.'];
                        return response()->json($response);die;
                    }
                }

                if (Input::get('product_cost') != '' && $response['success'] == 1) {

                    $match_value = Distance::Where('from', '<=', (float) trim(Input::get('product_cost')))->where('to', '>=', (float) trim(Input::get('product_cost')))->where(['Status' => 'Active', 'country_id' => Input::get('drop_off_country_id'), "type" => "item-value"])
                        ->select('price')
                        ->first();

                    if ($match_value) {
                        $response['success']=1;
                        $response["result"]['item_price']= $match_value->price;
                    } else {
                        $response = ['success' => 0, 'msg' => 'The item value you have specified is unsupported.'];
                        return response()->json($response);die;
                    }
                }

                //Distance calculation
                //print_r($distance); die;
                if ($distance >  0 && $distance != '' && $response['success'] == 1) {
                    //Distance in to miles
                    $distance = $distance * 0.000621371;
                    $match_distance = Distance::Where('from', '<=', (float) $distance)->where('to', '>=', (float) $distance)
                        ->where(['Status' => 'Active', 'country_id' => Input::get('drop_off_country_id'), "type" => "distance"])
                        ->select('price')
                        ->first();

                    if ($match_distance) {
                        $response['success'] = 1;
                        $response["result"]['distance_price'] = $match_distance->price;
                    } else {
                        $response = ['success' => 0, 'msg' => 'The shipping address/distance you have specified is unsupported.'];
                    }
                }

                if ($response['success'] == 1) {
                    //Resion Charges
                    $response["result"]['resionCharges'] = $this->resionCharges(['name' =>'','id'=> Input::get('drop_off_state_id')]);
                    
                    $response["result"]['total_amount'] = $response["result"]['distance_price'] + $response["result"]['category_price'] + $response["result"]['item_price'] + $response["result"]['resionCharges'];

                    //print_r($response['resionCharges']); die;
                    $currency_conversion = $this->currency_conversion($response["result"]['total_amount']);
                    $response['msg'] = "Calculation";
                    $response["result"]['total_weight'] = $this->get_weight(Input::get('weight'), $weight_unit);
                    $response["result"]['ghana_total_amount'] = $currency_conversion['ghanian_cost'];
                    $response["result"]['canadian_cost'] = $currency_conversion['canadian_cost'];
                    $response["result"]['philipins_cost'] = $currency_conversion['philipins_cost'];
                    $response["result"]['uk_cost'] = $currency_conversion['uk_cost'];
                    $response['result']['kenya_cost'] = $currency_conversion['kenya_cost'];
                }
            }
        }
        return response()->json($response);die;
    }

    public function get_weight($weight, $type) {
        $type = strtolower($type);
        if ($type == 'kg') {
            return (((float) $weight) * 2.20462);
        } else if ($type == 'gram') {
            return (((float) $weight) * 0.00220462);
        } else {
            return $weight;
        }
    }

    public function resionCharges($state) {
        //print_r($state['id']); die;
        $charges = Extraregion::where(['state_id' => $state['id'], 'status' => 'Active'])->select('amount')->first();
        if ($charges) {
            return $charges->amount;
        } else {
            return 0.00;
        }
    }

    public function currency_conversion($shipcost) {

        $data = ['cost' => 0, 'ghanian_cost' => 0, 'philipins_cost' => 0, 'uk_cost' => 0, 'canadian_cost' => 0, 'kenya_cost' => 0];
        $currency = Currency::where(['Status' => 'Active'])->select('CurrencyRate', 'FormatedText', 'CurrencyCode')->get();

        if ($currency) {
            foreach ($currency as $key) {
                if ($key->CurrencyCode == 'GHS') {
                    $data['ghanian_cost'] = str_replace('[AMT]', number_format($shipcost * $key->CurrencyRate, 2), $key->FormatedText);
                } else if ($key->CurrencyCode == 'CAD') {
                    $data['canadian_cost'] = str_replace('[AMT]', number_format($shipcost * $key->CurrencyRate, 2), $key->FormatedText);
                } else if ($key->CurrencyCode == 'PHP') {
                    $data['philipins_cost'] = str_replace('[AMT]', number_format($shipcost * $key->CurrencyRate, 2), $key->FormatedText);
                } else if ($key->CurrencyCode == 'GBP') {
                    $data['uk_cost'] = str_replace('[AMT]', number_format($shipcost * $key->CurrencyRate, 2), $key->FormatedText);
                } else if ($key->CurrencyCode == 'KES') {
                    $data['kenya_cost'] = str_replace('[AMT]', number_format($shipcost * $key->CurrencyRate, 2), $key->FormatedText);
                    $data['kenya_cost'] = str_replace('Shillings', 'Shilling', $data['kenya_cost']);
                }
            }
        }
        return $data;
    }
	/*
	*  15 Jan 2020
	* TODO show shiping cost at time of create request
	*/ 
	public function buy_for_me_shipping_cost(){
		
	}

    public function CheckAddItem() {
        $response = [ "success" => 0, "msg" => "Whoops! Something went wrong." ];

        $validation = Validator::make(Input::get(), [
            "item_list" => "required"
        ]);

        if ($validation->fails()) {
            return $validation->errors();
        } else {
            $item_list = json_decode(Input::get("item_list"));

            // Stop user to add new item of different category with shipping mode sea.
            $is_make_request = false;
            $category_id = $item_list[0]->productCategoryId;  // Category id of First element in array.
            $item_with_sea = false;
            $item_with_air = false;

            foreach ($item_list as $value) {
                if ($value->travelMode == "air") {
                    $item_with_air = true;
                }

                if ($value->travelMode == "ship") {
                    $item_with_sea = true;
                    if ($category_id != $value->productCategoryId) {
                        $is_make_request = true;
                    }
                }

                if ($item_with_air && $item_with_sea) {
                    $response = [
                        "success" => 0,
                        "msg" => "Sorry! You can’t have different shipping modes in the same listing. Please create a separate listing for either air or sea."
                    ];
                    return response()->json($response);
                    die();
                }
            }

            if ($is_make_request) {
                $response = [
                    "success" => 0,
                    "msg" => "If shipping by sea, all items must be of the same sea category. Please create a new listing if you need to use a different sea category."
                ];
                return response()->json($response);
                die();
            }

            $response = [ "success" => 1, "msg" => "Success!" ];
            return response()->json($response);
        }
    }

    // Retrive all address.
    public function AddressList() {
        $response = [ "success" => 0, "msg" => "Whoops! Something went wrong." ];

        $validation = Validator::make(Input::get(), [
            "user_id" => "required"
        ]);

        if ($validation->fails()) {
            return $validation->errors();
        } else {
            $where = [ "user_id" => Input::get("user_id") ];
            $addressList = Address::where($where)->get();

            if (count($addressList) > 0) {
                $response = [ "success" => 1, "msg" => "Address list.", "result" => $addressList ];
            } else {
                $response = [ "success" => 0, "msg" => "No address found." ];
            }

            return response()->json($response);
        }
    }

    // Create new address.
    public function CreateNewAddress() {
        $response = [ "success" => 0, "msg" => "Whoops! Something went wrong." ];

        $validation = Validator::make(Input::get(), [
            "country" => "required",
            "country_id" => "required",
            "state" => "required",
            "state_id" => "required",
            "city" => "required",
            "city_id" => "required",
            "state_available" => "required",
            "address_line_1" => "required",
            "user_id" => "required",
            "latitude" => "required",
            "longitude" => "required"
        ]);

        if ($validation->fails()) {
            return $validation->errors();
        } else {
            $insert = [
                "country" => Input::get("country"),
                "country_id" => Input::get("country_id"),
                "state" => Input::get("state"),
                "state_id" => Input::get("state_id"),
                "city" => Input::get("city"),
                "city_id" => Input::get("city_id"),
                "state_available" => Input::get("state_available"),
                "address_line_1" => Input::get("address_line_1"),
                "address_line_2" => Input::get("address_line_2"),
                "zipcode" => Input::get("zipcode"),
                "user_id" => Input::get("user_id"),
                "lat" => floatval(Input::get("latitude")),
                "lng" => floatval(Input::get("longitude"))
            ];

            Address::insert($insert);
            $response = [ "success" => 1, "msg" => "Success! Address has been added successfully." ];
            return response()->json($response);
        }
    }
}
