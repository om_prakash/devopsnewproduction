<?php namespace App\Modules\Api9\Controllers;

use App;
use App\Http\Controllers\Controller;
use App\Http\Models\Activitylog;
use App\Http\Models\Deliveryrequest;
use App\Http\Models\Notification;
use App\Http\Models\SendMail;
use App\Http\Models\Setting;
use App\Http\Models\User;
use App\Library\Notification\Pushnotification;
use App\Library\Reqhelper;
use App\Library\WebActivityLog;
use Illuminate\Http\Request;
use Input;
use MongoDate;
use mongoId;
use Validator;
use App\Library\NewEmail;
use App\Library\Notify;
use App\Http\Models\Configuration;

class LocalDeliveryTransporter extends Controller {

	public function LocalDeliveryAccept(Request $request) {
		$response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		$validate = Validator::make(Input::get(), [
			'user_id' => 'required',
			'productid' => 'required',
			'requestId' => 'required',
			//'status' => 'required',
		]);
		if ($validate->fails()) {
			$validate->errors()->add('field', trans('Oops! Something went wrong'));
			return $validate->errors();
		} else {
			$where = [
				"_id" => Input::get('requestId'),
				"ProductList" => ['$elemMatch' => [
					'_id' => Input::get('productid'),
					'tpid' => Input::get('user_id'),
					'status' => 'assign',
				],
				],
			];

			// $requestid = Deliveryrequest::where(['_id' => Input::get('requestId')])->where($where)->select('_id', 'ProductList.$', 'RequesterId', 'Status', 'ProductTitle', 'PackageId', 'PackageNumber', 'shippingCost', 'TotalCost', 'PickupFullAddress', 'DeliveryFullAddress', 'ProductList')->first();
			$requestid = Deliveryrequest::where($where)->select('_id', 'ProductList.$', 'RequesterId', 'Status', 'ProductTitle', 'PackageId', 'PackageNumber', 'shippingCost', 'TotalCost', 'PickupFullAddress', 'DeliveryFullAddress', 'ProductList')->first();

			if (count($requestid) > 0) {
				$requesterData = User::where(array('_id' => $requestid->RequesterId))->
					select('FirstName', 'LastName', 'Email', 'EmailStatus', 'NoficationStatus', 'NotificationId', 'DeviceType','Name')->first();

				$transporterData = User::where(array('_id' => Input::get('user_id')))->select('FirstName', 'LastName', 'Email', 'EmailStatus', 'NoficationStatus', 'NotificationId' . 'DeviceType', 'Name')->first();

				if (count($requesterData) > 0 && count($transporterData) > 0) {

					$update_array = $requestid->ProductList;

					foreach ($requestid->ProductList as $key => $value) {
						if ($value['_id'] == Input::get('productid')) {
							$update_array[$key]['status'] = 'accepted';
							$update_array[$key]['tpid'] = trim(Input::get('user_id'));
							$update_array[$key]['tpName'] = $transporterData->Name;
							
							$item_title = $update_array[$key]['product_name'];
							$packageid = $update_array[$key]['package_id'];
							$total = $update_array[$key]['after_update'];
							$shippingCost = $update_array[$key]['shippingCost'];
						}
					}

					// $updateData = Deliveryrequest::where($where)->select('_id', 'ProductList.$')->update(['ProductList.$.status' => 'accepted']);
					$updateData = Deliveryrequest::where($where)->update($update_array);

					$insertactivity = [
						'request_id' => $requestid->RequesterId,
						'request_type' => $requestid->RequestType,
						'PackageNumber' => $requestid->PackageNumber,
						'item_id' => Input::get('productid'),
						'package_id' => $packageid,
						'item_name' => $item_title,
						'log_type' => 'request',
						'message' => 'Item accepted.',
						'status' => 'accepted',
						'action_user_id' => trim(Input::get('user_id')),
						'EnterOn' => new MongoDate(),
					];
					Activitylog::insert($insertactivity);

					if ($updateData) {
						Reqhelper::update_status2(Input::get('requestId'));
						$Email = new NewEmail();

						
						$Email->send_mail('5694ce6e5509251cd67773ed', [
							"to" => $transporterData->Email,
							"replace" => [
								"[USERNAME]" => ucfirst($transporterData->Name),
								"[PACKAGETITLE]" => ucfirst($item_title),
								"[PACKAGEID]" => $packageid,
								"[SOURCE]" => $requestid->PickupFullAddress,
								"[DESTINATION]" => $requestid->DeliveryFullAddress,
								"[PACKAGENUMBER]" => $packageid,
								"[SHIPPINGCOST]" => $shippingCost,
								"[TOTALCOST]" => $total,
							],
						]);

						

						$Email->send_mail('57d7d4b37ac6f69c158b4569', [
							"to" => $requesterData->Email,
							"replace" => [
								"[USERNAME]" => ucfirst($requesterData->Name),
								"[PACKAGETITLE]" => ucfirst($item_title),
								"[PACKAGEID]" => $packageid,
								"[SOURCE]" => $requestid->PickupFullAddress,
								"[DESTINATION]" => $requestid->DeliveryFullAddress,
								"[PACKAGENUMBER]" => $packageid,
								"[SHIPPINGCOST]" => $shippingCost,
								"[TOTALCOST]" => $total,
							],
						]);
						//email to admin
						$setting = Setting::find('563b0e31e4b03271a097e1ca');
						if (count($setting) > 0) {
							
							$Email->send_mail('56ab46f95509251cd67773f3', [
								"to" => $setting->SupportEmail,
								"replace" => [
									"[TRANSPORTERNAME]" => $transporterData->Name,
									"[PACKAGETITLE]" => ucfirst($item_title),
									"[PACKAGEID]" => $packageid,
									"[SOURCE]" => $requestid->PickupFullAddress,
									"[DESTINATION]" => $requestid->DeliveryFullAddress,
								],
							]);
						}

						//Notification to requester

							if ($requesterData->NoficationStatus == "on" && !empty($requesterData->NotificationId)) {
							$Notification = new Notify();
							$Notification->setValue('title', trans('lang.SEND_ACCEPT_TITLE'));
							$Notification->setValue('message', sprintf(trans('lang.SEND_ACCEPT_MSG'), $transporterData->Name, $item_title));
							$Notification->setValue('type', 'LOCAL_DELIVERY_USER');
							$Notification->setValue('location', Input::get('requestId'));
							$Notification->setValue('locationkey', Input::get('requestId'));
							$Notification->add_user($requesterData->NotificationId, $requesterData->DeviceType);
							$Notification->fire();
						}
						//insertNotification

						Notification::Insert([
							array(
								"NotificationTitle" => trans('lang.SEND_ACCEPT_TITLE'),
								"NotificationShortMessage" => 'Request accepted successfully.',
								"NotificationMessage" => sprintf(trans('lang.SEND_ACCEPT_MSG'), $transporterData->Name, $item_title),
								"NotificationType" => "LOCAL_DELIVERY_USER",
								"NotificationUserId" => array(new MongoId($requesterData->_id)),
								"Date" => new MongoDate(),
								"GroupTo" => "User",
							),
							array(
								"NotificationTitle" => 'Request was accepted',
								"NotificationMessage" => sprintf(trans('lang.SEND_ACCEPT_MSG_ADMIN'), $transporterData->Name, $requestid->PickupFullAddress, $requestid->DeliveryFullAddress),
								"NotificationUserId" => array(),
								"NotificationReadStatus" => 0,
								"location" => "request_detail",
								"locationkey" => (string) Input::get('requestid'),
								"Date" => new MongoDate(),
								"GroupTo" => "Admin",
							),
						]);

						if (count($updateData) > 0) {
							$response = [
								'success' => 1,
								'msg' => 'Request has been accepted successfully.',
							];
						}
					}
				}
			}
		}
		return response()->json($response);
	}

	public function sendPackageCancelDelivery() {
		$response = array("success" => 0, "msg" => "Oops! Something went wrong.");
		$validate = Validator::make(Input::get(), [
			'user_id' => 'required',
			'productid' => 'required',
			'requestId' => 'required',
			'rejectBy' => 'required',
			'returnType' => 'required',
			'trackingNumber' => 'required',
			'message' => 'required',
			//	'image' => 'required',
		]);
		if ($validate->fails()) {
			$validate->errors()->add('field', trans('Oops! Something went wrong'));
			return $validate->errors();
		} else {
			$where = ["ProductList" => ['$elemMatch' => ['_id' => Input::get('productid'), 'status' => 'out_for_delivery']]];

			$deliveryData = Deliveryrequest::where($where)
				->select('ProductList.$', 'ProductTitle', 'RequesterId')
				->first();

			$setting = Setting::find('563b0e31e4b03271a097e1ca');
			if (count($deliveryData) > 0) {

				$requesterData = User::where(array('_id' => $deliveryData->RequesterId))
					->select('Email', 'EmailStatus', 'NoficationStatus', 'NotificationId', 'DeviceType', 'Name')
					->first();

				$transporterData = User::where(array('_id' => Input::get('user_id')))
					->select('Name', 'Email', 'EmailStatus', 'NoficationStatus', 'NotificationId' . 'DeviceType')
					->first();

				if (count($requesterData) > 0 && count($transporterData) > 0) {

					$updData = [
						'ProductList.$.status' => 'cancel',
						'ProductList.$.RejectBy' => Input::get('rejectBy'),
						'ProductList.$.ReturnType' => Input::get('returnType'),
						"ProductList.$.RejectTime" => new MongoDate(),
						'ProductList.$.TrackingNumber' => Input::get('trackingNumber'),
						'ProductList.$.TransporterMessage' => Input::get('message'),
						"ProductList.$.UpdateOn" => new MongoDate(),
					];

					if (isset($_FILES['image']['name']) && @$_FILES['image']['name'] != '') {
						$exts = explode('.', $_FILES['image']['name']);
						$ext = $exts[count($exts) - 1];
						$receipt = "receipt" . rand(2154, 45454) . time() . ".$ext";
						if (in_array(strtolower($ext), ['gif', 'png', 'jpg', 'jpeg'])) {
							$newpath = BASEURL_FILE . "package/{$receipt}";
							if (move_uploaded_file($_FILES['image']['tmp_name'], $newpath)) {
								$updData['ProductList.$.ReceiptImage'] = "package/{$receipt}";
							}
						}
					}
					$notifymsg = sprintf('Your package "%s" has been canceled by %s', $deliveryData->ProductList[0]['product_name'], $requesterData['Name']);

					$emailReasonforCancaletion = "Rejectby : " . ucfirst(Input::get('rejectBy')) . ", Retrun type: " . get_cancel_status(Input::get('returnType'));
					if (Input::get('returnType') == 'ipayment') {
						$notifymsg = sprintf(trans('lang.BUYFORME_CANCELED_MSG_IPAY'),
							$deliveryData->ProductList[0]['product_name'],
							@$setting->SupportEmail
						);
					} else if (strtolower(Input::get('returnType')) == 'creturn' && strtolower(Input::get('Rejectby')) == 'requester') {

						if (Input::get('date') != '') {
							$date = date_create_from_format('M d, Y h:i:s A', Input::get('date'));
							if ($date) {
								$updData['ProductList.$.CancelReturnDate'] = new MongoDate(strtotime(date_format($date, 'd-m-Y H:i:s')));
							}
						}

						$notifymsg = sprintf(trans('lang.BUYFORME_CANCELED_MSG_CRE'), $deliveryData->ProductList[0]['product_name'], $updData['ProductList.$.TransporterMessage']);

					} else if (Input::get('returnType') == 'osreturn' && strtolower(Input::get('Rejectby')) == 'requester') {

						$notifymsg = sprintf(trans('lang.BUYFORME_CANCELED_MSG_OSCRE'),
							$deliveryData->ProductList[0]['product_name'],
							$transporterData->Name,
							$updData['ProductList.$.TransporterMessage']
						);
					}
					$updateData = Deliveryrequest::where($where)->update($updData);
					if (count($updateData) > 0) {
						ReqHelper::update_status2($deliveryData->_id);
						$response = [
							'success' => 1,
							'msg' => 'Success! Request has been canceled successfully.',
						];
					}
					//email to transporter
					if ($transporterData->EmailStatus == "on") {
						send_mail('58762b057ac6f6f2128b4567', [
							"to" => $transporterData->Email,
							"replace" => [
								"[USERNAME]" => $transporterData->Name,
								"[PACKAGETITLE]" => $deliveryData->ProductList[0]['product_name'],
								"[REASON]" => $emailReasonforCancaletion,
								"[PACKAGEID]" => $deliveryData->ProductList[0]['package_id'],
							],
						]);
					}

					//email to requester
					if ($requesterData->EmailStatus == "on") {
						send_mail('588b56736befd90b9d7ea797', [
							"to" => $requesterData->Email,
							"replace" => [
								"[USERNAME]" => $requesterData->Name,
								'[PACKAGETITLE]' => $deliveryData->ProductList[0]['product_name'],
								"[REASON]" => $emailReasonforCancaletion,
								"[PACKAGEID]" => $deliveryData->ProductList[0]['package_id'],
							],
						]);
					}

					$Notification = new Pushnotification();

					//Notification to requester
					if ($requesterData->NoficationStatus == "on" && !empty(trim($requesterData->NotificationId))) {
						$Notification->setValue('title', trans('lang.BUYFORME_CANCELED_TITLE'));
						$Notification->setValue('message', $notifymsg);
						$Notification->setValue('type', 'delivery_detail');
						$Notification->setValue('locationkey', Input::get('requestId'));
						$Notification->add_user($requesterData->NotificationId, $requesterData->DeviceType);
						$Notification->fire();
					}
					//insertNotification
					Notification::Insert([
						array(
							"NotificationTitle" => 'Your request has been canceled',
							"NotificationShortMessage" => $notifymsg,
							"NotificationMessage" => $notifymsg,
							"NotificationType" => "canceled_delivery",
							"NotificationUserId" => array(new MongoId($requesterData->_id)),
							"Date" => new MongoDate(),
							"GroupTo" => "User",
						),
						array(
							"NotificationTitle" => 'Request canceled',
							"NotificationMessage" => $notifymsg,
							"NotificationReadStatus" => 0,
							"location" => "request_detail",
							"locationkey" => (string) Input::get('requestId'),
							"Date" => new MongoDate(),
							"GroupTo" => "Admin",
						),
					]);

				}
			}
		}
		return response()->json($response);
	}
	
	/*
		 	 * Author: Aakash Tejwal
		 	* Created Date : 28-02-2018
	*/
	public function localDeliveryProcess(Request $request) {
		$response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		$validate = Validator::make(Input::get(), [
			'user_id' => 'required',
			'requestId' => 'required',
			'productid' => 'required',
			'status' => 'required',
		]);
		if ($validate->fails()) {
			$validate->errors()->add('field', trans('Oops! Something went wrong'));
			return $validate->errors();
		} else {

			$where = [
				"ProductList" => ['$elemMatch' => [
					'_id' => $request->get('productid'),
					'tpid' => Input::get('user_id'),
				]],
			];

			$req_data = Deliveryrequest::where(['_id' => Input::get('requestId')])->where($where)->select('_id', 'ProductList.$', 'RequesterId', 'PackageId', 'ShippingCost', 'TotalCost')->first();

			if (count($req_data) == 0) {
				return response()->json($response);
			}
			$requester = User::where('_id', '=', $req_data->RequesterId)->first();
			$transporter = User::where('_id', '=', Input::get('user_id'))->first();

			if (count($requester) == 0 || count($transporter) == 0) {
				return response()->json($response);
			}

			if (Input::get('status') == 'out_for_pickup') {
				$old_status = 'assign';
				$new_status = 'out_for_pickup';
				$message = 'Request has been picked up.';
				$tpid = Input::get('user_id');
				$tpName = $transporter->Name;

				$requesterEmailTempId ='5694cb805509251cd67773ec'; 
				$requesterNotiTitle=trans('lang.SEND_OUTFORPICKUP_TITLE');
				$requesterNotiMsg=trans('lang.SEND_OUTFORPICKUP_MSG');

				$transporterEmailTempId =''; 
				$transporterNotiTitle='';
				$transporterNotiMsg='';
			} else if (Input::get('status') == 'out_for_delivery') {
				$old_status = 'out_for_pickup';
				$new_status = 'out_for_delivery';
				$message = 'Your package is en route to be delivered.';
				$tpid = Input::get('user_id');
				$tpName = $transporter->Name;

				$requesterEmailTempId ='562228e2e4b0252ad07d07a2'; 
				$requesterNotiTitle=trans('lang.SEND_OUTFORDELIVERY_TITLE');
				$requesterNotiMsg= sprintf(trans('lang.SEND_OUTFORDELIVERY_MSG'), ucfirst($req_data['ProductList'][0]['product_name']),$req_data['ProductList'][0]['package_id']);

				$transporterEmailTempId =''; 
				$transporterNotiTitle='';
				$transporterNotiMsg='';
			} else if (Input::get('status') == 'cancel') {
				$new_status = 'purchased';
				$message = 'Request has been cancel successfully.';
				$tpid = "";
				$tpName = '';

				$requesterEmailTempId =''; 
				$transporterEmailTempId =''; 
				$requesterNotiTitle='';
				$transporterNotiTitle='';
				$requesterNotiMsg='';
				$transporterNotiMsg='';
			}

			$DeliveryStatus = Deliveryrequest::where(['_id' => Input::get('requestId')])->where($where)->select('_id', 'ProductList.$')->update(['ProductList.$.status' => $new_status, 'ProductList.$.tpid' => $tpid, 'ProductList.$.tpName' => $tpName]);

			if ($DeliveryStatus) {

				Reqhelper::update_status2(Input::get('requestId'));
				$deliveryRequest = Deliveryrequest::where(['_id' => Input::get('requestId')])->where($where)->select('_id', 'ProductList.$', 'RequestType', 'RequesterId', 'PackageId', 'ShippingCost', 'TotalCost')->first();
				
				$deliveryRequest['action_user_id'] = trim(Input::get('user_id'));
				$Activity = new WebActivityLog();
				$Activity->ActivityLog($deliveryRequest);
				$Email = new NewEmail();

				if (in_array(Input::get('status'), ['out_for_pickup', 'out_for_delivery', 'cancel'])) {


					if ($requester->EmailStatus == "on") {

						$Email->send_mail($requesterEmailTempId, [
							"to" => $requester->Email,
							"replace" => [
								"[USERNAME]" => ucfirst($requester->Name),
								"[PACKAGETITLE]" => ucfirst($deliveryRequest['ProductList'][0]['product_name']),
								"[PACKAGEID]" => $deliveryRequest['ProductList'][0]['package_id'],
								"[SOURCE]" => $requester->PickupAddress,
								"[DESTINATION]" => $requester->DeliveryAddress,
								"[PACKAGENUMBER]" => $req_data->PackageId,
								"[SHIPPINGCOST]" => $req_data->ShippingCost,
								"[TOTALCOST]" => $req_data->TotalCost,
								"[REQUESTER]" => ucfirst($requester->Name),
							],
						]);
					}

					/*if ($transporter->EmailStatus == "on") {
						

						$Email->send_mail($transporterEmailTempId, [
							"to" => $transporter->Email,
							"replace" => [
								"[USERNAME]" => $transporter->Name,
								"[PACKAGETITLE]" => ucfirst($deliveryRequest['ProductList'][0]['product_name']),
								"[PACKAGEID]" => $deliveryRequest['ProductList'][0]['package_id'],
								"[SOURCE]" => $request->PickupAddress,
								"[DESTINATION]" => $request->DeliveryAddress,
								"[PACKAGENUMBER]" => $req_data->PackageId,
								"[SHIPPINGCOST]" => $req_data->ShippingCost,
								"[TOTALCOST]" => $req_data->TotalCost,
								"[REQUESTER]" => ucfirst($requester->Name),
							],
						]);
					}*/

					if ($requester->NoficationStatus == "on" && !empty($requester->NotificationId)) {
						/*$cron_noti = [
							"title" => 'notification',
							"message" => $message,
							"type" => Input::get('status'),
							"locationkey" => Input::get('request_id'),
							"NotificationId" => $requester->NotificationId,
							"DeviceType" => $requester->DeviceType,
							'status' => $new_status,
							'by_mean' => 'notification',
						];
						SendMail::insert($cron_noti);*/
						
						if(Input::get('status') != 'cancel'){
							$Notification = new Notify();
							$Notification->setValue('title',$requesterNotiTitle);
							$Notification->setValue('message',$requesterNotiMsg);
							$Notification->setValue('type','LOCAL_DELIVERY_USER');
							$Notification->setValue('location', Input::get('requestId'));
							$Notification->setValue('locationkey',$request->get('requestId'));
							$Notification->add_user($requester->NotificationId, $requester->DeviceType);
							$Notification->fire();

							Notification::Insert([
								"NotificationTitle" => $requesterNotiTitle,
								"NotificationShortMessage" => $requesterNotiMsg,
								"NotificationMessage" => $requesterNotiMsg,
								"NotificationType" => "LOCAL_DELIVERY_USER",
								"NotificationUserId" => array(new MongoId($requester->_id)),
								"Date" => new MongoDate(),
								"GroupTo" => "User",
							]);
						}

						
					}
				}

				
				$response = ['success' => 1, 'msg' => $message];
			}
		}
		return response()->json($response);
	}
	/*
		 * Author: Aakash Tejwal
		 * Created Date : 02-03-2018
	*/
	public function localDeliveryMobileVerify(Request $request) {
		$response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		$validate = Validator::make(Input::get(), [
			'user_id' => 'required',
			'requestId' => 'required',
			'productid' => 'required',
			'verify_code' => 'required',
		]);
		if ($validate->fails()) {
			$validate->errors()->add('field', trans('Oops! Something went wrong'));
			return $validate->errors();
		} else {

			$where = array("_id" => $request->get('requestId'),
				"ProductList" => array('$elemMatch' => array(
					'_id' => $request->get('productid'),
					'status' => 'out_for_delivery',
				)));

			$result = deliveryRequest::where($where)->first();

			if (count($result) > 0) {
				$requesterData = User::where(['_id' => $result->RequesterId])
					->select('FirstName', 'LastName', 'Email', 'EmailStatus', 'NoficationStatus', 'NotificationId', 'DeviceType', 'Name')
					->first();

				$transporterData = User::where(array('_id' => Input::get('user_id')))->select('FirstName', 'LastName', 'Email', 'EmailStatus', 'NoficationStatus', 'NotificationId', 'DeviceType', 'Name')->first();
				if (count($requesterData) > 0 && count($transporterData) > 0) {
					$update_array = $result->ProductList;

					foreach ($result->ProductList as $key => $value) {
						if ($value['_id'] == $request->get('productid')) {
							$update_array[$key]['status'] = 'delivered';
							$item_title = $update_array[$key]['product_name'];
							$packageid = $update_array[$key]['package_id'];
							$total = $update_array[$key]['after_update'];
							$shippingCost = $update_array[$key]['shippingCost'];
							$verifyCode = $update_array[$key]['DeliveryVerifyCode'];
							$update_array[$key]['DeliveredTime'] = new MongoDate();
						}
					}

					if ($verifyCode != Input::get('verify_code')) {
						$response = array("success" => 0, "msg" => "The verification code you entered is invalid.");
						echo json_encode($response);die;
					}

					$updateData = Deliveryrequest::where($where)->update(['ProductList' => $update_array]);

					if ($updateData) {
						Reqhelper::update_status2($request->get('requestId'));
						$Email = new NewEmail();
						if ($transporterData->EmailStatus == "on") {


							$Email->send_mail('5694d88a5509251cd67773f0', [
								"to" => $transporterData->Email,
								"replace" => [
									"[USERNAME]" => ucfirst($transporterData->Name),
									"[PACKAGETITLE]" => ucfirst($item_title),
									"[PACKAGEID]" => $packageid,
									"[SOURCE]" => $result->PickupFullAddress,
									"[DESTINATION]" => $result->DeliveryFullAddress,
									"[PACKAGENUMBER]" => $packageid,
									"[SHIPPINGCOST]" => $shippingCost,
									"[TOTALCOST]" => $total,
									"[REQUESTER]"=> ucfirst($requesterData->Name),
								],
							]);

							
						}

						if ($requesterData->EmailStatus == "on") {

							$Email->send_mail('5622492ce4b0d4bc235582bd', [
								"to" => $requesterData->Email,
								"replace" => [
									"[USERNAME]" => ucfirst($requesterData->Name),
									"[PACKAGETITLE]" => ucfirst($item_title),
									"[PACKAGEID]" => $packageid,
									"[SOURCE]" => $result->PickupFullAddress,
									"[DESTINATION]" => $result->DeliveryFullAddress,
									"[PACKAGENUMBER]" => $packageid,
									"[SHIPPINGCOST]" => $shippingCost,
									"[TOTALCOST]" => $total,
									"[REQUESTER]"=> ucfirst($requesterData->Name),
								],
							]);

							
						}

						if ($requesterData->NoficationStatus == "on" && !empty(trim($requesterData->NotificationId))) {


							$Notification = new Notify();
							$Notification->setValue('title', trans('lang.REQUEST_DELIVERED'));
							$Notification->setValue('message', sprintf(trans('lang.MSG_REQUEST_DELIVERED'), $transporterData->Name, ucfirst($item_title)));
							$Notification->setValue('type', 'LOCAL_DELIVERY_USER');
							$Notification->setValue('location', Input::get('requestId'));
							$Notification->setValue('locationkey', Input::get('requestId'));
							$Notification->add_user($requesterData->NotificationId, $requesterData->DeviceType);
							$Notification->fire();

							
						}

						Notification::Insert([
							array(
								"NotificationTitle" => trans('lang.REQUEST_DELIVERED'),
								"NotificationShortMessage" => sprintf(trans('lang.MSG_REQUEST_DELIVERED'), $transporterData->Name, ucfirst($item_title)),
								"NotificationMessage" => sprintf('Transporter "%s" has delivered your package "%s".', $transporterData->Name, ucfirst($item_title)),
								"NotificationType" => "LOCAL_DELIVERY_USER",
								"NotificationUserId" => array(new MongoId($requesterData->_id)),
								"Date" => new MongoDate(),
								"GroupTo" => "User",
							),
							array(
								"NotificationTitle" => trans('lang.SEND_DELIVERED_TITLE_ADMIN'),
								"NotificationMessage" => sprintf(trans('lang.SEND_DELIVERED_ADMIN'), $transporterData->Name, $packageid),
								"NotificationUserId" => array(),
								"NotificationReadStatus" => 0,
								"location" => "request_detail",
								"locationkey" => (string) $request->get('requestId'),
								"Date" => new MongoDate(),
								"GroupTo" => "Admin",
							),
						]);

						$insertactivity = [
							'request_id' => $result->_id,
							'request_type' => $result->RequestType,
							'item_id' => $request->get('productid'),
							'package_id' => $packageid,
							'item_name' => ucfirst($item_title),
							'log_type' => 'request',
							'message' => 'Package has been successfully delivered.',
							'status' => 'delivered',
							'EnterOn' => new MongoDate(),

						];
						Activitylog::insert($insertactivity);
						if (count($updateData) > 0) {
							$response = [
								'success' => 1,
								'msg' => 'Package has been successfully delivered.',
							];
						}
					}
				}
			}
		}
		return response()->json($response);
	}

	public function LocalTransporterDetail(Request $request) {
		$response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		$validate = Validator::make(Input::get(), [
			'user_id' => 'required',
			'item_id' => 'required',
		]);
		if ($validate->fails()) {
			$validate->errors()->add('field', trans('Oops! Something went wrong'));
			return $validate->errors();
		} else {

			$where = [
				"ProductList" => ['$elemMatch' => [
					'_id' => $request->get('item_id'),
				]],
			];

			$result = deliveryRequest::where($where)->select('_id', 'ProductTitle', 'PackageId', 'RequesterName', 'RequesterImage', 'RequestType', 'PublicPlace', 'InsuranceCost', 'RequesterId', 'DeliveryDate', 'FlexibleDeliveryDate', 'PackageMaterialShipped', 'AquantuoFees', 'JournyType', 'ReturnFullAddress', 'NotDelReturnFullAddress', 'DeliveryLatLong', 'PickupLatLong', 'PickupFullAddress', 'ReceiverCountrycode', 'ReceiverMobileNo', 'RequestDate', 'ReturnAddress', 'PickupDate', 'PickupAddress', 'PickupCountry', 'PickupPinCode', 'DeliveryAddress', 'DeliveryCountry', 'ReturnAddress', 'ReturnCountry', 'ReturnPincode', 'DeliveryFullAddress', 'ProductList.$','AreaCharges','ReceiverName','SenderCountrycode','SenderMobileNo')->first();

			//Calculation
			$configurationdata = Configuration::where(['_id' => '5673e33e6734c4f874685c84'])->first();
			$Deliveryrequest = Deliveryrequest::where($where)->select('ProductList')->first();
			$totalItemCount = count($Deliveryrequest->ProductList);
			$averageAreaCharge = number_format($result->AreaCharges / $totalItemCount,2) ;
			$itemCost = $result['ProductList'][0]['after_update'] + $averageAreaCharge;
			$aquantuoFess = ($itemCost / 100 ) * $configurationdata->aquantuo_fees_ld;
			$transporterFess = $itemCost - $aquantuoFess;
			//End calculation


			$data = User::where(['_id' => $result->RequesterId])->first();

			$Transporter = User::where(['_id' => Input::get('user_id')])->select('_id', 'Name')->first();

			$average_rating = 0;
			if ($data->RatingCount > 0 && $data->RatingByCount > 0) {
				$average_rating = $data->RatingCount / $data->RatingByCount;
				//$average_rating = 20 * $rating;
			}

			if ($result->PickupDate != '') {
				$date = $result['PickupDate']->sec;
			} else {
				$date = "";
			}

			if ($result->JournyType == 'one_way') {
				$journy_type = 'One way';
			} elseif ($result->JournyType == 'return') {
				$journy_type = 'Return';
			}

			if (isset($result->AquantuoFees)) {

				$AquantuoFees = $result->AquantuoFees;
			} else {
				$AquantuoFees = 0;
			}

			if ($result['ProductList'][0]['TransporterFeedbcak'] != '') {
				$TransporterFeedbcak = $result['ProductList'][0]['TransporterFeedbcak'];
			} elseif ($result->JournyType == 'return') {
				$TransporterFeedbcak = '';
			}

			if (isset($result['ProductList'][0]['DeliveredTime'])) {
				$deliveryTime = $result['ProductList'][0]['DeliveredTime']->sec;
			} else {
				$deliveryTime = '';
			}

			$data = [
				'_id' => $result->_id,
				'Title' => $result['ProductList'][0]['product_name'],
				'PackageId' => $result['ProductList'][0]['package_id'],
				'RequesterName' => $result->RequesterName,
				'RequesterImage' => $data->Image,
				'TransporterName' => $Transporter->Name,
				

				'ReceiverName'=>$result->ReceiverName,
				'SenderCountrycode'=>$result->SenderCountrycode,
				'SenderMobileNo'=>$result->SenderMobileNo,


				'UserRating' => $average_rating,
				'PickupDate' => $date,
				'RequestType' => $result->RequestType,
				'PickupAddress' => $result->PickupAddress,
				'PickupCountry' => $result->PickupCountry,
				'PickupPinCode' => $result->PickupPinCode,
				'PickupFullAddress' => $result->PickupFullAddress,
				'DeliveryAddress' => $result->DeliveryAddress,
				'DeliveryCountry' => $result->DeliveryCountry,
				'ReturnAddress' => $result->ReturnAddress,
				'ReturnCountry' => $result->ReturnCountry,
				'ReturnPincode' => $result->ReturnPincode,
				'DeliveryFullAddress' => $result->DeliveryFullAddress,
				'ReceiverCountrycode' => $result->ReceiverCountrycode,
				'ReceiverMobileNo' => $result->ReceiverMobileNo,
				'PublicPlace' => $result->PublicPlace,
				'AquantuoFees' => $AquantuoFees,
				'ReturnAddress' => $result->ReturnAddress,
				'PackageMaterialShipped' => $result->PackageMaterialShipped,
				'DeliveryDate' => $result->DeliveryDate->sec,
				'FlexibleDeliveryDate' => $result->FlexibleDeliveryDate,
				'JournyType' => $journy_type,
				'DeliveredTime' => $deliveryTime,
				'NotDelReturnFullAddress' => $result->NotDelReturnFullAddress,
				'ReturnFullAddress' => $result->ReturnFullAddress,
				'InsuranceCost' => $result->InsuranceCost,
				'PickupLat' => $result->PickupLatLong[0],
				'PickupLong' => $result->PickupLatLong[1],
				'DeliveryLat' => $result->DeliveryLatLong[1],
				'DeliveryLong' => $result->DeliveryLatLong[1],
				"ChatUserName" => $result->RequesterName,
				"ChatUserId" => (string) $result->RequesterId,
				"ChatUsrImage" => $data->Image,
				//"PackageMaterialShipped" => $result['ProductList'][0]['PackageMaterialShipped'],
				"TransporterEarnings" => ($result['ProductList'][0]['shippingCost'] - $result->AquantuoFees),
				'Itemid' => $result['ProductList'][0]['_id'],
				'ProductImage' => $result['ProductList'][0]['ProductImage'],
				'package_id' => $result['ProductList'][0]['package_id'],
				'tpid' => $result['ProductList'][0]['tpid'],
				'tpName' => $result['ProductList'][0]['tpName'],
				'product_name' => $result['ProductList'][0]['product_name'],
				'Description' => $result['ProductList'][0]['Description'],
				'PackageMaterial' => $result['ProductList'][0]['PackageMaterial'],
				'Status' => $result['ProductList'][0]['status'],
				'ShowStatus' => get_status_title($result['ProductList'][0]['status'], $result['RequestType'])['status'],
				'travelMode' => @$result['ProductList'][0]['travelMode'],
				'productWidth' => $result['ProductList'][0]['productWidth'],
				'productHeight' => $result['ProductList'][0]['productHeight'],
				'productLength' => $result['ProductList'][0]['productLength'],
				'productWeight' => $result['ProductList'][0]['productWeight'],
				'ProductWidthtUnit' => $result['ProductList'][0]['productHeightUnit'],
				'productHeightUnit' => $result['ProductList'][0]['productHeightUnit'],
				'ProductWeightUnit' => $result['ProductList'][0]['ProductWeightUnit'],
				'ProductLengthUnit' => $result['ProductList'][0]['ProductLengthUnit'],
				'category' => $result['ProductList'][0]['productCategory'],
				'categoryid' => $result['ProductList'][0]['productCategoryId'],
				'productCost' => $result['ProductList'][0]['productCost'],
				'Quantity' => $result['ProductList'][0]['productQty'],
				'shippingCost' => $result['ProductList'][0]['shippingCost'],
				"RequesterPaid" => $result['ProductList'][0]['shippingCost'],
				'InsuranceStatus' => $result['ProductList'][0]['InsuranceStatus'],
				'TransporterFeedbcak' => $result['ProductList'][0]['TransporterFeedbcak'],
				'TransporterRating' => $result['ProductList'][0]['TransporterRating'],
				'RequesterFeedbcak' => $result['ProductList'][0]['RequesterFeedbcak'],
				'RequesterRating' => $result['ProductList'][0]['RequesterRating'],

				'RejectBy' => @$result['ProductList'][0]['RejectBy'],
				'ReturnType' => @$result['ProductList'][0]['ReturnType'],
				'RejectTime' => @$result['ProductList'][0]['RejectTime']->sec,
				'TrackingNumber' => @$result['ProductList'][0]['TrackingNumber'],
				'TransporterMessage' => @$result['ProductList'][0]['TransporterMessage'],
				'ReceiptImage'=>@$result['ProductList'][0]['ReceiptImage'],
				
				
				//calculation
				'request_paid'=>$itemCost,
				'TransporterEarnings'=> $transporterFess,
				'AquantuoFees'=> $aquantuoFess,
				//end calculation

				//	'ReceiptImage' => $result['ProductList'][0]['ReceiptImage'],
				//	'RejectTime' => $result['ProductList'][0]['RejectTime'],
				//	'TransporterMessage' => $result['ProductList'][0]['TransporterMessage'],
				//zip -FF 1558425118_page_43.zip --out New.zip

			];

			if (count($result) > 0) {
				$response = [
					'success' => 1,
					'msg' => 'Request List',
					'result' => $data,
				];
			} else {
				$response = [
					'success' => 1,
					'msg' => 'Oops! Something went wrong',
				];
			}
		}
		return response()->json($response);
	}

	public function myDeliveries() {
		$response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		$validate = Validator::make(Input::get(), [
			'user_id' => 'required',
			'type' => 'required',
			'pageno' => 'required',
		]);
		if ($validate->fails()) {
			$validate->errors()->add('field', trans('Oops! Something went wrong'));
			return $validate->errors();
		} else {

			$query = Deliveryrequest::query();
			if (Input::get('type') == 'current') {
				$status = ['ready', 'accepted', 'out_for_pickup', 'out_for_delivery', 'assign'];
			} else {
				$status = ['delivered', 'cancel'];
			}

			$query->where('ProductList.tpid', Input::get('user_id'))->orwhere('TransporterId', new MongoId(Input::get('user_id')));

			//$query->whereIn('ProductList.status', $status);

			$limit = 20;
			$start = (Input::get('pageno') > 1) ? ($limit * (Input::get('pageno') - 1)) : 0;
			$query->skip($start)->limit($limit);
			$result = $query->orderby('_id', 'desc')->get();

			print_r($result);die;

		}
	}

	public function transporterAssignrequest() {
		$response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		$validate = Validator::make(Input::get(), [
			'user_id' => 'required',
			'type' => 'required',
			'pageno' => 'required',
		]);
		if ($validate->fails()) {
			$validate->errors()->add('field', trans('Oops! Something went wrong'));
			return $validate->errors();
		} else {
			$query = Deliveryrequest::query();

			if (Input::get('type') == 'current') {
				$status = ['ready', 'accepted', 'out_for_pickup', 'out_for_delivery', 'assign'];
			} else {
				$status = ['delivered', 'cancel'];
			}

			$query->whereIn('ProductList.status', $status);
			$query->where('ProductList.tpid', Input::get('user_id'));

			/*$query->where(function ($query) use ($status) {
					$query->whereIn('ProductList.status', $status);
					return $query->where('TransporterId', new MongoId(Input::get('user_id')));
				});

				$query->orwhere(function ($query) use ($status) {
					$query->whereIn('ProductList.status', $status);
					return $query->where('ProductList.tpid', Input::get('user_id'));
			*/

			$limit = 20;
			$start = (Input::get('pageno') > 1) ? ($limit * (Input::get('pageno') - 1)) : 0;
			$query->skip($start)->limit($limit);
			$result = $query->orderby('_id', 'desc')->get();

			$RequesterId = array();
			$Requesterdata = array();
			foreach ($result as $val) {
				$RequesterId[] = $val['RequesterId'];
			}

			$Requesterinfo = User::where(['_id' => ['$in' => $RequesterId]])->get();

			if ($Requesterinfo->count() > 0) {
				foreach ($Requesterinfo as $key) {
					$Requesterdata[(string) $key['_id']] = (Object) $key;
				}
			}

			foreach ($result as $responce) {
				$ChatName = '';
				$ChatUserName = '';
				$ChatUsrImage = '';
				if (isset($Requesterdata[(String) $responce['RequesterId']])) {
					$ChatName = $Requesterdata[(String) $responce['RequesterId']]->ChatName;
					$ChatUserName = $Requesterdata[(String) $responce['RequesterId']]->Name;
					$ChatUsrImage = $Requesterdata[(String) $responce['RequesterId']]->Image;
				}

				if (isset($responce['ProductList'])) {

					foreach ($responce['ProductList'] as $value) {

						if ($value['tpid'] == Input::get('user_id')) {
							if (isset($value['image'])) {
								$image = $value['image'];
							} else {
								if (isset($value['ProductImage'])) {
									if ($value['ProductImage'] != '') {
										$image = $value['ProductImage'];
									}
								} else {
									$image = "";
								}

							}
							if (isset($value['price'])) {
								$price = $value['price'];
							} else {
								$price = '';
							}
							if (isset($value['TripId'])) {
								$tripid = (String) $value['TripId'];
							} else {
								$tripid = "";
							}

							$data[] = [
								'request_id' => $responce->_id,
								'Itemid' => $value['_id'],
								'package_id' => $value['package_id'],
								'status' => $value['status'],
								'show_status' => get_status_title($value['status'], $responce['RequestType'])['status'],
								'item_cost' => $price,
								'tpid' => $value['tpid'],
								'tpName' => $value['tpName'],
								'TripId' => $tripid,
								'product_name' => $value['product_name'],
								'image' => $image,
								'ChatUserId' => (String) $responce->RequesterId,
								'ChatUserName' => $responce->RequesterName,
								'ChatUsrImage' => $ChatUsrImage,
								'PickupLat' => @$responce['PickupLatLong'][1],
								'PickupLong' => @$responce['PickupLatLong'][0],
								'DeliveryLat' => @$responce['DeliveryLatLong'][1],
								'DeliveryLong' => @$responce['DeliveryLatLong'][0],
								'package_type' => $responce->RequestType,
								'Date' => $responce['EnterOn']->sec,
							];
						}

					}
				} else {
					if (empty($responce['ProductList'])) {
						if ($responce['TransporterId'] == Input::get('user_id')) {
							if ($responce['RequestType'] == 'delivery') {
								if (isset($responce['image'])) {
									$image = $responce['image'];
								} else {
									$image = '';
								}
								if (isset($responce['TripId'])) {
									$tripid = (String) $responce['TripId'];
								} else {
									$tripid = "";
								}
								$data[] = [
									'_id' => $responce['_id'],
									'package_id' => $responce['PackageNumber'],
									'status' => $responce['Status'],
									'show_status' => get_status_title($responce['Status'], $responce['RequestType'])['status'],
									'tpid' => $responce['TransporterId'],
									'RequestLocation' => $responce['PickupCity'] . " to " . $responce['DeliveryCity'],
									'tpName' => $responce['TransporterName'],
									'product_name' => $responce['ProductTitle'],
									'image' => $image,
									'tripid' => $tripid,
									'package_type' => $responce['RequestType'],
									'request_id' => $responce->_id,
									'Date' => $responce['EnterOn']->sec,
								];
							}
						}
					}
				}
			}

			if (count($data) > 0) {
				$response = [
					'success' => 1,
					'msg' => 'Request List',
					'result' => $data,
				];
			} else {
				$response = [
					'success' => 1,
					'msg' => 'No recored found',
					'result' => [],
				];
			}
		}
		return response()->json($response);
	}

	public function request_accept_by_transporter(Request $request) {
		$response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		$validate = Validator::make(Input::get(), [
			'user_id' => 'required',
			'productid' => 'required',
			'requestId' => 'required',
			'status' => 'required',
		]);
		if ($validate->fails()) {
			$validate->errors()->add('field', trans('Oops! Something went wrong'));
			return $validate->errors();
		} else {
			$where = [
				"_id" => Input::get('requestId'),
				"ProductList" => ['$elemMatch' => [
					'_id' => Input::get('productid'),
					'status' => 'assign',
				],
				],
			];
			$requestid = Deliveryrequest::where($where)->select('RequesterId', 'Status', 'ProductTitle', 'PackageId', 'PackageNumber', 'shippingCost', 'TotalCost', 'PickupFullAddress', 'DeliveryFullAddress', 'ProductList')->first();

			if (count($requestid) > 0) {
				$requesterData = User::where(array('_id' => $requestid->RequesterId))->
					select('FirstName', 'LastName', 'Email', 'EmailStatus', 'NoficationStatus', 'NotificationId', 'DeviceType')->first();

				$transporterData = User::where(array('_id' => Input::get('user_id')))->select('FirstName', 'LastName', 'Email', 'EmailStatus', 'NoficationStatus', 'NotificationId' . 'DeviceType', 'Name')->first();

				if (count($requesterData) > 0 && count($transporterData) > 0) {

					$update_array = $requestid->ProductList;

					foreach ($requestid->ProductList as $key => $value) {
						if ($value['_id'] == Input::get('productid')) {
							$update_array[$key]['status'] = 'accepted';
							$item_title = $update_array[$key]['product_name'];
							$packageid = $update_array[$key]['package_id'];
							$total = $update_array[$key]['after_update'];
							$shippingCost = $update_array[$key]['shippingCost'];
						}
					}

					$updateData = Deliveryrequest::where(array('_id' => new MongoId(Input::get('requestId'))))->update(['ProductList' => $update_array]);
					if ($updateData) {
						Reqhelper::update_status2(Input::get('requestId'));

						//email to transporter
						/*	$cron_mail = [
							"USERNAME" => ucfirst($transporterData->Name),
							"PACKAGETITLE" => ucfirst($item_title),
							"PACKAGEID" => $packageid,
							"SOURCE" => $requestid->PickupFullAddress,
							"DESTINATION" => $requestid->DeliveryFullAddress,
							"PACKAGENUMBER" => $packageid,
							"SHIPPINGCOST" => $shippingCost,
							"TOTALCOST" => $total,
							'email_id' => '5694ce6e5509251cd67773ed',
							'email' => $transporterData->Email,
							'status' => 'ready',
						];
						SendMail::insert($cron_mail);*/

						//email to requester
						/*	$cron_mail2 = [
							"USERNAME" => ucfirst($requesterData->Name),
							"PACKAGETITLE" => ucfirst($item_title),
							"PACKAGEID" => $packageid,
							"SOURCE" => $requestid->PickupFullAddress,
							"DESTINATION" => $requestid->DeliveryFullAddress,
							"PACKAGENUMBER" => $packageid,
							"SHIPPINGCOST" => $shippingCost,
							"TOTALCOST" => $total,
							'email_id' => '57d7d4b37ac6f69c158b4569',
							'email' => $requesterData->Email,
							'status' => 'ready',
						];

						SendMail::insert($cron_mail2);*/
						//email to admin
						/*	$setting = Setting::find('563b0e31e4b03271a097e1ca');
						if (count($setting) > 0) {
							send_mail('56ab46f95509251cd67773f3', [
								"to" => $setting->SupportEmail,
								"replace" => [
									"[TRANSPORTERNAME]" => $transporterData->Name,
									"[PACKAGETITLE]" => ucfirst($item_title),
									"[PACKAGEID]" => $packageid,
									"[SOURCE]" => $requestid->PickupFullAddress,
									"[DESTINATION]" => $requestid->DeliveryFullAddress,
								],
							]);
						}*/

						//Notification to requester

						/*	if ($requesterData->NoficationStatus == "on" && !empty($requesterData->NotificationId)) {
							$Notification = new Pushnotification();
							$Notification->setValue('title', trans('lang.SEND_ACCEPT_TITLE'));
							$Notification->setValue('message', sprintf(trans('lang.SEND_ACCEPT_MSG'), $transporterData->Name, $item_title));
							$Notification->setValue('type', 'request_detail');
							$Notification->setValue('locationkey', Input::get('requestId'));
							$Notification->add_user($requesterData->NotificationId, $requesterData->DeviceType);
							$Notification->fire();
						}*/
						//insertNotification

						/*Notification::Insert([
							array(
								"NotificationTitle" => trans('lang.SEND_ACCEPT_TITLE'),
								"NotificationShortMessage" => 'Request accepted successfully.',
								"NotificationMessage" => sprintf(trans('lang.SEND_ACCEPT_MSG'), $transporterData->Name, $item_title),
								"NotificationType" => "request",
								"NotificationUserId" => array(new MongoId($requesterData->_id)),
								"Date" => new MongoDate(),
								"GroupTo" => "User",
							),
							array(
								"NotificationTitle" => 'Request was accepted',
								"NotificationMessage" => sprintf(trans('lang.SEND_ACCEPT_MSG_ADMIN'), $transporterData->Name, $requestid->PickupFullAddress, $requestid->DeliveryFullAddress),
								"NotificationUserId" => array(),
								"NotificationReadStatus" => 0,
								"location" => "request_detail",
								"locationkey" => (string) Input::get('requestid'),
								"Date" => new MongoDate(),
								"GroupTo" => "Admin",
							),
						]);*/

						if (count($updateData) > 0) {
							$response = [
								'success' => 1,
								'msg' => 'Request has been accepted successfully.',
							];
						}
					}
				}
			}
		}
		return response()->json($response);
	}

	public function LocalTransporterRating(Request $request) {
		$response = array("success" => 0, "msg" => "Oops! Something went wrong.");
		$validate = Validator::make(Input::get(), [
			'user_id' => 'required',
			'productid' => 'required',
			'requestId' => 'required',
			'rating' => 'required',
			'feedback' => 'required',
		]);
		if ($validate->fails()) {
			$validate->errors()->add('field', trans('Oops! Something went wrong'));
			return $validate->errors();
		} else {
			$where = [
				"_id" => Input::get('requestId'),
				"ProductList" => ['$elemMatch' => [
					'_id' => $request->get('productid'),
					'status' => 'delivered',
					"tpid" => Input::get('user_id'),
				],
				],
			];
			$del_info = Deliveryrequest::where($where)->first();
			if (count($del_info) > 0) {
				$update_array = $del_info->ProductList;
				foreach ($del_info->ProductList as $key => $value) {
					if ($value['_id'] == $request->get('productid')) {
						$update_array[$key]['TransporterRating'] = Input::get('rating');
						$update_array[$key]['TransporterFeedbcak'] = Input::get('feedback');
					}
				}
				$del_info->ProductList = $update_array;
				if ($del_info->save()) {
					$requester_rating = User::find($del_info->RequesterId);
					$requester_rating->RatingCount = $requester_rating->RatingCount + $request->get('rating');
					$requester_rating->RatingByCount = $requester_rating->RatingByCount + 1;
					$requester_rating->save();
					$response = [
						'success' => 1,
						'msg' => 'Thanks for your feedback.',
					];
				}
			}
		}
		return response()->json($response);
	}

	public function localCancelDelivery() {
		$response = array("success" => 0, "msg" => "Oops! Something went wrong.");
		$validate = Validator::make(Input::get(), [
			'user_id' => 'required',
			'productid' => 'required',
			'requestId' => 'required',
			'rejectBy' => 'required',
			'returnType' => 'required',
			'trackingNumber' => 'required',
			'message' => 'required',
			//	'image' => 'required',
		]);
		if ($validate->fails()) {
			$validate->errors()->add('field', trans('Oops! Something went wrong'));
			return $validate->errors();
		} else {
			$where = ["ProductList" => ['$elemMatch' => ['_id' => Input::get('productid'), 'status' => 'out_for_delivery']]];

			$deliveryData = Deliveryrequest::where($where)
				->select('ProductList.$', 'ProductTitle', 'RequesterId')
				->first();

			$setting = Setting::find('563b0e31e4b03271a097e1ca');
			if (count($deliveryData) > 0) {

				$requesterData = User::where(array('_id' => $deliveryData->RequesterId))
					->select('Email', 'EmailStatus', 'NoficationStatus', 'NotificationId', 'DeviceType', 'Name')
					->first();

				$transporterData = User::where(array('_id' => Input::get('user_id')))
					->select('Name', 'Email', 'EmailStatus', 'NoficationStatus', 'NotificationId' . 'DeviceType')
					->first();

				if (count($requesterData) > 0 && count($transporterData) > 0) {

					$updData = [
						'ProductList.$.status' => 'cancel',
						'ProductList.$.RejectBy' => Input::get('rejectBy'),
						'ProductList.$.ReturnType' => Input::get('returnType'),
						"ProductList.$.RejectTime" => new MongoDate(),
						'ProductList.$.TrackingNumber' => Input::get('trackingNumber'),
						'ProductList.$.TransporterMessage' => Input::get('message'),
						"ProductList.$.UpdateOn" => new MongoDate(),
					];

					if (isset($_FILES['receipt']['name']) && @$_FILES['receipt']['name'] != '') {
						$exts = explode('.', $_FILES['receipt']['name']);
						$ext = $exts[count($exts) - 1];
						$receipt = "receipt" . rand(2154, 45454) . time() . ".$ext";
						if (in_array(strtolower($ext), ['gif', 'png', 'jpg', 'jpeg'])) {
							$newpath = BASEURL_FILE . "package/{$receipt}";
							if (move_uploaded_file($_FILES['receipt']['tmp_name'], $newpath)) {
								$updData['ProductList.$.ReceiptImage'] = "package/{$receipt}";
							}
						}
					}
					$notifymsg = sprintf('Your package "%s" has been canceled by %s', $deliveryData->ProductList[0]['product_name'], $requesterData['Name']);

					$emailReasonforCancaletion = "Rejectby : " . ucfirst(Input::get('rejectBy')) . ", Retrun type: " . get_cancel_status(Input::get('returnType'));
					if (Input::get('returnType') == 'ipayment') {
						$notifymsg = sprintf(trans('lang.BUYFORME_CANCELED_MSG_IPAY'),
							$deliveryData->ProductList[0]['product_name'],
							@$setting->SupportEmail
						);
					} else if (strtolower(Input::get('returnType')) == 'creturn' && strtolower(Input::get('Rejectby')) == 'requester') {

						if (Input::get('date') != '') {
							$date = date_create_from_format('M d, Y h:i:s A', Input::get('date'));
							if ($date) {

								$updData['ProductList.$.CancelReturnDate'] = new MongoDate(strtotime(date_format($date, 'd-m-Y H:i:s')));
							}
						}

						$notifymsg = sprintf(trans('lang.BUYFORME_CANCELED_MSG_CRE'), $deliveryData->ProductList[0]['product_name'], $updData['ProductList.$.TransporterMessage']);

					} else if (Input::get('returnType') == 'osreturn' && strtolower(Input::get('Rejectby')) == 'requester') {

						$notifymsg = sprintf(trans('lang.BUYFORME_CANCELED_MSG_OSCRE'),
							$deliveryData->ProductList[0]['product_name'],
							$transporterData->Name,
							$updData['ProductList.$.TransporterMessage']
						);
					}
					$updateData = Deliveryrequest::where($where)->update($updData);
					if (count($updateData) > 0) {
						ReqHelper::update_status2($deliveryData->_id);
						$response = [
							'success' => 1,
							'msg' => 'Success! Request has been canceled successfully.',
						];
					}
					//email to transporter
					/*if ($transporterData->EmailStatus == "on") {
						send_mail('58762b057ac6f6f2128b4567', [
							"to" => $transporterData->Email,
							"replace" => [
								"[USERNAME]" => $transporterData->Name,
								"[PACKAGETITLE]" => $deliveryData->ProductList[0]['product_name'],
								"[REASON]" => $emailReasonforCancaletion,
								"[PACKAGEID]" => $deliveryData->ProductList[0]['package_id'],
							],
						]);
					}*/

					//email to requester
					$Email = new NewEmail();
					if ($requesterData->EmailStatus == "on") {
						$Email->send_mail('588b56736befd90b9d7ea797', [
							"to" => $requesterData->Email,
							"replace" => [
								"[USERNAME]" => $requesterData->Name,
								'[PACKAGETITLE]' => $deliveryData->ProductList[0]['product_name'],
								"[REASON]" => $emailReasonforCancaletion,
								"[PACKAGEID]" => $deliveryData->ProductList[0]['package_id'],
							],
						]);
					}

					$Notification = new Notify();

					//Notification to requester
					if ($requesterData->NoficationStatus == "on" && !empty(trim($requesterData->NotificationId))) {
						$Notification->setValue('title', trans('lang.BUYFORME_CANCELED_TITLE'));
						$Notification->setValue('message', $notifymsg);
						$Notification->setValue('type', 'LOCAL_DELIVERY_USER');
						$Notification->setValue('location', Input::get('requestId'));
						$Notification->setValue('locationkey', Input::get('requestId'));
						$Notification->add_user($requesterData->NotificationId, $requesterData->DeviceType);
						$Notification->fire();
					}
					//insertNotification
					Notification::Insert([
						array(
							"NotificationTitle" => 'Your request has been canceled',
							"NotificationShortMessage" => $notifymsg,
							"NotificationMessage" => $notifymsg,
							"NotificationType" => "LOCAL_DELIVERY_USER",
							"NotificationUserId" => array(new MongoId($requesterData->_id)),
							"Date" => new MongoDate(),
							"GroupTo" => "User",
						),
						array(
							"NotificationTitle" => 'Request canceled',
							"NotificationMessage" => $notifymsg,
							"NotificationReadStatus" => 0,
							"location" => "request_detail",
							"locationkey" => (string) Input::get('requestId'),
							"Date" => new MongoDate(),
							"GroupTo" => "Admin",
						),
					]);

				}
			}
		}
		return response()->json($response);
	}

	public function LocalMaterialStatus() {
		$response = array("success" => 0, "msg" => "Oops! Something went wrong.");
		$validate = Validator::make(Input::get(), [
			'user_id' => 'required',
			'productid' => 'required',
			'PackageMaterialShipped' => 'required',
		]);
		if ($validate->fails()) {
			$validate->errors()->add('field', trans('Oops! Something went wrong'));
			return $validate->errors();
		} else {
			$where = ["ProductList" => ['$elemMatch' => ['_id' => Input::get('productid')]]];
			$deliveryData = Deliveryrequest::where($where)->get();

			$deliveryData = Deliveryrequest::where($where)
				->update(['ProductList.$.PackageMaterialShipped' => Input::get('PackageMaterialShipped')]);

			if (count($deliveryData) > 0) {
				$response = [
					'success' => 1,
					'msg' => 'Package Material has been updated successfully.',
				];
			}
		}
		return response()->json($response);
	}

	public function transporterlist() {
		$response = array("success" => 0, "msg" => "Oops! Something went wrong.");
		$validate = Validator::make(Input::get(), [
			'user_id' => 'required',
			//'productid' => 'required',
			//'requestid' => 'required',
			//'PackageMaterialShipped' => 'required',
		]);
		if ($validate->fails()) {
			$validate->errors()->add('field', trans('Oops! Something went wrong'));
			return $validate->errors();
		} else {

			$result = Deliveryrequest::where('ProductList.tpid', Input::get('user_id'))->get();

			if (isset($result->ProductList)) {
				$ProductArray = $result->ProductList;
			} else {
				$ProductArray = '';
			}
			echo "<pre>";
			print_r($ProductArray);die;

		}
	}
}
