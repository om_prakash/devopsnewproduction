<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1 user-scalable=no">
      <meta name="description" content="A growing trustworthy community of Transporters and Requesters, Aquantuo bridges the gap between the person wanting to have an item delivered to another location and the person or company willing to do so.">
	  <meta name="keywords" content="Aquantuo, Aq, Delivery, Shipping, Package, Ghana, USA States, Transporter, Requester, app">
	  <meta name="author" content="Clement">
	  <meta http-equiv="refresh" content="3000">
	  <link rel="shortcut icon" type="image/png" href="theme/web/promo/images/favicon.png" />

      <title>Aquantuo</title>
      <link href="theme/web/promo/images/favicon.png" type="image/png" rel="icon">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

      <!-- Bootstrap -->
      {!! Html::style('theme/web/promo/css/bootstrap.min.css') !!}
<!--      {!! HTML::style('theme/web/promo/css/style.css') !!}-->
      {!! Html::style('theme/web/promo/css/style.min.css') !!}
      {!! Html::style('theme/web/promo/css/font-awesome.css') !!}
      <!-- <link href="css/bootstrap.min.css" rel="stylesheet">
         <link href="css/style.css" rel="stylesheet">
         <link href="css/font-awesome.css" rel="stylesheet"> -->
      <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
        <style>
        	.heading-c3{ font-weight:bold; color:#219ed4; margin-top:30px; margin-bottom:10px;}
			p{margin-bottom:20px;}
			ul{margin-top:10px; }
			li{margin-bottom:5px; }
			
        </style>
    </head>
    <body>

        <div class="row">
            <div class="container-fluid">
                <!--parallax 1 -->
                <div class="">
                    <nav class="navbar navbar-default nav-bg navbar-fixed-top responsive" style="">
                        <div class="container">
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                </button>
                                <a class="navbar-brand" href="#"><img src="{{asset('theme/web/promo/images/logo.png')}}">
                                </a>
                            </div>
                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <ul class="nav navbar-nav navbar-right">
                                    <li><a href="{{url('/')}}#home">Home</a>
                                    </li>
                                    <li><a href="{{url('/')}}#aboutapp">About App</a>
                                    </li>
                                    <li><a href="{{url('/')}}#features">Features</a>
                                    </li>
                                    <li><a href="{{url('/')}}#download">Download</a>
                                    </li>
                                    <li><a href="{{url('/')}}#contact">Contact</a>
                                    </li>
                                </ul>
                            </div>
                            <!-- /.navbar-collapse -->
                        </div>
                        <!-- /.container-fluid -->
                    </nav>
                    <div class="row">
                        <div class="container-fluid section-custom">
                            <div class="container">
                                <div class="heading2">Privacy Policy</div>
                                <?php echo $content['Content']; ?>
                                <br>          
                            </div>
                        </div>
                    </div>                    <div class="row">
                        <div class="container-fluid section-custom footer">
                            <div class="container">
                                <div class="col-md-12 col-sm-12 customf">
                     <div class="row">
                        <div class="col-md-6 col-sm-6">
                           <div class="heading3 offset-btm-20">About</div>
                           <br>
                           <img class="footer-logo" width="50%" src="theme/web/promo/images/white_logo.png">
                           <p class="color-white">A growing trustworthy community of Transporters and Requesters, Aquantuo bridges the gap between the person wanting to have an item delivered to another location and the person or company willing to do so.</p>
                        </div>
                        <div class="col-md-3 col-sm-3 sitemap-style">
                           <div class="heading3 offset-btm-20">Site map</div>
                           <br>
                           
                           
                           
                           <ul  style="margin-top:0px;" class="footer-list">
                              <li style="margin-bottom:0px;"><a href="{{url('/')}}#home">- Home</a>
                                                </li>
                                                <li style="margin-bottom:0px;"><a href="{{url('/')}}#aboutapp">- About App</a>
                                                </li>
                                                <li style="margin-bottom:0px;"><a href="{{url('/')}}#features">- Features</a>
                                                </li>
                                                <li style="margin-bottom:0px;"><a href="{{url('/')}}#download">- Download</a>
                                                </li>
                                                <li style="margin-bottom:0px;"><a href="{{url('/')}}#contact">- Contact</a>
                                                </li>
                           </ul>
                        </div>
                        <div class="col-md-3 col-sm-3">
                           <div class="heading3 offset-btm-20">Social media</div>
                           <br>
                           <div class="social-media clearfix">
                              <div class="col-md-4 col-sm-4 col-xs-4">
                                 <a href="https://www.facebook.com/Aquantuo" target="_blank">
                                    <div class="social-icon-facebook">
                                       <i class="fa fa-facebook"></i>
                                    </div>
                                 </a>
                              </div>
                              <div class="col-md-4 col-sm-4 col-xs-4">
                                 <a href="https://twitter.com/Aquantuo"  target="_blank">
                                    <div class="social-icon-twitter">
                                       <i class="fa fa-twitter"></i>
                                    </div>
                                 </a>
                              </div>
                              <div class="col-md-4 col-sm-4 col-xs-4">
                                 <a href="https://www.instagram.com/Aquantuo/" target="_blank">
                                    <div class="social-icon-instagram">
                                       <i class="fa fa-instagram"></i>
                                    </div>
                                 </a>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                            </div>
                            <div class="second-f clearfix">
                                <div class="col-xs-12 text-center">
                                    Copyright &copy; 2016 Aquantuo&nbsp;&nbsp; | &nbsp;&nbsp;
                                    
                                    <a href="{{url('terms-and-conditions')}}">Terms and conditions</a> &nbsp;&nbsp; | &nbsp;&nbsp; 
                                    <a href="{{url('privacy-policy')}}">Privacy Policy</a>
                                </div>
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        {!! Html::script('theme/web/promo/js/bootstrap.min.js') !!}
         {!! Html::script('theme/web/promo/js/waypoints.min.js') !!}
        {!! Html::script('theme/web/promo/js/jquery.counterup.js') !!} 
        {!! Html::script('theme/web/promo/js/modernizr-2.6.2.min.js') !!} 
        {!! Html::script('theme/web/promo/js/main.js') !!} 
        {!! Html::script('theme/web/promo/js/custom.js') !!} 
        {!! Html::script('theme/web/promo/js/slider.js') !!}
        <script>
            window.jQuery || document.write('<script src="js/jquery-1.9.1.min.js"><\/script>')
        </script>
    </body>
</html>

