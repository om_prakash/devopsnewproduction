@extends('Page::layout.one-column-page')
@section('content')
{!! Html::script('theme/web/rating/jquery.rateyo.js') !!}    
{!!Html::style('theme/web/rating/jquery.rateyo.min.css')!!}
<style>
.modal-feedback{
  margin:30px auto;
  width:420;
  position: relative;

}
</style>
<div class="container">
<div class="row">
<div class="col-sm-12">
<h2 class="color-blue">Delivery Details</h2>
<br />
<div id="success-message"></div>
</div>

     @foreach($transporterDetail as $user)

   <div class="col-md-12">
      <div class="box-shadow">
         <div class="content-section clearfix">

         
            
            <div class="col-md-2 col-sm-3">
               @if($user->ProductImage != '')
              <a class="fancybox" rel="group" href="{{ ImageUrl.$user->ProductImage}}" >
                <img src="{{ ImageUrl.$user->ProductImage}}" class="margin-top" width="150px" height="150px" >
              </a>        
            @else
                <img src="{{ ImageUrl}}no-image.jpg" width="150px" class="margin-top" height="150px"  class="img-rounded">
            @endif

              

            </div>
            <div class="col-md-10 col-sm-9">
            <span class="pull-right"></i>
          <a href="{{url('transporter')}}"  class="btn btn-default blue-btn">Back</a>&nbsp;&nbsp;</span>
               <h4><b>{{ucfirst($user->ProductTitle)}}</b></h4>
               <p class="color-black" id="status_div"><strong>Status:</strong>
        				@if($user->Status == 'out_for_pickup')
        					Out for Pickup
                        @elseif($user->Status == 'out_for_delivery')
        					Out for Delivery
        				@else
        					{{ucfirst($user->Status)}}
                @endif
                </p>
				<p class="color-black" style="display:none;" id="ajax_status"><strong>Status:</strong></P>
               
               <div class="form-group">
            

              @if($user->Status == "ready")    
               
                  <button  class="btn btn-default blue-btn"  title="Accept" id="accept_request"  data-target="#exampleModal1" data-toggle="modal"> 
                  &nbsp; Accept &nbsp;&nbsp;</button>
                @endif

                <?php $btn_status = 'display:none'; 
                 if(in_array($user->Status, ['accepted','out_for_pickup'])) { $btn_status = 'display:block';  } ?>        
                 <div class="col-sm-2 row" style="{{$btn_status}}">
                  <button  class="btn btn-danger"  title="Cancel" id="cancel_request" onclick="return confirm('Are you sure you want to cancel this request?')? cancel_request():'';" > 
                  &nbsp; Cancel &nbsp;&nbsp;</button>
                </div>
                

                <?php $btn_status = 'display:none'; 
                 if($user->Status == "accepted") { $btn_status = 'display:block';  } ?>
                      
                  <button  class="btn btn-default blue-btn"  title="Out for pickup" id= "out_for_picup" onclick="return confirm('Are you sure you want to pickup this request?')? out_for_pickup():'';" style="{{$btn_status}}"> 
                  &nbsp; Out for Pickup &nbsp;&nbsp;</button>

				

                 <?php $btn_status = 'display:none'; 
				          if($user->Status == "out_for_pickup") { $btn_status = 'display:block';  } ?>
				          
                     <button  class="btn btn-default blue-btn"  title="Out for delivery" id= "out_for_delivery"  onclick="return confirm('Are you sure You want to deliver this request?')? out_for_deliver():'';" style="{{$btn_status}}"> 
                  &nbsp; Out for Delivery &nbsp;&nbsp;</button>
                  
                  
				        <?php $btn_status = 'display:none'; 
				        if($user->Status == "out_for_delivery") { $btn_status = 'display:block';} ?>
                 <table><tr>
				         <td>
					 

                  <button class="btn btn-default blue-btn" href="javascript:void(0)" data-toggle="modal" data-target="#deliver_model" title="delivery"   id="deliver" style="{{$btn_status}}"> &nbsp;Delivered&nbsp;</button>

                  
				        </td>
				        <td>&nbsp;</td>
				          <td>
                    <button class="btn btn-default blue-btn" href="javascript:void(0)" data-toggle="modal" data-target="#exampleModal" title="Cancel Delivery"   id="delivery_btn" style="{{$btn_status}}"> &nbsp;No Delivery</button>
				        </td></tr>
				        </table>
				        <?php $btn_status = 'display:none'; 
				        if($user->Status == "delivered" && $user->TransporterRating == '') { $btn_status = 'display:true';  } ?>
                 <a href="javascript:void(0)" data-toggle="modal" data-target="#modal2" title="Review"  class="btn btn-default blue-btn" id= "feedback" style="{{$btn_status}}">Give Feedback</a>
                 
                                                                                                   
               </div>
            </div>
         </div>
      </div>
   </div>


  <div class="col-sm-4">
  <div class="clearfix detail-tab">
    <div class="tab-content box-shadow margin-top">
<div class="content-section clearfix">
  <div class="col-md-12"> <h4><b>Requester Details</b></h4> <br></div>
	<div class="col-md-4">
	<?php $file_exists = false; ?>
  @if(count($user_data) > 0)
				@if(!empty($user_data->Image))
					@if(file_exists(BASEURL_FILE.'/user/'.$user_data->Image))
						<?php $file_exists = true?>
						  <a class="fancybox" rel="group" href="{{ImageUrl}}/user/{{$user_data->Image}}"> 	
               	<img width="100px" height="100px" class="img-circle" src="{{ImageUrl}}/user/{{$user_data->Image}}">
              </a>
					@endif
        @endif        
    @endif
    @if(!$file_exists)
      <img width="100px" height="100px" class="img-circle" src="{{ImageUrl}}/no-image.jpg">
    @endif
	</div>
<div class="col-md-8">
	  

@if($user->Status == "ready")  
<p><b>{{ucfirst($user_data->FirstName)}}</b></p>
@else
<p><b>{{ucfirst($user_data->Name)}}</b></p>
@endif



<div class="row">
	@if(count($user_data) > 0)
	<?php	$average_rating = 0;
			if($user_data->RatingCount > 0 && $user_data->RatingByCount > 0)
			{
				$rating = $user_data->RatingCount/$user_data->RatingByCount;
				$average_rating = 20 * $rating;
			}
	?>
       <div class="col-sm-12">
			<div class="star-ratings-sprite pull-left">
			  <span style="width:<?php echo $average_rating?>%" class="star-ratings-sprite-rating"></span>
			</div>
       </div>
    @endif
   </div>
</div>
</div>

<div class="row">
		@if(!$user->TransporterFeedbcak == '')
		  <div class="col-sm-12">
			<h4><b>Your Feedback</b></h4>
			<p>{{ucfirst($user->TransporterFeedbcak)}}</p>
			<?php $transporter_rating = 0;
				if(count($user->TransporterRating) > 0)
				{
					$transporter_rating =20 *$user->TransporterRating;

				}

			?>
			<div class="star-ratings-sprite pull-left">
			  <span style="width:<?php echo $transporter_rating;?>%" class="star-ratings-sprite-rating"></span>
			</div>
		  </div>
		
			<div class="col-sm-12">
			<hr>
			</div>
		@endif
		@if(!$user->RequesterFeedbcak == '')
		  <div class="col-sm-12">
			<h4><b>Requester Feedback</b></h4>
			<p>{{ucfirst($user->RequesterFeedbcak)}}</p>
			<?php $requester_rating = 0;
				if(count($user->RequesterRating) > 0)
				{
					$requester_rating =20 *$user->RequesterRating;

				}

			?>
			<div class="star-ratings-sprite pull-left">
			  <span style="width:<?php echo $requester_rating;?>%" class="star-ratings-sprite-rating"></span>
			</div>
			<div class="col-sm-12">
			<hr>
			</div>
		  </div>
		@endif
</div>
    </div>
  </div>
</div>

   <div class="col-sm-8">
      <div class="clearfix margin-top detail-tab">
         <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active" ><a href="#profile" aria-controls="profile" class="active" role="tab" data-toggle="tab">Delivery Information</a></li>
            <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Package Information</a></li>
         </ul>
        
         <div class="tab-content box-shadow">
            <div role="tabpanel" class="tab-pane active" id="profile">
			<div class="details-list">	
            <p><b>Pickup Location : </b> {{ucfirst($user->PickupFullAddress)}} </p>
             
            </div>
            <div class="details-list">	
            <p><b>This is a Public Place :</b> {{ucfirst($user->PublicPlace)}} </p>
             @if($user->PublicPlace == 'yes')
             The Requester has opted to meet in public
             @endif 
             </div>
             <div class="details-list">	
            <p><b>Drop of Location :</b> {{ucfirst($user->DeliveryFullAddress)}}</p>
              {{date('d M ,Y  H:i A ',$user->DeliveryDate->sec)}}
            </div>
            <div class="details-list">  
            <p><b>Is Date Flexible :</b> {{ucfirst($user->FlexibleDeliveryDate)}} </p>
              
            </div>
            
            <?php if($user->Status != 'ready'){?>
              <div class="details-list">  
              <p><b>Receiver Phone Number :</b> 
              @if($user->ReceiverMobileNo != '')

               + {{$user->ReceiverCountrycode}} - {{$user->ReceiverMobileNo}} </p>
              @endif
              </div>
              <?php }?>
            
          


            <div class="details-list">	
            
            @if($user->JournyType == 'one_way')
            </div>
            <div class="details-list">	
            <p><b>Journy Type : </b>One way</p>
            </div>
            @endif
            @if($user->JournyType != 'one_way') 
            <div class="details-list">	
            <p><b>Return Address </b> {{ucfirst($user->ReturnAddress)}}    </p> 
            </div>
            @endif
            <div class="details-list">	
            <p><b>Travel Mode : </b>{{ucfirst($user->TravelMode)}}</p>
            </div>
            <div class="details-list">	
            <p><b>Return address (if item is not delivered) : </b> {{ucfirst($user->NotDelReturnFullAddress)}}</p>			</div>
            </div>
            <div role="tabpanel" class="tab-pane" id="messages">
            </br>                   
            <p><b>Package Value :</b> ${{ number_format((float)$user->ProductCost,2) }}</p>
            <p><b>Weight :</b> 
            
            <?php echo number_format(change_unit
                       ($user->ProductWeight,$user->ProductWeightUnit,'kg'),0).' Kg';?> / 
            <?php echo number_format(change_unit
                       ($user->ProductWeight,$user->ProductWeightUnit,'lbs'),0).' Lbs';?>   

            </p>
            <p><b>Package Dimensions :</b>
              <?php echo "L-"; echo number_format(change_unit
              ($user->ProductLength,$user->ProductLengthUnit,'inch'),0).'Inch';?>,
              <?php  echo "H-"; echo number_format(change_unit
                  ($user->ProductHeight,$user->ProductHeightUnit,'inch'),0).'Inch';?>,
              <?php echo "W-"; echo number_format(change_unit
                  ($user->ProductWidth,$user->ProductWidthUnit,'inch'),0).'Inch'; ?>/
              <?php echo "L-"; echo number_format(change_unit
                    ($user->ProductLength,$user->ProductLengthUnit,'cm'),0).'Cm'; ?>,
              <?php echo "H-"; echo number_format(change_unit
                    ($user->ProductHeight,$user->ProductHeightUnit,'cm'),0).'Cm'; ?>,
              <?php echo "W-"; echo number_format(change_unit
                    ($user->ProductWidth,$user->ProductWidthUnit,'cm'),0).'Cm'; ?>   

            </p>
            @if($user->BoxQuantity == '0')
            <p><b>Quantity :</b> 1 Box(es)</p>
            @else
            <p><b>Quantity :</b> {{$user->BoxQuantity }} Box(es)</p>
            @endif
            <p><b>Description :</b> {{ucfirst($user->Description) }}</p>
            <!--<p><b>Package Care Note :</b> {{ucfirst($user->PackageCareNote)}}</p>-->
            <p><b>Insurance : </b> {{ucfirst($user->InsuranceStatus)}}</p>
            <p><b>Need Package Material :</b> {{ucfirst($user->PackageMaterial)}}
             @if($user->PackageMaterial == 'yes')
              <p>Package Material Shipped  </p>
            @endif
             <div class="list-footer">
                            <div class="row">
								<div class="col-sm-4">                               
            
            
            
            <p><b>Shipping Cost : </b> 
               <span class="pull-right"> ${{ number_format((float)$user-> ShippingCost,2) }} </span></p>
            
            <p><b>Aquantuo Fees : </b> 
                <span class="pull-right">${{ number_format((float)$user->AquantuoFees,2) }} </p>
            <?php $result =  ($user->ShippingCost) - ($user->AquantuoFees); ?>  </span>
            <p><b>Your Income : </b>
               <span class="pull-right"> ${{ number_format((float)$result,2) }} </span></p>
            
                            </div>
                            </div>
                        </div>
            </div>
         </div>
      </div>
   </div>

  

</div>
<div class="col-sm-12">
      <div class=" box-shadow">
        <h2>Other Images</h2>
        @foreach($user->OtherImage  as $key )
          @if(!empty(trim($key)))
            @if(file_exists(BASEURL_FILE.$key))
              <div class="col-sm-2">
              
              <a class="fancybox" rel="group" href="{{ ImageUrl.$key}}" >
                <img src="{{ ImageUrl.$key}}" width="100px" height="100px" class="image-style">
              </a>
            </div>
          @endif  
          @endif
        @endforeach
        @if(count($user->OtherImage) <= 0)
          <center><h4>No other image uploaded</h4></center>
        @endif
        <div class="clearfix"></div>
        <br /><br />
      </div>
   </div>



<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
        <div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="exampleModalLabel">No Delivery</h4>
				</div>
          
		{!! Form::model('', ['name' => 'ServicesForm', 'id' => 'ServicesForm', 'method' => 'POST','files' => true] ) !!}
				<div class="modal-body">
              
				   <div class="form-group" id="valid-error">
					{!! Form::label('title', 'Title:',['class' => 'control-label']) !!}
					{!! Form::text('title', '', ['class'=>'form-control required', 'placeholder'=> 'Title','id' =>'title','maxlength' => 100]) !!}
                    <p class="help-block red" id='er_title'></p> 
                     @if($errors->has('title'))
                      <div class="error" style="color:red">{{ $errors->first('title') }}</div>
                     @endif
                    </div>

                    <div class="form-group" id="valid-error">
					{!! Form::label('package_id', 'Package ID:',['class' => 'control-label']) !!}
					{!! Form::text('package_id', '', ['class'=>'form-control required numeric', 'placeholder'=> 'Package Id','id' =>'package_id','maxlength' => 50]) !!}
                    <p class="help-block red" id='er_package_id'></p> 
                     @if($errors->has('package_id'))
                      <div class="error" style="color:red">{{ $errors->first('package_id') }}</div>
                     @endif
                    </div>

                    <div class="form-group" id="valid-error">
					{!! Form::label('return_address', 'Return Address:',['class' => 'control-label']) !!}
					{!! Form::textarea('return_address','',['class'=>'form-control required', 'placeholder'=> 'Return Address','id' =>'return_address','maxlength' =>200,'size' =>'10x5']) !!}
                    <p class="help-block red" id='er_return_address'></p> 
                     @if($errors->has('return_address'))
                      <div class="error" style="color:red">{{ $errors->first('return_address') }}</div>
                     @endif
                    </div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group" id="valid-error">

							<label class="controle-label">Reject By:</label><br>
                           <input type="radio" name="reject_by" value="transporter" <?php echo'checked=="checked"';?> onclick="return select_transporter()">&nbsp;Transporter
                           
                           &nbsp; &nbsp;
                           
                           <input type="radio" name="reject_by" value="requester" onclick="return select_requester()">&nbsp;Requester
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group" id="valid-error">
							<label class="controle-label">Return Type:</label><br>
							<input type="radio" name="return_type" value="os_return" <?php echo'checked=="checked"';?> onclick="return select_os()">&nbsp;OS Return
                           
                           &nbsp; &nbsp;
                           <span id="bycreturn" style="display:none;">
                           <input type="radio" name="return_type" value="by_creturn" onclick="return select_bycreturn()" >&nbsp;Bycreturn
                           </span>
                           <span id="ipayment">
                           <input type="radio" name="return_type" value="i_payment" onclick="return select_ipayment()">&nbsp;iPayment
							</span>
							
							</div>
						</div>
					</div>

                    <div class="form-group" id="msg_div">
						{!! Form::label('message', 'Message:',['class' => 'control-label']) !!}
						{!! Form::textarea('message','',['class'=>'form-control required', 'placeholder'=> 'Message','id' =>'message','maxlength' =>200,'size' =>'10x5']) !!}
						<p class="help-block red" id='er_message'></p> 
						 @if($errors->has('message'))
						  <div class="error" style="color:red">{{ $errors->first('message') }}</div>
						 @endif
                    </div>

                    <div class="form-group" id="tracking_div">
					{!! Form::label('tracking_number', 'Tracking Number:',['class' => 'control-label']) !!}
					{!! Form::text('tracking_number', '', ['class'=>'form-control required numeric', 'placeholder'=> 'Tracking Number','id' =>'tracking_number','maxlength' => 20]) !!}
                    <p class="help-block red" id='er_tracking_number'></p> 
                     @if($errors->has('tracking_number'))
                      <div class="error" style="color:red">{{ $errors->first('tracking_number') }}</div>
                     @endif
                    </div>


					<div class="form-group" id="date_div" style="">
							{!! Form::label('Date', 'Date:',['class' => 'control-label']) !!}
							{!!Form::text('date','',['class'=>'form-control required','readonly'=>'readonly','id'=>'date'])!!}
							<p class="help-block red" id='er_date'></p>
							@if($errors->has('date'))
							<div class="error" style="color:red">{{ $errors->first('date') }}</div>
							@endif
					</div>

                    
					<div class="form-group" id="receipt_div">
							{!! Form::label('image', 'Upload Receipt:',['class' => 'control-label']) !!}
							{!! Form::file('image', '', ['class'=>'form-control', 'placeholder'=> '','id'=>'image','width'=>'250px','height'=>'250px'])!!}
							<p class="help-block red" id='er_image'></p>
							@if($errors->has('image'))
							<div class="error" style="color:red">{{ $errors->first('image') }}</div>
							@endif
					</div>
			</div>
          <div class="modal-footer">
				{!! Form::button('Close', ['class' => 'btn btn-default', 'data-dismiss'=>'modal']) !!}  
				{!! Form::submit('Submit', ['class' => 'btn btn-primary','id'=>'cancel_delivery_submit']) !!}
          </div>
         
			{!! Form::close() !!}
			<div class="clearfix"></div>
		   </div>
        </div></div>


<!-- Rating section start -->



<div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
   <div class="modal-feedback modal-md">
      <div class="modal-content">
         <div class="modal-header popup-bg text-center">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <p class="popup-head color-white">Rate your experience with Requester</p>
         </div>
         <div class="clearfix"></div>
         <center>
            <div class="modal-body text-left">
               {!! Form::model('', ['name' => 'RequestReview', 'id' => 'RequestReview']) !!}
               <div class="col-md-12" id="rating_success_container"></div>
               <div class="col-md-12">
                  
                 
                  <div class="col-md-12" align="center">
                  
                  <div>
                      @if(count($user_data) > 0)
                      <?php $file_exists = false; ?>
                      
                          @if(!empty($user_data->Image))
                            @if(file_exists(BASEURL_FILE.'/user/'.$user_data->Image))
                              <?php $file_exists = true?>
                              <a class="fancybox" rel="group" href="{{ImageUrl}}/user/{{$user_data->Image}}">
                                  
                                    <img width="100px" height="100px" class="img-circle" src="{{ImageUrl}}/user/{{$user_data->Image}}">
                                      </a>
                            @endif
                                 @endif
                                 @if(!$file_exists)
                            <img width="100px" height="100px" class="img-circle" src="{{ImageUrl}}/no-image.jpg">
                                 @endif
                  @endif
                    </div>
                    </br>
                     <div>
                     <label class="rating_margin" >Rate Now :</label>
                     &nbsp;&nbsp;&nbsp;
                     <div class="rateyo" id="rateyo" ></div>
                     <div class="clearfix"></div>
                     <div id="er_rating"  class="error-msg"></div>
                     <input type="hidden" name="request_id" id="request_id" value='<?php echo $user->_id?>'>
                     <input type="hidden" name="requester_id" id="requester_id" value='<?php echo $user->RequesterId?>'> 
                     <br />
                     <script type="text/javascript">
                        $(document).ready(function() {
                        $(".fancybox").fancybox();
                        });
                        var $rateYo = $("#rateyo").rateYo();
                     </script>
                     </div>
                     <div class="form-group txt-area feedback-text">
                        {!! Form::textarea('review','',['class'=>'form-control required', 'placeholder'=> 'Feedback','id' =>'review','maxlength' =>250,'size' =>'30x4']) !!}
                     </div>
                     <div class="clearfix"></div>

                  </div>

               </div>
               <div class="col-md-12">
                  <div class="col-md-4"></div>
                  <div class="col-md-8">
                    
                  </div>
                  <div class="col-md-2"></div>
               </div>
               <div class="clearfix"></div>
               <div class="col-xs-12">
                 <div class="">
                    <div class="col-xs-6">
                       <div class="form-group">
                          <button type="submit" class="custom-btn1 btn-block" id='ratingButton'>SUBMIT</button>
                       <div class="error-msg"></div></div>
                    </div>
                    <div class="col-xs-6">
                       <div class="form-group text-center">
                          <a class="custom-btn1 btn-block" data-dismiss='modal' >NOT NOW</a>
                       </div>
                    </div>
                 </div> 
               </div>
               
               {!! Form::close() !!}
            </div>
         </center>
         <div class="clearfix"></div>
         </center>
      </div>
   </div>
</div>



<!-- Rating section end -->


<div class="modal fade" id="deliver_model" role="dialog">
	<div class="modal-feedback modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button  class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><b>Verify Delivery Code</b></h4>
				
			</div>
			<div class="modal-body" >
				 <div class="map-hloder">						
          <input type="hidden" name="request_id" id="request_id" value='<?php echo $user->_id?>'>
					 {!! Form::model('', ['name' => 'verify_delivery_code', 'id' => 'verify_code']) !!}
					 {!!Form::label('code','Verification Code (obtain this from Requester)',['class'=>'control-label'])!!}
					 {!!Form::text('code','',['class'=>'form-control required numeric','id'=>'code','placeholder'=>'Enter Verify Code'])!!}					 
					 
				 </div>
			</div>

			<div class="">
				
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{!! Form::submit('Submit', ['class' => 'btn btn-default','value'=>'SUBMIT','id'=>'verifybutton']) !!}
				&nbsp;&nbsp;&nbsp;{!! Form::button('Close', ['class' => 'btn btn-default', 'data-dismiss'=>'modal','value'=>"Not Now"]) !!} 
				
			</div>
			<div class="">&nbsp;&nbsp;&nbsp;&nbsp;</div>
			{!! Form::close() !!}
		</div>
	</div>
</div> 

</div>

@endforeach 



{!! Html::script('theme/web/js/validation.js') !!}
{!! Html::script('theme/web/js/utility.js') !!}
{!! Html::style('theme/date-time-picker/jquery.datetimepicker.css') !!}       
{!! Html::script('theme/date-time-picker/jquery.datetimepicker.js') !!}


<script type="text/javascript">
	new Validate({
		FormName : 'ServicesForm',
		ErrorLevel : 1,
		validateHidden: false,
		callback : function(){
			form_submit();
		}
		
	});

	function form_submit()
	{
		$('#cancel_delivery_submit').addClass('spinning');
		  $.ajax
		  ({
		  type: "POST",
		  url: "{{ url('/no_delivery') }}/{{Request::segment(3)}}",
		  data: $('#ServicesForm').serialize(),
		  
		  success: function(res)
		  {
		  var obj = eval('('+res+')');
		  
		  $('#cancel_delivery_submit').removeClass('spinning');
		  if(obj.success == 1)
		  {
			$( "#exampleModal" ).trigger( "click" );
			$('#success-message').html('<div role="alert" class="alert alert-success alert-dismissible fade in">'
			+'<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>'+obj.msg+'</div>');
			 
			$("#delivery_btn").hide();
			$("#complete").hide();
			$("#status_div").hide();
			$("#ajax_status").show();
			$("#ajax_status").html('<strong>Status:</strong> Cancel');
			
			 
		  }else {
			  alert(obj.msg);
			  window.location.replace("{{ url('/transporter') }}");
		  
		  }

		  }
		  });
			return false;

	}




	
	function accept_request()
	{
		$('#accept_request').addClass('spinning');
		  $.ajax
		  ({
		  type: "POST",
		  url: "{{ url('/accept-request-transporter') }}/{{Request::segment(3)}}",
		  
		  success: function(res)
		  {
		  var obj = eval('('+res+')');
		  console.log(obj);
		  $('#accept_request').removeClass('spinning');
		  if(obj.success == 1)
		  {   
			$('#success-message').html('<div role="alert" class="alert alert-success alert-dismissible fade in">'
			+'<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>'+obj.msg+'</div>');
			 
			$("#accept_request").hide();
			$("#cancel_request").show();
			$("#out_for_picup").show();
			$("#status_div").hide();
			$("#ajax_status").show();
			$("#ajax_status").html('<strong>Status:</strong> Accepted');
			
			
			 
		  }else {
			  alert(obj.msg);
			  window.location.replace("{{ url('/transporter') }}");

		  }

		  }
		  });
			return false;

	}


	function cancel_request()
	{
		$('#cancel_request').addClass('spinning');
		  $.ajax
		  ({
		  type: "POST",
		  url: "{{ url('/cancel-request-transporter') }}/{{Request::segment(3)}}",
		  
		  success: function(res)
		  {
		  var obj = eval('('+res+')');
		  console.log(obj);
		  $('#cancel_request').removeClass('spinning');
		  if(obj.success == 1)
		  {   
			$('#success-message').html('<div role="alert" class="alert alert-success alert-dismissible fade in">'
			+'<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>'+obj.msg+'</div>');
			 
			$("#cancel_request").hide();
			$("#accept_request").hide();
			$("#status_div").hide();
			$("#ajax_status").show();
			$("#ajax_status").html('<strong>Status:</strong> Cancel');
			
			 
		  }else {
				 alert(obj.msg);
			  window.location.replace("{{ url('/transporter') }}");
		  }

		  }
		  });
			return false;

	}

	function out_for_pickup()
	{
		$('#out_for_picup').addClass('spinning');
		  $.ajax
		  ({
		  type: "POST",
		  url: "{{ url('/pickup-request-transporter') }}/{{Request::segment(3)}}",
		  
		  success: function(res)
		  {
		  var obj = eval('('+res+')');
		  console.log(obj);
		  $('#out_for_picup').removeClass('spinning');
		  if(obj.success == 1)
		  {   
			$('#success-message').html('<div role="alert" class="alert alert-success alert-dismissible fade in">'
			+'<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>'+obj.msg+'</div>');
			 
			
			$("#out_for_picup").hide();
			$("#out_for_delivery").show();
			$("#status_div").hide();
			$("#ajax_status").show();
			$("#ajax_status").html('<strong>Status:</strong> Out for Pickup');
			
			 
		  }else {
				 alert(obj.msg);
			  window.location.replace("{{ url('/transporter') }}");
		  }

		  }
		  });
			return false;

	}

	function out_for_deliver()
	{
		$('#out_for_delivery').addClass('spinning');
		  $.ajax
		  ({
		  type: "POST",
		  url: "{{ url('/deliver-request-transporter') }}/{{Request::segment(3)}}",
		  
		  success: function(res)
		  {
		  var obj = eval('('+res+')');
		  console.log(obj);
		  $('#out_for_delivery').removeClass('spinning');
		  if(obj.success == 1)
		  {   
			$('#success-message').html('<div role="alert" class="alert alert-success alert-dismissible fade in">'
			+'<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>'+obj.msg+'</div>');
			 
			
			$("#out_for_delivery").hide();
      $("#cancel_request").hide();
			$("#delivery_btn").show();
			$("#deliver").show();
			$("#status_div").hide();
			$("#ajax_status").show();
			$("#ajax_status").html('<strong>Status:</strong> Out for Delivery');
			
			 
		  }else {
				 alert(obj.msg);
			  window.location.replace("{{ url('/transporter') }}");
		  }

		  }
		  });
			return false;

	}

	function select_os()
	{
		if($('[name="reject_by"]:checked').val() == "transporter")
		{
			$('#msg_div').show();
			$('#tracking_div').show();
			$('#receipt_div').show();
			$('#date_div').hide();
		}

		if($('[name="reject_by"]:checked').val() == "requester")
		{
			$('#msg_div').show();
			$('#tracking_div').show();
			$('#receipt_div').show();
			$('#date_div').hide();
		}
	}

	function select_ipayment()
	{
		if($('[name="reject_by"]:checked').val() == "transporter")
		{
			$('#msg_div').show();
			$('#tracking_div').hide();
			$('#receipt_div').hide();
			$('#date_div').hide();
		}
	}

	function select_requester()
	{
		if($('[name="return_type"]:checked').val() == "i_payment")
		{
			
			$('input:radio[name=return_type][value=os_return]').click();
		}
		$("#bycreturn").show();
		$("#ipayment").hide();
		

	}
	function select_transporter()
	{
		if($('[name="return_type"]:checked').val() == "by_creturn")
		{
			
			$('input:radio[name=return_type][value=os_return]').click();
		}
		$("#ipayment").show();
		$("#bycreturn").hide();
		$('#date_div').hide();
		

	}

	function select_bycreturn()
	{
		if($('[name="reject_by"]:checked').val() == "transporter")
		{
			$('#msg_div').show();
			$('#tracking_div').hide();
			$('#receipt_div').hide();
		}
		
		else if($('[name="reject_by"]:checked').val() == "requester")
		{
			$('#msg_div').show();
			$('#tracking_div').hide();
			$('#receipt_div').hide();
			$('#date_div').show();

		}


	}

	
	select_os();
	
 var newdate = new Date();
	 $('#date').datetimepicker({            
       format:'M d, Y H:i:s A',
       minDate:newdate,
       onChangeDateTime:function( ct ){
           $('#date2').datetimepicker({  minDate:ct  })
       },
   });


   new Validate({
		FormName : 'RequestReview',
		ErrorLevel : 1,
		validateHidden: false,
		callback : function()
    {
      if($rateYo.rateYo("rating") <= 0) {
        $('#er_rating').html('The rating field is required.');
        return false;
      }
        $("#ratingButton").addClass('spinning');
        $.ajax({
          url     : '{{url("/transporter-review")}}',
          type    : 'post',
          data    : {
              "review" : $("#review").val(),
              "request_id" : $('#request_id').val(),
              "rating" : $rateYo.rateYo("rating"),
              "requester_id": $('#requester_id').val()
            },
            success : function(res)
            {
                $("#ratingButton").removeClass('spinning');
                var obj = eval("("+res+")");
                if(obj.success == 1)
                {
                  $("#feedback").hide();
                  location.reload();
                  $("#RequestReview").trigger( "reset" );
                  $('#rating_success_container').html('<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>'+obj.msg+'</div>');
                }
                else
                {
                  $("#modal2").trigger('click' );
                  $("#feedback").hide();
                  $('#rating_success_container').html('<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>'+obj.msg+'</div>');
                }
            }
        });
		}
		
	});

	 
	new Validate({
		FormName : 'verify_delivery_code',
		ErrorLevel : 1,
		validateHidden: false,
		callback : function(){
				if(confirm('Are you sure You want to complete this request?'))
        {
          $("#verifybutton").addClass('spinning');
          $.ajax({
            url     : '{{url("/verify-delivery-code")}}',
            type    : 'post',
            data    : "code="+$("#code").val()+"&request_id="+$('#request_id').val(),
            success : function(res)
            {
              $("#verifybutton").removeClass('spinning');
              var obj = eval("("+res+")");
              if(obj.success == 1)
              {
                $( "#deliver_model" ).trigger( "click" );
                $( "#deliver_model" ).trigger( "reset" );
                $('#success-message').html('<div role="alert" class="alert alert-success alert-dismissible fade in">'
                +'<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>'+obj.msg+'</div>');

                document.getElementById("delivery_btn").style.display = "none";
                $("#deliver").hide();
                $("#delivery_btn").hide();
                $("#status_div").hide();
                $("#ajax_status").show();
                $("#ajax_status").html('<strong>Status:</strong> Delivered');
                $("#cancel_request").hide();
                $("#feedback").show();
              }
              else
              {
                $("#deliver_model" ).trigger( "click" );
                $("#deliver_model" ).trigger( "reset" );
                alert(obj.msg);
              }           
          }
          });

        }
		}
	});

  function buy_for_me(type)
  {

      if(type == 'pickup')
      {
        $('#b_pickup').addClass('spinning');
      }

      if(type == 'delivery')
      {
        $('#b_delivery').addClass('spinning');
      }


      $.ajax
      ({
      type: "POST",
      url: "{{ url('/transporter-buy-for-me') }}/{{Request::segment(3)}}",
      data: "type="+type,
      
      success: function(res)
      {
        var obj = eval('('+res+')');
        if(type == 'pickup')
        {
          $('#b_pickup').removeClass('spinning');
        }

        if(type == 'delivery')
        {
          $('#b_delivery').removeClass('spinning');
        }
      
        
      if(obj.success == 1)
      {   
      $('#success-message').html('<div role="alert" class="alert alert-success alert-dismissible fade in">'
      +'<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>'+obj.msg+'</div>');

        if(type == 'pickup')
        {
          $("#b_picup").hide(); 
          $("#b_delivery").show();
        }

        if(type == 'delivery')
        {
          $("#b_delivery").hide(); 
          
        }
      
        $("#status_div").hide();
        $("#ajax_status").show();
        $("#ajax_status").html('<strong>Status:</strong>'+obj.status);
        
      
      
       
      }else {
         alert(obj.msg);
        window.location.replace("{{ url('/transporter') }}");
      }

      }
      });
      return false;

  }

  new Validate({
    FormName : 'b_verify',
    ErrorLevel : 1,
    validateHidden: false,
    callback : function(){
        verify_by_for_me();
    }
  });


  function verify_by_for_me()
  {
    alert('Are you sure you want to complete this request?');
    $("#b_verify_button").addClass('spinning');
     $.ajax({
        url     : '{{url("/verify-delivery-code")}}',
        type    : 'post',
        data    : "code="+$("#code").val()+"&request_id="+$('#request_id').val(),
        success : function(res){
            $("#b_verify_button").removeClass('spinning');
                var obj = eval("("+res+")");
        if(obj.success == 1)
        {
          $( "#b_deliver_model" ).trigger( "click" );
          $( "#b_deliver_model" ).trigger( "reset" );
          $('#success-message').html('<div role="alert" class="alert alert-success alert-dismissible fade in">'
          +'<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>'+obj.msg+'</div>');
       
          
          $("#b_deliver").hide();
          $("#b_no_delivery").hide();
          $("#feedback").show();
          $("#status_div").hide();
          $("#ajax_status").show();
          $("#ajax_status").html('<strong>Status:</strong> Delivered');
          
        }
        else
        {
          $("#b_deliver_model" ).trigger( "click" );
          $("#b_deliver_model" ).trigger( "reset" );
          alert(obj.msg);
        }

                  
               }
            });
  
    return false; 
    
  }

</script>

<style>
.rating_margin
{
	transition: transform 0.8s cubic-bezier(0, 0.63, 0.41, 0.98) 0s, opacity 0.8s cubic-bezier(0, 0.63, 0.41, 0.98) 0s;
	transform: translateY(0px);
	opacity: 1;
	
}

.to-center{text-align:middle;}
</style>
@endsection
<?php

function change_unit($weight,$currentunit,$changeto)
{

//  echo $weight." ".$currentunit." ".$changeto; //die;

//  echo "<br>";

  $newweight = '';
  $newchangeto = $changeto;
  switch ($currentunit) {
    case 'kg':
      $newweight = $weight * 1000;  
    break;
    case 'lbs':
      $newweight = $weight * 0.00220462;  
    break;
    case 'inches':
      $newweight = $weight; 
    break;
    case 'cm':
      $newweight = $weight * 0.393701;  
    break;
  }
  
  switch ($changeto) {
    case 'kg':
      $newweight = $newweight / 1000; 
    break;
    case 'lbs':
      $newweight = $newweight * 0.00220462; 
    break;
    case 'cm':
      $newweight = $newweight / 0.393701; 
    break;
    case 'inches':
      $newweight = $newweight;  
    break;
  }

  return $newweight;

}

?>

<div aria-labelledby="exampleModalLabel" role="dialog" tabindex="-1" id="exampleModal1" class="modal fade in">
   <div role="document" class="modal-dialog">
      <div class="modal-content width">
         <div class="modal-header" id="Addaddress2">
            <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button>
            <h4 id="exampleModalLabel" class="modal-title"><b>{{ucfirst($user->ProductTitle)}}</b></h4>
         </div>
         {!! Form::model('', ['name' => 'new_address', 'id' => 'Addaddress', 'method' => 'POST']) !!}
            <div class="panel-body">
              <div class="row">
                <div class="col-md-12">
                  Are you sure you want to accept the package '{{ucfirst($user->ProductTitle)}}'? Accepted item must be delivered.
                  <hr />
                </div>
              </div>
               <div class="row">
          
                  <div class="col-sm-12">
                  <p class="text-primary"><b>Transporter Earning</b></p>
                  </div>
                  <div class="col-sm-12">
                     <div class="form-group">

                        <label>Requester Paid</label>
                        <div class="pull-right">${{ number_format((float)$user-> ShippingCost,2) }}</div> 
                     </div>
                  </div>
                  
                   <div class="col-sm-12">
                     <div class="form-group">
                        <label>Aquantuo Fee</label>
                          <div class="pull-right">${{ number_format((float)$user->AquantuoFees,2) }}</div>
                     </div>
                  </div>
                 
                    <div class="col-sm-12">
                     <div class="form-group">
                        <label>Transporter Earning</label>
                             <div class="pull-right">${{ number_format((float)$result,2) }}</div>
                     </div>
                  </div>
                 
                 
                  <div class="col-sm-12" align="center">
                      <div class="">
                           </hr class="border"> 
                        <button class="btn btn-danger btn-xs" data-dismiss="modal">
                           Cancel 
                        </button>
                         <button class="btn btn-default btn-xs"  title="Accept" data-dismiss="modal"  onclick="return confirm('Are you sure you want to accept this request?')? accept_request():'';">
                           Accept
                          
                        </button>
                     
                  </div>
                  </div>
               </div>
            </div>
         {!! Form::close() !!}
      </div>
   </div>
</div>
