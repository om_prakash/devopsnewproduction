@extends('Page::layout.one-column-page')
@section('content')

<div class="container">
    <div class="row">
    <div class="col-sm-12">
        <h2 class="color-blue mainHeading">Posts by Transporters</h2>
	</div>
	<div class="col-sm-12">

				<ul class="nav nav-tabs filter-tab" role="tablist">
					<li role="presentation" class="<?php if (in_array(Input::get('current_tab'), ['current', ''])) {echo "active";}?>" onclick="$('#current_tab').val('current')" id="curent_tab">
					<a href="#tab1" aria-controls="profile" role="tab" data-toggle="tab">Individual</a>
					</li>

					<li role="presentation" class="<?php if (in_array(Input::get('current_tab'), ['history'])) {echo "active";}?>" onclick="$('#current_tab').val('history')">
					<a href="#tab2" aria-controls="messages" role="tab" data-toggle="tab" >Business</a>
					</li>

				</ul>

				<div class="col-md-12 col-sm-12 col-xs-12 backgroun-grey margin-top">
					<div class="row">
						{!! Form::open(array('url'=>'post-by-transporter','method'=>'get')) !!}
						<div class=" col-sm-3  col-xs-12text-left">
							<input type="hidden" value="<?php echo Input::get('current_tab'); ?>" name="current_tab" id="current_tab">


							<div class="form-group">
								<label>Source/Departure Address</label>
								<input type="text" name="search_value" value="{{Input::get('search_value')}}" placeholder="Enter Source/Departure Address " id="search_value" class="form-control">
							</div>

						</div>

					<div class="col-md-3 col-sm-3 col-xs-12 text-left">
						<div class="form-group">


								<div class="form-group">
									<label>Destination/Arrival Address</label>
									<input type="text" name="search_value2" placeholder="Enter Destination/Arrival Address " id="search_value2" class="form-control">
								</div>

						</div>
					</div>
					<div class="col-md-3 col-sm-3 col-xs-12 text-left">
							<div class="form-group">
								<label>Departure Date</label>
									<div class="input-group">
										<input type="text"  placeholder="Search Date" id="startdate" class="form-control" name ="search_date" readonly="readonly"  value='<?php echo Input::get('search_date'); ?>'>
										<div class="input-group-addon"><i class="fa fa-calendar"></i>
										</div>
									</div>
							</div>

						</div>
						<div class="col-md-3 col-sm-3 col-xs-12 text-left">
							<div class="form-group">
								<label>Arrival Date</label>
									<div class="input-group">
										<input type="text"  placeholder="Search Date" id="enddate" value='<?php echo Input::get('search_date2'); ?>'  class="form-control" name ="search_date2" readonly="readonly">
											<div class="input-group-addon"><i class="fa fa-calendar"></i>
											</div>
									</div>
							</div>

						</div>
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="form-group">
					<input type="hidden" value="" id="ajaxPath">
					<button type="submit" value="Search" class="btn btn-primary">Search</button>

					<a class="btn btn-primary" href="{{url('post-by-transporter')}}">
					<span class="glyphicon glyphicon-refresh" aria-hidden="true">

					</span>&nbsp;&nbsp;Reset
					</a>

				</div>
			</div>
         {!! Form::close();  !!}
		</div>
    </div>
    </div>
	</div>

			<div class="row">



				<div class="tab-content">
					<!--  first  -->
					<div role="tabpanel" id="tab1"  class="tab-pane custom-tab-pane <?php if (in_array(Input::get('current_tab'), ['current', ''])) {echo 'active';}?>">

						<div id="containerdata1"></div>
					</div>
					<div role="tabpanel" id="tab2" class="custom-tab-pane tab-pane <?php if (Input::get('current_tab') == 'history') {echo 'active';}?>" >
					<!-- second -->

						 <div id="containerdata2"></div>
					</div>
				</div>



			</div>


</div>


@endsection
@section('script')
@parent
{!! Html::style('theme/date-time-picker/jquery.datetimepicker.css') !!}
{!! Html::script('theme/date-time-picker/jquery.datetimepicker.js') !!}
{!! Html::script('theme/front/js/jquery.rateit.min.js')!!}
{!! Html::script('theme/web/js/paginate.js') !!}

@endsection

@section('inline-script')
@parent
<script>




   var rawpost = {},rawpost2 = {},rawpost3 = {};
   @if(count(Input::get()) > 0)
       rawpost = eval('(' + '<?php echo json_encode(Input::get()); ?>' + ')' );
       rawpost2 = rawpost;

   @endif

   rawpost.container = 1;
   rawpost.list = "current";

   new paginate({
       url : "pagination/transporter/post-by-transporter",
       container : "containerdata1",
       postvalue : rawpost
   });

   rawpost2.container = 2;
   rawpost2.list = "history";

   new paginate({
       url : "pagination/transporter/post-by-transporter",
       container : "containerdata2",
       postvalue : rawpost2
   });

  $('#startdate').datetimepicker({
       format:'M d, Y',
       timepicker:false,
       scrollInput:false,
       onChangeDateTime:function( ct ){
          $('#enddate').datetimepicker({  minDate:ct  })
      },
   });

   $('#enddate').datetimepicker({
       format:'M d, Y',
       timepicker:false,
       scrollInput: false,
       onChangeDateTime:function( ct ){
           $('#startdate').datetimepicker({  minDate:ct  })
       },
   });

  </script>
@endsection

