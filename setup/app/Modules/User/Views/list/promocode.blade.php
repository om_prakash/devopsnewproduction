@extends('Page::layout.one-column-page')
@section('content')
<section class="promo-banner">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12">
                <h1 class="text-uppercase">upto 50% off | winter sale</h1>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris porta risus ac nisl tristique eleifend. Vivamus ultrices velit a turpis dictum semper.</p>
                <br>
                <span class="promocode_btn">Use Promocode <strong>PMO1234</strong></span>
                <a href="#" class="outline_btn">Share</a>
            </div>
        </div>
        
    </div>
</section>

<section class="page">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="coupon_box">
                        <div class="media_coupon code1">
                            <div class="media mb-4">
                                <div class="coupon-img mr-4">
                                    <img src="theme/web/promo/images/blue_logo.png" alt="..." width="80">
                                </div>
                                <div>
                                    <h1 class="text-uppercase mt-2 orange-text bold-text">Free Delivery</h1>
                                    <h4 class="green-text ng-binding">On order over $200.00</h4>
                                </div>

                            </div>
                            <p class="mb-0">Each coupon is identified by a Coupon Code and has different requirements and rewards.</p>
                            
                        </div>
                        <div class="couponfooter">
                            <a href="#">View Terms &amp; Conditions</a>
                            <a href="#">View promocode</a>
                        </div>
                    </div>
                </div>
            <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="coupon_box">
                        <div class="media_coupon code2">
                            <div class="media mb-4">
                                <div class="coupon-img mr-4">
                                    <img src="theme/web/promo/images/blue_logo.png" alt="..." width="80">
                                </div>
                                <div>
                                    <h2 class="text-uppercase mt-2 blue-text bold-text">Send Package by Ship</h2>
                                </div>

                            </div>
                            <p class="text-red bold-text">Each coupon is identified by a Coupon Code. </p>
                            <p class="mb-0">Please check your coupon — all requirements stated on the coupon must be met to receive the discount.</p>
                            
                        </div>
                        <div class="couponfooter">
                            <a href="#">View Terms &amp; Conditions</a>
                            <a href="#">View promocode</a>
                        </div>
                    </div>
                </div>
            <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="coupon_box">
                        <div class="media_coupon code2">
                            <div class="media mb-4">
                                <div class="coupon-img mr-4">
                                    <img src="theme/web/promo/images/blue_logo.png" alt="..." width="80">
                                </div>
                                <div>
                                    <h2 class="bold-text mt-2 blue-ext text-right padd-l-40"><span class="big-text">10%</span> Discount</h2>
                                </div>

                            </div>
                            <p class="yellow-text bold-text">Each coupon is identified by a Coupon Code. </p>
                            <p class="mb-0">Please check your coupon — all requirements stated on the coupon must be met to receive the discount.</p>
                            
                        </div>
                        <div class="couponfooter">
                            <a href="#">View Terms &amp; Conditions</a>
                            <a href="#">View promocode</a>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="coupon_box">
                        <div class="media_coupon code2">
                            <div class="media mb-4">
                                <div class="coupon-img mr-4">
                                    <img src="theme/web/promo/images/blue_logo.png" alt="..." width="80">
                                </div>
                                <div>
                                    <h2 class="bold-text mt-2 cyan-ext text-right padd-l-40"><span class="big-text">10%</span> Discount</h2>
                                </div>

                            </div>
                            <p class="purlpe-ext bold-text">Each coupon is identified by a Coupon Code. </p>
                            <p class="mb-0">Please check your coupon — all requirements stated on the coupon must be met to receive the discount.</p>
                            
                        </div>
                        <div class="couponfooter">
                            <a href="#">View Terms &amp; Conditions</a>
                            <a href="#">View promocode</a>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="coupon_box">
                        <div class="media_coupon code2">
                            <div class="media mb-4">
                                <div class="coupon-img mr-4">
                                    <img src="theme/web/promo/images/blue_logo.png" alt="..." width="120">
                                </div>
                                <div>
                                    
                                    <h2 class="bold-text blue-text red-text text-right text-uppercaase padd-l-40">Upto $50 cashback</h2>
                                </div>

                            </div>
                            <p class="purlpe-ext bold-text">Each coupon is identified by a Coupon Code. </p>
                            <p class="mb-0">Please check your coupon — all requirements stated on the coupon must be met to receive the discount.</p>
                            
                        </div>
                        <div class="couponfooter">
                            <a href="#">View Terms &amp; Conditions</a>
                            <a href="#">View promocode</a>
                        </div>
                    </div>
                </div>

            </div>
    </div>
</section>
@endsection