@extends('Page::layout.one-column-page')
@section('content')
<?php
$customer_cancel_deliveries = $customer_completed_deliveries = 0;
$tp_cancel_deliveries = $tp_completed_deliveries = 0;
foreach ($requester['result'] as $key => $val) {
    if ($val['_id'] == 'cancel') {
        $customer_cancel_deliveries += $val['count'];
    } elseif ($val['_id'] == 'completed') {
        $customer_completed_deliveries += $val['count'];
    }
}
foreach ($tp['result'] as $key => $val) {
    if ($val['_id'] == 'cancel') {
        $tp_cancel_deliveries += $val['count'];
    } elseif ($val['_id'] == 'completed') {
        $tp_completed_deliveries += $val['count'];
    }
}

?>
<div class="container">
<div class="row">
   <div class="col-sm-12">
   </div>
   <div class="col-md-9 col-sm-12">
      <div class=" box-shadow width80">
         <div class="row rewards">
            <div class="gift_wrapper"></div>
            <div class="col-sm-12">

               <br />
               <div class="tab-content">
                  <div id="home" class="tab-pane fade in active">
                     <div class="col-sm-12"  style="padding-top: 70px; padding-bottom: 70px;">
                           
                           <p><b>Click the button to invite your friends!</b></p>

                      <a  href="mailto:?subject=Invitation to Aquantuo&body=Hi&#44; %0D%0A%0D%0A %0D%0A

I came across Aquantuo and thought you might be interested.%0D%0A %0D%0A

It&#39;s a peer-to-peer delivery platform where travelers get paid for using the spare space in %0D%0Atheir luggage to deliver an item from the community of users when they travel internationally.%0D%0A %0D%0A

With this app, you can also list items you want to send to family and friends and someone will help get it there.%0D%0A %0D%0A

Users can also shop online from their favorite online stores and a verified Transporter will help deliver it.%0D%0A %0D%0A

If you are a business that ships packages across the globe, this app will help you find clients for your business as well. %0D%0A %0D%0A

Hope you check it out! %0D%0A %0D%0A

https://play.google.com/store/apps/details?id=com.it.aquantuo %0D%0A
https://itunes.apple.com/us/app/aquantuo/id1067797285?ls=1 %0D%0A
https://aquantuo.com %0D%0A %0D%0A
Thanks,%0D%0A
{{ ucfirst(Session::get('ShowName')) }}
">

                        <img   src="{{ asset('theme/web/images/MetroUI_Mail.png') }}"
                         height="70px" width="70px"></a>
                        </div>
                     </div>
           {{--           <div class="col-sm-12">
                        <div class="box-shadow">
                           <br/>
                           <p class="text-primary">{{$customer_completed_deliveries}}</p>
                           <p><b>Completed Deliveries</b></p><a href="{{url('pending-request')}}" class="pull-right">Details</a>
                        </div>
                     </div> --}}
                  {{--    <div class="col-sm-12">
                        <div class="box-shadow">
                           <br/>
                           <p class="text-primary">{{$customer_cancel_deliveries}}</p>
                           <p><b>Not Delivered </b></p><a href="{{url('pending-request')}}?status=not_delivered" class="pull-right">Details</a>
                        </div>
                     </div> --}}

                  <div id="menu2" class="tab-pane fade">
                 {{--     <div class="col-sm-12">
                        <div class="box-shadow">
                           <br/>
                           <p class="text-primary">{{$tp_completed_deliveries + $tp_cancel_deliveries}}</p>
                           <p><b>Total Delivery </b></p>
                        </div>
                     </div> --}}
             {{--         <div class="col-sm-12">
                        <div class="box-shadow">
                           <br/>
                           <p class="text-primary">{{$tp_completed_deliveries}}</p>
                           <p><b>Completed Deliveries</b></p><a href="{{url('transporter-pending-request')}}" class="pull-right">Details</a>
                        </div>
                     </div> --}}
                     {{-- <div class="col-sm-12">
                        <div class="box-shadow">
                           <br/>
                           <p class="text-primary">{{$tp_cancel_deliveries}}</p>
                           <p><b>Not Delivered </b></p><a href="{{url('transporter-pending-request')}}?status=not_delivered" class="pull-right">Details</a>
                        </div>
                     </div> --}}
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   @include('Page::layout.side_bar')
</div>
<script type="text/javascript">
   function transporter_type(type)
   {
      if(type == 'transporter') {

         $('#ac_type_bg').css('right','0%');
         $('#transporter').css('color','#fff');
         $('#requester').css('color','#000');
      } else {
         $('#ac_type_bg').css('right','50%');
         $('#requester').css('color','#fff');
         $('#transporter').css('color','#000');
      }
   }
</script>
@endsection

