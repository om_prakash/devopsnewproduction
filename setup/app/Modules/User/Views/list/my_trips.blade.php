@extends('Page::layout.one-column-page')
@section('content')

<div class="container">
    <div class="row">
    <div class="col-sm-12">
        <h2 class="color-blue mainHeading">Bussiness Trips</h2>
        <br />
	</div>
	<div class="col-sm-12">

				<ul class="nav nav-tabs filter-tab" role="tablist">
					<li role="presentation" class="<?php if(in_array(Input::get('current_tab'),['current','']))  { echo "active"; }  ?>" onclick="$('#current_tab').val('current')" id="curent_tab">
					<a href="#tab1" aria-controls="profile" role="tab" data-toggle="tab">Current</a>
					</li>

					<li role="presentation" class="<?php if(in_array(Input::get('current_tab'),['history'])) { echo "active"; }  ?>" onclick="$('#current_tab').val('history')">
					<a href="#tab2" aria-controls="messages" role="tab" data-toggle="tab" >History</a>
					</li>
					
				</ul>

				<div class="col-md-12 col-sm-12 col-xs-12 backgroun-grey margin-top">
					<div class="row">
						{!! Form::open(array('url'=>'transporter/bussiness-trips','method'=>'get')) !!}
						<div class=" col-sm-4 text-left">
							<input type="hidden" value="<?php echo Input::get('current_tab'); ?>" name="current_tab" id="current_tab">
							<div class="form-group">
									{!! Form::label('search','Search', ['class'=>'control-lable']) !!}      
									{!! Form::text('search',Input::get('search'),array('class' => 'form-control','id' => 'search','placeholder' => 'Search Category')) !!}
							</div>
					  
						</div>

						
						
    
						<div class=" col-sm-3 text-left">
							<div class="form-group">
								<label>Start Date</label>
									<div class="input-group">
										<input type="text"  placeholder="Search Date" id="startdate" value='<?php echo Input::get('search_date'); ?>'  class="form-control" name ="search_date" readonly="readonly">
										<div class="input-group-addon"><i class="fa fa-calendar"></i>
											</div>
									</div>

							</div>
						</div>

					<div class=" col-sm-3 text-left">
						<div class="form-group">
							<label>End Date</label>
								<div class="input-group">
									<input type="text"  placeholder="Search Date" id="enddate" value='<?php echo Input::get('search_date2'); ?>'  class="form-control" name ="search_date2" readonly="readonly">
									<div class="input-group-addon"><i class="fa fa-calendar"></i>
										</div>
								</div>

						</div>
					</div>

			<div class="col-md-12 col-sm-12">
				<div class="form-group">
					<input type="hidden" value="" id="ajaxPath">
					<button type="submit" value="Search" class="btn btn-primary">Search</button>
			
					<a class="btn btn-primary" href="{{url('transporter/bussiness-trips')}}">
					<span class="glyphicon glyphicon-refresh" aria-hidden="true">
					
					</span>&nbsp;&nbsp;Reset
					</a>
			
				</div>
			</div>     
         {!! Form::close();  !!} 
		</div> 
    </div>
    </div>
	</div>

			<div class="row">
        


				<div class="tab-content">
					<!--  first  -->
					<div role="tabpanel" id="tab1"  class="tab-pane custom-tab-pane <?php if(in_array(Input::get('current_tab'), ['current',''])) { echo 'active'; } ?>">
						
						<div id="containerdata1"></div>
					</div>
					<div role="tabpanel" id="tab2" class="custom-tab-pane tab-pane <?php if(Input::get('current_tab') == 'history') { echo 'active'; }?>" >
					<!-- second -->
						
						 <div id="containerdata2"></div>
					</div>
				</div>			


       
			</div>   


</div>


@endsection
@section('script')
@parent
{!! Html::style('theme/date-time-picker/jquery.datetimepicker.css') !!}       
{!! Html::script('theme/date-time-picker/jquery.datetimepicker.js') !!}
{!! Html::script('theme/front/js/jquery.rateit.min.js')!!} 
{!! Html::script('theme/web/js/paginate.js') !!}

@endsection

@section('inline-script')
@parent
<script>
	


	
   var rawpost = {},rawpost2 = {},rawpost3 = {};
   @if(count(Input::get()) > 0)
       rawpost = eval('(' + '<?php echo json_encode(Input::get()); ?>' + ')' );
       rawpost2 = rawpost;
      
   @endif
   
   rawpost.container = 1;
   rawpost.list = "current";
   
   new paginate({
       url : "pagination/transporter/my-trips",
       container : "containerdata1",
       postvalue : rawpost
   });
   
   rawpost2.container = 2;
   rawpost2.list = "history";
   
   new paginate({
       url : "pagination/transporter/my-trips",
       container : "containerdata2",
       postvalue : rawpost2
   });

   $('#startdate').datetimepicker({            
       format:'M d, Y',
       timepicker:false,
       onChangeDateTime:function( ct ){
           $('#enddate').datetimepicker({  minDate:ct  })
       },
   });

   $('#enddate').datetimepicker({            
       format:'M d, Y',
       timepicker:false,
       onChangeDateTime:function( ct ){
           $('#startdate').datetimepicker({  maxDate:ct  })
       },
   });

   
   
  </script>
@endsection

