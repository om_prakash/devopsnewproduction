@extends('Page::layout.one-column-page')
@section('page_title')
My Notification - Aquantuo
@endsection
@section('content')
<div class="container">
	<div class="">
    	<div class="">
		<h2 class="color-blue mainHeading">Notifications</h2>
		</div>

		<!-- Nav tabs -->

			<div class="col-md-12 col-sm-12 col-xs-12 backgroun-grey margin-top">
				<div class="row">
					{!! Form::open(array('url'=>'notification','method'=>'get')) !!}
						<div class="col-md-3 col-sm-6 col-xs-12 text-left">
							<div class="form-group">
								{!! Form::label('Search','Search', ['class'=>'control-lable']) !!}
								{!! Form::text('search',Input::get('search'),array('class' => 'form-control' , 'id' => 'search','placeholder' => 'Search Title')) !!}
		   					</div>
						</div>
						<div class="col-md-3 col-sm-6 col-xs-12 text-left">
							<div class="form-group">
								<label>Start Date</label>
									<div class="input-group">
										<input type="text"  placeholder="Search Date" id="startdate12" value="{{ Input::get('search_date') }}"  class="form-control" name ="search_date" readonly="readonly">
											<div class="input-group-addon"><i class="fa fa-calendar"></i>
											</div>
									</div>
							</div>
						</div>

						<div class="col-md-3 col-sm-6 col-xs-12 text-left">
							<div class="form-group">
								<label>End Date</label>
									<div class="input-group">
										<input type="text"  placeholder="Search Date" id="enddate" value="{{ Input::get('search_date2') }}"  class="form-control" name ="search_date2" readonly="readonly">
											<div class="input-group-addon"><i class="fa fa-calendar"></i>
											</div>
									</div>
							</div>

						</div>

						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="form-group">
								<input type="hidden" value="" id="ajaxPath">
								<button type="submit" value="Search" class="btn btn-primary">Search</button>
								<a class="btn btn-primary" href="{{url('/notification')}}" >
									<span class="glyphicon glyphicon-refresh" aria-hidden="true"></span>&nbsp;&nbsp;Reset
								</a>
								<a id="del_all" class="btn btn-primary" href="{{url('/delete-notification')}}">&nbsp;Delete All</a>

							</div>
						</div>
					{!! Form::close();  !!}
				</div>
			</div>
	    </div>
	    <div class="clear-fix"></div>
		<div class="">
			<div id="containerdata" class="row"></div>
		</div>
		<input type="hidden" value="pagination/notification-list" >


</div>





@endsection

@section('script')
@parent
{!! Html::style('theme/date-time-picker/jquery.datetimepicker.css') !!}
{!! Html::script('theme/date-time-picker/jquery.datetimepicker.js') !!}
{!! Html::script('theme/front/js/jquery.rateit.min.js')!!}
{!! Html::script('theme/web/js/paginate.js') !!}

@endsection

@section('inline-script')
@parent
<script>
$('#del_all').click(function(){
    return confirm('Do you want to delete all notifications?');
});



 var rawpost = {};
   @if(count(Input::get()) > 0)
       rawpost = eval('(' + '<?php echo json_encode(Input::get()); ?>' + ')' );

   @endif
	 new paginate({

       url : "pagination/notification-list",
       container : "containerdata",
       postvalue : rawpost,
       pageno: 1,
       limit: 10,

   });


    $('#startdate12').datetimepicker({
       format:'M d, Y',
       timepicker:false,
       scrollInput: false,
       onChangeDateTime:function( ct ){
           $('#enddate').datetimepicker({  minDate:ct  })
       },

   });

    $('#enddate').datetimepicker({
       format:'M d, Y',
       timepicker:false,
       scrollInput: false,
       onChangeDateTime:function( ct ){
           $('#startdate12').datetimepicker({  maxDate:ct  })
       },

   });



  </script>
@endsection
