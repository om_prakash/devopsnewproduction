@extends('Page::layout.one-column-page')
@section('page_title')
My Cards - Aquantuo
@endsection
@section('content')
<style>
.card-wrapper.gray-card {
    background: #f1f1f1 none repeat scroll 0 0;
    color: #d4d4d4;
    font-size: 19px;
    font-weight: bold;
    line-height: 85px;
    min-height: 89px;
    text-align: center;
    text-transform: uppercase;
}

</style>
<div class="container">
	<div class="row">
    	<div class="col-sm-6 col-xs-6">
		<h2 class="color-blue mainHeading">Card List</h2>
	</div><br/>
        </div>
        <div class="col-sm-6 col-xs-6">
        <small class="pull-right mainHeading"></small></h2>

	</div>
	<div class="clearfix"></div>
		<span id="card_msg"></span>
		<div class="row">

			<div class="col-md-9 col-sm-8 col-xs-12">
				<div class="box-shadow clearfix">
				<br/>

				@if(isset($cards['data']) && count(@$cards['data']) > 0)
					@foreach($cards['data'] as $key)
					<div class="col-sm-4" id="card_{{$key['id']}}">
						<div class="card-wrapper">
							<a class="custom-btn" title="Delete card" onclick="return confirm('Are you sure? You want to delete this card.')" href="delete-card/{{$key['id']}}" >Delete</a>
								<div class="media">
									<div class="media-left">
										<a href="#">
											@if($key['brand'] == 'Visa')
											<img height="30px" width="50px" src="{{ImageUrl}}/webcard/visa.png">
											@elseif($key['brand'] == 'Diners Club')
											<img height="30px" width="50px" src="{{ImageUrl}}/webcard/dinsers.jpeg">
											@elseif($key['brand'] == 'Discover')
											<img height="30px" width="50px" src="{{ImageUrl}}/webcard/discover.jpeg">
											@elseif($key['brand'] == 'MasterCard')
											<img height="30px" width="50px" src="{{ImageUrl}}/webcard/mastercard.jpeg">
											@elseif($key['brand'] == 'American Express')
											<img height="30px" width="50px" src="{{ImageUrl}}/webcard/ae.jpeg">
											@elseif($key['brand'] == 'JCB')
											<img height="30px" width="50px" src="{{ImageUrl}}/webcard/jcb.jpeg">
											@endif
										</a>
									</div>
									<div class="media-body">
										<p class="media-heading">xxxx-xxxx-xxxx-{{$key['last4']}}</p>
										<p> <br /></p>
									</div>
								</div>
							</div>
						</div>
						   @endforeach
					@endif
					<div class="col-md-4 col-sm-6 col-xs-12">
						<div class="card-wrapper gray-card">
							<a data-whatever="@mdo" data-target="#modal2" data-toggle="modal" href="" >
								<span><i class="fa fa-plus"></i>Add card</span>
							</a>
						</div>
					</div>
							</div>
						</div>
                   @include('Page::layout.side_bar')

    </div>
</div>

	<!--card modal-->
	<div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title color-white" id="myModalLabel">&nbsp;&nbsp;&nbsp;Add Card</h4>
          </div><div class="clearfix"></div>
          {!! Form::model('', ['name' => 'Addcard', 'id' => 'Addcard', 'method' => 'POST', 'url' => 'add-card']) !!}
          <input type="hidden" name="requestid" value="{{Request::segment(2)}}">
          <div class="modal-body">

          		<div class=col-md-12 id="succesdiv"></div>
				<div id='errordiv' class="col-sm-12"></div>

			<div class="form-group clearfix">
				<div class="col-sm-12">

					{!! Form::label('name_on_card', 'Card Holder Name',['class'=>'color-black']) !!}
					{!! Form::text('name_on_card', '', ['class'=>'form-control required','maxlength'=>'25' ,'placeholder'=> 'Card Holder Name','id' =>'name_card']) !!}
					<p class="help-block white" id='er_name_card'></p>
				</div>
			</div>


			<div class="form-group clearfix">
				<div class="col-sm-12">
				   {!! Form::label('credit_card_number', 'Card Number',['class'=>'color-black']) !!}
					  {!! Form::text('credit_card_number', '', ['class'=>'form-control required numeric','maxlength'=>'16	' , 'placeholder'=> 'Card Number','id' =>'credit_card_number','maxlength'=>'16']) !!}
					  <p class="help-block white" id='er_credit_card_number'></p>
				</div>
			</div>

			<div class="">
            <div class="col-sm-12">

               <div class="row">
	                <div class="col-sm-6">
		                <div class="form-group">
		                 {!! Form::label('expiration_date', 'Exp. Month',['class'=>'color-black']) !!}
		                 {!! Form::select('month', [''=>'Exp. Month','1'=>'01','2'=>'02','3'=>'03','4'=>'04','5'=>'05','6'=>'06','7'=>'07','8'=>'08','9'=>'09','10'=>'10','11'=>'11','12'=>'12'],Input::get('month'),['class'=>'form-control required numeric usename-#month#"','id'=>'month']) !!}
		                 <p class="help-block white" id='er_month'></p>
		                </div>
	                </div>

	                 <div class="col-sm-6">
		                 <div class="form-group">
		                  {!! Form::label('year', 'Year',['class'=>'color-black']) !!}
		                 {{ Form::selectRange('year', date('Y'), date('Y',strtotime('+15 year')), Input::get('year'), ['class'=>'form-control required numeric usename-#year#"', 'id'=>"year1",'placeholder'=>'Year']) }}
		                 <p class="help-block white" id='er_year'></p>
		                </div>
	                </div>
                </div>
            </div>
            </div>

            <div class="form-group">
				<div class="col-sm-12">
				   {!! Form::label('security_code', 'CVV',['class'=>'color-black']) !!}
					{!! Form::password('security_code',['class'=>'form-control required numeric usename-#CVV#', 'placeholder'=> 'CVV', 'maxlenght'=>'4', 'id' =>'security_code','maxlength'=>'4']) !!}

					  <p class="help-block white" id='er_security_code'></p>

				</div>

			</div>




          </div>
          <div class="clearfix"></div>
           <br>
			<div class="modal-footer">
			  <div class="col-sm-12 text-right">

				<button class="btn custom-btn1" data-dismiss="modal" type="button">Close</button>
				<button class="btn custom-btn1" id="addbutton" type="submit">Submit</button>

			  </div>
			</div>

			{!! Form::close() !!}
        </div>
      </div>
    </div>
    @endsection
     @section('script')
@parent

{!! Html::script('theme/web/js/utility.js') !!}
{!! Html::script('theme/web/js/validation.js') !!}
{!! Html::script('theme/web/js/request.js') !!}


@endsection

@section('inline-script')
@parent

<script>

new Validate({
	FormName : 'Addcard',
	ErrorLevel : 1,
	callback : function(){
      add_card();
    }
});

function add_card()
{

	$('#addbutton').addClass('spinning');
	$.ajax({
	url: SITEURL+"add-card",
	type:"post",
	dataType: 'json',
	data: $('#Addcard').serialize(),
		success: function(res)
		{
			$('#addbutton').removeClass('spinning');
			if(res.success == 0)
			{
				//$('#errordiv').html(res.msg);

				$('#errordiv').html('<div role="alert" class="alert alert-danger alert-dismissible fade in" style="margin-top: 0px;">'
			+'<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>'+res.msg+'</div>');
			}
			if(res.success == 1)
			{

				$("#Addcard").trigger("reset");
				$("#errordiv").hide();
				$('#succesdiv').html('<div role="alert" class="alert alert-success alert-dismissible fade in" style="margin-top: 0px;">'
			+'<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>'+'<b>Success !</b> Card has been added successfully.'+'</div>');


				setTimeout(function(){ location.reload(); }, 3000);

			}


		}

	});

}

	function delete_card(cardId){
{
if(confirm("Are you sure you want to delete this card?")){
	$("#card_"+cardId).addClass('spinning');
  if(cardId != ""){
    $.ajax({
      url     : '{{ url("remove-creditcard")}}',
      type    : 'post',
      data    : 'cardid='+cardId,
      success : function(res){
        $("#card_"+cardId).removeClass('spinning');
        var obj = eval("("+res+")");
        alert_message('card_msg',obj.success,obj.msg);
        if(obj.success == 1){
			$("#apend_saved_card").html(obj.new_cardlist);
        }if(obj.success == 2)
        {
			alert_message('card_msg',0,obj.msg);
		}

      }
    });
  }
  else{
    alert("Invalid card");
  }
}
}
}
</script>
@endsection
