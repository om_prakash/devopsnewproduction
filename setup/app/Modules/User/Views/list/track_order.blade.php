@extends('Page::layout.one-column-page')
@section('page_title')
Track Order - Aquantuo
@endsection
@section('content')
<section class="promo-banner"  @if (Session::get('UserId') == '') style="margin-top: -20px;" @endif>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12">
                <h1 class="text-uppercase">Track Delivery shipments</h1>
             </div>
        </div>
    </div>
</section>
<section class="container page">

    <div class="row mt-4">



        <br>
        {!! Form::open(array('url' =>'track-order','method' => 'GET','class' => '','id' => 'search_form_id','role' => "search")) !!}
        <div class="col-xs-12 col-sm-12 text-center">
            <span class="mr-4"><i class="fa fa-phone"></i><a class="black_anchor" target="_blank" href="callto:888 625 2233"> Call (+233) 30 243 4505</a></span>   <span><i class="fa fa-desktop"></i> <a class="black_anchor" target="_blank" href="http://aquantuo.com/">http://aquantuo.com/</a></span>
            <div class="form-group mt-4">
             <label class="radio-inline">
                <input type="radio" name="package_readio" id="inlineRadio1" checked @if(Input::get('package_readio') == 'package_id') checked @endif value="package_id">Package Number
                </label>
                <label class="radio-inline">
                <input type="radio" name="package_readio" id="inlineRadio2" @if(Input::get('package_readio') == 'item_id') checked @endif value="item_id" >Item Number
                </label>
            </div>
        </div>

        <div class="">
        <div class="col-sm-5 col-xs-8 col-sm-offset-3 mt-4 mb-4">
            <div class="input-group">
            <input type="text" class="form-control m-m-h" name="package_id" placeholder="Enter your package or item ID here" value="{{Input::get('package_id')}}">
            <span class="input-group-btn" >
                <button class="btn btn-default" style="margin-right: -15px" type="submit"><i class="fa fa-map-marker"></i> Track</button>
            </span>
            </div><!-- /input-group -->
            <br/>
            {{-- <div><strong>Try:</strong><span class="text-primary">18771515357412</span></div> --}}

        </div><!-- /.col-lg-6 -->
        <div class="col-sm-1 col-xs-1 mt-4"><a class="btn btn-default pull-left" href="{{ url('track_ordder') }}" ><i class="fa fa-refresh"></i> Reset</a></div>


        {!! Form::close() !!}
<div class="clearfix"></div>
        <div class="col-lg-8 col-sm-offset-2 mt-4 mb-4">
                <div class="track_box">
                    <div class="header_bg">
                    @if(input::get('package_id') == '')
                    Track Order
                    @else
                    {{ input::get('package_id') }}
                    @endif
                    </div>

                    <div class="sub-header mb-4">
                    <div class="div"> <div>
                    <p>Current Status:
                    

                    @if(isset($date->Status))
                        {{ get_status_title($date['ProductList'][0]['status'],$date->RequestType)['status'] }}
                    @elseif(isset($current_status))
                        {{ get_status_title($current_status->Status,$current_status->RequestType)['status'] }}
                    @endif
                    <p>

                        @if(isset($activity[0]))
                            {{ date(' M  d, Y H:i:s A',$activity[0]['EnterOn']->sec) }}
                        @endif
                    </p>

                    </div></div>

                     <div class="div"> <div>
                    <p>Expected Delivery </p> <small>
                     @if(isset($date['ProductList'][0]['ExpectedDate']))
                    {{ show_date($date['ProductList'][0]['ExpectedDate']) }}
                    @endif
                    </small>
                    </div></div>
                    </div>
                    @if(count($package) > 0)
                    @foreach($package as $value)
                     <div class="flex">
                        <div class="date_flex">
                            <p>{{date('M  d, Y',$value->EnterOn->sec)}}</p>
                            <small>{{date('H:i:s A',$value->EnterOn->sec)}}</small>
                            <small>{{ show_date(@$value->EnterOn) }}</small>
                            <span class="circle-box active"></span>
                        </div>
                        <div class="des_flex">
                            <p>
                               @if($value->request_type == 'buy_for_me')
                            {{ get_status_title($value['status'])['status'] }}   ({{ ucfirst($value['item_name']) }})
                               @else
                            {{ get_status_title($value['status'],$value->request_type)['status'] }}   ({{ ucfirst($value['item_name']) }})
                               @endif
                            </p>
                            <span>{{ ucfirst($value->message) }}</span>
                        </div>
                    </div>
                    @endforeach
                    @else
                    <div class="flex text-center">
                     <strong>Please verify the item/package ID number and try again. The number may be incorrect, the package may be in a pending status or the package is yet to be created.</strong>
                    </div>
                    @endif
            </div>
        </div>
    </div>
</section>
<script>

</script>
@endsection
