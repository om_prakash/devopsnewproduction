@extends('Page::layout.one-column-page')
@section('content')
{!! Html::style('theme/date-time-picker/jquery.datetimepicker.css') !!}       
{!! Html::script('theme/date-time-picker/jquery.datetimepicker.js') !!}
<div class="container">
	<div class="">
    	<div class="">
    	       <?php if( Input::get('status') == 'not_delivered') { ?>
		<h2 class="color-blue mainHeading">Not Delivered Requests</h2>
		        <?php } else { ?>
		<h2 class="color-blue mainHeading">Completed Requests</h2> 
		         <?php }  ?>       

		</div>
	
		<!-- Nav tabs -->
		<ul class="nav nav-tabs filter-tab" role="tablist">

		<!--	<li role="presentation" class="<?php /* if(in_array(Input::get('current_tab'),['new','']))  { echo "active"; }  */ ?>" onclick="manage_delivery_status(this,'#delivery_status','new');" ><a href="#tab1" aria-controls="home" role="tab" data-toggle="tab">New</a></li>  -->
			
		</ul>
			<div class="col-md-12 col-sm-12 col-xs-12 backgroun-grey margin-top">
				<div class="row">
					<div class="col-md-3 col-sm-4 col-xs-12 text-left">
							    {!! Form::open(array('url'=>'pending-request','method'=>'get')) !!}
							<input type="hidden" value="{{Input::get('current_tab')}}" name="current_tab" id="current_tab">
								<div class="form-group">
										{!! Form::label('Search','Search', ['class'=>'control-lable']) !!}      
										{!! Form::text('search',Input::get('search'),array('class' => 'form-control' , 'id' => 'search','placeholder' => 'Search Title')) !!}
			   					</div>
					</div>
						<div class="col-md-3 col-sm-4 col-xs-12 text-left">
							<div class="form-group">
								{!! Form::label('Status', 'Status', ['class'=>'control-lable']) !!}                   
								<select name="Status" class="form-control" value="{{Input::get('Status')}}" id="delivery_status">
								<option value="" >Select Status </option>
									<?php if(in_array(Input::get('current_tab'),['new','']))  {    ?>
									       	<option <?php if(Input::get('Status') == 'ready') { ?>  selected="selected"  <?php } ?>  value="ready">Ready</option>
									       	<option  <?php if(Input::get('Status') == 'pending') { ?>  selected="selected" <?php } ?>   value="pending">Pending</option>
								    <?php  }  ?>
							
									<?php if(in_array(Input::get('current_tab'),['current'])) { ?>
										<option <?php if(Input::get('Status') == 'accepted') { ?> selected="selected" <?php } ?> value="accepted">Accepted</option>  	
										<option <?php if(Input::get('Status') == 'out_for_pickup') { ?> selected="selected" <?php } ?> value="out_for_pickup">Out for pickup</option>
										<option <?php if(Input::get('Status') == 'out_for_delivery') { ?> selected="selected" <?php } ?> value="out_for_delivery">Out for delivery</option>
									<?php } ?>
									
									<?php if(in_array(Input::get('current_tab'),['history'])) { ?>
										<option <?php if(Input::get('Status') == 'delivered') { ?> selected="selected" <?php } ?>   value="delivered">Delivered</option>
										<option <?php if(Input::get('Status') == 'cancel') { ?> selected="selected" <?php } ?>  value="cancel">Cancel</option>
									<?php }  ?>
								</select>    
											
							</div>
						</div>
						<div class="col-md-3 col-sm-6 col-xs-12 text-left">
							<div class="form-group">
								<label>Start Date</label>
									<div class="input-group">
										<input type="text"  placeholder="Search Date" id="startdate" value='<?php echo Input::get('search_date'); ?>'  class="form-control" name ="search_date" readonly="readonly">
											<div class="input-group-addon"><i class="fa fa-calendar"></i>
											</div>
									</div>
							</div>
		
						</div>

						<div class="col-md-3 col-sm-6 col-xs-12 text-left">
							<div class="form-group">
								<label>End Date</label>
									<div class="input-group">
										<input type="text"  placeholder="Search Date" id="enddate" value='<?php echo Input::get('search_date2'); ?>'  class="form-control" name ="search_date2" readonly="readonly">
											<div class="input-group-addon"><i class="fa fa-calendar"></i>
											</div>
									</div>
							</div>
		
						</div>



						<div class="col-md-12 col-sm-12  col-xs-12 inline-form-btn">
							<div class="form-group">
								<input type="hidden" value="" id="ajaxPath">
								<button type="submit" value="Search" class="btn btn-primary">Search</button>
								<a class="btn btn-primary" href="{{url('pending-request')}}?current_tab={{Input::get('current_tab')}}" >
									<span class="glyphicon glyphicon-refresh" aria-hidden="true"></span>&nbsp;&nbsp;Reset
								</a>                                                                
							</div>
                        {!! Form::close();  !!} 
						</div>     
				</div>
			</div>
	    </div>
	<div class="row">
						<div class="col-md-12 col-xs-12">
							<div class="tab-content">
								<div role="tabpanel" id="tab1" class="tab-pane custom-tab-pane <?php if(in_array(Input::get('current_tab'), ['new',''])) { echo 'active'; } ?>">
								
										<div id="containerdata1" >   </div>
								</div>
							
								
							</div>
						</div>
					</div>				
</div> 
						
<div class="modal fade" id="myModal" role="dialog">
	<div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">View On Map</h4>
      </div>
      <div class="modal-body">
        	<div id="new_reqeust_path_container" style="width:100%; height:350px;">Loading
        	</div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div> 
 
 
@endsection

@section('script')
@parent

{!! Html::script('theme/front/js/jquery.rateit.min.js')!!} 
{!! Html::script('theme/web/js/paginate.js') !!}


@endsection

@section('inline-script')
@parent
<script>

   var rawpost = {},rawpost2 = {},rawpost3 = {};
   @if(count(Input::get()) > 0)
       rawpost = eval('(' + '<?php echo json_encode(Input::get()); ?>' + ')' );
       rawpost2 = rawpost;
       rawpost3 = rawpost;
   @endif
   
   rawpost.container = 1;
   rawpost.list = "new";
   
   new paginate({
       url : "pagination/pending-request",
       container : "containerdata1",
       postvalue : rawpost
   });
   
  
   
  
   
   function manage_delivery_status(obj,delivery_status,type){
   		$('#current_tab').val(type); 
   		if(type == 'new') {
   			$(delivery_status).html('<option value="">Select Status</option><option value="completed">Complate</option><option value="cancel">Cancel</option>');
   		}
   } 
  	
  	$('#startdate').datetimepicker({            
       format:'M d, Y',
       timepicker:false,
       scrollInput:false,
       onChangeDateTime:function( ct ){
          $('#enddate').datetimepicker({  minDate:ct  })
      },
   });
   
   $('#enddate').datetimepicker({            
       format:'M d, Y',
       timepicker:false,
       scrollInput: false,
       onChangeDateTime:function( ct ){
           $('#startdate').datetimepicker({  minDate:ct  })
       },
   });

var load_google_map_api = false;


var pickup = [];
var delivery = [];
function view_on_map(PickupLatLong,DeliveryLatLong)
{
	delivery = DeliveryLatLong;
	pickup = PickupLatLong;

	if(load_google_map_api == false) {
		load_google_map_api = true;

		 var s = document.createElement("script");
		   s.type = "text/javascript";
		   s.src = "https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyCT0uj-bffWWdOdwDp15LvrMko-OuoZUoY&callback=generate_map";
		    // Use any selector
		   $("head").append(s);


	} else {

		generate_map();
	}
	
	

}

function generate_map() 
{

	PickupLatLong = eval('('+pickup+')');
	DeliveryLatLong = eval('('+delivery+')');

	setTimeout(function(){
		var view_path = new google.maps.Map(document.getElementById('new_reqeust_path_container'), {
							center: new google.maps.LatLng(PickupLatLong[0],PickupLatLong[1]), 
							zoom: 1
						});

		new google.maps.Marker({
			position: {lat: parseFloat(PickupLatLong[1]), lng: parseFloat(PickupLatLong[0])},
			map: view_path,
			icon: '{{ImageUrl}}/pick.png',
			title: 'Pickup'
	    });	

	    new google.maps.Marker({
			position: {lat: parseFloat(DeliveryLatLong[1]), lng: parseFloat(DeliveryLatLong[0])},
			map: view_path,
			icon: '{{ImageUrl}}/drop.png',
			title: 'Drop off'
	    });	

	     view_path.setCenter(new google.maps.LatLng(
		  ((	PickupLatLong[0] + DeliveryLatLong[1]) / 2.0),
		  ((PickupLatLong[0] + DeliveryLatLong[1]) / 2.0)
		));
		view_path.fitBounds(new google.maps.LatLngBounds(
		  //bottom left
		  new google.maps.LatLng(PickupLatLong[1], PickupLatLong[0]),
		  //top right
		  new google.maps.LatLng(DeliveryLatLong[1], DeliveryLatLong[0])
		));


	},1000	);
}



  </script>


  
@endsection
