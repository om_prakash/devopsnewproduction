@extends('Page::layout.one-column-page')
@section('content')
<div class="container">
	<div class="row">
    	<div class="col-sm-12">
            <h2 class="color-blue mainHeading">Transporter Trip List</h2>
        </div>
	
<?php  $value =  Request::segment(2);   ?>
	<div class="col-sm-12">
		<!-- Nav tabs -->
		<ul class="nav nav-tabs filter-tab" role="tablist">
			
			<li role="presentation" class="<?php if(in_array(Input::get('current_tab'),['individual','']))  { echo "active"; }  ?>" onclick="$('#current_tab').val('individual')"><a href="#tab1" aria-controls="home" role="tab" data-toggle="tab">Individual</a></li>
			
			<li role="presentation" class="<?php if(in_array(Input::get('current_tab'),['business'])) { echo "active"; }  ?>" onclick="$('#current_tab').val('business')"><a href="#tab2" aria-controls="profile" role="tab" data-toggle="tab">Business</a></li>
			
		</ul>
			<div class="col-md-12 col-sm-12 col-xs-12 backgroun-grey margin-top">
				<div class="row">
					<div class="col-md-3 col-sm-4 col-xs-12 text-left">
						{!! Form::open(array('url'=>"trip-detail/{$value}",'method'=>'get')) !!}
						<input type="hidden" value="<?php echo Input::get('current_tab'); ?>" name="current_tab" id="current_tab">
							<div class="form-group">
								{!! Form::label('Search','Source/DepartureAddress', ['class'=>'control-lable']) !!}      
								{!! Form::text('search',Input::get('search'),array('class' => 'form-control' , 'id' => 'search','placeholder' => 'Source/DepartureAddress')) !!}
			   				</div>
					</div>	
					<div class="col-md-3 col-sm-4 col-xs-12 text-left" id="business">
<!--
						<input type="hidden" value="{{Input::get('current_tab')}}" name="current_tab_business" id="current_tab_business">
-->
							<div class="form-group">
								{!! Form::label('Search_business','Destiantion/ArrivalAddress', ['class'=>'control-lable']) !!}      
								{!! Form::text('search_business',Input::get('search_business'),array('class' => 'form-control' , 'id' => 'search_business','placeholder' => 'Destiantion/ArrivalAddress')) !!}
			   				</div>
					</div>
					<div class="col-md-3 col-sm-4 col-xs-12 inline-form-btn">
						<div class="form-group">
							<input type="hidden" value="" id="ajaxPath">
							<button type="submit" value="Search" class="btn btn-primary">Search</button>
							<a class="btn btn-black"  href="{{$value}}?current_tab={{Input::get('current_tab')}}" >
								Reset                                           
							</a>                                                                
						</div>
                        {!! Form::close();  !!} 
					</div>     
				</div>
			</div>
 	    </div>
	<!-- Tab panes -->

			<div class="col-md-12 col-xs-12">
				<div class="tab-content">

					<div role="tabpanel" id="tab1" class="tab-pane custom-tab-pane <?php if(in_array(Input::get('current_tab'), ['individual',''])) { echo 'active'; } ?>">			
						<div id="containerdata1" >   </div>
					</div>
					<div role="tabpanel" id="tab2" class="custom-tab-pane tab-pane <?php if(Input::get('current_tab') == 'business') { echo 'active'; } ?>"  >	
						<div id="containerdata2">   </div>
					</div>					

				</div>
			</div>
		</div>
		@endsection
</div>
@section('script')
@parent

{!! Html::script('theme/front/js/jquery.rateit.min.js')!!} 
{!! Html::script('theme/web/js/paginate.js') !!}

@endsection

@section('inline-script')
@parent
<script>

   var rawpost = {},rawpost2 = {},rawpost3 = {};
   @if(count(Input::get()) > 0)
       rawpost = eval('(' + '<?php echo json_encode(Input::get()); ?>' + ')' );
       rawpost2 = rawpost; 
   @endif
   
   rawpost.container = 1;
   rawpost.list = "individual";
   
   new paginate({
       url : "pagination/trip-detail/{{Request::segment(2)}}",
       container : "containerdata1",
       postvalue : rawpost
   });
   
   rawpost2.container = 2;
   rawpost2.list = "business";
   
   new paginate({
       url : "pagination/trip-detail/{{Request::segment(2)}}",
       container : "containerdata2",
       postvalue : rawpost2
   });
   
    $(document).ready(function(){
    $(".business-tab").click(function(){
        $("#business").css("display","none");
        $("#business").siblings().css("display","block");
        });
     $(".business-tab").siblings().click(function(){
        $("#business").css("display","block");
        $("#business").siblings().css("display","block");
        });
    });

  </script>
@endsection


