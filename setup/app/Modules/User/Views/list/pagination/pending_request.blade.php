<div class="">
    <div class="">
		@if(count($requests) > 0)
        @foreach($requests as $data)
		<div class="col-sm-12 col-xs-12 box-shadow" id="row-{{$data->_id}}">
            <div class="list-group-custom">
				<div class="row">
					<div class="col-sm-2 col-xs-2">
						 @if($data->ProductImage != '')
							<a class="fancybox" rel="group" href="{{ ImageUrl.$data->ProductImage }}" >
								<img src="{{ ImageUrl.$data->ProductImage}}" class="margin-top" width="150px" height="150px" >
							</a>        
						@else
								<img src="{{ ImageUrl}}no-image.jpg" width="150px" class="margin-top" height="150px"  class="img-rounded">
						@endif
					</div>
						<div class="col-sm-10 col-xs-10">

						<h3>{{ucfirst($data->ProductTitle)}}</h3>
						
							@if(isset($data->DeliveryDate->sec))
								<p><b>Drop of Date :</b> {{date('l,  M  d, Y ',$data->DeliveryDate->sec)}}</p>
							@endif

							<p ><b>Drop off Location :</b> {{ucfirst($data->DeliveryFullAddress)}}&nbsp;{{$data->DeliveryPincode}}</p>
							  <?php 
									if(ucfirst($data->Status) == 'Out_for_pickup')
										{ echo   "<strong>Status :</strong> Out for Pickup";  }
									else if(ucfirst($data->Status) == 'Out_for_delivery')
										{ echo "<strong>Status :</strong> Out of delivery"; }
								   	else
										{  $result = $data->Status; 
									echo "<strong>Status :</strong> ".ucfirst($result);  }
		               	                          ?><p></p>
		               	    @if(isset($data->EnterOn->sec))
								<p><b>Created Date : </b> {{date('l, M d, Y ',$data->EnterOn->sec)}}</p>
                            @endif 

                            @if($data->RequestType == 'online')
                            	<img src="{{THEME}}web/promo/images/online_purchase_blue.png" width="30px" class="margin-top" height="30px"  class="img-rounded " >
                            	<b>&nbsp;&nbsp;Item Delivery Request</b>
                            
                            @elseif($data->RequestType == 'buy_for_me')
                            	<img src="{{THEME}}web/promo/images/buy_for_me_blue.png" width="30px" class="margin-top" height="30px"  class="img-rounded image-css" >
                            	<b>&nbsp;&nbsp;Item Purchase request</b>

                            @endif
                            
                            <br />
                        @if($data->RequestType == 'online')
                        	<img src="{{THEME}}web/promo/images/online_purchase_blue.png" width="30px" class="" height="30px"  class="img-rounded " >
                        	<b>&nbsp;&nbsp;Item Delivery Request</b>

                        @elseif($data->RequestType == 'buy_for_me')
                        	<img src="{{THEME}}web/promo/images/buy_for_me_blue.png" width="30px" class="" height="30px"  class="img-rounded image-css" >
                        	<b>&nbsp;&nbsp;Item Purchase request</b>

                        @elseif($data->RequestType == 'delivery')
                        	<img src="{{THEME}}web/promo/images/sendpackage.png" width="30px" class="" height="30px"  class="img-rounded image-css" >
	                    	<b>&nbsp;&nbsp;Send A Package</b>
                        @endif
                             
                      	</div>
                </div>
            </div>   
            <div class="list-footer">
                <div class="row">
					<div class="col-sm-offset-2 col-sm-10 col-xs-12" >                              
						@if($data->RequestType == 'online')
							<a class="btn btn-primary" href="online-request-detail/{{$data->_id}}"><i class="fa fa-eye"></i>View Details</a>&nbsp;	
						@endif
						@if($data->RequestType == 'delivery')
								@if(isset($data->ProductList))
									<a class="btn btn-primary" title="Details" href="send-package-detail/{{$data->_id}}"><i class="fa fa-eye"></i> View Details</a>&nbsp;
								@else
									<a class="btn btn-primary" title="Details" href="delivery-details/{{$data->_id}}"><i class="fa fa-eye"></i> View Details</a>&nbsp;
								@endif	
						@endif   
						@if($data->RequestType == 'buy_for_me')
                            <a class="btn btn-primary" href="buy-for-me-detail/{{$data->_id}}"><i class="fa fa-eye"></i>View Details</a>&nbsp;
						@endif
						@if($data->RequestType == 'local_delivery')
								@if(isset($data->ProductList))
								<a class="btn btn-primary" title="Details" href="local-delivery-detail/{{$data->_id}}"><i class="fa fa-eye"></i> View Details</a>&nbsp;
								@else
								<a class="btn btn-primary" title="Details" href="delivery-details/{{$data->_id}}"><i class="fa fa-eye"></i> View Details</a>&nbsp;
							    @endif
						@endif
						
						@if(Input::get('list') == 'new')     
								@if($data->RequestType == 'buy_for_me')
									@if($data->Status == 'pending')
									<a class="btn btn-primary" href="edit-buy-for-me/{{$data->_id}}"><i class="fa fa-pencil"></i> Edit</a>&nbsp;
									@endif
								@elseif($data->RequestType == 'online')
									@if($data->Status == 'pending')
										<a class="btn btn-primary" href="edit-online-purchase/{{$data->_id}}"><i class="fa fa-pencil"></i> Edit</a>&nbsp;
									@endif
								@else
								@if($data->Status == 'pending')
							    <a class="btn btn-primary" href="{{url('edit-request',$data->_id)}}" title="Edit"><i class="fa fa-pencil"></i> Edit</a>&nbsp;
							@endif
							@endif
								@if($data->Status == 'pending')
								<a class="btn  btn-danger" title="cancel" id="delete"  onclick="remove_record('delete-request/{{$data->_id}}/DeliveryRequest','{{$data->_id}}')" href="javascript:void(0)" >Cancel</a>
								@endif

							@elseif(Input::get('list') == 'current')                                                        
													
					  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="view_on_map('{{json_encode($data->PickupLatLong)}}','{{json_encode($data->DeliveryLatLong)}}')" >View on Map</button>
					  	
								<a class="btn btn-primary" href="javascript:void(0)" id="chat-loader-{{$data->_id}}" onclick="open_chat_box('{{$data->TransporterId}}','{{$data->TransporterName}}','chat-loader-{{$data->_id}}')"><i class="fa fa-chat"></i> Chat</a>

								@if($data->RequestType == 'buy_for_me')
                                	<a class="btn btn-primary" href="buy-for-me-detail/{{$data->_id}}"><i class="fa fa-eye"></i>View Details</a>&nbsp;
								@endif

								@if($data->RequestType == 'online')
                                	<a class="btn btn-primary" href="online-request-detail/{{$data->_id}}"><i class="fa fa-eye"></i>View Details</a>&nbsp;
								@endif
						@endif   
                    </div>
                    
                </div>
            </div>
        </div>  

        @endforeach 
		@else
			<div class="col-sm-12 col-xs-12 box-shadow">
				<br><div align="center" class=""><b>No Record Found</b></div><br>
			</div>
		@endif
    </div>
   </div>
    <script type="text/javascript">
$(document).ready(function() {
	  
		$(".fancybox").fancybox();

	});
</script>
@extends('User::list.pagination.footer2') 
<style>
	
.image-css
{
	background-color:#219ed4;
	border-radius:25px 25px 25px 25px;
}


</style>
