<div class="">
    <div class="">
		@if(count($requests) > 0)
        @foreach($requests as $data)
        <?php $visible = true;?>
        @if(isset($data->ProductList))
        <?php
foreach ($data->ProductList as $key) {
    if (@$key['status'] == 'delivered') {

        $visible = true;
        if (Input::get('list') == 'completed') {
            $visible = true;
        }
    }
    ?>
		<?php
}
?>
		@endif
		@if($visible)
        @if($data->TripId != '')
           <div class="col-sm-12 box-shadow trip-img" id="row-{{$data->_id}}">
        @else
			<div class="col-sm-12 box-shadow" id="row-{{$data->_id}}">
        @endif

            <div class="list-group-custom">
				<div class="row">
					<div class="col-md-2 col-sm-3">

						@if($data->RequestType == 'online' || $data->RequestType == 'buy_for_me')
							<?php $image = '';
foreach ($data->ProductList as $key) {
    if (!empty(trim($key['image']))) {
        $image = $key['image'];
        break;
    }
}
?>
							@if(!empty(trim($image)))
								<a class="fancybox" rel="group" href="{{ ImageUrl.$image}}" >
									<img src="{{ ImageUrl.$image}}" class="margin-top" width="150px" height="150px" onclick="return false;" >
								</a>
								@else
								<img src="{{ ImageUrl}}no-image.jpg" width="" class="margin-top" height="150px"  class="img-rounded">
							@endif
						@endif

						@if($data->RequestType == 'delivery')
							@if($data->ProductImage != '')
								<a class="fancybox" rel="group" href="{{ ImageUrl.$data->ProductImage }}" onclick="return false;" >
									<img src="{{ ImageUrl.$data->ProductImage}}" class="margin-top" width="150px" height="150px" >
								</a>
							@else
								<img src="{{ ImageUrl}}no-image.jpg" width="150px" class="margin-top" height="150px"  class="img-rounded">
							@endif

						@endif
					</div>
					<div class="col-md-10 col-sm-9">

						<h3>{{ucfirst($data->ProductTitle)}}</h3>
						<p><b>Status:</b>  {{ get_status( $data->Status, $data->RequestType ) }}</p>
						@if($data->Status == 'delivered' && $data->RequestType == 'delivery')
							<p><b>Delivered Date:</b> {{ show_date($data->DeliveredTime) }}</p>
						@else
							<!-- <p><b>Expected Delivery Date:</b> {{ show_date($data->DeliveryDate) }}</p> -->
						@endif
						<p ><b>Drop off Location:</b>
							{{ucfirst($data->DeliveryFullAddress)}}&nbsp;{{$data->DeliveryPincode}}</p>
						
						<p><b>Created on: </b> {{ show_date($data->EnterOn) }}</p>
						<br />
                        @if($data->RequestType == 'online')
                        	<img src="{{THEME}}web/promo/images/online_purchase_blue.png" width="30px" class="" height="30px"  class="img-rounded " >
                        	<b>&nbsp;&nbsp;Item Delivery Request</b>

                        @elseif($data->RequestType == 'buy_for_me')
                        	<img src="{{THEME}}web/promo/images/buy_for_me_blue.png" width="30px" class="" height="30px"  class="img-rounded image-css" >
                        	<b>&nbsp;&nbsp;Item Purchase request</b>
                        @endif
                    </div>
                </div>
            </div>
            <div class="list-footer">
                <div class="row">
					<div class="col-sm-offset-2 col-sm-10" >
						@if($data->RequestType == 'online')
							<a class="btn btn-primary" title="Details" href="online-request-detail/{{$data->_id}}"><i class="fa fa-eye"></i> View Details</a>&nbsp;
						@elseif($data->RequestType == 'delivery')
							<a class="btn btn-primary" title="Details" href="delivery-details/{{$data->_id}}"><i class="fa fa-eye"></i> View Details</a>&nbsp;
						@elseif($data->RequestType == 'buy_for_me')
                            <a class="btn btn-primary" title="Details"  href="buy-for-me-detail/{{$data->_id}}"><i class="fa fa-eye"></i> View Details</a>&nbsp;
						@endif
						@if(Input::get('list') == 'new')
							@if($data->RequestType == 'buy_for_me')
								@if($data->Status == 'pending')
                                <a class="btn btn-primary" title="Edit" href="edit-buy-for-me/{{$data->_id}}"><i class="fa fa-pencil"></i> Edit</a>&nbsp;
                                @endif
                                <a class="btn  btn-danger" title="Delete" onclick="remove_record('delete-request/{{$data->_id}}/DeliveryRequestOnline','{{$data->_id}}','Do you want to delete Item Purchase request?')" href="javascript:void(0)" >Delete</a>
                            @elseif($data->RequestType == 'online')
                            	@if($data->Status == 'pending')
                            		<a class="btn btn-primary" title="Edit" href="edit-online-purchase/{{$data->_id}}"><i class="fa fa-pencil"></i> Edit</a>&nbsp;
                            	@endif

                            	<a class="btn  btn-danger" title="Delete" onclick="remove_record('delete-request/{{$data->_id}}/DeliveryRequestOnline','{{$data->_id}}','Do you want to delete item delivery request?')" href="javascript:void(0)" >Delete</a>


							@else
								@if($data->Status == 'pending')
							    <a class="btn btn-primary" href="{{url('edit-request',$data->_id)}}" title="Edit"><i class="fa fa-pencil"></i> Edit</a>&nbsp;
							    @endif
							    <a class="btn  btn-danger" title="Delete" onclick="remove_record('delete-request/{{$data->_id}}/DeliveryRequest','{{$data->_id}}','Do you want to delete delivery request?')" href="javascript:void(0)" >Delete</a>

							@endif

						@elseif(Input::get('list') == 'current')
					  		<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="view_on_map('{{json_encode($data->PickupLatLong)}}','{{json_encode($data->DeliveryLatLong)}}')" >View on Map</button>
					  		@if($data->RequestType == 'delivery')
								<a class="btn btn-primary" href="javascript:void(0)" id="chat-loader-{{$data->_id}}" onclick="open_chat_box('{{$data->TransporterId}}','{{$data->TransporterName}}','chat-loader-{{$data->_id}}')"><i class="fa fa-chat"></i> Chat</a>
							@endif

						@endif
                    </div>

                </div>
            </div>
        </div>

        @endif
        @endforeach
		@else
			<div class="col-sm-12 box-shadow">
				<br><div align="center" class=""><b>You have no requests yet. You can prepare a new delivery request by clicking on Create Request.</b></div><br>
			</div>
		@endif
    </div>
   </div>
    <script type="text/javascript" >

    	/*if ($(".fancybox").fancybox().length){
    		alert('ok');
    	}else{
    		alert('no');
    	}*/
		//console.log($(".fancybox").fancybox().length);
	    var $= jQuery.noConflict();


		$(document).ready(function() {
			$(".fancybox").fancybox();
		});
	</script>
@extends('User::list.pagination.footer2')
<style>

.image-css
{
	background-color:#219ed4;
	border-radius:25px 25px 25px 25px;
}


</style>