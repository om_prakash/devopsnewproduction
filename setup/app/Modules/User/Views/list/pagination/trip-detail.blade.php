



<div class="">

		@if(count($requests) > 0)
        	@foreach($requests as $data)
		<div class="col-sm-12 box-shadow">

                <div class="list-group-custom">
					<div class="row">

						<div class="col-sm-12"> <br>
			        		<b>
								@if(isset($data->SourceDate->sec))
									<p id="showdate">{{show_date($data->SourceDate)}}</p>
								@endif
			        		</b>
		            		<hr />
		        		</div>


		        	<div class="col-sm-12">
						<div class="row">

							<div class="col-md-8 col-sm-6 details-posts">
								<h4 class="color-blue"></h4>
								<div id="Date">
									@if(isset($data->UpdateDate->sec))
										<p id="showdate">{{show_date($data->UpdateDate)}}</p>
									@endif
								</div>
								<p><b>Source Address:</b>{{ucfirst($data->SourceCity)}}, {{ucfirst($data->SourceState)}}, {{ucfirst($data->SourceCountry)}}</p>
								<p><b>Destination Address:</b> {{ucfirst($data->DestiCity)}}, {{ucfirst($data->DestiState)}}, {{ucfirst($data->DestiCountry)}}</p>
								<p><b>Categories:</b>  	<?php
if (!empty($data->SelectCategory)) {

    echo ucfirst(implode(",", $data->SelectCategory));
}
?>	</p>
								<p><b>Weight upto: </b>{{$data->Weight}}{{$data->Unit}}</p>
								<p><b>Shipping Mode: </b>{{ucfirst($data->TravelMode)}}</p>

							</div>
						</div>
					</div>



                    </div>
                </div>


                <div class="list-footer">
                    <div class="row">

						<div class="col-sm-offset-2 col-sm-10 ">


									<button class="btn btn-default blue-btn pull-right" data-toggle="modal" data-target="#exampleModal{{$data->_id}}" title="Trip Detail"   id="#" > &nbsp;View Detail</button>&nbsp;&nbsp;

						</div>

                    </div>
                </div>
            </div>

<!--model-->
<div class="modal fade" id="exampleModal{{$data->_id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
	<div class="modal-content">
		  	<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="exampleModalLabel">Trip Detail</h4>
		  	</div>




				<div class="modal-body">
					@if(isset($data->SourceDate->sec))
						<p id="showdate">{{show_date($data->SourceDate)}}</p>
					@endif

					<p><b>Source Address: </b>{{ucfirst($data->SourceCity)}}, {{ucfirst($data->SourceState)}}, {{ucfirst($data->SourceCountry)}}</p>
					<p><b>Destination Address: </b> {{ucfirst($data->DestiCity)}}, {{ucfirst($data->DestiState)}}, {{ucfirst($data->DestiCountry)}}</p>
					<p><b>Categories: </b>  	<?php
if (!empty($data->SelectCategory)) {

    echo ucfirst(implode(",", $data->SelectCategory));
}
?>	</p>
					<p><b>Weight upto: </b>{{$data->Weight}}{{$data->Unit}}</p>
					<p><b>Shipping Mode: </b>{{ucfirst($data->TravelMode)}}</p>
					<p><b>Discription: </b>{{ucfirst($data->Description)}}</p>
					<hr>
					<div class="">
						<b class="color-blue">Transporter Information</b>
					</div><br/>
					@if(count($transporter_info) > 0)
						<div class="col-sm-4">
							@if($transporter_info->Image != '')
		                           <img id="senior-preview" src="{{ ImageUrl.'/'.$transporter_info->Image}}"  width="100px" height="100px" />
		                           @else
		                           <img id="senior-preview" src="{{ ImageUrl}}/no-image.jpg"  width="100px" height="100px" />&nbsp;&nbsp;
		                    @endif
	                    </div>

	                    <div class="col-sm-4">
	                   {{$transporter_info->FirstName}}
	                   		<?php $average = 0;?>
	                   		 @if($transporter_info->RatingCount > 0 && $transporter_info->RatingByCount > 0)
		                    	<?php
$value = $transporter_info->RatingCount / $transporter_info->RatingByCount;
$average = $value * 20;
?>

		                    @endif
		                    	<br><br>
		                    <div class="star-ratings-sprite pull-left">
				 				 <span style="width:<?php echo $average ?>%" class="star-ratings-sprite-rating"></span>
							</div>
	                    </div>


	                    <div class="col-sm-4">

						</div>



					<div class="col-sm-12" style="text-align:center;">
					<br/>

					<a class="btn btn-default blue-btn" href="{{url('prepare-request')}}?id={{$data->_id}}"> &nbsp;Create Package</a>&nbsp;&nbsp;

					<hr></div>
				</div>
				@endif

				<div class="clearfix"></div>
	</div>
  </div>
</div>
<!--end -->


        @endforeach
		@else

		<div class="col-sm-12 box-shadow">
				<br><div align="center" class=""><b>No Record Found</b></div><br>
		</div>
		@endif

</div>

@extends('User::list.pagination.footer2')

















 <script>

$(document).ready(function(){
    $("showdate").click(function(){
        $("p").hide( function(){

        });
    });
});

</script>


