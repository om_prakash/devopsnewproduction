<div class="">
    <div class="row">		 
		@if(count($request) > 0)
        @foreach($request as $data)
		<div class="col-sm-12 box-shadow" id="row-{{$data->_id}}">
            <div class="list-group-custom">
				<div class="row">
					<div class="col-sm-2">
						 @if($data->Image != '')
							<a class="fancybox" rel="group" href="{{ ImageUrl.($data->Image) }}" >
								<img src="{{ ImageUrl.($data->Image)}}" class="margin-top" width="150px" height="150px" >
							</a>        
						@else
								<img src="{{ ImageUrl}}no-image.jpg" width="150px" class="margin-top" height="150px"  class="img-rounded">
						@endif
					</div>
						<div class="col-sm-10">

						<h3>{{ucfirst($data->ProductTitle)}}</h3>
						
							@if(isset($data->DeliveryDate->sec))
								<p><b>Drop of Date :</b> {{date('l,  M  d, Y ',$data->DeliveryDate->sec)}}</p>
							@endif

							<p ><b>Drop off Location :</b> {{ucfirst($data->DeliveryFullAddress)}}&nbsp;{{$data->DeliveryPincode}}</p>
							  <?php 
									if(ucfirst($data->Status) == 'Out_for_pickup')
										{ echo   "<strong>Status :</strong> Out for Pickup";  }
									else if(ucfirst($data->Status) == 'Out_for_delivery')
										{ echo "<strong>Status :</strong> Out of delivery"; }
								   	else
										{  $result = $data->Status; 
									echo "<strong>Status :</strong> ".ucfirst($result);  }
		               	                          ?><p></p>
		               	    @if(isset($data->EnterOn->sec))
								<p><b>Created Date : </b> {{date('l, M d, Y ',$data->EnterOn->sec)}}</p>
                            @endif 

                            @if($data->RequestType == 'online')
                            	<img src="{{THEME}}web/promo/images/online_purchase_blue.png" width="30px" class="margin-top" height="30px"  class="img-rounded " >
                            	<b>&nbsp;&nbsp;Item Delivery Request</b>
                            
                            @elseif($data->RequestType == 'buy_for_me')
                            	<img src="{{THEME}}web/promo/images/buy_for_me_blue.png" width="30px" class="margin-top" height="30px"  class="img-rounded image-css" >
                            	<b>&nbsp;&nbsp;Item Purchase request</b>

                            @endif
                             
                      	</div>
                </div>
            </div>   
            <div class="list-footer">
                <div class="row">
					<div class="col-sm-offset-2 col-sm-10" >                              
						 
							@if($data->RequestType == 'online')
								<a class="btn btn-primary" href="{{url('online-request-detail',$data->_id)}} "><i class="fa fa-eye"></i>View Details</a>&nbsp;		
							@endif

							@if($data->RequestType == 'delivery')
								<a class="btn btn-primary" href="{{url('transporter/delivery-details',$data->_id)}}"><i class="fa fa-eye"></i>View Details</a>&nbsp;
							@endif  

							@if($data->RequestType == 'buy_for_me')
                                
                                <a class="btn btn-primary" href="{{url('buy-for-me-detail',$data->_id)}}"><i class="fa fa-eye"></i>View Details</a>&nbsp;
							@endif
							
						
                    </div>
                    
                </div>
            </div>
        </div>  

        @endforeach 
		@else
			<div class="col-sm-12 box-shadow">
				<br><div align="center" class=""><b>None at this time</b></div><br>
			</div>
		@endif
    </div>
   </div>
@extends('User::list.pagination.footer2') 
