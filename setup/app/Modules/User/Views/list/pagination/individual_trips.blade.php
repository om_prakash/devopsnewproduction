<div class="col-sm-12">
	@if(count($requests) > 0)
    @foreach($requests as $data)
	<div class="col-sm-12 box-shadow" id="row-{{$data->_id}}">

        <div class="list-group-custom">
		<div class="row"><br>


		<div class="col-sm-12">
				<p>
					<b>Source Address:</b>

					{{ucfirst($data->SourceCity)}}, {{ucfirst($data->SourceState)}}, {{ucfirst($data->SourceCountry)}}
				</p>
				<p>
					<b>Departure date:</b>
					{{ show_date(@$data->SourceDate,'M d, Y') }} at
					{{ show_date(@$data->SourceDate,'h:i A')}}
				</p>
				<p>
					<b>Destination Address:</b>
					{{ucfirst($data->DestiCity)}}, {{ucfirst($data->DestiState)}}, {{ucfirst($data->DestiCountry)}}
				</p>
				<p>
					<b>Arrival date:</b>
					{{ show_date(@$data->DestiDate,'M d, Y') }} at
					{{ show_date(@$data->DestiDate,'h:i A')}}
				</p>
				<p>
					<b>Category:</b>


					<?php
if (!empty($data->SelectCategory)) {

    echo implode(",", $data->SelectCategory);
}
?>
				</p>
				<p>
					<b>Status:</b>
					{{ucfirst($data->Status)}}
				</p>


			<div class="row">
				<div class="col-sm-12"><hr></div>
				<div class="col-sm-4">
					<label>Shipping Mode:</label>
					{{ucfirst($data->TravelMode)}}
				</div>
				<div class="col-sm-4">
					<label>Weight upto:</label>
					{{$data->Weight}} {{$data->Unit}}
				</div>
				@if($data->TravelMode == 'air')
				<div class="col-sm-4">
					<label>Airline:</label>
					{{ucfirst($data->Airline)}}
				</div>
				<div class="col-sm-12">
					<label>Flight No.:</label>
					{{$data->FlightNo}}
				</div>

				@endif
				<div class="col-sm-12">
					<label>Description:</label>
					{{ucfirst($data->Description)}}
				</div>

		</div>

			</div>
		</div>
        </div>

        <div class="list-footer" id="footer_div">
            <div class="row">
				<div class="col-sm-12 text-right " >
				@if($data->Status == 'active')
					<a class="btn  btn-danger" href="{{url('transporter/change-status',[$data->_id,'inactive'])}}" onclick="return confirm('Are you sure? You want to deactivate this trip!');">Deactivate Trip</a>
				@else
					<a class="btn btn-primary" href="{{url('transporter/change-status',[$data->_id,'active'])}}" onclick ="return confirm('Are you sure? You want to activate this trip!');">Activate Trip</a>
				@endif

					<a class="btn btn-primary" href="{{url('transporter/view-request',[$data->_id])}}"><i class="fa fa-eye"></i> View Request(s)</a>

					@if(in_array($tab,['current','']) )
					<a class="btn btn-primary" href="{{url('transporter/edit_individual',[$data->_id])}}"><i class="fa fa-pencil"></i> Edit</a>
					@endif

					<a class="btn  btn-danger" title="Delete" id="delete"  onclick="remove_record('delete-request/{{$data->_id}}/individual_trip','{{$data->_id}}')" href="javascript:void(0)" ><i class="fa fa-trash"></i> Delete</a>



                </div>
            </div>
        </div>

    </div>
    @endforeach
	@else
	<div class="col-sm-12 box-shadow">
			<br><div align="center" class=""><b>No Record Found</b></div><br>
		</div>
	@endif
</div>
@extends('User::list.pagination.footer2')
































