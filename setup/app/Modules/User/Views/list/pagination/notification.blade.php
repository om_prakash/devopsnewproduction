		@if(count($notification) > 0)
        @foreach($notification as $data)
		<div class="col-sm-4 col-xs-12" id="row-{{$data->_id}}">


      <div class="box-shadow notify_box">
       <div class="col-sm-2 col-xs-2">
          <i class="fa fa-bell fa-2x style"></i>
         </div>

      <div class="col-sm-10 notify col-xs-10"  >
      <p> </p>
      <h5 class="notif-title"><b>{{$data->NotificationTitle}}</b></h5>
      <p >{{$data->NotificationMessage}}</p>
      <p> {{ show_date(@$data->Date) }} </p>

      </div>
      <div class="col-sm-12 col-xs-12">
           <span class="pull-right"><a title="Delete" id="delete"  title="delete" onclick="remove_record('delete-request/{{$data->_id}}/notification','{{$data->_id}}')" href="javascript:void(0)" > <i class="fa fa-trash" aria-hidden="true"></i></a>
      </span>
      </div>
    </div>

    </div>


        @endforeach
		@else
			<div class="col-sm-12 col-xs-12 box-shadow">
				<br><div align="center" class=""><b>None at this time</b></div><br>
			</div>
		@endif

@extends('User::list.pagination.footer2')

<style>
.notify_box .col-sm-2{padding:0; text-align:center;}
.notify_box .col-sm-2 .fa{margin-top:20px;}
</style>