<div class="">
   <div class="">

      @if(count($requests) > 0)
      @foreach($requests as $data)


      @if(isset($data['RequestType']) && in_array(@$data['RequestType'],['online','buy_for_me']))
      <?php foreach ($data['ProductList'] as $value) {
	?>
	      @if(@$value['tpid'] == Session()->get('UserId') &&
        ((in_array($value['status'], ['assign','out_for_pickup','out_for_delivery','accepted']) && Input::get('list') == 'current') ||
        (in_array($value['status'], ['delivered','canceled']) && Input::get('list') != 'current')
        ))
	      <div class="col-sm-12 col-xs-12 box-shadow">
	         <div class="list-group-custom">
	            <div class="row">

	               <div class="col-sm-3 col-md-2">
	                  @if(@$data['ProductList'][0]['ProductImage'] != '')
	                  <a class="fancybox" rel="group" href="{{ImageUrl.(@$data['ProductList'][0]['ProductImage']) }}" >
	                  <img src="{{ImageUrl.(@$data['ProductList'][0]['ProductImage'])}}" class="margin-top" width="150px" height="150px" >
	                  </a>
	                  @else
	                  <img src="{{ImageUrl}}/no-image.jpg" width="150px" class="margin-top" height="150px"  class="img-rounded">
	                  @endif



	               </div>
	               <div class="col-sm-9 col-md-10 col-xs-12">
	                  <h3>{{ucfirst(@$value['product_name'])}}</h3>
                    <p><b>Status :</b> {{ get_status(@$value['status'], @$data['RequestType']) }}</p>
                    <p><b>Requester Name :</b> {{ ucfirst(@$data['RequesterName']) }}</p>
	                  @if(in_array($value['status'], ['delivered']))
	                     <p><b>Delivered At:</b> {{ show_date(@$value['DeliveredDate']) }}</p>
                    @endif

                    <p><b>Expected Delivery Date :</b> @if(@$value['ExpectedDate'] != '') {{ show_date(@$value['ExpectedDate']) }} @else N/A @endif </p>
                    <p><b>Created On  : </b>{{show_date(@$data->EnterOn)}}</p>



	                  @if($data->RequestType == 'online')
	                  <img src="{{THEME}}web/promo/images/online_purchase_blue.png" width="30px" class="margin-top" height="30px"  class="img-rounded " >
	                  <b>&nbsp;&nbsp;Online Purchase</b>
	                  @elseif($data->RequestType == 'buy_for_me')
	                  <img src="{{THEME}}web/promo/images/buy_for_me_blue.png" width="30px" class="margin-top" height="30px"  class="img-rounded image-css" >
	                  <b>&nbsp;&nbsp;Buy For Me</b>
	                  @endif
	               </div>
	            </div>
	         </div>
	         <div class="list-footer">
	            <div class="row">
	               <div class="col-sm-offset-2 col-sm-10">
	               @if($data['RequestType'] == 'buy_for_me')
	                  <a class="btn btn-primary" href="{{url('transporter/buy_for_me_detail',@$value['_id'])}}">View Detail</a>
	               @elseif($data['RequestType'] == 'online')
	                  <a class="btn btn-primary" href="{{url('transporter/online_detail',@$value['_id'])}}">View Detail</a>
                 @elseif($data['RequestType'] == 'delivery')
                    <a class="btn btn-primary" href="{{url('transporter/delivery-details',@$value['_id'])}}"> View Detail</a>
                 @elseif($data['RequestType'] == 'local_delivery')
                    <a class="btn btn-primary" href="{{url('transporter/local-delivery-details',@$value['_id'])}}"> View Detail</a>
	               @endif

	               @if(in_array($value['status'],['accepted','assign','out_for_pickup','out_for_delivery']))
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="view_on_map('{{json_encode($data->PickupLatLong)}}','{{json_encode($data->DeliveryLatLong)}}')" >View on Map</button>

	               		<!-- <a class="btn btn-primary" href="javascript:void(0)" id="mdc-{{$data->RequesterId}}" onclick="open_chat_box('{{$data->RequesterId}}','{{$data->RequesterName}}','mdc-{{$data->RequesterId}}')">Chat</a> -->
	               @endif

	               </div>
	            </div>
	         </div>
	      </div>
	      @endif
      <?php }?>
      @else
      <div class="col-sm-12 col-xs-12 box-shadow">
         <div class="list-group-custom">
               @if(isset($data['ProductList']))
               @if($data['RequestType'] == 'delivery' || $data['RequestType'] == 'local_delivery')
              @foreach($data['ProductList'] as $value)
               @if(@$value['tpid'] == Session()->get('UserId'))
                    <div class="row">
                    <div class="col-sm-2 col-xs-12">
                    <img src="{{ImageUrl}}/no-image.jpg" width="150px" class="margin-top" height="150px"  class="img-rounded">
                    </div>
                     <div class="col-sm-10 col-xs-12">
                      <h3>{{ $value['product_name'] }}</h3>
                      <p><b>Status :</b> {{ get_status(@$value['status']) }}</p>
                      <p><b>Requester Name :</b> {{ ucfirst(@$data['RequesterName']) }}</p>
                      @if(in_array($data->Status, ['delivered']))
                        <p><b>Delivered At:</b> {{ show_date(@$data->DeliveredTime) }}</p>
                      @endif

                      <p><b>Expected Delivery Date :</b> @if(@$value['ExpectedDate'] != '') {{ show_date(@$value['ExpectedDate'])}} @else N/A @endif </p>

                       <p><b>Created On:</b> {{ show_date(@$data['EnterOn']) }}</p>
                       <p><b>Package Type:</b>
                      @if($data['RequestType'] == 'buy_for_me')
                            Buy For Me
                      @elseif($data['RequestType'] == 'online')

                            Online Package
                      @elseif($data['RequestType'] == 'local_delivery')
                            Local Delivery
                      @else
                            Send a Package
                      @endif

                  </p>
                     </div>
                    </div>
                    <div class="list-footer">
            <div class="row">
               <div class="col-sm-offset-2 col-sm-10">
               @if($data['RequestType'] == 'local_delivery')
                <a class="btn btn-primary" href="{{ url('transporter/local-delivery-details/'.$value['_id']) }}">View Detail</a>
               @else
                <a class="btn btn-primary" href="{{ url('transporter/delivery-details/'.$value['_id']) }}">View Detail</a>
               @endif

                
                   @if(in_array($data->Status, ['accepted','out_for_pickup','out_for_delivery']))
                   @endif


               </div>
               </div>
               </div>
               </div>

              @endif
              
              @endforeach
              @endif
              @else
            <div class="row">
               <div class="col-sm-2 col-xs-12">
                  @if($data->ProductImage != '')
                  <img src="{{ImageUrl.($data->ProductImage)}}" class="margin-top" width="150px" height="150px" >
                  @else
                  <img src="{{ImageUrl}}/no-image.jpg" width="150px" class="margin-top" height="150px"  class="img-rounded">
                  @endif
               </div>
               <div class="col-sm-10 col-xs-12">
                  <h3>{{ucfirst($data->ProductTitle)}}</h3>
                  <p><b>Status :</b> {{ get_status(@$data->Status) }}</p>
                  <p><b>Requester Name :</b> {{ ucfirst(@$data['RequesterName']) }}</p>
                  @if(in_array($data->Status, ['delivered']))
                    <p><b>Delivered At:</b> {{ show_date(@$data->DeliveredTime) }}</p>
                  @else
                    <p><b>Desired Delivery Date:</b> {{ show_date(@$data->DeliveryDate) }}</p>
                  @endif
                  <p><b>Created On:</b> {{ show_date(@$data->EnterOn) }}</p>
                  <p><b>Package Type:</b>
                      @if($data->RequestType == 'buy_for_me')
                            Buy For Me
                      @elseif($data->RequestType == 'online')
                            Online Package
                      @else
                            Send a Package
                      @endif
                  </p>
               </div>
            </div>
         </div>
         <div class="list-footer">
            <div class="row">
               <div class="col-sm-offset-2 col-sm-10">
                  <a class="btn btn-primary" href="{{url('transporter/package-delivery-details',$data->_id)}}">View Detail</a>
                   @if(in_array($data->Status, ['accepted','out_for_pickup','out_for_delivery']))
	               		<a class="btn btn-primary" href="javascript:void(0)" id="mdc-{{$data->RequesterId}}" onclick="open_chat_box('{{$data->RequesterId}}','{{$data->RequesterName}}','mdc-{{$data->RequesterId}}')">Chat</a>
	               @endif
               </div>
            </div>
         </div>
          @endif
      </div>
      @endif
      @endforeach
      @else
      <div class="col-sm-12 col-xs-12 box-shadow">
         <br>
         <div align="center" class=""><b>No Record Found</b></div>
         <br>
      </div>
      @endif
   </div>
</div>
@extends('User::list.pagination.footer2')

