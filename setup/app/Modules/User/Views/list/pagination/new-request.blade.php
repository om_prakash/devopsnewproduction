<?php /* echo "<pre>"; print_r($transporter);  die;  */?>
@if(count($transporter) > 0)
@foreach($transporter as $data)

	<div class="col-md-4 col-sm-6">
		<div class="box-shadow-list">
			<div class="img-hldr clearfix">
				<div>
					@if($data->ProductImage != '')
                       @if(file_exists(BASEURL_FILE.$data->ProductImage))
                         <a class="fancybox" rel="group" href="{{ImageUrl.($data->ProductImage)}}" >
							<img src="{{ImageUrl.($data->ProductImage)}}"  width="100%" >
					    </a>
                       @endif
					@else
						<img width="100%" height="" src="{{ImageUrl}}/no-image.jpg">

					@endif
				</div>
			</div>
			<div class="content-hldr">
            	<div class="content-min-h-tp">
					<div class="details-list"><b>Title : </b>{{ucfirst($data->ProductTitle)}}</div>
					<div class="details-list"><b>Pickup Address : </b> {{ucfirst($data->PickupFullAddress)}}</div>
					<div class="details-list"><b>Pickup Date : </b>
					{{ show_date(@$data->PickupDate) }}</div>
					<div class="details-list"><b>Delivery Address : </b> {{ucfirst($data->DeliveryFullAddress)}}</div>
					<div class="details-list"><b>Delivery Date : </b>
					{{ show_date(@$data->DeliveryDate) }}</div>
                </div>
                <hr />
				<div class="row">
					<div class="col-xs-6">
						<a class="btn btn-primary btn-block" onclick="view_on_map('{{json_encode($data->PickupLatLong)}}','{{json_encode($data->DeliveryLatLong)}}')">View On Map</a>
					</div>
					<div class="col-xs-6">
						<!-- <a class="btn btn-primary btn-block" href="transporter/details/{{$data['ProductList'][0]['_id']}}">Detail</a> --> 
						<!-- change yesterday -->
						<!-- <a class="btn btn-primary btn-block"  href="javascript:void(0)" onclick="return get_title('{{$data}}')">Detail</a> -->
						<a class="btn btn-primary btn-block"  href="javascript:void(0)" onclick="return get_title('{{$data}}')">Detail</a>
					</div>
				</div>
			</div>
		</div>
	</div>
@endforeach
@elseif($page == 0)
	<div class="col-sm-12 box-shadow">
		<br><div align="center" class=""><b>No Record Found</b></div><br>
	</div>
@endif



<button id="hide_b" href="javascript:void(0)" data-toggle="modal" data-target="#modal2" style="display:none"></button>

   <script type="text/javascript">
$(document).ready(function() {

		$(".fancybox").fancybox();

	});

function get_title(obj){
		
		$('#apend_div').html('');
		var obj2 = JSON.parse(obj);
		var strin = '';
		var session_id = '<?php echo Session::get('UserId')?>';
		if(obj2.hasOwnProperty('ProductList')){
			$.each( obj2.ProductList, function( key, value ) {
				if(value.tpid == '' || value.tpid == null){
					strin += '<h4><a href="transporter/details/'+value._id+'" style="color:black;">'+value.product_name+'</a></h4>';
				}else if(value.tpid == session_id){
					strin += '<h4><a href="transporter/details/'+value._id+'" style="color:black;">'+value.product_name+'</a></h4>';
				}


			});


			$('#apend_div').html(strin);
			$('#hide_b').trigger('click');
		}else{
			document.location.href = SITEURL +'transporter/details/'+obj2._id;
		}
		
	}
</script>

@if(count($transporter) < $per_page)
	<script>$('#load_more').hide(); </script>
@endif

<div id="modal2" class="modal fade in" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
	           <button type="button" class="close" data-dismiss="modal" id="edit_item_close">&times;</button>
	           <h4 class="modal-title">Get Detail</h4>
        	</div>
        	<div class="modal-body">
        		<div id = "apend_div" style="color:black;"></div>
        		
        		
        	</div>
        	<div class="modal-footer text-right" >
		        <button type="button" class="btn btn-default " data-dismiss="modal" style="float: right;">Close</button>
		    </div>
		</div>
	</div>
</div>

