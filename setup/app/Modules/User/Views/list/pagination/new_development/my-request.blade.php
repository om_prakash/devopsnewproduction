@if(count($requests) > 0)
    @foreach($requests as $data)
    <div class="col-sm-12 box-shadow" id="row-{{$data->_id}}">
    	<div class="list-group-custom">

          <!-- Start loop -->
				<div class="row">
					<!-- start online and buyforme section -->
					@if($data->RequestType == 'online' || $data->RequestType == 'buy_for_me')
					<div class="col-md-2 col-sm-3">
						<?php $image = '';
						foreach ($data->ProductList as $key) {
						    if (!empty(trim(@$key['image']))) {
						        $image = @$key['image'];
						        break;
						    }
						}
						?>
						@if(!empty(trim($image)))
							<a class="fancybox" rel="group" href="{{ ImageUrl.$image}}" >
								<img src="{{ ImageUrl.$image}}" class="margin-top" width="150px" height="150px" onclick="return false;" >
							</a>
							@else
							<img src="{{ ImageUrl}}no-image.jpg" width="" class="margin-top" height="150px"  class="img-rounded">
						@endif
					</div>

					<div class="col-md-10 col-sm-9">
						<h3>{{ucfirst($data->ProductTitle)}}</h3>
						<p><b>Status:</b>  {{ get_status( $data->Status, $data->RequestType ) }}</p>
						
						<p ><b>Drop off Location:</b>
						{{ucfirst($data->DeliveryFullAddress)}}&nbsp;{{$data->DeliveryPincode}}</p>
						<p><b>Created on: </b> {{ show_date($data->EnterOn) }}</p>
						<!-- <p><b>Expected Delivery Date:</b> 
						@if($data->DeliveryDate != '')
							{{ show_date($data->DeliveryDate) }}
						@else
							N/A
						@endif
 -->
						</p>
						<!-- <p><b>Package Type: </b>@if($data->RequestType == 'online') Online Purchases @else Buy For Me @endif</p> -->
						<br>
						@if($data->RequestType == 'online')
	                    	<img src="{{THEME}}web/promo/images/online_purchase_blue.png" width="30px" class="" height="30px"  class="img-rounded " >
	                    	<b>&nbsp;&nbsp;Online Purchase</b>

	                    @elseif($data->RequestType == 'buy_for_me')
	                    	<img src="{{THEME}}web/promo/images/buy_for_me_blue.png" width="30px" class="" height="30px"  class="img-rounded image-css">
	                    	<b>&nbsp;&nbsp;Buy For Me</b>
	                    @endif
	                    <br><br>
					</div>
                 <!-- close online and buyforme section -->

                  <!-- start online local delivery section -->
					@elseif($data->RequestType == 'local_delivery')

						@if(isset($data->ProductList))
							<div class="col-md-2 col-sm-3">
							<?php $image = '';
							foreach ($data->ProductList as $key) {
							    if (!empty(trim(@$key['ProductImage']))) {
							        $image = @$key['ProductImage'];
							        break;
							    }
							}
							?>
						      @if(!empty(trim($image)))
									<a class="fancybox" rel="group" href="{{ ImageUrl.$image}}" >
										<img src="{{ ImageUrl.$image}}" class="margin-top" width="150px" height="150px" onclick="return false;">
									</a>
								@else
									<img src="{{ ImageUrl}}no-image.jpg" width="" class="margin-top" height="150px"  class="img-rounded">
								@endif
							</div>
						  @else
							<div class="col-md-2 col-sm-3">
								@if($data->ProductImage != '')
									<a class="fancybox" rel="group" href="{{ ImageUrl.$data->ProductImage }}" onclick="return false;" >
										<img src="{{ ImageUrl.$data->ProductImage}}" class="margin-top" width="150px" height="150px" >
									</a>
								@else
									<img src="{{ ImageUrl}}no-image.jpg" width="150px" class="margin-top" height="150px"  class="img-rounded">
								@endif
							</div>
						@endif

						<div class="col-md-10 col-sm-9">
							<h3>{{ucfirst($data->ProductTitle)}}</h3>
							<p><b>Status:</b>  {{ get_status( $data->Status, $data->RequestType ) }}</p>
							<!-- <p><b>Delivered Date:</b> {{ show_date($data->DeliveredTime) }}</p> -->
							<p ><b>Drop off Location:</b>
							{{ucfirst($data->DeliveryFullAddress)}}&nbsp;{{$data->DeliveryPincode}}</p>
							<p><b>Created on: </b> {{ show_date($data->EnterOn) }}</p>
							<!-- <p><b>Package Type: </b>Local Delivery</p> -->
							<br>
							<img src="{{THEME}}web/promo/images/local_delivery_rqst.png" width="30px" class="" height="30px"  class="img-rounded image-css" >
	                    	<b>&nbsp;&nbsp;Local Delivery</b>
	                    	<br>
						</div>
						<!-- close local delivery section -->

						<!-- start send package section -->
					@elseif($data->RequestType == 'delivery')

						@if(isset($data->ProductList))
							<div class="col-md-2 col-sm-3">
							<?php $image = '';
							foreach ($data->ProductList as $key) {
							    if (!empty(trim(@$key['ProductImage']))) {
							        $image = @$key['ProductImage'];
							        break;
							    }
							}
							?>
						@if(!empty(trim($image)))
									<a class="fancybox" rel="group" href="{{ ImageUrl.$image}}" >
										<img src="{{ ImageUrl.$image}}" class="margin-top" width="150px" height="150px" onclick="return false;" >
									</a>
								@else
									<img src="{{ ImageUrl}}no-image.jpg" width="" class="margin-top" height="150px"  class="img-rounded">
								@endif
							</div>
						@else
							<div class="col-md-2 col-sm-3">
								@if($data->ProductImage != '')
									<a class="fancybox" rel="group" href="{{ ImageUrl.$data->ProductImage }}" onclick="return false;" >
										<img src="{{ ImageUrl.$data->ProductImage}}" class="margin-top" width="150px" height="150px" >
									</a>
								@else
									<img src="{{ ImageUrl}}no-image.jpg" width="150px" class="margin-top" height="150px"  class="img-rounded">
								@endif
							</div>
						@endif

						<div class="col-md-10 col-sm-9">
							<h3>{{ucfirst($data->ProductTitle)}}</h3>
							<p><b>Status:</b>  {{ get_status( $data->Status, $data->RequestType ) }}</p>
							
							<p ><b>Drop off Location:</b>
							{{ucfirst($data->DeliveryFullAddress)}}&nbsp;{{$data->DeliveryPincode}}</p>
							<p><b>Created on: </b> {{ show_date($data->EnterOn) }}</p>
							<p><b>Delivered Date:</b> 
							@if($data->DeliveredTime != '')
								{{ show_date($data->DeliveredTime) }}
							@else
								N/A
							@endif

							</p>
							<!-- <p><b>Package Type: </b> Send A Package</p> -->
							<br>
							<img src="{{THEME}}web/promo/images/sendpackage.png" width="30px" class="" height="30px"  class="img-rounded image-css" >
	                    	<b>&nbsp;&nbsp;Send A Package</b>
	                    	<br>
						</div>
						<!-- close send package section  -->
                  @endif

			</div><!-- Close row  -->

				<div class="list-footer">
					<div class="row">
						<div class="col-sm-offset-2 col-sm-10">
							@if($data->RequestType == 'online')
								<a class="btn btn-primary" title="Details" href="online-request-detail/{{$data->_id}}"><i class="fa fa-eye"></i> View Details</a>&nbsp;

							@elseif($data->RequestType == 'delivery')
								@if(isset($data->ProductList))
								<a class="btn btn-primary" title="Details" href="send-package-detail/{{$data->_id}}"><i class="fa fa-eye"></i> View Details</a>&nbsp;
								@else
									<a class="btn btn-primary" title="Details" href="delivery-details/{{$data->_id}}"><i class="fa fa-eye"></i> View Details</a>&nbsp;
								@endif


							@elseif($data->RequestType == 'local_delivery')
								@if(isset($data->ProductList))
								<a class="btn btn-primary" title="Details" href="local-delivery-detail/{{$data->_id}}"><i class="fa fa-eye"></i> View Details</a>&nbsp;
								@else
								<a class="btn btn-primary" title="Details" href="delivery-details/{{$data->_id}}"><i class="fa fa-eye"></i> View Details</a>&nbsp;
							    @endif

							@elseif($data->RequestType == 'buy_for_me')
	                            <a class="btn btn-primary" title="Details"  href="buy-for-me-detail/{{$data->_id}}"><i class="fa fa-eye"></i> View Details</a>&nbsp;

							@endif

							@if(Input::get('list') == 'new')
									@if($data->RequestType == 'buy_for_me')
										@if($data->Status == 'pending')
		                                <a class="btn btn-primary" title="Edit" href="edit-buy-for-me/{{$data->_id}}"><i class="fa fa-pencil"></i> Edit</a>&nbsp;
		                                <!-- <a class="btn btn-primary" href="{{url('buyforme-payment',$data->_id)}}" title="Edit"></i>Pay Now</a>&nbsp; -->
		                                <a class="btn btn-primary" title="Details"  href="buy-for-me-detail/{{$data->_id}}"> Pay Now</a>&nbsp;
		                                @endif
		                                <a class="btn  btn-danger" title="Delete" onclick="remove_record('delete-request/{{$data->_id}}/DeliveryRequestOnline','{{$data->_id}}','Do you want to delete Item Purchase request?')" href="javascript:void(0)" >Delete</a>

		                            @elseif($data->RequestType == 'online')
		                            	@if($data->Status == 'pending')
		                            		<a class="btn btn-primary" title="Edit" href="edit-online-purchase/{{$data->_id}}"><i class="fa fa-pencil"></i> Edit</a>&nbsp;
		                            		<!-- <a class="btn btn-primary" href="{{url('online-payment',$data->_id)}}" title="Edit"></i>Pay Now</a>&nbsp; -->
		                            		<a class="btn btn-primary" title="Details" href="online-request-detail/{{$data->_id}}">Pay Now</a>&nbsp;
		                                @endif
		                            	<a class="btn  btn-danger" title="Delete" onclick="remove_record('delete-request/{{$data->_id}}/DeliveryRequestOnline','{{$data->_id}}','Do you want to delete item delivery request?')" href="javascript:void(0)" >Delete</a>

		                            @elseif($data->RequestType == 'local_delivery')
		                            	@if($data->Status == 'pending')
									     <a class="btn btn-primary" title="Edit" href="edit-local-delivery/{{$data->_id}}"><i class="fa fa-pencil"></i> Edit</a>&nbsp;
								        @endif
								         <a class="btn  btn-danger" title="Delete" onclick="remove_record('delete-request/{{$data->_id}}/DeliveryRequest','{{$data->_id}}','Do you want to delete delivery request?')" href="javascript:void(0)" >Delete</a>

								@elseif($data->RequestType == 'delivery')
									@if($data->Status == 'pending')
								      <a class="btn btn-primary" href="{{url('edit-request',$data->_id)}}" title="Edit"><i class="fa fa-pencil"></i> Edit</a>&nbsp;
								      <!-- <a class="btn btn-primary" href="{{url('process-card-list',$data->_id)}}?request_type=delivery" title="Edit"></i>Pay Now</a>&nbsp; -->
								      

								      	@if(isset($data->ProductList))
											<a class="btn btn-primary" title="Details" href="send-package-detail/{{$data->_id}}">Pay Now</a>&nbsp;
										@else
											<a class="btn btn-primary" title="Details" href="delivery-details/{{$data->_id}}">Pay Now</a>&nbsp;
									    @endif

								    @endif
								    <a class="btn  btn-danger" title="Delete" onclick="remove_record('delete-request/{{$data->_id}}/DeliveryRequest','{{$data->_id}}','Do you want to delete delivery request?')" href="javascript:void(0)" >Delete</a>

							    @endif

							@elseif(Input::get('list') == 'current')
						  		<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" onclick="view_on_map('{{json_encode($data->PickupLatLong)}}','{{json_encode($data->DeliveryLatLong)}}')" >View on Map</button>
						  		@if($data->RequestType == 'delivery')
									<a class="btn btn-primary" href="javascript:void(0)" id="chat-loader-{{$data->_id}}" onclick="open_chat_box('{{$data->TransporterId}}','{{$data->TransporterName}}','chat-loader-{{$data->_id}}')"><i class="fa fa-chat"></i> Chat</a>
								@endif

							@endif  <!-- End main if Input::get('list') -->
						</div>
					</div>
				</div>
<!-- End loop -->
		</div>
    </div>
    @endforeach
    @else
			<div class="col-sm-12 box-shadow">
				<br><div align="center" class=""><b>You have no requests yet. You can prepare a new delivery request by clicking on Create Request.</b></div><br>
			</div>

	@endif
	<script type="text/javascript" >
	    var $ = jQuery.noConflict();
		$(document).ready(function() {
			$(".fancybox").fancybox();
		});
	</script>
	@extends('User::list.pagination.footer2')
	<style>

	.image-css
	{
		background-color:#219ed4;
		border-radius:25px 25px 25px 25px;
	}


	</style>