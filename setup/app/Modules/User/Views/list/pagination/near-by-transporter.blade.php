@if(count($transporter) > 0)
		@foreach($transporter as $data)
			<div class="col-sm-4" >
				<div class="box-shadow-list">
					<div class="img-hldr clearfix">
						<div>
							@if($data['Image'] != '')
                               @if(file_exists(BASEURL_FILE.$data['Image']))
                                <a class="fancybox" rel="group" href="{{ImageUrl.$data['Image']}}" >
								 	<img src="{{ImageUrl.$data['Image']}}"  >
							    </a>
                                @endif
							   @else
								<img width="100%" height="" src="{{ImageUrl}}/no-image.jpg">

							@endif
						</div>
					</div>
					<div class="content-hldr">
						<div class="content-min-h">
						<p><b>{{ucfirst($data['FirstName'])}}</b></p>
					    <p><b>Location: </b>{{ucfirst($data['custom_address'])}}</p>
						<?php $result = $data['RatingCount'];
$width = ($result * 100) / 5;
?>
                        <div class="">
							<div class="">
								<div class="ratingAdjustmentcss ">
									<div class="graph" style="<?php echo $width; ?>">
										<img class="rate" src="{{ImageUrl}}/star_rating1.png">
									</div>
								</div>
							</div>
                        </div>
                        </div>
						<a class="btn btn-primary btn-block" href="{{url('trip-detail',$data['_id'])}}" >View Trip</a>
					</div>
				</div>
			</div>

		@endforeach
       @elseif($page == 0)
		<div class="col-sm-12 box-shadow">
			<br><div align="center" class=""><b>No Transporter near you. You can still post your item and we will help get it to you.</b></div><br>
		</div>
	@endif
      <script type="text/javascript">
$(document).ready(function() {

		$(".fancybox").fancybox();

	});
</script>
	@if(count($transporter) < $per_page)
		<script>$('#load_more').hide(); </script>
	@endif

