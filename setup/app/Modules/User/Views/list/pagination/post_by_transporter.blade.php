
	<div class="col-sm-12">
		@if(count($requests) > 0)
        @foreach($requests as $data)

		<div class="col-sm-12 box-shadow" id="row-{{$data->_id}}">

            <div class="list-group-custom">
			<div class="row">


			<div class="col-sm-12">
					<h3 class="color-blue"></h3>
					<p>
						<b>Source/Departure Address: </b>
						{{ucfirst($data->SourceCity)}}, {{ucfirst($data->SourceState)}}, {{ucfirst($data->SourceCountry)}}
					</p>

					<p>
						<b>Departure Date: </b>
						{{show_date($data->SourceDate)}}
					</p>

					<p>
						<b>Destination/Arrival Address:</b>
						{{ucfirst($data->DestiCity)}}, {{ucfirst($data->DestiState)}}, {{ucfirst($data->DestiCountry)}}
					</p>

					<p>
						<b>Arrival Date: </b>
						{{show_date($data->DestiDate)}}
					</p>


					<p>
						<b>Category: </b>


						<?php
if (!empty($data->SelectCategory)) {

    echo implode(",", $data->SelectCategory);
}
?>
					</p>
					<p><b>Weight Limit: </b>{{$data->Weight}} {{$data->Unit}}</p>



				</div>
			</div>

				<div class="" id="footer_div">
						<br />
						<div class=" text-left " >




						<button class="btn btn-default blue-btn" data-toggle="modal" data-target="#exampleModal{{$data->_id}}" title="Trip Detail"   id="#" > &nbsp;View Detail</button>&nbsp;&nbsp;

                    	</div>

            	</div>
            	<div class="col-sm-12 text-right " >&nbsp;</div>
            </div>

 <!--model-->
<div class="modal fade" id="exampleModal{{$data->_id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
	<div class="modal-content">
		  	<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="exampleModalLabel">Trip Detail</h4>
		  	</div>




				<div class="modal-body">
			<p><b>Departure Date/Time:</b>
					@if(isset($data->SourceDate->sec))
						{{show_date($data->SourceDate)}}
					@endif
                   </p>
					<p><b>Source/Departure Address: </b>{{ucfirst($data->SourceCity)}}, {{ucfirst($data->SourceState)}}, {{ucfirst($data->SourceCountry)}}</p>
					<p><b>Destination/Arrival Address: </b> {{ucfirst($data->DestiCity)}}, {{ucfirst($data->DestiState)}}, {{ucfirst($data->DestiCountry)}}</p>

					<p>
						<b>Arrival Date/Time: </b>
						{{show_date($data->DestiDate)}}
					</p>


					<p><b>Categories:</b>  	<?php
if (!empty($data->SelectCategory)) {

    echo ucfirst(implode(",", $data->SelectCategory));
}
?>	</p>
					<p><b>Weight Limit: </b>{{$data->Weight}}{{$data->Unit}}</p>
					<p><b>Shipping Mode: </b>{{ucfirst($data->TravelMode)}}</p>
					<p><b>Discription: </b>{{ucfirst($data->Description)}}</p>
					<hr>
					<div class="">
						<b class="color-blue">Transporter Information</b>
					</div><br/>


						<div class="col-sm-4">

		                         @if($data->TPImage != '')
		                           <a class="fancybox" rel="group" href="{{ImageUrl.$data->TPImage}}" >
								  <img id="senior-preview" src="{{ImageUrl.$data->TPImage}}"  width="100px" height="100px" />
							     </a>
		                         @else
		                           <img id="senior-preview" src="{{ ImageUrl}}/no-image.jpg"  width="100px" height="100px" />&nbsp;&nbsp;
		                         @endif
	                    </div>



	                   <div class="col-sm-8 pull-left">
	                   {{ucfirst($data->TransporterName)}}

		                    	</br></br>
		                    <div class="star-ratings-sprite pull-left">
				 				 <span style="width:<?php echo $data->TPRating ?>%" class="star-ratings-sprite-rating"></span>
							</div>
	                    </div>


	                    <div class="col-sm-4">

						</div>



					<div class="col-sm-12" style="text-align:center;">
					<br/>

					<a class="btn btn-default blue-btn" href="{{url('new-prepare-request')}}?id={{$data->_id}}"> &nbsp;Create Package</a>&nbsp;&nbsp;

					<hr></div>
				</div>


				<div class="clearfix"></div>
	</div>
  </div>
</div>
<!--end -->

        </div>
        @endforeach
		@else
		<div class="col-sm-12 box-shadow">
				<br><div align="center" class=""><b>No Record Found</b></div><br>
			</div>
		@endif
    </div>
<script type="text/javascript">
$(document).ready(function() {

		$(".fancybox").fancybox();

	});
</script>

@extends('User::list.pagination.footer2')

