<?php 
if(Input::get('pages'))
{
    $cur_page = Input::get('pages');
}

if(@$no_of_paginations!=0){
/* ---------------Calculating the starting and endign values for the loop----------------------------------- */
if ($cur_page >= 5) 
    {
        $start_loop = $cur_page - 2;
        if ($no_of_paginations > $cur_page + 2)
            $end_loop = $cur_page + 2;
        else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 4) 
        {
            $start_loop = $no_of_paginations - 4;
            $end_loop = $no_of_paginations;
        } 
        else 
        {
            $end_loop = $no_of_paginations;
        }
    } 
    else 
    {
        $start_loop = 1;
        if ($no_of_paginations > 5)
            $end_loop = 5;
        else
            $end_loop = $no_of_paginations;
    }
/* ----------------------------------------------------------------------------------------------------------- */
?>
<?php
if($count>0)
{

 ?>
<div class='pagination01 {{Input::get("unique")}}'>
	<form role="search" class="navbar-form navbar-left" style="width:100px;">
		<div class='displaypag form-group'> 
            <label>Display#</label>
			<select class='form-control btn-small changepagnum' >
   				<option value='10' <?php if($per_page == 10){ echo 'selected="selected"'; } ?> >10 </option>
				<option value='20' <?php if($per_page == 20){ echo 'selected="selected"'; } ?> >20 </option>
				<option value='50' <?php if($per_page == 50){ echo 'selected="selected"'; } ?> >50 </option>
				<option value='100' <?php if($per_page == 100){ echo 'selected="selected"'; } ?> >100 </option>
			</select>
		</div>
	</form>
<ul class="pagination navbar-center" > 
<label>&nbsp</label>
<div class="clearfix"></div>
<li p='1' <?php if ($first_btn && $cur_page > 1) { echo  'class="current-active"'; } else if ($first_btn) { echo  'class="inactive"'; } ?> ><a href='javascript:;'>First</a></li>


<?php if ($previous_btn && $cur_page > 1) {
    $pre = $cur_page - 1; ?>
    <li p='<?php echo $pre; ?>' class='current-active'><a href='javascript:;'>Previous</a></li>
<?php } else if ($previous_btn) { ?>
    <li class='inactive'><a href='javascript:;'>Previous</a></li>
<?php } 

for ($i = $start_loop; $i <= $end_loop; $i++) { ?>
        <li p='<?php echo $i; ?>' class='<?php echo ($cur_page == $i)? "active" : "current-active"; ?>' >
			<a href='javascript:;'><?php echo $i; ?></a>
		</li> <?php        
}

// TO ENABLE THE NEXT BUTTON
if ($next_btn && $cur_page < $no_of_paginations) {
    $nex = $cur_page + 1; ?>
    <li p='<?php echo $nex; ?>' class='current-active'><a href='javascript:;'>Next</a></li><?php
} else if ($next_btn) { ?>
    <li class='inactive'><a href='javascript:;'>Next</a></li><?php
}

// TO ENABLE THE END BUTTON
if ($last_btn && $cur_page < $no_of_paginations) { ?>
    <li p="<?php echo $no_of_paginations; ?>" class='current-active'><a href='javascript:;'>Last</a></li><?php
} else if ($last_btn) { ?>
    <li p="<?php echo $no_of_paginations; ?>" class='inactive'><a href='javascript:;'>Last</a></li><?php
} ?>
<input type='hidden' id='CurPage' value="<?php echo $cur_page; ?>" > 
</ul>
<form role="search" class="navbar-form navbar-right">
<label>&nbsp</label>
<div class="clearfix"></div>    
    <input type='text' class='goto-textbox form-control' placeholder=' Page No.' size='5' value="{{ Input::get('page') }}" style="width:80px;"/>
    <button type='button' class='btn btn-flat btn-secondary btn-primary goto-btn' >Go</button><?php 
if($no_of_paginations=='0'){
	$cur_page = 0; ?>
	<span class='total' a="<?php echo $no_of_paginations; ?>" >Page <b>1</b> of <b>1</b></span> <?php
}else{ ?>
	<span class='total' a="<?php echo $no_of_paginations; ?>">Page <b><?php echo $cur_page; ?></b> of <b><?php echo $no_of_paginations;?></b></span><?php
} ?>
</form>
<?php } ?>

</div>
<p class='searcherror text-center' style=""></p>

<?php 
}

?>
