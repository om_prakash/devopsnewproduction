
<div class="">
    <div class="row">
		@if(count($faq) > 0)
        @foreach($faq as $data)
		<div class="col-sm-12" id="row-{{$data->_id}}">
	        <div class="box-shadow">
	      		<br>
	          <div class="col-sm-12"  >

	                <span class="glyphicon glyphicon-question-sign special-ch" style="float:left;" aria-hidden="true" ></span>&nbsp;&nbsp;&nbsp;&nbsp;
	                  <b id="b{{$data->_id}}" style="margin-top:-5px;" onclick="return show_ans('<?php echo $data->_id; ?>');" style="margin-bottom:20px;">{{$data->Question}}
	                  </b>
	                  <div class="faq-ans" id="faq-ans{{$data->_id}}" style="margin-left:38px;">
	                      <span class="pull-left" style="margin-top:5px; margin-right:10px;"><i class="fa fa-caret-right" style="color:#219ed4; display:table-cell; margin-top:5px;"></i> &nbsp;
	                      </span><span style="display:table-cell;"><?php echo $data->Answer ?></span>
	                  </div>
	          </div>
	          <div class="col-sm-12">
	            <br>
	          </div>
	        </div>
    	</div>
        @endforeach
		@else
	    <div class="col-md-12">
				<div class="col-sm-12 box-shadow">
					<br><div align="center" class=""><b>No Record Found</b></div><br>
				</div>
	    </div>
		@endif
  	</div>
</div>

@if(count($faq) > 0)
  @foreach($faq as $data)
<!--model-->
<div class="modal fade" id="exampleModal{{$data->_id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="exampleModalLabel">FAQ</h4>
        </div>
        <div class="modal-body">
          <div class="col-sm-12"><span class="special-ch">Q:</span>&nbsp;&nbsp;&nbsp;&nbsp;
          <b>
              <div class="cursor">{{$data->Question}}</div></b>
          <p><?php echo trim($data->Answer); ?></p></div>

          <div class="clearfix"></div>
        </div>
    </div>
  </div>
</div>
  @endforeach
@endif
<!--end-->

@extends('User::list.pagination.footer2')
<script>

  function model_show(id)
  {
    $("#exampleModal"+id ).modal('show');
  }

  function show_ans(id)
  {
    $("#faq-ans"+id).slideToggle("slow");
  }


  $(function(){
    $(".faq-ans").hide();

    });

</script>
