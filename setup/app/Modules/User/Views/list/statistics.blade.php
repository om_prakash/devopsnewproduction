@extends('Page::layout.one-column-page')
@section('content')
<?php
   $customer_cancel_deliveries = $customer_completed_deliveries = 0;
   $tp_cancel_deliveries = $tp_completed_deliveries = 0;
   $customer_total_deleivery = 0;
   $tp_total_deleivery = 0;
   foreach($requester['result'] as $key => $val)
   {
      if($val['_id'] == 'cancel' || $val['_id'] == 'cancel_by_admin') {
         $customer_cancel_deliveries += $val['count'];
      } elseif($val['_id'] == 'completed' || $val['_id'] == 'delivered') {
         $customer_completed_deliveries += $val['count'];
      } 
      $customer_total_deleivery += $val['count'];
   }
   foreach($tp['result'] as $key => $val)
   {
      if($val['_id'] == 'cancel' || $val['_id'] == 'cancel_by_admin') {
         $tp_cancel_deliveries += $val['count'];
      } elseif($val['_id'] == 'completed' || $val['_id'] == 'delivered') {
         $tp_completed_deliveries += $val['count'];
      }
      $tp_total_deleivery += $val['count'];
   }
   
?>
<div class="container">
<div class="row">
   <div class="col-sm-12">
   </div>
   <div class="col-md-9 col-sm-12">
      <div class=" box-shadow width80">
         <div class="row rewards">
            <div class="gift_wrapper"></div>
            <div class="col-sm-12">
               <br/>
               <div class="form-group clearfix">
                  <div class="my-tab tabCustom clearfix">
                     <div class="sliding-div" id="ac_type_bg"></div>
                     <div class="tab1" data-toggle="tab" href="#home" id="requester" onclick ="transporter_type('requester')" style="color:#fff;"><span> Requester </span></div>
                     <div class="tab2" data-toggle="tab" href="#menu2" id="transporter" onclick="transporter_type('transporter')"><span> Transporter </span></div>
                  </div>
               </div>
               <br />
               <div class="tab-content">
                  <div id="home" class="tab-pane fade in active">
                     <div class="col-sm-12">
                        <div class="box-shadow">
                           <br/>
                           <p class="text-primary">{{ $customer_total_deleivery }}</p>
                           <p><b>Total Deliveries</b></p>
                        </div>
                     </div>
                     <div class="col-sm-12">
                        <div class="box-shadow">
                           <br/>
                           <p class="text-primary">{{$customer_completed_deliveries}}</p>
                           <p><b>Completed Deliveries</b></p><a href="{{url('pending-request')}}" class="pull-right">Details</a>                                         
                        </div>
                     </div>
                     <div class="col-sm-12">
                        <div class="box-shadow">
                           <br/>  
                           <p class="text-primary">{{$customer_cancel_deliveries}}</p>
                           <p><b>Not Delivered </b></p><a href="{{url('pending-request')}}?status=not_delivered" class="pull-right">Details</a>
                        </div>
                     </div>
                  </div>
                  <div id="menu2" class="tab-pane fade">
                     <div class="col-sm-12">
                        <div class="box-shadow">
                           <br/>
                           <p class="text-primary">{{$tp_total_deleivery}}</p>
                           <p><b>Total Deliveries </b></p>
                        </div>
                     </div>
                     <div class="col-sm-12">
                        <div class="box-shadow">
                           <br/>
                           <p class="text-primary">{{$tp_completed_deliveries}}</p>
                           <p><b>Completed Deliveries</b></p><a href="{{url('transporter-pending-request')}}" class="pull-right">Details</a>
                        </div>
                     </div>
                     <div class="col-sm-12">
                        <div class="box-shadow">
                           <br/>  
                           <p class="text-primary">{{$tp_cancel_deliveries}}</p>
                           <p><b>Not Delivered </b></p><a href="{{url('transporter-pending-request')}}?status=not_delivered" class="pull-right">Details</a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   @include('Page::layout.side_bar')
</div>
<script type="text/javascript">
   function transporter_type(type)
   { 
      if(type == 'transporter') {
   
         $('#ac_type_bg').css('right','0%');
         $('#transporter').css('color','#fff');
         $('#requester').css('color','#000');      
      } else {
         $('#ac_type_bg').css('right','50%');
         $('#requester').css('color','#fff');
         $('#transporter').css('color','#000');      
      }
   }
</script>
@endsection

