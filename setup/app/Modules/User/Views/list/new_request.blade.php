@extends('Page::layout.one-column-page')
@section('content')

<div class="container-fluid">
	<?php /*    echo "<pre>";  print_r($data['users']); die; */?>
    <div class="row">

<div class="map-hloder">
	<div class="request-counted">
    	<span>{{$total_delivery}}</span>
        <p>Request(s)</p>
    </div>
	<div id="map" style="min-height:500px;"> </div>
	<div class="filter-box col-md-3 col-sm-4 col-xs-12">
		<div class="clearfix filter-head">Search Nearby Requests </div>
		{!! Form::open(array('method'=>'get','id' => 'search','url'=>'transporter')) !!}
			<div class="clearfix"> </div>
			<div class="form-group">
				<label class="control-label">Title</label>
				{!! Form::text('search',Input::get('search'),array('class' => 'form-control' , 'id' => 'search','placeholder' => 'Title')) !!}
			</div>
			<div class="form-group">
				<label class="control-label">Source Address</label>
				{!! Form::text('search_address',Input::get('search_address'),array('class' => 'form-control' , 'id' => 'search_address','placeholder' => 'Country, City, State or Zipcode')) !!}
			</div>
			<div class="form-group">
				<label class="control-label">Destination Address</label>
				{!! Form::text('desti_address',Input::get('desti_address'),array('class' => 'form-control' , 'id' => 'desti_address','placeholder' => 'Country, City, State or Zipcode')) !!}
			</div>
			<div class="form-group">
				<div class="col-sm-6 col-xs-6">
					<button class="btn btn-primary btn-block">Search</button>
				</div>
				<div class="col-sm-6 col-xs-6">
					<a class="btn btn-primary btn-block" href="{{url('transporter')}}">Reset</a>
				</div>
			</div>

		{!! Form::close();  !!}
	</div>
</div>

    </div>
    <div class="col-sm-12">
	</br>
    </div>
</div>
<div class="container">
    <div class="row">

	<div class="col-sm-12">
			<div class="row">
				<div id="data_of_containerdata1"> </div>
				<div id="containerdata1"> </div>
			</div>

			<div style="width:100%; height:100px;" onmouseover="get_movedata()" id="load_more">

			</div>
		</div>
	</div>
</div>


<!-- Modal -->
<div id="tp_view_request_on_map" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">View On Map</h4>
      </div>
      <div class="modal-body">
        	<div id="new_reqeust_path_container" style="width:100%; height:350px;">Loading
        	</div>
      </div>
      <div class="modal-footer"></div>
    </div>

  </div>
</div>


<!-- Modal -->
<div id="modal3" class="modal fade in" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
	           <button type="button" class="close" data-dismiss="modal" id="edit_item_close">&times;</button>
	           <h4 class="modal-title">Get Detail</h4>
        	</div>
        	<div class="modal-body">
        		<div id = "apend_div2" style="color:black;"></div>
        		
        		
        	</div>
        	<div class="modal-footer text-right" >
		        <button type="button" class="btn btn-default " data-dismiss="modal" style="float: right;">Close</button>
		    </div>
		</div>
	</div>
</div>
<!-- End Modal -->
<button id="hide_b2" href="javascript:void(0)" data-toggle="modal" data-target="#modal3" style="display:none"></button>


@endsection
@section('script')
@parent
{!! Html::script('theme/web/js/paginate.js') !!}


@endsection

@section('inline-script')
@parent
<style>
.wrapper.clearfix {
  margin-top: 0px;
}
.map-popup-wrapper .map_profile {
  border: 4px solid #1fa0d9;
  border-radius: 100%;
  max-width: none;
}
.gm-style-iw > div > div {
  height: 150px;
  padding: 10px;
  width: 300px;
}
.map-popup-details {
  background: #fff none repeat scroll 0 0;
  float: left;
  padding: 5px;
  width: 60%;
}
.user-img {
  float: left;
  padding: 5px;
  width: 40%;
}
.map-detail-row.row{padding:10px;}
</style>
<script>
var marker = [], infowindow = [];
var image = '{{ImageUrl}}/request.png';
function myMap() {
  var myCenter = new google.maps.LatLng(parseFloat('{{$lat}}'),parseFloat('{{$lng}}'));
  var mapCanvas = document.getElementById("map");

  var map = new google.maps.Map(mapCanvas, {
  				center: myCenter,
  				zoom: 4,
  				scrollwheel: false
  			});


    @foreach($users as $key => $value)
      		@if(isset($value['PickupLatLong'][0]))

      			availibility = '<i class="fa fa-circle color-green"></i> &nbsp;Available';
				marker['{{$value->_id}}'] = new google.maps.Marker({
						position: {lat: parseFloat('{{$value["PickupLatLong"][1]}}'), lng: parseFloat('{{$value["PickupLatLong"][0]}}')},
						map: map,
						icon:image,
						title: '{{ ucwords($value->ProductTitle) }}'
			    });

				@if(!preg_match('/(?i)msie [5-9]/',$_SERVER['HTTP_USER_AGENT']))
				<?php $address = "";?>

					infowindow['{{$value->_id}}'] = new google.maps.InfoWindow({

				         content:  '<div id="content" class="map-popup-wrapper">'+
				                    '<div class="user-img">'+

				                        <?php $filefound = false;
if ($value['ProductImage'] != '') {
    if (file_exists(BASEURL_FILE . 'users/' . $value->ProductImage)) {$filefound = true;?>
				                            '<img class="map_profile" src="{{ImageUrl.$value["ProductImage"]}}" width="100%"  height="auto"/>'
				                          <?php }}
if (!$filefound) {?>
				                            '<img class="map_profile" src="{{ ImageUrl}}/no-image.jpg"  width="100%"  height="auto" />'
				                        <?php }?>+
				                    '</div>'+

			                          '<div class="map-popup-details">'+
			                        	'<div class="map-detail-row row">'+
			                          		'<div class="col-xs-12">Title: {{ucfirst($value["ProductTitle"])}}</div>'+
			                          	'</div>'+
			                                '<div class="map-detail-row row">'+
			                          			'<div class="col-xs-12">Pickup Date: '+
			                          				'{{ show_date(@$value["PickupDate"]) }}'+
										 		'</div>'+
										 	'</div>'+
			                                '<div class="map-detail-row row">'+
			                          			'<div class="col-xs-12">Pickup Address: {{$value["PickupFullAddress"]}}</div>'+
										 	'</div>'+

										 	'<div class="map-detail-row row">'+
			                          			'<div class="col-xs-12">Delivery Address: {{$value["DeliveryFullAddress"]}}</div>'+
										 	'</div>'+
										 	'<div class="map-detail-row row">'+
			                          			'<div class="col-xs-12">Delivery Date: '+
			                          				'{{ show_date(@$value["DeliveryDate"]) }}'+
										 		'</div>'+
										 	'</div>'+

										 	'<div class="map-detail-row row">'+
			                          			'<div class="col-xs-12">'+
			                          				'<a class="" onclick="return getTitle2({{$value}})" href="javascript:void(0)">Detail</a>'+
										 		'</div>'+
										 	'</div>'+

			                        	'</div>'+
			                        '</div>'+
				                    '</div>'+
				                   '</div>'
				         });

					marker['{{$value->_id}}'].addListener('click', function() {
			        for(var i in infowindow) {
			          infowindow[i].close();
			        }
			        infowindow['{{$value->_id}}'].open(map, marker['{{$value->_id}}']);
			      });

				@endif

      		@endif
    @endforeach
}

var rawpost = {};
@if(count(Input::get()) > 0)
   rawpost = eval('(' + '<?php echo json_encode(Input::get()); ?>' + ')' );
@endif

rawpost.container = 1;
rawpost.list = "new";

var pg = new paginate({
	   url : "pagination/transporter",
	   container : "containerdata1",
	   postvalue : rawpost
	});


function view_on_map(PickupLatLong,DeliveryLatLong)
{
	$('#tp_view_request_on_map').modal('show');
	PickupLatLong = eval('('+PickupLatLong+')');
	DeliveryLatLong = eval('('+DeliveryLatLong+')');

	setTimeout(function(){
		var view_path = new google.maps.Map(document.getElementById('new_reqeust_path_container'), {
							center: new google.maps.LatLng(PickupLatLong[1],PickupLatLong[0]),
							zoom: 2
						});

		new google.maps.Marker({
			position: {lat: parseFloat(PickupLatLong[1]), lng: parseFloat(PickupLatLong[0])},
			map: view_path,
			icon: '{{ImageUrl}}/pick.png',
			title: 'Pickup'
	    });

	    new google.maps.Marker({
			position: {lat: parseFloat(DeliveryLatLong[1]), lng: parseFloat(DeliveryLatLong[0])},
			map: view_path,
			icon: '{{ImageUrl}}/drop.png',
			title: 'Drop off'
	    });

	    view_path.setCenter(new google.maps.LatLng(
		  ((PickupLatLong[0] + DeliveryLatLong[1]) / 2.0),
		  ((PickupLatLong[0] + DeliveryLatLong[1]) / 2.0)
		));
		view_path.fitBounds(new google.maps.LatLngBounds(
		  //bottom left
		  new google.maps.LatLng(PickupLatLong[1], PickupLatLong[0]),
		  //top right
		  new google.maps.LatLng(DeliveryLatLong[1], DeliveryLatLong[0])
		));

	},1000	);

}

function getTitle2(obj){
	$('#apend_div2').html('');
	var obj2 = obj;
	
	var strin = '';
	var session_id = '<?php echo Session::get('UserId')?>';
	if(obj2.hasOwnProperty('ProductList')){
		$.each( obj2.ProductList, function( key, value ) {
			if(value.tpid == '' || value.tpid == null){
				strin += '<h4><a href="transporter/details/'+value._id+'" style="color:black;">'+value.product_name+'</a></h4>';
			}else if(value.tpid == session_id){
				strin += '<h4><a href="transporter/details/'+value._id+'" style="color:black;">'+value.product_name+'</a></h4>';
			}


		});


		$('#apend_div2').html(strin);
		$('#hide_b2').trigger('click');
	}else{
		document.location.href = SITEURL +'transporter/details/'+obj2._id;
	}
}


$(document).ready(function() {
		$(".fancybox").fancybox();
	});

  </script>
  <script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyCT0uj-bffWWdOdwDp15LvrMko-OuoZUoY&callback=myMap"></script>

<script>
var geocoder = new google.maps.Geocoder();
$('#search').submit(function(){

		if(!$('#search_address').val() == '')
		{
			try
			{

			geocoder.geocode( { 'address': $('#search_address').val()}, function(info, status) {

				var lng = '', lat = '';
				if (status == google.maps.GeocoderStatus.OK)
				{
					var lat = info[0].geometry.location.lat();
					var lng = info[0].geometry.location.lng();
				}
				document.location.href = '{{url("transporter")}}?search='+$('#search').val()+'&search_address='+$('#search_address').val()+'&lat='+lat+'&lng='+lng+'&desti_address='+$('#desti_address').val();
				return true;

			});
		}catch(e){
			console.log(e);
		}
			return false;
		}

});

var nr_pageno = 1;
function get_movedata(){
	$('#data_of_containerdata1').append($('#containerdata1').html());
	pg.defaults.page = nr_pageno;
	nr_pageno = nr_pageno + 1;
	load_data(pg.defaults);
}
</script>
@endsection
