@extends('Page::layout.one-column-page')
@section('page_title')
Nearby Transporter - Aquantuo
@endsection
@section('content')


<div class="container-fluid">
	<div class="row">

			<div class="map-hloder">
			<div id="map" style="min-height:500px;"> </div>
				<div class="filter-box col-md-3 col-sm-4 col-xs-12">
					<div class="clearfix filter-head">

						Search Nearby Transporter
					</div>
					 {!! Form::open(array('method'=>'get','id'=>'search_form','url'=>'near-by-transporter')) !!}
					<div class="clearfix">
						<div class="form-group">
							<label class="control-label">Transporter Name</label>
					{!! Form::text('search',Input::get('search'),array('class' => 'form-control' , 'id' => 'search','placeholder' => 'Transporter Name')) !!}
						</div>
						<div class="form-group">
							<label class="control-label">City, State or Zipcode</label>
					{!! Form::textarea("search_address", Input::get('search_address'), ['class' => 'form-control', 'rows' => 3, 'placeholder' => 'City, State or Zip Code/Postcode','id'=>'search_address']) !!}
						</div>
						<div class="form-group">
							<div class="col-md-6 col-xs-6">
								<button class="btn btn-primary btn-block" id="search_submit">Search</button>
							</div>
							<div class="col-md-6 col-xs-6">
								<a class="btn btn-primary btn-block" href="{{url('near-by-transporter')}}">Reset</a>
							</div>
						</div>
					</div>
					{!! Form::close();  !!}
				</div>
			</div>
	</div>
    </div>
    <div class="container">
	<div class="row">
        <div class="col-sm-12">
			</br>
        </div>
		<div class="col-sm-12">
			<div class="row">
				<div id="data_of_containerdata1"> </div>
				<div id="containerdata1"> </div>
			</div>
				<div style="width:100%; height:100px;" onmouseover="get_movedata()" id="load_more">
		</div>
	</div>
</div>

@endsection
@section('script')
@parent
{!! Html::script('theme/web/js/paginate.js') !!}


@endsection

@section('inline-script')
@parent
@extends('User::list.pagination.footer2')
<?php/* $a=51.508742 ; $b=-0.120850; */ ?>
<?php /* echo $a;?> , <?/* php  echo $b;   */?>
<style>
.wrapper.clearfix {
  margin-top: 0px;
}
.map-popup-wrapper .map_profile {
  border: 4px solid #1fa0d9;
  border-radius: 100%;
  max-width: none;
}
.gm-style-iw > div > div {
  height: 150px;
  padding: 10px;
  width: 300px;
}
.map-popup-details {
  background: #fff none repeat scroll 0 0;
  float: left;
  padding: 5px;
  width: 60%;
}
.user-img {
  float: left;
  padding: 5px;
  width: 40%;
}
</style>
<script>
var marker = [], infowindow = [];
var image = '{{ImageUrl}}tranporter_pin.png';
function myMap() {
  var myCenter = new google.maps.LatLng(parseFloat('{{$lat}}'),parseFloat('{{$lng}}'));
  var mapCanvas = document.getElementById("map");
  var mapOptions = {center: myCenter, zoom: 7,scrollwheel: false};
  var map = new google.maps.Map(mapCanvas, mapOptions);


  @foreach ($users as $key => $value)

      		 @if(isset($value['CurrentLocation'][0]))
      			availibility = '<i class="fa fa-circle color-green"></i> &nbsp;Available';
				marker['{{$value["_id"]}}'] = new google.maps.Marker({
					position: {lat: parseFloat("{{$value['CurrentLocation'][1]}}"), lng: parseFloat("{{$value['CurrentLocation'][0]}}")},
					map: map,
					icon:image,
					title: '{{ ucwords($value["FirstName"]) }}'
			    });

				@if(!preg_match('/(?i)msie [5-9]/',$_SERVER['HTTP_USER_AGENT']))
				<?php $address = "{$value['custom_address']}";?>

					infowindow['{{$value["_id"]}}'] = new google.maps.InfoWindow({

				         content:  '<div id="content" class="map-popup-wrapper">'+
				                    '<div class="user-img">'+
				                        <?php $filefound = false;
if ($value['Image'] != '') {
    if (file_exists(BASEURL_FILE . 'user/' . $value["Image"])) {$filefound = true;?>
				                            '<img class="map_profile" src="{{ImageUrl}}user/{{$value["Image"]}}" width="100%"  height="auto" />'
				                          <?php }
}
if (!$filefound) {?>
				                            '<img class="map_profile" src="{{ ImageUrl}}/no-image.jpg"  width="100%"  height="auto" />'
				                        <?php }?>+
				                    '</div>'+

				                          '<div class="map-popup-details">'+
				                        '<div class="map-detail-row row">'+

				                          '<div class="col-xs-12 details-list"><b>Name:</b>&nbsp;&nbsp;'+
				                            '<?php if ($value["FirstName"] != "") {?>'+
				                                '<a href="#">{{ucfirst($value["FirstName"])}}</a>'+
				                              '<?php } else {

}?></div></div>'+
				                             '<div class="map-detail-row row">'+
				                          '<div class="col-xs-12 details-list"><b>Address:</b>&nbsp;&nbsp; '+
				                          '<?php if ($value["custom_address"] != "") {?>'+
				                          '<?php echo ucfirst($value['custom_address']);} else {

} ?></div></div>'+''+
											 '<div class="map-detail-row row">'+
				                             '<div class="col-xs-12 details-list"> <?php
$average_rating = 0;
if ($value["RatingCount"] > 0 && $value["RatingByCount"] > 0) {

    $rating = $value["RatingCount"] / $value["RatingByCount"];
    $average_rating = 20 * $rating;
}?>'+
				                                  '<div class="star-ratings-sprite pull-left">'+
											'<span style="width:<?php echo $average_rating; ?>%" class="star-ratings-sprite-rating"></span>'+
													'</div>'+'</div></div>'+



				                        '</div>'+
				                        '</div>'+
				                    '</div>'+
				                   '</div>'
				         });

					marker['{{$value["_id"]}}'].addListener('click', function() {
			        for(var i in infowindow) {
			          infowindow[i].close();
			        }
			        infowindow['{{$value["_id"]}}'].open(map, marker['{{$value["_id"]}}']);
			      });

				@endif
      		@endif
      	@endforeach
}

var rawpost = {};
   @if(count(Input::get()) > 0)
       rawpost = eval('(' + '<?php echo json_encode(Input::get()); ?>' + ')' );
   @endif

   rawpost.container = 1;
   rawpost.list = "new";

 var pg =  new paginate({
       url : "pagination/near-by-transporter",
       container : "containerdata1",
       postvalue : rawpost
   });




   var nr_pageno = 1;
	function get_movedata(){
	$('#data_of_containerdata1').append($('#containerdata1').html());
	pg.defaults.page = nr_pageno;
	nr_pageno = nr_pageno + 1;
	load_data(pg.defaults);
}
</script>
  <script src="https://maps.googleapis.com/maps/api/js?callback=myMap&key=AIzaSyCT0uj-bffWWdOdwDp15LvrMko-OuoZUoY"></script>


<script>

	var geocoder = new google.maps.Geocoder();
$('#search_form').submit(function(){

	if(!$('#search_address').val() == '')
	{
		try
		{
			$('#search_submit').addClass('spinning');
			geocoder.geocode( { 'address': $('#search_address').val()}, function(info, status) {

			if (status == google.maps.GeocoderStatus.OK)
			{
				lat = info[0].geometry.location.lat();
				lng = info[0].geometry.location.lng();

				document.location.href = '{{url("near-by-transporter")}}?search='+$('#search').val()+'&search_address='+$('#search_address').val()+'&lat='+lat+'&lng='+lng;

				return true;

			} else {
				document.location.href = '{{url("near-by-transporter")}}?search='+$('#search').val()+'&search_address='+$('#search_address').val();
			}
		});
		}catch(e){
		console.log(e);
		}
		return false;
	}


});
</script>

<style>

</style>

@endsection
