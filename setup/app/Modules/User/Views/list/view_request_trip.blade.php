@extends('Page::layout.one-column-page')
@section('content')
<div class="container">
	<div class="row">
    	<div class="col-sm-12">
		<h2 class="color-blue mainHeading">Request List</h2>
			<br />
		</div>
	
		<!-- Nav tabs -->

		
			<div class="col-md-12 col-sm-12 col-xs-12 backgroun-grey margin-top">
				<div class="row">
					<div class="col-md-3 col-sm-4 text-left">
							    {!! Form::open(array('url'=>'notification','method'=>'get')) !!}
							<input type="hidden" value="{{Input::get('current_tab')}}" name="current_tab" id="current_tab">
								<div class="form-group">
										{!! Form::label('Search','Search', ['class'=>'control-lable']) !!}      
										{!! Form::text('search',Input::get('search'),array('class' => 'form-control' , 'id' => 'search','placeholder' => 'Search Title')) !!}
			   					</div>
					</div>

						<div class="col-md-3 col-sm-4 text-left">
							<div class="form-group">
								<label>Start Date</label>
									<div class="input-group">
										<input type="text"  placeholder="Search Date" id="startdate12" value="<?php echo Input::get('search_date')?>"  class="form-control" name ="search_date" readonly="readonly">
											<div class="input-group-addon"><i class="fa fa-calendar"></i>
											</div>
									</div>
							</div>
		
						</div>

						<div class="col-md-3 col-sm-4 text-left">
							<div class="form-group">
								<label>End Date</label>
									<div class="input-group">
										<input type="text"  placeholder="Search Date" id="enddate" value="<?php echo Input::get('search_date2')?>"  class="form-control" name ="search_date2" readonly="readonly">
											<div class="input-group-addon"><i class="fa fa-calendar"></i>
											</div>
									</div>
							</div>
		
						</div>

						<div class="col-md-3 col-sm-4 inline-form-btn">
							<div class="form-group">
								<input type="hidden" value="" id="ajaxPath">
								<button type="submit" value="Search" class="btn btn-primary">Search</button>
								<a class="btn btn-primary" href="{{url('/notification')}}" >
									<span class="glyphicon glyphicon-refresh" aria-hidden="true"></span>&nbsp;&nbsp;Reset
								</a>                                                                
							</div>
                        {!! Form::close();  !!} 
						</div>     
				</div>
			</div>
	    </div>
					<div class="row">
						<div class="col-md-12">
							<div id="containerdata"></div>
						</div>
					</div>
		<input type="hidden" value="pagination/notification-list" >

								
</div> 
						



 
@endsection

@section('script')
@parent
{!! Html::style('theme/date-time-picker/jquery.datetimepicker.css') !!}       
{!! Html::script('theme/date-time-picker/jquery.datetimepicker.js') !!}
{!! Html::script('theme/front/js/jquery.rateit.min.js')!!} 
{!! Html::script('theme/web/js/paginate.js') !!}

@endsection

@section('inline-script')
@parent
<script>
 var rawpost = {};
   @if(count(Input::get()) > 0)
       rawpost = eval('(' + '<?php echo json_encode(Input::get()); ?>' + ')' );
      
   @endif
	 new paginate({
		 
       url : "pagination/view_trip_request/{{Request::segment(3)}}",
       container : "containerdata",
       postvalue : rawpost,
       pageno: 1,
       limit: 10,
       
   });
   
   
    $('#startdate12').datetimepicker({            
       format:'M d, Y',
       timepicker:false,
       scrollInput: false,
       onChangeDateTime:function( ct ){
           $('#enddate').datetimepicker({  minDate:ct  })
       },
       
   });

    $('#enddate').datetimepicker({            
       format:'M d, Y',
       timepicker:false,
       scrollInput: false,
       onChangeDateTime:function( ct ){
           $('#startdate12').datetimepicker({  maxDate:ct  })
       },
       
   });
   
  
  
  </script>
@endsection
