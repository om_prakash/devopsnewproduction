@extends('Page::layout.one-column-page')
@section('content')
<div class="container">
	<div class="row">
    	<div class="col-sm-12">
		<h2 class="color-blue mainHeading">FAQ LIST</h2>
			<br />
		</div>

		<!-- Nav tabs -->
		<div class="col-md-12">
			<div class=" col-sm-12 col-xs-12 backgroun-grey margin-top">
				<div class="row">
					<div class="col-md-3 col-sm-4 text-left">
							    {!! Form::open(array('url'=>'faq-list','method'=>'Get')) !!}

								<div class="form-group">
								{!! Form::label('Search','Search Question', ['class'=>'control-lable']) !!}
								{!! Form::text('Search',Input::get('Search'),['class'=>'form-control', 'placeholder'=> 'Search by question','id' =>'inputError1']) !!}

			   					</div>
					</div>

						<div class="col-md-3 col-sm-4 inline-form-btn">
							<div class="form-group">
								<input type="hidden" value="" id="ajaxPath">
								<input type="submit" value="Search" class="btn btn-primary">
								<a class="btn btn-primary" href="{{url('faq-list')}}" >
									<span class="glyphicon glyphicon-refresh" aria-hidden="true"></span>&nbsp;&nbsp;Reset
								</a>
							</div>
                        {!! Form::close();  !!}
						</div>
				</div>
			</div>
		</div>
	    </div>
					<div class="row">
						<div class="col-md-12">
							<div id="containerdata"></div>
						</div>
					</div>
		<input type="hidden" value="pagination/faq-list" >


</div>





@endsection

@section('script')
@parent
{!! Html::style('theme/date-time-picker/jquery.datetimepicker.css') !!}
{!! Html::script('theme/date-time-picker/jquery.datetimepicker.js') !!}
{!! Html::script('theme/front/js/jquery.rateit.min.js')!!}
{!! Html::script('theme/web/js/paginate.js') !!}

@endsection

@section('inline-script')
@parent
<script>
 var rawpost = {};
   @if(count(Input::get()) > 0)
       rawpost = eval('(' + '<?php echo json_encode(Input::get()); ?>' + ')' );

   @endif
	 new paginate({

       url : "pagination/faq-list",
       container : "containerdata",
       postvalue : rawpost,
       pageno: 1,
       limit: 10,

   });


    $('#startdate12').datetimepicker({
       format:'M d, Y',
       timepicker:false,
       scrollInput: false,
       onChangeDateTime:function( ct ){
           $('#enddate').datetimepicker({  minDate:ct  })
       },

   });

    $('#enddate').datetimepicker({
       format:'M d, Y',
       timepicker:false,
       scrollInput: false,
       onChangeDateTime:function( ct ){
           $('#startdate12').datetimepicker({  maxDate:ct  })
       },

   });



  </script>
@endsection
