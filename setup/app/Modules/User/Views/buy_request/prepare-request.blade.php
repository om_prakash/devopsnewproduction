@extends('page::layout.one-column-page')
@section('content')
<div class="container">
   <div class="row">
   	  <div class="col-sm-12">
      <h2 class="color-blue">Prepare Request</h2>
      <br>
      </div>
         <div class="col-sm-12">
         	<div class="box-shadow">
               <h3>Please fill some required feild</h3>
               <hr>
               <p class="color-black">Lorem ipsum dolor sitt amet loremi dsftg dchuj.</p>
               <!------------------step1------------------>
              <!-- <div id="sec1" >  -->
               <div class="row">
                  <div class="col-sm-12">
                     <div class="step_box four_step clearfix">
                        <div class="step first selected">
                           <div>1</div>
                           <p class="text-center colol-black">Pickup Address</p>
                        </div>
                        <div class="step inner">
                           <div>2</div>
                           <p class="text-center colol-black">Drop off Address</p>
                        </div>
                        <div class="step inner">
                           <div>3</div>
                           <p class="text-center colol-black">Package Details</p>
                        </div>
                        <div class="step last ">
                           <div>4</div>
                           <p class="text-center colol-black">Payment</p>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-10 col-sm-offset-1" id="sec1" >
                     <form class="form-vertical">
						 <div>
                        <div class="col-sm-6">
                           <div class="form-group">
                              <label class="control-label">Package Title (1)</label>
                              <input class="form-control" placeholder="Package Title">
                           </div>
                        </div>

                        <div class="col-md-12">
                   		<h3 class="color-blue">Pickup Address</h3>

                        </div>
                        <div class="col-sm-12">
                            <div class="checkbox">
                                <label>
                                <input type="checkbox"> This is public place
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="form-group">
                              <label class="control-label">Address Line1</label>
                              <input class="form-control" placeholder="Address">
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="form-group">
                              <label class="control-label">Address Line2</label>
                              <input class="form-control" placeholder="Address">
                           </div>
                        </div>

                        <div class="col-sm-4">
                           <div class="form-group">
                              <label class="control-label">Country</label>
                              <select class="form-control">
                              	  <option>country1</option>
                                  <option>country2</option>
                                  <option>country3</option>
                                  <option>country4</option>
                              </select>
                           </div>
                        </div>
                        <div class="col-sm-4">
                           <div class="form-group">
                              <label class="control-label">State</label>
                              <select class="form-control">
                              	  <option>State1</option>
                                  <option>State2</option>
                                  <option>State3</option>
                                  <option>State4</option>
                              </select>
                           </div>
                        </div>
                        <div class="col-sm-4">
                           <div class="form-group">
                              <label class="control-label">City</label>
                              <select class="form-control">
                              	  <option>City1</option>
                                  <option>City2</option>
                                  <option>City3</option>
                                  <option>City4</option>
                              </select>
                           </div>
                        </div>

                        <div class="col-sm-4">
                           <div class="form-group">
                              <label class="control-label">Zipcode</label>
                              <input class="form-control" placeholder="Zipcode">
                           </div>
                        </div>

                        <div class="col-sm-4">
                           <div class="form-group">
                              <label class="control-label">Pickup Date</label>
                              <input class="form-control" placeholder="Date">
                           </div>
                        </div>

                        <div class="col-sm-4">
                           <div class="form-group">
                              <label class="control-label">Pickup Time</label>
                              <input class="form-control" placeholder="Time">
                           </div>
                        </div>


                        <div class="col-md-12">
                           <hr>
                        </div>
                        <div class="col-sm-8 col-sm-offset-2">
                           <div class="form-group">
                              <button class="custom-btn1 btn-block" onclick="one()">
                                 Next
                                 <div class="custom-btn-h"></div>
                              </button>
                           </div>
                        </div>
                     </div>
                  </div>
                  </div>
                          <!------------------step2------------------>

               <div class="row" id="sec2" style="display:none;">
                  <div class="col-sm-12">
                     <div class="step_box four_step clearfix">
                        <div class="step first selected">
                           <div>1</div>
                           <p class="text-center colol-black">Pickup Address</p>
                        </div>
                        <div class="step inner selected">
                           <div>2</div>
                           <p class="text-center colol-black">Drop off Address</p>
                        </div>
                        <div class="step inner">
                           <div>3</div>
                           <p class="text-center colol-black">Package Details</p>
                        </div>
                        <div class="step last ">
                           <div>4</div>
                           <p class="text-center colol-black">Payment</p>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-10 col-sm-offset-1">
                   <!--  <form class="form-vertical">   -->


                        <div class="col-md-12">
                   		<h3 class="color-blue">Drop off Address</h3>

                        </div>
                        <div class="col-sm-12">
                            <div class="checkbox">
                                <label>
                                <input type="checkbox"> This is public place
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="form-group">
                              <label class="control-label">Address Line1</label>
                              <input class="form-control" placeholder="Address">
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="form-group">
                              <label class="control-label">Address Line2</label>
                              <input class="form-control" placeholderone()="Address">
                           </div>
                        </div>

                        <div class="col-sm-4">
                           <div class="form-group">
                              <label class="control-label">Country</label>
                              <select class="form-control">
                              	  <option>country1</option>
                                  <option>country2</option>
                                  <option>country3</option>
                                  <option>country4</option>
                              </select>
                           </div>
                        </div>
                        <div class="col-sm-4">
                           <div class="form-group">
                              <label class="control-label">State</label>
                              <select class="form-control">
                              	  <option>State1</option>
                                  <option>State2</option>
                                  <option>State3</option>
                                  <option>State4</option>
                              </select>
                           </div>
                        </div>
                        <div class="col-sm-4">
                           <div class="form-group">
                              <label class="control-label">City</label>
                              <select class="form-control">
                              	  <option>City1</option>
                                  <option>City2</option>
                                  <option>City3</option>
                                  <option>City4</option>
                              </select>
                           </div>
                        </div>

                        <div class="col-sm-4">
                           <div class="form-group">
                              <label class="control-label">Zipcode</label>
                              <input class="form-control" placeholder="Zipcode">
                           </div>
                        </div>

                        <div class="col-sm-4">
                           <div class="form-group">
                              <label class="control-label">Drop off Date</label>
                              <input class="form-control" placeholder="Date">
                           </div>
                        </div>

                        <div class="col-sm-4">
                           <div class="form-group">
                              <label class="control-label">Drop off Time</label>
                              <input class="form-control" placeholder="Time">
                           </div>
                        </div>

                        <div class="col-sm-12">
                        <div class="form-group">
                        <label class="control-label">Is Delivery date flexible?</label>
                            <div class="checkbox">

                                <label>
                                <input type="checkbox"> Yes
                                </label>
                                &nbsp; &nbsp;
                                <label>
                                <input type="checkbox"> No
                                </label>
                            </div>
                        </div>
                        </div>

                        <div class="col-sm-12">
                        <div class="form-group">
                        <label class="control-label">Journy Type</label>
                            <div class="checkbox">

                                <label>
                                <input type="checkbox"> One Way
                                </label>
                                &nbsp; &nbsp;
                                <label>
                                <input type="checkbox"> Return
                                </label>
                            </div>
                        </div>
                        </div>

                        <div class="col-sm-12">
                        <div class="form-group">
                        <label class="control-label">Return Address (If item is not delivered)</label>
                            <div class="checkbox">

                                <label>
                                <input type="checkbox"> Same as pickup adddress
                                </label>

                            </div>
                        </div>
                        </div>

                        <div class="col-md-12">
                           <hr>
                        </div>
                        <div class="col-sm-8 col-sm-offset-2">
                           <div class="form-group">
                              <button class="custom-btn1 btn-block" onclick="two();">
                                 Next
                                 <div class="custom-btn-h"></div>
                              </button>
                           </div>
                        </div>
                   <!--  </form>   -->
                  </div>
               </div>

               <!------------------step2------------------>

               <!------------------step3------------------>

               <div class="row" id="sec3" style="display:none;">
                  <div class="col-sm-12">
                     <div class="step_box four_step clearfix">
                        <div class="step first selected">
                           <div>1</div>
                           <p class="text-center colol-black">Pickup Address (3)</p>
                        </div>
                        <div class="step inner selected">
                           <div>2</div>
                           <p class="text-center colol-black">Drop off Address</p>
                        </div>
                        <div class="step inner selected">
                           <div>3</div>
                           <p class="text-center colol-black">Package Details</p>
                        </div>
                        <div class="step last ">
                           <div>4</div>
                           <p class="text-center colol-black">Payment</p>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-10 col-sm-offset-1">
                   <!--  <form class="form-vertical">    -->
                     <div class="col-sm-12">
                           <div class="form-group">
                        <label class="control-label">Measurement Unit</label>
                        <div class="clearfix">
                        <label class="radio-inline">
                        	<input type="radio" name="optradio">Metric (Cm/Kg)
                        </label>
                        <label class="radio-inline">
                        	<input type="radio" name="optradio">Imperial (Inches/lbs)
                        </label>
                        </div>
                        </div>
                        </div>

                        <div class="col-sm-3">
                           <div class="form-group">
                              <label class="control-label">Lenght</label>
                              <input class="form-control" placeholder="Lenght">
                           </div>
                        </div>
                        <div class="col-sm-3">
                           <div class="form-group">
                              <label class="control-label">Width</label>
                              <input class="form-control" placeholder="Width">
                           </div>
                        </div>
                        <div class="col-sm-3">
                           <div class="form-group">
                              <label class="control-label">Height</label>
                              <input class="form-control" placeholder="Height">
                           </div>
                        </div>
                        <div class="col-sm-3">
                           <div class="form-group">
                              <label class="control-label">Weight</label>
                              <input class="form-control" placeholder="Weight">
                           </div>
                        </div>
                       <div class="col-sm-12">
                           <div class="form-group">
                        <label class="control-label">Travel Mode</label>
                        <div class="clearfix">
                        <label class="radio-inline">
                        	<input type="radio" name="optradio">By Air
                        </label>
                        <label class="radio-inline">
                        	<input type="radio" name="optradio">By Sea
                        </label>
                        </div>
                        </div>
                        </div>

                        <div class="col-sm-12">
                           <div class="form-group">
                              <label class="control-label">Select Package Category</label>
                              <select class="form-control">
                              	  <option>category1</option>
                                  <option>category2</option>
                                  <option>category3</option>
                                  <option>category4</option>
                              </select>
                           </div>
                        </div>

                        <div class="col-sm-12">
                           <div class="form-group">
                              <label class="control-label">Description/Instructions</label>
                              <textarea rows="3" class="form-control"></textarea>
                           </div>
                        </div>
                        <div class="col-sm-12">
                           <div class="form-group">
                              <label class="control-label">Describe your package</label>
                              <textarea rows="3" class="form-control"></textarea>
                           </div>
                        </div>
                        <div class="col-sm-12">
                           <div class="form-group">
                              <label class="control-label">Package care note</label>
                              <textarea rows="3" class="form-control"></textarea>
                           </div>
                        </div>
                        <div class="col-sm-12">
                        	<div class="form-group">
                              <label class="control-label">Upload default package image</label>
                              <input type="file" placeholder="Browse">
                           </div>
                        </div>
                        <div class="col-sm-12">
                        	<div class="form-group">
                              <label class="control-label">Upload extra package image</label>
                              <input type="file" placeholder="Browse">
                              <input type="file" placeholder="Browse">
                              <input type="file" placeholder="Browse">
                              <input type="file" placeholder="Browse">
                           </div>
                        </div>
                        <div class="col-sm-12">
                        <div class="form-group">

                              <label class="control-label">Receiver phone number</label>
                                <div class="row">
                                <div class="col-xs-2">
                                <input type="text" class="form-control" placeholder="Country Code">
                                </div>
                                <div class="col-xs-4">
                                <input type="text" class="form-control" placeholder="Phone Number">
                                </div>
                        </div>

                        </div>
                        </div>
                        <div class="col-sm-6">
                        	<div class="form-group">
                              <label class="control-label">Package Value</label>
                              <input class="form-control" placeholder="$">
                           </div>
                        </div>
                        <div class="col-sm-12">
                        <div class="form-group">
                        <label class="control-label">Insurance</label>
                            <div class="checkbox">

                                <label>
                                <input type="checkbox"> Yes
                                </label>
                                &nbsp; &nbsp;
                                <label>
                                <input type="checkbox"> No
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="checkbox">

                                <label>
                                <input type="checkbox"> Need package material
                                </label>

                            </div>
                        </div>
                        </div>



                        <div class="col-md-12">
                           <hr>
                        </div>
                        <div class="col-sm-8 col-sm-offset-2">
                           <div class="form-group">
                              <button class="custom-btn1 btn-block" onclick="three();">
                                 Next
                                 <div class="custom-btn-h"></div>
                              </button>
                           </div>
                        </div>
                  <!--   </form>   -->
                  </div>
               </div>
            <!--   </div>   -->
               <!------------------step3------------------>

               <!------------------step4------------------>

               <div class="row" id="sec4" style="display:none;">
                  <div class="col-sm-12">
                     <div class="step_box four_step clearfix">
                        <div class="step first selected">
                           <div>1</div>
                           <p class="text-center colol-black">Pickup Address (4)</p>
                        </div>
                        <div class="step inner selected">
                           <div>2</div>
                           <p class="text-center colol-black">Drop off Address</p>
                        </div>
                        <div class="step inner selected">
                           <div>3</div>
                           <p class="text-center colol-black">Package Details</p>
                        </div>
                        <div class="step last selected">
                           <div>4</div>
                           <p class="text-center colol-black">Payment</p>
                        </div>
                     </div>
                  </div>

                  <div class="col-md-10 col-sm-offset-1">
                     <div class="col-sm-12">
                        <div class="row custom-row">
                           <div class="col-sm-6"><b>Total Weight -</b></div>
                           <div class="col-sm-6">50lbs</div>
                        </div>
                        <div class="row custom-row">
                           <div class="col-sm-6"><b>Item Price -</b></div>
                           <div class="col-sm-6">$50.00</div>

                        </div>
                        <div class="row custom-row">
                           <div class="col-sm-6"><b>Retailer Shipping -</b></div>
                           <div class="col-sm-6">50lbs</div>
                        </div>
                        <div class="row custom-row">
                           <div class="col-sm-6"><b>Aquantuo Shipping -</b></div>
                           <div class="col-sm-6">50lbs</div>
                        </div>
                        <div class="row custom-row">
                           <div class="col-sm-6"><b>Delivery within Ghana -</b></div>
                           <div class="col-sm-6">50lbs</div>
                        </div>
                        <div class="row">
                           <hr>
                        </div>
                        <div class="col-sm-8 col-sm-offset-2">
                           <div class="form-group">
                              <button class="custom-btn1 btn-block">
                                 Next
                                 <div class="custom-btn-h"></div>
                              </button>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="gray-bg">
                     <div class="row">
                        <div class="col-sm-6">
                           <h4><b>Total Amount- $250.00</b></h4>
                           <small>(In Ghananian cedi GHS 315,8656.00)</small>
                        </div>
                        <div class="col-sm-6">
                      <!--     <form class="form-inline">     -->
                              <div class="form-group reward-group">
                                 <input type="text" placeholder="Reward Code" class="form-control" >
                                 <button class="btn default-btn" onclick="four();">
                                 <img src="theme/web/promo/images/green_check.png">
                                 Apply
                                 </button>
                                 <span>$21.00</span>
                              </div>
                         <!--  </form>    -->
                        </div>
                     </div>
                     <small class="color-red">You wil be billed or refunded any differences in price or shipping at time of purchase</small>
                  </div>
               </div>

               <!------------------step4------------------>
                     </form>
                  </div>
         </div>
            <!--   </div>  -->
               <!------------------step1------------------>
     </div>
</div>
<script>
function one()
{
	$("#sec1").hide();
	$("#sec2").show();
	document.getElementById("sec1").style.display = 'none';
	document.getElementById("sec2").style.display = 'block';
}
function two()
{

}
function three()
{

}
function four()
{

}
</script>
@endsection
