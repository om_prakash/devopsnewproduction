@extends('Page::layout.one-column-page')
@section('content')
<div class="container">
    <div class="row">
    	<div class="col-sm-12">
		<h2 class="color-blue mainHeading">Posts By Transporters</h2>
    </div>

    <!-- Nav tabs -->
	<div class="col-sm-12">
    <ul class="nav nav-tabs filter-tab" role="tablist">
		<li role="presentation" class="active"><a href="#tab1" aria-controls="home" role="tab" data-toggle="tab">Individual</a></li>
		<li role="presentation" class="business-tab"><a href="#tab2" aria-controls="profile" role="tab" data-toggle="tab">Busniess</a></li> 
	</ul> 
		<div class="col-md-12 col-sm-12 col-xs-12 backgroun-grey margin-top">
			<div class="row">
				<div class="col-md-3 col-sm-4 text-left">
					<form accept-charset="UTF-8" action="" method="GET">
						<input type="hidden" value="new" name="current_tab" id="current_tab">
						<div class="form-group">
							<label>Source Departure Address</label>
							<input type="text" name="search_value" placeholder="Enter Source/Departure Address " id="search_value" class="form-control">
						</div>
					</form>
				</div>
				<div class="col-md-3 col-sm-4 text-left" id="business">
					<input type="hidden" value="{{Input::get('current_tab')}}" name="current_tab" id="current_tab">
						<div class="form-group">
							<label>Destination/Arrival Address</label>
							<input type="text" name="search_value" placeholder="Enter Destination/Arrival Address " id="search_value" class="form-control">
						</div> 
				</div>
				<div class="col-md-3 col-sm-4 inline-form-btn">
					<div class="form-group">
						<input type="hidden" value="" id="ajaxPath">
						<input type="submit" value="Search" class="btn btn-primary"> 
						<a class="btn btn-primary" href="">
							<span class="glyphicon glyphicon-refresh" aria-hidden="true">
							</span>&nbsp;&nbsp;Reset
						</a>
					</div>
				</div>     
			</div> 
		</div>
    </div>
    </div>
	<!-- Tab panes -->
	<div class="custom-table1 clearfix">
    	<div class="col-sm-12">
        	<b>Friday 29 Arpil</b>
            <hr />
        </div>
        <div class="col-sm-12">
        	<div class="row">
            	<div class="col-md-4 col-sm-6 img-posts">
                	<img src="http://192.168.11.101/aq/dev3/upload//no-image.jpg">
                    <h4>&nbsp;Lorem ipsum</h4>
            	</div>
                <div class="col-md-8 col-sm-6 details-posts">
                	<h4 class="color-blue">Lorem ipsum dolor sit amet</h4>
                    <p>29 April,2016 at 05:00 PM</p>
                    <p>Category - Suitcase</p>
                    <p>Weight upto - 8Lbs</p>
            	</div>
        	</div>
            <hr />
    	</div>
        <div class="col-sm-12">
        	<div class="row">
            	<div class="col-md-4 col-sm-6 img-posts">
                	<img src="http://192.168.11.101/aq/dev3/upload//no-image.jpg">
                    <h4>&nbsp;Lorem ipsum</h4>
            	</div>
                <div class="col-md-8 col-sm-6 details-posts">
                	<h4 class="color-blue">Lorem ipsum dolor sit amet</h4>
                    <p>29 April,2016 at 05:00 PM</p>
                    <p>Category - Suitcase</p>
                    <p>Weight upto - 8Lbs</p>
            	</div>
        	</div>
            <hr />
    	</div>
        <div class="col-sm-12">
        	<div class="row">
            	<div class="col-md-4 col-sm-6 img-posts">
                	<img src="http://192.168.11.101/aq/dev3/upload//no-image.jpg">
                    <h4>&nbsp;Lorem ipsum</h4>
            	</div>
                <div class="col-md-8 col-sm-6 details-posts">
                	<h4 class="color-blue">Lorem ipsum dolor sit amet</h4>
                    <p>29 April,2016 at 05:00 PM</p>
                    <p>Category - Suitcase</p>
                    <p>Weight upto - 8Lbs</p>
            	</div>
        	</div>
            <hr />
    	</div>
        <div class="col-sm-12">
        	<div class="row">
            	<div class="col-md-4 col-sm-6 img-posts">
                	<img src="http://192.168.11.101/aq/dev3/upload//no-image.jpg">
                    <h4>&nbsp;Lorem ipsum</h4>
            	</div>
                <div class="col-md-8 col-sm-6 details-posts">
                	<h4 class="color-blue">Lorem ipsum dolor sit amet</h4>
                    <p>29 April,2016 at 05:00 PM</p>
                    <p>Category - Suitcase</p>
                    <p>Weight upto - 8Lbs</p>
            	</div>
        	</div>
            <hr />
    	</div>
        <div class="col-sm-12">
        	<div class="row">
            	<div class="col-md-4 col-sm-6 img-posts">
                	<img src="http://192.168.11.101/aq/dev3/upload//no-image.jpg">
                    <h4>&nbsp;Lorem ipsum</h4>
            	</div>
                <div class="col-md-8 col-sm-6 details-posts">
                	<h4 class="color-blue">Lorem ipsum dolor sit amet</h4>
                    <p>29 April,2016 at 05:00 PM</p>
                    <p>Category - Suitcase</p>
                    <p>Weight upto - 8Lbs</p>
            	</div>
        	</div>
            <hr />
    	</div>
        <div class="col-sm-12">
        	<div class="row">
            	<div class="col-md-4 col-sm-6 img-posts">
                	<img src="http://192.168.11.101/aq/dev3/upload//no-image.jpg">
                    <h4>&nbsp;Lorem ipsum</h4>
            	</div>
                <div class="col-md-8 col-sm-6 details-posts">
                	<h4 class="color-blue">Lorem ipsum dolor sit amet</h4>
                    <p>29 April,2016 at 05:00 PM</p>
                    <p>Category - Suitcase</p>
                    <p>Weight upto - 8Lbs</p>
            	</div>
        	</div>
            
    	</div>
    </div>

    <div class="row">
         <div class="col-md-12">
            <div class="tab-content">
          <div role="tabpanel" class="tab-pane active custom-tab-pane" id="tab1">

         <div id="containerdata1" >   </div>
      	 </div>
      		<div role="tabpanel" id="tab2" class="custom-tab-pane tab-pane ">
            
         <div id="containerdata2">   </div>
      	</div>
     		
         </div>			
         </div>   
</div>
<script>
	    
	    $(document).ready(function(){
    
    $(".business-tab").click(function(){
        $("#business").css("display","none");
        $("#business").siblings().css("display","block");
        });
        
     $(".business-tab").siblings().click(function(){
        $("#business").css("display","block");
        $("#business").siblings().css("display","block");
        });
    });
	</script>
@endsection



