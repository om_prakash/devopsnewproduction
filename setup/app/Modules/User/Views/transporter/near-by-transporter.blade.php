@extends('page::layout.one-column-page')
@section('content')

<div class="container">
	<h2 class="color-blue mainHeading">Near By Transporter</h2>
	<div id="googleMap" style="width:1150px;height:380px;"> </div>
	</br>
		
</div>
<script  src="http://maps.googleapis.com/maps/api/js"> </script>
<script>

	function initialize() {
		var mapProp = {
			center:new google.maps.LatLng(51.508742,-0.120850),
			zoom:5,
			mapTypeId:google.maps.MapTypeId.ROADMAP
					  };
			var map=new google.maps.Map(document.getElementById("googleMap"), mapProp);
                          }
			google.maps.event.addDomListener(window, 'load', initialize);
</script>
@endsection



