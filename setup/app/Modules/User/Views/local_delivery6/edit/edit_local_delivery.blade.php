@extends('Page::layout.one-column-page')
@section('content')
<div class="container">

<?php
$ua = strtolower($_SERVER['HTTP_USER_AGENT']);
// you can add different browsers with the same way ..
if (preg_match('/(chromium)[ \/]([\w.]+)/', $ua)) {
	$browser = 'chromium';
} elseif (preg_match('/(chrome)[ \/]([\w.]+)/', $ua)) {
	$browser = 'chrome';
} elseif (preg_match('/(safari)[ \/]([\w.]+)/', $ua)) {
	$browser = 'safari';
} elseif (preg_match('/(opera)[ \/]([\w.]+)/', $ua)) {
	$browser = 'opera';
} elseif (preg_match('/(msie)[ \/]([\w.]+)/', $ua)) {
	$browser = 'msie';
} elseif (preg_match('/(mozilla)[ \/]([\w.]+)/', $ua)) {
	$browser = 'mozilla';
}

preg_match('/(' . $browser . ')[ \/]([\w]+)/', $ua, $version);
?>

   <div class="row">
   		<div class="col-sm-12"  id="send_a_package_start_position">
      		<h2 class="color-blue mainHeading">Local Delivery</h2>
      		<br>
      	</div>
      	<div class="col-sm-12">
      		<div class="box-shadow">
        		<h3>Please complete the following fields</h3>
        		<hr>
        		<div class="row">
        			<!-- step header -->
        			<div class="col-sm-12">
			          	<div class="step_box four_step clearfix">

			          		<div class="step first selected" id="step1-header">
			                   <div>1</div>
			                   <p class="text-center colol-black">Package Details</p>
			                </div>

			                <div class="step inner" id="step2-header">
			                   <div>2</div>
			                   <p class="text-center colol-black">Pickup Address</p>
			                </div>

			                <div class="step inner" id="step3-header">
			                   <div>3</div>
			                   <p class="text-center colol-black">Drop Off Address</p>
			                </div>

			                <div class="step last " id="step4-header">
			                   <div>4</div>
			                   <p class="text-center colol-black">Payment</p>
			                </div>

			          	</div>
			        </div>
        			<!-- end step header -->


        			<!-- Edit form start -->
        			{!! Form::open(['class'=>'form-vertical','name'=>'local_prepare_request_form','id' => 'local_prepare_request_form']) !!}
        			<input type="hidden" name="request_id" id="request_id" value="{{$delivery_data->_id}}">
        			<input type="hidden" name="PickupLat" id="PickupLat" value="">
        			<input type="hidden" name="PickupLong" id="PickupLong" value="">
        			<input type="hidden" name="DeliveryLat" id="DeliveryLat" value="">
        			<input type="hidden" name="DeliveryLong" id="DeliveryLong" value="">


        			<!-- first sec -->
		            <div class="col-sm-12" id="sec1">
	            		<div class="col-sm-12 col-xs-12 text-right">
		                     <a class="btn btn-default"  id="add_item_button" href="{{url('local-prepare-add-item',[$delivery_data->_id])}}">Add Item</a>

		                  </br>
		                  </br>
		               	</div>
			            <div class="col-md-12">
			            	<table class="custom-table table table-bordered">
			                    <thead>
			                        <tr>
			                           <th>S.No.</th>
			                           <th>Image</th>
			                           <th>Name</th>
			                           <th>Dimension</th>
			                           <th>Category</th>
			                           <th>Insurance Status</th>
			                           <th>Quantity</th>
			                           <th>Package Value</th>
			                          {{--  <th>Shipping Mode</th> --}}
			                           <th>Need Package Material</th>
			                           <th>Action</th>
			                        </tr>
			                    </thead>
			                    <?php $no = 0;?>
			                    @if(count($delivery_data->ProductList) > 0)
			                    	@foreach($delivery_data->ProductList as $key)
			                    	<?php $no = $no + 1;?>
				                    <tbody>
				                    <tr id="row-{{$key['_id']}}">

				                    	<td><?php echo $no; ?></td>
	            						<td>
	            							@if($key['ProductImage'] != '')
	            								<img src="{{ ImageUrl.$key['ProductImage']}}" width="100px" height="80px" >
	            							@else
	            								<img src="{{ ImageUrl}}/no-image.jpg" width="130px" height="100px" >
	            							@endif
	            						</td>
	            						 <td>{{ $key['product_name'] }}</td>
				            						<td>

				            						L- {{ $key['productLength'] }} {{$key['ProductLengthUnit']}}<br>
				            						H-{{$key['productHeight']}} {{$key['productHeightUnit']}}<br>
				            						W-{{$key['productWidth']}} {{$key['productHeightUnit']}}<br>
				            						Weight-{{$key['productWeight']}} {{$key['ProductWeightUnit']}}<br>

				            						</td>
				            						<td>{{ucfirst($key['productCategory'])}}</td>
				            						<td>{{ucfirst( $key['InsuranceStatus'])}}</td>
				            						<td>{{@$key['BoxQuantity']}}</td>
				            						<td>{{$key['productCost']}}</td>
				            						{{-- <td>{{ucfirst($key['travelMode'])}}</td> --}}
				            						<td>{{ucfirst($key['PackageMaterial'])}}</td>
				            						<td>
				            							<a title="Edit"  href="{{url('local-prepare-request-edit-item',[$key['_id']])}}/{{$delivery_data->_id}}" > <i class="fa fa-pencil"></i></a>&nbsp;

				                          				<a title="delete" id="Delete"  onclick="remove_record('delete_item/<?php echo $key['_id']; ?>/edit_local_delivery_item','<?php echo $key['_id']; ?>','<?php echo $delivery_data->_id; ?>')" href="javascript:void(0)"><i class='fa fa-trash'></i></a>
				            						</td>

				            					</tr>
				                    </tbody>
				                    @endforeach
								@endif
			              </table>

			            </div>

			            <div class="col-md-12">
				            <div class="form-group ">
		                        <button class="custom-btn1 btn text-center" id="step1-next-btn">
		                           Next
		                           <div class="custom-btn-h"></div>
		                        </button>

		                    </div>
		               	</div>

				    </div>
		            <!-- end first section -->
		            <!-- pick up address -->
	             	<div class="col-md-10 col-sm-offset-1" id="sec2" style="display: none;">
		            	<div class="row">
		                  	<div class="col-md-12 col-xs-12">
		                    	<h3 class="color-blue bottom-border">Pickup Address</h3>
		                  	</div>

		                  	<div class="col-sm-12 col-xs-12">
		                      <div class="checkbox">
		                          <label>
		                          <span class="pull-left"><input type="checkbox" name="public_place"></span>
		                                <span class="Insurance_check">This is a public place.</span>
		                          </label>
		                      </div>
		                  	</div>

			                <div class="col-sm-4 col-xs-12">
			                    <div class="form-group">
			                        <label>Address Line 1</label>
			                        {!! Form::text('address_line_1',$delivery_data->PickupAddress,['class'=>"form-control required",'placeholder'=>"Address",'maxlength' => 100,'id' => 'address_line_1'])  !!}
			                    </div>
			                </div>
			                <div class="col-sm-4 col-xs-12">
			                    <div class="form-group">
			                        <label>Address Line 2</label>
			                        {!! Form::text('address_line_2',$delivery_data->PickupAddress2,['class'=>"form-control",'placeholder'=>"Address",'maxlength' => 80,'id'=>'address_line_2'])  !!}
			                    </div>
			                </div>
		              	</div>

		                <div class="row">
			                  <input type="hidden" name="country" class="form-control required" id="pp_pickup_country"  value='{"id":"{{$country[1]->_id}}","name":"{{$country[1]->Content}}","state_available":"{{$country[1]->state_available}}"}'>
			                   <div class="col-sm-4 col-xs-12">
			                     <div class="form-group">
			                        <label class="control-label">State/Region</label>
			                        <select name="state" class="form-control required left-disabled" id="pp_pickup_state" onchange="get_city('pp_pickup_state','pp_pickup_city','pp_pickup_city','{{$delivery_data->PickupCity}}')">
	                            		<option value="">Select State/Region</option>
	                            		 @foreach($all_state as $pickupState)
			                                  <option value='{"id":"{{$pickupState->_id}}","name":"{{$pickupState->Content}}"}'>{{$pickupState->Content}}
			                                  </option>
			                              @endforeach
	                        		</select>
			                     </div>
			                  </div>

			                  <div class="col-sm-4 col-xs-12">
			                     <div class="form-group">
			                        <label class="control-label">City</label>
			                        <select  name="city" class="form-control required" id="pp_pickup_city">
			                            <option value="">Select City</option>
			                        </select>
			                     </div>
			                  </div>
			            </div>


	                  	<div class="row">
		                  	<div class="col-sm-4 col-xs-12">
			                     <div class="form-group">
			                        <label>Digital Address(If known)</label>
			                        <!-- {!! Form::text('zipcode',$delivery_data->PickupPinCode,['class'=>"form-control alpha-numeric  maxlength-8",'placeholder'=>"Zip Code/Postcode",'maxlength' => 8])  !!} -->

			                        <input type="text" value="{{$delivery_data->PickupPinCode}}" name="zipcode" placeholder="Digital Address(If known)" maxlength="8 " class="form-control alpha-numeric  maxlength-8">
			                     </div>
		                  	</div>

		                  	<div class="col-sm-4 col-xs-12">
		                     	<div class="form-group">
		                        	<label>Pickup Date</label>
	      							<label>Pickup Date</label>
			                        {!! Form::text('pickup_date',show_date($delivery_data->PickupDate)
			                        ,['class'=>"form-control required",'placeholder'=>"Date","id"=>"pickup_date",'readonly'=>'true', 'readonly' => 'true'])  !!}

		                     	</div>
		                  	</div>
		              	</div>






		                <div class="">
		                     <hr>
		                </div>

		                <div class="form-group ">
	                        
	                        <a class="custom-btn1 btn text-center" onclick="switch_request_header('#sec1','#sec2',1)" href="javascript:void(0)">
	                           Back
	                           <div class="custom-btn-h"></div>
	                        </a>
	                        <button class="custom-btn1 btn text-center" id="step2-next-btn">
	                           Next
	                           <div class="custom-btn-h"></div>
	                        </button>
	                    </div>

	              	</div>
	             	<!-- end pickup -->
	             	<!-- drop of 3 -->
	             	<div class="row" id="sec3" style="display:none;">
	                 <div class="col-md-10 col-sm-offset-1">
	                 	<div class="">
		                    <div class="col-md-12 col-xs-12">
		                       <h3 class="color-blue">Drop Off Address</h3>
		                    </div>
		                    <div class="col-sm-4 col-xs-12">
		                       <div class="form-group">
		                          <label class="control-label">Address Line 1</label>
		                          <input name="drop_off_address_line_1" class="form-control required usename-#address line1#" placeholder="Address" name="drop_address" maxlength="100" id="drop_off_address_line_1" value="{{$delivery_data->DeliveryAddress}}">
		                       </div>
		                    </div>
		                    <div class="col-sm-4 col-xs-12">
		                       <div class="form-group">
		                          <label class="control-label">Address Line 2</label>
		                          <input name="drop_off_address_line_2" class="form-control usename-#address line2#" placeholder="Address" name="drop_address2"  maxlength="100" id="drop_off_address_line_2" value="{{$delivery_data->DeliveryAddress2}}">
		                       </div>
		                    </div>
		                </div>
	                    <div class="clearfix"></div>

	                    <div class="">
		                {{--   <div class="col-sm-4 col-xs-12">
		                     <div class="form-group">
		                        <label class="control-label">Country</label>
		                        <select name="drop_off_country" class="form-control required usename-#country#" id="pp_dropoff_country" onchange="get_state('pp_dropoff_country','pp_dropoff_state','pp_dropoff_city','pp_dropoff_state','')">
		                            <option value="">Select Country</option>
		                            @foreach($country as $key)
		                             <option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}","state_available":"{{$key->state_available}}"}' @if($delivery_data->DeliveryCountry == $key->Content) selected='selected' @endif>{{$key->Content}}</option>
		                            @endforeach
		                        </select>
		                     </div>
		                  </div> --}}

		                  <input type="hidden" name="drop_off_country" class="form-control required" id="pp_dropoff_country"  value='{"id":"{{$country[1]->_id}}","name":"{{$country[1]->Content}}","state_available":"{{$country[1]->state_available}}"}'>

		                  <div class="col-sm-4 col-xs-12">
		                     <div class="form-group">
		                        <label class="control-label">State/Region</label>
		                        <select name="drop_off_state" class="form-control required usename-#state# left-disabled" id="pp_dropoff_state" onchange="get_city('pp_dropoff_state','pp_dropoff_city','pp_dropoff_city','')">
		                            <option value="">Select State/Region</option>
		                            @foreach($all_state as $dropoffState)
			                                  <option value='{"id":"{{$dropoffState->_id}}","name":"{{$dropoffState->Content}}"}'>{{$dropoffState->Content}}
			                                  </option>
			                        @endforeach
		                        </select>
		                     </div>
		                  </div>

		                  <div class="col-sm-4 col-xs-12">
		                     <div class="form-group">
		                        <label class="control-label">City</label>
		                        <select  name="drop_off_city" class="form-control required usename-#city#" id="pp_dropoff_city">
		                            <option value="">Select City</option>
		                        </select>
		                     </div>
		                  </div>
		               </div>
		                <div class="clearfix"></div>

		               <div class="">
		                    <div class="col-sm-4 col-xs-12">
		                       <div class="form-group">
		                          <label class="control-label">Digital Address(If known)</label>
		                          <input class="form-control alpha-numeric usename-#zipcode#" placeholder="Digital Address(If known)" name="drop_off_zipcode" maxlength="8" value="{{$delivery_data->DeliveryPincode}}">
		                       </div>
		                    </div>
		                    <div class="col-sm-4 col-xs-12">
		                       <div class="form-group">
		                          <label class="control-label">Date</label>
		                          <input class="form-control required usename-#date#" placeholder="Date" name="drop_off_date" id="drop_off_date" readonly value="{{ show_date($delivery_data->DeliveryDate) }}">
		                       </div>
		                    </div>
		                </div>
	                    <div class="clearfix"> </div>

	                    <div class="col-sm-12 col-xs-12">
		                    <div class="form-group">
		                      	<label class="control-label">Receiver's Phone number<span class="red-star"> *</span></label>
		                      	<div class="row">
		                         	<!-- <div class="col-xs-2 col-xs-12">
		                            	<input type="text" class="form-control required" placeholder="Country Code" name="country_code" maxlength="4" value="{{ $delivery_data->ReceiverCountrycode }}">
		                         	</div> -->
		                         	<div class="col-xs-4 col-xs-12">
		                            	<input type="text" class="form-control required numeric between-8-12" name="phone_number" placeholder="Phone Number" pattern=".{8,12}" title="8 to 12 numbers" size="5" value="{{ $delivery_data->ReceiverMobileNo }}">
		                         	</div>
		                      	</div>
		                   	</div>
		                </div>





		                <div class="">
		                    <div class="col-sm-4 col-xs-12">
		                       <div class="form-group">
		                          <label class="control-label">Is Delivery Date/Time Flexible?</label>
		                          <div class="checkbox">
		                             <label>
		                             <span class="pull-left row"><input type="radio" name="is_delivery_date_flexible" value="yes" <?php if ($delivery_data->FlexibleDeliveryDate == "yes") {echo 'checked=="checked"';}?> ></span>  <span class="Insurance_check">  Yes </span>
		                             </label>
		                             &nbsp; &nbsp;
		                             <label>
		                             <span class="pull-left"><input type="radio" name="is_delivery_date_flexible" value="no" <?php if ($delivery_data->FlexibleDeliveryDate == "no") {echo 'checked=="checked"';}?> ></span>  <span class="Insurance_check">  No </span>
		                             </label>
		                          </div>
		                       </div>
		                    </div>




		                    <div class="col-sm-4 col-xs-12">
		                       <div class="form-group">
		                          <label class="control-label">Journey Type</label>
		                          <div class="checkbox">
		                             <label>
		                             <div class="pull-left row"><input type="radio" value="one_way" name="journey_type" onclick="$('#return_jurney_section').hide()" <?php if ($delivery_data->JournyType == "one_way") {echo 'checked=="checked"';}?> id="one_way_journey"></div>  <span class="Insurance_check">  One Way </span>
		                             </label>
		                             &nbsp; &nbsp;
		                             <label>
		                             <span class="pull-left"><input type="radio" value="return" name="journey_type" onclick="$('#return_jurney_section').show()" <?php if ($delivery_data->JournyType == "return") {echo 'checked=="checked"';}?> id="return_journy"></span>  <span class="Insurance_check">  Return  </span>
		                             </label>
		                          </div>
		                       </div>
		                    </div>


		                </div>
		                <div id="return_jurney_section"  style="display:none;">
		                	<div class="">
			                	<div class="col-sm-12 col-xs-12">
			                       	<div class="form-group">
			                       		<h3 class="color-blue">Return Address</h3>
			                          	<div class="checkbox">
			                             <span class="pull-left">
			                             <input type="checkbox" id="return_same_as_pickup" name="return_same_as_pickup">
			                             </span>
			                               <span class="Insurance_check"> Same as pickup address</span>
			                          	</div>
			                       	</div>
			                    </div>
			                </div>

		                   	<div id="return_jurney_address">
				               	<div class="">
				                    <div class="col-sm-4 col-xs-12">
				                       <div class="form-group">
				                          <label class="control-label">Address Line 1</label>
				                          <input name="return_address_line_1" class="form-control required usename-#address line 1#" placeholder="Address" name="return_address" maxlength="100" value="{{$delivery_data->ReturnAddress}}">
				                       </div>
				                    </div>
				                    <div class="col-sm-4 col-xs-12">
				                       <div class="form-group">
				                          <label class="control-label">Address Line 2</label>
				                          <input name="return_address_line_2" class="form-control usename-#address line 2#" placeholder="Address" name="return_address2" maxlength="100" value="{{$delivery_data->ReturnAddress2}}">
				                       </div>
				                    </div>
				                </div>
			                    <div class="">
			                     <input type="hidden" name="return_country" class="form-control required" id="pp_return_country"  value='{"id":"{{$country[1]->_id}}","name":"{{$country[1]->Content}}","state_available":"{{$country[1]->state_available}}"}'>
				                 {{--  <div class="col-sm-4 col-xs-12">
				                     <div class="form-group">
				                        <label class="control-label">Country</label>
				                        <select name="return_country" class="form-control required usename-#country#" id="pp_return_country" onchange="get_state('pp_return_country','pp_return_state','pp_return_city','pp_return_state','')">
				                            <option value="">Select Country</option>
				                            @foreach($country as $key)
				                             <option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}","state_available":"{{$key->state_available}}"}' @if($delivery_data->ReturnCountry == $key->Content) selected='selected' @endif>{{$key->Content}}</option>
				                            @endforeach
				                        </select>
				                     </div>
				                  </div> --}}
				                  <div class="col-sm-4 col-xs-12">
				                     <div class="form-group">
				                        <label class="control-label">State/Region</label>
				                        <select name="return_state" class="form-control required usename-#state# left-disabled" id="pp_return_state" onchange="get_city('pp_return_state','pp_return_city','pp_return_city','')">
				                            <option value="">Select State/Region</option>
				                             @foreach($all_state as $returnState)
			                                  <option value='{"id":"{{$returnState->_id}}","name":"{{$returnState->Content}}"}'>{{$returnState->Content}}
			                                  </option>
			                              @endforeach
				                        </select>
				                     </div>
				                  </div>
				                  <div class="col-sm-4 col-xs-12">
				                     <div class="form-group">
				                        <label class="control-label">City</label>
				                        <select  name="return_city" class="form-control required usename-#city#" id="pp_return_city">
				                            <option value="">Select City</option>
				                        </select>
				                     </div>
				                  </div>
				               </div>
				               <div class="">
				                    <div class="col-sm-4 col-xs-12">
				                       <div class="form-group">
				                          <label class="control-label">Digital Address(If known)</label>
				                          <input class="form-control alpha-numeric usename-#zipcode#" placeholder="Digital Address(If known)" name="return_zipcode" maxlength="8" value="{{$delivery_data->ReturnPincode}}">
				                       </div>
				                    </div>
				                </div>
			                </div>
		             	</div>
						<div class="clearfix"></div>
		                <div class="">
		                	<div class="col-sm-12 col-xs-12">
		                       <div class="form-group">
		                       		<h3 class="color-blue">Return address(If item is not delivered)</h3>
		                          	<div class="">
			                            <span class="pull-left">
			                             <input type="checkbox" id="same_as_pickup" name="same_as_pickup" >
			                             </span>
			                             <span class="Insurance_check">
			                              Same as pickup address
			                            </span>
			                        </div>
		                       </div>
		                    </div>
		                </div>

		                <div id="return_address_action" style="display:none;">
			               	<div class="">
			                    <div class="col-sm-4 col-xs-12">
			                       <div class="form-group">
			                          <label class="control-label">Address Line 1</label>
			                          <input name="nd_return_address_line_1" class="form-control required usename-#address line 1#" placeholder="Address" maxlength="100">
			                       </div>
			                    </div>
			                    <div class="col-sm-4 col-xs-12">
			                       <div class="form-group">
			                          <label class="control-label">Address Line 2</label>
			                          <input name="nd_return_address_line_2" class="form-control usename-#address line 2#" placeholder="Address" maxlength="100">
			                       </div>
			                    </div>
			                </div>
		                    <div class="">
		                    	<input type="hidden" name="nd_return_country" class="form-control required" id="pp_nd_return_country"  value='{"id":"{{$country[1]->_id}}","name":"{{$country[1]->Content}}","state_available":"{{$country[1]->state_available}}"}'>
			                 {{--  <div class="col-sm-4 col-xs-12">
			                     <div class="form-group">
			                        <label class="control-label">Country</label>
			                        <select name="nd_return_country" class="form-control required usename-#country#" id="pp_nd_return_country" onchange="get_state('pp_nd_return_country','pp_nd_return_state','pp_nd_return_city','pp_nd_return_state','')">
			                            <option value="">Select Country</option>
			                            @foreach($country as $key)
			                             <option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}","state_available":"{{$key->state_available}}"}'>{{$key->Content}}</option>
			                            @endforeach
			                        </select>
			                     </div>
			                  </div> --}}

			                  <div class="col-sm-4 col-xs-12">
			                     <div class="form-group">
			                        <label class="control-label">State/Region</label>
			                        <select name="nd_return_state" class="form-control required usename-#state# left-disabled" id="pp_nd_return_state" onchange="get_city('pp_nd_return_state','pp_nd_return_city','pp_nd_return_city','')">
			                            <option value="">Select State/Region</option>
			                             @foreach($all_state as $nd_returnState)
			                                  <option value='{"id":"{{$nd_returnState->_id}}","name":"{{$nd_returnState->Content}}"}'>{{$nd_returnState->Content}}
			                                  </option>
			                              @endforeach
			                        </select>
			                     </div>
			                  </div>
			                  <div class="col-sm-4 col-xs-12">
			                     <div class="form-group">
			                        <label class="control-label">City</label>
			                        <select  name="nd_return_city" class="form-control required usename-#city#" id="pp_nd_return_city">
			                            <option value="">Select City</option>
			                        </select>
			                     </div>
			                  </div>
			               </div>
			               <div class="">
			                    <div class="col-sm-4 col-xs-12">
			                       <div class="form-group">
			                          <label class="control-label">Zipcode</label>
			                          <input class="form-control alpha-numeric usename-#zipcode#" placeholder="Zipcode" name="nd_return_zipcode" maxlength="6">
			                       </div>
			                    </div>
			                </div>
		             	</div>
	                    <div class="clearfix"></div>
	                    <div class="">
	                       <hr>
	                    </div>
	                    <div class="col-sm-8 col-xs-12">
	                    	<div class="">
				                    <div class="form-group">
				                        <a class="custom-btn1 btn text-center" onclick="switch_request_header('#sec2','#sec3',2)" href="javascript:void(0)">
				                           Back
	                                       <div class="custom-btn-h"></div>
				                        </a>
				                     </div> 
				                <div class="col-xs-6 col-xs-12">
				                	<div class="form-group">
				                        <button class="custom-btn1 btn text-center" id="step3-next-btn">
				                           Next
				                           <div class="custom-btn-h"></div>
				                        </button>
				                     </div>
				                     
				                </div>
		                  </div>
	                    </div>

	                 </div>
	              	</div>
	             	<!-- end drop off 3 -->

	             	<!-- section 4 payment  "-->
	             	<div class="row" id="sec4" style="display:none;">
		                <div class="col-md-10 col-sm-offset-1">
		                   <div class="col-sm-12 col-xs-12">
		                   		<div class="row custom-row">
								   <div class="col-sm-6"><b>Total Item -</b></div>
								   <div class="col-sm-6" id="total_item"></div>
								</div>

		                   		<div class="row custom-row">
								   <div class="col-sm-6"><b>Distance -</b></div>
								   <div class="col-sm-6" id="distance"></div>
								</div>

								<div class="row custom-row">
								   <div class="col-sm-6"><b>Total Weight -</b></div>
								   <div class="col-sm-6" id="total_weight"></div>
								</div>


								<div class="row custom-row">
								   <div class="col-sm-6"><b>Total Volume -</b></div>
								   <div class="col-sm-6" id="total_volume"></div>
								</div>

								<div class="row custom-row">
								   <div class="col-sm-6"><b>Shipping Cost -</b></div>
								   <div class="col-sm-6" id="shipping_cost"></div>
								</div>
								<div class="row custom-row">
								   <div class="col-sm-6"><b>Insurance Cost -</b></div>
								   <div class="col-sm-6" id="insurance_cost"></div>
								</div>
								<div class="row custom-row">
								   <div class="col-sm-6"><b>Reason Charge  -</b></div>
								   <div class="col-sm-6" id="region_cost"></div>
								</div>
								<div class="row"> <hr /> </div>


								<div class="row custom-row">
								   <div class="col-sm-6"><b>Total Amount -</b></br>
								   <small id="user_currency"></small>

								    </div>
								   <div class="col-sm-6" id="total_amount"></div>
								</div>

								<input type="hidden" id="shipping_cost_input" value="" name="shipping_cost_input">
								<input type="hidden" id="total_amount_input" value="" name="total_amount_input">


								<div class="row">
									<hr>
									<div class="col-sm-12">
										<div class="form-group reward-group" id="olp_promo_input">
											<input type="text" placeholder="Promotion Code" class="form-control pull-left" id="promocode" name="promocode" >
											<a class="btn default-btn" onclick="check_promocode('promocode','shipping_cost_input','olp_promo','total_amount_input')" href="javascript:void(0)">
												<img src="{{asset('theme/web/promo/images/green_check.png')}}" />Apply
											</a>
											<div class="clearfix"></div>
											<div  id="er_promocode" class="color-red"></div>
										</div>
										<div class="form-group reward-group" id="olp_promo" style='display: none;'>
											<span class="" onclick="remove_promocode('olp_promo','promocode')"><i class="fa fa-trash"></i></span><span id="olp_promo_msg"></span>
										</div>
									</div>
								</div>

								<div class="row"> <hr /> </div>

								<div class="row custom-row">
									<div class="col-sm-6"><b class="color-blue">Estimated Cost -</b></br>
									<small id="olp_promo_payable_ghana"></small>
									</div>
									<div class="col-sm-6 color-blue" id="olp_promo_payable"></div>
								</div>




		                      <div class="">
		                    	<div class="">
			                  		<div class="">
					                     <div class="form-group">
					                        
					                        <a class="custom-btn1 btn text-center" onclick="switch_request_header('#sec3','#sec4',3)" href="javascript:void(0)">
					                           Back
		                                       <div class="custom-btn-h"></div>
					                        </a>
					                        <button class="custom-btn1 btn" id="last-stage" >
					                           Next
					                           <div class="custom-btn-h btn"></div>
					                        </button>
					                     </div>
					                </div>
			                  	</div>
		                       </div>
		                	</div>
		                </div>
	              	</div>
	             	<!--end section 4 payment -->


        			<!-- End edit form -->
        			{!! Form::close() !!}
        		</div>
        	</div>
      	</div>
   </div>
</div>
<style type="text/css">
    .Insurance_check{float: left; margin-top: 3px; margin-left: 5px;}

</style>

@endsection

@section('script')
@parent

<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyCT0uj-bffWWdOdwDp15LvrMko-OuoZUoY"></script>

{!! Html::script('theme/web/js/utility.js') !!}
{!! Html::script('theme/web/js/validation.js') !!}
{!! Html::script('theme/web/js/local_delivery.js') !!}
{!! Html::style('theme/date-time-picker/jquery.datetimepicker.css') !!}
{!! Html::script('theme/date-time-picker/jquery.datetimepicker.js') !!}

@endsection

@section('inline-script')
@parent
<script type="text/javascript" >
$('#pp_pickup_country').trigger('change');
$('#pp_dropoff_country').trigger('change');
$('#pp_return_country').trigger('change');
$('#pp_nd_return_country').trigger('change');

$(document).ready(function(){
	$('#pp_pickup_state').val('{"id":"{{$state->_id}}","name":"{{$state->Content}}"}');
	$('#pp_dropoff_state').val('{"id":"{{$drop_state->_id}}","name":"{{$drop_state->Content}}"}');
	$('#pp_return_state').val('{"id":"{{@$return_state->_id}}","name":"{{@$return_state->Content}}"}');
	$('#pp_nd_return_state').val('{"id":"{{@$nd_return_state->_id}}","name":"{{@$nd_return_state->Content}}"}');
	setTimeout(select_state,1000);
});

function select_state()
{
	$('#pp_pickup_state option').each(function() {
	    if($(this).val() == '{"id":"{{$state->_id}}","name":"{{$state->Content}}"}') {
	        $(this).prop("selected", true);
	    }
	});
	$('#pp_pickup_state').trigger('change');

	$('#pp_dropoff_state option').each(function() {
	    if($(this).val() == '{"id":"{{$drop_state->_id}}","name":"{{$drop_state->Content}}"}') {
	        $(this).prop("selected", true);
	    }
	});
	$('#pp_dropoff_state').trigger('change');
	$('#pp_return_state option').each(function() {

	    if($(this).val() == '{"id":"{{@$return_state->_id}}","name":"{{@$return_state->Content}}"}') {
	        $(this).prop("selected", true);
	    }
	});
	$('#pp_return_state').trigger('change');
	$('#pp_nd_return_state option').each(function() {

	    if($(this).val() == '{"id":"{{@$nd_return_state->_id}}","name":"{{@$nd_return_state->Content}}"}') {
	        $(this).prop("selected", true);
	    }
	});
	$('#pp_nd_return_state').trigger('change');
	setTimeout(select_city,1000);
}

function select_city(){
	$('#pp_pickup_city option').each(function() {
	    if($(this).val() == '{"id":"{{@$city->_id}}","name":"{{@$city->Content}}"}') {
	        $(this).prop("selected", true);
	    }
	});

	$('#pp_dropoff_city option').each(function() {
	    if($(this).val() == '{"id":"{{@$drop_city->_id}}","name":"{{@$drop_city->Content}}"}') {
	        $(this).prop("selected", true);
	    }
	});

	$('#pp_return_city option').each(function() {
	    if($(this).val() == '{"id":"{{@$return_city->_id}}","name":"{{@$return_city->Content}}"}') {
	        $(this).prop("selected", true);
	    }
	});
	$('#pp_nd_return_city option').each(function() {
	    if($(this).val() == '{"id":"{{@$nd_return_city->_id}}","name":"{{@$nd_return_city->Content}}"}') {
	        $(this).prop("selected", true);
	    }
	});
}

var newdate = new Date();
newdate.setHours(newdate.getHours()+3);

var minDateForTwo = newdate;

var drop_off_date = {};
    $('#pickup_date').datetimepicker({
        format:'M d, Y h:i A',
        formatTime:'h:i A',
        timepicker:true,
        datepicker:true,
        step:30,
        minDate : new Date(),
        minTime : newdate,
        onChangeDateTime:function( ct ){
            minDateForTwo = ct;
            drop_off_date.minDate = ct;
            var dt = new Date();
            dt.setHours(23);dt.setMinutes(59);dt.setSeconds(59);

            if(ct < dt) {
              $('#pickup_date').datetimepicker({  minTime: newdate });
              drop_off_date.minTime = ct;

              if(ct < newdate) {
                $('#pickup_date').val(dt.dateFormat('M d, Y ')+newdate.dateFormat('h:i A'));
                drop_off_date.minTime = minDateForTwo;
              }
            } else {
              dt.setHours(0);
              $('#pickup_date').datetimepicker({  minTime: dt });
              drop_off_date.minTime = false;
            }
            $('#drop_off_date').datetimepicker(drop_off_date);
            $('#drop_off_date').val('');
         },
   });

   $("#drop_off_date").datetimepicker({
       format:'M d, Y h:i A',
       formatTime:'h:i A',
       timepicker:true,
       datepicker:true,
       step:30,
       minDate : new Date(),
       minTime : minDateForTwo,
       closeOnDateSelect : false,
       onChangeDateTime:function( ct ){
          var date2option = {};
        if(($("#drop_off_date").val()).trim() != '') {

            $('#pickup_time').datetimepicker({  maxDate:ct, maxTime:ct  })

            if(ct < minDateForTwo) {
              $('#drop_off_date').val(ct.dateFormat('M d, Y ')+minDateForTwo.dateFormat('h:i A'));
              date2option.minTime = minDateForTwo;
            } else {
              date2option.minTime = false;
            }
            $('#drop_off_date').datetimepicker(date2option);


          }
       },
   });

function remove_record(url,rowid)
{
	//http://192.168.1.101/aq/dev3/delete_item/5a86c5d17ac6f6f24f8b4567/send_package_item
    if(confirm('Are you sure you want to delete this item?') == true)
    {

    	var url1 = $(location).attr('href');
    	var fn = url1.split('/').reverse()[0];

       $('#row-'+rowid).addClass('relative-pos spinning');
       url = SITEURL+'remove-item/'+rowid+"/"+fn;


       //alert(url);
        $.ajax
        ({
            url: SITEURL+'remove-item/'+rowid+"/"+fn,
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function(res)
            {
                var obj = JSON.parse(res);
                $('#row-'+rowid).removeClass('relative-pos spinning');
                if(obj.success == 1){

                    $('#row-'+rowid).css({'background-color':'red'});
                    $('#row-'+rowid).fadeOut('slow');
                    $('#error_msg_section').html('<div class="alert alert-success">'+ obj.msg + '</div>');
                }else{
                    $('#row-'+rowid).css({'background-color':'white'});
                    $('#error_msg_section').html('<div class="alert alert-danger">'+ obj.msg + '</div>');
                    alert(obj.msg);
                }
            }
        });
    }
    return false;
}
</script>

@endsection