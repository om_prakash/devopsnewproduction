@extends('Page::layout.one-column-page')
@section('content')
<div class="container">
		<?php
$ua = strtolower($_SERVER['HTTP_USER_AGENT']);
// you can add different browsers with the same way ..
if (preg_match('/(chromium)[ \/]([\w.]+)/', $ua)) {
	$browser = 'chromium';
} elseif (preg_match('/(chrome)[ \/]([\w.]+)/', $ua)) {
	$browser = 'chrome';
} elseif (preg_match('/(safari)[ \/]([\w.]+)/', $ua)) {
	$browser = 'safari';
} elseif (preg_match('/(opera)[ \/]([\w.]+)/', $ua)) {
	$browser = 'opera';
} elseif (preg_match('/(msie)[ \/]([\w.]+)/', $ua)) {
	$browser = 'msie';
} elseif (preg_match('/(mozilla)[ \/]([\w.]+)/', $ua)) {
	$browser = 'mozilla';
}

preg_match('/(' . $browser . ')[ \/]([\w]+)/', $ua, $version);
?>
   <div class="row">
   		<div class="col-sm-12"  id="send_a_package_start_position">
      		<h2 class="color-blue mainHeading">Local Delivery</h2>
      		<br>
      	</div>
      	<div class="col-sm-12">
      		<div class="box-shadow">
        		<h3>Please complete the following fields</h3>
        		<hr>
        		<div class="row">
        			<!-- step header -->
        			<div class="col-sm-12">
			            <div class="step_box four_step clearfix">
			                <div class="step first selected" id="step1-header">
			                   <div>1</div>
			                   <p class="text-center colol-black">Pickup Address</p>
			                </div>
			                <div class="step inner" id="step2-header">
			                   <div>2</div>
			                   <p class="text-center colol-black">Drop Off Address</p>
			                </div>
			                <div class="step inner" id="step3-header">
			                   <div>3</div>
			                   <p class="text-center colol-black">Package Details</p>
			                </div>
			                <div class="step last " id="step4-header">
			                   <div>4</div>
			                   <p class="text-center colol-black">Payment</p>
			                </div>
			             </div>
			        </div>
        			<!-- end step header -->

        			<!-- Edit form start -->
        			{!! Form::open(['class'=>'form-vertical','name'=>'local_prepare_request_form','id' => 'local_prepare_request_form']) !!}
        			<input type="hidden" name="request_id" id="request_id" value="{{$delivery_data->_id}}">
        			<!-- first sec -->
		            <div class="col-sm-12" id="sec1">
	            		<div class="col-sm-12 col-xs-12 text-right">
		                     <a class="btn btn-default"  id="add_item_button" href="{{url('local-prepare-add-item',[$delivery_data->_id])}}">Add Item</a>

		                  </br>
		                  </br>
		               	</div>
			            <div class="col-md-12">
			            	<table class="custom-table table table-bordered">
			                    <thead>
			                        <tr>
			                           <th>S.No.</th>
			                           <th>Image</th>
			                           <th>Name</th>
			                           <th>Dimension</th>
			                           <th>Category</th>
			                           <th>Insurance Status</th>
			                           <th>Quantity</th>
			                           <th>Package Value</th>
			                          {{--  <th>Shipping Mode</th> --}}
			                           <th>Need Package Material</th>
			                           <th>Action</th>
			                        </tr>
			                    </thead>
			                    <?php $no = 0;?>
			                    @if(count($delivery_data->ProductList) > 0)
			                    	@foreach($delivery_data->ProductList as $key)
			                    	<?php $no = $no + 1;?>
				                    <tbody>
				                    <tr >

				                    	<td><?php echo $no; ?></td>
	            						<td>
	            							@if($key['ProductImage'] != '')
	            								<img src="{{ ImageUrl.$key['ProductImage']}}" width="100px" height="80px" >
	            							@else
	            								<img src="{{ ImageUrl}}/no-image.jpg" width="130px" height="100px" >
	            							@endif
	            						</td>
	            						<td>{{ ucfirst($key['product_name']) }} </td>
				            						<td>

				            						L- {{ $key['productLength'] }} {{$key['ProductLengthUnit']}}<br>
				            						H-{{$key['productHeight']}} {{$key['productHeightUnit']}}<br>
				            						W-{{$key['productWidth']}} {{$key['productHeightUnit']}}<br>
				            						Weight-{{$key['productWeight']}} {{$key['ProductWeightUnit']}}<br>

				            						</td>
				            						<td>{{ucfirst($key['productCategory'])}}</td>
				            						<td>{{ucfirst( $key['InsuranceStatus'])}}</td>
				            						<td>{{$key['BoxQuantity']}}</td>
				            						<td>{{$key['productCost']}}</td>
				            					{{-- 	<td>{{ucfirst($key['travelMode'])}}</td> --}}
				            						<td>{{ucfirst($key['PackageMaterial'])}}</td>
				            						<td>
				            							<a title="Edit"  href="#" > <i class="fa fa-pencil"></i></a>&nbsp;

				                          				<a title="delete" id="Delete"  onclick="remove_record('delete_item/<?php echo $key['_id']; ?>/send_package_item','<?php echo $key['_id']; ?>','<?php echo Input::get('request_id'); ?>')" href="javascript:void(0)"><i class='fa fa-trash'></i></a>
				            						</td>

				            					</tr>
				                    </tbody>
				                    @endforeach
								@endif
			              </table>

			            </div>

			            <div class="col-md-12">
				            <div class="form-group ">
		                        <button class="custom-btn1 btn text-center" id="step1-next-btn">
		                           Next
		                           <div class="custom-btn-h"></div>
		                        </button>

		                    </div>
		               	</div>

				    </div>
		            <!-- end first section -->
		            <!-- pick up address -->
	             	<div class="col-md-10 col-sm-offset-1" id="sec2" style="display: none;">
		            	<div class="row">
		                  	<div class="col-md-12 col-xs-12">
		                    	<h3 class="color-blue bottom-border">Pickup Address</h3>
		                  	</div>

		                  	<div class="col-sm-12 col-xs-12">
		                      <div class="checkbox">
		                          <label>
		                          <span class="pull-left"><input type="checkbox" name="public_place"></span>
		                                <span class="Insurance_check">This is a public place.</span>
		                          </label>
		                      </div>
		                  	</div>
			                <div class="col-sm-4 col-xs-12">
			                    <div class="form-group">
			                        <label>Address Line 1</label>
			                        {!! Form::text('address_line_1','',['class'=>"form-control required",'placeholder'=>"Address",'maxlength' => 100,'id' => 'address_line_1'])  !!}
			                    </div>
			                </div>
			                <div class="col-sm-4 col-xs-12">
			                    <div class="form-group">
			                        <label>Address Line 2</label>
			                        {!! Form::text('address_line_2','',['class'=>"form-control",'placeholder'=>"Address",'maxlength' => 80,'id'=>'address_line_2'])  !!}
			                    </div>
			                </div>
		              	</div>

		                <div class="row">
			                  <div class="col-sm-4 col-xs-12">
			                     <div class="form-group">
			                        <label class="control-label">Country</label>
			                        <select name="country" class="form-control required" id="pp_pickup_country" onchange="get_state('pp_pickup_country','pp_pickup_state','pp_pickup_city','pp_pickup_state','')">
			                            <option value="">Select Country</option>
			                            @foreach($country as $key)
			                             <option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}","state_available":"{{$key->state_available}}"}'>{{$key->Content}}</option>
			                            @endforeach
			                        </select>
			                     </div>
			                  </div>
			                  <div class="col-sm-4 col-xs-12">
			                     <div class="form-group">
			                        <label class="control-label">State</label>
			                        <select name="state" class="form-control required left-disabled" id="pp_pickup_state" onchange="get_city('pp_pickup_state','pp_pickup_city','pp_pickup_city','')">
			                            <option value="">Select State</option>
			                        </select>
			                     </div>
			                  </div>
			                  <div class="col-sm-4 col-xs-12">
			                     <div class="form-group">
			                        <label class="control-label">City</label>
			                        <select  name="city" class="form-control required" id="pp_pickup_city">
			                            <option value="">Select City</option>
			                        </select>
			                     </div>
			                  </div>
			            </div>


	                  	<div class="row">
		                  	<div class="col-sm-4 col-xs-12">
			                     <div class="form-group">
			                        <label>Zip Code/Postcode</label>
			                        {!! Form::text('zipcode','',['class'=>"form-control alpha-numeric  maxlength-8",'placeholder'=>"Zip Code/Postcode",'maxlength' => 8])  !!}
			                     </div>
		                  	</div>

		                  	<div class="col-sm-4 col-xs-12">
		                     	<div class="form-group">
		                        	<label>Pickup Date</label>

	      							<input type="text" class="form-control required usename-#date#" name="pickup_date"  id="pickup_date" placeholder="Date" readonly="readonly" value="{{ Input::old('pickup_date') }}" >

		                     	</div>
		                  	</div>
		              	</div>






		                <div class="">
		                     <hr>
		                </div>

		                <div class="form-group ">
	                        <button class="custom-btn1 btn text-center" id="step2-next-btn">
	                           Next
	                           <div class="custom-btn-h"></div>
	                        </button>
	                        <a class="custom-btn1 btn text-center" onclick="switch_request_header('#sec1','#sec2',1)" href="javascript:void(0)">
	                           Back
	                           <div class="custom-btn-h"></div>
	                        </a>
	                    </div>

	              	</div>
	             	<!-- end pickup -->

        			<!-- End edit form -->
        			{!! Form::close() !!}
        		</div>
        	</div>
      	</div>
   </div>
</div>
<style type="text/css">
    .Insurance_check{float: left; margin-top: 3px; margin-left: 5px;}

</style>

@section('script')
@parent

<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyCT0uj-bffWWdOdwDp15LvrMko-OuoZUoY"></script>

{!! Html::script('theme/web/js/utility.js') !!}
{!! Html::script('theme/web/js/validation.js') !!}
{!! Html::script('theme/web/js/send_package.js') !!}
{!! Html::style('theme/date-time-picker/jquery.datetimepicker.css') !!}
{!! Html::script('theme/date-time-picker/jquery.datetimepicker.js') !!}

@endsection

@endsection

