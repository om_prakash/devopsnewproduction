@extends('Page::layout.one-column-page')
@section('content')
<div class="container">
    <h2 class="color-blue mainHeading">Local Delivery Details <span class="pull-right">
		<a href="{{url('my-request')}}" class="btn btn-primary" style="color:#FFFFFF;">Back</a></span></h2>
    <br>
    <div class="clearfix"></div>
    <div class="white_block box-shadow p-space">
        <div class="col">
            <p><b>Package Id :&nbsp;</b>{{$request_data->PackageId}}</p>
            <p><b>Created On:&nbsp;</b> {{date('M d,Y',@$request_data->EnterOn->sec)}}</p>
        </div>
        <div class="col">
            <p><b>Total Number of items: </b>{{@$total_item}}</p>
            <p><b>Delivery Type: </b>  @if(@$request_data->JournyType == 'one_way') One Way @endif </p>
        </div>
        <div class="col">
            <p><b>Receiver Phone: </b> {{$request_data->ReceiverCountrycode}} - {{$request_data->ReceiverMobileNo}}</p>

            <!-- <a href="#"  class="anchor-blue">View More</a> -->
        </div>
    </div>
<!-- Box for multiple items -->
@foreach($request_data->ProductList as $key => $value)
    <div class="box-shadow">
      <div class="box-header">
          {{ucfirst($value['product_name'])}}
      </div>

      <div class="row p-space p-10">
          <div class="col-sm-4 col-xs-12">
              <p><b>Item Id:</b> {{ucfirst(@$value['package_id'])}}</p>
              {{-- <p><b>Shipping Mode:</b> {{ucfirst($value['travelMode'])}}</p>
              <small>(Not all items can be shipped by air, Items shipped by sea typically take longer to arrive)</small> --}}
              <p><b>Item Category: </b>{{ucfirst($value['productCategory'])}}</p>
              <p><b>Description:</b> {{ucfirst($value['Description'])}}</p>
          </div>
          <div class="col-sm-4 col-xs-12">
              <p><b>Item Status: </b> {{ucfirst($value['status'])}}</p>
              <p><b>Item Value: </b>${{number_format($value['productCost'],2)}}</p>

          </div>


          <div class="col-sm-4 col-xs-12">
          @if($value['status'] == 'delivered')
              <div class="media">
                <div class="form-group media-left">
                    <a class="fancybox" rel="group" href="http://apis.aquantuo.com/upload/profile/283441493472599.jpeg">
                    <img src="http://apis.aquantuo.com/upload/profile/283441493472599.jpeg" class="img-circle" width="100px" height="100px">
                    </a>

                </div>
                <div class="media-body">
                  <br>
                  <p><b>Requester Name: </b>Aakash Prajapathi</p>
                  <div class="col-sm-12">
                    <div class="star-ratings-sprite pull-left">
                      <span style="width:60%" class="star-ratings-sprite-rating"></span>
                    </div>
                  </div>
                </div>
              </div>
          @endif


    </div>
    <div class="clearfix"></div>
    <br>


    <div class="col-sm-12 col-xs-12">
      <div class="table-responsive">
        <table class="custom-table table table-bordered">
          <thead>
             <tr>
                <th>Image</th>
                <th>Name</th>
                <th>Dimensions</th>
                <th>Verification Code</th>
                <th>Insurance</th>
                <th>Quantity</th>
                <th>Shipping Cost</th>
                <th>Need Package Material</th>
             </tr>
          </thead>
          <tbody>
             <tr>
                <td>
                <!-- <img src="http://apis.aquantuo.com/upload/profile/283441493472599.jpeg" class="" width="60px" height="60px"> -->
                  @if($value['ProductImage'] != '')
                  <a class="fancybox" rel="group" href="{{ ImageUrl.$value['ProductImage']}}" >
                  <img src="{{ ImageUrl.$value['ProductImage']}}" class="" width="60px" height="60px" >
                  </a>
                  @else
                  <img src="{{ ImageUrl}}no-image.jpg" width="60px" height="60px"  class="img-rounded">
                  @endif
                </td>
                <td>{{ ucfirst($value['product_name']) }}</td>
                <td>
                <p>L-{{$value['productLength']}} {{$value['ProductLengthUnit']}}</p>
                <p>H-{{$value['productHeight']}} {{$value['productHeightUnit']}}</p>
                <p>W-{{$value['productWidth']}} {{$value['ProductWeightUnit']}}</p>
                <p>Weight-{{$value['productWeight']}} {{$value['productHeightUnit']}}</p>
                </td>
                <td>{{$value['DeliveryVerifyCode']}}</td>
                <td>{{ucfirst($value['InsuranceStatus'])}}</td>
                <td>{{$value['productQty']}}</td>
                <td>${{number_format($value['shippingCost'])}}</td>
                <td>{{ucfirst($value['PackageMaterial'])}}</td>
             </tr>
          </tbody>
        </table>
      </div>
    </div>

  </div>

</div>
<!-- end Box for multiple items -->
@endforeach
    <div class="row">
        <div class="col-sm-6 col-xs-12"></div>
        <div class="col-sm-6 col-xs-12">
            <div class="">
            <table class="table table-bordered">
                     <tbody bgcolor="#FFFFFF">

                      <tr>
                        <td class="">
                          <div class="col-xs-8 text-left">
                            <label class="" for="exampleInputEmail1"><b>Shipping Cost:</b> </label>
                          </div>
                          <div class="col-xs-4 text-right">  ${{ number_format($request_data->ShippingCost),2}}</div>
                          </td>
                      </tr>

                      <tr>
                        <td class="">
                          <div class="col-xs-8 text-left">
                            <label class="" for="exampleInputEmail1"><b>Insurance Cost:</b> </label>
                          </div>
                          <div class="col-xs-4 text-right">  ${{ number_format($request_data->InsuranceCost),2}}</div>
                          </td>
                      </tr>

                      <tr>
                        <td class="">
                          <div class="col-xs-8 text-left">
                            <label class="" for="exampleInputEmail1"><b>Region Cost:</b> </label>
                          </div>
                          <div class="col-xs-4 text-right">  ${{ number_format($request_data->AreaCharges),2}}</div>
                          </td>
                      </tr>

                      <tr>
                        <td class="">
                          <div class="col-xs-8 text-left">
                            <label class="" for="exampleInputEmail1"><b>After Item Review:</b> </label>
                          </div>
                          <div class="col-xs-4 text-right">  ${{ number_format($request_data->after_update_difference),2}}</div>
                          </td>
                      </tr>



                      <tr>
                        <td class="">
                          <div class="col-xs-8 text-left">
                            <label class="" for="exampleInputEmail1"><b>Discount: (-)</b> </label>
                          </div>
                          <div class="col-xs-4 text-right">
                            ${{ number_format($request_data->discount)}}
                          </div>
                        </td>
                      </tr>
                      <tr style="background:#eee;">
                        <td>
                          <div class="col-xs-8 text-left">
                            <label class="" for="exampleInputEmail1"><b>Total Cost</b> </label>
                          </div>
                          <div class="col-xs-4 text-right">
                            ${{ number_format($request_data->TotalCost + $request_data->after_update_difference),2}}
                          </div>
                        </td>
                      </tr>
                     </tbody>
                  </table>
            </div>
         {{--    <div class="text-center">
            <button class="btn btn-default blue-btn" title="Out for picup" id="b_pickup" onclick="return confirm('Are you sure you are going to pickup the package?')? buy_for_me('pickup'):'';">
               &nbsp; View Receipt &nbsp;&nbsp;</button>
            </div> --}}
            <br>
            <br>
        </div>
    </div>
</div>



@endsection
