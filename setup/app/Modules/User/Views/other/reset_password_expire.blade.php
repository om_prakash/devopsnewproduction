<!DOCTYPE Html>
<Html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Reset Password</title>

    <!-- Bootstrap -->
    {!! Html::style('theme/web/css/bootstrap.min.css') !!}

    <!-- Html5 shim and Respond.js for IE8 support of Html5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/Html5shiv/3.7.2/Html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  	<div class="container">
    
    
    <div class="col-sm-6 col-sm-offset-3">
    	<div class="form-box">
            <div class="form-head">
                {!! Html::image('theme/admin/custome/images/logo_1242.png') !!}
            </div>
            {!! Form::model('', ['method' => 'POST']) !!}
            <div class="form-body">
            	<div class="heading" style="color:red; text-align:center;">Your link has expired!</div>
            </div>
            {!! Form::close() !!}
    		</div>
    	</div>
    </div>
    <style>
    .form-box{ margin-top:20px; margin-bottom:20px; background:#fff; border-radius:5px; border:1px solid #CCC; box-shadow:0 1px 3px #c1c1c1;}
		.form-body{padding:20px;}
		.form-box h1{margin-top:0px; padding:20px;}
		.heading {
			font-size: 24px;
			padding-bottom: 20px;
			padding-top: 0;
		}
		.form-head{background:#444444;  border-radius:5px;  text-align:center;}
		.logo{padding:20px;}
    </style>
  </body>
</Html> 
