@extends('Page::layout.one-column-page')
@section('content')
{!! Html::style('theme/dropdown_checklist/jquery-ui-1.11.2.custom.css') !!}
{!! Html::script('theme/dropdown_checklist/jquery-ui-1.11.2.custom.min.js') !!}
{!! Html::script('theme/dropdown_checklist/ui.dropdownchecklist-1.5-min.js') !!}

<div class="container">
<div class="row">
	<div class="col-sm-12">
   <h2 class="color-blue mainHeading">

    @if($user_info->TransporterType == 'business')
      Post a Trip or Shipment
    @else
      Post a Trip
    @endif
    </h2>
   <br />
   </div>

      <div class="col-sm-12">
      <div class=" box-shadow">
         <p class="color-black"></p>
         <!------------------step1------------------>
         <!-- <div id="sec1" >  -->
         <div class="row">
            <br />
            <div class="col-md-10 col-sm-offset-1" id="sec1" >

			 {!! Form::model('', ['name'=>'post_trip_form','method' => 'POST','class'=>'form-vertical','id'=>'post_trip_form','url'=>'post-trip']) !!}
        <input type="hidden" name="tr" value="{{$user_info->TransporterType}}" id="tr_type">
                  <div class="col-sm-12">
					@if($user_info->TransporterType == 'business')
                     <div class="col-sm-12 ">
                        <div class="form-group clearfix">
                           <div class="my-tab clearfix">
                              <div class="sliding-div" id="ac_type_bg"></div>
                              <div class="tab1" onclick ="transporter_type('individual');hide_and_show('individual')" id="individual">INDIVIDUAL<span> TRANSPORTER</span></div>
                              <div class="tab2" onclick="transporter_type('business');hide_and_show('business')" id="business" style="color:black">BUSINESS <span> TRANSPORTER</span></div>
                              <input type="hidden" name ='transportertype' value='individual' id='transportertype' >
                           </div>
                        </div>
                     </div>

                    @endif
                     <h4 class="color-blue smallHeading">Source/Departure Address</h4>
                     <div class="form-group">
                        <label class="control-label">Address</label>
                        <input class="form-control required usename-#address#" name="address_1" placeholder="Address" maxlength="200" value="{{ Input::old('address_1') }}">
                     </div>
                  </div>
                  <div class="col-sm-3 col-xs-12">
                     <div class="form-group">
                        <label class="control-label">Country</label>

                       <select name="source_country" class="form-control required usename-#country#" id="pp_pickup_country" onchange="get_state('pp_pickup_country','pp_pickup_state','pp_pickup_city','pp_pickup_state','')" >
	                            <option value="">Select Country</option>
	                            @foreach($country as $key)
	                             <option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}","state_available":"{{$key->state_available}}"}' >{{$key->Content}}</option>
	                            @endforeach
	                        </select>
                     </div>
                  </div>
                  <div class="col-sm-3 col-xs-12">
                     <div class="form-group">
                        <label class="control-label">State</label>
                            <select name="source_state" class="form-control usename-#state# left-disabled" id="pp_pickup_state" onchange="get_city('pp_pickup_state','pp_pickup_city','pp_pickup_city','')">
	                            <option value="">Select State</option>
	                        </select>
                     </div>
                  </div>
                  <div class="col-sm-3 col-xs-12">
                     <div class="form-group">
                        <label class="control-label">City</label>
                        <select  name="source_city" class="form-control required usename-#city#" id="pp_pickup_city">
	                            <option value="">Select City</option>
	                        </select>
                     </div>
                  </div>
                  <div class="col-sm-3 col-xs-12">

          						<div class="form-group input-group has-feedback">
                          <label class="control-label">Date</label>
          							 <input type="text" class="form-control required usename-#date#" name="source_date"  id="date1" placeholder="Date" name="search_date" readonly="readonly" value="{{ Input::old('source_date') }}">

          						</div>
                  </div>
                  <div class="col-sm-3 col-xs-12">

                  </div>
                  <div class="col-sm-12">
                     <h4 class="color-blue smallHeading">Destination/Arrival Address</h4>
                     <div class="form-group">
                        <label class="control-label">Address</label>
                        <input class="form-control required usename-#address#" name="address_2" placeholder="Address" maxlength="200" value="{{ Input::old('address_2') }}">
                     </div>
                  </div>
                  <div class="col-sm-3 col-xs-12">
                     <div class="form-group">
                        <label class="control-label">Country</label>
                         <select name="destination_country" class="form-control required usename-#country#" id="pp_dropoff_country" onchange="get_state('pp_dropoff_country','pp_dropoff_state','pp_dropoff_city','pp_dropoff_state','')">
	                            <option value="">Select Country</option>
	                            @foreach($country as $key)
	                             <option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}","state_available":"{{$key->state_available}}"}'>{{$key->Content}}</option>
	                            @endforeach
	                        </select>
                     </div>
                  </div>
                  <div class="col-sm-3 col-xs-12">
                     <div class="form-group">
                        <label class="control-label">State</label>
                      <select name="destination_state" class="form-control required usename-#state# left-disabled" id="pp_dropoff_state" onchange="get_city('pp_dropoff_state','pp_dropoff_city','pp_dropoff_city','')">
	                            <option value="">Select State</option>
	                        </select>
                     </div>
                  </div>
                  <div class="col-sm-3 col-xs-12">
                     <div class="form-group">
                        <label class="control-label">City</label>
                        <select  name="destination_city" class="form-control required usename-#city#" id="pp_dropoff_city">
	                            <option value="">Select City</option>
	                        </select>
                     </div>
                  </div>
                  <div class="col-sm-3 col-xs-12">

						<div class="form-group input-group has-feedback">
              <label class="control-label">Date</label>
							<input type="text" class="form-control required usename-#date#" name="destination_date"  id="date2" placeholder="Date" readonly="readonly" value="{{ Input::old('destination_date') }}" >
              <div class="" id="er_date2"></div>
						</div>
                  </div>
                  <div class="col-sm-4">

                  </div>
                  <div class="col-sm-12">
					<div class="row">

					<div class="col-sm-6">
                     <div class="form-group">
                        <label class="control-label">Travelling Mode</label>
                        <div class="radio">
                           <label >
                           {{ Form::radio('travel_mode', 'air', true, array('onclick'=>'return select_air()','style'=>'margin-bottom:2px')) }} By Air

                          </label>
                           &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;
                           <label>
                           {{ Form::radio('travel_mode', 'ship', Input::old('travel_mode'), array('onclick'=>'return select_sea()')) }} By Sea


                           </label>
                        </div>
                     </div>
					</div>

					@if($user_info->TransporterType != 'business')
					<input type="hidden" name="travel_mode" value="air">
					@endif

                    <div class="col-sm-6">
						<div class="form-group" >
							<label class="control-label">
                Item Categories<span class="red-star"> *</span> (items you will accept)
              </label>

								<span class="" id="list1">

									<select class="form-control required usename-#category#" name="category1[]" multiple="multiple" id='category_feild' onselect="return test();" placeholder="Select Category" >
										<?php foreach ($category1 as $key) {?>
										  <option value="{{$key['Content']}}" >{{ucfirst($key['Content'])}}</option>
										<?php }?>
									</select>
                  <p class="help-block red" style="color:red" id="">
                     @if($errors->has('category1.0')) The category field is required. @endif
                  </p>
								</span>

								<div class="" id="list2" >
									<select name="category2[]" multiple="true" id='category_feild2' class="form-control required usename-#category#">
										<!-- <option value="" disabled selected>Select your option</option> -->
										<?php foreach ($category2 as $key2) {
    ?>
										<option value="{{$key2['Content']}}"   >{{ucfirst($key2['Content'])}}</option>
										<?php }?>
									</select>
                  <p class="help-block red" style="color:red">
                     @if($errors->has('category2.0')) The category field is required. @endif
                  </p>
								</div>
                <p id="checkbox_error" style="color:#a94442;"></p>
						</div>

                        </div>
                       </div>
                  </div>


                  <div class="col-sm-12" id="flight_and_airline">
                     <div class="row">
                        <div class="col-sm-6">
                           <div class="form-group" id="flight_div">
                              <label class="control-label">Flight Number</label>
                              <input class="form-control required alpha-numeric usename-#flight number#" placeholder="Flight Number " name="flight" maxlength="20" value="{{ Input::old('flight') }}">
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div class="form-group" id="airline_div">
                              <label class="control-label" >Airline</label>
                              <input class="form-control required" placeholder="Airline" name="airline" maxlength="20" value="{{ Input::old('airline') }}">
                           </div>
                        </div>
                     </div>
                  </div>

                  <div class="col-sm-12">
					 <div class="row">

						 <div class="col-sm-6">
                          <label class="control-label">Max. weight you can accept</label>
                           <input class="form-control required numeric" placeholder="Weight" name="weight" maxlength="20" value="{{ Input::old('weight') }}">
                        </div>
						<div class="col-sm-6">
							<div class="form-group">
								<label class="control-label">Measurement Units</label>

                {!! Form::select('Unit', ['' => 'Select Unit','kg' => 'Kg', 'Lbs' => 'lbs'], Input::old('Unit'), ['class' => 'form-control required usename-#unit#']) !!}

							</div>
						</div>

                    </div>
                  </div>
                  <div class="col-sm-12">

                     <div class="row">
                        <div class="col-sm-6"></div>
                        <div class="col-sm-6"></div>

                     </div>
                  </div>

                  <div class="col-sm-12">
                     <div class="form-group">
                        <label class="control-label">Comments/Description</label>
                        <textarea rows="3" class="form-control required" placeholder="Comments/Description" name="description" maxlength="300">{{ Input::old('description') }}</textarea>
                     </div>
                  </div>


                  <div class="col-md-12">
                     <hr>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group text center">
                      <button type="submit" class="custom-btn1 btn">
                         Submit
                         <div class="custom-btn-h"></div>
                      </button><div class="error-msg"></div>

                      <button type="button" class="custom-btn1 btn" onclick="return redirect('{{$user_info->TransporterType}}')">
                         Cancel
                         <div class="custom-btn-h"></div>
                      </button>
                      <div class="error-msg"></div>
                    </div>
                  </div>
                  <div class="col-md-4"></div>

               </form>
            </div>
         </div>

      </div>
   </div>
</div>
</div>


@endsection

@section('script')
@parent

{!! Html::script('theme/web/js/validation.js') !!}
{!! Html::script('theme/web/js/utility.js') !!}

{!! Html::style('theme/date-time-picker/jquery.datetimepicker.css') !!}
{!! Html::script('theme/date-time-picker/jquery.datetimepicker.js') !!}

@endsection

@section('inline-script')
@parent


<script type="text/javascript">

function redirect(type){
//alert();

if(type == 'business'){
  if($('#transportertype').val() == 'business'){
    document.location.href = SITEURL + 'transporter/bussiness-trips';

  }else{
    document.location.href = SITEURL + 'transporter/individual-trips';
  }
}else{
  document.location.href = SITEURL + 'transporter/individual-trips';
}


}


function hide_and_show(tr){
//alert('ok');
  if(tr == 'individual'){
    $('#flight_and_airline').show();
  }else{
    $('#flight_and_airline').hide();
  }

}


hide_and_show('individual');


	new Validate({
		FormName : 'post_trip_form',
		ErrorLevel : 1,
		validateHidden: false,
	});

  $('#post_trip_form').submit(function(){
        $('#checkbox_error').html('');
        if ($("input[type=checkbox]:checked").length < 1)
        {
          $('#checkbox_error').html('You must choose at least one category.');
          return false;
        }
  });

		$(document).ready(function()
    {
			 $('#flight_div').show();
			 $('#airline_div').show();
       document.getElementById("list1").style.display = "block";
			 document.getElementById("list2").style.display = "none";
    });


var newdate = new Date();
  newdate.setHours(newdate.getHours()+3);

var minDateForTwo = newdate;

var date2 = {};
    $('#date1').datetimepicker({
        format:'M d, Y h:i A',
        formatTime:'h:i A',
        timepicker:true,
        datepicker:true,
        step:30,
        minDate : new Date(),
        minTime : newdate,
        onChangeDateTime:function( ct ){
            minDateForTwo = ct;
            date2.minDate = ct;
            var dt = new Date();
            dt.setHours(23);dt.setMinutes(59);dt.setSeconds(59);

            if(ct < dt) {
              $('#date1').datetimepicker({  minTime: newdate });
              date2.minTime = ct;

              if(ct < newdate) {
                $('#date1').val(dt.dateFormat('M d, Y ')+newdate.dateFormat('h:i A'));
                date2.minTime = minDateForTwo;
              }
            } else {
              dt.setHours(0);
              $('#date1').datetimepicker({  minTime: dt });
              date2.minTime = false;
            }
            $('#date2').datetimepicker(date2);
            $('#date2').val('');
         },
   });

   $("#date2").datetimepicker({
       format:'M d, Y h:i A',
       formatTime:'h:i A',
       timepicker:true,
       datepicker:true,
       step:30,
       minDate : new Date(),
       minTime : minDateForTwo,
       closeOnDateSelect : false,
       onChangeDateTime:function( ct ){
          var date2option = {};
        if(($("#date2").val()).trim() != '') {

            $('#date1').datetimepicker({  maxDate:ct, maxTime:ct  })

            if(ct < minDateForTwo) {
              $('#date2').val(ct.dateFormat('M d, Y ')+minDateForTwo.dateFormat('h:i A'));
              date2option.minTime = minDateForTwo;
            } else {
              date2option.minTime = false;
            }
            $('#date2').datetimepicker(date2option);


          }
       },
   });




   function transporter_type(type)
   {
   	if(type == 'individual') {

   	$('#transportertype').val('individual');
		$('#ac_type_bg').css('right','50%');
		$('.tab1').css('color','white');
		$('.tab2').css('color','black');
		$("#user_type").val("individual");


   	} else {

   	$('#transportertype').val('business');
		$('#ac_type_bg').css('right','0%');
		$('.tab1').css('color','black');
		$('.tab2').css('color','white');
		$("#user_type").val("business");
   	}
   }



	function select_air()
	{
		$('#flight_div').show();
		$('#airline_div').show();
		document.getElementById("list1").style.display = "block";
		document.getElementById("list2").style.display = "none";


	}

	function select_sea()
	{
		$('#flight_div').hide();
		$('#airline_div').hide();
		document.getElementById("list2").style.display = "block";
		document.getElementById("list1").style.display = "none";


	}


	function yescheck()
	{
      	$("#category_feild").dropdownchecklist( {icon: {}, width: 450,emptyText:'Item Categories' } );
      	$("#category_feild2").dropdownchecklist( {icon: {}, width: 450,emptyText:'Item Categories' } );
	}
	yescheck();
	select_air();
	select_sea();





/*  console.log('{{str_replace('&quot;','"',Input::old('source_country'))}}');
$('#pp_pickup_country').val('{{str_replace('&quot;','"',Input::old('source_country'))}}');
$('#pp_dropoff_country').val('{{ Input::old('destination_country') }}');*/


</script>

<style>
.multiselect {
    width:20em;
    height:15em;
    border:solid 1px #c0c0c0;
    overflow:auto;
}

.multiselect label {
    display:block;
}

.multiselect-on {
    color:#ffffff;
    background-color:#000099;
}
</style>
@endsection