
@extends('Page::layout.one-column-page')
@section('content')


<div class="container">
    <div class="col-sm-12">
        <h2 class="color-blue mainHeading">Change Password</h2>
        <br />
 	</div>
	<div class="col-md-12">

        <div class="box-shadow">
         	<div class="">   
            <div class="col-md-6 col-sm-offset-1">
            	</br>
            	{!! Form::open(array('url' => 'change_password', 'class' => 'form-horizontal' )) !!}	
                  <div class="form-group">
                     <label>Current Password </label>
                     <div class="{{ $errors->has('current_password') ? ' has-error' : '' }}"><input type="password" class="form-control" id="current_password" name="current_password" value="" placeholder="Current Password"></div>
                     <p class="controll-error error-label" id="current_password1">@if ($errors->has('current_password'))<i class="fa fa-times-circle-o"></i>&nbsp;
                        {{ $errors->first('current_password') }}@endif
                     </p>
                     <div id="er_current_password" ></div>
                  </div>
                  <div class="form-group">
                     <label>New Password</label>
                     <div class="{{ $errors->has('current_password') ? ' has-error' : '' }}" ><input type="password" class="form-control" name="new_password" id="new_password" value="" placeholder="New Password"></div>
                     <p class="error-label" id="new_password1">@if ($errors->has('new_password'))<i class="fa fa-times-circle-o"></i>&nbsp;
                        {{ $errors->first('new_password') }}@endif
                     </p>
                     <div id="er_new_password"></div>
                  </div>
                  <div class="form-group">
                     <label>Confirm Password</label>
                     <div class="{{ $errors->has('current_password') ? ' has-error' : '' }}"><input type="password" class="form-control" name="confirm_password" id="confirm_password" value="" placeholder="Confirm Password"></div>
                     <p class="error-label" id="confirm_password1">@if ($errors->has('confirm_password'))<i class="fa fa-times-circle-o"></i>&nbsp;
                        {{ $errors->first('confirm_password') }}@endif
                     </p>
                     <div id="er_confirm_password"></div>
                  </div>
                  <div class="form-group" ><input type="submit" class="btn btn-primary" name="save" onclick = "return check_password()" value="Submit">
                  </div>
                    {!! Form::close() !!}
            </div>
            
           
                
            </div>
         </div>
    </div>

</div>
{!! Html::script('custom/js/validation.js') !!}   

<script type="text/javascript">
      function check_password()
   {
     var flag = true;  
     if(valid.required('current_password','current password') == false) 
     { 
       $("#current_password").parents('.form-group').addClass('has-error'); 
       flag = false;
     } 
      else
      {
        if(valid.maxlength('current_password','current password',20) == false) 
        {  $("#current_password").parents('.form-group').addClass('has-error'); 
       flag = false; 
        }
	}
  
     if(valid.required('new_password','new password') == false) 
     { 
       $("#new_password").parents('.form-group').addClass('has-error'); 
       flag = false;
     }
     else
     {
       if(valid.minlength('new_password','new password',6) == false) 
       {  $("#new_password").parents('.form-group').addClass('has-error'); 
          flag = false; } 
          else
          {
        if(valid.maxlength('new_password','new password',20) == false) 
        {  $("#new_password").parents('.form-group').addClass('has-error'); 
       flag = false; }
     }
   } 
     if(valid.required('confirm_password','confirm password') == false) 
     { 
       $("#confirm_password").parents('.form-group').addClass('has-error'); 
       flag = false;
     }
     else
     {
           if(valid.match('confirm_password','new_password','new password','confirm password') == false) {
           $("#confirm_password").parents('.form-group').addClass('has-error');       
           flag = false;
       }
    }
    return flag; 
   } 
</script>


@endsection
