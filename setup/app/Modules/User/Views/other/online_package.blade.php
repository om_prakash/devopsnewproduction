@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')
@section('content')
{!! Html::script('theme/admin/fancybox/source/jquery.fancybox.js') !!}
{!! Html::style('theme/admin/fancybox/source/jquery.fancybox.css') !!}
{!! Html::script('theme/admin/custome/js/validation.js')!!}

<div class="container-fluid">

	<div id="msg_div" class=""></div>
   <div class="panel panel-default">
      <div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;Package Detail
         <a style="margin-right:5px;" class="btn btn-primary margin-top-less-7 pull-right" href="{{url('admin/online-package')}}">Go Back</a>
      </div>
      <div class="panel-body">
         <div class="col-xs-12">
            <h3>Other Detail</h3>
            <div class="row">
               <div class="col-sm-4 custom-table1">
                 <label>Package Id:</label>
                   {{ucfirst($info->PackageNumber)}}
               </div>
               <div class="col-sm-4 custom-table1">
 				 <label>Distance: </label>
                     {{ number_format($info->distance,2) }} Miles
               </div>
               <div class="col-sm-4 custom-table1">
                 <label>Status:</label>
                 @if($info->Status == 'purchased' )
					Received
				@else
					{{ get_status_title($info->Status)['status'] }}

				@endif

               </div>
               <div class="col-sm-4 custom-table1">
                 <label>Requester Name:</label>
                    {{ucfirst($info->RequesterName)}}
               </div>

               <div class="col-sm-4 custom-table1">
				 <label>Receiver Mobile No:</label>
					{{$info->ReceiverMobileNo}}
			    </div>
			    <div class="col-sm-4 custom-table1">
				  <label>Consolidate Shipping: </label>
					@if($info->consolidate_item == 'on') Yes @else No @endif
		       </div>
               <div class="col-sm-12 custom-table1">
				 <label>Drop Address: </label>
					{{ucfirst($info->DeliveryFullAddress)}}
               </div>
               @if($info->marked_description)
               <div class="col-sm-12 custom-table1">
				 <label>Description</label>(Marked as Paid)<label>:</label>
					{{ucfirst($info->marked_description)}}
               </div>
               @endif

            </div>
         </div>


         <div class="clearfix"></div>
      </div>
      <!-- Panel Body -->
		</div>
<!--panel body product-->

<div class="panel panel-default">
	<div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;Products Detail </div>
		<div class="panel-body">
					@if(count($info->ProductList) > 0)
						@foreach($info->ProductList as $index)

					<div class="panel panel-default panel-body">
							@if($index['tpName'] != '')
							<div class="col-sm-4 custom-table1">
								 <b>Transporter Name</b>:
								 {{ucfirst($index['tpName'])}}
							</div>
							@endif

                            <div class="col-sm-4 custom-table1">
								 <label>Package Id:</label>
								 {{ucfirst($index['package_id'])}}
								 <br>

								 <label>Tracking Number: </label>
									@if(empty(trim(@$index['tracking_number'])))
										N/A
									@else
										{{ucfirst(@$index['tracking_number'])}}
									@endif
									<br>
									 {{-- <label>Description:&nbsp;</label>
									{{ ucfirst($index['description']) }} --}}
							</div>

							<div class="col-sm-4 custom-table1">
								 <label>Insurance Status</label>:
								 {{ ucfirst($index['insurance_status']) }}
								 <br>

								  <label>Status</label>:
					               {{ get_status_title($index['status'], $info->RequestType)['status'] }}
								     <br>
							</div>

							<div class="col-sm-12">
						        <label>Purchased From:&nbsp;</label>
								 {{ ucfirst($index['url']) }}
								 <br>
							</div>

							<div class="col-sm-12">
						        <label>Description:&nbsp;</label>
								 {{ ucfirst($index['description']) }}
							</div>

							@if($index['status'] == 'cancel')
								<div class="col-sm-4 custom-table1">
									<b>Reject By</b>:
									{{ucfirst(@$index['RejectBy'])}}
								</div>

								<div class="col-sm-4 custom-table1">
									<b>Reject Time</b>:
									{{@$index['RejectTime']}}
								</div>

								<div class="col-sm-4 custom-table1">
									<b>Return Type</b>:
									{{@$index['ReturnType']}}
								</div>
								<div class="col-sm-4 custom-table1">
									<b>Cancel Date</b>:
									{{@$index['CancelDate']}}
								</div>
								<div class="col-sm-4 custom-table1">
									<b>Tracking Number</b>:
									{{@$index['TrackingNumber']}}
								</div>

								<div class="col-sm-4 custom-table1">
									<b>Receipt Image</b>:
									<a href="#">{{@$index['ReceiptImage']}}</a>
								</div>


							@endif

							<div class="col-md-12">
								@if($index['status'] == 'cancel')
									<label>Transporter Message:</label>
									&nbsp;{{ucfirst(@$index['reason'])}}
								@endif
{{--
								<div>
									<label>Tracking Number:&nbsp;</label>
									&nbsp;
									@if(empty(trim(@$index['tracking_number'])))
										N/A
									@else
										{{ucfirst(@$index['tracking_number'])}}
									@endif
								</div> --}}

									{{-- <div>
										<label>Description:&nbsp;</label>
										&nbsp;{{ucfirst($index['description'])}}
									</div> --}}

									@if(@$index['status'] == 'completed')
									<div id="complet_div"><br />
										<label>Status:&nbsp;</label>
										Completed
									</div>
									@endif
									@if(@$index['status'] == 'cancel')
									<div id="reject_div"><br />
										<label>Status:&nbsp;</label>
										Cancel
									</div>
									@endif
									<div id="complet_div2{{@$index['_id']}}" style="display:none"><br />
										<label>Status:&nbsp;</label>
										Completed
									</div>
									<div id="reject_div2{{@$index['_id']}}" style="display:none"><br />
										<label>Status:&nbsp;</label>
										Rejected
									</div>
                                 <br/>
							</div>


							<div class="col-md-12">
								<table class="custom-table table table-bordered" >
								<thead>
									<tr>
										<th> Image</th>
										<th>Name</th>
										<th >Weight</th>
										<th>Height</th>
										<th >Width</th>
										<th>Length</th>
										<th>Shipping Mode</th>
										<th>Verification Code</th>
										<th>Price</th>

										<th>Qty</th>
										<th>Category</th>
									</tr>

								</thead>
									<tbody>

											<tr>
											<td><?php
$fileexists = false;
if ($index['image'] != '') {
    if (file_exists(BASEURL_FILE . $index['image'])) {
        $fileexists = true;
        ?>
	<a class="fancybox" rel="group" href="{{ ImageUrl.$index['image'] }}" >
		<img id="senior-preview" src="{{ ImageUrl.$index['image'] }}" class="margin-top" width="120px" height="100px" >
	</a><?php
}
}
if ($fileexists == false) {?>
											<img src="{{ImageUrl}}no-image.jpg" class="margin-top" width="90px" height="70px">
										<?php }?>
										</td>


											<td>
												@if(isset($index['product_name']))
												{{ucfirst($index['product_name'])}}
												@endif
											</td>
											<td>
											@if($index['weight'] != '')
												{{ucfirst($index['weight'])}}&nbsp;{{$index['weight_unit']}}
											@else
												N/A
											@endif
											</td>
											<td>
											@if($index['height'] != '')
												{{ucfirst($index['height'])}}&nbsp;{{$index['heightUnit']}}
											@else
												N/A
											@endif

											</td>
											<td>
											@if($index['width'] != '')
												&nbsp;{{$index['width']}}&nbsp;{{$index['widthUnit']}}
											@else
												N/A
											@endif

											</td>
											<td>
											@if($index['width'] != '')
												&nbsp;{{$index['length']}}&nbsp;{{$index['lengthUnit']}}
											@else
												N/A
											@endif
											</td>
											<td>
												{{ucfirst($index['travelMode'])}}
											</td>
											<td> {{$index['verify_code']}} </td>
											<td>
												${{number_format($index['price'],2)}}
											</td>

											<td>
												{{$index['qty']}}
											</td>
											<td>
												{{ucfirst($index['category'])}}
											</td>

										</tr>
										<tr><?php
$value = $index['shippingCost'] - $index['aq_fee'];
?>



										</tr>
									</tbody>
								</table>
							</div>

							@if(!@$index['TransporterFeedbcak'] == '')
									<div class="">
								      <div class="col-sm-6">
								         <div class="panel panel-default">
								            <div class="panel-heading">&nbsp;&nbsp;&nbsp;Transporter Ratting</div>
								            <div class="panel-body">
								               <div class="col-md-12 col-sm-12 custom-table1 row">

								                  	<table><tr>
														<td><label style="font-size:14px;">Ratting:&nbsp;</label></td>
															<td><div class="ratingAdjustmentcss" style="margin-bottom:10px;">
																<div class="graph" style="width:<?php echo $index['TransporterRating'] * 20; ?>%">
																	<img class="rate" src="{{ImageUrl}}/5star.png" >
																</div>
															</div></td>
															</tr></table>
													@if(!@$index['TransporterFeedbcak'] == '')
															<label>Feedback:&nbsp;</label>
															&nbsp;{{ucfirst(@$index['TransporterFeedbcak'])}}
													@endif


								               </div>

								            </div>
								         </div>
								      </div>
								   </div>
								   	@endif
								   	@if(!@$index['RequesterFeedbcak'] == '')
								  	 <div class="">
								      <div class="col-sm-6">
								         <div class="panel panel-default">
								            <div class="panel-heading">&nbsp;&nbsp;&nbsp;Requester Ratting</div>
								            <div class="panel-body">

								               <div class="col-md-12 col-sm-12 custom-table1 row">


								                  <table><tr>
														<td><label style="font-size:14px;">Ratting:&nbsp;</label></td>
															<td><div class="ratingAdjustmentcss" style="margin-bottom:10px;">
																<div class="graph" style="width:<?php echo $index['RequesterRating'] * 20; ?>%">
																	<img class="rate" src="{{ImageUrl}}/5star.png" >
																</div>
															</div></td>
													</tr></table>
													@if(!@$index['RequesterFeedbcak'] == '')
															<label>Feedback:&nbsp;</label>
															&nbsp;{{ucfirst(@$index['RequesterFeedbcak'])}}
													@endif
								               </div>
								            </div>
								         </div>
								      </div>
								   </div>
								  @endif

							<div class="col-sm-6"><br />
							</div>
							<div class="col-sm-6">
								<div class=" pull-right">
{{--
@if(@$index['status'] == 'ready')

<a style="" class="btn btn-primary" href="javascript:void(0)" onclick="return confirm('Are you sure, you want to accept this request?')? accept_package('{{@$index['_id']}}'):'';" id="acceptButton{{@$index['_id']}}">Accept</a>
@endif --}}

<?php $btn_status = 'display:none';
if ($index['status'] == 'ready') {$btn_status = 'display:inline-block';}?>

<a  type="button" class="btn btn-primary" title="Item Reviewed"   style="{{$btn_status}}" id="reviewButton{{@$index['_id']}}" onclick="return confirm('Are you sure the item has been reviewed?')? status_review('{{@$index['_id']}}'):'';" >Review</a>

<?php $btn_status = 'display:none';
if ($index['status'] == 'reviewed') {$btn_status = 'display:inline-block';}?>

<a  type="button" class="btn btn-primary" title="Item Purchased" data-toggle="modal"  style="{{$btn_status}} " id="updated-{{$index['_id']}}"
	data-target="#update_item" onclick="item_updated('{{$index['_id']}}','{{$info['_id']}}',this)" >Item Updated</a>


<?php $btn_status = 'display:none';
if ($index['status'] == 'paid') {$btn_status = 'display:inline-block';}?>

<a  type="button" class="btn btn-primary" title="Item Received"   style="{{$btn_status}} " id="purchased-{{$index['_id']}}"

onclick="return confirm('Are you sure, you have received item?')? item_purchased('{{$index['_id']}}'):'';"
	  >Item Received</a>

<?php $btn_status = 'display:none';
if ($index['status'] == 'not_purchased') {$btn_status = 'display:inline-block';}?>

<span id="waiting-for-payment-{{$index['_id']}}" style="{{$btn_status}}" class="btn" >Waiting for a requester to review</span>



<!-- <a style="display:none;" type="button" class="btn btn-primary" title="Assign Transporter" data-toggle="modal"
data-target="#exampleModal2{{@$index['_id']}}" data-whatever="@mdo" id="assignButton{{@$index['_id']}}">Assign Transporter</a> -->

{{-- <a type="button" class="btn btn-danger" title="Cancel" data-toggle="modal"
data-target="#exampleModal{{@$index['_id']}}" data-whatever="@mdo" id="cancelButton{{@$index['_id']}}" style="display:none">Cancel</a> --}}

@if(!in_array(@$index['status'],['cancel','cancel_by_admin','delivered']))
<a type="button" class="btn btn-danger" title="Cancel" data-toggle="modal"
data-target="#exampleModal{{@$index['_id']}}" data-whatever="@mdo" id="cancelButton2{{@$index['_id']}}">Cancel</a>
@endif

@if(@$index['status'] == 'purchased')


<a  type="button" class="btn btn-primary" title="Assign Transporter" data-toggle="modal"
	data-target="#exampleModal2{{@$index['_id']}}" data-whatever="@mdo" id="assignButton2{{@$index['_id']}}">Assign Transporter</a>
@endif

</div>
</div>
</div>



<!--cancel-model-->

<div class="modal fade" id="exampleModal{{@$index['_id']}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title" id="exampleModalLabel{{@$index['_id']}}">Cancel Request</h4>
	  </div>

	  {!! Form::model('', ['name' => 'cancelReason', 'id' =>$index['_id'], 'method' => 'POST']) !!}

	  <div class="modal-body">
	       <div class="form-group" id="">

            </div>
            <div class="form-group">
					{!! Form::label('reason', 'Reason:',['class' => 'control-label']) !!}

					{{ Form::textarea('cancel_reason', null, ['class' => 'form-control','id' =>"cancel_reason".$index['_id'],'size' => '3x3','placeholder'=> 'Enter cancelation reason','maxlength' =>300]) }}
					<p class="help-block red" id="er_cancel_reason{{$index['_id']}}" style="color:#a94442"></p>
					@if($errors->has('cancel_reason'))
                      <div class="error" style="color:#a94442">{{ $errors->first('cancel_reason') }}</div>
					@endif
			</div>

	  </div>

	  <div class="modal-footer">
		{!! Form::button('Close', ['class' => 'btn btn-primary', 'data-dismiss'=>'modal']) !!}
		{!! Form::submit('Submit', ['class' => 'btn btn-primary','id'=>'reason_submit'.$index['_id']]) !!}
	  </div>

		{!! Form::close() !!}

	</div>
  </div>
</div>
<script type="text/javascript">
$("#{{$index['_id']}}").submit(function()
{

  var flag = true;
  if(valid.required("cancel_reason{{$index['_id']}}",'reason') == false) { flag = false; }

	if(flag == true)
	{
		$("#reason_submit{{@$index['_id']}}").addClass('spinning');
		var reason_value =$("#cancel_reason{{$index['_id']}}").val();
		$.ajax({
		url     : '{{url("cancel-request")}}',
		type    : 'post',
		data    : "request_id={{Request::segment(4)}}"+"&product_id={{$index['_id']}}"+"&reason="+reason_value,
		success : function(res){
			$("#reason_submit{{@$index['_id']}}").removeClass('spinning');
			var obj = eval("("+res+")");

			if(obj.success == 1)
			{
				$( "#exampleModal{{@$index['_id']}}" ).trigger( "click" );
				alert('Request has been canceled successfully.');
				document.location.reload();
				$("#completeButton{{$index['_id']}}").hide();
				$("#cancelButton{{$index['_id']}}").hide();
				$("#completeButton2{{$index['_id']}}").hide();
				//$("#cancelButton2{{$index['_id']}}").hide();

				$("#reject_div2{{$index['_id']}}").show();
				$("#acceptButton{{$index['_id']}}").hide();
				if(obj.count == 0)
				{
					$("#accept_all_button").hide();
				}
				$("#{{$index['_id']}}").trigger("reset");
			}
			else
				{
					alert(obj.msg);
				}
		 }
		});
	}
  return false;
});
</script>



<!--end-cancel-model-->

<!--accept-model-->
<div class="modal fade" id="exampleModal2{{@$index['_id']}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title" id="exampleModalLabel{{@$index['_id']}}">Assign transporter</h4>
	  </div>

	  {!! Form::model('', ['name' => '', 'id' =>'accept'.$index['_id'], 'method' => 'POST']) !!}

	  <div class="modal-body">
	       <div class="form-group" id="">

            </div>
            <div class="form-group">
					{!! Form::label('assignTransporter', 'Assign transporter', ['class'=>'control-lable']) !!}

					<select class="form-control" placeholder='Search by type' id="assignTp{{$index['_id']}}" name="">
						@if(count($transporter) > 0)
								<option value="">Select transporter</option>
							@foreach($transporter as $key)

								 <option value='{"id":"{{$key->_id}}","name":"{{$key->FirstName}} {{$key->LastName}}"}'>{{ucfirst($key->FirstName)}} {{ucfirst($key->LastName)}}</option>

							@endforeach
						@endif
					</select>

					<p class="help-block red" id="er_assignTp{{$index['_id']}}" style="color:#a94442"></p>
					@if($errors->has('assignTp'))
                      <div class="error" style="color:#a94442">{{ $errors->first('assignTp') }}</div>
					@endif
			</div>

	  </div>

	  <div class="modal-footer">
		{!! Form::button('Close', ['class' => 'btn btn-primary', 'data-dismiss'=>'modal']) !!}
		<!--{!! Form::submit('Submit ghjhg', ['class' => 'btn btn-primary','id'=>'tr_submit'.$index['_id']]) !!}   -->
		<button class="btn btn-primary" id="tr_submit_{{$index['_id']}}">Submit</button>
	  </div>

		{!! Form::close() !!}

	</div>
  </div>
</div>
<script type="text/javascript">
$("#accept{{$index['_id']}}").submit(function()
{

  var flag = true;
  if(valid.required("assignTp{{$index['_id']}}",'assign transporter') == false) { flag = false; }

	if(flag == true)
	{
		var tpid = $("#assignTp{{$index['_id']}}").val();
		if(!$("#tr_submit_{{$index['_id']}}").hasClass('spinning')){


			$("#tr_submit_{{$index['_id']}}").addClass('spinning');
			$.ajax({

			url     : '{{url("assign-transporter")}}',
			type    : 'post',
			data    : "request_id={{Request::segment(4)}}"+"&product_id={{$index['_id']}}"+"&tpid="+tpid,
			success : function(res){
			 $("#tr_submit_{{$index['_id']}}").removeClass('spinning');

					var obj = eval("("+res+")");

			if(obj.success == 1)
			{
				$( "#exampleModal2{{@$index['_id']}}" ).trigger( "click" );
				alert('You have assigned transporter successfully.');

				$("#assignButton2{{@$index['_id']}}").hide();
				$("#assignButton{{$index['_id']}}").hide();
				$("#pickupButton{{$index['_id']}}").show();
				if(obj.count == 0)
					{
						$("#accept_all_button").hide();
					}
				$("#accept{{$index['_id']}}").trigger("reset");
				location.reload();
			}
			else
				{
					alert(obj.msg);
				}
			 }
			});
		}
	}
  return false;
});

</script>
<!--end-accept-model-->
				@endforeach
						<div class="col-sm-12 custom-table1"><hr>
							<div class="row">

										<?php
$total_aq_fee = 0;
$total_shippingCost = 0;
foreach ($info->ProductList as $key => $value) {
    $total_aq_fee += @$value['aq_fee'];
    $total_shippingCost += @$value['shippingCost'];
}
$value = $total_shippingCost - $total_aq_fee;

?>

									 <div class="col-sm-12">
                                  <div class="col-sm-4">
                   <h3><b>Payment Information</b></h3>
                     <table class="table table-bordered">
  					<tbody bgcolor="#F1F1F1">

	                <tr>
	                   <td class="">
	                      <div class="col-xs-8 text-left">
	                        <label class="" for="exampleInputEmail1"><b>Shipping Cost:</b> </label>
	                      </div>
	                      <div class="col-xs-4 text-right">
	                        ${{number_format($info->shippingCost,2)}}
	                      </div>
	                    </td>
	               </tr>
			    <!--   <tr>
                      <td class="">
                        <div class="col-xs-8 text-left">
                           <label class="" for="exampleInputEmail1"><b>Item(s) Cost:</b> </label>
                        </div>
                          <div class="col-xs-4 text-right">
                            (+)${{number_format($info->total_item_price,2)}}
                          </div>
                       </td>
                    </tr>   -->

                  @if($info->insurance > 0)
                  <tr>
                    <td class="">
                      <div class="col-xs-8 text-left">
                        <label class="" for="exampleInputEmail1"><b>Insurance:</b> </label>
                      </div>
                      <div class="col-xs-4 text-right">
                          (+)${{number_format($info->insurance,2)}}
                      </div>
                    </td>
                  </tr>
                  @endif
                  @if($info->discount > 0)
                  <tr>
                    <td class="">
                        <div class="col-xs-8 text-left">
                          <label class="" for="exampleInputEmail1"><b>Aquantuo Discount:</b>
                          </label><br/>
                              @if(!$info->PromoCode == '')
                                 <small>(<b>Promocode:</b>{{$info->PromoCode }}) </small>
                                 @endif
                        </div>
                        <div class="col-xs-4 text-right">
                            (-)${{number_format($info->discount,2)}}
                        </div>
                    </td>
                  </tr>
                  @endif

                    <tr>
					 <td class="">
						<div class="col-xs-8 text-left">
							<label class="" for="exampleInputEmail1"><b>Total Price:</b> </label>
						</div>
							<div class="col-xs-4 text-right">
    							${{ number_format($info->TotalCost,2)}}
							</div>
					    </td>
					</tr>
                   </tbody>
				</table>

                                   </div>
                                   <div class="col-sm-4">

                                   </div>
                                   <div class="col-sm-4 text-right">
                                    <h3><b>Fee Detalis</b></h3>
                                   <table class="table table-bordered">
  					<tbody bgcolor="#F1F1F1">
               <!--     -->
	                <tr>
	                   <td class="">
	                      <div class="col-xs-8 text-left">
	                        <label class="" for="exampleInputEmail1"><b>Shipping Cost:</b> </label>
	                      </div>
	                      <div class="col-xs-4 text-right">
	                        ${{number_format($total_shippingCost,2)}}
	                      </div>
	                    </td>
	               </tr>
			       <tr>
                      <td class="">
                        <div class="col-xs-8 text-left">
                           <label class="" for="exampleInputEmail1"><b>Aquantuo Fee:</b> </label>
                        </div>
                          <div class="col-xs-4 text-right">
                           (-)${{number_format($total_aq_fee,2)}}
                          </div>
                       </td>
                    </tr>


                  <tr>
                    <td class="">
                      <div class="col-xs-8 text-left">
                        <label class="" for="exampleInputEmail1"><b>Transporter Earning:</b> </label>
                      </div>
                      <div class="col-xs-4 text-right">
                         ${{number_format($value,2)}}
                      </div>
                    </td>
                  </tr>


                   </tbody>
				</table>

				    </div>
				</div>


				      		</div>
				     	</div>


				<div class="col-sm-12 custom-table1">
					<div class="text-right">
							<hr>
						@if($accept_count > 0)
						<a  style="" class="btn btn-primary text-right" href="{{url('accept-all',[$info->_id])}}"  id="accept_all_button" onclick="return confirm('Are you sure all the items have been reviewed?')">Review All</a>
						@endif

						<?php $btn_status = 'display:none';
if ($info->Status == 'not_purchased') {$btn_status = 'display:inline-block';}?>
						<span id="reminder_button" style="{{$btn_status}}"  >
	     					<a class="btn btn-primary" title="Send payment reminder"
							onclick="reminder('payment_reminder','{{@$info->_id}}',this,'Are you sure, you want to send reminder.');" >Reminder for Payment</a>&nbsp;&nbsp;
						</span>
					</div>
				</div>

			@endif

			<div class="clearfix"></div>

	</div>

	</div>

<!--end-->


   <?php if (@$info->Penalty > 0): ?>
   <div class="row">
      	<div class="col-sm-12">
         	<div class="panel panel-default">
            	<div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;Penalty</div>
            		<div class="panel-body">
              			<div class="col-md-6 col-sm-6 custom-table1 row">
                  			<div class="row">
                     			<div class="col-md-6 col-sm-6"><b>Penalty Status</b> :</div>
                     			<div class="col-md-4 col-sm-4"> {{ucfirst($info->PenaltyStatus)}}</div>
                  			</div>
               			</div>
		               <div class="col-md-6 col-sm-6 custom-table1 row">
		                  <div class="row">
		                     <div class="col-md-6 col-sm-6 text-right"><b>Penalty Status</b> :</div>
		                     <div class="col-md-4 col-sm-4 text-right"> ${{number_format(($info->Penalty/100),2)}}</div>
		                  </div>
		               </div>
            		</div>
         		</div>
      		</div>
   		</div>
   <?php endif;?>
   <div class="row">

      <?php if ($info['ProductImage'] != "") {
    ?>
      <div class="col-sm-12">
         <div class="panel panel-default">
            <div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;Package Image</div>
            <div class="panel-body">
               <?php

    if ($info['ProductImage'] != '') {
        if (file_exists(BASEURL_FILE . $info['ProductImage'])) {?>
               <div class="col-md-2">
                  <a class="fancybox" href="{{ImageUrl}}{{ $info->ProductImage}}" data-fancybox-group="gallery" title="Product Image">
                  <img src="<?php echo ImageUrl . $info['ProductImage']; ?>" tag="product image" style="width:178px; height:178px;"/></a>
               </div>
               <?php }
    }
    if (is_array($info["OtherImage"])) {

        foreach ($info['OtherImage'] as $key) {
            if ($key != '') {
                if (file_exists(BASEURL_FILE . $key)) {?>
               <div class="col-md-2">
                  <a class="fancybox" href="{{ImageUrl}}{{$key}}" data-fancybox-group="gallery" title="Product Image">
                  <img src="<?php echo ImageUrl . $key; ?>" tag="product image" style="width:178px; height:178px;"/></a>
               </div>
               <?php }
            }
        }

    }?>
               <div class="clearfix"></div>
            </div>
            <!-- Panel Body -->
         </div>
      </div>
      <?php }?>
   </div>

   <div class="">
      <div class="panel panel-default">

      </div>
   </div>

   <div class="modal fade pop-up-map" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel-1" aria-hidden="true">
      <div class="modal-dialog modal-lg">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
               <div id="map1"></div>
               <p id="error"></p>
            </div>
         </div>
         <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
   </div>
   <!-- /.modal mixer image -->
</div>




<!-- Modal -->
<div id="edit_item" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" id="edit_item_close">&times;</button>
        <h4 class="modal-title">Edit Item</h4>
      </div>
      <form id="item_purchased_form">
      	<div class="modal-body">
	        <div class="row">
	        	<div class="col-md-12">
	        		<div class="form-group">
					    <label for="item_name">Item Name:</label>
					    <input type="text" class="form-control" id="item_name" name="item_name">
					    <div class="red" id="er_item_name"></div>
					</div>
	        	</div>
	        </div>
	        <div class="row">
	        	<div class="col-md-6">
	        		<div class="form-group">
					    <label for="item_name">Purchased From:</label>
					    <select class="form-control" id="purchase_from" name="purchase_from">
					    	<option value="">Select Market</option>
					    	<option value='Amazon'>Amazon</option>
					    	<option value='eBay'>eBay</option>
					    	<option value='Walmart'>Walmart</option>
					    	<option value='FlipKart'>FlipKart</option>
					    	<option value='Snapdeal'>Snapdeal</option>


					    </select>
					    <div class="red" id="er_purchase_from"></div>
					</div>
	        	</div>

	        	<div class="col-md-6">
	        		<div class="form-group">
					    <label for="item_name">Shipping:
					    	<small>(Retailer to Aquantuo's facility)</small></label>
					    <input type="text" class="form-control" id="shipping_cost_by_user" name="shipping_cost_by_user" placeholder="Shipping">
					    <div class="red" id="er_shipping_cost_by_user"></div>
					</div>
	        	</div>
	        	<div class="col-md-6">
	        		<div class="form-group">
					    <label for="item_name">Item Price:</label>
					    <input type="text" class="form-control" id="item_price" name="item_price">
					    <div class="red" id="er_item_price"></div>
					</div>
	        	</div>
	        </div>
	        <div class="row">
	        	<div class="col-md-12">
	        		<h4>Measurement Units</h4>
	        		<div class="clearfix"></div>
	        		<input type="radio" name="measurement_units" id="measurement_units_cm_kg" value="cm_kg" checked>&nbsp;Metric(cm/kg)&nbsp;&nbsp;
	        		<input type="radio" name="measurement_units" id="measurement_units_inches_lbs" value="inches_lbs">&nbsp;Imperial(inches/lbs)
	        	</div>
	        	<div class="col-md-12"><h4>Item Specification</h4></div>
	        	<div class="col-md-6">
	        		<div class="clearfix"></div>
	        		<div class="form-group">
					    <label for="item_name">Length:</label>
					    <input type="text" class="form-control" id="item_length" name="item_length">
					    <div class="red" id="er_item_length"></div>
					</div>
	        	</div>
	        	<div class="col-md-6">
	        		<div class="form-group">
					    <label for="item_name">Width:</label>
					    <input type="text" class="form-control" id="item_width" name="item_width">
					    <div class="red" id="er_item_width"></div>
					</div>
	        	</div>
	        </div>
	        <div class="row">
	        	<div class="col-md-6">
	        		<div class="form-group">
					    <label for="item_name">Height:</label>
					    <input type="text" class="form-control" id="item_height" name="item_height">
					    <div class="red" id="er_item_height"></div>
					</div>
	        	</div>
	        	<div class="col-md-6">
	        		<div class="form-group">
					    <label for="item_name">Weight:</label>
					    <input type="text" class="form-control" id="item_weight" name="item_weight">
					    <div class="red" id="er_item_weight"></div>
					</div>
	        	</div>
	        </div>
	    	<input type="hidden" name="itemid" id="itemid" >
	    	<input type="hidden" name="requestid" id="requestid" >
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" id="item_sub_button">Submit</button>
      </div>
      </form>
    </div>

  </div>
</div>






<!-- Modal -->
<div id="update_item" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" id="edit_item_close1">&times;</button>
        <h4 class="modal-title">Update Item</h4>
      </div>
      <form id="item_updated_form">
      	<div class="modal-body">
	        <div class="row">
	        	<div class="col-md-12">
	        		<div class="form-group">
					    <label for="item_name">Item Name:</label>
					    <input type="text" class="form-control" id="item_name1" name="item_name">
					    <div class="red" id="er_item_name1"></div>
					</div>
	        	</div>
	        </div>
	        <div class="row">
	        	<div class="col-md-6">
	        		<div class="form-group">
					    <label for="item_name">Purchased From:</label>
					    <select class="form-control" id="purchase_from1" name="purchase_from">
					    	<option value="">Select Market</option>
					    	<option value='Amazon'>Amazon</option>
					    	<option value='eBay'>eBay</option>
					    	<option value='Walmart'>Walmart</option>
					    	<option value='FlipKart'>FlipKart</option>
					    	<option value='Snapdeal'>Snapdeal</option>


					    </select>
					    <div class="red" id="er_purchase_from1"></div>
					</div>
	        	</div>

	        	<div class="col-md-6">
	        		<div class="form-group">
					    <label for="item_name">Item Price:</label>
					    <input type="text" class="form-control" id="item_price1" name="item_price">
					    <div class="red" id="er_item_price1"></div>
					</div>
	        	</div>
	        </div>
	        <div class="row">
	        	<div class="col-md-12">
	        		<h4>Measurement Units</h4>
	        		<div class="clearfix"></div>
	        		<input type="radio" name="measurement_units" id="measurement_units_cm_kg1" value="cm_kg" checked>&nbsp;Metric(cm/kg)&nbsp;&nbsp;
	        		<input type="radio" name="measurement_units" id="measurement_units_inches_lbs1" value="inches_lbs">&nbsp;Imperial(inches/lbs)
	        	</div>
	        	<div class="col-md-12"><h4>Item Specification</h4></div>
	        	<div class="col-md-6">
	        		<div class="clearfix"></div>
	        		<div class="form-group">
					    <label for="item_name">Length:</label>
					    <input type="text" class="form-control" id="item_length1" name="item_length">
					    <div class="red" id="er_item_length1"></div>
					</div>
	        	</div>
	        	<div class="col-md-6">
	        		<div class="form-group">
					    <label for="item_name">Width:</label>
					    <input type="text" class="form-control" id="item_width1" name="item_width">
					    <div class="red" id="er_item_width1"></div>
					</div>
	        	</div>
	        </div>
	        <div class="row">
	        	<div class="col-md-6">
	        		<div class="form-group">
					    <label for="item_name">Height:</label>
					    <input type="text" class="form-control" id="item_height1" name="item_height">
					    <div class="red" id="er_item_height1"></div>
					</div>
	        	</div>
	        	<div class="col-md-6">
	        		<div class="form-group">
					    <label for="item_name">Weight:</label>
					    <input type="text" class="form-control" id="item_weight1" name="item_weight">
					    <div class="red" id="er_item_weight1"></div>
					</div>
	        	</div>
	        </div>
	    	<input type="hidden" name="itemid" id="itemid1" >
	    	<input type="hidden" name="requestid" id="requestid1" >
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" id="item_update_button">Submit</button>
      </div>
      </form>
    </div>

  </div>
</div>







<?php
function get_feet($InFeet, $unit)
{
    $InFeet = (double) $InFeet;
    $unit = strtolower($unit);
    if ($unit == 'inch') {
        $InFeet = $InFeet * 0.0833333;
    } else if ($unit == 'meter') {
        $InFeet = $InFeet * 3.28084;
    } else if ($unit == 'centimeter' || $unit == 'centimetre') {
        $InFeet = $InFeet * 0.0328084;
    }
    return $InFeet;
}
?>
<script type="text/javascript">
   $(document).ready(function() {
   	$(".fancybox").fancybox();
   });


	function accept_package(prid)
	{
		$("#acceptButton"+prid).addClass('spinning');

		$.ajax({
		url     : '{{url("admin/accept-package")}}',
		type    : 'post',
		data    : "request_id={{Request::segment(4)}}"+"&product_id="+prid,
		success : function(res){

		$("#acceptButton"+prid).removeClass('spinning');

		var obj = eval("("+res+")");

		if(obj.success == 1)
		{
			alert('Request has been accepted successfully.');

			$("#acceptButton"+prid).hide();
			//$("#cancelButton2"+prid).hide();

			//$("#assignButton"+prid).show();
			$("#reviewButton"+prid).show();
			if(obj.count == 0)
				{
					$("#accept_all_button").hide();
				}
			//$("#assignButton"+prid).trigger( "click" ) ;
			$("#assignButton2"+prid).trigger( "click" ) ;


		}
		else
			{
				alert(obj.msg);
			}
		}
		});
	}

	function status_review(prid)
	{
		if(!$("#reviewButton"+prid).hasClass( "spinning" )){
			$("#reviewButton"+prid).addClass('spinning');
			$.ajax({
				url     : '{{url("admin/online-package-review")}}',
				type    : 'post',
				data    : "request_id={{Request::segment(4)}}"+"&product_id="+prid,
				success : function(res){

				$("#reviewButton"+prid).removeClass('spinning');

				var obj = eval("("+res+")");

				if(obj.success == 1)
				{
					alert('Request has been reviewed successfully.');
					location.reload();
					$("#reviewButton"+prid).hide();
					$("#updated-"+prid).show();

				}
				else
					{
						alert(obj.msg);
					}
				}
			});
		}
	}



	function pickup(prid)
	{
		$("#pickupButton"+prid).addClass('spinning');
		$.ajax({
		url     : '{{url("pickup")}}',
		type    : 'post',
		data    : "request_id={{Request::segment(4)}}"+"&product_id="+prid,
		success : function(res){
			$("#pickupButton").removeClass('spinning');
			var obj = eval("("+res+")");
			if(obj.success == 1)
			{
				alert('Request has been pickedup successfully.');
				$("#pickupButton"+prid).hide();
				$("#pickupButton2"+prid).hide();
				$("#deliveryButton"+prid).show();
			}
			else
				{
					alert(obj.msg);
				}


			 }
			});
	}

	function delivery(prid)
	{
		$("#deliveryButton"+prid).addClass('spinning');
		$.ajax({
		url     : '{{url("request-delivery")}}',
		type    : 'post',
		data    : "request_id={{Request::segment(4)}}"+"&product_id="+prid,
		success : function(res){
			$("#deliveryButton").removeClass('spinning');
			var obj = eval("("+res+")");
			if(obj.success == 1)
			{
				alert('Request has been delivered successfully.');
				$("#deliveryButton"+prid).hide();
				$("#deliveryButton2"+prid).hide();
				$("#completeButton"+prid).show();
				$("#cancelButton"+prid).show();
			}
			else
				{
					alert(obj.msg);
				}


			 }
			});
	}

	function complete(prid)
	{
		$("#completeButton").addClass('spinning');
		$.ajax({
		url     : '{{url("request-complete")}}',
		type    : 'post',
		data    : "request_id={{Request::segment(4)}}"+"&product_id="+prid,
		success : function(res){
			$("#completeButton").removeClass('spinning');
			var obj = eval("("+res+")");
			if(obj.success == 1)
			{
				alert('Request has been completed successfully.');
				$("#completeButton"+prid).hide();
				$("#cancelButton"+prid).hide();
				$("#completeButton2"+prid).hide();
				//$("#cancelButton2"+prid).hide();
				$("#complet_div2"+prid).show();
			}
			else
				{
					alert(obj.msg);
				}


			 }
			});
	}

function item_purchased(prid)
{
	if(!$("#purchased-"+prid).hasClass('spinning')){


		$("#purchased-"+prid).addClass('spinning');
			$.ajax({
			url     : '{{url("admin/online-package-purchase")}}',
			type    : 'post',
			data    : "request_id={{Request::segment(4)}}"+"&product_id="+prid,
			success : function(res){

			$("#purchased-"+prid).removeClass('spinning');
			var obj = eval("("+res+")");
			if(obj.success == 1)
			{
				$("#purchased-"+prid).hide();
				$("#assignButton"+prid).show();
				alert(obj.msg);
				location.reload();
			}
			alert(obj.msg);
			}
		});
	}
}


function item_updated(itemid,requestid,obj)
{
	$('#itemid1').val(itemid);
	$('#requestid1').val(requestid);
	$(obj).addClass('spinning');
	$.ajax({
		url     : '{{url("admin/get_online_item_info")}}',
		type    : 'post',
		data    : {itemid:itemid,requestid:requestid},
		dataType: 'json',
		success : function(res)
		{
			$(obj).removeClass('spinning');
			if(res.success == 1)
			{
				$('#item_name1').val(res.ProductList.product_name);
				if(res.ProductList.marketname != '') {
					$('#purchase_from1').val(res.ProductList.marketname);
				}
				$('#shipping_cost_by_user1').val(res.ProductList.shipping_cost_by_user);
				$('#item_price1').val(res.ProductList.price);
				$('#item_length1').val(res.ProductList.length);
				$('#item_width1').val(res.ProductList.width);
				$('#item_height1').val(res.ProductList.height);
				$('#item_weight1').val(res.ProductList.weight);
				if(res.ProductList.heightUnit == 'cm') {
					$('#measurement_units_cm_kg1').attr('checked','checked');
				} else {
					$('#measurement_units_inches_lbs1').attr('checked','checked');
				}
			}
			else
			{
				alert(res.msg);
			}
		}
	});
}

$('#item_updated_form').submit(function()
{
	var flag = true;

	if(valid.required('item_name1','item name') == false) { flag = false; }
	if(valid.required('item_price1','item price') == false) { flag = false; } else {
		if(valid.float('item_price1','item price') == false) { flag = false; }
	}
	if(valid.required('item_length1','item length') == false) { flag = false; } else {
		if(valid.float('item_length1','item length') == false) { flag = false; }
	}
	if(valid.required('item_width1','item width') == false) { flag = false; } else {
		if(valid.float('item_width1','item width') == false) { flag = false; }
	}
	if(valid.required('item_height1','item height') == false) { flag = false; } else {
		if(valid.float('item_height1','item height') == false) { flag = false; }
	}
	if(valid.required('item_weight1','item weight') == false) { flag = false; } else {
		if(valid.float('item_weight1','item weight') == false) { flag = false; }
	}

	if(flag)
	{
		var id=$('#itemid1').val();
		if(!$("#item_update_button").hasClass( "spinning" )){
			$('#item_update_button').addClass('spinning');
			$.ajax({
				url     : '{{url("admin/update_online_item_info")}}',
				type    : 'post',
				data    : $('#item_updated_form').serialize(),
				dataType: 'json',
				success : function(res)
				{
					console.log(res);
					$('#item_update_button').removeClass('spinning');
					if(res.success == 1)
					{
						$('#item_updated_form')[0].reset();
						$('#edit_item_close1').trigger('click');
						$('#updated-'+id).hide();
						$('#not_purchased-'+id).show();
						if(res.reminder_button == true) {
							$('#reminder_button').show();
							$('#waiting-for-payment-'+id).show();
						}
						if(res.item_purchase_bt == true) {
							$('#purchased-'+id).show();
						}
						alert(res.msg);
						location.reload();
					}
					alert(res.msg);
				}
			});
		}
	}
	return false;
});

function reminder()
{
	$('#reminder_button').addClass('spinning');
	$.ajax({
		url     : '{{url("admin/online-payment-reminder")}}',
		type    : 'post',
		data    :  "requestid={{Request::segment(4)}}",
		dataType: 'json',
		success : function(res)
		{
			console.log(res);
			$('#reminder_button').removeClass('spinning');
			if(res.success == 1)
			{
				alert(res.msg);
			}

		}
	});
}

</script>
@include('Admin::layouts.footer')
@stop
<style>
   .modal-header .close {
   margin-top: -9px !important;
   }

   .custom-table.table th {
    border-bottom: medium none !important;
    vertical-align: middle;
    height: 60px!important;
    background: #F1F1F1;
    /* text-align:center;*/
}
.custom-table.table > tbody td {
    /*text-align:center !important;*/

    vertical-align: middle;
}

.table-bordered td,.table-bordered th{border:1px solid #ddd!important}
</style>