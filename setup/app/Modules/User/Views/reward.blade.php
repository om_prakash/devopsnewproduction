@extends('Page::layout.one-column-page')
@section('content')
<div class="container">
<div class="row">
    <div class="col-sm-12">

        </div>
        <div class="col-md-9 col-sm-12" align="center">
         <div class="col-sm-2">
         </div>
    <div class="col-md-12 col-sm-12" >
    <div class=" box-shadow">
    <div class="row rewards">
    <div class="gift_wrapper"><img src="{{asset('theme/web/promo/images/ic_reward.png')}}" align="center" width="75px" height="100px"></div>
    <div class="blue-bg col-sm-12 top-box">

    </div>
    <div class="col-sm-12 btm-box text-center">
        <h3>Your Reward Code</h3>
        <h1 class="color-blue">
             @if(count($info) > 0)
                {{$info->Code}}
             @else
                <p class="text-primary">Your Promocode is empty</p>
             @endif

        </h1>
        <br />
        <p>If present, use the above reward code at the time of payment.</p>
    </div>
    </div>
    </div>
    </div>
    </div>
    @include('Page::layout.side_bar')
</div>
@endsection
