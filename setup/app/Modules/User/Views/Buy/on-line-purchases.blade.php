@extends('Page::layout.one-column-page')
@section('page_title')
Online Purchases - International Shipping -  Aquantuo
@endsection
@section('content')
<?php
$ua = strtolower($_SERVER['HTTP_USER_AGENT']);
// you can add different browsers with the same way ..
if (preg_match('/(chromium)[ \/]([\w.]+)/', $ua)) {
	$browser = 'chromium';
} elseif (preg_match('/(chrome)[ \/]([\w.]+)/', $ua)) {
	$browser = 'chrome';
} elseif (preg_match('/(safari)[ \/]([\w.]+)/', $ua)) {
	$browser = 'safari';
} elseif (preg_match('/(opera)[ \/]([\w.]+)/', $ua)) {
	$browser = 'opera';
} elseif (preg_match('/(msie)[ \/]([\w.]+)/', $ua)) {
	$browser = 'msie';
} elseif (preg_match('/(mozilla)[ \/]([\w.]+)/', $ua)) {
	$browser = 'mozilla';
}

preg_match('/(' . $browser . ')[ \/]([\w]+)/', $ua, $version);
?>
<div class="modal fade" id="item_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title color-white" id="myModalLabel">&nbsp;&nbsp;Edit Item</h4>
         </div>
         <div class="clearfix"></div>
         {!! Form::model('', ['name' => 'edit_item', 'id' =>'edit_item', 'method' => 'post']) !!}
         <div class="modal-body">
            <div class="row">
               <div class="col-sm-6 col-xs-12">
                  <div class="form-group">
                     <label class="control-label">Item Name</label>
                     <input type="hidden" value="" name="item_id" id="item_id">
                     <input class="form-control required"   placeholder="Item Name"  name="item_name" id="item_name" maxlength="110" >
                  </div>
               </div>

               <div class="col-sm-6 col-xs-12">
                  <div class="form-group">
                     <label class="control-label">Purchased From</label>
                     <input type="text" class="form-control required username-#purchase from#"  name="purchase_from"  id="add_item_url" placeholder="Purchased From" maxlength="700">
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-sm-6 col-xs-12">
                  <div class="form-group">
                     <label class="control-label">Item Cost<span class="red-star"> *</span></label>
                     <div class="input-group error-input">
                      <span class="input-group-addon">$</span>
                     <input class="form-control required float maxlength-9 " placeholder="Item Cost" name="item_price" id="item_price" maxlength="9">
                     </div>
                  </div>
               </div>
               <div class="col-sm-6 col-xs-12">
                  <div class="form-group">
                     <label class="control-label">Quantity</label>
                     <div style="width:160px;" class="input-group number-spinner">
                        <span class="input-group-btn data-dwn">
                        <button class="btn btn-primary" data-dir="dwn" onclick="return false;">
                        <span class="glyphicon glyphicon-minus"></span>
                        </button>
                        </span>
                        <input type="text" class="form-control text-center" value="1" min="1" max="40"  id="quentity" name="quentity" >
                        <input type="hidden" value="">
                        <span class="input-group-btn data-up">
                        <button class="btn btn-primary" data-dir="up" onclick="return false;">
                        <span class="glyphicon glyphicon-plus"></span>
                        </button>
                        </span>
                     </div>
                  </div>
               </div>
            </div>

            <div class="col-sm-12 col-xs-12 row">
               <div class="form-group">

                  <div class="">
                    <span class="pull-left">

                    <input name="buyforme_dimensions" id="buyforme_dimensions" value="buy_for_me_dimensions"  onclick="make_dimension_optional('#buyforme_dimensions')" type="checkbox">
                    </span>
                    <span class="Insurance_check">
                    <p>
                     &nbsp;&nbsp;I know item dimensions and weight
                     </p>
                     </span>

                  </div>
               </div>
            </div>

            <div id="buyformedimensions">
            <div class="row">
               <div class="col-sm-12 col-xs-12">
                  <div class="form-group">
                     <label class="control-label">Measurement Units</label>
                     <div class="radio">
                        <label class="">
                        <input type="radio" name="measurement_unit" id="measurement_unit" value="cm_kg" onclick="unit_show('cm_kg');">
                        </label>&nbsp;Metric (Cm/Kg)
                        <label class="">
                        <input type="radio" name="measurement_unit" id="measurement_unit2" checked value="inches_lbs" onclick="unit_show('inches_lbs');">
                        </label>&nbsp;Imperial (Inches/Lbs)
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-sm-12 col-xs-12 row">
               <label class="control-label">Item Specification</label>
            </div>
            <div class="row">
               <div class="col-sm-3 col-xs-12">
                  <div class="form-group">
                     <label class="control-label">Length</label>
                       <div class="input-group error-cm-input">
                     <input class="form-control float maxlength-9" placeholder="Length"  id="length" name="length" maxlength="9" />
                   <span class="input-group-addon" id="length_unit">Cm</span>
                   </div>
                  </div>
               </div>
               <div class="col-sm-3 col-xs-12">
                  <div class="form-group">
                     <label class="control-label">Width</label>
                       <div class="input-group error-cm-input">
                     <input class="form-control float maxlength-9" placeholder="Width" name="width"  id="width" maxlength="9">
                   <span class="input-group-addon" id="width_unit">Cm</span>
                   </div>
                  </div>
               </div>
               <div class="col-sm-3 col-xs-12">
                  <div class="form-group">
                     <label class="control-label">Height</label>
                       <div class="input-group error-cm-input">
                     <input class="form-control float maxlength-9" placeholder="Height" name="height" id="height" maxlength="9">
                  <span class="input-group-addon" id="height_unit">Cm</span>
                   </div>
                  </div>
               </div>
               <div class="col-sm-3 col-xs-12">
                  <div class="form-group">
                     <label class="control-label">Weight
                      <a data-toggle="popover" data-trigger="focus"  data-container="body" data-placement="top" type="button" data-html="true" href="javascript:void(0)" id="login1" class="info_icon" style="margin-top:0;color:black;"><img src="theme/web/images/info_icon.png" width="20px">
                    </a>
                     </label>
                       <div class="input-group error-cm-input">
                     <input class="form-control required float maxlength-9" placeholder="Weight" name="weight" id="weight" value="{{ (!empty(session()->get('weight'))) ? session()->get('weight') : '' }}" maxlength="9">
                     <span class="input-group-addon" id="weight_unit">Kg</span>
                   </div>
                  </div>
               </div>
            </div>
            </div>

            <div class="row">
               <div class="col-sm-6 col-xs-12">
                  <div class="form-group">
                     <label class="control-label"> Shipping Mode </label>&nbsp;&nbsp;(Air: receive package within 5 to 10 business days. Sea: receive package within 6 to 8 weeks.)
                     <div class="radio">
                        <label class="">
                        <input type="radio" name="travel_mode" id="travel_mode_air" value="air" onclick="toggle_category('.travel-mode-air','.travel-mode-ship','#package_category')" checked="checked">
                        </label>&nbsp;By Air
                        <label class="">
                        <input type="radio" name="travel_mode" id="travel_mode_ship" value="ship" onclick="toggle_category('.travel-mode-ship','.travel-mode-air','#package_category')">
                        </label>&nbsp;By Sea
                     </div>
                  </div>
               </div>
                <div class="col-sm-6 col-xs-12">
                  <div class="form-group">
                     <label class="control-label">Insurance</label>
                     <div class="radio">
                        <label>
                        <input type="radio" name="insurance" id="insurance" value="yes"  checked>
                        </label>&nbsp; Yes
                        &nbsp; &nbsp;
                        <label>
                        <input type="radio" name="insurance"  id="insurance2" value="no" data-toggle="modal" data-target="#insurance_change" >
                        </label> &nbsp;<span style="">No</span>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-sm-6 col-xs-12">
               <div class="row">
               <div class="col-sm-12 col-xs-12">
                  <div class="form-group">
                     <label class="control-label">Select Package Category</label>
                     <select class="form-control required" id="package_category" name="category">
                        <option value="" >Select Category</option>
                        <?php
$sessionCategory = json_decode(session()->get('category'));
foreach ($category as $key) {?>
                        <option {{ (@$sessionCategory->id == $key->_id) ? 'selected' : '' }}  value='{"id":"{{$key->_id}}"}' class="travel-mode-{{$key->TravelMode}}" >{{$key->Content}}</option>
                        <?php }?>
                     </select>
                  </div>
               </div>

               <div class="col-sm-12 col-xs-12">
                  <div class="form-group">
                     <label class="control-label">Description/Instructions</label>
                     <textarea class="form-control" name="description" rows="3" id="description" maxlength="500" ></textarea>
                  </div>
               </div>
               <div class="col-sm-12 col-xs-12">
               <div class="form-group">
                  <label class="control-label">Tracking Number</label>
                  <input class="form-control" placeholder="Tracking Number" name="tracking_number" id="tracking_number" maxlength="26">
               </div>
            </div>
            </div>
            </div>

               <div class="col-sm-6 col-xs-12">
                   <div class="selected-pic">
                    <label class="control-label">Image (Optional)</label>
                        <div>
                           <a class="fancybox" rel="group" href="" >
                           <img id="online_purchase_preview" name="item_image" src=""  width="150px" height="120px" />
                          </a>
                        </div>
                        <label class="custom-input-file">
                        {!! Form::file('item_image', ['class'=> 'custom-input-file','id'=>'senior_image','onchange'=>"image_preview(this,'online_purchase_preview',event)"]) !!}
                        </label>
                     </div>
                  </div>
            </div>
         </div>
         <input type="hidden" value="" name="address_id" id="address_id">
         <div class="clearfix"></div>
         <div class="modal-footer">

            {!! Form::button('Update', ['class' => 'btn custom-btn1 pull-left','id'=>'item_loader','type'=>'submit']) !!}
            {!! Form::button('Close', ['class' => 'btn custom-btn1 pull-left', 'data-dismiss'=>'modal']) !!}
         </div>
         {!! Form::close() !!}
      </div>
   </div>
</div>


<div id="popover-content-login1" class="hide" >
  <h4>Suggested item weights</h4>
  <small>These are suggested weights and may not reflect the actual weight of your item</small>
  <table class="table table-bordered table-striped">
    <thead>
      <tr>
        <th width="150">Item Name</th>
        <th width="60">Weight/Unit</th>
      </tr>
    </thead>
    <tbody>

      @if(count($item) > 0)
      @foreach($item as $key)
      <tr>
        <td>{{ucfirst($key->item_name)}} </td>
        <td >{{number_format($key->lbsWeight,2)}} Lbs / {{number_format($key->kgWeight,2)}} Kg</td>
      </tr>
      @endforeach
      @endif




    </tbody>
  </table>
</div>

<div aria-labelledby="exampleModalLabel" role="dialog" tabindex="-1" id="exampleModal" class="modal fade in">
   <div role="document" class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header" id="Addaddress2">
            <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button>
            <h4 id="exampleModalLabel" class="modal-title">Add New Address</h4>
         </div>
         {!! Form::model('', ['name' => 'new_address', 'id' => 'Addaddress', 'method' => 'POST']) !!}
         <div class="panel-body">
            <div class="">
               <div class="col-sm-12 col-xs-12">
                  <div class="form-group">
                     <label>Address Line 1</label>
                     <input type="text" placeholder="Address" name="address_line_1" id="address_line_1"  maxlength= "125" class="form-control required">
                  </div>
               </div>
               <div class="col-sm-12 col-xs-12">
                  <div class="form-group">
                     <label>Address Line 2</label>
                     <input type="text" placeholder="Address" name="address_line_2" id="address_line_2"  maxlength= "125" class="form-control">
                  </div>
               </div>
<?php
$sessionCountry = json_decode(session()->get('country'));
$sessionState = json_decode(session()->get('state'));
$sessionCity = json_decode(session()->get('city'));
?>
              <div class="">
               <div class="col-sm-6 col-xs-12">
                  <div class="form-group">
                     <label>Country</label>
                     <select name="country" class="form-control required" id="pp_pickup_country10" onchange="get_state2('pp_pickup_country10','pp_pickup_state10','pp_pickup_city10','pp_pickup_state10','','','','10','')">
                        <option value="" id="country">Select Country</option>
                        @foreach($country as $key)
                        <option {{ (@$sessionCountry->id == $key->_id) ? 'selected' : '' }} value='{"id":"{{$key->_id}}","name":"{{$key->Content}}","state_available":"{{$key->state_available}}"}'>{{$key->Content}}</option>
                        @endforeach
                     </select>
                  </div>
               </div>
               <div class="col-sm-6 col-xs-12">
                  <div class="form-group">
                     <label>State/Region</label>
                     <span id="ap_id10">
                     <select name="state" class="form-control required left-disabled chosen-select" id="pp_pickup_state10" onchange="get_city('pp_pickup_state10','pp_pickup_city10','pp_pickup_city10','')">
                        @if(!empty($sessionState))
                    <option value='{"id":"{{@$sessionState->id}}","name":"{{@$sessionState->name}}"}'>{{@$sessionState->name}}</option>
                    @else
                    <option value="">Select State/Region</option>
                    @endif
                     </select>
                     </span>
                  </div>
               </div>
               </div>
               <div class="clearfix"> </div>
               <div class="col-sm-6 col-xs-12">
                  <div class="form-group">
                     <label>City</label>
                     <select  name="city" class="form-control required chosen-select" id="pp_pickup_city10">
                         @if(!empty($sessionCity))
                    <option value='{"id":"{{@$sessionCity->id}}","name":"{{@$sessionCity->name}}"}'>{{@$sessionCity->name}}</option>
                    @else
                    <option value="">Select City</option>
                    @endif
                     </select>
                  </div>
               </div>
               <div class="col-sm-6 col-xs-12">
                  <div class="form-group">
                     <label>Zip Code/Postcode</label>
                     <input type="text" placeholder="Zip Code/Postcode" name="zipcode" id="zipcode"  maxlength="8" class="form-control alpha-numeric">
                  </div>
               </div>
               </hr>
               <div class="col-sm-8 col-xs-12 col-sm-offset-2" align="center">
               <br />
                  <div class="row">
                     <div class="col-sm-6 col-xs-6 col form-group">
                        <div class="form-group">
                           <button class="custom-btn1 btn-block" id="address_loader" style="margin-bottom:0;" >
                              Submit
                              <div class="custom-btn-h"></div>
                           </button>
                        </div>
                     </div>
                     <div class="col-sm-6 col-xs-6 col">
                        <div class="form-group">
                           <button class="custom-btn1 btn-block"  data-dismiss="modal" style="margin-bottom:0;">
                              Close
                              <div class="custom-btn-h"></div>
                           </button>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         {!! Form::close() !!}
      </div>
   </div>
</div>
<div class="container">
<div class="row">
<div class="col-sm-12 col-xs-12" id="online_start_position">
   <h2 class="color-blue mainHeading">Online Purchases</h2>
   <br />
</div>
<div class="col-sm-12 col-xs-12" id="sec1">
   <div class="box-shadow">

      <!-- <h3 class="midHeading" id="midHeading">At this time, prices shown are for shipping to the door only and do not include the cost of customs duty if applicable. Estimate 15-25% of the cost of your items as customs.</h3> -->
      <h3 class="midHeading" id="midHeading">Please complete the following fields</h3>
      <hr />
      <!------------------step1------------------>
      <div class="row">
         <div class="col-sm-12 col-xs-12">
            <div class="step_box three_step clearfix">
               <div class="step first selected">
                  <div>1</div>
                  <p class="text-center colol-black">Package Details</p>
               </div>
               <div class="step inner" id="step2-header">
                  <div>2</div>
                  <p class="text-center colol-black">Drop Off Address</p>
               </div>
               <div class="step last " id="step3-header">
                  <div>3</div>
                  <p class="text-center colol-black">Payment</p>
               </div>
            </div>
         </div>
         <div class="col-sm-10  col-xs-12 col-sm-offset-1">
            <div class="col-sm-12 col-xs-12">
               <strong>Ship your items to:</strong> {{ Session::get('AqAddress')}}
               <br>
               <strong>Phone Number:</strong> {{ Session::get('Aqcc')}} - {{ Session::get('Aqphone')}}
               <br /><br />
            </div>
         </div>
         <div id="table_hide" style="@if(count($additem) <= 0) display:none; @endif">
            <div class="col-md-10 col-xs-12 col-sm-offset-1">
               <div class="col-sm-12 col-xs-12 text-right">
                  <!-- <button class="btn btn-default"  onclick="$('#add_item').show();$('#table_hide').hide();"  >Add Item</button> -->
                     <button class="btn btn-default"  onclick="$('#add_item').show();$('#table_hide').hide();"  id="add_item_button">Add Item</button>
                  </br>
                  </br>
               </div>
            </div>
            <div class="col-md-10 col-xs-12 col-sm-offset-1">
               <div class="col-sm-12 col-xs-12"  >
                  <table class="custom-table table table-bordered" name="item_list" id="item_list">
                     <thead>
                        <tr>
                           <th>S.No. </th>
                           <th>Item Name</th>
                           <th>Item Cost</th>
                           <th>Item Weight</th>
                           <th>Quantity</th>
                           <th>Description</th>
                           <th>Action</th>
                        </tr>
                     </thead>
                     <tbody>
                           <?php $sno = 1;?>
                        @foreach($additem as $value)
                        <?php
$weight_unit = 'lbs';

if ($value->measurement_unit == 'cm_kg') {
	$weight_unit = 'kg';
}
?>
                        <tr id="row-{{$value->_id}}">
                           <td>{{$sno++}}</td>
                           <td>{{ucfirst($value->item_name)}}</td>
                           <td>${{number_format((float)$value->item_price,2)}}</td>
                           <td>
                           @if($value->weight != '')
                           {{$value->weight}}&nbsp;{{ucfirst($weight_unit)}}
                           @else
                           N/A
                           @endif
                           </td>
                           <td>{{ucfirst($value->quentity)}}&nbsp;</td>
                           <td>{{ucfirst($value->description)}}</td>
                           <td colspan="2" >&nbsp;&nbsp;
                              <span><a type=""  data-toggle="modal" onclick="get_item_info('{{$value['_id']}}')" id="Edit" title="Edit" data-whatever="@mdo" href="#item_modal"> <i class="fa fa-pencil"></i></a></span>&nbsp;&nbsp;
                              <a title="delete" id="Delete"  onclick="remove_record('delete_item/{{$value->_id}}/DeleteItem','{{$value->_id}}')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>
                           </td>
                        </tr>
                        @endforeach
                        </tr>
                     </tbody>
                  </table>
                  <div class="">
                     <div class="">
                        <div class="form-group">
                           <button class="custom-btn1 btn"  onclick="switch_request_header('#sec2','#sec1',0)">
                           Next
                           	<div class="custom-btn-h"></div>
                           </button>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-md-10 col-xs-12 col-sm-offset-1" style="@if(count($additem) > 0) display:none; @endif" id="add_item" >
            {!! Form::open(['class'=>'form-vertical','name'=>'buy_for_me_form_add','id' => 'buy_for_me_form_add','file' => true ]) !!}
               <input type="hidden" value="" name="item_id" id="item_id">

              <div class="col-sm-6 col-xs-12">
                 <div class="form-group">
                    <label class="control-label">Item Name</label>
                    <input class="form-control required"   placeholder="Item Name"  name="item_name" id="item_name" maxlength="110" >
                 </div>
              </div>
              <div class="col-sm-6 col-xs-12">
                 <div class="form-group">
                    <label class="control-label">Purchased From</label>
                    <input type="text" class="form-control required username-#purchase from "  name="purchase_from"  id="add_item_url" placeholder="Purchased From" maxlength="700">
                 </div>
              </div>
              <div class="col-sm-6 col-xs-12">
                 <div class="form-group">
                    <label class="control-label">Item Cost<span class="red-star"> *</span></label>
                    <div class="input-group error-input">
                      <span class="input-group-addon">$</span>
                    <input class="form-control required float maxlength-9 " placeholder="Item Cost" name="item_price" id="item_price" maxlength="9">
                    </div>
                 </div>
              </div>
              <div class="col-sm-6 col-xs-12">
                 <div class="form-group">
                    <label class="control-label">Quantity</label>
                    <div style="width:160px;" class="input-group number-spinner">
                       <span class="input-group-btn data-dwn">
                       <button class="btn btn-primary" data-dir="dwn" onclick="return false;">
                       <span class="glyphicon glyphicon-minus"></span>
                       </button>
                       </span>
                       <input type="text" class="form-control text-center" value="1" min="1" max="40"  id="quentity" name="quentity" >
                       <input type="hidden" value="">
                       <span class="input-group-btn data-up">
                       <button class="btn btn-primary" data-dir="up" onclick="return false;">
                       <span class="glyphicon glyphicon-plus"></span>
                       </button>
                       </span>
                    </div>
                 </div>
              </div>

              <div class="col-sm-12 col-xs-12">
               <div class="form-group">
                  <!-- <label>
                  <input name="buy_for_me_dimensions" id="buy_for_me_dimensions" value="buy_for_me_dimensions" type="checkbox" style="margin-top:2px;">
                  </label>
                   I know item dimensions and weight -->

                 <div class="">

                     <span class="pull-left">
                        <input name="buy_for_me_dimensions" id="buy_for_me_dimensions" value="buy_for_me_dimensions" type="checkbox">
                     </span>
                     <span class="Insurance_check">
                     <p>
                        &nbsp;&nbsp;I know item dimensions and weight
                     </p>
                     </span>
                  </div>


               </div>
            </div>

             <div id="measurment" style="display:none;" >
              <div class="col-sm-12 col-xs-12">
                 <div class="form-group">
                    <label class="control-label">Measurement Units</label>
                    <div class="radio">
                       <label class="">
                       <input type="radio" name="measurement_unit" id="measurement_unit1"  value="cm_kg" onclick="unit_show('cm_kg')">
                       </label>&nbsp;Metric (Cm/Kg)
                       <label class="">
                       <input type="radio" name="measurement_unit" id="measurement_unit3" checked value="inches_lbs" onclick="unit_show('inches_lbs')">
                       </label>&nbsp;Imperial (Inches/Lbs)
                    </div>
                 </div>
              </div>
              <div class="col-sm-12 col-xs-12">
                 <label class="control-label">Item Specification</label>
              </div>

              <div class="col-sm-3 col-xs-12">
                 <div class="form-group">
                    <label class="control-label">Length</label>
                     <div class="input-group error-cm-input">
                    <input class="form-control  float maxlength-9" placeholder="Length"  id="length" name="length" maxlength="9" />
                    <span class="input-group-addon" id="length_unit1">Cm</span>
                    </div>
                 </div>
              </div>
              <div class="col-sm-3 col-xs-12">
                 <div class="form-group">
                    <label class="control-label">Width</label>
                        <div class="input-group error-cm-input">
                    <input class="form-control float maxlength-9" placeholder="Width" name="width"  id="width" maxlength="9">
                    <span class="input-group-addon" id="width_unit1">Cm</span>
                    </div>
                 </div>
              </div>
              <div class="col-sm-3 col-xs-12">
                 <div class="form-group">
                    <label class="control-label">Height</label>
                      <div class="input-group error-cm-input">
                    <input class="form-control float maxlength-9" placeholder="Height" name="height" id="height" maxlength="9">
                    <span class="input-group-addon" id="height_unit1">Cm</span>
                    </div>
                 </div>
              </div>
              <div class="col-sm-3 col-xs-12">
                 <div class="form-group">
                    <label class="control-label">Weight&nbsp;<a data-toggle="popover" data-trigger="focus"  data-container="body" data-placement="top" type="button" data-html="true" href="javascript:void(0)" id="login1" class="info_icon" style="margin-top:0;color:black;"><img src="theme/web/images/info_icon.png" width="20px">
                    </a></label>
                      <div class="input-group error-cm-input">
                    <input class="form-control required float maxlength-9" placeholder="Weight" name="weight" value="{{ (!empty(session()->get('weight'))) ? session()->get('weight') : '' }}" id="weight" maxlength="9">
                    <span class="input-group-addon" id="weight_unit1">kg</span>
                    </div>
                 </div>
              </div>
              </div>
              <div class="col-sm-12 col-xs-12">
                 <div class="form-group">
                    <label class="control-label">Shipping Mode</label>&nbsp;&nbsp;(Air: receive package within 5 to 10 business days. Sea: receive package within 6 to 8 weeks.)
                    <div class="radio">
                       <label class="">
                       <input type="radio" name="travel_mode" id="travel_mode_air" value="air" onclick="toggle_category('.travel-mode-air','.travel-mode-ship','#package_category_add')" {{ (session()->get('travelMode') != 'ship') ? 'checked' : '' }}>
                       </label>&nbsp;By Air
                       <label class="">
                       <input type="radio" name="travel_mode" id="travel_mode_ship" value="ship" onclick="toggle_category('.travel-mode-ship','.travel-mode-air','#package_category_add')" {{ (session()->get('travelMode') == 'ship') ? 'checked' : '' }}>
                       </label>&nbsp;By Sea
                    </div>
                 </div>
              </div>
              <div class="col-sm-12 col-xs-12">
                 <div class="form-group">
                    <label class="control-label">Select Package Category</label>
                    <select class="form-control required" id="package_category_add" name="category">
                       <option value="">Select Category</option>
                       <?php
$sessionCategory = json_decode(session()->get('category'));
foreach ($category as $key) {?>
                       <option {{ (@$sessionCategory->id == $key->_id) ? 'selected' : '' }} value='{"id":"{{$key->_id}}"}' class="travel-mode-{{$key->TravelMode}}">{{$key->Content}}</option>
                       <?php }?>
                    </select>
                 </div>
              </div>
              <div class="col-sm-12 col-xs-12">
                 <div class="form-group">
                    <label class="control-label">Package Description / Instructions</label>
                    <textarea class="form-control" name="description" rows="3" id="description" maxlength="500" ></textarea>
                 </div>
              </div>
              <div class="col-sm-6 col-xs-12">
               <div class="form-group">
                  <label class="control-label">Tracking Number</label>
                  <input class="form-control" placeholder="Tracking Number" name="tracking_number" maxlength="26">
               </div>
            </div>
              <div class="col-sm-3 col-xs-12">
                 <div class="form-group">
                    <label class="control-label">Insurance</label>
                    <div class="radio">
                       <label>
                       <input type="radio" name="insurance" id="insurance" value="yes"  checked >
                       </label>&nbsp;Yes
                       &nbsp; &nbsp;
                       <label>
                       <input type="radio" name="insurance"  id="insurance2" value="no" data-toggle="modal" data-target="#insurance_change"  >
                       </label>&nbsp; No
                    </div>
                 </div>
                 </div>
                 <div class="col-sm-3 col-xs-12">
                 <div class="form-group">
                    <label class="control-label">Item Image (Optional)</label>
                    <input type="file" name="item_image" id="item_image" class="valid-filetype-jpg,png,gif,jpeg" />
                 </div>
              </div>
              <div class="clearfix"></div>
                 <div class="col-xs-12">
                    <hr />
                 </div>

                   <div class="">
               <div class="">
                  <div class=" col-xs-12">
                     <div class="form-group" >

                        <a  class="custom-btn1 btn"  onclick="$('#add_item').hide();$('#table_hide').show();"   >Back
                        <div class="custom-btn-h"></div>
                        </a>
                        <button type="submit"  id="add_item_loader" name="add_item" class="custom-btn1 btn">  Add
                        <div class="custom-btn-h"></div>
                        </button>
                     </div>
                  </div>
               </div>
            </div>
              </div>
            {!! Form::close() !!}

         </div>
      </div>
   </div>
</div>
<!------------------step1------------------>
<!------------------step2------------------>


{!! Form::open(['class'=>'form-vertical','name'=>'buy_for_me_form','id' => 'buy_for_me_form' ]) !!}
<input type="hidden" name="discount" id="olp_discount">
<div class="" id="sec2" style="display:none;">
   <div class="box-shadow">
      <!-- <h3 class="midHeading" id="midHeading">At this time, prices shown are for shipping to the door only and do not include the cost of customs duty if applicable. Estimate 15-25% of the cost of your items as customs.</h3> -->
      <h3 class="midHeading">Please complete the following fields</h3>
      <hr />
      <!------------------step1------------------>
      <div class="step_box three_step clearfix">
         <div class="step first selected">
            <div>1</div>
            <p class="text-center colol-black">Package Details</p>
         </div>
         <div class="step inner selected">
            <div>2</div>
            <p class="text-center colol-black">Drop Off Address</p>
         </div>
         <div class="step last ">
            <div>3</div>
            <p class="text-center colol-black">Payment</p>
         </div>
      </div>
      <div class="col-sm-10 col-xs-12 col-sm-offset-1">
            <strong>Your Aquantuo Address:</strong>{{ Session::get('AqAddress')}}
            <br /><br />
      </div>
      <div class="row">
         <div class="col-md-10 col-xs-12 col-sm-offset-1">
            <div class="col-sm-10 col-xs-12">
               <div class="form-group">
                  <label class="control-label">Select Saved Address</label>
                  <select class="form-control required usename-#address#" name="address" id="dropoff_address">
                     <option value="">Select address</option>
                     @foreach($address as $value)
                     <option value="{{json_encode($value)}}">{{ucfirst($value->address_line_1)}} {{$value->city}} {{$value->state}} {{$value->country}} {{$value->zipcode}}</option>
                     @endforeach
                  </select>
               </div>
            </div>
            <div class="col-sm-2 col-xs-12">
               <div class="form-group">
                  <label class="control-label">&nbsp;</label>
                  <button type="button" class="btn btn-primary blue-btn" data-target="#exampleModal" data-toggle="modal">Add New Address</button>
               </div>
            </div>



            <div class="col-sm-12 col-xs-12" >
               <div class="form-group">
                  <div class="checkbox">
                     <label>
                        <input type="checkbox" name="ReceiverIsDifferent"  id="ReceiverIsDifferent" onclick=" return receiver_name()">
                     </label>Receiver is different from Requester
                  </div>
               </div>
            </div>

            <div class="col-sm-4 col-xs-4" id="ReceiverName_in" style="display: none;">
               <div class="form-group">
                  <label class="control-label">Receiver Name</label><span class="red-star"> *</span>

                    <input type="text" class="form-control required  usename-#name#" name="ReceiverName" placeholder="Receiver Name" maxlength="100" value="">

               </div>
            </div>
            <div class="col-sm-12 col-xs-12"></div>
            <div class="col-sm-6 col-xs-12">
               <div class="form-group">
                  <label class="control-label">Receiver's Phone Number</label><span class="red-star"> *</span>

                  <div class="row">
                     <div class="col-xs-4 col-xs-12">
                        <input type="text" class="form-control required  usename-#country_code#" placeholder="Country Code" name="country_code" maxlength="4" value="{{Session::get('CountryCode')}}">
                     </div>
                     <div class="col-xs-8 col-xs-12">
                        <input type="text" class="form-control required numeric usename-#phone_number# between-8-12" name="phone_number" placeholder="Phone Number" maxlength="12" value="{{Session::get('PhoneNo')}}">
                     </div>
                  </div>
               </div>
            </div>

            <!-- <div class="col-sm-4 col-xs-12">
              <div class="form-group">
                  <label class="control-label">Desired Delivery Date</label>
                  <input class="form-control usename-#date#" placeholder="Date" name="desired_delivery_date" id="desired_delivery_date" readonly>
               </div>
            </div> -->

            <div class="col-sm-12 col-xs-12">
               <div class="form-group">
                  <label class="control-label">Return Address (if item is not delivered)</label>
                  <div class="checkbox">
                     <label>
                        <input type="checkbox" name="return_to_aq" value="yes">
                     </label> Return to Aquantuo
                  </div>
               </div>
            </div>

            <div class="col-sm-12 col-xs-12">
               <div class="form-group">
                  <div class="checkbox ">
                     <label>
                        <input type="checkbox" name="consolidate_check" value="on"  @if(Session::get('consolidate_item') == 'on') checked="checked" @endif>
                     </label> Consolidate my items and ship them together when possible.
                  </div>
               </div>
            </div>

            <div class="">
               <div class="">
                  <div class="col-xs-12">
                     <div class="form-group">

                          <a class="custom-btn1 btn text-center" onclick="switch_request_header('#sec1','#sec2',0)" href="javascript:void(0)">
                        Back
                        <div class="custom-btn-h"></div>

                        </a>
                        <button class="custom-btn1 btn" id="calculate_loader" data-toggle="modal" data-target="#exampleModal454650">
                        Next
                        <div class="custom-btn-h"></div>
                        </button>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="" id="sec3" style="display:none;">
   <div class="box-shadow">
      <h3 class="midHeading">Review your order</h3>
      <hr />
      <!------------------step1------------------>
      <div class="step_box three_step clearfix">
         <div class="step first selected">
            <div>1</div>
            <p class="text-center colol-black">Package Details</p>
         </div>
         <div class="step inner selected">
            <div>2</div>
            <p class="text-center colol-black">Drop Off Address</p>
         </div>
         <div class="step last selected" >
            <div>3</div>
            <p class="text-center colol-black">Payment</p>
         </div>
      </div>
      <div class="col-sm-10 col-xs-12 col-sm-offset-1">
            <strong>Your Aquantuo Address:</strong>{{ Session::get('AqAddress')}}
            <br /><br />
      </div>
      <div class="row">
         <div class="col-md-10 col-xs-12 col-sm-offset-1">
            <div class="col-sm-12 col-xs-12">
               <div id="shipping_detail">
                  Calculating! Please wait...
               </div>


               <div class="">
                  <div class="row">
                     <div class="col-xs-12">
                        <div class="form-group">

                            <a class="custom-btn1 btn text-center" onclick="switch_request_header('#sec2','#sec3',0)" href="javascript:void(0)">
                           Back
                           <div class="custom-btn-h"></div>
                           </a>

                           <button class="custom-btn1 btn" id="creating_req_btn">
                           Next
                              <div class="custom-btn-h"></div>
                           </button>
                        </div>

                     </div>

                     <div class="row custom-row">
                           <!-- <div class="col-sm-12"><b>&nbsp;&nbsp;&nbsp;&nbsp;You will be billed or refunded any difference in the cost of shipping when we receive your item(s)</b></br> -->

                           {{-- <div class="col-sm-12"><b>&nbsp;&nbsp;&nbsp;&nbsp;If actual weight wasn’t provided, you may be billed or refunded any differences in price or shipping upon review</b></br> --}}

                        </div>
                     <input type="hidden" name="distance" id="distance">
                     <input type="hidden" name="promo_code" id="promo_code_olp_promo">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!------------------step2------------------>
      <!------------------step3------------------>
      <div class="col-sm-12 col-xs-12" id="sec3" style="display:none;">
         <div class="box-shadow">
            <h3 class="midHeading" id="midHeading">At this time, prices shown are for shipping to the door only and do not include the cost of customs duty if applicable. Estimate 15-25% of the cost of your items as customs.</h3>
            <h3>Please complete the following fields</h3>
            <hr />
            <!------------------step1------------------>
            <div class="row" >
               <div class="step_box three_step clearfix">
                  <div class="step first selected">
                     <div>1</div>
                     <p class="text-center colol-black">Package Details</p>
                  </div>
                  <div class="step inner selected">
                     <div>2</div>
                     <p class="text-center colol-black">Drop Off Address</p>
                  </div>
                  <div class="step last selected">
                     <div>3</div>
                     <p class="text-center colol-black">Payment</p>
                  </div>
               </div>
            </div>
            <div class="col-md-10 col-xs-12 col-sm-offset-1">
               <div class="col-sm-12 col-xs-12">
                  <div class="row custom-row">
                     b
                     <div class="col-sm-6 col-xs-6"><b>Total Weight -</b></div>
                     <div class="col-sm-6 col-xs-6">50lbs</div>
                  </div>
                  <div class="row custom-row">
                     <div class="col-sm-6 col-xs-6"><b>Item Cost -</b></div>
                     <div class="col-sm-6 col-xs-6">$50.00</div>
                  </div>
                  <div class="row custom-row">
                     <div class="col-sm-6 col-xs-6"><b>Retailer Shipping -</b></div>
                     <div class="col-sm-6 col-xs-6">50lbs</div>
                  </div>
                  <div class="row custom-row">
                     <div class="col-sm-6 col-xs-6"><b>Aquantuo Shipping -</b></div>
                     <div class="col-sm-6 col-xs-6">50lbs</div>
                  </div>
                  <div class="row custom-row">
                     <div class="col-sm-6 col-xs-6" ><b>Delivery within Ghana -</b></div>
                     <div class="col-sm-6 col-xs-6">50lbs</div>
                  </div>
                  <div class="row">
                     <hr />
                  </div>
                  <div class="col-sm-8 col-sm-offset-2">
                     <div class="form-group">
                        <button class="custom-btn1 btn-block" >
                           Next
                           <div class="custom-btn-h"></div>
                        </button>
                     </div>
                  </div>
               </div>
            </div>
            <div class="clearfix"></div>
            <div class="list-footer">
               <div class="row">
                  <div class="col-sm-6">
                     <h4><b>Total Amount- $250.00</b></h4>
                     <small>(In Ghananian cedi GHS 315,8656.00)</small>
                  </div>
                  <div class="col-sm-6">
                     <div class="form-group reward-group">
                        <input class="form-control pull-left" placeholder="Reward Code" type="text" />
                        <button class="btn default-btn">
                        <img src="theme/web/promo/images/green_check.png" />
                        Apply
                        </button>
                        <span>$21.00</span>
                     </div>
                  </div>
               </div>
               <small class="color-red">You wil be billed or refunded any differences in price or shipping at time of purchase</small>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="insurance_change" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title"> Alert</h5>
            <a href="javascript::void(0)" class="close" data-dismiss="modal" aria-label="Close" onclick="alertClose()"> <span aria-hidden="true">&times;</span>
            </a>
          </div>
          <input type="hidden" value="{{session()->get('alert_status')}}" id="alertMsg">
          <div class="modal-body">By declining insurance, you agree to relieve  Aquantuo and all of its contracted agents from any and all liability in the event of loss or damage to your package.</div>

      </div>
    </div>
</div>
<input type="hidden" name="browser" id="browser" value="{{$browser}}">
<input type="hidden" name="version" id="version" value="{{$version[2]}}">
<input type="hidden" name="device_type" id="device_type" value="website">
{!! Form::close() !!}
	@if(session()->get('alert_content')=='on')
	<div class="modal fade" id="exampleModal454650" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
		  <div class="modal-content">
			  <div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel"></h5>
				<a href="javascript::void(0)" class="close" data-dismiss="modal" aria-label="Close" onclick="alertClose()"> <span aria-hidden="true">&times;</span>
				</a>
			  </div>
			  <input type="hidden" value="{{session()->get('alert_status')}}" id="alertMsg">
			  <div class="modal-body">{{session()->get('alert_content')}}</div>

		  </div>
		</div>
	</div>
	@endif	
@endsection
@section('script')
@parent
{!! Html::script('theme/web/js/utility.js') !!}
{!! Html::script('theme/web/js/validation.js') !!}
{!! Html::script('theme/web/js/online_purchase.js') !!}
{!! Html::style('theme/date-time-picker/jquery.datetimepicker.css') !!}
{!! Html::script('theme/date-time-picker/jquery.datetimepicker.js') !!}
{!! Html::style('theme/admin/choose-multiple/bootstrap-chosen.css') !!}
{!! Html::script('theme/admin/choose-multiple/chosen.jquery.js') !!}
@endsection
@section('inline-script')
@parent
<script type="text/javascript">
  var total_item_count = parseInt('{{count($additem)}}');
   $(function() {
       var action;
       $(".number-spinner button").mousedown(function () {
           btn = $(this);
           input = btn.closest('.number-spinner').find('input');
           btn.closest('.number-spinner').find('button').prop("disabled", false);

           if (btn.attr('data-dir') == 'up') {
               action = setInterval(function(){
                   if ( input.attr('max') == undefined || parseInt(input.val()) < parseInt(input.attr('max')) ) {
                       input.val(parseInt(input.val())+1);
                   }else{
                       btn.prop("disabled", true);
                       clearInterval(action);
                   }
               }, 50);
           } else {
               action = setInterval(function(){
                   if ( input.attr('min') == undefined || parseInt(input.val()) > parseInt(input.attr('min')) ) {
                       input.val(parseInt(input.val())-1);
                   }else{
                       btn.prop("disabled", true);
                       clearInterval(action);
                   }
               }, 50);
           }
       }).mouseup(function(){
           clearInterval(action);
       });
   });

   var newdate = new Date();
   newdate.setHours(newdate.getHours()+3);

var minDateForTwo = newdate;
var drop_off_date = {};
    $('#desired_delivery_date').datetimepicker({
        format:'M d, Y h:i A',
        formatTime:'h:i A',
        timepicker:true,
        datepicker:true,
        step:30,
        minDate : new Date(),
        minTime : newdate,
        onChangeDateTime:function( ct ){
            minDateForTwo = ct;
            drop_off_date.minDate = ct;
            var dt = new Date();
            dt.setHours(23);dt.setMinutes(59);dt.setSeconds(59);

            if(ct < dt) {
              $('#desired_delivery_date').datetimepicker({  minTime: newdate });
              drop_off_date.minTime = ct;

              if(ct < newdate) {
                $('#desired_delivery_date').val(dt.dateFormat('M d, Y ')+newdate.dateFormat('h:i A'));
                drop_off_date.minTime = minDateForTwo;
              }
            } else {
              dt.setHours(0);
              $('#desired_delivery_date').datetimepicker({  minTime: dt });
              drop_off_date.minTime = false;
            }
           // $('#desired_delivery_date').datetimepicker(drop_off_date);
           //$('#desired_delivery_date').val('');
         },
   });


new Validate({
  FormName :  'new_address',
  ErrorLevel : 1,
  callback: function() {
      $("#address_loader").addClass("spinning");
       $.ajax({
           url: SITEURL+'add-address',
           data: {     "address_line_1": $('#address_line_1').val() ,
                       "address_line_2": $('#address_line_2').val(),
                       "country": $('#pp_pickup_country10').val(),
                       "state": $('#pp_pickup_state10').val(),
                       "city":$('#pp_pickup_city10').val(),
                       "zipcode": $('#zipcode').val()},
           type : 'post',
           dataType: 'json',
           success : function(obj) {
                 $("#address_loader").removeClass("spinning");

                if(obj.success == 1) {
                    document.getElementById("address_loader").value = "Submit";
                    document.getElementById("Addaddress").reset();
                    $('#dropoff_address').html(obj.address_html);
                     $("#exampleModal").modal("hide");
                }
                alert(obj.msg);
           }
        });
   }
});



new Validate({
  	FormName :  'buy_for_me_form_add',
  	ErrorLevel : 1,
    validateHidden : false,
  	callback: function() {
	  	if(!$("#add_item_loader").hasClass("spinning"))
	  	{
	      $("#add_item_loader").addClass("spinning");
	       $.ajax({
	            url: 'add-online-item',
	            data: new FormData(document.getElementById('buy_for_me_form_add')),
	            type : 'post',
	            processData: false,
	            contentType: false,
	            dataType: "json",
	            success : function(obj) {
	                $("#add_item_loader").removeClass("spinning");
	                alert(obj.msg);
                   if(obj.success == 1)
                   {
                    total_item_count = total_item_count + 1;
                     try {
                       document.getElementById("buy_for_me_form_add").reset();
                     }catch(e) {}
                     $('#item_list').html(obj.html);
                     $('#table_hide').show();
                     $('#add_item').hide();
                     scroll_to('online_start_position');
                     unit_show('inches_lbs');
                   }

	           }
	        });
   		}
    }
});

   function remove_record(url,rowid)
   {
    if(confirm('Are you sure you want to delete this card?') == true)
    {
        $('#row-'+rowid).addClass('relative-pos spinning');

        url = SITEURL+url;
        $.ajax
            ({
                url: url,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(res)
                {
                    var obj = JSON.parse(res);
                    $('#row-'+rowid).removeClass('relative-pos spinning');
                    if(obj.success == 1){
                        --total_item_count;
                        $('#row-'+rowid).css({'background-color':'red'});
                        $('#row-'+rowid).fadeOut('slow');
                        $('#error_msg_section').html('<div class="alert alert-success">'+ obj.msg + '</div>');
                    }else{
                        $('#row-'+rowid).css({'background-color':'white'});
                        $('#error_msg_section').html('<div class="alert alert-danger">'+ obj.msg + '</div>');
                        alert(obj.msg);
                    }
                }
            });
    }
    return false;
   }

   $('#add_item_button').click(function(){
      $('#buy_for_me_dimensions').prop('checked', false);
      $("#measurment").hide();
   });

var sessionUnit = '<?php echo session()->get('weight_unit'); ?>';
  $(document).ready(function(){
    if(sessionUnit != ''){
      $('#measurment').show();
      document.getElementById("buy_for_me_dimensions").checked = true;
    }
  });

  function get_item_info(id){
    $.ajax({
       url: 'get_item_info/'+id,
       type : 'get',
       dataType: 'json',
       success : function(obj) {
          //alert(obj.result.item_name);
          get_edit_item(obj.result);
       }
    });
    return false;
  }


   function get_edit_item(obj)
   {

      $('#buyformedimensions').hide();
      $('#edit_item').trigger('reset');
      //obj = eval('('+obj+')');

    $('#item_name').val(obj.item_name);
    $('#add_item_url').val(obj.add_item_url);

    $('#buyforme_dimensions').prop('checked', false);
   if(obj.weight != '')
   {
      $('#buyformedimensions').show();
      document.getElementById('buyforme_dimensions').checked ='true';
      if(obj.measurement_unit == 'cm_kg') {
         document.getElementById('measurement_unit').checked='checked';
      }else{
         document.getElementById('measurement_unit2').checked='checked';
      }
       $('#length').val(obj.length);
       $('#width').val(obj.width);
       $('#height').val(obj.height);
       $('#weight').val(obj.weight);
   }else{
      document.getElementById('buyforme_dimensions').unchecked ='true';
   }

   make_dimension_optional('buyforme_dimensions');
   unit_show(obj.measurement_unit);
   $('#item_price').val(obj.item_price);
   $('#quentity').val(obj.quentity);

   if(typeof obj.image == "undefined"){
      obj.image = '';
   }
   if(obj.image != '') {
      $('#online_purchase_preview').attr('src',SITEURL+'upload/'+obj.image);
   } else {
      $('#online_purchase_preview').attr('src',SITEURL+'upload/no-image.jpg');
   }

   if(obj.travel_mode == 'air') {
      document.getElementById('travel_mode_air').checked='checked';
      toggle_category('.travel-mode-air','.travel-mode-ship','#package_category');
   }else{
      document.getElementById('travel_mode_ship').checked='checked';
      toggle_category('.travel-mode-ship','.travel-mode-air','#package_category');
   }
    //$('#package_category').val('{"id":"'+obj.category_id+'","name":"'+obj.category+'"}');
    $('#package_category').val('{"id":"'+obj.category_id+'"}');
    $('#description').val(obj.description);

    console.log(obj.insurance);
     if(obj.insurance == 'yes') {
      document.getElementById('insurance').checked='checked';
      }else{
      document.getElementById('insurance2').checked='checked';
    }
    $('#item_id').val(obj._id);
    $('#tracking_number').val(obj.tracking_number);
   }

new Validate({
    FormName :  'edit_item',
    validateHidden : false,
    ErrorLevel : 1,
    callback: function() {

       $("#item_loader").addClass("spinning");
       $.ajax({
           url: 'edit-online-item/{$value->_id}',
           data: $('#edit_item').serialize(),
           type : 'post',
           data: new FormData(document.getElementById('edit_item')),
           processData: false,
           contentType: false,
           dataType: 'json',
           success : function(obj) {
               $("#item_loader").removeClass("spinning");
               alert(obj.msg);

               if(obj.success == 1) {
                  document.getElementById("item_loader").value = "Submit";
                  document.getElementById("edit_item").reset();
                  $('#item_list').html(obj.edit_html);
                  $("#item_modal").modal("hide");
                  scroll_to('online_start_position');
               }
           }
        });
    }
   });





$("#buy_for_me_dimensions").click(function(){
   // If checked
   if ($("#buy_for_me_dimensions").is(":checked"))
   {
      //show the hidden div
      $("#measurment").show();
   }
   else
   {
      //otherwise, hide it
      $("#measurment").hide();
   }
});


$("#buyforme_dimensions").click(function(){
   if ($("#buyforme_dimensions").is(":checked")){
      $("#buyformedimensions").show();
   } else{
      $("#buyformedimensions").hide();
   }
});



function make_dimension_optional(id ) {
   if ($("#"+id).is(":checked")){
      $("#buyformedimensions").show();

   } else{
      $("#buyformedimensions").hide();
   }
}

function unit_show(unit)
{
   //var unit = $('input[name=measurement_unit]:checked').val();
   var weightUnit = 'Lbs';
   var dimentionUnit = 'Inches';
   if(unit == 'cm_kg'){
      weightUnit = 'Kg';
      dimentionUnit = 'cm';
   }
   $('#length_unit').html(dimentionUnit);
   $('#width_unit').html(dimentionUnit);
   $('#height_unit').html(dimentionUnit);
   $('#weight_unit').html(weightUnit);

   $('#length_unit1').html(dimentionUnit);
   $('#width_unit1').html(dimentionUnit);
   $('#height_unit1').html(dimentionUnit);
   $('#weight_unit1').html(weightUnit);

}

unit_show('inches_lbs');



$("[data-toggle=popover]").each(function(i, obj) {
  $(this).popover({
    html: true,
      content: function() {
      var id = $(this).attr('id')
      return $('#popover-content-' + id).html();
    }
  });
});


function receiver_name(){

  if($("#ReceiverIsDifferent").prop('checked') == true){
    $('#ReceiverName_in').show();
  }else{
    $('#ReceiverName_in').hide();
  }

}

$(document).ready(function(){
    var session_mode = '<?php echo session()->get('travelMode'); ?>';
    if(session_mode == 'ship'){
      toggle_category('.travel-mode-ship', '.travel-mode-air');
    }else{
      toggle_category('.travel-mode-air', '.travel-mode-ship');
    }
});





</script>

@endsection

