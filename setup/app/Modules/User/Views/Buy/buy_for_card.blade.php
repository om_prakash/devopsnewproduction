@extends('Page::layout.one-column-page')
@section('content')

<div class="container">

	<div class="row">
	    <div class="col-xs-6 col-sm-6">
			<h2 class="color-blue mainHeading" style="text-align:left;">Card List</h2>
	    </div>
	    <div class="col-xs-6 col-sm-6 text-right">
	    	{{-- <a class="btn btn-primary" href={{url('edit-request')}}/{{Request::segment(2)}} style="margin-right:0;">
			 <i class="fa fa-arrow-left"> Back</i></a> --}}
			 <br /><br />
		</div>

		<div class="col-xs-12 col-sm-12">
			<br/>
		<input type="checkbox" value='' name="check"  id="exist">&nbsp;
        <small style="color: #f00;">
        <!-- <span style="font-weight:bold">PLEASE READ:</span> -->
        {{$admin_msg}}
		</small>
		<br/><br/>
			<p style="font-weight:bold">Please select your card to pay the difference or <b class="color-blue"><a href="{{url('buy-for-me-detail')}}/{{Request::segment(2)}}">skip</a></b>
				to pay later.

			</p>

		</p>

		</div>
	</div>
	<div class="clearfix"></div>
		<span id="card_msg"></span>
		<div class="row">

			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="box-shadow clearfix">
				<br/>



				@if(isset($cards['data']) && count(@$cards['data']) > 0)
					@foreach($cards['data'] as $key)
					<div class="col-md-4 col-sm-6 col-xs-12" id="card_{{$key['id']}}">
						<div class="card-wrapper">

							<a class="custom-btn"    href="{{url('pay-now-by-for-me/'.Request::segment(2))}}/{{$key['id']}}?promocode={{Input::get('promocode')}}" onclick="return show_btn()">Pay Now</a>
								<div class="media">
									<div class="media-left">
										<a href="#">
											@if($key['brand'] == 'Visa')
											<img height="30px" width="50px" src="{{ImageUrl}}/webcard/visa.png">
											@elseif($key['brand'] == 'Diners Club')
											<img height="30px" width="50px" src="{{ImageUrl}}/webcard/dinsers.jpeg">
											@elseif($key['brand'] == 'Discover')
											<img height="30px" width="50px" src="{{ImageUrl}}/webcard/discover.jpeg">
											@elseif($key['brand'] == 'MasterCard')
											<img height="30px" width="50px" src="{{ImageUrl}}/webcard/mastercard.jpeg">
											@elseif($key['brand'] == 'American Express')
											<img height="30px" width="50px" src="{{ImageUrl}}/webcard/ae.jpeg">
											@elseif($key['brand'] == 'JCB')
											<img height="30px" width="50px" src="{{ImageUrl}}/webcard/jcb.jpeg">
											@endif
										</a>
									</div>
									<div class="media-body">
										<p class="media-heading color-blue" style="font-weight:bold">xxxx-xxxx-xxxx-{{$key['last4']}}</p>
										<p>Pricing may change if the actual item weight was not entered.</p>

									</div>
								</div>
							</div>
						</div>
					@endforeach

				@endif

				<div class="col-md-4 col-sm-6 col-xs-12">
                  <div class="card-wrapper gray-card">
						<a data-whatever="@mdo" data-target="#modal2" data-toggle="modal" href="" ><span><i class="fa fa-plus"></i>Add card</span></a>
                   </div>
				 </div>



			</div>
		</div>

         <div class="col-md-12 col-sm-6 col-xs-12" >
       			 <a href="{{url('pay-by-mobile-money/'.Request::segment(2))}}"> Pay by Mobile Money</a>
         </div>

    </div>
</div>






<!--card modal-->
<div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title color-white" id="myModalLabel">&nbsp;&nbsp;&nbsp;Add Card</h4>
         </div>
         <div class="clearfix"></div>
         {!! Form::model('', ['name' => 'Addcard', 'id' => 'Addcard', 'method' => 'POST', 'url' => 'add-card']) !!}
         <input type="hidden" name="requestid" value="{{Request::segment(2)}}">
         <div class="modal-body">
            <div class=col-md-12 id="succesdiv"></div>
            <div id='errordiv' class="col-sm-12"></div>
            <div class="form-group clearfix">
               <div class="col-sm-12">
                  {!! Form::label('name_on_card', 'Name on Card',['class'=>'color-black']) !!}
                  {!! Form::text('name_on_card', '', ['class'=>'form-control required','maxlength'=>'25' ,'placeholder'=> 'Name on Card','id' =>'name_card']) !!}
                  <p class="help-block white" id='er_name_card'></p>
               </div>
            </div>
            <div class="form-group clearfix">
               <div class="col-sm-12">
                  {!! Form::label('credit_card_number', 'Credit Card Number',['class'=>'color-black']) !!}
                  {!! Form::text('credit_card_number', '', ['class'=>'form-control required numeric','maxlength'=>'16	' , 'placeholder'=> 'Credit Card Number','id' =>'credit_card_number','maxlength'=>'16']) !!}
                  <p class="help-block white" id='er_credit_card_number'></p>
               </div>
            </div>
            <div class="">
               <div class="col-sm-12">
                  {!! Form::label('expiration_date', 'Expiration Date',['class'=>'color-black']) !!}
                  <div class="row">
                     <div class="col-sm-6">
                        <div class="form-group">
                           {!! Form::select('month', [''=>'Month','1'=>'01','2'=>'02','3'=>'03','4'=>'04','5'=>'05','6'=>'06','7'=>'07','8'=>'08','9'=>'09','10'=>'10','11'=>'11','12'=>'12'],Input::get('month'),['class'=>'form-control required numeric usename-#month#"','id'=>'month']) !!}
                           <p class="help-block white" id='er_month'></p>
                        </div>
                     </div>
                     <div class="col-sm-6">
                        <div class="form-group">
                           {!! Form::select('year', [],Input::get('year'),['class'=>'form-control required numeric usename-#year#"', 'id'=>"year",'placeholder'=>'Year']) !!}
                           <p class="help-block white" id='er_year'></p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="form-group">
               <div class="col-sm-12">
                  {!! Form::label('security_code', 'Security Code',['class'=>'color-black']) !!}
                  {!! Form::password('security_code',['class'=>'form-control required numeric', 'placeholder'=> 'Security code', 'maxlenght'=>'4', 'id' =>'security_code','maxlength'=>'4']) !!}
                  <p class="help-block white" id='er_security_code'></p>
               </div>
            </div>
         </div>
         <div class="clearfix"></div>
         <div class="modal-footer">
            <div class="col-sm-12 text-right">
               {!! Form::button('Sumbit', ['class' => 'btn custom-btn1','id'=>'addbutton', 'type'=>'submit', 'style'=>'margin-bottom:0;']) !!}

               {!! Form::button('Close', ['class' => 'btn custom-btn1', 'data-dismiss'=>'modal', 'style'=>'margin-bottom:0;']) !!}
            </div>
         </div>
         {!! Form::close() !!}
      </div>
   </div>
</div>



<script>

function show_btn()
{
	checked =$('#exist').prop('checked'); // true
	//checked = $("input[type=checkbox]:checked").length;
	if(checked == 0){
		alert('Please accept agreement.');
		//return false;
		flage=false;
	}else if(checked == 1)
	{
		flage=true;
		//return true;
	}
	return flage;
}




function delete_card(cardId){
{
if(confirm("Are you sure you want to delete this card?")){
	$("#card_"+cardId).addClass('spinning');
  if(cardId != ""){
    $.ajax({
      url     : '{{ url("remove-creditcard")}}',
      type    : 'post',
      data    : 'cardid='+cardId,
      success : function(res){
        $("#card_"+cardId).removeClass('spinning');
        var obj = eval("("+res+")");
        alert_message('card_msg',obj.success,obj.msg);
        if(obj.success == 1){
			$("#apend_saved_card").html(obj.new_cardlist);
        }if(obj.success == 2)
        {
			alert_message('card_msg',0,obj.msg);
		}

      }
    });
  }
  else{
    alert("Invalid card");
  }
}

}
}
	</script>
@endsection

    @section('script')
@parent

{!! Html::script('theme/web/js/utility.js') !!}
{!! Html::script('theme/web/js/validation.js') !!}
{!! Html::script('theme/web/js/request.js') !!}


@endsection

@section('inline-script')
@parent
 <script>
			var yearsToShow = 25;
			var thisYear = (new Date()).getFullYear();
			for (var y = thisYear; y < thisYear + yearsToShow; y++) {
			  var yearOption = document.createElement("option");
			  yearOption.value = y;
			  yearOption.text = y;
			  document.getElementById("year").appendChild(yearOption);
			}

new Validate({
	FormName : 'Addcard',
	ErrorLevel : 1,
	callback : function(){
      add_card();
    }
});





function add_card()
{

	$('#addbutton').addClass('spinning');
	$.ajax({
	url: SITEURL+"add-card",
	type:"post",
	dataType: 'json',
	data: $('#Addcard').serialize(),
		success: function(res)
		{
			$('#addbutton').removeClass('spinning');
			if(res.success == 0)
			{
				//$('#errordiv').html(res.msg);

				$('#errordiv').html('<div role="alert" class="alert alert-danger alert-dismissible fade in" style="margin-top: 0px;">'
			+'<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>'+res.msg+'</div>');
			}
			if(res.success == 1)
			{

				$("#Addcard").trigger("reset");
				$("#errordiv").hide();
				$('#succesdiv').html('<div role="alert" class="alert alert-success alert-dismissible fade in" style="margin-top: 0px;">'
			+'<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>'+'<b>Success !</b> Card has been added successfully.'+'</div>');


				setTimeout(function(){ location.reload(); }, 3000);

			}


		}

	});

}


</script>


@endsection
