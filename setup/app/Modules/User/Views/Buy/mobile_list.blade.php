@extends('Page::layout.one-column-page')
@section('content')
{!!Html::style('theme/web/css/parsley.css') !!}
<style>
.parsley-required {
    color: #a94442;
    font-weight: bold;
    font-size: 13px;
}
a.disabled
    pointer-events: none;
    opacity: .65;
}
</style>
<div class="container">

	<div class="row">
    	<div class="col-xs-6 col-sm-6">
		<h2 class="color-blue mainHeading" style="text-align:left;">Mobile Money Payment</h2>
        </div>
        <div class="col-xs-6 col-sm-6">
        <small class="pull-right color-blue mainHeading" style="text-align:right;">
		<div class="clearfix"></div>
		@if(Request::segment(1) == 'process-card-list' || Request::segment(1) == 'pay-by-mobile-money')
		 <a class="btn btn-primary" onclick="history.back();" style="margin-right:0;">
		 <i class="fa fa-arrow-left"> Back</i></a>
		 @endif
		</small></div>
        </div>
        <br/>
        <p id="statementPtag" style="color: #f00;">
		<!-- I understand that customs duty may be accessed on a value of my item other than what I provided. </p> -->
        <input type="checkbox" value='' name="check"  id="exist">&nbsp;
        @if($req_amount->RequestType == 'local_delivery')
         I understand that package will not be released to recipient until full payment is received </p>
        @else
        	{{$admin_msg}}</p>
        @endif
		<br/>
		@if(count($mobileMoney) > 0)
			<p style="font-weight:bold">Select a Mobile Money account below to pay, or click&nbsp;
	        @if($req_amount->RequestType == 'online')
	        <b class="color-blue"><a href="{{url('online-request-detail')}}/{{Request::segment(2)}}">SKIP</a></b>
	        @elseif($req_amount->RequestType == 'delivery')
	        <b class="color-blue"><a href="{{url('send-package-detail')}}/{{Request::segment(2)}}">SKIP</a></b>
	        @elseif($req_amount->RequestType == 'buy_for_me')
	        <b class="color-blue"><a href="{{url('buy-for-me-detail')}}/{{Request::segment(2)}}">SKIP</a></b>
	        @elseif($req_amount->RequestType == 'local_delivery')
	        <b class="color-blue"><a href="{{url('local-delivery-detail')}}/{{Request::segment(2)}}">SKIP</a></b>
	        @endif
	        &nbsp;to pay later.
			</p>
		@else
			<p style="font-weight:bold">Select a Mobile Money account below to pay, or click&nbsp;
	        @if($req_amount->RequestType == 'online')
	        <b class="color-blue"><a href="{{url('online-request-detail')}}/{{Request::segment(2)}}">SKIP</a></b>
	        @elseif($req_amount->RequestType == 'delivery')
	        <b class="color-blue"><a href="{{url('send-package-detail')}}/{{Request::segment(2)}}">SKIP</a></b>
	        @elseif($req_amount->RequestType == 'buy_for_me')
	        <b class="color-blue"><a href="{{url('buy-for-me-detail')}}/{{Request::segment(2)}}">SKIP</a></b>
	        @elseif($req_amount->RequestType == 'local_delivery')
	        <b class="color-blue"><a href="{{url('local-delivery-detail')}}/{{Request::segment(2)}}">SKIP</a></b>
	        @endif
	        &nbsp;to pay later.
			</p>
			ADD CARD
		@endif

	<div class="clearfix"></div>
		<span id="card_msg"></span>
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="box-shadow clearfix">
				<br/>
				<div class="col-md-12 col-sm-6 col-xs-12" >
                 </div>


				@if(count($mobileMoney) > 0)
					@foreach($mobileMoney as $key)
					<div class="col-md-4 col-sm-6 col-xs-12" id="card_{{$key['id']}}">
						<div class="card-wrapper">
                        <div class="custom-btn">
                            @if($key['mobile_type'] == 'vodafone')
                              <a data-whatever="@mdo" mobileid="{{$key['id']}}"  onclick="return Vodaphone('{{$key['id']}}')" id="vodafoneid"  data-target="#modal3" data-toggle="modal" href="" ><span>PAY NOW</span></a>
                            @elseif($key['mobile_type'] == 'mtn')
                            	<a data-whatever="@mdo" mobileid="{{$key['id']}}"  href="#" onclick="return mtn_submit('{{$key->_id}}')"><span>PAY NOW</span></a>
                            	<a id="mtn_{{$key->_id}}" style="display: none;" href="{{url('mobile-payment/'.Request::segment(2))}}/{{$key['id']}}"></a>
                            @else
								<a class="btn" id="paynow" href="{{url('mobile-payment/'.Request::segment(2))}}/{{$key['id']}}" onclick="return show_btn()">PAY NOW</a>
                            @endif

						</div>
						<a class="deletebtn" title="Delete card" onclick="return confirm('Are you sure you want to delete this Mobile number?')" href="{{url('delete-mobile/'.$key['_id'].'/'.Request::segment(2))}}"><i class="fa fa-trash"></i></a>

								<div class="media">
									<div class="media-left">
										<a href="#">
											@if($key['mobile_type'] == 'airtel')
											<img height="30px" width="50px" src="{{ImageUrl}}/webcard/airtel_money.png">
											@elseif($key['mobile_type'] == 'mtn')
											<img height="30px" width="50px" src="{{ImageUrl}}/webcard/mtn.png">
											@elseif($key['mobile_type'] == 'tigo')
											<img height="30px" width="50px" src="{{ImageUrl}}/webcard/tigo.png">
											@elseif($key['mobile_type'] == 'vodafone')
											<img height="30px" width="50px" src="{{ImageUrl}}/webcard/vodafone.png">
											@endif
										</a>
									</div>
									<div class="media-body">
										<p class="media-heading color-blue" style="font-weight:bold">xxxxxx-{{$key['alternet_moblie']}}</p>
										<p>Pricing may change if the actual item weight was not entered.</p>
									</div>
								</div>
							</div>
						</div>
					@endforeach
				@else
							<div class="col-md-12">
							</div>
					@endif



				<div class="col-md-4 col-sm-6 col-xs-12">
                 	<div class="card-wrapper gray-card">
						<a data-whatever="@mdo" data-target="#modal2" data-toggle="modal" href="" ><span><i class="fa fa-plus"></i>Add Mobile No.</span></a>
                   </div>
				</div>



			</div>
		</div>

    </div>
</div>
<button id="modal_open" data-target="#modal4" data-toggle="modal" style="display: none;">&nbsp;</button>
<script>

/*$(function () {
    $('a').on("click", function (e) {


    });
});*/

/*$(document).ready(function(){
	alert('ok');
	e.preventDefault();
});*/

function show_btn()
{
	checked =$('#exist').prop('checked'); // true
	//checked = $("input[type=checkbox]:checked").length;

	if(checked == 0){
		alert('Please read and accept statement');
		//return false;
		flage=false;
		$("#statementPtag").addClass("statement-style");
	}else if(checked == 1)
	{
		//return true;
		flage=true;
		$("a.btn").addClass('disabled');
	}

	return flage;

}

function mtn_pay(){
	
	var url = $("#mtn_url").val();
	document.location.href = url;
}

function mtn_submit(id){
	checked =$('#exist').prop('checked');
	if(checked == 1){
		$("#mtn_modal_sub_bt").addClass('spinning');
		var mtnid = $('#mtnid').val();
		$.ajax({
	      url     : SITEURL+'mobile-payment-mtn/{{Request::segment(2)}}/'+id,
	      type    : 'GET',
	      data    : '',
	      success : function(res){
	        
	        if(res.success == 1){
	        	//alert(res.msg);
	        	$("#mtn_url").val(SITEURL+res.url);
	        	$("#modal_open").trigger('click');
	        }else{
	        	alert(res.msg);
	        }
	        $("#mtn_modal_sub_bt").removeClass('spinning');
	      }
	    });
	}else{
		alert('Please read and accept statement');
		$("#statementPtag").addClass("statement-style");
		//$("#mtn_modal_bt").trigger('click');
	}
		

}




function delete_card(cardId){
{
if(confirm("Are you sure you want to delete this card?")){
	$("#card_"+cardId).addClass('spinning');
  if(cardId != ""){
    $.ajax({
      url     : '{{ url("remove-creditcard")}}',
      type    : 'post',
      data    : 'cardid='+cardId,
      success : function(res){
        $("#card_"+cardId).removeClass('spinning');
        var obj = eval("("+res+")");
        alert_message('card_msg',obj.success,obj.msg);
        if(obj.success == 1){
			$("#apend_saved_card").html(obj.new_cardlist);
        }if(obj.success == 2)
        {
			alert_message('card_msg',0,obj.msg);
		}

      }
    });
  }
  else{
    alert("Invalid card");
  }
}

}
}
	</script>
@endsection
<!--card modal-->
	<div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title color-white" id="myModalLabel">&nbsp;&nbsp;&nbsp;Add Mobile Number</h4>
          </div><div class="clearfix"></div>

           {!! Form::model('', ['name' => 'Addcard', 'id' => 'Addcard', 'method' => 'post', 'url'=> ['add-mobile-number/'.Request::segment(2)]]) !!}


          <input type="hidden" name="requestid" value="{{Request::segment(2)}}">
          <div class="modal-body">

          		<div class=col-md-12 id="succesdiv"></div>
				<div id='errordiv' class="col-sm-12"></div>

			<div class="form-group clearfix">
				<div class="col-sm-12">
					{!! Form::label('Select Country Code', 'Select Country code',['class'=>'color-black']) !!}
					{!! Form::select('country_code', ['+233'=>'+233','1'=>'+1','44'=>'+44'],Input::get('country_code'),['class'=>'form-control required usename-#country_code#"','id'=>'country_code']) !!}
                 <p class="help-block white" id='er_name_card'></p>
				</div>
			</div>

			<div class="col-sm-12">
               <div class="row">
                <div class="col-sm-12">
                <div class="form-group">
                {!! Form::label('Select Mobile Carrier', 'Select Mobile Carrier',['class'=>'color-black']) !!}
                 {!! Form::select('type', [''=>'Select Mobile Carrier','mtn'=>'MTN','airtel'=>'Airtel','tigo'=>'Tigo','vodafone' => 'Vodafone'],Input::get('type'),['class'=>'form-control required usename-#type#"','id'=>'type']) !!}
                 <p class="help-block white" id='type'></p>
                </div>
                </div>

                </div>
            </div>


			<div class="form-group clearfix">
				<div class="col-sm-12">
				   {!! Form::label('Mobile Number', 'Mobile Number',['class'=>'color-black']) !!}
					  {!! Form::text('mobile_number', '', ['class'=>'form-control required numeric','maxlength'=>'10	' , 'placeholder'=> 'Mobile Number','id' =>'mobile_number']) !!}
					  <p class="help-block white" id='mobile_number'></p>
				</div>
			</div>



          </div><div class="clearfix"></div>
          <br />
			<div class="modal-footer">
			  <div class="col-sm-12 text-right">
			  <div class="">
				{!! Form::button('Close ', ['class' => 'custom-btn1 btn', 'data-dismiss'=>'modal']) !!}
                {!! Form::button('Sumbit', ['class' => 'custom-btn1 btn','id'=>'addbutton','type'=>'submit']) !!}
				</div>
			  </div>
			</div>

			{!! Form::close() !!}
        </div>
      </div>
    </div>

    <div class="modal fade" id="modal4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    	<div class="modal-dialog">
    		<div class="modal-content">
    			<div class="modal-header">
            		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            		<h4 class="modal-title color-white" id="myModalLabel">&nbsp;&nbsp;&nbsp;MTN</h4>
          		</div>

          		<div class="clearfix"></div>
          		<Input type="hidden" value="" id="mtnid">
          		<Input type="hidden" value="" id="mtn_url">
          		<div class="modal-body">
          		<h4>Please follow these steps:</h4>
          		<table>
          			<tr>
          				<td><p>1.</p></td><td><p>Dial *170#</p></td>
          			</tr>
          			<tr>
          				<td><p>2.</p></td><td><p>Choose Option: 10) Wallet</p></td>
          			</tr>
          			<tr>
          				<td><p>3.</p></td><td><p>Choose Option: 3): My Approvals</p></td>
          			</tr>
          			<tr>
          				<td><p>4.</p></td><td><p>Enter your MOMO Pin to retrieve your pending approval list</p></td>
          			</tr>
          			<tr>
          				<td><p>5.</p></td><td><p>Choose a pending transaction</p></td>
          			</tr>
          			<tr>
          				<td><p>6.</p></td><td><p>Choose Option 1 to approve</p></td>
          			</tr>
          			<tr>
          				<td><p>7.</p></td><td><p> Tap button to continue</p></td>
          			</tr>
          		</table>
          		
          		</div>
          		<div class="clearfix"></div>
          		<div class="modal-footer">
          			<button id="mtn_modal_sub_bt" class="custom-btn1 btn" onclick="return mtn_pay()" >OK</button>
          			<!-- <button id="mtn_modal_bt" class="custom-btn1 btn" data-dismiss='modal'>Cancel</button> -->
          		</div>
          		
    		</div>
    	</div>
    </div>


    <div class="modal fade" id="modal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title color-white" id="myModalLabel">&nbsp;&nbsp;&nbsp;Voucher Code</h4>
          </div><div class="clearfix"></div>

          {!! Form::model('', ['name' => 'AddVoucher', 'id' => 'AddVoucher', 'method' => 'post', 'url'=> ['Voucher-mobile-payment/'. Request::segment(2)],'data-parsley-validate']) !!}
          <div class="modal-body">
      		<div class=col-md-12 id="succesdiv"></div>
			<div id='errordiv' class="col-sm-12"></div>

            <div class="form-group clearfix">
            <div class="col-sm-12">
		    <label>To get your voucher, dial *110# on your Vodafone Cash phone, select Generate Voucher and follow the prompt</label>
		    </div>
			</div>
			<div class="form-group clearfix">
				<div class="col-sm-12">
				 <input type="hidden" value="" name="voucherid" id="vodafoneId">
				   {!! Form::label('Voucher', 'Voucher',['class'=>'color-black']) !!}
					  {!! Form::text('Voucher', '', ['class'=>'form-control required numeric','maxlength'=>'10	' , 'placeholder'=> 'Voucher','id' =>'Voucher','required' => '','data-parsley-required-message' =>'Voucher code is required.']) !!}
					   @if($errors->has('Voucher'))
                     <p class="controll-error help-block"><i class="fa fa-times-circle-o"></i>&nbsp;
                        {{ $errors->first('Voucher') }}
                     </p>
                     @endif
				</div>
			</div>

          </div><div class="clearfix"></div>
          <br />
			<div class="modal-footer">
			  <div class="col-sm-12 text-right">
			  <div class="">
				{!! Form::button('Close ', ['class' => 'custom-btn1 btn', 'data-dismiss'=>'modal']) !!}
                {!! Form::button('Sumbit', ['class' => 'custom-btn1 btn','id'=>'addbutton2','type'=>'submit']) !!}
				</div>
			  </div>
			</div>

			{!! Form::close() !!}
        </div>
      </div>
    </div>

    @section('script')
@parent
{!! Html::script('theme/web/js/parsley.min.js') !!}
{!! Html::script('theme/web/js/utility.js') !!}
{!! Html::script('theme/web/js/validation.js') !!}
{!! Html::script('theme/web/js/request.js') !!}


@endsection

@section('inline-script')
@parent
 <script>

new Validate({
	FormName : 'Addcard',
	ErrorLevel : 1,
	});

$('#AddVoucher').parsley().subscribe('parsley:form:validated', function (parsleyForm) {
  	if (true === parsleyForm.validationResult ){
      $('#addbutton2').addClass('spinning');
      $('#addbutton2').prop('disabled', true);
   	}else{
   		$('#addbutton2').removeClass('spinning');
   	}

});

	 function Vodaphone(id){
	 	$('#vodafoneId').val('');
        $('#vodafoneId').val(id);
	 }
/*$('#paynow').click(function(){
    $(this).addClass("disabled");
});*/


</script>


@endsection
