<!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Item List</h4>
        </div>
        <div class="modal-body">
           <table id="example" class="table table-striped table-bordered" width="100%" cellspacing="0">
             <tr>
                <th>S.No.</th>
                <th>Item Name</th>
                <th>Price</th>
                <th>Aquantuo shipping</th>
             <tr>
             <?php $sno = 1;
$sum = 0;?>
            @foreach($product as $value)
              <tr>
                <td><?php echo $sno++; ?> </td>
                <td>{{ucfirst($value['product_name'])}}</td>
                <td>${{number_format($value['price'],2)}}</td>
                <td>${{number_format(@$value['shippingCost'],2)}}</td>
             <tr>
             <?php $sum = $sum + $value['price'];?>
             @endforeach
           </table>
            <div class="row">
             <div class="col-sm-4">
            	<label>Total item price: </label>
             </div>
             <div class="col-sm-8">
             <?php echo "$" . number_format($sum, 2); ?>
             	<br>
             </div>
            </div>

            <div class="row">
            <div class="col-sm-4">
            	<label>Aquantuo Charge: </label>
            </div>
            <div class="col-sm-8">
             	${{number_format(@$shipping_cost,2)}}
            </div>
            </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>





@if(count($error) > 0)

<div class="row custom-row">
	@foreach($error as $key)
   	<div class="col-sm-12"><p class="color-red">{{$key}}</p></div>
   	<p></p><p></p>
   	@endforeach
</div>

@else



<div class="row custom-row">
   <div class="col-sm-6"><b>No. of Items -</b></div>
   <div class="col-sm-6">{{count($product)}}</div>
</div>

<div class="row custom-row">
   <div class="col-sm-6"><b><a data-toggle="modal" style="cursor:pointer" data-target="#myModal">Int'l Shipping Cost -</a></b></div>
   <div class="col-sm-6">${{number_format($shipping_cost,2)}}</div>
</div>

@if($insurance > 0)
<div class="row custom-row">
   <div class="col-sm-6"><b>Insurance Cost -</b></div>
   <div class="col-sm-6">${{number_format($insurance,2)}}</div>
</div>
@endif

<div class="row custom-row">
   <div class="col-sm-6"><b>Region Charge -</b></div>
   <div class="col-sm-6">${{number_format($AreaCharges,2)}}</div>
</div>

<div class="row custom-row">
   <div class="col-sm-6"><b>Aquantuo Fess -</b></div>
   <div class="col-sm-6">${{number_format($ProcessingFees,2)}}</div>
</div>



<div class="row"> <hr /> </div>
<div class="row custom-row">
   <div class="col-sm-6"><b>Total Amount -</b></br>
   <small id="olp_promo_ghana">{{str_replace('[AMT]',number_format($ghana_total_amount,2),$formated_text)}}</small>


    </div>
   <div class="col-sm-6">${{number_format(($total_amount),2)}}</div>
</div>

<div class="row">
	<hr>
	<div class="col-sm-12">
		<div class="form-group reward-group" id="olp_promo_input">
			<input type="text" placeholder="Promotion Code" class="form-control pull-left" id="promocode" name="promocode" >
			<a class="btn default-btn" onclick="check_promocode('promocode',shipping_cost,'olp_promo',total_amount)" href="javascript:void(0)">
				<img src="{{url('theme/web/promo/images/green_check.png')}}" />Apply
			</a>
			<div class="clearfix"></div>
			<div  id="er_promocode" class="color-red"></div>
		</div>
		<div class="form-group reward-group" id="olp_promo" style='display: none;'>
			<span class="" onclick="remove_promocode('olp_promo','promocode')"><i class="fa fa-trash"></i></span><span id="olp_promo_msg"></span>
		</div>
	</div>
</div>
</div>

	<div class="row"> <hr /> </div>
		<div class="row custom-row">
			<div class="col-sm-6"><b>Payable -</b></br>
		<small>
			<div id="olp_promo_payable_ghana">
				{{str_replace('[AMT]',number_format($ghana_total_amount,2),$formated_text)}}
			</div>
		</small>
		</div>
		<div class="col-sm-6"><div id="olp_promo_payable">${{number_format($total_amount,2)}}</div></div>
</div>

</div>

<div class="row"> <hr /> </div>

@endif

