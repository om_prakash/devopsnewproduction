@if(!isset($calculationinfo->error))
<div class="row custom-row">
   <div class="col-sm-6"><b>Distance:</b></div>
   <div class="col-sm-6">{{number_format($calculationinfo->distance,2)}} Miles</div>
</div>
<div class="row custom-row">
   <div class="col-sm-6"><b>Weight:</b></div>
   <div class="col-sm-6">{{number_format($calculationinfo->weight,2)}} Lbs</div>
</div>
<div class="row custom-row"> 
   <div class="col-sm-6"><b>Volume:</b></div>
   <div class="col-sm-6">{{$calculationinfo->showVolume}} Cu.{{ucfirst($calculationinfo->widthUnit)}}</div>
</div>
<div class="row custom-row">
   <div class="col-sm-6"><b>Shipping Cost:</b></div>
   <div class="col-sm-6">${{number_format($calculationinfo->shippingcost,2)}}</div>
</div>
@if($calculationinfo->insurance > 0)
<div class="row custom-row">
   <div class="col-sm-6"><b>Insurance:</b></div>
   <div class="col-sm-6">${{number_format($calculationinfo->insurance,2)}}</div>
</div>
@endif
<div class="row"> <hr /> </div>
<div class="row custom-row">
   <div class="col-sm-6"><b>Total Amount:</b></br>
   <small>{{$calculationinfo->formated_currency}}</small>

    </div>
   <div class="col-sm-6">${{number_format(($calculationinfo->shippingcost + $calculationinfo->insurance),2)}}</div>
</div>

<div class="row">
	<hr>
	<div class="col-sm-12">
		<div class="form-group reward-group" id="olp_promo_input">
			<input type="text" placeholder="Promotion Code" class="form-control pull-left" id="promocode" name="promocode" >
			<a class="btn default-btn" onclick="check_promocode('promocode',shipping_cost,'olp_promo',total_amount)" href="javascript:void(0)">
				<img src="{{url('theme/web/promo/images/green_check.png')}}" />Apply
			</a>
			<div class="clearfix"></div>
			<div  id="er_promocode" class="color-red"></div>
		</div>
		<div class="form-group reward-group" id="olp_promo" style='display: none;'>
			<span class="" onclick="remove_promocode('olp_promo','promocode')"><i class="fa fa-trash"></i></span><span id="olp_promo_msg"></span>
		</div>
	</div>
</div>

</div>

	<div class="row"> <hr /> </div>
		<div class="row custom-row">
			<div class="col-sm-6"><b class="color-blue">Estimated Cost:</b></br>
		<small id="olp_promo_payable_ghana">{{$calculationinfo->formated_currency}}</small>
		</div>
		<div class="col-sm-6 color-blue" id="olp_promo_payable">${{number_format(($calculationinfo->shippingcost + $calculationinfo->insurance),2)}}</div>
</div>

	<!--<small class="color-red">You wil be billed or refunded any differences in price or shipping at time of purchase</small>-->

<div class="row"> <hr /> </div>

@else
<div class="row custom-row">
   <div class="col-sm-12"><p class="color-red" align="center">{{$calculationinfo->error}}</p></div>
   <p></p><p></p>
</div>

@endif
