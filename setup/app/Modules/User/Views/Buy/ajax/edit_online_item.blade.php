

                 
<tr >
<th>S.No.</th>
<th>Item Name</th>
<th>Item Price</th>
<th>Item Weight</th>
<th>Quantity</th>
<th>Description</th>
<th>Action</th>
</tr>
<tr>
<?php  $sno=1;   ?>
@foreach($items as $key)

   <tr id="row-{{$key['_id']}}">
      <td>{{$sno++}}</td>
      <td>{{ucfirst($key['product_name'])}}</td>

      <td>${{number_format((float)$key['price'],2)}}</td>

      <td>
      @if($key['weight'] != '')
      {{$key['weight']}}&nbsp;{{ucfirst($key['weight_unit'])}}
      @else
      N/A
      @endif
      </td>

      <td>{{$key['qty']}}</td>
    
      <td>{{ucfirst($key['description'])}}</td>
      <td colspan='2' >&nbsp;&nbsp;<a type=""  data-toggle="modal" onclick="get_item_info('{{$key['_id']}}')"  id="Edit" title="Edit" data-whatever="@mdo" href="#item_modal"><i class="fa fa-pencil"></i></a>&nbsp;
                <a title="delete" id="Delete"  onclick="remove_record('delete_item/<?php echo $key['_id']; ?>/DeleteItem','<?php echo $key['_id']; ?>','<?php echo Input::get('request_id'); ?>')" href="javascript:void(0)"><i class='fa fa-trash'></i></a>
            </td>
   </tr>
  
@endforeach
