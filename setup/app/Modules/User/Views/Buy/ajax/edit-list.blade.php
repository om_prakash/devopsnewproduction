<tr >
  <th>Purchase Form</th>
  <th>Item Price</th>
  <th>Item Weight</th>
  <th>Quantity</th>
  <th>Description</th>
  <th>Action</th>
</tr>
<tr>
  @foreach($items as $key)
  
   <tr id="row-{{$key->_id}}">
      <td>{{ucfirst($key->product_name)}}</td>

      <td>${{number_format((float)$key->price,2,'.','')}}</td>

      <td>
        @if($key->weight != '')
        {{$key->weight}}&nbsp;{{ucfirst($key->weight_unit)}}
        @else
        N/A
        @endif
      </td>

      <td>{{$key->qty}}&nbsp;Nos</td>
    
      <td>{{ucfirst($key->description)}}</td>
      <td colspan='2' >&nbsp;&nbsp;<a type=""  data-toggle="modal" onclick="get_edit_item('{{json_encode($key)}}')" id="Edit" title="Edit" data-whatever="@mdo" href="#item_modal"><i class="fa fa-pencil"></i></a>&nbsp;
	        	    <a title="delete" id="Delete"  onclick="remove_record('delete_item/{{$key->_id}}/DeleteItem','{{$key->_id}}')" href="javascript:void(0)"><i class='fa fa-trash'></i></a>
						</td>
   </tr>
  
@endforeach




                                                 