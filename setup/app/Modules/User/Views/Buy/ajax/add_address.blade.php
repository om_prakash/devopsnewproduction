@if(count($address) > 0)
  @foreach($address as $key)  
  <div class="col-md-6 col-sm-12" id="row-{{$key->_id}}">
    <div class="gray-bg address-btn-box">
      <div class="media addres-box">
        <div class="media-left">
        <a href="#">
        <img src="theme/web/promo/images/map-list.png" /> 
        </a>
        </div>
          <div class="media-body">
          <br/>
          <h4 class="media-heading">{{ucfirst($key->address_line_1)}}</h4>
          <h4 class="media-heading">{{ucfirst($key->address_line_2)}}</h4>
          <p>{{$key->country}}. {{$key->state}}<br/>{{$key->city}}, {{$key->zipcode}}</p>
          </div>
      </div>
        <div class="btn-box"><div><a onclick="remove_record('delete_address/{{$key->_id}}/Deleteaddress','{{$key->_id}}')"  title="Delete" href="javascript:void(0)"><i class="fa fa-trash"></i></a>
              <a  type=""  title="Edit" data-toggle="modal"   onclick="get_edit_address('{{json_encode($key)}}')"
              data-whatever="@mdo" href="#address_modal" ><i class="fa fa-pencil"></i></a>
        </div>
        </div>
    </div>
  </div>

@endforeach 
@else
  <div class="col-sm-12 box-shadow">
    <br><div align="center" class=""><b>No Recored Found</b></div><br>
  </div>
@endif
<div class="clearfix"></div>  
<br /><br />