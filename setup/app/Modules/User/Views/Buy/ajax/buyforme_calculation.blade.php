<!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Item List</h4>
        </div>
        <div class="modal-body">
           <table id="example" class="table table-striped table-bordered" width="100%" cellspacing="0">
             <tr>
                <th>S.No.</th>
                <th>Item Name</th>
                <th>Price</th>
                <th>Aquantuo shipping</th>
             <tr>
             <?php $sno = 1;?>
            @foreach($product as $value)
              <tr>
                <td><?php echo $sno++; ?> </td>
                <td>{{ucfirst($value['product_name'])}}</td>
                <td>${{number_format($value['price'],2)}}</td>
                <td class="price-show doller">${{number_format(@$value['shippingCost'],2)}}</td>
                
                <td class="price-show canada" style="display:none"  >Can${{number_format(($value['shippingCost']*$CADRate),2)}}</td>
				<td class="price-show ghana" style="display:none"  >GHS {{number_format(($value['shippingCost']*$GHSRate),2)}}</td>
				<td class="price-show uk" style="display:none"  >£{{number_format(($value['shippingCost']*$GBPRate),2)}}</td>
				<td class="price-show kenya" style="display:none"  >KES {{number_format(($value['shippingCost']*$KESRate),2)}}</td>
            
                
             <tr>
             @endforeach
           </table>
            <div class="row">
             <div class="col-sm-4">
            	<label>Total item cost: </label>
             </div>
				<div class="col-sm-8 price-show doller">${{number_format($total_item_cost,2)}}<br></div>
				
				<div class="col-sm-8 price-show canada" style="display:none"  >Can${{number_format(($total_item_cost*$CADRate),2)}}</div>
				<div class="col-sm-8 price-show ghana" style="display:none"  >GHS {{number_format(($total_item_cost*$GHSRate),2)}}</div>
				<div class="col-sm-8 price-show uk" style="display:none"  >£{{number_format(($total_item_cost*$GBPRate),2)}}</div>
				<div class="col-sm-8 price-show kenya" style="display:none"  >KES {{number_format(($total_item_cost*$KESRate),2)}}</div>
            </div>

            <div class="row">
				<div class="col-sm-4">
					<label>Aquantuo Charge: </label>
				</div>
				<div class="col-sm-8 price-show doller">${{number_format($shippingCost,2)}}</div>
				
				<div class="col-sm-8 price-show canada" style="display:none"  >Can${{number_format(($shippingCost*$CADRate),2)}}</div>
				<div class="col-sm-8 price-show ghana" style="display:none"  >GHS {{number_format(($shippingCost*$GHSRate),2)}}</div>
				<div class="col-sm-8 price-show uk" style="display:none"  >£{{number_format(($shippingCost*$GBPRate),2)}}</div>
				<div class="col-sm-8 price-show kenya" style="display:none"  >KES {{number_format(($shippingCost*$KESRate),2)}}</div>
            </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>






@if(count($error) > 0)

<div class="row custom-row">
	@foreach($error as $key)
   	<div class="col-sm-12"><p class="color-red">{{$key}}</p></div>
   	<p></p><p></p>
   	@endforeach
</div>

@else


<div class="row custom-row">
   <div class="col-sm-6"><b>Show Currency In:</b></div>
   
   <div class="col-sm-6">
	<select onchange="currencyChange(this)" class="form-control required" id="currency" name="currency">
		<option value="USD" selected="selected">USA - $</option>
		<option value="GBP">UK - £</option>
		<option value="GHS" >Ghana - GHS</option>
		<option value="CAD">Canada - C$</option>
		<option value="KES">Kenya - KES</option>
	</select>
   </div>
</div>

<div class="row custom-row">
   <div class="col-sm-6"><b>No. of Items:</b></div>
   <!-- <div class="col-sm-6">{{count($product)}}</div> -->
   <div class="col-sm-6">{{$product_count}}</div>
</div>

<div class="row custom-row" id="totalWeightInKg" style="display: none;">
  <div class="col-sm-6">
    <b> Total Weight: </b>
  </div>
  <div class="col-sm-6"> {{ $totalWeightInKg }} Kg </div>
</div>

<div class="row custom-row" id="totalWeightInLbs" style="display: none;">
  <div class="col-sm-6">
    <b> Total Weight: </b>
  </div>
  <div class="col-sm-6"> {{ $totalWeightInLbs }} lbs</div>
</div>

<div class="row custom-row" id="totalVolumeInKg" style="display: none;">
  <div class="col-sm-6">
    <b> Total Volume: </b>
  </div>
  <div class="col-sm-6"> {{ $totalVolumeInKg }} Cu.cm </div>
</div>

<div class="row custom-row" id="totalVolumeInLbs" style="display: none;">
  <div class="col-sm-6">
    <b> Total Volume: </b>
  </div>
  <div class="col-sm-6"> {{ $totalVolumeInLbs }} Cu.in </div>
</div>

<div class="row custom-row">
   <div class="col-sm-6"><b>Int'l Shipping Cost: </b></div>
   <div class="col-sm-6 price-show doller">${{number_format($shippingCost,2)}}</div>
   
   <div class="col-sm-6 price-show canada" style="display:none"  >Can${{number_format(($shippingCost*$CADRate),2)}}</div>
   <div class="col-sm-6 price-show ghana" style="display:none"  >GHS {{number_format(($shippingCost*$GHSRate),2)}}</div>
   <div class="col-sm-6 price-show uk" style="display:none"  > £{{number_format(($shippingCost*$GBPRate),2)}}</div>
   <div class="col-sm-6 price-show kenya" style="display:none"  > KES {{number_format(($shippingCost*$KESRate),2)}}</div>
</div>

<div class="row custom-row">
   <div class="col-sm-6">
   		<b>Shipping Cost: </b> <br />
   		<small>(Retailer to Aquantuo's facility)</small>
   	</div>
   <div class="col-sm-6 price-show doller">${{number_format($shipping_cost_by_user,2)}}</div>
   
   <div class="col-sm-6 price-show canada" style="display:none"  >Can${{number_format(($shipping_cost_by_user*$CADRate),2)}}</div>
   <div class="col-sm-6 price-show ghana" style="display:none"  >GHS {{number_format(($shipping_cost_by_user*$GHSRate),2)}}</div>
   <div class="col-sm-6 price-show uk" style="display:none"  >£{{number_format(($shipping_cost_by_user*$GBPRate),2)}}</div>
   <div class="col-sm-6 price-show kenya" style="display:none"  >KES {{number_format(($shipping_cost_by_user*$KESRate),2)}}</div>
   
</div>



<div class="row custom-row">

  <div class="col-sm-6">
    <b>
      Total Item Cost:
      <!-- <a data-toggle="modal" style="cursor:pointer" data-target="#myModal" ></a> -->
    </b>
  </div>
   <div class="col-sm-6 price-show doller">${{number_format($item_cost,2)}}</div>
   
   <div class="col-sm-6 price-show canada" style="display:none"  >Can${{number_format(($item_cost*$CADRate),2)}}</div>
   <div class="col-sm-6 price-show ghana" style="display:none"  > GHS {{number_format(($item_cost*$GHSRate),2)}}</div>
   <div class="col-sm-6 price-show uk" style="display:none"  >£{{number_format(($item_cost*$GBPRate),2)}}</div>
   <div class="col-sm-6 price-show kenya" style="display:none"  >KES {{number_format(($item_cost*$KESRate),2)}}</div>
   
</div>

<div class="row custom-row">
   <div class="col-sm-6">
      <b>Processing Cost:</b> <br />

    </div>
   <div class="col-sm-6 price-show doller">${{number_format($ProcessingFees,2)}}</div>
   
    <div class="col-sm-6 price-show canada" style="display:none" >Can${{number_format(($ProcessingFees*$CADRate),2)}}</div>
   <div class="col-sm-6 price-show ghana" style="display:none" >GHS {{number_format(($ProcessingFees*$GHSRate),2)}}</div>
   <div class="col-sm-6 price-show uk" style="display:none">£{{number_format(($ProcessingFees*$GBPRate),2)}}</div>
   <div class="col-sm-6 price-show kenya" style="display:none">KES {{number_format(($ProcessingFees*$KESRate),2)}}</div>
   
</div> 
@if($insurance_cost > 0)
<div class="row custom-row">
   <div class="col-sm-6"><b>Insurance:</b></div>
   <div class="col-sm-6 price-show doller">${{number_format($insurance_cost,2)}}</div>
   
   <div class="col-sm-6 price-show canada" style="display:none" >Can${{number_format(($insurance_cost*$CADRate),2)}}</div>
   <div class="col-sm-6 price-show ghana" style="display:none" >GHS {{number_format(($insurance_cost*$GHSRate),2)}}</div>
   <div class="col-sm-6 price-show uk" style="display:none">£{{number_format(($insurance_cost*$GBPRate),2)}}</div>
   <div class="col-sm-6 price-show kenya" style="display:none">KES {{number_format(($insurance_cost*$KESRate),2)}}</div>
   
</div>
@endif
@if($DutyAndCustom > 0)
<div class="row custom-row">
   <div class="col-sm-6">
      <b>Duty/Customs Clearing:</b> <br />

    </div>
   <div class="col-sm-6 price-show doller">${{number_format($DutyAndCustom,2)}}</div>
   
    <div class="col-sm-6 price-show canada" style="display:none" >Can${{number_format(($DutyAndCustom*$CADRate),2)}}</div>
   <div class="col-sm-6 price-show ghana" style="display:none" >GHS {{number_format(($DutyAndCustom*$GHSRate),2)}}</div>
   <div class="col-sm-6 price-show uk" style="display:none">£{{number_format(($DutyAndCustom*$GBPRate),2)}}</div>
   <div class="col-sm-6 price-show kenya" style="display:none">KES {{number_format(($DutyAndCustom*$KESRate),2)}}</div>
   
</div>
@endif
@if($Tax > 0)
<div class="row custom-row">
   <div class="col-sm-6">
      <b>Tax:</b> <br />

    </div>
   <div class="col-sm-6 price-show doller">${{number_format($Tax,2)}}</div>
   
    <div class="col-sm-6 price-show canada" style="display:none" >Can${{number_format(($Tax*$CADRate),2)}}</div>
   <div class="col-sm-6 price-show ghana" style="display:none" >GHS {{number_format(($Tax*$GHSRate),2)}}</div>
   <div class="col-sm-6 price-show uk" style="display:none">£{{number_format(($Tax*$GBPRate),2)}}</div>
   <div class="col-sm-6 price-show kenya" style="display:none">KES {{number_format(($Tax*$KESRate),2)}}</div>
   
</div>
@endif
@if($AreaCharges > 0)
<div class="row custom-row">
   <div class="col-sm-6">
      <b>Region Charge:</b> <br />

    </div>
   <div class="col-sm-6 price-show doller">${{number_format($AreaCharges,2)}}</div>
   
   <div class="col-sm-6 price-show canada" style="display:none" >Can${{number_format(($AreaCharges*$CADRate),2)}}</div>
   <div class="col-sm-6 price-show ghana" style="display:none" >GHS {{number_format(($AreaCharges*$GHSRate),2)}}</div>
   <div class="col-sm-6 price-show uk" style="display:none">£{{number_format(($AreaCharges*$GBPRate),2)}}</div>
   <div class="col-sm-6 price-show kenya" style="display:none">KES {{number_format(($AreaCharges*$KESRate),2)}}</div>
   
</div>
@endif




<div class="row"> <hr /> </div>
<div class="row custom-row">
   <div class="col-sm-6"><b>Total Amount:</b></br>
   <!-- <small id="olp_promo_ghana"><?php //echo str_replace('[AMT]', number_format($ghana_total_amount, 2), $formated_text); ?></small> -->

   <small id="olp_promo_ghana"><?php echo $ghana_total_amount; ?></small>

    </div>
   <div class="col-sm-6 price-show doller">${{number_format(($total_amount),2)}}</div>
   
   <div class="col-sm-6 price-show canada" style="display:none" >Can${{number_format(($total_amount*$CADRate),2)}}</div>
   <div class="col-sm-6 price-show ghana" style="display:none" >GHS {{number_format(($total_amount*$GHSRate),2)}}</div>
   <div class="col-sm-6 price-show uk" style="display:none">£{{number_format(($total_amount*$GBPRate),2)}}</div>
   <div class="col-sm-6 price-show kenya" style="display:none">KES {{number_format(($total_amount*$KESRate),2)}}</div>
   
   
</div>


<div class="row">
	<hr>
	<div class="col-sm-12">
		<div class="form-group reward-group" id="olp_promo_input">
			<input type="text" placeholder="Promotion Code" class="form-control pull-left" id="promocode" name="promocode" >
			<a class="btn default-btn" onclick="check_promocode('promocode',shipping_cost,'olp_promo',total_amount)" href="javascript:void(0)">
				<img src="{{url('theme/web/promo/images/green_check.png')}}" />Apply
			</a>
			<div class="clearfix"></div>
			<div  id="er_promocode" class="color-red"></div>
		</div>
		<div class="form-group reward-group" id="olp_promo" style='display: none;'>
			<span class="" onclick="remove_promocode('olp_promo','promocode')"><i class="fa fa-trash"></i></span><span id="olp_promo_msg"></span>
		</div>
	</div>
</div>
</div>

	<div class="row"> <hr /> </div>
		<div class="row custom-row">
			<div class="col-sm-6"><b>Payable:</b></br>
		    <!-- <small><div id="olp_promo_payable_ghana"><?php //echo str_replace('[AMT]', number_format($ghana_total_amount, 2), $formated_text); ?></div></small> -->

        <small><div id="olp_promo_payable_ghana"><?php echo $ghana_total_amount; ?></div></small>


		</div>
		<div class="col-sm-6  price-show doller"><div id="olp_promo_payable">${{number_format(($total_amount),2)}}</div></div>
		
		<div class="col-sm-6 price-show canada" style="display:none" ><div id="olp_promo_payable">Can${{number_format(($total_amount*$CADRate),2)}}</div></div>
		<div class="col-sm-6 price-show ghana" style="display:none" ><div id="olp_promo_payable">GHS {{number_format(($total_amount*$GHSRate),2)}}</div></div>
		<div class="col-sm-6 price-show uk" style="display:none"><div id="olp_promo_payable"> £{{number_format(($total_amount*$GBPRate),2)}}</div></div>
		<div class="col-sm-6 price-show kenya" style="display:none"><div id="olp_promo_payable"> KES {{number_format(($total_amount*$KESRate),2)}}</div></div>
   
</div>

	<!-- <small class="color-red">You wil be billed or refunded any differences in price or shipping at time of purchase</small> -->
  <small class="color-red">If actual weight wasn’t provided, you may be billed or refunded any differences in price or shipping upon review</small>

<div class="row"> <hr /> </div>

@endif
<script>
$(document).ready(function () {
  $("#totalWeightInLbs").show();
  $("#totalVolumeInLbs").show();
});
function currencyChange(selectObject){
	 var value = selectObject.value;  
	 $(".price-show").hide();

	if(value=="CAD"){
		$(".canada").show();
    $("#totalWeightInLbs").show();
    $("#totalVolumeInLbs").show();
    $("#totalWeightInKg").hide();
    $("#totalVolumeInKg").hide();
	}

	if(value=="GBP"){
		$(".uk").show();
    $("#totalWeightInLbs").show();
    $("#totalVolumeInLbs").show();
    $("#totalWeightInKg").hide();
    $("#totalVolumeInKg").hide();
	}

	if(value=="GHS") {
		$(".ghana").show();
		$("#totalWeightInKg").show();
    $("#totalVolumeInKg").show();
    $("#totalWeightInLbs").hide();
    $("#totalVolumeInLbs").hide();
	}

  if(value=="USD") {
    $(".doller").show();
    $("#totalWeightInLbs").show();
    $("#totalVolumeInLbs").show();
    $("#totalWeightInKg").hide();
    $("#totalVolumeInKg").hide();
  }

if(value=="KES") {
  $(".kenya").show();
  $("#totalWeightInLbs").show();
  $("#totalVolumeInLbs").show();
  $("#totalWeightInKg").hide();
  $("#totalVolumeInKg").hide();
}
}
</script>
