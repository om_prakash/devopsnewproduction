@extends('Page::layout.one-column-page')
@section('content')


<div class="container">
   <style type="text/css">
       .Insurance_check{float: left; margin-top: 3px; margin-left: 5px;}
  </style>
   <div class="row">
   	  <div class="col-sm-12" id="send_a_package_start_position">
      <h2 class="color-blue mainHeading">Send A Package</h2>
      <br>
      </div>
      <div class="col-sm-12">
      	<div class="box-shadow">
        <h3 class="midHeading">Please complete the following fields</h3>
        <hr>
        <div class="row">
        	@if(isset($user_data))
          		<div class="col-sm-10 col-sm-offset-1 box-shadow" >

	          			<div class="col-sm-2">

				          	<?php $file_exists = false;?>
							@if(!empty($user_data->Image))

								@if(file_exists(BASEURL_FILE.$user_data->Image))
									<?php $file_exists = true?>
			               		<br>
			               		<img id="senior-preview" src="{{ ImageUrl.$user_data->Image}}"  width="100px" height="100px" />
			               		<br>&nbsp;&nbsp;
								@endif
			               	@endif
			               	@if(!$file_exists)
								 <br>
						            <img id="" src="{{ ImageUrl}}/no-image.jpg"  width="100px" height="100px" />
						         <br>&nbsp;&nbsp;
			               	@endif
		                </div>

		                <div class="col-sm-10">
								<br>


			                <p><b>{{$user_data->FirstName}}</b></p>

			                 <p><b>{{$user_data->City}}&nbsp; {{$user_data->State}}&nbsp; {{$user_data->Country}}</b></p>

			                 <?php $average = 0;?>
	                   		 @if($user_data->RatingCount > 0 && $user_data->RatingByCount > 0)
		                    	<?php
$value = $user_data->RatingCount / $user_data->RatingByCount;
$average = $value * 20;
?>

		                    @endif

		                    <div class="star-ratings-sprite pull-left">
				 				 <span style="width:<?php echo $average ?>%" class="star-ratings-sprite-rating"></span>
							</div>
							<br>

		                </div>

          		</div>



          	@endif
        <!-- Fillable -->
          <div class="col-sm-12">
             <div class="step_box four_step clearfix">
                <div class="step first selected" id="step1-header">
                   <div>1</div>
                   <p class="text-center colol-black">Pickup Address</p>
                </div>
                <div class="step inner" id="step2-header">
                   <div>2</div>
                   <p class="text-center colol-black">Drop Off Address</p>
                </div>
                <div class="step inner" id="step3-header">
                   <div>3</div>
                   <p class="text-center colol-black">Package Details</p>
                </div>
                <div class="step last " id="step4-header">
                   <div>4</div>
                   <p class="text-center colol-black">Payment</p>
                </div>
             </div>
          </div>




             <!-- <div id="sec1" >  -->
             {!! Form::open(['class'=>'form-vertical','name'=>'prepare_request_form','id' => 'prepare_request_form','files'=>'true']) !!}
            <input type="hidden" name="PickupLat" id="PickupLat">
            <input type="hidden" name="PickupLong" id="PickupLong">
            <input type="hidden" name="DeliveryLat" id="DeliveryLat">
            <input type="hidden" name="DeliveryLong" id="DeliveryLong">
            @if(count($transporter_data) > 0)
            <input type="hidden" name="tripid" id="tripid" value="{{$transporter_data->_id}}">
            <input type="hidden" name="transporter_id" id="transporter_id" value="{{$transporter_data->TransporterId}}">
            <input type="hidden" name="TransporterName" id="TransporterName" value="{{$transporter_data->TransporterName}}">
            @endif
              <div class="col-md-10 col-sm-offset-1" id="sec1" >
              	<div class="row">
	                  <div class="col-sm-8 col-xs-12">
	                     <div class="form-group">
	                        <label>Package Title</label>
	                        {!! Form::text('title','',['class'=>"form-control required usename-#package title#", 'placeholder'=>"Package Title",'maxlength' => 80]) !!}


	                     </div>
	                  </div>
	            </div>
	            <div class="row">
                  <div class="col-md-12 col-xs-12">
                    <h3 class="color-blue bottom-border">Pickup Address</h3>
                  </div>

                  <div class="col-sm-12 col-xs-12">
                      <div class="checkbox">
                          <label>
                          <span class="pull-left"><input type="checkbox" name="public_place"></span>
                                <span class="Insurance_check">This is a public place.</span>
                          </label>
                      </div>
                  </div>
	                  <div class="col-sm-4 col-xs-12">
	                     <div class="form-group">
	                        <label>Address Line 1</label>
	                        {!! Form::text('address_line_1','',['class'=>"form-control required",'placeholder'=>"Address",'maxlength' => 100,'id' => 'address_line_1'])  !!}
	                     </div>
	                  </div>
	                  <div class="col-sm-4 col-xs-12">
	                     <div class="form-group">
	                        <label>Address Line 2</label>
	                        {!! Form::text('address_line_2','',['class'=>"form-control",'placeholder'=>"Address",'maxlength' => 80,'id'=>'address_line_2'])  !!}
	                     </div>
	                  </div>
	              </div>
                  <div class="row">
	                  <div class="col-sm-4 col-xs-12">
	                     <div class="form-group">
	                        <label class="control-label">Country</label>
	                        <select name="country" class="form-control required" id="pp_pickup_country" onchange="get_state('pp_pickup_country','pp_pickup_state','pp_pickup_city','pp_pickup_state','')">
	                            <option value="">Select Country</option>
	                            @foreach($country as $key)
	                             <option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}","state_available":"{{$key->state_available}}"}'>{{$key->Content}}</option>
	                            @endforeach
	                        </select>
	                     </div>
	                  </div>
	                  <div class="col-sm-4 col-xs-12">
	                     <div class="form-group">
	                        <label class="control-label">State</label>
	                        <select name="state" class="form-control required left-disabled" id="pp_pickup_state" onchange="get_city('pp_pickup_state','pp_pickup_city','pp_pickup_city','')">
	                            <option value="">Select State</option>
	                        </select>
	                     </div>
	                  </div>
	                  <div class="col-sm-4 col-xs-12">
	                     <div class="form-group">
	                        <label class="control-label">City</label>
	                        <select  name="city" class="form-control required" id="pp_pickup_city">
	                            <option value="">Select City</option>
	                        </select>
	                     </div>
	                  </div>
	               </div>
                  <div class="row">
	                  <div class="col-sm-4 col-xs-12">
	                     <div class="form-group">
	                        <label>Zip Code/Postcode</label>
	                        {!! Form::text('zipcode','',['class'=>"form-control alpha-numeric  maxlength-8",'placeholder'=>"Zip Code/Postcode",'maxlength' => 8])  !!}
	                     </div>
	                  </div>

	                  <div class="col-sm-4 col-xs-12">
	                     <div class="form-group">
	                        <label>Pickup Date</label>
	     {{--             {!! Form::text('pickup_date','',['class'=>"form-control required usename-#date#",'placeholder'=>"Date","id"=>"pickup_date",'readonly'=>'true'])  !!} --}}

      <input type="text" class="form-control required usename-#date#" name="pickup_date"  id="pickup_date" placeholder="Date" readonly="readonly" value="{{ Input::old('pickup_date') }}" >

	                     </div>
	                  </div>
	              </div>


                  <div class="">
                     <hr>
                  </div>
                  <div class="">
                     <div class="form-group">
                        <button class="custom-btn1">
                           Next
                           <div class="custom-btn-h"></div>
                        </button>
                     </div>
                  </div>

              </div>
              <!-- End of first section -->
              <!-- Start second section -->
              <div class="row" id="sec2" style="display:none;">
                 <div class="col-md-10 col-sm-offset-1">
                 	<div class="">
	                    <div class="col-md-12 col-xs-12">
	                       <h3 class="color-blue">Drop Off Address</h3>
	                    </div>
	                    <div class="col-sm-4 col-xs-12">
	                       <div class="form-group">
	                          <label class="control-label">Address Line 1</label>
	                          <input name="drop_off_address_line_1" class="form-control required usename-#address line1#" placeholder="Address" name="drop_address" maxlength="100" id="drop_off_address_line_1">
	                       </div>
	                    </div>
	                    <div class="col-sm-4 col-xs-12">
	                       <div class="form-group">
	                          <label class="control-label">Address Line 2</label>
	                          <input name="drop_off_address_line_2" class="form-control usename-#address line2#" placeholder="Address" name="drop_address2"  maxlength="100" id="drop_off_address_line_2">
	                       </div>
	                    </div>
	                </div>
                    <div class="clearfix"></div>
                    <div class="">
	                  <div class="col-sm-4 col-xs-12">
	                     <div class="form-group">
	                        <label class="control-label">Country</label>
	                        <select name="drop_off_country" class="form-control required usename-#country#" id="pp_dropoff_country" onchange="get_state('pp_dropoff_country','pp_dropoff_state','pp_dropoff_city','pp_dropoff_state','')">
	                            <option value="">Select Country</option>
	                            @foreach($country as $key)
	                             <option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}","state_available":"{{$key->state_available}}"}'>{{$key->Content}}</option>
	                            @endforeach
	                        </select>
	                     </div>
	                  </div>
	                  <div class="col-sm-4 col-xs-12">
	                     <div class="form-group">
	                        <label class="control-label">State</label>
	                        <select name="drop_off_state" class="form-control required usename-#state# left-disabled" id="pp_dropoff_state" onchange="get_city('pp_dropoff_state','pp_dropoff_city','pp_dropoff_city','')">
	                            <option value="">Select State</option>
	                        </select>
	                     </div>
	                  </div>
	                  <div class="col-sm-4 col-xs-12">
	                     <div class="form-group">
	                        <label class="control-label">City</label>
	                        <select  name="drop_off_city" class="form-control required usename-#city#" id="pp_dropoff_city">
	                            <option value="">Select City</option>
	                        </select>
	                     </div>
	                  </div>
	               </div>
	               <div class="">
	                    <div class="col-sm-4 col-xs-12">
	                       <div class="form-group">
	                          <label class="control-label">Zip Code/Postcode</label>
	                          <input class="form-control alpha-numeric usename-#zipcode#" placeholder="Zip Code/Postcode" name="drop_off_zipcode" maxlength="8">
	                       </div>
	                    </div>
	                    <div class="col-sm-4 col-xs-12">
	                       <div class="form-group">
	                          <label class="control-label">Date</label>
	                          <input class="form-control required usename-#date#" placeholder="Date" name="drop_off_date" id="drop_off_date" readonly>
	                       </div>
	                    </div>
	                </div>
                    <div class="clearfix"> </div>
	                <div class="">
	                    <div class="col-sm-4 col-xs-12">
	                       <div class="form-group">
	                          <label class="control-label">Is Delivery Date Flexible?</label>
	                          	<div class="clearfix">

		                          <label class="radio-inline">
	                                 <span class="pull-left">
	                                 <input type="radio" name="is_delivery_date_flexible" value="yes" >
	                                 </span>
	                                    <span class="Insurance_check">Yes </span>
	                             </label>

	                             <label class="radio-inline">
	                                 <span class="pull-left">
	                                 <input type="radio" name="is_delivery_date_flexible" value="no" checked>
	                                 </span>
	                                    <span class="Insurance_check">No </span>
	                             </label>
	                           	</div>




		                          <!-- <div class="radio">
		                             <label>
		                             <input type="radio" name="is_delivery_date_flexible" value="yes" >
		                             </label>&nbsp;Yes
		                             &nbsp; &nbsp;
		                             <label>
		                             <input type="radio" name="is_delivery_date_flexible" value="no" checked>
		                             </label>&nbsp;No
		                          </div> -->
	                       </div>
	                    </div>
	                    <div class="col-sm-4 col-xs-12">
	                       <div class="form-group">
	                          <label class="control-label">Journey Type</label>

	                          <div class="clearfix">

		                          <label class="radio-inline">
	                                 <span class="pull-left">
	                                 <input type="radio" value="one_way" name="journey_type" checked onclick="$('#return_jurney_section').hide()">
	                                 </span>
	                                    <span class="Insurance_check">One Way </span>
	                             </label>

	                             <label class="radio-inline">
	                                 <span class="pull-left">
	                                 <input type="radio" value="return" name="journey_type" onclick="$('#return_jurney_section').show()">
	                                 </span>
	                                    <span class="Insurance_check">Return </span>
	                             </label>
	                           	</div>
	                         <!--  <div class="radio">
	                             <label>
	                             <input type="radio" value="one_way" name="journey_type" checked onclick="$('#return_jurney_section').hide()">
	                             </label>&nbsp;One Way
	                             &nbsp; &nbsp;
	                             <label>
	                             <input type="radio" value="return" name="journey_type" onclick="$('#return_jurney_section').show()">
	                             </label>&nbsp;Return
	                          </div> -->
	                       </div>
	                    </div>
	                </div>
	                <div id="return_jurney_section"  style="display:none;">
	                	<div class="">
		                	<div class="col-sm-12 col-xs-12">
		                       	<div class="form-group">
		                       		<h3 class="color-blue">Return Address</h3>
		                          	<div class="checkbox">

		                             <span class="pull-left">
		                             <input type="checkbox" id="return_same_as_pickup" name="return_same_as_pickup">
		                             </span>
		                               <span class="Insurance_check"> Same as pickup adddress</span>

		                          	</div>

		                          	<!-- <div class="col-xs-12 form-group">
                     					<span class="pull-left">
                     					</span>
                     					<span class="Insurance_check"> Insurance</span>
                  					</div> -->
		                       	</div>
		                    </div>
		                </div>
	                   	<div id="return_jurney_address">
			               	<div class="">
			                    <div class="col-sm-4 col-xs-12">
			                       <div class="form-group">
			                          <label class="control-label">Address Line 1</label>
			                          <input name="return_address_line_1" class="form-control required usename-#address line 1#" placeholder="Address" name="return_address" maxlength="100">
			                       </div>
			                    </div>
			                    <div class="col-sm-4 col-xs-12">
			                       <div class="form-group">
			                          <label class="control-label">Address Line 2</label>
			                          <input name="return_address_line_2" class="form-control usename-#address line 2#" placeholder="Address" name="return_address2" maxlength="100">
			                       </div>
			                    </div>
			                </div>
		                    <div class="">
			                  <div class="col-sm-4 col-xs-12">
			                     <div class="form-group">
			                        <label class="control-label">Country</label>
			                        <select name="return_country" class="form-control required usename-#country#" id="pp_return_country" onchange="get_state('pp_return_country','pp_return_state','pp_return_city','pp_return_state','')">
			                            <option value="">Select Country</option>
			                            @foreach($country as $key)
			                             <option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}","state_available":"{{$key->state_available}}"}'>{{$key->Content}}</option>
			                            @endforeach
			                        </select>
			                     </div>
			                  </div>
			                  <div class="col-sm-4 col-xs-12">
			                     <div class="form-group">
			                        <label class="control-label">State</label>
			                        <select name="return_state" class="form-control required usename-#state# left-disabled" id="pp_return_state" onchange="get_city('pp_return_state','pp_return_city','pp_return_city','')">
			                            <option value="">Select State</option>
			                        </select>
			                     </div>
			                  </div>
			                  <div class="col-sm-4 col-xs-12">
			                     <div class="form-group">
			                        <label class="control-label">City</label>
			                        <select  name="return_city" class="form-control required usename-#city#" id="pp_return_city">
			                            <option value="">Select City</option>
			                        </select>
			                     </div>
			                  </div>
			               </div>
			               <div class="">
			                    <div class="col-sm-4 col-xs-12">
			                       <div class="form-group">
			                          <label class="control-label">Zip Code/Postcode</label>
			                          <input class="form-control alpha-numeric usename-#zipcode#" placeholder="Zip Code/Postcode" name="return_zipcode" maxlength="8">
			                       </div>
			                    </div>
			                </div>
		                </div>
	             	</div>
					<div class="clearfix"></div>
	                <div class="">
	                	<div class="col-sm-12 col-xs-12">
	                       <div class="form-group">
	                       		<h3 class="color-blue">Return address(If item is not delivered)</h3>
	                          <div class="">
	                             <span class="pull-left">
	                             <input type="checkbox" id="same_as_pickup" name="same_as_pickup" checked>
	                             </span>
	                             <span class="Insurance_check">
	                              Same as pickup adddress
	                              </span>

	                          </div>
	                       </div>
	                    </div>
	                </div>

	                <div id="return_address_action" style="display:none;">
		               	<div class="">
		                    <div class="col-sm-4 col-xs-12">
		                       <div class="form-group">
		                          <label class="control-label">Address Line1</label>
		                          <input name="nd_return_address_line_1" class="form-control required usename-#address line 1#" placeholder="Address" maxlength="100">
		                       </div>
		                    </div>
		                    <div class="col-sm-4 col-xs-12">
		                       <div class="form-group">
		                          <label class="control-label">Address Line2</label>
		                          <input name="nd_return_address_line_2" class="form-control usename-#address line 2#" placeholder="Address" maxlength="100">
		                       </div>
		                    </div>
		                </div>
	                    <div class="">
		                  <div class="col-sm-4 col-xs-12">
		                     <div class="form-group">

		                        <label class="control-label">Country</label>
		                        <select name="nd_return_country" class="form-control required usename-#country#" id="pp_nd_return_country" onchange="get_state('pp_nd_return_country','pp_nd_return_state','pp_nd_return_city','pp_nd_return_state','')">
		                            <option value="">Select Country</option>
		                            @foreach($country as $key)
		                             <option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}","state_available":"{{$key->state_available}}"}'>{{$key->Content}}</option>
		                            @endforeach
		                        </select>
		                     </div>
		                  </div>
		                  <div class="col-sm-4 col-xs-12">
		                     <div class="form-group">
		                        <label class="control-label">State</label>
		                        <select name="nd_return_state" class="form-control required usename-#state# left-disabled" id="pp_nd_return_state" onchange="get_city('pp_nd_return_state','pp_nd_return_city','pp_nd_return_city','')">
		                            <option value="">Select State</option>
		                        </select>
		                     </div>
		                  </div>
		                  <div class="col-sm-4 col-xs-12">
		                     <div class="form-group">
		                        <label class="control-label">City</label>
		                        <select  name="nd_return_city" class="form-control required usename-#city#" id="pp_nd_return_city">
		                            <option value="">Select City</option>
		                        </select>
		                     </div>
		                  </div>
		               </div>
		               <div class="">
		                    <div class="col-sm-4 col-xs-12">
		                       <div class="form-group">
		                          <label class="control-label">Zipcode</label>
		                          <input class="form-control alpha-numeric usename-#zipcode#" placeholder="Zipcode" name="nd_return_zipcode" maxlength="6">
		                       </div>
		                    </div>
		                </div>
	             	</div>
                    <div class="clearfix"></div>
                    <div class="">
                       <hr>
                    </div>
                    <div class="col-sm-8 col-xs-12">
                    	<div class="">
			                     <div class="form-group">
			                        <button class="custom-btn1 btn text-center" id="step2-next-btn">
			                           Next
			                           <div class="custom-btn-h"></div>
			                        </button>
			                     </div>
			                <div class="col-xs-6 col-xs-12">
			                     <div class="form-group">
			                        <a class="custom-btn1 btn text-center" onclick="switch_request_header('#sec1','#sec2',1)" href="javascript:void(0)">
			                           Back
                                       <div class="custom-btn-h"></div>
			                        </a>
			                     </div>
			                </div>
	                  </div>
                    </div>

                 </div>
              </div>

              <!-- End section2 -->
              <!-- Start section3 -->

             <div class="row" id="sec3" style="display:none;">

                 <div class="col-md-10 col-sm-offset-1">
                    <!--  <form class="form-vertical">    -->
                    <div class="col-sm-6 col-xs-6">
	                    <div class="form-group">
	                          <label class="control-label">Package Value</label>
	                          <div class="input-group error-input">
	                          <span class="input-group-addon">$</span>
	                          <input class="form-control required float maxlength-7 " placeholder="Package Value" name="package_value" maxlength="9" >
	                          </div>
	                    </div>
	                </div>



                    <div class="col-sm-12 col-xs-12">
                    <br>
                       <div class="form-group">
                          <label class="control-label">Measurement Unit</label>
                          <div class="clearfix">
                          		<div class="radio">
	                             <label class="">
	                             	<input type="radio" name="measurement_unit"  value="cm_kg" onclick="return unit_show();">
	                             </label>&nbsp;Metric (Cm/Kg)
	                             <label class="">
	                             	<input type="radio" name="measurement_unit" checked value="inches_lbs" onclick="return unit_show();">
	                             </label>&nbsp;Imperial (Inches/Lbs)
	                           	</div>

                          </div>
                       </div>
                    </div>

                    <div class="col-md-2 col-sm-3 col-xs-12">
                       <div class="form-group">
                          <label class="control-label">Length</label>
                          <div class="input-group error-cm-input">
                          <input class="form-control required  float maxlength-9" placeholder="Length" name="length" maxlength="9">
                          <span class="input-group-addon" id="length_unit"></span>
                          </div>
                       </div>
                    </div>

                    <div class="col-md-2 col-sm-3 col-xs-12">
                       <div class="form-group">
                          <label class="control-label">Width</label>
                          <div class="input-group error-cm-input">
                          <input class="form-control required float maxlength-9" placeholder="Width" name="width" maxlength="9">
                          <span class="input-group-addon" id="width_unit"></span>
                          </div>
                       </div>
                    </div>
                    <div class="col-md-2 col-sm-3 col-xs-12">
                       <div class="form-group">
                          <label class="control-label">Height</label>
                          <div class="input-group error-cm-input">
                          <input class="form-control required float maxlength-9" placeholder="Height" name="height" maxlength="9">
                          <span class="input-group-addon" id="height_unit"></span>
                          </div>
                       </div>
                    </div>
                    <div class="col-md-2 col-sm-3 col-xs-12">
                       <div class="form-group">
                          <label class="control-label">Weight</label>
                          <div class="input-group error-cm-input">
                          <input class="form-control required float maxlength-9" placeholder="Weight" name="weight" maxlength="9">
                          <span class="input-group-addon" id="weight_unit"></span>
                          </div>
                       </div>
                    </div>
                    <div class="col-sm-12 col-xs-12">
		                       <div class="form-group">

		                          <label class="control-label">Quantity</label>


		                          	<div class="radio">
			                            <label class="">
			                             	<input type="radio" name="quantity_type" value="single" onclick="show_quantity();" checked="true">
			                            </label>&nbsp;Single
		                             	<label class="">
		                             		<input type="radio" name="quantity_type" value="multiple" onclick="show_quantity();">
		                             	</label>&nbsp;Multiple
                          			</div>


		                       </div>
		                    </div>
		                <div class="col-sm-12 col-xs-12" id="quantity_input">
                       		<div class="form-group">
		              		<input type="text" class="form-control required numeric" name="quantity" placeholder="Quantity" >
		              		</div>
		              	</div>


		              	@if(count($transporter_data) > 0)
		              		<div class="col-sm-12 col-xs-12">
	                       	<div class="form-group">
	                       	<input type="hidden" name="travel_mode" value="{{$transporter_data->TravelMode}}" >
	                    		<label class="control-label">Shipping Mode: </label>&nbsp;&nbsp;{{ucfirst($transporter_data->TravelMode)}}
	                    	</div>
	                    </div>

	                    @else
	                    <div class="col-sm-12 col-xs-12">
	                       <div class="form-group">
	                          <label class="control-label">Shipping Mode</label> &nbsp;&nbsp;(Not all items can be shipped by Air. Items shipped by sea typically take longer to arrive.)
	                          <div class="radio">
	                             <label class="">
	                             	<input type="radio" name="travel_mode" value="air" onclick="toggle_category('.travel-mode-air','.travel-mode-ship','#package_category')" checked="checked">
	                             </label>&nbsp;By Air
	                             <label class="">
	                             	<input type="radio" name="travel_mode" value="ship" onclick="toggle_category('.travel-mode-ship','.travel-mode-air','#package_category')">
	                             </label>&nbsp;By Sea
	                          </div>
	                       </div>
	                    </div>
	                    @endif

	                    @if(count($transporter_data) > 0)
	                    <div class="col-sm-12 col-xs-12">
		                       <div class="form-group">
		                          <label class="control-label">Select Package Category</label>
		                          <select class="form-control required" id="package_category" name="category">
		                             <option value="">Select Category</option>

		                             <?php foreach ($category2 as $key) {?>
		                             	<option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}"}' class="travel-mode-air">{{$key->Content}}</option>
		                             <?php }?>


		                          </select>
		                       </div>
		                    </div>

		                @else
		                	<div class="col-sm-12 col-xs-12">
		                       <div class="form-group">
		                          <label class="control-label">Select Package Category</label>
		                          <select class="form-control required" id="package_category" name="category">
		                             <option value="">Select Category</option>
		                             <?php foreach ($category as $key) {?>
		                             	<option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}"}' class="travel-mode-{{$key->TravelMode}}">{{$key->Content}}</option>
		                             <?php }?>
		                          </select>
		                       </div>
		                    </div>
		                @endif

                    <div class="col-sm-12 col-xs-12">
                       <div class="form-group">
                          <label class="control-label">Package Content/Description/Instructions</label>
                          <textarea rows="3" class="form-control" name="description"></textarea>
                       </div>
                    </div>
                   <!-- <div class="col-sm-6">
                       <div class="form-group">
                          <label class="control-label">Package care note</label>
                          <textarea rows="3" class="form-control required" name="care_note"></textarea>
                       </div>
                    </div>-->
                    <div class="col-sm-12 col-xs-12">
                       	<div class="form-group">
                          <label class="control-label">Upload Default Package Image</label>
                          <input type="file" placeholder="Browse" name="default_image" class="required valid-filetype-jpg,png,gif,jpeg,JPEG,JPG,PNG,GIF">
                       </div>
                    </div>
                    <div class="col-sm-12 col-xs-12">
                       <div class="form-group">
                          <label class="control-label">Upload Extra Image(s)(Optional)</label>
                          <div class="row">
	                          <div class="col-sm-3 col-xs-12">
	                          	<input type="file" placeholder="Browse" name="package_image2" class="valid-filetype-jpg,png,gif,jpeg,JPEG,JPG,PNG,GIF">
	                          </div>
	                          <div class="col-sm-3 col-xs-12">
	                          	<input type="file" placeholder="Browse" name="package_image3" class="valid-filetype-jpg,png,gif,jpeg,JPEG,JPG,PNG,GIF">
	                          </div>
	                          <div class="col-sm-3 col-xs-12">
	                          	<input type="file" placeholder="Browse" name="package_image4" class="valid-filetype-jpg,png,gif,jpeg,JPEG,JPG,PNG,GIF">
	                          </div>
	                          <div class="col-sm-3 col-xs-12">
	                          	<input type="file" placeholder="Browse" name="package_image5" class="valid-filetype-jpg,png,gif,jpeg,JPEG,JPG,PNG,GIF">
	                          </div>
	                      </div>
                       </div>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                       <div class="form-group">
                          <label class="control-label">Receiver's Phone number<span class="red-star"> *</span></label>
                          <div class="row">
                             <div class="col-xs-4 col-xs-12">
                                <input type="text" class="form-control required numeric maxlength-3" placeholder="Country Code" name="country_code" maxlength="3">
                             </div>
                             <div class="col-xs-8 col-xs-12">
                                <input type="text" class="form-control required numeric between-8-12" name="phone_number" placeholder="Phone Number" pattern=".{8,12}" title="8 to 12 numbers">
                             </div>
                          </div>
                       </div>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                       <!-- <div class="form-group">
                          <label class="control-label">Package Value</label>
                          <input class="form-control required float maxlength-7 " placeholder="$" name="package_value" maxlength="9" >
                       </div> -->
                    </div>
                    <div class="col-sm-12 col-xs-12">
                       <div class="form-group">
                          <label class="control-label">Insurance</label>
                          <div class="radio">
                             <label>
                             	<input type="radio" name="insurance" value="yes">
                             </label>&nbsp; Yes
                             &nbsp; &nbsp;
                             <label>
                             	<input type="radio" name="insurance" value="no" checked>
                             </label>&nbsp;No
                          </div>
                       </div>
                       <div class="form-group">
                          <div class="checkbox">
                             <label>
                             	<span class="pull-left"><input type="checkbox" name="need_package_material"></span> <span class="Insurance_check"> Need Package Material</span>
                             </label>
                          </div>
                       </div>

                       <div class="form-group">
                          <div class="checkbox">
                             <!-- <label>
                             	<span class="pull-left"><input type="checkbox"  id="agree" name="checkbox" value="check"></span> <span class="Insurance_check"> I Accept the  <a href="{{url('terms-and-conditions')}}" target="_blank"> Terms and Conditions </a> .</span>
                             </label> -->
                          </div>
                       </div>
                    </div>
                    <div class="col-md-12 col-xs-12">
                       <hr>
                    </div>
                    <div class="col-sm-8 col-xs-12">
                    	<div class="row">

	                  		<div class="col-xs-12 col-xs-12">
			                     <div class="form-group">
			                        <button class="custom-btn1 btn" id="calculate_loader">
			                           Next
			                           <div class="custom-btn-h"></div>
			                        </button>
			             			    <a class="custom-btn1 btn text-center" onclick="switch_request_header('#sec2','#sec3',2)" href="javascript:void(0)">
			                           Back
                                       <div class="custom-btn-h"></div>
			                        </a>
			                     </div>
			                </div>
	                  </div>
                    </div>
                 </div>
              </div>
              <!-- End section 3 -->

              <!-- Start section 4 -->
              <div class="row" id="sec4" style="display:none;">
                <div class="col-md-10 col-sm-offset-1">
                   <div class="col-sm-12 col-xs-12">
                   		<div id="shipping_detail">
                   			Calculating! Please wait...
                   		</div>

                      <div class="">
                    	<div class="">
	                  		<div class="">
			                     <div class="form-group">
			                        <button class="custom-btn1 btn" id="item_loader">
			                           Next
			                           <div class="custom-btn-h btn"></div>
			                        </button>
			                        <a class="custom-btn1 btn text-center" onclick="switch_request_header('#sec3','#sec4',3)" href="javascript:void(0)">
			                           Back
                                       <div class="custom-btn-h"></div>
			                        </a>
			                     </div>
			                </div>
	                  	</div>
                       </div>
                	</div>
                </div>
              </div>
                <!-- End section 4 -->
             {!! Form::close() !!}
        </div>
     </div>
  </div>
</div>
@endsection

@section('script')
@parent

<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyCT0uj-bffWWdOdwDp15LvrMko-OuoZUoY"></script>

{!! Html::script('theme/web/js/utility.js') !!}
{!! Html::script('theme/web/js/validation.js') !!}
{!! Html::script('theme/web/js/request.js') !!}
{!! Html::style('theme/date-time-picker/jquery.datetimepicker.css') !!}
{!! Html::script('theme/date-time-picker/jquery.datetimepicker.js') !!}

@endsection

@section('inline-script')
@parent
<script>


$(function() {
       var action;
       $(".number-spinner a").mousedown(function () {
           btn = $(this);
           input = btn.closest('.number-spinner').find('input');
           btn.closest('.number-spinner').find('a').prop("disabled", false);

           if (btn.attr('data-dir') == 'up') {
               action = setInterval(function(){
                   if ( input.attr('max') == undefined || parseInt(input.val()) < parseInt(input.attr('max')) ) {
                       input.val(parseInt(input.val())+1);
                   }else{
                       btn.prop("disabled", true);
                       clearInterval(action);
                   }
               }, 50);
           } else {
               action = setInterval(function(){
                   if ( input.attr('min') == undefined || parseInt(input.val()) > parseInt(input.attr('min')) ) {
                       input.val(parseInt(input.val())-1);
                   }else{
                       btn.prop("disabled", true);
                       clearInterval(action);
                   }
               }, 50);
           }
       }).mouseup(function(){
           clearInterval(action);
       });
   });

  // var newdate = new Date();
  // newdate.setHours(newdate.getHours()+3);
  // var minDateForTwo = newdate;

// $('#pickup_date').datetimepicker({
//    format:'d/m/Y h:i A',
//    timepicker:true,
//    formatTime:'h:i A',
//    datepicker:true,
//    step:30,
//    minDate : new Date(),
//    onChangeDateTime:function( ct ){
//        $('#drop_off_date').datetimepicker({  minDate:ct  })
//    },
// });

// $('#pickup_date').datetimepicker({
//    format:'d/m/Y h:i A',
//    timepicker:true,
//    formatTime:'h:i A',
//    datepicker:true,
//    step:30,
//     minDate : new Date(),
//         minTime : newdate,
//         onChangeDateTime:function( ct ){
//             minDateForTwo = ct;
//             pickup_date.minDate = ct;
//             var dt = new Date();
//             dt.setHours(23);dt.setMinutes(59);dt.setSeconds(59);

//             if(ct < dt) {
//               $('#pickup_date').datetimepicker({  minTime: newdate });
//               date2.minTime = ct;

//               if(ct < newdate) {
//                 $('#pickup_date').val(dt.dateFormat('M d, Y ')+newdate.dateFormat('h:i A'));
//                 date2.minTime = minDateForTwo;
//               }
//             } else {
//               dt.setHours(0);
//               $('#pickup_date').datetimepicker({  minTime: dt });
//               date2.minTime = false;
//             }
//             $('#pickup_date').datetimepicker(pickup_date);
//             $('#pickup_date').val('');
//          },
//    });

// $('#drop_off_date').datetimepicker({
//    format:'d/m/Y h:i A',
//    timepicker:true,
//    formatTime:'h:i A',
//    datepicker:true,
//    step:30,
//    minDate : new Date(),
//    onChangeDateTime:function( ct ){
//        $('#pickup_date').datetimepicker({  maxDate:ct  })
//    },
// });


  var newdate = new Date();
  newdate.setHours(newdate.getHours()+3);
  var minDateForTwo = newdate;

var drop_off_date = {};
    $('#pickup_date').datetimepicker({
        format:'M d, Y h:i A',
        formatTime:'h:i A',
        timepicker:true,
        datepicker:true,
        step:30,
        minDate : new Date(),
        minTime : newdate,
        onChangeDateTime:function( ct ){
        	if(minDateForTwo > ct) {
       		//$("#drop_off_date").val(minDateForTwo.dateFormat('M d, Y h:i A'));
       		 return false;
        }
            minDateForTwo = ct;
            drop_off_date.minDate = ct;
            var dt = new Date();
            dt.setHours(23);dt.setMinutes(59);dt.setSeconds(59);

            if(ct < dt) {
              $('#pickup_date').datetimepicker({  minTime: newdate });
              drop_off_date.minTime = ct;

              if(ct < newdate) {
                $('#pickup_date').val(dt.dateFormat('M d, Y ')+newdate.dateFormat('h:i A'));
                drop_off_date.minTime = minDateForTwo;
              }
            } else {
              dt.setHours(0);
              $('#pickup_date').datetimepicker({  minTime: dt });
              drop_off_date.minTime = false;
            }
            $('#drop_off_date').datetimepicker(drop_off_date);
            $('#drop_off_date').val('');
         },
   });

   $("#drop_off_date").datetimepicker({
       format:'M d, Y h:i A',
       formatTime:'h:i A',
       timepicker:true,
       datepicker:true,
       step:30,
       minDate : new Date(),
       minTime : minDateForTwo,
       closeOnDateSelect : false,
       onChangeDateTime:function( ct ){
          var date2option = {};
        if(($("#drop_off_date").val()).trim() != '') {

            $('#pickup_time').datetimepicker({  maxDate:ct, maxTime:ct  })

            if(ct < minDateForTwo) {
              $('#drop_off_date').val(ct.dateFormat('M d, Y ')+minDateForTwo.dateFormat('h:i A'));
              date2option.minTime = minDateForTwo;
            } else {
              date2option.minTime = false;
            }
            $('#drop_off_date').datetimepicker(date2option);


          }
       },
   });

function unit_show()
{
	var unit =$('input[name=measurement_unit]:checked').val();

	if(unit == 'cm_kg'){
		$('#length_unit').html('Cm');
		$('#width_unit').html('Cm');
		$('#height_unit').html('Cm');
		$('#weight_unit').html('Kg');

	}else{
		$('#length_unit').html('Inches');
		$('#width_unit').html('Inches');
		$('#height_unit').html('Inches');
		$('#weight_unit').html('Lbs');
	}

}

unit_show();

function show_quantity()
{
	var type =$('input[name=quantity_type]:checked').val();
	if(type == 'multiple'){
		$('#quantity_input').show();
	}else{
		$('#quantity_input').hide();
	}

}
show_quantity();
</script>




@endsection
