@extends('Page::layout.one-column-page')
@section('content')
{!! Html::style('theme/dropdown_checklist/jquery-ui-1.11.2.custom.css') !!}
{!! Html::script('theme/dropdown_checklist/jquery-ui-1.11.2.custom.min.js') !!}
{!! Html::script('theme/dropdown_checklist/ui.dropdownchecklist-1.5-min.js') !!}

<div class="container">
<div class="row">
	<div class="col-sm-12">
   <h2 class="color-blue mainHeading">Edit Individual Trip</h2>
   <br />
   </div>

      <div class="col-sm-12">
      <div class="box-shadow">
         <h3 style="font-weight: bold;"></h3>
         </br>
         <!--step1 -->
         <!-- <div id="sec1" >  -->
         <div class="row">
            <br />

            <div class="col-md-10 col-sm-offset-1" id="sec1" >

			 {!! Form::model('', ['name'=>'edit_individual_trip','method' => 'POST','class'=>'form-vertical','id'=>'post_trip_form','url'=>['transporter/edit_individual_trip',$trip_data->_id]]) !!}

                  <div class="col-sm-12">

                     <h4 class="color-blue">Source /Departure Address</h4>
                     <div class="form-group">
                        <label class="control-label">Address</label>
                        <input class="form-control required usename-#address#" name="address1" placeholder="Address line1" value="{{$trip_data->SourceAddress}}">
                     </div>
                  </div>
                  <div class="col-sm-3">
                     <div class="form-group">
                        <label class="control-label">Country</label>
                       <select name="source_country" class="form-control required usename-#country#" id="pp_pickup_country" onchange="get_state('pp_pickup_country','pp_pickup_state','pp_pickup_city','pp_pickup_state','{{$trip_data->SourceState}}','{{$trip_data->SourceCity}}')">

                            <option value="">Select Country</option>
	                            @foreach($country as $key)
	                              <option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}","state_available":"{{$key->state_available}}"}' @if($trip_data->SourceCountry == $key->Content) selected='selected' @endif >{{$key->Content}}</option>
	                            @endforeach


	                        </select>
                     </div>
                  </div>
                  <div class="col-sm-3">
                     <div class="form-group">
                        <label class="control-label">State</label>
                            <select name="source_state" class="form-control required left-disabled usename-#state#" id="pp_pickup_state" onchange="get_city('pp_pickup_state','pp_pickup_city','pp_pickup_city','{{$trip_data->SourceCity}}')">
	                            <option value="">Select Country</option>
	                        </select>
                     </div>
                  </div>
                  <div class="col-sm-3">
                     <div class="form-group">
                        <label class="control-label">City</label>
                        <select  name="source_city" class="form-control required usename-#city#" id="pp_pickup_city">
	                            <option value="{{$trip_data->SourceState}}">Select State</option>
	                        </select>
                     </div>
                  </div>
                <div class="col-sm-3">

                <div class="form-group has-feedback">
                  <label class="control-label">Date</label>
                  {{Form::text('source_date',show_date($trip_data->SourceDate),['class'=>'form-control required usename-#date#','id'=>'date1','placeholder'=>'Date'])}}
                </div>

                  </div>
                  <div class="col-sm-4">

                  </div>
                  <div class="col-sm-12">
                     <h4 class="color-blue">Destination/Arrival Address</h4>
                     <div class="form-group">
                        <label class="control-label">Address</label>
                        <input class="form-control required usename-#address#" name="address2" placeholder="Address" value="{{$trip_data->DestiAddress}}">
                     </div>
                  </div>
                  <div class="col-sm-3">
                     <div class="form-group">
                        <label class="control-label">Country</label>
                         <select name="destination_country" class="form-control required usename-#country#" id="pp_dropoff_country" onchange="get_state('pp_dropoff_country','pp_dropoff_state','pp_dropoff_city','pp_dropoff_state','{{$trip_data->DestiState}}','{{$trip_data->DestiCity}}')">


	                            @foreach($country as $key)
	                             <option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}","state_available":"{{$key->state_available}}"}' @if($trip_data->DestiCountry == $key->Content) selected='selected' @endif >{{$key->Content}}</option>
	                            @endforeach

	                        </select>
                     </div>
                  </div>
                  <div class="col-sm-3">
                     <div class="form-group">
                        <label class="control-label">State</label>
                      <select name="destination_state" class="form-control required left-disabled usename-#state#" id="pp_dropoff_state" onchange="get_city('pp_dropoff_state','pp_dropoff_city','pp_dropoff_city','{{$trip_data->DestiCity}}')">
	                            <option value="">Select Country</option>
	                        </select>
                     </div>
                  </div>
                  <div class="col-sm-3">
                     <div class="form-group">
                        <label class="control-label">City</label>
                        <select  name="destination_city" class="form-control required usename-#city#" id="pp_dropoff_city">
	                            <option value="{{$trip_data->DestiCity}}">Select State</option>
	                        </select>
                     </div>
                  </div>
                  <div class="col-sm-3">
                     <div class="form-group has-feedback">
                        <label class="control-label">Date</label>

                        {{Form::text('destination_date',show_date($trip_data->DestiDate),['class'=>'form-control required usename-#date#','id'=>'date2','placeholder'=>'Date'])}}

                     </div>
                  </div>
                  <div class="col-sm-4">
                     <div class="form-group has-feedback">
<!--
                        <label class="control-label">Time<span class="color-red">*</span></label>
                        <span class="glyphicon glyphicon-time form-control-feedback" style="margin-top:10px"></span>
                        <input class="form-control required" name="destination_time" placeholder="Time" id="time2">
-->

                     </div>
                  </div>



                  <div class="col-sm-12">
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
									<label class="control-label">Shipping Mode</label>
									<div class="radio">
									   <label>
									   <input type="radio" name="travel_mode" value="air" <?php if ($trip_data->TravelMode == "air") {echo 'checked=="checked"';}?> onclick="return yescheck()" >By Air
									   </label>
									   &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;
									   <label>
									   <input type="radio" name="travel_mode" value="ship" <?php if ($trip_data->TravelMode == "ship") {echo 'checked=="checked"';}?> onclick="return yescheck()"> By Sea
									   </label>
									</div>
							</div>
						</div>

						<div class="col-sm-6">
							<label class="control-label">
                Item Categories<span class="red-star"> *</span> (items you will accept)
              </label>
							<div class="" id="list1">
									<?php $trip_data->SelectCategory = array_map('strtolower', array_values($trip_data->SelectCategory));?>
									<select class="form-control required usename-#category#" name="category1[]" multiple="multiple" id='category_feild'  >

										<?php foreach ($category1 as $key) {
    ?>
										<option value="{{$key['Content']}}" <?php if (in_array(strtolower($key['Content']), $trip_data->SelectCategory)) {echo 'selected="selected"';}?> >{{ucfirst($key['Content'])}}</option>
										<?php }?>
									</select>
                  <p class="help-block red" style="color:red">
                     @if($errors->has('category1.0')) The category field is required. @endif
                  </p>

								</div>

								<div class="" id="list2" >
									<?php $trip_data->SelectCategory = array_map('strtolower', array_values($trip_data->SelectCategory));?>
									<select name="category2[]" multiple="true" id='category_feild2' class="form-control required usename-#category#">

										<?php foreach ($category2 as $key2) {
    ?>
										<option value="{{$key2['Content']}}" <?php if (in_array(strtolower($key2['Content']), $trip_data->SelectCategory)) {echo 'selected="selected"';}?> >{{ucfirst($key2['Content'])}}</option>
										<?php }?>
									</select>
                  <p class="help-block red" style="color:red">
                     @if($errors->has('category2.0')) The category field is required. @endif
                  </p>
								</div>
                <p id="checkbox_error" style="color:#a94442;"></p>

						</div>
					</div>

                  </div>




                  <div class="col-sm-12">
                     <div class="row">
                        <div class="col-sm-6">
                           <div  <?php if ($trip_data->TravelMode == 'ship') {?> style="display:none;" <?php }?> class="form-group" id="flight_div">
                              <label class="control-label">Flight Number</label>
                              <input class="form-control required alpha-numeric" placeholder="Flight Number" name="flight" value="{{$trip_data->FlightNo}}" >
                           </div>
                        </div>
                        <div class="col-sm-6">
                           <div  <?php if ($trip_data->TravelMode == 'ship') {?> style="display:none;" <?php }?> class="form-group" id="airline_div">
                             <label class="control-label">Airline</label>
                              <input class="form-control required" placeholder="Airline" name="airline" value="{{$trip_data->Airline}}" >
                           </div>
                        </div>
                     </div>
                  </div>


                  <div class="col-sm-12">

                     <div class="row">
						 <div class="col-sm-6">
                           <label class="control-label">Max. weight you can accept</label>
                           <input class="form-control required numeric" placeholder="Weight" name="weight" value="{{$trip_data->Weight}}">
                        </div>
						<div class="col-sm-6">


								<div class="form-group">
									<label class="control-label">Measurement Units</label>

									<select name="Unit"  class="form-control required usename-#unit#">

										<option value="kg">Kg</option>
										<option value="Lbs">Lbs</option>
									</select>
								</div>
                        </div>

                     </div>
                  </div>




                  <div class="col-sm-12">
                     <div class="form-group">
                        <label class="control-label">Description</label>
                        <textarea rows="3" class="form-control required" placeholder="Description" name="description" value="{{$trip_data->Description}}">{{$trip_data->Description}}</textarea>
                     </div>
                  </div>
                  <div class="col-md-12">
                     <hr>
                  </div>
                  <div class="col-sm-8">
                     <div class="form-group">
                        <button type="submit" class="custom-btn1 btn" onclick="">
                           Submit
                           <div class="custom-btn-h"></div>
                        </button>
                     </div>
                  </div>
               </form>
            </div>
         </div>

      </div>
   </div>
</div>
</div>
@endsection

@section('script')
@parent

{!! Html::script('theme/web/js/validation.js') !!}
{!! Html::script('theme/web/js/utility.js') !!}

{!! Html::style('theme/date-time-picker/jquery.datetimepicker.css') !!}
{!! Html::script('theme/date-time-picker/jquery.datetimepicker.js') !!}

@endsection

@section('inline-script')
@parent



 <script type="text/javascript">
console.log(SITEURL);

	new Validate({
		FormName : 'edit_individual_trip',
		ErrorLevel : 1,
		validateHidden: false,

	});

  $('#post_trip_form').submit(function(){

        $('#checkbox_error').html('');
        if ($("input[type=checkbox]:checked").length < 1)
        {
          $('#checkbox_error').html('You must choose at least one category.');
          return false;
        }
  });




var newdate = new Date();
  newdate.setHours(newdate.getHours()+3);

var minDateForTwo = newdate;

var date2 = {};
    $('#date1').datetimepicker({
        format:'M d, Y h:i A',
        formatTime:'h:i A',
        timepicker:true,
        datepicker:true,
        step:30,
        minDate : new Date(),
        minTime : newdate,
        onChangeDateTime:function( ct ){
            minDateForTwo = ct;
            date2.minDate = ct;
            var dt = new Date();
            dt.setHours(23);dt.setMinutes(59);dt.setSeconds(59);

            if(ct < dt) {
              $('#date1').datetimepicker({  minTime: newdate });
              date2.minTime = ct;

              if(ct < newdate) {
                $('#date1').val(dt.dateFormat('M d, Y ')+newdate.dateFormat('h:i A'));
                date2.minTime = minDateForTwo;
              }
            } else {
              dt.setHours(0);
              $('#date1').datetimepicker({  minTime: dt });
              date2.minTime = false;
            }
            $('#date2').datetimepicker(date2);
            $('#date2').val('');
         },
   });

   $("#date2").datetimepicker({
       format:'M d, Y h:i A',
       formatTime:'h:i A',
       timepicker:true,
       datepicker:true,
       step:30,
       minDate : new Date(),
       minTime : minDateForTwo,
       closeOnDateSelect : false,
       onChangeDateTime:function( ct ){
          var date2option = {};
        if(($("#date2").val()).trim() != '') {

            $('#date1').datetimepicker({  maxDate:ct, maxTime:ct  })

            if(ct < minDateForTwo) {
              $('#date2').val(ct.dateFormat('M d, Y ')+minDateForTwo.dateFormat('h:i A'));
              date2option.minTime = minDateForTwo;
            } else {
              date2option.minTime = false;
            }
            $('#date2').datetimepicker(date2option);


          }
       },
   });



	var air = 0;
	var ship = 0;
	function yescheck()
	{
		if($('[name="travel_mode"]:checked').val() == "air")
		{
			$('#flight_div').show();
			$('#airline_div').show();

		  document.getElementById("list1").style.display = "block";
		  document.getElementById("list2").style.display = "none";
		  if(air == 0){
			air = 1;
			$("#category_feild").dropdownchecklist( {icon: {}, width: 450, emptyText:'Item Categories'  } );
		  }
		}
		else
		{
			$('#flight_div').hide();
			$('#airline_div').hide();
		  document.getElementById("list2").style.display = "block";
		  document.getElementById("list1").style.display = "none";
		  if(ship == 0){
			ship = 1;
			$("#category_feild2").dropdownchecklist( {icon: {}, width: 450, emptyText:'Item Categories'  } );
		  }
		}
	}




yescheck();
get_state('pp_pickup_country','pp_pickup_state','pp_pickup_city','pp_pickup_state','{{$trip_data->SourceState}}','{{$trip_data->SourceCity}}');
get_state('pp_dropoff_country','pp_dropoff_state','pp_dropoff_city','pp_dropoff_state','{{$trip_data->DestiState}}','{{$trip_data->DestiCity}}');

</script>

@endsection





