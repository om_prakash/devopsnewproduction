@extends('Page::layout.one-column-page')
@section('content')
<?php
$ua = strtolower($_SERVER['HTTP_USER_AGENT']);
// you can add different browsers with the same way ..
if (preg_match('/(chromium)[ \/]([\w.]+)/', $ua)) {
	$browser = 'chromium';
} elseif (preg_match('/(chrome)[ \/]([\w.]+)/', $ua)) {
	$browser = 'chrome';
} elseif (preg_match('/(safari)[ \/]([\w.]+)/', $ua)) {
	$browser = 'safari';
} elseif (preg_match('/(opera)[ \/]([\w.]+)/', $ua)) {
	$browser = 'opera';
} elseif (preg_match('/(msie)[ \/]([\w.]+)/', $ua)) {
	$browser = 'msie';
} elseif (preg_match('/(mozilla)[ \/]([\w.]+)/', $ua)) {
	$browser = 'mozilla';
}

preg_match('/(' . $browser . ')[ \/]([\w]+)/', $ua, $version);
?>
<div class="modal fade" id="item_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myModalLabel">&nbsp;&nbsp;Edit Item</h4>
         </div>
         <div class="clearfix"></div>
         {!! Form::model('', ['name' => 'edit_item', 'id' =>'edit_item', 'method' => 'post']) !!}
         <input type="hidden" value="{{Request::segment(2)}}" name="request_id">
         <div class="modal-body">
            <div class="row">
               <div class="col-sm-12">
                  <div class="form-group">

                     <label class="control-label">Item Name</label>
                     <input class="form-control required"   placeholder="Item Name"  name="item_name" id="item_name" maxlength="45" >
                     <input type="hidden" value="" name="item_id" id="item_id">
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-sm-12">
                  <div class="form-group">
                     <label class="control-label">Item URL/Purchased From</label>
                     <input type="text" class="form-control required valid_url"  name="add_item_url"  id="add_item_url" placeholder="Enter your own url or use sample below">
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-sm-12">
                  <a href="javascript:void()" target="iframe_a" type="button" class="btn btn-primary blue-btn" data-target="#amazonModal" data-toggle="modal" onclick="open_market('amazon')">AMAZON</a>&nbsp;&nbsp;
                  <a type="button" class="btn btn-primary blue-btn" data-target="#amazonModal" data-toggle="modal" onclick="open_market('ebay')">EBAY</a>&nbsp;&nbsp;
                  <a type="button" class="btn btn-primary blue-btn" data-target="#amazonModal" data-toggle="modal" onclick="open_market('walmart')">WALMART</a>&nbsp;&nbsp;

                  <a type="button" class="btn btn-primary blue-btn" data-target="#amazonModal" data-toggle="modal" onclick="open_market('asos')">ASOS</a>&nbsp;&nbsp;
                  <a type="button" class="btn btn-primary blue-btn" data-target="#amazonModal" data-toggle="modal" onclick="open_market('bestbuy')">BESTBUY</a>&nbsp;&nbsp;
                  <a type="button" class="btn btn-primary blue-btn" data-target="#amazonModal" data-toggle="modal" onclick="open_market('macys')">MACY’S</a>&nbsp;&nbsp;
                  <p></p>
               </div>
            </div>
            <div class="row">
               <div class="col-sm-12">
                  <div class="form-group">
                     <label class="control-label">
                        Shipping Cost
                     </label>(from retailer to Aquantuo's facility – if known)
                      <div class="input-group error-input">
                      <span class="input-group-addon">$</span>
                     <input type="text" class="form-control float"  name="shipping_cost"  id="edit_shipping_cost" placeholder="Shipping Cost" maxlength="15">
                     </div>
                  </div>
               </div>
            </div>

              <div class="col-sm-12 row">
               <div class="form-group">
                  <!-- I know item dimensions and weight -->
                   <div class="">
                    <span class="pull-left">
                    <input name="buyforme_dimensions" id="buyforme_dimensions" value="buy_for_me_dimensions" onclick="make_dimension_optional('#buyforme_dimensions')" type="checkbox">
                    </span>
                    <span class="Insurance_check">
                    <p>
                     &nbsp;&nbsp;I know item dimensions and weight
                     </p>
                     </span>

                  </div>
               </div>
            </div>

            <div id="buyformedimensions">
            <div class="row">
               <div class="col-sm-12">
                  <div class="form-group">
                     <label class="control-label">Measurement Units</label>
                     <div class="radio">
                        <label class="">
                        <input type="radio" name="measurement_unit" id="measurement_unit" value="cm_kg" onclick="showUnit('cm_kg')">
                        </label>&nbsp;Metric (Cm/Kg)
                        <label class="">
                        <input type="radio" name="measurement_unit" id="measurement_unit2" value="inches_lbs"  onclick="showUnit('inches_lbs')" checked >
                        </label>&nbsp;Imperial (Inches/Lbs)
                     </div>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-sm-12">
                  <label class="control-label">Item Specification</label>
               </div>
               <div class="col-sm-3">
                  <div class="form-group">
                     <label class="control-label">Lenght</label>
                     <div class="input-group error-cm-input">
                     <input class="form-control  float maxlength-9" placeholder="Length"  id="length" name="length" maxlength="9" />
                       <span class="input-group-addon" id="length_unit">Cm</span>
                       </div>
                  </div>
               </div>
               <div class="col-sm-3">
                  <div class="form-group">
                     <label class="control-label">Width</label>
                      <div class="input-group error-cm-input">
                     <input class="form-control float maxlength-9" placeholder="Width" name="width"  id="width" maxlength="9">
                     <span class="input-group-addon" id="width_unit">Cm</span>
                     </div>
                  </div>
               </div>
               <div class="col-sm-3">
                  <div class="form-group">
                     <label class="control-label">Height</label>
                      <div class="input-group error-cm-input">
                     <input class="form-control float maxlength-9" placeholder="Height" name="height" id="height" maxlength="9">
                     <span class="input-group-addon" id="height_unit">Cm</span>
                     </div>
                  </div>
               </div>
               <div class="col-sm-3">
                  <div class="form-group">
                     <label class="control-label">Weight&nbsp;<a data-toggle="popover" data-trigger="focus"  data-container="body" data-placement="top" type="button" data-html="true" href="javascript:void(0)" id="login1" class="info_icon" style="margin-top:0;color:black;"><img src="{{THEME}}/web/images/info_icon.png" width="20px">
                    </a></label>
                      <div class="input-group error-cm-input">
                     <input class="form-control required float maxlength-9" placeholder="Weight" name="weight" id="weight" maxlength="9">
                     <span class="input-group-addon" id="weight_unit">Kg</span>
                     </div>
                  </div>
               </div>
            </div>
            </div>
            <div class="row">
               <div class="col-sm-6">
                  <div class="form-group">
                     <label class="control-label">Item Price</label>
                      <div class="input-group error-input">
                      <span class="input-group-addon">$</span>
                     <input class="form-control required float maxlength-7 " placeholder="Item Price" name="item_price" id="item_price" maxlength="9">
                     </div>
                  </div>
               </div>
               <div class="col-sm-6">
                  <div class="form-group">
                     <label class="control-label">Quantity</label>
                     <div style="width:160px;" class="input-group number-spinner">
                        <span class="input-group-btn data-dwn">
                        <button class="btn btn-primary" data-dir="dwn" onclick="return false;">
                        <span class="glyphicon glyphicon-minus"></span>
                        </button>
                        </span>
                        <input type="text" class="form-control text-center" value="1" min="1" max="40"  id="quentity" name="quentity" >
                        <input type="hidden" value="">
                        <span class="input-group-btn data-up" onclick="return false;">
                        <button class="btn btn-primary" data-dir="up">
                        <span class="glyphicon glyphicon-plus"></span>
                        </button>
                        </span>
                     </div>
                  </div>
               </div>
            </div>

            <div class="row">
               <div class="col-sm-6">
                  <div class="form-group">
                     <label class="control-label">Shipping Mode</label> (Air: receive package within 5 to 10 business days. Sea: receive package within 6 to 8 weeks.)
                     <div class="radio">
                        <label class="">
                        <input type="radio" name="travel_mode" id="travel_mode_air" value="air" onclick="toggle_category('.travel-mode-air','.travel-mode-ship','#package_category')" checked="checked">
                        </label>&nbsp;By Air
                        <label class="">
                        <input type="radio" name="travel_mode" id="travel_mode_ship" value="ship" onclick="toggle_category('.travel-mode-ship','.travel-mode-air','#package_category')">
                        </label>&nbsp;By Sea
                     </div>
                  </div>
               </div>

                <div class="col-sm-6">
                  <div class="form-group">
                     <label class="control-label">Insurance</label>
                     <div class="radio">
                        <label class="row">
                        <label>
                        <input type="radio" name="insurance" id="edit_insurance" value="yes" checked >
                        </label>&nbsp;Yes
                        &nbsp;&nbsp;
                        <label>
                        <input type="radio" name="insurance"  id="edit_insurance2" value="no" data-toggle="modal" data-target="#insurance_change" >
                        </label>&nbsp;No
                     </div>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-sm-6">
               <div class="row">
               <div class="col-sm-12">
                  <div class="form-group">
                     <label class="control-label">Select Package Category</label>
                     <select class="form-control required" id="package_category" name="category">
                        <option value="">Select Category</option>
                        <?php foreach ($category as $key) {?>
                        <option  value='{"id":"{{$key->_id}}","name":"{{$key->Content}}"}' class="travel-mode-{{$key->TravelMode}}">{{$key->Content}}</option>
                        <?php }?>
                     </select>
                  </div>
                  </div>
                   <div class="col-sm-12">
                  <div class="form-group">
                     <label class="control-label">Package Description / Instructions</label>
                     <textarea class="form-control" name="description" rows="3" id="description" maxlength="500" ></textarea>
                  </div>
               </div>
               </div>
               </div>
               <div class="col-sm-6">
                   <div class="selected-pic">
                         <label class="control-label">Image (optional)</label>
                        <div>
                           <a class="fancybox" rel="group" href="" >
                           <img id="buy_for_me_preview" name="item_image" src=""  width="200px" height="150px" />
                           </a>
                        </div>
                        <label class="custom-input-file">
                        {!! Form::file('item_image', ['class'=> 'custom-input-file','id'=>'senior_image','onchange'=>"image_preview(this,'buy_for_me_preview',event)"]) !!}
                        </label>
                     </div>
                  </div>
            </div>
            </div>

         <input type="hidden" value="" name="address_id" id="address_id">
         <div class="clearfix"></div>
         <div class="modal-footer">

            {!! Form::button('Update', ['class' => 'btn custom-btn1','id'=>'item_loader','type'=>'submit']) !!}
             {!! Form::button('Close', ['class' => 'btn custom-btn1', 'data-dismiss'=>'modal']) !!}
         </div>
         {!! Form::close() !!}
      </div>
   </div>
</div>
<div aria-labelledby="exampleModalLabel" role="dialog" tabindex="-1" id="exampleModal" class="modal fade in">
   <div role="document" class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header" id="Addaddress2">
            <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button>
            <h4 id="exampleModalLabel" class="modal-title">Add New Address</h4>
         </div>
         {!! Form::model('', ['name' => 'new_address', 'id' => 'Addaddress', 'method' => 'POST']) !!}
         <div class="panel-body">
            <div class="row">
               <div class="col-sm-12">
                  <div class="form-group">
                     <label>Address Line1</label>
                     <input type="text" placeholder="Address" name="address_line_1" id="address_line_1"  maxlength= "125" class="form-control required">
                  </div>
               </div>
               <div class="col-sm-12">
                  <div class="form-group">
                     <label>Address Line2</label>
                     <input type="text" placeholder="Address" name="address_line_2" id="address_line_2"  maxlength= "125" class="form-control">
                  </div>
               </div>
               <div class="col-sm-6">
                  <div class="form-group">
                     <label>Country</label>
                     <select name="country" class="form-control required" id="pp_pickup_country10" onchange="get_state2('pp_pickup_country10','pp_pickup_state10','pp_pickup_city10','pp_pickup_state10','','','','10','')">
                        <option value="" id="country">Select Country</option>
                        @foreach($country as $key)
                        <option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}","state_available":"{{$key->state_available}}"}'>{{$key->Content}}</option>
                        @endforeach
                     </select>
                  </div>
               </div>
               <div class="col-sm-6">
                  <div class="form-group">
                     <label>State/Region</label>
                     <span id="ap_id10">
                     <select name="state" class="form-control required chosen-select" id="pp_pickup_state10" onchange="get_city('pp_pickup_state10','pp_pickup_city10','pp_pickup_city10','')">
                        <option value="">Select State/Region</option>
                     </select>
                     </span>
                  </div>
               </div>
               <div class="clearfix"> </div>
               <div class="col-sm-6">
                  <div class="form-group">
                     <label>City</label>
                     <select  name="city" class="form-control required chosen-select" id="pp_pickup_city10">
                        <option value="">Select City</option>
                     </select>
                  </div>
               </div>
               <div class="col-sm-6">
                  <div class="form-group">
                     <label>Zipcode</label>
                     <input type="text" placeholder="Zipcode" name="zipcode" id="zipcode"  maxlength="8" class="form-control alph-numeric">
                  </div>
               </div>
               </hr>
               <div class="col-sm-8 col-sm-offset-2" align="center">
                  <div class="row">
                     <div class="col-sm-6 col">
                        <div class="form-group">
                           <button class="custom-btn1 btn-block" id="address_loader" >
                              Submit
                              <div class="custom-btn-h"></div>
                           </button>
                        </div>
                     </div>
                     <div class="col-sm-6 col">
                        <div class="form-group">
                           <button class="custom-btn1 btn-block"  data-dismiss="modal">
                              Close
                              <div class="custom-btn-h"></div>
                           </button>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         {!! Form::close() !!}
      </div>
   </div>
</div>

<div class="container">
<div class="row">
<div class="col-sm-12" id="buy_for_me_start_position">
   <h2 class="color-blue">Buy for Me</h2>
   <br />
</div>
<div class="col-sm-12" id="sec1">
   <div class="box-shadow">
      <!-- <h3 class="midHeading" id="midHeading">At this time, prices shown are for shipping to the door only and do not include the cost of customs duty if applicable. Estimate 15-25% of the cost of your items as customs.</h3> -->
      <h3 class="midHeading">Please complete the following fields</h3>
      <hr />
      <!------------------step1------------------>
      <div class="row">
         <div class="col-sm-12">
            <div class="step_box three_step clearfix">
            <div class="step first selected">
               <div>1</div>
            <p class="text-center colol-black">Package Details</p>
            </div>

            <div class="step inner">
               <div>2</div>
               <p class="text-center colol-black">Drop Off Address</p>
            </div>

            <div class="step last" >
               <div>3</div>
               <p class="text-center colol-black">Payment</p>
            </div>

         </div>
         </div>

         <div id="table_hide" style="@if(count($additem) <= 0) display:none; @endif">
            <div class="col-md-10 col-sm-offset-1">
               <div class="col-sm-12 text-right">
                  <button class="btn btn-default"  onclick="$('#add_item').show();$('#table_hide').hide();"   >Add Item</button></br>
                  </br>
               </div>
            </div>
            <div class="col-md-10 col-sm-offset-1">
               <div class="col-sm-12"  >
                  <table class="custom-table table table-bordered" name="item_list" id="item_list">
                     <thead>
                        <tr>
                           <th>S.No.</th>
                           <th>Item Name</th>
                           <th>Item Price</th>
                           <th>Item Weight</th>
                           <th>Quantity</th>
                           <th>Description</th>
                           <th>Action</th>
                        </tr>
                     </thead>
                     <tbody>
                     <?php $sno = 1;?>
                        @foreach($additem as $value)
<?php
$weight_unit = 'lbs';
if ($value['weight_unit'] == 'kg') {
	$weight_unit = 'kg';
}
?>
                        <tr id="row-{{$value['_id']}}">
                           <td>{{ $sno++}} </td>
                           <td>{{ucfirst($value['product_name'])}}</td>
                           <td>${{number_format((float)$value['price'],2)}}</td>
                           <td>{{ucfirst($value['weight'])}}&nbsp;{{ucfirst($weight_unit)}}</td>
                           <td>{{ucfirst($value['qty'])}}&nbsp;</td>
                           <td>{{ucfirst($value['description'])}}</td>
                           <td colspan="2" >&nbsp;&nbsp;
                              <span><a type=""  data-toggle="modal" onclick="get_edit_item('{{json_encode($value)}}')" id="Edit" title="Edit" data-whatever="@mdo" href="#item_modal"> <i class="fa fa-pencil"></i></a></span>&nbsp;&nbsp;
                              <a title="delete" id="Delete"  onclick="remove_record('delete_item/{{$value['_id']}}/DeleteItem','{{$value['_id']}}')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>
                           </td>
                        </tr>
                        @endforeach
                     </tbody>
                  </table>
                  <div class="">
                     <div class="">
                        <div class="form-group">
                           <button class="custom-btn1"  onclick="go_to_address_menu()">
                           Next
                           <div class="custom-btn-h"></div>
                           </button>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-md-10 col-sm-offset-1" style="@if(count($additem) > 0) display:none; @endif" id="add_item" >
            {!! Form::open(['class'=>'form-vertical','name'=>'buy_for_me_form_add','id' => 'buy_for_me_form_add','files' => true ]) !!}
            <input type="hidden" value="{{Request::segment(2)}}" name="request_id">

            <div class="col-sm-12">
               <div class="form-group">
                  <label class="control-label">Item Name</label>
                  <input type="hidden" value="" name="item_id" id="item_id">
                  <input class="form-control required"   placeholder="Item Name"  name="item_name" maxlength="45" >
               </div>
            </div>
            <div class="col-sm-12">
               <div class="form-group">
                  <label class="control-label">Item URL/Purchased From</label>
                  <input type="text" class="form-control required valid_url"  name="add_item_url"  id="add_item_url" placeholder="Enter your own url or use sample below">
               </div>
            </div>
            <div class="col-sm-12">
               <a href="javascript:void()" target="iframe_a" type="button" class="btn btn-primary blue-btn" data-target="#amazonModal" data-toggle="modal" onclick="open_market('amazon')">AMAZON</a>&nbsp;&nbsp;
               <a type="button" class="btn btn-primary blue-btn" data-target="#amazonModal" data-toggle="modal" onclick="open_market('ebay')">EBAY</a>&nbsp;&nbsp;
               <a type="button" class="btn btn-primary blue-btn" data-target="#amazonModal" data-toggle="modal" onclick="open_market('walmart')">WALMART</a>&nbsp;&nbsp;

               <a type="button" class="btn btn-primary blue-btn" data-target="#amazonModal" data-toggle="modal" onclick="open_market('asos')">ASOS</a>&nbsp;&nbsp;
               <a type="button" class="btn btn-primary blue-btn" data-target="#amazonModal" data-toggle="modal" onclick="open_market('bestbuy')">BESTBUY</a>&nbsp;&nbsp;
               <a type="button" class="btn btn-primary blue-btn" data-target="#amazonModal" data-toggle="modal" onclick="open_market('macys')">MACY’S</a>&nbsp;&nbsp;

               <p></p>
            </div>
            <div class="col-sm-12">
               <div class="form-group">
                  <label class="control-label">
                        Shipping Cost
                  </label>(from retailer to Aquantuo's facility – if known)
                   <div class="input-group error-input">
                      <span class="input-group-addon">$</span>
                  <input type="text" class="form-control float"  name="shipping_cost"  id="add_item_url" placeholder="Shipping Cost" maxlength="15">
                  </div>
               </div>
            </div>

              <div class="col-sm-12">
               <div class="form-group">

                   <div class="">
                    <span class="pull-left">
                    <input name="buy_for_me_dimensions" id="buy_for_me_dimensions" onclick="myFunction()" value="buy_for_me_dimensions" type="checkbox">
                    </span>
                    <span class="Insurance_check">
                    <p>
                     &nbsp;&nbsp;I know item dimensions and weight
                     </p>
                     </span>

                  </div>
               </div>
            </div>

            <div id="measurment" style="display:none;" >
            <div class="col-sm-12">
               <div class="form-group">
                  <label class="control-label">Measurement Units</label>
                  <div class="radio">
                     <label class="">
                     <input type="radio" name="measurement_unit" id="measurement_unit1"  value="cm_kg" onclick="unit_show('cm_kg')">
                     </label>&nbsp;Metric (Cm/Kg)
                     <label class="">
                     <input type="radio" name="measurement_unit" id="measurement_unit3" value="inches_lbs" onclick="unit_show('inches_lbs')" checked >
                     </label>&nbsp;Imperial (Inches/Lbs)
                  </div>
               </div>
            </div>
            <div class="col-sm-12">
               <label class="control-label">Item Specification</label>
            </div>
            <div class="col-sm-3">
               <div class="form-group">
                  <label class="control-label">Length</label>
                   <div class="input-group error-cm-input">
                    <input class="form-control  float maxlength-9" placeholder="Length"  id="length" name="length" maxlength="9" />
                     <span class="input-group-addon" id="length_unit1">Cm</span>
                     </div>
               </div>
            </div>
            <div class="col-sm-3">
               <div class="form-group">
                  <label class="control-label">Width</label>
                    <div class="input-group error-cm-input">
                  <input class="form-control float maxlength-9" placeholder="Width" name="width"  id="width" maxlength="9">
                  <span class="input-group-addon" id="width_unit1">Cm</span>
                     </div>
               </div>
            </div>
            <div class="col-sm-3">
               <div class="form-group">
                  <label class="control-label">Height</label>
                  <div class="input-group error-cm-input">
                  <input class="form-control float maxlength-9" placeholder="Height" name="height" id="height" maxlength="9">
                  <span class="input-group-addon" id="height_unit1">Cm</span>
                     </div>
               </div>
            </div>
            <div class="col-sm-3">
               <div class="form-group">
                  <label class="control-label">Weight&nbsp;<a data-toggle="popover" data-trigger="focus"  data-container="body" data-placement="top" type="button" data-html="true" href="javascript:void(0)" id="login1" class="info_icon" style="margin-top:0;color:black;"><img src="{{THEME}}/web/images/info_icon.png" width="20px">
                    </a></label>
                  <div class="input-group error-cm-input">
                  <input class="form-control required float maxlength-9" placeholder="Weight" name="weight" id="weight" maxlength="9">
                  <span class="input-group-addon" id="weight_unit1">Cm</span>
                     </div>
               </div>
            </div>
            </div>
            <div class="col-sm-6">
               <div class="form-group">
                  <label class="control-label">Item Price</label>
                   <div class="input-group error-input">
                      <span class="input-group-addon">$</span>
                  <input class="form-control required float maxlength-7 " placeholder="Item Price" name="item_price" id="item_price" maxlength="9">
                  </div>
               </div>
            </div>
            <div class="col-sm-6">
               <div class="form-group">
                  <label class="control-label">Quantity</label>
                  <div style="width:160px;" class="input-group number-spinner">
                     <span class="input-group-btn data-dwn">
                     <button class="btn btn-primary" data-dir="dwn" onclick="return false;">
                     <span class="glyphicon glyphicon-minus"></span>
                     </button>
                     </span>
                     <input type="text" class="form-control text-center" value="1" min="1" max="40"  id="quentity" name="quentity" >
                     <input type="hidden" value="">
                     <span class="input-group-btn data-up">
                     <button class="btn btn-primary" data-dir="up" onclick="return false;">
                     <span class="glyphicon glyphicon-plus"></span>
                     </button>
                     </span>
                  </div>
               </div>
            </div>
            <div class="col-sm-12">
               <div class="form-group">
                  <label class="control-label">Shipping Mode</label> (Air: receive package within 5 to 10 business days. Sea: receive package within 6 to 8 weeks.)
                  <div class="radio">
                     <label class="">
                     <input type="radio" name="travel_mode" id="travel_mode_air" value="air" onclick="toggle_category('.travel-mode-air','.travel-mode-ship','#package_category')" checked="checked">
                     </label>&nbsp;By Air
                     <label class="">
                     <input type="radio" name="travel_mode" id="travel_mode_ship" value="ship" onclick="toggle_category('.travel-mode-ship','.travel-mode-air','#package_category')">
                     </label>&nbsp;By Sea
                  </div>
               </div>
            </div>
            <div class="col-sm-12">
               <div class="form-group">
                  <label class="control-label">Select Package Category</label>
                  <select class="form-control required section1" id="package_category" name="category">
                     <option value="">Select Category</option>
                     <?php foreach ($category as $key) {?>
                     <option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}"}' class="travel-mode-{{$key->TravelMode}}">{{$key->Content}}</option>
                     <?php }?>
                  </select>
               </div>
            </div>
            <div class="col-sm-12">
               <div class="form-group">
                  <label class="control-label"> Package Description / Instructions</label>
                  <textarea class="form-control" name="description" rows="3" id="description" maxlength="500" ></textarea>
               </div>
            </div>
            <div class="col-sm-12">
                  <div class="form-group">
                     <label class="control-label">Item Image (optional)</label>
                     <input type="file" name="item_image" id="item_image" class="valid-filetype-jpg,png,gif,jpeg" />
                  </div>
               </div>
            <div class="col-sm-12">
               <div class="form-group">
                  <label class="control-label">Insurance</label>
                  <div class="radio">
                     <label>
                     <input type="radio" name="insurance" id="insurance" value="yes" checked >
                     </label>&nbsp;Yes
                     &nbsp; &nbsp;
                     <label>
                     <input type="radio" name="insurance"  id="insurance2" value="no" data-toggle="modal" data-target="#insurance_change" >
                     </label>&nbsp;No
                  </div>
               </div>


               <div class="col-md-12">
                  <hr />
               </div>

               <div class="">
               <div class="row">
                  <div class="col-xs-6">
                     <div class="form-group">
                        
                        <a class="custom-btn1 btn"  onclick="$('#add_item').hide();$('#table_hide').show();"   >Back<div class="custom-btn-h"></div></a>
                        <button class="custom-btn1 btn">Add <div class="custom-btn-h"></div></button>
                     <div class="error-msg"></div></div>
                  </div>

               </div>
            </div>

            </div>
            {!! Form::close() !!}
         </div>
      </div>
   </div>
</div>

<!------------------step1------------------>
<!------------------step2------------------>
{!! Form::open(['class'=>'form-vertical','name'=>'buy_for_me_form','id' => 'buy_for_me_form' ]) !!}
<input type="hidden" name="discount" id="olp_discount" value="0">

<input type="hidden" value="{{Request::segment(2)}}" name="request_id">
<div class="col-sm-12" id="sec2" style="display:none;">
   <div class="box-shadow">
      <h3 class="midHeading" id="midHeading">At this time, prices shown are for shipping to the door only and do not include the cost of customs duty if applicable. Estimate 15-25% of the cost of your items as customs.</h3>
      <h3 class="midHeading">Please complete the following fields</h3>
      <hr />
      <!------------------step1------------------>
      <div class="col-sm-12">

         <div class="step_box three_step clearfix">
            <div class="step first selected">
               <div>1</div>
            <p class="text-center colol-black">Package Details</p>
            </div>

            <div class="step inner selected">
               <div>2</div>
               <p class="text-center colol-black">Drop Off Address</p>
            </div>

            <div class="step last" >
               <div>3</div>
               <p class="text-center colol-black">Payment</p>
            </div>

         </div>

      </div>

      <div class="row">
         <div class="col-md-10 col-sm-offset-1">
            <div class="col-sm-10">
               <div class="form-group">
                  <label class="control-label">Select Saved Address</label>
                  <select class="form-control required usename-#address#" name="address" id="dropoff_address">
                     <option value="">Select address</option>
                     @foreach($address as $value)
                     <?php $add = strtolower($value->address_line_1 . ' ' . $value->city . ' ' . $value->state . ' ' . $value->country . ' ' . $value->zipcode);?>
                     <option value="{{json_encode($value)}}" @if(strtolower($request->DeliveryFullAddress) == $add) selected="selected" @endif >{{$add}}</option>
                     @endforeach
                  </select>
               </div>
            </div>


            <div class="col-sm-2">
               <div class="form-group">
                  <label class="control-label">&nbsp;</label>
                  <button type="button" class="btn btn-primary blue-btn" data-target="#exampleModal" data-toggle="modal">Add New Address</button>
               </div>
            </div>
            <div class="col-sm-12 col-xs-12" >
               <div class="form-group">
                  <div class="checkbox">
                     <label>
                        <input type="checkbox" name="ReceiverIsDifferent"  id="ReceiverIsDifferent" onclick=" return receiver_name()" @if($request->ReceiverIsDifferent =='yes') checked='true' @endif>
                     </label>Receiver is different from Requester
                  </div>
               </div>
            </div>

            <?php
               $dis = 'none';
               if($request->ReceiverIsDifferent =='yes'){
                  $dis = 'block';
               }

            ?>

            <div class="col-sm-4 col-xs-4" id="ReceiverName_in" style="display: {{$dis}};"  >
               <div class="form-group">
                  <label class="control-label">Receiver Name</label><span class="red-star"> *</span>
                  
                    <input type="text" class="form-control required  usename-#name#" name="ReceiverName" placeholder="Receiver Name" maxlength="100" value="{{$request->ReceiverName}}">
                  
               </div>
            </div>
            <div class="col-sm-12 col-xs-12"></div>
            <div class="col-sm-6">
               <div class="form-group">
                  <label class="control-label">Receiver's Phone Number <span class="red-star"> *</span></label>
                  <div class="row">
                     <div class="col-xs-4">
                        <input type="text" class="form-control required usename-#country_code#" placeholder="Country Code" name="country_code" maxlength="4" value="{{ str_replace('+', '', $request->ReceiverCountrycode) }}">
                     </div>
                     <div class="col-xs-8">
                        <input type="text" class="form-control required numeric between-8-12 usename-#phone_number#" name="phone_number" placeholder="Phone Number" maxlength="12" value="{{$request->ReceiverMobileNo}}">
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-sm-12">
               <div class="form-group">
                  <label class="control-label">Return address </label>&nbsp;(If Item is Not Delivered)
                  <div class="checkbox">
                     <label>
                     <input type="checkbox" name="return_to_aquantuo" value="return_to_aquantuo">
                     </label> Return to Aquantuo
                  </div>
               </div>
            </div>

            <div class="col-sm-12">
               <div class="form-group">

                  <div class="checkbox curtomlabel">
                     <label>
                     <input type="checkbox" name="consolidate_check" value="on" @if ($request->consolidate_item == 'on') checked="checked" @endif >
                     <p >Consolidate my items and ship them together when possible.</p>
                     </label>
                  </div>
               </div>
            </div>

            <div class="">
               <div class="">
                  <div class="col-xs-12">
                     <div class="form-group">
                        <a class="btn custom-btn1 text-center"  onclick="switch_request_header('#sec1','#sec2',0)"  href="javascript:void(0)" >
                           Back
                           <div class="custom-btn-h"></div>
                        </a>
                        <button class="btn custom-btn1" id="nextbt">
                        Next
                        <div class="custom-btn-h"></div>
                        </button>
                        
                     </div>

                  </div>
                  <div class="col-xs-6">
                     <div class="form-group">

                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="" id="sec3" style="display:none;">
   <div class="col-md-12">
      <div class="box-shadow"><h3 class="midHeading" id="midHeading">Review your order</h3><hr/>
         <div class="step_box three_step clearfix">
         <div class="step first selected">
            <div>1</div>
            <p class="text-center colol-black">Package Details</p>
         </div>
         <div class="step inner selected">
            <div>2</div>
            <p class="text-center colol-black">Drop Off Address</p>
         </div>
         <div class="step last selected" >
            <div>3</div>
            <p class="text-center colol-black">Payment</p>
         </div>
      </div>

      <div class="col-sm-10  col-sm-offset-1">
         <div id="shipping_detail">
               Calculating! Please wait...
         </div>

         <div class="">
            <div class="row">
               <div class="col-xs-12">
                  <div class="form-group">
                        <a class="custom-btn1 btn text-center" onclick="switch_request_header('#sec2','#sec3',0)" href="javascript:void(0)">
                        Back
                        <div class="custom-btn-h"></div>
                        </a>
                        <button class="btn custom-btn1" id="creating_req_btn">
                           Next
                           <div class="custom-btn-h"></div>
                        </button>
                        
                  </div>
               </div>

                  <input type="hidden" name="distance" id="distance">
                  <input type="hidden" name="promo_code" id="promo_code_olp_promo">
                  <input type="hidden" name="browser" id="browser" value="{{$browser}}">
                  <input type="hidden" name="version" id="version" value="{{$version[2]}}">
                  <input type="hidden" name="device_type" id="device_type" value="website">
            </div>
         </div>
         </div>
      </div>
   </div>
</div>
{!! Form::close() !!}

<div class="modal fade" id="insurance_change" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title"> Alert</h5>
            <a href="javascript::void(0)" class="close" data-dismiss="modal" aria-label="Close" onclick="alertClose()"> <span aria-hidden="true">&times;</span>
            </a>
          </div>
          <input type="hidden" value="{{session()->get('alert_status')}}" id="alertMsg">
          <div class="modal-body">By declining insurance, you risk not getting the full item value of your item in the event of a damage or loss</div>

      </div>
    </div>
</div>
<div id="popover-content-login1" class="hide" >
  <h4>Suggested item weights</h4>
  <small>These are suggested weights and may not reflect the actual weight of your item</small>
  <table class="table table-bordered table-striped">
    <thead>
      <tr>
        <th width="150">Item Name</th>
        <th width="60">Weight/Unit</th>
      </tr>
    </thead>
    <tbody>
      
      @if(count($item) > 0)
      @foreach($item as $key)
      <tr>
        <td>{{ucfirst($key->item_name)}} </td>
        <td >{{number_format($key->lbsWeight,2)}} Lbs / {{number_format($key->kgWeight,2)}} Kg</td>
      </tr>
      @endforeach
      @endif
    </tbody>
  </table>
</div>

@endsection
@section('script')
@parent
{!! Html::script('theme/web/js/utility.js') !!}
{!! Html::script('theme/web/js/validation.js') !!}
{!! Html::script('theme/web/js/buyforme.js') !!}
{!! Html::style('theme/date-time-picker/jquery.datetimepicker.css') !!}
{!! Html::script('theme/date-time-picker/jquery.datetimepicker.js') !!}
{!! Html::style('theme/admin/choose-multiple/bootstrap-chosen.css') !!}
{!! Html::script('theme/admin/choose-multiple/chosen.jquery.js') !!}
<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyCT0uj-bffWWdOdwDp15LvrMko-OuoZUoY"></script>
@endsection
@section('inline-script')
@parent
<script type="text/javascript">

var total_item_count ='{{count($additem)}}';

$(function()
{
    var action;
    $(".number-spinner button").mousedown(function () {
        btn = $(this);
        input = btn.closest('.number-spinner').find('input');
        btn.closest('.number-spinner').find('button').prop("disabled", false);

        if (btn.attr('data-dir') == 'up') {
            action = setInterval(function(){
                if ( input.attr('max') == undefined || parseInt(input.val()) < parseInt(input.attr('max')) ) {
                    input.val(parseInt(input.val())+1);
                }else{
                    btn.prop("disabled", true);
                    clearInterval(action);
                }
            }, 50);
        } else {
            action = setInterval(function(){
                if ( input.attr('min') == undefined || parseInt(input.val()) > parseInt(input.attr('min')) ) {
                    input.val(parseInt(input.val())-1);
                }else{
                    btn.prop("disabled", true);
                    clearInterval(action);
                }
            }, 50);
        }
    }).mouseup(function(){
        clearInterval(action);
    });
});

   $('#desired_delivery_date').datetimepicker({
   format:'m/d/Y h:i A',
   timepicker:true,
   datepicker:true,
   minDate : new Date(),

   });

   new Validate({
   FormName :  'new_address',
   ErrorLevel : 1,
   callback: function()
   {

    var geocoder = new google.maps.Geocoder();

    var address = $('#address_line_1').val()+', '+$('#address_line_2').val()+', '+
        parse_json($('#pp_pickup_city10').val()).name+', '+
        parse_json($('#pp_pickup_state10').val()).name+', '+
        parse_json($('#pp_pickup_country10').val()).name;

      geocoder.geocode( { 'address': address}, function(getLatlong, status)
      {
        if (status == google.maps.GeocoderStatus.OK)
        {
            add_address(getLatlong[0].geometry.location.lat(),getLatlong[0].geometry.location.lng());

        } else {
            var address = parse_json($('#pp_pickup_city10').val()).name+', '+
                  parse_json($('#pp_pickup_state10').val()).name+', '+
                  parse_json($('#pp_pickup_country10').val()).name;
            geocoder.geocode( { 'address': address}, function(getLatlong, status)
            {
               if (status == google.maps.GeocoderStatus.OK) {
                  add_address(getLatlong[0].geometry.location.lat(),getLatlong[0].geometry.location.lng());
               } else {
                  add_address(0,0);
               }
            });

        }
    });

   }

   });

function add_address(lat,lng) {

   $("#address_loader").addClass("spinning");
   $.ajax({
       url: SITEURL+'add-address',
       data: {
         "address_line_1": $('#address_line_1').val() ,
         "address_line_2": $('#address_line_2').val(),
         "country": $('#pp_pickup_country10').val(),
         "state": $('#pp_pickup_state10').val(),
         "city":$('#pp_pickup_city10').val(),
         "zipcode": $('#zipcode').val(),
         "lat" : lat,
         "lng" : lng
       },
       type : 'post',
       dataType: 'json',
       success : function(obj) {
            $("#address_loader").removeClass("spinning");

           if(obj.success == 1) {
               document.getElementById("address_loader").value = "Submit";
               document.getElementById("Addaddress").reset();
               $('#dropoff_address').html(obj.address_html);

                $("#exampleModal").modal("hide");
           }
      }
   });

}
   function parse_json($string) {
   return eval('('+$string+')');
   }

   new Validate({
   FormName :  'buy_for_me_form_add',
   ErrorLevel : 1,
   validateHidden : false,
   callback: function() {

    try
    {

       $("#item_loader").addClass("spinning");
       $.ajax({
            url: SITEURL+'add-Item',
            type : 'post',
            data: new FormData(document.getElementById('buy_for_me_form_add')),
            processData: false,
            contentType: false,
            dataType: "json",
            success : function(obj) {
               $("#item_loader").removeClass("spinning");
               alert(obj.msg);
               if(obj.success == 1) {
                  document.getElementById("item_loader").value = "Submit";
                  document.getElementById("buy_for_me_form_add").reset();
                  $('#item_list').html(obj.html);
                  $('#table_hide').show();
                  $('#add_item').hide();
                  scroll_to('buy_for_me_start_position');
                  $('#measurment').hide();
                  unit_show('inches_lbs');
               }

           }
        });
     }catch(e) {
        console.log(e);
     }
   }
   });

   function remove_record(url,rowid)
   {
      var request_id = '{{Request::segment(2)}}';
    if(confirm('Are you sure? You want to delete this record.') == true)
    {
        $('#row-'+rowid).addClass('relative-pos spinning');

        $.ajax
            ({
                url: SITEURL+url+'?request_id='+request_id,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(res)
                {
                    var obj = JSON.parse(res);
                    $('#row-'+rowid).removeClass('relative-pos spinning');
                    if(obj.success == 1){

                        --total_item_count;
                        $('#row-'+rowid).css({'background-color':'red'});
                        $('#row-'+rowid).fadeOut('slow');
                        $('#error_msg_section').html('<div class="alert alert-success">'+ obj.msg + '</div>');
                    }else{
                        $('#row-'+rowid).css({'background-color':'white'});
                        $('#error_msg_section').html('<div class="alert alert-danger">'+ obj.msg + '</div>');
                        alert(obj.msg);

                    }
                }
            });
    }
    return false;
   }
   function get_edit_item(obj)
   {
      $('#buyforme_dimensions').prop('checked', false);

      obj = eval('('+obj+')');

      $('#item_name').val(obj.product_name);
      $('#add_item_url').val(obj.url);
      $('#edit_shipping_cost').val(obj.shipping_cost_by_user);

      $('#length').val(obj.length);
      $('#width').val(obj.width);
      $('#height').val(obj.height);
      $('#weight').val(obj.weight);

      document.getElementById('measurement_unit2').checked='checked';
      showUnit('inches_lbs');
      if(obj.weight != '')
      {
         document.getElementById('buyforme_dimensions').checked ='true';
         if(obj.weight_unit == 'kg') {
            showUnit('cm_kg');
            document.getElementById('measurement_unit').checked='checked';
         }
      }
      make_dimension_optional('#buyforme_dimensions');

      $('#item_price').val(obj.price);
      $('#quentity').val(obj.qty);

      if(obj.image != '') {
         $('#buy_for_me_preview').attr('src',SITEURL+'upload/'+obj.image);
      } else {
         $('#buy_for_me_preview').attr('src',SITEURL+'upload/no-image.jpg');
      }


      if(obj.travelMode == 'air') {
         document.getElementById('travel_mode_air').checked='checked';
         toggle_category('.travel-mode-air','.travel-mode-ship','#package_category');
      } else {
         document.getElementById('travel_mode_ship').checked='checked';
         toggle_category('.travel-mode-ship','.travel-mode-air','#package_category');
      }
      $('#package_category').val('{"id":"'+obj.categoryid+'","name":"'+obj.category+'"}');
      $('#description').val(obj.description);

      if(obj.insurance_status == 'yes') {
         document.getElementById('edit_insurance').checked='checked';
      }else{
         document.getElementById('edit_insurance2').checked='checked';
      }
      $('#item_id').val(obj._id);

   }



   var nfm_validate =  new Validate({
    FormName :  'edit_item',
    ErrorLevel : 1,
    validateHidden : false,
    callback: function() {

       $("#item_loader").addClass("spinning");
       $.ajax({
           url: SITEURL+'edit-item',
           data: $('#edit_item').serialize(),
           type : 'post',
            data: new FormData(document.getElementById('edit_item')),
            processData: false,
            contentType: false,
            dataType: 'json',
           success : function(obj) {
               $("#item_loader").removeClass("spinning");
               alert(obj.msg);

               if(obj.success == 1) {
                  document.getElementById("item_loader").value = "Submit";
                  document.getElementById("edit_item").reset();
                  $('#item_list').html(obj.edit_html);
                  $("#item_modal").modal("hide");
                  scroll_to('buy_for_me_start_position');
               }
           }
        });
    }
   });


$("#buy_for_me_dimensions").click(function(){
   // If checked
   if ($("#buy_for_me_dimensions").is(":checked"))
   {
      //show the hidden div
      $("#measurment").show();
   }
   else
   {
      //otherwise, hide it
      $("#measurment").hide();
   }
});


$("#buyforme_dimensions").click(function(){
   if ($("#buyforme_dimensions").is(":checked")){
      $("#buyformedimensions").show();
   } else{
      $("#buyformedimensions").hide();
   }
});


function make_dimension_optional(id ) {
   if ($(id).is(":checked")){
      $("#buyformedimensions").show();

   } else{
      $("#buyformedimensions").hide();
   }
}

function unit_show(unit)
{
   var weightUnit = 'Lbs';
   var dimentionUnit = 'Inches';
   if(unit == 'cm_kg') {
      weightUnit = 'Kg';
      dimentionUnit = 'Cm';
   }
   $('#length_unit1').html(dimentionUnit);
   $('#width_unit1').html(dimentionUnit);
   $('#height_unit1').html(dimentionUnit);
   $('#weight_unit1').html(weightUnit);

}
function showUnit(unit)
{
   var weightUnit = 'Lbs';
   var dimentionUnit = 'Inches';
   if(unit == 'cm_kg') {
      weightUnit = 'Kg';
      dimentionUnit = 'Cm';
   }
   $('#length_unit').html(dimentionUnit);
   $('#width_unit').html(dimentionUnit);
   $('#height_unit').html(dimentionUnit);
   $('#weight_unit').html(weightUnit);

}

unit_show('inches_lbs');

$("[data-toggle=popover]").each(function(i, obj) {
   $(this).popover({
      html: true,
      content: function() {
      var id = $(this).attr('id')
      return $('#popover-content-' + id).html();
      }
   });
});

function receiver_name(){
                
 if($("#ReceiverIsDifferent").prop('checked') == true){
   $('#ReceiverName_in').show();
 }else{
   $('#ReceiverName_in').hide();
 }
 
}

</script>


@endsection

