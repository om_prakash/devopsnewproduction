@if(!isset($calculationinfo->error))
<div class="row custom-row">
   <div class="col-sm-6"><b>Distance -</b></div>
   <div class="col-sm-6">{{number_format($calculationinfo->distance,2)}} Miles</div>
</div>
<div class="row custom-row">
   <div class="col-sm-6"><b>Weight -</b></div>
   <div class="col-sm-6">{{number_format($calculationinfo->weight,2)}} {{ucfirst($calculationinfo->weightUnit)}}</div>
</div>
<div class="row custom-row">
   <div class="col-sm-6"><b>Volume -</b></div>
   <div class="col-sm-6">{{$calculationinfo->showVolume}} Cu. Cm</div>
</div>
<div class="row custom-row">
   <div class="col-sm-6"><b>Shipping Cost -</b></div>
   <div class="col-sm-6">${{number_format($calculationinfo->shippingcost,2)}}</div>
</div>
@if($calculationinfo->insurance > 0)
<div class="row custom-row">
   <div class="col-sm-6"><b>Insurance Cost -</b></div>
   <div class="col-sm-6">${{number_format($calculationinfo->insurance,2)}}</div>
</div>
@endif
<div class="row"> <hr /> </div>
<div class="row custom-row">
   <div class="col-sm-6"><b>Total Delivery -</b></br>
   <small>(In Ghanaian cedi GHS {{number_format($calculationinfo->costInCurrency,2)}})</small>
   
    </div>
   <div class="col-sm-6">${{number_format(($calculationinfo->shippingcost + $calculationinfo->insurance),2)}}</div>
</div> 
    
 <!--    
<div class="row">
	<hr>
	<div class="col-sm-12">
		<div class="form-group reward-group" id="prf_promo_input">
			<input type="text" placeholder="Reward Code" class="form-control pull-left" id="promocode" name="promocode" >
			<a class="btn default-btn" onclick="check_promocode('promocode',total_amount,'prf_promo')" href="javascript:void(0)">
				<img src="theme/web/promo/images/green_check.png">Apply
			</a>
			<div class="clearfix"></div>
			<div  id="er_promocode"></div>
		</div>
		<div class="form-group reward-group" id="prf_promo">
			<span id="prf_promo_msg"></span>
		</div>
	</div>
</div>
</div>

	<div class="row"> <hr /> </div>
		<div class="row custom-row">
			<div class="col-sm-6"><b>Payable -</b></br>
		<small>(In Ghanaian cedi GHS {{number_format($calculationinfo->costInCurrency,2)}})</small>
		</div>
		<div class="col-sm-6">${{number_format(($calculationinfo->shippingcost + $calculationinfo->insurance),2)}}</div>
</div>  
	
	<small class="color-red">You wil be billed or refunded any differences in price or shipping at time of purchase</small>    

<div class="row"> <hr /> </div>   -->          
@else   
<div class="row custom-row">
   <div class="col-sm-12"><p class="color-red" align="center">{{$calculationinfo->error}}</p></div>
   <p></p><p></p>
</div>

@endif
 
