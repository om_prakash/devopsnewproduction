@extends('Page::layout.one-column-page')
@section('page_title')
My Currency - Aquantuo
@endsection
@section('content')
<div class="container">
   <div class="pull-left">
      <h2 class="inner-head color-blue mainHeading">Update Currency</h2>
   </div>
   <div class="clearfix"></div>
   <div class="row">
      {!! Form::open(array('url' => 'edit-currency' , 'files' => 'true') ) !!}
      <div class="col-md-9 col-sm-8 col-xs-12">
         <div class="box-shadow clearfix">
            <br/></br>
            <div class="col-sm-6">
            <p>
               Select your default currency
               <b>
                  <select class="form-control required" id="currency" name="currency">
                     <option value="">Select Currency</option>
                     <?php foreach ($currency as $key) {?>
                     <option  value='{{$key->CurrencyCode}}' @if($userinfo->Default_Currency == $key->CurrencyCode) selected="selected" @endif >{{$key->Content}} - {{$key->CurrencySymbol}}</option>
                     <?php }?>
                  </select>
               </b>
            </p>
            <hr/>
            <div class="form-group">
               <button class="custom-btn1 btn-primary">
                  Update
                  <div class="custom-btn-h"></div>
               </button>
            </div>
            </div>
         </div>
      </div>
      {!! Form::close() !!}
      @include('Page::layout.side_bar')
   </div>
</div>
@endsection
