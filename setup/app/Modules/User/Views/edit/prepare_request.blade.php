@extends('Page::layout.one-column-page')
@section('content')
<div class="container">
   <div class="row">
   	  <div class="col-sm-12"  id="send_a_package_start_position">
      <h2 class="color-blue mainHeading">Send A Package</h2>
      <br>
      </div>
      <?php
			$ua = strtolower($_SERVER['HTTP_USER_AGENT']);
		    // you can add different browsers with the same way ..
		    if(preg_match('/(chromium)[ \/]([\w.]+)/', $ua))
		            $browser = 'chromium';
		    elseif(preg_match('/(chrome)[ \/]([\w.]+)/', $ua))
		            $browser = 'chrome';
		    elseif(preg_match('/(safari)[ \/]([\w.]+)/', $ua))
		            $browser = 'safari';
		    elseif(preg_match('/(opera)[ \/]([\w.]+)/', $ua))
		            $browser = 'opera';
		    elseif(preg_match('/(msie)[ \/]([\w.]+)/', $ua))
		            $browser = 'msie';
		    elseif(preg_match('/(mozilla)[ \/]([\w.]+)/', $ua))
		            $browser = 'mozilla';

		    preg_match('/('.$browser.')[ \/]([\w]+)/', $ua, $version);
		?>
        <style type="text/css">
                  .Insurance_check{float: left; margin-top: 3px; margin-left: 5px;}

                </style>
      <div class="col-sm-12">
      	<div class="box-shadow">
        <h3>Please complete the following fields</h3>
        <hr>
        <div class="row">
        <!-- Fillable -->
          <div class="col-sm-12">
             <div class="step_box four_step clearfix">
                <div class="step first selected" id="step1-header">
                   <div>1</div>
                   <p class="text-center colol-black">Pickup Address</p>
                </div>
                <div class="step inner" id="step2-header">
                   <div>2</div>
                   <p class="text-center colol-black">Drop Off Address</p>
                </div>
                <div class="step inner" id="step3-header">
                   <div>3</div>
                   <p class="text-center colol-black">Package Details</p>
                </div>
                <div class="step last " id="step4-header">
                   <div>4</div>
                   <p class="text-center colol-black">Payment</p>
                </div>
             </div>
          </div>

             <!-- <div id="sec1" >  -->
             {!! Form::open(['class'=>'form-vertical','name'=>'prepare_request_form','id' => 'prepare_request_form']) !!}

             	<input type="hidden" name="request_id" id="request_id" value="{{$delivery_data->_id}}">
              <div class="col-md-10 col-sm-offset-1" id="sec1" >
              	<div class="row">
	                  <div class="col-sm-8">
	                     <div class="form-group">
	                        <label>Package Title</label>
	                        {!! Form::text('title',$delivery_data->ProductTitle,['class'=>"form-control required usename-#package title#", 'placeholder'=>"Package Title",'maxlength' => 80]) !!}
	                        <input type="hidden" name="PickupLat" id="PickupLat">
	                        <input type="hidden" name="PickupLong" id="PickupLong">
	                        <input type="hidden" name="DeliveryLat" id="DeliveryLat">
	                        <input type="hidden" name="DeliveryLong" id="DeliveryLong">
	                        <input type="hidden" name="browser" id="browser" value="{{$browser}}">
            				<input type="hidden" name="version" id="version" value="{{$version[2]}}">
            				<input type="hidden" name="device_type" id="device_type" value="website">
	                     </div>
	                  </div>
	            </div>
	            <div class="row">
                  <div class="col-md-12">
                    <h3 class="color-blue">Pickup Address</h3>
                  </div>

                  <div class="col-sm-12">
                      <div class="checkbox">
                          <label>
                          <span class="pull-left"><input type="checkbox" name="public_place" <?php if ($delivery_data->PublicPlace == "yes") {echo 'checked=="checked"';}?> ></span>
                           <span class="Insurance_check">  This is a public place. </span>
                          </label>
                      </div>
                  </div>
	                  <div class="col-sm-6">
	                     <div class="form-group">
	                        <label>Address Line 1</label>
	                        {!! Form::text('address_line_1',$delivery_data->PickupAddress,['class'=>"form-control required",'placeholder'=>"Address",'maxlength' => 100,'id' => 'address_line_1'])  !!}
	                     </div>
	                  </div>
	                  <div class="col-sm-6">
	                     <div class="form-group">
	                        <label>Address Line 2</label>
	                        {!! Form::text('address_line_2',$delivery_data->PickupAddress2,['class'=>"form-control",'placeholder'=>"Address",'maxlength' => 80,'id'=>'address_line_2'])  !!}
	                     </div>
	                  </div>
	              </div>
                  <div class="row">
	                  <div class="col-sm-4">
	                     <div class="form-group">
	                        <label class="control-label">Country</label>
	                        <select name="country" class="form-control required usename-#country#" id="pp_pickup_country" onchange="get_state('pp_pickup_country','pp_pickup_state','pp_pickup_city','pp_pickup_state','{{$delivery_data->PickupState}}','{{$delivery_data->PickupCity}}')">

	                            <option value="">Select Country</option>

	                            @foreach($country as $key)
	                             <option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}","state_available":"{{$key->state_available}}"}' @if($delivery_data->PickupCountry == $key->Content) selected='selected' @endif >{{$key->Content}}</option>
	                            @endforeach

	                        </select>
	                     </div>
	                  </div>
	                  <div class="col-sm-4">
	                     <div class="form-group">
	                        <label class="control-label">State</label>
	                        <select name="state" class="form-control required left-disabled" id="pp_pickup_state" onchange="get_city('pp_pickup_state','pp_pickup_city','pp_pickup_city','{{$delivery_data->PickupCity}}')">
	                            <option value="">Select State</option>
	                        </select>
	                     </div>
	                  </div>
	                  <div class="col-sm-4">
	                     <div class="form-group">
	                        <label class="control-label">City</label>
	                        <select  name="city" class="form-control required" id="pp_pickup_city">
	                            <option value="{{$delivery_data->PickupState}}">Select City</option>
	                        </select>
	                     </div>
	                  </div>
	               </div>
                  <div class="row">
	                  <div class="col-sm-4">
	                     <div class="form-group">
	                        <label>Zip Code/Postcode</label>
	                        {!! Form::text('zipcode',$delivery_data->PickupPinCode,['class'=>"form-control alpha-numeric  maxlength-8",'placeholder'=>"Zip Code/Postcode",'maxlength' => 8])  !!}
	                     </div>
	                  </div>

	                  <div class="col-sm-4">
	                     <div class="form-group">
	                        <label>Pickup Date</label>
	                        {!! Form::text('pickup_date',show_date($delivery_data->PickupDate)
	                        ,['class'=>"form-control required",'placeholder'=>"Date","id"=>"pickup_date",'readonly'=>'true', 'readonly' => 'true'])  !!}
	                     </div>
	                  </div>
	              </div>


                  <div class="">
                     <hr>
                  </div>
                  <div class="">
                     <div class="form-group">
                        <button class="custom-btn1">
                           Next
                           <div class="custom-btn-h"></div>
                        </button>
                     </div>
                  </div>

              </div>
              <!-- End of first section -->
              <!-- Start second section -->
              <div class="" id="sec2" style="display:none;">
                 <div class="col-md-10 col-sm-offset-1">
                 	<div class="">
	                    <div class="col-md-12 row">
	                       <h3 class="color-blue">Drop Off Address</h3>
	                    </div>
	                    <div class="col-sm-6 row">
	                       <div class="form-group">
	                          <label class="control-label">Address Line 1</label>
	                          <input name="drop_off_address_line_1" class="form-control required usename-#address line1#" placeholder="Address" name="drop_address" maxlength="100" id="drop_off_address_line_1" value="{{$delivery_data->DeliveryAddress}}">
	                       </div>
	                    </div>
	                    <div class="col-sm-6">
	                       <div class="form-group">
	                          <label class="control-label">Address Line 2</label>
	                          <input name="drop_off_address_line_2" class="form-control usename-#address line2#" placeholder="Address" name="drop_address2"  maxlength="100" id="drop_off_address_line_2" value="{{$delivery_data->DeliveryAddress2}}">
	                       </div>
	                    </div>
	                </div>
                    <div class="">
	                  <div class="col-sm-4 row">
	                     <div class="form-group">
	                        <label class="control-label">Country</label>
	                        <select name="drop_off_country" class="form-control required usename-#country#" id="pp_dropoff_country" onchange="get_state('pp_dropoff_country','pp_dropoff_state','pp_dropoff_city','pp_dropoff_state','{{$delivery_data->DeliveryState}}','{{$delivery_data->DeliveryCity}}')">

	                            <option value="">Select Country</option>
	                            @foreach($country as $key)
	                             <option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}","state_available":"{{$key->state_available}}"}' @if($delivery_data->DeliveryCountry == $key->Content) selected='selected' @endif >{{$key->Content}}</option>
	                            @endforeach
	                        </select>
	                     </div>
	                  </div>
	                  <div class="col-sm-4">
	                     <div class="form-group">
	                        <label class="control-label">State</label>
	                        <select name="drop_off_state" class="form-control required usename-#state# left-disabled" id="pp_dropoff_state" onchange="get_city('pp_dropoff_state','pp_dropoff_city','pp_dropoff_city','{{$delivery_data->DeliveryCity}}')">
	                            <option value="">Select State</option>
	                        </select>
	                     </div>
	                  </div>
	                  <div class="col-sm-4">
	                     <div class="form-group">
	                        <label class="control-label">City</label>
	                        <select  name="drop_off_city" class="form-control required usename-#city#" id="pp_dropoff_city">
	                            <option value="">Select City</option>
	                        </select>
	                     </div>
	                  </div>
	               </div>
	               <div class="">
	                    <div class="col-sm-4 row">
	                       <div class="form-group">
	                          <label class="control-label">Zip Code/Postcode</label>
	                          <input class="form-control alpha-numeric usename-#zipcode#" placeholder="Zip Code/Postcode" name="drop_off_zipcode" maxlength="8" value="{{$delivery_data->DeliveryPincode}}">
	                       </div>
	                    </div>
	                    <div class="col-sm-4">
	                       <div class="form-group">
	                          <label class="control-label">Date</label>
	                          <input class="form-control required usename-#date#" placeholder="Date" name="drop_off_date" id="drop_off_date" readonly
	                          value="{{ show_date($delivery_data->DeliveryDate) }}">
	                       </div>
	                    </div>
	                </div>
	                <div class="">
	                    <div class="col-sm-12 row">
	                       <div class="form-group">
	                          <label class="control-label">Is Delivery Date Flexible?</label>
	                          <div class="checkbox">
	                             <label>
	                             <span class="pull-left row"><input type="radio" name="is_delivery_date_flexible" value="yes" <?php if ($delivery_data->FlexibleDeliveryDate == "yes") {echo 'checked=="checked"';}?> ></span>  <span class="Insurance_check">  Yes </span>
	                             </label>
	                             &nbsp; &nbsp;
	                             <label>
	                             <span class="pull-left"><input type="radio" name="is_delivery_date_flexible" value="no" <?php if ($delivery_data->FlexibleDeliveryDate == "no") {echo 'checked=="checked"';}?> ></span>  <span class="Insurance_check">  No </span>
	                             </label>
	                          </div>
	                       </div>
	                    </div>
	                    <div class="col-sm-12 row">
	                       <div class="form-group">
	                          <label class="control-label">Journey Type</label>
	                          <div class="checkbox">
	                             <label>
	                             <div class="pull-left row"><input type="radio" value="one_way" name="journey_type" onclick="$('#return_jurney_section').hide()" <?php if ($delivery_data->JournyType == "one_way") {echo 'checked=="checked"';}?> id="one_way_journey"></div>  <span class="Insurance_check">  One Way </span>
	                             </label>
	                             &nbsp; &nbsp;
	                             <label>
	                             <span class="pull-left"><input type="radio" value="return" name="journey_type" onclick="$('#return_jurney_section').show()" <?php if ($delivery_data->JournyType == "return") {echo 'checked=="checked"';}?> id="return_journy"></span>  <span class="Insurance_check">  Return  </span>
	                             </label>
	                          </div>
	                       </div>
	                    </div>
	                </div>
	                <div id="return_jurney_section"  style="display:none;">
	                	<div class="row">
		                	<div class="col-sm-12">
		                       <div class="form-group">
		                       		<h3 class="color-blue">Return Address</h3>
		                          <div class="checkbox">
		                             <label>
		                             	<span class="pull-left"><input type="checkbox" id="return_same_as_pickup" name="return_same_as_pickup" ></span>
		                             	     <span class="Insurance_check">  Same as pickup  adddress </span>
 		                             </label>
		                          </div>
		                       </div>
		                    </div>
		                </div>
	                   	<div id="return_jurney_address">
			               	<div class="row">
			                    <div class="col-sm-6">
			                       <div class="form-group">
			                          <label class="control-label">Address Line 1</label>
			                        	<input name="return_address_line_1" class="form-control required usename-#address line 1#" placeholder="Address" name="return_address" maxlength="100" value="{{$delivery_data->ReturnAddress}}">
			                       </div>
			                    </div>
			                    <div class="col-sm-6">
			                       <div class="form-group">
			                          <label class="control-label">Address Line 2</label>
			                          <input name="return_address_line_2" class="form-control usename-#address line 2#" placeholder="Address" name="return_address2" maxlength="100" value="{{$delivery_data->ReturnAddress2}}">
			                       </div>
			                    </div>
			                </div>
		                    <div class="row">
			                  <div class="col-sm-4">
			                     <div class="form-group">
			                        <label class="control-label">Country</label>
			                        <select name="return_country" class="form-control required usename-#country#" id="pp_return_country" onchange="get_state('pp_return_country','pp_return_state','pp_return_city','pp_return_state','{{$delivery_data->ReturnStateTitle}}','{{$delivery_data->ReturnCityTitle}}')">


			                            <option value="">Select Country</option>

			                            @foreach($country as $key)
			                             <option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}","state_available":"{{$key->state_available}}"}' @if($delivery_data->ReturnCountry == $key->Content) selected='selected' @endif >{{$key->Content}}</option>
			                            @endforeach


			                        </select>
			                     </div>
			                  </div>
			                  <div class="col-sm-4">
			                     <div class="form-group">
			                        <label class="control-label">State</label>
			                        <select name="return_state" class="form-control required usename-#state# left-disabled" id="pp_return_state" onchange="get_city('pp_return_state','pp_return_city','pp_return_city','{{$delivery_data->ReturnCityTitle}}')">
			                            <option value="">Select State</option>
			                        </select>
			                     </div>
			                  </div>
			                  <div class="col-sm-4">
			                     <div class="form-group">
			                        <label class="control-label">City</label>
			                        <select  name="return_city" class="form-control required usename-#city#" id="pp_return_city">
			                            <option value="">Select City</option>
			                        </select>
			                     </div>
			                  </div>
			               </div>
			               <div class="row">
			                    <div class="col-sm-4">
			                       <div class="form-group">
			                          <label class="control-label">Zip Code/Postcode</label>
			                          <input class="form-control alpha-numeric usename-#zipcode#" placeholder="Zip Code/Postcode" name="return_zipcode" maxlength="8" value="{{$delivery_data->ReturnPincode}}">
			                       </div>
			                    </div>
			                </div>
		                </div>
	             	</div>

	                <div class="row">
	                	<div class="col-sm-12">
	                       <div class="form-group">
	                       		<h3 class="color-blue">Return address(If item is not delivered)</h3>
	                          <!-- <div class="checkbox curtomlabel">
	                             <label>
	                             	<input type="checkbox" id="same_as_pickup" name="same_as_pickup" >
	                             	Same as pickup adddress
	                             </label>
	                          </div> -->


	                          <div class="">
	                             <span class="pull-left">
	                             <input type="checkbox" id="same_as_pickup" name="same_as_pickup" >
	                             </span>
	                             <span class="Insurance_check">
	                              Same as pickup adddress
	                              </span>

	                          </div>


	                       </div>
	                    </div>
	                </div>

	                <div id="return_address_action"  >
		               	<div class="row">
		                    <div class="col-sm-6">
		                       <div class="form-group">
		                          <label class="control-label">Address Line 1</label>
		                          <input value='{{$delivery_data->InCaseNotDelReturnAddress}}' name="nd_return_address_line_1" class="form-control required usename-#address line 1#" placeholder="Address" maxlength="100" >


		                       </div>
		                    </div>
		                    <div class="col-sm-6">
		                       <div class="form-group">
		                          <label class="control-label">Address Line 2</label>
		                          <input value="{{$delivery_data->InCaseNotDelReturnAddress2}}" name="nd_return_address_line_2" class="form-control usename-#address line 2#" placeholder="Address" maxlength="100">



		                       </div>
		                    </div>
		                </div>
	                    <div class="row">
		                  <div class="col-sm-4">
		                     <div class="form-group">
		                        <label class="control-label">Country</label>
		                        <select name="nd_return_country" class="form-control required usename-#country#" id="pp_nd_return_country" onchange="get_state('pp_nd_return_country','pp_nd_return_state','pp_nd_return_city','pp_nd_return_state','{{$delivery_data->InCaseNotDelReturnState}}','{{$delivery_data->InCaseNotDelReturnCity}}')">
		                            <option value="">Select Country</option>			                            @foreach($country as $key)
			                            <option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}","state_available":"{{$key->state_available}}"}' @if($delivery_data->InCaseNotDelReturnCountry == $key->Content) selected='selected' @endif >{{$key->Content}}</option>
			                        @endforeach
		                        </select>
		                     </div>
		                  </div>
		                  <div class="col-sm-4">
		                     <div class="form-group">
		                        <label class="control-label">State</label>
		                        <select name="nd_return_state" class="form-control required usename-#state# left-disabled" id="pp_nd_return_state" onchange="get_city('pp_nd_return_state','pp_nd_return_city','pp_nd_return_city','{{$delivery_data->InCaseNotDelReturnCity}}')">
		                            <option value="">Select State</option>
		                        </select>
		                     </div>
		                  </div>
		                  <div class="col-sm-4">
		                     <div class="form-group">
		                        <label class="control-label">City</label>
		                        <select  name="nd_return_city" class="form-control required usename-#city#" id="pp_nd_return_city">
		                            <option value="">Select City</option>
		                        </select>
		                     </div>
		                  </div>
		               </div>
		               <div class="row">
		                    <div class="col-sm-4">
		                       <div class="form-group">
		                          <label class="control-label">Zip Code/Postcode</label>
		                          <input class="form-control alpha-numeric usename-#zipcode#" placeholder="Zip Code/Postcode" name="nd_return_zipcode" maxlength="8" value="{{$delivery_data->InCaseNotDelReturnPincode}}">
		                       </div>
		                    </div>
		                </div>
	             	</div>
                    <div class="">
                       <hr>
                    </div>
                    <div class="">
                    	<div class="row">
	                  		<div class="col-xs-6">
			                     <div class="form-group">
			                        <button class="custom-btn1 btn" id="step2-next-btn">
			                           Next
			                           <div class="custom-btn-h"></div>
			                        </button>
			                        <a class="custom-btn1 btn text-center" onclick="switch_request_header('#sec1','#sec2',1)" href="javascript:void(0)">
			                           Back
                                       <div class="custom-btn-h"></div>
			                        </a>
			                     </div>
			                </div>

	                  </div>
                    </div>

                 </div>
              </div>

              <!-- End section2 -->
              <!-- Start section3 -->

              <div class="row" id="sec3" style="display:none;">
                 <div class="col-md-10 col-sm-offset-1">
                    <!--  <form class="form-vertical">    -->
                    <div class="col-sm-6">
                       <div class="form-group">
                     	<label class="control-label">Package Value</label>
                     	<div class="input-group error-input">
                     	<span class="input-group-addon">$</span>
                        	<input class="form-control required float maxlength-7 " placeholder="Package Value" name="package_value" maxlength="9" value="{{ucfirst($delivery_data->ProductCost)}}">
                        </div>
                        </div>
                    </div>


                    <div class="col-sm-12">
                       <div class="form-group">
                          <label class="control-label">Measurement Unit</label>
                          <div class="clearfix">
                             <label class="radio-inline">
                             	<input type="radio" name="measurement_unit"  value="cm_kg" @if($delivery_data->ProductWeightUnit == 'kg') checked @endif onclick="return unit_show();" >Metric (cm/Kg)
                             </label>
                             <label class="radio-inline">
                             	<input type="radio" name="measurement_unit" value="inches_lbs" @if($delivery_data->ProductWeightUnit == 'lbs') checked @endif onclick="return unit_show();">Imperial (Inches/lbs)
                             </label>
                          </div>
                       </div>
                    </div>
                    <div class="col-sm-3">
                       <div class="form-group">
                          <label class="control-label">Length</label>
                          <div class="input-group error-cm-input">
                          <input class="form-control required  float maxlength-9" placeholder="Length" name="length" maxlength="9" value="{{$delivery_data->ProductLength}}">
                          <span class="input-group-addon" id="length_unit"></span>
                          </div>
                       </div>
                    </div>
                    <div class="col-sm-3">
                       <div class="form-group">
                          <label class="control-label">Width</label>
                          <div class="input-group error-cm-input">
                          <input class="form-control required float maxlength-9" placeholder="Width" name="width" maxlength="9" value="{{$delivery_data->ProductWidth}}">
                          <span class="input-group-addon" id="width_unit"></span>
                          </div>
                       </div>
                    </div>
                    <div class="col-sm-3">
                       <div class="form-group">
                          <label class="control-label">Height</label>
                          <div class="input-group error-cm-input">
                          <input class="form-control required float maxlength-9" placeholder="Height" name="height" maxlength="9" value="{{$delivery_data->ProductHeight}}">
                          <span class="input-group-addon" id="height_unit"></span>
                          </div>
                       </div>
                    </div>
                    <div class="col-sm-3">
                       <div class="form-group">
                          <label class="control-label">Weight</label>
                          <div class="input-group error-cm-input">
                          <input class="form-control required float maxlength-9" placeholder="Weight" name="weight" maxlength="9" value="{{$delivery_data->ProductWeight}}">
                          <span class="input-group-addon" id="weight_unit"></span>
                          </div>
                       </div>
                    </div>
                    <div class="row">
                    	<div class="col-sm-12">
		                    <div class="col-sm-3">
		                       <div class="form-group">
<!--
		                          <label class="control-label">Quantity</label>
		                          <input type="number" class="form-control required numeric" name="quantity" placeholder="Quantity" value="1">
-->



                      <label class="control-label">Quantity</label>


                      	<div class="radio">
                            <label class="">
                             	<input type="radio" name="quantity_type" value="single" onclick="show_quantity();" @if($delivery_data->QuantityStatus == 'single') checked @endif>
                            </label>&nbsp;Single
                         	<label class="">
                         		<input type="radio" name="quantity_type" value="multiple" onclick="show_quantity();" @if($delivery_data->QuantityStatus == 'multiple') checked @endif>
                         	</label>&nbsp;Multiple
              			</div>
                      <!-- <div style="width:160px;" class="input-group number-spinner">
                         <span class="input-group-btn data-dwn">
                         	<a class="btn btn-primary" data-dir="dwn">
                         		<span class="glyphicon glyphicon-minus"></span>
                         	</a>
                         </span>
                         <input type="text" class="form-control text-center" value="{{$delivery_data->BoxQuantity}}" min="1" max="40"  id="quantity" name="quantity" >
                         <input type="hidden" value="">
                         <input type="hidden" value="" name="calculated_distance" id="calculated_distance">
                         <span class="input-group-btn data-up">
	                         <a class="btn btn-primary" data-dir="up">
	                         	<span class="glyphicon glyphicon-plus"></span>
	                         </a>
                         </span>

                   </div> -->
		                       </div>
		                    </div>
		                </div>
	                </div>

	                <div class="col-sm-12 col-xs-12" id="quantity_input">
                       		<div class="form-group">
		              		<input type="text" class="form-control required numeric" name="quantity" placeholder="Quantity" value="{{$delivery_data->BoxQuantity}}">
		              		</div>
		            </div>

		            @if(!isset($trip_data))
	                    <div class="col-sm-12">
	                       <div class="form-group">
	                          <label class="control-label">Shipping Mode</label> &nbsp;&nbsp;(Not all items can be shipped by Air. Items shipped by sea typically take longer to arrive.)
	                          <div class="clearfix">
	                             <label class="radio-inline">
	                             	<span class="pull-left"><input type="radio" name="travel_mode" value="air" onclick="toggle_category('.travel-mode-air','.travel-mode-ship','#package_category')" checked="checked" <?php if ($delivery_data->TravelMode == "air") {echo 'checked=="checked"';}?> ></span> <span class="Insurance_check">  By Air </span>
	                             </label>
	                             <label class="radio-inline">
	                             	<span class="pull-left"><input type="radio" name="travel_mode" value="ship" onclick="toggle_category('.travel-mode-ship','.travel-mode-air','#package_category')" <?php if ($delivery_data->TravelMode == "ship") {echo 'checked=="checked"';}?>>
	                             	  </span> <span class="Insurance_check"> By Sea  </span>
	                             </label>
	                          </div>
	                       </div>
	                    </div>
	                @else
	                	<div class="col-sm-12 ">
	                       	<div class="form-group">
	                       	<input type="hidden" name="travel_mode" value="{{$trip_data->TravelMode}}" >
	                    		<label class="control-label">Shipping Mode: </label>&nbsp;&nbsp;{{ucfirst($trip_data->TravelMode)}}
	                    	</div>
	                    </div>
	                @endif

	                @if(!isset($trip_data))
                    <div class="col-sm-12">
                       <div class="form-group">
                          <label class="control-label">Select Package Category</label>
                          <select class="form-control required" id="package_category" name="category">
                             <option value="">Select Category</option>

                             <?php foreach ($category as $key) {?>

                             	<option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}"}'  class="travel-mode-{{$key->TravelMode}}" <?php if (in_array(strtolower($key['Content']), [strtolower($delivery_data->Category)])) {echo 'selected="selected"';}?> >{{$key->Content}}
                             	</option>

                             <?php }?>
                          </select>
                       </div>
                    </div>
                    @else
                    	<div class="col-sm-12">
                       <div class="form-group">
                          <label class="control-label">Select Package Category</label>
                          <select class="form-control required" id="package_category" name="category">
                             <option value="">Select Category</option>

                             <?php foreach ($category2 as $key) {?>

                             	<option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}"}'  class="travel-mode-{{$key->TravelMode}}" <?php if (in_array(strtolower($key['Content']), [strtolower($delivery_data->Category)])) {echo 'selected="selected"';}?> >{{$key->Content}}
                             	</option>

                             <?php }?>
                          </select>
                       </div>
                    </div>
                    @endif

                    <div class="col-sm-12">
                       <div class="form-group">
                          <label class="control-label ">Package Description / Instructions</label>
                          <textarea rows="3" class="form-control" name="description">{{ucfirst($delivery_data->Description)}}</textarea>
                       </div>
                    </div>
                    <div class="col-sm-6">
                       <!--<div class="form-group">
                          <label class="control-label">Package care note</label>
                          <textarea rows="3" class="form-control required" name="care_note">{{ucfirst($delivery_data->PackageCareNote)}}</textarea>
                       </div>-->
                    </div>
					<div class="col-sm-12">
						<label class="control-label">Upload Default Package Image</label>
						<div class="selected-pic"> <div>
				           @if($delivery_data->ProductImage != '')
					           @if(file_exists(BASEURL_FILE.$delivery_data->ProductImage))
					            <a class="fancybox" rel="group" href="{{ ImageUrl.$delivery_data->ProductImage}}" >
					           <img id="senior-preview" src="{{ ImageUrl.$delivery_data->ProductImage}}"  width="200px" height="150px" />
					           </a>
					           @else
					           <img id="senior-preview" src="{{ ImageUrl}}/no-image.jpg"  width="200px" height="150px" />
					           @endif
				           @endif
						</div>
					    <label class="custom-input-file">
					    	{!! Form::file('default_image', ['class'=> 'custom-input-file','id'=>'senior_image','onchange'=>"image_preview(this,'senior-preview',event)"]) !!}
					    </label>
					</div>

					</div>
                  		<div class="col-sm-12">
                       <div class="form-group">
                          <label class="control-label">Upload Extra Image(s)(Optional)</label>
                          <div class="row">
	                          <div class="col-sm-3">
	                          	<input  type="file" placeholder="Browse" name="package_image2" class="valid-filetype-jpg,png,gif,jpeg,JPG,PNG,GIF,JPEG">
	                          	@if(isset($delivery_data->OtherImage[0]))
	                          		@if(file_exists(BASEURL_FILE.$delivery_data->OtherImage[0]))
	                          			<a class="fancybox" rel="group" href="{{ ImageUrl.$delivery_data->OtherImage[0]}}" >
	                          				<img id="senior-preview" src="{{ ImageUrl.$delivery_data->OtherImage[0]}}"  width="40px" height="40px" />
	                          			</a>
	                          		@endif
	                          	@endif
	                          </div>
	                          <div class="col-sm-3">
	                          	<input type="file" placeholder="Browse" name="package_image3" class="valid-filetype-jpg,png,gif,jpeg,JPG,PNG,GIF,JPEG">
	                          	@if(isset($delivery_data->OtherImage[1]))
	                          		@if(file_exists(BASEURL_FILE.$delivery_data->OtherImage[1]))
	                          			<a class="fancybox" rel="group" href="{{ ImageUrl.$delivery_data->OtherImage[1]}}" >
	                          				<img id="senior-preview" src="{{ ImageUrl.$delivery_data->OtherImage[1]}}"  width="40px" height="40px" />
	                          			</a>
	                          		@endif
	                          	@endif
	                          </div>
	                          <div class="col-sm-3">
	                          	<input type="file" placeholder="Browse" name="package_image4" class="valid-filetype-jpg,png,gif,jpeg,JPG,PNG,GIF,JPEG">
	                          	@if(isset($delivery_data->OtherImage[2]))
	                          		@if(file_exists(BASEURL_FILE.$delivery_data->OtherImage[2]))
	                          			<a class="fancybox" rel="group" href="{{ ImageUrl.$delivery_data->OtherImage[2]}}" >
	                          				<img id="senior-preview" src="{{ ImageUrl.$delivery_data->OtherImage[2]}}"  width="40px" height="40px" />
	                          			</a>
	                          		@endif
	                          	@endif
	                          </div>
	                          <div class="col-sm-3">
	                          	<input type="file" placeholder="Browse" name="package_image5" class="valid-filetype-jpg,png,gif,jpeg,JPG,PNG,GIF,JPEG">
	                          	@if(isset($delivery_data->OtherImage[3]))
	                          		@if(file_exists(BASEURL_FILE.$delivery_data->OtherImage[3]))
	                          			<a class="fancybox" rel="group" href="{{ ImageUrl.$delivery_data->OtherImage[3]}}" >
	                          				<img id="senior-preview" src="{{ ImageUrl.$delivery_data->OtherImage[3]}}"  width="40px" height="40px" />
	                          			</a>
	                          		@endif
	                          	@endif
	                          </div>
	                      </div>
                       </div>
                    </div>

                    <div class="col-sm-6">
                       <div class="form-group">
                          <label class="control-label">Receiver's Phone number <span class="red-star"> *</span></label>
                          <div class="row">
                             <div class="col-xs-4">
                                <input type="text" class="form-control required" placeholder="Country Code" name="country_code" maxlength="4" value="{{ucfirst($delivery_data->ReceiverCountrycode)}}">
                             </div>
                             <div class="col-xs-8">
                                <input type="text" class="form-control required numeric between-8-12" name="phone_number" placeholder="Phone Number" value="{{ucfirst($delivery_data->ReceiverMobileNo)}}" pattern=".{8,12}" title="8 to 12 numbers">
                             </div>
                          </div>
                       </div>
                    </div>
                    <div class="col-sm-6">
                       <div class="form-group">
                          <!-- <label class="control-label">Package Value</label>
                          <input class="form-control required float maxlength-7 " placeholder="$" name="package_value" maxlength="9" value="{{ucfirst($delivery_data->ProductCost)}}"> -->
                       </div>
                    </div>
                    <div class="col-sm-12">
                       <div class="form-group">
                          <label class="control-label">Insurance</label>
                          <div class="checkbox">
                             <label>
                             	<span class="pull-left row"><input type="radio" name="insurance" value="yes" <?php if ($delivery_data->InsuranceStatus == "yes") {echo 'checked=="checked"';}?> ></span> <span class="Insurance_check"> Yes </span>
                             </label>
                             &nbsp; &nbsp;
                             <label>
                             	<span class="pull-left"><input type="radio" name="insurance" value="no" <?php if ($delivery_data->InsuranceStatus == "no") {echo 'checked=="checked"';}?> ></span> <span class="Insurance_check"> No </span>
                             </label>
                          </div>
                       </div>
                       <div class="form-group">
                          <div class="checkbox">
                             <label>
                             	<span class="pull-left"><input type="checkbox" value="yes" name="need_package_material" <?php if ($delivery_data->PackageMaterial == "yes") {echo 'checked=="checked"';}?>></span> <span class="Insurance_check"> Need package material </span>
                             </label>
                          </div>
                       </div>

                        <div class="form-group">
                          <div class="checkbox">
                             <!-- <label>
                             	<span class="pull-left"><input type="checkbox" id="agree" name="terms_conditions"></span> <span class="Insurance_check"> I Accept the  <a href="{{url('terms-and-conditions')}}" target="_blank"> Terms and Conditions </a> .</span>
                             </label> -->
                          </div>
                       </div>
                    </div>
                    <div class="col-md-12">
                       <hr>
                    </div>
                    <div class="">
                    	<div class="">

	                  		<div class="col-xs-12">
			                     <div class="form-group">
			                        <button class="custom-btn1 btn"  id="calculate_loader">
			                           Next
			                           <div class="custom-btn-h"></div>
			                        </button>
			                         <a class="custom-btn1 btn  text-center" onclick="switch_request_header('#sec2','#sec3',2)" href="javascript:void(0)">
			                           Back
                                       <div class="custom-btn-h"></div>
			                        </a>
			                     </div>
			                </div>

	                  </div>
                    </div>
                 </div>
              </div>
              <!-- End section 3 -->

              <!-- Start section 4 -->
              <div class="row" id="sec4" style="display:none;">
                <div class="col-md-10 col-sm-offset-1">
                   <div class="col-sm-12">
                   		<div id="shipping_detail">
                   			Calculating! Please wait...
                   		</div>

                      <div class="">
                    	<div class="">
	                  		<div class="row">
			                     <div class="form-group">
			                        <button class="custom-btn1 btn" id="item_loader">
			                           Next
			                           <div class="custom-btn-h"></div>
			                        </button>
			                         <a class="custom-btn1 btn text-center" onclick="switch_request_header('#sec3','#sec4',3)" href="javascript:void(0)">
			                           Back
                                       <div class="custom-btn-h"></div>
			                        </a>
			                     </div>
			                </div>

	                  	</div>
                       </div>
                	</div>
                </div>
              </div>

                <!-- End section 4 -->
             {!! Form::close() !!}
        </div>
     </div>
  </div>
</div>
@endsection

@section('script')
@parent

<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyCT0uj-bffWWdOdwDp15LvrMko-OuoZUoY"></script>

{!! Html::script('theme/web/js/utility.js') !!}
{!! Html::script('theme/web/js/validation.js') !!}
{!! Html::script('theme/web/js/request.js') !!}
{!! Html::style('theme/date-time-picker/jquery.datetimepicker.css') !!}
{!! Html::script('theme/date-time-picker/jquery.datetimepicker.js') !!}

@endsection

@section('inline-script')
@parent
<script>

$('#pp_pickup_country').trigger('change');
$('#pp_dropoff_country').trigger('change');
$('#pp_nd_return_country').trigger('change');
$('#pp_return_country').trigger('change');


$(document).ready(function() {

    $(".fancybox").fancybox();

  });

$(function() {
       var action;
       $(".number-spinner a").mousedown(function () {
           btn = $(this);
           input = btn.closest('.number-spinner').find('input');
           btn.closest('.number-spinner').find('a').prop("disabled", false);

           if (btn.attr('data-dir') == 'up') {
               action = setInterval(function(){
                   if ( input.attr('max') == undefined || parseInt(input.val()) < parseInt(input.attr('max')) ) {
                       input.val(parseInt(input.val())+1);
                   }else{
                       btn.prop("disabled", true);
                       clearInterval(action);
                   }
               }, 50);
           } else {
               action = setInterval(function(){
                   if ( input.attr('min') == undefined || parseInt(input.val()) > parseInt(input.attr('min')) ) {
                       input.val(parseInt(input.val())-1);
                   }else{
                       btn.prop("disabled", true);
                       clearInterval(action);
                   }
               }, 50);
           }
       }).mouseup(function(){
           clearInterval(action);
       });
   });


  var newdate = new Date();
  newdate.setHours(newdate.getHours()+3);
  var minDateForTwo = newdate;
  var drop_off_date = {};

    $('#pickup_date').datetimepicker({
        format:'M d, Y h:i A',
        formatTime:'h:i A',
        timepicker:true,
        datepicker:true,
        step:30,
        minDate : new Date(),
        minTime : newdate,
        onChangeDateTime:function( ct ){
            minDateForTwo = ct;
            drop_off_date.minDate = ct;
            var dt = new Date();
            dt.setHours(23);dt.setMinutes(59);dt.setSeconds(59);

            if(ct < dt) {
              $('#pickup_date').datetimepicker({  minTime: newdate });
              drop_off_date.minTime = ct;

              if(ct < newdate) {
                $('#pickup_date').val(dt.dateFormat('M d, Y ')+newdate.dateFormat('h:i A'));
                drop_off_date.minTime = minDateForTwo;
              }
            } else {
              dt.setHours(0);
              $('#pickup_date').datetimepicker({  minTime: dt });
              drop_off_date.minTime = false;
            }
            $('#drop_off_date').datetimepicker(drop_off_date);
            $('#drop_off_date').val('');
         },
   });

   $("#drop_off_date").datetimepicker({
       format:'M d, Y h:i A',
       formatTime:'h:i A',
       timepicker:true,
       datepicker:true,
       step:30,
       minDate : new Date(),
       minTime : minDateForTwo,
       closeOnDateSelect : false,
       onChangeDateTime:function( ct ){
       	  	if(minDateForTwo > ct) {
       		$("#drop_off_date").val(minDateForTwo.dateFormat('M d, Y h:i A'));
       		 return false;
        }

         var date2option = {};
         if(($("#drop_off_date").val()).trim() != '') {

            $('#pickup_time').datetimepicker({  maxDate:ct, maxTime:ct  })

            if(ct < minDateForTwo) {
              $('#drop_off_date').val(ct.dateFormat('M d, Y ')+minDateForTwo.dateFormat('h:i A'));
              date2option.minTime = minDateForTwo;
            } else {
              date2option.minTime = false;
            }
            $('#pickup_date').datetimepicker(date2option);

          }
       },
   });



</script>

<script type="text/javascript">



function journey_show()
{
	if($('[name="journey_type"]:checked').val() == "return")
		{
			$("#return_journy").trigger("onclick");
		}
}

journey_show();

function image_preview(obj,previewid,evt)
{
    var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
    if ($.inArray($(obj).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
       $(obj).val('');
       $('#'+previewid).attr('src', '{{ImageUrl}}/user-no-image.jpg');
        alert("Only "+fileExtension.join(', ')+" formats are allowed.");
     } else {

       var file = evt.target.files[0];
       if (file)
       {
         var reader = new FileReader();

         reader.onload = function (e) {
             $('#'+previewid).attr('src', e.target.result)
         };
         reader.readAsDataURL(file);
       }
     }
}

@if($delivery_data->TravelMode == "air")
	toggle_category('.travel-mode-air','.travel-mode-ship','#package_category');
@else
	toggle_category('.travel-mode-ship','.travel-mode-air','#package_category');
@endif
$('#package_category').val('{"id":"{{$delivery_data->CategoryId}}","name":"{{$delivery_data->Category}}"}');


function unit_show()
{
	var unit =$('input[name=measurement_unit]:checked').val();

	if(unit == 'cm_kg'){
		$('#length_unit').html('Cm');
		$('#width_unit').html('Cm');
		$('#height_unit').html('Cm');
		$('#weight_unit').html('Kg');

	}else{
		$('#length_unit').html('Inches');
		$('#width_unit').html('Inches');
		$('#height_unit').html('Inches');
		$('#weight_unit').html('lbs');
	}

}
unit_show();

function show_quantity()
{
	var type =$('input[name=quantity_type]:checked').val();
	if(type == 'multiple'){
		$('#quantity_input').show();
	}else{
		$('#quantity_input').hide();
	}

}
show_quantity();

</script>

<script>
var newdate = new Date();
  newdate.setHours(newdate.getHours()+3);

var minDateForTwo = newdate;

var drop_off_date = {};
    $('#pickup_date').datetimepicker({
        format:'M d, Y h:i A',
        formatTime:'h:i A',
        timepicker:true,
        datepicker:true,
        step:30,
        minDate : new Date(),
        minTime : newdate,
        onChangeDateTime:function( ct ){
            minDateForTwo = ct;
            drop_off_date.minDate = ct;
            var dt = new Date();
            dt.setHours(23);dt.setMinutes(59);dt.setSeconds(59);

            if(ct < dt) {
              $('#pickup_date').datetimepicker({  minTime: newdate });
              drop_off_date.minTime = ct;

              if(ct < newdate) {
                $('#pickup_date').val(dt.dateFormat('M d, Y ')+newdate.dateFormat('h:i A'));
                drop_off_date.minTime = minDateForTwo;
              }
            } else {
              dt.setHours(0);
              $('#pickup_date').datetimepicker({  minTime: dt });
              drop_off_date.minTime = false;
            }
            $('#drop_off_date').datetimepicker(drop_off_date);
            $('#drop_off_date').val('');
         },
   });

   $("#drop_off_date").datetimepicker({
       format:'M d, Y h:i A',
       formatTime:'h:i A',
       timepicker:true,
       datepicker:true,
       step:30,
       minDate : new Date(),
       minTime : minDateForTwo,
       closeOnDateSelect : false,
       onChangeDateTime:function( ct ){
          var date2option = {};
        if(($("#drop_off_date").val()).trim() != '') {

            $('#pickup_time').datetimepicker({  maxDate:ct, maxTime:ct  })

            if(ct < minDateForTwo) {
              $('#drop_off_date').val(ct.dateFormat('M d, Y ')+minDateForTwo.dateFormat('h:i A'));
              date2option.minTime = minDateForTwo;
            } else {
              date2option.minTime = false;
            }
            $('#drop_off_date').datetimepicker(date2option);


          }
       },
   });

</script>

@endsection
