
@extends('Page::layout.one-column-page')
@section('content')
<div class="container">
   <div class="row">
   	  <div class="col-sm-12">
      <h2 class="color-blue mainHeading">Edit Request</h2>
      <br />
      </div>
      <?php /*  echo "<pre>"; print_r($users);  die; */ ?>
      
         {!! Form::open(array('url' => 'post-edit-profile' , 'files' => 'true') ) !!}
        
         <div class="col-sm-12 col-xs-12">
            <div class="box-shadow clearfix">
               <h3>Please fill some required field</h3>
               <hr/>
                <div class="row">
					<div class="col-sm-8">
                  <div class="col-sm-12">
                   
                        <div class="form-group">
                           <label class="control-label">Product Title</label>
                           <input type="text" value="" placeholder="Product Title" name="first_name" class="form-control" maxlength="40"> 		  
                           <p class="help-block red" style="color:red;"  >{{$errors->first('first_name')}} </p>
                       </div>
                       
                     </div>                    
                     <div class="col-sm-6">
                        <div class="form-group">
                           <label class="control-label">Date</label>
                           <div class="input-group">
										<input type="date"  placeholder="Search Date" id="startdate" value='<?php echo Input::get('search_date'); ?>'  class="form-control" name ="search_date" readonly="readonly">
											<div class="input-group-addon"><i class="fa fa-calendar"></i>
											</div>
									</div>
                       </div> 
                     </div>
                     <div class="col-sm-6">
							<div class="form-group">
								{!! Form::label('Status', 'Status', ['class'=>'control-lable']) !!}                   
								{!! Form::select('Status', ['pending'=>'Pending','delivered'=>'Delivered','out for pickup'=>'Out for pickup','pending trip'=>'Pending trip','out for delivery'=>'Out for delivery',
									                        'accepted'=>'Accepted','cancel'=>'Cancel','ready'=>'Ready','online_pending'=>'Online pending'],Input::get('Status'),['class'=>'form-control', 'placeholder'=> 'Select Status']) !!}
								
								</div>
							</div>
                         <div class="col-sm-12">
                        <div class="form-group">
                           <label class="control-label">Drop of Location</label>
                           <input type="text" value="" name="last_name" placeholder="Location" class="form-control" maxlength="200">  
                           <p class="help-block red" style="color:red;"  >{{$errors->first('Last_name')}} </p>
                        </div>
                     </div>
                        
                     
                     
                            </div>
                            <div class="col-sm-4">
                  <div class="col-sm-4">
                     <div class="selected-pic">
                        <div> 

                           <img id="senior-preview" src=""  width="200px" height="150px" />

                           <img id="senior-preview" src=""  width="200px" height="150px" />
                          
                        </div>
                        <label class="custom-input-file">
                        {!! Form::file('user_image', ['class'=> 'custom-input-file','id'=>'senior_image']) !!}
                        </label>
                     </div>
                  </div>
               </div>
             </div>
              
               
     
              
               <div class="col-sm-12">
                  <hr />
               </div>
               <div class="col-sm-8 col-sm-offset-2">
                  <div class="form-group">
                     <button class="custom-btn1 btn">
                        update
                        <div class="custom-btn-h"></div>
                     </button>
                     <br></br><br></br>
                  </div>
               </div>
            </div>
           
            {!! Form::close() !!}
         </div>
   </div>
</div>
@parent
{!! Html::style('theme/date-time-picker/jquery.datetimepicker.css') !!}       
{!! Html::script('theme/date-time-picker/jquery.datetimepicker.js') !!}
@parent
<script>
   $('#senior_image').on('change', function(evt)
   {
   var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
   if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
     $(this).val('');
     $('#senior-preview').attr('src', '{{ImageUrl}}/user-no-image.jpg');
      alert("Only "+fileExtension.join(', ')+" formats are allowed.");
   } else {
   
     var file = evt.target.files[0];
     if (file) 
     {
       var reader = new FileReader();
   
       reader.onload = function (e) {
           $('#senior-preview').attr('src', e.target.result)
       };
       reader.readAsDataURL(file);
     }
   }          
   });
    $('#startdate').datetimepicker({            
       format:'m/d/Y',
       timepicker:false,
       onChangeDateTime:function( ct ){
           $('#enddate').datetimepicker({  minDate:ct  })
       },
   });
      $('#enddate').datetimepicker({            
       format:'m/d/Y',
       timepicker:false,
       onChangeDateTime:function( ct ){
           $('#startdate').datetimepicker({  minDate:ct  })
       },
   });
</script>
@endsection
