@extends('Page::layout.one-column-page')
@section('content')
<div class="modal fade" id="item_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title color-black" id="myModalLabel">&nbsp;&nbsp;Edit Address</h4>
         </div>
         <div class="clearfix"></div>
         {!! Form::model('', ['name' => 'edit_item', 'id' =>'edit_item', 'method' => 'post']) !!}
         <div class="modal-body">
            <div class="row">
               <div class="col-sm-12">
                  <div class="form-group">
                     <label class="control-label">Item Name</label>
                     <input class="form-control required"   placeholder="Item Name"  name="item_name" id="item_name" maxlength="45" >
                     <input type="hidden" value="" name="item_id" id="item_id">
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-sm-12">
                  <div class="form-group">
                     <label class="control-label">Item URL</label>
                     <input type="text" class="form-control required valid_url"  name="add_item_url"  id="add_item_url" placeholder="Item URL">
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-sm-12">
                  <div class="form-group">
                     <label class="control-label">Shipping cost(from retailer to aquantu'os facility)</label>
                     <input type="text" class="form-control float"  name="shipping_cost"  id="add_item_url" placeholder="$" maxlength="15">
                  </div>
               </div>
            </div>            
            

            <div class="row">
               <div class="col-sm-12">
                  <div class="form-group">
                     <label class="control-label">Measurement Units</label>
                     <div class="radio">
                        <label class="">
                        <input type="radio" name="measurement_unit" id="measurement_unit" checked value="cm_kg">
                        </label>&nbsp;Metric (cm/Kg)
                        <label class="">
                        <input type="radio" name="measurement_unit" id="measurement_unit2" value="inches_lbs" >
                        </label>&nbsp;Imperial (Inches/lbs)
                     </div>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-sm-12">
                  <label class="control-label">Item Specification</label>
               </div>
               <div class="col-sm-3">
                  <div class="form-group">
                     <label class="control-label">Lenght</label>
                     <input class="form-control required  float maxlength-9" placeholder="Length"  id="length" name="length" maxlength="9" />
                  </div>
               </div>
               <div class="col-sm-3">
                  <div class="form-group">
                     <label class="control-label">Width</label>
                     <input class="form-control required float maxlength-9" placeholder="Width" name="width"  id="width" maxlength="9">
                  </div>
               </div>
               <div class="col-sm-3">
                  <div class="form-group">
                     <label class="control-label">Height</label>
                     <input class="form-control required float maxlength-9" placeholder="Height" name="height" id="height" maxlength="9">
                  </div>
               </div>
               <div class="col-sm-3">
                  <div class="form-group">
                     <label class="control-label">Weight</label>
                     <input class="form-control required float maxlength-9" placeholder="weight" name="weight" id="weight" maxlength="9">
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-sm-6">
                  <div class="form-group">
                     <label class="control-label">Item Price</label>
                     <input class="form-control required float maxlength-7 " placeholder="$" name="item_price" id="item_price" maxlength="9">
                  </div>
               </div>
               <div class="col-sm-6">
                  <div class="form-group">
                     <label class="control-label">Quantity</label>
                     <div style="width:160px;" class="input-group number-spinner">
                        <span class="input-group-btn data-dwn">
                        <button class="btn btn-primary" data-dir="dwn" onclick="return false;">
                        <span class="glyphicon glyphicon-minus"></span>
                        </button>
                        </span>
                        <input type="text" class="form-control text-center" value="1" min="1" max="40"  id="quentity" name="quentity" >
                        <input type="hidden" value="">
                        <span class="input-group-btn data-up" onclick="return false;">
                        <button class="btn btn-primary" data-dir="up">
                        <span class="glyphicon glyphicon-plus"></span>
                        </button>
                        </span>
                     </div>
                  </div>
               </div>
            </div>

            <div class="row">
               <div class="col-sm-6">
                  <div class="form-group">
                     <label class="control-label">Shipping Mode</label>&nbsp;&nbsp;(Air: receive package in 13 days or less. Sea: receive package in 6 to 8 weeks)
                     <div class="radio">
                        <label class="">
                        <input type="radio" name="travel_mode" id="travel_mode_air" value="air" onclick="toggle_category('.travel-mode-air','.travel-mode-ship','#package_category')" checked="checked">
                        </label>&nbsp;By Air
                        <label class="">
                        <input type="radio" name="travel_mode" id="travel_mode_ship" value="ship" onclick="toggle_category('.travel-mode-ship','.travel-mode-air','#package_category')">
                        </label>&nbsp;By Sea
                     </div>
                  </div>
               </div>

                <div class="col-sm-6">
                  <div class="form-group">
                     <label class="control-label">Insurance</label>
                     <div class="radio">
                        <label class="row">
                        <label>
                        <input type="radio" name="insurance" id="insurance" value="yes" > 
                        </label>&nbsp;Yes
                        &nbsp;&nbsp; 
                        <label>
                        <input type="radio" name="insurance"  id="insurance2" value="no" checked> 
                        </label>&nbsp;No
                     </div>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-sm-6">
               <div class="row">
               <div class="col-sm-12">
                  <div class="form-group">
                     <label class="control-label">Select Package Category</label>
                     <select class="form-control required" id="package_category" name="category">
                        <option value="">Select Category</option>
                        <?php foreach($category as $key){ ?>
                        <option  value='{"id":"{{$key->_id}}","name":"{{$key->Content}}"}' class="travel-mode-{{$key->TravelMode}}">{{$key->Content}}</option>
                        <?php } ?>
                     </select>
                  </div>
                  </div>
                   <div class="col-sm-12">
                  <div class="form-group">
                     <label class="control-label">Description/Instructions</label>
                     <textarea class="form-control required" name="description" rows="3" id="description" maxlength="500" ></textarea>
                  </div>
               </div>
               </div>
               </div>
               <div class="col-sm-6">
                   <div class="selected-pic">
                         <label class="control-label">Image</label>
                        <div> 
                           <a class="fancybox" rel="group" href="" >  
                           <img id="buy_for_me_preview" name="item_image" src=""  width="200px" height="150px" />
                           </a>
                        </div>
                        <label class="custom-input-file">
                        {!! Form::file('item_image', ['class'=> 'custom-input-file','id'=>'senior_image','onchange'=>"image_preview(this,'buy_for_me_preview',event)"]) !!}
                        </label>
                     </div>
                  </div>
            </div>
            </div>
            
              
            
            
         <input type="hidden" value="" name="address_id" id="address_id">
         <div class="clearfix"></div>
         <div class="modal-footer">
            
            {!! Form::button('Update', ['class' => 'btn custom-btn1','id'=>'item_loader','type'=>'submit']) !!}
             {!! Form::button('Close', ['class' => 'btn custom-btn1', 'data-dismiss'=>'modal']) !!} 
         </div>
         {!! Form::close() !!}
      </div>
   </div>
</div>
<div aria-labelledby="exampleModalLabel" role="dialog" tabindex="-1" id="exampleModal" class="modal fade in">
   <div role="document" class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header" id="Addaddress2">
            <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button>
            <h4 id="exampleModalLabel" class="modal-title">Add New Address</h4>
         </div>
         {!! Form::model('', ['name' => 'new_address', 'id' => 'Addaddress', 'method' => 'POST']) !!}
         <div class="panel-body">
            <div class="row">
               <div class="col-sm-12">
                  <div class="form-group">
                     <label>Address Line 1</label>
                     <input type="text" placeholder="Address" name="address_line_1" id="address_line_1"  maxlength= "125" class="form-control required">
                  </div>
               </div>
               <div class="col-sm-12">
                  <div class="form-group">
                     <label>Address Line 2</label>
                     <input type="text" placeholder="Address" name="address_line_2" id="address_line_2"  maxlength= "125" class="form-control">
                  </div>
               </div>
               <div class="col-sm-6">
                  <div class="form-group">
                     <label>Country</label>
                     <select name="country" class="form-control required" id="pp_pickup_country" onchange="get_state('pp_pickup_country','pp_pickup_state','pp_pickup_city','pp_pickup_state','','')">
                        <option value="" id="country">Select Country</option>
                        @foreach($country as $key)
                        <option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}"}'>{{$key->Content}}</option>
                        @endforeach
                     </select>
                  </div>
               </div>
               <div class="col-sm-6">
                  <div class="form-group">
                     <label>State</label>
                     <select name="state" class="form-control required" id="pp_pickup_state" onchange="get_city('pp_pickup_state','pp_pickup_city','pp_pickup_city','')">
                        <option value="">Select State</option>
                     </select>
                  </div>
               </div>
               <div class="col-sm-6">
                  <div class="form-group">
                     <label>City</label>
                     <select  name="city" class="form-control required" id="pp_pickup_city">
                        <option value="">Select City</option>
                     </select>
                  </div>
               </div>
               <div class="col-sm-6">
                  <div class="form-group">
                     <label>Zipcode</label>
                     <input type="text" placeholder="Zipcode" name="zipcode" id="zipcode"  maxlength="8" class="form-control alpha-numeric">
                  </div>
               </div>
               </hr>
               <div class="col-sm-8 col-sm-offset-2" align="center">
                  <div class="row">
                     <div class="col-sm-6 col">
                        <div class="form-group">
                           <button class="custom-btn1 btn-block" id="address_loader" >
                              Submit
                              <div class="custom-btn-h"></div>
                           </button>
                        </div>
                     </div>
                     <div class="col-sm-6 col">
                        <div class="form-group">
                           <button class="custom-btn1 btn-block"  data-dismiss="modal">
                              Close
                              <div class="custom-btn-h"></div>
                           </button>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         {!! Form::close() !!}
      </div>
   </div>
</div>

<div class="container">
<div class="row">
<div class="col-sm-12">
   <h2 class="color-blue">Buy for Me</h2>
   <br />
</div>
<div class="col-sm-12" id="sec1">
   <div class="box-shadow">
      <h3>Please complete the following fields</h3>
      <hr />
      <!------------------step1------------------>
      <div class="row">
         <div class="col-sm-12">
            <div class="step_box three_step clearfix">
            <div class="step first selected">
               <div>1</div>
            <p class="text-center colol-black">Package Details</p>
            </div>

            <div class="step inner">
               <div>2</div>
               <p class="text-center colol-black">Drop off Address</p>
            </div>

            <div class="step last" >
               <div>3</div>
               <p class="text-center colol-black">Payment</p>
            </div>

         </div>
         </div>
         <div id="table_hide" style="@if(count($additem) <= 0) display:none; @endif">
            <div class="col-md-10 col-sm-offset-1">
               <div class="col-sm-12 text-right">
                  <button class="btn btn-default"  onclick="$('#add_item').show();$('#table_hide').hide();"   >Add Item</button></br>
                  </br>        
               </div>
            </div>
            <div class="col-md-10 col-sm-offset-1">
               <div class="col-sm-12"  >
                  <table class="custom-table table table-bordered" name="item_list" id="item_list">
                     <thead>
                        <tr>
                           <th>S.No.</th> 
                           <th>Item Name</th>
                           <th>Item Price</th>
                           <th>Item Weight</th>
                           <th>Quantity</th>
                           <th>Description</th>
                           <th>Action</th>
                        </tr>
                     </thead>
                     <tbody>
                     <?php $sno=1;     ?>
                        @foreach($additem as $value)
                        <?php
                           $weight_unit ='lbs'; 
                           if($value->measurement_unit == 'cm_kg')
                           {
                               $weight_unit ='kg'; 
                           }
                           ?>
                        <tr id="row-{{$value->_id}}">
                           <td>{{$sno++}}</td>
                           <td>{{ucfirst($value->item_name)}}</td>
                           <td>${{number_format((float)$value->item_price,2,'.','')}}</td>
                           <td>{{ucfirst($value->weight)}}&nbsp;{{ucfirst($weight_unit)}}</td>
                           <td>{{ucfirst($value->quentity)}}&nbsp;</td>
                           <td>{{ucfirst($value->description)}}</td>
                           <td colspan="2" >&nbsp;&nbsp;
                              <span><a type=""  data-toggle="modal" onclick="get_edit_item('{{json_encode($value)}}')" id="Edit" title="Edit" data-whatever="@mdo" href="#item_modal"> <i class="fa fa-pencil"></i></a></span>&nbsp;&nbsp;
                              <a title="delete" id="Delete"  onclick="remove_record('delete_item/{{$value->_id}}/DeleteItem','{{$value->_id}}')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>
                           </td>
                        </tr>
                        @endforeach                                        
                        </tr>                               
                     </tbody>
                  </table>
                  <div class="">
                     <div class="col-sm-6">
                        <div class="form-group">
                           <button class="custom-btn1 btn"  onclick="$('#sec1').hide();$('#sec2').show();">
                           Next 
                           <div class="custom-btn-h"></div>
                           </button>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-sm-12" style="@if(count($additem) > 0) display:none; @endif" id="add_item" >
            {!! Form::open(['class'=>'form-vertical','name'=>'buy_for_me_form_add','id' => 'buy_for_me_form_add','files' => true ]) !!}
            <input type="hidden" 
            <div class="col-sm-12">
               <div class="form-group">
                  <label class="control-label">Item Image</label>
                  <input type="file" name="item_image" id="item_image" class="valid-filetype-jpg,png,gif,jpeg" />
               </div>
            </div>
            <div class="col-sm-12">
               <div class="form-group">
                  <label class="control-label">Item Name</label>
                  <input type="hidden" value="" name="item_id" id="item_id">
                  <input class="form-control required"   placeholder="Item Name"  name="item_name" maxlength="45" >
               </div>
            </div>
            <div class="col-sm-12">
               <div class="form-group">
                  <label class="control-label">Item URL</label>
                  <input type="text" class="form-control required valid_url"  name="add_item_url"  id="add_item_url" placeholder="Item URL">
               </div>
            </div>
            <div class="col-sm-12">
               <a href="javascript:void()" target="iframe_a" type="button" class="btn btn-primary blue-btn" data-target="#amazonModal" data-toggle="modal" onclick="open_market('amazon')">AMAZON</a>&nbsp;&nbsp;
               <a type="button" class="btn btn-primary blue-btn" data-target="#amazonModal" data-toggle="modal" onclick="open_market('ebay')">EBAY </a>&nbsp;&nbsp;
               <a type="button" class="btn btn-primary blue-btn" data-target="#amazonModal" data-toggle="modal" onclick="open_market('walmart')">WALMART</a>&nbsp;&nbsp;
               <a type="button" class="btn btn-primary blue-btn" data-target="#amazonModal" data-toggle="modal" onclick="open_market('flipkart')">FLIPKART</a>&nbsp;&nbsp;
               <a type="button" class="btn btn-primary blue-btn" data-target="#amazonModal" data-toggle="modal" onclick="open_market('snapdeal')">SNAPDEAL</a>&nbsp;&nbsp;
               <p></p>
            </div>
            <div class="col-sm-12">
               <div class="form-group">
                  <label class="control-label">Shipping cost(from retailer to aquantu'os facility)</label>
                  <input type="text" class="form-control float"  name="shipping_cost"  id="add_item_url" placeholder="$" maxlength="15">
               </div>
            </div>
            <div class="col-sm-12">
               <div class="form-group">
                  <label class="control-label">Measurement Units</label>
                  <div class="radio">
                     <label class="">
                     <input type="radio" name="measurement_unit" id="measurement_unit" checked value="cm_kg">
                     </label>&nbsp;Metric (cm/Kg)
                     <label class="">
                     <input type="radio" name="measurement_unit" id="measurement_unit2" value="inches_lbs" >
                     </label>&nbsp;Imperial (Inches/lbs)
                  </div>
               </div>
            </div>
            <div class="col-sm-12">
               <label class="control-label">Item Specification</label>
            </div>
            <div class="col-sm-3">
               <div class="form-group">
                  <label class="control-label">Lenght</label>
                  <input class="form-control required  float maxlength-9" placeholder="Length"  id="length" name="length" maxlength="9" />
               </div>
            </div>
            <div class="col-sm-3">
               <div class="form-group">
                  <label class="control-label">Width</label>
                  <input class="form-control required float maxlength-9" placeholder="Width" name="width"  id="width" maxlength="9">
               </div>
            </div>
            <div class="col-sm-3">
               <div class="form-group">
                  <label class="control-label">Height</label>
                  <input class="form-control required float maxlength-9" placeholder="Height" name="height" id="height" maxlength="9">
               </div>
            </div>
            <div class="col-sm-3">
               <div class="form-group">
                  <label class="control-label">Weight</label>
                  <input class="form-control required float maxlength-9" placeholder="weight" name="weight" id="weight" maxlength="9">
               </div>
            </div>
            <div class="col-sm-6">
               <div class="form-group">
                  <label class="control-label">Item Price</label>
                  <input class="form-control required float maxlength-7 " placeholder="$" name="item_price" id="item_price" maxlength="9">
               </div>
            </div>
            <div class="col-sm-6">
               <div class="form-group">
                  <label class="control-label">Quantity</label>
                  <div style="width:160px;" class="input-group number-spinner">
                     <span class="input-group-btn data-dwn">
                     <button class="btn btn-primary" data-dir="dwn" onclick="return false;">
                     <span class="glyphicon glyphicon-minus"></span>
                     </button>
                     </span>
                     <input type="text" class="form-control text-center" value="1" min="1" max="40"  id="quentity" name="quentity" >
                     <input type="hidden" value="">
                     <span class="input-group-btn data-up">
                     <button class="btn btn-primary" data-dir="up" onclick="return false;">
                     <span class="glyphicon glyphicon-plus"></span>
                     </button>
                     </span>
                  </div>
               </div>
            </div>
            <div class="col-sm-12">
               <div class="form-group">
                  <label class="control-label">Shipping Mode</label>&nbsp;&nbsp;(Air: receive package in 13 days or less. Sea: receive package in 6 to 8 weeks)
                  <div class="radio">
                     <label class="">
                     <input type="radio" name="travel_mode" id="travel_mode_air" value="air" onclick="toggle_category('.travel-mode-air','.travel-mode-ship','#package_category')" checked="checked">
                     </label>&nbsp;By Air
                     <label class="">
                     <input type="radio" name="travel_mode" id="travel_mode_ship" value="ship" onclick="toggle_category('.travel-mode-ship','.travel-mode-air','#package_category')">
                     </label>&nbsp;By Sea
                  </div>
               </div>
            </div>
            <div class="col-sm-12">
               <div class="form-group">
                  <label class="control-label">Select Package Category</label>
                  <select class="form-control required section1" id="package_category" name="category">
                     <option value="">Select Category</option>
                     <?php foreach($category as $key){ ?>
                     <option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}"}' class="travel-mode-{{$key->TravelMode}}">{{$key->Content}}</option>
                     <?php } ?>
                  </select>
               </div>
            </div>
            <div class="col-sm-12">
               <div class="form-group">
                  <label class="control-label">Description/Instructions</label>
                  <textarea class="form-control required" name="description" rows="3" id="description" maxlength="500" ></textarea>
               </div>
            </div>
            <div class="col-sm-12">
               <div class="form-group">
                  <label class="control-label">Insurance</label>
                  <div class="radio">
                     <label>
                     <input type="radio" name="insurance" id="insurance" value="yes" > 
                     </label>&nbsp;Yes
                     &nbsp; &nbsp;
                     <label>
                     <input type="radio" name="insurance"  id="insurance2" value="no" checked> 
                     </label>&nbsp;No
                  </div>
               </div>
               <div class="col-md-12">
                  <hr />
               </div>
               <div class="">
               <div class="row">
                  <div class="col-xs-6">
                     <div class="form-group">
                        <button class="custom-btn1 btn">ADD
                        <div class="custom-btn-h"></div>
                        </button>
                        <a class="custom-btn1 btn"  onclick="$('#add_item').hide();$('#table_hide').show();"   >Back<div class="custom-btn-h"></div>
                        </a>
                     <div class="error-msg"></div></div>
                  </div>
               </div>
            </div>

            </div>            
            {!! Form::close() !!}
         </div>
      </div>
   </div>
</div>

<!------------------step1------------------>
<!------------------step2------------------>
{!! Form::open(['class'=>'form-vertical','name'=>'buy_for_me_form','id' => 'buy_for_me_form' ]) !!}

<div class="col-sm-12" id="sec2" style="display:none;">
   <div class="box-shadow">
      <h3>Please fill some required feild</h3>
      <hr />
      <!------------------step1------------------>
      <div class="col-sm-12">

         <div class="step_box three_step clearfix">
            <div class="step first selected">
               <div>1</div>
            <p class="text-center colol-black">Package Details</p>
            </div>

            <div class="step inner selected">
               <div>2</div>
               <p class="text-center colol-black">Drop off Address</p>
            </div>

            <div class="step last" >
               <div>3</div>
               <p class="text-center colol-black">Payment</p>
            </div>

         </div>

      </div>
      <div class="row">
         <div class="col-md-10 col-sm-offset-1">
            <div class="col-sm-10">
               <div class="form-group">
                  <label class="control-label">Select Saved Address</label>
                  <select class="form-control required usename-#address#" name="address" id="dropoff_address">
                     <option value="">Select address</option>
                     @foreach($address as $value)
                     <option value="{{json_encode($value)}}">{{ucfirst($value->address_line_1)}} {{$value->city}} {{$value->state}} {{$value->country}} {{$value->zipcode}}</option>
                     @endforeach
                  </select>
               </div>
            </div>
            <div class="col-sm-2">
               <div class="form-group">
                  <label class="control-label">&nbsp;</label>
                  <button type="button" class="btn btn-primary blue-btn" data-target="#exampleModal" data-toggle="modal">Add New Address</button>
               </div>
            </div>
            <div class="col-sm-6">
               <div class="form-group">
                  <label class="control-label">Receiver's Phone Number <span class="red-star"> *</span></label>  
                  <div class="row">
                     <div class="col-xs-4">
                        <input type="text" class="form-control required numeric maxlength-3 usename-#country_code#" placeholder="Country Code" name="country_code" maxlength="3">
                     </div>
                     <div class="col-xs-8">
                        <input type="text" class="form-control required numeric between-8-12 usename-#phone_number#" name="phone_number" placeholder="Phone Number" maxlength="12">
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-sm-12">
               <div class="form-group">
                  <label class="control-label">Return address (if item is not delivered)</label>
                  <div class="checkbox">
                     <label>
                     <input type="checkbox"> 
                     </label> Return to Aquantuo
                  </div>
               </div>
            </div>
            <div class="">
               <div class="row">
                  <div class="col-xs-6">
                     <div class="form-group">
                        <button class="custom-btn1 btn" id="nextbt">
                        Next
                        <div class="custom-btn-h"></div>
                        </button>
                         <a class="custom-btn1 btn text-center"  onclick="switch_request_header('#sec1','#sec2',0)"  href="javascript:void(0)" >
                           Back
                           <div class="custom-btn-h"></div>
                        </a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="" id="sec3" style="display:none;">
   <div class="col-md-12">
      <div class="box-shadow">
         <div class="step_box three_step clearfix">
         <div class="step first selected">
            <div>1</div>
            <p class="text-center colol-black">Package Details</p>
         </div>
         <div class="step inner selected">
            <div>2</div>
            <p class="text-center colol-black">Drop off Address</p>
         </div>
         <div class="step last selected" >
            <div>3</div>
            <p class="text-center colol-black">Payment</p>
         </div>
      </div>
      <div class="col-sm-12">
         <div id="shipping_detail">
               Calculating! Please wait...
         </div>
      </div>
         <div class="">
            <div class="row">
               <div class="col-xs-6">
                  <div class="form-group">
                        <button class="custom-btn1 btn" id="creating_req_btn">
                           Next 
                           <div class="custom-btn-h"></div>
                        </button>
                        <a class="custom-btn1 btn text-center" onclick="switch_request_header('#sec2','#sec3',0)" href="javascript:void(0)">
                        Back
                        <div class="custom-btn-h"></div>
                        </a>
                  </div>
               </div>
                  <input type="hidden" name="distance" id="distance">
                  <input type="hidden" name="promo_code" id="promo_code_olp_promo">
            </div>
         </div>
      </div>
   </div>
</div>
{!! Form::close() !!}

@endsection   
@section('script')
@parent
{!! Html::script('theme/web/js/utility.js') !!}
{!! Html::script('theme/web/js/validation.js') !!}
{!! Html::script('theme/web/js/buyforme.js') !!}
{!! Html::style('theme/date-time-picker/jquery.datetimepicker.css') !!}       
{!! Html::script('theme/date-time-picker/jquery.datetimepicker.js') !!}
<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyCT0uj-bffWWdOdwDp15LvrMko-OuoZUoY"></script>
@endsection
@section('inline-script')
@parent
<script type="text/javascript">
   $(function() {
       var action;
       $(".number-spinner button").mousedown(function () {
           btn = $(this);
           input = btn.closest('.number-spinner').find('input');
           btn.closest('.number-spinner').find('button').prop("disabled", false);
   
           if (btn.attr('data-dir') == 'up') {
               action = setInterval(function(){
                   if ( input.attr('max') == undefined || parseInt(input.val()) < parseInt(input.attr('max')) ) {
                       input.val(parseInt(input.val())+1);
                   }else{
                       btn.prop("disabled", true);
                       clearInterval(action);
                   }
               }, 50);
           } else {
               action = setInterval(function(){
                   if ( input.attr('min') == undefined || parseInt(input.val()) > parseInt(input.attr('min')) ) {
                       input.val(parseInt(input.val())-1);
                   }else{
                       btn.prop("disabled", true);
                       clearInterval(action);
                   }
               }, 50);
           }
       }).mouseup(function(){
           clearInterval(action);
       });
   });
   
   $('#desired_delivery_date').datetimepicker({            
   format:'m/d/Y h:i A',
   timepicker:true,
   datepicker:true,
   minDate : new Date(),
   
   });
    
   new Validate({
   FormName :  'new_address',
   ErrorLevel : 1,
   callback: function() 
   {
   
    var geocoder = new google.maps.Geocoder();
   
    var address = $('#address_line_1').val()+', '+$('#address_line_2').val()+', '+
        parse_json($('#pp_pickup_city').val()).name+', '+
        parse_json($('#pp_pickup_state').val()).name+', '+
        parse_json($('#pp_pickup_country').val()).name;
   
      geocoder.geocode( { 'address': address}, function(getLatlong, status) 
      {
   
        if (status == google.maps.GeocoderStatus.OK)
        {
          
   
           $("#address_loader").addClass("spinning");
           $.ajax({
                url: 'add-address',
                data: {  
                  "address_line_1": $('#address_line_1').val() , 
                  "address_line_2": $('#address_line_2').val(),
                  "country": $('#pp_pickup_country').val(),
                  "state": $('#pp_pickup_state').val(),
                  "city":$('#pp_pickup_city').val(),
                  "zipcode": $('#zipcode').val(),
                  "lat" : getLatlong[0].geometry.location.lat(),
                  "lng" : getLatlong[0].geometry.location.lng()
                },
                type : 'post',
                dataType: 'json',
                success : function(obj) {
                     $("#address_loader").removeClass("spinning");
                    
                    if(obj.success == 1) {
                        document.getElementById("address_loader").value = "Submit";
                        document.getElementById("Addaddress").reset();
                        $('#dropoff_address').html(obj.address_html);
                        
                         $("#exampleModal").modal("hide");
                    }
               }
            });
        } else {
          alert('Oops! We are unable to find your location. Please correct it');
        }
    });
   
   }
   
   });
   
   
   function parse_json($string) {
   return eval('('+$string+')');
   }
    
   new Validate({
   FormName :  'buy_for_me_form_add',
   ErrorLevel : 1,
   callback: function() {

    try
    { 

       $("#item_loader").addClass("spinning");
       $.ajax({
            url: 'add-Item',
            type : 'post',
            data: new FormData(document.getElementById('buy_for_me_form_add')),
            processData: false,
            contentType: false,
            dataType: "json",
            success : function(obj) {
                 $("#item_loader").removeClass("spinning");
                if(obj.success == 1) {
                    document.getElementById("item_loader").value = "Submit";
                    document.getElementById("buy_for_me_form_add").reset();
                     $('#item_list').html(obj.html);
                      alert(obj.msg);
                     $('#table_hide').show();
                     $('#add_item').hide();      
                }
                
           }
        });
     }catch(e) {
        console.log(e);
     }
   }
   });   
   
   function remove_record(url,rowid)
   {    
    if(confirm('Are you sure? You want to delete this record.') == true)
    {
        $('#row-'+rowid).addClass('relative-pos spinning');
        
        url = SITEURL+url;
        $.ajax
            ({
                url: url,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(res)
                {                    
                    var obj = JSON.parse(res);
                    $('#row-'+rowid).removeClass('relative-pos spinning');
                    if(obj.success == 1){
                        
                        $('#row-'+rowid).css({'background-color':'red'});
                        $('#row-'+rowid).fadeOut('slow');
                        $('#error_msg_section').html('<div class="alert alert-success">'+ obj.msg + '</div>');
                    }else{
                        $('#row-'+rowid).css({'background-color':'white'});
                        $('#error_msg_section').html('<div class="alert alert-danger">'+ obj.msg + '</div>');
                        alert(obj.msg);
            
                    }
                }
            });
    }
    return false;
   }
   function get_edit_item(obj)
   { 
   obj = eval('('+obj+')');
   console.log(obj);
    $('#item_name').val(obj.item_name);
    $('#add_item_url').val(obj.add_item_url);
     if(obj.measurement_unit == 'cm_kg') {
      document.getElementById('measurement_unit').checked='checked';
      }else{
      document.getElementById('measurement_unit2').checked='checked';
   
    }

    $('#length').val(obj.length);
    $('#width').val(obj.width);
    $('#height').val(obj.height);
    $('#weight').val(obj.weight);
  //  $('#buy_for_me_preview').attr('src',SITEURL+'upload/'+obj.image);
    $('#item_price').val(obj.item_price);
    $('#quentity').val(obj.quentity);
    
    if(obj.image != '') {
         $('#buy_for_me_preview').attr('src',SITEURL+'upload/'+obj.image);
        } else {
         $('#buy_for_me_preview').attr('src',SITEURL+'upload/no-image.jpg');   
         }
   
    if(obj.travel_mode == 'air') {
      document.getElementById('travel_mode_air').checked='checked';
      toggle_category('.travel-mode-air','.travel-mode-ship','#package_category');
    } else {
      document.getElementById('travel_mode_ship').checked='checked';
      toggle_category('.travel-mode-ship','.travel-mode-air','#package_category');
    }
    $('#package_category').val('{"id":"'+obj.category_id+'","name":"'+obj.category+'"}'); 
    $('#description').val(obj.description);
     if(obj.insurance == 'yes') {
      document.getElementById('insurance').checked='checked';
      }else{
      document.getElementById('insurance2').checked='checked';
   
    }
    $('#item_id').val(obj._id);
   }
    
   
   var nfm_validate =  new Validate({
    FormName :  'edit_item',
    ErrorLevel : 1,
    callback: function() {
   
       $("#item_loader").addClass("spinning");
       $.ajax({
           url: 'edit-item/{$value->_id}',
           data: $('#edit_item').serialize(),
           type : 'post',
            data: new FormData(document.getElementById('edit_item')),
            processData: false,
            contentType: false,
           dataType: 'json',
           success : function(obj) {
                 $("#item_loader").removeClass("spinning");
                
                if(obj.success == 1) {
                    document.getElementById("item_loader").value = "Submit";
                    document.getElementById("edit_item").reset();
                    $('#item_list').html(obj.edit_html);
                    alert(obj.msg);     
                     $("#item_modal").modal("hide");
                }
           }
        });
    }
   });
   
   function open_market(markettype)
   {
   if(markettype == 'amazon') {
   window.open("http://www.amazon.com", "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=400,height=400");
   } 
   if(markettype == 'ebay') {
   window.open("https://www.ebay.com", "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=400,height=400");
   }
   if(markettype == 'walmart') {
   window.open("https://www.walmart.com/", "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=400,height=400");
   }
   if(markettype == 'flipkart') {
   window.open("https://www.flipkart.com/?affid=arjunsaff&affExtParam1=d369f9530716f52c6c09163450aa99c2", "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=400,height=400");
   }
   if(markettype == 'snapdeal') {
   window.open("https://www.snapdeal.com/", "_blank",  "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=400,height=400");
   }
   
   }

function image_preview(obj,previewid,evt)
{   
    var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
    if ($.inArray($(obj).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
       $(obj).val('');
       $('#'+previewid).attr('src', 'user-no-image.jpg');
        alert("Only "+fileExtension.join(', ')+" formats are allowed.");
     } else {
  
       var file = evt.target.files[0];

       if (file) 
       {
         var reader = new FileReader();
     
         reader.onload = function (e) {
             $('#'+previewid).attr('src', e.target.result)
         };
         reader.readAsDataURL(file);
       }
     }
} 
</script>
@endsection

