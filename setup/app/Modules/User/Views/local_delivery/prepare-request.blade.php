@extends('Page::layout.one-column-page')
@section('page_title')
Create Shipping Request Local Delivery - Aquantuo
@endsection
@section('content')


<div class="container">
   <style type="text/css">
       .Insurance_check{float: left; margin-top: 3px; margin-left: 5px;}
  </style>
   <div class="row">
   	  <div class="col-sm-12" id="send_a_package_start_position">
      <h2 class="color-blue mainHeading">Local Delivery</h2>
      <br>
      </div>
      <div class="col-sm-12">
      	<div class="box-shadow">
        <h3 class="midHeading">Please complete the following fields
        <br>
        	<?php
$ua = strtolower($_SERVER['HTTP_USER_AGENT']);
// you can add different browsers with the same way ..
if (preg_match('/(chromium)[ \/]([\w.]+)/', $ua)) {
	$browser = 'chromium';
} elseif (preg_match('/(chrome)[ \/]([\w.]+)/', $ua)) {
	$browser = 'chrome';
} elseif (preg_match('/(safari)[ \/]([\w.]+)/', $ua)) {
	$browser = 'safari';
} elseif (preg_match('/(opera)[ \/]([\w.]+)/', $ua)) {
	$browser = 'opera';
} elseif (preg_match('/(msie)[ \/]([\w.]+)/', $ua)) {
	$browser = 'msie';
} elseif (preg_match('/(mozilla)[ \/]([\w.]+)/', $ua)) {
	$browser = 'mozilla';
}

preg_match('/(' . $browser . ')[ \/]([\w]+)/', $ua, $version);
?>
        </h3>
        <hr>
        <div class="row">
        	@if(isset($user_data))
          		<div class="col-sm-10 col-sm-offset-1 box-shadow" >

	          			<div class="col-sm-2">

				          	<?php $file_exists = false;?>
							@if(!empty($user_data->Image))

								@if(file_exists(BASEURL_FILE.$user_data->Image))
									<?php $file_exists = true?>
			               		<br>
			               		<img id="senior-preview" src="{{ ImageUrl.$user_data->Image}}"  width="100px" height="100px" />
			               		<br>&nbsp;&nbsp;
								@endif
			               	@endif
			               	@if(!$file_exists)
								 <br>
						            <img id="" src="{{ ImageUrl}}/no-image.jpg"  width="100px" height="100px" />
						         <br>&nbsp;&nbsp;
			               	@endif
		                </div>

		                <div class="col-sm-10">
								<br>


			                <p><b>{{$user_data->FirstName}}</b></p>

			                 <p><b>{{$user_data->City}}&nbsp; {{$user_data->State}}&nbsp; {{$user_data->Country}}</b></p>

			                 <?php $average = 0;?>
	                   		 @if($user_data->RatingCount > 0 && $user_data->RatingByCount > 0)
		                    	<?php
$value = $user_data->RatingCount / $user_data->RatingByCount;
$average = $value * 20;
?>

		                    @endif

		                    <div class="star-ratings-sprite pull-left">
				 				 <span style="width:{{$average}}%" class="star-ratings-sprite-rating"></span>
							</div>
							<br>

		                </div>

          		</div>



          	@endif
        <!-- Fillable -->


	        <div class="col-sm-12">
	          	<div class="step_box four_step clearfix">

	          		<div class="step first selected" id="step1-header">
	                   <div>1</div>
	                   <p class="text-center colol-black">Package Details</p>
	                </div>

	                <div class="step inner" id="step2-header">
	                   <div>2</div>
	                   <p class="text-center colol-black">Pickup Address</p>
	                </div>

	                <div class="step inner" id="step3-header">
	                   <div>3</div>
	                   <p class="text-center colol-black">Drop Off Address</p>
	                </div>

	                <div class="step last " id="step4-header">
	                   <div>4</div>
	                   <p class="text-center colol-black">Payment</p>
	                </div>

	          	</div>
	        </div>
<!-- new form -->
	        {!! Form::open(['class'=>'form-vertical','name'=>'local_prepare_request_form','id' => 'local_prepare_request_form','files'=>'true']) !!}

		        <input type="hidden" name="PickupLat" id="PickupLat">
	            <input type="hidden" name="PickupLong" id="PickupLong">
	            <input type="hidden" name="DeliveryLat" id="DeliveryLat">
	            <input type="hidden" name="DeliveryLong" id="DeliveryLong">

	            <input type="hidden" name="browser" id="browser" value="{{$browser}}">
	            <input type="hidden" name="version" id="version" value="{{$version[2]}}">
	            <input type="hidden" name="device_type" id="device_type" value="website">
	            <input type="hidden" name="calculated_distance" id="calculated_distance" value="">



	            @if(count($transporter_data) > 0)
	            <input type="hidden" name="tripid" id="tripid" value="{{$transporter_data->_id}}">
	            <input type="hidden" name="transporter_id" id="transporter_id" value="{{$transporter_data->TransporterId}}">
	            <input type="hidden" name="TransporterName" id="TransporterName" value="{{$transporter_data->TransporterName}}">
	            @endif


	            <!-- first sec -->
	            <div class="col-sm-12" id="sec1">
	            		<div class="col-sm-12 col-xs-12 text-right">
		                     <a class="btn btn-default"  id="add_item_button" href="{{url('local-prepare-add-item')}}">Add Item</a>

		                  </br>
		                  </br>
		               	</div>
			            <div class="col-md-12">
			            	<table class="custom-table table table-bordered">
			                    <thead>
			                        <tr>
			                           <th>S.No.</th>
			                           <th>Image</th>
			                           <th>Name</th>
			                           <th>Dimension</th>
			                           <th>Category</th>
			                           <th>Insurance Status</th>
			                           <th>Quantity</th>
			                           <th>Package Value</th>
			                      {{--      <th>Shipping Mode</th> --}}
			                           <th>Need Package Material</th>
			                           <th>Action</th>
			                        </tr>
			                    </thead>
			                    <?php $no = 0;?>
			                    @if(count($items) > 0)
			                    	@foreach($items as $key)
			                    	<?php $no = $no + 1;?>
			                    <tbody>
			                    <tr id="row-{{$key->_id}}">

			                    	<td><?php echo $no; ?></td>
			            						<td>
			            							@if($key->ProductImage != '')
			            								<img src="{{ ImageUrl.$key->ProductImage}}" width="100px" height="80px" >
			            							@else
			            								<img src="{{ ImageUrl}}/no-image.jpg" width="130px" height="100px" >
			            							@endif
			            						</td>
			            						<td> {{ ucfirst($key->product_name) }}</td>
			            						<td>

			            						L- {{ $key->productLength }} {{$key->ProductLengthUnit}}<br>
			            						H-{{$key->productHeight}} {{$key->ProductLengthUnit}}<br>
			            						W-{{$key->productWidth}} {{$key->ProductLengthUnit}}<br>
			            						Weight-{{$key->productWeight}} {{$key->ProductWeightUnit}}<br>

			            						</td>
			            						<td>{{ucfirst($key->productCategory)}}</td>
			            						<td>{{ucfirst($key->InsuranceStatus)}}</td>
			            						<td>{{ucfirst($key->BoxQuantity)}}</td>
			            						<td>{{ucfirst($key->productCost)}}</td>
			            						{{-- <td>{{ucfirst($key->travelMode)}}</td> --}}
			            						<td>{{ucfirst($key->PackageMaterial)}}</td>
			            						<td>

			            			<a title="Edit"  href="{{url('local-prepare-request-edit-item',[$key->_id])}}" > <i class="fa fa-pencil"></i>
			            			</a>&nbsp;

			                        <a title="delete" id="Delete"  onclick="remove_record('delete_item/{{$key->_id}}/local_delivery_item', '{{$key->_id}}')" href="javascript:void(0)"><i class='fa fa-trash'></i></a>

			            				</td>
			            			</tr>
			                    </tbody>
			                    @endforeach
							 @endif
			              </table>

			            </div>

			            <div class="col-md-12">
				            <div class="form-group ">
		                        <button class="custom-btn1 btn text-center" id="step1-next-btn">
		                           Next
		                           <div class="custom-btn-h"></div>
		                        </button>

		                    </div>
		               	</div>

			    </div>
	            <!-- end first section -->



             	<!-- pick up address -->
             	<div class="col-md-10 col-sm-offset-1" id="sec2" style="display: none;">
	            	<div class="row">
	                  	<div class="col-md-12 col-xs-12">
	                    	<h3 class="color-blue bottom-border">Pickup Address</h3>
	                  	</div>

	                  	<div class="col-sm-12 col-xs-12">
	                      <div class="checkbox">
	                          <!-- <label>
	                          <span class="pull-left"><input type="checkbox" name="public_place"></span>
	                                <span class="Insurance_check">This is a public place.</span>
	                          </label> -->
	                      </div>
	                  	</div>
		                <div class="col-sm-4 col-xs-12">
		                    <div class="form-group">
		                        <label>Address Line 1</label>
		                        {!! Form::text('address_line_1','',['class'=>"form-control required",'placeholder'=>"Address",'maxlength' => 100,'id' => 'address_line_1'])  !!}
		                    </div>
		                </div>
		                <div class="col-sm-4 col-xs-12">
		                    <div class="form-group">
		                        <label>Address Line 2</label>
		                        {!! Form::text('address_line_2','',['class'=>"form-control",'placeholder'=>"Address",'maxlength' => 80,'id'=>'address_line_2'])  !!}
		                    </div>
		                </div>
	              	</div>

	                <div class="row">

		             {{--      <div class="col-sm-4 col-xs-12">
		                     <div class="form-group">
		                        <label class="control-label">Country</label>
		                         <input type="hidden" name="country" class="form-control required usename-#country#" id="pp_pickup_country" value='{"id":"{{$country[1]->_id}}","name":"{{$country[1]->Content}}","state_available":"{{$country[1]->state_available}}"}'>
		                     </div>
		                  </div> --}}

		                  <input type="hidden" name="country" class="form-control required usename-#country#" id="pp_pickup_country" value='{"id":"56bdbfb4cf32079714ee89ca","name":"{{$country[1]->Content}}","state_available":"{{$country[1]->state_available}}"}'>

		                  <div class="col-sm-4 col-xs-12">
		                     <div class="form-group">
		                        <label class="control-label">State/Region</label>
		                        <select name="state" class="form-control required left-disabled"
		                        id="pp_pickup_state" onchange="get_city('pp_pickup_state','pp_pickup_city',
		                        'pp_pickup_city','')">
		                            <option value="">Select State/Region</option>
		                              @foreach($state as $pickupState)
		                                  <option value='{"id":"{{$pickupState->_id}}","name":"{{$pickupState->Content}}"}'>{{$pickupState->Content}}</option>
		                            @endforeach
		                        </select>
		                     </div>
		                  </div>

		                  <div class="col-sm-4 col-xs-12">
		                     <div class="form-group">
		                        <label class="control-label">City</label>
		                        <select  name="city" class="form-control required" id="pp_pickup_city">
		                            <option value="">Select City</option>
		                        </select>
		                     </div>
		                  </div>
		            </div>


                  	<div class="row">
	                  	<div class="col-sm-4 col-xs-12">
		                     <div class="form-group">
		                        <label>Digital Address(If known)</label>
		                        {!! Form::text('zipcode','',['class'=>"form-control  alpha-numeric  maxlength-14 minlength-14 usename-#Digital Address#",'placeholder'=>"Digital Address(If known)",'maxlength' => 14])  !!}
		                     </div>
	                  	</div>

	                  	<div class="col-sm-4 col-xs-12">
	                     	<div class="form-group">
	                        	<label>Pickup Date and Time</label>

      							<input type="text" class="form-control required usename-#date#" name="pickup_date"  id="pickup_date" placeholder="Pickup Date and Time" readonly="readonly" value="{{ Input::old('pickup_date') }}" >

	                     	</div>
	                  	</div>

	                  	<div class="col-sm-12 col-xs-12">
		                    <div class="form-group">
		                      	<label class="control-label">Sender's Phone number</label>
		                      	<div class="row">
		                         	<!-- <div class="col-xs-2 col-xs-12">
		                            	<input type="text" class="form-control required" placeholder="Country Code" name="country_code" maxlength="4">
		                         	</div> -->
		                         	<div class="col-xs-4 col-xs-12">
		                            	<input type="text" class="form-control required numeric between-8-12" name="SenderMobileNo" placeholder="Phone Number" pattern=".{8,12}" title="8 to 12 numbers" size="5">
		                         	</div>
		                      	</div>
		                   	</div>
		                </div>
	              	</div>






	                <div class="">
	                     <hr>
	                </div>

	                <div class="form-group ">
                        
                        <a class="custom-btn1 btn text-center" onclick="switch_request_header('#sec1','#sec2',1)" href="javascript:void(0)">
                           Back
                           <div class="custom-btn-h"></div>
                        </a>
                        <button class="custom-btn1 btn text-center" id="step2-next-btn">
                           Next
                           <div class="custom-btn-h"></div>
                        </button>
                    </div>

              	</div>
             	<!-- end pickup -->

             	<!-- drop of 3 -->
             	<div class="row" id="sec3" style="display:none;">
                 <div class="col-md-10 col-sm-offset-1">
                 	<div class="">
	                    <div class="col-md-12 col-xs-12">
	                       <h3 class="color-blue">Drop Off Address</h3>
	                    </div>
	                    <div class="col-sm-4 col-xs-12">
	                       <div class="form-group">
	                          <label class="control-label">Address Line 1</label>
	                          <input name="drop_off_address_line_1" class="form-control required usename-#address line1#" placeholder="Address" name="drop_address" maxlength="100" id="drop_off_address_line_1">
	                       </div>
	                    </div>
	                    <div class="col-sm-4 col-xs-12">
	                       <div class="form-group">
	                          <label class="control-label">Address Line 2</label>
	                          <input name="drop_off_address_line_2" class="form-control usename-#address line2#" placeholder="Address" name="drop_address2"  maxlength="100" id="drop_off_address_line_2">
	                       </div>
	                    </div>
	                </div>
                    <div class="clearfix"></div>


                    <div class="">
                     <input type="hidden" name="drop_off_country" class="form-control required usename-#country#" id="pp_dropoff_country" value='{"id":"56bdbfb4cf32079714ee89ca","name":"{{$country[1]->Content}}","state_available":"{{$country[1]->state_available}}"}'>


	                 {{--  <div class="col-sm-4 col-xs-12">
	                     <div class="form-group">
	                        <label class="control-label">Country</label>
	                            <input type="hidden" name="drop_off_country" class="form-control required usename-#country#" id="pp_dropoff_country" value='{"id":"{{$country[1]->_id}}","name":"{{$country[1]->Content}}","state_available":"{{$country[1]->state_available}}"}'> --}}

	                    {{--     <select name="drop_off_country" class="form-control required usename-#country#" id="pp_dropoff_country" onchange="get_state('pp_dropoff_country','pp_dropoff_state','pp_dropoff_city','pp_dropoff_state','')">
	                          <option value='{"id":"{{$country[1]->_id}}","name":"{{$country[1]->Content}}","state_available":"{{$country[1]->state_available}}"}'>{{$country[1]->Content}}</option>
	                            @foreach($country as $key)
	                             <option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}","state_available":"{{$key->state_available}}"}'>{{$key->Content}}</option>
	                            @endforeach
	                        </select> --}}
	                  {{--    </div>
	                  </div> --}}


	                  <div class="col-sm-4 col-xs-12">
	                     <div class="form-group">
	                        <label class="control-label">State/Region</label>
	                        <select name="drop_off_state" class="form-control required usename-#state# left-disabled" id="pp_dropoff_state" onchange="get_city('pp_dropoff_state','pp_dropoff_city','pp_dropoff_city','')">
	                           <option value="">Select State/Region</option>
		                              @foreach($state as $dropState)
		                                  <option value='{"id":"{{$dropState->_id}}","name":"{{$dropState->Content}}"}'>{{$dropState->Content}}</option>
		                            @endforeach
	                        </select>
	                     </div>
	                  </div>

	                  <div class="col-sm-4 col-xs-12">
	                     <div class="form-group">
	                        <label class="control-label">City</label>
	                        <select  name="drop_off_city" class="form-control required usename-#city#" id="pp_dropoff_city">
	                             <option value="">Select City</option>
	                        </select>
	                     </div>
	                  </div>
	               </div>
	               <div class="clearfix"></div>

	               <div class="">
	                    <div class="col-sm-4 col-xs-12">
	                       <div class="form-group">
	                          <label class="control-label">Digital Address(If known)</label>
	                          <input class="form-control alpha-numeric  minlength-14 usename-#Digital Address#" placeholder="Digital Address(If known)" name="drop_off_zipcode" maxlength="14">
	                       </div>
	                    </div>
	                    <div class="col-sm-4 col-xs-12">
	                       <div class="form-group">
	                          <label class="control-label">Desired Drop Off Date</label>
	                          <input class="form-control required usename-#date#" placeholder="Desired Drop Off Date" name="drop_off_date" id="drop_off_date" readonly>
	                       </div>
	                    </div>
	                </div>
                    <div class="clearfix"> </div>


                    <div class="col-sm-12 col-xs-12" >
		               <div class="form-group">
		                  <div class="checkbox">
		                     <label>
		                        <input type="checkbox" name="ReceiverIsDifferent"  id="ReceiverIsDifferent" onclick=" return receiver_name()">
		                     </label>Receiver is different from Requester
		                  </div>
		               </div>
		            </div>

		            <div class="col-sm-4 col-xs-4" id="ReceiverName_in" style="display: none;">
		               <div class="form-group">
		                  <label class="control-label">Receiver Name</label><span class="red-star"> *</span>

		                    <input type="text" class="form-control required  usename-#name#" name="ReceiverName" placeholder="Receiver Name" maxlength="100" value="">

		               </div>
		            </div>
		            <div class="col-sm-12 col-xs-12"></div>



                    <div class="col-sm-12 col-xs-12">
	                    <div class="form-group">
	                      	<label class="control-label">Receiver's Phone number<span class="red-star"> *</span></label>
	                      	<div class="row">
	                         	<!-- <div class="col-xs-2 col-xs-12">
	                            	<input type="text" class="form-control required" placeholder="Country Code" name="country_code" maxlength="4">
	                         	</div> -->
	                         	<div class="col-xs-4 col-xs-12">
	                            	<input type="text" class="form-control required numeric between-8-12" name="phone_number" placeholder="Phone Number" pattern=".{8,12}" title="8 to 12 numbers" size="5">
	                         	</div>
	                      	</div>
	                   	</div>
	                </div>




	                <div class="">
	                    <div class="col-sm-4 col-xs-12">
	                       <div class="form-group">
	                          <label class="control-label">Is Delivery Date/Time Flexible?</label>
	                          	<div class="clearfix">

		                          <label class="radio-inline">
	                                 <span class="pull-left">
	                                 <input type="radio" name="is_delivery_date_flexible" value="yes" >
	                                 </span>
	                                    <span class="Insurance_check">Yes </span>
	                             </label>

	                             <label class="radio-inline">
	                                 <span class="pull-left">
	                                 <input type="radio" name="is_delivery_date_flexible" value="no" checked>
	                                 </span>
	                                    <span class="Insurance_check">No </span>
	                             </label>
	                           	</div>
	                       </div>
	                    </div>
	                    <div class="col-sm-4 col-xs-12">
	                       <div class="form-group">
	                          <label class="control-label">Journey Type</label>

	                          <div class="clearfix">

		                          <label class="radio-inline">
	                                 <span class="pull-left">
	                                 <input type="radio" value="one_way" name="journey_type" checked onclick="$('#return_jurney_section').hide()">
	                                 </span>
	                                    <span class="Insurance_check">One Way </span>
	                             </label>

	                             <label class="radio-inline">
	                                 <span class="pull-left">
	                                 <input type="radio" value="return" name="journey_type" onclick="$('#return_jurney_section').show()">
	                                 </span>
	                                    <span class="Insurance_check">Return </span>
	                             </label>
	                           	</div>
	                       </div>
	                    </div>
	                </div>
	                <div id="return_jurney_section"  style="display:none;">
	                	<div class="">
		                	<div class="col-sm-12 col-xs-12">
		                       	<div class="form-group">
		                       		<h3 class="color-blue">Return Address</h3>
		                          	<div class="checkbox">

		                             <span class="pull-left">
		                             <input type="checkbox" id="return_same_as_pickup" name="return_same_as_pickup">
		                             </span>
		                               <span class="Insurance_check"> Same as pickup address</span>

		                          	</div>
		                       	</div>
		                    </div>
		                </div>
	                   	<div id="return_jurney_address">
			               	<div class="">
			                    <div class="col-sm-4 col-xs-12">
			                       <div class="form-group">
			                          <label class="control-label">Address Line 1</label>
			                          <input name="return_address_line_1" class="form-control required usename-#address line 1#" placeholder="Address" name="return_address" maxlength="100">
			                       </div>
			                    </div>
			                    <div class="col-sm-4 col-xs-12">
			                       <div class="form-group">
			                          <label class="control-label">Address Line 2</label>
			                          <input name="return_address_line_2" class="form-control usename-#address line 2#" placeholder="Address" name="return_address2" maxlength="100">
			                       </div>
			                    </div>
			                </div>
		                    <div class="">
		                    <input type="hidden" name="return_country" class="form-control required usename-#country#" id="pp_return_country" value='{"id":"{{$country[1]->_id}}","name":"{{$country[1]->Content}}","state_available":"{{$country[1]->state_available}}"}'>
			           {{--        <div class="col-sm-4 col-xs-12">
			                     <div class="form-group">
			                        <label class="control-label">Country</label>
			                        <select name="return_country" class="form-control required usename-#country#" id="pp_return_country" onchange="get_state('pp_return_country','pp_return_state','pp_return_city','pp_return_state','')">
			                            <option value="">Select Country</option>
			                            @foreach($country as $key)
			                             <option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}","state_available":"{{$key->state_available}}"}'>{{$key->Content}}</option>
			                            @endforeach
			                        </select>
			                     </div>
			                  </div> --}}
			  {{--                 <div class="col-sm-4 col-xs-12">
			                     <div class="form-group">
			                        <label class="control-label">State/Region</label>
			                        <select name="return_state" class="form-control required usename-#state# left-disabled" id="pp_return_state" onchange="get_city('pp_return_state','pp_return_city','pp_return_city','')">
			                            <option value="">Select State/Region</option>
			                        </select>
			                     </div>
			                  </div> --}}

				            <div class="col-sm-4 col-xs-12">
		                     <div class="form-group">
		                        <label class="control-label">State/Region</label>
		                        <select name="return_state" class="form-control required usename-#state# left-disabled" id="pp_return_state" onchange="get_city('pp_return_state','pp_return_city','pp_return_city','')">
		                           <option value="">Select State/Region</option>
			                              @foreach($state as $returnState)
			                                  <option value='{"id":"{{$returnState->_id}}","name":"{{$returnState->Content}}"}'>{{$returnState->Content}}</option>
			                              @endforeach
		                        </select>
		                     </div>
		                  </div>

			                  <div class="col-sm-4 col-xs-12">
			                     <div class="form-group">
			                        <label class="control-label">City</label>
			                        <select  name="return_city" class="form-control required usename-#city#" id="pp_return_city">
			                            <option value="">Select City</option>
			                        </select>
			                     </div>
			                  </div>
			               </div>
			               <div class="">
			                    <div class="col-sm-4 col-xs-12">
			                       <div class="form-group">
			                          <label class="control-label">Digital Address(If known)</label>
			                          <input class="form-control alpha-numeric minlength-14 usename-#Digital Address#" placeholder="Digital Address(If known)" name="return_zipcode" maxlength="14">
			                       </div>
			                    </div>
			                </div>
		                </div>
	             	</div>
					<div class="clearfix"></div>
	                <div class="">
	                	<div class="col-sm-12 col-xs-12">
	                       <div class="form-group">
	                       		<h3 class="color-blue">Return address(If item is not delivered)</h3>
	                          <div class="">
	                             <span class="pull-left">
	                             <input type="checkbox" id="same_as_pickup" name="same_as_pickup" checked>
	                             </span>
	                             <span class="Insurance_check">
	                              Same as pickup address
	                              </span>

	                          </div>
	                       </div>
	                    </div>
	                </div>

	                <div id="return_address_action" style="display:none;">
		               	<div class="">
		                    <div class="col-sm-4 col-xs-12">
		                       <div class="form-group">
		                          <label class="control-label">Address Line 1</label>
		                          <input name="nd_return_address_line_1" class="form-control required usename-#address line 1#" placeholder="Address" maxlength="100">
		                       </div>
		                    </div>
		                    <div class="col-sm-4 col-xs-12">
		                       <div class="form-group">
		                          <label class="control-label">Address Line 2</label>
		                          <input name="nd_return_address_line_2" class="form-control usename-#address line 2#" placeholder="Address" maxlength="100">
		                       </div>
		                    </div>
		                </div>

	                    <div class="">
	                       {{--
		                  <div class="col-sm-4 col-xs-12">
		                 <div class="form-group">
		                        <label class="control-label">Country</label>
		                        <select name="nd_return_country" class="form-control required usename-#country#" id="pp_nd_return_country" onchange="get_state('pp_nd_return_country','pp_nd_return_state','pp_nd_return_city','pp_nd_return_state','')">
		                            <option value="">Select Country</option>
		                            @foreach($country as $key)
		                             <option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}","state_available":"{{$key->state_available}}"}'>{{$key->Content}}</option>
		                            @endforeach
		                        </select>
		                     </div>
		                  </div>  --}}

		                     <input type="hidden" name="nd_return_country" class="form-control required usename-#country#" id="pp_nd_return_country" value='{"id":"{{$country[1]->_id}}","name":"{{$country[1]->Content}}","state_available":"{{$country[1]->state_available}}"}'>

		                  <div class="col-sm-4 col-xs-12">
		                     <div class="form-group">
		                        <label class="control-label">State/Region</label>
		                        <select name="nd_return_state" class="form-control required usename-#state# left-disabled" id="pp_nd_return_state" onchange="get_city('pp_nd_return_state','pp_nd_return_city','pp_nd_return_city','')">
		                            @foreach($state as $nd_returnState)
			                                  <option value='{"id":"{{$nd_returnState->_id}}","name":"{{$nd_returnState->Content}}"}'>{{$nd_returnState->Content}}
			                                  </option>
			                              @endforeach
		                        </select>
		                     </div>
		                  </div>
		                  <div class="col-sm-4 col-xs-12">
		                     <div class="form-group">
		                        <label class="control-label">City</label>
		                        <select  name="nd_return_city" class="form-control required usename-#city#" id="pp_nd_return_city">
		                            <option value="">Select City</option>
		                        </select>
		                     </div>
		                  </div>
		               </div>
		               <div class="">
		                    <div class="col-sm-4 col-xs-12">
		                       <div class="form-group">
		                          <label class="control-label">Digital Address(If known)</label>
		                          <input class="form-control alpha-numeric minlength-14 usename-#Digital Address#" placeholder="Digital Address(If known)" name="nd_return_zipcode" maxlength="14">
		                       </div>
		                    </div>
		                </div>
	             	</div>
                    <div class="clearfix"></div>
                    <div class="">
                       <hr>
                    </div>
                    <div class="col-sm-8 col-xs-12">
                    		<div class="">
			                    <div class="form-group">
			                        <a class="custom-btn1 btn text-center" onclick="switch_request_header('#sec2','#sec3',2)" href="javascript:void(0)">
			                           Back
                                       <div class="custom-btn-h"></div>
			                        </a>
			                    </div>
			                <div class="col-xs-6 col-xs-12">
			                     

			                     <div class="form-group">
			                        <button class="custom-btn1 btn text-center" id="step3-next-btn">
			                           Next
			                           <div class="custom-btn-h"></div>
			                        </button>
			                     </div>
			                </div>
	                  </div>
                    </div>

                 </div>
              	</div>
             	<!-- end drop off 3 -->

             	<!-- section 4 payment  "-->
             	<div class="row" id="sec4" style="display:none;">
	                <div class="col-md-10 col-sm-offset-1">
	                   <div class="col-sm-12 col-xs-12">
	                   		<div class="row custom-row">
								<div class="col-sm-6"><b> Total Item: </b></div>
								<div class="col-sm-6" id="total_item"></div>
							</div>
	                   		<div class="row custom-row">
							   <div class="col-sm-6"><b> Distance: </b></div>
							   <div class="col-sm-6" id="distance"></div>
							</div>

							<div class="row custom-row">
							   <div class="col-sm-6"><b> Total Weight: </b></div>
							   <div class="col-sm-6" id="total_weight"></div>
							</div>

							<div class="row custom-row">
							   <div class="col-sm-6"><b> Total Volume: </b></div>
							   <div class="col-sm-6" id="total_volume"></div>
							</div>

							<div class="row custom-row">
							   <div class="col-sm-6"><b> Total Item Cost: </b></div>
							   <div class="col-sm-6" id="totalItemCost"></div>
							</div>

							<div class="row custom-row">
							   <div class="col-sm-6"><b> Shipping Cost: </b></div>
							   <div class="col-sm-6" id="shipping_cost"></div>
							</div>
							<div class="row custom-row insurance_cost_show">
							   <div class="col-sm-6"><b> Insurance Cost: </b></div>
							   <div class="col-sm-6" id="insurance_cost"></div>
							</div>

							<div class="row custom-row region_cost_show">
							   <div class="col-sm-6"><b> Region Cost: </b></div>
							   <div class="col-sm-6" id="region_cost"></div>
							</div>
							<div class="row"> <hr /> </div>


							<div class="row custom-row">
							   <div class="col-sm-6"><b> Total Amount: </b></br>
							   <!-- <small id="total_amount"></small> -->

							    </div>
							   <div class="col-sm-6"  id="user_currency"></div>
							</div>

							<input type="hidden" id="shipping_cost_input" value="" name="shipping_cost_input">
							<input type="hidden" id="total_amount_input" value="" name="total_amount_input">


							<div class="row">
								<hr>
								<div class="col-sm-12">
									<div class="form-group reward-group" id="olp_promo_input">
										<input type="text" placeholder="Promotion Code" class="form-control pull-left" id="promocode" name="promocode" >
										<a class="btn default-btn" onclick="check_promocode('promocode','shipping_cost_input','olp_promo','total_amount_input')" href="javascript:void(0)">
											<img src="{{asset('theme/web/promo/images/green_check.png')}}" />Apply
										</a>
										<div class="clearfix"></div>
										<div  id="er_promocode" class="color-red"></div>
									</div>
									<div class="form-group reward-group" id="olp_promo" style='display: none;'>
										<span class="" onclick="remove_promocode('olp_promo','promocode')"><i class="fa fa-trash"></i></span><span id="olp_promo_msg"></span>
									</div>
								</div>
							</div>

							<div class="row"> <hr /> </div>

<!--
							<div class="row custom-row">
								<div class="col-sm-6"><b class="color-blue"> Total Cost: </b></br>
								<small id="olp_promo_payable_ghana"></small>
								</div>
								<div class="col-sm-6 color-blue" id="olp_promo_payable"></div>
							</div>
-->




	                      <div class="">
	                    	<div class="">
		                  		<div class="">
				                     <div class="form-group">
				                        
				                        <a class="custom-btn1 btn text-center" onclick="switch_request_header('#sec3','#sec4',3)" href="javascript:void(0)">
				                           Back
	                                       <div class="custom-btn-h"></div>
				                        </a>
				                        <button class="custom-btn1 btn" id="last-stage" >
				                           Next
				                           <div class="custom-btn-h btn"></div>
				                        </button>
				                     </div>
				                </div>
		                  	</div>
	                       </div>
	                	</div>
	                </div>
              	</div>
             	<!--end section 4 payment -->


	        {!! Form::close() !!}

<!-- end form new -->

        </div>
     </div>
  </div>
</div>
@endsection

@section('script')
@parent

<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyCT0uj-bffWWdOdwDp15LvrMko-OuoZUoY"></script>

{!! Html::script('theme/web/js/utility.js') !!}
{!! Html::script('theme/web/js/validation.js') !!}
{!! Html::script('theme/web/js/local_delivery.js') !!}
{!! Html::style('theme/date-time-picker/jquery.datetimepicker.css') !!}
{!! Html::script('theme/date-time-picker/jquery.datetimepicker.js') !!}

@endsection

@section('inline-script')
@parent
<script>
	var total_item_count = parseInt('{{ $items_count }}');
	$("#step1-next-btn").click(function () {
		if (total_item_count == 0) {
			alert('Please add at least one item.');
			return false;
		}
	});

 function remove_record(url,rowid)
   {
    if(confirm('Are you sure you want to delete this item?') == true)
    {
        $('#row-'+rowid).addClass('relative-pos spinning');

        $('#row-'+rowid).css({'background-color':'red'});
        $('#row-'+rowid).fadeOut('slow');

        url = SITEURL+url;
        $.ajax
        ({
            url: url,
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function(res)
            {
                var obj = JSON.parse(res);
                $('#row-'+rowid).removeClass('relative-pos spinning');
                if(obj.success == 1){
                	--total_item_count;
                    $('#row-'+rowid).css({'background-color':'red'});
                    $('#row-'+rowid).fadeOut('slow');
                    $('#error_msg_section').html('<div class="alert alert-success">'+ obj.msg + '</div>');
                }else{
                    $('#row-'+rowid).css({'background-color':'white'});
                    $('#error_msg_section').html('<div class="alert alert-danger">'+ obj.msg + '</div>');
                    alert(obj.msg);
                }
            }
        });
    }
    return false;
   }

$(function() {
       var action;
       $(".number-spinner a").mousedown(function () {
           btn = $(this);
           input = btn.closest('.number-spinner').find('input');
           btn.closest('.number-spinner').find('a').prop("disabled", false);

           if (btn.attr('data-dir') == 'up') {
               action = setInterval(function(){
                   if ( input.attr('max') == undefined || parseInt(input.val()) < parseInt(input.attr('max')) ) {
                       input.val(parseInt(input.val())+1);
                   }else{
                       btn.prop("disabled", true);
                       clearInterval(action);
                   }
               }, 50);
           } else {
               action = setInterval(function(){
                   if ( input.attr('min') == undefined || parseInt(input.val()) > parseInt(input.attr('min')) ) {
                       input.val(parseInt(input.val())-1);
                   }else{
                       btn.prop("disabled", true);
                       clearInterval(action);
                   }
               }, 50);
           }
       }).mouseup(function(){
           clearInterval(action);
       });
   });



  var newdate = new Date();
  newdate.setHours(newdate.getHours()+1);
  var minDateForTwo = newdate;

  var newdate2 = new Date();
  newdate2.setHours(newdate2.getHours()+2);
  var minDateForTwo2 = newdate2;

var drop_off_date = {};
    $('#pickup_date').datetimepicker({
        format:'M d, Y h:i A',
        formatTime:'h:i A',
        timepicker:true,
        datepicker:true,
        step:30,
        minDate : new Date(),
        minTime : newdate,
        onChangeDateTime:function( ct ){
        	if(minDateForTwo > ct) {
       		//$("#drop_off_date").val(minDateForTwo.dateFormat('M d, Y h:i A'));
       		 return false;
        }
            minDateForTwo = ct;
            drop_off_date.minDate = ct;
            var dt = new Date();
            dt.setHours(23);dt.setMinutes(59);dt.setSeconds(59);

            if(ct < dt) {
              $('#pickup_date').datetimepicker({  minTime: newdate });
              drop_off_date.minTime = ct;

              if(ct < newdate) {
                $('#pickup_date').val(dt.dateFormat('M d, Y ')+newdate.dateFormat('h:i A'));
                drop_off_date.minTime = minDateForTwo;
              }
            } else {
              dt.setHours(0);
              $('#pickup_date').datetimepicker({  minTime: dt });
              drop_off_date.minTime = false;
            }
            $('#drop_off_date').datetimepicker(drop_off_date);
            $('#drop_off_date').val('');
         },
   });

   $("#drop_off_date").datetimepicker({
       format:'M d, Y h:i A',
       formatTime:'h:i A',
       timepicker:true,
       datepicker:true,
       step:30,
       minDate : new Date(),
       minTime : minDateForTwo2,
       closeOnDateSelect : false,
       onChangeDateTime:function( ct ){
          var date2option = {};
        if(($("#drop_off_date").val()).trim() != '') {

            $('#pickup_time').datetimepicker({  maxDate:ct, maxTime:ct  })

            if(ct < minDateForTwo2) {
              $('#drop_off_date').val(ct.dateFormat('M d, Y ')+minDateForTwo2.dateFormat('h:i A'));
              date2option.minTime = minDateForTwo2;
            } else {
              date2option.minTime = false;
            }
            $('#drop_off_date').datetimepicker(date2option);


          }
       },
   });

function unit_show()
{
	var unit =$('input[name=measurement_unit]:checked').val();

	if(unit == 'cm_kg'){
		$('#length_unit').html('Cm');
		$('#width_unit').html('Cm');
		$('#height_unit').html('Cm');
		$('#weight_unit').html('Kg');

	}else{
		$('#length_unit').html('Inches');
		$('#width_unit').html('Inches');
		$('#height_unit').html('Inches');
		$('#weight_unit').html('Lbs');
	}

}

unit_show();

function show_quantity()
{
	var type =$('input[name=quantity_type]:checked').val();
	if(type == 'multiple'){
		$('#quantity_input').show();
	}else{
		$('#quantity_input').hide();
	}

}
show_quantity();
function receiver_name(){

  if($("#ReceiverIsDifferent").prop('checked') == true){
    $('#ReceiverName_in').show();
  }else{
    $('#ReceiverName_in').hide();
  }

}
</script>




@endsection
