@extends('Page::layout.one-column-page')
@section('content')
{!! Html::script('theme/web/rating/jquery.rateyo.js') !!}
{!!Html::style('theme/web/rating/jquery.rateyo.min.css')!!}
<style>
   .modal-feedback-width{
   margin:30px auto;
   width:420px;
   position: relative;
   }
   .form-error{
   color:#AA5669;
   }
   .modal-width{
   width:420px;
   }
</style>

<div class="container">
<div class="row">
<div class="col-sm-12">
<h2 class="color-blue mainHeading">Local Delivery</h2>
   <br />
   <div id="success-message"></div>
</div>


<div class="col-sm-12">

</div>
<div class="col-md-12">
   <div class="box-shadow">
   <!-- <button onclick="chat_test();">Chat</button> -->
      <div class="content-section clearfix">
         <div class="">
            <span class="pull-right">
            <a href="{{url('transporter/my-deliveries')}}"  class="btn btn-default blue-btn" style="color:#FFFFFF;"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;&nbsp;Back</a>&nbsp;&nbsp;</span>
         </div>


         <?php $ProductList = $transporterDetail->ProductList[0];?>
         @foreach($transporterDetail->ProductList as $value)
         <?php
$item_id = $value['_id'];
$requester_id = $transporterDetail->RequesterId;

?>
         <br />
         <div class="col-md-2 col-sm-3">

            @if(@$ProductList['ProductImage'] == '')
            <img src="{{ImageUrl}}/no-image.jpg"  width="150px" height="150px" >
            @else
            <a class="fancybox" rel="group" href="{{ ImageUrl.$ProductList['ProductImage']}}" >
            <img src="{{ ImageUrl.$ProductList['ProductImage']}}"  width="150px" height="150px">
            </a>
            @endif
         </div>
         <div class="col-md-10 col-sm-9">
            <h4><b>{{ucfirst($value['product_name'])}}</b></h4>
            <p class="color-black" ><strong>Package Id: </strong>
               {{$ProductList['package_id']}}
            </p>
            <p class="color-black" id="status_div"><strong>Status: </strong>
               @if($value['status'] == 'out_for_pickup')
               &nbsp;&nbsp;Out for Pickup
               @elseif($value['status'] == 'out_for_delivery')
               &nbsp;&nbsp;Out for Delivery
               @elseif($value['status'] == 'assign')
               &nbsp;&nbsp;Transporter Assigned
               @elseif($value['status'] == 'purchased')
               Canceled
               @else
               &nbsp;{{ucfirst($value['status'])}}
               @endif
            </p>
            <p class="color-black" style="display:none;" id="ajax_status"><strong>Status:</strong></P>
            <div class="form-group">

            @if($value['status'] == "assign")
               <button  class="btn btn-default blue-btn"  title="Accept" id="accept_request"  data-target="#exampleModal1" data-toggle="modal">
               &nbsp; Accept &nbsp;&nbsp;</button>
            @endif

            @if (in_array($value['status'], ['accepted', 'assign', 'out_for_pickup']))
               <button class="btn btn-danger"  title="Cancel" id="cancel_request" onclick="return confirm('Are you sure you want to cancel this request?')? cancel_request('{{$value['_id']}}'):'';" >
                     &nbsp; Cancel &nbsp;&nbsp;</button>
            @endif

            @if ($value['status'] == 'accepted')
               <button  class="btn btn-default blue-btn"  title="Out for pickup" id= "out_for_picup" onclick="return confirm('Are you sure you are going to pick up this request?')? out_for_pickup('{{$value['_id']}}'):'';" >
               &nbsp; Out for Pickup &nbsp;&nbsp;</button>
            @endif
            @if ($value['status'] == "out_for_pickup")
               <button  class="btn btn-default blue-btn"  title="Out for delivery" id= "out_for_delivery"  onclick="return confirm('Are you sure this package is going to be delivered?')? out_for_deliver('{{$value['_id']}}'):'';">
               &nbsp; Out for Delivery &nbsp;&nbsp;</button>
            @endif

            @if ($value['status'] == "out_for_delivery")
               <button class="btn btn-default blue-btn" href="javascript:void(0)" data-toggle="modal" data-target="#b_deliver_model" title="delivery"   id="delivery"> &nbsp;Delivered&nbsp;</button>
               <button class="btn btn-default blue-btn" href="javascript:void(0)" data-toggle="modal" data-target="#exampleModal" title="Cancel Delivery"   id="delivery_btn"> &nbsp;No Delivery</button>
            @endif
             @if(isset($value['TransporterFeedbcak']))
            @if($value['TransporterFeedbcak'] == '' &&  $value['status'] == 'delivered')
               <a href="javascript:void(0)" data-toggle="modal" data-target="#modal2" title="Review"  class="btn btn-default blue-btn" id= "feedback">Give Feedback</a>
            @endif
            @endif
            <!-- <button class="btn btn-default blue-btn" id="chat-btn" onclick="open_chat_box('{{$transporterDetail->RequesterId}}','{{$transporterDetail->RequesterName}}')"><i class="fa fa-chat"></i> Chat</button> -->
            @endforeach
            </div>
         </div>
      </div>
   </div>
</div>
<div class="col-sm-12">
   <div class="">
      <br />
      <div class="box-shadow">
         <!------------------step1------------------>
         <div class="content-section row clearfix">
            <div class="col-sm-6">
               @foreach($transporterDetail->ProductList as $value)
               @if(isset($transporterDetail->EnterOn->sec))
               <p><b>Drop of Date :</b> {{date('l,  M  d, Y ',$transporterDetail->EnterOn->sec)}}</p>
               @endif
               <p><b>Return to Aquantuo :&nbsp;</b><?php echo ucfirst(Session()->get('AqAddress')); ?></p>
            </div>
            <div class="col-sm-6">
            </div>
         </div>
         <br />
         <div class="row">
            <?php $sno = 0;?>
            <div class="">
               <div class="col-sm-6">

                  <p><b>Description :</b>{{ucfirst(@$value['Description'])}}</p>
                  @if(@$value['TransporterRating'] != '')
                  <p><b>Your Feedback :</b> {{ucfirst(@$value['TransporterFeedbcak'])}} </p>
                  <p><b><span class="pull-left">Rating : &nbsp; </span> </b>
                  <div class="star-ratings-sprite pull-left">
                     <span style="width:<?php echo $value['TransporterRating'] * 20; ?>%" class="star-ratings-sprite-rating"></span>
                  </div>
                  </br>
                  </p>
                  @endif
                  @if(@$value['RequesterFeedbcak'] != '')
                  <p><b>Requester Feedback :</b> {{ucfirst($value['RequesterFeedbcak'])}} </p>
                  <p><b><span class="pull-left">Rating : &nbsp; </span> </b>
                  <div class="star-ratings-sprite pull-left">
                     <span style="width:<?php echo @$value['RequesterRating'] * 20; ?>%" class="star-ratings-sprite-rating"></span>
                  </div>
                  </br>
                  </p>
                  @endif
               </div>
               <div class="col-sm-6">
                  <div class="media">


                     <div class="form-group media-left">
                        @if($user_data->Image != '')
                        <a class="fancybox" rel="group" href="{{ ImageUrl.@$user_data->Image}}" >
                        <img src="{{ ImageUrl.@$user_data->Image}}"  width="100px" height="100px" class="img-circle">
                        </a>
                        @else
                        <img src="{{ImageUrl}}/user-no-image.jpg" width="100px" height="150px" class="img-circle">
                        @endif
                     </div>
                     <div class="media-body">
                        <br>
                        <p><b>Requester Name: </b>{{ucfirst($user_data->Name)}}</p>
                        @if(count($user_data) > 0)
                        <?php $average_rating = 0;
if ($user_data->RatingCount > 0 && $user_data->RatingByCount > 0) {
	$rating = $user_data->RatingCount / $user_data->RatingByCount;
	$average_rating = 20 * $rating;
}
?>
                        <div class="col-sm-12">
                           <div class="star-ratings-sprite pull-left">
                              <span style="width:<?php echo $average_rating; ?>%" class="star-ratings-sprite-rating"></span>
                           </div>
                        </div>
                        @endif
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-sm-12">
               <table class="custom-table table table-bordered">
                  <thead>
                     <tr>
                        <th>Product Name</th>
                        <th>Item Category</th>
                        <th>Weight</th>
                        <th>Quantity</th>
                        <th>Item Price</th>
                     </tr>
                  </thead>
                  <tbody>
                     <tr>
                        <td>{{ucfirst(@$value['product_name'])}}</td>
                        <td>{{@$value['productCategory']}}</td>
                        <td>{{@$value['productWeight']}} {{ucfirst(@$value['ProductWeightUnit'])}}</td>
                        <td>{{@$value['productQty']}}</td>
                        <td>${{number_format((float)@$value['productCost'],2)}}</td>
                     </tr>
                  </tbody>
               </table>
            </div>
            <?php $sno++;?>
            <!-- verify_model -->
            <div class="modal fade" id="b_deliver_model" role="dialog">
               <div class="modal-dialog modal-width">
                  <div class="modal-content">
                     <div class="modal-header">
                        <button  class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"><b>Delivery Verification</b></h4>
                     </div>
                     {!! Form::model('', ['name' => 'b_verify', 'id' => 'b_verify']) !!}
                     <input type="hidden" name="request_id" id="request_id" value="<?php echo $value['_id']; ?>">
                     <div class="modal-body" >
                        <div class="map-hloder">
                           {!!Form::label('b_code','Verification Code(obtain this
                           from Requester)',['class'=>'control-label'])!!}
                           {!!Form::text('b_code','',['class'=>'form-control required numeric minlength-4 usename-#verification code#','id'=>'b_code','placeholder'=>'Verification Code',
                           'maxlength' => 4])!!}
                        </div>
                        <br />
                        <div class="row">
                           <div class="col-sm-2 col-xs-3">
                              {!! Form::button('Submit', ['type' => 'submit', 'class' => 'btn btn-default','value'=>"Submit",'id'=>'b_verify_button']) !!}
                           </div>
                           <div class="col-sm-10 col-xs-11">
                              &nbsp;&nbsp;&nbsp;{!! Form::button('Close', ['class' => 'btn btn-default', 'data-dismiss'=>'modal','value'=>"Not Now"]) !!}
                           </div>
                           <div class="clearfix"></div>
                        </div>
                     </div>
                     <div class="">&nbsp;&nbsp;&nbsp;&nbsp;</div>
                     {!! Form::close() !!}
                  </div>
               </div>
            </div>
            <!--end-->
            @endforeach
         </div>
         <div class="list-footer clearfix">
            <div class="row">
               <div class="col-sm-4">
                  <p><b>Phone Number - </b>
                     @if(!$transporterDetail->country_code == '')
                     +{{$transporterDetail->country_code}} -
                     @endif
                     &nbsp;{{$transporterDetail->ReceiverMobileNo}}
                  </p>
                  <p><b>Delivery Address - </b>{{ucfirst($transporterDetail->DeliveryFullAddress)}}
                  </p>
                  <p class=""><b>Desire Delivery Date :</b>
                     @if(isset($transporterDetail->DeliveryDate->sec))
                     {{show_date($transporterDetail->DeliveryDate)}}
                     @endif
                  </p>
                  <p class=""><b>Distance :</b>
                     {{number_format($transporterDetail->distance,2)}}&nbsp;Miles
                  </p>
               </div>
               @if($value['status'] == 'cancel')
               <div class="payment-sec col-sm-4 pull-right">
                  <h3 class="">Cancel Information</h3>
                  <table class="table table-bordered">
                     <tbody bgcolor="#FFFFFF">
                        <tr>
                           <td class="">
                              <div class="col-xs-12 text-left">
                                 <label class="" for="exampleInputEmail1">
                                 <b>Return Address:</b>
                                 </label> <br />
                                 {{ucfirst(@$transporterDetail['ReturnAddress'])}}
                              </div>
                           </td>
                        </tr>
                        <tr>
                           <td class="">
                              <div class="col-xs-6 text-left">
                                 <label class="" for="exampleInputEmail1"><b>Reject By:</b> </label>
                              </div>
                              <div class="col-xs-6 text-right">
                                 {{ucfirst($ProductList['RejectBy'])}}
                              </div>
                           </td>
                        </tr>
                        <tr>
                           <td class="">
                              <div class="col-xs-6 text-left">
                                 <label class="" for="exampleInputEmail1"><b>Return Type:</b> </label>
                              </div>
                              <div class="col-xs-6 text-right">
                                 {{get_cancel_status($ProductList['ReturnType'])}}
                              </div>
                           </td>
                        </tr>
                        <tr>
                           <td class="">
                              <div class="col-xs-12 text-left">
                                 <label class="" for="exampleInputEmail1"><b>Message:</b> </label>
                                 <br />
                                 {{ucfirst($ProductList['TransporterMessage'])}}
                              </div>
                           </td>
                        </tr>
                        @if(!empty(trim($ProductList['TrackingNumber'])))
                        <tr>
                           <td class="">
                              <div class="col-xs-6 text-left">
                                 <label class="" for="exampleInputEmail1"><b>Tracking Number:</b> </label>
                              </div>
                              <div class="col-xs-6 text-right">
                                 {{$ProductList['TrackingNumber']}}
                              </div>
                           </td>
                        </tr>
                        @endif
                        @if(isset($ProductList['CancelReturnDate']->sec))
                        <tr>
                           <td class="">
                              <div class="col-xs-6 text-left">
                                 <label class="" for="exampleInputEmail1"><b>Date:</b> </label>
                              </div>
                              <div class="col-xs-6 text-right">
                                 {{date('d M ,Y  h:i A',$ProductList['CancelReturnDate']->sec)}}
                              </div>
                           </td>
                        </tr>
                        @endif
                        @if(!empty(trim($ProductList['ReceiptImage'])))
                        @if(file_exists(BASEURL_FILE.$ProductList['ReceiptImage']))
                        <tr>
                           <td class="">
                              <div class="col-xs-6 text-left">
                                 <label class="" for="exampleInputEmail1"><b>Receipt: </b> </label>
                              </div>
                              <div class="col-xs-6 text-right">
                                 <a class="fancybox" rel="group" href="{{ImageUrl.$ProductList['ReceiptImage']}}" >
                                 <img src="{{ImageUrl.$ProductList['ReceiptImage']}}" alt="receipt" height="100" width="100" >
                                 </a>
                              </div>
                           </td>
                        </tr>
                        @endif
                        @endif
                     </tbody>
                  </table>
               </div>
               @endif
               @if($transporterDetail['shippingCost'] > 0)
               <div class="payment-sec col-sm-4 pull-right">
                  <h3 class="">Earning Detail</h3>
                  <table class="table table-bordered">
                     <tbody bgcolor="#FFFFFF">
                        <tr>
                        <td class="">
                          <div class="col-xs-8 text-left">
                            <label class="" for="exampleInputEmail1"><b>Your Earning:</b> </label>
                          </div>
                          <div class="col-xs-4 text-right">
                            @if(!$transporterDetail['shippingCost'] == '')
                              ${{ number_format( ($value['shippingCost'] - floatval(@$value['aq_fee'])) ,2) }}
                            @endif
                          </div>
                          </td>
                      </tr>
                      <tr>
                        <td class="">
                          <div class="col-xs-8 text-left">
                            <label class="" for="exampleInputEmail1"><b>Aquantuo Earning:</b> </label>
                          </div>
                          <div class="col-xs-4 text-right">
                              ${{ number_format(floatval(@$value['aq_fee']),2) }}
                          </div>
                          </td>
                      </tr>

                      <tr>
                        <td class="">
                          <div class="col-xs-8 text-left">
                            <label class="" for="exampleInputEmail1"><b>Shipping Cost:</b> </label>
                          </div>
                          <div class="col-xs-4 text-right">
                            ${{ number_format($value['shippingCost'],2) }}
                          </div>
                        </td>
                      </tr>
                     </tbody>
                  </table>
               </div>
               @endif
            </div>
         </div>
      </div>
      <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="exampleModalLabel">No Delivery</h4>
               </div>
               {!! Form::model('', ['name' => 'ServicesForm', 'id' => 'ServicesForm', 'method' => 'POST','files' => true] ) !!}
               <div class="modal-body">
                  <div class="form-group">
                     {!! Form::label('package_id', 'Package Title:',['class' => 'control-label']) !!} {{ucfirst($ProductList['product_name'])}}
                  </div>
                  <div class="form-group">
                     {!! Form::label('package_id', 'Package ID:',['class' => 'control-label']) !!} {{$ProductList['package_id']}}
                  </div>
                  <div class="form-group">
                     {!! Form::label('return_address', 'Return Address:',['class' => 'control-label']) !!} {{$transporterDetail->ReturnAddress}}
                  </div>
                  <div class="row">
                     <div class="col-sm-6">
                        <div class="form-group" id="valid-error">
                           <label class="controle-label">Reject By:</label><br>
                           <label class="radio-inline">
                           <input type="radio" name="reject_by" value="transporter" checked onclick="return select_transporter()"> Transporter
                           </label>
                           <label class="radio-inline">
                           <input type="radio" name="reject_by" value="requester" onclick="return select_requester()">&nbsp;Requester
                           </label>
                        </div>
                     </div>
                     <div class="col-sm-6">
                        <div class="form-group" id="valid-error">
                           <label class="controle-label">Return Type:</label><br>
                           <label class="radio-inline">
                           <input type="radio" name="return_type" value="osreturn" <?php echo 'checked=="checked"'; ?> onclick="return select_os()">&nbsp;OS Return
                           </label>
                           <span id="bycreturn" style="display:none;">
                           <label class="radio-inline">
                           <input type="radio" name="return_type" value="creturn" onclick="return select_bycreturn()">&nbsp;Bycreturn
                           </label>
                           </span>
                           <span id="ipayment">
                           <label class="radio-inline">
                           <input type="radio" name="return_type" value="ipayment" onclick="return select_ipayment()">&nbsp;iPayment
                           </label>
                           </span>
                        </div>
                     </div>
                  </div>
                  <div class="form-group" id="msg_div">
                     {!! Form::label('message', 'Message:',['class' => 'control-label']) !!}
                     {!! Form::textarea('message','',['class'=>'form-control required', 'placeholder'=> 'Message','id' =>'message','maxlength' =>200,'size' =>'10x4']) !!}
                     <p class="help-block red" id='er_message'></p>
                     @if($errors->has('message'))
                     <div class="error" style="color:red">{{ $errors->first('message') }}</div>
                     @endif
                  </div>
                  <div class="form-group" id="tracking_div">
                     {!! Form::label('tracking_number', 'Tracking Number:',['class' => 'control-label']) !!}
                     {!! Form::text('tracking_number', '', ['class'=>'form-control required numeric', 'placeholder'=> 'Tracking Number','id' =>'tracking_number','maxlength' => 20]) !!}
                     <p class="help-block red" id='er_tracking_number'></p>
                     @if($errors->has('tracking_number'))
                     <div class="error" style="color:red">{{ $errors->first('tracking_number') }}</div>
                     @endif
                  </div>
                  <div class="form-group" id="date_div" style="">
                     {!! Form::label('Date', 'Date:',['class' => 'control-label']) !!}
                     {!!Form::text('date','',['class'=>'form-control required','readonly'=>'readonly','id'=>'date'])!!}
                     <p class="help-block red" id='er_date'></p>
                     @if($errors->has('date'))
                     <div class="error" style="color:red">{{ $errors->first('date') }}</div>
                     @endif
                  </div>
                  <div class="form-group" id="receipt_div">
                     {!! Form::label('image', 'Upload Receipt:',['class' => 'control-label']) !!}
                     {!! Form::file('image', '', ['class'=>'form-control', 'placeholder'=> '','id'=>'image','width'=>'250px','height'=>'250px'])!!}
                     <p class="help-block red" id='er_image'></p>
                     @if($errors->has('image'))
                     <div class="error" style="color:red">{{ $errors->first('image') }}</div>
                     @endif
                  </div>
               </div>
               <div class="">
                  <div class="col-sm-2">
                     <button type="submit" class="btn btn-default" id="cancel_delivery" >Submit</button>
                  </div>
                  <div class="col-sm-10">
                     {!! Form::button('Close', ['class' => 'btn btn-default', 'data-dismiss'=>'modal']) !!}
                  </div>
                  <div class="clearfix"></div>
               </div>
               {!! Form::close() !!}
               <div class="clearfix"></div>
               <br>
            </div>
         </div>
      </div>
      <div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
         <div class="modal-feedback-width modal-md">
            <div class="modal-content">
               <div class="modal-header popup-bg text-center">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                  <p class="popup-head color-white">Rate your experience with customer</p>
               </div>
               <div class="clearfix"></div>
               <center>
                  <div class="modal-body text-left">
                     {!! Form::model('', ['name' => 'RequestReview', 'id' => 'RequestReview']) !!}
                     <div id="success-message-online"></div>
                     <div class="col-md-12" align="center">
                        <div>
                           @if($user_data->Image != '')
                           <a class="fancybox" rel="group" href="{{ ImageUrl.$user_data->Image}}" >
                           <img src="{{ ImageUrl.$user_data->Image}}"  width="150px" height="150px" class="img-circle">
                           </a>
                           @else
                           <img src="{{ImageUrl}}/user-no-image.jpg" width="150px" height="150px" class="img-circle">
                           @endif
                           </br>
                           <div>
                              </br>
                              <label class="rating_margin" >Rate Now :</label>
                              &nbsp;&nbsp;&nbsp;
                              <div class="rateyo" id="rateyo" ></div>
                              <div class="clearfix"></div>
                              <div id="er_rating"  class="form-error"></div>
                              <input type="hidden" name="item_id" id="item_id" value='<?php echo $item_id; ?>'>
                              <input type="hidden" name="requester_id" id="requester_id" value='<?php echo $requester_id; ?>'>
                              <input id="rating-value" name="rating"  type="hidden" value="">
                              <br />
                              <script type="text/javascript">
                                 $(function () {
                                 $(".rateyo").rateYo();

                                 $(".rateyo-readonly-widg").rateYo({


                                   numStars: 5,
                                   precision: 2,
                                   minValue: '',
                                   maxValue: 5,
                                   starWidth: "30px"

                                   })
                                 }).on("rateyo.change", function (e, data) {

                                     $('#rating-value').val(data.rating);
                                     if($('#rating-value').val() == 0){
                                       $('#rating-value').val('');
                                     }
                                 });
                              </script>
                           </div>
                           <div class="col-md-2"></div>
                        </div>
                        <div class="col-sm-10  col-sm-offset-1" >
                           <div class="form-group txt-area feedback-text">
                              {!! Form::textarea('review','',['class'=>'form-control required', 'placeholder'=> 'Feedback','id' =>'review','maxlength' =>250,'size' =>'30x4']) !!}
                              <div id="er_review" class="help-block white"></div>
                              <div class="clearfix"></div>
                              <span id="review_msg"></span>
                           </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-xs-12 text-right">
                           <div class="col-xs-6">
                              <div class="form-group">
                                 <button type="submit" class="custom-btn1 btn-block" id='ratingButton'>SUBMIT</button>
                                 <div class="error-msg"></div>
                              </div>
                           </div>
                           <div class="col-xs-6">
                              <div class="form-group text-center">
                                 <button class="custom-btn1 btn-block" data-dismiss='modal' >NOT NOW</button>
                              </div>
                           </div>
                        </div>
                     </div>
                     {!! Form::close() !!}
                  </div>
               </center>
               <div class="clearfix"></div>
               </center>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- Accept Modal -->
<div id="exampleModal1" class="modal fade in" role="dialog">
  <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
           <button type="button" class="close" data-dismiss="modal" id="edit_item_close">&times;</button>
           <h4 class="modal-title">Accept Item</h4>
         </div>

         <div class="modal-body">
            <div class="row">
               <div class="col-sm-10 col-sm-offset-1  text-primary" align="center">
                  <b>Are you sure you want to accept the Item '{{ucfirst($transporterDetail->ProductList[0]['product_name'])}}'? Accepted item must be delivered.</b>
                  <hr />
               </div>
            </div>
            <?php
if ($transporterDetail->itemCount == 0) {
	$count = 1;
} else {
	$count = $transporterDetail->itemCount;
}
$aq_fee = $transporterDetail->AquantuoFees / $count;

?>

            <div class="row">

               <div class="col-sm-10 col-sm-offset-1 ">
                  <div class="form-group">
                     <label>Requester Paid</label>
                     <div class="pull-right">${{ number_format((float) $itemCost,2) }}</div>
                  </div>
               </div>

               <div class="col-sm-10 col-sm-offset-1 ">
                  <div class="form-group">
                     <label>Aquantuo Fee</label>
                     <div class="pull-right">(-)${{ number_format((float)$aquantuoFess,2) }}</div>
                  </div>
               </div>

               <div class="col-sm-10 col-sm-offset-1 ">
                  <div class="form-group">
                     <label>Transporter Earning</label>
                     <div class="pull-right">${{ number_format((float) $transporterFess,2) }}</div>
                  </div>
               </div>

            </div>

         </div>

         <div class="modal-footer">
            <div class="col-sm-6">
               <button class="custom-btn1 btn-block"  title="Accept" data-dismiss="modal"  onclick="return confirm('Are you sure you want to accept this request?')? accept_request():'';">
               Accept
               </button>
            </div>
            <div class="col-sm-6">
               <button class="custom-btn1 btn-block" data-dismiss="modal">
               Cancel
               </button>
            </div>
         </div>
      </div>
  </div>
</div>
<!-- End accept Modal -->





{!! Html::script('theme/web/js/validation.js') !!}
{!! Html::script('theme/web/js/utility.js') !!}
{!! Html::style('theme/date-time-picker/jquery.datetimepicker.css') !!}
{!! Html::script('theme/date-time-picker/jquery.datetimepicker.js') !!}
<script type="text/javascript">







   new Validate({
      FormName : 'ServicesForm',
      ErrorLevel : 1,
      validateHidden: false,
      callback : function(){
         if(!$("#cancel_delivery").hasClass('spinning'))
         {
           $("#cancel_delivery").addClass('spinning');
           $.ajax
           ({
             type: "POST",
             url: "{{ url('/by_for_cancel_delivery') }}/{{Request::segment(3)}}",
             data: new FormData(document.getElementById('ServicesForm')),
             processData: false,
             contentType: false,
             dataType: "json",
             success: function(obj)
             {
               alert(obj.msg);
               $("#cancel_delivery").removeClass('spinning');
               if(obj.success == 1) {
                   window.location.reload();
               }

             }
           });
         }
      }

    });

                      $(document).ready(function() {
                           $(".fancybox").fancybox();
                       });
                           var $rateYo = $("#rateyo").rateYo();


    function select_os()
    {
      if($('[name="reject_by"]:checked').val() == "transporter")
      {
        $('#msg_div').show();
        $('#tracking_div').show();
        $('#receipt_div').show();
        $('#date_div').hide();
      }

      if($('[name="reject_by"]:checked').val() == "requester")
      {
        $('#msg_div').show();
        $('#tracking_div').show();
        $('#receipt_div').show();
        $('#date_div').hide();
      }
    }

    function select_ipayment()
    {
      if($('[name="reject_by"]:checked').val() == "transporter")
      {
        $('#msg_div').show();
        $('#tracking_div').hide();
        $('#receipt_div').hide();
        $('#date_div').hide();
      }
    }

    function select_requester()
    {
      if($('[name="return_type"]:checked').val() == "ipayment")
      {

        $('input:radio[name=return_type][value=osreturn]').click();
      }
      $("#bycreturn").show();
      $("#ipayment").hide();


    }
    function select_transporter()
    {
      if($('[name="return_type"]:checked').val() == "creturn")
      {

        $('input:radio[name=return_type][value=osreturn]').click();
      }
      $("#ipayment").show();
      $("#bycreturn").hide();
      $('#date_div').hide();


    }

    function select_bycreturn()
    {
      if($('[name="reject_by"]:checked').val() == "transporter")
      {
        $('#msg_div').show();
        $('#tracking_div').hide();
        $('#receipt_div').hide();
      }

      else if($('[name="reject_by"]:checked').val() == "requester")
      {
        $('#msg_div').show();
        $('#tracking_div').hide();
        $('#receipt_div').hide();
        $('#date_div').show();

      }


    }


    select_os();

    var newdate = new Date();
     $('#date').datetimepicker({
          format:'M d, Y H:i:s A',
          minDate:newdate,
          onChangeDateTime:function( ct ){
              $('#date2').datetimepicker({  minDate:ct  })
          },
      });



    function accept_request()
      {

         $('#accept_request').addClass('spinning');
         $('#accept_request').prop('disabled', true);
           $.ajax
           ({
           type: "POST",
           url: "{{ url('/local-delivery-accept-request') }}/{{Request::segment(3)}}/{{@$transporterDetail->_id}}",

           success: function(res)
           {
           var obj = eval('('+res+')');
           console.log(obj);
           $('#accept_request').removeClass('spinning');
           if(obj.success == 1)
           {
            alert('Request has been accepted successfully.');
            window.location.reload();
            //$('#success-message').html('<div role="alert" class="alert alert-success alert-dismissible fade in">'
            //+'<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>'+obj.msg+'</div>');

            $("#accept_request").hide();
            $("#cancel_request").show();
            $("#out_for_picup").show();
            $("#status_div").hide();
            $('#chat-btn').show();
            $("#ajax_status").show();
            $("#ajax_status").html('<strong>Status:</strong> Accepted');



           }else {
              alert(obj.msg);
              window.location.reload();

           }

           }
           });
            return false;

      }


      function cancel_request(id)
      {
         $('#cancel_request').addClass('spinning');
         $('#cancel_request').prop('disabled', true);


         var url = "{{ url('/send-package-cancel-request') }}/{{$transporterDetail->_id}}/"+id;
         $.ajax
           ({
           type: "POST",
           url: url,
           success: function(res)
           {
           var obj = eval('('+res+')');
           alert(obj.msg);
           $('#cancel_request').removeClass('spinning');
           if(obj.success == 1)
           {
            //$("#ajax_status").html('<strong>Status:</strong> Ready');
           window.location.reload();
           }

           }
           });
            return false;

      }

      function out_for_pickup(id)
      {
         $('#out_for_picup').addClass('spinning');
         $('#out_for_picup').prop('disabled', true);
         $('#cancel_request').prop('disabled', true);
           $.ajax
           ({
           type: "POST",
           url: "{{ url('local-delivery-pickup-request') }}/{{$transporterDetail->_id}}/"+id,

           success: function(res)
           {
           var obj = eval('('+res+')');

           $('#out_for_picup').removeClass('spinning');
           $('#cancel_request').prop('disabled',false);
           if(obj.success == 1)
           {
             alert('Request has been pickedup successfully.');
              window.location.reload();
            //$('#success-message').html('<div role="alert" class="alert alert-success alert-dismissible fade in">'
           // +'<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>'+obj.msg+'</div>');


            $("#out_for_picup").hide();
            $("#out_for_delivery").show();
            $("#status_div").hide();
            $("#ajax_status").show();
            $("#ajax_status").html('<strong>Status:</strong> Out for Pickup');


           }else {
                alert(obj.msg);
           }

           }
           });
            return false;

      }

      function out_for_deliver(id)
      {
         $('#out_for_delivery').addClass('spinning');
         $('#out_for_delivery').prop('disabled', true);
         $('#cancel_request').prop('disabled',true);
         $.ajax
           ({
           type: "POST",
           url: "{{ url('local-delivery-deliver-request') }}/{{$transporterDetail->_id}}/"+id,
           success: function(res)
           {
           var obj = eval('('+res+')');
           console.log(obj);
           $('#out_for_delivery').removeClass('spinning');
           $('#cancel_request').prop('disabled',false);
           if(obj.success == 1)
           {
               alert('The package is out for delivery.');
               window.location.reload();
            //$('#success-message').html('<div role="alert" class="alert alert-success alert-dismissible fade in">'
            //+'<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>'+obj.msg+'</div>');


            $("#out_for_delivery").hide();
         $("#cancel_request").hide();
            $("#delivery_btn").show();
            $("#delivery").show();
            $("#status_div").hide();
            $("#ajax_status").show();
            $("#ajax_status").html('<strong>Status:</strong> Out for Delivery');

           }else {
                alert(obj.msg);
           }

           }
           });
            return false;

      }
   new Validate({
     FormName : 'b_verify',
     ErrorLevel : 1,
     validateHidden: false,
     callback : function(){
       if(!$("#b_verify_button").hasClass('spinning'))
       {
         if(confirm('Are you sure the package was successfully delivered?'))
         {
           $("#b_verify_button").addClass('spinning');
           $.ajax({
             url     : '{{url("local-delivery-verify-delivery-code")}}',
             type    : 'post',
             data    : "code="+$("#b_code").val()+"&item_id="+$('#request_id').val()+"&request_id={{$transporterDetail->_id}}",
             success : function(res)
             {
               $("#b_verify_button").removeClass('spinning');
               var obj = eval("("+res+")");
               if(obj.success == 1)
               {
                  alert('You have successfully delivered an item.');
                  window.location.reload();

                 $( "#b_deliver_model" ).trigger( "click" );
                 $( "#b_deliver_model" ).trigger( "reset" );
                 $('#success-message-online').html('<div role="alert" class="alert alert-success alert-dismissible fade in">'
                 +'<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>'+obj.msg+'</div>');


                 $("#b_deliver").hide();
                 $("#b_no_delivery").hide();

                 $("#status_div").hide();
                 $("#ajax_status").show();
                 $("#ajax_status").html('<strong>Status:</strong> Delivered');

               }
               alert(obj.msg);
             }
           });
         }
       }
     }
   });


     new Validate({
       FormName : 'RequestReview',
       ErrorLevel : 1,
       validateHidden: false,
       callback : function(){
         form_review();
       }

     });

     function form_review()
     {
           if($rateYo.rateYo("rating") <= 0) {
           $('#er_rating').html('The rating field is required.');
           return false;
         }
           $("#ratingButton").addClass('spinning');
           $.ajax({
           url     : '{{url("/send-package-transporter-review")}}',
           type    : 'post',
           data:"request_id={{$transporterDetail->_id}}&item_id={{Request::segment(3)}}&review="+$("#review").val()+"&rating="+$('#rating-value').val()+"&user_id={{$transporterDetail->RequesterId}}",
           success : function(res){
               $("#ratingButton").removeClass('spinning');
               var obj = eval("("+res+")");
           if(obj.success == 1)
           {
               alert(obj.msg);
               location.reload();
             $("#feedback").hide();
             $("#RequestReview").trigger( "reset" );
             $('#success-message-online').html('<div role="alert" class="alert alert-success alert-dismissible fade in">'
            +'<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>'+obj.msg+'</div>');
           }
           else
           {
            alert(obj.msg);
             $("#modal2").trigger('click' );
             $("#feedback").hide();
             $('#success-message').html('<div role="alert" class="alert alert-danger alert-dismissible fade in">'
         +'<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>'+obj.msg+'</div>');

           }


                  }
               });

       return false;

     }










</script>
<style>
   .rating_margin
   {
   transition: transform 0.8s cubic-bezier(0, 0.63, 0.41, 0.98) 0s, opacity 0.8s cubic-bezier(0, 0.63, 0.41, 0.98) 0s;
   transform: translateY(0px);
   opacity: 1;
   }
   .to-center{text-align:middle;}
</style>
@endsection
<?php
function change_unit($weight, $currentunit, $changeto) {

	//  echo $weight." ".$currentunit." ".$changeto; //die;

	//  echo "<br>";

	$newweight = '';
	$newchangeto = $changeto;
	switch ($currentunit) {
	case 'kg':
		$newweight = $weight * 1000;
		break;
	case 'lbs':
		$newweight = $weight * 0.00220462;
		break;
	case 'inches':
		$newweight = $weight;
		break;
	case 'cm':
		$newweight = $weight * 0.393701;
		break;
	}

	switch ($changeto) {
	case 'kg':
		$newweight = $newweight / 1000;
		break;
	case 'lbs':
		$newweight = $newweight * 0.00220462;
		break;
	case 'cm':
		$newweight = $newweight / 0.393701;
		break;
	case 'inches':
		$newweight = $newweight;
		break;
	}

	return $newweight;

}

?>

