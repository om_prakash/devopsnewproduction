 <script>

function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else {
        alert("Geolocation is not supported by this browser.");
    }
}
function showPosition(position) {

  var url = '{{url("set_location")}}' +"?latitude=" + position.coords.latitude +"&longitude=" + position.coords.longitude;
  url = url+'&href='+window.location.href;  
  window.location.href = url;  
}
getLocation();


</script>