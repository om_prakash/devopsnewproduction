<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1 user-scalable=no">
    <meta name="description" content="A growing trustworthy community of Transporters and Requesters, Aquantuo bridges the gap between the person wanting to have an item delivered to another location and the person or company willing to do so.">
    <meta name="keywords" content="Aquantuo, Aq, Delivery, Shipping, Package, Ghana, USA States, Transporter, Requester, app">
    <meta name="author" content="Clement">
    <meta http-equiv="refresh" content="3000">
    
    <link rel="icon" type="image/png" href="{{ url('theme/web/promo/images/favicon.png')}}"/>
    
    @if (trim($__env->yieldContent('page_title')))
		<title>@yield('page_title')</title>
    @else<title>Shipping Internationally - Air Freight and Sea Freight - Aquantuo</title>
    @endif
    <link href="theme/web/promo/images/favicon.png" type="image/png" rel="icon">
    {!! Html::script('theme/web/js/jquery.js') !!}
    {!! Html::script('theme/web/promo/js/bootstrap.min.js') !!}
    {!! Html::script('theme/fancybox/source/jquery.fancybox.js') !!}
    {!! Html::style('theme/fancybox/source/jquery.fancybox.css') !!}
    {!! Html::script('theme/fancybox/source/mouse_wheel.js') !!} 
    <!-- Bootstrap -->
    {!! Html::style('theme/web/promo/css/bootstrap.min.css') !!} 
    {!! Html::style('theme/web/promo/css/style.css') !!}
    {!! Html::style('theme/web/promo/css/style.min.css') !!}
    {!! Html::style('theme/web/promo/css/font-awesome.css') !!}

    
    <script type="text/javascript"> 

      var SITEURL = '{{URL::to("")}}/';
      var USERID = '{{Session::get("UserId")}}',
          USERIMAGE = '{{Session::get("Image")}}',
          USERNAME = '{{Session::get("Name")}}';


      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                              })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
      ga('create', 'UA-78880450-1', 'auto');
      ga('send', 'pageview'); 
    </script>
    
  </head>
<body>
    <!--pop up open -->
    
    <!-- pop close-->
    <div class="container-fluid">
    <div class="row">
      <!--parallax 1 -->
      <nav class="navbar navbar-default nav-bg navbar-fixed-top responsive first_menu" style="">
        <div class="container">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <div class="navbar-left">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
            </div>
    
            <div class="cetner-m">
              <a class="navbar-brand " href="#"><img src="{{asset('theme/web/promo/images/logo.png')}}">
              </a>
            </div>
            <div class="navbar-right">
              <button type="button" class="navbar-toggle collapsed pull-left" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
            </div>        
          </div>
          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <div class="row">


            <?php if(Session::get( 'UserId') != '' ) {   ?>
             <ul class="nav navbar-nav pull-right dropdown downarrow">
          <li class="nav-item avatar"> 
          
             <?php if(Session::get('Image') != '') {  ?>
                  <img src="{{ImageUrl.Session::get('Image')}}" class="img-fluid"  width="40" height="40"> 
                <?php } else {  ?>
                  <img src="{{ImageUrl}}/user-no-image.jpg" class="img-fluid"  width="40" height="40"> 
              <?php } ?>
          
          </li>
         
         <div class="dropdown-menu dropdown-info logoutbar" aria-labelledby="dropdownMenu3" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
            <a class="dropdown-item waves-effect waves-light" href="{{url('logout')}}">Log Out</a>
        </div>
        <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="{{url('my-profile')}}">My Profile</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1"  href="{{url('statistics')}}">Statistics</a></li>

                    <li role="presentation"><a role="menuitem" tabindex="-1" href="{{url('change-password')}}">Change Password</a>
                    </li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="{{url('support')}}">Support</a>
                    </li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="{{url('logout')}}">Logout</a>
                    </li>
                  </ul>
                  </li>
        </ul>
        <?php } ?>
 


 
  


              <ul class="nav navbar-nav navbar-right">
                <li id="add-class" class="@if(in_array(Request::segment(1),['dashboard']))  active @endif" ><a href="{{url('')}}#home" onclick="add_class()">Home</a></li>
                <li id="add-class" class="@if(in_array(Request::segment(1),['price-estimator']))  active @endif"><a href="{{url('price-estimator')}}" >Price Estimator</a><div></div></li>
                <?php if(Session::get( 'UserId') != '' ) {   ?>
                <li class="@if(in_array(Request::segment(1),['my-request','trip-detail','prepare-request','near-by-transporter','delivery-details','posts-by-transporters','buy-for-me-detail','online-request-detail','payment-by-for-me','on-line-purchases','online-payment','buy-for-me','process-card-list','post-by-transporter','edit-online-purchase','buyforme-payment']))  active @endif "><a href="{{url('my-request')}}">Requester</a></li>
                @if(in_array(Session::get( 'Usertype'), ['transporter','both'] ))
                <li class="@if(in_array(Request::segment(1),['transporter']))  active @endif" ><a href="{{url('transporter')}}">Transporter </a></li>
                @endif
                <li class="visible-md-lg  @if(in_array(Request::segment(1),['my-profile','change-password','address','faq-list','reward','edit-transporter-profile','setting','edit-profile','my-aquantuo-address','card-list','support','become-transporter','statistics','pending-request','transporter-pending-request','bank-information','edit-bank-information']))  active @endif 
                      
                 dropdown">
                
                
                 <!--  <a class="dropdown-toggle " id="menu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Account<span class="caret"></span></a>
                  <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="{{url('my-profile')}}">My Profile</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1"  href="{{url('statistics')}}">Statistics</a></li>

                    <li role="presentation"><a role="menuitem" tabindex="-1" href="{{url('change-password')}}">Change Password</a>
                    </li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="{{url('support')}}">Support</a>
                    </li>
                  </ul>   -->
                  </li>
                   <li class="nav-item storeName  waves-effect waves-light" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <?php  echo Session::get('FirstName'); echo " ";
                  echo Session::get('LastName');   ?>
                
          </li>
                  
                  <li class="visible-sm-xs">
                  <!-- for responsive -->
                  <a class="custom-drp-tggl">Account<span class="caret"></span></a>
                  <ul class="custom-drp-d" style="display:none;">
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="{{url('my-profile')}}">My Profile</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="{{url('change-password')}}">Change Password</a></li>
                  </ul>
                  <!-- for responsive -->
                  
                  
    
                </li>
               <!-- <li><a href="{{url('logout')}}">Logout</a></li>   -->
                <?php } else { ?>
                <li id="add-class" class="@if(in_array(Request::segment(1),['#aboutapp']))  active @endif" ><a href="{{url('')}}#aboutapp">How it works </a></li>
<!--
                <li id="add-class" class="@if(in_array(Request::segment(1),['#features']))  active @endif"><a href="{{url('')}}#features">Features</a></li>
                <li id="add-class" class=""><a href="{{url('')}}?type=place-an-order">Place an Order</a></li>
                <li id="add-class" class="@if(in_array(Request::segment(1),['#download']))  active @endif"><a href="{{url('')}}#download">Download</a></li>
                <li id="add-class" class="@if(in_array(Request::segment(1),['#contact']))  active @endif"><a href="{{url('')}}#contact">Contact</a></li>
-->
                <li id="add-class" class="visible-md-lg dropdown @if(in_array(Request::segment(1),['signup','login']))  active @endif @if(in_array(Request::segment(1),['#login']))  active @endif">

                  <a class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >Account<span class="caret"></span></a>
                  <ul class="dropdown-menu {{ (Request::is('signup') ? 'active' : '') }} {{ (Request::is('login') ? 'active' : '') }}" >
                    <li ><a href="{{url('signup')}}">Sign up</a>
                    </li>
                    <li ><a href="{{url('login')}}">Sign in</a>
                    </li>
                  </ul>   
                  </li>
                  <li class="visible-sm-xs">
                  <!-- for responsive -->
                  <a class="custom-drp-tggl">Account<span class="caret"></span></a>
                  <ul class="custom-drp-d {{ (Request::is('signup') ? 'active' : '') }} {{ (Request::is('login') ? 'active' : '') }}" style="display:none;">
                    <li ><a href="{{url('signup')}}">Sign up</a>
                    </li>
                    <li ><a href="{{url('login')}}">Sign in</a>
                    </li>
                  </ul>
                  </li>   
				  <!-- for responsive -->
					                         
                  <li class="{{ (Request::is('transporter-signup') ? 'active' : '') }}" ><a href="{{url('transporter-signup')}}?type=transporter">Become a Transporter</a></li>    
                <!-- <li class="<?php/* if(in_array(Input::get('type'),['transporter'])) { echo "active"; }  */?>"><a href="{{url('transporter-signup')}}?type=transporter">Become a Transporter</a></li> -->    
                <?php } ?>
              </ul>
              
            </div>
          </div>
          <!-- /.navbar-collapse -->
          <?php if(Session::get( 'UserId') !='' ) { ?>
          <?php  }  ?>
        </div>
    
        <!-- /.container-fluid -->
      </nav>
      <!-- Button trigger modal -->
    </div>    
    @if(Session::get('UserId') != '')
    <div class="row">  
      <nav class="navbar navbar-default second-menu">
        <div class="container">
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
            <div class="row">
              <ul class="nav navbar-nav">
                
                @if(Request::segment(1) == 'transporter')
                <li class="@if(in_array(Request::segment(2),['','details']))  active @endif" ><a  href="{{url('transporter')}}">New Requests</a></li>
                <li class="@if(in_array(Request::segment(2),['my-deliveries','delivery-details','online_detail','buy_for_me_detail']))  active @endif"><a  href="{{url('transporter/my-deliveries')}}">My Deliveries</a></li>
                <li class="@if(in_array(Request::segment(2),['post-a-trip']))  active @endif"><a  href="{{url('transporter/post-a-trip')}}">Post a Trip</a></li>
                <li class="visible-md-lg  @if(in_array(Request::segment(2),['bussiness-trips','individual-trips','edit_individual','view-request']))  active @endif dropdown">							                    
                  <a class="dropdown-toggle" id="menu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">My Trips<span class="caret"></span></a>
                  <ul class="dropdown-menu" role="menu" aria-labelledby="menu2">
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="{{url('transporter/individual-trips')}}">Individual</a></li>
                    @if(Session()->get('TransporterType') == 'business')
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="{{url('transporter/bussiness-trips')}}">Business</a></li>
                    @endif
                  </ul>
                  </li>
                  <li class="visible-sm-xs">
                   <!-- for responsive -->
                   <a class="custom-drp-tggl" id="menu2">My Trips<span class="caret"></span></a>
                  <ul class="custom-drp-d" style="display:none;">
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="{{url('transporter/individual-trips')}}">Individual</a></li>
                    @if(Session()->get('TransporterType') == 'business')
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="{{url('transporter/bussiness-trips')}}">Business</a></li>
                    @endif 
                  </ul>
                   <!-- for responsive -->
                </li>
    
    
                @else
                <li class="@if(in_array(Request::segment(1),['prepare-request','buy-for-me','process-card-list','on-line-purchases','buyforme-payment'])) active @endif"><a data-toggle="modal" id="Edit" title="Edit" data-whatever="@mdo" href="#popup"  onclick="$('.animat').removeClass('animat');" href="#">Create Request</a></li>

                <li class="@if(in_array(Request::segment(1),['my-request','delivery-details','buy-for-me-detail','edit-request','edit-buy-for-me','online-request-detail','payment-by-for-me','online-payment','edit-online-purchase'])) active @endif"  ><a  href="{{url('my-request')}}">My Requests</a></li>
                <li class="@if(in_array(Request::segment(1),['near-by-transporter','trip-detail'])) active @endif "    class="@if(in_array(Request::segment(4),[''])) active @endif"><a  href="{{url('near-by-transporter')}}">Nearby Transporters</a></li>
                <li class="@if(in_array(Request::segment(1),['post-by-transporter'])) active @endif"><a  href="{{url('post-by-transporter')}}">Posts by Transporters</a></li>
               
                
              @endif

              <li class="@if(in_array(Request::segment(1),['notification'])) active @endif"><a  href="{{url('notification')}}">Notifications</a></li>


              <div class="profile-side-menu">
                  <li><a href="{{url('my-profile')}}"><i aria-hidden="true" class="fa fa-user"></i>General Information</a></li>

                  <li><a href="{{url('my-aquantuo-address')}}" ><i aria-hidden="true" class="fa fa-map-marker"></i>My Aquantuo Address</a></li>
                  <li><a href="{{url('card-list')}}"><i aria-hidden="true" class="fa fa-credit-card"></i>Cards</a></li>
                  <li><a href="{{url('bank-information')}}"><i aria-hidden="true" class="fa fa-bank"></i>Bank Information</a></li>
    
                  <li class="dropdown visible-md-lg">
                  <a class="dropdown-toggle" id="setting1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i aria-hidden="true" class="fa fa-cog"></i>Setting<span class="glyphicon glyphicon-chevron-right pull-right"></span></a>
                    <ul class="dropdown-menu nav nav-pills nav-stacked new" role="menu" aria-labelledby="setting1">
                      <li><a href="#">General Setting</a></li>
                      <li><a href="#">Shipping Setting</a></li>
                      <li><a href="#">Saved Address</a></li>
                      <li><a href="#">Buy for me / Shopping Assistant</a></li>
                      <li><a href="#">Payments</a></li>
                      <li><a href="#">Order History</a></li>
                    </ul>
                   </li> 
                    <li class="visible-sm-xs">
                    <a class="custom-drp-tggl"><i aria-hidden="true" class="fa fa-cog"></i>Setting<span class="glyphicon glyphicon-chevron-right pull-right"></span></a>
                    <ul class="custom-drp-d" style="display:none;">
                      <li><a href="#">General Setting</a></li>
                      <li><a href="#">Shipping Setting</a></li>
                      <li><a href="#">Saved Address</a></li>
                      <li><a href="#">Buy for me / Shopping Assistant</a></li>
                      <li><a href="#">Payments</a></li>
                      <li><a href="#">Order History</a></li>
                    </ul>
                    
                  </li>
                </li> 
    
              </div>
            </ul>
          </div>
        </div>
      </div>
    </nav>
    </div>
    @endif
    
    <script>
    $(document).ready(function(e) {
      $(".custom-drp-tggl").click(function(){
        $(this).siblings().toggleClass("menu-open")	
      });
    });
    </script>
    
    
    
    
    <div class="clearfix"></div>
    <div class="margin-sec"></div>
    <div class="clearfix"></div>
    <div class="wrapper clearfix row">
    <div class="@yield('bg-color')">
        <div class="container">
          <div class="row">
            <div class="flash-message text-left" id="error_msg_section">
              @foreach (['danger', 'warning', 'success', 'info'] as $msg) 
                @if(Session::has($msg))
                  <br />
                  <p class="alert alert-{{ $msg }}"><?php print_r(Session()->get($msg));?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  </p>
                @endif
              @endforeach
            </div>
          </div>
        </div>
      @yield('content')
    </div>
    </div>
    <div class="row">
    <div class="container-fluid section-custom footer">
      <div class="container">
        <div class="col-md-12 col-sm-12 customf">
          <div class="row">
            <div class="col-md-6 col-sm-6">
              <div class="heading3 offset-btm-20">About</div>
              <br>
              {!! HTML::image('theme/web/promo/images/white_logo.png','',array('width' =>'50%','class'=>'footer-logo' )); !!}
              <p class="color-white">A growing trustworthy community of Transporters and Requesters, Aquantuo bridges the gap between the person wanting to have an item delivered to another location and the person or company willing to do so.</p>
            </div>
            <div class="col-md-3 col-sm-3 sitemap-style">
              <div class="heading3 offset-btm-20">Site map</div>
              <br>
              <ul style="margin-top:0px;" class="footer-list">
                <li style="margin-bottom:0px;"><a href="{{url('/')}}#home">- Home</a></li>
                <li style="margin-bottom:0px;"><a href="{{url('/')}}#aboutapp">- About App</a></li>
                <li style="margin-bottom:0px;"><a href="{{url('/')}}#features">- Features </a></li>
                <li style="margin-bottom:0px;"><a href="{{url('/')}}#download">- Download</a></li>
                <li style="margin-bottom:0px;"><a href="{{url('')}}#contact">- Contact</a></li>
              </ul> 
            </div>
            <div class="col-md-3 col-sm-3">
              <div class="heading3 offset-btm-20">Social media</div>
              <br>
              <div class="social-media clearfix">
                <div class="col-md-4 col-sm-4 col-xs-4">
                  <a href="https://www.facebook.com/Aquantuo" target="_blank">
                    <div class="social-icon-facebook">
                      <i class="fa fa-facebook"></i>
                    </div>
                  </a>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4">
                  <a href="https://twitter.com/Aquantuo" target="_blank">
                    <div class="social-icon-twitter">
                      <i class="fa fa-twitter"></i>
                    </div>
                  </a>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4">
                  <a href="https://www.instagram.com/Aquantuo/" target="_blank">
                    <div class="social-icon-instagram">
                      <i class="fa fa-instagram"></i>
                    </div>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="second-f clearfix row">
        <div class="col-xs-12 text-center">
          Copyright &copy; {{date('Y')}} Aquantuo&nbsp;&nbsp; | &nbsp;&nbsp;
          <a href="{{url('terms-and-conditions')}}" target="_blank">Terms and Conditions</a> &nbsp;&nbsp; | &nbsp;&nbsp;
          <a href="{{url('privacy-policy')}}" target="_blank">Privacy Policy</a>
        </div>
      </div>
    </div>
    </div>
    </div>
    </div>
    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    {!! Html::script('theme/web/promo/js/waypoints.min.js') !!}

    @section('script')
    
    @show
    
    @yield('inline-script')
    
    <script>
    $('body')
    .off('click.dropdown touchstart.dropdown.data-api', '.dropdown')
    .on('click.dropdown touchstart.dropdown.data-api', '.dropdown form', function (e) { e.stopPropagation() })
    window.jQuery || document.write('<script src="js/jquery-1.9.1.min.js"><\/script>')

    function myFunction() {
      document.getElementById("#mypopup1").removeClass("animat");
    }

    $(".custom-table").wrap('<div class="table-responsive">');
    $('.required').each(function(){
      if($(this).hasClass('required'))
      {
        var myhtml = "<span class='red-star'> *</span>" 
        $(this).prev('label').append(myhtml);
      }
    });
    $(document).ready(function(e) {
           
      $(".close-button").click(function() {
        $(".custom-popup").addClass("animat");
      });

    });
    </script>
    @if(!session::has('latitude'))
      @include('Page::layout.share_location')
    @endif

  <!--   @if(!empty(session()->get('UserId')))
      {!! Html::script('theme/web/js/user.js')!!}
      @include('Page::layout.chat')
    @endif -->
    
</body>
</html>
<div class="modal fade" id="popup" data-backdrop="false" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div id="mypopup1" class="custom-popup animat">
      <div class="times"><a href="#" data-dismiss="modal" class="close-button"></a></div>
      <div class="">
        <a href="{{url('prepare-request')}}">
          <div class="row-opt clearfix">
            <div class="col-md-2">
              <img class="row-opt-img" src="{{asset('theme/web/promo/images/preapre_rqst.png')}}">
            </div>
            <div class="col-md-10 row">
              <div class="options">
                <div>Prepare Request</div>
                <p>Request for a package to be moved from one place to another.</p>
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
        </a>
        <a href="{{url('buy-for-me')}}">
          <div class="row-opt clearfix">
            <div class="col-md-2">
              <img class="row-opt-img" src="{{asset('theme/web/promo/images/buy_for_me.png')}}">
            </div>
            <div class="col-md-10 row">
              <div class="options">
                <div>Buy For Me</div>
                <p>Tell us what you want to buy. We will buy it for you and have it sent to you.
</p>
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
        </a>
        <a href="{{url('on-line-purchases')}}">
          <div class="row-opt clearfix">
            <div class="col-md-2">
              <img class="row-opt-img" src="{{asset('theme/web/promo/images/online_purchase.png')}}">
            </div>
            <div class="col-md-10 row">
              <div class="options">
                <div>Your Online Purchases</div>
                <p> Shop online, pay for it and have it shipped to your unique Aquantuo address. We will then have it sent to an address of your choosing.</p>

              </div>
            </div>
            <div class="clearfix"></div>
          </div>
        </a>
      </div>  
    </div>
    </div> 
</div>
</div>
