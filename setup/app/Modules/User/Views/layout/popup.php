
<!-- Modal -->
<div class="modal fade" id="price_est" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg my-popup-width" role="document">
    <div class="modal-content price_estimator">
        <form name="price_estimatory_form" id="price_estimatory_form">
            <div class="modal-body  clearfix">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>

                <div class="col-sm-8">
                    <h3 class="modal-title text-left" id="myModalLabel">Price Estimator</h3>
            	<div class="form-group clearfix">
                    <label class="control-label">Delivery Address</label>
                    <div class="dotted-line"></div>
                    <div class="address-bar-custom">
                        <div class="from clearfix">
                        	<div class="point"></div>
                            <div class="custom-input">
                        		<input class="" placeholder="From" name="" id="">
                            </div>
                        </div>
                        <div class="to clearfix">
                        	<div class="point"></div>
                        	<div class="custom-input">
                        		<input class="" placeholder="To">
                            </div>
                        </div>
                    </div>
                </div> 
                <div class="form-group clearfix">
                    <div class="col-sm-12">
                       <div class="form-group">
                          <label class="control-label">Shipping Mode</label>
                          <div class="clearfix">
                             <label class="radio-inline">
                                 <input type="radio" name="travel_mode" id="travel_mode" value="air" onclick="toggle_category('.travel-mode-air','.travel-mode-ship','#package_category')" checked="checked">By Air
                             </label>
                             <label class="radio-inline">
                                 <input type="radio" name="travel_mode" id="travel_mode" value="ship" onclick="toggle_category('.travel-mode-ship','.travel-mode-air','#package_category')">By Sea
                             </label>
                          </div>
                       </div>
                    </div>
                    <div class="col-sm-12">
                       <div class="form-group">
                          <label class="control-label">Select Package Category</label>
                          <select name="category" id="package_category" class="form-control required">
                             <option value="">Select Category</option>
                                                            <option class="travel-mode-ship" value="{&quot;id&quot;:&quot;56b49cc1cf32074f325a2899&quot;,&quot;name&quot;:&quot;Suitcase&quot;}" disabled="disabled" style="display: none;">Suitcase</option>
                                                            <option class="travel-mode-air" value="{&quot;id&quot;:&quot;568e2f7ccf3207975ae0d8be&quot;,&quot;name&quot;:&quot;Other&quot;}">Other</option>
                                                            <option class="travel-mode-ship" value="{&quot;id&quot;:&quot;56b4a26fcf32074b355a2899&quot;,&quot;name&quot;:&quot;Drum / Barrel&quot;}" disabled="disabled" style="display: none;">Drum / Barrel</option>
                                                            <option class="travel-mode-ship" value="{&quot;id&quot;:&quot;56b4a3b3cf320786355a2899&quot;,&quot;name&quot;:&quot;Wardrobe Box&quot;}" disabled="disabled" style="display: none;">Wardrobe Box</option>
                                                            <option class="travel-mode-air" value="{&quot;id&quot;:&quot;567cab04cf32073f26a024ce&quot;,&quot;name&quot;:&quot;Document&quot;}">Document</option>
                                                            <option class="travel-mode-air" value="{&quot;id&quot;:&quot;5650140b6734c4af698b4567&quot;,&quot;name&quot;:&quot;Electronics&quot;}">Electronics</option>
                                                            <option class="travel-mode-ship" value="{&quot;id&quot;:&quot;56b4a50ecf320724365a2899&quot;,&quot;name&quot;:&quot;Furniture&quot;}" disabled="disabled" style="display: none;">Furniture</option>
                                                            <option class="travel-mode-ship" value="{&quot;id&quot;:&quot;568e2e12cf3207d25ae0d8bd&quot;,&quot;name&quot;:&quot;Automobile - Van&quot;}" disabled="disabled" style="display: none;">Automobile - Van</option>
                                                            <option class="travel-mode-ship" value="{&quot;id&quot;:&quot;56b19ed5cf3207b6185a2899&quot;,&quot;name&quot;:&quot;Box (Small)&quot;}" disabled="disabled" style="display: none;">Box (Small)</option>
                                                            <option class="travel-mode-ship" value="{&quot;id&quot;:&quot;56b19f29cf3207bd185a2899&quot;,&quot;name&quot;:&quot;Other&quot;}" disabled="disabled" style="display: none;">Other</option>
                                                            <option class="travel-mode-air" value="{&quot;id&quot;:&quot;568e3b0fcf3207d860e0d8bd&quot;,&quot;name&quot;:&quot;Suitcase&quot;}">Suitcase</option>
                                                            <option class="travel-mode-ship" value="{&quot;id&quot;:&quot;56d8fdffcf3207063869c999&quot;,&quot;name&quot;:&quot;Auto - Fullsize SUV&quot;}" disabled="disabled" style="display: none;">Auto - Fullsize SUV</option>
                                                            <option class="travel-mode-ship" value="{&quot;id&quot;:&quot;56d8fe2acf3207673069c999&quot;,&quot;name&quot;:&quot;Auto - Intermediate SUV&quot;}" disabled="disabled" style="display: none;">Auto - Intermediate SUV</option>
                                                            <option class="travel-mode-ship" value="{&quot;id&quot;:&quot;56d8fe15cf3207bb2b69c999&quot;,&quot;name&quot;:&quot;Auto - Standard SUV&quot;}" disabled="disabled" style="display: none;">Auto - Standard SUV</option>
                                                            <option class="travel-mode-ship" value="{&quot;id&quot;:&quot;56ee5869cf3207d471525620&quot;,&quot;name&quot;:&quot;Auto - Sedan Intermediate&quot;}" disabled="disabled" style="display: none;">Auto - Sedan Intermediate</option>
                                                            <option class="travel-mode-ship" value="{&quot;id&quot;:&quot;56ee584ccf32071c0d52561f&quot;,&quot;name&quot;:&quot;Auto - Sedan Economy&quot;}" disabled="disabled" style="display: none;">Auto - Sedan Economy</option>
                                                       </select>
                       </div>
                    </div>
                </div>

                <div class="form-group clearfix">
                    <label class="control-label">Measurment Unit</label>
                    <br>
                    <label class="radio-inline"><input type="radio" name="measurement_unit" value="cm_kg">Metric (cm/kg)</label>
                    <label class="radio-inline">
                        <input type="radio" name="measurement_unit" value="inches_lbs">Imperial (inches/lbs)</label>
                    
                </div>
                <div class="form-group clearfix">
                    <label class="control-label">Item Weight</label>
                    <br>
                   <div class="custom-input">
                        <input class="" name="pe_item_weight" value="">
                   </div>
                    
                </div>
                <div class="row">
                    <div class="col-xs-4">
                        <div class="form-group clearfix">
                            <label class="control-label">Length</label>
                            <br>
                           <div class="custom-input">
                                <input class="" name="length" id="pe_length" >
                           </div>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group clearfix">
                            <label class="control-label">Height</label>
                            <br>
                           <div class="custom-input">
                                <input class="" name="height" id="pe_height">
                           </div>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group clearfix">
                            <label class="control-label">Width</label>
                            <br>
                           <div class="custom-input">
                                <input class="" name="width" id="pe_width">
                           </div>
                        </div>
                    </div>
                </div>
                        <div class="form-group clearfix">
                            <button class="btn btn-primary btn-block">Calculate Now</button>
                        </div>
            </div>
            <div class="col-sm-4 calcu">
            	<div class="blue-bg row text-center color-white">
                <br />
                	<h4>Estimated Shipping Cost</h4>
                    <h1 class="large_price">$<span id="ps_total_cost">00.00</span></h1>
                    <p class="color-white">(In Ghananian cedi GHS 0.00)</p>
                    <br />
                </div>
                <div class="row">
                <div class="col-xs-12">
                	<div class="pull-left">
                		<label class="control-label">Distance-</label>
                    </div>
                    <div class="pull-right">
                		<label class="control-label">5,000.00 Miles</label>
                    </div> 
            	</div>
                <div class="col-xs-12">
                	<div class="pull-left">
                		<label class="control-label">Item Weight-</label>
                    </div>
                    <div class="pull-right">
                		<label class="control-label">58 kg</label>
                    </div> 
            	</div>
                <div class="col-xs-12">
                	<div class="pull-left">
                		<label class="control-label">Volume-</label>
                    </div>
                    <div class="pull-right">
                		<label class="control-label">58 kg</label>
                    </div> 
            	</div>
                
            </div>
            	<div class="form-group clearfix">
                            <button class="btn btn-primary btn-block">Make Request</button>
                        </div>
            </div>
            </div>
        </form>      
    </div>
  </div>
</div>
