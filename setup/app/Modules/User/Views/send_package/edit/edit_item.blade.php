@extends('Page::layout.one-column-page')
@section('content')
<div class="container">
  <div class="row">
      <div class="col-sm-12" id="send_a_package_start_position">
      <h2 class="color-blue mainHeading">Send A Package</h2>
      <br>
      </div>
      <div class="col-sm-12">
        <div class="box-shadow">
          <h3 class="midHeading">Please complete the following fields</h3>
          <br>
          <hr>


          <div class="col-sm-12">
              <div class="step_box four_step clearfix">

                <div class="step first selected" id="step1-header">
                     <div>1</div>
                     <p class="text-center colol-black">Package Details</p>
                  </div>

                  <div class="step inner" id="step2-header">
                     <div>2</div>
                     <p class="text-center colol-black">Pickup Address</p>
                  </div>

                  <div class="step inner" id="step3-header ">
                     <div>3</div>
                     <p class="text-center colol-black">Drop Off Address</p>
                  </div>

                  <div class="step last " id="step4-header">
                     <div>4</div>
                     <p class="text-center colol-black">Payment</p>
                  </div>

              </div>
          </div>

          <!-- Transporter -->
          @if(count($transporter_data) > 0)
          <div class="col-sm-10 col-sm-offset-1 box-shadow" >
            <div class="col-sm-2">
              <?php $file_exists = false;?>
              @if(!empty($transporter_data->Image))
              @if(file_exists(BASEURL_FILE.$transporter_data->Image))
              <?php $file_exists = true?>
              <br>
              <img id="senior-preview" src="{{ ImageUrl.$transporter_data->Image}}"  width="100px" height="100px" />
              <br>&nbsp;&nbsp;
              @endif
              @endif
              @if(!$file_exists)
              <br>
              <img id="" src="{{ ImageUrl}}/no-image.jpg"  width="100px" height="100px" />
              <br>&nbsp;&nbsp;
              @endif
            </div>
            <div class="col-sm-10">
              <br>
              <p><b>{{$transporter_data->FirstName}}</b></p>
              <p><b>{{$trip_data->SourceCity}}&nbsp; >> &nbsp; {{$trip_data->DestiCity}}</b></p>
              <?php $average = 0;?>
              @if($transporter_data->RatingCount > 0 && $transporter_data->RatingByCount > 0)
              <?php
                $value = $transporter_data->RatingCount / $transporter_data->RatingByCount;
                $average = $value * 20;
                ?>
              @endif
              <div class="star-ratings-sprite pull-left">
                <span style="width:<?php echo $average ?>%" class="star-ratings-sprite-rating"></span>
              </div>
              <br>
            </div>
          </div>
          @endif
         <!-- End Transporter -->


          <form name="add_item" id="add_item">
          <input type="hidden" name="item_id" id="item_id" value="{{$delivery_data['_id']}}">
          <input type="hidden" name="request_id" id="request_id" value="{{Request::segment(3)}}">
          <div class="row" id="sec3">

             <div class="col-md-10 col-sm-offset-1">

                <div class="col-sm-6 col-xs-6">
                  <div class="form-group">
                    <label class="control-label">Product Title</label>
                    <input class="form-control required  usename-#title#" placeholder="Product Title" name="title" maxlength="100" value="{{$delivery_data['product_name']}}">
                  </div>
                </div>

                <div class="col-sm-6 col-xs-6">
                  <div class="form-group">
                    <label class="control-label">Package Value</label>
                    <div class="input-group error-input">
                    <span class="input-group-addon">$</span>
                    <input class="form-control required float maxlength-7 " placeholder="Package Value" name="package_value" maxlength="9" value="{{$delivery_data['productCost']}}" >
                  </div>
                </div>
              </div>



                <div class="col-sm-12 col-xs-12">
                <br>
                   <div class="form-group">
                      <label class="control-label">Measurement Unit </label>

                      <div class="clearfix">
                          <div class="radio">
                           <label class="">

                            <input type="radio" name="measurement_unit"  value="cm_kg" @if($delivery_data['ProductWeightUnit'] == 'kg') checked @endif onclick="return unit_show();" >

                           </label>&nbsp;Metric (Cm/Kg)
                           <label class="">

                            <input type="radio" name="measurement_unit" value="inches_lbs" @if($delivery_data['ProductWeightUnit'] == 'lbs') checked @endif onclick="return unit_show();">
                           </label>&nbsp;Imperial (Inches/Lbs)
                          </div>

                      </div>
                   </div>
                </div>

                <div class="col-md-3 col-sm-3 col-xs-12">
                   <div class="form-group">
                      <label class="control-label">Length</label>
                      <div class="input-group error-cm-input">
                      <input class="form-control required  float maxlength-9" placeholder="Length" name="length" maxlength="9" value="{{$delivery_data['productLength']}}">
                      <span class="input-group-addon" id="length_unit"></span>
                      </div>
                   </div>
                </div>

                <div class="col-md-3 col-sm-3 col-xs-12">
                   <div class="form-group">
                      <label class="control-label">Width</label>
                      <div class="input-group error-cm-input">
                      <input class="form-control required float maxlength-9" placeholder="Width" name="width" maxlength="9" value="{{$delivery_data['productWidth']}}">
                      <span class="input-group-addon" id="width_unit"></span>
                      </div>
                   </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                   <div class="form-group">
                      <label class="control-label">Height</label>
                      <div class="input-group error-cm-input">
                      <input class="form-control required float maxlength-9" placeholder="Height" name="height" maxlength="9" value="{{$delivery_data['productHeight']}}">
                      <span class="input-group-addon" id="height_unit"></span>
                      </div>
                   </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                   <div class="form-group">
                      <label class="control-label">Weight&nbsp;<a data-toggle="popover" data-trigger="focus"  data-container="body" data-placement="top" type="button" data-html="true" href="javascript:void(0)" id="login1" class="info_icon" style="margin-top:0;color:black;"><img src="{{THEME}}/web/images/info_icon.png" width="20px"></label>
                      <div class="input-group error-cm-input">
                      <input class="form-control required float maxlength-9" placeholder="Weight" name="weight" maxlength="9" value="{{$delivery_data['productWeight']}}">
                      <span class="input-group-addon" id="weight_unit"></span>
                      </div>
                   </div>
                </div>
                <div class="col-sm-12 col-xs-12">
                       <div class="form-group">

                          <label class="control-label">Quantity</label>


                            <div class="radio">
                                <label class="">
                                    <input type="radio" name="quantity_type" value="single" onclick="show_quantity();" @if($delivery_data['QuantityStatus'] == 'single') checked @endif>
                                  </label>&nbsp;Single
                                <label class="">
                                  <input type="radio" name="quantity_type" value="multiple" onclick="show_quantity();" @if($delivery_data['QuantityStatus'] == 'multiple') checked @endif>
                                </label>&nbsp;Multiple
                            </div>


                       </div>
                    </div>
                <div class="col-sm-12 col-xs-12" id="quantity_input">
                      <div class="form-group">
                  <input type="text" class="form-control required numeric" name="quantity" placeholder="Quantity" value="{{$delivery_data['productQty']}}">
                  </div>
                </div>


                  @if(count($transporter_data) > 0)
                  <div class="col-sm-12 col-xs-12">
                      <div class="form-group">
                      <input type="hidden" name="travel_mode" value="{{$trip_data->TravelMode}}" >
                      <label class="control-label">Shipping Mode: </label>&nbsp;&nbsp;{{ucfirst($trip_data->TravelMode)}}
                    </div>
                  </div>

                  @else


                  <div class="col-sm-12">
                      <div class="form-group">
                        <label class="control-label">Shipping Mode</label> &nbsp;&nbsp;(Air: receive package within 5 to 10 business days. Sea: receive package within 6 to 8 weeks)
                            <div class="clearfix">
                               <label class="radio-inline">
                                <span class="pull-left"><input type="radio" name="travel_mode" value="air" onclick="toggle_category('.travel-mode-air','.travel-mode-ship','#package_category')" checked="checked" <?php if ($delivery_data['travelMode'] == "air") {echo 'checked=="checked"';}?> ></span> <span class="Insurance_check">  By Air </span>
                               </label>
                               <label class="radio-inline">
                                <span class="pull-left"><input type="radio" name="travel_mode" value="ship" onclick="toggle_category('.travel-mode-ship','.travel-mode-air','#package_category')" <?php if ($delivery_data['travelMode'] == "ship") {echo 'checked=="checked"';}?>>
                                  </span> <span class="Insurance_check"> By Sea  </span>
                               </label>
                            </div>
                         </div>
                    </div>
                  @endif

                  @if(count($transporter_data) > 0)
                    <div class="col-sm-12 col-xs-12">
                       <div class="form-group">
                          <label class="control-label">Select Package Category</label>
                          <select class="form-control required" id="package_category" name="category">
                             <option value="">Select Category</option>

                             <?php foreach ($category2 as $key) {?>
                              <option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}"}' class="travel-mode-air">{{$key->Content}}</option>
                             <?php }?>


                          </select>
                       </div>
                    </div>

                @else
 <!-- <?php //echo  strtolower($delivery_data['productCategory']); die;?> -->
                    <div class="col-sm-12 col-xs-12">
                      <div class="form-group">
                          <label class="control-label">Select Package Category</label>
                          <select class="form-control required" id="package_category" name="category">
                             <option value="">Select Category</option>
                             <?php foreach ($category as $key) {?>
                              <option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}"}'  class="travel-mode-{{$key->TravelMode}}" <?php if (in_array(strtolower($key->Content), [strtolower($delivery_data['productCategory'])])) {echo 'selected="selected"';}?> >{{$key->Content}}
                              </option>
                             <?php }?>
                          </select>
                      </div>
                    </div>

                @endif

                <div class="col-sm-12 col-xs-12">
                   <div class="form-group">
                      <label class="control-label">Package Content/Description/Instructions</label>
                      <textarea rows="3" class="form-control" name="description" value="">{{$delivery_data['Description']}}</textarea>
                   </div>
                </div>
               <!-- <div class="col-sm-6">
                   <div class="form-group">
                      <label class="control-label">Package care note</label>
                      <textarea rows="3" class="form-control required" name="care_note"></textarea>
                   </div>
                </div>-->
                <div class="col-sm-12">
                  <label class="control-label">Upload Default Package Image</label>
                  <div class="selected-pic"> <div>
                         @if($delivery_data['ProductImage'] != '')
                           @if(file_exists(BASEURL_FILE.$delivery_data['ProductImage']))
                            <a class="fancybox" rel="group" href="{{ ImageUrl.$delivery_data['ProductImage']}}" >
                           <img id="senior-preview" src="{{ ImageUrl.$delivery_data['ProductImage']}}"  width="200px" height="150px" />
                           </a>
                           @else
                           <img id="senior-preview" src="{{ ImageUrl}}/no-image.jpg"  width="200px" height="150px" />
                           @endif
                         @endif
                  </div>
                    <label class="custom-input-file">
                      {!! Form::file('default_image', ['class'=> 'custom-input-file','id'=>'senior_image','onchange'=>"image_preview(this,'senior-preview',event)"]) !!}
                    </label>
                </div>
                <!-- <div class="col-sm-12 col-xs-12">
                   <div class="form-group">
                      <label class="control-label">Upload Extra Image(s)(Optional)</label>
                      <div class="row">
                        <div class="col-sm-3 col-xs-12">
                          <input type="file" placeholder="Browse" name="package_image2" class="valid-filetype-jpg,png,gif,jpeg,JPEG,JPG,PNG,GIF">
                        </div>
                        <div class="col-sm-3 col-xs-12">
                          <input type="file" placeholder="Browse" name="package_image3" class="valid-filetype-jpg,png,gif,jpeg,JPEG,JPG,PNG,GIF">
                        </div>
                        <div class="col-sm-3 col-xs-12">
                          <input type="file" placeholder="Browse" name="package_image4" class="valid-filetype-jpg,png,gif,jpeg,JPEG,JPG,PNG,GIF">
                        </div>
                        <div class="col-sm-3 col-xs-12">
                          <input type="file" placeholder="Browse" name="package_image5" class="valid-filetype-jpg,png,gif,jpeg,JPEG,JPG,PNG,GIF">
                        </div>
                    </div>
                   </div>
                </div> -->



                <div class="col-sm-12">
                       <div class="form-group">
                          <label class="control-label">Insurance</label>
                          <div class="checkbox">
                             <label>
                              <span class="pull-left row"><input type="radio" name="insurance" value="yes" <?php if ($delivery_data['InsuranceStatus'] == "yes") {echo 'checked=="checked"';}?> ></span> <span class="Insurance_check"> Yes </span>
                             </label>
                             &nbsp; &nbsp;
                             <label>
                              <span class="pull-left"><input data-toggle="modal" data-target="#insurance_change" type="radio" name="insurance" value="no" <?php if ($delivery_data['InsuranceStatus'] == "no") {echo 'checked=="checked"';}?> ></span> <span class="Insurance_check"> No </span>
                             </label>
                          </div>
                       </div>
                       <div class="form-group">
                          <div class="checkbox">
                             <label>
                              <span class="pull-left"><input type="checkbox" value="yes" name="need_package_material" <?php if ($delivery_data['PackageMaterial'] == "yes") {echo 'checked=="checked"';}?>></span> <span class="Insurance_check"> Need package material </span>
                             </label>
                          </div>
                       </div>

                        <div class="form-group">
                          <div class="checkbox">
                             <!-- <label>
                              <span class="pull-left"><input type="checkbox" id="agree" name="terms_conditions"></span> <span class="Insurance_check"> I Accept the  <a href="{{url('terms-and-conditions')}}" target="_blank"> Terms and Conditions </a> .</span>
                             </label> -->
                          </div>
                       </div>
                    </div>
                <div class="col-md-12 col-xs-12">
                   <hr>
                </div>
                <div class="col-sm-8 col-xs-12">
                  <div class="row">

                    <div class="col-xs-12 col-xs-12">
                       <div class="form-group">
                          

                          <a class="custom-btn1 btn text-center" href="{{url('edit-request')}}/{{Request::segment(3)}}">
                             Back
                            <div class="custom-btn-h"></div>
                          </a>

                          <button class="custom-btn1 btn" id="calculate_loader">
                             Next
                             <div class="custom-btn-h"></div>
                          </button>

                       </div>

                  </div>
                </div>
                </div>
             </div>
          </div>
          </form>

        </div>
      </div>
  </div>
</div>


<div id="popover-content-login1" class="hide" >
  <h4>Suggested item weights</h4>
  <small>These are suggested weights and may not reflect the actual weight of your item</small>
  <table class="table table-bordered table-striped">
    <thead>
      <tr>
        <th width="150">Item Name</th>
        <th width="60">Weight/Unit</th>
      </tr>
    </thead>
    <tbody>
      
      @if(count($item) > 0)
      @foreach($item as $key)
      <tr>
        <td>{{ucfirst($key->item_name)}} </td>
        <td >{{number_format($key->lbsWeight,2)}} Lbs / {{number_format($key->kgWeight,2)}} Kg</td>
      </tr>
      @endforeach
      @endif
    </tbody>
  </table>
</div>
<div class="modal fade" id="insurance_change" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title"> Alert</h5>
            <a href="javascript::void(0)" class="close" data-dismiss="modal" aria-label="Close" onclick="alertClose()"> <span aria-hidden="true">&times;</span>
            </a>
          </div>
          <input type="hidden" value="{{session()->get('alert_status')}}" id="alertMsg">
          <div class="modal-body">By declining insurance, you risk not getting the full item value of your item in the event of a damage or loss</div>

      </div>
    </div>
</div>
@endsection

@section('script')

@parent
{!! Html::script('theme/web/js/utility.js') !!}
{!! Html::script('theme/web/js/validation.js') !!}
@endsection

@section('inline-script')
@parent
<script type="text/javascript">

function unit_show()
{
  var unit =$('input[name=measurement_unit]:checked').val();

  if(unit == 'cm_kg'){
    $('#length_unit').html('cm');
    $('#width_unit').html('cm');
    $('#height_unit').html('cm');
    $('#weight_unit').html('Kg');

  }else{
    $('#length_unit').html('Inches');
    $('#width_unit').html('Inches');
    $('#height_unit').html('Inches');
    $('#weight_unit').html('Lbs');
  }

}

unit_show();

new Validate({
    FormName : 'add_item',
    ErrorLevel : 1,
    validateHidden: false,
    callback : function(){
      $("#calculate_loader").addClass('spinning');
      $.ajax({
          url: SITEURL+"edit-item-exist-request",
          method: 'post',
          data: new FormData(document.getElementById('add_item')),
          processData: false,
          contentType: false,
          dataType: "json",

          success: function(res){
            if(res.success == 1){
              alert(res.msg);
              document.location.href = SITEURL + 'edit-request/'+'{{Request::segment(3)}}';
            }else{
              alert(res.msg);
            }
            $("#calculate_loader").removeClass('spinning');
          }
      });
    }

});

function toggle_category(showid, hideid, id) {
  $(id).val('');
  $(showid).attr('disabled', false);
  $(showid).show();
  $(hideid).attr('disabled', true);
  $(hideid).hide();
}

@if(@$trip_data->TravelMode == '')

  @if($delivery_data['travelMode'] == "air")
    toggle_category('.travel-mode-air','.travel-mode-ship','#package_category');
  @else
    toggle_category('.travel-mode-ship','.travel-mode-air','#package_category');
  @endif

@else
  @if($trip_data->TravelMode == "air")
    toggle_category('.travel-mode-air','.travel-mode-ship','#package_category');
  @else
    toggle_category('.travel-mode-ship','.travel-mode-air','#package_category');
  @endif
@endif


$('#package_category').val('{"id":"{{$delivery_data['productCategoryId']}}","name":"{{$delivery_data['productCategory']}}"}');
function show_quantity()
{
  var type =$('input[name=quantity_type]:checked').val();
  if(type == 'multiple'){
    $('#quantity_input').show();
  }else{
    $('#quantity_input').hide();
  }

}
show_quantity();
// for selected true for package category
/*$('#package_category').find('option[selected="true"]').each(function(){
    $(this).prop('selected', true);
});*/

function image_preview(obj,previewid,evt)
{
    var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
    if ($.inArray($(obj).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
       $(obj).val('');
       $('#'+previewid).attr('src', '{{ImageUrl}}/user-no-image.jpg');
        alert("Only "+fileExtension.join(', ')+" formats are allowed.");
     } else {

       var file = evt.target.files[0];
       if (file)
       {
         var reader = new FileReader();

         reader.onload = function (e) {
             $('#'+previewid).attr('src', e.target.result)
         };
         reader.readAsDataURL(file);
       }
     }
}

$("[data-toggle=popover]").each(function(i, obj) {
$(this).popover({
html: true,
content: function() {
var id = $(this).attr('id')
return $('#popover-content-' + id).html();
}
});
});

</script>
@endsection
