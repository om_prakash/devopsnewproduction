@extends('Page::layout.one-column-page')
@section('content')
<div class="container">
    <h2 class="color-blue mainHeading">Send a Package Detail fdgdfgd <span class="pull-right">
		<a href="http://192.168.1.101/aq/dev3/my-request" class="btn btn-primary" style="color:#FFFFFF;">Back</a></span></h2>
    <br>
    <div class="clearfix"></div>
    <div class="white_block box-shadow p-space">
        <div class="col">
            <p><b>Package Id :&nbsp;</b>295315170556090</p>
            <p><b>Created On:&nbsp;</b> Wednesday Nov 08, 2018</p>
        </div>
        <div class="col">
            <p><b>Total Number of items: </b>123</p>
            <p><b>Delivery Type: </b>On Way</p>
        </div>
        <div class="col">
            <p><b>Shipping Mode: </b> Air </p>
            <small>(Not all items can be shipped by air, Items shipped by sea typically take longer to arrive)</small>
            <a href="#"  class="anchor-blue">View More</a>
        </div>
    </div>
    <div class="box-shadow">
        <div class="box-header">
            Asd, test
        </div>
        <div class="row p-space p-10">
            <div class="col-sm-4 col-xs-12">
                <p><b>Item Id :</b>295315170556090</p>
                <p><b>Shipping Mode:</b> Wednesday Nov 08, 2018</p>
                <p><b>Description:</b> Lorem Ipsum</p>
            </div>
            <div class="col-sm-4 col-xs-12">
                <p><b>Item Status: </b> Ready</p>
                <p><b>Item Value: </b>$500</p>
                <p><b>Item Category: </b>Other</p>
            </div>
            <div class="col-sm-4 col-xs-12">
                <div class="media">
                <div class="form-group media-left">
                    <a class="fancybox" rel="group" href="http://apis.aquantuo.com/upload/profile/283441493472599.jpeg">
                    <img src="http://apis.aquantuo.com/upload/profile/283441493472599.jpeg" class="img-circle" width="100px" height="100px">
                    </a>
                </div>
                <div class="media-body">
                    <br>
                    <p><b>Requester Name: </b>Aakash Prajapathi</p>
                    <div class="col-sm-12">
                    <div class="star-ratings-sprite pull-left">
                    <span style="width:60%" class="star-ratings-sprite-rating"></span>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <div class="clearfix"></div>
        <br>
        <div class="col-sm-12 col-xs-12">
        <div class="table-responsive"><table class="custom-table table table-bordered">
                  <thead>
                     <tr>
                        <th>Image</th>
                        <th>Dimentions</th>
                        <th>Verification Code</th>
                        <th>Insurance</th>
                        <th>Quantity</th>
                        <th>Item Price</th>
                        <th>Shipping Cost</th>
                        <th>Need Package Material</th>
                     </tr>
                  </thead>
                  <tbody>
                     <tr>
                        <td><img src="http://apis.aquantuo.com/upload/profile/283441493472599.jpeg" class="" width="60px" height="60px"></td>
                        <td>
                        <p>L-100 Inch</p>
                        <p>H-100 Inch</p>
                        <p>W-100 Inch</p>
                        <p>Weight-100 Inch</p>
                        </td>
                        <td>989595</td>
                        <td>No</td>
                        <td>1</td>
                        <td>$100</td>
                        <td>$56.00</td>
                        <td>N/A</td>
                     </tr>
                  </tbody>
               </table>
               </div>
        </div>
    </div>

        </div>

    <div class="row">
        <div class="col-sm-6 col-xs-12"></div>
        <div class="col-sm-6 col-xs-12">
            <div class="">
            <table class="table table-bordered">
                     <tbody bgcolor="#FFFFFF">
                        <tr>
                        <td class="">
                          <div class="col-xs-8 text-left">
                            <label class="" for="exampleInputEmail1"><b>Shipping Cost:</b> </label>
                          </div>
                          <div class="col-xs-4 text-right"> $48.20</div>
                          </td>
                      </tr>
                      <tr>
                        <td class="">
                          <div class="col-xs-8 text-left">
                            <label class="" for="exampleInputEmail1"><b>Aquantuo Discount:</b> </label>
                          </div>
                          <div class="col-xs-4 text-right">
                              $0.00
                          </div>
                          </td>
                      </tr>

                      <tr>
                        <td class="">
                          <div class="col-xs-8 text-left">
                            <label class="" for="exampleInputEmail1"><b>Discount: (-)</b> </label>
                          </div>
                          <div class="col-xs-4 text-right">
                            $2
                          </div>
                        </td>
                      </tr>
                      <tr style="background:#eee;">
                        <td>
                          <div class="col-xs-8 text-left">
                            <label class="" for="exampleInputEmail1"><b>Total Cost</b> </label>
                          </div>
                          <div class="col-xs-4 text-right">
                            $28
                          </div>
                        </td>
                      </tr>
                     </tbody>
                  </table>
            </div>
            <div class="text-center">
            <button class="btn btn-default blue-btn" title="Out for picup" id="b_pickup" onclick="return confirm('Are you sure you are going to pickup the package?')? buy_for_me('pickup'):'';">
               &nbsp; View Receipt &nbsp;&nbsp;</button>

            </div>
            <br>
            <br>
        </div>
    </div>
</div>



@endsection