
@extends('Page::layout.one-column-page')
@section('bg-color')

@endsection
@section('content')
{!! Html::script('theme/web/js/validations.js') !!}
<style>
.wrapper {
  background: rgba(0, 0, 0, 0) url("theme/web/promo/images/slider_img_01.jpg") repeat fixed 0 0 / cover ;
}
.wrapper.clearfix {
  padding-top: 80px;
  margin-top:0px;
}
@media screen and (max-width: 768px) {
	.wrapper.clearfix {
		padding-top: 50px;
		margin-top:15px;
}
}
</style>
<div class="row">
      <div id="" class="container">
         <div class="col-sm-7 hidden-sm hidden-xs">
            <h2 class="color-blue">What is new in Aquantuo</h2>
            <ul class="features-ul">
               <li>-   Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
               <li>-   or sit amet, consg elit.</li>
               <li>-   Lorem ipsumt amet, ctetur adipiscing elit.</li>
               <li>-    ipsum dolor sit amet, consectetur adipiscing elit.</li>
               <li>-   Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
               <li>-   or sit amet, consg elit.</li>
               <li>-   Lorem ipsumt amet, ctetur adipiscing elit.</li>
               <li>-    ipsum dolor sit amet, consectetur adipiscing elit.</li>
               <li>-   Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
               <li>-   or sit amet, consg elit.</li>
               <li>-   Lorem ipsumt amet, ctetur adipiscing elit.</li>
               <li>-    ipsum dolor sit amet, consectetur adipiscing elit.</li>
            </ul>
         </div>
         <div class="col-md-5 login-bg pull-right ">
            <div class="heading-blue">
              Join
            </div>
           
            {!! Form::open(array('url' => 'signup' , 'files' => 'true', 'id' => 'signup' , 'class' => 'form-vertical label-less business') ) !!}
            <?php   if(Input::get('type') == 'transporter') { ?>
               <div class="col-sm-12 ">
                  <div class="form-group clearfix">
                     <div class="my-tab clearfix">
                        <div class="sliding-div" id="ac_type_bg"></div>
                        <div class="tab1" id="individual" onclick ="transporter_type('individual')">Individual<span> Transporter</span></div>
                        <div class="tab2" id="transporter" onclick="transporter_type('business')" style="color:#fff;">Business<span> Transporter</span></div>
                        <input type="hidden" name ='transportertype' value='individual' id='transportertype' >
                     </div>
                  </div>
               </div>
            <?php }  ?>
            <div class="col-sm-6" id="first_name">
               <div class="form-group">
                  {!! Form::text('firstName', '',['class'=>'form-control', 'placeholder'=> 'First Name' ,'maxlength' => 20]) !!}
                  <p class="help-block red" id='er_firstName' style="color:red">
                     @if($errors->has('firstName')){{ $errors->first('firstName') }}@endif
                  </p>
               </div>
            </div>
            <div class="col-sm-6" id="last_name">
               <div class="form-group">
                  {!! Form::text('lastName', '',['class'=>'form-control', 'placeholder'=> 'Last Name','maxlength' => 20]) !!} 
                  <p class="help-block red" id='er_lastName' style="color:red">
                     @if($errors->has('lastName')){{ $errors->first('lastName') }}@endif
                  </p>
               </div>
            </div>
             @if(Request::segment(1) != 'signup')
            <div class="col-sm-12" id="business_name" style="display:none;" >
               <div class="form-group">
                  {!! Form::text('business_name', '',['class'=>'form-control', 'placeholder'=> 'Business Name','maxlength' => 20]) !!} 
                  <p class="help-block red" id='er_business_name' style="color:red">
                     @if($errors->has('business_name')){{ $errors->first('business_name') }}@endif
                  </p>
               </div>
            </div>
           @endif  

            <div class="col-sm-12">
               <div class="form-group">
                  {!! Form::text('email', '',['class'=>'form-control', 'placeholder'=> 'Email','id'=>'email','maxlength' =>60]) !!} 
                  <p class="help-block red" id='er_email' style="color:red">
                     @if($errors->has('email')){{ $errors->first('email') }}@endif
                  </p>
               </div>
            </div>
            <div class="col-sm-12">
               <div class="form-group">
                  {!! Form::password('password',['class'=>'form-control', 'placeholder'=> 'Password','maxlength' =>80]) !!}  
                  <p class="help-block red" id='er_password' style="color:red">
                     @if($errors->has('password')){{ $errors->first('password') }}@endif
                  </p>
               </div>
            </div>
            <div class="col-sm-12">
               <div class="form-group">
                  {!! Form::password('confirmPassword',['class'=>'form-control', 'placeholder'=> 'Confirm Password','maxlength' =>15]) !!}  
                  <p class="help-block red" id='er_confirmPassword' style="color:red">
                     @if($errors->has('confirmPassword')){{ $errors->first('confirmPassword') }}@endif
                  </p>
               </div>
            </div>
            <div class="col-sm-12">
               <input type="hidden"  name="usertype"   value="requester">
            </div>
            <div class="col-sm-12">
               <div class="form-group">
                  <!-- <div class="checkbox">
                     <label><input type="checkbox" value="">I also want to move package.</label>
                     </div>  -->
               </div>
            </div>
            <div class="col-sm-12">
               <p class="color-white text-center">By sign up, you agree to Aquantuo’s <a href="{{url('terms-and-conditions')}}" target="_blank">Terms and 
                  Conditions</a> and <a href="{{url('privacy-policy')}}" target="_blank">Privacy Policy</a>
               </p>
            </div>
            <div class="col-sm-12">
               <div class="form-group">
                  <button class="custom-btn1 btn-block " >
                     Signup
                     <div class="custom-btn-h"></div>
                  </button>
               </div>
            </div>
            <div class="clearfix"></div>
            <br />
            {!! Form::close() !!}
         </div>
         <div class="col-md-7 hidden-md hidden-lg">
        <h2 class="color-blue">What is new in Aquantuo</h2>
        <ul class="features-ul">
           <li>-   Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
           <li>-   or sit amet, consg elit.</li>
           <li>-   Lorem ipsumt amet, ctetur adipiscing elit.</li>
           <li>-    ipsum dolor sit amet, consectetur adipiscing elit.</li>
           <li>-   Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
           <li>-   or sit amet, consg elit.</li>
           <li>-   Lorem ipsumt amet, ctetur adipiscing elit.</li>
           <li>-    ipsum dolor sit amet, consectetur adipiscing elit.</li>
           <li>-   Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
           <li>-   or sit amet, consg elit.</li>
           <li>-   Lorem ipsumt amet, ctetur adipiscing elit.</li>
           <li>-    ipsum dolor sit amet, consectetur adipiscing elit.</li>
        </ul>
     </div>
      </div>
</div>
<style>

</style>
<script type="text/javascript">
   function transporter_type(type)
   {
   	if(type == 'individual') {
    
   		$('#ac_type_bg').css('right','50%');
   		$('#transportertype').val('individual');
   	} else {

   		$('#ac_type_bg').css('right','0%');
   		$('#transportertype').val('business');
   	}
   	
   }
   $('#individual').click(function(){
  
 //   $('#first_name').show();
  //  $('#last_name').show();
    $('#business_name').hide();
   // $('#VatTaxNo').hide();
    
  });  

   $('#transporter').click(function(){
  
   // $('#first_name').hide();
   // $('#last_name').hide();
    $('#business_name').show();
  //  $('#VatTaxNo').show();
    
  }); 



new Validate({
    FormName :  'signup',
    ErrorLevel : 1,
 
   });

</script>
@endsection
