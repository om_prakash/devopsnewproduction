@extends('Page::layout.one-column-page')
@section('bg-color')

@endsection
@section('content') 

<style>
.wrapper {
  background: rgba(0, 0, 0, 0) url("theme/web/promo/images/slider_img_01.jpg") repeat fixed 0 0 / cover ;
}
.wrapper.clearfix {
  padding-top: 80px;
  margin-top:0px;
}
@media screen and (max-width: 768px) {
	.wrapper.clearfix {
		padding-top: 50px;
		margin-top:15px;
}
}
</style>

<div class="row">
  <div id="" class="container">
     <div class="col-md-7 col-sm-7 col-xs-12 hidden-sm hidden-xs">
        <h2 class="color-blue">What is new in Aquantuo</h2>
        <ul class="features-ul">
           <li>-   Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
           <li>-   or sit amet, consg elit.</li>
           <li>-   Lorem ipsumt amet, ctetur adipiscing elit.</li>
           <li>-    ipsum dolor sit amet, consectetur adipiscing elit.</li>
           <li>-   Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
           <li>-   or sit amet, consg elit.</li>
           <li>-   Lorem ipsumt amet, ctetur adipiscing elit.</li>
           <li>-    ipsum dolor sit amet, consectetur adipiscing elit.</li>
           <li>-   Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
           <li>-   or sit amet, consg elit.</li>
           <li>-   Lorem ipsumt amet, ctetur adipiscing elit.</li>
           <li>-    ipsum dolor sit amet, consectetur adipiscing elit.</li>
        </ul>
     </div>
     <div class="col-md-5  col-sm-5 col-xs-12 login-bg pull-right ">
        <div class="heading-blue">
           Sign in 
        </div>
       
        {!! Form::open(['name'=>'ServicesForm','id' => 'ServicesForm','url'=> 'login', 'class' => 'form-vertical','method' => 'POST']) !!}
        <div class="col-sm-12">
           <div class="form-group">
              {!! Form::label('Email', 'Email',['class'=>'control-label']) !!}           
              <?php $email = isset($_COOKIE['Email'])? $_COOKIE['Email'] : ''; 
                $password = isset($_COOKIE['password'])? $_COOKIE['password'] : ''; 
              ?>
             <div class="input-img-hldr"> {!! Form::text('email', $email,['class'=>'form-control', 'placeholder'=> 'Email','id'=>'email','maxlength' =>60]) !!} 
              <span><i class="fa fa-envelope input-img"  aria-hidden="true"></i></span> </div>   
              <p class="help-block red" id='er_email' style="color:red">
                 @if($errors->has('email')){{ $errors->first('email') }}@endif
              </p>
           </div>
        </div>
        <div class="col-sm-12">
           <div class="form-group">
              {!! Form::label('password', 'Password',['class'=>'control-label']) !!}           
              <div class="input-img-hldr">
                <input type="password" class="form-control" placeholder='Password' id='password' maxlength="80" value="{{$password}}" name="password">
              <span><i class="fa fa-lock input-img" aria-hidden="true"></i></span>   </div>
              <p class="help-block red" id='er_password' style="color:red">
                 @if ($errors->has('password')){{ $errors->first('password') }}@endif
              </p>
           </div>
        </div>
        <div class="col-sm-12">
           <div class="form-group">
              <div class="checkbox">
                 <label><input type="checkbox" tabindex="4" style="margin-top:1px;" class="field login-checkbox" name="remember" id="Field" @if(isset($_COOKIE['Email'])) checked @endif >&nbsp;Remember me</label>
              </div>
           </div>
        </div>

        <div class="col-sm-12">
           <div class="form-group">
              <button class="custom-btn1 btn-block">
                 Login
                 <div class="custom-btn-h"></div>
              </button>
           </div>
        </div>
        <div class="clearfix"></div>
        <br />
        <p class="color-blue text-center" ><a href="{{url('forgot-password')}}">Forgot Password?</a></p>
        {!! Form::close() !!}
     </div>
     <div class="col-md-7 hidden-md hidden-lg">
        <h2 class="color-blue">What is new in Aquantuo</h2>
        <ul class="features-ul">
           <li>-   Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
           <li>-   or sit amet, consg elit.</li>
           <li>-   Lorem ipsumt amet, ctetur adipiscing elit.</li>
           <li>-    ipsum dolor sit amet, consectetur adipiscing elit.</li>
           <li>-   Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
           <li>-   or sit amet, consg elit.</li>
           <li>-   Lorem ipsumt amet, ctetur adipiscing elit.</li>
           <li>-    ipsum dolor sit amet, consectetur adipiscing elit.</li>
           <li>-   Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
           <li>-   or sit amet, consg elit.</li>
           <li>-   Lorem ipsumt amet, ctetur adipiscing elit.</li>
           <li>-    ipsum dolor sit amet, consectetur adipiscing elit.</li>
        </ul>
     </div>
  </div>
</div>

@endsection
