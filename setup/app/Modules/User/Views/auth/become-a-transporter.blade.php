@extends('page::layout.one-column-page')
@section('content')
<style>
.wrapper {
  background: rgba(0, 0, 0, 0) url("theme/web/promo/images/slider_img_01.jpg") repeat fixed 0 0 / cover ;
}
.wrapper.clearfix {
  padding-top: 80px;
  margin-top:0px;
}
@media screen and (max-width: 768px) {
	.wrapper.clearfix {
		padding-top: 50px;
		margin-top:15px;
}
}
</style>
<div class="row">
<!--parallax 1 -->

<div id="" class="container">
<div class="row">
<div class="col-sm-7 hidden-sm hidden-xs">
<h2 class="color-blue">What is new in Aquantuo</h2>
<ul class="features-ul">
<li>-   Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
<li>-   or sit amet, consg elit.</li>
<li>-   Lorem ipsumt amet, ctetur adipiscing elit.</li>
<li>-    ipsum dolor sit amet, consectetur adipiscing elit.</li>
<li>-   Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
<li>-   or sit amet, consg elit.</li>
<li>-   Lorem ipsumt amet, ctetur adipiscing elit.</li>
<li>-    ipsum dolor sit amet, consectetur adipiscing elit.</li>
<li>-   Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
<li>-   or sit amet, consg elit.</li>
<li>-   Lorem ipsumt amet, ctetur adipiscing elit.</li>
<li>-    ipsum dolor sit amet, consectetur adipiscing elit.</li>
</ul>
</div>
<div class="col-md-5 login-bg pull-right ">
<div class="heading-blue">
Signin to Aqunatuo
</div>
<p>Users Aquantuo are real people with package or people willing 
to help move packages, that is why we ask everyone to verify 
a few things to activate their account</p>
<div class="col-sm-12">
    <div class="custom-tab clearfix">
        <div class="tab1 selected">INDIVIDUAL<span> TRANSPORTER</span></div>
        <div class="tab2">BUSINESS<span> TRANSPORTER</span></div>
    </div>
</div>
<!--INDIVIDUAL TRANSPORTER-->
<form class="form-vertical label-less individual">
<div class="col-sm-6">
<div class="form-group">

<input placeholder="First Name" class="form-control">
</div>
</div>
<div class="col-sm-6">
<div class="form-group">
<input placeholder="Last Name" class="form-control">
</div>
</div>
<div class="col-sm-12">
<div class="form-group">
<input placeholder="Email" class="form-control">
</div>
</div>

<div class="col-sm-12">
<div class="form-group">
<input placeholder="Password" class="form-control">
</div>
</div>

<div class="col-sm-12">
<div class="form-group">
<input placeholder="Confirm Password" class="form-control">
</div>
</div>

<div class="col-sm-12">
<div class="form-group">
<div class="checkbox">
<label><input type="checkbox" value="">I also want to move package.</label>
</div>
</div>
</div>
<div class="col-sm-12">
<p class="color-white text-center">By sign up, you agree to Aquantuo’s <a href="#">Terms and 
Conditions</a> and <a href="#">Privacy Policy</a></p>
</div>

<div class="col-sm-12">
<div class="form-group">
<button class="custom-btn1 btn-block">
Signup
<div class="custom-btn-h"></div>
</button>
</div>
</div>
<div class="clearfix"></div>
<br />
<p class="color-blue text-center">Forgot Password?</p>


</form>
<!--INDIVIDUAL TRANSPORTER-->


<!--BUSINESS TRANSPORTER-->
<form class="form-vertical label-less business">
<div class="col-sm-6">
<div class="form-group">

<input placeholder="First Name" class="form-control">
</div>
</div>
<div class="col-sm-6">
<div class="form-group">
<input placeholder="Last Name" class="form-control">
</div>
</div>
<div class="col-sm-12">
<div class="form-group">
<input placeholder="Email" class="form-control">
</div>
</div>

<div class="col-sm-12">
<div class="form-group">
<input placeholder="Password" class="form-control">
</div>
</div>

<div class="col-sm-12">
<div class="form-group">
<input placeholder="Confirm Password" class="form-control">
</div>
</div>

<div class="col-sm-12">
<div class="form-group">
<div class="checkbox">
<label><input type="checkbox" value="">I also want to move package.</label>
</div>
</div>
</div>
<div class="col-sm-12">
<p class="color-white text-center">By sign up, you agree to Aquantuo’s <a href="#">Terms and 
Conditions</a> and <a href="#">Privacy Policy</a></p>
</div>

<div class="col-sm-12">
<div class="form-group">
<button class="custom-btn1 btn-block">
Signup
<div class="custom-btn-h"></div>
</button>
</div>
</div>
<div class="clearfix"></div>
<br />
<p class="color-blue text-center">Forgot Password?</p>



</form>
<!--BUSINESS TRANSPORTER-->
</div>
<div class="col-md-7 hidden-md hidden-lg">
        <h2 class="color-blue">What is new in Aquantuo</h2>
        <ul class="features-ul">
           <li>-   Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
           <li>-   or sit amet, consg elit.</li>
           <li>-   Lorem ipsumt amet, ctetur adipiscing elit.</li>
           <li>-    ipsum dolor sit amet, consectetur adipiscing elit.</li>
           <li>-   Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
           <li>-   or sit amet, consg elit.</li>
           <li>-   Lorem ipsumt amet, ctetur adipiscing elit.</li>
           <li>-    ipsum dolor sit amet, consectetur adipiscing elit.</li>
           <li>-   Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
           <li>-   or sit amet, consg elit.</li>
           <li>-   Lorem ipsumt amet, ctetur adipiscing elit.</li>
           <li>-    ipsum dolor sit amet, consectetur adipiscing elit.</li>
        </ul>
     </div>
</div>
</div>
</div>

@endsection


