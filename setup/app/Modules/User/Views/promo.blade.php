<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      
      <meta name="viewport" content="width=device-width, initial-scale=1 user-scalable=no"> 
      <meta name="description" content="Shop US stores and have it delivered to you in Ghana. Earn money on your next trip to Ghana. List your item on Aquantuo and get it in Ghana or the US"> 
      <meta name="keywords" content="Shipping to Ghana, , ship to Ghana, Shop US stores and deliver to Ghana, Buy from US stores, shop US Stores, send to Ghana, traveling to Ghana, traveling to the US, Ghana Shipping, peer to peer shipping, uber shipping, Ghana freight, send items to Ghana, find a traveler, traveling to Ghana, Ghana shipping, send items home, get paid to travel, akwantuo, akwantu, send money to Ghana, send items overseas, container shipping"> 
      <meta name="author" content="Aquantuo/Akwantuo"> 
      <meta http-equiv="refresh" content="3000"> 

	  <link rel="shortcut icon" type="image/png" href="theme/web/promo/images/favicon.png" />

      <title>Aquantuo</title>
      <link href="theme/web/promo/images/favicon.png" type="image/png" rel="icon">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
      <script>
         // site preloader -- also uncomment the div in the header and the css style for #preloader
         $(document).ready(function(){
         //$('#preloader').fadeOut('fast',function(){$(this).remove();});
         //$("#phone1").css('transform','translateX(0px)')
         $('#phone1').addClass('slide');
         });
      </script>
      <!-- Bootstrap -->
      {!! Html::style('theme/web/promo/css/bootstrap.min.css') !!}
      {!! Html::script('theme/web/js/validation.js') !!}
      {!! Html::style('theme/web/promo/css/style.min.css') !!}
      {!! Html::style('theme/web/promo/css/font-awesome.css') !!}
      <!-- <link href="css/bootstrap.min.css" rel="stylesheet">
         <link href="css/style.css" rel="stylesheet">
         <link href="css/font-awesome.css" rel="stylesheet"> -->
      <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
   </head>
   <body>
      <!--<div id="preloader"></div>-->
      <!-- Modal -->
      
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="myModalLabel">Join Newsletter</h4>
               </div>
               <div class="modal-body clearfix">
               <span id="form_subscribe">
                  <div class="col-md-12">
                     <p>Subscribe to our newsletter to be notified when Aquantuo is available in your Country or State/Region.</p>
                     <div class="form-group">
                        <label for="fname">Name</label>
                        <input class="form-control" placeholder="Name" id="subscribe_name">
                        <span id="err_subscribe_name" style="color:red"></span>
                     </div>
                     <div class="form-group">
                        <label for="fname">Phone no.</label>
                        <input class="form-control" placeholder="Phone no." id="subscribe_phoneNo">
                        <span id="err_subscribe_phoneNo" style="color:red"></span>
                     </div>
                     <div class="form-group">
                        <label for="fname">Email</label>
                        <input class="form-control" placeholder="Email" id="subscribe_email">
                        <span id="err_subscribe_email" style="color:red"></span>
                     </div>
                     <!--<div class="row">
                        <div class="col-md-4 col-sm-4">
                           <div class="form-group">
                              <label for="fname">Select Country</label>
                              <select class="form-control" id="Country" name="Country">
                                 <option value="">Select Country</option>
                                 
                              </select>
                           </div>
                        </div>                       
                     </div>-->
                     <div class="form-group">
                        <label>Message</label>
                        <textarea rows="2" class="form-control" id="subscribe_message"></textarea>
                        <span id="err_subscribe_message" style="color:red"></span>
                     </div>
                     <div class="form-group">
                        <span id="ajx_subscribe">
                        	<div class="custom-btn1" data-toggle="modal" data-target="#myModal">
                           		Subscribe
                           <div class="custom-btn-h"></div>
                        </span>
                        </div>
                     </div>
                  <span>
                  </div>
                  <br>
                  <br>
               </div>
            </div>
         </div>

      </div>
      <!-- Modal -->
      <div class="container-fluid">
         <div class="row slider">

               <nav class="navbar navbar-default nav-bg responsive">
                  <div class="container">
                     <!-- Brand and toggle get grouped for better mobile display -->
                     <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">
                        {!! Html::image('theme/web/promo/images/logo.png')!!}
                        </a>
                     </div>
                     <!-- Collect the nav links, forms, and other content for toggling -->
                     <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                     <div class="row">
                        <ul class="nav navbar-nav navbar-right">
                           <li><a href="#home">Home</a>
                           </li>
                           
                           <li><a href="#aboutapp">How it works</a>
                           </li>
                            <li id="add-class" class="">
								<a href="{{url('price-estimator')}}" >Price Estimator</a>
						   </li>

<!--
                           <li><a href="#features">Features</a>
                           </li>

                           <li>
							   <a href="#place-an-order" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo" onclick="reset_form('placeOrder');">Place an Order</a>
                           </li>
                           <li><a href="#download">Download</a>
                           </li>
                           <li><a href="#contact">Contact</a>
                           </li>
-->
                <?php if(Session::get( 'UserId') !='' ) { ?>
               <?php if(Session::get( 'UserId') != '' ) {   ?>
             <ul class="nav navbar-nav pull-right dropdown downarrow">
          <li class="nav-item avatar"> 
          
             <?php if(Session::get('Image') != '') {  ?>
                  <img src="{{ImageUrl.Session::get('Image')}}" class="img-fluid"  width="40" height="40"> 
                <?php } else {  ?>
                  <img src="{{ImageUrl}}/user-no-image.jpg" class="img-fluid"  width="40" height="40"> 
              <?php } ?>
          
          </li>
          <li class="nav-item storeName  waves-effect waves-light" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <?php  echo Session::get('FirstName'); echo " ";
                  echo Session::get('LastName');   ?>
                
          </li>
         <div class="dropdown-menu dropdown-info logoutbar" aria-labelledby="dropdownMenu3" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
            <a class="dropdown-item waves-effect waves-light" href="{{url('logout')}}">Log Out</a>
        </div>
        <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="{{url('my-profile')}}">My Profile</a></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1"  href="{{url('statistics')}}">Statistics</a></li>

                    <li role="presentation"><a role="menuitem" tabindex="-1" href="{{url('change-password')}}">Change Password</a>
                    </li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="{{url('support')}}">Support</a>
                    </li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="{{url('logout')}}">Logout</a>
                    </li>
                  </ul>
                  </li>
        </ul>
        <?php } ?>            
               <?php  } else { ?>

               <li class="dropdown">
    <a class="dropdown-toggle" type="button" id="menu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Account
    <span class="caret"></span></a>
    <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
      <li><a href="{{url('signup')}}">Sign up</a></li>
      <li><a href="{{url('login')}}">Sign in</a></li>
                  <?php }  ?>
    </ul>
      <?php if(Session::get( 'UserId') == '' ) {   ?>
     <li><a href="{{url('transporter-signup')}}?type=transporter">Become a Transporter</a></li> 
      <?php } ?>
  </li>                      
                        </ul>
                     </div>
                     </div>
                     <!-- /.navbar-collapse -->
                  </div>
                  <!-- /.container-fluid -->
               </nav>
               <div id="home" class="container">
                  <div class="row landing-sec">
                     <div id="phone1" class="col-md-5 col-sm-5">
                        {!! Html::image('theme/web/promo/images/iphone1.png','', array('width' => 350, 'class' => 'responsive-img'))!!}
                        <!-- <img class="responsive-img" width="350" src="images/iphone1.png"> -->
                        
                     </div>
                     <div class="col-md-7 col-sm-7">
                        <div class="heading1 color-white">What is Aquantuo?</div>
                        <p class="landing-txt color-white">A growing trustworthy community of Transporters and 	Requesters, Aquantuo bridges the gap between the person wanting to have an item delivered to another location and the person or company willing to do so. 
                        </p>
                        <p class="landing-txt color-white">
                           From the laptop that was left behind to the car that you need to move across the country or across the ocean, list it, and your family at Aquantuo will help get it there. Affordable, faster and safer.
                        </p>
                        <p class="landing-txt color-white">
                           Sign up is free and easy with our free app.
                        </p>
                        </p>
                        <div class="play-store-btn">
                           <div class="gplay">
                              <a href="https://play.google.com/store/apps/details?id=com.it.aquantuo&hl=en" target="_blank">
                                 {!! Html::image('theme/web/promo/images/play_store_btn.png') !!}
                              </a>
                           </div>
                           <div class="appstore">
                              <a href="https://itunes.apple.com/us/app/aquantuo/id1067797285?ls=1&mt=8" target="_blank">
                                 {!! Html::image('theme/web/promo/images/app_store_btn.png') !!}
                              </a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>

         </div>
         <div class="row" id="aboutapp">
            <div class="container-fluid section-custom">
               <div class="container">
                  <div class="row">
                     <div class="col-md-12 col-sm-12">
                        <div class="heading2 text-center">How Aquantuo Works</div>
                        <p class="text-center">Aquantuo is a trusted community platform for users (Requesters) to list items that they would want delivered to another location.
                        </p><p class="text-center">Other users (Transporters),discover these items and help take them to the intended destinations.</p>

                     </div>
                  </div>
                  <div class="row offset-top-btm70">
                     <div class="col-md-4 col-sm-4 circle-holder text-center">
                        <div class="circle-icon">
                           {!! Html::image('theme/web/promo/images/list_in2.png', '', array('class' => 'second-img1')) !!}
                           {!! Html::image('theme/web/promo/images/list_in.png', '', array('class' => 'circle-img')) !!}
                        </div>
                        <h3>List it</h3>
                        <p>On the app, make a request by listing the item you want to have delivered.</p>
                     </div>
                     <div class="col-md-4 col-sm-4 circle-holder text-center">
                        <div class="circle-icon">
                           <!--{!! HTML::image('theme/web/promo/images/arrow.png', '', array('class' => 'second-img2')) !!}-->
                           {!! Html::image('theme/web/promo/images/truck.png', '', array('class' => 'circle-img','style' => 'width:150px;')) !!}
                        </div>
                        <h3>Item is picked up or shipped</h3>
                        <p>A Transporter agrees to deliver your item and you pay for it. You meet the transporter to handover the package or you ship it to the transporter.</p>
                     </div>
                     <div class="col-md-4 col-sm-4 circle-holder text-center">
                        <div class="circle-icon">
                           {!! Html::image('theme/web/promo/images/check.png', '', array('class' => 'second-img3')) !!}
                           {!! Html::image('theme/web/promo/images/box.png', '', array('class' => 'circle-img')) !!}
                        </div>
                        <h3>Item is delivered</h3>
                        <p>Transporter delivers your item with our agent at the airport and we arrange pick up with you, the Requester.</p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="row blue-bg" id="sec3">
            <div class="container-fluid section-custom">
               <div class="container">
                  <div class="row">
                     <div class="col-md-6">
                        <div class="heading2 color-white">We Are Available in</div>
                        <p class="color-white">Aquantuo currently serves all 10 regions of Ghana and all 50 States in the US along with The District of Columbia. Subscribe to our newsletter to be notified when Aquantuo is available in your Country or State/Region.</p>
                        <br>
                        <!--<div class="row">
                           <div class="col-md-4 col-sm-4 col-xs-4">
                              <div class="heading3 color-white">USA States</div>
                              <ul class="state-list">
                                 <li><span>Delaware</span></li>
                                 <li><span>Maryland</span></li>
                                 <li><span>New Jersey</span></li>
                                 <li><span>New York</span></li>
                                 <li><span>Pennsylvania</span></li>
                                 <li><span>Virginia</span></li>
                                 <li><span>Washington, D.C.</span></li>
                              </ul>
                           </div>
                           <div class="col-md-4 col-sm-4 col-xs-4">
                              <div class="heading3 color-white">Ghana Regions</div>
                              <ul class="state-list">
                                 <li><span>Ashanti</span></li>
                                 <li><span>Central</span></li>
                                 <li><span>Greater Accra</span></li>
                                 <li><span>Upper West</span></li>
                                 <li><span>Western</span></li>
                              </ul>
                           </div>
                           <div class="col-md-4 col-sm-4 col-xs-4">
                              <div class="heading3 color-white">&nbsp;</div>
                              <ul class="state-list">
                                 <li><span>Brong Ahafo</span></li>
                                 <li><span>Eastern</span></li>
                                 <li><span>Northern</span></li>
                                 <li><span>Upper East</span></li>
                                 <li><span>Volta</span></li>
                              </ul>
                           </div>
                        </div>-->
                        <br>
                        <span id="ajx_country">
	                        <div class="custom-btn2" data-toggle="modal" data-target="#myModal">
	                           <div>Join Newsletter</div>
	                           <div class="custom-btn-h"></div>
	                        </div>
                        </span>
                     </div>
                     <div id="phone2" class="col-md-6 right-animation">
                        {!! Html::image('theme/web/promo/images/map.png', '', array('class' => 'responsive-img pull-right', 'width' => '100%')) !!}
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div id="features" class="row light-bg">
            <div class="container-fluid section-custom">
               <div class="container">
                  <div class="row">
                     <div class="heading2 text-center">Transporter/Requester</div>
                     <div class="col-md-4 col-sm-4">
                        <div class="heading3 offset-btm-20">Money in your pocket</div>
                        <br>
                        <p>Travel frequently or occasionally? Make money from the extra space in your luggage or pick up a suitcase and make more money from it. Even better, you are helping someone out.</p>
                        <p>Is your business into shipping large items like drums, furniture and automobiles? Could you do with a few more clients? Find Requesters who need these items shipped in your area and offer your services.</p>
                        <br>
                        <ul class="fancy-list">
                           <li>Fast and easy sign up</li>
                           <li>Post in a few minutes</li>
                           <li>Efficient tracking of items</li>
                           <li>Instant on demand communication between Requester and Transporter</li>
                        </ul>
                     </div>
                     <div id="phone3" class="col-md-4 col-sm-4">
                        {!! Html::image('theme/web/promo/images/img2.png', '', array('class' => 'responsive-img','width' => 350)) !!} 
                     </div>
                     <div class="col-md-4 col-sm-4">
                        <div class="heading3 offset-btm-20">Ask no more…list confidently</div>
                        <br>
                        <p>The days of asking: “Who is traveling to…?” or “How can I get this package to…?” are over. Aquantuo takes care of that! Place your listing today and a verified Transporter will get it there. It's affordable, faster and safer. On our platform, you will find Individual and Business Transporters local to your community and much farther who will help get your package(s) to its destination. From the Jimmy Choo shoe to the Mercedes S Class and anything in between, place your listing today and Aquantuo will take care of the rest.</p>
                        <br>
                        <ul class="fancy-list">
                           <li>Trusted community</li>
                           <li>Nearby Requesters and Transporters</li>
                           <li>Easy to navigate platform</li>
                           <li>Affordable pricing </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div id="download" class="row download-bg">
            <div class="container-fluid section-custom">
               <div class="container">
                  <div class="row">
                     <div class="col-md-12 col-sm-12">
                        <div class="heading2 text-center">Download Aquantuo</div>
                        <p class="text-center">Start listing or transporting now by downloading our free app from the App Store or from Google Play. </p>
                        <p class="text-center">Simply search for Aquantuo. You could make money on your next trip.</p>
                        <div class="play-store-btn1">
                           <div class="gplay">
                              <a href="https://play.google.com/store/apps/details?id=com.it.aquantuo&hl=en" target="_blank">
                                 {!! Html::image('theme/web/promo/images/play_store_btn.png') !!}
                              </a>
                           </div>
                           <div class="appstore">
                              <a href="https://itunes.apple.com/us/app/aquantuo/id1067797285?ls=1&mt=8" target="_blank">
                                 {!! Html::image('theme/web/promo/images/app_store_btn.png') !!}
                              </a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
        <div class="row">   
          <div id="canvas1">
              <iframe id="map_canvas1" src="https://www.google.com/maps/d/u/0/embed?mid=zEXCW-LbfQ5g.kdplrdU16yvM" style="border:0" allowfullscreen="" class="scrolloff" width="100%" height="600" frameborder="0"></iframe>
            </div>
        </div>

         <div id="contact" class="row">
          
            <div class="container-fluid">
               <div class="contact-sec row">
                  <div class="col-md-12">
                     <div class="heading-custom text-center">Contact Us

                     <p style="text-decoration:none;" class="text-center">Call us 
                        <a href="javascript:void(0)">
                           Ghana: 026 840 6998  or 055 496 7571 or 050 675 2415
                           USA:  (+1) 888
                           </a>
                           </p>
                           </div> 652 2233
                        </a></p>
                     </div>
                     
                     <span id="contactUsmessage">
                        <span id="error-msg" style='color: red; font-size: 16px;'></span>
                     <div class="row">
                        <div class="form-group">
                           <label>Full name</label>
                           <input type="text" class="form-control" id="fullname">
                           <span id="err_fullname" style="color:red"></span>
                        </div>
                        <div class="form-group">
                           <label>Email address</label>
                           <input type="text" class="form-control" id="email">
                           <span id="err_email" style="color:red"></span>
                        </div>
                        <div class="form-group">
                           <label>Subject</label>
                           <input type="text" class="form-control" id="subject">
                           <span id="err_subject" style="color:red"></span>
                        </div>
                        <div class="form-group">
                           <label>Message</label>
                           <textarea class="form-control" rows="2" id="message"></textarea>
                           <span id="err_message" style="color:red"></span>
                        </div>
                        <div class="form-group">
                           <div class="custom-btn1" id="contactUs">
                              <span >Submit</span>
                            <!--  <div class="custom-btn-h"></div>   -->
                           </div>
                        </div>
                     </div>
                     </span>
                  </div>
               </div>
            </div>
<!--             ==========place an order model================ -->
      <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
             <div class="modal-dialog" role="document">
                <div class="modal-content">
                   <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title" id="exampleModalLabel">Place an Order</h4>
                   </div>
                   <form role="form" id="placeOrder" name="placeorder">
                   <div class="panel-body">  
                      <div class="col-md-12 row">
                         <div class="form-group">
                           <span>Please use the form below to place an order</span>
                           
                        </div>
                         <span id="placeOrdermessage">
                        <span id="error-msg" style='color: red; font-size: 16px;'></span>
                        <div class="">
                         <div class="col-sm-8">
                             <div class="form-group ">
                                 <label>Item url</label>
                                 <input type="text" class="form-control " id="item_url" placeholder="Item url">
                                 <span id="err_item_url" style="color:red" class="help-block"></span>
                              </div>
                           </div>     
                          


                           <div class="col-sm-4">
                             <div class="form-group">
                                 
                      <label class="control-label">Quantity</label>
                      <div style="width:160px;" class="input-group number-spinner">
                         <span class="input-group-btn data-dwn">
                         <button class="btn btn-primary" data-dir="dwn">
                         <span class="glyphicon glyphicon-minus"></span>
                         </button>
                         <div class="error-msg"></div></span>
                         <input class="form-control text-center" value="1" min="1" max="40" id="quantity" name="quentity" type="text">
                         <input value="" type="hidden">
                         <input value="0" name="calculated_distance" id="calculated_distance" type="hidden">
                         <span class="input-group-btn data-up">
                         <button class="btn btn-primary" data-dir="up">
                         <span class="glyphicon glyphicon-plus"></span>
                         </button>
                         <div class="error-msg"></div></span>
                     
                   <div class="error-msg"></div><div class="error-msg"></div><div class="error-msg"></div></div>
                             </div>
                          </div>
                        <div class="col-sm-12">
                        <div class="form-group">
                           <label> Your Email</label>
                           <input type="text" class="form-control required" id="email_id" placeholder="Email">
                           <span id="err_email_id" style="color:red" class="help-block"></span>
                        </div>
                        </div>
                        <div class="col-sm-12">
                        <div class="form-group">
                           <label>Your Phone#</label>
                           <input type="text" class="form-control required" id="phone_no" class="Phone no." minlength="9" maxlength="14" placeholder="Phone No.">
                           <span id="err_phone_no" style="color:red" class="help-block"></span>
                        </div></div>
                        <div class="col-sm-12">
                        <div class="form-group">
                           <label>Your Name</label>
                           <input type="text" class="form-control required" id="name" placeholder="Name">
                           <span id="err_name" style="color:red" class="help-block"></span>
                        </div></div>
                        <div class="col-sm-12">
                        <div class="form-group">
                           <label>Comments</label>
                           <textarea class="form-control required" rows="2" id="comments"></textarea>
                           <span id="err_comments" style="color:red" class="help-block"></span>
                        </div></div>
                        <div class="col-sm-12">
                        <div class="form-group">
                           <div class="" id="">
                              <input type="submit" class="custom-btn1" id="item_loader" value="Submit"  />
                              <div class="custom-btn-h"></div>
                           </div></div>
                           </form>
                        </div>
                     </div>
                     </span>
                     
                  
 
               
             </div>
          </div>
          </div>
          </div>
           
           <!--  ====================end place an order model============= -->
			<div id="canvas1">
            	<iframe id="map_canvas1" src="https://www.google.com/maps/d/u/0/embed?mid=zEXCW-LbfQ5g.kdplrdU16yvM" width="100%" height="650" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
            
         </div>
         <div class="row">
            <div class="container-fluid section-custom footer">
               <div class="container">
                  <div class="col-md-12 col-sm-12 customf">
                     <div class="row">
                        <div class="col-md-6 col-sm-6">
                           <div class="heading3 offset-btm-20">About</div>
                           <br>
                           <img class="footer-logo" width="50%" src="theme/web/promo/images/white_logo.png">
                           <p class="color-white">A growing trustworthy community of Transporters and Requesters, Aquantuo bridges the gap between the person wanting to have an item delivered to another location and the person or company willing to do so.</p>
                        </div>
                        <div class="col-md-3 col-sm-3 sitemap-style">
                           <div class="heading3 offset-btm-20">Site map</div>
                           <br>
                           
                           
                           
                           <ul class="footer-list">
                              <li><a href="#home">- Home</a>
                           </li>
                           <li><a href="#aboutapp">- About App</a>
                           </li>
                           <li><a href="#features">- Features</a>
                           </li>
                           <li><a href="#place-an-order" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo" onclick="reset_form('placeOrder');">- Place an Order</a></li>
                           <li><a href="#download">- Download</a>
                           </li> 
                           <li><a href="#contact">- Contact</a>
                           </li>
                           </ul>
                        </div>
                        <div class="col-md-3 col-sm-3">
                           <div class="heading3 offset-btm-20">Social media</div>
                           <br>
                           <div class="social-media clearfix">
                              <div class="col-md-4 col-sm-4 col-xs-4">
                                 <a href="https://www.facebook.com/Aquantuo" target="_blank">
                                    <div class="social-icon-facebook">
                                       <i class="fa fa-facebook"></i>
                                    </div>
                                 </a>
                              </div>
                              <div class="col-md-4 col-sm-4 col-xs-4">
                                 <a href="https://twitter.com/Aquantuo"  target="_blank">
                                    <div class="social-icon-twitter">
                                       <i class="fa fa-twitter"></i>
                                    </div>
                                 </a>
                              </div>
                              <div class="col-md-4 col-sm-4 col-xs-4">
                                 <a href="https://www.instagram.com/Aquantuo/" target="_blank">
                                    <div class="social-icon-instagram">
                                       <i class="fa fa-instagram"></i>
                                    </div>
                                 </a>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="second-f clearfix">
                     <div class="col-xs-12 text-center">
                        Copyright &copy; 2016 Aquantuo&nbsp;&nbsp; | &nbsp;&nbsp;
                        <a href="{{url('terms-and-conditions')}}"> Terms and conditions </a>&nbsp;&nbsp; | &nbsp;&nbsp; 
                        <a href="{{url('privacy-policy')}}"> Privacy Policy </a>
                     </div>
                     
                  </div>
               </div>
            </div>
         </div>
      </div>
      </div>
      </div>
      
	  <style>
      
    .scrolloff {
        pointer-events: none;
    }

      </style>
      <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
      <!-- Include all compiled plugins (below), or include individual files as needed -->
      {!! Html::script('theme/web/promo/js/bootstrap.min.js') !!}
      {!! Html::script('theme/web/promo/js/waypoints.min.js') !!}
      {!! Html::script('theme/web/promo/js/jquery.counterup.js') !!}
      {!! Html::script('theme/web/promo/js/modernizr-2.6.2.min.js') !!}
      {!! Html::script('theme/web/promo/js/main.js') !!}

     
      <script>
	
    $(document).ready(function () {

        // you want to enable the pointer events only on click;

        $('#map_canvas1').addClass('scrolloff'); // set the pointer events to none on doc ready
        $('#canvas1').on('click', function () {
            $('#map_canvas1').removeClass('scrolloff'); // set the pointer events true on click
        });

        // you want to disable pointer events when the mouse leave the canvas area;

        $("#map_canvas1").mouseleave(function () {
            $('#map_canvas1').addClass('scrolloff'); // set the pointer events to none when mouse leaves the map area
        });
    });

	  
	  
         jQuery(document).ready(function( $ ) {
             $('.counter').counterUp({
                 delay: 10,
                 time: 1000
             });
			 $('.nav.navbar-nav.navbar-right a').click(function(){
			$('#bs-example-navbar-collapse-1').removeClass('in');	 
				 });
         });
		 
      </script>
      <script>
       $(function() {
       var action;
       $(".number-spinner button").mousedown(function () {
           btn = $(this);
           input = btn.closest('.number-spinner').find('input');
           btn.closest('.number-spinner').find('button').prop("disabled", false);
   
           if (btn.attr('data-dir') == 'up') {
               action = setInterval(function(){
                   if ( input.attr('max') == undefined || parseInt(input.val()) < parseInt(input.attr('max')) ) {
                       input.val(parseInt(input.val())+1);
                   }else{
                       btn.prop("disabled", true);
                       clearInterval(action);
                   }
               }, 50);
           } else {
               action = setInterval(function(){
                   if ( input.attr('min') == undefined || parseInt(input.val()) > parseInt(input.attr('min')) ) {
                       input.val(parseInt(input.val())-1);
                   }else{
                       btn.prop("disabled", true);
                       clearInterval(action);
                   }
               }, 50);
           }
       }).mouseup(function(){
           clearInterval(action);
       });
   });
         jQuery(document).ready(function($) {  
         $(window).scroll(function(){
         if($(this).scrollTop() >= $('#home').offset().top+400) {
         $('.navbar').addClass('navbar-fixed-top'); 
         $('.landing-sec').addClass('offset-150');
         }else
         {
         $('.navbar').removeClass('navbar-fixed-top'); 
         $('.landing-sec').removeClass('offset-150');
         }
         });
         $(window).scroll(function(){
         if($(this).scrollTop() >= $('#aboutapp').offset().top-200) {
          $('.circle-icon').addClass('list-animation');
         $('.second-img1').addClass('animation-img1')
         $('.second-img2').addClass('animation-img1');
         $('.second-img3').addClass('animation-img1');
         }
         });
         $(window).scroll(function(){
         if($(this).scrollTop() >= $('#sec3').offset().top-200) {
          $('#phone2').addClass('right-animation');
         }
         });
         $(window).scroll(function(){
         if($(this).scrollTop() >= $('#features').offset().top-200) {
          $('.fancy-list').addClass('list-animation');
         }
         });

        });
             $(function() {
               $('a[href*=#]:not([href=#])').click(function() {
                 if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
                   var target = $(this.hash);
                   target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
                   if (target.length) {
                     $('html,body').animate({
                       scrollTop: target.offset().top
                     }, 1000);
                     return false;
                   }
                 }
               });
             });        
      </script>
      <!-- *********************************Some Userful functions for javascripe************************ -->
      <script type="text/javascript">
         $("#contactUs").click(function(){
            var flag = true;
            var email_expr = /^[A-Z0-9\._%-]+@[A-Z0-9\.-]+\.[A-Z]{2,4}(?:(?:[,;][A-Z0-9\._%-]+@[A-Z0-9\.-]+))?$/i;
            var phone_expr = 
            
            $("#err_fullname").html('');
            $("#err_email").html('');
            $("#err_subject").html('');
            $("#err_message").html('');

            if($("#fullname").val() == ""){
               $("#err_fullname").html('Please enter your full name.');
               flag = false;
            }
            if($("#email").val() == ""){
               $("#err_email").html('Please enter email id.');
               flag = false;
            }
            else if(email_expr.test($("#email").val()) == false){
               $("#err_email").html('Please enter valid email id.');
            }
            if($("#subject").val() == ""){
               $("#err_subject").html('Please enter some subject.');
               flag = false;
            }
            if($("#message").val() == ""){
               $("#err_message").html('Please enter message.');
               flag = false;
            }
            if (flag) {
              $("#contactUs").addClass('spinning');
               $.ajax({
                  url  : 'ajax/contact-us',
                  type : 'get',
                  data : "fullname="+$('#fullname').val()+"&email="+$('#email').val()+"&subject="+$('#subject').val()+"&message="+$('#message').val(),
                  success : function(res){
                    $("#contactUs").removeClass('spinning');
                     var obj = eval('('+res+')');
                     if(obj.success == 1){
                        $("#contactUsmessage").html("<span style='color: green; font-size: 16px;'>"+obj.msg+"</span>");  
                     } else {
                        $("#error-msg").html(obj.msg);   
                     }
                     
                  }
               });

            }
            
         });
      </script>
      <!-- ***********************************Subscribe************************************** -->
      <script type="text/javascript">

      	$('#Country').click(function(){
      		if($('#Country').val() == ""){
      			$("#Country").addClass('err_country');
      		}
      		else{
      			$("#Country").removeClass('err_country');
      		}
      	});
      
      	$("#ajx_subscribe").click(function(){
      		$("#err_subscribe_name").html('');
      		$("#err_subscribe_email").html('');
      		$("#err_subscribe_phoneNo").html('');
      		$("#err_subscribe_message").html('');

      		var flag = true;
      		var email_expr = /^[A-Z0-9\._%-]+@[A-Z0-9\.-]+\.[A-Z]{2,4}(?:(?:[,;][A-Z0-9\._%-]+@[A-Z0-9\.-]+))?$/i;
      		if($('#subscribe_name').val() == ""){
      			$("#err_subscribe_name").html('Please enter name');
      			flag = false;
      		}
      		if($('#subscribe_phoneNo').val() == ""){
      			$("#err_subscribe_phoneNo").html('Please enter phone number');
      			flag = false;
      		}
      		if($('#subscribe_email').val() == ""){
      			$("#err_subscribe_email").html('Please enter email');
      			flag = false;
      		}
      		else if(email_expr.test($('#subscribe_email').val()) == false){
      			$("#err_subscribe_email").html('Please enter valid email');
      			flag = false;
      		}
      		if($('#subscribe_message').val() == ""){
      			$("#err_subscribe_message").html('Please enter message');
      			flag = false;
      		}
      		if($('#Country').val() == ""){
      			$("#Country").addClass('err_country');
      			flag = false;
      		}
      		
      		if(flag){
      			$.ajax({
	      			url  : "ajax/subscribe",
	      			type : 'get',
	      			data : "subscribe_name="+$('#subscribe_name').val()+"&subscribe_phoneNo="+$('#subscribe_phoneNo').val()+"&subscribe_email="+$("#subscribe_email").val()+"&subscribe_message="+$("#subscribe_message").val()+"&Country="+$("#Country").val(),
	      			success : function(res){
	      				 var obj = eval('('+res+')');
	      				 if(obj.success == 1){
	      				 	$("#form_subscribe").addClass('successMsg').html('Thank you for joining our newsletter!!');
	      				 }
	      				 else {
	      				 	$("#form_subscribe").addClass('failMsg').html('Sorry!! failed to joining newsletter.');
	      				 }
	      			}
      			});
      		}
      		
      		return false;
      	});
      </script>
      <!-- *************************************End Subscribe******************************* -->
     <!--  ***************************************place an order********************* -->
      <script type="text/javascript">
         $("#placeOrder").submit(function(){
            var flag = true;
            var email_expr = /^[A-Z0-9\._%-]+@[A-Z0-9\.-]+\.[A-Z]{2,4}(?:(?:[,;][A-Z0-9\._%-]+@[A-Z0-9\.-]+))?$/i;
              var numbers = /^[0-9]+$/;  
           // $("#placeOrdermessage").addClass('spinning');
            $("#err_item_url").html('');
            $("#err_quantity").html('');
            $("#err_email_id").html('');
            $("#err_phone_no").html('');
            $("#err_name").html('');
            $("#err_comments").html('');

            if($("#item_url").val() == ""){
               $("#err_item_url").html('Please enter item url.');
               flag = false;
            }
            if($("#quantity").val() == ""){
               $("#err_quantity").html('Please enter quantity.');
               flag = false;
            }

            if($("#email_id").val() == ""){
               $("#err_email_id").html('Please enter email id.');
               flag = false;
            } 
            else if(email_expr.test($("#email_id").val()) == false){
               $("#err_email_id").html('Please enter valid email id.');
            }
            if($("#phone_no").val() == ""){
               $("#err_phone_no").html('Please enter phone no.');
               flag = false;
            }
            else if(numbers.test($("#phone_no").val()) == false){
               $("#err_phone_no").html('Please enter numeric value.');
               flag = false;
            }
            
            if($("#name").val() == ""){
               $("#err_name").html('Please enter name.');
               flag = false;
            }
            if($("#comments").val() == ""){
               $("#err_comments").html('Please enter comments.');
               flag = false;
            }
            if (flag) {
                $("#item_loader").addClass("spinning");
               $.ajax({
                  url  : 'ajax/place-an-order',
                  type : 'get',
                  data : "item_url="+$('#item_url').val()+"&quantity="+$('#quantity').val()+"&email_id="+$('#email_id').val()+"&phone_no="+$('#phone_no').val()+"&name="+$('#name').val()+"&comments="+$('#comments').val(),
                  success : function(res){
                     $("#item_loader").removeClass("spinning");
                     var obj = eval('('+res+')');
                     if(obj.success == 1){
                        $("#placeOrdermessage").html("<span style='color: green; font-size: 16px;'>"+obj.msg+"</span>");  
                     } else {
                        $("#error-msg").html(obj.msg);
                         reset_form('placeOrder');  
                     }
                     //$("#placeOrdermessage").removeClass('spinning');
                  }
               });

            }
            return false;
            
         });
      </script>
      <!-- ************************************end place an order*************************** -->

      <!-- *************************************Country************************************** -->
      <script type="text/javascript">
      	$("#ajx_country").click(function(){
      		$.ajax({
      			url :'ajax/country',
      			typr : 'get',
      			success : function(res){
      				var obj = eval('('+res+')');
      				$("#Country").html(obj.html);
      			}
      		});
      	});
      </script>
      <script>
      (function ($) {
           $('.spinner .btn:first-of-type').on('click', function() {
           if($('.spinner input').val() < 100){
             $('.spinner input').val( parseInt($('.spinner input').val(), 10) + 1);
          }
           });
           $('.spinner .btn:last-of-type').on('click', function() {
            if($('.spinner input').val() > 1){
             $('.spinner input').val( parseInt($('.spinner input').val(), 10) - 1);
          }
           });
         })(jQuery);
      </script>
   <script type="text/javascript">
   function reset_form(formid) {
    
   $("#"+formid).trigger("reset");
   $('.help-block').html('');
   $('.has-error').removeClass('has-error');
}
 new Validate({
    FormName :  'placeorder',
    ErrorLevel : 1,
    callback: function() {

    }
});
</script>
      <style type="text/css">
      .err_country{
      	color: red;
      }
      .successMsg{
      	color : #104F6A;
      	widht: 100px;
      }
      .failMsg{
      	color: red;
      }
      .spinner {
  width: 100%;
}
.spinner input {
  text-align: right;
}
.input-group-btn-vertical {
  position: relative;
  white-space: nowrap;
  width: 1%;
  vertical-align: middle;
  display: table-cell;
}
.input-group-btn-vertical > .btn {
  display: block;
  float: none;
  width: 100%;
  max-width: 100%;
  padding: 8px;
  margin-left: -1px;
  position: relative;
  border-radius: 0;
}
.input-group-btn-vertical > .btn:first-child {
  border-top-right-radius: 4px;
}
.input-group-btn-vertical > .btn:last-child {
  margin-top: -2px;
  border-bottom-right-radius: 4px;
}
.input-group-btn-vertical i{
  position: absolute;
  top: 0;
  left: 4px;
}

      </style>
    
   </body>
</html>
