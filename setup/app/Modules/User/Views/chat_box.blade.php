<!--chat box -->

<script type="text/javascript">
    
    SITEURL = '{{URL::to("")}}/';
    USERID = '{{Session::get("UserId")}}';
    USERIMAGE = '{{Session::get("Image")}}';
    USERNAME = '{{Session::get("Name")}}';


</script>

<div class="chat_box" id="chat_box" style="display:none;">
    <div class="row chat-window col-xs-12 col-md-4" id="chat_window" style="margin-left:10px;">
        <div class="col-xs-12 col-md-12">
            <div class="panel panel-default chat_panel">
                <div class="panel-heading top-bar">
                    <div class="col-md-8 col-xs-8">
                        <h3 class="panel-title"><span class="fa fa-comments" id="customer_name">&nbsp;</span> </h3>
                        <h5><span id="user_status">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Online</span></h5>
                    </div>
                    <div class="col-md-4 col-xs-4" style="text-align: right; padding-right:0px;">
                        <!-- <a href="#"><span id="minim_chat_window" class="fa fa-minus"></span></a> -->
                        <a href="#" onclick="return close_box();"><span class="fa fa-times icon_close" data-id="chat_window"></span></a>
                    </div>
                </div>
                <div class="panel-body msg_container_base" >
                    <div class="frame" id="msg">
                        <ul>
                        </ul>
                    </div>
                    <div class="panel-footer">
                        <p style="font-size:15px;color:green;display:none;" id="typing">Typing.....</p>
                        <div class="input-group">
                            <input id="msg-input" type="text" class="form-control input-sm chat_input" placeholder="Write your message here..."  onkeypress="return myFunction()"/>
                            <span class="input-group-btn">

                            <input type="hidden" id="to_id" name="to_id" value="">
                            <input type="hidden" id="user-id" name="user-id" value="">
                            <button class="btn btn-primary btn-sm" id="btn-chat" onclick="return mes_send()">Send</button>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<style type="text/css">
    
    .chat-window {
        bottom: -20px;
        float: right;
        max-width: 350px;
        min-width: 350px;
        position: fixed;
        right: 10px;
        z-index: 1000;
    }
    
    .chat-window>div>.panel {
        border-radius: 5px 5px 0 0;
    }
    
    .icon_minim {
        padding: 2px 10px;
    }
    
    .msg_container_base {
        background: #e5e5e5;
        margin: 0;
        padding: 0 10px 10px;
        max-height: 300px;
        overflow-x: hidden;
    }
    
    .top-bar {
        background: #666;
        color: white;
        padding: 10px;
        position: relative;
        overflow: hidden;
    }
    
    .msg_receive {
        padding-left: 0;
        margin-left: 0;
    }
    
    .msg_sent {
        padding-bottom: 20px !important;
        margin-right: 0;
    }
    
    .msg_container {
        padding: 10px;
        overflow: hidden;
        display: flex;
    }
    
    .msg_container_base::-webkit-scrollbar-track {
        -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
        background-color: #F5F5F5;
    }
    
    .msg_container_base::-webkit-scrollbar {
        width: 12px;
        background-color: #F5F5F5;
    }
    
    .msg_container_base::-webkit-scrollbar-thumb {
        -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, .3);
        background-color: #555;
    }
    
    .btn-group.dropup {
        position: fixed;
        left: 0px;
        bottom: 0;
    }
    
    .mytext {
        border: 0;
        padding: 10px;
        background: whitesmoke;
    }
    
    .text {
        width: 75%;
        display: flex;
        flex-direction: column;
    }
    
    .text>p:first-of-type {
        width: 100%;
        margin-top: 0;
        margin-bottom: auto;
        line-height: 13px;
        font-size: 14px;
        color: #333;
        word-wrap: break-word;
        word-break: break-all;
    }
    
    .text>p:last-of-type {
        width: 100%;
        text-align: left;
        color: silver;
        margin-bottom: 0px;
        margin-top: auto;
        color: #333;
        word-wrap: break-word;
        word-break: break-all;
    }
    
    .text-l {
        float: left;
        padding-right: 10px;
    }
    
    .text-r {
        float: right;
        padding-left: 10px;
    }
    
    .avatar {
        display: flex;
        justify-content: center;
        align-items: flex-start;
        width: 20%;
        float: left;
        padding-right: 10px;
    }
    
    .macro {
        margin-top: 5px;
        width: 85%;
        border-radius: 5px;
        padding: 5px 0;
        display: flex;
        margin-left: 10px;
    }
    
    .msj-rta {
        float: right;
        background: whitesmoke;
        margin-right: 10px;
    }
    
    .msj {
        float: left;
        background: white;
    }
    
    .frame {
        background: #e0e0de;
        height: 240px;
        overflow: auto;
        padding: 0;
        position: relative;
    }
    
    .frame>div:last-of-type {
        position: absolute;
        bottom: 5px;
        width: 100%;
        display: flex;
    }
    
    .frame ul {
        width: 100%;
        list-style-type: none;
        padding-top: 5px;
        padding-left: 0;
        position: absolute;
        top: 0px;
        height: 100%;
        display: flex;
        flex-direction: column;
    }
    
    .msj:before {
        width: 0;
        height: 0;
        content: "";
        top: -5px;
        left: -10px;
        position: relative;
        border-style: solid;
        border-width: 0 13px 13px 0;
        border-color: transparent #ffffff transparent transparent;
    }
    
    .msj-rta:after {
        width: 0;
        height: 0;
        content: "";
        top: -5px;
        left: 10px;
        position: relative;
        border-style: solid;
        border-width: 13px 13px 0 0;
        border-color: whitesmoke transparent transparent transparent;
    }
    
    /*.chat_panel {
        box-shadow: 0 0 40px #D3D3D3;
        -webkit-box-shadow: 0 0 40px #D3D3D3;
        border: 1px solid #bfbfbf;
    }*/

    .chat_panel {
        border: 1px solid #bfbfbf;
        box-shadow: 0 0 40px rgba(0, 0, 0, 0.4);
        -webkit-box-shadow: 0 0 40px rgba(0, 0, 0, 0.4);
        -moz-box-shadow: 0 0 40px rgba(0, 0, 0, 0.4);
    }
    
    .chat_panel .panel-footer {
        padding: 5px 5px;
    }
    
    .chat_panel small {
        font-size: 70%;
        color: #b6b6b6;
    }
    
    .chat_panel .fa-plus::before {
        content: "\f067" !important;
    }
    
    .chat_panel .panel-title {
        margin-top: 0;
        margin-bottom: 0;
        font-size: 17px;
        color: inherit;
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;
        max-width: 196px;
    }
    
    .icon_close {
        margin-left: 8px;
    }
    
    .cursor {
        cursor: pointer;
    }
    
    .forgot-bg {
        background-image: url('../public/img/login.jpg')
    }
    
    .text-overflow {
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;
        width: 100%;
    }
</style>

<!-- end box