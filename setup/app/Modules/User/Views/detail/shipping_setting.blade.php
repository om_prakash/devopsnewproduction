@extends('page::layout.one-column-page')
@section('content')
<div class="container">
    <div class="row">
    	<div class="col-sm-12">
        <h2 class="color-blue">Shipping Settings</h2>
        <br />
        </div>
        <div class="col-md-9 col-sm-8 col-xs-12">
					<div class="box-shadow clearfix">
						<h3>Saved Address</h3>
						<hr/>
						<p>Lorem ipsum is simple dummy text of the printing and typesetting industry.</p>
						<div class="col-md-6 col-sm-12">
							<div class="gray-bg">
								
								<div class="media">
										<div class="media-left">
										<a href="#">
										<img src="theme/web/promo/images/map-list.png" />	
										</a>
									</div>
									<div class="media-body">
									<br/>
										<h4 class="media-heading">Home</h4>
										<p>552 Kent Ave.<br/>Palmdale, CA 93550</p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-sm-12">
							<div class="gray-bg">
								<div class="media">
										<div class="media-left">
										<a href="#">
										<img src="theme/web/promo/images/map-list.png" />	
										</a>
									</div>
									<div class="media-body">
									<br/>
										<h4 class="media-heading">Home</h4>
										<p>552 Kent Ave.<br/>Palmdale, CA 93550</p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-sm-12">
							<div class="gray-bg">
								<div class="media">
										<div class="media-left">
										<a href="#">
										<img src="theme/web/promo/images/map-list.png" />	
										</a>
									</div>
									<div class="media-body">
									<br/>
										<h4 class="media-heading">Home</h4>
										<p>552 Kent Ave.<br/>Palmdale, CA 93550</p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-sm-12">
							<div class="gray-bg">
								<div class="media">
										<div class="media-left">
										<a href="#">
										<img src="theme/web/promo/images/map-list.png" />	
										</a>
									</div>
									<div class="media-body">
									<br/>
										<h4 class="media-heading">Home</h4>
										<p>552 Kent Ave.<br/>Palmdale, CA 93550</p>
									</div>
								</div>
							</div>
						</div>
					<div class="col-sm-8 col-sm-offset-2">
                           <div class="form-group">
                              <button class="custom-btn1 btn-block">
                                 Update
                                 <div class="custom-btn-h"></div>
                              </button>
                             <p></p> <p></p>
                           </div>
                           <br/><br/>
                        </div>
					</div>
                </div>@include('page::layout.side_bar')
    </div> 
</div>

@endsection
