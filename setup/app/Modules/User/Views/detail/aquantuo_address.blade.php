@extends('Page::layout.one-column-page')
@section('page_title')
Unique International Shipping Address - Aquantuo
@endsection
@section('content')
<style>
   .panel {  margin-top:20px; }
</style>
<div class="container">
<style>
.box-height{
  height:450px;
}
</style>
   <div class="row">
   	  <div class="col-sm-12">
      <h2 class="color-blue mainHeading">My Aquantuo Address </h2>
      <br />
      </div>

         <div class="col-md-9 col-sm-8 col-xs-12">
            <div class="box-shadow clearfix">
               <div class="media">
                  <div class="media-left"></div>
                  <div class="media-body">
                  <br/>
                     @if($users->AqAddress != '')
                     <h2>{{$users->Name}}</h2>
                     <h4>{{ucfirst($users->AqAddress)}}</h4>
                     <h4><b>Unit#  {{$users->UniqueNo}}</b><h4>
                     <h4>{{ucfirst($users->AqCity)}}&nbsp;{{ucfirst($users->AqState)}}&nbsp;{{ucfirst($users->AqZipcode)}}</h4>
                      <h4>{{ucfirst($users->AqCountry)}}</h4>
                      <strong>Phone Number:</strong><span> {{$users->Aqcc}}-{{$users->Aqphone}}</span>
                      <br />
                     <h4> This is your unique address. This is how it works: </h4>
                     <ol type="1" class="address-note">
                     <li> Shop online from your favorite stores and at checkout, choose to ship it to the address above – your Aquantuo address. </li></br>
                     <li> On this website, go to “Create Request”  then “Your Online Purchases” and tell us about the items you bought. </li></br>
                     <li> Once we receive the package you purchased and paid for at your Aquantuo address, we will send it to the destination address you told us about. </li>
                     </ol>
                     </br></br>


                     @endif

                  </div>
               </div>
            </div>
         </div>
         @include('Page::layout.side_bar')
   </div>
</div>
@endsection
