@extends('Page::layout.one-column-page')
@section('content')
{!! Html::script('theme/web/rating/jquery.rateyo.js') !!}    
{!!Html::style('theme/web/rating/jquery.rateyo.min.css')!!}

<div class="container">
    <div class="">
    <h2 class="color-blue">Online Request Detail</h2>
    <br />

    <?php   echo "<pre>"; print_r($user_data);  die; ?>
      <div class="box-shadow">
                              <!------------------step1------------------>
    			<div class="box-header">
					{{ucfirst($item->ProductTitle)}}<span class="pull-right"><i class="fa fa-arrow-left" aria-hidden="true"></i>
					<a href="{{url('my-request')}}" style="color:#FFFFFF;">Back</a></span>
                </div>
                <div class="content-section row clearfix">
					<div class="col-sm-6">                              
							<p><b>Package Number :&nbsp;</b> {{$item->PackageNumber}}</p>
                @if(isset($item->EnterOn->sec))
								<p><b>Created on :&nbsp;</b> {{date('l,  M  d, Y ',$item->EnterOn->sec)}}</p>
							@endif 
							<p><b>Return to Aquantuo :&nbsp;</b><?php echo ucfirst(Session()->get('AqAddress')); ?></p>
							
					</div>
					<div class="col-sm-6">
						
					</div>
      </div>
                <br />                                     
      <div class="row">
                    <?php $sno=0; ?>		
                       @foreach($item->ProductList as $value)
      
      <div class="">	
				<div class="col-sm-6 col-xs-12">
								<p><b>Purchase From - </b>
                <a  href='<?php echo $value['url'] ; ?>'>{{$value['url']}}</a></p>
                 <p><b>Package Id :&nbsp;</b>{{ucfirst($value['package_id'])}}</p>
								<p><b>Description : </b>{{ucfirst($value['description'])}}</p>
                @if($value['TransporterRating'] != '')
                  <p><b>Your Feedback :</b> {{ucfirst($value['TransporterFeedbcak'])}} </p>
                  <p><b><span class="pull-left">Rating : </span> </b> 
                      
                      <div class="star-ratings-sprite pull-left">
                        <span style="width:<?php echo $value['TransporterRating']*20; ?>%" class="star-ratings-sprite-rating"></span>
                     </div>
                     </br>
                  </p> 
                  @endif	
                  <p><b>Item Status :&nbsp;</b>
                 @if($value['status'] == 'out_for_delivery')
                   Out for Delivery
                 @elseif($value['status'] == 'out_for_pickup') 
                   Out for pickup 
                 @elseif($value['status'] == 'assign')
                   Assign  
                 @elseif($value['status'] == 'pending')
                   Pending  
                 @elseif($value['status'] == 'purchased')
                   Purchased
                 @elseif($value['status'] == 'paid')
                   Paid  
                 @elseif($value['status'] == 'delivered')
                   Delivered
                 @elseif($value['status'] == 'cancel')
                   Cancel
                 @elseif($value['status'] == 'ready')
                   Ready       
                 @else($value['status'] == 'accepted')
                   Accepted  
                 @endif  
                
                </p>
				</div>

        <div class="col-sm-6 col-xs-12">
								
               <div class="media">
                    <div class="form-group media-left">
                     @if($user_data->Image != '')
                        <a class="fancybox" rel="group" href="{{ ImageUrl.$user_data->Image}}" >
                            <img src="{{ ImageUrl.$user_data->Image}}"  width="150px" height="150px" class="img-circle">
                          </a>   
                      @else
                          <img src="{{ImageUrl}}/user-no-image.jpg" width="150px" height="150px" class="img-circle">
                      @endif
                    </div>
                    <div class="media-body">
                    <br>
                    <b>Transporter Name: </b>{{ucfirst($user_data->Name)}}
                    <br><br>
                       @if(count($user_data) > 0)
                <?php $average_rating = 0;
                    if($user_data->RatingCount > 0 && $user_data->RatingByCount > 0)
                    {
                      $rating = $user_data->RatingCount/$user_data->RatingByCount;
                      $average_rating = 20 * $rating;
                    }
                ?>
                     <div class="col-sm-12">
                    <div class="star-ratings-sprite pull-left">
                      <span style="width:<?php echo $average_rating?>%" class="star-ratings-sprite-rating"></span>
                    </div>
                     </div>
                  @endif
                    </div> 
                 </div> 
                
        </div> 
      </div>
            
        <div class="col-sm-12">
        </br>
                <table class="custom-table table table-bordered">
                    <thead>
                        <tr>
										      <th>Product Image</th>
                          <th>Product Name</th>
                          <th>Item Category</th>
                          <th>Shipping Mode</th>
                          <th>Weight</th>
                          <th>Quantity</th>
                          <th>Verification Code</th>
                          <th>Item Price</th>
                                        
                        </tr>
                    </thead>
                        <tbody>
                            <tr>    
              									<td>
                                  @if($value['image'] != '')         
                                    <img src="{{ ImageUrl.$value['image']}}" width="130px" height="80px" >
                                  @else
                                    <img src="{{ ImageUrl}}/no-image.jpg" width="130px" height="100px" >
                                  @endif
                                </td>
              									<td>{{ucfirst($value['product_name'])}}</td>
              									<td>{{$value['category']}}</td>
              									<td>{{ucfirst($value['travelMode'])}}</td>
              									<td>{{$value['weight']}} {{$value['weight_unit']}}</td>
              									<td>{{$value['qty']}}</td>
              									<td>{{$value['verify_code']}}</td>
              									<td>${{number_format((float)$value['price'],2,'.','')}}</td>
                              </tr>                       
                          </tbody>
                  </table>
                   <p class="pull-right">
                    @if(in_array($value['status'] , ['out_for_delivery','out_for_pickup','assign'])) 
                      <a class="btn btn-primary" href="javascript:void(0)" id="chat-loader-{{$value['tpid']}}" onclick="open_chat_box('{{$value['tpid']}}','{{$item->TransporterName}}','chat-loader-{{$value['tpid']}}')"><i class="fa fa-chat"></i> Chat</a> 
                    @endif 
                   </p>
                        
                        <p class="col-sm-6">
                        @if(!$value['RequesterFeedbcak'] == '')
                          <b>Requester Feedbcak :</b> 
                            {{ucfirst($value['RequesterFeedbcak'])}}
                          
                        @endif
                        </p> 
                          <p class="col-sm-12"></p>

                           @if($value['TransporterFeedbcak'] == '' &&   $value['status'] == 'delivered')
                             <div class="pull-right"> <a href="javascript:void(0)" data-toggle="modal" data-target="#modal2" title="Review"  class="btn btn-default blue-btn" id= "feedback" onclick="give_feedback('{{$value['_id']}}')" style="">Give Feedback</a></div>
                           @endif 
                        <div class="clearfix"></div>

                        <hr>
                      </div>
                    <?php $sno++; ?>
                  @endforeach
                </div>
               
                    <div class="list-footer clearfix">
                    	<div class="row">
                      <div class="col-sm-4">
          
                <p><b>Total Number of Item(s): </b><?php echo $sno; ?> </p>
                <p><b>Delivery Address: </b>{{ucfirst($item->DeliveryFullAddress)}}</p>
                <p><b>Phone Number:</b> {{$item->country_code}} - {{$item->ReceiverMobileNo}}</p> 
                <p><b>Distance</b>: {{number_format($item->distance,2)}} Miles</p> 
							</div>
							<div class="col-sm-3">
								
							</div>
						
							<div class="payment-sec col-sm-4 pull-right">
								<h3 class="">Payment Info</h3>
                                	<table class="table table-bordered">
  										<tbody bgcolor="#FFFFFF">	

                      <tr>
                        <td class="">
                          <div class="col-xs-8 text-left">
                            <label class="" for="exampleInputEmail1"><b>Shipping Cost:</b> </label>
                          </div>
                          <div class="col-xs-4 text-right">
                            @if(!$item->shippingCost == '')
                             ${{number_format($item->shippingCost,2)}}
                            @endif
                            
                          </div>  
                        </td>
                      </tr>   


                      @if($item->insurance > 0)
                      <tr>
                        <td class="">
                          <div class="col-xs-8 text-left">
                            <label class="" for="exampleInputEmail1"><b>Insurance Cost:</b> </label>
                          </div>
                          <div class="col-xs-4 text-right">
                            (+)${{number_format($item->insurance,2)}}
                          </div>  
                        </td>
                      </tr>  

                      @endif   
                      @if($item->discount > 0)
                      <tr>
                        <td class="">
                        
                            <div class="col-xs-8 text-left">
                              <label class="" for="exampleInputEmail1"><b>Aquantuo Discount:</b> </label>
                            </div>
                            <div class="col-xs-4 text-right">
                                (-)${{number_format($item->discount,2)}}                               
                            </div>
                        </td>
                      </tr>                                   
                      @endif											
                      <tr>
												<td class="">
													<div class="col-xs-8 text-left">
														<label class="" for="exampleInputEmail1"><b>Total Cost:</b> </label>
													</div>
													<div class="col-xs-4 text-right">

                            ${{number_format((($item->shippingCost + $item->insurance) - $item->discount),2)}}
														 
													</div>
											    </td>   
											</tr>

											
											                                           
                                        </tbody>
									</table>
                                 
                                 
                            </div>
                        </div>
                    </div>
                </div>
        
        </div>
</div>
{!! Html::script('theme/web/js/validation.js') !!}



<div aria-labelledby="exampleModalLabel" role="dialog" tabindex="-1" id="exampleModal" class="modal fade in">
   <div role="document" class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header" id="Addaddress2">
            <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button>
            <h4 id="exampleModalLabel" class="modal-title">Payment</h4>
         </div>
         {!! Form::model('', ['name' => 'new_address', 'id' => 'Addaddress', 'method' => 'POST']) !!}
            <div class="panel-body">
               <div class="row">
                  <div class="col-sm-12">
                     <div class="form-group">
                        <label>Address Line1</label>
                        <input type="text" placeholder="Address" name="address_line_1" id="address_line_1"  maxlength= "125" class="form-control required">
                     </div>
                  </div>
                  <div class="col-sm-12">
                     <div class="form-group">
                        <label>Address Line2</label>
                        <input type="text" placeholder="Address" name="address_line_2" id="address_line_2"  maxlength= "125" class="form-control">
                     </div>
                  </div>
                 
                 
                
                  <div class="col-sm-6">
                     <div class="form-group">
                        <label>Zipcode</label>
                        <input type="text" placeholder="Zipcode" name="zipcode" id="zipcode"  maxlength="6" class="form-control">
                     </div>
                  </div>
                  </hr>
                  <div class="col-sm-8 col-sm-offset-2" align="center">
                      <div class="row">
                  <div class="col-sm-6 col">
                     <div class="form-group">
                        <button class="custom-btn1 btn-block" id="address_loader" >
                           Submit
                           <div class="custom-btn-h"></div>
                        </button>
                     </div>
                  </div>
                     <div class="col-sm-6 col">
                     <div class="form-group">
                        <button class="custom-btn1 btn-block"  data-dismiss="modal">
                           Close
                           <div class="custom-btn-h"></div>
                        </button>
                     </div>
                  </div>
                  </div>
                  </div>
               </div>
            </div>
         {!! Form::close() !!}
      </div>
   </div>
</div>

<!--review-model-->
<div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
      <div class="pre-modal-dialog modal-md">
        <div class="modal-content">
          <div class="modal-header popup-bg text-center">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <p class="popup-head color-white">Rate your experience with transporter</p>

          </div>
          <div class="clearfix"></div>
        <!-- <input type="hidden" name="item_id"  id="item_id" value="<?php /*echo $item->_id   */?>" >   -->
        <center>
         <div class="modal-body text-left">
        <div id="rating_success_container"></div>
        <div id="rating_danger_container"></div>
            {!! Form::model('', ['name' => 'RequestReview', 'id' => 'RequestReview']) !!}
            <div class="col-md-12">
               <div class="col-md-12" align="center">
               <div>             
                     @if($user_data->Image != '')
                        <a class="fancybox" rel="group" href="{{ ImageUrl.$user_data->Image}}" >
                            <img src="{{ ImageUrl.$user_data->Image}}"  width="150px" height="150px" class="img-circle">
                          </a>   
                      @else
                          <img src="{{ImageUrl}}/user-no-image.jpg" width="150px" height="150px" class="img-circle">
                      @endif
               </div>
               </br>    
               <div> 
                  <label class="rating_margin" >Rate Now :</label>
                     &nbsp;&nbsp;&nbsp;
                     <div class="rateyo required" id="rateyo" ></div>
                     <div class="clearfix"></div>
                     <div id="er_rating"  class="error-msg"></div>
                 <input type="hidden" name="item_id" id="item_id" value="{{$value['_id']}}">
                     </div>
                     </br>
                  </div>   
               
                  <div class="col-md-12">
                   <div class="">
                     <div class="form-group txt-area feedback-text">                        
                        {!! Form::textarea('review','',['class'=>'form-control required', 'placeholder'=> 'Feedback','id' =>'review','maxlength' =>250,'size' =>'30x4']) !!} 
                     </div>                         
                   <div id="er_review" class="help-block white"></div>
                   <div class="clearfix"></div>
                     <span id="review_msg"></span>
                  </div>
               </div>
               <div class="col-md-2"></div>
            </div>   
             <div class="clearfix"></div>
                    <div class="col-xs-6">
                       <div class="form-group">
                          <button type="submit" class="custom-btn1 btn-block" id='ratingButton'>SUBMIT</button>
                       <div class="error-msg"></div></div>
                    </div>
                    <div class="col-xs-6">
                       <div class="form-group text-center">
                          <button class="custom-btn1 btn-block" data-dismiss='modal' >NOT NOW</button>
                       </div>
                    </div>
                

                  {!! Form::close() !!}
         
          <div class="clearfix"></div>
          
        </div>
      </div>


<!--end-->

<script>

$(document).ready(function() {
  $(".fancybox").fancybox();
});

$('#cancel').click(function(){
  $('#exampleModal').trigger("click");
}); 

new Validate({
      FormName : 'RequestReview',
      ErrorLevel : 1,
      validateHidden: false,
      callback : function()
    {
      if($rateYo.rateYo("rating") <= 0) {
        $('#er_rating').html('The rating field is required.');
        return false;
      }
        $("#ratingButton").addClass('spinning');
        $.ajax({
          url     : '{{url("/online-requester-review")}}',
          type    : 'post',
          data    : {
              "review" : $("#review").val(), 
              "item_id" : $('#item_id').val(),
              "rating" : $rateYo.rateYo("rating"),
            },
            success : function(res)
            {
                $("#ratingButton").removeClass('spinning');
                var obj = eval("("+res+")");
                if(obj.success == 1)
                {
                  $("#feedback").hide();
                  location.reload();
                  $("#RequestReview").trigger( "reset" );
                  $('#rating_success_container').html('<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>'+obj.msg+'</div>');
                }
                else
                {
                  
                  $('#rating_danger_container').html('<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>'+obj.msg+'</div>');
                }
            }
        });
      }
      
   });
</script>
@endsection
<style>
.pre-modal-dialog {
    margin: 30px auto;
    width: 440px;
}
</style>
@section('inline-script')
@parent
<script>
function give_feedback($id)
{
   $('#item_id').val($id);
}


$(".fancybox").fancybox();

var $rateYo = $("#rateyo").rateYo();


</script>
@endsection