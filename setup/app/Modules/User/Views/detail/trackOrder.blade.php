@extends('Page::layout.one-column-page')
@section('content')
<section class="">
    <div class="container">

    <div class="box-shadow">
      <div class="content-section clearfix">
            <span class="pull-right">
             @if($date->RequestType == 'online')
             <a href="{{ url('online-request-detail/'.$date->_id) }}" class="btn btn-default blue-btn" style="color:#FFFFFF;"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;&nbsp;Back</a>&nbsp;&nbsp;
             @elseif($date->RequestType == 'buy_for_me')
            <a href="{{ url('buy-for-me-detail/'.$date->_id) }}" class="btn btn-default blue-btn" style="color:#FFFFFF;"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;&nbsp;Back</a>&nbsp;&nbsp;
            @elseif($date->RequestType == 'delivery')
            <a href="{{ url('send-package-detail/'.$date->_id) }}" class="btn btn-default blue-btn" style="color:#FFFFFF;"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;&nbsp;Back</a>&nbsp;&nbsp;
            @endif
            </span>

            <div class="col-md-2 col-sm-3">
           @if(isset($date['ProductList'][0]['ProductImage']))
            @if($date['ProductList'][0]['ProductImage'] != '')
               <img src="{{ ImageUrl.$date['ProductList'][0]['ProductImage']}}"  width="150px" height="150px">
            @else
                <img src="{{ImageUrl}}/no-image.jpg" width="150px" height="150px" >
            @endif
            @endif

            </div>
        <div class="col-md-10 col-sm-9">
            <h4><b> {{ ucfirst($date['ProductList'][0]['product_name'])}}</b></h4>
            <p class="color-black"><strong>Package Id: </strong>{{ Input::get('package_id') }}</p>
            <p class="color-black" id="status_div"><strong>Status: </strong>
            @if($date['ProductList'][0]['status'] == 'cancel' &&  @$date['ProductList'][0]['RejectBy'] != '')
            Not Delivered<br>
            <p class="color-black" id=""><strong>Transporter Message: </strong>
            {{@$date['ProductList'][0]['TransporterMessage']}}
            @else
            {{ get_status_title($date['ProductList'][0]['status'],$date->RequestType)['status'] }}
            @endif

            </p>
            <p class="color-black" style="display:none;" id="ajax_status"><strong>Status:</strong></p>
         </div>
      </div>
   </div>

   
    @if($date->RequestType == 'online')
     <div class="box-border">
        <h3 class="midHeading"><strong>Track Order</strong><br></h3>
        <br/>

        <?php 
        
            $number_set = 1;
            $wait_pay_show = 'no';

            if($current_status == 'pending'){
                $number_set = 1;
            }else if($current_status == 'ready'){
                $number_set = 2;
            }else if($current_status == 'reviewed'){
                $number_set = 3;
            }else if($current_status == 'not_purchased'){
                $number_set = 3;
            }else if($current_status == 'paid'){
                $number_set = 4;
            }else if($current_status == 'purchased'){
                $number_set = 5;
            }else if($current_status == 'assign'){
                $number_set = 6;
            }else if($current_status == 'out_for_pickup'){
                $number_set = 7;
            }else if($current_status == 'out_for_delivery'){
                $number_set = 8;
            }else if($current_status == 'delivered'){
                $number_set = 9;
            }

            if($current_status == 'cancel' && $RejectBy != '' ){
                $number_set = 8;
            }
            
        ?>
<style type="text/css">
    .row-warp.online-step .step.inner:after {bottom: 43px;}
    .row-warp.online-step .step:before{bottom: 43px;}
</style>

        <div class="row-warp step_box online-step">
            <div class="step first col  @if($number_set > 0) selected done @endif">
                
                <div></div>
                <p class="text-center colol-black">Pending</p>
            </div>
            <div class="step first col  @if($number_set > 1) selected done @endif">
                
                <div></div>
                <p class="text-center colol-black">Ready</p>
            </div>
            <div class="step first col  @if($number_set > 2) selected done @endif">
                
                <div></div>
                <p class="text-center colol-black">Reviewed</p>
            </div>
            @if($current_status == 'not_purchased')
            <div class="step inner col @if($number_set > 3) selected done @endif ">
                
                <div></div>
                <p class="text-center colol-black">Item Updated</p>
            </div>
            @endif
            <div class="step inner col @if($number_set > 4) selected done @endif ">
                
                <div></div>
                <p class="text-center colol-black">Item Received</p>
            </div>
             <div class="step inner col @if($number_set > 5) selected done @endif ">
                
                <div></div>
                <p class="text-center colol-black">Shipment Departed</p>    <!-- Transporter Assigned -->
            </div>
            <div class="step inner col @if($number_set > 6) selected done @endif ">
                
                <div></div>
                <p class="text-center colol-black">Out for Pickup</p>
            </div>
            <div class="step inner col @if($number_set > 7) selected done @endif ">
                
                <div></div>
                <p class="text-center colol-black">Out for Delivery</p>
            </div>
            <div class="step inner col @if($number_set > 8) selected done @else last @endif ">
                
                <div></div>
                <p class="text-center colol-black">Delivered</p>
            </div>
        </div>
        </div>
        <div class="clearfix"></div>
    </div>

    @elseif ($date->RequestType == 'buy_for_me')

        <?php 

            $number_set = 1;
            if($current_status == 'pending'){
                $number_set = 1;
            }else if($current_status == 'ready'){
                $number_set = 2;
            }else if($current_status == 'reviewed'){
                $number_set = 3;
            }else if($current_status == 'not_purchased'){
                $number_set = 4;
            }else if($current_status == 'paid'){
                $number_set = 4;
            }else if($current_status == 'purchased'){
                $number_set = 5;
            }else if($current_status == 'item_received'){
                $number_set = 6;
            }else if($current_status == 'item_received'){
                $number_set = 6;
            }else if($current_status == 'assign'){
                $number_set = 7;
            }else if($current_status == 'out_for_pickup'){
                $number_set = 8;
            }else if($current_status == 'out_for_delivery'){
                $number_set = 9;
            }else if($current_status == 'delivered'){
                $number_set = 10;
            }
            if($current_status == 'cancel' && $RejectBy != '' ){
                $number_set = 9;
            }

            
        ?>
    <div class="box-border">
        <h3 class="midHeading"><strong>Track Order</strong><br></h3>
        <br/>
        <div class="row-warp step_box">
            <div class="step first col  @if($number_set > 0) selected @endif">
                <div></div>
                <p class="text-center colol-black">Pending</p>
            </div>
            <div class="step inner col  @if($number_set > 1) selected done @endif">
                <div></div>
                <p class="text-center colol-black">Ready</p>
            </div>
            <div class="step inner col @if($number_set > 3) selected done @endif ">
                <div></div>
                <p class="text-center colol-black">Pending Purchase</p>
            </div>
            <div class="step inner col @if($number_set > 4) selected done @endif ">
                <div></div>
                <p class="text-center colol-black">Purchased</p>
            </div>

            <div class="step inner col  @if($number_set > 5) selected done @endif">
                <div></div>
                <p class="text-center colol-black">Listing Updated/Pending Item Receipt</p>
            </div>

            <div class="step inner col @if($number_set > 5) selected done @endif ">
                <div></div>
                <p class="text-center colol-black">Item Received</p>
            </div>
            <div class="step inner col @if($number_set > 6) selected done @endif ">
                <div></div>
                <p class="text-center colol-black">Shipment Departed</p>    <!-- Transporter Assigned -->
            </div>
            <div class="step inner col @if($number_set > 7) selected done @endif ">
                <div></div>
                <p class="text-center colol-black">Out for Pickup</p>
            </div>
            <div class="step inner col @if($number_set > 8) selected done @endif ">
                <div></div>
                <p class="text-center colol-black">Out for Delivery</p>
            </div>
            <div class="step inner col @if($number_set > 9) selected done @else last @endif ">
                <div></div>
                  <p class="text-center colol-black">Delivered</p>
            </div>
        </div>
        </div>
        <div class="clearfix"></div>
    </div>
    @elseif ($date->RequestType == 'delivery')
    <style type="text/css">
    .row-warp.online-step .step.inner:after {bottom: 43px;}
    .row-warp.online-step .step:before{bottom: 43px;}
</style>

<?php
    $number_set = 1;
    if ($current_status == 'pending') {
        $number_set = 1;
    } else if ($current_status == 'ready') {
        $number_set = 2;
    } else if ($current_status == 'accepted') {
        $number_set = 3;
    } else if ($current_status == 'out_for_pickup') {
        $number_set = 4;
    } else if ($current_status == 'shipment_departed') {
        $number_set = 5;
    } else if ($current_status == 'out_for_delivery') {
        $number_set = 6;
    }else if($current_status == 'delivered'){
        $number_set = 7;
    }

    if ($current_status == 'cancel' && $RejectBy != '' ) {
        $number_set = 6;
    }
?>
    
        <div class="box-border">
            <h3 class="midHeading"><strong>Track Order</strong><br></h3>
            <br/>
            <div class="row-warp step_box online-step">
                <div class="step first col  @if($number_set > 0) selected @endif">
                    <div></div>
                    <p class="text-center colol-black">Pending</p>
                </div>

                <div class="step inner col  @if($number_set > 1) selected done @endif">
                    <div></div>
                    <p class="text-center colol-black">Paid</p>
                </div>

                <div class="step inner col @if($number_set > 2) selected done @endif ">
                    <div></div>
                    <p class="text-center colol-black">Listing Accepted</p>
                </div>
                
                <div class="step inner col @if($number_set > 3) selected done @endif ">
                    <div></div>
                    <p class="text-center colol-black">Out for Pickup</p>
                </div>
                
                <div class="step inner col @if($number_set > 4) selected done @endif ">
                    <div></div>
                    <p class="text-center colol-black">Shipment Departed</p>
                </div>

                <div class="step inner col @if($number_set > 5) selected done @endif ">
                    <div></div>
                    <p class="text-center colol-black">Out for Delivery</p>
                </div>
                
                <div class="step inner col @if($number_set > 6) selected done @else last @endif ">
                    <div></div>
                    <p class="text-center colol-black">Delivered</p>
                </div>
            </div>
        </div>
    @endif

</div>
</section>


<section class="container page">

    <div class="row mt-4">
        <div class="col-lg-8 col-sm-offset-2 mt-4 mb-4">
                <div class="track_box">
                    <div class="header_bg">
                    @if(input::get('package_id') == '')
                    Track Order
                    @else
                    {{ input::get('package_id') }}
                    @endif
                    </div>
                    <div class="sub-header mb-4">
                    <div class="div"> <div><p>Current Status: 
                    @if($date['ProductList'][0]['status'] == 'cancel' &&  @$date['ProductList'][0]['RejectBy'] != '')
                    Not Delivered
                    @else
                    {{ get_status_title($date['ProductList'][0]['status'],$date->RequestType)['status'] }}
                    @endif
                    </p>

                    @if(@$Activity['EnterOn']->sec != '')
                        {{ date('M  d, Y H:i:s A',@$Activity['EnterOn']->sec) }}
                    @endif

                    </div></div>
                     <div class="div"> <div>
                    <p>Expected Delivery</p> <small>
                    @if(isset($date['ProductList'][0]['ExpectedDate']))
                    {{ show_date($date['ProductList'][0]['ExpectedDate']) }}
                    @endif
                    </small>
                    </div></div>
                    </div>

                    @if(count($package) > 0)
                    @foreach($package as $value)

                        @if($date['RequestType'] == 'buy_for_me')
                            @if($value->status != 'reviewed')
                            <div class="flex">
                                <div class="date_flex">
                                    <p>{{date(' M  d, Y',$value->EnterOn->sec)}}</p>
                                    <small>{{date('H:i:s A',$value->EnterOn->sec)}}</small>
                                    <span class="circle-box active"></span>
                                </div>
                                <div class="des_flex">
                                    <p>
                                    {{ get_status_title($value->status,$date->RequestType)['status'] }}
                                    </p>
                                    <span>{{ ucfirst($value->message) }}</span>
                                </div>
                            </div>
                            @endif
                        @else
                        <div class="flex">
                            <div class="date_flex">
                                <p>{{date(' M  d, Y',$value->EnterOn->sec)}}</p>
                                <small>{{date('H:i:s A',$value->EnterOn->sec)}}</small>
                                <span class="circle-box active"></span>
                            </div>
                            <div class="des_flex">
                                <p>
                                {{ get_status_title($value->status,$date->RequestType)['status'] }}
                                </p>
                                <span>{{ ucfirst($value->message) }}</span>
                            </div>
                        </div>
                        @endif
                     @endforeach
                    @else
                    <div class="flex">
                      No Recored found.
                    </div>
                    @endif
                </div>
        </div>
    </div>
</section>
<script>

</script>
@endsection
