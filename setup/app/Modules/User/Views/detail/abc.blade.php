@extends('Page::layout.one-column-page')
@section('content')
{!! Html::script('theme/web/rating/jquery.rateyo.js') !!}
{!!Html::style('theme/web/rating/jquery.rateyo.min.css')!!}
<style>
   .modal-feedback{
   margin:30px auto;
   width:420px;
   position: relative;
   }
</style>
<div class="container">
<div class="">
   <div class="col-sm-12">
      <h2 class="color-blue mainHeading">Send A Package Detail</h2>
      <br>
   </div>
      <div class="col-md-12">
      <div class="box-shadow">
         <div class="content-section clearfix">
            <br>
            <div class="col-md-2 col-sm-2 col-xs-12">
                   @if($package->ProductImage != '')
                     <img src="{{ ImageUrl.$package->ProductImage}}" class="" width="150px" height="150px" >
                   @else
                      <img src="{{ ImageUrl}}no-image.jpg" width="150px" class="" height="150px"  class="img-rounded">
                   @endif
                  </div>
            <div class="col-md-10 col-sm-10 col-xs-12">
               <span class="pull-right">

               </span>

               <h4><b>{{ $package->ProductTitle }}</b></h4>
               <p class="color-black"><strong>Package No:</strong>{{ $package->PackageNumber }}</p>
               <p class="color-black"><strong>Status:</strong>
                  @if($package->Status == 'out_for_pickup')
                     Out for Pickup
                     @elseif($package->Status == 'out_for_delivery')
                     Out for Delivery
                     @else
                     {{ucfirst($package->Status)}}
                     @endif
                                 </p>
               <div class="form-group">

                                                   </div>
            </div>
         </div>
      </div>
   </div>
   <div class="clearfix"> </div>
<div class="col-sm-4 col-xs-12">
      </div>
<div class="col-sm-12 col-xs-12">
      <div class="clearfix margin-top detail-tab">
      <ul class="nav nav-tabs" role="tablist">
         <li role="presentation" class=""><a href="#profile" aria-controls="profile" class="active" role="tab" data-toggle="tab" aria-expanded="false">Delivery Information</a></li>
         <li role="presentation" class="active"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab" aria-expanded="true">Package Information</a></li>




      </ul>
      <div class="tab-content box-shadow">
         <div role="tabpanel" class="tab-pane" id="profile">
            <br>
            <p> </p>
            <p><b>Pickup Location: </b> {{ ucfirst($package->PickupFullAddress) }}</p>
            <p><b>Pickup Date: </b> {{ show_date($package->PickupDate) }}</p>
            <p><b>This is a Public Place:</b> Yes </p>
                        The Requester has opted to meet in public
                        <p><b>Drop off Location:</b> {{ $package->DeliveryFullAddress }}</p>
            <p><b>Drop off Date: </b>{{ show_date($package->DeliveryDate) }}</p>
            <p><b>Delivery Date Flexible: </b> No</p>
                        <p><b>Delivery Type:</b> One way</p>


            <p><b>Shipping Mode</b><small>(Not all items can be shipped by Air. Items shipped by sea typically take longer to arrive)</small><b>:</b> Air</p>

                        <p><b>Verification Code: </b>3552
                           </p><p><b>Return address (if item is not delivered): </b> Vijay Nagar Square, Abbeville, Alabama, USA - 12345</p>
         </div>
         <div role="tabpanel" class="tab-pane active" id="messages">
            <br>
            <p><b>Package Value:</b>${{ number_format($package->ProductCost,2) }}</p>
            <p><b>Weight:</b> {{ number_format($package->ProductWeight,2) }}  {{ $package->ProductWeightUnit }}</p>
            <p><b>Package Dimensions:</b>
               L-0.39Inch,
               H-1.18Inch,
               W-0.79Inch/
               L-1.00Cm,
               H-3.00Cm,
               W-2.00Cm            </p>
                        <p><b>Quantity:</b> {{ $package->BoxQuantity }} Box(es)</p>
                        <p><b>Description:</b> {{ ucfirst($package->Description) }}</p>
            <p><b>Package Category: </b> {{ ucfirst($package->Category) }}</p>
            <p><b>Receiver's Phone No. : </b>+{{ $package->ReceiverCountrycode }}- {{ $package->ReceiverMobileNo }}
            </p>
            <p><b>Need Package Material: </b>
            N/A            </p>
                        <p><b>Insurance:</b> {{ ucfirst($package->InsuranceStatus) }} </p>
            <p><b>Created Date:</b> {{ show_date($package->EnterOn) }} </p>
            <div class="list-footer">
               <div class="row">
                  <div class="col-sm-4">
                     <p><b>Shipping Cost: </b>
                        <span class="pull-right">${{number_format($package->ShippingCost,2) }}</span>
                     </p>
                      <b>Aquantuo Discount : </b>
                        <span class="pull-right"> $0.00</span>
                      <div></div><br>
                        <p><b>Total Cost:</b>
                        <span class="pull-right">${{ number_format($package->TotalCost,2) }} </span>
                     </p>
                  </div>
               </div>
            </div>
         </div>
         <div role="tabpanel" class="tab-pane" id="cancel_detail">
              <br><br>
              <div class="col-sm-12 text-right">
              <button class="btn btn-primary" id="print" onclick="print()"><i class="fa fa-print" aria-hidden="true"></i>
                Print</button>

                </div>
              <div class="details-list">
                <label for="return_address" class="control-label">Return Address:</label> Vijay Nagar Square, Abbeville, Alabama, USA - 12345
              </div>
              <div class="details-list">
                <label for="reject_by" class="control-label">Reject By:</label>

              </div>
              <div class="details-list">
                <label for="reject_by" class="control-label">Return Type:</label>

              </div>

              <div class="details-list">
                <p>
                  <b>Message: </b>
                </p>
              </div>


                            <br><br>
            </div>

             <div role="tabpanel" class="tab-pane" id="delivered">
                <div class="details-list">
                 <h3 class="text-primary">Pragya</h3>
                 <span class="pull-right"><b>Package Number:</b> 29471503036191</span>
                 </div>

                <div class="details-list" style="margin: 0px">
                <p style="margin: 0px"> <strong>Address: </strong>Vijay Nagar Square
                                  </p>
                     <span class="pull-right"><b>Delivered By:</b> </span>
                </div>




                <div class="details-list" style="margin: 0px">
                <p style="margin: 0px">
                     Abbeville
                     Alabama
                     USA
                     12345
                </p>
                </div>

                <div class="details-list" style="margin: 0px">
                  <p style="margin: 0px"> <strong>Created Date: </strong>Aug 18, 2017 11:33 AM
                  <span class="pull-right"><b>Delivered on:</b> Aug 18, 2017 01:26 PM</span>
                  </p>

                </div>
              <br>
              <table id="example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                <tbody><tr>
                <th>S.No.</th>
                <th>Name</th>
                <th style="text-align:right;">Qty</th>
                <th style="text-align:right;">Shipping Price</th>
                </tr>
                <tr>
                <td>1</td>
                <td>Test 18 Aug 2017</td>
                <td align="right">1 Box(es)</td>
                <td align="right"> $30.00</td>
                </tr>
                <tr>
                  <th colspan="3" class="text-right">Insurance Cost</th>
                  <td align="right">$0.00</td>
                </tr>
                <tr>
                  <th colspan="3" class="text-right">Total Cost</th>
                  <td align="right"><code>$30.00</code></td>
                </tr>
              </tbody></table>
              <!-- <div class="details-list">
                <label for="date" class="control-label">Delivery Request Date:</label>
                  Aug 18, 2017 11:33 AM
              </div>

              <div class="details-list">
                <p>
                  <b>Delivered To: </b> Pragya
                </p>
              </div>

              <div class="details-list">
                <p>
                  <b>Delivered By: </b>
                </p>
              </div>

              <div class="details-list">
                <p>
                                    <b>Delivered on: </b> N/A
                                  </p>
              </div>   -->




              <br><br>

            </div>

      </div>
   </div>
</div>
<div class="col-sm-12 col-xs-12">
   <div class=" box-shadow">
      <h2>Other Images</h2>
                  <center>
         <h4>No other image uploaded</h4>
      </center>
            <div class="clearfix"></div>
      <br><br>
   </div>
</div>
<div class="clearfix"></div>
<br>
<br>
<!--review-model-->
<div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
   <div class="pre-modal-dialog modal-md">
      <div class="modal-content">
         <div class="modal-header popup-bg text-center">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <p class="popup-head color-white">Rate your experience with transporter</p>
         </div>
         <div class="clearfix"></div>
         <center>
            <div class="modal-body text-left">
               <div id="success-message"> </div>
               <form method="POST" action="http://apis.aquantuo.com/delivery-details/5996831fcf32071b698b459b" accept-charset="UTF-8" name="RequestReview" id="RequestReview"><input name="_token" value="nePYogJGgKreSYxslwalP2JpSyLR900SR7NNoUVN" type="hidden"><div class="error-msg"></div>
               <div class="col-md-12">
                  <div class="col-md-12" align="center">
                     <div>
                                                <img src="http://apis.aquantuo.com/upload//user-no-image.jpg" class="img-circle" width="150px" height="150px">
                                             </div>
                     <br>
                     <div>
                        <label class="rating_margin">Rate Now :</label>
                        &nbsp;&nbsp;&nbsp;
                        <div class="rateyo jq-ry-container" id="rateyo" style="width: 160px;"><div class="jq-ry-group-wrapper"><div class="jq-ry-normal-group jq-ry-group"><!--?xml version="1.0" encoding="utf-8"?--><svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 12.705 512 486.59" x="0px" y="0px" xml:space="preserve" width="32px" height="32px" fill="gray"><polygon points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "/></svg><!--?xml version="1.0" encoding="utf-8"?--><svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 12.705 512 486.59" x="0px" y="0px" xml:space="preserve" width="32px" height="32px" fill="gray" style="margin-left: 0px;"><polygon points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "/></svg><!--?xml version="1.0" encoding="utf-8"?--><svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 12.705 512 486.59" x="0px" y="0px" xml:space="preserve" width="32px" height="32px" fill="gray" style="margin-left: 0px;"><polygon points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "/></svg><!--?xml version="1.0" encoding="utf-8"?--><svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 12.705 512 486.59" x="0px" y="0px" xml:space="preserve" width="32px" height="32px" fill="gray" style="margin-left: 0px;"><polygon points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "/></svg><!--?xml version="1.0" encoding="utf-8"?--><svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 12.705 512 486.59" x="0px" y="0px" xml:space="preserve" width="32px" height="32px" fill="gray" style="margin-left: 0px;"><polygon points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "/></svg></div><div class="jq-ry-rated-group jq-ry-group" style="width: 0%;"><!--?xml version="1.0" encoding="utf-8"?--><svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 12.705 512 486.59" x="0px" y="0px" xml:space="preserve" width="32px" height="32px" fill="#f39c12"><polygon points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "/></svg><!--?xml version="1.0" encoding="utf-8"?--><svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 12.705 512 486.59" x="0px" y="0px" xml:space="preserve" width="32px" height="32px" fill="#f39c12" style="margin-left: 0px;"><polygon points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "/></svg><!--?xml version="1.0" encoding="utf-8"?--><svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 12.705 512 486.59" x="0px" y="0px" xml:space="preserve" width="32px" height="32px" fill="#f39c12" style="margin-left: 0px;"><polygon points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "/></svg><!--?xml version="1.0" encoding="utf-8"?--><svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 12.705 512 486.59" x="0px" y="0px" xml:space="preserve" width="32px" height="32px" fill="#f39c12" style="margin-left: 0px;"><polygon points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "/></svg><!--?xml version="1.0" encoding="utf-8"?--><svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 12.705 512 486.59" x="0px" y="0px" xml:space="preserve" width="32px" height="32px" fill="#f39c12" style="margin-left: 0px;"><polygon points="256.814,12.705 317.205,198.566 512.631,198.566 354.529,313.435 414.918,499.295 256.814,384.427 98.713,499.295 159.102,313.435 1,198.566 196.426,198.566 "/></svg></div></div></div>
                        <div class="clearfix"></div>
                        <div id="er_rating" class="error-msg"></div>
                        <input name="request_id" id="request_id" value="5996831fcf32071b698b459b" type="hidden"><div class="error-msg"></div>
                        <input name="transporter_id" id="transporter_id" value="" type="hidden"><div class="error-msg"></div>
                        <br>
                        <script type="text/javascript">
                           $(document).ready(function() {
                           $(".fancybox").fancybox();
                           });
                           var $rateYo = $("#rateyo").rateYo();
                        </script>
                     </div>
                     <div class="col-md-12">
                        <div class="">
                           <div class="form-group txt-area feedback-text">
                              <textarea class="form-control required" placeholder="Feedback" id="review" maxlength="250" name="review" cols="30" rows="4"></textarea><div class="error-msg"></div>
                           </div>
                           <div id="er_review" class="help-block white"></div>
                           <div class="clearfix"></div>
                           <span id="review_msg"></span>
                        </div>
                     </div>
                     <div class="col-md-2"></div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="col-xs-12">
                     <div class="">
                        <div class="col-xs-6">
                           <div class="form-group">
                              <button type="submit" class="custom-btn1 btn-block" id="ratingButton">SUBMIT</button>
                              <div class="error-msg"></div>
                           </div>
                        </div>
                        <div class="col-xs-6">
                           <div class="form-group text-center">
                              <button class="custom-btn1 btn-block" data-dismiss="modal">NOT NOW</button><div class="error-msg"></div>
                           </div>
                        </div>
                     </div>
                  </div>

               </div></form>
         </div></center>
         <div class="clearfix"></div>
         </div>
      </div>
      <!--end-->
         </div>
</div>
<style>
   .pre-modal-dialog {
   margin: 30px auto;
   width: 440px;
   }
   .transporter-height{
   height:300px;
   }
</style>
<script type="text/javascript">
new Validate({
  FormName : 'RequestReview',
  ErrorLevel : 1,
  validateHidden: false,
  callback : function(){
        if($rateYo.rateYo("rating") &lt;= 0) {
           $('#er_rating').html('The rating field is required.');
           return false;
            }
           $("#ratingButton").addClass('spinning');
           $.ajax({

           url     : 'http://apis.aquantuo.com/requester-review',
           type    : 'post',
           data    : "review="+$("#review").val()+
                     "&amp;request_id="+$('#request_id').val()+
                     "&amp;transporter_id="+$('#transporter_id').val()+
                     "&amp;rating="+$rateYo.rateYo("rating"),
           success : function(res){
               $("#ratingButton").removeClass('spinning');
                   var obj = eval("("+res+")");
           if(obj.success == 1)
           {
             $("#feedback").hide();
             location.reload();
             $("#RequestReview").trigger( "reset" );
             $('#success-message').html('&lt;div role="alert" class="alert alert-success alert-dismissible fade in"&gt;'
         +'&lt;button aria-label="Close" data-dismiss="alert" class="close" type="button"&gt;&lt;span aria-hidden="true"&gt;×&lt;/span&gt;&lt;/button&gt;'+obj.msg+'&lt;/div&gt;');

           }
           else
           {
             $("#modal2").trigger('click' );
             $("#feedback").hide();
             $('#success-message').html('&lt;div role="alert" class="alert alert-danger alert-dismissible fade in"&gt;'
         +'&lt;button aria-label="Close" data-dismiss="alert" class="close" type="button"&gt;&lt;span aria-hidden="true"&gt;×&lt;/span&gt;&lt;/button&gt;'+obj.msg+'&lt;/div&gt;');
           }

                  }
               });
      }

    });

    /*$(document).ready(function() {
      $('#example').DataTable();
    } );*/

function print() {
  document.getElementById("print").innerHTML ="Page location is: " + window.location;
}

</script>
    </div>
@endsection
<?php
function change_unit($weight, $currentunit, $changeto) {

	//  echo $weight." ".$currentunit." ".$changeto; //die;

	//  echo "<br>";

	$newweight = '';
	$newchangeto = $changeto;
	switch ($currentunit) {
	case 'kg':
		$newweight = $weight * 1000;
		break;
	case 'lbs':
		$newweight = $weight * 0.00220462;
		break;
	case 'inches':
		$newweight = $weight;
		break;
	case 'cm':
		$newweight = $weight * 0.393701;
		break;
	}

	switch ($changeto) {
	case 'kg':
		$newweight = $newweight / 1000;
		break;
	case 'lbs':
		$newweight = $newweight * 0.00220462;
		break;
	case 'cm':
		$newweight = $newweight / 0.393701;
		break;
	case 'inches':
		$newweight = $newweight;
		break;
	}

	return $newweight;

}

?>

