@extends('Page::layout.one-column-page')
@section('page_title')
Settings - Aquantuo
@endsection
@section('content')

<div class="container">
<div class="row">
   <div class="col-sm-12">
   	   <h2 class="color-blue mainHeading">General Settings</h2>
   	   <br />
   </div>

      <div class="col-md-9 col-sm-8 col-xs-12">
         <div class="box-shadow tabing_sys">
           
            <div class="row">
            
               <div class="col-md-6 col-xs-12 col-sm-6">
                  <h4 class="toggle">ENABLE NOTIFICATIONS</h4>
               </div>
               <div class="col-md-6 col-xs-12 col-sm-6">
                  <div class="custom-tab-new toggle-tap clearfix pull-right">
                 
                       <input type="hidden" value="{{$users->NoficationStatus}}" name="notify" id="notify">
					  
                     <div class="option-tab @if($users->NoficationStatus == 'on') on @else off @endif option1 " id="notify_status" value="on" onclick="$('#notify').val('on');email_status()">On</div>
                     <div class="option-tab @if($users->NoficationStatus == 'on') off @else on @endif option2 " id="notify_status1" value="off" onclick="$('#notify').val('off');email_status()">Off</div>
                     
                     <div class="slide-bg @if($users->NoficationStatus == 'on') left @else right @endif">&nbsp;</div>
                  </div>
               </div>
             
            </div>
            <div class="row">
               <div class="col-md-6 col-xs-12 col-sm-6">
                  <h4 class="toggle">ENABLE EMAILS</h4>
               </div>
               <div class="col-md-6 col-xs-12 col-sm-6">
               	  <div class="custom-tab-new toggle-tap  pull-right">
					           <input type="hidden" value="{{$users->EmailStatus}}" name="emailt_notify" id="emailt_notify">
					  
                     <div class="option-tab  @if($users->EmailStatus == 'on') on @else off @endif option1 " id="email_status" value="on" onclick="$('#emailt_notify').val('on');email_status()">On</div>
                     <div class="option-tab  @if($users->EmailStatus == 'on') off @else on @endif option2 " id="email_status1" value="off" onclick="$('#emailt_notify').val('off');email_status()">Off</div>
                     <div class="slide-bg @if($users->EmailStatus == 'on') left @else right @endif ">&nbsp;</div>
                  </div>
               </div>
            </div>

            <div class="row">
               <div class="col-md-6 col-xs-12 col-sm-6">
                  <h4 class="toggle">CONSOLIDATE PACKAGE</h4>
               </div>
               <div class="col-md-6 col-xs-12 col-sm-6">
                  <div class="custom-tab-new toggle-tap  pull-right">
                     <input type="hidden" value="{{$users->consolidate_item}}" name="consolidate_item" id="consolidate_item">
            
                     <div class="option-tab  @if($users->consolidate_item == 'on') on @else off @endif option1 " id="consolidate_status" value="on" onclick="$('#consolidate_item').val('on');email_status()">On</div>

                     <div class="option-tab  @if($users->consolidate_item == 'on') off @else on @endif option2 " id="consolidate_status1" value="off" onclick="$('#consolidate_item').val('off');email_status()">Off</div>

                     <div class="slide-bg @if($users->consolidate_item == 'on') left @else right @endif ">&nbsp;</div>
                  </div>
               </div>
            </div>

            
            <div class="col-sm-12">
               <hr />
            </div>
            <div class="col-sm-12">
               <div class="form-group clearfix">
                  <a  href="{{url('delete_user')}}/{{$users->_id}}" onclick = "return confirm('Are you sure, you want to delete this account')" >
                     <div class="custom-btn1 btn text-center" >
                     Delete Account
                     <div class="custom-btn-h"></div>
                     </div>
                  </a>
               </div>
               <p></p>
            </div>
            <p></p>
         </div>
      </div>
    @include('Page::layout.side_bar')
   </div>
</div>
<script>
   $(".option1").click(function(){
   		 $(this).siblings(":last").removeClass("right");
   		 $(this).siblings(":last").addClass("left");
   		$(this).addClass("on");
   		$(this).removeClass("off");
   		$(this).siblings().addClass("off");
   		$(this).siblings().removeClass("on");
   	});
   $(document).ready(function(e) {
      $(".option1").click(function(){
   		 $(this).siblings(":last").removeClass("right");
   		 $(this).siblings(":last").addClass("left");
   		$(this).addClass("on");
   		$(this).removeClass("off");
   		$(this).siblings().addClass("off");
   		$(this).siblings().removeClass("on");
   	});
   $(".option2").click(function(){
   		$(this).siblings(":last").removeClass("left");
   		$(this).siblings(":last").addClass("right");
   		$(this).addClass("on");
   		$(this).removeClass("off");
   		$(this).siblings().addClass("off");
   		$(this).siblings().removeClass("on");
   		
   	});
     });
     
     function email_status()
     {
		 var flag= true;
			if(flag==true)
		{ 
      
			$.ajax({
			url: 'email-status',
			data: {    "EmailStatus": $('#emailt_notify').val(),
			           "NoficationStatus": $('#notify').val(),
                    "consolidate_item": $('#consolidate_item').val(),      
				  },
            type : 'post',
            dataType: 'json',
            success : function(obj) {
                if(obj.success == 1) {
                   // document.getElementById("item_loader").value = "Submit";
                  //  document.getElementById("buyforme").reset();
                    alert(obj.msg);     
                
                    
				}
                
			}
        });
    }

return false;
}  
	 
</script>
@endsection
<style>
   #option1 > input {
   display: none;
   }
   #option2 > input {
   display: none;
   }
</style>

