@extends('Page::layout.one-column-page')
@section('page_title')
My Shipping Request Detail - Aquantuo
@endsection
@section('content')
{!! Html::script('theme/web/rating/jquery.rateyo.js') !!}
{!!Html::style('theme/web/rating/jquery.rateyo.min.css')!!}
<?php $total_item_price = 0;

$total_item_cost = 0;
foreach ($item->ProductList as $key => $value) {
    $total_item_cost = $total_item_cost + ($value['price'] * $value['qty']);
}

?>
<div class="container">
    <div class="">
    <h2 class="color-blue mainHeading">Online Request Detail</h2>
    <br />

    <?php /*  echo "<pre>"; print_r($item);  die; */?>
      <div class="box-shadow">
                              <!------------------step1------------------>
    			<div class="box-header">
					{{ucfirst($item->ProductTitle)}}<span class="pull-right"><i class="fa fa-arrow-left" aria-hidden="true"></i>
					<a href="{{url('my-request')}}" style="color:#FFFFFF;">Back</a></span>
                </div>
                <div class="content-section row clearfix">
					<div class="col-sm-12">

                @if(isset($item->EnterOn->sec))
								<p><b>Created on :&nbsp;</b> {{ show_date($item->EnterOn,'l M d, Y') }}</p>
							@endif
							<p><b>Ship items to your Aquantuo address :&nbsp;</b><?php echo ucfirst(Session()->get('AqAddress')); ?></p>

               @if($item->Status == 'delivered')
                     <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal">View Recipt</button>
                @endif
					</div>
					<div class="col-sm-12">

					</div>
      </div>
                <br />
      <div class="row">
          <?php $sno = 0;?>
          @foreach($item->ProductList as $value)

      <div class="">
				<div class="col-sm-6 col-xs-12">
                <p><b>Package Id :&nbsp;</b>{{ucfirst($value['package_id'])}}</p>
                <p><b>Status :&nbsp;</b>
                @if($value['status'] == 'not_purchased')
                    Waiting for your payment
                @elseif($value['status'] == 'cancel')
                  Canceled
                @elseif($value['status'] == 'purchased')
                  Received
                @else
                  {{ get_status( $value['status'], $item->RequestType ) }}
                @endif
                </p>
                
            </div>
            <div class="col-sm-6 col-xs-12">
                <p><b>Tracking Number : </b>
                  @if(empty(trim(@$value['tracking_number'])))
                    N/A
                  @else
                    {{$value['tracking_number']}}
                  @endif
                </p>
                  <p><b>Insurance Status : </b>{{ucfirst($value['insurance_status'])}}</p>
              {{--   <p><b>Description : </b>{{ucfirst($value['description'])}}</p> --}}

                </div>

                  <div class="col-sm-12 col-xs-12">
                   <p><b>Purchased From : </b> {{$value['url']}} </p>
                  </div>

                   <div class="col-sm-12 col-xs-12">
                    <p><b>Description : </b>{{ucfirst($value['description'])}}</p>
                  </div>

       <div class="col-sm-6 col-xs-12">

                @if($value['TransporterRating'] != '')
                  <p><b>Transporter Feedback :</b> {{ucfirst($value['TransporterFeedbcak'])}} </p>
                  <p><b><span class="pull-left">Rating : </span> </b>

                      <div class="star-ratings-sprite pull-left">
                        <span style="width:<?php echo $value['TransporterRating'] * 20; ?>%" class="star-ratings-sprite-rating"></span>
                     </div>
                     </br>
                  </p>
                  @endif

                  @if($value['RequesterRating'] != '')
                  <p><b>Your Feedback :</b> {{ucfirst($value['RequesterFeedbcak'])}} </p>
                  <p><b><span class="pull-left">Rating : </span> </b>

                      <div class="star-ratings-sprite pull-left">
                        <span style="width:<?php echo $value['RequesterRating'] * 20; ?>%" class="star-ratings-sprite-rating"></span>
                     </div>
                     </br>
                  </p>
                  @endif

				</div>

         <!--image -->
               @foreach($user_data as $key)
               @if($key['_id'] == $value['tpid'])
               <div class="col-sm-6">
                     <div class="media">
                    <div class="form-group media-left">
                     @if($key->Image != '')
                        <a class="fancybox" rel="group" href="{{ ImageUrl.$key->Image}}" >
                            <img src="{{ ImageUrl.$key->Image}}"  width="100px" height="100px" class="img-circle">
                          </a>
                      @else
                          <img src="{{ImageUrl}}/user-no-image.jpg" width="100px" height="100px" class="img-circle">
                      @endif
                    </div>
                    <div class="media-body">
                    <br>
                    <b style="margin-left:13px">Transporter Name: </b>{{ucfirst($key->Name)}}
                    <br><br>
                       @if(count($key) > 0)
                        <?php $average_rating = 0;
if ($key->RatingCount > 0 && $key->RatingByCount > 0) {
    $rating = $key->RatingCount / $key->RatingByCount;
    $average_rating = 20 * $rating;
}
?>
                     <div class="col-sm-12">
                    <div class="star-ratings-sprite pull-left" >
                      <span style="width:{{ $average_rating }}%" class="star-ratings-sprite-rating"></span>
                    </div>
                     </div>
                  @endif
                    </div>
                 </div>
               </div>
               @endif
              @endforeach
               <!-- end image -->
      </div>

        <div class="col-sm-12">
        </br>
                <table class="custom-table table table-bordered">
                    <thead>
                        <tr>
										      <th> Image</th>
                          <th> Name</th>
                          <th> Category</th>
                          <th>Shipping Mode</th>
                          <th>Weight</th>
                          <th>Height</th>
                          <th>Width</th>
                          <th>Length</th>
                          <th>Quantity</th>
                          <th>Verification Code</th>
                          <th> Price</th>

                        </tr>
                    </thead>
                        <tbody>
                            <tr>
              									<td>
                                  @if($value['image'] != '')
                                    <img src="{{ ImageUrl.$value['image']}}" width="130px" height="80px" >
                                  @else
                                    <img src="{{ ImageUrl}}/no-image.jpg" width="90px" height="70px" >
                                  @endif
                                </td>
              									<td>{{ucfirst($value['product_name'])}}</td>
              									<td>{{$value['category']}}</td>
              									<td>{{ucfirst($value['travelMode'])}}</td>
              									<td>
                                  @if($value['weight'] != '')
                                    {{$value['weight']}} {{ucfirst($value['weight_unit'])}}
                                  @else
                                    N/A
                                  @endif
                                </td>

                                <td>
                                  @if($value['height'] != '')
                                    {{$value['height']}} {{ucfirst($value['heightUnit'])}}
                                  @else
                                    N/A
                                  @endif
                                </td>

                                <td>
                                  @if($value['width'] != '')
                                    {{$value['width']}} {{ucfirst($value['widthUnit'])}}
                                  @else
                                    N/A
                                  @endif
                                </td>

                                <td>
                                  @if($value['length'] != '')
                                    {{$value['length']}} {{ucfirst($value['lengthUnit'])}}
                                  @else
                                    N/A
                                  @endif
                                </td>



              									<td>{{$value['qty']}}</td>
              									<td>{{$value['verify_code']}}</td>
              									<td>${{number_format($value['price'],2)}}</td>

                              </tr>
                          </tbody>
                  </table>

           <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">View Recipt</h4>
        </div>
        <div class="modal-body">
            <div class="details-list" style="margin: 0px">
                 <h4 style="margin: 0px" class="text-primary">  {{ucfirst($item->RequesterName)}} </h4><br>
                  <span class="pull-right"><b>Package Number: </b> {{$item->PackageNumber}} </span>
                <p style="margin: 0px"> <strong>Address: </strong>{{ucfirst($item->DeliveryAddress)}}</p>
                 @foreach($user_data as $key)
                    @if($key['_id'] == $value['tpid'])
                    <span class="pull-right"> <b>Delivered By: </b>{{ucfirst($key->Name)}}</span>
                    @endif
                 @endforeach
                </div>


                <div class="details-list" style="margin: 0px">
                <p style="margin: 0px">
                    {{ucfirst($item->DeliveryCity)}}
                    {{ucfirst($item->DeliveryState)}}
                    {{ucfirst($item->DeliveryCountry)}}
                    {{ucfirst($item->DeliveryPincode)}}
                </p>
                </div>
              <br>
              <table id="example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                <tr>
                <th>S.No.</th>
                <th>Name</th>
                <th style="text-align:right;">Qty</th>
                <th style="text-align:right;">Price</th>
                </tr>
               <?php $sno = 1;?>
              @foreach($item->ProductList as $value)
                <tr>
                <td><?php echo $sno++; ?></td>
                <td>{{ucfirst($value['product_name'])}}</td>
                <td align="right">{{$value['qty']}} Box(es)</td>
                <td align="right" > ${{number_format($value['price'],2)}}</td>
                </tr>
               @endforeach

              </table>
                @if(!$item->shippingCost == '')
                <div class="details-list" style="margin: 0px">
                 <div class="col-sm-4">
                    <strong>Shipping Cost:</strong>
                 </div>
                 <div class="col-sm-8">
                   ${{number_format($item->shippingCost,2)}}
                </p>
                 </div>
                </div>
                  @endif

                  @if($item->insurance > 0)
                   <div class="details-list" style="margin: 0px">
                 <div class="col-sm-4">
                    <strong>Insurance Cost:</strong>
                 </div>
                 <div class="col-sm-8">
                     ${{number_format($item->insurance,2)}}
                </p>
                 </div>
                </div>
                @endif

                  @if($item->discount > 0)
                  <div class="details-list" style="margin: 0px">
                 <div class="col-sm-4">
                    <strong>Aquantuo Discount:</strong>
                 </div>
                 <div class="col-sm-8">
                  (-)${{number_format($item->discount,2)}}
                </p>
                 </div>
                </div>
                @endif

                 <hr></hr>
                  <div class="details-list" style="margin: 0px">
                 <div class="col-sm-4">
                    <strong>Total Price:</strong>
                 </div>
                 <div class="col-sm-8">
                ${{number_format((($item->shippingCost + $item->insurance) - $item->discount),2)}}
                </p>
                 </div>
                </div>
                                  <br /><br /></br></br></br>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>
  <!-- endrecipt -->

                   <p class="pull-right">
                    @if(in_array($value['status'] , ['out_for_delivery','out_for_pickup','assign']))
                      <a class="btn btn-primary" href="javascript:void(0)" id="chat-loader-{{$value['tpid']}}" onclick="open_chat_box('{{$value['tpid']}}','{{$item->TransporterName}}','chat-loader-{{$value['tpid']}}')"><i class="fa fa-chat"></i> Chat</a>
                    @endif
                   </p>

                   @if($value['RequesterFeedbcak'] == '' &&  $value['status'] == 'delivered')
                <div class="pull-right"> <a href="javascript:void(0)" data-toggle="modal" data-target="#modal2" title="Review"  class="btn btn-default blue-btn" id= "feedback" onclick="give_feedback('{{$value['_id']}}')" style="">Give Feedback</a></div>
               @endif




@if($value['status'] == 'cancel')
<h3 class="">Cancel Information</h3>
<div class="row">
  <div class="col-md-6">
    <p><b>Return Address: </b> {{ucfirst(@$item['ReturnAddress'])}} </p>

    <p><b>Reject By: </b> {{ucfirst(@$value['RejectBy'])}} </p>
    <p><b>Return Type: </b>
    @if(@$value['ReturnType'] == 'osreturn')
      OS Return
    @else
      {{ucfirst(@$value['ReturnType'])}}
    @endif

     </p>
    @if(!empty(trim($value['TrackingNumber'])))
    <p><b>Tracking Number: </b> {{@$value['TrackingNumber']}} </p>
    @endif

  </div>
  <div class="col-md-6">
    <p><b>Message: </b> {{ucfirst(@$value['TransporterMessage'])}} </p>

    @if(isset($value['CancelReturnDate']->sec))
      <p><b>Date: </b>  {{date('d M ,Y  h:i A',$value['CancelReturnDate']->sec)}} </p>
    @endif
    @if(!empty(trim($value['ReceiptImage'])))
      @if(file_exists(BASEURL_FILE.$value['ReceiptImage']))
        <p><b>Receipt: <br /></b>
            <a class="fancybox" rel="group" href="{{ImageUrl.$value['ReceiptImage']}}" >
              <img src="{{ImageUrl.$value['ReceiptImage']}}" alt="receipt" height="80" width="80" >
            </a>
        </p>
      @endif
    @endif

  </div>
</div>
@endif





                        <p class="col-sm-6">
                        @if(!$value['RequesterFeedbcak'] == '')
                          <b>Requester Feedback :</b>
                            {{ucfirst($value['RequesterFeedbcak'])}}

                        @endif
                        </p>
                          <p class="col-sm-12"></p>

                        <div class="clearfix"></div>

                        <hr>
                      </div>
                    <?php $sno++;?>
                  @endforeach
                </div>

                    <div class="list-footer clearfix">
                    	<div class="row">
                      <div class="col-sm-4">

                <p><b>Total Number of Item(s) : </b>
                  <?php $sno2 = 0;?>
                    @foreach($item->ProductList as $value)
                      <?php $sno2++;?>
                    @endforeach

                  <?php echo $sno2; ?>
                </p>
                <p><b>Consolidate Shipping :</b>@if($item->consolidate_item == 'yes') Yes @else No @endif</p>
                <p><b>Recipient's Phone Number :</b> @if($item->country_code != '') {{$item->country_code}} - @endif{{$item->ReceiverMobileNo}}</p>
                <p><b>Distance :</b>{{number_format($item->distance,2)}} Miles</p>
                <p><b>Delivery Address : </b>{{ucfirst($item->DeliveryFullAddress)}}
                 </p>
                  <p><b>Delivery Date : </b>
                  @if(isset($item->DeliveryDate->sec))
                     {{ show_date($item->DeliveryDate,'l M d, Y') }}
                  @endif
                  </p>


							</div>
							<div class="col-sm-3">

							</div>

							<div class="payment-sec col-sm-4 pull-right">
								<h3 class="">Payment Info</h3>
                                	<table class="table table-bordered">
  										<tbody bgcolor="#FFFFFF">

                      <tr>
                        <td class="">
                          <div class="col-xs-8 text-left">
                            <label class="" for="exampleInputEmail1"><b>Shipping Cost:</b> </label>
                          </div>
                          <div class="col-xs-4 text-right">
                            @if(!$item->shippingCost == '')
                             ${{number_format($item->shippingCost,2)}}
                            @endif

                          </div>
                        </td>
                      </tr>


                      @if($item->insurance > 0)
                      <tr>
                        <td class="">
                          <div class="col-xs-8 text-left">
                            <label class="" for="exampleInputEmail1"><b>Insurance Cost:</b> </label>
                          </div>
                          <div class="col-xs-4 text-right">
                            (+)${{number_format($item->insurance,2)}}
                          </div>
                        </td>
                      </tr>

                      @endif


                      <tr>
                        <td class="">

                            <div class="col-xs-8 text-left">
                              <label class="" for="exampleInputEmail1"><b>Aquantuo Discount:</b>
                               </label><br/>
                                 @if(!$item->PromoCode == '')
                                 <small>(<b>Promocode:</b>{{$item->PromoCode }}) </small>
                                 @endif
                            </div>
                            <div class="col-xs-4 text-right">
                            @if($item->discount > 0)
                              (-)${{number_format($item->discount,2)}}
                            @else
                              $0.00
                            @endif
                            </div>
                        </td>
                      </tr>

                      <tr>
												<td class="">
													<div class="col-xs-8 text-left">
														<label class="" for="exampleInputEmail1"><b>Total Cost:</b> </label>
													</div>
													<div class="col-xs-4 text-right">

                            ${{number_format((($item->shippingCost + $item->insurance) - $item->discount),2)}}

													</div>
											    </td>
											</tr>



                                        </tbody>
									</table>




                  <br>
                  @if($item['Status'] == 'not_purchased')
                  <table class="table table-bordered" style="margin:0;">
                     <tbody bgcolor="#FFFFFF">
                       <tr>
                           <td class="">
                              <div class="col-xs-8 text-left">
                                 <label class="" for="exampleInputEmail1">
                                    <b>Before Purchase Cost: <br />
                                    </b>
                                 </label>
                              </div>
                              <div class="col-xs-4 text-right">
                                 @if($item['BeforePurchaseTotalCost'] != '')
                                 ${{number_format($item['BeforePurchaseTotalCost'],2)}}
                                 @else
                                 $0.00
                                 @endif
                              </div>
                           </td>
                        </tr>


                        <tr>
                           <td class="">
                              <div class="col-xs-8 text-left">
                                 <label class="" for="exampleInputEmail1">
                                    <b style="color:green;">Payable Difference: <br />
                                    </b>
                                 </label>
                              </div>
                              <div class="col-xs-4 text-right">
                                 @if($item['BeforePurchaseTotalCost'] != ''  &&  $item['TotalCost'] != '')
                                 <?php $dif = $item['TotalCost'] - $item['BeforePurchaseTotalCost'] ?>
                                 ${{number_format($dif,2)}}
                                 @else
                                 $0.00
                                 @endif
                              </div>
                           </td>
                        </tr>
                     </tbody>
                  </table>
                  @endif

                  <br>
                  @if($item['Status'] == 'not_purchased')
                  <button  class="btn btn-primary" type="button" class="btn btn-primary blue-btn"   data-toggle="modal" data-target="#exampleModal">Pay Now</button>
                  @endif

                            </div>
                        </div>
                    </div>
                </div>

        </div>
</div>


<!--pay now modal-->
<div aria-labelledby="exampleModalLabel" role="dialog" tabindex="-1" id="exampleModal" class="modal fade in">
   <div role="document" class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header" id="Addaddress2">
            <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button>
            <h4 id="exampleModalLabel" class="modal-title">Quote</h4>
         </div>
         <br >
         {!! Form::open(['url' => 'online-payment/'.Request::segment(2),'method' => 'get']) !!}
         <div class="modal-body" >
            <div class="col-md-12">
               <div class="row">
                  <div class="col-xs-6">
                     <b>No. of items</b>
                  </div>
                  <div class="col-xs-6 text-right">
                    <?php $sno3 = 0;?>
                    @foreach($item->ProductList as $value)
                      <?php $sno3++;?>
                    @endforeach
                    <p><?php echo $sno3; ?></p>

                  </div>
               </div>
               <div class="row">
                  <div class="col-xs-6">
                     <b>Int'l Shipping Cost</b>
                  </div>
                  <div class="col-xs-6 text-right">
                     <p>${{number_format($item->shippingCost,2)}}</p>
                  </div>
               </div>

               @if($item->insurance > 0)
               <div class="row">
                  <div class="col-xs-6">
                     <b>Insurance Cost</b>
                  </div>
                  <div class="col-xs-6 text-right">
                     <p>${{number_format($item->insurance,2)}}</p>
                  </div>
               </div>
               @endif

               @if($item->discount > 0)
               <div class="row">
                  <div class="col-xs-6">
                     <b>Discount: </b>
                  </div>
                  <div class="col-xs-6 text-right">
                     <p>${{number_format($item->discount,2)}}</p>
                  </div>
               </div>
               @endif
               <div class="row">
               <hr />
                  <div class="col-xs-6">
                     <b>Total Cost</b><br />
                  </div>
                  <div class="col-xs-6 text-right">
                     <p>${{number_format($item->TotalCost,2)}}</p>
                  </div>
               </div>
               @if($item->BeforePurchaseTotalCost < $item->TotalCost)
               <hr />
               <div class="row">
                  <div class="col-xs-6"> <b>Already Paid</b> </div>
                  <div class="col-xs-6 text-right">
                     <p>${{number_format($item->BeforePurchaseTotalCost,2)}}</p>
                  </div>
               </div>
               <hr />
               <div class="row">
                  <div class="col-xs-6">
                     <b>Remaining Payment</b><br />
                  </div>
                  <div class="col-xs-6 text-right">
                     <p>${{number_format(($item->TotalCost - $item->BeforePurchaseTotalCost),2)}}</p>
                  </div>
               </div>
               @endif
            </div>
         </div>
         <div class="clearfix"></div>
         <hr>
         <div class="text-right">
            <button type="submit"  class="btn btn-default blue-btn "  title="Accept" value="Accept">Accept</button>
            <button type="button"  class="btn btn-default blue-btn "  title="Cancel" id="cancel">
            &nbsp; Cancel &nbsp;&nbsp;</button>&nbsp;&nbsp;
         </div>
         {!! Form::close() !!}
         <br/>
      </div>
   </div>
</div>
<!--end modal-->
{!! Html::script('theme/web/js/validation.js') !!}



<div aria-labelledby="exampleModalLabel" role="dialog" tabindex="-1" id="exampleModal" class="modal fade in">
   <div role="document" class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header" id="Addaddress2">
            <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button>
            <h4 id="exampleModalLabel" class="modal-title">Payment</h4>
         </div>
         {!! Form::model('', ['name' => 'new_address', 'id' => 'Addaddress', 'method' => 'POST']) !!}
            <div class="panel-body">
               <div class="row">
                  <div class="col-sm-12">
                     <div class="form-group">
                        <label>Address Line1</label>
                        <input type="text" placeholder="Address" name="address_line_1" id="address_line_1"  maxlength= "125" class="form-control required">
                     </div>
                  </div>
                  <div class="col-sm-12">
                     <div class="form-group">
                        <label>Address Line2</label>
                        <input type="text" placeholder="Address" name="address_line_2" id="address_line_2"  maxlength= "125" class="form-control">
                     </div>
                  </div>



                  <div class="col-sm-6">
                     <div class="form-group">
                        <label>Zipcode</label>
                        <input type="text" placeholder="Zipcode" name="zipcode" id="zipcode"  maxlength="6" class="form-control">
                     </div>
                  </div>
                  </hr>
                  <div class="col-sm-8 col-sm-offset-2" align="center">
                      <div class="row">
                  <div class="col-sm-6 col">
                     <div class="form-group">
                        <button class="custom-btn1 btn-block" id="address_loader" >
                           Submit
                           <div class="custom-btn-h"></div>
                        </button>
                     </div>
                  </div>
                     <div class="col-sm-6 col">
                     <div class="form-group">
                        <button class="custom-btn1 btn-block"  data-dismiss="modal">
                           Close
                           <div class="custom-btn-h"></div>
                        </button>
                     </div>
                  </div>
                  </div>
                  </div>
               </div>
            </div>
         {!! Form::close() !!}
      </div>
   </div>
</div>

<!--review-model-->
<div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
      <div class="pre-modal-dialog modal-md">
        <div class="modal-content">
          <div class="modal-header popup-bg text-center">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <p class="popup-head color-white">Rate your experience with transporter</p>

          </div>
          <div class="clearfix"></div>
        <!-- <input type="hidden" name="item_id"  id="item_id" value="<?php /*echo $item->_id   */?>" >   -->
        <center>
         <div class="modal-body text-left">
        <div id="rating_success_container"></div>
        <div id="rating_danger_container"></div>
            {!! Form::model('', ['name' => 'RequestReview', 'id' => 'RequestReview']) !!}
            <div class="col-md-12">
               <div class="col-md-12" align="center">
               <div>

                          <img src="{{ImageUrl}}/user-no-image.jpg" width="150px" height="150px" class="img-circle">

               </div>
               </br>
               <div>
                  <label class="rating_margin" >Rate Now :</label>
                     &nbsp;&nbsp;&nbsp;
                     <div class="rateyo required" id="rateyo" ></div>
                     <div class="clearfix"></div>
                     <div id="er_rating"  class="error-msg"></div>
                 <input type="hidden" name="tpid" id="tpid" value="{{$value['tpid']}}">
                 <input type="hidden" name="item_id" id="item_id" value="{{$value['_id']}}">
                     </div>
                     </br>
                  </div>

                  <div class="col-md-12">
                   <div class="">
                     <div class="form-group txt-area feedback-text">
                        {!! Form::textarea('review','',['class'=>'form-control required', 'placeholder'=> 'Feedback','id' =>'review','maxlength' =>250,'size' =>'30x4']) !!}
                     </div>
                   <div id="er_review" class="help-block white"></div>
                   <div class="clearfix"></div>
                     <span id="review_msg"></span>
                  </div>
               </div>
               <div class="col-md-2"></div>
            </div>
             <div class="clearfix"></div>
                    <div class="col-xs-6">
                       <div class="form-group">
                          <button type="submit" class="custom-btn1 btn-block" id='ratingButton'>SUBMIT</button>
                       <div class="error-msg"></div></div>
                    </div>
                    <div class="col-xs-6">
                       <div class="form-group text-center">
                          <button class="custom-btn1 btn-block" data-dismiss='modal' >NOT NOW</button>
                       </div>
                    </div>


                  {!! Form::close() !!}

          <div class="clearfix"></div>

        </div>
      </div>


<!--end-->
<?php
function custome_date_format($d, $timestamp = false)
{
    $date = "N/A";

    $ts = new DateTime('now', new DateTimeZone('GMT'));

    if ($d != "") {
        try
        {
            if (!$timestamp) {
                if (isset($d->sec)) {
                    $d = $d->sec;
                } else {
                    $d = strtotime($d);
                }
            }

            $ts->setTimestamp($d);
            $ts->add(DateInterval::createFromDateString($_COOKIE['tzo'] . ' minutes'));
            return $ts->format('M d, Y') . " " . $ts->format('h:i A');
        } catch (exception $e) {

        }
        /*print_r($d);
    if(isset($d->sec)){
    $ts->setTimestamp($d->sec);
    return $ts->format('M d, Y')." at ".$ts->format('h:i A');
    //$date = date('M d, Y', $d->sec)." at ".date('h:i A', $d->sec);
    }
    else{
    return date('M d, Y', strtotime($d))." at ".date('h:i A', strtotime($d));
    }*/
    }
    //return $date;
}

?>





<script>

$(document).ready(function() {
  $(".fancybox").fancybox();
});

$('#cancel').click(function(){
  $('#exampleModal').trigger("click");
});

new Validate({
      FormName : 'RequestReview',
      ErrorLevel : 1,
      validateHidden: false,
      callback : function()
    {
      if($rateYo.rateYo("rating") <= 0) {
        $('#er_rating').html('The rating field is required.');
        return false;
      }
        $("#ratingButton").addClass('spinning');
        $.ajax({
          url     : '{{url("/online-requester-review")}}',
          type    : 'post',
          data    : {
              "review" : $("#review").val(),
              "item_id" : $('#item_id').val(),
              "tpid"  : $('#tpid').val(),
              "rating" : $rateYo.rateYo("rating"),
            },
            success : function(res)
            {
                $("#ratingButton").removeClass('spinning');
                var obj = eval("("+res+")");
                if(obj.success == 1)
                {
                  $("#feedback").hide();
                  location.reload();
                  $("#RequestReview").trigger( "reset" );
                  $('#rating_success_container').html('<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>'+obj.msg+'</div>');
                }
                else
                {

                  $('#rating_danger_container').html('<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>'+obj.msg+'</div>');
                }
            }
        });
      }

   });
</script>
@endsection
<style>
.pre-modal-dialog {
    margin: 30px auto;
    width: 440px;
}


</style>
@section('inline-script')
@parent
<script>
function give_feedback($id)
{
   $('#item_id').val($id);
}


$(".fancybox").fancybox();

var $rateYo = $("#rateyo").rateYo();


</script>
@endsection
