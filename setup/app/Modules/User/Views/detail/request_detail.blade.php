

@extends('Page::layout.one-column-page')
@section('content')
<div class="container">
<div class="row">
<div class="col-sm-12">
<h2 class="color-blue mainHeading">Delivery Details12</h2>
<br />
</div>
   <?php /* echo "<pre>"; print_r($data);  die;*/?>
     @foreach($data as $user)

   <div class="col-md-12">
      <div class="box-shadow">
         <div class="content-section clearfix">
            <br />
            <div class="col-md-3 col-sm-3">
               <img src="{{ImageUrl}}/no-image.jpg"  width="150px" height="150px" >
            </div>
            <div class="col-md-9 col-sm-9">
               <h4><b>{{ucfirst($user->ProductTitle)}}</b></h4>
               <p class="color-black">Status : {{ucfirst($user->Status)}}</p>
               <div class="form-group">
                  <a  class="btn btn-primary blue-btn"  title="Edit" href="#"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                  &nbsp; Edit &nbsp;</a>&nbsp;
                  <a class="btn btn-danger" title="Delete" onclick="return confirm('Are you sure ? you want to delete this request')" href="delete-request/{{$user->id}}"><i class="fa fa-trash-o" aria-hidden="true"></i>
                  Delete </a>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="col-sm-12">
      <div class="clearfix margin-top detail-tab">
         <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active" ><a href="#profile" aria-controls="profile" class="active" role="tab" data-toggle="tab">Delivery Information</a></li>
            <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Package Information</a></li>
         </ul>

         <div class="tab-content box-shadow">
            <div role="tabpanel" class="tab-pane active" id="profile">
			</br>
            <p><b>Pickup Location : </b></br> {{$user->PickupFullAddress}}</p></br>
            <p><b>This is a Public Place No. :</b></br> {{ucfirst($user->PublicPlace)}} </p>
             @if($user->PublicPlace == 'yes')
             The Requester has opted to meet in public
             @endif </br>
            </br><p><b>Drop of Location :</b></br> {{ucfirst($user->DeliveryFullAddress)}}</p></br>
            <p><b>Date in flexible No. : </b></br> {{ucfirst($user->FlexibleDeliveryDate)}}</p></br>
            <p><b>Delivery type. : </b></br>    </p></br>
            <p><b>Return address (if item is not delivered) : </b></br> {{$user->ReturnFullAddress}}</p></br>
            </div>
            <div role="tabpanel" class="tab-pane" id="messages">
            </br>
            <p><b>Package value :</b> ${{ number_format((float)$user->ProductCost,2,'.','') }}</p></br>
            <p><b>Weight :</b> {{$user->ProductWeight }} {{$user->ProductWeightUnit}} </p></br>
            <p><b>Package Dimension :</b> L-{{$user->ProductLength }} {{$user->ProductLengthUnit}}, H-{{$user->ProductHeight }} {{$user->ProductHeightUnit}}, W-{{$user->ProductWidth }} {{$user->ProductWidthUnit}}</p></br>
            <p><b>Quantity :</b> {{$user->BoxQuantity }} Box(es)</p></br>
            <p><b>Description :</b> {{ucfirst($user->Description) }}</p></br>
             <div class="list-footer">
                            <div class="row">
								<div class="col-sm-10">
            <p><b>Package State :</b></p></br>
            <p><b>Insurances :</b> {{ucfirst($user->InsuranceStatus) }} </p></br>
            <p><b>Shipping Cost : </b> ${{ number_format((float)$user-> ShippingCost,2,'.','') }}</p></br>
            <?php $result = ($user->TotalCost) - ($user->AquantuoFees);?>
            <p><b>Aquantuo Fee : </b> <?php echo "$" . number_format((float) $result, 2, '.', ''); ?> </p></br>
            <p><b>Transporter Earning : </b>  </p></br>
            <p><b>Need Package Material : </b>{{ucfirst($user->PackageMaterial)}}  </p></br>
                            </div>
                            </div>
                        </div>
            </div>
         </div>
      </div>


  @endforeach
</div>
</div>
</div>
@endsection

