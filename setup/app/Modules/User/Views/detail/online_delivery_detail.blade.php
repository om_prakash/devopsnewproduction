@extends('Page::layout.one-column-page')
@section('content')

<div class="container">
    <div class="row">
    <h2 class="color-blue mainHeading">Delivery details</h2>
    <br />
        <div class="col-sm-12">
        	<div class="box-shadow">
                <!--    step1     -->
    			<div class="box-header">
					{{$data->ProductTitle}} <span class="pull-right"><i class="fa fa-arrow-left" aria-hidden="true"></i>
					<a href="{{url('my-request')}}" style="color:#FFFFFF;">Back</a></span>
                </div>
                <div class="content-section row clearfix">
					<div class="col-sm-6">
							<p><b>Package Number-</b> {{$data->PackageNumber}}</p>
						@if($data->EnterOn != '')
							<p><b>Created Date-</b>{{date(' M  d, Y ',$data->EnterOn->sec)}} </p>
						@endif
						@if($data->ReturnToAquantuo != '')
							<p><b>Return to Aquantuo -</b> Yes</p>
						@else
							<p><b>Return to Aquantuo -</b> No</p>
						@endif
					</div>
					<div class="col-sm-6">
						<p class="text-right"><b>Delivery Status-</b> {{ucfirst($data->Status)}} </p>
					</div>
                </div>
                <br />
                <?php $sum = 0;
$sno = 1;?>

                    <div class="row">
                        <div class="">
							<div class="col-sm-6">
								<?php $sno++;?>
								<p><b>Product Link - </b><a href="{{$data->url}}">{{$data->url}}</a></p>
								<p><b>Description : </b>{{ucfirst($data->description)}}</p>
							</div>
                        	<div class="col-sm-6">
								<p  class="text-right"><a href="#"><b>View Invoice</b></a></p>
								<p class="text-right"><b>Delivered Date :</b> {{date(' M  d, Y ',$data->DeliveryDate->sec)}} </p>
                            </div>
                        </div>
                        <div class="col-sm-12">
                        	<table class="custom-table table table-bordered">
                            	<thead>
                                	<tr>
										<th>Product Image</th>
                                    	<th>Product Name</th>
                                    	<th>Item Category</th>
                                        <th>Shipping Mode</th>
                                        <th>Weight</th>
                                        <th>Item Volume</th>
                                        <th>Quantity</th>
                                        <th>Item Price</th>
                                    </tr>
                                </thead>
                                <tbody>
                                	<tr>
										@if($data->ProductImage != '')
											<td><img src="{{ ImageUrl.'package/'.$data->ProductImage}}" width="130px" height="80px" ></td>
										@else
											<td><img src="{{ ImageUrl}}/no-image.jpg" width="130px" height="100px" ></td>
										@endif
											<td>{{$data->product_name}}</td>
											<td>{{$data->category}}</td>
											<td>{{$data->travelMode}}</td>
											<td>{{$data->weight}}{{$data->weight_unit}}</td>
											<td>{{$data->weight}}{{$data->weight_unit}}</td>
											<td>{{$data->qty}}</td>
											<?php
$sum += $data->price;
$result = count($data->price);
?>
											<td>${{ number_format((float)$data->price,2,'.','')}}</td>
                                    </tr>
											<td colspan="7"></td>
											<th colspan="5" class=""><b>Total - <?php echo "$" . number_format((float) $sum, 2, '.', ''); ?></b></th>
                                    </tr>
                                </tbody>
                            </table>
                            <hr/>
                        </div>
                    </div>

                    <div class="list-footer clearfix">
                    	<div class="row">
							<div class="col-sm-3">
								<p><b>Total Number of Item(s) - </b> <?php echo --$sno; ?></p>
								<p><b>Total shipment weight- </b></p>
							</div>
							<div class="col-sm-5">
								<p><b>Delivery Address -</b> {{ucfirst($data->DeliveryFullAddress)}}</p>
								<p><b>Delivery Country -</b>{{ucfirst($data->DeliveryCountry)}}</p>
								<p><b>Delivery Pincode -</b>{{ucfirst($data->DeliveryPincode)}}</p>
								<p><b>Delivery Date -</b>{{date('l,  M  d, Y ',$data->DeliveryDate->sec)}}</p>
								<p><b>Distance</b></p>
							</div>
							<div class="payment-sec col-sm-4 pull-right">
								<h3 class="text-right">Payment Info</h3>
                                	<table class="table table-bordered">
  										<tbody bgcolor="#FFFFFF">
											<tr>
												<td class="">
													<div class="col-xs-8 text-left">
														<label class="" for="exampleInputEmail1"><b>Total Item price:</b> </label>
													</div>
													<div class="col-xs-4 text-right">
														<?php echo "$" . number_format((float) $sum, 2, '.', ''); ?>
													</div>
											    </td>
											</tr>
											<tr>
												<td class="">
													<div class="col-xs-8 text-left">
														<label class="" for="exampleInputEmail1"><b>Retailer Shipping price:</b> </label>
													</div>
													<div class="col-xs-4 text-right">
														${{ number_format((float)$data->ShippingCost,2,'.','')}}
													</div>
												</td>
											</tr>
											<tr>
												<td class="">
													<?php	$result = $data->TotalCost - $data->AquantuoFees;?>
														<div class="col-xs-8 text-left">
															<label class="" for="exampleInputEmail1"><b>Aquantuo Shipping price:</b> </label>
														</div>
														<div class="col-xs-4 text-right">
															<?php echo "$" . number_format((float) $result, 2, '.', ''); ?>
														</div>
												</td>
											</tr>
											<tr>
												<td class="">
													<div class="col-xs-8 text-left">
														<label class="" for="exampleInputEmail1"><b>Delivery within Ghana:</b> </label>
													</div>
													<div class="col-xs-4 text-right">
														${{ number_format((float)$data->GhanaTotalCost,2,'.','')}}
													</div>
												</td>
											</tr>
                                        </tbody>
									</table>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
        </div>
</div>
@endsection
