@extends('Page::layout.one-column-page')
@section('page_title')
My Profile - Aquantuo
@endsection
@section('content')
<div class="container">
	<div class="row">
		<div class="col-sm-12">
		<h2 class="color-blue mainHeading">General Information</h2>
		<br />
		</div>
	</div>
	<div class="row mobile_setrow">

				<div class="col-md-9 col-sm-8 col-xs-12 second">
					<?php /* print_r($users);  die; */?>
					@foreach($users as $user)
					<div class="box-shadow">
						<div class="content-section clearfix">
						<br />
							<div class="col-md-3 col-sm-3">
							@if($user->Image != '')
							<a class="fancybox" rel="group" href="{{ ImageUrl.$user->Image}}" >
							    <img src="{{ ImageUrl.$user->Image}}"  width="150px" height="150px" class="img-circle">
						    </a>
							@else
								<img src="{{ImageUrl}}/user-no-image.jpg" width="150px" height="150px" class="img-circle">
							@endif
							</div>
							<div class="col-md-9 col-sm-9">
									<p></p>
									<label class="color-black">{{ucfirst($user->FirstName)}} {{ucfirst($user->LastName)}}</label></br></br>
									<label class="color-black">Mobile No: {{$user->PhoneNo}}</label></br></br>
									<label class="color-black">Email: {{$user->Email}}</label></br></br>
							<div class="form-group">
							     <?php if (Session::get('Usertype') == 'both') {?>
							    <a  class="btn btn-primary blue-btn"  href="{{url('edit-transporter-profile')}}">&nbsp; Edit &nbsp;</a>
							     <?php } else {?>
								<a  class="btn btn-primary blue-btn"  href="{{url('edit-profile')}}">&nbsp; Edit &nbsp;</a>
								<?php }?>
							</div>
						</div>
					</div>
					</br>
					<h4><b>Address</b></h4>
						<div class="media">
						</br>
							<div class="media-left"><i class="fa fa-map-marker fa-2x"></i></div>
								<div class="media-body">
									<label class="color-black">Address  :&nbsp;&nbsp;{{ucfirst($user->Street1)}} &nbsp;&nbsp;{{ucfirst($user->City)}} &nbsp;&nbsp;{{ucfirst($user->State)}} &nbsp;&nbsp;{{ucfirst($user->Country)}} &nbsp;&nbsp;{{ucfirst($user->ZipCode)}}</label>
									@if($user->Street != '')
       									<p class="color-black">Address  :&nbsp;&nbsp;{{$user->Street2}}</p>
									@endif
								</div>
							</div>
							<hr/>
							@if($user->UserType == 'transporter')
								<h4><b>SSN-</b></h4>
								<p>{{$user->SSN}}</p>
								<hr/>
								<h4><b>License Id</b></h4>
									@if($user->IDProof != '')
										<img src="{{ UserImageUrl.($user->IDProof)}}" width="300" height="180">
									@else
										<img src="{{ UserImageUrl}}/no-image.jpg" width="300" height="180">
									@endif
										<p></p>
									@endif
					     </div>

							@endforeach
				</div>
							@include('Page::layout.side_bar')
		</div>
	</div>
    <script type="text/javascript">
$(document).ready(function() {

		$(".fancybox").fancybox();

	});
</script>
@endsection


