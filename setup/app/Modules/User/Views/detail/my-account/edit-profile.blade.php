
@extends('Page::layout.one-column-page')
@section('content')
{!! Html::style('theme/admin/choose-multiple/bootstrap-chosen.css') !!}
{!! Html::script('theme/admin/choose-multiple/chosen.jquery.js') !!}
<div class="container">
    <div class="row">
    	<div class="col-sm-12">
			<h2 class="color-blue mainHeading">Edit General Information</h2>
			<br />
        </div>
			{!! Form::open(array('url' => 'post-edit-profile' , 'id' => 'edit-profile', 'files' => 'true') ) !!}
			@foreach($users as $user)
				<div class="col-md-9 col-sm-8 col-xs-12">
					<div class="box-shadow clearfix">
						<h3>Please complete your profile information</h3>
						<hr/>
						@if(empty($user->CountryCode))
						<div class="">
							<div class="col-sm-12">
								<p>Users on Aquantuo are real people with package or people willng to help move packagees, that is why we ask everyone to verify a few things to activate their account.</p><br><br>
							</div>
						</div>
						@endif
						<div class="row">
							<div class="col-sm-8">
								<div class="col-sm-10">
									<div class="form-group">

										<?php
if (!empty(old('first_name'))) {
    $user->FirstName = old('first_name');
} elseif ($user->FirstName != '') {
    $user->FirstName = $user->FirstName;
}
?>

									   <label class="control-label">First Name
									                  </label>
									   <input type="text" value="{{ucfirst($user->FirstName)}}" placeholder="First Name" name="first_name" class="form-control required" maxlength="40">
									   <p class="help-block red" style="color:red;"  >{{$errors->first('first_name')}} </p>
									</div>
								</div>

								<div class="col-sm-10">
									<div class="form-group">
										<?php
if (!empty(old('last_name'))) {
    $user->LastName = old('last_name');
} elseif ($user->LastName != '') {
    $user->LastName = $user->LastName;
}
?>

									   <label class="control-label">Last Name</label>
									   <input type="text" value="{{ucfirst($user->LastName)}}" name="last_name" placeholder="Last Name" class="form-control required" maxlength="40">
									   <p class="help-block red" style="color:red;"  >{{$errors->first('Last_name')}} </p>
									</div>
								</div>
								<div class="col-sm-10">
									<div class="form-group">
										<label class="control-label">Phone Number
										           <span class="red-star"> *</span></label>
										<div class="row">

											<?php
if (!empty(old('country_code'))) {
    $user->CountryCode = old('country_code');
} elseif ($user->CountryCode != '') {
    $user->CountryCode = $user->CountryCode;
}
?>

											<?php
if (!empty(old('phoneno'))) {
    $user->PhoneNo = old('phoneno');
} elseif ($user->PhoneNo != '') {
    $user->PhoneNo = $user->PhoneNo;
}
?>


											<div class="col-xs-4">
												<input type="text"  name="country_code" value="{{$user->CountryCode}}" placeholder="Country Code" class="form-control required" maxlength="4">
												<p class="help-block red" style="color:red;"  >{{$errors->first('Country_code')}} </p>
											</div>
											<div class="col-xs-8">
												 <input type="text"  class="form-control required minlength-8 minlength-12 usename-#phone number#" placeholder="Phone number" name="phoneno" value="{{$user->PhoneNo}}" maxlength="12">
												 <p class="help-block red" style="color:red;"  >{{$errors->first('phoneno')}} </p>
											</div>
										</div>
									</div>
								</div>

				                 <div class="col-sm-10">
									<div class="form-group">
										<label class="control-label">Alternate Phone Number
										           <!-- <span class="red-star"> *</span> --></label>
										<div class="row">

<?php
if (!empty(old('alternet_country_code'))) {
    $user->AlternateCCode = old('alternet_country_code');
} elseif ($user->AlternateCCode != '') {
    $user->AlternateCCode = $user->AlternateCCode;
}
?>

<?php
if (!empty(old('alternet_phoneno'))) {
    $user->AlternatePhoneNo = old('alternet_phoneno');
} elseif ($user->AlternatePhoneNo != '') {
    $user->AlternatePhoneNo = $user->AlternatePhoneNo;
}
?>


											<div class="col-xs-4">
												<input type="text"  name="alternet_country_code" value="{{ $user->AlternateCCode }}" placeholder="Country Code" class="form-control" maxlength="4">
												<p class="help-block red" style="color:red;"  >{{$errors->first('alternet_country_code')}} </p>
											</div>
											<div class="col-xs-8">
												 <input type="text"  class="form-control minlength-8 minlength-12 " placeholder="Alternate Phone Number" name="alternet_phoneno" value="{{$user->AlternatePhoneNo }}" maxlength="12">
												 <p class="help-block red" style="color:red;"  >{{$errors->first('alternet_phoneno')}} </p>
											</div>
										</div>
									</div>
								</div>

							</div>

							<div class="col-sm-4">
								<div class="selected-pic">
									<div>
										@if($user->Image != '')
								          <a class="fancybox" rel="group" href="{{ ImageUrl.$user->Image}}" >
											<img id="senior-preview" src="{{ ImageUrl.$user->Image}}"  width="200px" height="150px" />
										  </a>
										@else
											<img id="senior-preview" src="{{ ImageUrl}}/user-no-image.jpg"  width="200px" height="150px" />
										@endif
									</div>
									<p> </p>
									<label class="custom-input-file">
									{!! Form::file('user_image', ['class'=> 'custom-input-file','id'=>'senior_image']) !!}
									</label>
								</div>
							</div>
						</div>

						<div class="col-sm-12">
							<div class="form-group">

							<?php
if (!empty(old('address'))) {
    $user->Street1 = old('address');
} elseif ($user->Street1 != '') {
    $user->Street1 = $user->Street1;
}
?>

								<label class="control-label">Address 1
								         </label>
								<input type="text" value="{{$user->Street1}}" name="address" placeholder="Address" class="form-control required" maxlength="200">
								<p class="help-block red" style="color:red;"  >{{$errors->first('address')}} </p>
							</div>
						</div>

						<div class="col-sm-12">
							<div class="form-group">
								<label class="control-label">Address 2
								         </label>
								<?php
if (!empty(old('address2'))) {
    $user->Street2 = old('address2');
} elseif ($user->Street2 != '') {
    $user->Street2 = $user->Street2;
}
?>


								<input type="text" value="{{ $user->Street2 }}" name="address2" placeholder="Address2" class="form-control" maxlength="200">
								<p class="help-block red" style="color:red;"  >{{$errors->first('address2')}} </p>
							</div>
						</div>

						<div class="col-sm-4">
							<div class="form-group">
								<label class="control-label">Country</label>

								<?php
if (!empty(old('country'))) {
    $user->Country = json_decode(old('country'))->name;
} elseif ($user->Country != '') {
    $user->Country = $user->Country;
}
?>

								<?php
if (!empty(old('state'))) {
    $user->State = json_decode(old('state'))->name;
} elseif ($user->State != '') {
    $user->State = $user->State;
}
?>

								<?php
if (!empty(old('city'))) {
    $user->City = json_decode(old('city'))->name;
} elseif ($user->City != '') {
    $user->City = $user->City;
}
?>





								<select name="country" class="form-control required usename-#country#" id="pp_pickup_country10" onchange="get_state2('pp_pickup_country10','pp_pickup_state10','pp_pickup_city10','pp_pickup_state10','','','','10','')">

	                            <option value="">Select Country</option>
	                            @foreach($country as $key)
                                    <option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}","state_available":"{{$key->state_available}}"}' @if($key->Content == $user->Country) selected='selected' @endif >{{$key->Content}}</option>
                               	@endforeach

	                        </select>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">
								<label class="control-label">State</label>
								<span id="ap_id10">
								 <select name="state10" class="form-control usename-#state# left-disabled chosen-select" id="pp_pickup_state10" onchange="get_city('pp_pickup_state10','pp_pickup_city10','pp_pickup_city10','{{$user->City}}')">
	                            <option value="" >Select State</option>
	                            @foreach($state as $key)
	                                <option value='{"id":"<?php echo $key['_id']; ?>","name":"<?php echo $key['Content']; ?>  "}' @if($user->State == $key['Content']) selected="selected" @endif>{{$key['Content']}}</option>
	                            @endforeach
	                        	</select>
	                        	</span>
	                        	<p class="help-block red" style="color:red;"  >{{$errors->first('state10')}} </p>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">
								<label class="control-label">City</label>
								<select  name="city" class="form-control chosen-select usename-#city#" id="pp_pickup_city10" >
	                            <option value="" >Select City</option>
	                            @foreach($city as $key)
                                  <option value='{"id":"<?php echo $key['_id']; ?>","name":"<?php echo $key['Content']; ?>  "}' @if($user->City == $key['Content']) selected="selected" @endif>{{$key['Content']}}</option>
                              @endforeach
	                        </select>
	                        <p class="help-block red" style="color:red;"  >{{$errors->first('city')}} </p>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">
								<label class="control-label">Zip Code/Postcode</label>

								@if($user->ZipCode != '')
								<?php $zipcode = $user->ZipCode;?>
								@else
									<?php $zipcode = old('zipcode');?>
								@endif

								<input type="text" value="{{$zipcode}}" placeholder="Zip Code/Postcode" name="zipcode" class="form-control alpha-numeric" maxlength="8">
								<p class="help-block red" style="color:red;" >{{$errors->first('zipcode')}} </p>
							</div>
						</div>
						<div class="col-sm-12">
							<hr />
						</div>
						<div class="col-sm-8 col-sm-offset-2">
							<div class="form-group">
								<button class="custom-btn1 btn-block">Update <div class="custom-btn-h"></div> </button>
								<br></br><br></br>
							</div>
						</div>
					</div>
				@endforeach
            {!! Form::close() !!}
			</div>
        @include('Page::layout.side_bar')
	</div>
</div>
@endsection


@section('script')
@parent

{!! Html::script('theme/web/js/utility.js') !!}
{!! Html::script('theme/web/js/validation.js') !!}

@endsection

@section('inline-script')
@parent
   <script type="text/javascript">

jQuery(document).ready(function ($) {

  $(".chosen-select").chosen({no_results_text: "Oops, nothing found!"});
  $(".chosen-select").chosen().change(function() {
        //alert(+$(this).val());
    });
});


$(document).ready(function() {

		$(".fancybox").fancybox();

	});


new Validate({
    FormName :  'edit-profile',
    ErrorLevel : 1,

   });
</script>
<script>
   $('#senior_image').on('change', function(evt)
   {
   var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
   if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
     $(this).val('');
     $('#senior-preview').attr('src', '{{ImageUrl}}/user-no-image.jpg');
      alert("Only "+fileExtension.join(', ')+" formats are allowed.");
   } else {

     var file = evt.target.files[0];
     if (file)
     {
       var reader = new FileReader();

       reader.onload = function (e) {
           $('#senior-preview').attr('src', e.target.result)
       };
       reader.readAsDataURL(file);
     }
   }
   });

   get_state('pp_pickup_country','pp_pickup_state','pp_pickup_city','pp_pickup_state','{{$user->State}}',
   	'{{$user->City}}');
   //get_city('pp_pickup_state','pp_pickup_city','pp_pickup_city','{{$user->City}}');
</script>
@endsection
