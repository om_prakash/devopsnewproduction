
@extends('Page::layout.one-column-page')
@section('content')
<div class="container">
    <div class="row">
    	<div class="col-sm-12">
			<h2 class="color-blue">Become a Transporter</h2>
			<br />
        </div>
		<?php  /* echo "<pre>"; print_r($users);  die;  */ ?>
			
		
				<div class="col-md-9 col-sm-8 col-xs-12">
					<div class="box-shadow clearfix">
						<h3>Please fill some required field</h3>
						<hr/>

						<div class="form-group clearfix">
                  <div class="my-tab tabCustom clearfix">
                     <div class="sliding-div" id="ac_type_bg"></div>
                     <div class="tab1" data-toggle="tab" href="#home" id="requester" onclick ="transporter_type('requester')" style="color:#fff;"><span> INDIVIDUAL TRANSPORTER</span></div>
                     <div class="tab2" data-toggle="tab" href="#menu2" id="transporter" onclick="transporter_type('transporter')"><span> BUSINESS TRANSPORTER </span></div>
                  </div>
               </div>

                    <div class="tab-content">
                  <div id="home" class="tab-pane fade in active">
                     <div class="col-sm-12">
                        <div class="">
                           <br/>
                           <p class="text-primary"></p>
                           {!! Form::open(array('url' => 'transporter-edit-profile' , 'id'=>'edit_transporter', 'name' => 'edit_transporter', 'files' => 'true' !!} 
                           <div class="col-sm-12 row">
                           <div class="col-sm-8">
                            <div class="">
									<div class="form-group">
									   <label class="control-label">First Name
									                  </label>
									   <input type="text" value="{{ucfirst($users->FirstName)}}" placeholder="First Name" name="first_name" class="form-control required" maxlength="40"> 		  
									   <p class="help-block red" style="color:red;"  >{{$errors->first('first_name')}} </p>
									</div>
								</div>  
								<div class="">
									<div class="form-group">
									   <label class="control-label">Last Name</label>
									   <input type="text" value="{{ucfirst($users->LastName)}}" name="last_name" placeholder="Last Name" class="form-control required" maxlength="40">  
									   <p class="help-block red" style="color:red;"  >{{$errors->first('Last_name')}} </p>
									</div>
								</div> 

								<div class="">
									<div class="form-group">
										<label class="control-label">Phone Number
										           <span class="red-star"> *</span></label>  
										<div class="row">
											<div class="col-xs-3">
												<input type="text"  name="country_code" value="{{$users->CountryCode}}" placeholder="CC" class="form-control required" maxlength="4">
												<p class="help-block red" style="color:red;"  >{{$errors->first('Country_code')}} </p>
											</div>
											<div class="col-xs-9">
												 <input type="text"  class="form-control required" placeholder="Phone number" name="phone_number" value="{{$users->PhoneNo}}" maxlength="10">
												 <p class="help-block red" style="color:red;"  >{{$errors->first('phone_number')}} </p>
											</div>
										</div>
									</div>
								</div>

								<div class="form-group">
										<label class="control-label">Alternate Phone Number
										           <span class="red-star"> *</span></label>  
										<div class="row">
											<div class="col-xs-3">  
												<input type="text"  name="alternate_country_code" value="{{$users->AlternateCCode}}" placeholder="CC" class="form-control required" maxlength="4">
												<p class="help-block red" style="color:red;"  >{{$errors->first('alternate_country_code')}} </p>
											</div>
											<div class="col-xs-9">
												 <input type="text"  class="form-control required" placeholder="Phone number" name="alternate_phone_number" value="{{$users->AlternatePhoneNo}}" maxlength="10">
												 <p class="help-block red" style="color:red;"  >{{$errors->first('alternate_phone_number')}} </p>
											</div>
										</div>
									</div>
                              </div>


                              <div class="col-sm-4">
                               <div class="">
								<div class="selected-pic">
									<div> 
										@if($users->Image != '')  
								          <a class="fancybox" rel="group" href="{{ ImageUrl.$users->Image}}" >
											<img id="individual-transporter-image" src="{{ ImageUrl.$users->Image}}"  width="200px" height="150px" />
										  </a>
										@else
											<img id="individual-transporter-image" src="{{ ImageUrl}}/user-no-image.jpg"  width="200px" height="150px" />
										@endif
									</div>
									<label class="custom-input-file">
									{!! Form::file('user_image', ['class'=> 'custom-input-file','id'=>'individual-transporter-image','onchange'=>"image_preview(this,'individual-transporter-image',event)"]) !!}
									</label>
								</div>
							</div>
                              </div>
								</div>

                             <div class="col-sm-12">

                             <div class="form-group">
								<label class="control-label">Social Security No. or Other form of National ID.</label>
								<input type="text" value="{{$users->SSN}}" name="ssn" placeholder="Social Security No. or Other form of National ID." class="form-control required maxlength-15" maxlength="15">
								<p class="help-block red" style="color:red;"  >{{$errors->first('ssn')}} </p>
							</div>

							<div class="form-group">
								<label class="control-label">Type of ID  above.
								         </label>
								<input type="text" value="{{$users->type_of_id}}" name="type_of_id" placeholder="Type of ID  above." class="form-control required maxlength-25" maxlength="15">
								<p class="help-block red" style="color:red;"  >{{$errors->first('type_of_id')}} </p>
							</div>

							<div class="form-group">
								<label class="control-label">Address 1
								         </label>
								<input type="text" value="{{$users->Street1}}" name="address" placeholder="Address 1" class="form-control required" maxlength="150">
								<p class="help-block red" style="color:red;"  >{{$errors->first('address')}} </p>
							</div>

							<div class="form-group">
								<label class="control-label">Address 2
								          </label>
								<input type="text" value="{{$users->Street2}}" name="address2" placeholder="Address 2" class="form-control" maxlength="150">
								<p class="help-block red" style="color:red;"  >{{$errors->first('address')}} </p>
							</div>
						</div>

						<div class="col-sm-4">
							<div class="form-group">
								<label class="control-label">Country</label>
								 <select name="country" class="form-control required usename-#country#" id="pp_pickup_country" onchange="get_state('pp_pickup_country','pp_pickup_state','pp_pickup_city','pp_pickup_state','{{$users->State}}')">
	                            <option value="">Select Country</option>
	                            @foreach($country as $key)
	                             <option  @if($key->Content == $users->Country) selected ='selected' @endif  value='{"id":"{{$key->_id}}","name":"{{$key->Content}}"}'>{{$key->Content}}</option>

	                            @endforeach
	                        </select>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">
								<label class="control-label">State</label>
								 <select name="state" class="form-control required usename-#state#" id="pp_pickup_state" onchange="get_city('pp_pickup_state','pp_pickup_city','pp_pickup_city','{{$users->City}}')">
	                            <option value="" >Select State</option>
	                        </select>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">
								<label class="control-label">City</label>
								<select  name="city" class="form-control required usename-#city#" id="pp_pickup_city" > 
	                            <option value="" >Select City</option>
	                        </select>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">
								<label class="control-label">Zip Code/Postcode</label>
								<input type="text" value="{{$users->ZipCode}}" placeholder="Zip Code/Postcode" name="zipcode" class="form-control alpha-numeric" maxlength="8">
								<p class="help-block red" style="color:red;" >{{$errors->first('zipcode')}} </p>
							</div>
						</div>

				 <div class="col-sm-12 row">
				 <div class="col-sm-3">
                  <label class="control-label"> License ID</label>
                  <div class="selected-pic">
                     <div>
                        @if($users->IDProof != '')
                          <a class="fancybox" rel="group" href="{{ ImageUrl.$users->IDProof}}" >  
                           <img src="{{ ImageUrl.$users->IDProof}}" class="required" id="individual-id-proof" width="320px" height="170px" >
                          </a>
                        @else
                        <p></p>
                        <img src="{{ImageUrl}}/user-no-image.jpg" id="individual-id-proof" width="200px" height="150px">
                        @endif
                     </div>
                      <label class="custom-input-file">
                      @if($users->IDProof != '')
                     <input type="file" name="license_id" class="custom-input-file" id="individual-id-proof" onchange ="image_preview(this,'individual-id-proof',event)">
                      @else
                       <input type="file" name="license_id" class="custom-input-file required" id="individual-id-proof" onchange ="image_preview(this,'individual-id-proof',event)">
                      @endif
                     </label>
                  </div>
                </div>
                </div>  
                <div class="col-sm-12">

                <div class="form-group">
                          <div class="checkbox">
                             <label>
                             	<span class="pull-left"><input type="checkbox"  id="terms_conditions" class="required" name="terms_conditions"></span> <span class="Insurance_check"> I acknowledgement and agree that i have read and understand this BACKGROUND CHECK DISCLOSURE and further acknowledge that i have read, understand and agree with the statement contained in the ADDITIONAL BACKGROUND CHECK DISCLOSURE and this BACKGROUND CHECK DISCLOSURE and this BACKGROUND CHECK AUTHORIZATION. i agree that a am providing my electronic signature and that my electronic signature is binding just like a signature in ink.   </a> </span>
                             </label>
                          </div>
                       </div>
                </div>
						<div class="col-sm-8 col-sm-offset-2">
                      </br>
							<div class="form-group">
								<button class="custom-btn1 btn-block">Update <div class="custom-btn-h"></div> </button>
								<br></br><br></br>
							
							</div>
						</div>	
                           {!! Form::close() !!}	
                        </div>
                     </div>
                  </div>
                  <div id="menu2" class="tab-pane fade">
                     <div class="col-sm-12">
                        <div class="">
                           <br/>
                           <p class="text-primary"></p>
                          {!! Form::open(array('url' => 'post-transporter-edit-profile' ,'id' => 'post-transporter' ,'files' => 'true') ) !!} 

                           <div class="col-sm-12 row">
                           <div class="col-sm-8">
                            <div class="">
									<div class="form-group">
									   <label class="control-label">Business Name
									                  </label>
									   <input type="text" value="{{ucfirst($users->BusinessName)}}" placeholder="Business Name" name="business_name" class="form-control required" maxlength="40"> 		  
									   <p class="help-block red" style="color:red;"  >{{$errors->first('business_name')}} </p>
									</div>
								</div>  
								<div class="">
									<div class="form-group">
									   <label class="control-label">Tax/License/VAT ID</label>
									   <input type="text" value="{{ucfirst($users->VatTaxNo)}}" name="VatTaxNo" placeholder="Vat/Tax/No ID" class="form-control required" maxlength="40">  
									   <p class="help-block red" style="color:red;"  >{{$errors->first('VatTaxNo')}} </p>
									</div>
								</div> 

								<div class="">
									<div class="form-group">
										<label class="control-label">Phone Number
										           <span class="red-star"> *</span></label>  
										<div class="row">
											<div class="col-xs-3">
												<input type="text"  name="country_code" value="{{$users->CountryCode}}" placeholder="CC" class="form-control required" maxlength="4">
												<p class="help-block red" style="color:red;"  >{{$errors->first('Country_code')}} </p>
											</div>
											<div class="col-xs-9">
												 <input type="text"  class="form-control required" placeholder="Phone number" name="phone_number" value="{{$users->PhoneNo}}" maxlength="10">
												 <p class="help-block red" style="color:red;"  >{{$errors->first('phone_number')}} </p>
											</div>
										</div>
									</div>
								</div>

								<div class="form-group">
										<label class="control-label">Alternate Phone Number
										           <span class="red-star"> *</span></label>  
										<div class="row">
											<div class="col-xs-3">
												<input type="text"  name="alternate_country_code" value="{{$users->AlternateCCode}}" placeholder="CC" class="form-control required" maxlength="4">
												<p class="help-block red" style="color:red;"  >{{$errors->first('alternate_country_code')}} </p>
											</div>
											<div class="col-xs-9">
												 <input type="text"  class="form-control required" placeholder="Phone number" name="alternate_phone_number" value="{{$users->AlternatePhoneNo}}" maxlength="10">
												 <p class="help-block red" style="color:red;"  >{{$errors->first('alternate_phone_number')}} </p>
											</div>
										</div>
									</div>
                              </div>


                              <div class="col-sm-4">
                               <div class="">
								<div class="selected-pic">
									<div> 
										@if($users->Image != '')  
								          <a class="fancybox" rel="group" href="{{ ImageUrl.$users->Image}}" >
											<img id="business_transporter_image" src="{{ ImageUrl.$users->Image}}"  width="200px" height="150px" />
										  </a>
										@else
											<img id="business_transporter_image" src="{{ ImageUrl}}/user-no-image.jpg"  width="200px" height="150px" />
										@endif
									</div>
									<label class="custom-input-file">
									{!! Form::file('user_image', ['class'=> 'custom-input-file','id'=>'business_transporter_image','onchange'=>"image_preview(this,'business_transporter_image',event)"]) !!}
									
									</label>
								</div>
							</div>
                              </div>
								</div>
                             <div class="col-sm-12">
                             <div class="form-group">
								<label class="control-label">SSN No.
								         </label>
								<input type="text" value="{{$users->SSN}}" name="ssn" placeholder="SSN No." class="form-control required maxlength-15" maxlength="15">
								<p class="help-block red" style="color:red;"  >{{$errors->first('ssn')}} </p>
							</div>

							<div class="form-group">
								<label class="control-label">Type of ID  above.
								         </label>
								<input type="text" value="{{$users->type_of_id}}" name="type_of_id" placeholder="Type of ID." class="form-control required maxlength-25" maxlength="15">
								<p class="help-block red" style="color:red;"  >{{$errors->first('type_of_id')}} </p>
							</div>
							<div class="form-group">
								<label class="control-label">Address 1
								          </label>
								<input type="text" value="{{$users->Street1}}" name="address" placeholder="Address 1" class="form-control required" maxlength="200">
								<p class="help-block red" style="color:red;"  >{{$errors->first('address')}} </p>
							</div>

							<div class="form-group">
								<label class="control-label">Address 2
								          </label>
								<input type="text" value="{{$users->Street2}}" name="address2" placeholder="Address 2" class="form-control" maxlength="200">
								<p class="help-block red" style="color:red;"  >{{$errors->first('address')}} </p>
							</div>
						</div>

						<div class="col-sm-4">
							<div class="form-group">
								<label class="control-label">Country</label>
								 <select name="country" class="form-control required usename-#country#" id="pp_pickup_country2" onchange="get_state('pp_pickup_country2','pp_pickup_state2','pp_pickup_city2','pp_pickup_state2','{{$users->State}}')">
	                            <option value="">Select Country</option>
	                            @foreach($country as $key)
	                             <option  @if($key->Content == $users->Country) selected ='selected' @endif  value='{"id":"{{$key->_id}}","name":"{{$key->Content}}"}'>{{$key->Content}}</option>

	                            @endforeach
	                        </select>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">
								<label class="control-label">State</label>
								 <select name="state" class="form-control required usename-#state#" id="pp_pickup_state2" onchange="get_city('pp_pickup_state2','pp_pickup_city2','pp_pickup_city2','{{$users->City}}')">
	                            <option value="" >Select State</option>
	                        </select>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">
								<label class="control-label">City</label>
								<select  name="city" class="form-control required usename-#city#" id="pp_pickup_city2" > 
	                            <option value="" >Select City</option>
	                        </select>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">
								<label class="control-label">Zipcode</label>
								<input type="text" value="{{$users->ZipCode}}" placeholder="Zipcode" name="zipcode" class="form-control alpha-numeric" maxlength="8">
								<p class="help-block red" style="color:red;" >{{$errors->first('zipcode')}} </p>
							</div>
						</div>

				 <div class="col-sm-12 row">
				 <div class="col-sm-3">
                  <label class="control-label"> License ID</label>
                  <div class="selected-pic">
                     <div>
                        @if($users->IDProof != '')
                          <a class="fancybox" rel="group" href="{{ ImageUrl.$users->IDProof}}" >  
                           <img src="{{ ImageUrl.$users->IDProof}}" class="required" id="business-id-proof" width="320px" height="170px" >
                          </a>
                        @else
                        <p></p>
                        <img src="{{ImageUrl}}/user-no-image.jpg" id="business-id-proof" width="200px" height="150px">
                        @endif
                     </div>
                     <label class="custom-input-file">
                      @if($users->IDProof != '')
                     <input type="file" name="id_proof" class="custom-input-file" id="business-id-proof" onchange ="image_preview(this,'business-id-proof',event)">
                      @else
                       <input type="file" name="license_id" class="custom-input-file required" id="business-id-proof" onchange ="image_preview(this,'business-id-proof',event)">
                      @endif
                     </label>
                  </div>
                     </div>
                     <div class="col-sm-3">
                  <label class="control-label"> Business License ID</label>
                  <div class="selected-pic">
                     <div>
                        @if($users->LicenceId != '')
                          <a class="fancybox" rel="group" href="{{ ImageUrl.$users->LicenceId}}" >  
                           <img src="{{ ImageUrl.$users->LicenceId}}" class="required" id="business-licenceid" width="320px" height="170px" >
                          </a>
                        @else
                        <p></p>
                        <img src="{{ImageUrl}}/user-no-image.jpg" id="business-licenceid" width="200px" height="150px">
                        @endif
                     </div>
                     <label class="custom-input-file">
                      @if($users->IDProof != '')
                     <input type="file" name="LicenceId" class="custom-input-file" id="business-licenceid" onchange ="image_preview(this,'business-licenceid',event)">
                      @else
                       <input type="file" name="business_LicenceId" class="custom-input-file required" id="business-licenceid" onchange ="image_preview(this,'business-licenceid',event)">
                      @endif
                     </label>
                  </div>
                  <p>  </p> 
               </div>
               </div>
               </div>

                      </br> </br>

						<div class="col-sm-8 col-sm-offset-2">
							<div class="form-group">
								<button class="custom-btn1 btn-block">Update <div class="custom-btn-h"></div> </button>
								<br></br><br></br>
							
							</div>
						</div>	
                           {!! Form::close() !!}
                        </div>
                     </div>

                  </div>
               
                         
						
						
					
						
					</div>
		
            
			</div>
        @include('Page::layout.side_bar')
	</div>
</div>
@endsection


@section('script')
@parent

{!! Html::script('theme/web/js/utility.js') !!}
{!! Html::script('theme/web/js/validations.js') !!}

@endsection

@section('inline-script')
@parent
   <script type="text/javascript">

$(document).ready(function() {
	  
		$(".fancybox").fancybox();

	});

new Validate({
    FormName :  'edit_transporter',
    ErrorLevel : 1,
 	
   });

new Validate({
    FormName :  'post-transporter',
    ErrorLevel : 1,
    
   });

function transporter_type(type)
   { 
      if(type == 'transporter') {
   
         $('#ac_type_bg').css('right','0%');
         $('#transporter').css('color','#fff');
         $('#requester').css('color','#000');      
      } else {
         $('#ac_type_bg').css('right','50%');
         $('#requester').css('color','#fff');
         $('#transporter').css('color','#000');      
      }
   }
</script>
<script>

function checkCheckBox(f) {
  if (f.agree.checked == false) {
    alert('Please check the box to continue.');
    return false;
  } else
    return true;
}


function image_preview(obj,previewid,evt)
{
    var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
    if ($.inArray($(obj).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
       $(obj).val('');
       $('#'+previewid).attr('src', '{{ImageUrl}}/user-no-image.jpg');
        alert("Only "+fileExtension.join(', ')+" formats are allowed.");
     } else {
  
       var file = evt.target.files[0];
       if (file) 
       {
         var reader = new FileReader();
     
         reader.onload = function (e) {
             $('#'+previewid).attr('src', e.target.result)
         };
         reader.readAsDataURL(file);
       }
     }
} 
   
   get_state('pp_pickup_country','pp_pickup_state','pp_pickup_city','pp_pickup_state','{{$users->State}}');
   get_city('pp_pickup_state','pp_pickup_city','pp_pickup_city','{{$users->City}}');

   get_state('pp_pickup_country2','pp_pickup_state2','pp_pickup_city2','pp_pickup_state2','{{$users->State}}');
   get_city('pp_pickup_state2','pp_pickup_city2','pp_pickup_city2','{{$users->City}}');
</script>
@endsection
