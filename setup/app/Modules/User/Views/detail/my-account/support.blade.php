@extends('Page::layout.one-column-page')
@section('content')
<div class="container">
	<div class="">
		<div class="col-sm-12">
		<h2 class="color-blue mainHeading">Support</h2>
		<br />
	</div>

<div class="col-md-9 col-sm-12 box-shadow">
         	<div class="">
          </br></br>
          <h4>What can we help you with? Also check out our<a href="http://support.aquantuo.com/help-support " target="_blank"> FAQ</a> page.</h4>
          <p><label>Ghana: &nbsp;</label>030 243 4505<br><label>US: &nbsp;</label>888 652 2233</p>
            <div class="col-md-6">
            	</br>
            	{!! Form::open(array('url' => 'post-support', 'class' => 'form-horizontal','name'=>'support_form' )) !!}
                  <div class="form-group">
                     <label>Title</label>
                     <div class="{{ $errors->has('current_password') ? ' has-error' : '' }}">

                     <input type="text" class="form-control required" id="title" name="title" value="" placeholder="Title">

                     </div>

                     <p class="controll-error error-label" id="title">
                     @if ($errors->has('title'))<i class="fa fa-times-circle-o"></i>&nbsp;
                        {{ $errors->first('title') }}@endif
                     </p>


                  </div>

                  <div class="form-group">
                     <label>Message</label>
                     <div class="{{ $errors->has('message') ? ' has-error' : '' }}" >

                     <textarea name="message" id="message" rows="6"  cols="70" placeholder="Message" class="form-control required"></textarea>

                     </div>

                     <p class="error-label" id="message">@if ($errors->has('message'))<i class="fa fa-times-circle-o"></i>&nbsp;
                        {{ $errors->first('message') }}@endif
                     </p>

                  </div>

                  <div class="form-group" >

                  <input type="submit" class="btn custom-btn1" name="save" onclick = "return validation()" value="Submit">
                    <input type="reset" class="btn custom-btn1" name="save"  value="Reset">
                  </div>
                    {!! Form::close() !!}
            </div>

            </div>
         </div>
           @include('Page::layout.side_bar')
    </div>
<!--gjhg-->
	</div>
</div>
{!! Html::script('custome/js/validation.js') !!}
{!! Html::script('theme/web/js/validation.js') !!}
<script type="text/javascript">

new Validate({
		FormName : 'support_form',
		ErrorLevel : 1,
		validateHidden: false,
	});



</script>
@endsection
