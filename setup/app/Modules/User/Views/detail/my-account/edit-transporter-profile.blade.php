@extends('Page::layout.one-column-page')
@section('content')
{!! Html::script('theme/web/js/validation.js') !!}
{!! Html::style('theme/admin/choose-multiple/bootstrap-chosen.css') !!}
{!! Html::script('theme/admin/choose-multiple/chosen.jquery.js') !!}
<div class="container">
   <div class="row">
   	  <div class="col-sm-12">
      <h2 class="color-blue mainHeading">Edit General Information</h2>
      <br />
      </div>
     <style>
     .text-color {
    color: red;
}
     </style>

         {!! Form::open(array('url' => 'post-edit-transporter-profile' , 'files' => 'true','name'=>'edit_transporter_profile') ) !!}
         @foreach($users as $user)
         <div class="col-md-9 col-sm-12 col-xs-12">
            <div class="box-shadow clearfix">
             <!--  <h3>Please fill some required field</h3>
               <hr/>    -->

               </br></br>
               <div class="row">
                  <div class="col-sm-8">

                   <!--     end of business transpoeter block  -->
                     <div class="col-sm-10">
                        <div class="form-group">
                           <label class="control-label">First Name</label>
                           <input type="text" value="{{ucfirst($user->FirstName)}}" placeholder="First Name" name="first_name" class="form-control required usename-#first Name# " maxlength="40">
                           <p class="help-block red text-color"   >{{$errors->first('first_name')}} </p>
                        </div>
                     </div>
                     <div class="col-sm-10">
                        <div class="form-group">
                           <label class="control-label">Last Name</label>
                           <input type="text" value="{{ucfirst($user->LastName)}}" name="last_name" placeholder="Last Name" class="form-control required usename-#last Name#" maxlength="40">
                           <p class="help-block red text-color">{{$errors->first('Last_name')}} </p>
                        </div>
                     </div>

                  <!--  only for show business transporter  -->
                    @if($user->BusinessName != '')
                  <div class="col-sm-10">
                        <div class="form-group">
                           <label class="control-label">Business Name</label>
                           <input type="text" value="{{ucfirst($user->BusinessName)}}" placeholder="First Name" name="business_name" class="form-control required usename-#first Name# " maxlength="40">
                           <p class="help-block red text-color"   >{{$errors->first('first_name')}} </p>
                        </div>
                     </div>
                     <div class="col-sm-10">
                        <div class="form-group">
                           <label class="control-label">Tax/License/VAT ID</label>
                           <input type="text" value="{{ucfirst($user->VatTaxNo)}}" placeholder="First Name" name="business_tex" class="form-control required usename-#first Name# " maxlength="40">
                           <p class="help-block red text-color"   >{{$errors->first('first_name')}} </p>
                        </div>
                     </div>
                     @endif

                  </div>
                  <div class="col-sm-4">
                     <div class="selected-pic">
                        <div>
                           @if($user->Image != '')
                           <a class="fancybox" rel="group" href="{{ ImageUrl.$user->Image}}" >
                           <img id="senior-preview" src="{{ ImageUrl.$user->Image}}"  width="200px" height="150px" />
                          </a>
                           @else
                           <img id="senior-preview" src="{{ ImageUrl}}/user-no-image.jpg"  width="200px" height="150px" />
                           @endif
                        </div>
                        <label class="custom-input-file">
                        {!! Form::file('user_image', ['class'=> 'custom-input-file','id'=>'senior_image','onchange'=>"image_preview(this,'senior-preview',event)"]) !!}
                        </label>
                     </div>
                  </div>
               </div>


              <div class="col-sm-12">
                <div class="form-group">
                           <label class="control-label">Phone Number
                           <span class="red-star"> *</span></label>
                           <div class="row">
                              <div class="col-xs-3">
                                 <input type="text"  name="country_code" value="{{$user->CountryCode}}" placeholder="Country Code" class="form-control required usename-#country code#" maxlength="4">
                                 <p class="help-block red text-color"   >{{$errors->first('country_code')}} </p>
                              </div>
                              <div class="col-xs-9">
                                 <input type="text"  class="form-control required numeric usename-#phone number#" placeholder="Phone number" name="phoneno" value="{{$user->PhoneNo}}" pattern=".{8,12}" title="8 to 12 numbers">
                                 <p class="help-block red text-color"   >{{$errors->first('phoneno')}} </p>
                              </div>
                           </div>
                        </div>
              </div>

              <div class="col-sm-12">
                        <div class="form-group">
                           <label class="control-label">Alternate Phone Number</label>
                           <div class="row">
                              <div class="col-xs-3">
                                 <input type="text"  name="alt_country_code" value="{{$user->AlternateCCode}}" placeholder="Country Code" class="form-control maxlength-3" maxlength="4">
                                 <p class="help-block red text-color"  >{{$errors->first('country_code')}} </p>
                              </div>
                              <div class="col-xs-9">
                                 <input type="text"  class="form-control numeric minlength-8 maxlength-12" placeholder="Phone number" name="alt_phonNumber" value="{{$user->AlternatePhoneNo}}" pattern=".{8,12}" title="8 to 12 numbers">
                                 <p class="help-block red text-color"   >{{$errors->first('phoneno')}} </p>
                              </div>
                           </div>
                        </div>
                     </div>
              <div class="col-sm-12">
                  <div class="form-group">
                     <label class="control-label">SSN or other National ID No.</label>
                     <input type="text" name="ssn" placeholder="SSN or other National ID No" class="form-control required  usename-#ssn# maxlength-15" value="{{ucfirst($user->SSN)}}" maxlength="15">
                     <p class="help-block red text-color"  >{{$errors->first('ssn')}} </p>
                  </div>
               </div>
              <div class="col-sm-12">
                 <div class="form-group">
                  <label class="control-label">Type of ID  above.</label>
                  <input  name="type_of_id" placeholder="Type of ID." class="form-control required maxlength-25" maxlength="15" type="text" value="{{ucfirst($user->type_of_id)}}">
                  <p class="help-block red text-color">{{$errors->first('type_of_id')}}</p>
                </div>
              </div>

               <div class="col-sm-12">
                      <div class="form-group">
                         <label class="control-label">Address line 1</label>
                         <input type="text" value="{{$user->Street1}}" name="address1" placeholder="Address" class="form-control required usename-#address#" maxlength="200">
                         <p class="help-block red text-color" >{{$errors->first('address')}} </p>
                      </div>
                </div>

               <div class="col-sm-4">
                  <div class="form-group">
                     <label class="control-label">Country</label>
                      <select name="country" class="form-control required usename-#country#" id="pp_pickup_country10" onchange="get_state2('pp_pickup_country10','pp_pickup_state10','pp_pickup_city10','pp_pickup_state10','','','','10','')">


                               <option value="">Select Country</option>
                               @foreach($country as $key)
                                 <option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}","state_available":"{{$key->state_available}}"}' @if($key->Content == $user->Country) selected='selected' @endif >{{$key->Content}}</option>

                               @endforeach
                           </select>
                     </div>
                  </div>
                  <div class="col-sm-4">
                     <div class="form-group">
                        <label class="control-label">State</label>
                         <span id="ap_id10">
                         <select name="state10" class="form-control required usename-#state# left-disabled chosen-select" id="pp_pickup_state10" onchange="get_city('pp_pickup_state10','pp_pickup_city','pp_pickup_city','{{$user->City}}')">
                               <option value="">Select State</option>
                               @foreach($state as $key)
                                  <option value='{"id":"<?php echo $key['_id']; ?>","name":"<?php echo $key['Content']; ?>  "}' @if($user->State == $key['Content']) selected="selected" @endif>{{$key['Content']}}</option>
                              @endforeach
                           </select>
                        </span>
                     </div>
                  </div>

                  <div class="col-sm-4">
                     <div class="form-group">
                        <label class="control-label">City</label>
                        <select  name="city" class="form-control required chosen-select usename-#city#" id="pp_pickup_city10">
                               <option value="{{$user->City}}">Select City</option>
                               @foreach($city as $key)
                                  <option value='{"id":"<?php echo $key['_id']; ?>","name":"<?php echo $key['Content']; ?>  "}' @if($user->City == $key['Content']) selected="selected" @endif>{{$key['Content']}}</option>
                              @endforeach
                        </select>

                  </div>
               </div>
               <div class="col-sm-12 row">
               <div class="col-sm-4">
                  <div class="form-group">
                     <label class="control-label">Zipcode</label>
                     <input type="text" value="{{$user->ZipCode}}" placeholder="Zipcode" name="zipcode" class="form-control numeric usename-#zipcode#" maxlength="8">
                     <p class="help-block red text-color"  >{{$errors->first('zipcode')}} </p>
                  </div>
               </div>
           <!--    <div class="col-sm-4">
                  <div class="form-group">
                     <label class="control-label">Age</label>
                     <input type="text" value="{{$user->Age}}" name="age" placeholder="Age" class="form-control numeric usename-#age#" maxlength="3">
                     <p class="help-block red text-color"  >{{$errors->first('age')}} </p>
                  </div>
               </div>   -->
               </div>

               <div class="col-sm-12">
               <div class="col-sm-3">
                  <label class="control-label">License ID</label>
                  <div class="selected-pic">
                     <div>
                        @if($user->LicenceId != '')
                          <a class="fancybox" rel="group" href="{{ ImageUrl.$user->LicenceId}}" >
                           <img src="{{ ImageUrl.$user->LicenceId}}"  id="licenceid" width="320px" height="170px" >
                          </a>
                        @else
                        <p></p>
                        <img src="{{ImageUrl}}/user-no-image.jpg" id="licenceid" width="200px" height="150px">
                        @endif
                     </div>
                     <label class="custom-input-file">
                       <input type="file" name="licenceid" class="custom-input-file" id="id-proof" onchange ="image_preview(this,'licenceid',event)">

                     </label>
                  </div>

               </div>
                 @if($user->BusinessName != '')
                <div class="col-sm-3">
                  <label class="control-label">Business ID</label>
                  <div class="selected-pic">
                     <div>
                        @if($user->IDProof != '')
                          <a class="fancybox" rel="group" href="{{ ImageUrl.$user->IDProof}}" >
                           <img src="{{ ImageUrl.$user->IDProof}}" id="id_proof" width="320px" height="170px" >
                          </a>
                        @else
                        <p></p>
                        <img src="{{ImageUrl}}/user-no-image.jpg" id="id_proof" width="200px" height="150px">
                        @endif
                     </div>
                     <label class="custom-input-file">
                       <input type="file" name="id_proof" class="custom-input-file" id="licenceid" onchange="image_preview(this,'id_proof',event)">

                     </label>
                  </div>
               </div>
               @endif
               </div>




               <div class="col-sm-12">
                  <hr />
               </div>



               <div class="col-sm-8">
                  <div class="form-group">
                     <button class="custom-btn1 btn">
                        Update
                        <div class="custom-btn-h"></div>
                     </button>
                     <br></br><br></br>
                  </div>
               </div>
            </div>
            @endforeach
            {!! Form::close() !!}
         </div>
         @include('Page::layout.side_bar')
   </div>
</div>
   <script type="text/javascript">
$(document).ready(function() {

    $(".fancybox").fancybox();

  });


jQuery(document).ready(function ($) {

  $(".chosen-select").chosen({no_results_text: "Oops, nothing found!"});
  $(".chosen-select").chosen().change(function() {
        //alert(+$(this).val());
    });
});
</script>
@endsection


@section('script')
@parent

{!! Html::script('theme/web/js/utility.js') !!}


@endsection

@section('inline-script')
@parent
<script>
  new Validate({
    FormName : 'edit_transporter_profile',
    ErrorLevel : 1,
    validateHidden: false,

  });



function image_preview(obj,previewid,evt)
{
    var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
    if ($.inArray($(obj).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
       $(obj).val('');
       $('#'+previewid).attr('src', '{{ImageUrl}}/user-no-image.jpg');
        alert("Only "+fileExtension.join(', ')+" formats are allowed.");
     } else {

       var file = evt.target.files[0];
       if (file)
       {
         var reader = new FileReader();

         reader.onload = function (e) {
             $('#'+previewid).attr('src', e.target.result)
         };
         reader.readAsDataURL(file);
       }
     }
}

//get_state('pp_pickup_country','pp_pickup_state','pp_pickup_city','pp_pickup_state','{{$user->State}}','{{$user->City}}');

</script>

<style>
.text-color
{
  color:#a944423;
}
</style>
@endsection
