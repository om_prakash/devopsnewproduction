@extends('Page::layout.one-column-page')
@section('page_title')
Shipping Dashboard - Aquantuo
@endsection
@section('content')

<div class="container">
		<div class="row">
        	<div class="col-sm-12">
			   <h2 class="color-blue mainHeading">Dashboard &nbsp;&nbsp; <a href="/my-aquantuo-address" style="color: #219ed4">
			   Your Aquantuo address is: {{$address->AqAddress}} Unit#  {{$address->UniqueNo}} {{ucfirst($address->AqCity)}}&nbsp;{{ucfirst($address->AqState)}}&nbsp;{{ucfirst($address->AqZipcode)}} {{ucfirst($address->AqCountry)}}
			   </a></h2>
        	</div>
			<br /><br />
			<div class="col-md-12 ">
				<div class="row">

					<div class="col-md-4 col-xs-12">
						<div class="dashboard-bx blue-bg clearfix">
							<div class="pull-left dashb-txt">
								<a href="#">

								<h1>{{$users}}</h1>

								<p>Request(s)</p>
								</a>
							</div>
							<div class="dashb-icon"><i class="fa fa-archive fa-4x"></i></div>
						</div>
					</div>



					</div>
			</div>
		</div>
				<!-- Main row -->
         		<div class="clearfix"></div>

	</div><!--/.col-->



@endsection
