

@extends('Page::layout.one-column-page')
@section('content')
{!! Html::script('theme/web/rating/jquery.rateyo.js') !!}
{!!Html::style('theme/web/rating/jquery.rateyo.min.css')!!}
<style>
   .modal-feedback{
   margin:30px auto;
   width:420px;
   position: relative;
   }
</style>
<div class="container">
   <div class="row">
      <div class="col-sm-12">
         <h2 class="color-blue mainHeading">Delivery Details</h2>
         <br />
         <div id="success-message"></div>
      </div>
      @foreach($transporterDetail as $user)
      <div class="col-md-12">
         <div class="box-shadow">
            <div class="content-section clearfix">
               <div class="col-md-2 col-sm-3">
                  @if($user->ProductImage != '')
                  <a class="fancybox" rel="group" href="{{ ImageUrl.$user->ProductImage}}" >
                  <img src="{{ ImageUrl.$user->ProductImage}}" class="" width="150px" height="150px" >
                  </a>
                  @else
                  <img src="{{ ImageUrl}}no-image.jpg" width="150px" class="" height="150px"  class="img-rounded">
                  @endif
               </div>
               <div class="col-md-10 col-sm-9">
                  <span class="pull-right">
                  <a href="{{url('transporter')}}"  class="btn btn-default blue-btn">
                  <i class="fa fa-arrow-left"> </i> Back</a>&nbsp;&nbsp;</span>
                  <h4><b>{{ucfirst($user->ProductTitle)}}</b></h4>
                  <p class="color-black">
                     <strong>Package Id:</strong> {{$user->PackageNumber}}
                  </p>
                  <p class="color-black" id="status_div"><strong>Status:</strong>
                     @if($user->Status == 'out_for_pickup')
                     Out for Pickup
                     @elseif($user->Status == 'out_for_delivery')
                     Out for Delivery
                     @else
                     {{ucfirst($user->Status)}}
                     @endif
                  </p>
                  <p class="color-black" style="display:none;" id="ajax_status"><strong>Status:</strong></P>
                  <div class="form-group">
                     @if($user->Status == "ready")
                     <button  class="btn btn-default blue-btn"  title="Accept" id="accept_request"  data-target="#exampleModal1" data-toggle="modal">
                     &nbsp; Accept &nbsp;&nbsp;</button>
                     @endif
                     <?php $btn_status = 'display:none';
if (in_array($user->Status, ['accepted', 'out_for_pickup'])) {$btn_status = 'display:inline';}?>
                     <button style="{{$btn_status}}" class="btn btn-danger"  title="Cancel" id="cancel_request" onclick="return confirm('Are you sure you want to cancel this request?')? cancel_request():'';" >
                     &nbsp; Cancel &nbsp;&nbsp;</button>
                     <?php $btn_status = 'display:none';
if ($user->Status == "accepted") {$btn_status = 'display:inline';}?>
                     <button  class="btn btn-default blue-btn"  title="Out for pickup" id= "out_for_picup" onclick="return confirm('Are you sure you are going to pick up this request?')? out_for_pickup():'';" style="{{$btn_status}}">
                     &nbsp; Out for Pickup &nbsp;&nbsp;</button>
                     <?php $btn_status = 'display:none';
if ($user->Status == "out_for_pickup") {$btn_status = 'display:inline';}?>
                     <button  class="btn btn-default blue-btn"  title="Out for delivery" id= "out_for_delivery"  onclick="return confirm('Are you sure this package is going to be delivered?')? out_for_deliver():'';" style="{{$btn_status}}">
                     &nbsp; Out for Delivery &nbsp;&nbsp;</button>
                     <?php $btn_status = 'display:none';
if ($user->Status == "out_for_delivery") {$btn_status = 'display:inline';}?>
                     <button class="btn btn-default blue-btn" href="javascript:void(0)" data-toggle="modal" data-target="#deliver_model" title="delivery"   id="deliver" style="{{$btn_status}}"> &nbsp;Delivered&nbsp;</button>

                     <!-- <button class="btn btn-default blue-btn" href="javascript:void(0)" data-toggle="modal" data-target="#deliver_model" title="delivery"   id="deliver" > &nbsp;Delivered&nbsp;</button> -->


                     <button class="btn btn-default blue-btn" href="javascript:void(0)" data-toggle="modal" data-target="#exampleModal" title="Cancel Delivery"   id="delivery_btn" style="{{$btn_status}}"> &nbsp;Not Delivered</button>
                     <?php $btn_status = 'display:none';
if ($user->Status == "delivered" && $user->TransporterRating == '') {$btn_status = 'display:true';}?>
                     <a href="javascript:void(0)" data-toggle="modal" data-target="#modal2" title="Review"  class="btn btn-default blue-btn" id= "feedback" style="{{$btn_status}}">Give Feedback</a>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="col-sm-4">
         <div class="clearfix detail-tab">
            <div class="tab-content box-shadow">
               <div class="content-section clearfix">
                  <div class="col-md-12">
                     <h4><b>Requester Details</b></h4>
                     <br>
                  </div>
                  <div class="col-md-4">
                     <?php $file_exists = false;?>
                     @if(count($user_data) > 0)
                     @if(!empty($user_data->Image))
                     <?php $file_exists = true?>
                     <a class="fancybox" rel="group" href="{{ImageUrl.$user_data->Image}}">
                     <img width="100px" height="100px" class="img-circle" src="{{ImageUrl.$user_data->Image}}">
                     </a>
                     @endif
                     @endif
                     @if(!$file_exists)
                     <img width="100px" height="100px" class="img-circle" src="{{ImageUrl}}/no-image.jpg">
                     @endif
                  </div>
                  <div class="col-md-8">
                     <p><b>{{ucfirst($user->RequesterName)}}</b></p>
                     <div class="form-group">
                        @if(count($user_data) > 0)
                        <?php $average_rating = 0;
if ($user_data->RatingCount > 0 && $user_data->RatingByCount > 0) {
    $rating = $user_data->RatingCount / $user_data->RatingByCount;
    $average_rating = 20 * $rating;
}
?>
                        <div class="form-group">
                           <div class="star-ratings-sprite pull-left">
                              <span style="width:<?php echo $average_rating ?>%" class="star-ratings-sprite-rating"></span>
                           </div>
                           <div class="clearfix"></div>
                        </div>
                        @endif
                        <div class="clearfix"></div>
                     </div>
                     <div class="clearfix"></div>
                     <div class="form-group">
                        <div class="">
                           <?php $btn_status = 'display:none';
if (in_array($user->Status, ['out_for_pickup', 'out_for_delivery', 'accepted'])) {
    $btn_status = 'display:inline';}?>
                           <a class="btn btn-primary" href="javascript:void(0)"  style="{{$btn_status}}" id="chat-btn" onclick="open_chat_box('{{$user->RequesterId}}','{{$user->RequesterName}}','chat-btn')"><i class="fa fa-chat"></i> Chat</a>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row">
                  @if(!$user->TransporterFeedbcak == '')
                  <div class="col-sm-12">
                     <h4><b>Your Feedback</b></h4>
                     <p>{{ucfirst($user->TransporterFeedbcak)}}</p>
                     <?php $transporter_rating = 0;
if (count($user->TransporterRating) > 0) {
    $transporter_rating = 20 * $user->TransporterRating;

}

?>
                     <div class="star-ratings-sprite pull-left">
                        <span style="width:<?php echo $transporter_rating; ?>%" class="star-ratings-sprite-rating"></span>
                     </div>
                  </div>
                  <div class="col-sm-12">
                     <hr>
                  </div>
                  @endif
                  @if(!$user->RequesterFeedbcak == '')
                  <div class="col-sm-12">
                     <h4><b>Requester Feedback</b></h4>
                     <p>{{ucfirst($user->RequesterFeedbcak)}}</p>
                     <?php $requester_rating = 0;
if (count($user->RequesterRating) > 0) {
    $requester_rating = 20 * $user->RequesterRating;

}

?>
                     <div class="star-ratings-sprite pull-left">
                        <span style="width:<?php echo $requester_rating; ?>%" class="star-ratings-sprite-rating"></span>
                     </div>
                     <div class="col-sm-12">
                        <hr>
                     </div>
                  </div>
                  @endif
               </div>
            </div>
         </div>
      </div>
      <div class="col-sm-8">
         <div class="clearfix detail-tab">
            <ul class="nav nav-tabs" role="tablist">
               <li role="presentation" class="active" ><a href="#profile" aria-controls="profile" class="active" role="tab" data-toggle="tab">Delivery Information</a></li>
               <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Package Information</a></li>
               @if($user->Status == "cancel")
               <li role="presentation"><a href="#cancel_detail" aria-controls="cancel_detail" role="tab" data-toggle="tab">Cancel Detail</a></li>
               @endif


            </ul>
            <div class="tab-content box-shadow">
               <div role="tabpanel" class="tab-pane active" id="profile">
                  <div class="details-list">
                     <p><b>Pickup Location: </b> {{ucfirst($user->PickupFullAddress)}} </p>
                  </div>
                  <div class="details-list">
                     <p><b>This is a Public Place:</b> {{ucfirst($user->PublicPlace)}} </p>
                     @if($user->PublicPlace == 'yes')
                     The Requester has opted to meet in public
                     @endif
                  </div>
                  <div class="details-list">
                     <p><b>Drop of Location:</b> {{ucfirst($user->DeliveryFullAddress)}}</p>
                     {{ show_date(@$user->DeliveryDate) }}
                  </div>
                  <div class="details-list">
                     <p><b>Is Date Flexible:</b> {{ucfirst($user->FlexibleDeliveryDate)}} </p>
                  </div>
                  <?php if ($user->Status != 'ready') {?>
                  <div class="details-list">
                     <p><b>Receiver Phone Number:</b>
                        @if($user->ReceiverMobileNo != '')
                         {{$user->ReceiverCountrycode}} - {{$user->ReceiverMobileNo}}
                     </p>
                     @endif
                  </div>
                  <?php }?>
                  <div class="details-list">
                     @if($user->JournyType == 'one_way')
                  </div>
                  <div class="details-list">
                     <p><b>Journy Type: </b>One way</p>
                  </div>
                  @endif
                  @if($user->JournyType != 'one_way')
                  <div class="details-list">
                     <p><b>Return Address: </b> {{ucfirst($user->ReturnAddress)}}    </p>
                  </div>
                  @endif
                  <div class="details-list">
                     <p><b>Shipping Mode: </b>{{ucfirst($user->TravelMode)}}</p>
                  </div>
                  <div class="details-list">
                     <p><b>Return Address (if item is not delivered) : </b> {{ucfirst($user->NotDelReturnFullAddress)}}</p>
                  </div>
                  <div class="details-list">
                     <p><b>Need Package Material: </b>
                     <?php echo ($user->PackageMaterial == 'Yes') ? 'Yes' : 'N/A'; ?>
                     </p>
                  </div>
                  @if($user->PackageMaterial == 'yes' && in_array($user->Status, ['out_for_pickup']))
                  <div class="details-list">
                     <p><b>Package Material Shipped: </b>
                        <span id="package_mateiral_text">
                        {{ucfirst(str_replace('_',' ',$user->PackageMaterialShipped))}}
                        </span> <br />
                        @if($user->PackageMaterialShipped == '')
                        <button  class="btn btn-default blue-btn"  title="Yes" id="package_mateiral_yes" onclick="package_material('yes','package_mateiral_yes')">
                        &nbsp; Yes &nbsp;&nbsp;</button>
                        <button  class="btn btn-default blue-btn"  title="Not Yet" id="package_mateiral_not_yet" onclick="package_material('not_yet','package_mateiral_not_yet')">
                        &nbsp; Not Yet &nbsp;&nbsp;</button>
                        @endif
                     </p>
                  </div>
                  @endif
               </div>
               <div role="tabpanel" class="tab-pane" id="cancel_detail">
                  <br /><br />
                  <div class="details-list">
                     {!! Form::label('return_address', 'Return Address:',['class' => 'control-label']) !!} {{$user->NotDelReturnFullAddress}}
                  </div>
                  <div class="details-list">
                     {!! Form::label('reject_by', 'Reject By:',['class' => 'control-label']) !!}
                     {{ucfirst($user->RejectBy)}}
                  </div>
                  <div class="details-list">
                     {!! Form::label('reject_by', 'Return Type:',['class' => 'control-label']) !!}
                     {{get_cancel_status($user->ReturnType)}}
                  </div>
                  <div class="details-list">
                     <p>
                        <b>Message: </b> {{ucfirst($user->TransporterMessage)}}
                     </p>
                  </div>
                  @if(!empty(trim($user->TrackingNumber)))
                  <div class="details-list">
                     <p>
                        <b>Tracking Number: </b> {{$user->TrackingNumber}}
                     </p>
                  </div>
                  @endif
                  @if(isset($user->CancelReturnDate->sec))
                  <div class="details-list">
                     <p>
                        <b>Date: </b> {{date('d M ,Y  h:i A',$user->CancelReturnDate->sec)}}
                     </p>
                  </div>
                  @endif
                  @if(!empty(trim($user->ReceiptImage)))
                  @if(file_exists(BASEURL_FILE.$user->ReceiptImage))
                  <div class="details-list">
                     <p>
                        <b>Receipt: </b>  <br />
                        <a class="fancybox" rel="group" href="{{ImageUrl.$user->ReceiptImage}}" >
                        <img src="{{ImageUrl.$user->ReceiptImage}}" alt="receipt" height="100" width="100" >
                        </a>
                     </p>
                  </div>
                  @endif
                  @endif
                  <br /><br />
               </div>



               <div role="tabpanel" class="tab-pane" id="messages">
                  </br>
                  <p><b>Package Value:</b> ${{ number_format((float)$user->ProductCost,2) }}</p>
                  <p><b>Weight:</b>
                     <?php echo number_format(change_unit
    ($user->ProductWeight, $user->ProductWeightUnit, 'kg'), 0) . ' Kg'; ?> /
                     <?php echo number_format(change_unit
    ($user->ProductWeight, $user->ProductWeightUnit, 'lbs'), 0) . ' Lbs'; ?>
                  </p>
                  <p><b>Package Dimensions:</b>
                     <?php echo "L-";
echo number_format(change_unit
    ($user->ProductLength, $user->ProductLengthUnit, 'inch'), 0) . 'Inch'; ?>,
                     <?php echo "H-";
echo number_format(change_unit
    ($user->ProductHeight, $user->ProductHeightUnit, 'inch'), 0) . 'Inch'; ?>,
                     <?php echo "W-";
echo number_format(change_unit
    ($user->ProductWidth, $user->ProductWidthUnit, 'inch'), 0) . 'Inch'; ?>/
                     <?php echo "L-";
echo number_format(change_unit
    ($user->ProductLength, $user->ProductLengthUnit, 'cm'), 0) . 'Cm'; ?>,
                     <?php echo "H-";
echo number_format(change_unit
    ($user->ProductHeight, $user->ProductHeightUnit, 'cm'), 0) . 'Cm'; ?>,
                     <?php echo "W-";
echo number_format(change_unit
    ($user->ProductWidth, $user->ProductWidthUnit, 'cm'), 0) . 'Cm'; ?>
                  </p>
                  @if($user->BoxQuantity == '0')
                  <p><b>Quantity:</b> 1 Box(es)</p>
                  @else
                  <p><b>Quantity:</b> {{$user->BoxQuantity }} Box(es)</p>
                  @endif
                  <p><b>Description :</b> {{ucfirst($user->Description) }}</p>

                  <!--<p><b>Insurance : </b> {{ucfirst($user->InsuranceStatus)}}</p>-->
                  <p><b>Need Package Material:</b> {{ucfirst($user->PackageMaterial)}}
                     @if($user->PackageMaterial == 'yes')
                  <p>Package Material Shipped  </p>
                  @endif
                  <div class="list-footer">
                     <div class="row">
                        <div class="col-sm-4">
                           <p><b>Shipping Cost: </b>
                              <span class="pull-right"> ${{ number_format((float)$user-> ShippingCost,2) }} </span>
                           </p>
                           <p><b>Aquantuo Fees: </b>
                              <span class="pull-right">(-)${{ number_format((float)$user->AquantuoFees,2) }}
                           </p>
                           <?php $result = ($user->ShippingCost) - ($user->AquantuoFees);?>  </span>
                           <p><b>Your Income: </b>
                              <span class="pull-right"> ${{ number_format((float)$result,2) }} </span>
                           </p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="">
      <div class=" box-shadow">
         <h4 class="">Other Images</h4>
         @foreach($user->OtherImage  as $key )
         @if(!empty(trim($key)))
         @if(file_exists(BASEURL_FILE.$key))
         <div class="col-sm-2">
            <a class="fancybox" rel="group" href="{{ ImageUrl.$key}}" >
            <img src="{{ ImageUrl.$key}}" width="100px" height="100px" class="image-style">
            </a>
         </div>
         @endif
         @endif
         @endforeach
         @if(count($user->OtherImage) <= 0)
         <center>
            <h4>No other image uploaded</h4>
         </center>
         @endif
         <div class="clearfix"></div>
         <br /><br />
      </div>
   </div>
   <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
      <div class="modal-dialog" role="document">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
               <h4 class="modal-title" id="exampleModalLabel">Not Delivered</h4>
            </div>
            {!! Form::model('', ['name' => 'ServicesForm', 'id' => 'ServicesForm', 'method' => 'POST','files' => true] ) !!}
            <div class="modal-body">
               <div class="form-group">
                  {!! Form::label('package_id', 'Package Title:',['class' => 'control-label']) !!} {{ucfirst($user->ProductTitle)}}
               </div>
               <div class="form-group">
                  {!! Form::label('package_id', 'Package ID:',['class' => 'control-label']) !!} {{$user->PackageNumber}}
               </div>
               <div class="form-group">
                  {!! Form::label('return_address', 'Return Address:',['class' => 'control-label']) !!} {{$user->NotDelReturnFullAddress}}
               </div>
               <div class="row">
                  <div class="col-sm-6">
                     <div class="form-group" id="valid-error">
                        <label class="controle-label">Reject By:</label><br>
                        <label class="radio-inline">
                        <input type="radio" name="reject_by" value="transporter" checked onclick="return select_transporter()"> Transporter
                        </label>
                        <label class="radio-inline">
                        <input type="radio" name="reject_by" value="requester" onclick="return select_requester()">&nbsp;Requester
                        </label>
                     </div>
                  </div>
                  <div class="col-sm-6">
                     <div class="form-group" id="valid-error">
                        <label class="controle-label">Return Type:</label><br>
                        <label class="radio-inline">
                        <input type="radio" name="return_type" value="osreturn" <?php echo 'checked=="checked"'; ?> onclick="return select_os()">&nbsp;OS Return
                        </label>
                        <span id="bycreturn" style="display:none;">
                        <label class="radio-inline">
                        <input type="radio" name="return_type" value="creturn" onclick="return select_bycreturn()">&nbsp;Bycreturn
                        </label>
                        </span>
                        <span id="ipayment">
                        <label class="radio-inline">
                        <input type="radio" name="return_type" value="ipayment" onclick="return select_ipayment()">&nbsp;iPayment
                        </label>
                        </span>
                     </div>
                  </div>
               </div>
               <div class="form-group" id="msg_div">
                  {!! Form::label('message', 'Message:',['class' => 'control-label']) !!}
                  {!! Form::textarea('message','',['class'=>'form-control required', 'placeholder'=> 'Message','id' =>'message','maxlength' =>200,'size' =>'10x4']) !!}
                  <p class="help-block red" id='er_message'></p>
                  @if($errors->has('message'))
                  <div class="error" style="color:red">{{ $errors->first('message') }}</div>
                  @endif
               </div>
               <div class="form-group" id="tracking_div">
                  {!! Form::label('tracking_number', 'Tracking Number:',['class' => 'control-label']) !!}
                  {!! Form::text('tracking_number', '', ['class'=>'form-control required numeric', 'placeholder'=> 'Tracking Number','id' =>'tracking_number','maxlength' => 20]) !!}
                  <p class="help-block red" id='er_tracking_number'></p>
                  @if($errors->has('tracking_number'))
                  <div class="error" style="color:red">{{ $errors->first('tracking_number') }}</div>
                  @endif
               </div>
               <div class="form-group" id="date_div" style="">
                  {!! Form::label('Date', 'Date:',['class' => 'control-label']) !!}
                  {!!Form::text('date','',['class'=>'form-control required','readonly'=>'readonly','id'=>'date'])!!}
                  <p class="help-block red" id='er_date'></p>
                  @if($errors->has('date'))
                  <div class="error" style="color:red">{{ $errors->first('date') }}</div>
                  @endif
               </div>
               <div class="form-group" id="receipt_div">
                  {!! Form::label('image', 'Upload Receipt:',['class' => 'control-label']) !!}
                  {!! Form::file('image', '', ['class'=>'form-control', 'placeholder'=> '','id'=>'image','width'=>'250px','height'=>'250px'])!!}
                  <p class="help-block red" id='er_image'></p>
                  @if($errors->has('image'))
                  <div class="error" style="color:red">{{ $errors->first('image') }}</div>
                  @endif
               </div>
            </div>
            <div class="modal-footer">
               <div class="form-group">
                  <button type="submit" class="custom-btn1" id="cancel_delivery_submit">
                     Submit
                     <div class="custom-btn-h"></div>
                  </button>
                  <div class="col-xs-6">
                     <button class="custom-btn1" data-dismiss="modal">
                        Close
                        <div class="custom-btn-h"></div>
                     </button>
                  </div>
               </div>
            </div>
            {!! Form::close() !!}
            <div class="clearfix"></div>
         </div>
      </div>
   </div>
   <!-- Rating section start -->
   <div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
      <div class="modal-feedback modal-md">
         <div class="modal-content">
            <div class="modal-header popup-bg text-center">
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
               <p class="popup-head color-white">Rate your experience with Requester</p>
            </div>
            <div class="clearfix"></div>
            <center>
               <div class="modal-body text-left">
                  {!! Form::model('', ['name' => 'RequestReview', 'id' => 'RequestReview']) !!}
                  <div class="col-md-12" id="rating_success_container"></div>
                  <div class="col-md-12">
                     <div class="col-md-12" align="center">
                        <div>
                           @if(count($user_data) > 0)
                           <?php $file_exists = false;?>
                           @if(!empty($user_data->Image))
                           @if(file_exists(BASEURL_FILE.$user_data->Image))
                           <?php $file_exists = true?>
                           <a class="fancybox" rel="group" href="{{ImageUrl}}{{$user_data->Image}}">
                              <img width="100px" height="100px" class="img-circle" src="{{ImageUrl}}{{$user_data->Image}}">
                           </a>
                           @endif
                           @endif
                           @if(!$file_exists)
                              <img width="100px" height="100px" class="img-circle" src="{{ImageUrl}}/no-image.jpg">
                           @endif
                           @endif
                        </div>
                        </br>

                        <div>
                           <label class="rating_margin" >Rate Now</label>
                           &nbsp;&nbsp;&nbsp;
                           <div class="rateyo" id="rateyo" ></div>
                           <div class="clearfix"></div>
                           <div id="er_rating"  class="error-msg"></div>
                           <input type="hidden" name="request_id" id="request_id" value='<?php echo $user->_id ?>'>
                           <input type="hidden" name="requester_id" id="requester_id" value='<?php echo $user->RequesterId ?>'>
                           <br />
                           <script type="text/javascript">
                              $(document).ready(function() {
                              $(".fancybox").fancybox();
                              });
                              var $rateYo = $("#rateyo").rateYo();
                           </script>
                        </div>
                        <div class="form-group txt-area feedback-text">
                           {!! Form::textarea('review','',['class'=>'form-control required', 'placeholder'=> 'Feedback','id' =>'review','maxlength' =>250,'size' =>'30x4']) !!}
                        </div>
                        <div class="clearfix"></div>
                     </div>
                  </div>
                  <div class="col-md-12">
                     <div class="col-md-4"></div>
                     <div class="col-md-8">
                     </div>
                     <div class="col-md-2"></div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="col-xs-12">
                     <div class="">
                        <div class="col-xs-6">
                           <div class="form-group">
                              <button type="submit" class="custom-btn1 btn-block" id='ratingButton'>SUBMIT</button>
                              <div class="error-msg"></div>
                           </div>
                        </div>
                        <div class="col-xs-6">
                           <div class="form-group text-center">
                              <button class="custom-btn1 btn-block" data-dismiss='modal' >NOT NOW</button>
                           </div>
                        </div>
                     </div>
                  </div>
                  {!! Form::close() !!}
               </div>
            </center>
            <div class="clearfix"></div>
            </center>
         </div>
      </div>
   </div>
   <!-- Rating section end -->
   <div class="modal fade" id="deliver_model" role="dialog">
      <div class="modal-feedback modal-sm">
         <div class="modal-content">
            <div class="modal-header">
               <button  class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title"><b>Delivery Verification</b></h4>
            </div>
            <div class="modal-body" >
               {{-- <div class="checkbox">
              <label>
               <input type="checkbox" value="" class="mt0" id="accept_tc" >
               I Accept <a href="{{url('terms-and-conditions')}}" target="_blank" style="font-weight:600;margin-bottom:0px;">Terms and Conditions.</a>
              </label>
            </div> --}}

               <div class="map-hloder" >
                  <input type="hidden" name="request_id" id="request_id" value='<?php echo $user->_id ?>'>
                  {!! Form::model('', ['name' => 'verify_delivery_code', 'id' => 'verify_code']) !!}
                  {!!Form::label('code','Verification Code (obtain this from Requester)',['class'=>'control-label'])!!}
                  {!!Form::text('code','',['class'=>'form-control required numeric','id'=>'code','placeholder'=>'Enter Verification Code','maxlength' => 4])!!}
               </div>
            </div>
            <div class="col-sm-2 col-xs-3">
               {!! Form::button('Submit', ['class' => 'btn btn-default','value'=>'SUBMIT','id'=>'verifybutton','type' => 'submit']) !!}
            </div>
            <div class="col-sm-10 col-xs-11">
               {!! Form::button('Close', ['class' => 'btn btn-default', 'data-dismiss'=>'modal','value'=>"Not Now"]) !!}
            </div>
            <div class="">&nbsp;&nbsp;&nbsp;&nbsp;</div>
            {!! Form::close() !!}
         </div>
      </div>
   </div>
</div>
@endforeach
{!! Html::script('theme/web/js/validation.js') !!}
{!! Html::script('theme/web/js/utility.js') !!}
{!! Html::style('theme/date-time-picker/jquery.datetimepicker.css') !!}
{!! Html::script('theme/date-time-picker/jquery.datetimepicker.js') !!}
<script type="text/javascript">
   function package_material(status,id) {

       if(!$("#"+id).hasClass('spinning'))
       {
         $("#"+id).addClass('spinning');
         $.ajax({
           url     : '{{url("/need_package_material")}}',
           type    : 'post',
           data    : {
             id : '{{$transporterDetail[0]->_id}}',
             status: status
           },
           dataType: "json",
           success : function(res) {
               $("#"+id).removeClass('spinning');

               if(res.success == 1)
               {
                   $('#package_mateiral_text').html('Not Applicable');
                   if(res.status == 'yes') {
                     $('#package_mateiral_text').html('Yes');
                   }
                   $('#package_mateiral_yes').hide();
                   $('#package_mateiral_not_yet').hide();
               } else {
                 alert(res.msg);
               }

             }
         });
       }
   }

      new Validate({
         FormName : 'ServicesForm',
         ErrorLevel : 1,
         validateHidden: false,
         callback : function()
       {
         if(!$('#cancel_delivery_submit').hasClass('spinning'))
         {
            $('#cancel_delivery_submit').addClass('spinning');
           $.ajax
           ({
             type: "POST",
             url: "{{ url('/no_delivery') }}/{{Request::segment(3)}}",
             data: new FormData(document.getElementById('ServicesForm')),
             processData: false,
             contentType: false,
             dataType: "json",
             success: function(obj)
             {
                 alert(obj.msg);
                 $('#cancel_delivery_submit').removeClass('spinning');
                 if(obj.success == 1)
                 {
                     $( "#exampleModal" ).trigger( "click" );
                     $('#success-message').html('<div role="alert" class="alert alert-success alert-dismissible fade in">'
                     +'<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>'+obj.msg+'</div>');

                     $("#delivery_btn").hide();
                     $("#complete").hide();
                     $("#status_div").hide();
                     $("#ajax_status").show();
                     $('#chat-btn').hide();
                     $("#ajax_status").html('<strong>Status:</strong> Cancel');
                     window.location.reload();
                 }
             }
           });
         }
         }

      });


      function accept_request()
      {
         $('#accept_request').addClass('spinning');
         $('#accept_request').prop('disabled', true);
           $.ajax
           ({
           type: "POST",
           url: "{{ url('/accept-request-transporter') }}/{{Request::segment(3)}}",

           success: function(res)
           {
           var obj = eval('('+res+')');
           console.log(obj);
           $('#accept_request').removeClass('spinning');
           if(obj.success == 1)
           {
            $('#success-message').html('<div role="alert" class="alert alert-success alert-dismissible fade in">'
            +'<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>'+obj.msg+'</div>');

            $("#accept_request").hide();
            $("#cancel_request").show();
            $("#out_for_picup").show();
            $("#status_div").hide();
         $('#chat-btn').show();
            $("#ajax_status").show();
            $("#ajax_status").html('<strong>Status:</strong> Accepted');



           }else {
              alert(obj.msg);
              window.location.reload();

           }

           }
           });
            return false;

      }


      function cancel_request()
      {
         $('#cancel_request').addClass('spinning');
         $('#cancel_request').prop('disabled', true);
           $.ajax
           ({
           type: "POST",
           url: "{{ url('/cancel-request-transporter') }}/{{Request::segment(3)}}",
           success: function(res)
           {
           var obj = eval('('+res+')');
           alert(obj.msg);
           $('#cancel_request').removeClass('spinning');
           if(obj.success == 1)
           {
            //$("#ajax_status").html('<strong>Status:</strong> Ready');
           window.location.reload();
           }

           }
           });
            return false;

      }

      function out_for_pickup()
      {
         $('#out_for_picup').addClass('spinning');
         $('#out_for_picup').prop('disabled', true);
         $('#cancel_request').prop('disabled', true);
           $.ajax
           ({
           type: "POST",
           url: "{{ url('/pickup-request-transporter') }}/{{Request::segment(3)}}",

           success: function(res)
           {
           var obj = eval('('+res+')');

           $('#out_for_picup').removeClass('spinning');
           $('#cancel_request').prop('disabled',false);
           if(obj.success == 1)
           {
            $('#success-message').html('<div role="alert" class="alert alert-success alert-dismissible fade in">'
            +'<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>'+obj.msg+'</div>');


            $("#out_for_picup").hide();
            $("#out_for_delivery").show();
            $("#status_div").hide();
            $("#ajax_status").show();
            $("#ajax_status").html('<strong>Status:</strong> Out for Pickup');


           }else {
                alert(obj.msg);
           }

           }
           });
            return false;

      }

      function out_for_deliver()
      {
         $('#out_for_delivery').addClass('spinning');
         $('#out_for_delivery').prop('disabled', true);
         $('#cancel_request').prop('disabled',true);
           $.ajax
           ({
           type: "POST",
           url: "{{ url('/deliver-request-transporter') }}/{{Request::segment(3)}}",

           success: function(res)
           {
           var obj = eval('('+res+')');
           console.log(obj);
           $('#out_for_delivery').removeClass('spinning');
           $('#cancel_request').prop('disabled',false);
           if(obj.success == 1)
           {
            $('#success-message').html('<div role="alert" class="alert alert-success alert-dismissible fade in">'
            +'<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>'+obj.msg+'</div>');


            $("#out_for_delivery").hide();
         $("#cancel_request").hide();
            $("#delivery_btn").show();
            $("#deliver").show();
            $("#status_div").hide();
            $("#ajax_status").show();
            $("#ajax_status").html('<strong>Status:</strong> Out for Delivery');

           }else {
                alert(obj.msg);
           }

           }
           });
            return false;

      }

      function select_os()
      {
         if($('[name="reject_by"]:checked').val() == "transporter")
         {
            $('#msg_div').show();
            $('#tracking_div').show();
            $('#receipt_div').show();
            $('#date_div').hide();
         }

         if($('[name="reject_by"]:checked').val() == "requester")
         {
            $('#msg_div').show();
            $('#tracking_div').show();
            $('#receipt_div').show();
            $('#date_div').hide();
         }
      }

      function select_ipayment()
      {
         if($('[name="reject_by"]:checked').val() == "transporter")
         {
            $('#msg_div').show();
            $('#tracking_div').hide();
            $('#receipt_div').hide();
            $('#date_div').hide();
         }
      }

      function select_requester()
      {
         if($('[name="return_type"]:checked').val() == "ipayment")
         {

            $('input:radio[name=return_type][value=osreturn]').click();
         }
         $("#bycreturn").show();
         $("#ipayment").hide();


      }
      function select_transporter()
      {
         if($('[name="return_type"]:checked').val() == "creturn")
         {

            $('input:radio[name=return_type][value=osreturn]').click();
         }
         $("#ipayment").show();
         $("#bycreturn").hide();
         $('#date_div').hide();


      }

      function select_bycreturn()
      {
         if($('[name="reject_by"]:checked').val() == "transporter")
         {
            $('#msg_div').show();
            $('#tracking_div').hide();
            $('#receipt_div').hide();
         }

         else if($('[name="reject_by"]:checked').val() == "requester")
         {
            $('#msg_div').show();
            $('#tracking_div').hide();
            $('#receipt_div').hide();
            $('#date_div').show();

         }


      }


      select_os();

    var newdate = new Date();
       $('#date').datetimepicker({
          format:'M d, Y h:i:s A',
          minDate:newdate,
          onChangeDateTime:function( ct ){
              $('#date2').datetimepicker({  minDate:ct  })
          },
      });


      new Validate({
         FormName : 'RequestReview',
         ErrorLevel : 1,
         validateHidden: false,
         callback : function()
       {
         if($rateYo.rateYo("rating") <= 0) {
           $('#er_rating').html('The rating field is required.');
           return false;
         }
           $("#ratingButton").addClass('spinning');
           $('#ratingButton').prop('disabled', true);
           $.ajax({
             url     : '{{url("/transporter-review")}}',
             type    : 'post',
             data    : {
                 "review" : $("#review").val(),
                 "request_id" : $('#request_id').val(),
                 "rating" : $rateYo.rateYo("rating"),
                 "requester_id": $('#requester_id').val()
               },
               success : function(res)
               {
                   $("#ratingButton").removeClass('spinning');
                   var obj = eval("("+res+")");
                   if(obj.success == 1)
                   {
                     $("#feedback").hide();
                     location.reload();
                     $("#RequestReview").trigger( "reset" );
                     $('#rating_success_container').html('<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>'+obj.msg+'</div>');
                   }
                   else
                   {
                     $("#modal2").trigger('click' );
                     $("#feedback").hide();
                     $('#rating_success_container').html('<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>'+obj.msg+'</div>');
                   }
               }
           });
         }

      });


      new Validate({
         FormName : 'verify_delivery_code',
         ErrorLevel : 1,
         validateHidden: false,
         callback : function(){
         if(!$("#verifybutton").hasClass('spinning'))
         {
               if(confirm('Are you sure the package was successfully delivered?'))
               {
                $("#verifybutton").addClass('spinning');
                $.ajax({
                  url     : '{{url("/verify-delivery-code")}}',
                  type    : 'post',
                  data    : "code="+$("#code").val()+"&request_id="+$('#request_id').val(),
                  success : function(res)
                  {
                    $("#verifybutton").removeClass('spinning');
                    var obj = eval("("+res+")");
                    if(obj.success == 1)
                    {
                      $('#chat-btn').hide();
                      $( "#deliver_model" ).trigger( "click" );
                      $( "#deliver_model" ).trigger( "reset" );
                      $('#success-message').html('<div role="alert" class="alert alert-success alert-dismissible fade in">'
                      +'<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>'+obj.msg+'</div>');

                      document.getElementById("delivery_btn").style.display = "none";
                      $("#deliver").hide();
                      $("#delivery_btn").hide();
                      $("#status_div").hide();
                      $("#ajax_status").show();
                      $("#ajax_status").html('<strong>Status:</strong> Delivered');
                      $("#cancel_request").hide();
                      $("#feedback").show();
                    }
                    alert(obj.msg);
                }
                });
            }

         }
         }
      });

     function buy_for_me(type)
     {

         if(type == 'pickup')
         {
           $('#b_pickup').addClass('spinning');
         }

         if(type == 'delivery')
         {
           $('#b_delivery').addClass('spinning');
         }


         $.ajax
         ({
         type: "POST",
         url: "{{ url('/transporter-buy-for-me') }}/{{Request::segment(3)}}",
         data: "type="+type,

         success: function(res)
         {
           var obj = eval('('+res+')');
           if(type == 'pickup')
           {
             $('#b_pickup').removeClass('spinning');
           }

           if(type == 'delivery')
           {
             $('#b_delivery').removeClass('spinning');
           }


         if(obj.success == 1)
         {
         $('#success-message').html('<div role="alert" class="alert alert-success alert-dismissible fade in">'
         +'<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>'+obj.msg+'</div>');

           if(type == 'pickup')
           {
             $("#b_picup").hide();
             $("#b_delivery").show();
           }

           if(type == 'delivery')
           {
             $("#b_delivery").hide();

           }

           $("#status_div").hide();
           $("#ajax_status").show();
           $("#ajax_status").html('<strong>Status:</strong>'+obj.status);




         }else {
            alert(obj.msg);
           window.location.reload();
         }

         }
         });
         return false;

     }


</script>
<style>
   .rating_margin
   {
   transition: transform 0.8s cubic-bezier(0, 0.63, 0.41, 0.98) 0s, opacity 0.8s cubic-bezier(0, 0.63, 0.41, 0.98) 0s;
   transform: translateY(0px);
   opacity: 1;
   }
   .pre-modal-dialog
   {
   margin: 30px auto;
   width:500px;
   }
   .to-center{text-align:middle;}
</style>
<div aria-labelledby="exampleModalLabel" role="dialog" tabindex="-1" id="exampleModal1" class="modal fade in">
   <div role="document" class="pre-modal-dialog">
      <div class="modal-content width">
         <div class="modal-header" id="Addaddress2">
            <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button>
            <h4 id="exampleModalLabel" class="modal-title"><b>{{ucfirst($user->ProductTitle)}}</b></h4>
         </div>
         {!! Form::model('', ['name' => 'new_address', 'id' => 'Addaddress', 'method' => 'POST']) !!}
         <div class="panel-body">
            <div class="row">
               <div class="col-sm-10 col-sm-offset-1  text-primary" align="center">
                  <b>Are you sure you want to accept the package '{{ucfirst($user->ProductTitle)}}'? Accepted item must be delivered.</b>
                  <hr />
               </div>
            </div>
            <div class="row">
               <div class="col-sm-10 col-sm-offset-1 ">
                  <p class="text-primary"><b>Transporter Earning</b></p>
               </div>
               <div class="col-sm-10 col-sm-offset-1 ">
                  <div class="form-group">
                     <label>Requester Paid</label>
                     <div class="pull-right">${{ number_format((float)$user-> ShippingCost,2) }}</div>
                  </div>
               </div>
               <div class="col-sm-10 col-sm-offset-1 ">
                  <div class="form-group">
                     <label>Aquantuo Fee</label>
                     <div class="pull-right">(-)${{ number_format((float)$user->AquantuoFees,2) }}</div>
                  </div>
               </div>
               <div class="col-sm-10 col-sm-offset-1 ">
                  <div class="form-group">
                     <label>Transporter Earning</label>
                     <div class="pull-right">${{ number_format((float)$result,2) }}</div>
                  </div>
               </div>
               <div class="col-sm-10 col-sm-offset-1 " align="center">
                  </hr class="border">
                  <div class="col-sm-6">
                     <button class="custom-btn1 btn-block"  title="Accept" data-dismiss="modal"  onclick="return confirm('Are you sure you want to accept this request?')? accept_request():'';">
                     Accept
                     </button>
                  </div>
                  <div class="col-sm-6">
                     <button class="custom-btn1 btn-block" data-dismiss="modal">
                     Cancel
                     </button>
                  </div>
               </div>
            </div>
         </div>
         {!! Form::close() !!}
      </div>
   </div>
</div>
@endsection
<?php
function change_unit($weight, $currentunit, $changeto)
{

    //  echo $weight." ".$currentunit." ".$changeto; //die;

    //  echo "<br>";

    $newweight = '';
    $newchangeto = $changeto;
    switch ($currentunit) {
        case 'kg':
            $newweight = $weight * 1000;
            break;
        case 'lbs':
            $newweight = $weight * 0.00220462;
            break;
        case 'inches':
            $newweight = $weight;
            break;
        case 'cm':
            $newweight = $weight * 0.393701;
            break;
    }

    switch ($changeto) {
        case 'kg':
            $newweight = $newweight / 1000;
            break;
        case 'lbs':
            $newweight = $newweight * 0.00220462;
            break;
        case 'cm':
            $newweight = $newweight / 0.393701;
            break;
        case 'inches':
            $newweight = $newweight;
            break;
    }

    return floatval($newweight);

}

?>

