@extends('Page::layout.one-column-page')
@section('content')
{!! Html::script('theme/web/js/validation.js') !!}
{!! Html::script('theme/web/rating/jquery.rateyo.js') !!}
{!! Html::style('theme/web/rating/jquery.rateyo.min.css')!!}
<script>
   $(document).ready(function() {
     $(".fancybox").fancybox();
   });
</script>
<div class="container">
<div class="">
   <div class="col-sm-12">
      <h2 class="color-blue mainHeading">Send A Package Detail</h2>
      <br />
   </div>
   @foreach($data as $user)
   <div class="col-md-12">
      <div class="box-shadow">
         <div class="content-section clearfix">
            <br />
            <div class="col-md-2 col-sm-2 col-xs-12">
               @if($user->ProductImage != '')
               <a class="fancybox" rel="group"  href="{{ ImageUrl.$user->ProductImage }}" >
               <img src="{{ ImageUrl.$user->ProductImage}}" class="margin-top" width="150px" height="150px"  >
               </a>
               @else
               <img src="{{ImageUrl}}/no-image.jpg"  width="150px" height="150px" >
               @endif
            </div>
            <div class="col-md-10 col-sm-10 col-xs-12">
               <span class="pull-right">
                  <a href="{{url('my-request')}}"  class="btn btn-default blue-btn">
                     <i class="fa fa-arrow-left"> </i> Back</a>
               </span>

               <h4><b>{{ucfirst($user->ProductTitle)}}</b></h4>
               <p class="color-black"><strong>Package No. :</strong>{{$user->PackageNumber}}</p>
               <p class="color-black"><strong>Status:</strong>
                  @if($user->Status == 'out_for_pickup')
                  Out for Pickup
                  @elseif($user->Status == 'out_for_delivery')
                  Out for Delivery
                  @else
                  {{ucfirst($user->Status)}}
                  @endif
               </p>
               <div class="form-group">
                  @if($user->Status == 'pending')
                  @if($user->RequestType == 'buy_for_me')
                  <a  class="btn btn-primary blue-btn"  title="Edit" href="{{url('edit-online-purchese',Request::segment(2))}}"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                  &nbsp; Edit &nbsp;</a>&nbsp;
                  @else
                  <a  class="btn btn-primary blue-btn"  title="Edit" href="{{url('edit-request',Request::segment(2))}}"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                  &nbsp; Edit &nbsp;</a>&nbsp;
                  @endif
                  @endif
                  @if(in_array($user->Status,['ready','pending']))
                  <a class="btn btn-danger" href="{{url('delivery-details/delete-request/'.$user->id)}}" title="Delete" onclick="return confirm('Are you sure, You want to delete this delivery request.')" > Delete</a>
                  @endif
                  <?php $btn_status = 'display:none';
if ($user->Status == "delivered" && $user->RequesterFeedbcak == '') {$btn_status = 'display:true';}?>
                  @if($user->RequesterFeedbcak == '')
                  <a href="javascript:void(0)" data-toggle="modal" data-target="#modal2" title="Transfer Fund"  class="btn btn-default blue-btn" id= "feedback" style="{{$btn_status}}">Give Feedback</a>
                  @endif
                  @if(in_array($user->Status, ['out_for_pickup', 'out_for_delivery','accepted'] ) )

                    <a class="btn btn-primary" href="javascript:void(0)" id="chat-loader-{{$user->_id}}" onclick="open_chat_box('{{$user->TransporterId}}','{{$user->TransporterName}}','chat-loader-{{$user->_id}}')"><i class="fa fa-chat"></i> Chat</a>

                    
                  @endif
               </div>
            </div>
         </div>
      </div>
   </div>
   @if(!$user->TransporterId == '')
   <div class="col-sm-4">
      <style>
         .pre-modal-dialog {
         margin: 30px auto;
         width: 440px;
         }
      </style>
   </div>
</div>
@endif
<div class="clearfix"> </div>
<div class="col-sm-4 col-xs-12">
   @if(count($transporter_data) > 0)
   <div class="clearfix detail-tab">
      <div class="tab-content box-shadow margin-top">
         <div class="content-section clearfix">
            <div class="col-sm-12 col-xs-12  text-center">
               <h4><b>Transporter Details </b></h4>
               <br>
            </div>
            <div class="col-sm-12 col-xs-12  text-center">
               @if(@$transporter_data->Image != '')
               <a class="fancybox" rel="group" href="{{ ImageUrl.$transporter_data->Image}}" >
               <img src="{{ ImageUrl.$transporter_data->Image}}"  width="100px" height="100px" class="img-circle">
               </a>
               @else
               <img src="{{ImageUrl}}/user-no-image.jpg" width="100px" height="100px" class="img-circle">
               @endif

            </div>
            <div class="col-sm-12 col-xs-12  text-center">
               {{$transporter_data->TripId}}
               <p><b>{{$transporter_data->Name}}</b></p>
               @if(count($transporter_data) > 0)
               <?php $average_rating = 0;
if ($transporter_data->RatingCount > 0 && $transporter_data->RatingByCount > 0) {
    $rating = $transporter_data->RatingCount / $transporter_data->RatingByCount;
    $average_rating = 20 * $rating;
}
?>
               <div class="col-sm-12 col-xs-12 text-center">
                  <div class="star-ratings-sprite">
                     <span style="width:<?php echo $average_rating ?>%" class="star-ratings-sprite-rating"></span>
                  </div>
                  <br />
               </div>
               @endif
            </div>
         </div>
      </div>
   </div>
   @endif
   @if(($user->TransporterFeedbcak != '') ||  ($user->RequesterFeedbcak != '') )
   <div class="clearfix detail-tab">
      <div class="tab-content box-shadow margin-top">
         <div class="">
            <br />
            <div class="col-sm-12 col-xs-12  text-left">
               @if(!empty($user->TransporterFeedbcak))
               <div class="col-sm-12 col-xs-12  text-left">
                  <h4><b>Transporter Feedback</b></h4>
                  <p>{{ucfirst($user->TransporterFeedbcak)}}</p>
                  <?php $transporter_rating = 0;
if (count($user->TransporterRating) > 0) {
    $transporter_rating = 20 * $user->TransporterRating;
}
?>
                  <div class="star-ratings-sprite pull-left">
                     <span style="width:<?php echo $transporter_rating; ?>%" class="star-ratings-sprite-rating"></span>
                  </div>
                  <br /><br />
               </div>
               <hr />
               @endif
            @if(!empty($user->RequesterFeedbcak))
            <div class="col-sm-12 col-xs-12  text-left">
               <h4><b>Your Feedback</b></h4>
               <p>{{ucfirst($user->RequesterFeedbcak)}}</p>
               <?php $requester_rating = 0;
if (count($user->RequesterRating) > 0) {
    $requester_rating = 20 * $user->RequesterRating;

}

?>
               <div class="star-ratings-sprite pull-left">
                  <span style="width:<?php echo $requester_rating; ?>%" class="star-ratings-sprite-rating"></span> <
               </div>
            </div>
             <div class="clearfix"></div> <br><br>
            @endif

         </div>
      </div>
   </div>
</div>
   @endif
</div>
@if(!$user->TransporterId == '')
<div class="col-sm-8 col-xs-12">
@else
<div class="col-sm-12 col-xs-12">
   @endif
   <div class="clearfix margin-top detail-tab">
      <ul class="nav nav-tabs" role="tablist">
         <li role="presentation" class="active" ><a href="#profile" aria-controls="profile" class="active" role="tab" data-toggle="tab">Delivery Information</a></li>
         <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Package Information</a></li>

         @if($user->Status == "cancel")
           <li role="presentation"><a href="#cancel_detail" aria-controls="cancel_detail" role="tab" data-toggle="tab">Cancel Detail</a></li>
         @endif

          @if($user->Status == "delivered")
           <li role="presentation"><a href="#delivered" aria-controls="delivered" role="tab" data-toggle="tab">View Receipt</a></li>
         @endif


      </ul>
      <div class="tab-content box-shadow">
         <div role="tabpanel" class="tab-pane active" id="profile">
            </br>
            <p> </p>
            <p><b>Pickup Location: </b> {{ucfirst($user->PickupFullAddress)}}</p>
            <p><b>Pickup Date: </b> {{ show_date(@$user->PickupDate) }}</p>
            <p><b>This is a Public Place:</b> {{ucfirst($user->PublicPlace)}} </p>
            @if($user->PublicPlace == 'yes')
            The Requester has opted to meet in public
            @endif
            <p><b>Drop off Location:</b> {{ucfirst($user->DeliveryFullAddress)}}</p>
            <p><b>Drop off Date: </b> {{ show_date($user->DeliveryDate) }} </p>
            <p><b>Delivery Date Flexible: </b> {{ucfirst(@$user->FlexibleDeliveryDate)}}</p>
            @if($user->JournyType == 'one_way')
            <p><b>Delivery Type:</b> One way</p>
            @endif
            @if($user->JournyType != 'one_way')
            <p><b>Return Address </b>{{ucfirst($user->ReturnFullAddress)}}    </p>
            @endif


            <p><b>Shipping Mode</b><small>(Not all items can be shipped by Air. Items shipped by sea typically take longer to arrive)</small><b>:</b> {{ucfirst($user->TravelMode)}}</p>

            @if($user->DeliveryVerifyCode != '')
            <p><b>Verification Code: </b>{{$user->DeliveryVerifyCode}}</b>
               @endif
            <p><b>Return address (if item is not delivered): </b> {{ucfirst($user->NotDelReturnFullAddress)}}</p>
         </div>
         <div role="tabpanel" class="tab-pane" id="messages">
            <br />
            <p><b>Package Value:</b> ${{ number_format((float)$user->ProductCost,2) }}</p>
            <p><b>Weight:</b>
               <?php echo number_format(change_unit($user->ProductWeight, $user->ProductWeightUnit, 'kg'), 2) . ' Kg'; ?> / <?php echo number_format(change_unit($user->ProductWeight, $user->ProductWeightUnit, 'lbs'), 2) . ' Lbs'; ?>
            </p>
            <p><b>Package Dimensions:</b>
               <?php echo "L-";
echo number_format(change_unit
    ($user->ProductLength, $user->ProductLengthUnit, 'inch'), 2) . 'Inch'; ?>,
               <?php echo "H-";
echo number_format(change_unit
    ($user->ProductHeight, $user->ProductHeightUnit, 'inch'), 2) . 'Inch'; ?>,
               <?php echo "W-";
echo number_format(change_unit
    ($user->ProductWidth, $user->ProductWidthUnit, 'inch'), 2) . 'Inch'; ?>/
               <?php echo "L-";
echo number_format(change_unit
    ($user->ProductLength, $user->ProductLengthUnit, 'cm'), 2) . 'Cm'; ?>,
               <?php echo "H-";
echo number_format(change_unit
    ($user->ProductHeight, $user->ProductHeightUnit, 'cm'), 2) . 'Cm'; ?>,
               <?php echo "W-";
echo number_format(change_unit
    ($user->ProductWidth, $user->ProductWidthUnit, 'cm'), 2) . 'Cm'; ?>
            </p>
            @if($user->BoxQuantity == '0')
            <p><b>Quantity:</b> 1 Box(es)</p>
            @else
            <p><b>Quantity:</b> {{$user->BoxQuantity }} Box(es)</p>
            @endif
            <p><b>Description:</b> {{ucfirst($user->Description) }}</p>
            <p><b>Package Category: </b> {{ucfirst($user->Category)}}</p>
            <p><b>Receiver's Phone No. : </b>
               @if(!$user->ReceiverCountrycode == '')
                {{$user->ReceiverCountrycode}}-
               @endif
               {{$user->ReceiverMobileNo}}
            </p>
            <p><b>Need Package Material: </b>
            <?php echo ($user->PackageMaterial == 'Yes') ? 'Yes' : 'N/A'; ?>
            </p>
            @if($user->PackageMaterial == 'yes')
            <p>Package Material Shipped  </p>
            @endif
            <p><b>Insurance:</b> {{ucfirst($user->InsuranceStatus) }} </p>
            <p><b>Created Date:</b> {{ show_date(@$user->EnterOn) }} </p>
            <div class="list-footer">
               <div class="row">
                  <div class="col-sm-4">
                     <p><b>Shipping Cost: </b>
                        <span class="pull-right"> ${{ number_format((float)$user-> ShippingCost,2) }}</span>
                     </p>

                      <b>Aquantuo Discount : </b>
                        <span class="pull-right"> ${{ number_format((float)$user-> discount,2) }}</span>
                      <div>@if(!$user->PromoCode == '')
                            <small>(<b>Promocode:</b>{{ $user->PromoCode }}) </small>
                             @endif</div><br>



                     @if($user->InsuranceCost > 0)
                     <p><b>Insurance : </b>(+)
                        <span class="pull-right"> ${{number_format($user->InsuranceCost,2)}} </span>
                     </p>
                     @endif
                     @if($user->Discount > 0)
                     <p><b>Discount: </b>(-)
                        <span class="pull-right"> ${{number_format($user->Discount,2)}} </span>
                     </p>
                     @endif
                     <?php $result = ($user->TotalCost) - ($user->AquantuoFees);?>
                     <p><b>Total Cost:</b>
                        <span class="pull-right"> ${{number_format((float)$user->TotalCost,2,'.','')}} </span>
                     </p>
                  </div>
               </div>
            </div>
         </div>
         <div role="tabpanel" class="tab-pane" id="cancel_detail">
              <br /><br />
              <div class="col-sm-12 text-right">
              <button class="btn btn-primary" id="print" onclick="print()"><i class="fa fa-print" aria-hidden="true"></i>
                Print</button>

                </div>
              <div class="details-list">
                {!! Form::label('return_address', 'Return Address:',['class' => 'control-label']) !!} {{$user->NotDelReturnFullAddress}}
              </div>
              <div class="details-list">
                {!! Form::label('reject_by', 'Reject By:',['class' => 'control-label']) !!}
                  {{ucfirst($user->RejectBy)}}
              </div>
              <div class="details-list">
                {!! Form::label('reject_by', 'Return Type:',['class' => 'control-label']) !!}
                  {{get_cancel_status($user->ReturnType)}}
              </div>

              <div class="details-list">
                <p>
                  <b>Message: </b> {{ucfirst($user->TransporterMessage)}}
                </p>
              </div>
              @if(!empty(trim($user->TrackingNumber)))
              <div class="details-list">
                <p>
                  <b>Tracking Number: </b> {{$user->TrackingNumber}}
                </p>
              </div>
              @endif

              @if(isset($user->CancelReturnDate->sec))
              <div class="details-list">
                <p>
                  <b>Date: </b> {{ show_date($user->CancelReturnDate) }}
                </p>
              </div>
              @endif

              @if(!empty(trim($user->ReceiptImage)))
                @if(file_exists(BASEURL_FILE.$user->ReceiptImage))
                  <div class="details-list">
                    <p>
                      <b>Receipt: </b>  <br />
                      <a class="fancybox" rel="group" href="{{ImageUrl.$user->ReceiptImage}}" >
                        <img src="{{ImageUrl.$user->ReceiptImage}}" alt="receipt" height="100" width="100" >
                      </a>
                    </p>
                  </div>
                @endif
              @endif
              <br /><br/>
            </div>

             <div role="tabpanel" class="tab-pane" id="delivered">
                <div class="details-list">
                 <h3 class="text-primary">{{ucfirst($user->RequesterName)}}</h3>
                 <span class="pull-right"><b>Package Number:</b> {{$user->PackageNumber}}</span>
                 </div>

                <div class="details-list" style="margin: 0px">
                <p style="margin: 0px"> <strong>Address: </strong>{{ucfirst($user->PickupAddress)}}
                  @if($user->PickupAddress2 != '')
                ,{{ucfirst($user->PickupAddress2)}}
                 @endif
                </p>
                     <span class="pull-right"><b>Delivered By:</b> {{ucfirst($user->TransporterName)}}</span>
                </div>




                <div class="details-list" style="margin: 0px">
                <p style="margin: 0px">
                     {{ucfirst($user->PickupCity)}}
                     {{ucfirst($user->PickupState)}}
                     {{ucfirst($user->PickupCountry)}}
                     {{$user->PickupPinCode}}
                </p>
                </div>

                <div class="details-list" style="margin: 0px">
                  <p style="margin: 0px"> <strong>Created Date: </strong>{{ show_date(@$user->EnterOn) }}
                  <span class="pull-right"><b>Delivered on:</b> {{ show_date($user->DeliveryDate) }}</span>
                  </p>

                </div>
              <br>
              <table id="example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                <tr>
                <th>S.No.</th>
                <th>Name</th>
                <th style="text-align:right;">Qty</th>
                <th style="text-align:right;">Shipping Price</th>
                </tr>
                <tr>
                <td>1</td>
                <td>{{ucfirst($user->ProductTitle)}}</td>
                <td align="right">{{$user->BoxQuantity}} Box(es)</td>
                <td align="right" > ${{number_format($user->ShippingCost,2)}}</td>
                </tr>
                <tr>
                  <th colspan="3" class="text-right">Insurance Cost</th>
                  <td align="right">${{(number_format($user->InsuranceCost,2))}}</td>
                </tr>
                <tr>
                  <th colspan="3" class="text-right">Total Cost</th>
                  <td align="right"><code>${{(number_format($user->TotalCost,2))}}</code></td>
                </tr>
              </table>
              <!-- <div class="details-list">
                {!! Form::label('date', 'Delivery Request Date:',['class' => 'control-label']) !!}
                  {{ show_date($user->EnterOn) }}
              </div>

              <div class="details-list">
                <p>
                  <b>Delivered To: </b> {{ucfirst($user->RequesterName)}}
                </p>
              </div>

              <div class="details-list">
                <p>
                  <b>Delivered By: </b> {{ucfirst($user->TransporterName)}}
                </p>
              </div>

              <div class="details-list">
                <p>
                  @if($user->DeliveredTime != '')
                  <b>Delivered on: </b> {{ show_date($user->DeliveredTime) }}
                  @else
                  <b>Delivered on: </b> N/A
                  @endif
                </p>
              </div>   -->




              <br /><br />

            </div>

      </div>
   </div>
</div>
<div class="col-sm-12 col-xs-12">
   <div class=" box-shadow">
      <h2>Other Images</h2>
      @foreach($user->OtherImage  as $key )
      @if(!empty(trim($key)))
      @if(file_exists(BASEURL_FILE.$key))
      <div class="col-md-2 col-sm-2 col-xs-6">
         <a class="fancybox" rel="group" href="{{ ImageUrl.$key }}" >
         <img src="{{ ImageUrl.$key}}" width="100px" height="100px" class="image-style">
         </a>
         <p> </p>
      </div>
      @endif
      @endif
      @endforeach
      @if(count($user->OtherImage) <= 0)
      <center>
         <h4>No other image uploaded</h4>
      </center>
      @endif
      <div class="clearfix"></div>
      <br /><br />
   </div>
</div>
<div class="clearfix"></div>
<br />
<br />
<!--review-model-->
<div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
   <div class="pre-modal-dialog modal-md">
      <div class="modal-content">
         <div class="modal-header popup-bg text-center">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <p class="popup-head color-white">Rate your experience with transporter</p>
         </div>
         <div class="clearfix"></div>
         <center>
            <div class="modal-body text-left">
               <div id="success-message"> </div>
               {!! Form::model('', ['name' => 'RequestReview', 'id' => 'RequestReview']) !!}
               <div class="col-md-12">
                  <div class="col-md-12" align="center">
                     <div>
                        @if(count($transporter_data) > 0)
                        @if($transporter_data->Image != '')
                        <a class="fancybox" rel="group" href="{{ ImageUrl.$transporter_data->Image}}" >
                        <img src="{{ ImageUrl.$transporter_data->Image}}"  width="150px" height="150px" class="img-circle">
                        </a>
                        @else
                        @endif
                        @else
                        <img src="{{ImageUrl}}/user-no-image.jpg" width="150px" height="150px" class="img-circle">
                        @endif
                     </div>
                     </br>
                     <div>
                        <label class="rating_margin" >Rate Now :</label>
                        &nbsp;&nbsp;&nbsp;
                        <div class="rateyo" id="rateyo" ></div>
                        <div class="clearfix"></div>
                        <div id="er_rating"  class="error-msg"></div>
                        <input type="hidden" name="request_id" id="request_id" value='<?php echo $user->_id ?>'>
                        <input type="hidden" name="transporter_id" id="transporter_id" value='<?php echo $user->TransporterId ?>'>
                        <br />
                        <script type="text/javascript">
                           $(document).ready(function() {
                           $(".fancybox").fancybox();
                           });
                           var $rateYo = $("#rateyo").rateYo();
                        </script>
                     </div>
                     <div class="col-md-12">
                        <div class="">
                           <div class="form-group txt-area feedback-text">
                              {!! Form::textarea('review','',['class'=>'form-control required', 'placeholder'=> 'Feedback','id' =>'review','maxlength' =>250,'size' =>'30x4']) !!}
                           </div>
                           <div id="er_review" class="help-block white"></div>
                           <div class="clearfix"></div>
                           <span id="review_msg"></span>
                        </div>
                     </div>
                     <div class="col-md-2"></div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="col-xs-12">
                     <div class="">
                        <div class="col-xs-6">
                           <div class="form-group">
                              <button type="submit" class="custom-btn1 btn-block" id='ratingButton'>SUBMIT</button>
                              <div class="error-msg"></div>
                           </div>
                        </div>
                        <div class="col-xs-6">
                           <div class="form-group text-center">
                              <button class="custom-btn1 btn-block" data-dismiss='modal' >NOT NOW</button>
                           </div>
                        </div>
                     </div>
                  </div>
                  {!! Form::close() !!}
               </div>
         </center>
         <div class="clearfix"></div></center>
         </div>
      </div>
      <!--end-->
      @endforeach
   </div>
</div>
<style>
   .pre-modal-dialog {
   margin: 30px auto;
   width: 440px;
   }
   .transporter-height{
   height:300px;
   }
</style>
<?php
function change_unit($weight, $currentunit, $changeto)
{

    $newweight = '';
    $newchangeto = $changeto;

    // Convert to Gram
    switch ($currentunit) {
        case 'kg':
            $newweight = $weight * 1000;
            break;
        case 'lbs':
            $newweight = $weight * 453.592;
            break;
        case 'inches':
            $newweight = $weight;
            break;
        case 'cm':
            $newweight = $weight * 0.393701;
            break;
    }
    // end convert to gram

    switch ($changeto) {
        case 'kg':
            $newweight = $newweight / 1000;
            break;
        case 'lbs':
            $newweight = $newweight / 453.592;
            break;
        case 'cm':
            $newweight = $newweight / 0.393701;
            break;
        case 'inches':
            $newweight = $newweight;
            break;
    }
    return floatval($newweight);

}

?>
<script type="text/javascript">
new Validate({
  FormName : 'RequestReview',
  ErrorLevel : 1,
  validateHidden: false,
  callback : function(){
        if($rateYo.rateYo("rating") <= 0) {
           $('#er_rating').html('The rating field is required.');
           return false;
            }
           $("#ratingButton").addClass('spinning');
           $.ajax({

           url     : '{{url("/requester-review")}}',
           type    : 'post',
           data    : "review="+$("#review").val()+
                     "&request_id="+$('#request_id').val()+
                     "&transporter_id="+$('#transporter_id').val()+
                     "&rating="+$rateYo.rateYo("rating"),
           success : function(res){
               $("#ratingButton").removeClass('spinning');
                   var obj = eval("("+res+")");
           if(obj.success == 1)
           {
             $("#feedback").hide();
             location.reload();
             $("#RequestReview").trigger( "reset" );
             $('#success-message').html('<div role="alert" class="alert alert-success alert-dismissible fade in">'
         +'<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>'+obj.msg+'</div>');

           }
           else
           {
             $("#modal2").trigger('click' );
             $("#feedback").hide();
             $('#success-message').html('<div role="alert" class="alert alert-danger alert-dismissible fade in">'
         +'<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>'+obj.msg+'</div>');
           }

                  }
               });
      }

    });

    $(document).ready(function() {
      $('#example').DataTable();
    } );

function print() {
  document.getElementById("print").innerHTML ="Page location is: " + window.location;
}

</script>
@endsection

