@extends('Page::layout.one-column-page')
@section('page_title')
My Shipping Request Detail - Aquantuo
@endsection
@section('content')

{!! Html::script('theme/web/rating/jquery.rateyo.js') !!}
{!!Html::style('theme/web/rating/jquery.rateyo.min.css')!!}
<?php
$total_item = 0;
$total_weight = 0;
foreach ($request_data->ProductList as $key) {
	$total_item = $total_item + 1;
  $total_weight = $total_weight + $key['weight'];
	$shipping_cost_by_user = $total_item_price = 0;
}

?>

<div class="container">
    <h2 class="color-blue mainHeading">Buy For Me Details <span class="pull-right">
    <a href="{{url('my-request')}}" class="btn btn-primary" style="color:#FFFFFF;">Back</a></span></h2>
    <br>
    <div class="clearfix"></div>
    <div class="white_block box-shadow p-space">
        <div class="col">
            <p><b>Package Id :&nbsp;</b>{{$request_data->PackageNumber}}</p>
            <p><b>Created On:&nbsp;</b> {{date('M d,Y',@$request_data->EnterOn->sec)}}</p>
            @if($request_data->ReceiverName != '')
            <p><b>Total Weight: </b>  {{ucfirst($total_weight)}} lbs</p>
            @endif
        </div>
        <div class="col">
            <p><b>Total Number of Item(s): </b>{{@$total_item}}</p>
            <p><b>Delivery Address: </b>  {{$request_data->DeliveryFullAddress}} </p>
        </div>
        <div class="col">
            <p><b>Receiver Phone: </b> {{$request_data->ReceiverCountrycode}} - {{$request_data->ReceiverMobileNo}}</p>
            @if($request_data->ReceiverName != '')
            <p><b>Receiver Name: </b>  {{ucfirst($request_data->ReceiverName)}} </p>
            @endif
            @if($request_data->ReceiverName == '')
            <p><b>Total Weight: </b>  {{ucfirst($total_weight)}} lbs</p>
            @endif
            <!-- <a href="#"  class="anchor-blue">View More</a> -->
        </div>
    </div>
<!-- Box for multiple items -->
@foreach($request_data->ProductList as $key => $value)
<?php $total_item_price += $value['price'] * $value['qty'];?>
    <div class="box-shadow">
      <div class="box-header">
          {{ucfirst($value['product_name'])}}
      </div>

      <div class="row p-space p-10">
          <div class="col-sm-4 col-xs-12">
              <p><b>Item Id :</b> {{ucfirst(@$value['package_id'])}}</p>
              <p><b>Item Url :</b>
              <?php

if (preg_match("/http/", $value['url']) == false && preg_match("/https/", $value['url']) == false) {
	$value['url'] = 'http://' . $value['url'];
}

?>
        <a target="_blank" href='<?php echo $value['url']; ?>' >
                  {{$value['url']}}</a></p>

              <p><b>Shipping Mode:</b> {{ucfirst($value['travelMode'])}}</p>
              <p><b>Expected Delivery Date:</b>
              @if(isset($value['ExpectedDate']->sec))
                 {{ show_date(@$value['ExpectedDate']) }}
              @else
                  N/A
              @endif
              </p>
              <p><b>Description:</b> {{ucfirst($value['description'])}}</p>
          </div>
          <div class="col-sm-4 col-xs-12">
              <p><b>Item Status: </b>
              <?php if ($value['status'] == 'cancel' && @$value['RejectBy'] != '') {echo "Not Delivered";} else {echo get_status_title($value['status'], 'buy_for_me')['status'];}?> </p>
              <p><b>Item Value: </b>${{number_format($value['price'],2)}}</p>
              <p><b>Item Category: </b>{{ucfirst($value['category'])}}</p>
          </div>


          <div class="col-sm-4 col-xs-12">
         @if($value['tpid'] != '')

              @foreach($user_data as $key)
              @if($key->_id == $value['tpid'])

                <?php $average_rating = 0;
if ($key->RatingCount > 0 && $key->RatingByCount > 0) {
	$rating = $key->RatingCount / $key->RatingByCount;
	$average_rating = 20 * $rating;
}

?>
                    <div class="media">
                      <div class="form-group media-left">
                          <a class="fancybox" rel="group" href="{{ ImageUrl.$key->Image}}" >
                          <img src="{{ ImageUrl.$key->Image}}" class="img-circle" width="100px" height="100px">
                          </a>

                      </div>
                      <div class="media-body">
                        <br>
                        <p><b>Transporter Name: </b>{{ucfirst($value['tpName'])}}</p>
                        <div class="col-sm-12">
                          <div class="star-ratings-sprite pull-left">
                            <span class="star-ratings-sprite-rating" style="width:{{$average_rating}}%"></span>
                          </div>
                        </div>
                      </div>
                    </div>
              @endif
             @endforeach
          @endif
        </div>

    @if($value['status'] == 'delivered')
        <div class="col-sm-12 col-xs-12">
        <p><b>Your Feedback :</b> @if($value['RequesterFeedbcak'] != '') {{ucfirst(@$value['RequesterFeedbcak'])}}@else - @endif</p>
        <p><b><span class="pull-left">Rating : &nbsp; </span> </b>
          <div class="star-ratings-sprite pull-left">
             <span style="width:<?php echo $value['RequesterRating'] * 20; ?>%" class="star-ratings-sprite-rating"></span>
          </div>
          </br>
        </p> </br>
        <p><b>Transporter Feedback :</b> @if($value['TransporterFeedbcak'] != '') {{ucfirst(@$value['TransporterFeedbcak'])}}@else - @endif</p>

        <p><b><span class="pull-left">Rating : &nbsp; </span> </b>
          <div class="star-ratings-sprite pull-left">
             <span style="width:<?php echo $value['TransporterRating'] * 20; ?>%" class="star-ratings-sprite-rating"></span>
          </div>
          </br>
        </p>

        </div>
    @endif
    <div class="clearfix"></div>
    <br>


    <div class="col-sm-12 col-xs-12">
      <div class="table-responsive">
        <table class="custom-table table table-bordered">
          <thead>
             <tr>
                <th>Image</th>
                <th>Dimentions</th>
                <th>Verification Code</th>
                <th>Insurance</th>
                <th>Quantity</th>
<!--
                <th>Shipping Cost</th>
-->
                <!-- <th>Need Package Material</th> -->
                <th>Action</th>
             </tr>
          </thead>
          <tbody>
             <tr>
                <td>
                <!-- <img src="http://apis.aquantuo.com/upload/profile/283441493472599.jpeg" class="" width="60px" height="60px"> -->
                  @if($value['image'] != '')
                  <a class="fancybox" rel="group" href="{{ ImageUrl.$value['image']}}" >
                  <img src="{{ ImageUrl.$value['image']}}" class="" width="60px" height="60px" >
                  </a>
                  @else
                  <img src="{{ ImageUrl}}no-image.jpg" width="60px" height="60px"  class="img-rounded">
                  @endif
                </td>
                <td>
                <p>L-@if($value['length'] != ''){{$value['length']}} {{$value['lengthUnit']}}@else N/A @endif</p>
                <p>H-@if($value['height'] != ''){{$value['height']}} {{$value['heightUnit']}}@else N/A @endif</p>
                <p>W-@if($value['width'] != ''){{$value['width']}} {{$value['widthUnit']}}@else N/A @endif</p>
                <p>Weight-@if($value['weight'] != ''){{$value['weight']}} {{$value['weight_unit']}}@else N/A @endif</p>

                </td>
                <td>{{$value['verify_code']}}</td>
                <td>
                  <?php
if ($value['insurance_status'] == 'yes' && $value['insurance'] > 0) {
	echo '$' . number_format($value['insurance'], 2);
} else {
	echo ucfirst($value['insurance_status']);
}
?>
                </td>
                <td>{{$value['qty']}}</td>
<!--
                <td>
                ${{number_format($value['shippingCost'],2)}}
                </td>
-->

               <td>
                <!-- @if($value['status'] == 'not_purchased')
                    Payable <?php //echo '$' . number_format($value['after_update'] - $value['total_cost'], 2) ?>
                    <br><br>
                    <a  class="btn btn-primary" class="btn btn-primary blue-btn" href="{{url('send_package-item-pay')}}/{{Request::segment(2)}}/{{$value['_id']}}">Pay Now</a>
                @endif -->
                @if($value['status'] == 'delivered')
                  @if($value['RequesterFeedbcak'] == '')
                  <a href="javascript:void(0)" data-toggle="modal" data-target="#modal2" title="Transfer Fund"  class="btn btn-default blue-btn" id= "feedback" onclick="return give_feedback('{{$value['_id']}}','{{$value['tpid']}}')">Give Feedback</a>
                  @endif
                @endif

               <a  class="btn btn-primary" class="btn btn-primary blue-btn" href="{{url('track-order-detail')}}?package_readio=item_id&package_id={{$value['package_id']}}">Track</a>

                @if($value['tpid'] != '')
                <a class="btn btn-primary" href="javascript:void(0)" id="chat-loader-{{$value['tpid']}}" onclick="open_chat_box('{{$value['tpid']}}','{{$value['tpName']}}')" style="margin-left:15px"><i class="fa fa-chat"></i> Chat</a>
                @endif


               </td>
             </tr>
          </tbody>
        </table>
      </div>
    </div>

  </div>

</div>
<!-- end Box for multiple items -->
@endforeach
    <div class="row">
        <div class="col-sm-6 col-xs-12">
          <div class="">
         <style>
          .card_info{backgroung:#fff; display:flex; display:-webkit-flex; align-items:center; padding:10px; border:1px solid #eee;}
          .card_box{float:left; width:80px; height:50px; display:flex; display:-webkit-flex; align-items:center; background:#eee; border:1px solid #eee; overflow:hidden;}
          .card_box img{max-width:100%;}
          .card_info_text{ padding-left:15px; display:flex; display:-webkit-flex; flex:1;}
          .justify-right{justify-content:end; padding-right:10px;}
         </style>

          @if(isset($request_data->mobilemoney))
          <h3>Payment Information</h3>
          @foreach($request_data->mobilemoney as $data)
         <div class="card_info">
          <div class="card_box">
          @if($data['mobile_type'] == 'airtel')
            <img  src="{{ImageUrl}}/webcard/airtel_money.png">
          @elseif($data['mobile_type'] == 'mtn')
            <img  src="{{ImageUrl}}/webcard/mtn.png">
          @elseif($data['mobile_type'] == 'tigo')
            <img src="{{ImageUrl}}/webcard/tigo.png">
          @elseif($data['mobile_type'] == 'vodafone')
            <img src="{{ImageUrl}}/webcard/vodafone.png">
          @endif
          </div>
          <div class="card_info_text">
            <h5>XXXX-XXXX-{{ $data['alternet_moblie'] }}</span></h5>
          </div>
          <div class="card_info_text">
            <h5><span>
             @if($data['mobile_money_status'] == 'awaiting_payment')
                Awaiting Payment
            @else
                Completed
            @endif

            </span></h5>
          </div>
          <div class="card_info_text justify-right">
            <b class="pull-right">${{ number_format($data['mobile_money_amount']/$data['CurrencyRate']  ,2) }}</b>
          </div>
         </div>
         @endforeach
         @endif
          @if(count($updated_data) > 0)
            <table class="table table-bordered">
                     <tbody bgcolor="#FFFFFF">
                        @foreach($updated_data as $key)
                        @if(@$key->update_difference > 0)
                        <tr>
                          <td class="">
                            <div class="col-xs-12 text-left">
                              <b>After reviewing your listing, '{{$key->product_name}}', the cost was increased by ${{@$key->update_difference}}</b>
                            </div>
                            </td>
                        </tr>
                        @endif
                        @endforeach
                      @if($request_data->need_to_pay > 0.1 && $request_data->Status != "pending")
                      <tr>
                        <td>
                          <div class="text-center">


                            <b style="color:green;"> Pay difference of ${{$request_data->need_to_pay}}</b><br><br>
                            <a  class="btn btn-primary" class="btn btn-primary blue-btn" href="{{url('send_package-item-pay2')}}/{{Request::segment(2)}}">Pay Now</a>
                          </div>
                        </td>
                      </tr>
                      @endif
                     </tbody>
            </table>
            @endif
          </div>

        </div>
        <div class="col-sm-6 col-xs-12">
            <div class="">
            <table class="table table-bordered">
                     <tbody bgcolor="#FFFFFF">

					@if (@$request_data->shippingCost)
                      <tr>
                        <td class="">
                          <div class="col-xs-8 text-left">
                            <label class="" for="exampleInputEmail1"><b>Int'l Shipping Cost:</b> </label>
                          </div>
                          <div class="col-xs-4 text-right">  ${{ number_format($request_data->shippingCost,2)}}</div>
                          </td>
                      </tr>
                    @endif

					
                      <tr>
                        <td class="">
                          <div class="col-xs-8 text-left">
                            <label class="" for="exampleInputEmail1"><b>Shipping Cost:</b> </label>
                          </div>
                          <div class="col-xs-4 text-right">  ${{ number_format($request_data->shipping_cost_by_user,2)}}</div>
                          </td>
                      </tr>


					@if (@$request_data->insurance)
                      <tr>
                        <td class="">
                          <div class="col-xs-8 text-left">
                            <label class="" for="exampleInputEmail1"><b>Insurance:</b> </label>
                          </div>
                          <div class="col-xs-4 text-right">  ${{ number_format($request_data->insurance,2)}}</div>
                          </td>
                      </tr>
                    @endif
                    
                    @if (@$request_data->DutyAndCustom)
						 <tr>
                        <td class="">
                          <div class="col-xs-8 text-left">
                            <label class="" for="exampleInputEmail1"><b>Duty/Customs Clearing:</b> </label>
                          </div>
                          <div class="col-xs-4 text-right">  ${{ number_format(@$request_data->DutyAndCustom,2)}}</div>
                          </td>
                      </tr>
                    @endif
                    
                    @if (@$request_data->Tax)
                        <tr>
                        <td class="">
                          <div class="col-xs-8 text-left">
                            <label class="" for="exampleInputEmail1"><b>Tax:</b> </label>
                          </div>
                          <div class="col-xs-4 text-right">  ${{ number_format(@$request_data->Tax,2)}}</div>
                          </td>
                      </tr>
                    @endif
                    
                    @if (@$request_data->total_item_price)
                      <tr>
                        <td class="">
                          <div class="col-xs-8 text-left">
                            <label class="" for="exampleInputEmail1"><b>Total Item(s) Price:</b> </label>
                          </div>
                          <div class="col-xs-4 text-right">  ${{number_format($total_item_price,2)}}</div>
                          </td>
                      </tr>
					@endif


					@if (@$request_data->AreaCharges)
                      <tr>
                        <td class="">
                          <div class="col-xs-8 text-left">
                            <label class="" for="exampleInputEmail1"><b>Region Cost:</b> </label>
                          </div>
                          <div class="col-xs-4 text-right">  ${{ number_format($request_data->AreaCharges,2)}}</div>
                          </td>
                      </tr>
					@endif
					
					@if (@$request_data->ProcessingFees)
                      <tr>
                        <td class="">
                          <div class="col-xs-8 text-left">
                            <label class="" for="exampleInputEmail1"><b>Processing Fees:</b> </label>
                          </div>
                          <div class="col-xs-4 text-right">
                              ${{ number_format($request_data->ProcessingFees,2)}}
                          </div>
                          </td>
                      </tr>
                    @endif

					@if (@$request_data->after_update_difference)
                      <tr>
                        <td class="">
                          <div class="col-xs-8 text-left">
                            <label class="" for="exampleInputEmail1"><b>After Item Review:</b> </label>
                          </div>
                          <div class="col-xs-4 text-right">
                              ${{ number_format($request_data->after_update_difference,2)}}
                          </div>
                          </td>
                      </tr>
                    @endif

                      @if($request_data->PromoCode != '')
                      <tr>
                        <td class="">
                          <div class="col-xs-8 text-left">
                            <label class="" for="exampleInputEmail1"><b>Aquantuo Discount: (-)</b>
                            <br/>(Promocode:{{$request_data->PromoCode  }})  </label>
                          </div>
                          <div class="col-xs-4 text-right">
                            ${{ number_format($request_data->discount,2)}}
                          </div>
                        </td>
                      </tr>
                      @endif
                      <tr style="background:#eee;">
                        <td>
                          <div class="col-xs-8 text-left">
                            <label class="" for="exampleInputEmail1"><b>Total Cost</b> </label>
                          </div>
                          <div class="col-xs-4 text-right">
                          <?php $total_cost = $request_data->TotalCost + $request_data->after_update_difference;?>
                            ${{ number_format($total_cost,2)}}

                          </div>
                        </td>
                      </tr>
                     </tbody>
                  </table>
            </div>
			<div class="text-center">
				@if($request_data->Status == 'pending')
					<a class="btn btn-primary" href="{{ url('buyforme-payment', $request_data->_id) }}" title="Pay Now"> Pay Now </a>
					<a class="btn btn-primary" href="{{ url('edit-buy-for-me', $request_data->_id) }}" title="Edit"> Edit </a>
				@endif
			</div>
            <br>
            <br>
        </div>
    </div>
</div>

<!--review-model-->
<div id="modal2" class="modal fade in" role="dialog">
  <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
           <button type="button" class="close" data-dismiss="modal" id="edit_item_close">&times;</button>
           <h4 class="modal-title">Rate your experience with transporter</h4>
        </div>

        <div class="modal-body">
          <div id="rating_success_container"></div>
          <div id="rating_danger_container"></div>
          {!! Form::model('', ['name' => 'RequestReview', 'id' => 'RequestReview']) !!}
            <div class="col-md-12" align="center" >
              <div class="col-md-12" align="center">
                  <img src="{{ImageUrl}}/user-no-image.jpg" width="150px" height="150px" class="img-circle">
              </div>

              <div class="col-md-12">
                <!-- <label class="rating_margin" >Rate Now :</label> -->
                     <br>
                     <div class="rateyo required" id="rateyo" ></div>
                     <div class="clearfix"></div>
                     <div id="er_rating"  class="error-msg"></div>
                     <br>
                 <input type="hidden" name="tpid" id="tpid" value="{{$value['tpid']}}">
                 <input type="hidden" name="item_id" id="item_id" value="{{$value['_id']}}">
              </div>

              <div class="col-md-12">

                     <div class="form-group txt-area feedback-text">
                        {!! Form::textarea('review','',['class'=>'form-control required', 'placeholder'=> 'Feedback','id' =>'review','maxlength' =>250,'size' =>'30x4']) !!}
                     </div>
                   <div id="er_review" class="help-block white"></div>
                   <div class="clearfix"></div>
                     <span id="review_msg"></span>
              </div>

              <div class="col-xs-6">
                 <div class="form-group">
                    <button type="submit" class="custom-btn1 btn-block" id='ratingButton'>SUBMIT</button>
                 <div class="error-msg"></div></div>
              </div>
              <div class="col-xs-6">
                 <div class="form-group text-center">
                    <button class="custom-btn1 btn-block" data-dismiss='modal' >NOT NOW</button>
                 </div>
              </div>


            </div>
            <div class="clearfix"></div>




          {!! Form::close() !!}
        </div>
      </div>
  </div>
</div>
<!--end-->
{!! Html::script('theme/web/js/validation.js') !!}
<script type="text/javascript">
var $rateYo = $("#rateyo").rateYo();
function give_feedback($id)
{
   $('#item_id').val($id);
}

$(document).ready(function() {
   $(".fancybox").fancybox();
});

new Validate({
  FormName : 'RequestReview',
  ErrorLevel : 1,
  validateHidden: false,
  callback : function()
  {
    if($rateYo.rateYo("rating") <= 0) {
      $('#er_rating').html('The rating field is required.');
      return false;
    }
    $("#ratingButton").addClass('spinning');
    $.ajax({
      url     : '{{url("/post-requester-review")}}',
      type    : 'post',
      data    : {
          "review" : $("#review").val(),
          "item_id" : $('#item_id').val(),
          "tpid"    : $('#tpid').val(),
          "rating" : $rateYo.rateYo("rating"),
        },
        success : function(res)
        {
            $("#ratingButton").removeClass('spinning');
            var obj = eval("("+res+")");
            if(obj.success == 1)
            {
              $("#feedback").hide();
              location.reload();
              $("#RequestReview").trigger( "reset" );
              $('#rating_success_container').html('<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>'+obj.msg+'</div>');
            }
            else
            {

              $('#rating_danger_container').html('<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>'+obj.msg+'</div>');
            }
        }
    });
  }

});













</script>

<style>
   .rating_margin
   {
   transition: transform 0.8s cubic-bezier(0, 0.63, 0.41, 0.98) 0s, opacity 0.8s cubic-bezier(0, 0.63, 0.41, 0.98) 0s;
   transform: translateY(0px);
   opacity: 1;
   }
   .to-center{text-align:middle;}
</style>

@endsection
