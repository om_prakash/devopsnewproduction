@extends('Page::layout.one-column-page')
@section('page_title')
My Address - Aquantuo
@endsection
@section('content')

{!! Html::style('theme/admin/choose-multiple/bootstrap-chosen.css') !!}
{!! Html::script('theme/admin/choose-multiple/chosen.jquery.js') !!}

	<div class="modal fade" id="address_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title color-white" id="myModalLabel">&nbsp;&nbsp;Edit Address</h4>
			</div><div class="clearfix"></div>

				{!! Form::model('', ['name' => 'edit_address', 'id' =>'edit_address', 'method' => 'post', 'onsubmit' => "return validateForm()"]) !!}
					<div class="modal-body">
						<div class="col-sm-12">
							<div class="form-group"> 
					{!! Form::label('Address Line1', 'Address Line1',['class'=>'color-black']) !!}           
	    			{!! Form::text('address_line_1','', ['class'=>'form-control required', 'maxlength' => 25, 'placeholder'=> 'Address','id' =>'address_line_1']) !!}
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
								{!! Form::label('Address Line2', 'Address Line2',['class'=>'color-black']) !!}           
								{!! Form::text('address_line_2','', ['class'=>'form-control ', 'maxlenght' => 25, 'placeholder'=> 'Address','id' =>"address_line_2"]) !!}	
							</div>	
                        </div>
						<div class="col-sm-6">
							<div class="form-group">  
								<label>Country</label>
                                <select name="country" class="form-control required" id="pp_pickup_country" onchange="get_state('pp_pickup_country','pp_pickup_state','pp_pickup_city','pp_pickup_state',selected_state,selected_city)">
                                <option value="" id="country">Select Country</option>

                                @foreach($country as $key)
                                	<option  value='{"id":"{{$key->_id}}","name":"{{$key->Content}}","state_available":"{{$key->state_available}}"}'   >{{$key->Content}}
                                	</option>
                                @endforeach
                                </select>
                            </div>
						</div>		
						<div class="col-sm-6">
							<div class="form-group">	  
								<label>State</label>
								<select name="state" class="form-control required left-disabled" id="pp_pickup_state" onchange="get_city('pp_pickup_state','pp_pickup_city','pp_pickup_city',selected_city)" >
                                <option selected="selected" value="">Select Country</option>
                                </select>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<label>City</label>
								<select  name="city" class="form-control required" id="pp_pickup_city">
                                <option   value="{{$key->PickupState}}">Select city</option>
                                </select>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">  
								{!! Form::label('zipcode', 'zipcode',['class'=>'color-black']) !!}           
								{!! Form::text('zipcode','', ['class'=>'form-control', 'placeholder'=> 'Zipcode','id' =>'zipcode']) !!}
							</div>
						</div>
					</div>
					<input type="hidden" value="" name="address_id" id="address_id">
					<div class="clearfix"></div>
						<div class="modal-footer">
						<div class="col-sm-6">	 
							{!! Form::button('Update', ['class' => 'custom-btn1 btn-block','id'=>'address_loader','type'=>'submit']) !!}
						</div> 
						 <div class="col-sm-6">
							{!! Form::button('Close', ['class' => 'custom-btn1 btn-block ', 'data-dismiss'=>'modal']) !!} 
						</div>
						</div>
							{!! Form::close() !!}
		</div>
	</div>
</div>
<div aria-labelledby="exampleModalLabel" role="dialog" tabindex="-1" id="exampleModal" class="modal fade in">
   <div role="document" class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header" id="Addaddress2">
            <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button>
            <h4 id="exampleModalLabel" class="modal-title">Add New Address</h4>
         </div>
         {!! Form::model('', ['name' => 'new_address', 'id' => 'Addaddress', 'method' => 'POST']) !!}
         <div class="panel-body">
            <div class="row">
               <div class="col-sm-12">
                  <div class="form-group">
                     <label>Address Line 1</label>
                     <input type="text" placeholder="Address" name="address_line_1" id="address_line_11"  maxlength= "125" class="form-control required">
                  </div>
               </div>
               <div class="col-sm-12">
                  <div class="form-group">
                     <label>Address Line 2</label>
                     <input type="text" placeholder="Address" name="address_line_2" id="address_line_12"  maxlength= "125" class="form-control">
                  </div>
               </div>
               <div class="col-sm-6">
                  <div class="form-group">
                     <label>Country</label>
                     <select name="country" class="form-control required" id="pp_pickup_country1" onchange="get_state2('pp_pickup_country1','pp_pickup_state1','pp_pickup_city1','pp_pickup_state1','','','','1','')">


                        <option value="" id="country">Select Country</option>
                        @foreach($country as $key)
                        <option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}","state_available":"{{$key->state_available}}"}'>{{$key->Content}}</option>
                        @endforeach
                     </select>
                  </div>
               </div>
               <div class="col-sm-6">
                  <div class="form-group">
                     <label>State</label>
                     <span id="ap_id1">
                     <select name="state1" class="form-control required left-disabled chosen-select" id="pp_pickup_state1" onchange="get_city('pp_pickup_state1','pp_pickup_city1','pp_pickup_city1','{{$key->PickupCity}}')">
                        <option value="">Select State</option>
                     </select>
                     </span>
                  </div>
               </div>
               <div class="col-sm-12"> </div>
               <div class="col-sm-6 ">
                  <div class="form-group">
                     <label>City</label>
                     <select  name="city" class="form-control required" id="pp_pickup_city1">
                        <option value="">Select City</option>
                     </select>
                  </div>
               </div>
               <div class="col-sm-6">
                  <div class="form-group">
                     <label>Zip Code/Postcode</label>
                     <input type="text" placeholder="Zip Code/Postcode" name="zipcode" id="new_zipcode"  maxlength="8" class="form-control alpha-numeric">
                  </div>
               </div>
	            
               <div class="clearfix"></div>
               
               <div class="modal-footer">
						<div class="col-sm-6">	 
							{!! Form::button('Submit', ['class' => 'custom-btn1 btn-block','id'=>'address_loader2','type'=>'submit']) !!}
						</div> 
						 <div class="col-sm-6">
							{!! Form::button('Close', ['class' => 'custom-btn1 btn-block ', 'data-dismiss'=>'modal']) !!} 
						</div>
						</div>
               	
            </div>
         </div>
         {!! Form::close() !!}
      </div>
   </div>
</div>


	<div class="container">
		<div class="row">
			<div class="col-sm-12">
                <div class="col-sm-12 row">
				 	<div class="col-sm-4">
						<h2 class="color-blue" >Saved Addresses</h2>
					</div>
					<div class="col-sm-4"></div>
				 	<div class="col-sm-4">
						<h4 class="color-blue" align="right"><b><button type="button" class="btn btn-primary blue-btn" data-target="#exampleModal" data-toggle="modal">Add New Address</button></b></h4>
					</div>
				</div>
				<br />
			</div>
        	<div class="col-md-9 col-sm-8 col-xs-12">
				<div class="box-shadow clearfix" id="address_container">
				
				@if(count($address) > 0)
					@foreach($address as $key)	
					<div class="col-md-6 col-sm-12" id="row-{{$key->_id}}">
						<div class="gray-bg address-btn-box">
							<div class="media addres-box">
								<div class="media-left">
									<a href="#">
									<img src="theme/web/promo/images/map-list.png" />	
									</a>
								</div>
								<div class="media-body">
									<br/>
									<h4 class="media-heading">{{ucfirst($key->address_line_1)}}</h4>
									<h4 class="media-heading">{{ucfirst($key->address_line_2)}}</h4>
									<p>{{$key->country}}. {{$key->state}}<br/>
									{{$key->city}}
									@if($key->zipcode != '')
									, {{$key->zipcode}}
									@endif
									</p>
								</div>
							</div>
							<div class="btn-box"><div>
							<a onclick="remove_record('delete_address/{{$key->_id}}/Deleteaddress','{{$key->_id}}')"  title="Delete" href="javascript:void(0)"><i class="fa fa-trash"></i></a>
								    	<a  type=""  title="Edit" data-toggle="modal"   onclick="get_edit_address('{{json_encode($key)}}')"
											data-whatever="@mdo" href="#address_modal" ><i class="fa fa-pencil"></i></a>
							</div>
							</div>
						</div>
						</br></br>
					</div>
					@endforeach	
				@else
				    </br></br>
					<div class="col-sm-12">
						<br><div align="center" class=""><b>No Record Found</b></div><br>
						<hr>
					</div>
				@endif	
					<div class="col-sm-8 col-sm-offset-2">     
                           <br/><br/>
                    </div>
				</div>
			</div>
			@include('Page::layout.side_bar')
		</div> 
	</div>
@endsection


@section('script')
@parent

{!! Html::script('theme/web/js/utility.js') !!}
{!! Html::script('theme/web/js/validation.js') !!}

@endsection

@section('inline-script')
@parent
<script>
var selected_state = '';
var selected_city = '';

function get_edit_address(obj)
{
	obj = eval('('+obj+')');
	console.log(obj);
	$('#address_line_1').val(obj.address_line_1);
	$('#address_line_2').val(obj.address_line_2);

	$('#pp_pickup_country').val('{"id":"'+obj.country_id+'","name":"'+obj.country+'","state_available":"'+obj.state_available+'"}');

	
	selected_state = obj.state;
	selected_city = obj.city;
	$('#zipcode').val(obj.zipcode);
	$('#address_id').val(obj._id);
	
	$('#pp_pickup_country').trigger('change');
    
}


	function remove_record(url,rowid)
	{	
	if(confirm('Are you sure? You want to delete this record.') == true)
	{
		$('#row-'+rowid).addClass('relative-pos spinning');
		
		url = SITEURL+url;
		$.ajax
			({
				url: url,
				headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
				success: function(res)
				{					
					var obj = JSON.parse(res);
					$('#row-'+rowid).removeClass('relative-pos spinning');
					if(obj.success == 1){
						$('#row-'+rowid).css({'background-color':'red'});
						$('#row-'+rowid).fadeOut('slow');
						$('#error_msg_section').html('<div class="alert alert-success">'+ obj.msg + '</div>');
					}else{
						$('#row-'+rowid).css({'background-color':'white'});
						$('#error_msg_section').html('<div class="alert alert-danger">'+ obj.msg + '</div>');
						alert(obj.msg);
					}
				}
			});
	}
	return false;
} 
 
 new Validate({
  FormName :  'new_address',
  ErrorLevel : 1,
  callback: function() {
      $("#address_loader2").addClass("spinning");
      
       $.ajax({
           url: SITEURL+'add-address',
           data: {     "address_line_1": $('#address_line_11').val() , 
                       "address_line_2": $('#address_line_12').val(),
                       "country": $('#pp_pickup_country1').val(),
                       "state": $('#pp_pickup_state1').val(),
                       "city":$('#pp_pickup_city1').val(),
                       "zipcode": $('#new_zipcode').val()},
           type : 'post',
           dataType: 'json',
           success : function(obj) {
                 $("#address_loader2").removeClass("spinning");
                
                if(obj.success == 1) {
                    document.getElementById("address_loader").value = "Submit";
                    document.getElementById("Addaddress").reset();
                    $('#address_container').html(obj.html);
                    $("#exampleModal").modal("hide");
                }
                alert(obj.msg);
           }
        });
   }
});


  new Validate({
    FormName :  'edit_address',
    ErrorLevel : 1,
    callback: function() {

       $("#address_loader").addClass("spinning");
       $.ajax({
           url: 'edit-address/{$key->_id}',
           data: $('#edit_address').serialize(),
           type : 'post',
		   dataType: 'json',
           success : function(obj) {
                 $("#address_loader").removeClass("spinning");
                
                if(obj.success == 1) {
                   

					document.getElementById("address_loader").value = "Submit";
                    document.getElementById("edit_address").reset();
                    $('#address_container').html(obj.html);
                    $("#address_modal").modal("hide");
                }
                alert(obj.msg);
           }
        });
    }
});


</script>

@endsection
