@extends('Page::layout.one-column-page')
@section('page_title')
My Shipping Request Detail - Aquantuo
@endsection
@section('content')
{!! Html::script('theme/web/rating/jquery.rateyo.js') !!}
{!!Html::style('theme/web/rating/jquery.rateyo.min.css')!!}
<?php $shipping_cost_by_user = $total_item_price = 0;?>
<div class="container">
   <div class="">
      <h2 class="color-blue mainHeading">Buy For Me Details</h2>
      <br />
      <div class="box-shadow">

         <!------------------step1------------------>
         <div class="box-header clearfix">
            {{ucfirst($item->ProductTitle)}}<span class="pull-right"><i class="fa fa-arrow-left" aria-hidden="true"></i>
            <a href="{{url('my-request')}}" style="color:#FFFFFF;">Back</a></span>
         </div>
         <div class="content-section row clearfix">
            <div class="col-sm-12">
                @if(isset($item->EnterOn->sec))
                <p style="margin-bottom:0;"><b>Created on:&nbsp;</b>
                {{show_date(@$item->EnterOn,'l M d, Y')}}</p>
                @endif



                @if($item->Status == 'delivered')
                     <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal">View Recipt</button>
                @endif
            </div>

         </div>
         <br />
         <div class="row">
            <?php $sno = 0;?>

            @foreach($item->ProductList as $value)
            <?php $total_item_price += $value['price'] * $value['qty'];
$shipping_cost_by_user += @$value['shipping_cost_by_user'];
?>
            <div class="">
               <div class="col-sm-12">
                  <p><b>Item Status:&nbsp;</b>
                   <?php echo get_status($value['status']); ?>

                  


                  <p><b>Item Url:&nbsp;</b> 
                  
                  <?php 

                    if(strpos($value['url'],'http://') === false && strpos($value['url'],'https://') == false){
                      $value['url'] = 'http://'.$value['url'];
                    }
                    
                  ?>
                 <a target="_blank" href='<?php echo $value['url']; ?>' >
                  {{$value['url']}}</a>

                  
                  </p>


                  <p><b>Package Id:&nbsp;</b>{{ucfirst($value['package_id'])}}</p>
                  </p>
                  <p><b>Description: </b>{{ucfirst($value['description'])}}</p>


                  @if($value['TransporterRating'] != '')
                  <p><b>Transporter Feedback :</b> {{ucfirst($value['TransporterFeedbcak'])}} </p>
                  <p><b><span class="pull-left">Rating: </span> </b>

                      <div class="star-ratings-sprite pull-left">
                        <span style="width:<?php echo $value['TransporterRating'] * 20; ?>%" class="star-ratings-sprite-rating"></span>
                     </div>
                     </br>
                  </p>
                  @endif
                   @if($value['RequesterFeedbcak'] != '')
                  <p><b>Your Feedback :</b> {{ucfirst($value['RequesterFeedbcak'])}} </p>
                  <p><b><span class="pull-left">Rating: </span> </b>

                      <div class="star-ratings-sprite pull-left">
                        <span style="width:<?php echo $value['RequesterRating'] * 20; ?>%" class="star-ratings-sprite-rating"></span>
                     </div>
                     </br>
                  </p>
                  @endif


               </div>
               <!--image -->
               @foreach($user_data as $key)
               @if($key['_id'] == $value['tpid'])

               <div class="col-sm-6">
                     <div class="media">
                    <div class="form-group media-left">
                     @if($key->Image != '')
                        <a class="fancybox" rel="group" href="{{ ImageUrl.$key->Image}}" >
                            <img src="{{ ImageUrl.$key->Image}}"  width="100px" height="100px" class="img-circle" style="margin-top:10px;">
                          </a>
                      @else
                          <img src="{{ImageUrl}}/user-no-image.jpg" width="100px" height="100px" class="img-circle" style="margin-top:10px;">
                      @endif
                    </div>
                    <div class="media-body">
                    <br>
                    <b style="margin-left:13px">Transporter Name: </b>{{ucfirst($key->Name)}}
                    <br>
                       @if(count($key) > 0)
                        <?php $average_rating = 0;
if ($key->RatingCount > 0 && $key->RatingByCount > 0) {
    $rating = $key->RatingCount / $key->RatingByCount;
    $average_rating = 20 * $rating;
}
?>
                     <div class="col-sm-12">
                      <div class="star-ratings-sprite pull-left">
                      <span style="width:<?php echo $average_rating; ?>%" class="star-ratings-sprite-rating"></span>
                    </div>
                     </div>
                     <a class="btn btn-primary" href="javascript:void(0)" id="chat-loader-{{$value['tpid']}}" onclick="open_chat_box('{{$value['tpid']}}','{{$key->Name}}','chat-loader-{{$value['tpid']}}')" style="margin-top:10px;margin-left:15px"><i class="fa fa-chat"></i> Chat</a>

                  @endif
                    </div>
                 </div>
               </div>
               @endif



              @endforeach
               <!-- end image -->

            </div>
            <div class="col-sm-12">
               <table class="custom-table table table-bordered">
                  <thead>
                     <tr>
                        <th>Item Image</th>
                        <th>Item Name</th>
                        <th>Item Category</th>
                        <th>Shipping Mode</th>
                        <th>Weight</th>
                        <th>Verification Code</th>
                        <th>Quantity</th>
                        <th>Item Price</th>
                     </tr>
                  </thead>
                  <tbody>
                     <tr>
                        <td>
                           @if($value['image'] != '')
                        <a class="fancybox" rel="group" href="{{ ImageUrl.$value['image'] }}" >
                           <img id="senior-preview" src="{{ ImageUrl.$value['image'] }}" class="" width="70px" height="70px" style="margin:0;" >
                        </td>
                        </a>
                        @else
                        <img src="{{ ImageUrl}}no-image.jpg" width="70px" class="" height="70px"  class="img-rounded">
                        @endif
                        </td>
                        <td>{{ucfirst($value['product_name'])}}</td>
                        <td>{{$value['category']}}</td>
                        <td>{{ucfirst($value['travelMode'])}}</td>
                        <td>
                          @if($value['weight'] != '')
                          {{$value['weight']}} {{ucfirst($value['weight_unit'])}}
                          @else
                          N/A
                          @endif

                        </td>
                        <td>{{$value['verify_code']}}</td>
                        <td>{{$value['qty']}}&nbsp;</td>
                        <td>${{number_format((float)$value['price'],2)}}</td>
                     </tr>
                     </tr>
                  </tbody>
               </table>

               @if($value['RequesterFeedbcak'] == '' &&  $value['status'] == 'delivered')
                <div class="pull-right"> <a href="javascript:void(0)" data-toggle="modal" data-target="#modal2" title="Review"  class="btn btn-default blue-btn" id= "feedback" onclick="give_feedback('{{$value['_id']}}')" style="">Give Feedback</a></div>
               @endif

           <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">View Recipt</h4>
        </div>
        <div class="modal-body">
            <div class="details-list" style="margin: 0px">
                 <h4 style="margin: 0px" class="text-primary">  {{ucfirst($item->RequesterName)}} </h4><br>
                  <span class="pull-right"><b>Package Number: </b> {{$item->PackageNumber}} </span>
                <p style="margin: 0px"> <strong>Address: </strong>{{ucfirst($item->DeliveryAddress)}}</p>
                    @foreach($user_data as $key)
                      @if($key['_id'] == $value['tpid'])
                        <span class="pull-right"> <b>Delivered By: </b> {{ucfirst($key->Name)}}</span>
                     @endif
                    @endforeach
                </div>



                <div class="details-list" style="margin: 0px">
                <p style="margin: 0px">
                    {{ucfirst($item->DeliveryCity)}}
                    {{ucfirst($item->DeliveryState)}}
                    {{ucfirst($item->DeliveryCountry)}}
                    {{ucfirst($item->DeliveryPincode)}}
                </p>
                </div>
              <br>
              <table id="example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                <tr>
                <th>S.No.</th>
                <th>Name</th>
                <th style="text-align:right;">Qty</th>
                <th style="text-align:right;">Price</th>
                </tr>
               <?php $sno = 1;?>
              @foreach($item->ProductList as $value)
                <tr>
                <td><?php echo $sno++; ?></td>
                <td>{{ucfirst($value['product_name'])}}</td>
                <td align="right">{{$value['qty']}} Box(es)</td>
                <td align="right" > ${{number_format($value['price'],2)}}</td>
                </tr>
               @endforeach

              </table>

                <div class="details-list" style="margin: 0px">
                 <div class="col-sm-4">
                    <strong>Int'l Shipping Cost:</strong>
                 </div>
                 <div class="col-sm-8">
                   ${{number_format($item->shippingCost,2)}}
                </p>
                 </div>
                </div>

                   <div class="details-list" style="margin: 0px">
                 <div class="col-sm-4">
                    <strong>Shipping Cost:</strong>
                 </div>
                 <div class="col-sm-8">
                  ${{number_format($shipping_cost_by_user,2)}}
                </p>
                 </div>
                </div>

                  <div class="details-list" style="margin: 0px">
                 <div class="col-sm-4">
                    <strong>Total Item(s) Price:</strong>
                 </div>
                 <div class="col-sm-8">
                  ${{number_format($total_item_price,2)}}
                </p>
                 </div>
                </div>
                 <hr></hr>
                  <div class="details-list" style="margin: 0px">
                 <div class="col-sm-4">
                    <strong>Total Price:</strong>
                 </div>
                 <div class="col-sm-8">
                 ${{ number_format($item->TotalCost,2)}}
                </p>
                 </div>
                </div>

                @if($item->insurance > 0)
                <div class="details-list" style="margin: 0px">
                 <div class="col-sm-4">
                    <strong>Insurance:</strong>
                 </div>
                 <div class="col-sm-8">
                  (+)${{number_format($item->insurance,2)}}
                </p>
                 </div>
                </div>
                @endif

                 @if($item->discount > 0)
                <div class="details-list" style="margin: 0px">
                 <div class="col-sm-4">
                    <strong>Aquantuo Discount:</strong>
                 </div>
                 <div class="col-sm-8">
                 (-)${{number_format($item->discount,2)}}
                </p>
                 </div>
                </div>
                @endif



                                  <br /><br /></br></br></br>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>
  <!-- endrecipt -->



@if($value['status'] == 'cancel')
<h3 class="">Cancel Information</h3>
<div class="row">
  <div class="col-md-6">
    <p><b>Return Address: </b> {{ucfirst(@$item['ReturnAddress'])}} </p>

    <p><b>Reject By: </b> {{ucfirst(@$value['RejectBy'])}} </p>
    <p><b>Return Type: </b> {{ucfirst(@$value['ReturnType'])}} </p>
    @if(!empty(trim($value['TrackingNumber'])))
    <p><b>Tracking Number: </b> {{@$value['TrackingNumber']}} </p>
    @endif


  </div>
  <div class="col-md-6">
    <p><b>Message: </b> {{ucfirst(@$value['TransporterMessage'])}} </p>

    @if(isset($value['CancelReturnDate']->sec))
      <p><b>Date: </b>  {{date('d M ,Y  h:i A',$value['CancelReturnDate']->sec)}} </p>
    @endif
    @if(!empty(trim($value['ReceiptImage'])))
      @if(file_exists(BASEURL_FILE.$value['ReceiptImage']))
        <p><b>Receipt: <br /></b>
            <a class="fancybox" rel="group" href="{{ImageUrl.$value['ReceiptImage']}}" >
              <img src="{{ImageUrl.$value['ReceiptImage']}}" alt="receipt" height="80" width="80" >
            </a>
        </p>
      @endif
    @endif

  </div>
</div>
@endif




               <div class="clearfix"></div>
               <hr/>
            </div>
            <?php $sno++;?>
            @endforeach

         </div>
         <div class="list-footer clearfix">
         	<br />
            <div class="row">
               <div class="col-sm-6">
               <?php $item_count = 0;?>
                @foreach($item->ProductList as $value)
                <?php $item_count++;?>
                @endforeach

                  <p><b>Total Number of Item(s): </b><?php echo $item_count; ?> </p>
                  <p><b>Distance</b>: {{number_format($item->distance,2)}} Miles</p>
                  <p><b>Recipient's Phone Number: </b>
                     @if(!$item->ReceiverCountrycode == '')
                     {{$item->ReceiverCountrycode}}-
                     @endif
                     {{$item->ReceiverMobileNo}}
                  </p>
                  <p><b>Delivery Address: </b>{{ucfirst($item->DeliveryFullAddress)}}
                  <br>
                  @if(isset($item->EnterOn->sec))
                  {{show_date($item->EnterOn,'l M d, Y')}}
                  @endif
                  </p>
               </div>

               <div class="payment-sec col-sm-4 pull-right">
                  <h3 class="smallHeading">Payment Information</h3>
                  <table class="table table-bordered" style="margin:0;">
                     <tbody bgcolor="#FFFFFF">
                        <tr>
                           <td class="">
                              <div class="col-xs-8 text-left">
                                 <label class="" for="exampleInputEmail1"><b>Int'l Shipping Cost:</b> </label>
                              </div>
                              <div class="col-xs-4 text-right">
                                 ${{number_format($item->shippingCost,2)}}
                              </div>
                           </td>
                        </tr>
                        <tr>
                           <td class="">
                              <div class="col-xs-8 text-left">
                                 <label class="" for="exampleInputEmail1">
                                    <b>Shipping Cost: <br />
                                    <small>(Retailer to Aquantuo's facility)</small>
                                    </b>
                                 </label>
                              </div>
                              <div class="col-xs-4 text-right">
                                 ${{number_format($shipping_cost_by_user,2)}}
                              </div>
                           </td>
                        </tr>

                        <tr>
                           <td class="">
                              <div class="col-xs-8 text-left">
                                 <label class="" for="exampleInputEmail1"><b>Total Item(s) Price:</b> </label>
                              </div>
                              <div class="col-xs-4 text-right">
                                 ${{number_format($total_item_price,2)}}
                              </div>
                           </td>
                        </tr>
                        @if($item->insurance > 0)
                        <tr>
                           <td class="">
                              <div class="col-xs-8 text-left">
                                 <label class="" for="exampleInputEmail1"><b>Insurance:</b> </label>
                              </div>
                              <div class="col-xs-4 text-right">
                                 (+)${{number_format($item->insurance,2)}}
                              </div>
                           </td>
                        </tr>
                        @endif
                         <tr>
                           <td class="">
                              <div class="col-xs-8 text-left">
                                 <label class="" for="exampleInputEmail1">
                                    <b>Processing Fee: <br />
                                    </b>
                                 </label>
                              </div>
                              <div class="col-xs-4 text-right">
                                 @if($item['ProcessingFees'] != '')
                                 ${{number_format($item['ProcessingFees'],2)}}
                                 @else
                                 $0.00
                                 @endif
                              </div>
                           </td>
                        </tr>
                        <tr>
                           <td class="">
                              <div class="col-xs-8 text-left">
                                 <label class="" for="exampleInputEmail1"><b>Aquantuo Discount:
                                 </b></label>
                                 <br/>
                                 @if(!$item->PromoCode == '')
                                 <small>(<b>Promocode:</b>{{$item->PromoCode }}) </small>
                                 @endif
                              </div>
                              <div class="col-xs-4 text-right">
                              @if($item->discount > 0)
                                 (-)${{number_format($item->discount,2)}}
                              @else
                              (-)$0.00
                              @endif
                              </div>
                           </td>
                        </tr>

                        <tr>
                           <td class="">
                              <div class="col-xs-8 text-left">
                                 <label class="" for="exampleInputEmail1"><b>Total Price:</b> </label>
                              </div>
                              <div class="col-xs-4 text-right">
                                 ${{ number_format($item->TotalCost,2)}}
                              </div>
                           </td>
                        </tr>
                     </tbody>
                  </table>


                  <br>
                  @if($item['Status'] == 'not_purchased')
                  <table class="table table-bordered" style="margin:0;">
                     <tbody bgcolor="#FFFFFF">
                       <tr>
                           <td class="">
                              <div class="col-xs-8 text-left">
                                 <label class="" for="exampleInputEmail1">
                                    <b>Before Purchase Cost: <br />
                                    </b>
                                 </label>
                              </div>
                              <div class="col-xs-4 text-right">
                                 @if($item['BeforePurchaseTotalCost'] != '')
                                 ${{number_format($item['BeforePurchaseTotalCost'],2)}}
                                 @else
                                 $0.00
                                 @endif
                              </div>
                           </td>
                        </tr>


                        <tr>
                           <td class="">
                              <div class="col-xs-8 text-left">
                                 <label class="" for="exampleInputEmail1">
                                    <b style="color:green;">Payable Difference: <br />
                                    </b>
                                 </label>
                              </div>
                              <div class="col-xs-4 text-right">
                                 @if($item['BeforePurchaseTotalCost'] != ''  &&  $item['TotalCost'] != '')
                                 <?php $dif = $item['TotalCost'] - $item['BeforePurchaseTotalCost'] ?>
                                 ${{number_format($dif,2)}}
                                 @else
                                 $0.00
                                 @endif
                              </div>
                           </td>
                        </tr>
                     </tbody>
                  </table>
                  @endif


                  
                  @if($item['Status'] == 'not_purchased')
                  <br>
                  <button  class="btn btn-primary" type="button" class="btn btn-primary blue-btn"   data-toggle="modal" data-target="#exampleModal">Pay Now</button>
                  @endif
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div aria-labelledby="exampleModalLabel" role="dialog" tabindex="-1" id="exampleModal" class="modal fade in">
   <div role="document" class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header" id="Addaddress2">
            <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button>
            <h4 id="exampleModalLabel" class="modal-title">Quote</h4>
         </div>
         <br >
         {!! Form::open(['url' => 'payment-by-for-me/'.Request::segment(2),'method' => 'get']) !!}
         <div class="modal-body" >
            <div class="col-md-12">
               <div class="row">
                  <div class="col-xs-6">
                     <b>Distance</b>
                  </div>
                  <div class="col-xs-6 text-right">
                     <p>{{number_format($item->distance,2)}} Miles</p>
                  </div>
               </div>
               <div class="row">
                  <div class="col-xs-6">
                     <b>No. of items</b>
                  </div>
                  <div class="col-xs-6 text-right">
                     <p><?php echo $sno; ?></p>
                  </div>
               </div>
               <div class="row">
                  <div class="col-xs-6">
                     <b>Int'l Shipping Cost</b>
                  </div>
                  <div class="col-xs-6 text-right">
                     <p>${{number_format($item->shippingCost,2)}}</p>
                  </div>
               </div>
               <div class="row">
                  <div class="col-xs-6">
                     <b>Shipping Cost</b><br />
                     <small>(Retailer to Aquantuo's facility)</small>
                  </div>
                  <div class="col-xs-6 text-right">
                     <p>${{number_format($shipping_cost_by_user,2)}}</p>
                  </div>
               </div>
               @if($item->insurance > 0)
               <div class="row">
                  <div class="col-xs-6">
                     <b>Insurance Cost</b>
                  </div>
                  <div class="col-xs-6 text-right">
                     <p>${{number_format($item->insurance,2)}}</p>
                  </div>
               </div>
               @endif
                 <div class="row">
                    <div class="col-xs-6">
                       <b>Items Price</b>
                    </div>
                    <div class="col-xs-6 text-right">
                       <p>${{number_format($total_item_price,2)}}</p>
                    </div>
                 </div>

                 <div class="row">
                    <div class="col-xs-6">
                       <b>Processing Fee</b>
                    </div>
                    <div class="col-xs-6 text-right">

                      <p>${{number_format($item->ProcessingFees,2)}}</p>

                    </div>
                 </div>

               @if($item->discount > 0)
               <div class="row">
                  <div class="col-xs-6">
                     <b>Discount: </b>
                  </div>
                  <div class="col-xs-6 text-right">
                     <p>${{number_format($item->discount,2)}}</p>
                  </div>
               </div>
               @endif
               <div class="row">
               <hr />
                  <div class="col-xs-6">
                     <b>Total Cost</b><br />
                  </div>
                  <div class="col-xs-6 text-right">
                     <p>${{number_format($item->TotalCost,2)}}</p>
                  </div>
               </div>
               @if($item->BeforePurchaseTotalCost < $item->TotalCost)
               <hr />
               <div class="row">
                  <div class="col-xs-6"> <b>Already Paid</b> </div>
                  <div class="col-xs-6 text-right">
                     <p>${{number_format($item->BeforePurchaseTotalCost,2)}}</p>
                  </div>
               </div>
               <hr />
               <div class="row">
                  <div class="col-xs-6">
                     <b>Remaining Payment</b><br />
                  </div>
                  <div class="col-xs-6 text-right">
                     <p>${{number_format(($item->TotalCost - $item->BeforePurchaseTotalCost),2)}}</p>
                  </div>
               </div>
               @endif
            </div>
         </div>
         <div class="clearfix"></div>
         <hr>
         <div class="text-right">
            <button type="submit"  class="btn btn-default blue-btn "  title="Accept" value="Accept">Accept</button>
            <button type="button"  class="btn btn-default blue-btn "  title="Cancel" id="cancel">
            &nbsp; Cancel &nbsp;&nbsp;</button>&nbsp;&nbsp;
         </div>
         {!! Form::close() !!}
         <br/>
      </div>
   </div>
</div>
{!! Html::script('theme/web/js/validation.js') !!}

<!-- Model -->
<!--review-model-->
<div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
      <div class="pre-modal-dialog modal-md">
        <div class="modal-content">
          <div class="modal-header popup-bg text-center">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <p class="popup-head color-white">Rate your experience with transporter</p>

          </div>
          <div class="clearfix"></div>
        <!-- <input type="hidden" name="item_id"  id="item_id" value="<?php /*echo $item->_id   */?>" >   -->
        <center>
         <div class="modal-body text-left">
        <div id="rating_success_container"></div>
        <div id="rating_danger_container"></div>
            {!! Form::model('', ['name' => 'RequestReview', 'id' => 'RequestReview']) !!}
            <div class="col-md-12">
               <div class="col-md-12" align="center">
               <div>



                          <img src="{{ImageUrl}}/user-no-image.jpg" width="150px" height="150px" class="img-circle">


                                 </div>
               </br>
               <div>
                  <label class="rating_margin" >Rate Now :</label>
                     &nbsp;&nbsp;&nbsp;
                     <div class="rateyo required" id="rateyo" ></div>
                     <div class="clearfix"></div>
                     <div id="er_rating"  class="error-msg"></div>
                 <input type="hidden" name="tpid" id="tpid" value="{{$value['tpid']}}">
                 <input type="hidden" name="item_id" id="item_id" value="{{$value['_id']}}">
                     </div>
                     </br>
                  </div>

                  <div class="col-md-12">
                   <div class="">
                     <div class="form-group txt-area feedback-text">
                        {!! Form::textarea('review','',['class'=>'form-control required', 'placeholder'=> 'Feedback','id' =>'review','maxlength' =>250,'size' =>'30x4']) !!}
                     </div>
                   <div id="er_review" class="help-block white"></div>
                   <div class="clearfix"></div>
                     <span id="review_msg"></span>
                  </div>
               </div>
               <div class="col-md-2"></div>
            </div>
             <div class="clearfix"></div>
                    <div class="col-xs-6">
                       <div class="form-group">
                          <button type="submit" class="custom-btn1 btn-block" id='ratingButton'>SUBMIT</button>
                       <div class="error-msg"></div></div>
                    </div>
                    <div class="col-xs-6">
                       <div class="form-group text-center">
                          <button class="custom-btn1 btn-block" data-dismiss='modal' >NOT NOW</button>
                       </div>
                    </div>


                  {!! Form::close() !!}

          <div class="clearfix"></div>

        </div>
      </div>


<!--end-->


<script type="text/javascript">
$(document).ready(function() {
  $(".fancybox").fancybox();
});

$('#cancel').click(function(){
  $('#exampleModal').trigger("click");
});

new Validate({
      FormName : 'RequestReview',
      ErrorLevel : 1,
      validateHidden: false,
      callback : function()
    {
      if($rateYo.rateYo("rating") <= 0) {
        $('#er_rating').html('The rating field is required.');
        return false;
      }
        $("#ratingButton").addClass('spinning');
        $.ajax({
          url     : '{{url("/post-requester-review")}}',
          type    : 'post',
          data    : {
              "review" : $("#review").val(),
              "item_id" : $('#item_id').val(),
              "tpid"    : $('#tpid').val(),
              "rating" : $rateYo.rateYo("rating"),
            },
            success : function(res)
            {
                $("#ratingButton").removeClass('spinning');
                var obj = eval("("+res+")");
                if(obj.success == 1)
                {
                  $("#feedback").hide();
                  location.reload();
                  $("#RequestReview").trigger( "reset" );
                  $('#rating_success_container').html('<div role="alert" class="alert alert-success alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>'+obj.msg+'</div>');
                }
                else
                {

                  $('#rating_danger_container').html('<div role="alert" class="alert alert-danger alert-dismissible fade in"><button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>'+obj.msg+'</div>');
                }
            }
        });
      }

   });


</script>
@endsection
<style>
.pre-modal-dialog {
    margin: 30px auto;
    width: 440px;
}
.modal-feedback {
    margin: 30px auto;
    position: relative;
    width: 420px;
}
</style>

@section('inline-script')
@parent
<script>
function give_feedback($id)
{
   $('#item_id').val($id);
}


$(".fancybox").fancybox();

var $rateYo = $("#rateyo").rateYo();


</script>
@endsection
