@extends('Page::layout.one-column-page')
@section('content')

<div class="container">
	
	 <div class="pull-left">
        <h2 class="inner-head color-blue mainHeading">Bank Information</h2>
        
   </div>
   <div class="clearfix"></div>
    <div class="row">
  
  
         
	<div class="col-md-9 col-sm-8 col-xs-12">
							
					<div class="box-shadow clearfix">
					 <br/></br>	
					    <p><i class="fa fa-bank" aria-hidden="true"></i>  <b>{{ucfirst($users->BankName)}}</b></p>
					    <hr/>
						<p><b>Account Holder Name :</b> {{ucfirst($users->AccountHolderName)}}</p>
						<hr/>
						<p><b>Account No. :</b> {{$users->BankAccountNo}}</p>
						<hr/>
						<p><b>Routing No. :</b> {{$users->RoutingNo}}</p>
						<hr/>
						<p><b>Paypal Id :</b> {{$users->PaypalId}}</p>
						<hr/>
					<div class="form-group">
						<a href="{{url('edit-bank-information')}}" class="custom-btn1 btn" type="button">&nbsp; Change Bank Information &nbsp;</a>
					</div>
					</div>
                    </div>
                 
                   @include('Page::layout.side_bar')
    
    </div>
</div>


@endsection
