 @extends('Page::layout.one-column-page')
@section('content')

<div class="container">
<div class="pull-left">
   <h2 class="inner-head color-blue mainHeading">Change Bank Information</h2>
</div>
<div class="clearfix"></div>
<div class="row">
<div class="col-md-9 col-sm-8 col-xs-12">
   <div class="box-shadow clearfix">
      <h3>Please provide your bank information to receive payments</h3>
      <hr/>

      {!! Form::open(['name'=>'ServicesForm','id' => 'ServicesForm','url'=> 'post-edit-bank-information', 'class' => 'form-vertical','method' => 'POST']) !!}
        </br>
      <label>BANK A/C COUNTRY</label>
      <div class="">
         <select name="bank_country" class="form-control required usename-#bank country#">
            <option vlaue="">Select Country</option>
            @foreach($country as $key)
               <option value="{{$key}}" @if($user->BankCountryCode == $key->ShortCode) selected @endif >{{$key->Content}}</option>
            @endforeach
         </select>
         <p class="help-block red" style="color:red;"  >{{$errors->first('bank_country')}} </p>
      </div>
      </br>

      <label>BANK NAME</label>
      <div class="">
         <input type="text" value="<?php if (!empty(trim(Input::old('bank_name')))) {echo Input::old('bank_name');} else {echo $user->BankName;}?>" name="bank_name" class="form-control required" placeholder="Bank name" maxlength="60" >
         <p class="help-block red" style="color:red;" >{{$errors->first('bank_name')}} </p>
      </div>
      </br>
      <label>ACCOUNT HOLDER NAME</label>
      <div class="">
         <input type="text" value="<?php if (!empty(trim(Input::old('account_holder_name')))) {echo Input::old('account_holder_name');} else {echo $user->AccountHolderName;}?>" name="account_holder_name" class="form-control required" placeholder="Account holder name" maxlength="60">
         <p class="help-block red" style="color:red;" >{{$errors->first('account_holder_name')}}</p>
      </div>
      </br>
      <label>ACCOUNT NO.</label>
      <div class="">
         <input type="text" value="<?php if (!empty(Input::old('account_no'))) {echo trim(Input::old('account_no'));} else {echo $user->BankAccountNo;}?>" name="account_no" placeholder="Account no" class="form-control required numeric  maxlength="30">
         <p class="help-block red" style="color:red;"  >{{$errors->first('account_no')}} </p>
      </div>
      </br>
      <label>ROUTING NO.</label>
      <div class="">
         <input type="text" value="<?php if (!empty(trim(Input::old('routing_no')))) {echo trim(Input::old('routing_no'));} else {echo $user->RoutingNo;}?>" name="routing_no" placeholder="Routing no" class="form-control required" maxlength="10">
         <p class="help-block red" style="color:red;"  >{{$errors->first('routing_no')}} </p>
      </div>
         Note: <small>Routing number should be given in the format local to your country</br>
         CANADA: 5 Digit transit number and 3 digit institution number (Ex: 11000-000)</br>
         USA: 9 Digit routing number.</br>
         UK: 6 Digit routing number</small></br >
      </br>
      <label>PAYPAL ID</label>
      <div class="">
         <input type="text" value="<?php if (!empty(trim(Input::old('PaypalId')))) {echo Input::old('PaypalId');} else {echo $user->PaypalId;}?>" name="PaypalId" placeholder="Paypal ID" class="form-control" maxlength="50" >
         <p class="help-block red" style="color:red;"  >{{$errors->first('PaypalId')}} </p>
      </div>
      </br>
      <div class="col-sm-8 col-sm-offset-2">
         <div class="form-group">
            <button class="custom-btn1 btn-block">
               Update
               <div class="custom-btn-h"></div>
            </button>
            <br></br><br></br>
         </div>
      </div>
      {!! Form::close() !!}

   </div>
</div>
@include('Page::layout.side_bar')
@endsection
@section('script')
@parent
	{!! Html::script('theme/web/js/validations.js') !!}
@endsection
@section('inline-script')
@parent
	<script>

	   new Validate({
	       FormName :  'ServicesForm',
	       ErrorLevel : 1,

	      });

	</script>
@endsection

