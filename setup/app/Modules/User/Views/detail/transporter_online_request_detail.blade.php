@extends('Page::layout.one-column-page')
@section('content')
{!! Html::script('theme/web/rating/jquery.rateyo.js') !!}
{!!Html::style('theme/web/rating/jquery.rateyo.min.css')!!}
<style>
.pre-modal-dialog {
    margin: 30px auto;
    width: 440px;

}
</style>
<div class="container">
<div class="row">
<div class="col-sm-12">
<h2 class="color-blue mainHeading">Online Delivery Details </h2>
<br />
<div id="success-message"></div>
</div>

<?php $ProductList = $transporterDetail->ProductList[0];?>

   <div class="col-md-12">
      <div class="box-shadow">
         <div class="content-section clearfix">

          <div class="">

          <span class="pull-right"></i>
          <a href="{{url('transporter/my-deliveries')}}"  class="btn btn-default blue-btn" style="color:#FFFFFF;"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;&nbsp;Back</a>&nbsp;&nbsp;</span>

                </div>

            @foreach($transporterDetail->ProductList as $value)
            <?php

$item_id = $value['_id'];
$requester_id = $transporterDetail->RequesterId;

?>

            <br />
            <div class="col-md-2 col-sm-3">
               @if(@$ProductList['image'] == '')
                <img src="{{ImageUrl}}/no-image.jpg"  width="150px" height="150px" >
                @else
                <a class="fancybox" rel="group" href="{{ ImageUrl.$ProductList['image']}}" >
                <img src="{{ ImageUrl.$ProductList['image']}}"  width="150px" height="150px">
                </a>
                @endif
            </div>
            <div class="col-md-10 col-sm-9">
               <h4><b>{{ucfirst($value['product_name'])}}</b></h4>

               <p class="color-black" id="status_div"><strong>Status:</strong>

				          @if($value['status'] == 'out_for_pickup')
					          Out for Pickup
                  @elseif($value['status'] == 'out_for_delivery')
					           Out for Delivery
				          @elseif($value['status'] == 'assign')
					         Shipment Departed <!-- Transporter Assigned -->
                  @elseif($value['status'] == 'cancel')
                    Canceled
                  @else
                   {{ucfirst($value['status'])}}
                  @endif
                </p>
				          <p class="color-black" style="display:none;" id="ajax_status"><strong>Status:</strong></P>

               <div class="form-group">

                    @if(in_array($value['status'],['assign','out_for_pickup']))
                    <button  class="btn btn-default blue-btn"  title="Cancel" id= "b_cancel" onclick="return confirm('Are you sure you want to cancel this request?')? online_request('cancel'):'';" style="display:inline-block;" >
                    &nbsp; Cancel &nbsp;&nbsp;</button>
                    @endif

                    @if($value['status'] == "assign")
                    <button  class="btn btn-default blue-btn"  title="Out for pickup" id= "b_pickup" onclick="return confirm('Are you sure you are going to pickup the package?')? online_request('pickup'):'';" >
                    &nbsp; Out for Pickup &nbsp;&nbsp;</button>
                    @endif

<?php $btn_status = 'display:none';
if ($value['status'] == "out_for_pickup") {$btn_status = 'display:inline-block';}?>

                       <button  class="btn btn-default blue-btn"  title="Out for delivery" id= "b_delivery"  onclick="return confirm('Are you sure the package has been picked up?')? online_request('delivery'):'';" style="{{$btn_status}}">
                       &nbsp; Out for Delivery &nbsp;&nbsp;</button>

<?php $btn_status = 'display:none';
if (in_array($value['status'], ['assign', 'out_for_pickup', 'out_for_delivery', 'accepted'])) {
    $btn_status = 'display:inline-block;';
}?>
<button class="btn btn-default blue-btn" style="{{$btn_status}}" id="chat-btn" onclick="open_chat_box('{{$transporterDetail->RequesterId}}','{{$transporterDetail->RequesterName}}','chat-btn')"><i class="fa fa-chat"></i> Chat</button>

<?php $btn_status = 'display:none';
if ($value['status'] == "out_for_delivery") {$btn_status = 'display:inline-block';}?>
<button class="btn btn-default blue-btn" href="javascript:void(0)" data-toggle="modal" data-target="#b_deliver_model" title="delivery"   id="b_deliver" style="{{$btn_status}}"> &nbsp;Delivered&nbsp;</button>

<button class="btn btn-default blue-btn" href="javascript:void(0)" data-toggle="modal" data-target="#exampleModal" title="Cancel Delivery"   id="b_no_delivery" style="{{$btn_status}}"> &nbsp;Not Delivered</button>

                 <?php $btn_status = 'display:none';
if ($value['status'] == "delivered") {$btn_status = 'display:true';}?>
  @if($value['TransporterFeedbcak'] == '' && $value['TransporterRating'] == '')
       <a href="javascript:void(0)" data-toggle="modal" data-target="#modal2" title="Review"  class="btn btn-default blue-btn" id= "feedback" style="{{$btn_status}}">Give Feedback</a>
  @endif


              @endforeach


               </div>
            </div>
         </div>
      </div>
   </div>




<div class="col-sm-12">
<div class="">

    <br />

          <div class="box-shadow">
                              <!------------------step1------------------>

                <div class="content-section row clearfix">
          <div class="col-sm-6">
          @foreach($transporterDetail->ProductList as $value)

            <p><b>Package Id: </b> {{$value['package_id']}}</p>
            @if(isset($transporterDetail->EnterOn->sec))
            	<p><b>Date of Request: </b> {{date('l,  M  d, Y ',$transporterDetail->EnterOn->sec)}}</p>
            @endif

			<p class=""><b>Desired Delivery Date :</b>
				@if(isset($transporterDetail->DeliveryDate->sec))
				   {{date('F d,Y',$transporterDetail->DeliveryDate->sec)}}
         @else
            N/A
				@endif
			</p>



          </div>
          <div class="col-sm-6">
            <p class=""><b>Expected Delivery Date :</b>
        @if(isset($value['ExpectedDate']->sec))
           
           {{ show_date(@$value['ExpectedDate']) }}
         @else
            N/A
        @endif
      </p>
          </div>
                </div>
                <br />
                    <div class="row">
                    <?php $sno = 0;?>



                        <div class="">

              <div class="col-sm-6">
                <p><b>Purchased from: </b><a href="#">{{$value['url']}}</a></p>
                <p><b>Description: </b>{{ucfirst($value['description'])}}</p>
                <p><b>Insurance Status: </b> {{ucfirst($value['insurance_status'])}} </p>
                @if($value['TransporterRating'] != '')
                  <p><b>Your Feedback :</b> {{ucfirst($value['TransporterFeedbcak'])}} </p>
                  <p><b><span class="pull-left">Rating : &nbsp; </span> </b>

                      <div class="star-ratings-sprite pull-left">
                        <span style="width:<?php echo $value['TransporterRating'] * 20; ?>%" class="star-ratings-sprite-rating"></span>
                     </div>

                     </br>
                  </p>
                  @endif
              </div>
                          <div class="col-sm-6">




                 <div class="media">
                    <div class="form-group media-left">
                     @if($user_data->Image != '')
                        <a class="fancybox" rel="group" href="{{ ImageUrl.$user_data->Image}}" >
                            <img src="{{ ImageUrl.$user_data->Image}}"  width="150px" height="150px" class="img-circle">
                          </a>
                      @else
                          <img src="{{ImageUrl}}/user-no-image.jpg" width="150px" height="150px" class="img-circle">
                      @endif
                    </div>
                    <div class="media-body">
                    <br>
                    <p><b>Requester Name: </b>{{ucfirst($user_data->Name)}}</p>
@if(count($user_data) > 0)
<?php $average_rating = 0;
if ($user_data->RatingCount > 0 && $user_data->RatingByCount > 0) {
    $rating = $user_data->RatingCount / $user_data->RatingByCount;
    $average_rating = 20 * $rating;
}
?>
            <div class="col-sm-12">
              <div class="star-ratings-sprite pull-left">
                <span style="width:{{ $average_rating }}%" class="star-ratings-sprite-rating"></span>
              </div>
            </div>
          @endif
        </div>
      </div>
    </div>
</div>
<div class="col-sm-12">
<table class="custom-table table table-bordered">
<thead>
  <tr>
    <th>Item Image</th>
    <th>Item Name</th>
    <th>Item Category</th>
    <th>Shipping Mode</th>
    <th>Weight</th>
    <th>Quantity</th>
    <th>Item Price</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td><img id="senior-preview" src="{{ ImageUrl}}/no-image.jpg"  width="120px" height="100px" /></td>
    <td>{{ucfirst($value['product_name'])}}</td>
    <td>{{$value['category']}}</td>
    <td>{{ucfirst($value['travelMode'])}}</td>
    <td>
    {{$value['weight']}}
      @if($value['weight_unit'] == 'cm_kg')
        Cm kg
      @else
        {{$value['weight_unit']}}
      @endif
    </td>
    <td>{{$value['qty']}}</td>
    <td>${{number_format((float)$value['price'],2,'.','')}}</td>
  </tr>
</tbody>
</table>

</div>
                        <?php $sno++;?>
<!--verify_model -->

<div class="modal fade" id="b_deliver_model" role="dialog">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button  class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><b>Delivery Verification</b></h4>
      </div>
      <div class="modal-body form-group" >
        {!! Form::model('', ['name' => 'b_verify', 'id' => 'b_verify']) !!}
          {{-- <div class="checkbox">
            <label>
              <input type="checkbox" value="" class="mt0" id="accept_tc" >
              I Accept <a href="{{url('terms-and-conditions')}}" target="_blank">Terms and Conditions.</a>
            </label>
          </div>
 --}}
          <div class="map-hloder">
            <input type="hidden" name="request_id" id="request_id" value="{{ $value['_id'] }}">
            {!!Form::label('b_code','Verification Code (obtain this from Requester)',['class'=>'control-label'])!!}
            <div>
              {!!Form::text('b_code','',['class'=>'form-control required numeric minlength-4 usename-#verification code#','id'=>'b_code','placeholder'=>'Enter Verification Code','maxlength' => 4])!!}
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-sm-3 col-xs-2">
              {!! Form::button('Submit', ['class' => 'btn btn-default','value'=>'SUBMIT','id'=>'b_verify_button','type' => 'submit']) !!}
            </div>
            <div class="col-sm-9 col-xs-10">
              {!! Form::button('Close', ['class' => 'btn btn-default', 'data-dismiss'=>'modal','value'=>"Not Now"]) !!}
            </div>
          </div>
          <div class="clearfix"></div>
        {!! Form::close() !!}
      </div>
    </div>
  <div class="clearfix"></div>
  </div>
</div>
<!--end-->

@endforeach

 </div>

    <div class="list-footer clearfix">
        <div class="row">
          <div class="col-sm-4">
                @if($transporterDetail['insurance'] > 0)
               @endif
                <p><b>Phone Number: </b>
                    @if(!$transporterDetail->country_code == '')
                      {{$transporterDetail->country_code}} -
                    @endif

                &nbsp;{{$transporterDetail->ReceiverMobileNo}}</p>
                <p><b>Receiver Name:</b> @if(@$transporterDetail->ReceiverName != '') {{ucfirst(@$transporterDetail->ReceiverName)}} @else N/A @endif</p>
                <p><b>Distance: </b>{{number_format($transporterDetail->distance,2)}} Miles</p>
                <p><b>Delivery Address: </b>{{ucfirst($transporterDetail->DeliveryFullAddress)}} </p>
              </div>

              @if($value['status'] == 'cancel')

                <div class="payment-sec col-sm-4 pull-right">
                 <h3 class="">Cancel Information</h3>
                                  <table class="table table-bordered">
                      <tbody bgcolor="#FFFFFF">

                      <tr>
                        <td class="">
                          <div class="col-xs-12 text-left">
                            <label class="" for="exampleInputEmail1">
                              <b>Return Address:</b>
                            </label> <br />
                            {{ucfirst(@$transporterDetail['ReturnAddress'])}}
                          </div>
                          </td>
                      </tr>

                      <tr>
                        <td class="">
                          <div class="col-xs-6 text-left">
                            <label class="" for="exampleInputEmail1"><b>Reject By:</b> </label>
                          </div>
                          <div class="col-xs-6 text-right">
                             {{ucfirst($ProductList['RejectBy'])}}
                          </div>
                          </td>
                      </tr>

                      <tr>
                        <td class="">
                          <div class="col-xs-6 text-left">
                            <label class="" for="exampleInputEmail1"><b>Return Type:</b> </label>
                          </div>
                          <div class="col-xs-6 text-right">
                             {{get_cancel_status($ProductList['ReturnType'])}}
                          </div>
                          </td>
                      </tr>


                      <tr>
                        <td class="">
                          <div class="col-xs-12 text-left">
                            <label class="" for="exampleInputEmail1"><b>Message:</b> </label>
                            <br />
                            {{ucfirst($ProductList['TransporterMessage'])}}
                          </div>
                        </td>
                      </tr>
                       @if(!empty(trim(@$ProductList['TrackingNumber'])))
                       <tr>
                        <td class="">
                          <div class="col-xs-6 text-left">
                            <label class="" for="exampleInputEmail1"><b>Tracking Number:</b> </label>
                          </div>
                          <div class="col-xs-6 text-right">
                             {{@$ProductList['TrackingNumber']}}
                          </div>
                          </td>
                      </tr>
                       @endif

                      @if(isset($ProductList['CancelReturnDate']->sec))
                       <tr>
                        <td class="">
                          <div class="col-xs-6 text-left">
                            <label class="" for="exampleInputEmail1"><b>Date:</b> </label>
                          </div>
                          <div class="col-xs-6 text-right">
                             {{date('d M ,Y  h:i A',$ProductList['CancelReturnDate']->sec)}}
                          </div>
                          </td>
                      </tr>
                      @endif

                      @if(!empty(trim($ProductList['ReceiptImage'])))
                        @if(file_exists(BASEURL_FILE.$ProductList['ReceiptImage']))
                          <tr>
                            <td class="">
                              <div class="col-xs-6 text-left">
                                <label class="" for="exampleInputEmail1"><b>Receipt: </b> </label>
                                </div>
                              <div class="col-xs-6 text-right">
                                  <a class="fancybox" rel="group" href="{{ImageUrl.$ProductList['ReceiptImage']}}" >
                                    <img src="{{ImageUrl.$ProductList['ReceiptImage']}}" alt="receipt" height="100" width="100" >
                                  </a>
                              </div>
                              </td>
                          </tr>
                        @endif
                      @endif
                    </tbody>
                  </table>
                </div>
               @endif


            @if(!$transporterDetail['shippingCost'] == '')
              <div class="payment-sec col-sm-4 pull-right">
                <h3 class="">Earning Detail</h3>
                                  <table class="table table-bordered">
                      <tbody bgcolor="#FFFFFF">
                      <tr>
                        <td class="">
                          <div class="col-xs-8 text-left">
                            <label class="" for="exampleInputEmail1"><b>Your Earning:</b> </label>
                          </div>
                          <div class="col-xs-4 text-right">
                            @if(!$transporterDetail['shippingCost'] == '')
                              ${{ number_format( ($value['shippingCost'] - floatval(@$value['aq_fee'])) ,2) }}
                            @endif
                          </div>
                          </td>
                      </tr>
                      <tr>
                        <td class="">
                          <div class="col-xs-8 text-left">
                            <label class="" for="exampleInputEmail1"><b>Aquantuo Earning:</b> </label>
                          </div>
                          <div class="col-xs-4 text-right">
                              ${{ number_format(floatval(@$value['aq_fee']),2) }}
                          </div>
                          </td>
                      </tr>

                      <tr>
                        <td class="">
                          <div class="col-xs-8 text-left">
                            <label class="" for="exampleInputEmail1"><b>Shipping Cost:</b> </label>
                          </div>
                          <div class="col-xs-4 text-right">
                            ${{ number_format($value['shippingCost'],2) }}
                          </div>
                        </td>
                      </tr>
                                        </tbody>
                  </table>


               @endif



                            </div>
                        </div>
                    </div>
                </div>

        </div>




</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
        <div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="exampleModalLabel">Not Delivered</h4>
				</div>


    {!! Form::model('', ['name' => 'ServicesForm', 'id' => 'ServicesForm', 'method' => 'POST','files' => true] ) !!}
        <div class="modal-body">
           <div class="form-group">

            {!! Form::label('package_id', 'Package Title:',['class' => 'control-label']) !!} {{ucfirst($ProductList['product_name'])}}
          </div>
          <div class="form-group">
            {!! Form::label('package_id', 'Package ID:',['class' => 'control-label']) !!} {{$ProductList['package_id']}}
          </div>
          <div class="form-group">
            {!! Form::label('return_address', 'Return Address:',['class' => 'control-label']) !!} {{$transporterDetail->ReturnAddress}}
          </div>

  <div class="row">
     <div class="col-sm-6">
        <div class="form-group" id="valid-error">
           <label class="controle-label">Rejected by:</label><br>
           <label class="radio-inline">
           <input type="radio" name="reject_by" value="transporter" checked onclick="return select_transporter()" style="margin-top:2px;"> Transporter
           </label>
           <label class="radio-inline">
           <input type="radio" name="reject_by" value="requester" onclick="return select_requester()" style="margin-top:2px;">&nbsp;Requester
           </label>
        </div>
     </div>
     <div class="col-sm-6">
        <div class="form-group" id="valid-error">
           <label class="controle-label">Return Reason:</label><br>
           <label class="radio-inline">
           <input type="radio" name="return_type" value="osreturn" <?php echo 'checked=="checked"'; ?> onclick="return select_os()" style="margin-top:2px;">&nbsp;OS Return
           </label>
           <span id="bycreturn" style="display:none;">
           <label class="radio-inline">
           <input type="radio" name="return_type" value="creturn" onclick="return select_bycreturn()" style="margin-top:2px;">&nbsp;Bycreturn
           </label>
           </span>
           <span id="ipayment">
           <label class="radio-inline">
           <input type="radio" name="return_type" value="ipayment" onclick="return select_ipayment()" style="margin-top:2px;">&nbsp;iPayment
           </label>
           </span>
        </div>
     </div>
  </div>



  <div class="form-group" id="msg_div">
  {!! Form::label('message', 'Message:',['class' => 'control-label']) !!}
  {!! Form::textarea('message','',['class'=>'form-control required', 'placeholder'=> 'Message','id' =>'message','maxlength' =>200,'size' =>'10x4']) !!}
  <p class="help-block red" id='er_message'></p>
  @if($errors->has('message'))
  <div class="error" style="color:red">{{ $errors->first('message') }}</div>
  @endif
  </div>
  <!-- <div class="form-group" id="tracking_div">
     {!! Form::label('tracking_number', 'Tracking Number:',['class' => 'control-label']) !!}
     {!! Form::text('tracking_number', '', ['class'=>'form-control required numeric', 'placeholder'=> 'Tracking Number','id' =>'tracking_number','maxlength' => 20]) !!}
     <p class="help-block red" id='er_tracking_number'></p>
     @if($errors->has('tracking_number'))
     <div class="error" style="color:red">{{ $errors->first('tracking_number') }}</div>
     @endif
  </div> -->
  <div class="form-group" id="date_div" style="">
     {!! Form::label('Date', 'Date:',['class' => 'control-label']) !!}
     {!!Form::text('date','',['class'=>'form-control required','readonly'=>'readonly','id'=>'date'])!!}
     <p class="help-block red" id='er_date'></p>
     @if($errors->has('date'))
     <div class="error" style="color:red">{{ $errors->first('date') }}</div>
     @endif
  </div>
  <div class="form-group" id="receipt_div">
     {!! Form::label('image', 'Upload Receipt:',['class' => 'control-label']) !!}
     {!! Form::file('image', '', ['class'=>'form-control', 'placeholder'=> '','id'=>'image','width'=>'250px','height'=>'250px'])!!}
     <p class="help-block red" id='er_image'></p>
     @if($errors->has('image'))
     <div class="error" style="color:red">{{ $errors->first('image') }}</div>
     @endif
  </div>
</div>
          <div class="">
        <div class="col-sm-2">
        <button type="submit" class="btn btn-default" id="cancel_delivery" >Submit</button>
        </div>
        <div class="col-sm-10">
        {!! Form::button('Close', ['class' => 'btn btn-default', 'data-dismiss'=>'modal']) !!}
         </div>
         <div class="clearfix"></div>
          </div>

      {!! Form::close() !!}
      <div class="clearfix"></div><br>
       </div>
        </div></div>

        <div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
      <div class="pre-modal-dialog modal-md">
        <div class="modal-content">
          <div class="modal-header popup-bg text-center">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <p class="popup-head color-white">Rate your experience with customer</p>

          </div>
          <div class="clearfix"></div>

        <center>
      <div class="modal-body text-left">

                  {!! Form::model('', ['name' => 'RequestReview', 'id' => 'RequestReview']) !!}
                      <div id="success-message-online"></div>
                    <div class="col-md-12" align="center">

              <div>
                      @if($user_data->Image != '')
                        <a class="fancybox" rel="group" href="{{ ImageUrl.$user_data->Image}}" >
                            <img src="{{ ImageUrl.$user_data->Image}}"  width="150px" height="150px" class="img-circle">
                          </a>
                      @else
                          <img src="{{ImageUrl}}/user-no-image.jpg" width="150px" height="150px" class="img-circle">
                      @endif

                    </br>
                    <div>
                    </br>
              <label class="rating_margin" >Rate Now :</label>

              &nbsp;&nbsp;&nbsp;

                          <div class="rateyo" id="rateyo" ></div>
                       <div class="clearfix"></div>
                        <div id="er_rating"  class="form-error"></div>
                       <input type="hidden" name="item_id" id="item_id" value='<?php echo $item_id; ?>'>
                       <input type="hidden" name="requester_id" id="requester_id" value='<?php echo $requester_id; ?>'>
            <input id="rating-value" name="rating"  type="hidden" value="">
            <br />
            <script type="text/javascript">
            $(function () {
            $(".rateyo").rateYo();

            $(".rateyo-readonly-widg").rateYo({


              numStars: 5,
              precision: 2,
              minValue: '',
              maxValue: 5,
              starWidth: "30px"

              })
            }).on("rateyo.change", function (e, data) {

                $('#rating-value').val(data.rating);
                if($('#rating-value').val() == 0){
                  $('#rating-value').val('');
                }
            });
            </script>


                    </div>
                    <div class="col-md-2"></div>
                    </div>


               <div class="col-sm-10  col-sm-offset-1" >

              <div class="form-group txt-area feedback-text">
                {!! Form::textarea('review','',['class'=>'form-control required', 'placeholder'=> 'Feedback','id' =>'review','maxlength' =>250,'size' =>'30x4']) !!}
                 <div id="er_review" class="help-block white"></div>
                 <div class="clearfix"></div>
                 <span id="review_msg"></span>

            </div>

                    </div>

                    <div class="clearfix"></div>
                     <div class="col-xs-12 text-right">
                         <div class="col-xs-6">
                       <div class="form-group">
                     <button type="submit" class="custom-btn1 btn-block" id='ratingButton'>SUBMIT</button>
                       <div class="error-msg"></div></div>
                    </div>
                    <div class="col-xs-6">
                       <div class="form-group text-center">
                        <button class="custom-btn1 btn-block" data-dismiss='modal' >NOT NOW</button>
                       </div>
                    </div>


                        </div>
                        </div>

                  {!! Form::close() !!}

          </div></center>
          <div class="clearfix"></div></center>

        </div>
      </div>
</div>







</div>





{!! Html::script('theme/web/js/validation.js') !!}
{!! Html::script('theme/web/js/utility.js') !!}
{!! Html::style('theme/date-time-picker/jquery.datetimepicker.css') !!}
{!! Html::script('theme/date-time-picker/jquery.datetimepicker.js') !!}


<script type="text/javascript">

new Validate({
	FormName : 'ServicesForm',
	ErrorLevel : 1,
	validateHidden: false,
	callback : function() {
    if(!$('#cancel_delivery').hasClass('spinning'))
    {
        $('#cancel_delivery').addClass('spinning');

        $.ajax
        ({
            type: "POST",
            url: "{{ url('/online_cancel_delivery') }}/{{Request::segment(3)}}",
            data: new FormData(document.getElementById('ServicesForm')),
            processData: false,
            contentType: false,
            dataType: "json",

            success: function(res)
            {
              alert(res.msg);
                $("#cancel_delivery").removeClass('spinning');
                if(res.success == 1) {
                    window.location.reload();
                }
            }
        });
    }

	}

});



	function select_os()
	{
		if($('[name="reject_by"]:checked').val() == "transporter")
		{
			$('#msg_div').show();
			$('#tracking_div').show();
			$('#receipt_div').show();
			$('#date_div').hide();
		}

		if($('[name="reject_by"]:checked').val() == "requester")
		{
			$('#msg_div').show();
			$('#tracking_div').show();
			$('#receipt_div').show();
			$('#date_div').hide();
		}
	}

	function select_ipayment()
	{
		if($('[name="reject_by"]:checked').val() == "transporter")
		{
			$('#msg_div').show();
			$('#tracking_div').hide();
			$('#receipt_div').hide();
			$('#date_div').hide();
		}
	}

	function select_requester()
	{
		if($('[name="return_type"]:checked').val() == "ipayment")
		{

			$('input:radio[name=return_type][value=osreturn]').click();
		}
		$("#bycreturn").show();
		$("#ipayment").hide();


	}
	function select_transporter()
	{
		if($('[name="return_type"]:checked').val() == "creturn")
		{

			$('input:radio[name=return_type][value=osreturn]').click();
		}
		$("#ipayment").show();
		$("#bycreturn").hide();
		$('#date_div').hide();


	}

	function select_bycreturn()
	{
		if($('[name="reject_by"]:checked').val() == "transporter")
		{
			$('#msg_div').show();
			$('#tracking_div').hide();
			$('#receipt_div').hide();
		}

		else if($('[name="reject_by"]:checked').val() == "requester")
		{
			$('#msg_div').show();
			$('#tracking_div').hide();
			$('#receipt_div').hide();
			$('#date_div').show();

		}


	}


	select_os();

 var newdate = new Date();
	 $('#date').datetimepicker({
       format:'M d, Y H:i:s A',
       minDate:newdate,
       onChangeDateTime:function( ct ){
           $('#date2').datetimepicker({  minDate:ct  })
       },
   });



  function online_request(type)
  {

      if(type == 'pickup')
      {
        $('#b_pickup').addClass('spinning');
        $('#b_pickup').prop('disabled', true);
        $('#b_cancel').prop('disabled', true);
      }

      if(type == 'delivery')
      {
        $('#b_delivery').addClass('spinning');
        $('#b_delivery').prop('disabled', true);
        $('#b_cancel').prop('disabled', true);
      }

      if(type == 'cancel')
      {
        $('#b_cancel').addClass('spinning');
        $('#b_cancel').prop('disabled', true);
      }


      $.ajax
      ({
      type: "POST",
      url: "{{ url('/transporter-online') }}/{{Request::segment(3)}}",
      data: "type="+type,

      success: function(res)
      {
        var obj = eval('('+res+')');
        if(type == 'pickup')
        {
          $('#b_pickup').removeClass('spinning');
          $('#b_cancel').prop('disabled',false);
        }

        if(type == 'delivery')
        {
          $('#b_delivery').removeClass('spinning');
          $('#b_cancel').prop('disabled',false);
        }

        if(type == 'cancel')
        {
          $('#b_cancel').removeClass('spinning');
        }


        if(obj.success == 1)
        {
          $('#success-message').html('<div role="alert" class="alert alert-success alert-dismissible fade in">'
          +'<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>'+obj.msg+'</div>');

          if(type == 'pickup')
          {
            $("#b_pickup").hide();
            $("#b_delivery").show();
          }

          if(type == 'delivery')
          {
            $("#b_delivery").hide();
            $("#b_deliver").show();
            $("#b_no_delivery").show();
            $('#b_cancel').hide();

          }

          if(type == 'cancel')
          {
            $('#b_cancel').hide();
            $("#b_delivery").hide();
            $("#b_pickup").hide();
          }

          $("#status_div").hide();
          $("#ajax_status").show();
          $("#ajax_status").html('<strong>Status: </strong>'+obj.status);




        }else {
           alert(obj.msg);
          window.location.replace("{{ url('/transporter') }}");
        }

      }
    });
      return false;

  }


new Validate({
  FormName : 'b_verify',
    ErrorLevel : 1,
    validateHidden: false,
    callback : function(){

            if(confirm('Are you sure the package was successfully delivered?'))
            {
                $("#b_verify_button").addClass('spinning');
                $('#b_verify_button').prop('disabled', true);
                $.ajax({
                  url     : '{{url("/online-verify")}}',
                  type    : 'post',
                  data    : "code="+$("#b_code").val()+"&item_id="+$('#request_id').val(),
                  success : function(res){
                      $("#b_verify_button").removeClass('spinning');
                      var obj = eval("("+res+")");
                      if(obj.success == 1)
                      {
                        $( "#b_deliver_model" ).trigger( "click" );
                        $( "#b_deliver_model" ).trigger( "reset" );
                        $('#success-message').html('<div role="alert" class="alert alert-success alert-dismissible fade in">'
                        +'<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>'+obj.msg+'</div>');


                        $("#b_deliver").hide();
                        $("#b_no_delivery").hide();
                        $("#feedback").show();

                        $("#status_div").hide();
                        $("#ajax_status").show();
                        $("#ajax_status").html('<strong>Status:</strong> Delivered');

                      }
                      alert(obj.msg);
                    }


                });
                return false;
            }


    }

})



new Validate({
  FormName : 'RequestReview',
  ErrorLevel : 1,
  validateHidden: false,
  callback : function(){

      if($rateYo.rateYo("rating") <= 0) {
        $('#er_rating').html('The rating field is required.');
        return false;
      }
      $("#ratingButton").addClass('spinning');
      $.ajax({
      url     : '{{url("/review-online")}}',
      type    : 'post',
      data    : "review="+$("#review").val()+"&item_id="+$('#item_id').val()+"&rating="+$('#rating-value').val()+"&requester_id="+$('#requester_id').val(),
      success : function(res){
        $("#ratingButton").removeClass('spinning');
            var obj = eval("("+res+")");
        if(obj.success == 1)
        {
            $("#feedback").hide();
            location.reload();
            $("#RequestReview").trigger( "reset" );
            $('#success-message-online').html('<div role="alert" class="alert alert-success alert-dismissible fade in">'
          +'<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>'+obj.msg+'</div>');
        }else{
            $("#modal2").trigger('click' );
            $("#feedback").hide();
            $('#success-message').html('<div role="alert" class="alert alert-danger alert-dismissible fade in">'
          +'<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>'+obj.msg+'</div>');
        }
      }
  });

    return false;
}

});

                   $(document).ready(function() {
                        $(".fancybox").fancybox();
                        });
                        var $rateYo = $("#rateyo").rateYo();


            $(function () {
            $(".rateyo").rateYo();

            $(".rateyo-readonly-widg").rateYo({


              numStars: 5,
              precision: 2,
              minValue: '',
              maxValue: 5,
              starWidth: "30px"

              })
            }).on("rateyo.change", function (e, data) {

                $('#rating-value').val(data.rating);
                if($('#rating-value').val() == 0){
                  $('#rating-value').val('');
                }
            });



</script>

<style>
.rating_margin
{
	transition: transform 0.8s cubic-bezier(0, 0.63, 0.41, 0.98) 0s, opacity 0.8s cubic-bezier(0, 0.63, 0.41, 0.98) 0s;
	transform: translateY(0px);
	opacity: 1;

}

.to-center{text-align:middle;}
</style>
@endsection
<?php

function change_unit($weight, $currentunit, $changeto)
{

//  echo $weight." ".$currentunit." ".$changeto; //die;

//  echo "<br>";

    $newweight = '';
    $newchangeto = $changeto;
    switch ($currentunit) {
        case 'kg':
            $newweight = $weight * 1000;
            break;
        case 'lbs':
            $newweight = $weight * 0.00220462;
            break;
        case 'inches':
            $newweight = $weight;
            break;
        case 'cm':
            $newweight = $weight * 0.393701;
            break;
    }

    switch ($changeto) {
        case 'kg':
            $newweight = $newweight / 1000;
            break;
        case 'lbs':
            $newweight = $newweight * 0.00220462;
            break;
        case 'cm':
            $newweight = $newweight / 0.393701;
            break;
        case 'inches':
            $newweight = $newweight;
            break;
    }

    return $newweight;

}

?>

