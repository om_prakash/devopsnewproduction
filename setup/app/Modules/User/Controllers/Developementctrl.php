<?php namespace App\Modules\User\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Models\Activitylog;
use App\Http\Models\Deliveryrequest;
use App\Http\Models\User;
use Input;
use Redirect;
use Session;
use App\Http\Models\Additem;

class Developementctrl extends Controller {
	public function __construct() {
		if (!session()->has('UserId')) {
			return Redirect::to('login')->send();
		}

		$user = User::where(["_id" => session()->get('UserId')])
			->where(['delete_status' => 'yes'])
			->first();
		if (count($user) > 0) {
			Session::flush();
			return Redirect::to('login');
		}
	}

	public function track_order_detail() {
		$query = Activitylog::query();
		$searchvalue = trim(Input::get('package_id'));
		// echo "searchvalue - ".$searchvalue;
		if ($searchvalue != "") {
			$data['package'] = Activitylog::where("package_id", "=", $searchvalue)
				->orWhere('PackageNumber', '=', $searchvalue)->orderby('EnterOn', 'desc')->get();

			// return $data['package'];
			// if (isset($data['package'][0])) {
				$where = [
					"ProductList" => ['$elemMatch' => [
						'package_id' => $searchvalue,
					]],
				];

				$data['date'] = Deliveryrequest::where($where)->select('_id', 'EnterOn', 'ExpectedDate', 'ProductList.$', 'RequestType', 'Status')->first();

				if(isset($data['date']['ProductList'][0]['status'])){
					$data['Activity'] = Activitylog::where(['package_id' => $searchvalue])->select('_id', 'EnterOn')
					->orderby('_id','desc')
					->first();
					$data['current_status'] = $data['date']['ProductList'][0]['status'];
					$data['RejectBy'] = @$data['date']['ProductList'][0]['RejectBy'];
					
					return view('User::detail.trackOrder', $data);
				}


			// } else {
			// 	$data['date'] = [];
			// }
		}

		return Redirect::back();
		
	}

	public function get_item_info($id){
		$response = array("success" => 0, "msg" => "Oops! Something went wrong.");
		if($id){
			$data = Additem::where(['_id'=>$id])->first();
			if(count($data) > 0){
				$response = array("success" => 1, "msg" => "result.",'result' => $data);
			}
		}
		return response()->json($response);
	}

	public function get_item_info2($rid,$id){
		$response = array("success" => 0, "msg" => "Oops! Something went wrong.");
		if($id != '' && $rid != ''){

			$request_info = Deliveryrequest::where(['_id'=>$rid])->select('ProductList')->first();
			if($request_info){
				$product_list = session::get(trim($rid));
				$data = [];
				foreach ($product_list as $key => $value) {
					if($value['_id'] == $id){
						$data = $value;
					}
				}
				if(empty($data)){
					$data = Additem::where(['_id'=>$id])->first();
				}

				if(count($data) > 0){

					$response = array("success" => 1, "msg" => "result.",'result' => $data);
				}
			}
			
		}
		return response()->json($response);
	}

}
