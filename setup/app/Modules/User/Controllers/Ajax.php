<?php namespace App\Modules\User\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Models\Additem;
use App\Http\Models\Address;
use App\Http\Models\CityStateCountry;
use App\Http\Models\Configuration;
use App\Http\Models\Deliveryrequest;
use App\Http\Models\Setting;
use App\Http\Models\User;
use App\Library\Promo;
use App\Library\Reqhelper;
use App\Library\Requesthelper;
use App\Library\Utility;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Input;
use MongoId;
use Redirect;
use Session;
use App\Http\Models\Extraregion;
use App\Http\Models\Currency;

class Ajax extends Controller
{
    public function prepare_request_calculation()
    {
        $response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];

        //lhwunit == length/height/width unit
        $lhwunit = 'cm';
        $weightunit = 'kg';
        if (Input::get('measurement_unit') == 'inches_lbs') {
            $lhwunit = 'inches';
            $weightunit = 'lbs';
        }

        $distance = Input::get('calculated_distance');

        $category = json_decode(Input::get('category'));
        if ($distance <= 0) {
            $distance = Utility::getDistanceBetweenPointsNew(Input::get('PickupLat'), Input::get('PickupLong'), Input::get('DeliveryLat'), Input::get('DeliveryLong'));
        }

        $address = ['lat' => Input::get('DeliveryLat'), 'lng' => Input::get('DeliveryLong')];
        
        $requesthelper = new Requesthelper(
            [
                "needInsurance" => Input::get('insurance'),
                "productQty" => (Input::get('quantity') < 1) ? 1 : Input::get('quantity'),
                "productCost" => Input::get('package_value'),
                "productWidth" => Input::get('width'),
                "productWidthUnit" => $lhwunit,
                "productHeight" => Input::get('height'),
                "productHeightUnit" => $lhwunit,
                "productLength" => Input::get('length'),
                "productLengthUnit" => $lhwunit,
                "productWeight" => Input::get('weight'),
                "productWeightUnit" => $weightunit,
                "productCategory" => @$category->name,
                "productCategoryId" => @$category->id,
                "distance" => $distance,
                "travelMode" => Input::get('travel_mode'),
                'currency' => Session::get('Default_Currency'),
            ]
        );

        $requesthelper->calculate();
        $data['calculationinfo'] = $requesthelper->get_information();

        $view = view('User::Buy.ajax.prepare-request-calculation', $data);
        if (!isset($data['calculationinfo']->error)) {
            $response['success'] = 1;
        }
        $response['html'] = $view->render();
        $response['total_amount'] = 0;
        $response['ghana_total_amount'] = 0;
        $response['shipping_cost'] = 0;
        $response['outside_accra_charge'] = 0;
        if (!isset($data['calculationinfo']->error)) {

            $response['shipping_cost'] = $data['calculationinfo']->shippingcost;
            $response['total_amount'] = $data['calculationinfo']->shippingcost + $data['calculationinfo']->insurance;
            $response['ghana_total_amount'] = $data['calculationinfo']->costInCurrency;
            $response['formated_currency'] = $data['calculationinfo']->formated_currency;

            $extraa_charge = $this->outsideOfAccraCharge($address);
            if ($extraa_charge != false) {
                $rate = Configuration::where(['_id' => '5673e33e6734c4f874685c84'])->select('accra_charge', 'accra_charge_type')->first();

                if ($rate) {
                    if ($rate->accra_charge_type == 'Percentage') {
                        $response['outside_accra_charge'] = ($response['total_amount'] * $rate->accra_charge) / 100;
                    } else {
                        $response['outside_accra_charge'] = $rate->accra_charge;
                    }

                }
                $response['total_amount'] = $response['total_amount'] + $response['outside_accra_charge'];
                $response['shipping_cost'] = $response['shipping_cost'] + $response['outside_accra_charge'];
            }

            //print_r($response['total_amount']); die;

        } else {
            $response['msg'] = $data['calculationinfo']->error;
        }

        return json_encode($response);

    }

    public function post_prepare_request()
    {
        $response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
        $users = User::find(Session()->get('UserId'));
        if (count($users) > 0) {
            $req = new Reqhelper();
            $res = $req->request_create($users);
            if ($res['success'] == 1) {
                $response['success'] = 1;
                $response['type'] = $res['type'];
                $request = Deliveryrequest::where(['_id' => $res['reqid']])->first();
                if ($res['type'] == 'update') {
                    $response['msg'] = "Your request has been updated, Please proceed to payment method.";

                } else {
                    $response['msg'] = "Your request has been created, Please proceed to payment method.";

                }
                $response['reqid'] = $res['reqid'];
            } else {
                $response['msg'] = $res['msg'];
            }
        }

        return json_encode($response);
    }

    public function edit_prepare_request()
    {

        $response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
        $req = new Reqhelper();
        $res = $req->request_edit();

        if ($res['success'] == 1) {
            $response['success'] = 1;
            $response['msg'] = "Your request has been updated successfully.";
            $response['reqid'] = $res['reqid'];
        } else {
            $response['msg'] = $res['msg'];
        }

        return json_encode($response);

    }

    public function local_edit_prepare_request()
    {

        $response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
        $req = new Reqhelper();
        $res = $req->request_edit();

        if ($res['success'] == 1) {
            $response['success'] = 1;
            $response['msg'] = "Your request has been updated successfully.";
            $response['reqid'] = $res['reqid'];
        } else {
            $response['msg'] = $res['msg'];
        }

        return json_encode($response);

    }

    public function validate_promocode()
    {
        $response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];

        $res = Promo::get_validate(Input::get('promocode'), Input::get('shipping_cost'), Input::get('total_amount'), Session::get('UserId'), Session::get('Default_Currency'));
        $response['msg'] = $res['msg'];
        if ($res['success'] == 1) {
            $response['success'] = 1;
            $response['result'] = $res;
        }

        return response()->json($response);

    }

    public function check_chat_user()
    {
        $response = array('success' => 0, 'msg' => 'Oops! You are not authorized.');

        $query = Deliveryrequest::query();

        $query->where(function ($query) {
            $query->where([
                'RequesterId' => new MongoId(Session::get("UserId")),
                'TransporterId' => new MongoId(Input::get('chat_to_id')),
            ]);
            $query->whereIn('Status', ['accepted', 'out_for_pickup', 'out_for_delivery']);
            return $query;
        });
        $query->orwhere(function ($query) {
            $query->where([
                'TransporterId' => new MongoId(Session::get("UserId")),
                'RequesterId' => new MongoId(Input::get('chat_to_id')),
            ]);
            $query->whereIn('Status', ['accepted', 'out_for_pickup', 'out_for_delivery']);
            return $query;
        });

        $query->orwhere(function ($query) {
            $query->where([
                'RequesterId' => new MongoId(Session::get("UserId")),
                //'ProductList.tpid' => Input::get('chat_to_id'),
            ]);
            $query->whereIn('ProductList.status', ['assign', 'accepted', 'out_for_pickup', 'out_for_delivery']);
            return $query;
        });
        $query->orwhere(function ($query) {
            $query->where([
                'RequesterId' => new MongoId(Input::get('chat_to_id')),
                //    'ProductList.tpid' => Session::get("UserId"),
            ]);
            $query->whereIn('ProductList.status', ['assign', 'accepted', 'out_for_pickup', 'out_for_delivery']);
            return $query;
        });
        if ($query->count() > 0) {
            $response['success'] = 1;
            $response['msg'] = '';
        }

        echo json_encode($response);
    }

    public function outsideOfAccraCharge($address)
    {

        if (isset($address->lat) && isset($address->lng)) {
            $lat = $address->lat;
            $lng = $address->lng;
        } else {
            $lat = $address['lat'];
            $lng = $address['lng'];
        }

        $distance = Utility::getDistanceBetweenPointsNew(5.5187188, -0.2046139, $lat, $lng);
        $charge = false;
        if ($distance != false && $distance > 0) {
            $distance_km = $distance / 1000;
            $range = 30;
            //print_r($address->lat); die;
            if ($distance_km > 30) {
                $charge = true;
            }
        }
        return $charge;
    }

     public function currency_conversion($shipcost,$currency_code) {

        $data = ['cost' => 0, 'GHS' => 0, 'PHP' => 0, 'GBP' => 0, 'CAD' => 0,'USD'=>0,'KES'=>0];
        $currency = Currency::where(['Status' => 'Active','CurrencyCode'=>$currency_code])->select('CurrencyRate', 'FormatedText', 'CurrencyCode')->get();

        if ($currency) {
            foreach ($currency as $key) {
                if ($key->CurrencyCode == 'GHS') {
                    $data['GHS'] = str_replace('[AMT]', number_format($shipcost * $key->CurrencyRate, 2), $key->FormatedText);
                } else if ($key->CurrencyCode == 'CAD') {
                    $data['CAD'] = str_replace('[AMT]', number_format($shipcost * $key->CurrencyRate, 2), $key->FormatedText);
                } else if ($key->CurrencyCode == 'PHP') {
                    $data['PHP'] = str_replace('[AMT]', number_format($shipcost * $key->CurrencyRate, 2), $key->FormatedText);
                } else if ($key->CurrencyCode == 'GBP') {
                    $data['GBP'] = str_replace('[AMT]', number_format($shipcost * $key->CurrencyRate, 2), $key->FormatedText);
                } else if ($key->CurrencyCode == 'USD') {
                    $data['USD'] = str_replace('[AMT]', number_format($shipcost * $key->CurrencyRate, 2), $key->FormatedText);
                } else if ($key->CurrencyCode == 'KES') {
                    $data['KES'] = str_replace('[AMT]', number_format($shipcost * $key->CurrencyRate, 2), $key->FormatedText);
                }
            }
        }
        return $data;

    }

    public function get_weight_kg($weight, $type) {
        $type = strtolower($type);
        if ($type == "kg") {
            return $weight;
        } else if ($type == "lbs") {
            return (number_format( $weight / 2.2046, 2) );
        } else {
            return $weight;
        }
    }

    public function get_weight_lbs($weight, $type) {
        $type = strtolower($type);
        if ($type == "kg") {
            return (number_format(((float) $weight) * 2.20462, 2));
        } else if ($type == "lbs") {
            return $weight;
        } else {
            return $weight;
        }
    }

    public function volumetricWeightInKg($array) {
        return ($this->get_size_in_cm($array['width'], $array['WHLUnit']) * $this->get_size_in_cm($array['height'], $array['WHLUnit']) * $this->get_size_in_cm($array['length'], $array['WHLUnit']));
    }

    public function volumetricWeightInLbs($array) {
        return ($this->getSizeInInches($array['width'], $array['WHLUnit']) * $this->getSizeInInches($array['height'], $array['WHLUnit']) * $this->getSizeInInches($array['length'], $array['WHLUnit']));
    }

    public function get_size_in_cm($height, $unit) {
        $in_cm = (float) $height;
        $unit = strtolower($unit);
        if ($unit == "inches") {
            $in_cm = $in_cm * 2.54;
        }
        return $in_cm;
    }

    public function getSizeInInches($whl, $unit) {
        $in_inches = (float) $whl;
        $unit = strtolower($unit);
        if ($unit == "cm") {
            $in_inches = $in_inches / 2.54;
        }
        return $in_inches;
    }

    public function online_purchase_calculation() {
        $userinfo = User::find(Session::get('UserId'));
        $user_currency = $userinfo->Default_Currency;
        $aqlat = Session::get('AqLat');
        $aqlong = Session::get('AqLong');

        $data = [
            'shippingcost' => 0,
            'insurance' => 0,
            'ghana_total_amount' => 0,
            'total_amount' => 0,
            'error' => [],
            'formated_text' => '',
            'distance_in_mile' => 0,
            'type' => 'online',
            'item_cost' => 0,
            'shipping_cost_by_user' => 0,
            'product_count' => 0,
            'ProcessingFees' => 0,
            'drop_address' => '',
            "totalWeightInKg" => 0,
            "totalWeightInLbs" => 0,
            "totalVolumeInKg" => 0,
            "totalVolumeInLbs" => 0
        ];

        $configurationdata = Configuration::where(['_id' => '5673e33e6734c4f874685c84'])->first();
        if (count($userinfo) > 0) {
            $address = json_decode(Input::get('address'));
            $response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
            $response['total_amount'] = 0;
            $response['ghana_total_amount'] = 0;
            $response['shipping_cost'] = 0;

            if (empty(trim(Input::get('request_id')))) {
                $data['product'] = Additem::where(array('user_id' => session()->get('UserId'), 'request_type' => 'online'))->get();
            } else {
                $data['product'] = session()->get(trim(Input::get('request_id')));
            }

            if (count($data['product']) > 0 && count($address) > 0) {
                $totalWeightInKg = 0;
                $totalWeightInLbs = 0;
                $totalVolumeInKg = 0;
                $totalVolumeInLbs = 0;
                
                foreach ($data["product"] as $value) {
                    $WHLUnit = "";
                    $measurementUnit = "";
                    $pWeightUnit = "";
                    $pWeight = 0;
                    $pWidth = 0;
                    $pHeight = 0;
                    $pLength = 0;
                    
                    if (Input::get("request_id")) {
                        $measurementUnit = $value["heightUnit"];
                        $pWeightUnit = $value["weight_unit"];
                        $pWeight = $value["weight"];
                        $pWidth = $value["width"];
                        $pHeight = $value["height"];
                        $pLength = $value["length"];
                    } else {
                        $measurementUnit = @$value->measurement_unit;
                        $pWeightUnit = @$value->weight_unit;
                        $pWeight = @$value->weight;
                        $pWidth = @$value->width;
                        $pHeight = @$value->height;
                        $pLength = @$value->length;
                    }

                    if ($measurementUnit == "cm_kg" || $measurementUnit == "cm") {
                        $WHLUnit = "cm";
                    }

                    if ($measurementUnit == "inches_lbs" || $measurementUnit == "inches") {
                        $WHLUnit = "inches";
                    }

                    $totalWeightInKg += $this->get_weight_kg($pWeight, $pWeightUnit);
                    $totalWeightInLbs += $this->get_weight_lbs($pWeight, $pWeightUnit);

                    $totalVolumeInKg += $this->volumetricWeightInKg(array(
                        "width" => $pWidth,
                        "WHLUnit" => $WHLUnit,
                        "height" => $pHeight,
                        "WHLUnit" => $WHLUnit,
                        "length" => $pLength,
                        "WHLUnit" => $WHLUnit
                    ));

                    $totalVolumeInLbs += $this->volumetricWeightInLbs(array(
                        "width" => $pWidth,
                        "WHLUnit" => $WHLUnit,
                        "height" => $pHeight,
                        "WHLUnit" => $WHLUnit,
                        "length" => $pLength,
                        "WHLUnit" => $WHLUnit
                    ));
                }
                
                $data['is_customs_duty_applied'] = false;
				
				if (@$address->country != "USA") {
					$data['is_customs_duty_applied'] = true;
				}

                $data['distance'] = Utility::GetDrivingDistance($aqlat, $aqlong, $address->lat, $address->lng);
                $data['drop_address'] = $address;

                if ($data['distance']->distance > 0) {
                    $data['consolidate_check'] = trim(Input::get('consolidate_check'));
                    $calculate = new NewCalculation(); 
                    $response = $calculate->getRate($data['product'], $data, $userinfo, @$address->country);

                    $response['distance'] = $data['distance'];

                    if ($response['success'] == 1) {
                        // $calculate = new NewCalculation(); 
                        // $prod = $data['product'][0];
                        // $prod['weight'] = $totalWeightInLbs;
                        // $prod['weight_unit'] = 'lbs';
                        // $data['product'] = [$prod];
                        // $response1 = $calculate->getRate([$prod], $data, $userinfo);

                        $response['outside_accra_charge'] = 0;
                        // Out side accra charges
                        $response['AreaCharges'] = $this->resionCharges(['name' => $address->state,'id'=>$address->state_id]);
                        
                        $response['ProcessingFees'] = ((floatval($response['total_amount']) * $configurationdata->aquantuoFees) / 100);
                        $total_item = count($data['product']);
                        $response['ProcessingFees_per_item'] = $response['ProcessingFees'] / $total_item;
                        $response['total_amount'] = $response['total_amount'] +$response['AreaCharges'];

                        $response['shippingCost'] = $response['shippingCost'];
                        $response['total_amount'] = $response['total_amount'];
                        $currency_conversion = $this->currency_conversion($response['total_amount'],trim($user_currency));
                        
                        $response['ghana_total_amount'] = $currency_conversion[trim($user_currency)];

                        $response['error'] = [];
                        $response["totalWeightInKg"] = $totalWeightInKg;
                        $response["totalWeightInLbs"] = $totalWeightInLbs;
                        $response["totalVolumeInKg"] = number_format($totalVolumeInKg, 2);
                        $response["totalVolumeInLbs"] = number_format($totalVolumeInLbs, 2);
                        
                        // Fetch currency rate.
						$currencyInfo = Currency::where(['Status' => 'Active'])->select('CurrencyRate', 'FormatedText', 'CurrencyCode')->get();
						$response['USDRate'] = 1;
						$response['GBPRate'] = $response['GHSRate'] = $response['CADRate'] = $response['KESRate'] = 0;

						foreach ($currencyInfo as $currencyVal) {
							if ($currencyVal->CurrencyCode == 'CAD') {
								$response['CADRate'] = $currencyVal->CurrencyRate;
							}

							if ($currencyVal->CurrencyCode == 'GHS') {
								$response['GHSRate'] = $currencyVal->CurrencyRate;
							}

							if ($currencyVal->CurrencyCode == 'GBP') {
								$response['GBPRate'] = $currencyVal->CurrencyRate;
							}
							if ($currencyVal->CurrencyCode == 'KES') {
								$response['KESRate'] = $currencyVal->CurrencyRate;
							}
						}
                        
                        $view = view('User::Buy.ajax.online_purchase', $response);
                        $response['html'] = $view->render();
                    }
                }
            }
        }
        return json_encode($response);
    }

    public function resionCharges($state) {
        $charges = Extraregion::where(['state_id'=> $state['id'] ,'status'=>'Active'])->select('amount')->first();
        if($charges){
            return $charges->amount;
        }else{
            return 0.00;
        }
    } 
    public function buyforme_calculation()
    {
        $userinfo = User::find(Session::get('UserId'));
        $aqlat = floatval(Session::get('AqLat'));
        $aqlong = floatval(Session::get('AqLong'));

        $data = ['shippingcost' => 0, 'insurance' => 0, 'ghana_total_amount' => 0, 'total_amount' => 0, 'error' => [], 'formated_text' => '', 'distance_in_mile' => 0, 'item_cost' => 0, 'shipping_cost_by_user' => 0];
        if (count($userinfo) > 0) {
            $address = json_decode(Input::get('address'));

            $response = ['success' => 0, 'msg' => 'Oops! Something went wrong.', 'shipping_cost' => 0];
            $response['total_amount'] = 0;
            $response['ghana_total_amount'] = 0;

            if (empty(trim(Input::get('request_id')))) {
                $data['product'] = Additem::where(array('user_id' => session()->get('UserId'), 'request_type' => 'buy_for_me'))->get();
            } else {
                $data['product'] = session()->get(trim(Input::get('request_id')));
            }

            if (count($data['product']) > 0 && count($address) > 0) {

                $data['distance'] = Utility::GetDrivingDistance($aqlat, $aqlong, $address->lat, $address->lng);

                if ($data['distance']->distance > 0) {

                    // Applying new charges

                    foreach ($data['product'] as $key => $value) {

                    }
                    // End new changes
                    foreach ($data['product'] as $key => $value) {

                        if ($value['shipping_cost_by_user'] == 0) {
                            $value['shipping_cost_by_user'] = 0;
                        } elseif ($value['shipping_cost_by_user'] < 5) {
                            $value['shipping_cost_by_user'] = 5;
                        }

                        $lhwunit = 'cm';
                        $weightunit = 'kg';
                        if ($value['weight_unit'] == 'lbs') {
                            $lhwunit = 'inches';
                            $weightunit = 'lbs';
                        }

                        $requesthelper = new Requesthelper(
                            [
                                "needInsurance" => @$value['insurance_status'],
                                "productQty" => ($value['qty'] < 1) ? 1 : $value['qty'],
                                "productCost" => $value['price'],
                                "productWidth" => $value['width'],
                                "productWidthUnit" => $lhwunit,
                                "productHeight" => $value['height'],
                                "productHeightUnit" => $lhwunit,
                                "productLength" => $value['length'],
                                "productLengthUnit" => $lhwunit,
                                "productWeight" => $value['weight'],
                                "productWeightUnit" => $weightunit,
                                "productCategory" => @$value['category'],
                                "productCategoryId" => @$value['categoryid'],
                                "distance" => $data['distance']->distance,
                                "travelMode" => $value['travelMode'],
                            ]
                        );

                        $requesthelper->calculate();
                        $calculationinfo = $requesthelper->get_information();

                        if (isset($calculationinfo->error)) {
                            $response['success'] = 0;
                            $response['msg'] = $calculationinfo->error;
                            return json_encode($response);
                            die;
                        }

                        $data['ProcessingFees'] = Utility::get_processing_fee();

                        $data['item_cost'] += ($value['price'] * $value['qty']);
                        $data['shippingcost'] += $calculationinfo->shippingcost;
                        $data['insurance'] += $calculationinfo->insurance;
                        $data['shipping_cost_by_user'] += $value['shipping_cost_by_user'];

                        $response['shipping_cost'] = $data['shippingcost'];
                        $response['total_amount'] = $data['shippingcost'] + $data['insurance'] + $data['item_cost'] + $value['shipping_cost_by_user'] + $data['ProcessingFees'];
                        $data['total_amount'] = $response['total_amount'];
                        $response['ghana_total_amount'] = 0;
                        $response['distance'] = $data['distance']->distance;
                        $data['distance_in_mile'] = $calculationinfo->distance;

                        $data['product'][$key]['shipping_cost'] = $calculationinfo->shippingcost;
                    }
                    $response['success'] = 1;
                    $response['product'] = $data['product'];
                }

                $currency = CityStateCountry::where(['CurrencyCode' => $userinfo->Default_Currency])->first();

                if (count($currency) > 0) {
                    $response['ghana_total_amount'] = $response['total_amount'] * $currency->CurrencyRate;
                    $data['ghana_total_amount'] = $response['ghana_total_amount'];
                    $data['formated_text'] = $currency->FormatedText;
                }

                $view = view('User::Buy.ajax.buyforme_calculation', $data);

                $response['html'] = $view->render();

            }
        }

        return json_encode($response);

    }

    public function transporter_edit_profile(Request $request)
    {
        $v = Validator::make($request->all(), [
            'first_name' => 'required|Min:3|Max:80',
            'last_name' => 'required',
            'phone_number' => 'required|Numeric',
            'user_image' => 'mimes:jpeg,jpg,png',
            'address' => 'required',
            'country_code' => 'required',
        ]);

        if ($v->fails()) {
            return Redirect::to("become-transporter")->withErrors($v)->withInput(Input::get());
        } else {
            $user_info = User::find(Session::get('UserId'));
            if (count($user_info) > 0) {
                $zipcode_get = Input::get('zipcode');
                if ($user_info->user_address_id != '') {
                    Address::where(['_id' => $user_info->user_address_id])->delete();
                }
                $add_user_address = $this->add_user_address(Input::get('address'), json_decode(Input::get('country')), json_decode(Input::get('state')), json_decode(Input::get('city')), $zipcode_get);
                if ($add_user_address != 1) {
                    return Redirect::to("become-transporter")->with('danger', 'We are unable to find your location. Please  correct it.');
                }

                $obj_source_country = json_decode(Input::get('country'));
                $obj_desti_country = json_decode(Input::get('state'));
                $obj_source_city = json_decode(Input::get('city'));

                $user_info->FirstName = ucfirst(Input::get('first_name'));
                $user_info->LastName = ucfirst(Input::get('last_name'));
                $user_info->Name = trim($user_info->FirstName) . ' ' . trim($user_info->LastName);
                $user_info->FirstName = Input::get('first_name');
                $user_info->LastName = Input::get('last_name');
                $user_info->PhoneNo = Input::get('phone_number');
                $user_info->AlternatePhoneNo = Input::get('alternate_phone_number');
                $user_info->AlternateCCode = Input::get('alternate_country_code');
                $user_info->Street1 = Input::get('address');
                $user_info->Street2 = Input::get('address2');
                $user_info->ZipCode = Input::get('zipcode');
                $user_info->SSN = Input::get('ssn');
                $user_info->type_of_id = Input::get('type_of_id');
                $user_info->CountryCode = Input::get('country_code');
                $user_info->BusinessName = Input::get('business_name');
                $user_info->TransporterType = 'individual';
                $user_info->UserType = 'both';
                $user_info->Country = $obj_source_country->name;
                $user_info->State = @$obj_desti_country->name;
                $user_info->City = $obj_source_city->name;

                if (Input::file('user_image')) {

                    $extension = Input::file('user_image')->getClientOriginalExtension();
                    $fileName = 'profile_' . time() . '.' . $extension; // Rename img image
                    if (Input::file('user_image')->move(BASEURL_FILE . 'profile/', $fileName)) {
                        if (isset($user_info->Image) && ($user_info->Image) != "") {
                            if (file_exists(BASEURL_FILE . $user_info->Image)) {
                                unlink(BASEURL_FILE . $user_info->Image);
                            }
                        }
                        $user_info->Image = "profile/$fileName";
                        Session::set('Image', $user_info->Image);
                    }
                }

                if (Input::file('license_id')) {
                    $extension = Input::file('license_id')->getClientOriginalExtension(); // getting image extension

                    $fileName1 = time() . '.' . $extension; // Rename img image
                    if (Input::file('license_id')->move(BASEURL_FILE . 'profile/', $fileName1)) {

                        if (isset($user_info->LicenceId) && ($user_info->LicenceId) != "") {
                            if (file_exists(BASEURL_FILE . $user_info->LicenceId)) {
                                unlink(BASEURL_FILE . $user_info->LicenceId);
                            }
                        }
                        $user_info->LicenceId = "profile/$fileName1";
                    }
                }

                if (Input::file('business_license_id')) {
                    $extension = Input::file('business_license_id')->getClientOriginalExtension(); // getting image extension

                    $fileName = time() . '.' . $extension; // Rename img image
                    if (Input::file('business_license_id')->move(BASEURL_FILE . 'profile/', $fileName)) {

                        if (isset($user_info->IDProof) && ($user_info->IDProof) != "") {
                            if (file_exists(BASEURL_FILE . $user_info->IDProof)) {
                                unlink(BASEURL_FILE . $user_info->IDProof);
                            }
                        }
                        $user_info->IDProof = "profile/$fileName";
                    }
                }

                Session::put('Usertype', $user_info->UserType);
                Session::put('TransporterType', $user_info->TransporterType);
                Session::put('Profile_Status', $user_info->ProfileStatus);
                Session::put('ShowName', $user_info->Name);

                $user_info->ProfileStatus = "step-third";

                $user_info->save();

                $supportemail = Setting::find('563b0e31e4b03271a097e1ca');

                if ($user_info->_id) {
                    // email to user
                    send_mail('55d5a0be6734c4fb378b4567', [
                        'to' => trim(Input::get('email')),
                        'replace' => [
                            '[USERNAME]' => $user_info->Name,
                        ],
                    ]);

                    // email to admin
                    if (count($supportemail) > 0) {
                        send_mail('565fd3e3e4b076e7a2a176f5', [
                            'to' => $supportemail->RegEmail,
                            'replace' => [
                                '[USERNAME]' => $user_info->Name,
                                '[EMAIL]' => $user_info->Email,
                            ],
                        ]);
                    }

                    $response = array("success" => 1, "msg" => "Registered successfully.");
                }
                Session::put('Profile_Status', $user_info->ProfileStatus);
                return Redirect::to("edit-bank-information")
                    ->withSuccess('Success! Profile has been updated successfully.');
            }

        }
    }

    public function post_transporter_edit_profile(Request $request) // transporter

    {

        $user_info = User::find(Session::get('UserId'));
        $v = Validator::make($request->all(), [
            'business_name' => 'required|Min:3|Max:80',
            'VatTaxNo' => 'required|Min:3|Max:80',
            'phone_number' => 'required|Numeric',
            'country_code' => 'required',
            'user_image' => 'mimes:jpeg,jpg,png',
            'address' => 'required',
            'country_code' => 'required|Numeric',
        ]);

        if ($v->fails()) {
            return Redirect::to("become-transporter")->withErrors($v)->withInput(Input::get());
        } else {

            // print_r(Input::file('business_license_id'));die;
            $zipcode_get = Input::get('zipcode');
            if ($user_info->user_address_id != '') {
                Address::where(['_id' => $user_info->user_address_id])->delete();
            }
            $add_user_address = $this->add_user_address(Input::get('address'), json_decode(Input::get('country')), json_decode(Input::get('state')), json_decode(Input::get('city')), $zipcode_get);
            if ($add_user_address != 1) {
                return Redirect::to("become-transporter")->with('danger', 'We are unable to find your location. Please  correct it.');
            }

            $obj_source_country = json_decode(Input::get('country'));
            $obj_desti_country = json_decode(Input::get('state'));
            $obj_source_city = json_decode(Input::get('city'));

            $user_info->FirstName = ucfirst(Input::get('first_name'));
            $user_info->LastName = ucfirst(Input::get('last_name'));
            $user_info->Name = $user_info->FirstName . ' ' . $user_info->LastName;
            $user_info->BusinessName = Input::get('business_name');
            $user_info->VatTaxNo = Input::get('VatTaxNo');
            $user_info->PhoneNo = Input::get('phone_number');
            $user_info->AlternatePhoneNo = Input::get('alternate_phone_number');
            $user_info->AlternateCCode = Input::get('alternate_country_code');
            $user_info->Street1 = Input::get('address');
            $user_info->Street2 = Input::get('address2');
            $user_info->ZipCode = Input::get('zipcode');
            $user_info->SSN = Input::get('ssn');
            $user_info->CountryCode = Input::get('country_code');
            $user_info->BusinessName = Input::get('business_name');
            $user_info->type_of_id = Input::get('type_of_id');
            $user_info->TransporterType = 'business';
            $user_info->UserType = 'both';
            $user_info->Country = @$obj_source_country->name;
            $user_info->State = @$obj_desti_country->name;
            $user_info->City = @$obj_source_city->name;

            if (Input::file('user_image')) {
                $extension = Input::file('user_image')->getClientOriginalExtension();
                $fileName = rand(100, 999) . time() . '.' . $extension; // Rename img i
                if (Input::file('user_image')->move(BASEURL_FILE . 'profile/', $fileName)) {
                    if (isset($user_info->Image) && ($user_info->Image) != "") {
                        if (file_exists(BASEURL_FILE . $user_info->Image)) {
                            unlink(BASEURL_FILE . $user_info->Image);
                        }
                    }
                    $user_info->Image = "profile/$fileName";
                    Session::set('Image', $user_info->Image);
                }
            }

            if (Input::file('license_id') != '') {
                $extension = Input::file('license_id')->getClientOriginalExtension(); // getting image extension

                $fileName = rand(100, 999) . time() . '.' . $extension; // Rename img image
                if (Input::file('license_id')->move(BASEURL_FILE . 'profile/', $fileName)) {

                    if (isset($user_info->LicenceId) && ($user_info->LicenceId) != "") {
                        if (file_exists(BASEURL_FILE . $user_info->LicenceId)) {
                            unlink(BASEURL_FILE . $user_info->LicenceId);
                        }
                    }
                    $user_info->LicenceId = "profile/$fileName";
                }
            }

            if (Input::file('business_license_id') != '') {
                $extension = Input::file('business_license_id')->getClientOriginalExtension(); // getting image extension

                $fileName = rand(100, 999) . time() . '.' . $extension; // Rename img image
                if (Input::file('business_license_id')->move(BASEURL_FILE . 'profile/', $fileName)) {

                    if (isset($user_info->IDProof) && ($user_info->IDProof) != "") {
                        if (file_exists(BASEURL_FILE . $user_info->IDProof)) {
                            unlink(BASEURL_FILE . $user_info->IDProof);
                        }
                    }
                    $user_info->IDProof = "profile/$fileName";
                }
            }
            $user_info->ProfileStatus = 'step-third';

            Session::put('Usertype', $user_info->UserType);
            Session::put('TransporterType', $user_info->TransporterType);
            Session::put('Profile_Status', $user_info->ProfileStatus);
            Session::put('ShowName', $user_info->Name);
            if ($user_info->TransporterType == 'business') {
                if (!empty(trim($user_info->BusinessName))) {
                    Session()->put('ShowName', $user_info->BusinessName);
                }
            }
            $supportemail = Setting::find('563b0e31e4b03271a097e1ca');
            if ($user_info->save()) {
                // email to admin
                /*if (count($supportemail) > 0) {
                send_mail('56ab567c5509251cd67773f4', [
                'to' => $supportemail->RegEmail,
                'replace' => [
                '[USERNAME]' => $user_info->Name,
                '[EMAIL]' => $user_info->Email,
                '[USERTYPE]'=>$user_info->UserType,
                '[PHONENO]'=>$user_info->PhoneNo,
                '[ALTERNATEPHONENO]'=>$user_info->AlternatePhoneNo,
                '[BUSINESSNAME]'=>$user_info->BusinessName,
                '[STREET1]'=>$user_info->Street1,
                '[STREET2]'=>$user_info->Street2,
                '[COUNTRY]'=>$user_info->Country,
                '[STATE]'=>$user_info->State,
                '[CITY]'=>$user_info->City,
                '[ZIPCODE]'=>$user_info->ZipCode,

                ],
                ]);

                }*/
                return Redirect::to("edit-bank-information")->withSuccess('Success! Profile has been updated successfully.');
            }

        }
    }

    public function add_user_address($address, $country, $state, $city, $zip)
    {

        $addlat = true;
        $lat = '';
        $lng = '';

        $addlat = Utility::getLatLong($address,
            Input::get('address_line_2'),
            $city->name,
            @$state->name,
            $country->name
        );

        if ($addlat != false) {
            $lat = $addlat['lat'];
            $lng = $addlat['lng'];
        } elseif ($addlat == false) {
            $addlat = Utility::getLatLong(
                $city->name,
                @$state->name,
                $country->name
            );
        }

        if ($addlat == false) {
            $addlat == true;
        }

        if ($addlat) {

            $insert = [
                'country' => $country->name,
                'country_id' => $country->id,
                'state' => @$state->name,
                'state_id' => @$state->id,
                'city' => $city->name,
                'city_id' => $city->id,
                'state_available' => $country->state_available,
                'address_line_1' => $address,
                'address_line_2' => Input::get('address_line_2'),
                'zipcode' => $zip,
                'user_id' => Session::get('UserId'),
                'lat' => $lat,
                'lng' => $lng,
            ];

            $insert_data = Address::insertGetId($insert);
            $user_info = User::find(Session::get('UserId'));
            $user_info->user_address_id = (string) $insert_data;
            $user_info->save();
            $addlat = true;

        }
        return $addlat;

    }

    public function buy_for_me_requester_review(Request $request)
    {
        $response = array("success" => 0, "msg" => "Oops! Something went wrong.");

        $where = array("ProductList" => array('$elemMatch' => array('_id' => $request->get('item_id'), 'status' => 'delivered')));
        $delivery_info = Deliveryrequest::where($where)->select('ProductList.$')->first();

        if (count($delivery_info) > 0) {
            foreach ($delivery_info->ProductList as $value) {
                $RequesterFeedbcak = $value['RequesterFeedbcak'];
                $RequesterRating = $value['RequesterRating'];
            }

            if ($RequesterRating == '' && $RequesterFeedbcak == '') {
                $update['RequesterRating'] = $request->get('rating');
                $update['RequesterFeedbcak'] = $request->get('review');

                $update = [
                    'ProductList.$.RequesterFeedbcak' => $request->get('review'),
                    'ProductList.$.RequesterRating' => $request->get('rating'),

                ];

                $updateData = Deliveryrequest::where($where)->update($update);



                $requester_rating = User::find($request->get('tpid'));
                //echo '<pre>';

                //print_r($request->get('tpid')); die;
                $requester_rating->RatingCount = $requester_rating->RatingCount + $request->get('rating');
                $requester_rating->RatingByCount = $requester_rating->RatingByCount + 1;
                $requester_rating->save();

                if (count($updateData) > 0 && count($requester_rating) > 0) {
                    $response = array("success" => 1, "msg" => "Review has been given successfully.");
                }
                if (count($updateData) > 0) {
                    $response = array(
                        "success" => 1,
                        "msg" => "Thanks for your feedback.",
                    );
                }
            }

        }

        echo json_encode($response);
    }

    public function online_requester_review(Request $request)
    {
        $response = array("success" => 0, "msg" => "Oops! Something went wrong.");

        $where = array("ProductList" => array('$elemMatch' => array('_id' => $request->get('item_id'), 'status' => 'delivered')));
        $delivery_info = Deliveryrequest::where($where)->select('ProductList.$')->first();
        if (count($delivery_info) > 0) {
            foreach ($delivery_info->ProductList as $value) {
                $RequesterFeedbcak = $value['RequesterFeedbcak'];
                $RequesterRating = $value['RequesterRating'];
            }

            if ($RequesterRating == '' && $RequesterFeedbcak == '') {
                $update['RequesterRating'] = $request->get('rating');
                $update['RequesterFeedbcak'] = $request->get('review');

                $update = [
                    'ProductList.$.RequesterFeedbcak' => $request->get('review'),
                    'ProductList.$.RequesterRating' => $request->get('rating'),

                ];

                $requester_rating = User::find($request->get('tpid'));
                $requester_rating->RatingCount = $requester_rating->RatingCount + $request->get('rating');
                $requester_rating->RatingByCount = $requester_rating->RatingByCount + 1;

                $updateData = Deliveryrequest::where($where)->update($update);

                $requester_rating->save();

                if (count($updateData) > 0) {
                    $response = array(
                        "success" => 1,
                        "msg" => "Thanks for your feedback.",
                    );
                }
            }

        }

        echo json_encode($response);
    }

    public function requester_review(Request $request)
    {

        $response = array("success" => 0, "msg" => "Something went wrong.");

        $update = Deliveryrequest::where(array('_id' => new MongoId($request->get('request_id'))))->first();
        if (count($update) > 0) {
            if ($update->RequesterRating == '' && $update->RequesterFeedbcak == '') {

                $update->RequesterRating = $request->get('rating');
                $update->RequesterFeedbcak = $request->get('review');

                $update->save();

                $requester_rating = User::find($request->get('transporter_id'));
                $requester_rating->RatingCount = $requester_rating->RatingCount + $request->get('rating');
                $requester_rating->RatingByCount = $requester_rating->RatingByCount + 1;
                $requester_rating->save();

                if (count($update) > 0) {
                    $response = array("success" => 1, "msg" => "Review has been given successfully.");
                }
            }
        }

        echo json_encode($response);
    }

    public function update_information()
    {
        $response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
        $validator = Validator::make(Input::get(), array(
            'DeviceId' => 'required',
            'DeviceType' => 'required',
            'NotificationId' => 'required',
        ));
        if ($validator->fails()) {
            $response['field'] = $validator->errors();
            $response['msg'] = 'Validation error';
        } else {
            $result = User::find(Session()->get('UserId'));
            if (count($result) > 0) {
                $result->NotificationId = trim(Input::get('NotificationId'));

                $result->save();
                $response = [
                    'success' => 1,
                    'msg' => 'User information has been updated successfully.',
                    'result' => $result,
                ];
            }
        }
    }

    public function need_package_material()
    {
        $response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
        $info = Deliveryrequest::where([
            '_id' => Input::get('id'),
            'TransporterId' => new MongoId(Session::get('UserId')),
        ])->first();
        if (count($info) > 0) {
            $info->PackageMaterialShipped = (Input::get('status') == 'yes') ? 'yes' : 'not_applicable';
            $info->save();
            $response = ['success' => 1, 'msg' => '', 'status' => $info->PackageMaterialShipped];
        }

        echo json_encode($response);
    }

}
