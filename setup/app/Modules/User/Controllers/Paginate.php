<?php namespace App\Modules\User\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Models\Deliveryrequest;
use App\Http\Models\FAQ;
use App\Http\Models\Notification;
use App\Http\Models\Trips;
use App\Http\Models\User;
use DateTime;
use Input;
use MongoDate;
use MongoId;
use MongoRegex;
use Session;
use DB;

class Paginate extends Controller {
	public function request_list() {

		$query = Deliveryrequest::query();
		$data = $this->common_page();
		$search_drop = Input::get('search_date');
		$session_id = Session()->get('UserId');

		$query->where('RequesterId', '=', new MongoId(Session()->get('UserId')));

		if (Input::get('list') == 'new') {
			$query->whereIn('Status', array('pending', 'momo_initiated'));
		} else if (Input::get('list') == 'current') {
			$query->whereIn('Status', array('accepted','ready', 'out_for_pickup', 'purchased', 'out_for_delivery', 'assign', 'reviewed', 'not_purchased', 'paid', 'item_received'));
		} else if (Input::get('list') == 'completed') {
			$query->whereIn('Status', array('delivered', 'cancel', 'cancel_by_admin'));

		}

		if (Input::get('search') != '') {

			$query->where('ProductTitle', 'like', '%' . trim(Input::get('search')) . '%');
		}

		$Status = Input::get('Status');
		if (Input::get('Status') != '') {

			$query->where('Status', '=', $Status);
		}
		if (Input::get('search_date') != '') {
			$startDate = Input::get('search_date');
			$query->where('EnterOn', '>=', new MongoDate(strtotime(DateTime::createFromFormat('M d, Y', $startDate)->format('d-m-Y 00:00:00'))));
		}

		if (Input::get('search_date2') != '') {
			$endDate = Input::get('search_date2');
			$query->where('EnterOn', '<=', new MongoDate(strtotime(DateTime::createFromFormat('M d, Y', $endDate)->format('d-m-Y 23:59:59'))));
		}

		$data['count'] = $query->count();
		$data['requests'] = $query->skip($data['start'])->take($data['per_page'])
			->orderBy('_id', 'desc')->select('_id', 'RequesterName', 'PickupFullAddress', 'DeliveryFullAddress', 'RequestType', 'length', 'DeliveryDate', 'Status', 'ProductTitle', 'EnterOn', 'PickupLatLong', 'DeliveryLatLong', 'PickupDate', 'ProductList', 'ProductImage', 'image', 'TripId', 'TransporterId', 'TransporterName', 'DeliveredTime')
			->get();

		$data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;
		$data['list'] = Input::get('list');
		return view('User::list.pagination.my-request', $data);

	}

	public function request_list2() {
		$query = Deliveryrequest::query();
		$data = $this->common_page();
		$search_drop = Input::get('search_date');
		$session_id = Session()->get('UserId');

		$query->where('RequesterId', '=', new MongoId(Session()->get('UserId')));
		if (Input::get('list') == 'new') {
			$query->whereIn('Status', array('pending', 'momo_initiated'));
		} else if (Input::get('list') == 'current') {
			$query->whereIn('Status', array('accepted', 'ready','out_for_pickup', 'purchased', 'out_for_delivery', 'assign', 'reviewed', 'not_purchased', 'paid', 'item_received', 'ready'));
		} else if (Input::get('list') == 'completed') {
			$query->whereIn('Status', array('delivered', 'cancel', 'cancel_by_admin'));

		}

		if (Input::get('search') != '') {

			$query->where('ProductTitle', 'like', '%' . trim(Input::get('search')) . '%');
		}

		$Status = Input::get('Status');
		if (Input::get('Status') != '') {

			$query->where('Status', '=', $Status);
		}
		if (Input::get('search_date') != '') {
			$startDate = Input::get('search_date');
			$query->where('EnterOn', '>=', new MongoDate(strtotime(DateTime::createFromFormat('M d, Y', $startDate)->format('d-m-Y 00:00:00'))));
		}

		if (Input::get('search_date2') != '') {
			$endDate = Input::get('search_date2');
			$query->where('EnterOn', '<=', new MongoDate(strtotime(DateTime::createFromFormat('M d, Y', $endDate)->format('d-m-Y 23:59:59'))));
		}

		$data['count'] = $query->count();
		$data['requests'] = $query->skip($data['start'])->take($data['per_page'])
			->orderBy('_id', 'desc')->select('_id', 'RequesterName', 'PickupFullAddress', 'DeliveryFullAddress', 'RequestType', 'length', 'DeliveryDate', 'Status', 'ProductTitle', 'EnterOn', 'PickupLatLong', 'DeliveryLatLong', 'PickupDate', 'ProductList', 'ProductImage', 'image', 'TripId', 'TransporterId', 'TransporterName', 'DeliveredTime')
			->get();

		$data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;
		$data['list'] = Input::get('list');
		//echo "<pre>"; print_r($data);  die;
		return view('User::list.pagination.new_development.my-request', $data);

	}

	public function my_deliveries_old() {
		$query = Deliveryrequest::query();
		$data = $this->common_page();
		$search_drop = Input::get('search_date');
		/*	$data['requests3'] = $query->skip($data['start'])->take($data['per_page'])
				->where(['RequestType' => 'delivery', 'request_version' => 'old'])
				->orderBy('_id', 'desc')->get();

			echo "<pre>";
		*/

		if (Input::get('list') == 'current') {
			$status = ['ready', 'accepted', 'out_for_pickup', 'out_for_delivery', 'assign'];
		} else {
			$status = ['delivered', 'cancel'];
		}

		//$query->orwhere(['TransporterId' => new MongoId(Session()->get('UserId')), 'RequestType' => 'delivery'])
			//->whereIn('Status', $status);


		$query->orwhere(function ($query) use ($status) {
			//'RequestType' => 'delivery'
			$query->where(['TransporterId' => new MongoId(Session()->get('UserId'))])
			->whereIn('Status', $status)->whereIn('RequestType', 'local_delivery');
			if (trim(Input::get('search'))) {
				 
				$query->where('RequesterName', 'like', '%' . trim(Input::get('search')) . '%');
			}
			if (Input::get('Status') != '') {
				$query->where('Status', '=', trim(Input::get('Status')));
			}
		});

		$query->orwhere(function ($query) use ($status) {

			$query->whereIn('ProductList.status', $status);
			if (Input::get('Status') != '') {
				$query->where('ProductList.status', '=', trim(Input::get('Status')));
			}

			
			if (trim(Input::get('search'))) {
				$query->where('RequesterName', 'like', '%' . trim(Input::get('search')) . '%');
			}
			return $query->where('ProductList.tpid', Session()->get('UserId'));

		});

		/*$query->orwhere(function ($query) use ($status) {

			$query->whereIn('ProductList.status', $status);
			if (Input::get('Status') != '') {
				$query->where('ProductList.status', '=', trim(Input::get('Status')));
			}

			if (trim(Input::get('search'))) {
				$query->where('RequesterName', 'like', '%' . trim(Input::get('search')) . '%');
			}
			return $query->where('ProductList.tpid', Session()->get('UserId'));

		});*/

		if (Input::get('search_date') != '') {

			$startDate = Input::get('search_date');
			$query->where('EnterOn', '>=', new MongoDate(strtotime(DateTime::createFromFormat('M d, Y', $startDate)->format('d-m-Y 00:00:00'))));
		}

		if (Input::get('search_date2') != '') {
			$endDate = Input::get('search_date2');
			$query->where('EnterOn', '<=', new MongoDate(strtotime(DateTime::createFromFormat('M d, Y', $endDate)->format('d-m-Y 23:59:59'))));
		}

		$data['count'] = $query->count();

		$data['requests'] = $query->skip($data['start'])->take($data['per_page'])
			->select('PickupFullAddress', 'DeliveryFullAddress', 'PickupDate', 'EnterOn', 'ProductTitle', 'Status', 'RequestType', 'ProductList.DeliveredDate', 'ProductList.product_name', 'ProductList.image', 'ProductList.status', 'ProductList._id', 'ProductList.tpid', 'ProductList.ProductImage', 'DeliveryDate', 'RequesterId', 'RequesterName', 'ProductImage', 'PickupLatLong', 'DeliveryLatLong', 'DeliveredTime', 'ProductList.ExpectedDate')
		//->orwhere(['TransporterId' => new MongoId(Session()->get('UserId')), 'RequestType' => 'delivery'])
			->orderBy('_id', 'desc')->get();
			//echo '<pre>';

		//print_r($data['requests']); die;

		if(Input::get('Status') != '' ){
			foreach ($data['requests'] as $key ) {

				if($key->RequestType == 'delivery'){
					if(isset($key->ProductList)){
						foreach ($key->ProductList as  $value) {
							if($value['status'] != Input::get('Status')){
								unset($value);
							}
						}
					}else{
						if($key->Status != Input::get('Status')){
							unset($key);
						}
					}
				}else{
					if(isset($key->ProductList)){
						foreach ($key->ProductList as  $value) {
							if($value['status'] != Input::get('Status')){
								unset($value);
							}
						}
					}
				}	

				
			}

			
		}

		$data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;
		$data['list'] = Input::get('list');
		return view('User::list.pagination.my_deliveries', $data);
		//return view('User::list.pagination.my_deliveries2', $data);

	}
	public function my_deliveries() {
		$query = Deliveryrequest::query();
		$data = $this->common_page();
		$search_drop = Input::get('search_date');
		/*	$data['requests3'] = $query->skip($data['start'])->take($data['per_page'])
				->where(['RequestType' => 'delivery', 'request_version' => 'old'])
				->orderBy('_id', 'desc')->get();

			echo "<pre>";
		*/

		if (Input::get('list') == 'current') {
			$status = ['ready', 'accepted', 'out_for_pickup', 'out_for_delivery', 'assign'];
		} else {
			$status = ['delivered', 'cancel'];
		}

		//$query->orwhere(['TransporterId' => new MongoId(Session()->get('UserId')), 'RequestType' => 'delivery'])
			//->whereIn('Status', $status);


		$query->where(function ($query) use ($status) {
			$query->where(['TransporterId' => new MongoId(Session()->get('UserId')), 'RequestType' => 'delivery'])
			->whereIn('Status', $status);
			if (trim(Input::get('search'))) {
				 
				$query->where('RequesterName', 'like', '%' . trim(Input::get('search')) . '%');
			}
			if (Input::get('Status') != '') {
				$query->where('Status', '=', trim(Input::get('Status')));
			}
		});

		$query->orwhere(function ($query) use ($status) {

			$query->whereIn('ProductList.status', $status);
			if (Input::get('Status') != '') {
				$query->where('ProductList.status', '=', trim(Input::get('Status')));
			}

			
			if (trim(Input::get('search'))) {
				$query->where('RequesterName', 'like', '%' . trim(Input::get('search')) . '%');
			}
			return $query->where('ProductList.tpid', Session()->get('UserId'));

		});

		/*$query->orwhere(function ($query) use ($status) {

			$query->whereIn('ProductList.status', $status);
			if (Input::get('Status') != '') {
				$query->where('ProductList.status', '=', trim(Input::get('Status')));
			}

			if (trim(Input::get('search'))) {
				$query->where('RequesterName', 'like', '%' . trim(Input::get('search')) . '%');
			}
			return $query->where('ProductList.tpid', Session()->get('UserId'));

		});*/

		if (Input::get('search_date') != '') {

			$startDate = Input::get('search_date');
			$query->where('EnterOn', '>=', new MongoDate(strtotime(DateTime::createFromFormat('M d, Y', $startDate)->format('d-m-Y 00:00:00'))));
		}

		if (Input::get('search_date2') != '') {
			$endDate = Input::get('search_date2');
			$query->where('EnterOn', '<=', new MongoDate(strtotime(DateTime::createFromFormat('M d, Y', $endDate)->format('d-m-Y 23:59:59'))));
		}

		$data['count'] = $query->count();
		
		$data['requests'] = $query->skip($data['start'])->take($data['per_page'])
			
		//->orwhere(['TransporterId' => new MongoId(Session()->get('UserId')), 'RequestType' => 'delivery'])
			->orderBy('_id', 'desc')->get();
			//echo '<pre>';
		
		//print_r($data['requests']); die;

		$data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;
		$data['list'] = Input::get('list');
		$return_array = [];
		$temp_array = [];
			foreach ($data['requests'] as $key) {
				

				if (isset($key->ProductList)) {
					foreach ($key->ProductList as $value) {
						$image = "";
						if($value['tpid'] == Input::get('user_id')){
							

							$temp_array = [
								'request_id' => $key->_id,
								'_id' => $value['_id'],
								'Itemid' => $value['_id'],
								'package_id' => $value['package_id'],
								'status' => $value['status'],
								'show_status' => get_status_title($value['status'], $key['RequestType'])['status'],
								'tpid' => $value['tpid'],
								'RequestLocation' => $key['PickupCity'] . " to " . $key['DeliveryCity'],
								'tpName' => $value['tpName'],
								'product_name' => $value['product_name'],
								'image' => $image,
								'TripId' => "",
								'package_type' => $key['RequestType'],
								'item_cost' => @$value['productCost'],
								'PickupLat' => @$key['PickupLatLong'][1],
								'PickupLong' => @$key['PickupLatLong'][0],
								'DeliveryLat' => @$key['DeliveryLatLong'][1],
								'DeliveryLong' => @$key['DeliveryLatLong'][0],
								'Date' => $key['EnterOn']->sec,
								'request_version'=> @$key['request_version'],
							];
						}
						
						if (Input::get('type') == 'current') {
							if (in_array($value['status'], ['ready', 'accepted', 'out_for_pickup', 'out_for_delivery', 'assign'])) {
								$return_array[] = $temp_array;
							}
						} else {
							if (in_array($value['status'], ['delivered', 'cancel'])) {
								$return_array[] = $temp_array;
							}
						}
					}
				} else {
					$temp_array = [
						'request_id' => $key->_id,
						'_id' => $key['_id'],
						'package_id' => $key['PackageNumber'],
						'status' => $key['Status'],
						'show_status' => get_status_title($key['Status'], $key['RequestType'])['status'],
						'tpid' => $key['TransporterId'],
						'RequestLocation' => $key['PickupCity'] . " to " . $key['DeliveryCity'],
						'tpName' => $key['TransporterName'],
						'product_name' => $key['ProductTitle'],
						'image' => @$key['ProductImage'],
						'TripId' => "",
						'package_type' => $key['RequestType'],
						'request_id' => $key->_id,
						'Date' => $key['EnterOn']->sec,
						'PickupLat' => @$key['PickupLatLong'][1],
						'PickupLong' => @$key['PickupLatLong'][0],
						'DeliveryLat' => @$key['DeliveryLatLong'][1],
						'DeliveryLong' => @$key['DeliveryLatLong'][0],
						'ChatUserId' => (String) $key->RequesterId,
						'ChatUserName' => $ChatName,
						'ChatUsrImage' => @$ChatUsrImage,
						'request_version'=> @$key['request_version'],
					];

					if (Input::get('type') == 'current') {
						if (in_array($key['Status'], ['ready', 'accepted', 'out_for_pickup', 'out_for_delivery', 'assign'])) {
							$return_array[] = $temp_array;
						}
					} else {
						if (in_array($key['Status'], ['delivered', 'cancel'])) {
							$return_array[] = $temp_array;
						}
					}
				}
			}
			
			$data['return_array'] = $return_array;
		return view('User::list.pagination.my_deliveries', $data);
		//return view('User::list.pagination.my_deliveries2', $data);

	}
	public function on_line_purchases() {
		$query = Deliveryrequest::query();
		$data = $this->common_page();
		$search_drop = Input::get('search_date');
		$session_id = Session()->get('UserId');
		$where = array('id' => $session_id);
		$where = array();

		//$query->where('id','=',Session()->get('UserId'));
		if (Input::get('list') == 'new') {
			$query->whereIn('Status', array('pending', 'ready'));
		} else if (Input::get('list') == 'current') {
			$query->whereIn('Status', array('accepted', 'out_for_pickup', 'out_for_delivery'));
		} else if (Input::get('list') == 'completed') {
			$query->whereIn('Status', array('delivered', 'cancel'));
		}

		if (Input::get('search') != '') {
			$query->where('ProductTitle', 'like', '%' . trim(Input::get('search')) . '%');
		}
		$Status = Input::get('Status');
		if (Input::get('Status') != '') {
			$query->where('Status', '=', $Status);
		}

		if (Input::get('search_date') != '') {
			$startDate = Input::get('search_date');
			$query->where('PickupDate', '=', new MongoDate(strtotime(DateTime::createFromFormat('m/d/Y', $startDate)->format('d-m-Y 00:00:00'))));
		}

		$query->where($where);
		$data['count'] = $query->count();

		$data['requests'] = $query->skip($data['start'])->take($data['per_page'])
			->orderBy('_id', 'desc')->select('RequesterName', 'PickupFullAddress', 'DeliveryFullAddress', 'PickupDate', 'ProductTitle')
			->get();
		$data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;
		$data['list'] = Input::get('list');
		return view('User::list.pagination.on-line-purchases', $data);
	}

	public function trip_detail($id) {
		$query = Trips::query();
		$data = $this->common_page();
		$id = (strlen($id) == 24) ? new MongoId($id) : $id;
		$data['transporter_info'] = User::where(array('_id' => $id))->first();
		$query->where(array('TransporterId' => $id));
		$query->where(array('Status' => 'active'));

		if (Input::get('list') == 'individual') {
			$query->whereIn('TripType', array('individual'));
		} else if (Input::get('list') == 'business') {
			$query->whereIn('TripType', array('business'));
		}
		if (Input::get('search') != '') {

			$search = str_replace(",", '', Input::get('search'));
			$value = explode(" ", $search);
			$query->where('SourceCity', 'like', '%' . trim($value[0]) . '%')->orwhere('SourceState', 'like', '%' . trim($value[1]) . '%')->orwhere('SourceCountry', 'like', '%' . trim($value[2]) . '%');
		}
		if (Input::get('search_business') != '') {

			$search = str_replace(",", '', Input::get('search_business'));
			$value = explode(" ", $search);

			$query->where('DestiCity', 'like', '%' . trim($value[0]) . '%')->orwhere('DestiState', 'like', '%' . trim($value[1]) . '%')->orwhere('DestiCountry', 'like', '%' . trim($value[2]) . '%');
		}

		$data['count'] = $query->where('SourceDate', '>=', new MongoDate())->count();
		$data['requests'] = $query->where('SourceDate', '>=', new MongoDate())->skip($data['start'])->take($data['per_page'])
			->orderBy('_id', 'desc')->select()->orderBy('UpdateDate', 'asc')
			->get();
		$data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;
		$data['list'] = Input::get('list');

		return view('User::list.pagination.trip-detail', $data);
	}

	public function near_by_transporter() {

		$data = $this->common_page();
		$where = [
			'_id' => ['$ne' => new MongoId(Session::get('UserId'))],
			'UserType' => 'both',
			'TransporterStatus' => 'active',
			'CurrentLocation.0' => ['$nin' => [0, '']],
		];

		$tp_ids = [];
		$transporter_id = Trips::where('TransporterId', '!=', new MongoId(Session::get('UserId')))->where('SourceDate', '>=', new MongoDate)->select('id', 'TransporterId')->GroupBY('TransporterId')->distinct()->get();

		if (count($transporter_id) > 0) {
			foreach ($transporter_id as $key) {
				$tp_ids[] = $key->TransporterId;
			}
		}

		$where['_id']['$in'] = $tp_ids;
		if (!empty(trim(Input::get('search')))) {
			$where['Name'] = new MongoRegex("/" . trim(Input::get('search')) . "/i");
		}
		if (!empty(trim(Input::get('search_address')))) {
			$where['user_address'] = new MongoRegex("/" . trim(Input::get('search_address')) . "/i");
		}

		$res = User::raw(function ($collection) use ($where, $data) {
			return $collection->aggregate([
				[
					'$project' => [
						'user_address' => ['$concat' => [
							'$Street1', ' ', '$Street2', ' ', '$City', ' ', '$State', ' ', '$Country', ' ', '$ZipCode',
						]],
						'custom_address' => ['$concat' => [
							'$City', ' ', '$State', ' ', '$Country',
						]],
						'Image' => '$Image',
						'Name' => '$Name',
						'FirstName' => '$FirstName',
						'LastName' => '$LastName',
						'UserType' => '$UserType',
						'RatingCount' => '$RatingCount',
						'RatingByCount' => '$RatingByCount',
						'CurrentLocation' => '$CurrentLocation',
						'TransporterStatus' => '$TransporterStatus',

					],
				],
				[
					'$match' => $where,
				],
				[
					'$skip' => $data['start'],
				],
				[
					'$limit' => (int) $data['per_page'],
				],

			]);

		});
		$data['transporter'] = $res['result'];

		//print_r($data['transporter']); die;

		return view('User::list.pagination.near-by-transporter', $data);
	}

	public function new_request() {
		
		$data = $this->common_page();
		$query = Deliveryrequest::query();
		$query->where('RequesterId', '!=', new MongoId(Session::get('UserId')));
		$query->where(['Status' => 'ready', 'TransporterId' => '']);
		$query->whereNotIn('RequestType', ['online', 'buy_for_me', 'local_delivery']);
		//$query->where(array("ProductList" => array('$elemMatch' => array('tpid' =>Session::get('UserId')))));
		if (Input::get('search') != '') {
			$query->where('ProductTitle', 'like', '%' . trim(Input::get('search')) . '%');
		}

		if (!empty(trim(Input::get('search_address')))) {
			$query->where('PickupFullAddress', 'like', '%' . trim(Input::get('search_address')) . '%');
		}
		if (!empty(trim(Input::get('desti_address')))) {
			$query->where('DeliveryFullAddress', 'like', '%' . trim(Input::get('desti_address')) . '%');
		}
/*
if(!empty(trim(Input::get('lng'))) && Input::get('lng') != 0)
{
$distance = 10000;
$config = Setting::find('563b0e31e4b03271a097e1ca');
if(count($config) > 0) {
$distance = $config->CoverageArea;
}
$query->where([
"PickupLatLong" => [
'$near' => [
'$geometry' => [
"type" => "Point",
"coordinates" => [floatval(Input::get('lng')),floatval(Input::get('lat'))]
],
'$maxDistance' => $distance,
'$uniqueDocs' => 1
]
]]);
}
 */
		$data['transporter'] = $query->skip($data['start'])->take($data['per_page'])
			->orderBy('_id', 'desc')->get();
		//echo '<pre>';
		//print_r($data['transporter']); die;

		return view('User::list.pagination.new-request', $data);

	}

	public function common_page($pageNo = '', $pnum = 0) {
		if ($pageNo != '') {
			$data['cur_page'] = $data['page'] = $pageNo;
		} else {
			$data['cur_page'] = $data['page'] = Input::get('page');
		}
		$data['page'] -= 1;
		$data['per_page'] = ($pnum > 0) ? $pnum : Input::get('Pnum');
		$data['previous_btn'] = true;
		$data['next_btn'] = true;
		$data['first_btn'] = true;
		$data['last_btn'] = true;
		$data['sno'] = ($data['start'] = $data['page'] * $data['per_page']) + 1;
		return $data;
	}
	public function my_trips() {
		$query = Trips::query();
		$data = $this->common_page();
		$search_drop = Input::get('search_date');
		$where = array('TripType' => 'business', 'TransporterId' => new MongoId(Session()->get('UserId')));

		if (Input::get('search') != '') {
			$value = Input::get('search');
			$query->where('SelectCategory', 'like', '%' . trim($value) . '%');
		}

		if (Input::get('search_date') != '') {

			$startDate = Input::get('search_date');
			$query->where('SourceDate', '>=', new MongoDate(strtotime(DateTime::createFromFormat('M d, Y', $startDate)->format('d-m-Y 00:00:00'))));

		}

		if (Input::get('search_date2') != '') {
			$endDate = Input::get('search_date2');
			$query->where('SourceDate', '<=', new MongoDate(strtotime(DateTime::createFromFormat('M d, Y', $endDate)->format('d-m-Y 23:59:59'))));
		}

		if (Input::get('list') == 'current') {
			$query->where('SourceDate', '>=', new MongoDate());
		} else if (Input::get('list') == 'history') {
			$query->where('SourceDate', '<=', new MongoDate());
		}
		$query->where($where);
		$data['count'] = $query->count();
		$data['tab'] = Input::get('list');

		$data['requests'] = $query->skip($data['start'])->take($data['per_page'])
			->orderBy('_id', 'desc')->get();

		$data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;
		$data['list'] = Input::get('list');
		return view('User::list.pagination.my_trips', $data);

	}

	public function individual_trips() {
		$query = Trips::query();
		$data = $this->common_page();
		$search_drop = Input::get('search_date');
		$where = [
			'TripType' => 'individual',
			'TransporterId' => new MongoId(Session()->get('UserId')),
		];

		if (Input::get('search') != '') {
			$value = Input::get('search');
			$query->where('SelectCategory', 'like', '%' . trim($value) . '%');
		}

		if (Input::get('search_date') != '') {

			$startDate = Input::get('search_date');
			$query->where('SourceDate', '>=', new MongoDate(strtotime(DateTime::createFromFormat('M d, Y', $startDate)->format('d-m-Y 00:00:00'))));

		}

		if (Input::get('search_date2') != '') {
			$endDate = Input::get('search_date2');
			$query->where('SourceDate', '<=', new MongoDate(strtotime(DateTime::createFromFormat('M d, Y', $endDate)->format('d-m-Y 23:59:59'))));
		}

		if (Input::get('list') == 'current') {
			$query->where('SourceDate', '>=', new MongoDate());
		} else if (Input::get('list') == 'history') {
			$query->where('SourceDate', '<=', new MongoDate());
		}
		$data['tab'] = Input::get('list');
		$data['count'] = $query->where('SourceDate', '>=', new MongoDate())->where($where)->count();

		$data['requests'] = $query->where('SourceDate', '>=', new MongoDate())->where($where)->skip($data['start'])->take($data['per_page'])
			->orderBy('_id', 'desc')->get();

		$data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;
		$data['list'] = Input::get('list');
		return view('User::list.pagination.individual_trips', $data);

	}

	public function notification() {
		$query = Notification::query();
		$data = $this->common_page('', 12);
		$search_drop = Input::get('search_date');

		if (Input::get('search') != '') {
			$value = Input::get('search');
			$query->where('NotificationTitle', 'like', '%' . trim($value) . '%');
		}
		if (Input::get('search_date') != '') {
			$startDate = Input::get('search_date');
			$query->where('Date', '>=', new MongoDate(strtotime(DateTime::createFromFormat('M d, Y', $startDate)->format('M d, Y 00:00:00'))));

		}

		if (Input::get('search_date2') != '') {
			$endDate = Input::get('search_date2');

			$query->where('Date', '<=', new MongoDate(strtotime(DateTime::createFromFormat('M d, Y', $endDate)->format('M d, Y 23:59:59'))));
		}
		$query->where(array('NotificationUserId' => new MongoId(Session()->get('UserId'))));
		$data['count'] = $query->count();

		$data['notification'] = $query->skip($data['start'])->take($data['per_page'])
			->orderBy('_id', 'desc')->get();

		$data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;

		return view('User::list.pagination.notification', $data);

	}

	public function post_by_transporter() {
		$query = Trips::query();
		$data = $this->common_page();
		$search_drop = Input::get('search_date');

		$query->where('TransporterId', '!=', new MongoId(Session()->get('UserId')));
		$query->where(['Status' => 'active']);

		//$data['transporter_info'] = User::where(array('_id'=>Session()->get('UserId')))->first();

		if (Input::get('search_value') != '') {
			$query->where('SourceFullAddress', 'like', '%' . Input::get('search_value') . '%');
		}

		if (Input::get('search_value2') != '') {
			$query->where('DestFullAddress', 'like', '%' . Input::get('search_value2') . '%');
		}
		if (Input::get('search_date') != '') {
			$startDate = Input::get('search_date');
			$query->where('SourceDate', '>=', new MongoDate(strtotime(DateTime::createFromFormat('M d, Y', $startDate)->format('M d, Y 00:00:00'))));
			$query->where('SourceDate', '<=', new MongoDate(strtotime(DateTime::createFromFormat('M d, Y', $startDate)->format('M d, Y 23:59:59'))));

		}

		if (Input::get('search_date2') != '') {
			$endDate = Input::get('search_date2');

			$query->where('DestiDate', '>=', new MongoDate(strtotime(DateTime::createFromFormat('M d, Y', $endDate)->format('M d, Y 00:00:00'))));
			$query->where('DestiDate', '<=', new MongoDate(strtotime(DateTime::createFromFormat('M d, Y', $endDate)->format('M d, Y 23:59:59'))));
		}
		if (Input::get('list') == 'current') {
			$query->where('SourceDate', '>=', new MongoDate());
			$query->where(array('TripType' => 'individual'));

		} else if (Input::get('list') == 'history') {
			$query->where('SourceDate', '>=', new MongoDate());
			$query->where(array('TripType' => 'business'));
		}

		$data['count'] = $query->count();
		$data['tab'] = Input::get('list');

		$data['requests'] = $query->skip($data['start'])->take($data['per_page'])
			->orderBy('_id', 'desc')->get();

		$data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;
		$data['list'] = Input::get('list');
		return view('User::list.pagination.post_by_transporter', $data);

	}

	public function view_trip_request($id) {
		$query = Deliveryrequest::query();
		$data = $this->common_page();
		$search_drop = Input::get('search_date');

		$id = (strlen($id) == 24) ? new MongoId($id) : $id;

		if (Input::get('search') != '') {
			$value = Input::get('search');
			$query->where('', 'like', '%' . trim($value) . '%');
		}
		if (Input::get('search_date') != '') {
			$startDate = Input::get('search_date');
			$query->where('Date', '>=', new MongoDate(strtotime(DateTime::createFromFormat('M d, Y', $startDate)->format('M d, Y 00:00:00'))));

		}

		if (Input::get('search_date2') != '') {
			$endDate = Input::get('search_date2');

			$query->where('Date', '<=', new MongoDate(strtotime(DateTime::createFromFormat('M d, Y', $endDate)->format('M d, Y 23:59:59'))));
		}
		$query->where(['TripId' => $id]);

		$data['count'] = $query->count();
		$data['request'] = $query->skip($data['start'])->take($data['per_page'])
			->orderBy('_id', 'desc')->get();

		$data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;

		return view('User::list.pagination.view_request_trip', $data);

	}

	public function faq_list() {
		$query = FAQ::query();
		$data = $this->common_page('', 12);

		if (Input::get('Search') != '') {
			$value = trim(Input::get('Search'));
			$query->where('Question', 'like', "%$value%");
		}

		$data['count'] = $query->count();

		$data['faq'] = $query->skip($data['start'])->take($data['per_page'])
			->orderBy('_id', 'desc')->get();

		$data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;
		return view('User::list.pagination.faq_list', $data);

	}

	public function pending_request() {

		$query = Deliveryrequest::query();
		$data = $this->common_page();
		$search_drop = Input::get('search_date');
		$session_id = Session()->get('UserId');

		$query->where('RequesterId', '=', new MongoId(Session()->get('UserId')));

		if (Input::get('status') == 'not_delivered') {
			$query->where('Status', '=', 'cancel');
		} else {
			//$query->where('Status', '=', 'completed');
			  $query->whereIn('Status',array('completed','delivered'));
		}

		if (Input::get('search') != '') {

			$query->where('ProductTitle', 'like', '%' . trim(Input::get('search')) . '%');
		}

		$Status = Input::get('Status');
		if (Input::get('Status') != '') {

			$query->where('Status', '=', $Status);
		}
		if (Input::get('search_date') != '') {
			$startDate = Input::get('search_date');
			$query->where('EnterOn', '>=', new MongoDate(strtotime(DateTime::createFromFormat('M d, Y', $startDate)->format('d-m-Y 00:00:00'))));
		}

		if (Input::get('search_date2') != '') {
			$endDate = Input::get('search_date2');
			$query->where('EnterOn', '<=', new MongoDate(strtotime(DateTime::createFromFormat('M d, Y', $endDate)->format('d-m-Y 23:59:59'))));
		}

		$data['count'] = $query->count();
		$data['requests'] = $query->skip($data['start'])->take($data['per_page'])
			->orderBy('_id', 'desc')->select('_id', 'RequesterName', 'PickupFullAddress', 'DeliveryFullAddress', 'RequestType', 'length', 'DeliveryDate', 'Status', 'ProductTitle', 'EnterOn', 'PickupLatLong', 'DeliveryLatLong', 'PickupDate', 'ProductList', 'ProductImage', 'image')
			->get();
		$data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;
		$data['list'] = Input::get('list');
		//echo "<pre>"; print_r($data);  die;
		return view('User::list.pagination.pending_request', $data);
	}

	public function transporter_pending() {

		$query = Deliveryrequest::query();
		$data = $this->common_page();
		$search_drop = Input::get('search_date');
		$session_id = Session()->get('UserId');

		$query->where('RequesterId', '=', new MongoId(Session()->get('UserId')));

		if (Input::get('status') == 'not_delivered') {
			$query->where('Status', '=', 'cancel');
		} else {
			$query->where('Status', '=', 'completed');
		}

		/*if(Input::get('list') == 'new') {
			        $query->whereIn('Status',array('completed'));
		*/

		if (Input::get('search') != '') {

			$query->where('ProductTitle', 'like', '%' . trim(Input::get('search')) . '%');
		}

		$Status = Input::get('Status');
		if (Input::get('Status') != '') {

			$query->where('Status', '=', $Status);
		}
		if (Input::get('search_date') != '') {
			$startDate = Input::get('search_date');
			$query->where('EnterOn', '>=', new MongoDate(strtotime(DateTime::createFromFormat('M d, Y', $startDate)->format('d-m-Y 00:00:00'))));
		}

		if (Input::get('search_date2') != '') {
			$endDate = Input::get('search_date2');
			$query->where('EnterOn', '<=', new MongoDate(strtotime(DateTime::createFromFormat('M d, Y', $endDate)->format('d-m-Y 23:59:59'))));
		}

		$data['count'] = $query->count();
		$data['requests'] = $query->skip($data['start'])->take($data['per_page'])
			->orderBy('_id', 'desc')->select('_id', 'RequesterName', 'PickupFullAddress', 'DeliveryFullAddress', 'RequestType', 'length', 'DeliveryDate', 'Status', 'ProductTitle', 'EnterOn', 'PickupLatLong', 'DeliveryLatLong', 'PickupDate', 'ProductList', 'ProductImage', 'image')
			->get();
		$data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;
		$data['list'] = Input::get('list');
		//echo "<pre>"; print_r($data);  die;
		return view('User::list.pagination.transporter_pending', $data);
	}
}
