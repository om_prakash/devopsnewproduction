<?php namespace App\Modules\User\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Models\Activitylog;
use App\Http\Models\Additem;
use App\Http\Models\Address;
use App\Http\Models\Category;
use App\Http\Models\CityStateCountry;
use App\Http\Models\Configuration;
use App\Http\Models\Deliveryrequest;
use App\Http\Models\Emailtemplate;
use App\Http\Models\Extraregion;
use App\Http\Models\Notification;
use App\Http\Models\Promocode;
use App\Http\Models\Setting;
use App\Http\Models\Support;
use App\Http\Models\Transaction;
use App\Http\Models\Trips;
use App\Http\Models\User;
use App\Library\Promo;
use App\Library\Reqhelper;
use App\Library\Requesthelper;
use App\Library\RSStripe;
use App\Library\Utility;
use DateTime;
use Illuminate\Http\Request;
use Input;
use MongoDate;
use MongoId;
use MongoRegex;
use Redirect;
use Session;
use Stripe;
use Validator,Artisan;
use App\Http\Models\RequestCard;
use App\Http\Models\Item;
use App\Http\Models\Itemhistory;
use App\Library\NewEmail;
use App\Http\Models\Currency;
use App\Http\Models\PaymentInfo;
use App\Http\Models\SendMail;

class Userctrl extends Controller {
	public function __construct() {
		if (!session()->has('UserId')) {
			return Redirect::to('login')->send();
		}

		$user = User::where(["_id" => session()->get('UserId')])
			->where(['delete_status' => 'yes'])
			->first();
		if (count($user) > 0) {
			Session::flush();
			return Redirect::to('login');
		}

	}
	/* Developer Name : Aakash tejwal
		     * Date : 19-08-2016
		     * About Function : This function is for display dashboard from
		     * Method : Post
		     * Lest Update :
		     * Update by :
		     *58b69e087ac6f6f1238b4567
	*/
	public function dashboard() {
		$data['address'] = User::where(['_id' => Session::get('UserId')])->select('_id', 'AqAddress', 'UniqueNo', 'Name', 'AqCity', 'AqState', 'AqCountry', 'AqZipcode', 'Aqphone', 'Aqcc')->first();
		$data['users'] = Deliveryrequest::where('RequesterId', '=', new MongoId(Session()->get('UserId')))->whereIn('Status', array('pending', 'momo_initiated'))->count();
		if (session()->has('type')) {
			if (strtolower(session()->get('type')) == 'online') {
				return redirect('on-line-purchases');
			} elseif (strtolower(session()->get('type')) == 'buyforme_form') {
				return redirect('buy-for-me');
			} else {
				return redirect('prepare-request');
			}
		}
		return view('User::detail.dashboard', $data);
	}

	public function buy_for_me() {

		$data['country'] = CityStateCountry::where(['type' => 'Country', 'Status' => 'Active'])
			->orderBy('Content', 'Asc')
			->get(['_id', 'Content', 'state_available']);

		$data['category'] = Category::where(['type' => 'Category', 'Status' => 'Active'])->whereIn('TravelMode', ['ship', 'air'])->orderBy('Content', 'asc')->get(['TravelMode', 'Content']);
		$data['address'] = Address::where('user_id', '=', Session::get('UserId'))->select('address_line_1', 'city', 'state', 'country', 'zipcode', '_id', 'lat', 'lng', 'country_id', 'state_id')->get();
		$data['additem'] = Additem::where('user_id', '=', Session::get('UserId'))->where(['request_type' => 'buy_for_me'])->limit(5)->get();
		$data['item'] = Item::Where(['Status' => 'Active'])->get();
		return view('User::Buy.buy-for-me', $data);
	}

	public function delivery_detail() {
		return view('User::detail.delivery');
	}

	public function change_password() {

		return view('User::other.change_password');
	}

	public function delivery_details($id) {
		
		$data['data'] = Deliveryrequest::where('_id', '=', $id)->get();
		
		$data['transporter_data'] = [];
		if (!count($data['data']) > 0) {
			return redirect('my-request');
		}


		if (!empty($data['data'][0]['TransporterId'])) {
			$data['transporter_data'] = User::where(array('_id' => $data['data'][0]['TransporterId']))
				->first();
		}

		return view('User::detail.delivery_detail', $data);
	}

	public function local_delivery_details($id) {
		//echo "hi";die;
		$data['data'] = Deliveryrequest::where('_id', '=', $id)->get();
		$data['transporter_data'] = [];
		if (!count($data['data']) > 0) {
			return redirect('my-request');
		}

		if (!empty($data['data'][0]['TransporterId'])) {
			$data['transporter_data'] = User::where(array('_id' => $data['data'][0]['TransporterId']))
				->first();
		}

		return view('User::detail.local_delivery_detail', $data);
	}

	public function online_delivery_details($id) {
		$data['data'] = Deliveryrequest::where('_id', '=', $id)->first();
		return view('User::detail.online_delivery_detail', $data);
	}

	public function delete_card($id, Request $request) {
		$responce = array('success' => 0, 'msg' => 'Oops! Fail to delete record.');
		$user = User::where(array('_id' => Session()->get('UserId')))->first();
		if (count($user) > 0) {
			$stripe = Stripe::make();
			if ($user->StripeId != '') {
				try {
					$StripeResponse = $stripe->cards()->delete($user->StripeId, $id);
					$response['success'] = 1;
					$response['msg'] = "Credit card deleted successfully.";
					return Redirect::back()->withSuccess('Success! Card has been deleted successfully.');
				} catch (Exception $e) {
					$response['msg'] = $e->getMessage();
				}
			}
		} else {
			$responce = array('success' => 2, 'msg' => 'Oops! Something went wrong.');
		}

		return response()->json($response);
	}

	public function card_list() {
		$data['cards'] = array();
		$user = User::where(array('_id' => Session()->get('UserId')))->select('StripeId')->first();
		if (count($user) > 0) {
			$data['cards'] = Payment::card_list($user->StripeId);
		}
		return view('User::list.card_list', $data);
	}
	public function add_address(Request $request) {
		$validation = Validator::make($request->all(), [
			'address_line_1' => 'required',
			'country' => 'required',
			'city' => 'required',
		]);

		if ($validation->fails()) {
			$validation->errors()->add('field', trans('Something is wrong'));
			return $validation->errors();
		} else {

			$addlat = true;
			$lat = floatval(Input::get('lat'));
			$lng = floatval(Input::get('lng'));
			$json_decode_country = json_decode(Input::get('country'));
			$json_decode_state = json_decode(Input::get('state'));
			$json_decode_city = json_decode(Input::get('city'));

			if (empty($lng)) {
				$addlat = Utility::getLatLong(
					Input::get('address_line_1'),
					Input::get('address_line_2'),
					$json_decode_city->name,
					@$json_decode_state->name,
					$json_decode_country->name
				);
				if ($addlat == false) {
					$addlat = Utility::getLatLong(
						$json_decode_city->name,
						@$json_decode_state->name,
						$json_decode_country->name
					);
				}
				if ($addlat != false) {
					$lat = $addlat['lat'];
					$lng = $addlat['lng'];
				}
			}

			if ($addlat == false) {
				$response = array("success" => 0, "msg" => "We are unable to find your location. Please correct it.");
			} else {

				$insert = [
					'country' => $json_decode_country->name,
					'country_id' => $json_decode_country->id,
					'state' => @$json_decode_state->name,
					'state_id' => @$json_decode_state->id,
					'city' => $json_decode_city->name,
					'city_id' => $json_decode_city->id,
					'state_available' => $json_decode_country->state_available,
					'address_line_1' => Input::get('address_line_1'),
					'address_line_2' => Input::get('address_line_2'),
					'zipcode' => Input::get('zipcode'),
					'user_id' => Session::get('UserId'),
					'lat' => floatval($lat),
					'lng' => floatval($lng),
				];

				Address::insert($insert);
				$field = ['country', 'state', 'city', 'address_line_1', 'address_line_2', 'zipcode', 'lat', 'lng', 'state_available'];
				$address = Address::where(['user_id' => Session::get('UserId')])->get();
				$response = array("success" => 1, "msg" => "Address has been added successfully.", 'address_html' => '<option value="">Select address</option>');

				foreach ($address as $key) {
					$response['address_html'] .= "<option value='" . json_encode($key) . "'>" . ucfirst($key->address_line_1) . " " . $key->city . " " . $key->state . " " . $key->country . " " . $key->zipcode . "</option>";
				}

				$view = view('User::Buy.ajax.add_address', ['address' => $address]);
				$response['html'] = $view->render();
			}

		}
		return response()->json($response);
	}

	public function add_card() {
		$response = array('success' => 0, 'msg' => 'Something went wrong.');

		$validate = Validator::make(input::all(), [
			'name_on_card' => 'required',
			'credit_card_number' => 'required',
			'month' => 'required',
			'year' => 'required',
			'security_code' => 'required',
		]);

		if ($validate->fails()) {
			return Redirect::to("process-card-list/" . Input::get('requestid'))->withErrors($validate)->withInput();
		} else {
			$user = User::where('_id', '=', Session()->get('UserId'))->select('StripeId')->first();
			if (count($user) > 0) {
				$res = Payment::add_card($user);

				if ($res == 1) {

					$response = array('success' => 1, 'msg' => $res);
				} else {

					$response = array('success' => 0, 'msg' => 'Fail !' . '&nbsp;&nbsp;' . $res);
				}
			}

		}
		echo json_encode($response);
	}

	public function delete_request($id, $function) {
		$responce = array('success' => 0, 'msg' => 'Fail to delete request.');

		if ($function == 'DeliveryRequest') {
			$delivery = Deliveryrequest::where(['_id' => $id])
				->whereIn('Status', ['ready', 'pending'])->first();

			if (count($delivery) > 0) {
				if (isset($delivery->ProductImage)) {
					if (!empty(trim($delivery->ProductImage))) {
						if (file_exists(BASEURL_FILE . $delivery->ProductImage)) {
							unlink(BASEURL_FILE . $delivery->ProductImage);
						}
					}
				}

				if (isset($delivery->OtherImage)) {
					foreach ($delivery->OtherImage as $img) {
						if (!empty(trim($img))) {
							if (file_exists(BASEURL_FILE . $img)) {
								unlink(BASEURL_FILE . $img);
							}
						}
					}
				}
				if (isset($delivery->ProductList)) {
					foreach ($delivery->ProductList as $img) {
						if (!empty(trim(@$img['image']))) {
							if (file_exists(BASEURL_FILE . @$img['image'])) {
								unlink(BASEURL_FILE . @$img['image']);
							}
						}
					}
				}

				$delivery->delete();
			}
			$responce['success'] = 1;
			$responce['msg'] = 'Deleted successfully.';
		}

		if ($function == 'DeliveryRequestOnline') {
			$delivery = Deliveryrequest::where(['_id' => $id])
				->whereIn('Status', ['ready', 'pending'])->first();

			if (count($delivery) > 0) {

				$e = Deliveryrequest::where(['_id' => $id])
					->where(['ProductList' => ['$elemMatch' => ['$ne' => [
						'status' => 'null',
					]]]])
					->update([
						'Status' => 'deleted',
						'ProductList.$.status' => 'deleted',
					]);
			}
			$responce['success'] = 1;
			$responce['msg'] = 'Request has been deleted successfully.';
		}

		if ($function == 'Deleteaddress') {
			Address::find($id)->delete();
			$responce['success'] = 1;
			$responce['msg'] = 'Deleted successfully.';
		}

		if ($function == 'DeleteItem') {
			Additem::find($id)->delete();
			$responce['success'] = 1;
			$responce['msg'] = 'Deleted successfully.';
		}

		if ($function == 'individual_trip') {
			Trips::find($id)->delete();
			$responce['success'] = 1;
			$responce['msg'] = 'Deleted successfully.';
		}

		if ($function == 'business_trip') {
			Trips::find($id)->delete();
			$responce['success'] = 1;
			$responce['msg'] = 'Deleted successfully.';
		}

		if ($function == 'notification') {
			Notification::find($id)->delete();
			$responce['success'] = 1;
			$responce['msg'] = 'Deleted successfully.';
		}

		echo json_encode($responce);
	}

	public function delete_item($id, $function) {

		$responce = array('success' => 0, 'msg' => 'Fail to delete request.');

		if ($function == 'DeleteItem') {
			if (!empty(trim(Input::get('request_id'))) && session()->has(trim(Input::get('request_id')))) {
				$product_list = [];
				if (is_array(session()->get(trim(Input::get('request_id'))))) {
					foreach (session()->get(trim(Input::get('request_id'))) as $key => $val) {
						$product_list[(String) $val['_id']] = $val;
					}
					if (isset($product_list[$id])) {
						unset($product_list[$id]);
						session()->put(trim(Input::get('request_id')), []);
						foreach ($product_list as $key) {
							session()->push(trim(Input::get('request_id')), $key);
						}
						$responce['success'] = 1;
						$responce['msg'] = 'Item has been removed successfully.';
					}
				}

			} else {
				$item = Additem::find($id);

				if (count($item) > 0) {
					if (!empty(trim($item->image))) {
						if (file_exists(BASEURL_FILE . $item->image)) {
							unlink(BASEURL_FILE . $item->image);
						}
					}
					$item->delete();
					$responce['success'] = 1;
					$responce['msg'] = 'Item has been removed successfully.';
				}
			}
		}

		if ($function == 'send_package_item') {
			$item = Additem::find($id);
			if (count($item) > 0) {
				$item->delete();
				$responce['success'] = 1;
				$responce['msg'] = 'Item has been removed successfully.';

			}
		}

		if ($function == 'edit_send_package_item') {
			if ($requestid) {
				$res_data = Deliveryrequest::where(['_id' => trim($requestid), 'RequesterId' => new MongoId(Session::get('UserId'))])
					->select('ProductList')->first();
				if (count($res_data) > 0) {
					if (isset($res_data->ProductList)) {
						$update_array = $res_data->ProductList;
						foreach ($res_data->ProductList as $key) {
							if ($key->_id == $id) {
								unset($update_array[$key->_id]);
							}
						}

						//	print_r($update_array);die;
					}

				}

			}
			$item = Additem::find($id);
			if (count($item) > 0) {
				$item->delete();
				$responce['success'] = 1;
				$responce['msg'] = 'Item has been removed successfully.';

			}
		}

		if ($function == 'local_delivery_item') {
			$item = Additem::find($id);
			if (count($item) > 0) {
				$item->delete();
				$responce['success'] = 1;
				$responce['msg'] = 'Item has been removed successfully.';
			}
		}

		echo json_encode($responce);
	}

	public function delete_delivery_request($id) {
		$delivery = Deliveryrequest::where(array('_id' => $id))->whereIn('Status', ['ready', 'pending'])->first();

		if (count($delivery) > 0) {
			if (isset($delivery->ProductImage)) {
				if (!empty(trim($delivery->ProductImage))) {
					if (file_exists(BASEURL_FILE . $delivery->ProductImage)) {
						unlink(BASEURL_FILE . $delivery->ProductImage);
					}
				}
			}

			if (isset($delivery->OtherImage)) {
				foreach ($delivery->OtherImage as $img) {
					if (!empty(trim($img))) {
						if (file_exists(BASEURL_FILE . $img)) {
							unlink(BASEURL_FILE . $img);
						}
					}
				}
			}
			if (isset($delivery->ProductList)) {
				foreach ($delivery->ProductList as $img) {
					if (!empty(trim($img['image']))) {
						if (file_exists(BASEURL_FILE . $img['image'])) {
							unlink(BASEURL_FILE . $img['image']);
						}
					}
				}
			}

			$delivery->delete();
			return Redirect::to('my-request')->withSuccess('Success! Send a package has been removed successfully.');
		}
		return Redirect::to('delivery-details/' . $id)
			->with(['danger' => 'Fail!  Something went wrong.']);
	}
	/*public function edit_request($id)
		    {
		    return view('User::edit.edit_request');
	*/

	public function add_product() {

		$validation = Validator::make(Input::get(), [
			'item_name' => 'required',
			'add_item_url' => 'required',
			'item_price' => 'required',
			'category' => 'required',
			'description' => 'required',

		]);
		if ($validation->fails()) {
			$response = ['success' => 0, 'msg' => 'Oops! Please fill all information.'];
			$response['error'] = $validation->errors();
		} else {
			$json_decode_category = json_decode(Input::get('category'));
			$bfm_product = [];

			foreach (session()->get('bfm_product') as $key => $v) {
				$bfm_product[$v['_id']] = $v;
			}

			$weight_unit = 'lbs';
			$heightUnit = 'Inche';
			if ($key['weight_unit'] == 'kg') {
				$weight_unit = 'kg';
				$heightUnit = 'cm';
			}

			$insert = [
				'_id' => $v['_id'],
				'product_name' => Input::get('item_name'),
				'item_name' => Input::get('item_name'),
				'url' => Input::get('add_item_url'),
				'length' => Input::get('length'),
				'lengthUnit' => $heightUnit,
				'width' => Input::get('width'),
				'weight_unit' => $weight_unit,
				'height' => Input::get('height'),
				'heightUnit' => $heightUnit,
				'weight' => Input::get('weight'),
				'weightUnit' => $heightUnit,
				'price' => Input::get('item_price'),
				'qty' => Input::get('quentity'),
				'travel_mode' => Input::get('travel_mode'),
				'travelMode' => Input::get('travel_mode'),
				'category' => $json_decode_category->name,
				'categoryid' => $json_decode_category->id,
				'category_id' => $json_decode_category->id,
				'description' => Input::get('description'),
				'insurance_status' => Input::get('insurance'),
				'insurance' => Input::get('insurance'),
				'shipping_cost' => Input::get('shipping_cost'),
				'request_type' => 'online',
				'measurement_unit' => $key['measurement_unit'],
				'user_id' => Session::get('UserId'),
			];

			$data['items'] = [];
			if (isset($bfm_product[Input::get('item_id')])) {
				$bfm_product[Input::get('item_id')] = $insert;
			} else {
				$insert['_id'] = (String) new MongoId();
				$bfm_product[$insert['_id']] = $insert;
			}
			foreach ($bfm_product as $key) {
				$data['items'][] = $key;
			}

			session()->set('bfm_product', $data['items']);
			$response = array("success" => 1, "msg" => "Item has been added successfully .");
			$view = view('User::edit.ajax.edit-list', $data);

			$response['html'] = $view->render();

		}
		return response()->json($response);
	}

	/* function add_online_item()
		    {

		    $validation = Validator::make(Input::get(), [
		    'item_name'     => 'required',
		    'add_item_url'     => 'required',
		    'length'            => 'required',
		    'width'          => 'required',
		    'height'           => 'required',
		    'weight'           => 'required',
		    'item_price'       => 'required',
		    'category'       => 'required',
		    'description'   => 'required'

		    ]);
		    if ($validation->fails())
		    {
		    $response = ['success' => 0,'msg' => 'Oops! Please fill all information.'];
		    $response['error'] = $validation->errors();
		    }
		    else
		    {
		    $json_decode_category      =   json_decode(Input::get('category'));
		    $bfm_product = [];

		    foreach(session()->get('bfm_product') as $key => $v) {
		    $bfm_product[$v['_id']]    = $v;
		    }

		    $weight_unit ='lbs';
		    $heightUnit = 'Inche';
		    if($key['weight_unit'] == 'kg')
		    {
		    $weight_unit ='kg';
		    $heightUnit = 'cm';
		    }

		    $insert = [
		    '_id'                =>     $v['_id'],
		    'product_name'       =>    Input::get('item_name'),
		    'item_name'            =>  Input::get('item_name'),
		    'url'               =>    Input::get('add_item_url'),
		    'length'               =>  Input::get('length'),
		    'lengthUnit'        =>    $heightUnit,
		    'width'               =>    Input::get('width'),
		    'weight_unit'         =>     $weight_unit,
		    'height'               =>    Input::get('height'),
		    'heightUnit'        =>    $heightUnit,
		    'weight'             =>     Input::get('weight'),
		    'weightUnit'        =>    $heightUnit,
		    'price'                =>  Input::get('item_price'),
		    'qty'               =>    Input::get('quentity'),
		    'travel_mode'       =>    Input::get('travel_mode'),
		    'travelMode'           =>    Input::get('travel_mode'),
		    'category'             =>     $json_decode_category->name,
		    'categoryid'           =>    $json_decode_category->id,
		    'category_id'           =>    $json_decode_category->id,
		    'description'       =>  Input::get('description'),
		    'insurance_status'  =>  Input::get('insurance'),
		    'insurance'  =>  Input::get('insurance'),
		    'shipping_cost'     =>  Input::get('shipping_cost'),
		    'request_type'      =>  'online',
		    'measurement_unit'    =>  $key['measurement_unit'],
		    'user_id'           =>  Session::get('UserId')
		    ];

		    $data['items'] = [];
		    if(isset($bfm_product[Input::get('item_id')])) {
		    $bfm_product[Input::get('item_id')] = $insert;
		    } else {
		    $insert['_id'] = (String)new MongoId();
		    $bfm_product[$insert['_id']] = $insert;
		    }
		    foreach($bfm_product as $key)  {
		    $data['items'][] = $key;
		    }

		    session()->set('bfm_product',$data['items']);
		    $response = array("success" => 1, "msg" => "Item has been added successfully .");
		    $view = view('User::edit.ajax.edit-list',$data);

		    $response['html'] = $view->render();

		    }
		    return response()->json($response);
	*/

	public function setting() {
		return view('User::detail.setting');
	}
	public function address() {

		$data['address'] = Address::where(['user_id' => Session::get('UserId')])->get();
		$data['country'] = CityStateCountry::where(['type' => 'Country', 'Status' => 'Active'])
			->orderBy('Content', 'Asc')
			->get(['_id', 'Content', 'state_available']);

		return view('User::detail.address', $data);
	}
	public function delete_user() {
		$insert = User::where(array('_id' => Session::get('UserId')))->delete();
		return Redirect::to("logout")->withSuccess('Success! User has been deleted successfully.');
	}

	/* Developer Name : Aakash tejwal
		     * Date : 19-08-2016
		     * About Function : This function is use for bank information
		     * Method : Post
		     * Lest Update :
		     * Update by :
		     *
	*/
	public function bank_information() {
		$users = User::where('_id', '=', Session::get('UserId'))->select('_id', 'AccountHolderName', 'BankAccountNo', 'RoutingNo', 'BankName', 'PaypalId')->first();
		return view('User::detail.bank-information.bank_information', array('users' => $users));
	}

	/* Developer Name : Aakash tejwal
		     * Date : 19-08-2016
		     * About Function : This function is use for edit bank information
		     * Method : Post
		     * Lest Update :
		     * Update by :
		     *
	*/
	public function edit_bank_information() {
		$data['user'] = User::where('_id', '=', Session::get('UserId'))->select('_id', 'BankName', 'AccountHolderName', 'BankAccountNo', 'RoutingNo', 'BankName', 'PaypalId', 'BankCountryCode')->first();
		$data['country'] = CityStateCountry::where(['type' => 'Country'])
			->orderBy('Content', 'Asc')
			->get(['Content', '_id', 'ShortCode', 'CurrencyCode']);

		return view('User::detail.bank-information.edit_bank_information', $data);
	}

	/* Developer Name : Aakash tejwal
		     * Date : 19-08-2016
		     * About Function : This function is use for edit bank information
		     * Method : Post
		     * Lest Update :
		     * Update by :
		     *
	*/
	public function post_edit_bank_information(Request $request) {
		$response = ["success" => 0, "msg" => "Oops! Something went wrong."];

		$validation = Validator::make($request->all(), [
			'bank_country' => 'required',
			'bank_name' => 'required',
			'account_holder_name' => 'required',
			'account_no' => 'required',
			'routing_no' => 'required',
			'PaypalId' => 'email',
		]);

		if ($validation->fails()) {

			return Redirect::to("edit-bank-information")->withErrors($validation)->withInput();
		} else {
			$data = User::find(Session::get('UserId'));

			if (count($data) > 0) {
				$bank_country = json_decode(Input::get('bank_country'));
				if (empty($bank_country)) {

					return Redirect::to("edit-bank-information")->with('danger', 'Bank country is required.');
				}

				$legal_entity = [
					'type' => 'individual',
					'first_name' => trim($data->FirstName),
					'last_name' => trim($data->LastName),
					//'personal_id_number'    => $data->SSN
				];
				if (in_array(@$bank_country->Content, ['Canada', 'UK', 'USA'])) {
					$res = RSStripe::create_account([
						"legal_entity" => $legal_entity,
						"external_account" => [
							'object' => 'bank_account',
							'account_number' => trim(Input::get('account_no')),
							'routing_number' => trim(Input::get('routing_no')),
							/*'country'    => 'ca',
                            'currency'   => 'cad',*/
							'country' => @$bank_country->ShortCode,
							'currency' => @$bank_country->CurrencyCode,
							'AccountHolderName' => trim(Input::get('account_holder_name')),
						],
					], @$bank_country->ShortCode, $data->StripeBankId);

					if (!isset($res['id'])) {
						return Redirect::to("edit-bank-information")->with('danger', $res)
							->withInput();
					}

					$data->StripeBankId = $res['id'];
				}

				$data->BankCountryCode = $bank_country->ShortCode;
				$data->BankName = trim(Input::get('bank_name'));
				$data->AccountHolderName = trim(Input::get('account_holder_name'));
				$data->BankAccountNo = trim(Input::get('account_no'));
				$data->RoutingNo = trim(Input::get('routing_no'));
				$data->PaypalId = trim(Input::get('PaypalId'));

				if (in_array($data->ProfileStatus, ['step-two', 'step-third'])) {
					$data->ProfileStatus = 'complete';
					Session()->put(['Profile_Status' => 'complete']);
				}

				if ($data->save()) {
					$response = array(
						"success" => 1,
						"msg" => "Success! Bank information has been change successfully.",
					);
				}

			}
		}
		if ($response['success'] == 0) {
			return Redirect::to("edit-bank-information")->with('danger', $response['msg'])->withInput();
		}
		return Redirect::to("bank-information")->withSuccess($response['msg']);
	}

	public function posts_by_transporters() {
		return view('User::transporter.posts_by_transporters');
	}

	public function near_by_transporter(Request $request) {

		$data = ['lat' => 38.1769023, 'lng' => -107.272751];
		//print_r(session::get('latitude')); die;
		if (session::has('latitude')) {
			$data = [
				'lat' => floatval(session::get('latitude')),
				'lng' => floatval(session::get('longitude')),
			];
		}
		if (!empty(trim(Input::get('lng'))) && Input::get('lng') != 0) {
			$data['lat'] = floatval(Input::get('lat'));
			$data['lng'] = floatval(Input::get('lng'));
		}
		$where = [
			'_id' => ['$ne' => new MongoId(Session::get('UserId'))],
			'UserType' => 'both',
			'TransporterStatus' => 'active',
			'CurrentLocation.0' => ['$nin' => [0, '']],
		];

		$tp_ids = [];
		$transporter_id = Trips::where('TransporterId', '!=', new MongoId(Session::get('UserId')))->where('SourceDate', '>=', new MongoDate)->select('id', 'TransporterId')->GroupBY('TransporterId')->distinct()->get();

		if (count($transporter_id) > 0) {
			foreach ($transporter_id as $key) {
				$tp_ids[] = $key->TransporterId;
			}
		}
		$where['_id']['$in'] = $tp_ids;

		if (!empty(trim(Input::get('search')))) {
			$where['Name'] = new MongoRegex("/" . trim(Input::get('search')) . "/i");
		}
		if (!empty(trim(Input::get('search_address')))) {
			$where['user_address'] = new MongoRegex("/" . trim(Input::get('search_address')) . "/i");
		}

		$res = User::raw(function ($collection) use ($where) {
			return $collection->aggregate([
				[
					'$project' => [
						'user_address' => ['$concat' => [
							'$Street1', ' ', '$Street2', ' ', '$City', ' ', '$State', ' ', '$Country', ' ', '$ZipCode',
						]],
						'custom_address' => ['$concat' => [
							'$City', ' ', '$State', ' ', '$Country',
						]],
						'Image' => '$Image',
						'FirstName' => '$FirstName',
						'LastName' => '$LastName',
						'RatingCount' => '$RatingCount',
						'RatingByCount' => '$RatingByCount',
						'CurrentLocation' => '$CurrentLocation',
						'UserType' => '$UserType',
						'TransporterStatus' => '$TransporterStatus',

					],
				],
				[
					'$match' => $where,
				],

			]);

		});
		$data['users'] = $res['result'];

		$data['paginationUrl'] = "pagination/near-by-transporter";
		$data['postValue'] = "&search_name=" . Input::get('search_name') . "&search_address=" . Input::get('search_address');
		return view('User::list.near-by-transporter', $data);
	}

	public function new_request() {
		$data['paginationUrl'] = "Paginate@new_request";
		$data['users'] = User::where(['RequestType' => 'ready'])->select()->limit(5)->get();
		return view('User::list.new-request', $data);
	}

	/* Developer Name : Aakash tejwal
		     * Date : 19-08-2016
		     * About Function : This function is use for Change Password
		     * Method : Post
		     * Lest Update :
		     * Update by :
		     *
	*/
	public function changepassword(Request $request) {

		$user_email = User::where(array('_id' => Session::get('UserId')))->select('Email', 'Password')->first();
		if (count($user_email) > 0) {
			$userId = Session::get('UserId');
			$v = Validator::make($request->all(), [
				'current_password' => 'required',
				'new_password' => 'required|different:current_password|min:6',
				'confirm_password' => 'required',
			]);
			if ($v->fails()) {
				return Redirect::to('change-password')->withErrors($v);

			} else {
				if ($user_email->Password != md5(trim(Input::get('current_password')))) {
					return Redirect::to(url('change-password'))->with('danger', 'Failed ! Your current password is wrong.');
				} else {
					User::where(array('_id' => Session::get('UserId')))
						->update(array('Password' => md5(trim(Input::get('new_password')))));

					return Redirect::to("change-password")->withSuccess('Success! Password has been changed successfully.');

				}
			}

		}
		return Redirect::to(url('change-password'))->with('danger', 'Failed ! Something went wrong.');
	}

	public function buy_for_me_list() {
		return view('User::list.buy-for-me');
	}

	public function my_request() {
		$data['paginationUrl'] = "pagination/my-request";
		$data['postValue'] = "&search=" . Input::get('search') . "&search_date=" . Input::get('search_date') . "&Status=" . Input::get('Status');

		return view('User::list.my-request', array('data' => $data));
	}
	public function trip_detail($id) {
		$data['paginationUrl'] = "pagination/trip-detail/{$id}";
		$data['postValue'] = "&search=" . Input::get('search');
		return view('User::list.trip-detail', $data);
	}

	public function delivery_detail2() {
		return view('User::detail.delivery-detail');
	}
	public function email_status(Request $request) {
		$validation = Validator::make($request->all(), [
			'EmailStatus' => 'required',
			'NoficationStatus' => 'required',
			'consolidate_item' => 'required',
			//'TPSetting' => 'required',

		]);
		if ($validation->fails()) {
			$validation->errors()->add('field', trans('Something is wrong'));
			return $validation->errors();
		} else {
			Session()->put(['consolidate_item' => Input::get('consolidate_item')]);
			//Session()->put(['TPSetting' => Input::get('TPSetting')]);

			$update['EmailStatus'] = Input::get('EmailStatus');
			$update['NoficationStatus'] = Input::get('NoficationStatus');
			$update['consolidate_item'] = Input::get('consolidate_item');
			//$update['TPSetting'] = Input::get('TPSetting');
			$response = User::where(array('_id' => Session()->get('UserId')))->update($update);
		}
		return response()->json($response);
	}
	public function prepare_request() {

		$data['transporter_data'] = array();

		if (!Input::get('id') == '') {
			$data['transporter_data'] = Trips::where(array('_id' => Input::get('id')))->first();

			if (!count($data['transporter_data']) > 0) {
				return redirect('near-by-transporter');
			}

			$data['user_data'] = User::where(array('_id' => $data['transporter_data']->TransporterId))->first();

			$data['category2'] = Category::where(['type' => 'Category', 'Status' => 'Active'])->whereIn('TravelMode', ['ship', 'air'])->whereIn('Content', $data['transporter_data']->SelectCategory)->get(['TravelMode', 'Content']);

		}

		$data['RequestType'] = Input::get("delivery");
		$data['country'] = CityStateCountry::where(['type' => 'Country', 'Status' => 'Active'])
			->orderBy('Content', 'Asc')
			->get(['_id', 'Content', 'state_available']);
		$data['category'] = Category::where(['type' => 'Category', 'Status' => 'Active'])->whereIn('TravelMode', ['ship', 'air'])->get(['TravelMode', 'Content']);

		return view('User::Buy.prepare-request', $data);
	}

	public function currency_conversion($shipcost) {

		$data = ['cost' => 0, 'ghanian_cost' => 0, 'philipins_cost' => 0, 'uk_cost' => 0, 'canadian_cost' => 0];
		$currency = Currency::where(['Status' => 'Active'])->select('CurrencyRate', 'FormatedText', 'CurrencyCode')->get();

		if ($currency) {
			foreach ($currency as $key) {
				if ($key->CurrencyCode == 'GHS') {
					$data['ghanian_cost'] = str_replace('[AMT]', number_format($shipcost * $key->CurrencyRate, 2), $key->FormatedText);
				} else if ($key->CurrencyCode == 'CAD') {
					$data['canadian_cost'] = str_replace('[AMT]', number_format($shipcost * $key->CurrencyRate, 2), $key->FormatedText);
				} else if ($key->CurrencyCode == 'PHP') {
					$data['philipins_cost'] = str_replace('[AMT]', number_format($shipcost * $key->CurrencyRate, 2), $key->FormatedText);
				} else if ($key->CurrencyCode == 'GBP') {
					$data['uk_cost'] = str_replace('[AMT]', number_format($shipcost * $key->CurrencyRate, 2), $key->FormatedText);
				}
			}
		}
		return $data;

	}

	public function request_card_list($id, Request $request) {
		$data['admin_msg'] = '';
		$config_data = Configuration::find(ConfigId);
		if (count($config_data) > 0) {
			$data['admin_msg'] = $config_data->message;
		}

		$del_request = Deliveryrequest::where(array('_id' => $id))->first();
		if (!count($del_request) > 0) {
			return redirect('/my-request');
		}
		if ($del_request->Status == 'ready' && $del_request->RequestType!="delivery") {
			return redirect('delivery-details/' . $request->segment(2));
		}
		$data['cards'] = array();
		$del_request->TotalCost = $del_request->TotalCost + $del_request->after_update_difference;
		$data['info'] = $del_request;
		$user = User::where(array('_id' => Session()->get('UserId')))->select('StripeId','Default_Currency')->first();
		if (count($user) > 0) {
			$currency_conversion = $this->currency_conversion($data['info']->TotalCost);

			if($user->Default_Currency == 'CAD'){
				$data['user_currency'] = $currency_conversion['canadian_cost'];
			}else if($user->Default_Currency == 'PHP'){
				$data['user_currency'] = $currency_conversion['philipins_cost'];
			}else if($user->Default_Currency == 'GBP'){
				$data['user_currency'] = $currency_conversion['uk_cost'];
			}else{
				$data['user_currency'] = $currency_conversion['ghanian_cost'];
			}
			$data['cards'] = Payment::card_list($user->StripeId);
		}
		return view('User::Buy.card_list', $data);
	}

	public function buyforme_payment($id) {
		$data['admin_msg'] = '';
		$config_data = Configuration::find(ConfigId);
		if (count($config_data) > 0) {
			$data['admin_msg'] = $config_data->message;
		}
		$request = Deliveryrequest::where(array('_id' => $id))->first();
		if (!count($request) > 0) {

			return redirect('/my-request');
		}
		$data['cards'] = array();
		$request->TotalCost = $request->TotalCost + $request->after_update_difference;
		$data['request_data'] =  $request;
		$user = User::where(array('_id' => Session()->get('UserId')))->select('StripeId','Default_Currency')->first();
		if (count($user) > 0) {
			$currency_conversion = $this->currency_conversion($request->TotalCost);

			if($user->Default_Currency == 'CAD'){
				$data['user_currency'] = $currency_conversion['canadian_cost'];
			}else if($user->Default_Currency == 'PHP'){
				$data['user_currency'] = $currency_conversion['philipins_cost'];
			}else if($user->Default_Currency == 'GBP'){
				$data['user_currency'] = $currency_conversion['uk_cost'];
			}else{
				$data['user_currency'] = $currency_conversion['ghanian_cost'];
			}
			$data['cards'] = Payment::card_list($user->StripeId);
		}
		return view('User::Buy.buyforme_request_card', $data);
	}

	public function buyforme_request_card_pay($requestid, $id, Request $request) {


		$update['Status'] = 'ready';
		$userinfo = User::find(Session()->get('UserId'));
		if (count($userinfo) > 0) {

			$where = [
				'RequesterId' => new MongoId(Session()->get('UserId')),
				'_id' => $requestid,
				'Status' => 'pending',
			];

			$data = Deliveryrequest::where($where)->whereIn('Status', ['pending', 'ready'])->first();

			if(isset($data->request_version) &&  $data->request_version == 'new'){

				$array = $data['ProductList'];

				foreach ($array as $key) {
					$package_id[] = $key['package_id'];
				}

				if (count($data) > 0) {

					$porductlist = $data->ProductList;
					foreach ($porductlist as $key => $val) {
						$porductlist[$key]['status'] = 'ready';
						$porductlist[$key]['PaymentStatus'] = 'yes';
						$porductlist[$key]['inform_mail_sent'] = 'no';
					}
					$cost = $data->TotalCost + $data->after_update_difference;
					$res = Payment::capture($userinfo, $cost, $request->segment(3),true);
					if (isset($res['id'])) {
						// $Email = new NewEmail();
						if ($userinfo->EmailStatus == "on") {
							/*send_mail('587748397ac6f63c1a8b456d', [
								"to" => $userinfo->Email,
								"replace" => [
									"[USERNAME]" => $userinfo->Name,
									"[PACKAGETITLE]" => ucfirst($data->ProductTitle),
									"[SOURCE]" => $data->PickupFullAddress,
									"[DESTINATION]" => $data->DeliveryFullAddress,
									"[PACKAGENUMBER]" => $data->PackageNumber,
									"[SHIPPINGCOST]" => $data->ShippingCost,
									"[TOTALCOST]" => $cost,
									"[PRFEE]"=> @$data->ProcessingFees,
								],
							]);*/

							$cron_mail = [
								"USERNAME" => ucfirst($userinfo->Name),
								"PACKAGETITLE" => ucfirst($data->ProductTitle),
								"PACKAGEID" => $data->PackageNumber,
								"SOURCE" => $data->PickupFullAddress,
								"DESTINATION" => $data->DeliveryFullAddress,
								"PACKAGENUMBER" => $data->PackageNumber,
								"SHIPPINGCOST" => $data->ShippingCost,
								"TOTALCOST" => $cost,
								"email_id" => "587748397ac6f63c1a8b456d",
								"email" => $userinfo->Email,
								"status" => "ready"
							];
							SendMail::insert($cron_mail);
						}

						if (Deliveryrequest::where($where)->update([
							'ProductList' => $porductlist,
							'Status' => 'ready',
							'StripeCard' => $request->segment(3),
							//'ProcessingFees' => $data->ProcessingFees,
							//'TotalCost' => $data->TotalCost,
							//'BeforePurchaseTotalCost' => $data->TotalCost,
							//'PromoCode' => trim(Input::get('promocode')),
							'StripeChargeId' => $res['id'],
							"need_to_pay" => 0
						])) {

							$admin = Setting::where(['_id' => '563b0e31e4b03271a097e1ca'])->first();
							if (count($admin) > 0) {

								/*send_mail('583967f17ac6f6b21a8b4567', [
									"to" => $admin->SupportEmail,
									"replace" => [
										"[PACKAGETITLE]" => ucfirst($data->ProductTitle),
										"[USERNAME]" => $userinfo->Name,
										"[PACKAGEID]" => $data->PackageNumber,
										"[DESTINATION]" => $data->DeliveryFullAddress,
										"[DELIVERY_TYPE]" => 'Buy for Me',
										"[DISTANCE]" => sprintf('%s Mile', number_format($data->distance, 2)),
										"[EMAIL]" => $userinfo->Email,
										"[USERTYPE]" => ucfirst($userinfo->UserType),
										"[TITLE]" => ucfirst($data->ProductTitle),
									],
								]);*/

								$cron_mail2 = [
									"USERNAME" => ucfirst($userinfo->Name),
									"PACKAGETITLE" => ucfirst($data->ProductTitle),
									"PACKAGEID" => $data->PackageNumber,
									"SOURCE" => $data->PickupFullAddress,
									"DESTINATION" => $data->DeliveryFullAddress,
									"PACKAGENUMBER" => $data->PackageNumber,
									"SHIPPINGCOST" => $data->ShippingCost,
									"TOTALCOST" => $cost,
									"email_id" => "583967f17ac6f6b21a8b4567",
									"email" => $admin->SupportEmail,
									"status" => "ready"
								];
								SendMail::insert($cron_mail2);
							}

							Notification::insert([
								"NotificationTitle" => trans('lang.BUYFORME_REQUEST_CREATED_TITLE'),
								"NotificationMessage" => sprintf(trans('lang.BUYFORME_REQUEST_CREATED'), ucfirst($data->ProductTitle), $data->PackageNumber),
								"NotificationReadStatus" => 0,
								"location" => "buy_for_me",
								"locationkey" => (String) @$data->_id,
								"Date" => new MongoDate(),
								"GroupTo" => "Admin",
							]);

							RequestCard::Insert([
		                        'request_id' => (String) @$data->_id,
		                        'item_id' => "",
		                        'card_id' => trim($request->segment(3)),
		                        'type' => 'buy_for_me',
		                        'payment' => $cost,
		                        'brand' => @$res['source']['brand'],
		                        'last4' => @$res['source']['last4'],
		                        'description' => "Payment done for request Title: '" . $data->ProductTitle . "' Package Id:" . $data->PackageNumber . ".",
		                        "EnterOn" => new MongoDate(),
		                    ]);

		                    Transaction::insert(array(
								"SendById" => $userinfo->_id,
								"SendByName" => @$userinfo->Name,
								"SendToId" => "aquantuo",
								"RecieveByName" => "Aquantuo",
								"Description" => "Amount deposited against delivery request for {$data->ProductTitle}, PackageId: {$data->PackageNumber} .",
								"Credit" => $cost,
								"Debit" => "",
								"Status" => "completed",
								"TransactionType" => "buy_for_me",
								'request_id'=> $requestid,
								'item_id'=>'',
								'transaction_id'=> $res['id'],
								"EnterOn" => new MongoDate(),
							));

							/* Activity Log */
							if ($data->Status == 'pending') {

								$array = $data->ProductList;

								foreach ($array as $key) {

									/* Activity Log */
									$insertactivity = [
										'request_id' => $data->_id,
										'request_type' => $data->RequestType,
										'PackageNumber' => $data->PackageNumber,
										'item_id' => $key['_id'],
										'package_id' => $key['package_id'],
										'item_name' => $key['product_name'],
										'log_type' => 'request',
										'message' => 'Payment successful.',
										'status' => 'ready',
										'action_user_id'=> Session::get('UserId'),
										'EnterOn' => new MongoDate(),
									];
									Activitylog::insert($insertactivity);
								}
							}
							return Redirect::to("buy-for-me-detail/{$requestid}")->with('success', "Success! Your Buy for me request has been successfully submitted. Aquantuo will review and proceed with next steps immediately.");
							//Your Buy for me request has been successfully submitted. Aquantuo will review and proceed with next steps immediately.
						}
					} else {
						return Redirect::to("buyforme-payment/{$requestid}")->with('danger', $res);
					}
				}
			}else{
				if (count($data) > 0) {
					$porductlist = $data->ProductList;
	                foreach ($porductlist as $key => $val) {
	                    $porductlist[$key]['status'] = 'ready';

	                    /* Start discount section  */
	                    if (!empty(trim(Input::get('promocode')))) {
	                        $res = Promo::get_validate(Input::get('promocode'),
	                            $val['shippingCost'],
	                            $val['shippingCost'],
	                            Session::get('UserId')
	                        );
	                        if ($res['success'] == 1) {

	                            $data->discount += $res['discount'];
	                            $porductlist[$key]['discount'] = $res['discount'];
	                        }
	                    }

	                    /* End discount section  */
	                }

	                $res = Payment::capture($userinfo, (($data->TotalCost - $data->discount)), $request->segment(3),false);
	                if (isset($res['id'])) {

	                    if ($userinfo->EmailStatus == "on") {
	                        send_mail('587748397ac6f63c1a8b456d', [
	                            "to" => $userinfo->Email,
	                            "replace" => [
	                                "[USERNAME]" => $userinfo->Name,
	                                "[PACKAGETITLE]" => ucfirst($data->ProductTitle),
	                                "[SOURCE]" => $data->PickupFullAddress,
	                                "[DESTINATION]" => $data->DeliveryFullAddress,
	                                "[PACKAGENUMBER]" => $data->PackageNumber,
	                                "[SHIPPINGCOST]" => $data->ShippingCost,
	                                "[TOTALCOST]" => $data->TotalCost,
	                            ],
	                        ]);
	                    }

	                    if (Deliveryrequest::where($where)->update([
	                        'ProductList' => $porductlist,
	                        'Status' => 'ready',
	                        'StripeCard' => $request->segment(3),
	                        'discount' => $data->discount,
	                        'ProcessingFees' => $data->ProcessingFees,
	                        'TotalCost' => $data->TotalCost - $data->discount,
	                        'BeforePurchaseTotalCost' => $data->TotalCost - $data->discount,
	                        'PromoCode' => trim(Input::get('promocode')),
	                        'StripeChargeId' => $res['id'],
	                    ])) {

	                        $admin = Setting::where(['_id' => '563b0e31e4b03271a097e1ca'])->first();
	                        if (count($admin) > 0) {

	                            send_mail('583967f17ac6f6b21a8b4567', [
	                                "to" => $admin->SupportEmail,
	                                "replace" => [
	                                    "[PACKAGETITLE]" => ucfirst($data->ProductTitle),
	                                    "[USERNAME]" => $userinfo->Name,
	                                    "[PACKAGEID]" => $data->PackageNumber,
	                                    "[DESTINATION]" => $data->DeliveryFullAddress,
	                                    "[DELIVERY_TYPE]" => 'Buy for Me',
	                                    "[DISTANCE]" => sprintf('%s Mile', number_format($data->distance, 2)),
	                                    "[EMAIL]" => $userinfo->Email,
	                                    "[USERTYPE]" => ucfirst($userinfo->UserType),
	                                    "[TITLE]" => ucfirst($data->ProductTitle),
	                                ],
	                            ]);

	                        }

	                        Notification::insert([
	                            "NotificationTitle" => trans('lang.BUYFORME_REQUEST_CREATED_TITLE'),
	                            "NotificationMessage" => sprintf(trans('lang.BUYFORME_REQUEST_CREATED'), ucfirst($data->ProductTitle), $data->PackageNumber),
	                            "NotificationReadStatus" => 0,
	                            "location" => "buy_for_me",
	                            "locationkey" => (String) @$data->_id,
	                            "Date" => new MongoDate(),
	                            "GroupTo" => "Admin",
	                        ]);

	                        return Redirect::to("buy-for-me-detail/{$requestid}")->with('success', "Success! Your Buy for me request has been successfully submitted. Aquantuo will review and proceed with next steps immediately.");
	                        //Your Buy for me request has been successfully submitted. Aquantuo will review and proceed with next steps immediately.
	                    }
	                } else {
	                    return Redirect::to("buyforme-payment/{$requestid}")->with('danger', $res);
	                }
	            }
			}
		}
		return Redirect::to("buyforme-payment/{$requestid}")->with('danger', "Fail! Something is worng");
	}

	public function online_request_card_pay($requestid, $id, Request $request) {
		
		$userinfo = User::find(Session()->get('UserId'));
		if (count($userinfo) > 0) {

			$where = [
				'RequesterId' => new MongoId(Session()->get('UserId')),
				'_id' => $requestid,
				'Status' => 'pending',
			];

			$data = Deliveryrequest::where($where)->first();
			if (count($data) > 0) {

				if(isset($data->request_version) &&  $data->request_version == 'new'){
					$porductlist = $data->ProductList;
					$email_sent = 'no';
					$andwhere = [
						'RequesterId' => new MongoId(Session()->get('UserId')),
						'Status' => 'pending',
						'_id' => $requestid,
					];
					$status = 'ready';
					foreach ($porductlist as $key => $val) {
						$porductlist[$key]['status'] = $status;
						$porductlist[$key]['inform_mail_sent'] = $email_sent;
						$porductlist[$key]['PaymentStatus'] = 'yes';
					}
					$cost = $data->TotalCost+$data->after_update_difference;
					$data->BeforePurchaseTotalCost = $cost;
					$res = Payment::capture($userinfo, $cost, $request->segment(3),true);
					if (!isset($res['id'])) {
						return Redirect::to("online-payment/{$requestid}")
							->with('danger', $res);
					}
					$data->Status = $status;
					$data->ProductList = $porductlist;
					$data->PaymentDate = new MongoDate();
					$data->need_to_pay = 0;

					if ($data->save()) {
						/* Activity Log */
						$array = $data->ProductList;
						foreach ($array as $key) {
							/* Activity Log */
							$insertactivity = [
								'request_id' => $data->_id,
								'request_type' => $data->RequestType,
								'PackageNumber' => $data->PackageNumber,
								'item_id' => $key['_id'],
								'package_id' => $key['package_id'],
								'item_name' => $key['product_name'],
								'log_type' => 'request',
								'message' => 'Payment successful.',
								'status' => 'ready',
								'action_user_id'=> Session::get('UserId'),
								'EnterOn' => new MongoDate(),

							];
							Activitylog::insert($insertactivity);
						}

						RequestCard::Insert([
	                        'request_id' => (String) @$data->_id,
	                        'item_id' => "",
	                        'card_id' => trim($request->segment(3)),
	                        'type' => 'online',
	                        'payment' => $cost,
	                        'brand' => @$res['source']['brand'],
	                        'last4' => @$res['source']['last4'],
	                        'description' => "Payment done for request Title: '" . $data->ProductTitle . "' Package Id:" . $data->PackageNumber . ".",
	                        "EnterOn" => new MongoDate(),
	                    ]);

	                    Transaction::insert(array(
							"SendById" => $userinfo->_id,
							"SendByName" => @$userinfo->Name,
							"SendToId" => "aquantuo",
							"RecieveByName" => "Aquantuo",
							"Description" => "Amount deposited against delivery request for {$data->ProductTitle}, PackageId: {$data->PackageNumber} .",
							"Credit" => $cost,
							"Debit" => "",
							"Status" => "completed",
							"TransactionType" => "online",
							'request_id'=> $requestid,
							'item_id'=>'',
							'transaction_id'=> $res['id'],
							"EnterOn" => new MongoDate(),
						));

						$this->online_notify($data, $userinfo);

						return Redirect::to("online-request-detail/{$requestid}")->with('success', "Success! Your Online Purchase request has been successfully submitted. Aquantuo will review and proceed with next steps immediately.");
					}
				}else{
					$porductlist = $data->ProductList;
	                if ($data->Status == 'not_purchased') {
	                    $andwhere = [
	                        'RequesterId' => new MongoId(Session()->get('UserId')),
	                        '_id' => $requestid,
	                    ];
	                    $status = 'paid';
	                } else {
	                    $andwhere = [
	                        'RequesterId' => new MongoId(Session()->get('UserId')),
	                        'Status' => 'pending',
	                        '_id' => $requestid,
	                    ];
	                    $status = 'ready';
	                }
	                foreach ($porductlist as $key => $val) {
	                    /* Start discount section  */
	                    if ($data->Status != 'not_purchased') {
	                        if (!empty(trim(Input::get('promocode')))) {
	                            $res = Promo::get_validate(Input::get('promocode'),
	                                $val['shippingCost'],
	                                $val['shippingCost'],
	                                Session::get('UserId')
	                            );
	                            if ($res['success'] == 1) {

	                                $data->discount += $res['discount'];
	                                $porductlist[$key]['discount'] = $res['discount'];
	                            }
	                        }
	                    }

	                    $porductlist[$key]['status'] = $status;
	                    $porductlist[$key]['inform_mail_sent'] = 'no';
	                }
	                if ($data->Status == 'not_purchased') {
	                    $cost = $data->TotalCost - $data->BeforePurchaseTotalCost;
	                } else {
	                    $cost = $data->TotalCost - $data->discount;
	                    $data->BeforePurchaseTotalCost = $cost;
	                }

	                $res = Payment::capture($userinfo, $cost, $request->segment(3),false);

	                if (!isset($res['id'])) {
	                    return Redirect::to("online-payment/{$requestid}")
	                        ->with('danger', $res);
	                }

	                if ($data->Status == 'not_purchased') {
	                    $data->StripeChargeId2 = $res['id'];
	                } else {
	                    $data->StripeChargeId = $res['id'];
	                    $data->PaymentStatus = 'capture';
	                    $data->PromoCode = Input::get('promocode');
	                    $data->TotalCost = $data->TotalCost - $data->discount;
	                }

	                $data->Status = $status;
	                $data->ProductList = $porductlist;
	                $data->need_to_pay = 0;
	                $data->PaymentDate = new MongoDate();

	                if ($data->save()) {

	                    $this->online_notify($data, $userinfo);

	                    return Redirect::to("online-request-detail/{$requestid}")->with('success', "Success! Your Online Purchase request has been successfully submitted. Aquantuo will review and proceed with next steps immediately.");
	                }
				}
			}
		}
		return Redirect::to("online-payment/{$requestid}")->with('danger', "Fail! Something is worng");
	}
	public function online_notify($request, $user_data) {
		$Email = new NewEmail();
		if ($user_data->EmailStatus == "on") {
			$Email->send_mail('587750587ac6f6e61c8b4567', [
				"to" => $user_data->Email,
				"replace" => [
					"[USERNAME]" => $user_data->Name,
					"[PACKAGETITLE]" => ucfirst($request->ProductTitle),
					"[DESTINATION]" => $request->DeliveryFullAddress,
					"[SHIPPINGCOST]" => $request->ShippingCost,
					"[TOTALCOST]" => $request->TotalCost,
				],
			]);
		}

		Notification::insert([
			"NotificationTitle" => trans('lang.ONLINE_REQUEST_CREATED_TITLE'),
			"NotificationMessage" => sprintf(trans('lang.ONLINE_REQUEST_CREATED_MSG'), ucfirst($request->ProductTitle), $request->PackageNumber),
			"NotificationUserId" => [],
			"NotificationReadStatus" => 0,
			"location" => "online",
			"locationkey" => (String) @$request->_id,
			"Date" => new MongoDate(),
			"GroupTo" => "Admin",
		]);

		$admin = Setting::where(['_id' => '563b0e31e4b03271a097e1ca'])->first();
		if (count($admin) > 0) {
			$Email->send_mail('587751d47ac6f6e61c8b4568', [
				"to" => $admin->SupportEmail,
				"replace" => [
					"[USERNAME]" => $user_data->Name,
					"[DESTINATION]" => $request->DeliveryFullAddress,
					"[SHIPPINGCOST]" => $request->ShippingCost,
					"[TOTALCOST]" => $request->TotalCost,
					"[REQUEST]" => 'Online',
					"[EMAIL]" => $user_data->Email,
					"[TITLE]" => ucfirst($request["ProductTitle"]),
					"[USERTYPE]" => $user_data->UserType,
					"[DEVICETYPE]" => $user_data->DeviceType,
					"[PACKAGETITLE]" => ucfirst($request->ProductTitle),
					"[PACKAGEID]" => ucfirst($request["PackageNumber"]),
				],
			]);
		}

	}

	public function transporter() {
		return view('User::detail.transporter');
	}
	public function pay_now($requestid, $id, Request $request) {

		$update['Status'] = 'ready';

		$where = [
			'RequesterId' => new MongoId(Session()->get('UserId')),
			'_id' => $requestid,
		];
		$user = User::where(array('_id' => Session()->get('UserId')))->first();
		$del_detail = Deliveryrequest::where($where)->whereIn('Status', ['pending', 'ready'])->first();

		if (count($del_detail) <= 0 || count($user) <= 0) {
			return Redirect::to("process-card-list/{$requestid}")->with('danger', "Fail! Something is worng");
		}
		if (count($del_detail) > 0 && count($user) > 0) {

			if (!empty($del_detail->StripeChargeId)) {
				RSStripe::refund($del_detail->StripeChargeId);
			}
			$res = Payment::capture($user, $del_detail->TotalCost, $request->segment(3));

			if (isset($res['id'])) {
				$update['StripeChargeId'] = $res['id'];

				Transaction::insert(array(
					"SendById" => (String) $user->_id,
					"SendByName" => @$user->RequesterName,
					"SendToId" => "aquantuo",
					"RecieveByName" => "Aquantuo",
					"Description" => "Amount deposited against delivery request for {$del_detail->ProductTitle}, PackageId: {$del_detail->PackageNumber} from {$del_detail->PickupFullAddress} to {$del_detail->DeliveryFullAddress}",
					"Credit" => floatval($del_detail->TotalCost),
					"Debit" => "",
					"Status" => "pending",
					"TransactionType" => "delivery_request",
					"EnterOn" => new MongoDate(),
				));

				if (Deliveryrequest::where($where)->update($update)) {

					Reqhelper::send_notification_to_tp(['id' => $requestid,'request_type' => Input::get('request_type')]);

					Notification::Insert([
						array(
							"NotificationTitle" => trans('lang.SEND_CREATE_TITLE'),
							"NotificationMessage" => sprintf(trans('lang.SEND_CREATE_MSG'), $del_detail->ProductTitle, $del_detail->PackageNumber),
							"NotificationUserId" => array(),
							"NotificationReadStatus" => 0,
							"location" => "request_detail",
							"locationkey" => (string) $requestid,
							"Date" => new MongoDate(),
							"GroupTo" => "Admin",
						),
					]);

					if ($user->EmailStatus == "on") {
						send_mail('5694a10f5509251cd67773eb', [
							"to" => $user->Email,
							"replace" => [
								"[USERNAME]" => $user->Name,
								"[PACKAGETITLE]" => ucfirst($del_detail->ProductTitle),
								"[PACKAGEID]" => $del_detail->PackageId,
								"[SOURCE]" => $del_detail->PickupFullAddress,
								"[DESTINATION]" => $del_detail->DeliveryFullAddress,
								"[PACKAGENUMBER]" => $del_detail->PackageNumber,
								"[SHIPPINGCOST]" => $del_detail->ShippingCost,
								"[TOTALCOST]" => $del_detail->TotalCost,
							],
						]);
					}

					$setting = Setting::find('563b0e31e4b03271a097e1ca');
					if (count($setting) > 0) {

						send_mail('56cd3e1f5509251cd677740c', [
							"to" => trim($setting->SupportEmail),
							"replace" => [
								"[USERNAME]" => $user->Name,
								"[PACKAGETITLE]" => ucfirst($del_detail->ProductTitle),
								"[PACKAGEID]" => $del_detail->PackageId,
								"[SOURCE]" => $del_detail->PickupFullAddress,
								"[DESTINATION]" => $del_detail->DeliveryFullAddress,
								"[PACKAGENUMBER]" => $del_detail->PackageNumber,
								"[SHIPPINGCOST]" => $del_detail->ShippingCost,
								"[TOTALCOST]" => $del_detail->TotalCost,
								"[REQUEST]" => 'Delivery',
							],
						]);

					}

					if (empty($del_detail->TransporterId)) {

						return Redirect::to("delivery-details/{$requestid}")->with('success', "Success! Your item has been successfully posted. Once a transporter reviews and accepts it, you will be notified.");

					} else {

						$tpdetail = User::where('_id', $del_detail->TransporterId)->first();
						if (count($tpdetail) > 0) {
							$tp_template_id = '569e16b65509251cd67773f1';

							if (Input::get('request_type') == 'update') {
								$tp_template_id = '569f82bc5509251cd67773f2';
							}

							if ($tpdetail->EmailStatus == "on") {
								send_mail($tp_template_id, [
									"to" => $tpdetail->Email,
									"replace" => [
										"[SENDERNAME]" => @$del_detail->RequesterName,
										"[PACKAGEID]" => $del_detail->PackageNumber,
										"[PACKAGETITLE]" => ucfirst($del_detail->ProductTitle),
										"[DESTINATION]" => $del_detail->DeliveryFullAddress,
										"[SOURCE]" => $del_detail->PickupFullAddress,
										"[TOTALCOST]" => $del_detail->TotalCost,
										"[USERNAME]" => $tpdetail->Name,
									],
								]
								);
							}
						}

						return Redirect::to("delivery-details/{$requestid}")
							->with('Success! Your item has been successfully posted.');
					}
				} else {
					return Redirect::to("process-card-list/{$requestid}")->with('danger', "Fail! Something is wrong");
				}
			} else {
				return Redirect::to("process-card-list/{$requestid}")->with('danger', "Fail! " . $res);
			}
		}
	}

	public function online_request_detail($id) {
		$data['item'] = Deliveryrequest::where(array('_id' => $id, 'RequesterId' => new MongoId(Session()->get('UserId'))))->first();

		if (count($data['item']) > 0) {
			$tpid = [];
			foreach ($data['item']->ProductList as $ProductList) {
				$tpid[] = $ProductList['tpid'];
			}
			$data['user_data'] = User::whereIn('_id', $tpid)->get(['_id', 'Image', 'RatingCount', 'RatingByCount', 'Name']);
			$data['activitylog'] = Activitylog::where(['request_id'=>$id])->select('_id', 'package_id')->get();

			$publish_date = strtotime('03/08/2018');
			$current_date = $data['item']->EnterOn;
			$data['updated_data'] = Itemhistory::where(['request_id'=>$id])->get();
			// echo "<pre>";
			// print_r($data);
			if(isset($data['item']->request_version) && $data['item']->request_version == 'new'){
				$data['request_data'] = $data['item'];
				return view('User::detail.new_detail.requester_online_detail', $data);
			}else {
				return view('User::detail.online_request', $data);
			}
		} else {
			return redirect('my-request');
		}

	}
	/* Developer Name : Aakash tejwal
		     * Date : 19-08-2016
		     * About Function : This function is for display my-profile
		     * Method : Post
		     * Lest Update :
		     * Update by :
		     *
	*/
	public function my_profile() {
		$users = User::where('_id', '=', Session::get('UserId'))->select('_id', 'FirstName', 'LastName', 'Email', 'Country', 'CountryCode', 'PhoneNo', 'Image', 'Street1', 'Street2', 'City', 'State', 'ZipCode', 'Age', 'SSN', 'IDProof', 'UserType')->get();
		return view('User::detail.my-account.my-profile', array('users' => $users));
	}

	/* Developer Name : Aakash tejwal
		     * Date : 19-08-2016
		     * About Function : This function is for edit my-profile
		     * Method : Post
		     * Lest Update :
		     * Update by :
		     *
	*/
	public function post_edit_profile(Request $request) {
		//dd($request->all());
		$user_info = User::find(Session::get('UserId'));
		$messages = [
		    'state10.required' => 'The state field is required. ',
		];
		$v = Validator::make($request->all(), [
			'first_name' => 'required|Min:3|Max:80',
			'last_name' => 'required|Min:3|Max:80',
			'phoneno' => 'required|Numeric',
			'user_image' => 'mimes:jpeg,jpg,png',
			'address' => 'required',
			'country_code' => 'required|Numeric',
			'state10'  => 'required','city'  => 'required',
		],$messages);
		//dd($v->errors());
		if ($v->fails()) {
			return Redirect::to("edit-profile")->withErrors($v)->withInput(Input::get());
		} else {

			$old_data = "";
			$update_data = "";
			if ($user_info->user_address_id != '') {
				Address::where(['_id' => $user_info->user_address_id])->delete();
			}
			$add_user_address = $this->add_user_address(Input::get('address'), json_decode(Input::get('country')), json_decode(Input::get('state10')), json_decode(Input::get('city')), Input::get('zipcode'));

			if ($add_user_address != 1) {
				return Redirect::to("edit-profile")->with('danger', 'We are unable to find your location. Please  correct it.');
			}

			$obj_source_country = json_decode(Input::get('country'));
			$obj_desti_country = json_decode(Input::get('state10'));
			$obj_source_city = json_decode(Input::get('city'));

			if ($user_info->FirstName != Input::get('first_name')) {

				$old_data .= "<br/> <strong>First name: </strong>" . ucfirst($user_info->FirstName);
				$update_data .= "<br/> <strong>First name: </strong>" . ucfirst(Input::get('first_name'));

			}if ($user_info->LastName != Input::get('last_name')) {

				$old_data .= "<br/> <strong>Last name: </strong>" . ucfirst($user_info->LastName);
				$update_data .= "<br/> <strong>Last name: </strong>" . ucfirst(Input::get('last_name'));
				//echo $old_data;die;

			}if ($user_info->CountryCode != Input::get('country_code')) {

				$old_data .= "<br/> <strong>Country code : </strong>" . $user_info->CountryCode;
				$update_data .= "<br/> <strong>Country code : </strong>" . Input::get('country_code');

			}if ($user_info->PhoneNo != Input::get('phoneno')) {

				$old_data .= "<br/> <strong>Phone number: </strong>" . $user_info->PhoneNo;
				$update_data .= "<br/> <strong>Phone number: </strong>" . Input::get('phoneno');

			}if ($user_info->AlternateCCode != Input::get('alternet_country_code')) {

				$old_data .= "<br/> <strong> Alternate country code : </strong>" . $user_info->AlternateCCode;
				$update_data .= "<br/> <strong> Alternate country code : </strong>" . Input::get('alternet_country_code');

			}if ($user_info->AlternatePhoneNo != Input::get('alternet_phoneno')) {

				$old_data .= "<br/> <strong>Alternate phone number: </strong>" . $user_info->AlternatePhoneNo;
				$update_data .= "<br/> <strong>Alternate phone number: </strong>" . Input::get('alternet_phoneno');

			}if ($user_info->Street1 != Input::get('address')) {

				$old_data .= "<br/> <strong>Address 1: </strong>" . ucfirst($user_info->Street1);
				$update_data .= "<br/> <strong>Address 1: </strong>" . ucfirst(Input::get('address'));

			}if ($user_info->Street2 != Input::get('address2')) {

				$old_data .= "<br/> <strong>Address 2: </strong> " . ucfirst($user_info->Street2);
				$update_data .= "<br/> <strong>Address 2: </strong>" . ucfirst(Input::get('address2'));

			}if ($user_info->ZipCode != Input::get('zipcode')) {

				$old_data .= "<br/> <strong>Zipcode : </strong>" . $user_info->ZipCode;
				$update_data .= "<br/> <strong>Zipcode : </strong>" . Input::get('zipcode');

			}if ($user_info->BusinessName != Input::get('business_name')) {

				$old_data .= "<br/> <strong>Business name: </strong>" . ucfirst($user_info->BusinessName);
				$update_data .= "<br/> <strong>Business name: </strong>" . ucfirst(Input::get('business_name'));

			}if ($user_info->Country != $obj_source_country->name) {

				$old_data .= "<br/> <strong>Country:  </strong>" . ucfirst($user_info->Country);
				$update_data .= "<br/> <strong>Country:  </strong>" . ucfirst($obj_source_country->name);

			}if ($user_info->State != @$obj_desti_country->name) {

				$old_data .= "<br/> <strong>State:  </strong>" . ucfirst($user_info->State);
				$update_data .= "<br/> <strong>State:  </strong>" . ucfirst(@$obj_desti_country->name);

			}if ($user_info->City != $obj_source_city->name) {

				$old_data .= "<br/> <strong>City:  </strong>" . ucfirst($user_info->City);
				$update_data .= "<br/> <strong>City:  </strong>" . ucfirst(@$obj_source_city->name);

			}

			// email to admin
			if ($old_data != "") {
				$admin_detail = Setting::where(['_id' => '563b0e31e4b03271a097e1ca'])->first();
				send_mail('55d5a0be6734c4fb378b4576', [
					'to' => @$admin_detail->SupportEmail,
					'replace' => [
						'[USERNAME]' => $user_info->Name,
						'[OLD_INFORMATION]' => $old_data,
						'[UPDATED_INFORMATION]' => $update_data,
					],
				]);

			}

			$user_info->FirstName = Input::get('first_name');
			$user_info->LastName = Input::get('last_name');
			$user_info->Name = Input::get('first_name') . " " . Input::get('last_name');
			$user_info->PhoneNo = Input::get('phoneno');
			$user_info->AlternatePhoneNo = Input::get('alternet_phoneno');
			$user_info->Street1 = Input::get('address');
			$user_info->Street2 = Input::get('address2');
			$user_info->ZipCode = Input::get('zipcode');
			$user_info->CountryCode = Input::get('country_code');
			$user_info->AlternateCCode = Input::get('alternet_country_code');
			$user_info->BusinessName = Input::get('business_name');
			$user_info->UserType = 'requester';
			$user_info->Country = trim($obj_source_country->name) ;
			$user_info->State = trim(@$obj_desti_country->name);
			$user_info->City = trim($obj_source_city->name);
			if (trim(@$obj_source_country->name) === 'Kenya') {
				$user_info->Default_Currency = 'KES';
			} else if (trim(@$obj_source_country->name) === 'Canada') {
				$user_info->Default_Currency = 'CAD';
			} else if (trim(@$obj_source_country->name) === 'USA') {
				$user_info->Default_Currency = 'USD';
			} else if (trim(@$obj_source_country->name) === 'Ghana') {
				$user_info->Default_Currency = 'GHS';
			} else {
				$user_info->Default_Currency = 'GHS';
			}
			//dd($user_info);

			if (Input::file('user_image')) {
				$extension = Input::file('user_image')->getClientOriginalExtension();
				// getting image extension

				$fileName = time() . '.' . $extension; // Rename img image
				Input::file('user_image')->move(BASEURL_FILE . 'profile/', $fileName);
				if (isset($user_info->Image) && ($user_info->Image) != "") {
					if (file_exists(BASEURL_FILE . $user_info->Image)) {
						unlink(BASEURL_FILE . $user_info->Image);
					}
				}
				$user_info->Image = "profile/$fileName";
				Session::set('Image', $user_info->Image);

			}

			$user_info->save();
			$result = Deliveryrequest::where('RequesterId', '=', new MongoId(Session::get('UserId')))->update(['RequesterName' => Input::get('first_name') . ' ' . Input::get('last_name')]);

			//return Redirect::to("my-profile")->withSuccess('Success! Profile has been updated successfully.');
			return Redirect::to('dashboard?string=dashboard')->withSuccess('Success! Profile has been updated successfully.');
		}
	}

	public function post_transporter_edit_profile(Request $request) // transporter

	{

		$user_info = User::find(Session::get('UserId'));
		$v = Validator::make($request->all(), [
			'business_name' => 'required|Min:3|Max:80|Alpha',
			'VatTaxNo' => 'required|Min:3|Max:80',
			'phoneno' => 'required|Numeric',
			'user_image' => 'mimes:jpeg,jpg,png',
			'address' => 'required',
			'zipcode' => 'required|Numeric|Min:6',
			'country_code' => 'required|Numeric',
		]);

		if ($v->fails()) {
			return Redirect::to("become-transporter")->withErrors($v)->withInput(Input::get());
		} else {

			if ($user_info->user_address_id != '') {
				Address::where(['_id' => $user_info->user_address_id])->delete();
			}
			$add_user_address = $this->add_user_address(Input::get('address'), json_decode(Input::get('country')), json_decode(Input::get('state')), json_decode(Input::get('city')), Input::get('zipcode'));
			if ($add_user_address != 1) {
				return Redirect::to("become-transporter")->with('danger', 'We are unable to find your location. Please  correct it.');
			}

			$obj_source_country = json_decode(Input::get('country'));
			$obj_desti_country = json_decode(Input::get('state'));
			$obj_source_city = json_decode(Input::get('city'));

			$user_info->BusinessName = Input::get('business_name');
			$user_info->VatTaxNo = Input::get('VatTaxNo');
			$user_info->PhoneNo = Input::get('phoneno');
			$user_info->AlternatePhoneNo = Input::get('alternet_phoneno');
			$user_info->AlternateCCode = Input::get('alternet_country_code');
			$user_info->Street1 = Input::get('address');
			$user_info->Street2 = Input::get('address2');
			$user_info->ZipCode = Input::get('zipcode');
			$user_info->CountryCode = Input::get('country_code');
			$user_info->BusinessName = Input::get('business_name');
			$user_info->UserType = 'both';
			$user_info->Country = $obj_source_country->name;
			$user_info->State = $obj_desti_country->name;
			$user_info->City = $obj_source_city->name;

			if (Input::file('user_image')) {
				$extension = Input::file('user_image')->getClientOriginalExtension();
				// getting image extension

				$fileName = rand(100, 999) . time() . '.' . $extension; // Rename img image
				Input::file('user_image')->move(BASEURL_FILE . 'profile/', $fileName);
				if (isset($user_info->Image) && ($user_info->Image) != "") {
					if (file_exists(BASEURL_FILE . $user_info->Image)) {
						unlink(BASEURL_FILE . $user_info->Image);
					}
				}
				$user_info->Image = "profile/$fileName";
				Session::set('Image', $user_info->Image);
			}

			if (Input::file('id_proof')) {
				$extension = Input::file('id_proof')->getClientOriginalExtension(); // getting image extension

				$fileName = rand(100, 999) . time() . '.' . $extension; // Rename img image
				if (Input::file('id_proof')->move(BASEURL_FILE . 'profile/', $fileName)) {

					if (isset($user_info->IDProof) && ($user_info->IDProof) != "") {
						if (file_exists(BASEURL_FILE . $user_info->IDProof)) {
							unlink(BASEURL_FILE . $user_info->IDProof);
						}
					}
					$user_info->IDProof = "profile/$fileName";
				}
			}

			if (Input::file('licenceid')) {
				$extension = Input::file('licenceid')->getClientOriginalExtension(); // getting

				$fileName = rand(100, 999) . time() . '.' . $extension; // Rename img image
				if (Input::file('licenceid')->move(BASEURL_FILE . 'profile/', $fileName)) {

					if (isset($user_info->LicenceId) && ($user_info->LicenceId) != "") {
						if (file_exists(BASEURL_FILE . $user_info->LicenceId)) {
							unlink(BASEURL_FILE . $user_info->LicenceId);
						}
					}
					$user_info->LicenceId = "profile/$fileName";
				}
			}

			//    echo "<pre>";    print_r($user_info);   die;

			$user_info->save();
			return Redirect::to("my-profile")->withSuccess('Success! Profile has been updated successfully.');
		}
	}

	public function post_edit_transporter_profile(Request $request) {
		$result = Session::get('Usertype');
		$user_info = User::find(Session::get('UserId'));
		$v = Validator::make($request->all(), [
			'phoneno' => 'required|Numeric|Min:10,',
			'address1' => 'required',
			'zipcode' => 'Numeric|Min:6',
			'country_code' => 'required|Numeric',
			'ssn' => 'required',
		]);

		if ($v->fails()) {

			return Redirect::to("edit-transporter-profile")->withErrors($v)->withInput(Input::get());
		} else {

			$insert['UserType'] = (in_array(Input::get('transportertype'), ['individual', 'business'])) ? 'transporter' : 'requester';
			if ($user_info->user_address_id != '') {
				Address::where(['_id' => $user_info->user_address_id])->delete();
			}

			$add_user_address = $this->add_user_address(Input::get('address1'), json_decode(Input::get('country')), json_decode(Input::get('state')), json_decode(Input::get('city')), Input::get('zipcode'));
			if ($add_user_address != 1) {
				return Redirect::to("edit-transporter-profile")->with('danger', 'We are unable to find your location. Please  correct it.');
			}

			$obj_source_country = json_decode(Input::get('country'));
			$obj_desti_country = json_decode(Input::get('state10'));
			$obj_source_state = json_decode(Input::get('state10'));
			$obj_source_city = json_decode(Input::get('city'));

			$old_data = "";
			$update_data = "";

			if ($user_info->FirstName != Input::get('first_name')) {

				$old_data .= "<br/> <strong>First name: </strong>" . ucfirst($user_info->FirstName);
				$update_data .= "<br/> <strong>First name: </strong>" . ucfirst(Input::get('first_name'));

			}if ($user_info->LastName != Input::get('last_name')) {

				$old_data .= "<br/> <strong>Last name: </strong>" . ucfirst($user_info->LastName);
				$update_data .= "<br/> <strong>Last name: </strong>" . ucfirst(Input::get('last_name'));
				//echo $old_data;die;

			}if ($user_info->CountryCode != Input::get('country_code')) {

				$old_data .= "<br/> <strong>Country code : </strong>" . $user_info->CountryCode;
				$update_data .= "<br/> <strong>Country code : </strong>" . Input::get('country_code');

			}if ($user_info->PhoneNo != Input::get('phoneno')) {

				$old_data .= "<br/> <strong>Phone number: </strong>" . $user_info->PhoneNo;
				$update_data .= "<br/> <strong>Phone number: </strong>" . Input::get('phoneno');

			}if ($user_info->AlternateCCode != Input::get('alternet_country_code')) {

				$old_data .= "<br/> <strong> Alternate country code : </strong>" . $user_info->AlternateCCode;
				$update_data .= "<br/> <strong> Alternate country code : </strong>" . Input::get('alternet_country_code');

			}if ($user_info->AlternatePhoneNo != Input::get('alternet_phoneno')) {

				$old_data .= "<br/> <strong>Alternate phone number: </strong>" . $user_info->AlternatePhoneNo;
				$update_data .= "<br/> <strong>Alternate phone number: </strong>" . Input::get('alternet_phoneno');

			}if ($user_info->Street1 != Input::get('address')) {

				$old_data .= "<br/> <strong>Address 1: </strong>" . ucfirst($user_info->Street1);
				$update_data .= "<br/> <strong>Address 1: </strong>" . ucfirst(Input::get('address'));

			}if ($user_info->Street2 != Input::get('address2')) {

				$old_data .= "<br/> <strong>Address 2: </strong> " . ucfirst($user_info->Street2);
				$update_data .= "<br/> <strong>Address 2: </strong>" . ucfirst(Input::get('address2'));

			}if ($user_info->ZipCode != Input::get('zipcode')) {

				$old_data .= "<br/> <strong>Zipcode : </strong>" . $user_info->ZipCode;
				$update_data .= "<br/> <strong>Zipcode : </strong>" . Input::get('zipcode');

			}if ($user_info->BusinessName != Input::get('business_name')) {

				$old_data .= "<br/> <strong>Business name: </strong>" . ucfirst($user_info->BusinessName);
				$update_data .= "<br/> <strong>Business name: </strong>" . ucfirst(Input::get('business_name'));

			}if ($user_info->Country != $obj_source_country->name) {

				$old_data .= "<br/> <strong>Country:  </strong>" . ucfirst($user_info->Country);
				$update_data .= "<br/> <strong>Country:  </strong>" . ucfirst($obj_source_country->name);

			}if ($user_info->State != @$obj_desti_country->name) {

				$old_data .= "<br/> <strong>State:  </strong>" . ucfirst($user_info->State);
				$update_data .= "<br/> <strong>State:  </strong>" . ucfirst(@$obj_desti_country->name);

			}if ($user_info->City != $obj_source_city->name) {

				$old_data .= "<br/> <strong>City:  </strong>" . ucfirst($user_info->City);
				$update_data .= "<br/> <strong>City:  </strong>" . ucfirst(@$obj_source_city->name);

			}

			if ($old_data != "") {
				$admin_detail = Setting::where(['_id' => '563b0e31e4b03271a097e1ca'])->first();
				send_mail('55d5a0be6734c4fb378b4576', [
					'to' => "support@aquantuo.com",
					'replace' => [
						'[USERNAME]' => $user_info->Name,
						'[OLD_INFORMATION]' => $old_data,
						'[UPDATED_INFORMATION]' => $update_data,
					],
				]);

			}

			$user_info->FirstName = trim(Input::get('first_name'));
			$user_info->LastName = trim(Input::get('last_name'));
			$user_info->BusinessName = trim(Input::get('business_name'));
			$user_info->VatTaxNo = trim(Input::get('business_tex'));
			$user_info->Name = ucfirst(trim(Input::get('first_name')) . ' ' . trim(Input::get('last_name')));
			$user_info->PhoneNo = Input::get('phoneno');
			$user_info->Street1 = Input::get('address1');
			$user_info->ZipCode = Input::get('zipcode');
			$user_info->CountryCode = Input::get('country_code');

			$user_info->Country = trim(@$obj_source_country->name) ;
			$user_info->State = trim(@$obj_desti_country->name) ;
			$user_info->City = trim(@$obj_source_city->name) ;
			$user_info->Age = Input::get('age');
			$user_info->SSN = Input::get('ssn');
			$user_info->type_of_id = Input::get('type_of_id');
			if (trim(@$obj_source_country->name) === 'Kenya') {
				$user_info->Default_Currency = 'KES';
			} else if (trim(@$obj_source_country->name) === 'Canada') {
				$user_info->Default_Currency = 'CAD';
			} else if (trim(@$obj_source_country->name) === 'USA') {
				$user_info->Default_Currency = 'USD';
			} else if (trim(@$obj_source_country->name) === 'Ghana') {
				$user_info->Default_Currency = 'GHS';
			} else {
				$user_info->Default_Currency = 'GHS';
			}
			$user_info->AlternateCCode = Input::get('alt_country_code');
			$user_info->AlternatePhoneNo = Input::get('alt_phonNumber');

			if ($user_info->ProfileStatus == 'step-one') {
				$user_info->ProfileStatus = 'step-two';
				Session()->put(['Profile_Status' => 'step-two']);
			}
			if (Input::file('user_image')) {
				$destinationPath = 'upload/user/';
				$extension = Input::file('user_image')->getClientOriginalExtension(); // getting image extension

				$fileName = time() . '.' . $extension; // Rename img image
				if (Input::file('user_image')->move(BASEURL_FILE . 'profile/', $fileName)) {

					if (isset($user_info->Image) && $user_info->Image != "") {
						if (file_exists(BASEURL_FILE . $user_info->Image)) {
							unlink(BASEURL_FILE . $user_info->Image);
						}

					}
					$user_info->Image = "profile/$fileName";
					Session::set('Image', $user_info->Image);
				}
			}

			if (Input::file('id_proof')) {
				$extension = Input::file('id_proof')->getClientOriginalExtension(); // getting image extension

				$fileName = rand(100, 999) . time() . '.' . $extension; // Rename img image
				if (Input::file('id_proof')->move(BASEURL_FILE . 'profile/', $fileName)) {

					if (isset($user_info->IDProof) && ($user_info->IDProof) != "") {
						if (file_exists(BASEURL_FILE . $user_info->IDProof)) {
							unlink(BASEURL_FILE . $user_info->IDProof);
						}
					}
					$user_info->IDProof = "profile/$fileName";
				}
			}

			if (Input::file('licenceid')) {
				$extension = Input::file('licenceid')->getClientOriginalExtension(); // getting image extension

				$fileName = rand(100, 999) . time() . '.' . $extension; // Rename img image
				if (Input::file('licenceid')->move(BASEURL_FILE . 'profile/', $fileName)) {

					if (isset($user_info->LicenceId) && ($user_info->LicenceId) != "") {
						if (file_exists(BASEURL_FILE . $user_info->LicenceId)) {
							unlink(BASEURL_FILE . $user_info->LicenceId);
						}
					}
					$user_info->LicenceId = "profile/$fileName";
				}
			}

			$user_info->save();
			$result = Deliveryrequest::where('RequesterId', '=', new MongoId(Session::get('UserId')))->update(['RequesterName' => Input::get('first_name') . ' ' . Input::get('last_name')]);

			// email to admin
			/*if (count($supportemail) > 0) {
	            send_mail('58b69e087ac6f6f1238b4567', [
	            'to' => $supportemail->RegEmail,
	            'replace' => [
	            '[USERNAME]' => $user_info->Name,
	            '[EMAIL]' => $user_info->Email,
	            '[USERTYPE]' => ucfirst($user_info->UserType),
	            ],
	            ]);
	            }
			*/
			$supportemail = Setting::find('563b0e31e4b03271a097e1ca');

			// email to admin
			if (count($supportemail) > 0) {
				send_mail('56ab567c5509251cd67773f4', [
					'to' => $supportemail->RegEmail,

					'replace' => [
						'[USERNAME]' => $user_info->Name,
						'[EMAIL]' => $user_info->Email,
						'[USERTYPE]' => $user_info->UserType,
						'[PHONENO]' => $user_info->PhoneNo,
						'[ALTERNATEPHONENO]' => $user_info->AlternatePhoneNo,
						'[BUSINESSNAME]' => $user_info->BusinessName,
						'[STREET1]' => $user_info->Street1,
						'[STREET2]' => $user_info->Street2,
						'[COUNTRY]' => $user_info->Country,
						'[STATE]' => $user_info->State,
						'[CITY]' => $user_info->City,
						'[ZIPCODE]' => $user_info->ZipCode,
						'[AGE]' => $user_info->Age,

					],
				]);
			}

			Notification::insert([
				"NotificationTitle" => "User has updated profile",
				"NotificationMessage" => sprintf('%s has updated profile.', ucfirst($user_info->Name)),

				"NotificationReadStatus" => 0,
				"location" => "",
				"locationkey" => '',
				"Date" => new MongoDate(),
				"GroupTo" => "Admin",
			]);

			return Redirect::to("my-profile")
				->withSuccess('Success! Profile has been updated successfully.');
		}
	}

	public function add_user_address($address, $country, $state, $city, $zip) {

		$addlat = true;
		$lat = '';
		$lng = '';

		$addlat = Utility::getLatLong($address,
			Input::get('address_line_2'),
			$city->name,
			@$state->name,
			$country->name
		);

		if ($addlat != false) {
			$lat = $addlat['lat'];
			$lng = $addlat['lng'];
		} elseif ($addlat == false) {
			$addlat = Utility::getLatLong(
				$city->name,
				@$state->name,
				$country->name
			);
		}

		if ($addlat == false) {
			$addlat == true;
		}

		if ($addlat) {

			$insert = [
				'country' => $country->name,
				'country_id' => $country->id,
				'state' => @$state->name,
				'state_id' => @$state->id,
				'city' => $city->name,
				'city_id' => $city->id,
				'state_available' => $country->state_available,
				'address_line_1' => $address,
				'address_line_2' => Input::get('address_line_2'),
				'zipcode' => $zip,
				'user_id' => Session::get('UserId'),
				'lat' => $lat,
				'lng' => $lng,
			];

			$insert_data = Address::insertGetId($insert);
			$user_info = User::find(Session::get('UserId'));
			$user_info->user_address_id = (string) $insert_data;
			$user_info->save();
			$addlat = true;

		}
		return $addlat;

	}

	public function aquantuo_address() {
		$data['users'] = User::where(['_id' => Session::get('UserId')])
			->select('_id', 'AqAddress', 'UniqueNo', 'Name', 'AqCity', 'AqState', 'AqCountry', 'AqZipcode', 'Aqphone', 'Aqcc')->first();
		return view('User::detail.aquantuo_address', $data);
		//print_r($data['users']);   die;
	}

	/* Developer Name : Aakash tejwal
		     * Date : 19-08-2016
		     * About Function : This function is for edit  my-profile
		     * Method : Post
		     * Lest Update :
		     * Update by :
		     *
	*/
	public function edit_profile() {
		$data['users'] = User::where(array('_id' => Session::get('UserId')))->select('_id', 'FirstName', 'LastName', 'Email', 'Country', 'CountryCode', 'PhoneNo',
			'AlternateCCode', 'AlternatePhoneNo', 'Image', 'Street1', 'Street2', 'City', 'State', 'ZipCode', 'UserType', 'type_of_id')->get();
		$data['country'] = CityStateCountry::where(['type' => 'Country', 'Status' => 'Active'])
			->orderBy('Content', 'Asc')
			->get(['_id', 'Content', 'state_available']);

		$data['state'] = CityStateCountry::where(['type' => 'State', 'Status' => 'Active','SuperName'=> $data['users'][0]->Country ])
			->orderBy('Content', 'Asc')
			->get(['_id', 'Content', 'state_available']);
		$data['city'] = CityStateCountry::where(['type' => 'city', 'Status' => 'Active','SuperName'=>$data['users'][0]->State])
			->orderBy('Content', 'Asc')
			->get(['_id', 'Content', 'state_available']);

		return view('User::detail.my-account.edit-profile', $data);
	}

	public function edit_transporter_profile() {
		$data['users'] = User::where(array('_id' => Session::get('UserId')))->select('_id', 'FirstName', 'LastName', 'Email', 'Country', 'CountryCode', 'PhoneNo', 'Image', 'Street1', 'Street2', 'City', 'State', 'ZipCode', 'Age', 'SSN', 'IDProof', 'UserType', 'AlternateCCode', 'AlternatePhoneNo', 'VatTaxNo', 'BusinessName', 'LicenceId', 'type_of_id')->get();

		$data['country'] = CityStateCountry::where(['type' => 'Country', 'Status' => 'Active'])
			->orderBy('Content', 'Asc')
			->get(['_id', 'Content', 'state_available']);

		$data['state'] = CityStateCountry::where(['type' => 'State', 'Status' => 'Active','SuperName'=> $data['users'][0]->Country ])
			->orderBy('Content', 'Asc')
			->get(['_id', 'Content', 'state_available']);
		$data['city'] = CityStateCountry::where(['type' => 'city', 'Status' => 'Active','SuperName'=>$data['users'][0]->State])
			->orderBy('Content', 'Asc')
			->get(['_id', 'Content', 'state_available']);

		return view('User::detail.my-account.edit-transporter-profile', $data);
	}

	public function settings() {
		$data['users'] = User::where(array('_id' => Session::get('UserId')))->select('EmailStatus', 'NoficationStatus', 'consolidate_item', 'TPSetting')->first();

		return view('User::detail.setting', $data);
	}
	public function edit_address($id, Request $request) {

		$validation = Validator::make($request->all(), [
			'address_line_1' => 'required',

			'country' => 'required',

			'city' => 'required',
		]);
		if ($validation->fails()) {

			$validation->errors()->add('field', trans('Something is wrong'));
			return $validation->errors();
		} else {
			$json_decode_country = json_decode(Input::get('country'));
			$json_decode_state = json_decode(Input::get('state'));
			$json_decode_city = json_decode(Input::get('city'));

			$update = [
				'country' => @$json_decode_country->name,
				'state' => @$json_decode_state->name,
				'city' => @$json_decode_city->name,
				'state_id' => @$json_decode_state->id,
				'country_id' => $json_decode_country->id,
				'city_id' => $json_decode_city->id,
				'address_line_1' => Input::get('address_line_1'),
				'address_line_2' => Input::get('address_line_2'),
				'zipcode' => Input::get('zipcode'),

			];

			$result = Address::where(array('_id' => Input::get('address_id'), 'user_id' => Session::get('UserId')))
				->update($update);

			$address = Address::where(['user_id' => Session::get('UserId')])->get();
			$response = array("success" => 1, "msg" => "Address has been updated successfully.", 'address_html' => '<option value="">Select address</option>');

			foreach ($address as $key) {
				$response['address_html'] .= "<option value='" . json_encode($key) . "'>" . ucfirst($key->address_line_1) . " " . $key->city . " " . $key->state . " " . $key->country . " " . $key->zipcode . "</option>";
			}

			$view = view('User::Buy.ajax.add_address', ['address' => $address]);
			$response['html'] = $view->render();

			/*$response = array(
				        "success" => 1,
				        "msg" => "Thanks for successfully updated Address.",
				        "country"=>$json_decode_country->name,
				        "country_id"=>$json_decode_country->id,
				        "state_id"=>$json_decode_state->id,
				        "state_name"=>$json_decode_state->name,
				        "state_available"=>$json_decode_city->name
			*/

		}
		return response()->json($response);
	}

	public function add_item(Request $request) {
		$data['items'] = [];
		$response = array("success" => 0, "msg" => "Oops! Something went wrong.");

		$validation = Validator::make($request->all(), [
			'item_name' => 'required',
			'add_item_url' => 'required',
			'item_price' => 'required',
			'category' => 'required',

		]);
		if ($validation->fails()) {

			$validation->errors()->add('field', trans('Something is wrong'));
			return $validation->errors();
		} else {
			$json_decode_category = json_decode(Input::get('category'));

			$lhwunit = 'inches';
			$weightunit = 'lbs';
			if (Input::get('measurement_unit') == 'cm_kg') {
				$lhwunit = 'cm';
				$weightunit = 'kg';
			}
			//user t stop to if mode wa sea and category diffrent
			$is_make_request = 'yes';
			$item_with_sea = false;
			$item_with_air = false;

			if (empty(trim(Input::get('request_id')))) {
				$shipItemList = Additem::where(array('user_id' => Session::get('UserId'), 'request_type' => 'buy_for_me'))->select('categoryid', 'travelMode')->get();
				
				foreach ($shipItemList as $value) {
					if ($value->categoryid != $json_decode_category->id && $value->travelMode == 'ship') {
						$is_make_request = 'no';
						$item_with_sea = true;
					}

					if (Input::get('travel_mode') == 'ship') {
						$item_with_sea = true;
					}

					if ($value->travelMode == 'air' || Input::get('travel_mode') == 'air') {
						$item_with_air = true;
					}

					if ($item_with_air && $item_with_sea) {
						$response['msg'] = "Sorry! You can’t have different shipping modes in the same listing. Please create a separate listing for either air or sea.";
						return response()->json($response);
						die();
					}
				}
			} else {
				$item_list = Session::get(trim(Input::get('request_id')));
				foreach ($item_list as $key) {
					if ($key['categoryid'] != $json_decode_category->id && $key['travelMode'] == 'ship') {
						$is_make_request = 'no';
						$item_with_sea = true;
					}

					if (Input::get('travel_mode') == 'ship') {
						$item_with_sea = true;
					}

					if ($key['travelMode'] == 'air' || Input::get('travel_mode') == 'air') {
						$item_with_air = true;
					}

					if ($item_with_air && $item_with_sea) {
						$response['msg'] = "Sorry! You can’t have different shipping modes in the same listing. Please create a separate listing for either air or sea.";
						return response()->json($response);
						die();
					}
				}	
			}
			/*if(Input::get('travel_mode')=='ship'){
				if (empty(trim(Input::get('request_id')))) {
					$shipItemList =  Additem::where(array('travel_mode'=>'ship','user_id' => Session::get('UserId'), 'request_type' => 'buy_for_me'))->select('categoryid')->get();
					foreach($shipItemList as $value){
						if($value->categoryid!=$json_decode_category->id){
							$is_make_request = 'no';
						}
					}
				
				}else{
					$item_list = Session::get(trim(Input::get('request_id')));
					foreach ($item_list as $key) {
						
						if($key['categoryid']!=$json_decode_category->id && $key['travelMode']=='ship'){
							$is_make_request = 'no';
						}
						
					}
					
				}
				
			}*/
			if ($is_make_request == 'no') {
				$response['msg'] = "If shipping by sea, all items must be of the same sea category. Please create a new listing if you need to use a different sea category.";
				return response()->json($response);
				die();
			}

			$requesthelper = new Requesthelper(
				[
					"needInsurance" => Input::get('insurance'),
					"productQty" => (Input::get('quentity') < 1) ? 1 : Input::get('quentity'),
					"productCost" => Input::get('item_price'),
					"productWeight" => Input::get('weight'),
					"productWeightUnit" => $weightunit,
					"productCategory" => @$json_decode_category->name,
					'productCategoryId' => @$json_decode_category->id,
					"distance" => 0,
					"travelMode" => Input::get('travel_mode'),

				]
			);

			//~ $requesthelper->calculate();
			//~ $calculationinfo = $requesthelper->get_information();

			//~ if (isset($calculationinfo->error)) {

				//~ $response['msg'] = $calculationinfo->error;
				//~ return response()->json($response);
				//~ die();
			//~ }

			$insert = [

				'item_image' => '',
				'shipping_cost_by_user' => Input::get('shipping_cost'),
				'product_name' => Input::get('item_name'),
				'item_name' => Input::get('item_name'),
				'url' => Input::get('add_item_url'),
				'add_item_url' => Input::get('add_item_url'),
				'measurement_unit' => Input::get('measurement_unit'),
				'heightUnit' => Input::get('measurement_unit'),
				'weight_unit' => (Input::get('measurement_unit') == 'cm_kg') ? 'kg' : 'lbs',
				'length' => Input::get('length'),
				'width' => Input::get('width'),
				'height' => Input::get('height'),
				'weight' => Input::get('weight'),
				'item_price' => Input::get('item_price'),
				'price' => Input::get('item_price'),
				'quentity' => Input::get('quentity'),
				'qty' => Input::get('quentity'),
				'travel_mode' => Input::get('travel_mode'),
				'travelMode' => Input::get('travel_mode'),
				'category' => $json_decode_category->name,
				'categoryid' => $json_decode_category->id,
				'description' => Input::get('description'),
				'insurance_status' => Input::get('insurance'),
				'image' => '',
				'request_type' => 'buy_for_me',
				'user_id' => Session::get('UserId'),
				"buy_for_me_dimensions" => Input::get('buy_for_me_dimensions'),
			];

			if (Input::file('item_image')) {
				$extension = Input::file('item_image')->getClientOriginalExtension(); // getting image extension

				$fileName = time() . rand(100, 999) . '.' . $extension; // Rename img image
				if (Input::file('item_image')->move(BASEURL_FILE . 'package/', $fileName)) {
					$insert['image'] = "package/" . $fileName;
				}
			}
			if (empty(trim(Input::get('request_id')))) {
				Additem::insert($insert);
				$data['items'] = Additem::where(['user_id' => Session::get('UserId'), 'request_type' => 'buy_for_me'])->get();
			} else {
				$insert['_id'] = (String) new MongoId();
				Session::push(trim(Input::get('request_id')), $insert);
				$item_list = Session::get(trim(Input::get('request_id')));
				foreach (Session::get(trim(Input::get('request_id'))) as $key) {
					$data['items'][] = $key;
				}
			}

			$response = array("success" => 1, "msg" => "Item has been added successfully.");

			$view = view('User::Buy.ajax.add_list', $data);

			$response['html'] = $view->render();

		}
		return response()->json($response);
	}
	public function edit_item(Request $request) {

		$product_list = [];
		$response = array("success" => 0, "msg" => "Oops! Something went wrong.");
		if (empty(trim(Input::get('request_id')))) {
			$product_list[Input::get('item_id')] = Additem::where(array('_id' => Input::get('item_id'), 'user_id' => Session::get('UserId'), 'request_type' => 'buy_for_me'))->select('image')->first();

		} else {
			$item_list = Session::get(trim(Input::get('request_id')));

			foreach ($item_list as $key) {
				$product_list[(String) $key['_id']] = $key;
			}
		}

		$validation = Validator::make($request->all(), [
			'item_name' => 'required',
			'add_item_url' => 'required',
			'item_price' => 'required',
			'quentity' => 'required',
			'travel_mode' => 'required',
			'category' => 'required',
			'insurance' => 'required',
		]);
		if ($validation->fails()) {
			$validation->errors()->add('field', trans('Something is wrong'));
			return $validation->errors();
		} else {
			if (isset($product_list[Input::get('item_id')])) {
				$data['items'] = [];
				$json_decode_category = json_decode(Input::get('category'));
				
				//user t stop to if mode wa sea and category diffrent
				$is_make_request = 'yes';
				$item_with_sea = false;
				$item_with_air = false;

				if (empty(trim(Input::get('request_id')))) {
					$shipItemList = Additem::where(array('user_id' => Session::get('UserId'), 'request_type' => 'buy_for_me'))->select('categoryid', 'travelMode')->get();
					
					// Shipping mode and package category will be checked on more then one items.
					if (count($shipItemList) > 1) {
						foreach ($shipItemList as $value) {
							if ($value->categoryid != $json_decode_category->id && $value->travelMode == 'ship') {
								$is_make_request = 'no';
								$item_with_sea = true;
							}

							if (Input::get('travel_mode') == 'ship') {
								$item_with_sea = true;
							}

							if ($value->travelMode == 'air' || Input::get('travel_mode') == 'air') {
								$item_with_air = true;
							}

							if ($item_with_air && $item_with_sea) {
								$response['msg'] = "Sorry! You can’t have different shipping modes in the same listing. Please create a separate listing for either air or sea.";
								return response()->json($response);
								die();
							}
						}
					}
				} else {
					$item_list = Session::get(trim(Input::get('request_id')));

					// Shipping mode and package category will be checked on more then one items.
					if (count($item_list) > 1) {
						foreach ($item_list as $key) {
							if ($key['categoryid'] != $json_decode_category->id && $key['travelMode'] == 'ship') {
								$is_make_request = 'no';
								$item_with_sea = true;
							}

							if (Input::get('travel_mode') == 'ship') {
								$item_with_sea = true;
							}

							if ($key['travelMode'] == 'air' || Input::get('travel_mode') == 'air') {
								$item_with_air = true;
							}

							if ($item_with_air && $item_with_sea) {
								$response['msg'] = "Sorry! You can’t have different shipping modes in the same listing. Please create a separate listing for either air or sea.";
								return response()->json($response);
								die();
							}
						}
					}
				}
				/*if(Input::get('travel_mode')=='ship'){
					if (empty(trim(Input::get('request_id')))) {
						$shipItemList =  Additem::where(array('travel_mode'=>'ship','user_id' => Session::get('UserId'), 'request_type' => 'buy_for_me'))->select('categoryid')->get();
						foreach($shipItemList as $value){
							if($value->categoryid!=$json_decode_category->id){
								$is_make_request = 'no';
							}
						}
					
					}else{
						$item_list = Session::get(trim(Input::get('request_id')));
						foreach ($item_list as $key) {
							
							if($key['categoryid']!=$json_decode_category->id && $key['request_type']=='buy_for_me' && $key['travelMode']=='ship'){
								$is_make_request = 'no';
							}
							
						}
						
						
					}
					
				}*/
				if($is_make_request == 'no'){
					$response['msg'] = "If shipping by sea, all items must be of the same sea category. Please create a new listing if you need to use a different sea category.";
					return response()->json($response);
					die();
				}

				$lhwunit = 'inches';
				$weightunit = 'lbs';
				if (Input::get('measurement_unit') == 'cm_kg') {
					$lhwunit = 'cm';
					$weightunit = 'kg';
				}

				$requesthelper = new Requesthelper(
					[
						"needInsurance" => Input::get('insurance'),
						"productQty" => (Input::get('quentity') < 1) ? 1 : Input::get('quentity'),
						"productCost" => Input::get('item_price'),
						"productWeight" => Input::get('weight'),
						"productWeightUnit" => $weightunit,
						"productCategory" => @$json_decode_category->name,
						'productCategoryId' => @$json_decode_category->id,
						"distance" => 0,
						"travelMode" => Input::get('travel_mode'),

					]
				);
				// comment the code because new library
					//~ $requesthelper->calculate();
					//~ $calculationinfo = $requesthelper->get_information();

					//~ if (isset($calculationinfo->error)) {

						//~ $response['msg'] = $calculationinfo->error;
						//~ return response()->json($response);
						//~ die();
					//~ }
				

				$update = [
					'item_image' => '',
					'shipping_cost_by_user' => Input::get('shipping_cost'),
					'product_name' => Input::get('item_name'),
					'item_name' => Input::get('item_name'),
					'url' => Input::get('add_item_url'),
					'add_item_url' => Input::get('add_item_url'),
					'measurement_unit' => Input::get('measurement_unit'),
					'heightUnit' => Input::get('measurement_unit'),
					'weight_unit' => (Input::get('measurement_unit') == 'cm_kg') ? 'kg' : 'lbs',
					'length' => (Input::get('buyforme_dimensions') != '') ? Input::get('length') : '',
					'width' => (Input::get('buyforme_dimensions') != '') ? Input::get('width') : '',
					'height' => (Input::get('buyforme_dimensions') != '') ? Input::get('height') : '',
					'weight' => (Input::get('buyforme_dimensions') != '') ? Input::get('weight') : '',
					'item_price' => Input::get('item_price'),
					'price' => Input::get('item_price'),
					'quentity' => Input::get('quentity'),
					'qty' => Input::get('quentity'),
					'travel_mode' => Input::get('travel_mode'),
					'travelMode' => Input::get('travel_mode'),
					'category' => $json_decode_category->name,
					'categoryid' => $json_decode_category->id,
					'description' => Input::get('description'),
					'insurance_status' => Input::get('insurance'),
					'request_type' => 'buy_for_me',
					'image' => '',
					'buy_for_me_dimensions' => Input::get('buyforme_dimensions'),

				];

				if (Input::file('item_image') != '') {
					$extension = Input::file('item_image')->getClientOriginalExtension(); // getting image extension

					$fileName = time() . rand(100, 999) . '.' . $extension; // Rename img image
					if (Input::file('item_image')->move(BASEURL_FILE . 'package/', $fileName)) {

						if (!empty(trim($product_list[Input::get('item_id')]['image']))) {
							if (file_exists(BASEURL_FILE . $product_list[Input::get('item_id')]['image'])) {
								unlink(BASEURL_FILE . $product_list[Input::get('item_id')]['image']);
							}
						}
						$update['image'] = "package/" . $fileName;
					}
				}
				if (empty(trim(Input::get('request_id')))) {
					Additem::where([
						'_id' => Input::get('item_id'),
						'user_id' => Session::get('UserId'),
						'request_type' => 'buy_for_me',
					])->update($update);
					$data['items'] = Additem::where([
						'user_id' => Session::get('UserId'),
						'request_type' => 'buy_for_me',
					])->get();
				} else {
					$update['_id'] = Input::get('item_id');
					$product_list[Input::get('item_id')] = $update;
					foreach ($product_list as $key => $val) {
						$data['items'][] = $val;
					}
					Session::put(trim(Input::get('request_id')), $data['items']);
				}

				$view = view('User::Buy.ajax.add_list', $data);
				$response = array("success" => 1, "msg" => "Item has been updated successfully.");
				$response['edit_html'] = $view->render();

			}
		}
		return response()->json($response);
	}
	/*
		     * Online purchase section start
	*/
	public function add_online_item(Request $request) {
		$response = array("success" => 0, "msg" => "Oops! Something went wrong.");
		$validation = Validator::make($request->all(), [
			'item_name' => 'required',
			'purchase_from' => 'required',
			'item_price' => 'required',
			'category' => 'required',
		]);
		if ($validation->fails()) {
			$response['error'] = $validation->errors();
			return response()->json($response);
			die();
		} else {
			$lhwunit = 'inches';
			$weightunit = 'lbs';
			if (Input::get('measurement_unit') == 'cm_kg') {
				$lhwunit = 'cm';
				$weightunit = 'kg';
			}

			$json_decode_category = json_decode(Input::get('category'));
			$cat_name = Category::where(['_id'=> $json_decode_category->id])->first();

			if(count($cat_name) == 0){
				return response()->json($response); die;
			}
			//user t stop to if mode wa sea and category diffrent
			$is_make_request = 'yes';
			$item_with_sea = false;
			$item_with_air = false;

			if (empty(trim(Input::get('request_id')))) { // in where 'travel_mode'=>'ship',
				$shipItemList = Additem::where(array('user_id' => Session::get('UserId'), 'request_type' => 'online'))->select('categoryid', 'travelMode')->get();
				
				foreach ($shipItemList as $value) {
					if ($value->categoryid != $json_decode_category->id && $value->travelMode == 'ship') {
						$is_make_request = 'no';
						$item_with_sea = true;
					}

					if (Input::get('travel_mode') == 'ship') {
						$item_with_sea = true;
					}

					if ($value->travelMode == 'air' || Input::get('travel_mode') == 'air') {
						$item_with_air = true;
					}

					if ($item_with_air && $item_with_sea) {
						$response['msg'] = "Sorry! You can’t have different shipping modes in the same listing. Please create a separate listing for either air or sea.";
						return response()->json($response);
						die();
					}
				}
			} else {
				$item_list = Session::get(trim(Input::get('request_id')));
				foreach ($item_list as $key) {
					if ($key['categoryid'] != $json_decode_category->id && $key['travelMode'] == 'ship') {
						$is_make_request = 'no';
						$item_with_sea = true;
					}

					if (Input::get('travel_mode') == 'ship') {
						$item_with_sea = true;
					}

					if ($key['travelMode'] == 'air' || Input::get('travel_mode') == 'air') {
						$item_with_air = true;
					}

					if ($item_with_air && $item_with_sea) {
						$response['msg'] = "Sorry! You can’t have different shipping modes in the same listing. Please create a separate listing for either air or sea.";
						return response()->json($response);
						die();
					}
				}	
			}

			if ($is_make_request == 'no') {
				$response['msg'] = "If shipping by sea, all items must be of the same sea category. Please create a new listing if you need to use a different sea category.";
				return response()->json($response);
				die();
			}

			$insert = [
				'item_name' => Input::get('item_name'),
				'product_name' => Input::get('item_name'),
				'add_item_url' => Input::get('purchase_from'),
				'url' => Input::get('purchase_from'),
				'measurement_unit' => Input::get('measurement_unit'),
				'heightUnit' => Input::get('measurement_unit'),
				'weight_unit' => (Input::get('measurement_unit') == 'cm_kg') ? 'kg' : 'lbs',
				'length' => Input::get('length'),
				'width' => Input::get('width'),
				'height' => Input::get('height'),
				'weight' => Input::get('weight'),
				'item_price' => trim(Input::get('item_price')),
				'price' => trim(Input::get('item_price')),
				'quentity' => Input::get('quentity'),
				'qty' => Input::get('quentity'),
				'travel_mode' => Input::get('travel_mode'),
				'travelMode' => Input::get('travel_mode'),
				'category' => @$cat_name->Content,
				'category_id' => @$json_decode_category->id,
				'categoryid' => @$json_decode_category->id,
				'description' => Input::get('description'),
				'insurance' => Input::get('insurance'),
				'insurance_status' => Input::get('insurance'),
				'request_type' => 'online',
				'user_id' => Session::get('UserId'),
				"buy_for_me_dimensions" => Input::get('buy_for_me_dimensions'),
				"tracking_number" => Input::get('tracking_number'),
			];

			// Calculate value
			$requesthelper = new Requesthelper(
				[
					"needInsurance" => Input::get('insurance'),
					"productQty" => (Input::get('quentity') < 1) ? 1 : Input::get('quentity'),
					"productCost" => $insert['price'],
					"productWidth" => $insert['width'],
					"productWidthUnit" => $lhwunit,
					"productHeight" => $insert['height'],
					"productHeightUnit" => $lhwunit,
					"productLength" => $insert['length'],
					"productLengthUnit" => $lhwunit,
					"productWeightUnit" => $weightunit,
					"productCategory" => @$json_decode_category->name,
					'productCategoryId' => @$json_decode_category->id,
					"distance" => 0,
					"travelMode" => $insert['travelMode'],
				]
			);

			$requesthelper->calculate();
			$calculationinfo = $requesthelper->get_information();

			if (isset($calculationinfo->error)) {

				$response['msg'] = $calculationinfo->error;
				return response()->json($response);
				die();
			}

			if (Input::file('item_image')) {
				$extension = Input::file('item_image')->getClientOriginalExtension(); // getting image extension

				$fileName = time() . rand(100, 999) . '.' . $extension; // Rename img image
				if (Input::file('item_image')->move(BASEURL_FILE . 'package/', $fileName)) {
					$insert['image'] = "package/" . $fileName;
				}
			}
			$data['items'] = [];

			if (!empty(trim(Input::get('request_id')))) {
				// This will execute on edit reqeust
				$insert['_id'] = (String) new MongoId();
				$data['items'][] = $insert;
				foreach (session::get(trim(Input::get('request_id'))) as $key) {
					$data['items'][] = $key;
				}
				session::put(trim(Input::get('request_id')), $data['items']);
			} else {
				$response = Additem::insert($insert);
				$data['items'] = Additem::where(['user_id' => Session::get('UserId'), 'request_type' => 'online'])->get();

			}

			$response = array("success" => 1, "msg" => "Item has been added successfully.");
			$view = view('User::Buy.ajax.edit_online_item', $data);

			$response['html'] = $view->render();

		}
		return response()->json($response);
	}

	public function edit_online_item(Request $request) {
		$response = array("success" => 0, "msg" => "Oops! Something went wrong.");

		$online_item = [];
		if (empty(trim(Input::get('request_id')))) {
			$online_item[Input::get('item_id')] = Additem::where([
				'_id' => Input::get('item_id'),
				'user_id' => Session::get('UserId'),
				'request_type' => 'online',
			])->select('image')->first();
		} else {
			$product_list = session::get(trim(Input::get('request_id')));

			foreach ($product_list as $key => $val) {
				$online_item[(String) $val['_id']] = $val;
			}
		}

		if (isset($online_item[Input::get('item_id')])) {

			$validation = Validator::make($request->all(), [
				'item_name' => 'required',
				'purchase_from' => 'required',
				'item_price' => 'required',
				'quentity' => 'required',
				'travel_mode' => 'required',
				'category' => 'required',
				'insurance' => 'required',
			]);

			if ($validation->fails()) {
				$validation->errors()->add('field', trans('Something is wrong'));
				return $validation->errors();
			} else {

				$json_decode_category = json_decode(Input::get('category'));
				//print_r($json_decode_category); die;

				$cat_name = Category::where(['_id'=> $json_decode_category->id])->first();

				if(count($cat_name) == 0){
					return response()->json($response); die;
				}

				//user t stop to if mode wa sea and category diffrent
				$is_make_request = 'yes';
				$item_with_sea = false;
				$item_with_air = false;

				if (empty(trim(Input::get('request_id')))) { // in where 'travel_mode'=>'ship',
					$shipItemList = Additem::where(array('user_id' => Session::get('UserId'), 'request_type' => 'online'))->select('categoryid', 'travelMode')->get();

					// Shipping mode and package category will be checked on more then one.
					if (count($shipItemList) > 1) {
						foreach ($shipItemList as $value) {
							if ($value->categoryid != $json_decode_category->id && $value->travelMode == 'ship') {
								$is_make_request = 'no';
								$item_with_sea = true;
							}

							if (Input::get('travel_mode') == 'ship') {
								$item_with_sea = true;
							}

							if ($value->travelMode == 'air' || Input::get('travel_mode') == 'air') {
								$item_with_air = true;
							}

							if ($item_with_air && $item_with_sea) {
								$response['msg'] = "Sorry! You can’t have different shipping modes in the same listing. Please create a separate listing for either air or sea.";
								return response()->json($response);
								die();
							}
						}
					}
				} else {
					$item_list = Session::get(trim(Input::get('request_id')));

					// Shipping mode and package category will be checked on more then one.
					if (count($item_list) > 1) {
						foreach ($item_list as $key) {
							if ($key['categoryid'] != $json_decode_category->id && $key['travelMode'] == 'ship') {
								$is_make_request = 'no';
								$item_with_sea = true;
							}

							if (Input::get('travel_mode') == 'ship') {
								$item_with_sea = true;
							}

							if ($key['travelMode'] == 'air' || Input::get('travel_mode') == 'air') {
								$item_with_air = true;
							}

							if ($item_with_air && $item_with_sea) {
								$response['msg'] = "Sorry! You can’t have different shipping modes in the same listing. Please create a separate listing for either air or sea.";
								return response()->json($response);
								die();
							}
						}
					}
				}

				if ($is_make_request == 'no') {
					$response['msg'] = "If shipping by sea, all items must be of the same sea category. Please create a new listing if you need to use a different sea category.";
					return response()->json($response);
					die();
				}

				$lhwunit = 'inches';
				$weightunit = 'lbs';
				if (Input::get('measurement_unit') == 'cm_kg') {
					$lhwunit = 'cm';
					$weightunit = 'kg';
				}

				$data['items'] = [];

				$update = [
					'item_image' => '',
					'item_name' => Input::get('item_name'),
					'product_name' => Input::get('item_name'),
					'add_item_url' => Input::get('purchase_from'),
					'url' => Input::get('purchase_from'),
					'measurement_unit' => Input::get('measurement_unit'),
					'heightUnit' => Input::get('measurement_unit'),
					'weight_unit' => (Input::get('measurement_unit') == 'cm_kg') ? 'kg' : 'lbs',
					'length' => (Input::get('buyforme_dimensions') != '') ? Input::get('length') : '',
					'width' => (Input::get('buyforme_dimensions') != '') ? Input::get('width') : '',
					'height' => (Input::get('buyforme_dimensions') != '') ? Input::get('height') : '',
					'weight' => (Input::get('buyforme_dimensions') != '') ? Input::get('weight') : '',
					'item_price' => Input::get('item_price'),
					'price' => Input::get('item_price'),
					'quentity' => Input::get('quentity'),
					'qty' => Input::get('quentity'),
					'travel_mode' => Input::get('travel_mode'),
					'travelMode' => Input::get('travel_mode'),
					'category' => @$cat_name->Content,
					'category_id' => @$json_decode_category->id,
					'categoryid' => @$json_decode_category->id,
					'description' => Input::get('description'),
					'insurance' => Input::get('insurance'),
					'insurance_status' => Input::get('insurance'),
					'request_type' => 'online',
					"buy_for_me_dimensions" => Input::get('buyforme_dimensions'),
					"tracking_number" => Input::get('tracking_number'),
				];

				// Calculate value
				/*$requesthelper = new Requesthelper(
					[
						"needInsurance" => Input::get('insurance'),
						"productQty" => (Input::get('quentity') < 1) ? 1 : Input::get('quentity'),
						"productCost" => $update['price'],
						"productWidth" => $update['width'],
						"productWidthUnit" => $lhwunit,
						"productHeight" => $update['height'],
						"productHeightUnit" => $lhwunit,
						"productLength" => $update['length'],
						"productLengthUnit" => $lhwunit,
						"productWeight" => $update['weight'],
						"productWeightUnit" => $weightunit,
						"productCategory" => @$json_decode_category->name,
						'productCategoryId' => @$json_decode_category->id,
						"distance" => 0,
						"travelMode" => $update['travelMode'],
					]
				);*/

				////$requesthelper->calculate();
				//$calculationinfo = $requesthelper->get_information();

				/*if (isset($calculationinfo->error)) {

					$response['msg'] = $calculationinfo->error;
					return response()->json($response);
					die();
				}
*/
				if (Input::file('item_image')) {
					$extension = Input::file('item_image')->getClientOriginalExtension(); // getting image extension
					$fileName = time() . rand(100, 999) . '.' . $extension; // Rename img image
					if (Input::file('item_image')->move(BASEURL_FILE . 'package/', $fileName)) {

						if (isset($online_item[Input::get('item_id')]['image']) && ($online_item[Input::get('item_id')]['image']) != "") {
							if (file_exists(BASEURL_FILE . $online_item[Input::get('item_id')]['image'])) {
								unlink(BASEURL_FILE . $online_item[Input::get('item_id')]['image']);
							}
						}

						$update['image'] = "package/" . $fileName;
					}
				}
				if (empty(trim(Input::get('request_id')))) {
					$result = Additem::where(array('_id' => Input::get('item_id'), 'user_id' => Session::get('UserId'), 'request_type' => 'online'))->update($update);

					$data['items'] = Additem::where(['user_id' => Session::get('UserId'), 'request_type' => 'online'])->get();

				} else {
					$update['_id'] = Input::get('item_id');
					$online_item[Input::get('item_id')] = $update;
					foreach ($online_item as $okey => $oval) {
						$data['items'][] = $oval;
					}

					session::put(trim(Input::get('request_id')), $data['items']);
				}

				$response = array("success" => 1, "msg" => "Item has been updated successfully.");
				$view = view('User::Buy.ajax.edit_online_item', $data);
				$response['edit_html'] = $view->render();
			}
		}
		return response()->json($response);
	}

	public function post_new_request() {
		$data['paginationUrl'] = "Paginate@new_request";
		$data['users'] = Deliveryrequest::where(['Status' => 'pending'])->select()->limit(5)->get();
		$data['postValue'] = "&search=" . Input::get('search');
		return view('User::list.new_request', $data);
	}

	public function post_buy_for_me(Request $request) {
		$response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		$validation = Validator::make($request->all(), [
			'address' => 'required',
			'country_code' => 'required',
			'phone_number' => 'required',
		]);
		if ($validation->fails()) {
			$validation->errors()->add('field', trans('Something is wrong'));
			return $validation->errors();
		} else {

			$userinfo = User::find(Session::get('UserId'));
			if (count($userinfo) <= 0) {
				return response()->json(['success' => 0, ' msg' => 'You are not authorized, Please reload your page.']);
			}

			$total_item_price = $insurance = $shippingCost = 0;
			$aqlat = Session::get('AqLat');
			$aqlong = Session::get('AqLong');
			$address = json_decode(Input::get('address'));

			$data = [
				'shippingCost' => 0,
				'insurance' => 0,
				'ghana_total_amount' => 0,
				'total_amount' => 0,
				'error' => [],
				'formated_text' => '',
				'distance_in_mile' => 0,
				'product_count' => 0,
				'ProcessingFees' => 0,
				'item_cost' => 0,
				'shipping_cost_by_user' => 0,
				'type' => 'buyforme',
				'request_type' => 'buyforme',
				'consolidate_check' => trim(Input::get('consolidate_check')),
				'is_customs_duty_applied' => false,
			];
					
			if(@$address->country!="USA"){
				$data['is_customs_duty_applied'] = true;
			}

			// Get distance
			$data['distance'] = Utility::GetDrivingDistance($aqlat, $aqlong, @$address->lat, @$address->lng);
			// End get distance
			$DeliveryDate = '';
			if (!Input::get('desired_delivery_date') == '') {
				$DeliveryDate = new MongoDate(strtotime(DateTime::createFromFormat('m/d/Y h:i A', Input::get('desired_delivery_date'))->format('d-m-Y h:i a')));
			}

			if (!Input::get('desired_delivery_date') == '') {
				$DeliveryDate = new MongoDate(strtotime(DateTime::createFromFormat('m/d/Y h:i A', Input::get('desired_delivery_date'))->format('d-m-Y h:i a')));
			}

			$json_decode_category = json_decode(Input::get('category'));

			if (!empty(trim(Input::get('request_id')))) {
				$RequestId = Deliveryrequest::where('_id', '=', Input::get('request_id'))->select('_id', 'PackageNumber')->first();
				$PackageNumber = $RequestId->PackageNumber;
			} else {
				$PackageNumber = Utility::sequence('Request') . time();
			}

			$insert = [
				'RequesterId' => new MongoId(Session::get('UserId')),
				'RequesterName' => Session::get('Name'),
				'PackageNumber' => $PackageNumber,
				'ProductTitle' => '',
				'RequestType' => 'buy_for_me',
				'request_version'=>'new',

				'ReceiverIsDifferent'=> (Input::get('ReceiverIsDifferent') == 'on')?'yes':'no', 
				'ReceiverName'=> (Input::get('ReceiverIsDifferent') == 'on')? ucfirst(Input::get('ReceiverName')):'' , 

				'device_version' => Input::get('browser'),
				'app_version' => Input::get('version'),
				'device_type' => Input::get('device_type'),
				'Status' => 'pending',
				'DeliveryFullAddress' => $address->address_line_1 . ' ' . $address->city . ' ' . $address->state . ' ' . $address->country . ' ' . $address->zipcode,
				'DeliveryAddress' => $address->address_line_1,
				'DeliveryCity' => $address->city,
				'DeliveryState' => $address->state,
				'DeliveryCountry' => $address->country,
				'DeliveryPincode' => $address->zipcode,
				'DeliveryLatLong' => [floatval(@$address->lng), floatval(@$address->lat)],

				"DeliveryStateId" => @$address->state_id,
				"DeliveryCountryId" => @$address->country_id,
				
				'ReturnToAquantuo' => (Input::get('return_to_aquantuo') == 'return_to_aquantuo') ? true : false,
				'ReturnAddress' => Session::get('AqAddress'),
				'PickupLatLong' => [floatval($aqlong), floatval($aqlat)],
				'PaymentStatus' => 'paid',
				'ReceiverCountrycode' => Input::get('country_code'),
				'ReceiverMobileNo' => Input::get('phone_number'),
				'DeliveryDate' => new MongoDate(),
				'PromoCode' => '',
				'distance' => 0,
				'distanceType' => 'Miles',
				'shipping_cost_by_user' => 0,
				'discount_in_percent' => 0,
				'discount' => 0,
				'shippingCost' => 0,
				'insurance' => 0,
				'DutyAndCustom' => 0,
				'Tax' => 0,
				'TotalCost' => 0,
				'total_item_price' => 0,
				'BeforePurchaseTotalCost' => 0,
				'AquantuoFees' => 0,
				'GhanaTotalCost' => 0,
				"consolidate_item" => (Input::get('consolidate_check') == 'on') ? 'on' : 'off',
				'PickupAddress' => '',
				'UpdateOn' => '',
				'ProductList' => [],
				'ProcessingFees' => 0, //Utility::get_processing_fee(),
				'AreaCharges' => 0,
				'after_update_difference' => 0,
				'need_to_pay' => 0,
				'EnterOn' => new MongoDate,
			];

			if (!empty(trim(Input::get('request_id')))) {
				$productlist = session()->get(trim(Input::get('request_id')));
			} else {
				$productlist = Additem::where(['user_id' => Session::get('UserId'), 'request_type' => 'buy_for_me'])->get();
			}

			$calculation = new NewCalculation();
			$info = $calculation->GetRate($productlist, $data, $userinfo);
			$insert["distance"] = $calculation->get_distance($data['distance']->distance);
			//$insert['ProcessingFees'] = Utility::get_processing_fee($info['data']['item_cost']);
			$shippingCost = 0;
			$insurance_cost = 0;
			
			
			foreach ($info['product'] as $key => $val) {

				$lhwunit = 'cm';
				$weightunit = 'kg';

				if ($val['weight_unit'] == 'lbs') {
					$lhwunit = 'inches';
					$weightunit = 'lbs';
				}

				if (!empty(trim($insert["ProductTitle"]))) {
					$insert["ProductTitle"] = "{$insert['ProductTitle']}, ";
				}
				$insert["ProductTitle"] .= $val['product_name'];
				$total_item_price += floatval(@$val['price']) * $val['qty'];
				$shippingCost += $val['shippingCost'];
				$insert['shippingCost'] = $shippingCost;
				$insurance_cost += $val['insurance'];
				$insert['insurance'] = $insurance_cost;

				$insert['shipping_cost_by_user'] += $val['shipping_cost_by_user'];
				//$aq_fee = Reqhelper::get_aquantuo_fees(@$val['shippingCost']);

				$insert['ProcessingFees'] += $val['ProcessingFees'];
				$insert['AquantuoFees'] += @$val['aq_fee'];
				$insert["TotalCost"] += (floatval(@$val['price']) * $val['qty']) + $val['shipping_cost_by_user'] + $val['shippingCost'] + $val['insurance'];
				
				//$insert["AquantuoFees"] += $aq_fee;

				$insert["ProductList"][] = [
					'_id' => (string) new MongoId(),
					'request_type' => 'buy_for_me',
					'package_id' => $insert['PackageNumber'] . $key,
					'tpid' => '',
					'tpName' => '',
					'marketid' => '',
					'marketname' => '',
					'txnid' => '',
					'product_name' => @$val['product_name'],
					'url' => @$val['url'],
					'price' => floatval(@$val['price']),
					'qty' => (int) @$val['qty'],
					'description' => @$val['description'],
					'travelMode' => @$val['travelMode'],
					'weight' => @$val['weight'],
					'weight_unit' => @$weightunit,
					'height' => @$val['height'],
					'heightUnit' => @$lhwunit,
					'length' => @$val['length'],
					'lengthUnit' => @$lhwunit,
					'width' => @$val['width'],
					'widthUnit' => @$lhwunit,
					'category' => @$val['category'],
					'categoryid' => @$val['categoryid'],
					'insurance_status' => @$val['insurance_status'],
					'image' => @$val['image'],
					'TransporterFeedbcak' => '',
					'TransporterRating' => 0,
					'RequesterFeedbcak' => '',
					'RequesterRating' => 0,
					'RejectBy' => '',
					'ReturnType' => '',
					'ReceiptImage' => '',
					'RejectTime' => '',
					'TrackingNumber' => '',
					'TransporterMessage' => '',
					'CancelDate' => '',
					'StripeChargeId' => '',
					'DeliveredDate' => '',
					"discount" => 0,
					'shipping_cost_by_user' => floatval(@$val['shipping_cost_by_user']),
					'insurance' => $val['insurance'],
					'shippingCost' => floatval(@$val['shippingCost']),
					'aq_fee' => @$val['aq_fee'],
					'status' => 'pending',
					'PaymentStatus' => 'no',
					'verify_code' => rand(1000, 9999),
					'ProcessingFees' => @$val['ProcessingFees'],
					'total_cost' => (floatval(@$val['price']) * $val['qty']) + $val['shipping_cost_by_user'] + $val['shippingCost'] + $val['insurance'],
					'after_update' => (floatval(@$val['price']) * $val['qty']) + $val['shipping_cost_by_user'] + $val['shippingCost'] + $val['insurance'],
					'ExpectedDate' => '',
					'EnterOn' => new MongoDate,
					'inform_mail_sent' => 'no',
					
				];
			}

			if (count($insert["ProductList"]) <= 0) {
				return response()->json($response);
			}
		
			$insert['discount'] = floatval(Input::get('discount')); 
			$insert['PromoCode'] = Input::get('promo_code');
			
			

			//Resion Charges
			$insert['AreaCharges'] = $this->resionCharges(['name' => $address->state, 'id' => $address->state_id]);
			$insert['TotalCost'] = ($insert['TotalCost'] + $insert['AreaCharges'] + $info['DutyAndCustom']+$info['Tax']) - $insert['discount'];
			$insert['DutyAndCustom'] = $info['DutyAndCustom'];
			$insert['Tax']  = $info['Tax'];
			$insert['TotalCost'] = $insert['TotalCost'] + $insert['ProcessingFees'];
			$insert['BeforePurchaseTotalCost'] = $insert['TotalCost'];
			//end
			$insert['total_item_price'] = floatval($total_item_price);
			if (Input::get('request_id') != '') {
				$response = array("success" => 1, "msg" => "Request has been updated successfully.");
				$updat = Deliveryrequest::where('_id', '=', Input::get('request_id'))->update($insert);
				$response['reqid'] = Input::get('request_id');

				PaymentInfo::insert([
					'request_id' => $response['reqid'],
					'user_id'=> Session::get('UserId'),
					'action_user_id'=> Session::get('UserId'),
					'RequestType'=> 'buy_for_me',
					'action' => 'update_request',
					'item_id'=> '',
					'item_unic_number' => '',
					'TotalCost'=> floatval($insert['TotalCost']),
					'shippingCost'=> floatval($insert['shippingCost']),
					'insurance'=> floatval($insert['insurance']),
					'AreaCharges'=> floatval($insert['AreaCharges']),
					'ProcessingFees'=> floatval($insert['ProcessingFees']),
					'shipping_cost_by_user'=> floatval($insert['shipping_cost_by_user']),
					'item_cost'=> floatval($total_item_price),
					'discount'=> floatval($insert['discount']),
					'PromoCode'=> $insert['PromoCode'],
					'difference'=> floatval($insert['after_update_difference']),
					'difference_before'=> floatval($insert['after_update_difference']),
					'date'=> New MongoDate(),
				]);

				Notification::insert([
					'NotificationTitle' => "New Buy for me request updated",
					'NotificationMessage' => sprintf('The buy for me request titled:"%s", ID:%s has been updated.', ucfirst(@$insert['ProductTitle']), @$insert['PackageNumber']),
					"NotificationReadStatus" => 0,
					"location" => "buy_for_me",
					'Date' => new MongoDate(),
					'GroupTo' => 'Admin',
				]);

			} else {
					
				$response = array("success" => 1, "msg" => "Request has been created successfully.");
				$response['reqid'] = (String) Deliveryrequest::insertGetId($insert);
				Additem::where(['user_id' => Session::get('UserId'), 'request_type' => 'buy_for_me'])->delete();

				PaymentInfo::insert([
					'request_id' => $response['reqid'],
					'user_id'=> Session::get('UserId'),
					'action_user_id'=> Session::get('UserId'),
					'RequestType'=> 'buy_for_me',
					'action' => 'create',
					'item_id'=> '',
					'item_unic_number' => '',
					'TotalCost'=> floatval($insert['TotalCost']),
					'shippingCost'=> floatval($insert['shippingCost']),
					'insurance'=> floatval($insert['insurance']),
					'AreaCharges'=> floatval($insert['AreaCharges']),
					'ProcessingFees'=> floatval($insert['ProcessingFees']),
					'shipping_cost_by_user'=> floatval($insert['shipping_cost_by_user']),
					'item_cost'=> floatval($total_item_price),
					'discount'=> floatval($insert['discount']),
					'PromoCode'=> $insert['PromoCode'],
					'difference'=> floatval($insert['after_update_difference']),
					'difference_before'=> floatval($insert['after_update_difference']),
					'date'=> New MongoDate(),
				]);

				/*SendMail::insert([
					"USERNAME" => Session::get('Name'),
					"PACKAGETITLE" => ucfirst($insert['ProductTitle']),
					"DESTINATION" => $insert['DeliveryFullAddress'],
					"PACKAGEID"=> $insert['PackageNumber'],
					"SHIPPINGCOST" => $insert['shippingCost'],
					"TOTALCOST" => $insert['TotalCost'],
					'email_id' => '587748397ac6f63c1a8b456d', 
					'email' => $userinfo->Email,
					'status' => 'ready',
					'PRFEE' => @$insert['ProcessingFees'],
					'by_mean'=>'email_2'
				]);*/

				/*SendMail::insert([
					"USERNAME" => Session::get('Name'),
					"PACKAGETITLE" => ucfirst($insert['ProductTitle']),
					"DESTINATION" => $insert['DeliveryFullAddress'],
					"PACKAGEID" => $insert['PackageNumber'],
					"DELIVERY_TYPE" => "Buy for Me",
					'email_id' => '583967f17ac6f6b21a8b4567', 
					'email' => 'support@aquantuo.com',
					'status' => 'ready',
					'PRFEE' => @$insert['ProcessingFees'],
					'by_mean'=>'email_2'
				]);*/
				
				/*Notification::insert([
					'NotificationTitle' => "New Buy for me request created",
					'NotificationMessage' => sprintf('The buy for me request titled:"%s", ID:%s has been created.', ucfirst(@$insert['ProductTitle']), @$insert['PackageNumber']),
					"NotificationReadStatus" => 0,
					"location" => "buy_for_me",
					'Date' => new MongoDate(),
					'GroupTo' => 'Admin',
				]);*/

				$ActivityLog = DeliveryRequest::where('_id', '=', $response['reqid'])->select('_id', 'PackageNumber', 'ProductList')->first();

				$array = $ActivityLog->ProductList;
				foreach ($array as $key) {
					/* Activity Log */
					$insertactivity = [
						'request_id' => $response['reqid'],
						'request_type' => 'buy_for_me',
						'PackageNumber' => $ActivityLog->PackageNumber,
						'item_id' => $key['_id'],
						'package_id' => $key['package_id'],
						'item_name' => $key['product_name'],
						'log_type' => 'request',
						'message' => 'Package has been created.',
						'status' => 'pending',
						'action_user_id'=> Session::get('UserId'),
						'EnterOn' => new MongoDate(),

					];
					Activitylog::insert($insertactivity);
				}
			}

		}
		return response()->json($response);

	}

	public function resionCharges($state) {

		$charges = Extraregion::where(['state_id' => $state['id'], 'status' => 'Active'])->select('amount')->first();
		if ($charges) {
			return $charges->amount;
		} else {
			return 0.00;
		}
	}

	public function old_post_buy_for_me_(Request $request) {
		$validation = Validator::make($request->all(), [
			'address' => 'required',
			'country_code' => 'required',
			'phone_number' => 'required',
		]);
		

		if ($validation->fails()) {
			$validation->errors()->add('field', trans('Something is wrong'));
			return $validation->errors();
		} else {

			$userinfo = User::find(Session::get('UserId'));
			if (count($userinfo) <= 0) {
				return response()->json(['success' => 0, ' msg' => 'You are not authorized, Please reload your page.']);
			}

			$total_item_price = $insurance = $shippingCost = 0;
			$aqlat = Session::get('AqLat');
			$aqlong = Session::get('AqLong');
			$address = json_decode(Input::get('address'));

			$data = [
				'shippingcost' => 0,
				'insurance' => 0,
				'ghana_total_amount' => 0,
				'total_amount' => 0,
				'error' => [],
				'formated_text' => '',
				'distance_in_mile' => 0,
				'product_count' => 0,
				'ProcessingFees' => 0,
				'item_cost' => 0,
				'shipping_cost_by_user' => 0,
				'type' => 'buyforme',
			];

			// Get distance
			$data['distance'] = Utility::GetDrivingDistance($aqlat, $aqlong, @$address->lat, @$address->lng);
			// End get distance

			$DeliveryDate = '';
			if (!Input::get('desired_delivery_date') == '') {
				$DeliveryDate = new MongoDate(strtotime(DateTime::createFromFormat('m/d/Y h:i A', Input::get('desired_delivery_date'))->format('d-m-Y h:i a')));
			}

			$json_decode_category = json_decode(Input::get('category'));

			if (!empty(trim(Input::get('request_id')))) {
				$RequestId = Deliveryrequest::where('_id', '=', Input::get('request_id'))->select('_id', 'PackageNumber')->first();
				$PackageNumber = $RequestId->PackageNumber;
			} else {
				$PackageNumber = Utility::sequence('Request') . time();
			}

			$insert = [
				'RequesterId' => new MongoId(Session::get('UserId')),
				'RequesterName' => Session::get('Name'),
				'PackageNumber' => $PackageNumber,
				'ProductTitle' => '',
				'RequestType' => 'buy_for_me',
				'device_version' => Input::get('browser'),
				'app_version' => Input::get('version'),
				'device_type' => Input::get('device_type'),
				'Status' => 'pending',
				'DeliveryFullAddress' => $address->address_line_1 . ' ' . $address->city . ' ' . $address->state . ' ' . $address->country . ' ' . $address->zipcode,
				'DeliveryAddress' => $address->address_line_1,
				'DeliveryCity' => $address->city,
				'DeliveryState' => $address->state,
				'DeliveryCountry' => $address->country,
				'DeliveryPincode' => $address->zipcode,
				'DeliveryLatLong' => [floatval(@$address->lng), floatval(@$address->lat)],
				'ReturnToAquantuo' => (Input::get('return_to_aquantuo') == 'return_to_aquantuo') ? true : false,
				'ReturnAddress' => Session::get('AqAddress'),
				'PickupLatLong' => [floatval($aqlong), floatval($aqlat)],
				'PaymentStatus' => '',
				'ReceiverCountrycode' => Input::get('country_code'),
				'ReceiverMobileNo' => Input::get('phone_number'),
				'DeliveryDate' => new MongoDate(),
				'PromoCode' => '',
				'distance' => 0,
				'distanceType' => 'Miles',
				'shipping_cost_by_user' => 0,
				'discount_in_percent' => 0,
				'discount' => 0,
				'shippingCost' => 0,
				'insurance' => 0,
				'TotalCost' => 0,
				'BeforePurchaseTotalCost' => 0,
				'AquantuoFees' => 0,
				'GhanaTotalCost' => 0,
				"consolidate_item" => (Input::get('consolidate_check') == 'on') ? 'on' : 'off',
				'PickupAddress' => '',
				'UpdateOn' => '',
				'ProductList' => [],
				'ProcessingFees' => 0, //Utility::get_processing_fee(),
				'EnterOn' => new MongoDate,
			];

			if (!empty(trim(Input::get('request_id')))) {
				$productlist = session()->get(trim(Input::get('request_id')));
			} else {
				$productlist = Additem::where(['user_id' => Session::get('UserId'), 'request_type' => 'buy_for_me'])->get();
			}
			$calculation = new Calculation();

			$info = $calculation->getRate($productlist, $data, $userinfo);

			$insert["distance"] = $calculation->get_distance($info['data']['distance']->distance);
			$insert['ProcessingFees'] = Utility::get_processing_fee($info['data']['item_cost']);
			foreach ($productlist as $key => $val) {

				$shippingCost = 0;

				if (floatval(@$val['shipping_cost_by_user']) > 0 && floatval($val['shipping_cost_by_user']) < 5) {
					$productlist[$key]['shipping_cost_by_user'] = 5;
				}

				$lhwunit = 'cm';
				$weightunit = 'kg';

				if ($val['weight_unit'] == 'lbs') {
					$lhwunit = 'inches';
					$weightunit = 'lbs';
				}

				$requesthelper = new Requesthelper(
					[
						"needInsurance" => @$val['insurance_status'],
						"productQty" => @$val['qty'],
						"productCost" => @$val['price'],
						"productCategory" => @$val['category'],
						'productCategoryId' => @$val['categoryid'],
						"productWeight" => @$val['weight'],
						"productWeightUnit" => $weightunit,
						"travelMode" => @$val['travelMode'],
						"distance" => floatval(@$data['distance']->distance),
						"productWidth" => $val['width'],
						"productWidthUnit" => $lhwunit,
						"productHeight" => $val['height'],
						"productHeightUnit" => $lhwunit,
						"productLength" => $val['length'],
						"productLengthUnit" => $lhwunit,
					]
				);

				$requesthelper->calculate();
				$calculationinfo = $requesthelper->get_information();

				if (isset($calculationinfo->error)) {

					$response['msg'] = $calculationinfo->error;
					return response()->json($response);
					die();
				}

				if ($val['travelMode'] == 'ship') {
					$shippingCost = $calculationinfo->shippingcost;
				} else {
					$shippingCost = ($info['data']['airShippingCost'] / $info['data']['airShippingItemQty']);
				}

				$insert['shippingCost'] += $shippingCost;
				$insert['insurance'] += $calculationinfo->insurance;

				if (!empty(trim($insert["ProductTitle"]))) {
					$insert["ProductTitle"] = "{$insert['ProductTitle']}, ";
				}
				$insert["ProductTitle"] .= $val['product_name'];

				$total_item_price += floatval(@$val['price']) * $val['qty'];

				$insert['shipping_cost_by_user'] += $val['shipping_cost_by_user'];

				$insert["ProductList"][] = [
					'_id' => (string) new MongoId(),
					'package_id' => $insert['PackageNumber'] . $key,
					'tpid' => '',
					'tpName' => '',
					'marketid' => '',
					'marketname' => '',
					'txnid' => '',
					'product_name' => @$val['product_name'],
					'url' => @$val['url'],
					'price' => floatval(@$val['price']),
					'qty' => (int) @$val['qty'],
					'description' => @$val['description'],
					'travelMode' => @$val['travelMode'],
					'weight' => @$val['weight'],
					'weight_unit' => @$weightunit,
					'height' => @$val['height'],
					'heightUnit' => @$lhwunit,
					'length' => @$val['length'],
					'lengthUnit' => @$lhwunit,
					'width' => @$val['width'],
					'widthUnit' => @$lhwunit,
					'category' => @$val['category'],
					'categoryid' => @$val['categoryid'],
					'insurance_status' => @$val['insurance_status'],
					'image' => @$val['image'],
					'TransporterFeedbcak' => '',
					'TransporterRating' => '',
					'RequesterFeedbcak' => '',
					'RequesterRating' => '',
					'RejectBy' => '',
					'ReturnType' => '',
					'ReceiptImage' => '',
					'RejectTime' => '',
					'TrackingNumber' => '',
					'TransporterMessage' => '',
					'CancelDate' => '',
					'StripeChargeId' => '',
					'DeliveredDate' => '',
					"discount" => 0,
					'shipping_cost_by_user' => floatval(@$val['shipping_cost_by_user']),
					'insurance' => $calculationinfo->insurance,
					'shippingCost' => $shippingCost,
					'aq_fee' => Reqhelper::get_aquantuo_fees($shippingCost),
					'status' => 'pending',
					'PaymentStatus' => 'no',
					'verify_code' => rand(1000, 9999),
					'ProcessingFees' => 0,
					'ExpectedDate' => '',
					'EnterOn' => new MongoDate(),
				];

			}

			if (count($insert["ProductList"]) <= 0) {
				return response()->json($response);
			}
			/* End discount section  */

			$insert['outside_accra_charge'] = 0;

			/*$extraa_charge = $this->outsideOfAccraCharge($address);
				            if ($extraa_charge != false) {
				            $rate = Configuration::where(['_id' => ConfigId])->select('accra_charge', 'accra_charge_type')->first();
				            if ($rate) {
				            if ($rate->accra_charge_type == 'Percentage') {
				            $insert['outside_accra_charge'] = ($insert['shippingCost'] * $rate->accra_charge) / 100;
				            } else {
				            $insert['outside_accra_charge'] = $rate->accra_charge;
				            }

				            }
				            $insert['shippingCost'] = $insert['shippingCost'] + $insert['outside_accra_charge'];
			*/

			$insert['TotalCost'] = ($insert['shipping_cost_by_user'] + $insert['insurance'] + $insert['shippingCost'] + $total_item_price + $insert["ProcessingFees"]) - $insert["discount"];
			$insert['BeforePurchaseTotalCost'] = $insert['TotalCost'];
			$insert['AquantuoFees'] = Reqhelper::get_aquantuo_fees($insert['shippingCost']);

			if (Input::get('request_id') != '') {
				$response = array("success" => 1, "msg" => "Request has been updated successfully.");
				$updat = Deliveryrequest::where('_id', '=', Input::get('request_id'))->update($insert);
				$response['reqid'] = Input::get('request_id');

			} else {
				Additem::where(['user_id' => Session::get('UserId'), 'request_type' => 'buy_for_me'])->delete();
				$response = array("success" => 1, "msg" => "Request has been created successfully.");
				$response['reqid'] = (String) Deliveryrequest::insertGetId($insert);

				$ActivityLog = DeliveryRequest::where('_id', '=', $response['reqid'])->select('_id', 'PackageNumber', 'ProductList')->first();

				$array = $ActivityLog->ProductList;
				foreach ($array as $key) {
					/* Activity Log */
					$insertactivity = [
						'request_id' => $response['reqid'],
						'request_type' => 'buy_for_me',
						'PackageNumber' => $ActivityLog->PackageNumber,
						'item_id' => $key['_id'],
						'package_id' => $key['package_id'],
						'item_name' => $key['product_name'],
						'log_type' => 'request',
						'message' => 'Package has been created.',
						'status' => 'pending',
						'EnterOn' => new MongoDate(),

					];
					Activitylog::insert($insertactivity);
				}

			}
		}
		return response()->json($response);
	}

	public function edit_buy_for_me($id) {
		$data['request'] = Deliveryrequest::where(['_id' => $id, 'RequestType' => 'buy_for_me'])->first();

		if (count($data['request']) > 0) {
			session()->set($id, $data['request']['ProductList']);
			$data['additem'] = $data['request']['ProductList'];

			$data['country'] = CityStateCountry::where(['type' => 'Country', 'Status' => 'Active'])
				->orderBy('Content', 'Asc')
				->get(['_id', 'Content', 'state_available']);
			$data['category'] = Category::where(['type' => 'Category', 'Status' => 'Active'])->whereIn('TravelMode', ['ship', 'air'])->orderBy('Content', 'asc')->get(['TravelMode', 'Content']);
			$data['address'] = Address::where('user_id', '=', Session::get('UserId'))->select('address_line_1', 'city', 'state', 'country', 'zipcode', '_id', 'lat', 'lng', 'country_id', 'state_id')->get();
			$data['item'] = Item::Where(['Status' => 'Active'])->get();
			return view('User::edit.buy-for-me', $data);
		} else {
			return redirect('my-request');
		}
	}

	public function buy_for_me_detail($id) {
		$data['item'] = Deliveryrequest::where(array('_id' => $id, 'RequesterId' => new MongoId(Session()->get('UserId'))))->first();
		if (count($data['item']) > 0) {
			$tpid = [];
			foreach ($data['item']->ProductList as $ProductList) {
				$tpid[] = $ProductList['tpid'];
			}
			$data['user_data'] = User::whereIn('_id', $tpid)->get(['_id', 'Image', 'RatingCount', 'RatingByCount', 'Name']);
			// print_r($data);die();
			$data['activitylog'] = Activitylog::where(['request_id'=>$id])->select('_id', 'package_id')->get();

			$publish_date = strtotime('03/05/2018');
			$current_date = $data['item']->EnterOn;

			$data['updated_data'] = Itemhistory::where(['request_id'=>$id])->get();

			if(isset($data['item']->request_version) && $data['item']->request_version == 'new'){
				$data['request_data'] = $data['item'];
				return view('User::detail.new_detail.requester_bfm_detail', $data);
			}else {
				return view('User::detail.buy_for_me_detail', $data);
			}

		} else {
			return redirect('my-request');
		}
	}
	/*
		     * end buy for me section
	*/
	public function online_purchase_details($id) {
		$data = Deliveryrequest::where('_id', '=', $id)->first();
		return view('User::detail.online_purchase_details', array('data' => $data));
	}

	/* Developer Name : Aakash tejwal
		     * Date : 19-08-2016
		     * About Function : This function is for display logout (user)
		     * Method : Post
		     * Lest Update :
		     * Update by :
		     *
	*/
	public function logout() {

		Session::flush();
		/*session()->forget('UserId');
			        session()->forget('Usertype');
		*/
		return Redirect::to('login');
	}

	public function Notification() {
		$data['paginationUrl'] = "pagination/notification-list";
		$data['postValue'] = "&search=" . Input::get('search') . "&search_date=" . Input::get('search_date') . "&Status=" . Input::get('Status');
		return view('User::list.notification', $data);
	}
	public function delete_notification() {
		$notify = Notification::where('NotificationUserId', 'all', [new MongoId(Session::get('UserId'))]);

		if ($notify->pull('NotificationUserId', new MongoId(Session::get('UserId')))) {
			return Redirect::to("notification")
				->with('success', "Success! Notification has been deleted successfully.");

		} else {
			return Redirect::to("notification")
				->with('danger', "Don't have notification to deleted.");
		}
	}

	public function edit_prepare_request($id) {
		$data['delivery_data'] = Deliveryrequest::where(array('_id' => $id, 'RequestType' => 'delivery'))
			->whereIn('Status', ['ready', 'pending'])->first();
		$data['trip_data'] = $data['tr_data']=[];
		if (!count($data['delivery_data']) > 0) {
			return redirect('delivery-details/' . $id);
		}

		if (!empty($data['delivery_data']->TripId)) {
			$data['trip_data'] = Trips::where(array('_id' => $data['delivery_data']->TripId))->first();

			if (!count($data['trip_data']) > 0) {
				return redirect()->back();
			}

			$data['tr_data'] = User::where(array('_id' => $data['trip_data']->TransporterId))->first();

			$data['category2'] = Category::where(['type' => 'Category', 'Status' => 'Active'])->whereIn('TravelMode', ['ship', 'air'])->whereIn('Content', $data['trip_data']->SelectCategory)->get(['TravelMode', 'Content']);
		}

		$data['country'] = CityStateCountry::where(['type' => 'Country', 'Status' => 'Active'])
			->orderBy('Content', 'Asc')
			->get(['_id', 'Content', 'state_available']);
		$data['category'] = Category::where(['type' => 'Category', 'Status' => 'Active'])->whereIn('TravelMode', ['ship', 'air'])->get(['TravelMode', 'Content']);

		$data['state'] = CityStateCountry::where(['type' => 'State', 'Status' => 'Active', 'SuperName' => @$data['delivery_data']['PickupCountry']])->get();
		

		$data['drop_state'] = CityStateCountry::where(['type' => 'State', 'Status' => 'Active', 'SuperName' => @$data['delivery_data']['DeliveryCountry']])->get();

		$data['return_state'] = CityStateCountry::where(['type' => 'State', 'Status' => 'Active', 'SuperName' => @$data['delivery_data']['ReturnCountry']])->get();

		$data['nd_return_state'] = CityStateCountry::where(['type' => 'State', 'Status' => 'Active', 'SuperName' => @$data['delivery_data']['InCaseNotDelReturnCountry']])->get();

		if ($data['state']) {
			$data['city'] = CityStateCountry::where(['type' => 'city', 'Status' => 'Active', 'SuperName' => @$data['delivery_data']['PickupState']])->get();
		}
		


		if ($data['drop_state']) {
			$data['drop_city'] = CityStateCountry::where(['type' => 'city', 'Status' => 'Active', 'SuperName' => @$data['delivery_data']['DeliveryState']])->get();
		}

		

		if ($data['return_state']) {
			$data['return_city'] = CityStateCountry::where(['type' => 'city', 'Status' => 'Active', 'SuperName' => @$data['delivery_data']['ReturnStateTitle']])->get();
		}

		if ($data['nd_return_state']) {
			$data['nd_return_city'] = CityStateCountry::where(['type' => 'city', 'Status' => 'Active', 'SuperName' => @$data['delivery_data']['InCaseNotDelReturnState']])->get();
		}
		$data["items_count"] = count($data['delivery_data']->ProductList);
		if (isset($data['delivery_data']->ProductList)) {
			return view('User::send_package.edit.edit_send_package', $data);
		} else {
			return view('User::edit.prepare_request', $data);
		}

	}

	/*
		     * Description: Online delivery section start
	*/
	public function on_line_purchases() {
		$data['country'] = CityStateCountry::where(['type' => 'Country', 'Status' => 'Active'])
			->orderBy('Content', 'Asc')
			->get(['_id', 'Content', 'state_available']);

		$data['category'] = Category::where(['type' => 'Category', 'Status' => 'Active'])->whereIn('TravelMode', ['ship', 'air'])->orderBy('Content', 'asc')->get(['TravelMode', 'Content']);
		$field = ['country', 'state', 'city', 'address_line_1', 'address_line_2', 'zipcode', 'lat', 'lng', 'state_id','country_id'];
		$data['address'] = Address::where('user_id', '=', Session::get('UserId'))->get($field);

		$data['additem'] = Additem::where('user_id', '=', Session::get('UserId'))->where(array('request_type' => 'online'))->limit(5)->get();
		$data['item'] = Item::Where(['Status' => 'Active'])->get();
		return view('User::Buy.on-line-purchases', $data);
	}
	/*
		     * Description: edit online purchase request
	*/
	public function edit_online_purchese($id) {
		$data['request'] = Deliveryrequest::where(['_id' => $id, 'RequestType' => 'online'])->first();

		if (count($data['request']) > 0) {
			session()->set($id, $data['request']['ProductList']);
			$data['product_list'] = $data['request']['ProductList'];
			$data['additem'] = $data['request']['ProductList'];

			$data['country'] = CityStateCountry::where(['type' => 'Country', 'Status' => 'Active'])
				->orderBy('Content', 'Asc')
				->get(['_id', 'Content', 'state_available']);
			$data['category'] = Category::where(['type' => 'Category', 'Status' => 'Active'])->whereIn('TravelMode', ['ship', 'air'])->orderBy('Content', 'asc')->get(['TravelMode', 'Content']);
			$field = ['country', 'state', 'city', 'address_line_1', 'address_line_2', 'zipcode', 'lat', 'lng', 'DeliveryFullAddress', 'DeliveryDate', 'state_id'];
			$data['address'] = Address::where('user_id', '=', Session::get('UserId'))->get($field);
			$data['item'] = Item::Where(['Status' => 'Active'])->get();
			return view('User::edit.edit_online_purchese', $data);
		} else {
			return Redirect::to("my-request")->with('danger', "Fail! Something is worng");
		}
	}

	public function outsideOfAccraCharge($address) {
		//print_r($address->lat); die;
		$distance = Utility::getDistanceBetweenPointsNew(5.5913754, -0.2499413, $address->lat, $address->lng);
		$charge = false;
		if ($distance != false && $distance > 0) {
			$distance_km = $distance / 1000;
			$range = 30;
			if ($distance_km > 30) {
				$charge = true;
			}
		}
		return $charge;
	}

	public function post_online_purchase(Request $request) {
		$validation = Validator::make($request->all(), [
			'address' => 'required',
			'country_code' => 'required',
			'phone_number' => 'required',
		]);

		if ($validation->fails()) {
			$validation->errors()->add('field', trans('Something is wrong'));
			return $validation->errors();
		} else {
			$userinfo = User::find(Session::get('UserId'));

			$aqlat = Session::get('AqLat');
			$aqlong = Session::get('AqLong');
			$address = json_decode(Input::get('address'));

			

			$data = [
				'shippingcost' => 0,
				'insurance' => 0,
				'ghana_total_amount' => 0,
				'total_amount' => 0,
				'error' => [],
				'formated_text' => '',
				'distance_in_mile' => 0,
				'product_count' => 0,
				'ProcessingFees' => 0,
				'item_cost' => 0,
				'shipping_cost_by_user' => 0,
				'type' => 'online',
				'outside_accra_charge' => 0,
				'consolidate_check' => trim(Input::get('consolidate_check')) , 
				
			];
			// Get distance
			$data['distance'] = Utility::GetDrivingDistance($aqlat, $aqlong, @$address->lat, @$address->lng);

			// End get distance
			$data['is_customs_duty_applied'] = false;
			if(@$address->country!="USA"){
				$data['is_customs_duty_applied'] = true;
			}
			
			$calculate = new NewCalculation();

			$distance_in_mile = $calculate->get_distance($data['distance']->distance);
			$discount_in_percent = 0;

			$productlist = [];
			if (empty(trim(Input::get('request_id')))) {
				$productlist = Additem::where('user_id', '=', Session::get('UserId'))->where(array('request_type' => 'online'))->get();
			} else {
				$productlist = session::get(Input::get('request_id'));
			}

			$DeliveryDate = '';

			$json_decode_category = json_decode(Input::get('category'));
			$address = json_decode(Input::get('address'));

			if (!empty(trim(Input::get('request_id')))) {
				$requestId = Deliveryrequest::where('_id', '=', Input::get('request_id'))->select('_id', 'PackageNumber')->first();
				$PackageNumber = $requestId->PackageNumber;
			} else {
				$PackageNumber = Utility::sequence('Request') . time();
			}

			

			$insert = [
				'RequesterId' => new MongoId(Session::get('UserId')),
				'RequesterName' => Session::get('Name'),
				'TransporterId' => '',
				'TransporterName' => '',
				'PackageNumber' => $PackageNumber,
				'ProductTitle' => '',
				'RequestType' => 'online',
				'request_version'=>'new',
				'ReceiverIsDifferent'=> (Input::get('ReceiverIsDifferent') == 'on')?'yes':'no', 
				'ReceiverName'=> (Input::get('ReceiverIsDifferent') == 'on')? ucfirst(Input::get('ReceiverName')):'' , 
				"device_version" => Input::get('browser'),
				"app_version" => Input::get('version'),
				"device_type" => Input::get('device_type'),
				'Status' => 'pending',
				'DeliveryFullAddress' => $address->address_line_1 . ', ' . $address->address_line_2 . ' ' . $address->city . ' ' . $address->state . ' ' . $address->country . ' ' . $address->zipcode,
				'DeliveryAddress' => $address->address_line_1,
				'DeliveryCity' => $address->city,
				'DeliveryState' => $address->state,
				'DeliveryCountry' => $address->country,
				'DeliveryPincode' => $address->zipcode,

				"DeliveryStateId" => @$address->state_id,
				"DeliveryCountryId" => @$address->country_id,


				'ReturnToAquantuo' => (Input::get('return_to_aq') == 'yes') ? true : false,
				'ReturnAddress' => Session::get('AqAddress'),
				'PaymentStatus' => 'paid',
				'ReceiverCountrycode' => '+' . Input::get('country_code'),
				'ReceiverMobileNo' => Input::get('phone_number'),
				'DeliveryDate' => get_utc_time(Input::get('desired_delivery_date')),
				'PromoCode' => Input::get('promo_code'),
				'discount' => 0,
				'distance' => $distance_in_mile,
				'distanceType' => 'Miles',
				'shippingCost' => 0,
				'insurance' => 0,
				'DutyAndCustom'=>0,
				'Tax'=>0,
				'TotalCost' => 0,
				'GhanaTotalCost' => 0,
				'BeforePurchaseTotalCost' => 0,
				'shipping_cost_by_user' => 0,
				'AquantuoFees' => 0,
				"after_update_difference" => 0,
				'need_to_pay' => 0,
				'PickupAddress' => '',
				"consolidate_item" => (Input::get('consolidate_check') == 'on') ? 'on' : 'off',
				'UpdateOn' => new MongoDate(),
				'ExpectedDate' => new Mongodate(strtotime('+7 day')),
				'EnterOn' => new MongoDate(),
				'PickupLatLong' => [floatval(Session::get('AqLong')), floatval(Session::get('AqLat'))],
				'DeliveryLatLong' => [floatval(@$address->lng), floatval(@$address->lat)],
				'ProductList' => [],
			];

			$response = $calculate->GetRate($productlist, $data, $userinfo);

			if ($response['success'] == 0) {
				return json_encode($response);
			}

			$k = 0;
			foreach ($response['product'] as $productKey => $productValue) {
				$lhwunit = 'cm';
				$weightunit = 'kg';
				if ($productValue['weight_unit'] == 'lbs') {
					$lhwunit = 'inches';
					$weightunit = 'lbs';
				}

				$cost = $productValue['insurance'] + $productValue['shippingCost'];

				$insert['shippingCost'] += $productValue['shippingCost'];
				$insert['insurance'] += $productValue['insurance'];
				//$insert['GhanaTotalCost'] += $calculationinfo->costInCurrency;
				//$insert['shipping_cost_by_user'] += $key['shipping_cost_by_user'];
				if (!empty(trim($insert["ProductTitle"]))) {
					$insert["ProductTitle"] = "{$insert['ProductTitle']}, ";
				}

				$insert["ProductTitle"] .= $productValue['product_name'];
				$k++;

				$fees = $productValue['aq_fee'];
				$insert['AquantuoFees'] += $fees;

				$insert["ProductList"][] = [
					'_id' => (string) new MongoId(),
					'marketid' => '',
					'marketname' => '',
					'tpid' => '',
					'tpName' => '',
					'package_id' => $insert['PackageNumber'] . $k,
					'product_name' => @$productValue['product_name'],
					'url' => @$productValue['url'],
					'description' => @$productValue['description'],
					'travelMode' => @$productValue['travelMode'],
					'weight' => @$productValue['weight'],
					'weight_unit' => $weightunit,
					'height' => @$productValue['height'],
					'heightUnit' => $lhwunit,
					'length' => @$productValue['length'],
					'lengthUnit' => $lhwunit,
					'width' => @$productValue['width'],
					'widthUnit' => $lhwunit,
					'category' => @$productValue['category'],
					'categoryid' => @$productValue['categoryid'],
					'price' => @$productValue['price'],
					'qty' => @$productValue['qty'],
					'insurance_status' => @$productValue['insurance_status'],
					'image' => @$productValue['image'],
					///'shipping_cost_by_user' => '',
					'status' => 'pending',
					'verify_code' => rand(1000, 9999),
					'txnid' => '',
					'shippingCost' => $productValue['shippingCost'],
					'insurance' => $productValue['insurance'],
					'discount' => 0,
					'aq_fee' => floatval($fees),
					'TransporterFeedbcak' => '',
					'TransporterRating' => 0,
					'RequesterFeedbcak' => '',
					'RequesterRating' => 0,
					'RejectBy' => '',
					'ReturnType' => '',
					'ReceiptImage' => '',
					'RejectTime' => '',
					'TrackingNumber' => '',
					'TransporterMessage' => '',
					'CancelDate' => '',
					'StripeChargeId' => '',
					'DeliveredDate' => '',
					'tracking_number' => @$productValue['tracking_number'],
					'ExpectedDate' => '',
					'EnterOn' => new MongoDate,
					
					'total_cost' => $productValue['shippingCost'] + $productValue['insurance'],
					'after_update' => $productValue['shippingCost'] + $productValue['insurance'],
					'inform_mail_sent' => 'no',
					'PaymentStatus' => 'no',
				];
			}

			if (count($insert["ProductList"]) <= 0) {
				return response()->json($response);
			}

			$insert['AreaCharges'] = $this->resionCharges(['name' => $address->state, 'id' => $address->state_id]);
			//end accra charge
			//echo $insert['shippingCost'];die;
			$insert['discount'] = floatval(Input::get('discount')) ;
			$insert['TotalCost'] = $insert['AreaCharges'] + $insert['TotalCost'] + (($insert['shippingCost'] + $insert['insurance']) +$response['DutyAndCustom']+$response['Tax']  - $insert['discount']);
			$insert['BeforePurchaseTotalCost'] = $insert['TotalCost'];
			$insert['DutyAndCustom'] = $response['DutyAndCustom'];
			$insert['Tax']  = $response['Tax'];

			if ($insert['TotalCost'] <= 0) {
				$insert['Status'] = 'ready';
				foreach ($insert["ProductList"] as $key => $val) {
					$insert["ProductList"][$key]['status'] = 'ready';
				}
			}

			if (empty(trim(Input::get('request_id')))) {
				$response = array("success" => 1, "msg" => "Success! Request has been created successfully.");
				$response['reqid'] = (String) Deliveryrequest::insertGetId($insert);
				PaymentInfo::insert([
					'request_id' => $response['reqid'],
					'user_id'=> Session::get('UserId'),
					'action_user_id'=> Session::get('UserId'),
					'RequestType'=> 'online',
					'action' => 'create',
					'item_id'=> '',
					'TotalCost'=> floatval($insert['TotalCost']),
					'shippingCost'=> floatval($insert['shippingCost']),
					'insurance'=> floatval($insert['insurance']),
					'AreaCharges'=> floatval($insert['AreaCharges']),
					'ProcessingFees'=> 0.0,
					'discount'=> floatval($insert['discount']),
					'PromoCode'=> $insert['PromoCode'],
					'shipping_cost_by_user'=> 0.0,
					'item_cost'=> 0.0,
					'difference'=> floatval($insert['after_update_difference']),
					'difference_before'=> floatval($insert['after_update_difference']),
					'date'=> New MongoDate(),
				]);

				/*SendMail::Insert([
					"USERNAME" => Session::get('Name'),
					"PACKAGETITLE" => ucfirst($insert['ProductTitle']),
					"DESTINATION" => ucfirst($insert['DeliveryFullAddress']),
					'email_id' => '587750587ac6f6e61c8b4567',
					'email' => $userinfo->Email,
					'status' => 'ready',
				]);*/

				/*Notification::Insert([
					'NotificationTitle' => "Online purchase request",
					'Color' => 'Green',
					'NotificationMessage' => sprintf('The online request titled:"%s", ID:%s has been created.', ucfirst($insert['ProductTitle']), $insert['PackageNumber']),
					'NotificationType' => 1,
					'NotificationReadStatus' => 0,
					'Date' => new MongoDate(),
					'GroupTo' => 'Admin',
				]);*/

				$ActivityLog = DeliveryRequest::where('_id', '=', $response['reqid'])->select('_id', 'PackageNumber', 'ProductList')->first();

				$array = $ActivityLog->ProductList;
				foreach ($array as $key) {

					/* Activity Log */
					$insertactivity = [
						'request_id' => $response['reqid'],
						'request_type' => 'online',
						'PackageNumber' => $ActivityLog->PackageNumber,
						'item_id' => $key['_id'],
						'package_id' => $key['package_id'],
						'item_name' => $key['product_name'],
						'log_type' => 'request',
						'message' => 'Package has been created.',
						'status' => 'pending',
						'action_user_id'=> Session::get('UserId'),
						'EnterOn' => new MongoDate(),

					];
					Activitylog::insert($insertactivity);
				}

				if (count($response['reqid']) > 0) {
					Additem::where('user_id', '=', Session::get('UserId'))
						->where(['request_type' => 'online'])->delete();
				}

			} else {
				$response = array("success" => 1, "msg" => "Success! Request has been updated successfully.");
				$update = Deliveryrequest::where(['_id' => Input::get('request_id')])->update($insert);
				$response['reqid'] = Input::get('request_id');
				//$user_data = User::where(['_id' => Session::get('UserId')])->first();

				PaymentInfo::insert([
					'request_id' => $response['reqid'],
					'user_id'=> Session::get('UserId'),
					'action_user_id'=> Session::get('UserId'),
					'RequestType'=> 'online',
					'action' => 'update_request',
					'item_id'=> '',
					'TotalCost'=> floatval($insert['TotalCost']),
					'shippingCost'=> floatval($insert['shippingCost']),
					'insurance'=> floatval($insert['insurance']),
					'AreaCharges'=> floatval($insert['AreaCharges']),
					'ProcessingFees'=> 0.0,
					'shipping_cost_by_user'=> 0.0,
					'item_cost'=> 0.0,
					'discount'=> floatval($insert['discount']),
					'PromoCode'=> $insert['PromoCode'],
					'difference'=> floatval($insert['after_update_difference']),
					'difference_before'=> floatval($insert['after_update_difference']),
					'date'=> New MongoDate(),
				]);

			}
			$url = "promocode=" . Input::get('promo_code');
			if ($insert['TotalCost'] <= 0) {
				$response['redirect_to'] = url("online-request-detail/{$response['reqid']}?$url");
			} else {
				$response['redirect_to'] = url("online-payment/{$response['reqid']}?$url");
			}

		}
		return response()->json($response);
	}

	public function online_request_payment($id) {
		$data['admin_msg'] = '';
		$config_data = Configuration::find(ConfigId);
		if (count($config_data) > 0) {
			$data['admin_msg'] = $config_data->message;
		}
		$data['info'] = Deliveryrequest::where(array('_id' => $id))
			->select('Status','TotalCost','PackageNumber','ProductTitle','after_update_difference')->first();
		if (!count($data['info']) > 0) {

			return redirect('/my-request');
		}
		$data['cards'] = array();
		$user = User::where(array('_id' => Session()->get('UserId')))->select('StripeId','Default_Currency')->first();
		if (count($user) > 0) {
			$data['info']->TotalCost = $data['info']->TotalCost + $data['info']->after_update_difference;
			$currency_conversion = $this->currency_conversion($data['info']->TotalCost);

			if($user->Default_Currency == 'CAD'){
				$data['user_currency'] = $currency_conversion['canadian_cost'];
			}else if($user->Default_Currency == 'PHP'){
				$data['user_currency'] = $currency_conversion['philipins_cost'];
			}else if($user->Default_Currency == 'GBP'){
				$data['user_currency'] = $currency_conversion['uk_cost'];
			}else{
				$data['user_currency'] = $currency_conversion['ghanian_cost'];
			}
			$data['cards'] = Payment::card_list($user->StripeId);
		}
		return view('User::Buy.online_request_card', $data);
	}

	public function buy_for_me_card_list($id) {
		$data['admin_msg'] = '';
		$config_data = Configuration::find(ConfigId);
		if (count($config_data) > 0) {
			$data['admin_msg'] = $config_data->message;
		}

		$request = Deliveryrequest::where(array('_id' => $id))->first();
		if (count($request) > 0) {

			$data['cards'] = array();

			$user = User::where(array('_id' => Session()->get('UserId')))->select('StripeId')->first();
			if (count($user) > 0) {
				$data['cards'] = Payment::card_list($user->StripeId);
			}
			return view('User::Buy.buy_for_card', $data);

		} else {
			return redirect('/buy-for-me-detail{$id}');
		}
	}

	public function pay_for_buy_for_item($requestid, $id, Request $request) {
		$array = [];
		$where = [
			'RequesterId' => new MongoId(Session()->get('UserId')),
			'Status' => 'not_purchased',
			'_id' => $requestid,
		];
		$user = User::where(array('_id' => Session()->get('UserId')))->first();
		$del_detail = Deliveryrequest::where($where)->first();

		if (count($del_detail) > 0 && count($user) > 0) {
			$array = $del_detail->ProductList;
			foreach ($del_detail->ProductList as $key => $value) {
				$array[$key]['status'] = 'paid';
			}

			//$del_detail->save();

			$res = RSStripe::capture($user->StripeId, ($del_detail->TotalCost - $del_detail->BeforePurchaseTotalCost), $request->segment(3), false);
			if (isset($res['id'])) {
				if (Deliveryrequest::where($where)->update([
					'ProductList' => $array,
					'Status' => 'paid',
					'StripeChargeId2' => $res['id'],
				])) {
					Notification::insert([
						'NotificationTitle' => "Buy for me payment complete",
						'NotificationMessage' => sprintf('The requester has made payment of buy for me request titled:"%s", ID:%s.', ucfirst($del_detail['ProductTitle']), $del_detail['PackageNumber']),
						"NotificationReadStatus" => 0,
						"location" => "buy_for_me",
						'Date' => new MongoDate(),
						'GroupTo' => 'Admin',
					]);

					/* Activity Log */

					$insertactivity = [
						'request_id' => $del_detail->_id,
						'request_type' => 'buy_for_me',
						'PackageNumber' => $del_detail->PackageNumber,
						'item_id' => '',
						'package_id' => '',
						'item_name' => '',
						'log_type' => 'request',
						'message' => 'Package has been created.',
						'status' => 'ready',
						'EnterOn' => new MongoDate(),

					];

					Activitylog::insert($insertactivity);

					return Redirect::to("buy-for-me-detail/{$requestid}")->with('success', "Success! Your payment has been completed successfully.");
				} else {
					return Redirect::to("payment-by-for-me/{$requestid}")->with('danger', "Fail! Something is worng");
				}
			} else {
				return Redirect::to("payment-by-for-me/{$requestid}")->with('danger', $res);
			}
		} else {
			return Redirect::to("payment-by-for-me/{$requestid}")->with('danger', "Fail! Something is worng");
		}
	}

	/*
		     * Description: Trip list posted by transporter
	*/
	public function post_by_transporter() {
		return view('User::list.post_by_transporter');
	}

	/*
		    developer name : Aakash tejwal
		     *  Date : 12-11-2016
		     *    About function : this function is use for display currency page
		     *  Method : get
		     *    Last Update :
		     *  Update Date :
	*/

	public function currency() {
		$data['userinfo'] = User::find(Session::get('UserId'));
		$data['currency'] = CityStateCountry::where(['type' => 'Country'])
			->select('_id', 'Content', 'CurrencySymbol', 'CurrencyCode')->get();
		return view('User::edit.currency', $data);
	}

	/*
		    developer name : Aakash tejwal
		     *  Date : 12-11-2016
		     *    About function : this function is use for edit currency
		     *  Method : get
		     *    Last Update :
		     *  Update Date :
	*/

	public function edit_currency() {
		$userinfo = User::find(Session::get('UserId'));
		$userinfo->Default_Currency = trim(Input::get('currency'));
		if ($userinfo->save()) {
			Session::put('Default_Currency', trim(Input::get('currency')));
			return Redirect::to("currency")->withSuccess("Success! Currency has been updated successfully.");
		} else {
			return Redirect::to("currency")->with('danger', "Oops! Fail to update currency.");
		}

	}

	public function rewords() {
		$data['info'] = Promocode::where('assign_promocode_users', 'all', [Session::get('UserId')])->where('ValidFrom', '<', new MongoDate())
			->where('ValidTill', '>', new MongoDate())
			->select('Code')->first();
		return view('User::detail.reward', $data);
	}

	public function support_page() {
		return view('User::detail.my-account.support');
	}
	/*
		     *  developer name : Kapil pancholi
		     *  Update Date : 23-5-2017
		     *  About function : this function is used to email to support
	*/
	public function post_support(Request $request) {
		$validation = Validator::make($request->all(), [
			'title' => 'required',
			'message' => 'required',
		]);

		if ($validation->fails()) {
			$validation->errors()->add('field', trans('Something is wrong'));
			return $validation->errors();
		} else {

			if (!empty(Session()->get('UserId'))) {
				$user_info = User::where(['_id' => Session()->get('UserId')])->first();
				$admin = Setting::where(['_id' => '563b0e31e4b03271a097e1ca'])->first();

				if (count($user_info) > 0) {
					$array = [
						'Email' => $user_info->Email,
						'UserName' => $user_info->Name,
						'Query' => $request->get('message'),
						'title' => $request->get('title'),
						'Token' => Utility::sequence('support'),
						'EnterOn' => new MongoDate(),

					];

					$insert = Support::insert($array);
					if ($insert) {
						//Mail to user
						send_mail('566550b813308b84d5997e89', [
							"to" => $user_info->Email,
							"replace" => [
								"[USERNAME]" => $user_info->Name,
								"[EMAIL]" => $user_info->Email,
								"[TITLE]" => ucfirst($request->get('title')),
								"[QUERY]" => ucfirst($request->get('message')),
								"[SUBJECT]" => 'Aquantuo Support',

							],
						]
						);
						Emailtemplate::where(['_id' => "563b5dffe4b03271a097e1ce"])->update(['temp_sub' => ucfirst($request->get('title'))]);
						//Mail to admin
						if (count($admin) > 0) {
							send_mail('563b5dffe4b03271a097e1ce', [
								"to" => $admin->SupportEmail,
								"replace" => [
									"[USERNAME]" => $user_info->Name,
									"[EMAIL]" => $user_info->Email,
									"[TITLE]" => ucfirst($request->get('title')),
									"[QUERY]" => ucfirst($request->get('message')),
									"[USERTYPE]" => $user_info->UserType,
									"[DEVICETYPE]" => $user_info->DeviceType,
								],
							]
							);
						}

						Notification::insert([
							"NotificationTitle" => sprintf('"%s" has sent a message.', ucfirst($user_info->Name)),
							"NotificationMessage" => ucfirst($request->get('message')),
							"NotificationReadStatus" => 0,
							"NotificationUserId" => array($user_info->_id),
							"location" => "support_request",
							"locationkey" => $array['Token'],
							"Date" => new MongoDate(),
							"GroupTo" => "Admin",
						]);

						return Redirect::to("support")->with('success', "Success! Your request has been sent successfully. We will review and get back to you ASAP");
					}
				}
			}
			return Redirect::to("support")->with('danger', "Oops! Something went wrong.");
		}

	}

	/*
		    developer name : Aakash tejwal
		     *  Date : 17-11-2016
		     *    About function : this function is show details for deliver
		     *  Method : get
		     *    Last Update :
		     *  Update Date :
	*/
	public function statistics() {
		$userids = new MongoId(Session()->get('UserId'));
		$data['requester'] = Deliveryrequest::raw(function ($collection) use ($userids) {
			return $collection->aggregate([
				[
					'$match' => ['$or' =>
						[
							['RequesterId' => $userids],
						],
					],

				],
				[
					'$group' => [
						'_id' => '$Status',
						'count' => ['$sum' => 1],
					],
				],
			]);
		});
		$data['tp'] = Deliveryrequest::raw(function ($collection) use ($userids) {
			return $collection->aggregate([
				[
					'$match' => ['$or' =>
						[
							['TransporterId' => $userids],
						],
					],

				],
				[
					'$group' => [
						'_id' => '$Status',
						'count' => ['$sum' => 1],
					],
				],
			]);
		});
		return view('User::list.statistics', $data);
	}

	public function faq_list() {
		$data['paginationUrl'] = "pagination/faq-list";
		$data['postValue'] = "&search=" . Input::get('Search');
		return view('User::list.faq_list', $data);

	}
	public function requester_list() {
		$data['paginationUrl'] = "pagination/pending-request";
		$data['postValue'] = "&search=" . Input::get('search') . "&search_date=" . Input::get('search_date') . "&Status=" . Input::get('Status');

		return view('User::list.pending_request', array('data' => $data));
	}

	public function transporter_pending() {
		$data['paginationUrl'] = "pagination/transporter-pending-request";
		$data['postValue'] = "&search=" . Input::get('search') . "&search_date=" . Input::get('search_date') . "&Status=" . Input::get('Status');

		return view('User::list.transporter_pending', array('data' => $data));
	}

	public function post_become_transporter() {

		$data['users'] = User::where(array('_id' => Session::get('UserId')))->first();
		if (count($data['users']) == 0) {
			Session::flush();
			return Redirect::to('login');
		}

		$data['country'] = CityStateCountry::where(['type' => 'Country', 'Status' => 'Active'])
			->orderBy('Content', 'Asc')
			->get(['_id', 'Content', 'state_available']);
		return view('User::detail.my-account.post-edit-trensporter', $data);
	}

	public function delete_account($id) {
		if ($id) {
			Deliveryrequest::where(['RequesterId', new MongoId($id)])
				->whereIn('Status', ['ready', 'pending'])->delete();

			$user = User::where(array('_id' => $id))->first();
			$user->delete_status = 'yes';
			if ($user->save()) {
				Session::flush();
				return Redirect::to('login');
			}
		} else {
			return Redirect::to('setting')
				->with(['danger' => 'Fail!  Something went wrong.']);
		}
	}

	/*
		    developer name : deepak pateriya
		     *  Date : 29-04-2017
		     *    About function : this function is show details for deliver
		     *  Method : get
		     *    Last Update :
		     *  Update Date :
	*/
	public function invite_friend() {
		$userids = new MongoId(Session()->get('UserId'));
		$data['requester'] = Deliveryrequest::raw(function ($collection) use ($userids) {
			return $collection->aggregate([
				[
					'$match' => ['$or' =>
						[
							['RequesterId' => $userids],
						],
					],

				],
				[
					'$group' => [
						'_id' => '$Status',
						'count' => ['$sum' => 1],
					],
				],
			]);
		});
		$data['tp'] = Deliveryrequest::raw(function ($collection) use ($userids) {
			return $collection->aggregate([
				[
					'$match' => ['$or' =>
						[
							['TransporterId' => $userids],
						],
					],

				],
				[
					'$group' => [
						'_id' => '$Status',
						'count' => ['$sum' => 1],
					],
				],
			]);
		});
		return view('User::list.invite_friend', $data);
	}

	public function promo_code() {
		return view('User::list.promocode');
	}

	/*
		     *function : for track orders
		     *
	*/
/*
public function track_ordder() {

$query = Activitylog::query();
$searchvalue = trim(Input::get('package_id'));

if ($searchvalue != "") {
if (Input::get('package_readio') == 'package_id') {
$data['package'] = Activitylog::where("PackageNumber", "=", $searchvalue)->orderby('_id')->get();
} else {
$data['package'] = Activitylog::where("package_id", "=", $searchvalue)->orderby('_id')->get();
}

// created and Expected date for use
if (isset($data['package'][0])) {
$where = array("ProductList" => array('$elemMatch' => array('package_id' => $searchvalue)));
$data['date'] = Deliveryrequest::where($where)->select('_id', 'EnterOn', 'UpdateOn', 'ExpectedDate', 'Status', 'updated_at', 'RequestType', 'ProductList.$')->first();
}
} else {
$data['package'] = [];
$data['date'] = '';
}
return view('User::list.track_order', $data);
}*/

	/*
		     *function : post for track orders
		     *
	*/

	public function post_track_ordder() {

		$query = Deliveryrequest::Query();

		if (Input::get('package_readio') == 'PackageNumber') {

			$query->where('PackageNumber', '=', Input::get('package'));
			$data['result'] = $query->first();
			$data['product'] = 'multiple';

		} else {
			$query->where('ProductList.package_id', '=', Input::get('package'));
			$res = $query->first();

			if (count($res) > 0) {
				for ($i = 0; $i < count($res->ProductList); $i++) {
					if ($res->ProductList[$i]['package_id'] == Input::get('package')) {
						$data['result'] = $res->ProductList[$i];
						$data['result']['EnterOn'] = $res->EnterOn;
						$data['result']['DeliveryDate'] = $res->DeliveryDate;
						$data['result']['distance'] = $res->distance;
						$data['result']['shippingCost'] = $res->shippingCost;
						break;
					}
				}
			} else { $data['result'] = array();}

			$data['product'] = '';

		}

		$data['package'] = Input::get('package');
		$data['package_readio'] = Input::get('package_readio');

		// echo '<pre>';
		// print_r($data);die;
		return view('User::list.track_order', $data);
	}

	public function testimonial() {
		return view('User::other.testimonial');
	}

	public function local_edit_prepare_request($id) {
		//echo "hhh";die;
		$data['delivery_data'] = Deliveryrequest::where(array('_id' => $id, 'RequestType' => 'local_delivery'))
			->whereIn('Status', ['ready', 'pending'])->first();

		if (!count($data['delivery_data']) > 0) {
			return redirect('local-delivery-details/' . $id);
		}

		if (!empty($data['delivery_data']->TripId)) {
			$data['trip_data'] = Trips::where(array('_id' => $data['delivery_data']->TripId))->first();

			if (!count($data['trip_data']) > 0) {
				return redirect()->back();
			}

			$data['tr_data'] = User::where(array('_id' => $data['trip_data']->TransporterId))->first();

			// $data['category2'] = Category::where(['type' => 'Category', 'Status' => 'Active'])->whereIn('TravelMode', ['ship', 'air'])->whereIn('Content', $data['trip_data']->SelectCategory)->get(['TravelMode', 'Content']);

			$data['category2'] = Category::where(['type' => 'Category', 'Status' => 'Active'])->whereIn('TravelMode', ['ship', 'air'])->whereIn('Content', $data['trip_data']->SelectCategory)->get(['TravelMode', 'Content']);
		}

		$data['all_state'] = CityStateCountry::where(['type' => 'State', 'Status' => 'Active', 'CountryId' => '56bdbfb4cf32079714ee89ca'])
			->orderBy('Content', 'Asc')
			->get(['_id', 'Content', 'CountryId']);

		// $data['category'] = Category::where(['type' => 'localCategory', 'Status' => 'Active', "country_id" => "56bdbfb4cf32079714ee89ca"])->get();

		$data['country'] = CityStateCountry::where(['type' => 'Country', 'Status' => 'Active'])
			->orderBy('Content', 'Asc')
			->get(['_id', 'Content', 'state_available']);

		$data['category'] = Category::where(['type' => 'Category', 'Status' => 'Active'])->whereIn('TravelMode', ['ship', 'air'])->get(['TravelMode', 'Content']);

		$data['state'] = CityStateCountry::where(['type' => 'State', 'Status' => 'Active', 'Content' => @$data['delivery_data']['PickupState']])->first();

		$data['drop_state'] = CityStateCountry::where(['type' => 'State', 'Status' => 'Active', 'Content' => @$data['delivery_data']['DeliveryState']])->first();

		$data['return_state'] = CityStateCountry::where(['type' => 'State', 'Status' => 'Active', 'Content' => @$data['delivery_data']['ReturnStateTitle']])->first();

		$data['nd_return_state'] = CityStateCountry::where(['type' => 'State', 'Status' => 'Active', 'Content' => @$data['delivery_data']['InCaseNotDelReturnState']])->first();

		if ($data['state']) {
			$data['city'] = CityStateCountry::where(['type' => 'city', 'Status' => 'Active', 'Content' => @$data['delivery_data']['PickupCity']])->first();
		}

		if ($data['drop_state']) {
			$data['drop_city'] = CityStateCountry::where(['type' => 'city', 'Status' => 'Active', 'Content' => @$data['delivery_data']['DeliveryCity']])->first();
		}

		if ($data['return_state']) {
			$data['return_city'] = CityStateCountry::where(['type' => 'city', 'Status' => 'Active', 'Content' => @$data['delivery_data']['ReturnCityTitle']])->first();
		}

		if ($data['nd_return_state']) {
			$data['nd_return_city'] = CityStateCountry::where(['type' => 'city', 'Status' => 'Active', 'Content' => @$data['delivery_data']['InCaseNotDelReturnCity']])->first();
		}

		//print_r(count($data['state']));die;
		$data['items_count'] = count($data['delivery_data']->ProductList);
		if (isset($data['delivery_data']->ProductList)) {
			return view('User::local_delivery.edit.edit_local_delivery', $data);
		} else {
			return view('User::edit.local_delivery_prepare_request', $data);
		}

	}

	public function local_delivery_request_card_list($id, Request $request) {
		$data['admin_msg'] = '';
		$config_data = Configuration::find(ConfigId);
		if (count($config_data) > 0) {
			$data['admin_msg'] = $config_data->message;
		}

		$del_request = Deliveryrequest::where(array('_id' => $id))->first();
		if (!count($del_request) > 0) {
			return redirect('/my-request');
		}
		if ($del_request->Status == 'ready') {
			return redirect('local-delivery-details/' . $request->segment(2));
		}
		$data['cards'] = array();
		$user = User::where(array('_id' => Session()->get('UserId')))->select('StripeId')->first();
		if (count($user) > 0) {
			$data['cards'] = Payment::card_list($user->StripeId);
		}
		return view('User::Buy.local_delivery_card_list', $data);
	}

	public function clear_cache()
	{
		$exitCode = Artisan::call('cache:clear');
		echo 'ok';
	}

}
