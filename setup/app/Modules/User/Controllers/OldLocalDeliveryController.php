<?php namespace App\Modules\User\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Models\Activitylog;
use App\Http\Models\Additem;
use App\Http\Models\Category;
use App\Http\Models\CityStateCountry;
use App\Http\Models\Configuration;
use App\Http\Models\Currency;
use App\Http\Models\Deliveryrequest;
use App\Http\Models\Distance;
use App\Http\Models\Extraregion;
use App\Http\Models\Notification;
use App\Http\Models\SendMail;
use App\Http\Models\Setting;
use App\Http\Models\Transaction;
use App\Http\Models\User;
use App\Library\Promo;
use App\Library\Reqhelper;
use App\Library\RSStripe;
use App\Library\Utility;
use Illuminate\Http\Request;
use Input;
use MongoDate;
use MongoId;
use Redirect;
use Session;


class OldLocalDeliveryController extends Controller {

	public function __construct() {
		if (!session()->has('UserId')) {
			return Redirect::to('login')->send();
		}

		$user = User::where(["_id" => session()->get('UserId')])
			->where(['delete_status' => 'yes'])
			->first();
		if (count($user) > 0) {
			Session::flush();
			return Redirect::to('login');
		}

	}

	public function formated_address($array, $zip) {
		$address = '';
		foreach ($array as $ky => $val) {
			if (trim($val) != '') {
				if (trim($address) != '') {$address = "$address, ";}
				$address .= $val;
			}
		}
		if (trim($zip) != '') {$address .= "- $zip";}
		return $address;
	}

	public function localSendPackage() {

		$data['items'] = Additem::where(['user_id' => Session::get('UserId'), 'type' => 'local_delivery'])->get();
		//echo '<pre>';
		//print_r($data['items'][0]['productLength']); die;
		return view('User::Buy.send_package_items', $data);

	}

	public function localEditSendPackage($id, $requestid = "") {
		// echo "hi";die;
		$data['transporter_data'] = array();

		$data['country'] = CityStateCountry::where(['type' => 'Country', 'Status' => 'Active'])
			->orderBy('Content', 'Asc')
			->get(['_id', 'Content', 'state_available']);

		$data['state'] = CityStateCountry::where(['type' => 'State', 'Status' => 'Active', 'CountryId' => '56bdbfb4cf32079714ee89ca'])
			->orderBy('Content', 'Asc')
			->get(['_id', 'Content', 'CountryId']);

		$data['category'] = Category::where(['type' => 'localCategory', 'Status' => 'Active', "country_id" => "56bdbfb4cf32079714ee89ca"])->get();

		if ($requestid != '') {
			$data['delivery_data'] = Deliveryrequest::where(['_id' => $requestid])
				->select('ProductList')->first();
		} else {
			$data['delivery_data'] = Additem::where(['user_id' => Session::get('UserId'), 'type' => 'local_delivery', '_id' => $id])->first();
		}

		// echo "<pre>";
		//print_r($data['delivery_data']);die;

		$data['items_count'] = 1;
		if ($data['delivery_data']) {
			if ($requestid != '') {
				$array = [];
				foreach ($data['delivery_data']->ProductList as $key) {
					if ($key['_id'] == $id) {
						$array = [
							"_id" => $key['_id'],
							"product_name" => $key['product_name'],
							"productWidth" => $key['productWidth'],
							"productHeight" => $key['productHeight'],
							"productLength" => $key['productLength'],
							"productCost" => $key['productCost'],
							"productWeight" => $key['productWeight'],
							"productHeightUnit" => $key['productHeightUnit'],
							"ProductWeightUnit" => $key['ProductWeightUnit'],
							"ProductLengthUnit" => $key['ProductLengthUnit'],
							"productCategory" => $key['productCategory'],
							"productCategoryId" => $key['productCategoryId'],
							"travelMode" => $key['travelMode'],
							"InsuranceStatus" => $key['InsuranceStatus'],
							"productQty" => $key['productQty'],
							"ProductImage" => $key['ProductImage'],
							"QuantityStatus" => $key['QuantityStatus'],
							"BoxQuantity" => $key['BoxQuantity'],
							"Description" => $key['Description'],
							"PackageMaterial" => $key['PackageMaterial'],
							"PackageMaterialShipped" => $key['PackageMaterialShipped'],
						];
					}
				}

				$data['delivery_data'] = $array;
				return view('User::local_delivery.edit.edit_item', $data);
			} else {

				return view('User::local_delivery.edit_local_delivery_items', $data);
			}

		} else {
			return Redirect::back();
		}
	}

	public function local_edit_calculation() {
		$response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		if (Input::get('request_id') != '') {
			$request_data = Deliveryrequest::where(['_id' => trim(Input::get('request_id')), 'RequesterId' => new MongoId(Session::get('UserId'))])
				->select('ProductList')
				->first();
			if (count($request_data) == 0 && !isset($request_data->ProductList)) {
				return json_encode($response);die;
			}
			$rate = 0;
			$insurance = 0;
			$weight = 0;
			$volume = 0;
			$distance = $this->get_distance(Input::get('calculated_distance'));
			$drop_lat_long = ['lat' => Input::get('DeliveryLat'), 'lng' => Input::get('DeliveryLong')];
			$distance_cal = Utility::GetDrivingDistance(Input::get('PickupLat'), Input::get('PickupLong'), Input::get('DeliveryLat'), Input::get('DeliveryLong'));
			if ($distance_cal == false) {
				$response['msg'] = 'Oops! We are unable to calculate distance.';
				return json_encode($response);die;
			} else {
				$distance = $this->get_distance($distance_cal->distance);
				$distance2 = $distance_cal->distance;
			}

			$setting = Configuration::where(['_id' => '5673e33e6734c4f874685c84'])->select('accra_charge', 'accra_charge_type')->first();
			$accra_commision = 0;
			$response = ['success' => 0, 'shipping_cost' => '', 'shipping_cost' => '', 'ghana_total_amount' => '', 'formated_currency' => '', 'total_weight' => 0];
			$return_array = [];

			foreach ($request_data->ProductList as $key) {
				$category = Category::where(['_id' => $key['productCategoryId'], 'TravelMode' => $key['travelMode'], 'Status' => 'Active'])->first();
				$key['InsuranceCost'] = 0;
				if ($category) {
					$weight += $this->get_weight($key['productWeight'], $key['ProductWeightUnit']);
					if ($key['InsuranceStatus'] == 'yes') {
						$key['InsuranceCost'] = $this->get_insurance($key['productCost'], $key['productQty']);
						$insurance += $key['InsuranceCost'];
					}

					$volume += $this->get_volume(array(
						"width" => $key['productWidth'],
						"widthunit" => $key['ProductLengthUnit'],
						"height" => $key['productHeight'],
						"heightunit" => $key['productHeightUnit'],
						"length" => $key['productLength'],
						"lengthunit" => $key['ProductLengthUnit'],
					));

					if (isset($category->Shipping) && is_array($category->Shipping)) {

						foreach ($category->Shipping as $key2) {
							if ($category->ChargeType == 'distance') {

								if ($distance >= $key2['MinDistance'] && $distance <= $key2['MaxDistance']) {
									$key['shippingCost'] = $key2['Rate'] * (int) $key['productQty'];
									$rate += $key2['Rate'] * (int) $key['productQty'];
								}
							} else if ($category->ChargeType == 'fixed') {
								if ($weight >= $key2['MinDistance'] && $weight <= $key2['MaxDistance']) {
									$key['shippingCost'] = $key2['Rate'] * (int) $key['productQty'];
									$rate += $key2['Rate'] * (int) $key['productQty'];

								}
							}
						}
					}
				}
			}

			$return_array = $request_data->ProductList;
			$response['success'] = 1;

			$response['accra_commision'] = 0;
			$response['shipping_cost'] = $rate;
			$response['insurance'] = $insurance;
			$response['total_amount'] = $response['shipping_cost'] + $insurance;
			$response['product'] = $return_array;
			$currency_conversion = $this->currency_conversion($response['total_amount']);

			if (Session()->get('Default_Currency') == 'GHS') {
				$response['user_currency'] = $currency_conversion['ghanian_cost'];
			} else if (Session()->get('Default_Currency') == 'CAD') {
				$response['user_currency'] = $currency_conversion['canadian_cost'];
			} else if (Session()->get('Default_Currency') == 'PHP') {
				$response['user_currency'] = $currency_conversion['philipins_cost'];
			} else if (Session()->get('Default_Currency') == 'GBP') {
				$response['user_currency'] = $currency_conversion->uk_cost;
			} else {
				$response['user_currency'] = $currency_conversion['ghanian_cost'];
			}

			$response['distance'] = $distance;
			$response['volume'] = $volume;

			$response['total_weight'] = $weight;

		}
		return json_encode($response);
	}

	public function local_prepare_request_calculation() {

		$response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		$response = ['success' => 0, 'shipping_cost' => '', 'shipping_cost' => '', 'ghana_total_amount' => '', 'formated_currency' => '', 'total_weight' => 0, 'insurance_cost' => '', 'reasion_charge' => '', 'item_price'];
		if (Input::get('request_id') == '') {
			$items = Additem::where(['user_id' => Session::get('UserId'), 'type' => 'local_delivery'])->get();
		} else {
			$items = Deliveryrequest::where(['_id' => Input::get('request_id')])->select('ProductList')->first();
			$items = $items->ProductList;
		}
		$data['item_price'] = 0;
		$data['category_price'] = 0;
		$data['distance_price'] = 0;
		$data['region_price'] = 0;
		$data['insurance_price'] = 0;
		$data['shipping_cost'] = 0;
		$data['total_weight'] = 0;
		$data['volume'] = 0;
		$data['distance'] = 0;

		$return_array = $items;

		if (count($items) == 0) {
			return json_encode($response);
		}
		$rate = $insurance = $weight = $volume = 0;
		$distance = Utility::getDistanceBetweenPointsNew(Input::get('PickupLat'), Input::get('PickupLong'), Input::get('DeliveryLat'), Input::get('DeliveryLong'));
		if ($distance == false) {
			$response['msg'] = 'Oops! We are unable to calculate distance.';
			return json_encode($response);die;
		}
		$distance = $distance * 0.000621371;
		$country2 = json_decode(Input::get('drop_off_country'));
		$state2 = json_decode(Input::get('drop_off_state'));

		$match_distance = Distance::Where('from', '<=', (float) $distance)->where('to', '>=', (float) $distance)
			->where(['Status' => 'Active', 'country_id' => $country2->id, "type" => "distance"])
			->select('price')
			->first();
		if (count($match_distance) == 0) {
			$response = ['success' => 0, 'msg' => 'The shipping address/distance you have specified is unsupported.'];
			return response()->json($response);
		}

		$match_value = Distance::where(['Status' => 'Active', 'country_id' => $country2->id, "type" => "item-value"])
			->select('price', 'from', 'to')->get();
		if (count($match_distance) == 0) {
			return response()->json($response);
		}

		if ($items) {
			foreach ($items as $key => $value) {
				# code...
				$value['shippingCost'] = 0;
				//Weight
				$weight += $this->get_weight($value['productWeight'], $value['ProductWeightUnit']);
				//Volume
				$volume += $this->get_volume(array(
					"width" => $value['productWidth'],
					"widthunit" => $value['productHeightUnit'],
					"height" => $value['productHeight'],
					"heightunit" => $value['productHeightUnit'],
					"length" => $value['productLength'],
					"lengthunit" => $value['productHeightUnit'],
				));
				//Insurance
				if ($value['InsuranceStatus'] == 'yes') {
					$value['InsuranceCost'] = $this->get_insurance($value['productCost'], $value['productQty']);
					$data['insurance_price'] += $value['InsuranceCost'];
				}
				//Price Item category
				$match_category = Category::where(['_id' => $value['productCategoryId'], 'country_id' => $country2->id, 'Status' => 'Active', 'type' => 'localCategory'])->select('price')
					->first();

				if ($match_category) {
					$data['category_price'] += $match_category->price;
					//$return_array[$key]['shippingCost'] = $match_category->price;
					$value['shippingCost'] = $match_category->price;
				} else {
					$response = ['success' => 0, 'msg' => "The shipping category you have specified is unsupported for product '" . $value['product_name'] . '.'];
					return response()->json($response);
				}

				$temp = 0;
				foreach ($match_value as $key2) {
					
					if ($key2->from <= (float) trim($value['productCost']) && $key2->to >= (float) trim($value['productCost'])) {
						$data['item_price'] += $key2->price;
						$value['shippingCost'] += $key2->price;
						$temp = $key2->price;
						//$return_array[$key]['shippingCost'] += $match_category->price;
					}
					 
				}

				if($temp == 0){
					$response = ['success' => 0, 'msg' => "The item value you have specified is unsupported for product '" . $value['product_name'] . '.'];
					return response()->json($response);
				}

				

				$data['distance_price'] += $match_distance->price;
				$value['shippingCost'] += $match_distance->price;

			}
		}

		//extraa charge of out side accra
		$data['region_price'] = $this->resionCharges(['name' => $state2->name, 'id' => $state2->id]);
		//end accra charge
		$data['shipping_cost'] = $data['category_price'] + $data['item_price'] + $data['distance_price'];
		$data['total_amount'] = $data['shipping_cost'] + $data['region_price'] + $data['insurance_price'];
		// print_r($data['category_price']); die;
		$data['volume'] = $volume;
		$data['total_weight'] = $weight;
		$data['distance'] = $distance;
		$data['product'] = $items;
		$data['item_count'] = count($items);
		$currency_conversion = $this->currency_conversion($data['total_amount']);

		if (Session::get('Default_Currency') == 'GHS') {
			$data['user_currency'] = $currency_conversion['ghanian_cost'];
		} else if (Session()->get('Default_Currency') == 'CAD') {
			$data['user_currency'] = $currency_conversion['canadian_cost'];
		} else if (Session()->get('Default_Currency') == 'PHP') {
			$data['user_currency'] = $currency_conversion['philipins_cost'];
		} else if (Session()->get('Default_Currency') == 'GBP') {
			$data['user_currency'] = $currency_conversion['uk_cost'];
		} else {
			$data['user_currency'] = $currency_conversion['ghanian_cost'];
		}

		//$data['product_list'] = $items;

		$response = ['success' => 1, 'msg' => 'Calculation', 'result' => $data, 'product' => $return_array];
		//print_r($data);die;
		//return response()->json($response);
		return json_encode($response);
	}

	public function local_delivery_detail($id) {
		$data['request_data'] = Deliveryrequest::where(['_id' => $id])->first();
		if (count($data['request_data']) > 0) {
			$data['total_item'] = 0;
			if (isset($data['request_data']['ProductList'])) {
				foreach ($data['request_data']['ProductList'] as $key) {
					$data['total_item'] = $data['total_item'] + 1;
				}
			}
		}
		return view('User::local_delivery.requester_local_delivery_detail', $data);
	}

	public function get_volume($array) {
		return ($this->get_size_in_cm($array['length'], $array['lengthunit']) * $this->get_size_in_cm($array['width'], $array['widthunit']) * $this->get_size_in_cm($array['height'], $array['heightunit']));
	}

	public function get_size_in_cm($height, $unit) {

		$in_cm = (float) $height;
		$unit = strtolower($unit);
		if ($unit == 'inches') {
			$in_cm = $in_cm * 2.54;
		}
		return $in_cm;

	}

	public function outsideOfAccraCharge($address) {
		$distance = Utility::getDistanceBetweenPointsNew(5.5913754, -0.2499413, $address['lat'], $address['lng']);
		$charge = false;
		if ($distance > 0) {
			$distance_km = $distance / 1000;
			$range = 30;
			if ($distance_km > 30) {
				$charge = true;
			}
		}
		return $charge;
	}

	public function currency_conversion($shipcost) {

		$data = ['cost' => 0, 'ghanian_cost' => 0, 'philipins_cost' => 0, 'uk_cost' => 0, 'canadian_cost' => 0];
		$currency = Currency::where(['Status' => 'Active'])->select('CurrencyRate', 'FormatedText', 'CurrencyCode')->get();

		if ($currency) {
			foreach ($currency as $key) {
				if ($key->CurrencyCode == 'GHS') {
					$data['ghanian_cost'] = str_replace('[AMT]', number_format($shipcost * $key->CurrencyRate, 2), $key->FormatedText);
				} else if ($key->CurrencyCode == 'CAD') {
					$data['canadian_cost'] = str_replace('[AMT]', number_format($shipcost * $key->CurrencyRate, 2), $key->FormatedText);
				} else if ($key->CurrencyCode == 'PHP') {
					$data['philipins_cost'] = str_replace('[AMT]', number_format($shipcost * $key->CurrencyRate, 2), $key->FormatedText);
				} else if ($key->CurrencyCode == 'GBP') {
					$data['uk_cost'] = str_replace('[AMT]', number_format($shipcost * $key->CurrencyRate, 2), $key->FormatedText);
				}
			}
		}
		return $data;

	}

	public function get_distance($distance) {
		return floatval($distance) * 0.000621371;
	}

	public function get_weight($weight, $type) {
		$type = strtolower($type);
		if ($type == 'kg') {
			return (((float) $weight) * 2.20462);
		} else if ($type == 'gram') {
			return (((float) $weight) * 0.00220462);
		} else {
			return $weight;
		}
	}

	public function get_insurance($cost, $quantity) {
		$data = Configuration::where(array('_id' => '5673e33e6734c4f874685c84'))
			->select('Insurance')->first();

		$value = 0;

		if (count($data) > 0) {
			if (is_array($data->Insurance)) {
				foreach ($data->Insurance as $key) {
					if ($key['MinPrice'] <= (float) $cost && $key['MaxPrice'] >= (float) $cost) {
						$value = $key['Rate'] * (int) $quantity;
					}
				}
			}
		}

		return $value;
	}

	public function local_add_item_page($id = "") {
		$data['transporter_data'] = array();
		$data['country'] = CityStateCountry::where(['type' => 'Country', 'Status' => 'Active'])
			->orderBy('Content', 'Asc')
			->get(['_id', 'Content', 'state_available']);

		$data['category'] = Category::where(['type' => 'localCategory', 'Status' => 'Active', "country_id" => "56bdbfb4cf32079714ee89ca"])->get();
		//echo '<pre>';

		//print_r($data['category']); die;

		$data['items_count'] = Additem::where(['user_id' => Session::get('UserId'), 'type' => 'local_delivery'])->count();
		if ($id != '') {
			return view('User::local_delivery.edit.add_item', $data);
		} else {
			return view('User::local_delivery.prepare_add_item', $data);
		}

	}

	public function local_post_add_item() {
		$response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		$ProductImage = '';
		$otherImages = [];
		if (Input::get('request_id') != '') {
			$item = Additem::where(['_id' => Input::get('request_id')])->first();
			$ProductImage = $item->ProductImage;
			$ProductImage = $item->ProductImage;
			//$otherImages = $item->OtherImage;
		}

		if (Input::hasFile('default_image')) {
			if (Input::file('default_image')->isValid()) {
				$ext = Input::file('default_image')->getClientOriginalExtension();
				$ProductImage = time() . rand(100, 9999) . ".$ext";

				if (Input::file('default_image')->move(BASEURL_FILE . 'package/', $ProductImage)) {
					$ProductImage = "package/$ProductImage";
				} else {
					$ProductImage = '';
				}
			}
		}

		$imageArray = array('package_image2', 'package_image3', 'package_image4', 'package_image5');
		foreach ($imageArray as $key => $val) {
			if (Input::hasFile($val)) {
				if (Input::file($val)->isValid()) {
					$ext = Input::file($val)->getClientOriginalExtension();
					$img = time() . rand(100, 9999) . ".$ext";

					if (Input::file($val)->move(BASEURL_FILE . 'package/', $img)) {
						$img = "package/$img";
						if (isset($otherImages[$key])) {
							// Start remove old image
							if (!empty(trim($otherImages[$key]))) {
								if (file_exists(BASEURL_FILE . $otherImages[$key])) {
									unlink(BASEURL_FILE . $otherImages[$key]);
								}
							}
							// End remove old image
							$otherImages[$key] = $img;
						} else {
							$otherImages[] = $img;
						}
					}
				}
			}
		}

		$lhwunit = 'cm';
		$weightunit = 'kg';
		if (Input::get('measurement_unit') == 'inches_lbs') {
			$lhwunit = 'inches';
			$weightunit = 'lbs';
		}
		$category = json_decode(Input::get('category'));
		//print_r($category); die;
		$add_item = [
			'product_name' => ucfirst(Input::get('title')),
			'user_id' => Session::get('UserId'),
			'type' => 'local_delivery',
			'productWidth' => Input::get('width'),
			'productHeight' => Input::get('height'),
			'productLength' => Input::get('length'),
			'productCost' => Input::get('package_value'),
			'productWeight' => Input::get('weight'),
			'productHeightUnit' => $lhwunit,
			'ProductWeightUnit' => $weightunit,
			'ProductLengthUnit' => $lhwunit,
			'productCategory' => @$category->name,
			'productCategoryId' => @$category->id,
			'travelMode' => Input::get('travel_mode'),
			'currency' => Input::get('Default_Currency'),
			'needInsurance' => Input::get('insurance'),
			"productQty" => (Input::get('quantity') < 1) ? 1 : Input::get('quantity'),
			'ProductImage' => $ProductImage,
			//'OtherImage'=>$otherImages,
			'ProductTitle' => ucfirst(Input::get('title')),
			"QuantityStatus" => (Input::get('quantity') > 1) ? 'multiple' : 'single',
			"BoxQuantity" => (Input::get('quantity') == 0) ? 1 : (int) Input::get('quantity'),
			"Description" => Input::get('description'),
			"InsuranceStatus" => (Input::get('insurance') == 'yes') ? 'yes' : 'no',
			"PackageMaterial" => (Input::get('need_package_material') == 'yes') ? 'yes' : 'no',
			"PackageMaterialShipped" => '',
		];

		if ($add_item['needInsurance'] == 'yes') {
			$insurance = $this->get_insurance($add_item['productCost'], $add_item['productQty']);
			if ($insurance == 0) {
				$response['msg'] = 'Sorry! We are not able to provide insurance.';
				return json_encode($response);
			}
		}
		if (Input::get('request_id') != '') {
			// print_r(Input::get('request_id')); die;
			if (Additem::where(['_id' => Input::get('request_id')])->update($add_item)) {
				$response = ['success' => 1, 'msg' => 'Item has been updated successfully.'];
			}
		} else {
			if (Additem::insert($add_item)) {
				$response = ['success' => 1, 'msg' => 'Item has been added successfully.'];
			}
		}
		return json_encode($response);
	}

	public function local_prepare_request() {
		$data['transporter_data'] = array();
		$data['RequestType'] = Input::get("local_delivery");
		$data['country'] = CityStateCountry::where(['type' => 'Country', 'Status' => 'Active'])
			->orderBy('Content', 'Asc')
			->get(['_id', 'Content', 'state_available']);

		$data['state'] = CityStateCountry::where(['type' => 'State', 'Status' => 'Active', 'CountryId' => '56bdbfb4cf32079714ee89ca'])
			->orderBy('Content', 'Asc')
			->get(['_id', 'Content', 'CountryId']);

		$data['category'] = Category::where(['type' => 'localCategory', 'Status' => 'Active', "country_id" => "56bdbfb4cf32079714ee89ca"])->get();

		$data['items'] = Additem::where(['user_id' => Session::get('UserId'), 'type' => 'local_delivery'])->get();
		$data['items_count'] = count($data['items']);

		if (count($data['items']) > 0) {
			return view('User::local_delivery.prepare-request', $data);
		} else {
			return view('User::local_delivery.prepare_add_item', $data);
		}

	}

	private function get_packageno() {
		return Utility::sequence('Request');
	}

	public function localDeliveryPriceEstimator() {

		$response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		$category = json_decode(Input::get('category'));
		$distance = 0;
		$weight = Input::get('weight');
		$weight_unit = Input::get('weight_unit');

		$city = json_decode(Input::get('city'));
		$state = json_decode(Input::get('state'));
		$country = json_decode(Input::get('country'));
		$city2 = json_decode(Input::get('city2'));
		$state2 = json_decode(Input::get('state2'));
		$country2 = json_decode(Input::get('country2'));

		//print_r($state2); die;

		$dest_add = $this->latlongapi2(Input::get('dropoff_address') . ' ' . $city->name . ' ,' . $state->name . ' ,' . $country->name);

		if ($dest_add != false) {
			$souce_add = $this->latlongapi2(Input::get('pickup_address') . ' ' . $city2->name . ' ,' . $state2->name . ' ,' . $country2->name);

			if ($souce_add != false) {
				$distance = Utility::getDistanceBetweenPointsNew($souce_add['lat'], $souce_add['lng'], $dest_add['lat'], $dest_add['lng']);
				if ($distance == false) {
					$response['msg'] = 'Oops! We are unable to calculate distance.';
					return json_encode($response);
					die;
				}
			} else {
				$response['msg'] = 'Oops! We are unable to find your source address, please correct.';
				return json_encode($response);
				die;
			}
		} else {
			$response['msg'] = 'Oops! We are unable to find your destination address, please correct.';
			return json_encode($response);
			die;
		}

		//Category calculation
		if ($category->id) {
			//,'country' => $country->id
			$match_category = Category::where(['_id' => $category->id, 'Status' => 'Active', 'type' => 'localCategory'])
				->select('price')
				->first();

			if ($match_category) {
				$response['success'] = 1;
				$response['category_price'] = $match_category->price;
			} else {
				$response = ['success' => 0, 'msg' => 'Sorry! Shipping condition does not match according to choose category.'];
			}
		}
		//Kumasi-Sunyani Rd .. Brong ahafo..sunyani
		//Kumasi City Mall..ashanti..kumasi

		//Item value calculation
		if (Input::get('pe_product_cost') != '' && $response['success'] == 1) {

			$match_value = Distance::Where('from', '<=', (float) trim(Input::get('pe_product_cost')))->where('to', '>=', (float) trim(Input::get('pe_product_cost')))->where(['Status' => 'Active', 'country_id' => $country->id, "type" => "item-value"])
				->select('price')
				->first();

			if ($match_value) {
				$response['success'] = 1;
				$response['item_price'] = $match_value->price;
			} else {
				$response = ['success' => 0, 'msg' => 'Sorry! Shipping condition does not match according to given item value.'];
			}
		}

		//Distance calculation
		if ($distance > 0 != '' && $response['success'] == 1) {
			//Distance in to miles
			$distance = $distance * 0.000621371;
			$match_distance = Distance::Where('from', '<=', (float) $distance)->where('to', '>=', (float) $distance)
				->where(['Status' => 'Active', 'country_id' => $country->id, "type" => "distance"])
				->select('price')
				->first();

			if ($match_distance) {
				$response['success'] = 1;
				$response['distance_price'] = $match_distance->price;
			} else {
				$response = ['success' => 0, 'msg' => 'Sorry! Shipping condition does not match according to choose addresses.'];
			}
		}

		if ($response['success'] == 1) {
			//Resion Charges
			$response['resionCharges'] = $this->resionCharges(['name' => $state->name, 'id' => $state->id]);
			$response['total_amount'] = $response['distance_price'] + $response['category_price'] + $response['item_price'] + $response['resionCharges'];

			//print_r($response['resionCharges']); die;
			$currency_conversion = $this->currency_conversion($response['total_amount']);
			$response['msg'] = "Calculation";
			$response['total_weight'] = $this->get_weight(Input::get('weight'), $weight_unit);
			$response['ghana_total_amount'] = $currency_conversion['ghanian_cost'];
			$response['canadian_cost'] = $currency_conversion['canadian_cost'];
			$response['philipins_cost'] = $currency_conversion['philipins_cost'];
			$response['uk_cost'] = $currency_conversion['uk_cost'];
		}
		return json_encode($response);
	}

	public function local_create_prepare_request() {

		$response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];

		$data['items'] = Additem::where(['user_id' => Session::get('UserId'), 'type' => 'local_delivery'])->get();
		$user = User::where(['_id' => Session::get('UserId')])->first();
		$PackageId = $this->get_packageno();
		$city = json_decode(Input::get('city'));
		$state = json_decode(Input::get('state'));
		$country = json_decode(Input::get('country'));

		$drop_off_city = json_decode(Input::get('drop_off_city'));
		$drop_off_state = json_decode(Input::get('drop_off_state'));
		$drop_off_country = json_decode(Input::get('drop_off_country'));

		$return_city = json_decode(Input::get('return_city'));
		$return_state = json_decode(Input::get('return_state'));
		$return_country = json_decode(Input::get('return_country'));

		$nd_return_city = json_decode(Input::get('nd_return_city'));
		$nd_return_state = json_decode(Input::get('nd_return_state'));
		$nd_return_country = json_decode(Input::get('nd_return_country'));

		$requester = User::where(['_id' => Session::get('UserId')])->first();

		if (count($requester) == 0) {
			return json_encode($response);die;
		}

		$calculated_distance = floatval(Input::get('calculated_distance'));
		if ($calculated_distance > 0) {
			$calculated_distance = $calculated_distance * 0.000621371;
		} else {
			$calculated_distance = Utility::getDistanceBetweenPointsNew(
				floatval(Input::get('PickupLat')),
				floatval(Input::get('PickupLong')),
				floatval(Input::get('DeliveryLat')),
				floatval(Input::get('DeliveryLong'))
			);
		}

		$insert = [
			'RequesterId' => new MongoId(Session::get('UserId')),
			'RequesterName' => ucfirst($requester->Name),
			'ProductTitle' => '',
			'RequestType' => "local_delivery",
			'Status' => 'pending',
			'PackageId' => $PackageId,
			'device_version' => Input::get('browser'),
			'app_version' => Input::get('version'),
			'device_type' => Input::get('device_type'),
			"PackageNumber" => $PackageId . time(),
			"PickupFullAddress" => $this->formated_address([
				(Input::get('address_line_1') != '') ? Input::get('address_line_1') : '',
				(Input::get('address_line_2') != '') ? Input::get('address_line_2') : '',
				$city->name,
				$state->name,
				$country->name,
			], Input::get('zipcode')),
			'PickupAddress' => Input::get('address_line_1'),
			'PickupAddress2' => Input::get('address_line_2'),
			'PickupCity' => $city->name,
			'PickupState' => $state->name,
			'PickupCountry' => $country->name,
			'PickupPinCode' => Input::get('zipcode'),
			'PickupLatLong' => [floatval(Input::get('PickupLong')), floatval(Input::get('PickupLat'))],
			'PickupDate' => get_utc_time(Input::get('pickup_date')),
			'DeliveryFullAddress' => $this->formated_address([
				(Input::get('drop_off_address_line_1') != '') ? Input::get('drop_off_address_line_1') : '',
				(Input::get('drop_off_address_line_2') != '') ? Input::get('drop_off_address_line_2') : '',
				$drop_off_city->name,
				$drop_off_state->name,
				$drop_off_country->name,
			], Input::get('drop_off_zipcode')),
			'DeliveryAddress' => Input::get('drop_off_address_line_1'),
			'DeliveryAddress2' => Input::get('drop_off_address_line_2'),
			'DeliveryCity' => $drop_off_city->name,
			'DeliveryState' => $drop_off_state->name,
			'DeliveryCountry' => $drop_off_country->name,
			'DeliveryPincode' => Input::get('drop_off_zipcode'),
			"DeliveryLatLong" => [floatval(Input::get('DeliveryLong')), floatval(Input::get('DeliveryLat'))],
			'DeliveryDate' => get_utc_time(Input::get('drop_off_date')),

			"ReturnFullAddress" => $this->formated_address([
				Input::get('return_address_line_1'),
				Input::get('return_address_line_2'),
				@$return_city->name,
				@$return_state->name,
				@$return_country->name,
			], Input::get('return_zipcode')),
			"ReturnAddress" => Input::get('return_address_line_1'),
			"ReturnAddress2" => Input::get('return_address_line_2'),
			"ReturnCityTitle" => @$return_city->name,
			"ReturnStateTitle" => @$return_state->name,
			'ReturnCountry' => @$return_country->name,
			'ReturnPincode' => Input::get('return_zipcode'),
			"FlexibleDeliveryDate" => (Input::get('is_delivery_date_flexible') == 'yes') ? 'yes' : 'no',
			"JournyType" => (Input::get('journey_type') == 'return') ? 'return' : 'one_way',
			'NotDelReturnFullAddress' => $this->formated_address([
				Input::get('nd_return_address_line_1'),
				Input::get('nd_return_address_line_2'),
				@$nd_return_city->name,
				@$nd_return_state->name,
				@$nd_return_country->name,
			], Input::get('nd_return_zipcode')),
			"InCaseNotDelReturnAddress" => Input::get('nd_return_address_line_1'),
			"InCaseNotDelReturnAddress2" => Input::get('nd_return_address_line_2'),
			"InCaseNotDelReturnCity" => @$nd_return_city->name,
			"InCaseNotDelReturnState" => @$nd_return_state->name,
			"InCaseNotDelReturnCountry" => @$nd_return_country->name,
			"InCaseNotDelReturnPincode" => Input::get('nd_return_zipcode'),
			"PackageMaterial" => (Input::get('need_package_material') == 'yes') ? 'yes' : 'no',
			"PackageMaterialShipped" => '',
			"PublicPlace" => (Input::get('public_palce') == 'yes') ? 'yes' : 'no',
			'ReceiverCountrycode' => Input::get('country_code'),
			'ReceiverMobileNo' => Input::get('phone_number'),
			'FlexibleDeliveryDate' => Input::get('is_delivery_date_flexible'),
			"Distance" => $calculated_distance,
			'ShippingCost' => 0,
			'InsuranceCost' => 0,
			'Discount' => 0,
			'TotalCost' => 0,
			'AreaCharges' => 0,
			'after_update_difference' => 0,
			'promocode' => Input::get('promocode'),
			'drop_off_state_id' => $drop_off_state->id,
			'country_id' => $drop_off_country->id,
			'ProductList' => [],
		];

		if (Input::get('request_id') == '') {
			$res = json_decode($this->local_prepare_request_calculation());
			$insert["EnterOn"] = new MongoDate();
		} else {
			$res = json_decode($this->local_prepare_request_calculation());

		}

		if (count($res->product) > 0) {
			$k = 0;
			foreach ($res->product as $key) {
				$k++;
				if (!empty(trim($insert["ProductTitle"]))) {
					$insert["ProductTitle"] = "{$insert['ProductTitle']}, ";
				}
				$insert["ProductTitle"] .= @$key->product_name;
				$insert['ShippingCost'] += @$key->shippingCost;
				$insert['InsuranceCost'] += @$key->InsuranceCost;
				$insert['TotalCost'] += @$key->shippingCost+@$key->InsuranceCost;

				$insert['ProductList'][] = [
					'_id' => (Input::get('request_id') == '') ? (string) new MongoId() : $key->_id,
					"product_name" => (@$key->product_name != '') ? @$key->product_name : @$key->item_name,
					'package_id' => (Input::get('request_id') == '') ? $insert['PackageNumber'] . $k : $key->package_id,
					'status' => (Input::get('request_id') == '') ? 'pending' : $key->status,
					"productWidth" => $key->productWidth,
					"productHeight" => $key->productHeight,
					"productLength" => $key->productLength,
					"productCost" => $key->productCost,
					"productWeight" => $key->productWeight,
					"productHeightUnit" => $key->productHeightUnit,
					"ProductWeightUnit" => $key->ProductWeightUnit,
					"ProductLengthUnit" => $key->ProductLengthUnit,
					"productCategory" => $key->productCategory,
					"productCategoryId" => $key->productCategoryId,
					"travelMode" => $key->travelMode,
					"InsuranceStatus" => $key->InsuranceStatus,
					"productQty" => $key->productQty,
					"ProductImage" => $key->ProductImage,
					"QuantityStatus" => $key->QuantityStatus,
					"BoxQuantity" => $key->BoxQuantity,
					"Description" => $key->Description,
					"PackageMaterial" => $key->PackageMaterial,
					"PackageMaterialShipped" => $key->PackageMaterialShipped,
					"shippingCost" => @$key->shippingCost,
					'InsuranceCost' => @$key->InsuranceCost,
					"DeliveryVerifyCode" => (Input::get('request_id') == '') ? rand(1000, 9999) : $key->DeliveryVerifyCode,
					'tpid' => '',
					'tpName' => '',
					'inform_mail_sent' => 'no',
					'PaymentStatus' => 'no',
					'total_cost' => @$key->shippingCost+@$key->InsuranceCost,
					'after_update' => @$key->shippingCost+@$key->InsuranceCost,
					'TransporterFeedbcak' => '',
					'TransporterRating' => '',
					'RequesterFeedbcak' => '',
					'RequesterRating' => '',
					'EnterOn' => '',
					'ExpectedDate' => '',
				];
			}

			$discount = Promo::get_validate(Input::get('promocode'), $insert['ShippingCost'], Session::get('UserId'), $user->Default_Currency);
			$insert['AreaCharges'] = $this->resionCharges(['name' => $drop_off_state->name, 'id' => $drop_off_state->id]);
			$insert['TotalCost'] = $insert['TotalCost'] + $insert['AreaCharges'];
			if ($discount['success'] == 1) {
				$insert['discount'] = $discount['discount'];
				$insert['TotalCost'] = $insert['TotalCost'] - $discount['discount'];
			}
			//print_r($insert);die;

		} else {
			return json_encode($response);die;
		}

		if (Input::get('request_id') == '') {
			$record = (String) Deliveryrequest::insertGetId($insert);

			$ActivityLog = DeliveryRequest::where('_id', '=', $record)->select('_id', 'PackageNumber', 'ProductList')->first();

			$array = $ActivityLog->ProductList;
			foreach ($array as $key) {
				/* Activity Log */
				$insertactivity = [
					'request_id' => $record,
					'request_type' => 'local_delivery',
					'PackageNumber' => $ActivityLog->PackageNumber,
					'item_id' => $key['_id'],
					'package_id' => $key['package_id'],
					'item_name' => $key['product_name'],
					'log_type' => 'request',
					'message' => 'Package has been created.',
					'status' => 'pending',
					'EnterOn' => new MongoDate(),

				];
				Activitylog::insert($insertactivity);
			}

		} else {
			$record = Deliveryrequest::where(['_id' => trim(Input::get('request_id')), 'RequesterId' => new MongoId(Session::get('UserId'))])
				->update($insert);
		}

		if ($record) {
			if (Input::get('request_id') == '') {
				Additem::where(['user_id' => Session::get('UserId'), 'type' => 'local_delivery'])->delete();
				$response = ['success' => 1, 'msg' => 'Request has been created successfully.', 'reqid' => $record, 'type' => 'create'];
			} else {
				$response = ['success' => 1, 'msg' => 'Request has been updated successfully.', 'reqid' => trim(Input::get('request_id')), 'type' => 'update'];
			}
		}

		return json_encode($response);

	}

	public function local_pay_now($requestid, $id, Request $request) {
		$update['Status'] = 'ready';
		$where = [
			'RequesterId' => new MongoId(Session()->get('UserId')),
			'_id' => $requestid,
		];
		$user = User::where(array('_id' => Session()->get('UserId')))->first();

		$del_detail = Deliveryrequest::where($where)->whereIn('Status', ['pending', 'ready'])->first();

		if (count($del_detail) <= 0 || count($user) <= 0) {
			return Redirect::to("process-card-list/{$requestid}")->with('danger', "Fail! Something is wrong");
		}

		if (count($del_detail) > 0 && count($user) > 0) {
			if (!empty($del_detail->StripeChargeId)) {
				RSStripe::refund($del_detail->StripeChargeId);
			}
			$res = Payment::capture($user, $del_detail->TotalCost, $request->segment(3),true);

			if (isset($res['id'])) {

				$update['StripeChargeId'] = $res['id'];
				Transaction::insert(array(
					"SendById" => (String) $user->_id,
					"SendByName" => @$user->RequesterName,
					"SendToId" => "aquantuo",
					"RecieveByName" => "Aquantuo",
					"Description" => "Amount deposited against delivery request for {$del_detail->ProductTitle}, PackageId: {$del_detail->PackageNumber} from {$del_detail->PickupFullAddress} to {$del_detail->DeliveryFullAddress}",
					"Credit" => floatval($del_detail->TotalCost),
					"Debit" => "",
					"Status" => "pending",
					"TransactionType" => "delivery_request",
					"EnterOn" => new MongoDate(),
				));

				if (isset($del_detail->ProductList)) {
					$p_array = $del_detail->ProductList;
					foreach ($p_array as $key => $value) {
						$p_array[$key]['status'] = 'ready';
						$p_array[$key]['PaymentStatus'] = 'yes';

						/* Activity Log */
						$insertactivity = [
							'request_id' => $requestid,
							'request_type' => $del_detail->RequestType,
							'PackageNumber' => $del_detail->PackageNumber,
							'item_id' => $value['_id'],
							'package_id' => $value['package_id'],
							'item_name' => $value['product_name'],
							'log_type' => 'request',
							'message' => 'Payment successful.',
							'status' => 'ready',
							'EnterOn' => new MongoDate(),
						];
						Activitylog::insert($insertactivity);
					}
					$update['ProductList'] = $p_array;
				}

				if (Deliveryrequest::where($where)->update($update)) {
					Reqhelper::send_notification_to_tp(['id' => $requestid,
						'request_type' => Input::get('request_type')]);

					Notification::Insert([
						array(
							"NotificationTitle" => trans('lang.SEND_CREATE_TITLE'),
							"NotificationMessage" => sprintf(trans('lang.SEND_CREATE_MSG'), $del_detail->ProductTitle, $del_detail->PackageNumber),
							"NotificationUserId" => array(),
							"NotificationReadStatus" => 0,
							"location" => "request_detail",
							"locationkey" => (string) $requestid,
							"Date" => new MongoDate(),
							"GroupTo" => "Admin",
						),
					]);

					RequestCard::Insert([
						'request_id' => $requestid,
						'item_id' => '',
						'card_id' => $request->segment(3),
						'type' => 'local_delivery',
						'payment' => $del_detail->TotalCost + $del_detail->AquantuoFees,
						'brand' => @$res['source']['brand'],
						'last4' => @$res['source']['last4'],
						'description' => "Payment done for request Title: '" . $del_detail->ProductTitle . "' Package Id:" . $del_detail->PackageNumber . ".",
						"EnterOn" => new MongoDate(),
					]);

					if ($user->EmailStatus == "on") {
						$cron_mail = [
							"USERNAME" => ucfirst($user->Name),
							"PACKAGETITLE" => ucfirst($del_detail->ProductTitle),
							"PACKAGEID" => $del_detail->PackageId,
							"SOURCE" => $del_detail->PickupFullAddress,
							"DESTINATION" => $del_detail->DeliveryFullAddress,
							"PACKAGENUMBER" => $del_detail->PackageNumber,
							"SHIPPINGCOST" => $del_detail->ShippingCost,
							"TOTALCOST" => $del_detail->TotalCost,
							'email_id' => '5694a10f5509251cd67773eb',
							'email' => $user->Email,
							'status' => 'ready',
						];
						SendMail::insert($cron_mail);
					}

					$setting = Setting::find('563b0e31e4b03271a097e1ca');
					if (count($setting) > 0) {
						$cron_mail2 = [
							"USERNAME" => ucfirst($user->Name),
							"PACKAGETITLE" => ucfirst($del_detail->ProductTitle),
							"PACKAGEID" => $del_detail->PackageId,
							"SOURCE" => $del_detail->PickupFullAddress,
							"DESTINATION" => $del_detail->DeliveryFullAddress,
							"PACKAGENUMBER" => $del_detail->PackageNumber,
							"SHIPPINGCOST" => $del_detail->ShippingCost,
							"TOTALCOST" => $del_detail->TotalCost,
							'email_id' => '56cd3e1f5509251cd677740c',
							'email' => $setting->SupportEmail,
							'status' => 'ready',
						];
						SendMail::insert($cron_mail2);
					}

					return Redirect::to("local-delivery-detail/{$requestid}")->with('success', "Success! Your item has been successfully posted. Once a transporter reviews and accepts it, you will be notified.");
				}

			}
			return Redirect::to("local-delivery-detail/{$requestid}")->with('danger', "Your card has been Expaired.");
		}
	}

	public function local_my_request() {
		$data['paginationUrl'] = "pagination/new-my-request";
		$data['postValue'] = "&search=" . Input::get('search') . "&search_date=" . Input::get('search_date') . "&Status=" . Input::get('Status');
		return view('User::list.my-request', array('data' => $data));
	}

	public function localAddItemInExistRequest() {
		$response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		if (Input::get('request_id') != '') {
			$data['request_data'] = Deliveryrequest::where(['_id' => trim(Input::get('request_id'))])->select('ProductList')->first();
			$lhwunit = 'cm';
			$weightunit = 'kg';
			if (Input::get('measurement_unit') == 'inches_lbs') {
				$lhwunit = 'inches';
				$weightunit = 'lbs';
			}

			$ProductImage = '';

			if (Input::hasFile('default_image')) {

				if (Input::file('default_image')->isValid()) {
					$ext = Input::file('default_image')->getClientOriginalExtension();
					$ProductImage = time() . rand(100, 9999) . ".$ext";

					if (Input::file('default_image')->move(BASEURL_FILE . 'package/', $ProductImage)) {
						$ProductImage = "package/$ProductImage";
					} else {
						$ProductImage = '';
					}
				}
			}
			$otherImages = [];
			$imageArray = array('package_image2', 'package_image3', 'package_image4', 'package_image5');
			foreach ($imageArray as $key => $val) {
				if (Input::hasFile($val)) {
					if (Input::file($val)->isValid()) {
						$ext = Input::file($val)->getClientOriginalExtension();
						$img = time() . rand(100, 9999) . ".$ext";

						if (Input::file($val)->move(BASEURL_FILE . 'package/', $img)) {
							$img = "package/$img";
							if (isset($otherImages[$key])) {
								// Start remove old image
								if (!empty(trim($otherImages[$key]))) {
									if (file_exists(BASEURL_FILE . $otherImages[$key])) {
										unlink(BASEURL_FILE . $otherImages[$key]);
									}
								}
								// End remove old image
								$otherImages[$key] = $img;
							} else {
								$otherImages[] = $img;
							}
						}
					}
				}
			}
			$category = json_decode(Input::get('category'));

			$add_item = [
				"_id" => new MongoId(),
				'product_name' => ucfirst(Input::get('title')),
				'user_id' => Session::get('UserId'),
				'type' => 'local_delivery',
				'productWidth' => Input::get('width'),
				'productHeight' => Input::get('height'),
				'productLength' => Input::get('length'),
				'productCost' => Input::get('package_value'),
				'productWeight' => Input::get('weight'),
				'productHeightUnit' => $lhwunit,
				'ProductWeightUnit' => $weightunit,
				'ProductLengthUnit' => $lhwunit,
				'productCategory' => @$category->name,
				'productCategoryId' => @$category->id,
				'travelMode' => Input::get('travel_mode'),
				'currency' => Input::get('Default_Currency'),
				'needInsurance' => Input::get('insurance'),
				"productQty" => (Input::get('quantity') < 1) ? 1 : Input::get('quantity'),
				'ProductImage' => $ProductImage,
				"QuantityStatus" => (Input::get('quantity') > 1) ? 'multiple' : 'single',
				"BoxQuantity" => (Input::get('quantity') == 0) ? 1 : (int) Input::get('quantity'),
				"Description" => Input::get('description'),
				"InsuranceStatus" => (Input::get('insurance') == 'yes') ? 'yes' : 'no',
				"PackageMaterial" => (Input::get('need_package_material') == 'yes') ? 'yes' : 'no',
				"PackageMaterialShipped" => '',
			];

			if (isset($data['request_data']->ProductList)) {
				$update = Deliveryrequest::where(['_id' => trim(Input::get('request_id'))])->push(['ProductList' => $add_item]);
				if ($update) {
					$response = ['success' => 1, 'msg' => 'Item has been added successfully.'];
				}
			}
		}
		return json_encode($response);
	}

	public function localEditItemInExistRequest() {
		//echo "hi";die;
		$response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];

		if (Input::get('request_id') != '' && Input::get('item_id') != '') {
			$request_data = Deliveryrequest::where(['_id' => trim(Input::get('request_id')), 'RequesterId' => new MongoId(Session::get('UserId'))])
				->select('ProductList')->first();

			if ($request_data) {

				$update_array = $request_data->ProductList;
				$ProductImage = '';
				$otherImages = [];

				$lhwunit = 'cm';
				$weightunit = 'kg';
				if (Input::get('measurement_unit') == 'inches_lbs') {
					$lhwunit = 'inches';
					$weightunit = 'lbs';
				}

				if (Input::hasFile('default_image')) {
					if (Input::file('default_image')->isValid()) {
						$ext = Input::file('default_image')->getClientOriginalExtension();
						$ProductImage = time() . rand(100, 9999) . ".$ext";
						if (Input::file('default_image')->move(BASEURL_FILE . 'package/', $ProductImage)) {
							$ProductImage = "package/$ProductImage";
						} else {
							$ProductImage = '';
						}
					}
				}

				$imageArray = array('package_image2', 'package_image3', 'package_image4', 'package_image5');
				foreach ($imageArray as $key => $val) {
					if (Input::hasFile($val)) {
						if (Input::file($val)->isValid()) {
							$ext = Input::file($val)->getClientOriginalExtension();
							$img = time() . rand(100, 9999) . ".$ext";

							if (Input::file($val)->move(BASEURL_FILE . 'package/', $img)) {
								$img = "package/$img";
								if (isset($otherImages[$key])) {
									// Start remove old image
									if (!empty(trim($otherImages[$key]))) {
										if (file_exists(BASEURL_FILE . $otherImages[$key])) {
											unlink(BASEURL_FILE . $otherImages[$key]);
										}
									}
									// End remove old image
									$otherImages[$key] = $img;
								} else {
									$otherImages[] = $img;
								}
							}
						}
					}
				}

				$category = json_decode(Input::get('category'));
				foreach ($request_data->ProductList as $key => $value) {
					if ($value['_id'] == trim(Input::get('item_id'))) {

						$update_array[$key]['product_name'] = Input::get('title');
						$update_array[$key]['productWidth'] = Input::get('width');
						$update_array[$key]['productHeight'] = Input::get('height');
						$update_array[$key]['productLength'] = Input::get('length');
						$update_array[$key]['productCost'] = trim(Input::get('package_value'));
						$update_array[$key]['productWeight'] = trim(Input::get('weight'));
						$update_array[$key]['productHeightUnit'] = trim($lhwunit);
						$update_array[$key]['ProductWeightUnit'] = trim($weightunit);
						$update_array[$key]['ProductLengthUnit'] = trim($lhwunit);
						$update_array[$key]['productCategory'] = $category->name;
						$update_array[$key]['productCategoryId'] = $category->id;
						$update_array[$key]['travelMode'] = trim(Input::get('travel_mode'));
						$update_array[$key]['InsuranceStatus'] = (Input::get('insurance') == 'yes') ? 'yes' : 'no';
						$update_array[$key]['productQty'] = (Input::get('quantity') < 1) ? 1 : Input::get('quantity');
						$update_array[$key]['ProductImage'] = ($ProductImage == '') ? $value['ProductImage'] : $ProductImage;
						//$update_array[$key]['OtherImage'] = (empty($otherImages))? $value['OtherImage'] : $otherImages;
						$update_array[$key]['QuantityStatus'] = (Input::get('quantity') > 1) ? 'multiple' : 'single';
						$update_array[$key]['BoxQuantity'] = (Input::get('quantity') == 0) ? 1 : (int) Input::get('quantity');
						$update_array[$key]['Description'] = Input::get('description');
						$update_array[$key]['PackageMaterial'] = (Input::get('need_package_material') == 'yes') ? 'yes' : 'no';
						$update_array[$key]['PackageMaterialShipped'] = "";

						if (Input::get('insurance') == 'yes') {
							$insurance = $this->get_insurance(Input::get('package_value'), Input::get('package_value'));
							if ($insurance == 0) {
								$response['msg'] = 'Sorry! We are not able to provide insurance.';
								return json_encode($response);
							}
						}

					}
				}

				$update_data = Deliveryrequest::where(['_id' => trim(Input::get('request_id')), 'RequesterId' => new MongoId(Session::get('UserId'))])
					->update(['ProductList' => $update_array]);
				if ($update_data) {
					$response = ['success' => 1, 'msg' => 'Item has been updated successfully.'];
				}
			}

		}
		return json_encode($response);
	}

	public function local_remove_item($id, $requestid) {
		$response = array('success' => 0, 'msg' => 'Fail to delete request.');
		if ($requestid) {
			$res_data = Deliveryrequest::where(['_id' => trim($requestid), 'RequesterId' => new MongoId(Session::get('UserId'))])
				->select('ProductList')->first();
			if (count($res_data) > 0) {
				if (isset($res_data->ProductList)) {
					$update_array = $res_data->ProductList;
					foreach ($update_array as $key => $value) {
						if ($value['_id'] == $id) {
							unset($update_array[$key]);
						}
					}
					$update = Deliveryrequest::where(['_id' => trim($requestid), 'RequesterId' => new MongoId(Session::get('UserId'))])
						->update(['ProductList' => $update_array]);
					if ($update) {
						$response['success'] = 1;
						$response['msg'] = 'Item has been removed successfully.';
					}
				}

			}
		}
		return json_encode($response);
	}

	// Resion charge
	public function resionCharges($state) {
		//print_r($state['id']); die;
		$charges = Extraregion::where(['state_id' => $state['id'], 'status' => 'Active'])->select('amount')->first();
		if ($charges) {
			return $charges->amount;
		} else {
			return 0.00;
		}
	}

}
