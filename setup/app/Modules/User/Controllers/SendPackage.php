<?php namespace App\Modules\User\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Models\Activitylog;
use App\Http\Models\Additem;
use App\Http\Models\Category;
use App\Http\Models\CityStateCountry;
use App\Http\Models\Configuration;
use App\Http\Models\Currency;
use App\Http\Models\Deliveryrequest;
use App\Http\Models\Extraregion;
use App\Http\Models\Notification;
use App\Http\Models\RequestCard;
use App\Http\Models\SendMail;
use App\Http\Models\Setting;
use App\Http\Models\Transaction;
use App\Http\Models\User;
use App\Http\Models\Itemhistory;
use App\Library\Promo;
use App\Library\Reqhelper;
use App\Library\RSStripe;
use App\Library\Utility;
use App\Library\upsRate;
use Illuminate\Http\Request;
use Input;
use MongoDate;
use MongoId;
use Redirect;
use Session;
use App\Http\Models\Trips;
use App\Http\Models\Item;
use App\Http\Models\PaymentInfo;

class SendPackage extends Controller {

	public function __construct() {
		if (!session()->has('UserId')) {
			return Redirect::to('login')->send();
		}

		$user = User::where(["_id" => session()->get('UserId')])
			->where(['delete_status' => 'yes'])
			->first();
		if (count($user) > 0) {
			Session::flush();
			return Redirect::to('login');
		}
	}
	
	public function formated_address($array, $zip) {
		$address = '';
		foreach ($array as $ky => $val) {
			if (trim($val) != '') {
				if (trim($address) != '') {$address = "$address, ";}
				$address .= $val;
			}
		}
		if (trim($zip) != '') {$address .= "- $zip";}
		return $address;
	}

	public function sendPackage() {
		$data['items'] = Additem::where(['user_id' => Session::get('UserId'), 'type' => 'delivery'])->get();
		return view('User::Buy.send_package_items', $data);
	}

	public function editSendPackage($id, $requestid = "") {
		$data['transporter_data'] = array();
		$data['trip_data'] = array();
		if(Session::get('session_trip') != ''){
			$data['trip_data'] = Trips::where(array('_id' => Session::get('session_trip')))->first();
	        if (!count($data['trip_data']) > 0) {
	        	return redirect('near-by-transporter');
	        }

			$data['transporter_data'] = User::where(array('_id' => $data['trip_data']->TransporterId))->first();
			$data['category2'] = Category::where(['type' => 'Category', 'Status' => 'Active'])->whereIn('TravelMode', ['ship', 'air'])->whereIn('Content', $data['trip_data']->SelectCategory)->get(['TravelMode', 'Content']);
		}


		$data['item'] = Item::Where(['Status' => 'Active'])->get();
		$data['country'] = CityStateCountry::where(['type' => 'Country', 'Status' => 'Active'])
			->orderBy('Content', 'Asc')
			->get(['_id', 'Content', 'state_available']);
		$data['category'] = Category::where(['type' => 'Category', 'Status' => 'Active'])->whereIn('TravelMode', ['ship', 'air'])->get(['TravelMode', 'Content']);
		if ($requestid != '') {
			$data['delivery_data'] = Deliveryrequest::where(['_id' => $requestid])
				->select('ProductList')->first();
		} else {
			$data['delivery_data'] = Additem::where(['user_id' => Session::get('UserId'), 'type' => 'delivery', '_id' => $id])->first();
		}
		$data['items_count'] = 1;
		if ($data['delivery_data']) {
			if ($requestid != '') {
				$array = [];
				foreach ($data['delivery_data']->ProductList as $key) {
					if ($key['_id'] == $id) {
						$array = [
							"_id" => $key['_id'],
							"product_name" => $key['product_name'],
							"productWidth" => $key['productWidth'],
							"productHeight" => $key['productHeight'],
							"productLength" => $key['productLength'],
							"productCost" => $key['productCost'],
							"productWeight" => $key['productWeight'],
							"productHeightUnit" => $key['productHeightUnit'],
							"ProductWeightUnit" => $key['ProductWeightUnit'],
							"ProductLengthUnit" => $key['ProductLengthUnit'],
							"productCategory" => $key['productCategory'],
							"productCategoryId" => $key['productCategoryId'],
							"travelMode" => $key['travelMode'],
							"InsuranceStatus" => $key['InsuranceStatus'],
							"productQty" => $key['productQty'],
							"ProductImage" => $key['ProductImage'],
							//"OtherImage"=>$key['OtherImage'],
							"QuantityStatus" => $key['QuantityStatus'],
							"BoxQuantity" => @$key['BoxQuantity'],
							"Description" => $key['Description'],
							"PackageMaterial" => $key['PackageMaterial'],
							"PackageMaterialShipped" => $key['PackageMaterialShipped'],
						];
					}
				}
				$data['delivery_data'] = $array;
				return view('User::send_package.edit.edit_item', $data);
			} else {
				return view('User::send_package.edit_send_package_items', $data);
			}

		} else {
			return Redirect::back();
		}
	}

	public function edit_calculation() {
		$response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		if (Input::get('request_id') != '') {
			$request_data = Deliveryrequest::where(['_id' => trim(Input::get('request_id')), 'RequesterId' => new MongoId(Session::get('UserId'))])
				->select('ProductList')
				->first();

			if (count($request_data) == 0 && !isset($request_data->ProductList)) {
				return json_encode($response); die;
			}

			$rate = 0;
			$insurance = 0;
			$weight = 0;
			$totalWeight = 0;
			$volume = 0;
			$distance = $this->get_distance(Input::get('calculated_distance'));
			$drop_lat_long = ['lat' => Input::get('DeliveryLat'), 'lng' => Input::get('DeliveryLong')];
			$distance_cal = Utility::GetDrivingDistance(Input::get('PickupLat'), Input::get('PickupLong'), Input::get('DeliveryLat'), Input::get('DeliveryLong'));

			if ($distance_cal == false) {
				$response['msg'] = 'Oops! We are unable to calculate distance.';
				return json_encode($response);die;
			} else {
				$distance = $this->get_distance($distance_cal->distance);
				$distance2 = $distance_cal->distance;
			}

			$drop_off_state = json_decode(Input::get('state20'));
			$response = ['success' => 0, 'shipping_cost' => '', 'shipping_cost' => '', 'ghana_total_amount' => '', 'formated_currency' => '', 'total_weight' => 0];
			$return_array = $request_data->ProductList;
			$inputData['consolidate_check'] = Input::get('consolidate_check');
			$res['air_rate'] = 0;
			$set_air_category = $this->set_air_category($request_data->ProductList,$inputData);
			$total_productcost = 0;
			$ups_rate = 0;
			$warehouse_transit_fee = 0;
			$is_ups_rate_applied = false;
			$total_ups_rate = 0;
			$total_custom_duty_rate = 0;
			$is_customs_duty_applied = false;
			$pickup_country = json_decode(Input::get("country"));	// Pickup.
			$drop_off_country = json_decode(Input::get("drop_off_country"));	// Drop off.
			$destination_postal_code = Input::get("drop_off_zipcode");
			$pickup_postal_code = Input::get("zipcode");
			$pickup_state = json_decode(Input::get("state10"));
			$pickup_city = json_decode(Input::get("city"));
			$drop_off_state = json_decode(Input::get("state20"));

			foreach ($request_data->ProductList as $key => $val) {
				// Calculate ups rate of each item.
				$is_macth = false;
				$val['productUpsRate'] = 0;
				$last_price = 0;

				if ($pickup_country->name != $drop_off_country->name) {
					$is_customs_duty_applied = true;
					$weight = $val["productWeight"];

					if ($val["ProductWeightUnit"] == "kg") {
						$weight = $this->kgToLb($weight);
					}

					if ($pickup_country->name == "USA") {
						$params = array(
							"width" => $val["productWidth"],
							"height" => $val["productHeight"],
							"length" => $val["productLength"],
							"weight" => $weight,
							"pickupPostalCode" => $pickup_postal_code,
							"pickupStateProvinceCode" => $pickup_state->short_name,
							"pickupCity" => $pickup_city->name,
							"pickupCountryCode" => "US",
							"destinationPostalCode" => "19977",
							"service"=> "03"
						);

						$ups = new upsRate();
						$ups->setCredentials();
						$ups_rate = $ups->getRate($params);
						$val["productUpsRate"] = $ups_rate;
						$total_ups_rate += $ups_rate;
						$is_ups_rate_applied = true;
					} else if ($drop_off_country->name == "USA") {	// destination country.
						if ($val['productCost'] <= 2000) {
							$is_customs_duty_applied = false;
						}

						$params = array(
							"width" => $val["productWidth"],
							"height" => $val["productHeight"],
							"length" => $val["productLength"],
							"weight" => $weight,
							"pickupPostalCode" => "19977",
							"pickupStateProvinceCode" => "DE",
							"pickupCity" => "Smyrna",
							"pickupCountryCode" => "US",
							"destinationPostalCode" => $destination_postal_code,
							"service"=> "03"
						);

						$ups = new upsRate();
						$ups->setCredentials();
						$ups_rate = $ups->getRate($params);
						$val["productUpsRate"] = $ups_rate;
						$total_ups_rate += $ups_rate;
						$is_ups_rate_applied = true;
					}
				}

				// UPS charge also applied in USA if state is diffrent
        		if ($pickup_country->name == "USA" && $drop_off_country->name == "USA") {
					if($pickup_state->name != $drop_off_state->name){
						$weight = $val["productWeight"];

						if ($val["ProductWeightUnit"] == "kg") {
							$weight = $this->kgToLb($weight);
						}

						$params = array(
							"width" => $val["productWidth"],
							"height" => $val["productHeight"],
							"length" => $val["productLength"],
							"weight" => $weight,
							"pickupPostalCode" => "19977",
							"pickupStateProvinceCode" => "DE",
							"pickupCity" => "Smyrna",
							"pickupCountryCode" => "US",
							"destinationPostalCode" => $destination_postal_code,
							"service"=> "03"
						);

						$ups = new upsRate();
						$ups->setCredentials();
						$ups_rate = $ups->getRate($params);
						$val["productUpsRate"] = $ups_rate;
						$total_ups_rate += $ups_rate;
						$is_ups_rate_applied = true;
					}
				}

				$return_array[$key]['shippingCost'] = 0;
				$return_array[$key]['InsuranceCost'] = 0;
				$total_productcost += ($val['productCost'] * $val['productQty']);
				//$weight2 = $this->get_weight($val['productWeight'], $val['ProductWeightUnit']);
				$weight2 = $this->GetVolumetricWeightInLbs($val['productLength'],$val['productHeight'],$val['productWidth'],$val['productWeight'],$val['ProductWeightUnit']);
				// $weight += $weight2;
				$totalWeight += $weight;

				if ($val['InsuranceStatus'] == 'yes') {
					$return_array[$key]['InsuranceCost'] = $this->get_insurance($val['productCost'], $val['productQty']);
					$insurance += $return_array[$key]['InsuranceCost'];
				}

				$volume += $this->get_volume(array(
					"width" => $val['productWidth'],
					"widthunit" => $val['ProductLengthUnit'],
					"height" => $val['productHeight'],
					"heightunit" => $val['productHeightUnit'],
					"length" => $val['productLength'],
					"lengthunit" => $val['ProductLengthUnit'],
				));
				
				// match with newRequesthelper
				$category = Category::where(['_id' => $val['productCategoryId'],'Status' => 'Active'])->first();
				
				if (count($category) == 0) {
					$response['msg'] = 'Oops! category not found';
					return json_encode($response);die;
				}

				// Get custom duty.
				if (isset($category->custom_duty)) {
					$total_custom_duty_rate +=  (($val['productCost'] * (int) $val['productQty']) * $category->custom_duty)/100;
					$val['customDuty'] = (($val['productCost'] * (int) $val['productQty']) * $category->custom_duty)/100;
				}

				if ($category->ChargeType == 'distance') {
					$categoryConstantArray = array(AUTO_FULLSIZE_SUV, AUTO_INTERMEDIATE_SUV, AUTO_SEDAN_ECONOMY, AUTO_SEDAN_FULLSIZE, AUTO_SEDAN_INTERMEDIATE, AUTO_STANDARD_SUV, AUTOMOBILE_VAN);

					if ($val['travelMode'] == 'ship' && in_array($val['productCategoryId'], $categoryConstantArray)) {
						foreach ($category->Shipping as $key2) {
							if ($distance >= $key2['MinDistance'] && $distance <= $key2['MaxDistance']) {
								if ($inputData['consolidate_check'] == 'on') {
									$return_array[$key]['shippingCost'] = $key2['CONSOLIDATE_RATE'] * (int) $val['productQty'];
									$rate += $key2['CONSOLIDATE_RATE'] * (int) $val['productQty'];
								} else {
									$return_array[$key]['shippingCost'] = $key2['Rate'] * (int) $val['productQty'];
									$rate += $key2['Rate'] * (int) $val['productQty'];
								}
							}		
						}
					} else {
						$cat_count = $this->category_count($request_data->ProductList, $val['productCategoryId']);

						foreach ($category->Shipping as $key2) {
							if ($distance >= $key2['MinDistance'] && $distance <= $key2['MaxDistance']) {
								if ($inputData['consolidate_check'] == 'on') {
									$return_array[$key]['shippingCost'] = $key2['CONSOLIDATE_RATE'] / $cat_count['count'];
									$rate += $key2['CONSOLIDATE_RATE'] / $cat_count['count'];
								} else {
									$return_array[$key]['shippingCost'] = $key2['Rate'] / $cat_count['count'];
									$rate += $key2['Rate'] / $cat_count['count'];
								}
							}		
						}
					}
				} else if ($val['travelMode'] == 'air' && ($val['productCategoryId'] != OtherCategory && $val['productCategoryId'] != ElectronicsCategory)) {
					$air_rate = $this->get_category_price($set_air_category,$val['productCategoryId']);
					$cat_count = $this->category_count($request_data->ProductList,$val['productCategoryId']);
					$return_array[$key]['shippingCost'] = $air_rate / $cat_count['count'];
					$rate += $air_rate / $cat_count['count'];
				} else if ($val['travelMode'] == 'air' && ($val['productCategoryId'] == OtherCategory || $val['productCategoryId'] != ElectronicsCategory)) {
					foreach ($category->Shipping as $key2) {
						if ($distance >= $key2['MinDistance'] && $distance <= $key2['MaxDistance']) {
							$is_macth = true;
							$return_array[$key]['shippingCost'] = $key2['Rate'] * (int) $val['productQty'];
							$rate += $key2['Rate'] * (int) $val['productQty'];
						}

						$last_price = $key2['Rate'];		
					}

					if (!$is_macth && ($val['productCategoryId'] == OtherCategory || $val['productCategoryId'] == ElectronicsCategory)) {
						$itemWeight_1 = $weight2-2;
						$itemWeight_2 = intval($itemWeight_1/2);
						$rate_1 =  $itemWeight_2 * $category->weight_increase_by;
						$rate_2 = $rate_1 + $last_price;

						if ($val['productCost'] > 1) {
							$p = $val['productCost'] - 1;
							$return_array[$key]['shippingCost'] = $rate_2 + ($p * $category->price_increase_by) * (int) $val['productQty'];
							$rate += $rate_2 + ($p * $category->price_increase_by) * (int) $val['productQty'];
						} else {
							$rate += $rate_2 * (int) $val['productQty'];
							$return_array[$key]['shippingCost'] = $rate_2 * (int) $val['productQty'];
						}
					}
				} else {
					if (isset($category->Shipping) && is_array($category->Shipping)) {
						foreach ($category->Shipping as $key2) {
							if ($distance >= $key2['MinDistance'] && $distance <= $key2['MaxDistance']) {
								$return_array[$key]['shippingCost'] = $key2['Rate'] * (int) $val['productQty'];
								$rate += $key2['Rate'] * (int) $val['productQty'];
							}
						}
					}
				}
			}

			$totalWeightInKg = 0;
			$totalWeightInLbs = 0;
			$totalVolumeInKg = 0;
			$totalVolumeInLbs = 0;
			$totalItemCost = 0;

			foreach ($return_array as $value) {
				$WHLUnit = "";
				if ($value["productHeightUnit"] == "cm") {
					$WHLUnit = "cm";
				}

				if ($value["productHeightUnit"] == "inches") {
					$WHLUnit = "inches";
				}

				$totalWeightInKg += $this->get_weight_kg($value["productWeight"], $value["ProductWeightUnit"]);
				$totalWeightInLbs += $this->get_weight_lbs($value["productWeight"], $value["ProductWeightUnit"]);

				$totalVolumeInKg += $this->volumetricWeightInKg(array(
					"width" => $value["productWidth"],
					"WHLUnit" => $WHLUnit,
					"height" => $value["productHeight"],
					"WHLUnit" => $WHLUnit,
					"length" => $value["productLength"],
					"WHLUnit" => $WHLUnit
				));

				$totalVolumeInLbs += $this->volumetricWeightInLbs(array(
					"width" => $value["productWidth"],
					"WHLUnit" => $WHLUnit,
					"height" => $value["productHeight"],
					"WHLUnit" => $WHLUnit,
					"length" => $value["productLength"],
					"WHLUnit" => $WHLUnit
				));

				$totalItemCost += $value["productCost"];
			}

			$response["total_item_cost"] = $totalItemCost;
			$response["totalWeightInKg"] = $totalWeightInKg;
			$response["totalWeightInLbs"] = $totalWeightInLbs;
			$response["totalVolumeInKg"] = number_format($totalVolumeInKg, 2);
			$response["totalVolumeInLbs"] = number_format($totalVolumeInLbs, 2);
			$response['success'] = 1;
			$DutyAndCustom = 0;

			if ($is_customs_duty_applied) {
				$DutyAndCustom = $total_custom_duty_rate;
			}

			$Tax = $this->get_tax($total_productcost);

			if ($is_ups_rate_applied) {
				$warehouse_transit_fee = $this->get_warehouse_transit_fee();
			}

			$response['shipping_cost'] = $rate + $total_ups_rate + $warehouse_transit_fee;
			$config = Configuration::find(CongigId);
            if ($drop_off_country->name === 'Kenya') {
				$DutyAndCustom = 0;
                $extra = 0;
                if ($total_productcost > 100) {
                    $extra = ($total_productcost * $config->kenya_per_item_value)/100;
                    $extra = number_format($extra, 2, '.', '');
                }
                // if ($weightunit === 'kg') {
                //     $ratePerKg = number_format(($weight * $config->kenya_rate_per_kilogram), 2, '.', '');
                // } else {
                    $ratePerKg = number_format(($totalWeightInLbs * 0.453592 * $config->kenya_rate_per_kilogram), 2, '.', '');
                // }
                $ratePerKg = $ratePerKg < $config->kenya_rate_per_kilogram ? $config->kenya_rate_per_kilogram : $ratePerKg;
                $response['shipping_cost'] = $ratePerKg;
                // echo $ratePerKg;
                $response['total_amount'] = $ratePerKg + $extra;
                
                if ($response['total_amount'] < $config->kenya_rate_per_kilogram) {
                    $response['total_amount'] = $config->kenya_rate_per_kilogram;
                }
            } else {
				$response['total_amount'] = $response['shipping_cost'] + $insurance + $DutyAndCustom + $Tax;
            }
		
			$response['totalUpsCharge'] = $total_ups_rate;	// totalUpsRate
			$response['warehouse_transit_fee'] = $warehouse_transit_fee;
			$response['DutyAndCustom'] = $DutyAndCustom;
			$response['Tax'] = $Tax;
			$response['insurance'] = $insurance;
			$response['product'] = $return_array;
			$response['AquantuoFees'] = $this->get_aquantuo_fees($response['shipping_cost']);

			//extraa charge of out side accra
			$response['regionCharges'] = $this->resionCharges(['name' => $drop_off_state->name, 'id' => $drop_off_state->id]);
			$response['total_amount'] = $response['total_amount'] + $response['regionCharges'];
			//end accra charge

			$currency_conversion = $this->currency_conversion($response['total_amount']);

			if (Session()->get('Default_Currency') == 'GHS') {
				$response['user_currency'] = $currency_conversion['ghanian_cost'];
			} else if (Session()->get('Default_Currency') == 'CAD') {
				$response['user_currency'] = $currency_conversion['canadian_cost'];
			} else if (Session()->get('Default_Currency') == 'PHP') {
				$response['user_currency'] = $currency_conversion['philipins_cost'];
			} else if (Session()->get('Default_Currency') == 'GBP') {
				$response['user_currency'] = $currency_conversion['uk_cost'];
			} else if (Session()->get('Default_Currency') == 'KES') {
				$response['user_currency'] = $currency_conversion['kenya_cost'];
			} else {
				$response['user_currency'] = $currency_conversion['ghanian_cost'];
			}

			// Ftech currency Rate 
			$currencyInfo = Currency::where(['Status' => 'Active'])->select('CurrencyRate', 'FormatedText', 'CurrencyCode')->get();
			$response['USDRate'] = 1;
			$response['GBPRate'] = $response['GHSRate'] = $response['CADRate'] = $response['KESRate'] = 0;
			foreach($currencyInfo as $currencyVal) {
				if ($currencyVal->CurrencyCode == 'CAD') {
					$response['CADRate'] = $currencyVal->CurrencyRate;
				}

				if ($currencyVal->CurrencyCode == 'GHS') {
					$response['GHSRate'] = $currencyVal->CurrencyRate;
				}

				if ($currencyVal->CurrencyCode == 'GBP') {
					$response['GBPRate'] = $currencyVal->CurrencyRate;
				}

				if ($currencyVal->CurrencyCode == 'KES') {
					$response['KESRate'] = $currencyVal->CurrencyRate;
				}
			}

			$response['total_item'] = count($request_data->ProductList);
			$response['distance'] = number_format($distance,2);
			$response['volume'] = number_format($volume,2);
			$response['total_weight'] = number_format($totalWeight,2);
		}
		return json_encode($response);
	}

	public function setInitialValue($array){
		$return_array = [ 'weight' => 0];
		foreach ($array as $key){
			//$weight = $this->get_weight($key->productWeight, $key->productWeightUnit);
			$weight = $this->GetVolumetricWeightInLbs($key->productLength,$key->productHeight,$key->productWidth,$key->productWeight,$key->ProductWeightUnit);
		}
		$return_array['weight'] = $weight;
	}

	public function category_count($array,$cat_id){
		$res['count'] = 0;
		$res['weight'] = 0;
		foreach ($array as $key => $value) {
			if($cat_id == $value['productCategoryId']){
				$res['count'] += 1;
				//$res['weight'] += $this->get_weight(($value['productWeight'] * (int) $value['productQty']),$value['ProductWeightUnit']) ;
				//~ echo $key->productLength;
				//~ echo "<br/>";
				//~ echo $key->productHeight;
				//~ echo "<br/>";
				//~ echo $key->productWidth;
				//~ echo "<br/>";
				//~ echo $key->productWeight;
				//~ echo "<br/>";
				//~ echo $key->ProductWeightUnit;
				//~ echo "<br/>";
				//~ die;
				$res['weight'] += $this->GetVolumetricWeightInLbs($value['productLength'],$value['productHeight'],$value['productWidth'],$value['productWeight'],$value['ProductWeightUnit']);
			}
		}
		//print_r($res); die;
		return $res;
	}

	public function set_air_category($array, $inputData){
		$already_calculated_category = [];
		$category_set = [];
		
		foreach ($array as $key => $val) {
			if ($val['travelMode'] == 'air') {
				if(!in_array($val['productCategoryId'], $already_calculated_category)){
					$cat_weight = $this->category_count($array,$val['productCategoryId']);
					$weight = $cat_weight['weight'];
					$already_calculated_category[]=$val['productCategoryId'];
					$data = Category::where(['_id' => $val['productCategoryId']])->where(['Status' => 'Active', 'TravelMode' => 'air', 'ChargeType' => 'fixed'])
							->select('Shipping','price_increase_by')
							->first();
					if (count($data) > 0) {
						$rate = 0;
						// ajay code here
						$is_other_category = 'no';
						// if category is other then new code works
						if ($data["_id"] == OtherCategory) {
							foreach ($data['Shipping'] as $key1) {
								if ($key1['MinDistance'] <= floatval($weight) && $key1['MaxDistance'] >= floatval($weight)) {
									$rate = $key1['Rate'];
									
									if($val['productCost']  > 1){
											$p = $val['productCost'] - 1;
											$rate = $rate + ($p * $data['price_increase_by']);
									}
									
								}
							}
							
						}else{
							foreach ($data['Shipping'] as $key1) {
								if ($key1['MinDistance'] <= floatval($weight) && $key1['MaxDistance'] >= floatval($weight)) {
									if($inputData['consolidate_check'] == 'on'){
										$rate = @$key1['CONSOLIDATE_RATE'];
									}else{
										$rate = @$key1['Rate'];
									}
								}
							}
						}

						if($rate > 0){
							$category_set[]=[
								'cat_id'=>$val['productCategoryId'],
								'rate'=> $rate,
							];
						}

					}
				}
			}
		}

		//echo '<pre>';
		//print_r($category_set); die;

		return $category_set;
	}

	public function get_category_price($category_set,$id){
		$price = 0;
		foreach ($category_set as $key) {
			if($key['cat_id'] == $id){
				$price = $key['rate'];
			}
		}
		return $price;
	}

	public function kgToLb ($val) {
        return $val * 2.20462;
    }

    public function get_weight_kg($weight, $type) {
		$type = strtolower($type);
		if ($type == "kg") {
			return $weight;
		} else if ($type == "lbs") {
			return (number_format( $weight / 2.2046, 2) );
		} else {
			return $weight;
		}
	}

	public function get_weight_lbs($weight, $type) {
		$type = strtolower($type);
		if ($type == "kg") {
			return (number_format(((float) $weight) * 2.20462, 2));
		} else if ($type == "lbs") {
			return $weight;
		} else {
			return $weight;
		}
	}

	public function volumetricWeightInKg($array) {
		return ($this->get_size_in_cm($array['width'], $array['WHLUnit']) * $this->get_size_in_cm($array['height'], $array['WHLUnit']) * $this->get_size_in_cm($array['length'], $array['WHLUnit']));
	}

	public function volumetricWeightInLbs($array) {
		return ($this->getSizeInInches($array['width'], $array['WHLUnit']) * $this->getSizeInInches($array['height'], $array['WHLUnit']) * $this->getSizeInInches($array['length'], $array['WHLUnit']));
	}

	public function getSizeInInches($whl, $unit) {
		$in_inches = (float) $whl;
		$unit = strtolower($unit);
		if ($unit == "cm") {
			$in_inches = $in_inches / 2.54;
		}
		return $in_inches;
	}

	public function prepare_request_calculation() {
		$items = Additem::where(['user_id' => Session::get('UserId'), 'type' => 'delivery'])->get();
		$rate = 0;
		$insurance = 0;
		$weight = 0;
		$totalWeight = 0;
		$volume = 0;
		$distance = $this->get_distance(Input::get('calculated_distance'));
		$drop_lat_long = ['lat' => Input::get('DeliveryLat'), 'lng' => Input::get('DeliveryLong')];
		$distance_cal = Utility::GetDrivingDistance(Input::get('PickupLat'), Input::get('PickupLong'), Input::get('DeliveryLat'), Input::get('DeliveryLong'));

		if ($distance_cal == false) {
			$response['msg'] = 'Oops! We are unable to calculate distance.';
			return json_encode($response);die;
		} else {
			$distance = $this->get_distance($distance_cal->distance);
			$distance2 = $distance_cal->distance;
		}
		
		$drop_off_state = json_decode(Input::get('state20'));
		$response = ['success' => 0, 'shipping_cost' => '', 'shipping_cost' => '', 'ghana_total_amount' => '', 'formated_currency' => '', 'total_weight' => 0];
		$return_array = [];
		$inputData['consolidate_check'] = Input::GET('consolidate_check');
		$res['air_rate'] = 0;
		$total_productcost = 0;

		if ($items) {
			$set_air_category = $this->set_air_category($items, $inputData);
			$ups_rate = 0;
			$warehouse_transit_fee = 0;
			$is_ups_rate_applied = false;
			$total_ups_rate = 0;
			$total_custom_duty_rate = 0;
			$is_customs_duty_applied =false;
			$pickup_country = json_decode(Input::get("country"));	// Pickup.
			$drop_off_country = json_decode(Input::get("drop_off_country"));	// Drop off.
			$destination_postal_code = Input::get("drop_off_zipcode");
			$pickup_postal_code = Input::get("zipcode");
			$pickup_state = json_decode(Input::get("state10"));
			$drop_off_state = json_decode(Input::get("state20"));
			$pickup_city = json_decode(Input::get("city"));
			$msg_str = '';
			
			foreach ($items as $key) {
				// Calculate ups rate of each item.
				$last_price = 0;// is used for only other categry in air mode
				$is_macth = false;
				$key->productUpsRate = 0;

				// Get Volumetric Weight of product.
				$proVMWInLbs = $this->GetVolumetricWeightInLbs($key->productLength, $key->productHeight, $key->productWidth, $key->productWeight, $key->ProductWeightUnit);
				$proWInLbs = $this->get_weight_lbs($key->productWeight, $key->ProductWeightUnit);
				
				if ($proVMWInLbs > $proWInLbs) {
					$proWInLbs = $proVMWInLbs;
				}

				if ($pickup_country->name != $drop_off_country->name) {
					$is_customs_duty_applied  = true;
					$weight = $key->productWeight;

					if ($key->ProductWeightUnit == "kg") {
						$weight = $this->kgToLb($weight);
					}

					if ($pickup_country->name == "USA") {
						$params = array(
							"width" => $key->productWidth,
							"height" => $key->productHeight,
							"length" => $key->productLength,
							"weight" => $proWInLbs,
							"pickupPostalCode" => $pickup_postal_code,
							"pickupStateProvinceCode" => $pickup_state->short_name,
							"pickupCity" => $pickup_city->name,
							"pickupCountryCode" => "US",
							"destinationPostalCode" => "19977",
							"service"=> "03"
						);

						$ups = new upsRate();
						$ups->setCredentials();
						$ups_rate = $ups->getRate($params);
						$key->productUpsRate = $ups_rate;
						$total_ups_rate += $ups_rate;
						$is_ups_rate_applied = true;
					} else if ($drop_off_country->name == "USA") {	// destination country.
						if ($key->productCost <= 2000) {
							$is_customs_duty_applied = false;
						}

						$params = array(
							"width" => $key->productWidth,
							"height" => $key->productHeight,
							"length" => $key->productLength,
							"weight" => $proWInLbs,
							"pickupPostalCode" => "19977",
							"pickupStateProvinceCode" => "DE",
							"pickupCity" => "Smyrna",
							"pickupCountryCode" => "US",
							"destinationPostalCode" => $destination_postal_code,
							"service"=> "03"
						);

						$ups = new upsRate();
						$ups->setCredentials();
						$ups_rate = $ups->getRate($params);
						$key->productUpsRate = $ups_rate;
						$total_ups_rate += $ups_rate;
						$is_ups_rate_applied = true;
					}
				}

				// UPS charge also applied in USA if state is diffrent
        		if ($pickup_country->name == "USA" && $drop_off_country->name == "USA") {
					if ($pickup_state->name != $drop_off_state->name) {
						$weight = $key->productWeight;

						if ($key->ProductWeightUnit == "kg") {
							$weight = $this->kgToLb($weight);
						}

						$params = array(
							"width" => $key->productWidth,
							"height" => $key->productHeight,
							"length" => $key->productLength,
							"weight" => $proWInLbs,
							"pickupPostalCode" => "19977",
							"pickupStateProvinceCode" => "DE",
							"pickupCity" => "Smyrna",
							"pickupCountryCode" => "US",
							"destinationPostalCode" => $destination_postal_code,
							"service"=> "03"
						);

						$ups = new upsRate();
						$ups->setCredentials();
						$ups_rate = $ups->getRate($params);
						$key->productUpsRate = $ups_rate;
						$total_ups_rate += $ups_rate;
						$is_ups_rate_applied = true;
					}
				}
				
				// If weight or volumetric weight will exceed 160lbs, please show the following message.
				if ($proWInLbs > 160 && $is_ups_rate_applied) {
					$msg_str = 'Aquantuo will review and provide you with an updated cost based on the weight/dimensions provided.';
				}

				$key->InsuranceCost = 0;
				$key->shippingCost = 0;
				$key->customDuty = 0;
				$total_productcost += ($key->productCost * $key->productQty);
				$weight2 = $this->get_weight($key->productWeight, $key->productWeightUnit);
				$totalWeight += $weight;

				if ($key->InsuranceStatus == 'yes') {
					$key->InsuranceCost = $this->get_insurance($key->productCost, $key->productQty);
					$insurance += $key->InsuranceCost;
				}
				
				$volume += $this->get_volume(array(
					"width" => $key->productWidth,
					"widthunit" => $key->ProductLengthUnit,
					"height" => $key->productHeight,
					"heightunit" => $key->productHeightUnit,
					"length" => $key->productLength,
					"lengthunit" => $key->ProductLengthUnit,
				));

				// match with newRequesthelper
				$category = Category::where(['_id' => $key->productCategoryId,'Status' => 'Active'])->first();
				
				if (count($category) == 0) {
					$response['msg'] = 'Oops! category not found';
					return json_encode($response);die;
				}

				// Get custom duty.
				if (isset($category->custom_duty)) {
					$total_custom_duty_rate += (($key->productCost * (int) $key->productQty) * $category->custom_duty)/100;
					$key->customDuty = (($key->productCost * (int) $key->productQty) * $category->custom_duty)/100;
				}

				if ($category->ChargeType == 'distance') {
					$categoryConstantArray = array(AUTO_FULLSIZE_SUV, AUTO_INTERMEDIATE_SUV, AUTO_SEDAN_ECONOMY, AUTO_SEDAN_FULLSIZE, AUTO_SEDAN_INTERMEDIATE, AUTO_STANDARD_SUV, AUTOMOBILE_VAN);

					if ($key->travelMode == 'ship' && in_array($key->productCategoryId, $categoryConstantArray)) {
						foreach ($category->Shipping as $key2) {
							if ($distance >= $key2['MinDistance'] && $distance <= $key2['MaxDistance']) {
								if ($inputData['consolidate_check'] == "on") {
									$key->shippingCost = $key2['CONSOLIDATE_RATE'] * (int) $key->productQty;
									$rate += $key2['CONSOLIDATE_RATE'] * (int) $key->productQty;
								} else {
									$key->shippingCost = $key2['Rate'] * (int) $key->productQty;
									$rate += $key2['Rate'] * (int) $key->productQty;
								}
							}		
						}
					} else {
						$cat_count = $this->category_count($items, $key->productCategoryId);
						
						foreach ($category->Shipping as $key2) {
							if ($distance >= $key2['MinDistance'] && $distance <= $key2['MaxDistance']) {
								if ($inputData['consolidate_check'] == "on") {
									$key->shippingCost = $key2['CONSOLIDATE_RATE'] / $cat_count['count'];
									$rate += $key2['CONSOLIDATE_RATE'] / $cat_count['count'];
								} else {
									$key->shippingCost = $key2['Rate'] / $cat_count['count'];
									$rate += $key2['Rate'] / $cat_count['count'];
								}
							}		
						}
					}
				} else if ($key->travelMode == 'air' && ($key->productCategoryId != OtherCategory && $key->productCategoryId != ElectronicsCategory)) {
					$air_rate = $this->get_category_price($set_air_category, $key->productCategoryId);
					$cat_count = $this->category_count($items, $key->productCategoryId);
					$key->shippingCost = $air_rate / $cat_count['count'];
					$res['air_rate'] += $air_rate / $cat_count['count'];
				} else if ($key->travelMode == 'air' && ($key->productCategoryId == OtherCategory || $key->productCategoryId != ElectronicsCategory)) {
					foreach ($category->Shipping as $key2) {
						if ($distance >= $key2['MinDistance'] && $distance <= $key2['MaxDistance']) {
							$is_macth = true;
							$key->shippingCost = $key2['Rate'] * (int) $key->productQty;
							$rate += $key2['Rate'] * (int) $key->productQty;
							$res['air_rate'] = $rate;
						}
						$last_price = $key2['Rate'];		
					}

					if (!$is_macth && ($key->productCategoryId == OtherCategory || $key->productCategoryId == ElectronicsCategory)) {
						$itemWeight_1 = $weight2-2;
						$itemWeight_2 = intval($itemWeight_1/2);
						
						$rate_1 =  $itemWeight_2 * $category->weight_increase_by;
						$rate_2 = $rate_1 + $last_price;
						if ($key->productCost  > 1) {
							$p = $key->productCost - 1;
							$key->shippingCost = $rate_2 + ($p * $category->price_increase_by) * (int) $key->productQty;
							$rate += $rate_2 + ($p * $category->price_increase_by) * (int) $key->productQty;
							$res['air_rate'] = $rate ;
						} else {
							$rate += $rate_2 * (int) $key->productQty;
							$key->shippingCost = $rate_2 * (int) $key->productQty;
							$res['air_rate'] = $rate;
						}
					}
				} else {
					if (isset($category->Shipping) && is_array($category->Shipping)) {
						foreach ($category->Shipping as $key2) {
							if ($distance >= $key2['MinDistance'] && $distance <= $key2['MaxDistance']) {
								$key->shippingCost = $key2['Rate'] * (int) $key->productQty;
								$rate += $key2['Rate'] * (int) $key->productQty;
							}
						}
					}
				}
			}
		}

		$totalWeightInKg = 0;
		$totalWeightInLbs = 0;
		$totalVolumeInKg = 0;
		$totalVolumeInLbs = 0;
		$totalItemCost = 0;

		foreach ($items as $value) {
			$WHLUnit = "";
			if ($value->productHeightUnit == "cm") {
				$WHLUnit = "cm";
			}

			if ($value->productHeightUnit == "inches") {
				$WHLUnit = "inches";
			}

			$totalWeightInKg += $this->get_weight_kg($value->productWeight, $value->ProductWeightUnit);
			$totalWeightInLbs += $this->get_weight_lbs($value->productWeight, $value->ProductWeightUnit);

			$totalVolumeInKg += $this->volumetricWeightInKg(array(
				"width" => $value->productWidth,
				"WHLUnit" => $WHLUnit,
				"height" => $value->productHeight,
				"WHLUnit" => $WHLUnit,
				"length" => $value->productLength,
				"WHLUnit" => $WHLUnit
			));

			$totalVolumeInLbs += $this->volumetricWeightInLbs(array(
				"width" => $value->productWidth,
				"WHLUnit" => $WHLUnit,
				"height" => $value->productHeight,
				"WHLUnit" => $WHLUnit,
				"length" => $value->productLength,
				"WHLUnit" => $WHLUnit
			));

			$totalItemCost += $value->productCost;
		}

		$response["totalWeightInKg"] = $totalWeightInKg;
		$response["totalWeightInLbs"] = $totalWeightInLbs;
		$response["totalVolumeInKg"] = number_format($totalVolumeInKg, 2);
		$response["totalVolumeInLbs"] = number_format($totalVolumeInLbs, 2);

		$return_array = $items;
		$response['success'] = 1;
		// custome duty and tax 
		$DutyAndCustom = 0;
		if ($is_customs_duty_applied) {
			$DutyAndCustom = $total_custom_duty_rate;
		}

		$Tax = $this->get_tax($total_productcost);
		
		if ($is_ups_rate_applied) {
			$warehouse_transit_fee = $this->get_warehouse_transit_fee();
		}
		
		$response['msg_str'] = $msg_str;
		$response['DutyAndCustom'] = $DutyAndCustom;
		$response['Tax'] = $Tax;
		$response["totalUpsCharge"] = $total_ups_rate;
		$response['regionCharges'] = 0;
		$response['warehouse_transit_fee'] = $warehouse_transit_fee;
		$response['ups_rate'] = $total_ups_rate;
		$response['shipping_cost'] = $rate + $total_ups_rate + $warehouse_transit_fee;
		$response['insurance'] = $insurance;
		$response['total_amount'] = $response['shipping_cost'] + $insurance + $DutyAndCustom + $Tax;
		$response['product'] = $return_array;
		$response['total_item_cost'] = $totalItemCost;
		$response['AquantuoFees'] = $this->get_aquantuo_fees($response['shipping_cost']);
		//extraa charge of out side accra
		$response['regionCharges'] = $this->resionCharges(['name' => $drop_off_state->name, 'id' => $drop_off_state->id]);
		$response['total_amount'] = $response['total_amount'] + $response['regionCharges'];
		//end accra charge

		$currency_conversion = $this->currency_conversion($response['total_amount']);

		if (Session()->get('Default_Currency') == 'GHS') {
			$response['user_currency'] = $currency_conversion['ghanian_cost'];
		} else if (Session()->get('Default_Currency') == 'CAD') {
			$response['user_currency'] = $currency_conversion['canadian_cost'];
		} else if (Session()->get('Default_Currency') == 'PHP') {
			$response['user_currency'] = $currency_conversion['philipins_cost'];
		} else if (Session()->get('Default_Currency') == 'GBP') {
			$response['user_currency'] = $currency_conversion['uk_cost'];
		} else if (Session()->get('Default_Currency') == 'KES') {
			$response['user_currency'] = $currency_conversion['kenya_cost'];
		} else {
			$response['user_currency'] = $currency_conversion['ghanian_cost'];
		}
		// Ftech currency Rate 
		$currencyInfo = Currency::where(['Status' => 'Active'])->select('CurrencyRate', 'FormatedText', 'CurrencyCode')->get();
		$response['USDRate'] = 1;
		$response['GBPRate'] = $response['GHSRate'] = $response['CADRate'] = 0;
		foreach($currencyInfo as $currencyVal){
			if($currencyVal->CurrencyCode=='CAD'){
				$response['CADRate'] = $currencyVal->CurrencyRate;
			}
			if($currencyVal->CurrencyCode=='GHS'){
				$response['GHSRate'] = $currencyVal->CurrencyRate;
			}
			if($currencyVal->CurrencyCode=='GBP'){
				$response['GBPRate'] = $currencyVal->CurrencyRate;
			}
			if($currencyVal->CurrencyCode=='KES'){
				$response['KESRate'] = $currencyVal->CurrencyRate;
			}
		}

		$response['total_item'] = count($items);
		$response['distance'] = number_format($distance,2);
		$response['volume'] = number_format($volume,2);
		$response['total_weight'] = number_format($totalWeight,2);
		
		return json_encode($response);
	}

	public function get_tax($cost=0){
		$data = Configuration::where(array('_id' => '5673e33e6734c4f874685c84'))
			->select('tax')->first();
		if (count($data) > 0) {
			$Tax = $cost * $data->tax;
			return $Tax / 100;
		}
		return 0;
	}

    public function get_DutyAndCustom($cost=0){
        $DutyAndCustom = $cost * 6.20;
        return  $DutyAndCustom / 100;
    }

	public function resionCharges($state) {
		$charges = Extraregion::where(['state_id' => $state['id'], 'status' => 'Active'])->select('amount')->first();
		if ($charges) {
			return $charges->amount;
		} else {
			return 0.00;
		}
	}

	public function package_detail($id) {
		$data['request_data'] = Deliveryrequest::where(['_id' => $id])->first();
		$data['updated_data'] = [];
		if (count($data['request_data']) > 0) {
			$data['total_item'] = 0;
			if (isset($data['request_data']['ProductList'])) {
				foreach ($data['request_data']['ProductList'] as $key) {
					$data['total_item'] = $data['total_item'] + 1;
				}
			}

			$tpid = [];
			foreach ($data['request_data']->ProductList as $ProductList) {
				$tpid[] = $ProductList['tpid'];
			}
			$data['user_data'] = User::whereIn('_id', $tpid)->get(['_id', 'Image', 'RatingCount', 'RatingByCount', 'Name']);

			$data['updated_data'] = Itemhistory::where(['request_id'=>$id])->get();
		}
		return view('User::send_package.requester_package_detail', $data);
	}

	public function get_volume($array) {
		return ($this->get_size_in_cm($array['length'], $array['lengthunit']) * $this->get_size_in_cm($array['width'], $array['widthunit']) * $this->get_size_in_cm($array['height'], $array['heightunit']));
	}

	public function get_size_in_cm($height, $unit) {
		$in_cm = (float) $height;
		$unit = strtolower($unit);
		if ($unit == 'inches') {
			$in_cm = $in_cm * 2.54;
		}
		return $in_cm;
	}

	public function outsideOfAccraCharge($address) {
		$distance = Utility::getDistanceBetweenPointsNew(5.5913754, -0.2499413, $address['lat'], $address['lng']);
		$charge = false;
		if ($distance > 0) {
			$distance_km = $distance / 1000;
			$range = 30;
			if ($distance_km > 30) {
				$charge = true;
			}
		}
		return $charge;
	}

	public function currency_conversion($shipcost) {
		$data = ['cost' => 0, 'ghanian_cost' => 0, 'philipins_cost' => 0, 'uk_cost' => 0, 'canadian_cost' => 0, 'kenya_cost' => 0];
		$currency = Currency::where(['Status' => 'Active'])->select('CurrencyRate', 'FormatedText', 'CurrencyCode')->get();

		if ($currency) {
			foreach ($currency as $key) {
				if ($key->CurrencyCode == 'GHS') {
					$data['ghanian_cost'] = str_replace('[AMT]', number_format($shipcost * $key->CurrencyRate, 2), $key->FormatedText);
				} else if ($key->CurrencyCode == 'CAD') {
					$data['canadian_cost'] = str_replace('[AMT]', number_format($shipcost * $key->CurrencyRate, 2), $key->FormatedText);
				} else if ($key->CurrencyCode == 'PHP') {
					$data['philipins_cost'] = str_replace('[AMT]', number_format($shipcost * $key->CurrencyRate, 2), $key->FormatedText);
				} else if ($key->CurrencyCode == 'GBP') {
					$data['uk_cost'] = str_replace('[AMT]', number_format($shipcost * $key->CurrencyRate, 2), $key->FormatedText);
				} else if ($key->CurrencyCode == 'KES') {
					$data['kenya_cost'] = str_replace('[AMT]', number_format($shipcost * $key->CurrencyRate, 2), $key->FormatedText);
				}
			}
		}
		return $data;

	}

	public function get_distance($distance) {
		return floatval($distance) * 0.000621371;
	}
	/*
	 * TODO return weight into pounds
	 * 1 lbs = 2.20462 kg
	 * 1lbs = 0.00220462 gram
	 */
	public function get_weight($weight, $type) {
		$type = strtolower($type);
		if ($type == 'kg') {
			return (((float) $weight) * 2.20462);
		} else if ($type == 'gram') {
			return (((float) $weight) * 0.00220462);
		} else {
			return $weight;
		}
	}
	/*
	 * TODO return Volumetric Weight in lbs
	 * 1 lbs = 2.20462 kg
	 * 1lbs = 0.00220462 gram
	 *
	 */
	public function GetVolumetricWeightInLbs($length,$height,$width,$weight,$type) {
		$VolumetricInfo = ["length"=>$length,"height"=>$height,"width"=>$width,"type"=>$type];
        $VolumetricWeight =  Utility::calculate_volumetric_weight($VolumetricInfo);
       
		if($weight>$VolumetricWeight){
			$VolumetricWeight  = $weight;
		}
        $type = strtolower($type);
		if ($type == 'kg') {
			$VolumetricWeightLbs = (((float) $VolumetricWeight) * 2.20462);
		} else if ($type == 'gram') {
			$VolumetricWeightLbs = (((float) $VolumetricWeight) * 0.00220462);
		} else {
			$VolumetricWeightLbs = $VolumetricWeight;
		}
		return $VolumetricWeightLbs;
	}

	public function get_insurance($cost, $quantity) {
		$data = Configuration::where(array('_id' => '5673e33e6734c4f874685c84'))
			->select('Insurance','main_insurance')->first();

		$value = 0;

		if (count($data) > 0) {
			
			$totalPrice = $cost * (int) $quantity;
            $insurance =  $totalPrice * $data->main_insurance;
            $value =  $insurance / 100;
           
			// Old is 
			//~ if (is_array($data->Insurance)) {
				//~ foreach ($data->Insurance as $key) {
					//~ if ($key['MinPrice'] <= (float) $cost && $key['MaxPrice'] >= (float) $cost) {
						//~ $value = $key['Rate'] * (int) $quantity;
					//~ }
				//~ }
			//~ }
		}
		return $value;
	}

	public function get_warehouse_transit_fee() {
		$data = Configuration::where(array('_id' => '5673e33e6734c4f874685c84'))
			->select('warehouse_transit_fee')->first();

		$warehouse_transit_fee = 0;

		if (count($data) > 0) {
			return $data->warehouse_transit_fee;
		}
		return 0;
	}

	public function add_item_page($id = "") {
		$data['transporter_data'] = array();

		if (!Input::get('id') == '') {
			Session()->put(['session_trip' => trim(Input::get('id'))]);
			$data['trip_data'] = Trips::where(array('_id' => Input::get('id')))->first();
	        if (!count($data['trip_data']) > 0) {
	        	return redirect('near-by-transporter');
	        }
			$data['transporter_data'] = User::where(array('_id' => $data['trip_data']->TransporterId))->first();
			$data['category2'] = Category::where(['type' => 'Category', 'Status' => 'Active'])->whereIn('TravelMode', ['ship', 'air'])->whereIn('Content', $data['trip_data']->SelectCategory)->get(['TravelMode', 'Content']);
		}

		$data['country'] = CityStateCountry::where(['type' => 'Country', 'Status' => 'Active'])
			->orderBy('Content', 'Asc')
			->get(['_id', 'Content', 'state_available']);
		$data['category'] = Category::where(['type' => 'Category', 'Status' => 'Active'])->whereIn('TravelMode', ['ship', 'air'])->get(['TravelMode', 'Content']);
		$data['items_count'] = Additem::where(['user_id' => Session::get('UserId'), 'type' => 'delivery'])->count();

		$data['item'] = Item::Where(['Status' => 'Active'])->get();
		if ($id != '') {
			return view('User::send_package.edit.add_item', $data);
		} else {
			return view('User::send_package.prepare_add_item', $data);
		}
	}

	public function post_add_item() {
		$response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		$ProductImage = '';
		$otherImages = [];

		if (Input::get('request_id') != '') {
			$item = Additem::where(['_id' => Input::get('request_id')])->first();
			$ProductImage = $item->ProductImage;
			$ProductImage = $item->ProductImage;
			//$otherImages = $item->OtherImage;
		}

		if (Input::hasFile('default_image')) {
			if (Input::file('default_image')->isValid()) {
				$ext = Input::file('default_image')->getClientOriginalExtension();
				$ProductImage = time() . rand(100, 9999) . ".$ext";

				if (Input::file('default_image')->move(BASEURL_FILE . 'package/', $ProductImage)) {
					$ProductImage = "package/$ProductImage";
				} else {
					$ProductImage = '';
				}
			}
		}

		$imageArray = array('package_image2', 'package_image3', 'package_image4', 'package_image5');
		foreach ($imageArray as $key => $val) {
			if (Input::hasFile($val)) {
				if (Input::file($val)->isValid()) {
					$ext = Input::file($val)->getClientOriginalExtension();
					$img = time() . rand(100, 9999) . ".$ext";

					if (Input::file($val)->move(BASEURL_FILE . 'package/', $img)) {
						$img = "package/$img";
						if (isset($otherImages[$key])) {
							// Start remove old image
							if (!empty(trim($otherImages[$key]))) {
								if (file_exists(BASEURL_FILE . $otherImages[$key])) {
									unlink(BASEURL_FILE . $otherImages[$key]);
								}
							}
							// End remove old image
							$otherImages[$key] = $img;
						} else {
							$otherImages[] = $img;
						}
					}
				}
			}
		}

		$lhwunit = 'cm';
		$weightunit = 'kg';
		if (Input::get('measurement_unit') == 'inches_lbs') {
			$lhwunit = 'inches';
			$weightunit = 'lbs';
		}
		$category = json_decode(Input::get('category'));
		
		//user t stop to if mode wa sea and category diffrent
		$is_make_request = 'yes';
		$item_with_sea = false;
		$item_with_air = false;

		$shipItemList = Additem::where(array('user_id' => Session::get('UserId'), 'type' => 'delivery'))->select('productCategoryId', 'travelMode')->get();

		if (count($shipItemList) > 0) {
			foreach ($shipItemList as $value) {
				if ($value->productCategoryId != @$category->id && $value->travelMode == 'ship') {
					$is_make_request = 'no';
					$item_with_sea = true;
				}

				if (Input::get('travel_mode') == 'ship') {
					$item_with_sea = true;
				}

				if ($value->travelMode == 'air' || Input::get('travel_mode') == 'air') {
					$item_with_air = true;
				}
				
				if ($item_with_air && $item_with_sea) {
					$response['msg'] = "Sorry! You can’t have different shipping modes in the same listing. Please create a separate listing for either air or sea.";
					return response()->json($response);
					die();
				}
			}
		}

		if ($is_make_request == 'no') {
			$response['msg'] = "If shipping by sea, all items must be of the same sea category. Please create a new listing if you need to use a different sea category.";
			return response()->json($response);
			die();
		}
		
		//print_r($category); die;
		$add_item = [
			'product_name' => ucfirst(Input::get('title')),
			'user_id' => Session::get('UserId'),
			'type' => 'delivery',
			'productWidth' => Input::get('width'),
			'productHeight' => Input::get('height'),
			'productLength' => Input::get('length'),
			'productCost' => Input::get('package_value'),
			'productWeight' => Input::get('weight'),
			'productHeightUnit' => $lhwunit,
			'ProductWeightUnit' => $weightunit,
			'ProductLengthUnit' => $lhwunit,
			'productCategory' => @$category->name,
			'productCategoryId' => @$category->id,
			'travelMode' => Input::get('travel_mode'),
			'currency' => Input::get('Default_Currency'),
			'needInsurance' => Input::get('insurance'),
			"productQty" => (Input::get('quantity') < 1) ? 1 : Input::get('quantity'),
			'ProductImage' => $ProductImage,
			//'OtherImage'=>$otherImages,
			'ProductTitle' => ucfirst(Input::get('title')),
			"QuantityStatus" => (Input::get('quantity') > 1) ? 'multiple' : 'single',
			"BoxQuantity" => (Input::get('quantity') == 0) ? 1 : (int) Input::get('quantity'),
			"Description" => Input::get('description'),
			"InsuranceStatus" => (Input::get('insurance') == 'yes') ? 'yes' : 'no',
			"PackageMaterial" => (Input::get('need_package_material') == 'yes') ? 'yes' : 'no',
			"PackageMaterialShipped" => '',
		];

		if ($add_item['needInsurance'] == 'yes') {
			$insurance = $this->get_insurance($add_item['productCost'], $add_item['productQty']);
			if ($insurance == 0) {
				$response['msg'] = 'Sorry! We are not able to provide insurence.';
				return json_encode($response);
			}
		}
		if (Input::get('request_id') != '') {
			// print_r(Input::get('request_id')); die;
			if (Additem::where(['_id' => Input::get('request_id')])->update($add_item)) {
				$response = ['success' => 1, 'msg' => 'Item has been updated successfully.'];
			}
		} else {
			if (Additem::insert($add_item)) {
				$response = ['success' => 1, 'msg' => 'Item has been added successfully.'];
			}
		}
		return json_encode($response);
	}

	public function prepare_request() {

		$data['trip_data'] = array();
		$data['transporter_data'] = array();
		$data['RequestType'] = Input::get("delivery");
		$data['country'] = CityStateCountry::where(['type' => 'Country', 'Status' => 'Active'])
			->orderBy('Content', 'Asc')
			->get(['_id', 'Content', 'state_available']);
		$data['category'] = Category::where(['type' => 'Category', 'Status' => 'Active'])->whereIn('TravelMode', ['ship', 'air'])->get(['TravelMode', 'Content']);

		$data['items'] = Additem::where(['user_id' => Session::get('UserId'), 'type' => 'delivery'])->get();
		$data['items_count'] = count($data['items']);
		$data['item'] = Item::Where(['Status' => 'Active'])->get();
		if (!Input::get('id') == '') {
			Session()->put(['session_trip' => trim(Input::get('id'))]);
			$data['trip_data'] = Trips::where(array('_id' => Input::get('id')))->first();
	        if (!count($data['trip_data']) > 0) {
	        	return redirect('near-by-transporter');
	        }
			$data['transporter_data'] = User::where(array('_id' => $data['trip_data']->TransporterId))->first();
			$data['category2'] = Category::where(['type' => 'Category', 'Status' => 'Active'])->whereIn('TravelMode', ['ship', 'air'])->whereIn('Content', $data['trip_data']->SelectCategory)->get(['TravelMode', 'Content']);
		}else{
			Session()->put(['session_trip' => '']);
		}
		
		if (count($data['items']) > 0) {
			return view('User::send_package.prepare-request', $data);
		} else {
			return view('User::send_package.prepare_add_item', $data);
		}
	}

	private function get_packageno() {
		return Utility::sequence('Request');
	}

	public function create_prepare_request() {
		$response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		$data['items'] = Additem::where(['user_id' => Session::get('UserId'), 'type' => 'delivery'])->get();
		$user = User::where(['_id' => Session::get('UserId')])->first();
		$PackageId = $this->get_packageno();
		$city = json_decode(Input::get('city'));
		$state = json_decode(Input::get('state10'));
		$country = json_decode(Input::get('country'));

		$drop_off_city = json_decode(Input::get('drop_off_city'));
		$drop_off_state = json_decode(Input::get('state20'));
		$drop_off_country = json_decode(Input::get('drop_off_country'));

		$return_city = json_decode(Input::get('return_city'));
		$return_state = json_decode(Input::get('state30'));
		$return_country = json_decode(Input::get('return_country'));

		$nd_return_city = json_decode(Input::get('nd_return_city'));
		$nd_return_state = json_decode(Input::get('state40'));
		$nd_return_country = json_decode(Input::get('nd_return_country'));

		$requester = User::where(['_id' => Session::get('UserId')])->first();

		if (count($requester) == 0) {
			return json_encode($response);die;
		}

		$calculated_distance = floatval(Input::get('calculated_distance'));

		if ($calculated_distance > 0) {
			$calculated_distance = $calculated_distance * 0.000621371;
		} else {
			$calculated_distance = Utility::getDistanceBetweenPointsNew(
				floatval(Input::get('PickupLat')),
				floatval(Input::get('PickupLong')),
				floatval(Input::get('DeliveryLat')),
				floatval(Input::get('DeliveryLong'))
			);
		}

		$configurationdata = Configuration::where(['_id' => '5673e33e6734c4f874685c84'])->first();
		$tripid = $transporterName = $transporterId = '';

		/*Trip Section*/
		if (Input::get('tripid') != '' && Input::get('transporter_id') != '') {
			$transporterId = new MongoId(Input::get('transporter_id'));
			$data['trip_data'] = Trips::where(['_id' => Input::get('tripid'),'TransporterId' => new MongoId(Input::get('transporter_id'))])->first();
			if (count($data['trip_data']) > 0) {
				if ($data['trip_data']->TripType == 'individual') {
					if ($data['trip_data']->SourceDate < new MongoDate()) {
						$response = ['success' => 0, 'msg' => 'Trip has been started. You are not able to create request.'];
						return json_encode($response);die;
					}
				}

				$transporterName = @$data['trip_data']->TransporterName;
				$tripid = $data['trip_data']->_id;
			} else {
				$response = ['success' => 0, 'msg' => 'Invalid trip or transporter.'];
				return json_encode($response);die;
			}
		}
		/*End trip section*/

		$insert = [
			'RequesterId' => new MongoId(Session::get('UserId')),
			'RequesterName' => ucfirst($requester->Name),
			'ProductTitle' => '',
			'RequestType' => "delivery",
			'Status' => 'pending',
			'request_version'=>'new',
			'ReceiverIsDifferent'=> (Input::get('ReceiverIsDifferent') == 'on')?'yes':'no', 
			'ReceiverName'=> (Input::get('ReceiverIsDifferent') == 'on')? ucfirst(Input::get('ReceiverName')):'' , 
			/*Trip*/
			"TransporterName" => $transporterName,
			"TransporterId" => $transporterId,
			"TripId" => $tripid,
			/*End trip*/
			'PackageId' => $PackageId,
			'device_version' => Input::get('browser'),
			'app_version' => Input::get('version'),
			'device_type' => Input::get('device_type'),
			"PackageNumber" => $PackageId . time(),
			"PickupFullAddress" => $this->formated_address([
				(Input::get('address_line_1') != '') ? Input::get('address_line_1') : '',
				(Input::get('address_line_2') != '') ? Input::get('address_line_2') : '',
				$city->name,
				$state->name,
				$country->name,
			], Input::get('zipcode')),
			'PickupAddress' => Input::get('address_line_1'),
			'PickupAddress2' => Input::get('address_line_2'),
			'PickupCity' => trim($city->name),
			'PickupState' => trim($state->name),
			'PickupCountry' => trim($country->name),
			'PickupPinCode' => Input::get('zipcode'),
			'PickupLatLong' => [floatval(Input::get('PickupLong')), floatval(Input::get('PickupLat'))],
			'PickupDate' => get_utc_time(Input::get('pickup_date')),
			'DeliveryFullAddress' => $this->formated_address([
				(Input::get('drop_off_address_line_1') != '') ? Input::get('drop_off_address_line_1') : '',
				(Input::get('drop_off_address_line_2') != '') ? Input::get('drop_off_address_line_2') : '',
				$drop_off_city->name,
				$drop_off_state->name,
				$drop_off_country->name,
			], Input::get('drop_off_zipcode')),
			'DeliveryAddress' => Input::get('drop_off_address_line_1'),
			'DeliveryAddress2' => Input::get('drop_off_address_line_2'),
			'DeliveryCity' => trim($drop_off_city->name),
			'DeliveryState' => trim($drop_off_state->name),
			'DeliveryCountry' => trim($drop_off_country->name),
			'DeliveryPincode' => Input::get('drop_off_zipcode'),
			"DeliveryLatLong" => [floatval(Input::get('DeliveryLong')), floatval(Input::get('DeliveryLat'))],
			'DeliveryDate' => get_utc_time(Input::get('drop_off_date')),

			"DeliveryStateId" => trim($drop_off_state->id),
			"DeliveryCountryId" => trim($drop_off_country->id),

			"ReturnFullAddress" => $this->formated_address([
				Input::get('return_address_line_1'),
				Input::get('return_address_line_2'),
				@$return_city->name,
				@$return_state->name,
				@$return_country->name,
			], Input::get('return_zipcode')),
			"ReturnAddress" => Input::get('return_address_line_1'),
			"ReturnAddress2" => Input::get('return_address_line_2'),
			"ReturnCityTitle" => trim(@$return_city->name),
			"ReturnStateTitle" => trim(@$return_state->name),
			'ReturnCountry' => trim(@$return_country->name),
			'ReturnPincode' => Input::get('return_zipcode'),
			"FlexibleDeliveryDate" => (Input::get('is_delivery_date_flexible') == 'yes') ? 'yes' : 'no',
			"JournyType" => (Input::get('journey_type') == 'return') ? 'return' : 'one_way',
			'NotDelReturnFullAddress' => $this->formated_address([
				Input::get('nd_return_address_line_1'),
				Input::get('nd_return_address_line_2'),
				@$nd_return_city->name,
				@$nd_return_state->name,
				@$nd_return_country->name,
			], Input::get('nd_return_zipcode')),
			"InCaseNotDelReturnAddress" => Input::get('nd_return_address_line_1'),
			"InCaseNotDelReturnAddress2" => Input::get('nd_return_address_line_2'),
			"InCaseNotDelReturnCity" => trim(@$nd_return_city->name),
			"InCaseNotDelReturnState" => trim(@$nd_return_state->name) ,
			"InCaseNotDelReturnCountry" => trim(@$nd_return_country->name),
			"InCaseNotDelReturnPincode" => Input::get('nd_return_zipcode'),
			"PackageMaterial" => (Input::get('need_package_material') == 'yes') ? 'yes' : 'no',
			"PackageMaterialShipped" => '',
			"PublicPlace" => (Input::get('public_palce') == 'yes') ? 'yes' : 'no',
			'ReceiverCountrycode' => Input::get('country_code'),
			'ReceiverMobileNo' => Input::get('phone_number'),
			"consolidate_item" => (Input::get('consolidate_check') == 'on') ? 'on' : 'off',
			"Distance" => $calculated_distance,
			'ShippingCost' => 0,
			"totalUpsRate" => 0,
			"WareHouseTransitFee"=> 0,
			'InsuranceCost' => 0,
			'Discount' => 0,
			'DutyAndCustom'=>0,
			'Tax'=>0,
			'TotalCost' => 0,
			'promocode' => Input::get('promocode'),
			'AquantuoFees' => 0,
			'region_charge' => 0,
			'after_update_difference' => 0,
			'itemCount' => 0,
			'ProductList' => [],
		];

		if (Input::get('request_id') == '') {
			$res = json_decode($this->prepare_request_calculation());
			$insert["EnterOn"] = new MongoDate();
		} else {
			$res = json_decode($this->edit_calculation());
		}
		
		if (count($res->product) > 0) {
			$k = 0;
			$total_item_price = 0;
			foreach ($res->product as $key) {
				$k++;

				if (!empty(trim($insert["ProductTitle"]))) {
					$insert["ProductTitle"] = "{$insert['ProductTitle']}, ";
				}
				// print_r($key);
				$insert["ProductTitle"] .= @$key->product_name;
				$insert['ShippingCost'] += $key->shippingCost + @$key->productUpsRate;
				$insert['totalUpsRate'] += @$key->productUpsRate;
				$insert['InsuranceCost'] += $key->InsuranceCost;
				$insert['TotalCost'] += $key->shippingCost + $key->InsuranceCost + @$key->productUpsRate;
				$total_item_price += floatval($key->productCost) * $key->productQty;

				$fees = ((floatval($key->shippingCost + @$key->productUpsRate) * $configurationdata->aquantuoFees) / 100);
				$insert['AquantuoFees'] += $fees;

				$insert['ProductList'][] = [
					'_id' => (Input::get('request_id') == '') ? (string) new MongoId() : $key->_id,
					"product_name" => @$key->product_name,
					'package_id' => (Input::get('request_id') == '') ? $insert['PackageNumber'] . $k : $key->package_id,
					'status' => (Input::get('request_id') == '') ? 'pending' : $key->status,
					"productUpsRate" => @$key->productUpsRate,
					"productWidth" => $key->productWidth,
					"productHeight" => $key->productHeight,
					"productLength" => $key->productLength,
					"productCost" => $key->productCost,
					"productWeight" => $key->productWeight,
					"productHeightUnit" => $key->productHeightUnit,
					"ProductWeightUnit" => $key->ProductWeightUnit,
					"ProductLengthUnit" => $key->ProductLengthUnit,
					"productCategory" => $key->productCategory,
					"productCategoryId" => $key->productCategoryId,
					"travelMode" => $key->travelMode,
					"InsuranceStatus" => $key->InsuranceStatus,
					"productQty" => $key->productQty,
					"ProductImage" => $key->ProductImage,
					//"OtherImage" => $key->OtherImage,
					"QuantityStatus" => $key->QuantityStatus,
					"BoxQuantity" => @$key->BoxQuantity,
					"Description" => $key->Description,
					"PackageMaterial" => $key->PackageMaterial,
					"PackageMaterialShipped" => $key->PackageMaterialShipped,
					"shippingCost" => $key->shippingCost + @$key->productUpsRate,
					'InsuranceCost' => $key->InsuranceCost,
					"DeliveryVerifyCode" => (Input::get('request_id') == '') ? rand(1000, 9999) : $key->DeliveryVerifyCode,
					'tpid' => Input::get('transporter_id'),
					'tpName' => $transporterName,
					'inform_mail_sent' => 'no',
					'PaymentStatus' => 'no',
					'aq_fees' => floatval($fees),
					'total_cost' => $key->shippingCost + $key->InsuranceCost + @$key->productUpsRate,
					'after_update' => $key->shippingCost + $key->InsuranceCost + @$key->productUpsRate,
					'TransporterFeedbcak' => '',
					'TransporterRating' => '',
					'RequesterFeedbcak' => '',
					'RequesterRating' => '',
					'EnterOn' => new Mongodate(),
					'ExpectedDate' => '',
					'DeliveredTime' => '',
				];
			}

			$insert['itemCount'] = $k;
			$insert['ShippingCost'] += $res->warehouse_transit_fee;
			$discount = Promo::get_validate(Input::get('promocode'), $insert['ShippingCost'], Session::get('UserId'), $user->Default_Currency);

			//$insert['AquantuoFees'] = $this->get_aquantuo_fees($insert['TotalCost']);
			//extraa charge of out side accra
			$insert['region_charge'] = $this->resionCharges(['name' => $drop_off_state->name, 'id' => $drop_off_state->id]);
			//end accra charge
			$insert['TotalCost'] = $insert['TotalCost'] + $insert['region_charge']+$res->DutyAndCustom+$res->Tax+$res->warehouse_transit_fee;
			$insert['DutyAndCustom'] = $res->DutyAndCustom;
			$insert['WareHouseTransitFee'] = $res->warehouse_transit_fee;
			$insert['Tax'] = $res->Tax;
			
			if ($discount['success'] == 1) {
				$insert['Discount'] = $discount['discount'];
				$insert['TotalCost'] = $insert['TotalCost'] - $discount['discount'];
			}

			$insert['need_to_pay'] = floatval($insert['TotalCost']);
		} else {
			return json_encode($response);die;
		}
		

		if (Input::get('request_id') == '') {
			$record = (String) Deliveryrequest::insertGetId($insert);
		} else {
			$record = Deliveryrequest::where(['_id' => trim(Input::get('request_id')), 'RequesterId' => new MongoId(Session::get('UserId'))])
				->update($insert);
		}

		if ($record) {
			if (Input::get('request_id') == '') {
				Additem::where(['user_id' => Session::get('UserId'), 'type' => 'delivery'])->delete();
				$response = ['success' => 1, 'msg' => 'Request has been created successfully. Please proceed to payment page to make payment', 'reqid' => $record, 'type' => 'create'];

				$ActivityLog = DeliveryRequest::where('_id', '=', $response['reqid'])->select('_id', 'PackageNumber', 'ProductList')->first();

				PaymentInfo::insert([
					'request_id' => $response['reqid'],
					'user_id'=> Session::get('UserId'),
					'action_user_id'=> Session::get('UserId'),
					'RequestType'=> 'delivery',
					'action' => 'create',
					'item_id'=> '',
					'item_unic_number' => '',
					'TotalCost'=> floatval($insert['TotalCost']),
					'shippingCost'=> floatval($insert['ShippingCost']),
					'insurance'=> floatval($insert['InsuranceCost']),
					'AreaCharges'=> floatval($insert['region_charge']),
					'ProcessingFees'=> 0,
					'shipping_cost_by_user'=> 0,
					'item_cost'=> floatval($total_item_price),
					'discount'=> floatval($insert['Discount']),
					'PromoCode'=> $insert['promocode'],
					'difference'=> floatval($insert['after_update_difference']),
					'difference_before'=> floatval($insert['after_update_difference']),
					'date'=> New MongoDate(),
				]);

				$array = $ActivityLog->ProductList;
				foreach ($array as $key) {
					/* Activity Log */
					$insertactivity = [
						'request_id' => $response['reqid'],
						'request_type' => 'delivery',
						'PackageNumber' => $ActivityLog->PackageNumber,
						'item_id' => $key['_id'],
						'package_id' => $key['package_id'],
						'item_name' => $key['product_name'],
						'log_type' => 'request',
						'message' => 'Package has been created.',
						'status' => 'pending',
						'action_user_id'=> Session::get('UserId'),
						'EnterOn' => new MongoDate(),

					];
					Activitylog::insert($insertactivity);
				}
			} else {
				PaymentInfo::insert([
					'request_id' => Input::get('request_id'),
					'user_id'=> Session::get('UserId'),
					'action_user_id'=> Session::get('UserId'),
					'RequestType'=> 'delivery',
					'action' => 'update_request',
					'item_id'=> '',
					'item_unic_number' => '',
					'TotalCost'=> floatval($insert['TotalCost']),
					'shippingCost'=> floatval($insert['ShippingCost']),
					'insurance'=> floatval($insert['InsuranceCost']),
					'AreaCharges'=> floatval($insert['region_charge']),
					'ProcessingFees'=> 0,
					'shipping_cost_by_user'=> 0,
					'item_cost'=> floatval($total_item_price),
					'discount'=> floatval($insert['Discount']),
					'PromoCode'=> $insert['promocode'],
					'difference'=> floatval($insert['after_update_difference']),
					'difference_before'=> floatval($insert['after_update_difference']),
					'date'=> New MongoDate(),
				]);
				
				Notification::insert([
					"NotificationTitle" => "Update request of Send a package",
					"NotificationMessage" => sprintf('The send a package request titled:"%s", ID:%s has been updated.', ucfirst($insert['ProductTitle']), $insert['PackageNumber']),
					"NotificationUserId" => [],
					"NotificationReadStatus" => 0,
					"location" => "delivery",
					"locationkey" => (String) Input::get('request_id'),
					"Date" => new MongoDate(),
					"GroupTo" => "Admin",
				]);

				$response = ['success' => 1, 'msg' => 'Request has been updated successfully.Please proceed to payment page to make payment', 'reqid' => trim(Input::get('request_id')), 'type' => 'update'];
			}
		}
		return json_encode($response);
	}

	public function get_aquantuo_fees($totalcost) {
		$fees = 0;
		$configurationdata = Configuration::where(['_id' => '5673e33e6734c4f874685c84'])->first();
		if ($configurationdata) {
			$fees = ((floatval($totalcost) * $configurationdata->aquantuoFees) / 100);
		}
		return $fees;
	}


	public function checkTrip($data)
	{
		$response = ['success' => 1, 'msg' => 'Something went wrong.'];
		if ($data['tripid'] != '' && $data['transporter_id'] != '') {
			
			$data['trip_data'] = Trips::where(['_id' => $data['tripid'],'TransporterId' => new MongoId($data['transporter_id'])])->first();
			if(count($data['trip_data']) > 0){

				if($data['trip_data']->TripType == 'individual'){
					if ($data['trip_data']->SourceDate < new MongoDate()) {
						$response = ['success' => 0, 'msg' => 'Trip has been started. You are not able to create request.'];
					}
				}

				$transporterName = @$data['trip_data']->TransporterName;
				$tripid = $data['trip_data']->_id;
				$response = ['success' => 1, 'msg' => 'Success'];
			}else{
				$response = ['success' => 0, 'msg' => 'Invalid trip or transporter.'];
			}
		}
		return $response;
	}

	public function activityLog($inputArray) {

		
		$req_data = DeliveryRequest::where(["_id" => $inputArray['_id']])->select('PackageNumber', 'RequestType', 'Status', 'ProductList', 'ProductTitle')->first();

		if (count($req_data) > 0) {
			
			if ($req_data->Status == 'pending') {
				$message = 'Package has been created.';
			} elseif ($req_data->Status == 'ready') {
				$message = 'Payment successful.';
			} elseif ($req_data->Status == 'out_for_pickup') {
				$message = 'Request has been pickedup.';
			} elseif ($req_data->Status == 'out_for_delivery') {
				$message = 'Request has been changed to Out for Delivery.';
			} elseif ($req_data->Status == 'delivered') {
				$message = 'You have delivered an item.';
			} elseif ($req_data->Status == 'cancel') {
				$message = 'Product has been cancel.';
			}

			
			if (isset($req_data->ProductList)) {
				foreach ($req_data->ProductList as $key) {
					Activitylog::insert(['request_id' => $inputArray['_id'],
						'request_type' => $req_data->RequestType,
						'PackageNumber' => $req_data->PackageNumber,
						'log_type' => 'request',
						'status' => $req_data->Status,
						'message' => $message,
						'package_id' => $key['package_id'],
						'item_name' => $key['product_name'],
						'action_user_id'=> Session::get('UserId'),
						'EnterOn' => new MongoDate]
					);
				}
			}
		}
	}


	public function pay_now($requestid, $id, Request $request) {
		
		$where = [
			'RequesterId' => new MongoId(Session()->get('UserId')),
			'_id' => $requestid,
		];
		$user = User::where(array('_id' => Session()->get('UserId')))->first();
		$del_detail = Deliveryrequest::where($where)->whereIn('Status', ['pending', 'ready'])->first();
		if (count($del_detail) <= 0 || count($user) <= 0) {
			return Redirect::to("process-card-list/{$requestid}")->with('danger', "Fail! Something is wrong");
		}

		if (count($del_detail) > 0 && count($user) > 0) {

			$status = 'ready'; $tpname= ''; $tpid='';
			if($del_detail->TripId != ''){
				$tripCheck = $this->checkTrip(['tripid'=>$del_detail->TripId,'transporter_id'=> (String) $del_detail->TransporterId]);

				if($tripCheck['success'] == 0){
					return Redirect::to("process-card-list/{$requestid}")->with('danger', $tripCheck['msg']);
				}else{
					$status = 'accepted';
					$tpname = $del_detail->TransporterName;
					$tpid = (String) $del_detail->TransporterId;
				}
			}

			if (!empty($del_detail->StripeChargeId)) {
				RSStripe::refund($del_detail->StripeChargeId);
			}
			$cost = $del_detail->TotalCost + $del_detail->after_update_difference;
			$res = Payment::capture($user, $cost, $request->segment(3), true);
			if (isset($res['id'])) {

				$update['StripeChargeId'] = $res['id'];
				$update['need_to_pay'] = 0;

				Transaction::insert(array(
					"SendById" => (String) $user->_id,
					"SendByName" => @$user->RequesterName,
					"SendToId" => "aquantuo",
					"RecieveByName" => "Aquantuo",
					"Description" => "Amount deposited against delivery request for {$del_detail->ProductTitle}, PackageId: {$del_detail->PackageNumber} from {$del_detail->PickupFullAddress} to {$del_detail->DeliveryFullAddress}",
					"Credit" => $cost,
					"Debit" => "",
					"Status" => "completed",
					"TransactionType" => "delivery_request",
					'request_id'=> $requestid,
					'item_id'=>'',
					"EnterOn" => new MongoDate(),
				));

				if (isset($del_detail->ProductList)) {
					$p_array = $del_detail->ProductList;
					foreach ($p_array as $key => $value) {
						$p_array[$key]['status'] = $status;
						$p_array[$key]['PaymentStatus'] = 'yes';


						/* Activity Log */
						$insertactivity = [
							'request_id' => $requestid,
							'request_type' => 'delivery',
							'PackageNumber' => $del_detail->PackageNumber,
							'item_id' => $p_array[$key]['_id'],
							'package_id' => $p_array[$key]['package_id'],
							'item_name' => $p_array[$key]['product_name'],
							'log_type' => 'request',
							'message' => 'Payment successful.',
							'status' => 'ready',
							'EnterOn' => new MongoDate(),
						];
						//Activitylog::insert($insertactivity);
					}
					$update['ProductList'] = $p_array;
				}

				RequestCard::Insert([
					'request_id' => $requestid,
					'item_id' => '',
					'card_id' => $request->segment(3),
					'type' => 'request',
					'payment' => $cost,
					'brand' => @$res['source']['brand'],
					'last4' => @$res['source']['last4'],
					'description' => "Payment done for request Title: '" . $del_detail->ProductTitle . "' Package Id:" . $del_detail->PackageNumber . ".",
					"EnterOn" => new MongoDate(),
				]);
				$update['Status'] = $status;

				if (Deliveryrequest::where($where)->update($update)) {
					//Reqhelper::send_notification_to_tp(['id' => $requestid,
					//'request_type' => Input::get('request_type')]);
					Reqhelper::send_notification_to_tp(['id' => $requestid,'request_type' => 'delivery']);

					Notification::Insert([
						array(
							"NotificationTitle" => trans('lang.SEND_CREATE_TITLE'),
							"NotificationMessage" => sprintf(trans('lang.SEND_CREATE_MSG'), $del_detail->ProductTitle, $del_detail->PackageNumber),
							"NotificationUserId" => array(),
							"NotificationReadStatus" => 0,
							"location" => "request_detail",
							"locationkey" => (string) $requestid,
							"Date" => new MongoDate(),
							"GroupTo" => "Admin",
						),
					]);

					if ($user->EmailStatus == "on") {
						$cron_mail = [
							"USERNAME" => ucfirst($user->Name),
							"PACKAGETITLE" => ucfirst($del_detail->ProductTitle),
							"PACKAGEID" => $del_detail->PackageNumber,
							"SOURCE" => $del_detail->PickupFullAddress,
							"DESTINATION" => $del_detail->DeliveryFullAddress,
							"PACKAGENUMBER" => $del_detail->PackageNumber,
							"SHIPPINGCOST" => $del_detail->ShippingCost,
							"TOTALCOST" => $cost,
							'email_id' => '5694a10f5509251cd67773eb',
							'email' => $user->Email,
							'status' => 'ready',
						];
						SendMail::insert($cron_mail);
					}

					$setting = Setting::find('563b0e31e4b03271a097e1ca');
					if (count($setting) > 0) {
						$cron_mail2 = [
							"USERNAME" => ucfirst($user->Name),
							"PACKAGETITLE" => ucfirst($del_detail->ProductTitle),
							"PACKAGEID" => $del_detail->PackageNumber,
							"SOURCE" => $del_detail->PickupFullAddress,
							"DESTINATION" => $del_detail->DeliveryFullAddress,
							"PACKAGENUMBER" => $del_detail->PackageNumber,
							"SHIPPINGCOST" => $del_detail->ShippingCost,
							"TOTALCOST" => $cost,
							'email_id' => '56cd3e1f5509251cd677740c',
							'email' => $setting->SupportEmail,
							'status' => 'ready',
						];
						SendMail::insert($cron_mail2);
					}
					$inputArray = [
						'requestid' => $del_detail->_id,
						'_id' => $del_detail->_id,
						'log_type' => "request",
						'message' => "Request Created.",
					];

					$this->activityLog($inputArray);

					return Redirect::to("send-package-detail/{$requestid}")->with('success', "Success! Your item has been successfully posted. Once a transporter reviews and accepts it, you will be notified.");

				}

			}
		}
	}

	public function my_request() {
		$data['paginationUrl'] = "pagination/new-my-request";
		$data['postValue'] = "&search=" . Input::get('search') . "&search_date=" . Input::get('search_date') . "&Status=" . Input::get('Status');
		return view('User::list.my-request', array('data' => $data));
	}

	public function addItemInExistRequest() {
		$response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		if (Input::get('request_id') != '') {
			$data['request_data'] = Deliveryrequest::where(['_id' => trim(Input::get('request_id'))])->select('ProductList','PackageNumber')->first();
			$lhwunit = 'cm';
			$weightunit = 'kg';

			if (Input::get('measurement_unit') == 'inches_lbs') {
				$lhwunit = 'inches';
				$weightunit = 'lbs';
			}

			$ProductImage = '';

			if (Input::hasFile('default_image')) {
				if (Input::file('default_image')->isValid()) {
					$ext = Input::file('default_image')->getClientOriginalExtension();
					$ProductImage = time() . rand(100, 9999) . ".$ext";

					if (Input::file('default_image')->move(BASEURL_FILE . 'package/', $ProductImage)) {
						$ProductImage = "package/$ProductImage";
					} else {
						$ProductImage = '';
					}
				}
			}
			
			$otherImages = [];
			$imageArray = array('package_image2', 'package_image3', 'package_image4', 'package_image5');

			foreach ($imageArray as $key => $val) {
				if (Input::hasFile($val)) {
					if (Input::file($val)->isValid()) {
						$ext = Input::file($val)->getClientOriginalExtension();
						$img = time() . rand(100, 9999) . ".$ext";

						if (Input::file($val)->move(BASEURL_FILE . 'package/', $img)) {
							$img = "package/$img";
							if (isset($otherImages[$key])) {
								// Start remove old image
								if (!empty(trim($otherImages[$key]))) {
									if (file_exists(BASEURL_FILE . $otherImages[$key])) {
										unlink(BASEURL_FILE . $otherImages[$key]);
									}
								}
								// End remove old image
								$otherImages[$key] = $img;
							} else {
								$otherImages[] = $img;
							}
						}
					}
				}
			}

			$category = json_decode(Input::get('category'));
			$itemCount = count($data['request_data']->ProductList);
			$itemCount = $itemCount + 1;

			//user t stop to if mode wa sea and category diffrent
			$is_make_request = 'yes';
			$item_with_sea = false;
			$item_with_air = false;

			foreach ($data['request_data']['ProductList'] as $value) {
				if ($value['productCategoryId'] != @$category->id && $value['travelMode'] == 'ship' ) {
					$is_make_request = 'no';
					$item_with_sea = true;
				}

				if (Input::get('travel_mode') == 'ship') {
					$item_with_sea = true;
				}

				if ($value['travelMode'] == 'air' || Input::get('travel_mode') == 'air') {
					$item_with_air = true;
				}

				if ($item_with_air && $item_with_sea) {
					$response['msg'] = "Sorry! You can’t have different shipping modes in the same listing. Please create a separate listing for either air or sea.";
					return response()->json($response);
					die();
				}
			}

			if ($is_make_request == 'no') {
				$response['msg'] = "If shipping by sea, all items must be of the same sea category. Please create a new listing if you need to use a different sea category.";
				return response()->json($response);
				die();
			}
			
			$add_item = [
				"_id" => (String) new MongoId(),
				'product_name' => ucfirst(Input::get('title')),
				'package_id'=>$data['request_data']->PackageNumber.$itemCount,
				'status' => 'pending',
				'user_id' => Session::get('UserId'),
				'type' => 'delivery',
				'productWidth' => Input::get('width'),
				'productHeight' => Input::get('height'),
				'productLength' => Input::get('length'),
				'productCost' => Input::get('package_value'),
				'productWeight' => Input::get('weight'),
				'productHeightUnit' => $lhwunit,
				'ProductWeightUnit' => $weightunit,
				'ProductLengthUnit' => $lhwunit,
				'productCategory' => @$category->name,
				'productCategoryId' => @$category->id,
				'travelMode' => Input::get('travel_mode'),
				'currency' => Input::get('Default_Currency'),
				'needInsurance' => Input::get('insurance'),
				"productQty" => (Input::get('quantity') < 1) ? 1 : Input::get('quantity'),
				'ProductImage' => $ProductImage,
				'ProductTitle' => ucfirst(Input::get('title')),
				"QuantityStatus" => (Input::get('quantity') > 1) ? 'multiple' : 'single',
				"BoxQuantity" => (Input::get('quantity') == 0) ? 1 : (int) Input::get('quantity'),
				"Description" => Input::get('description'),
				"InsuranceStatus" => (Input::get('insurance') == 'yes') ? 'yes' : 'no',
				"PackageMaterial" => (Input::get('need_package_material') == 'yes') ? 'yes' : 'no',
				"PackageMaterialShipped" => '',
				'shippingCost'=>0,
				'InsuranceCost'=>0,
				"DeliveryVerifyCode" => rand(1000, 9999),
				'tpid'=>'',
				'inform_mail_sent' => 'no',
				'PaymentStatus' => 'no',
				'aq_fees'=>0,
				'total_cost'=>0,
				'after_update'=>0,
				'TransporterFeedbcak' => '',
				'TransporterRating' => '',
				'RequesterFeedbcak' => '',
				'RequesterRating' => '',
				'TransporterMessage' => '',
				'ExpectedDate' => '',
				'DeliveredTime' => '',

			];

			if (isset($data['request_data']->ProductList)) {
				$update = Deliveryrequest::where(['_id' => trim(Input::get('request_id'))])->push(['ProductList' => $add_item]);
				if ($update) {
					$response = ['success' => 1, 'msg' => 'Item has been added successfully.'];
				}
			}
		}
		return json_encode($response);
	}

	public function editItemInExistRequest() {
		$response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];

		if (Input::get('request_id') != '' && Input::get('item_id') != '') {
			$request_data = Deliveryrequest::where(['_id' => trim(Input::get('request_id')), 'RequesterId' => new MongoId(Session::get('UserId'))])
				->select('ProductList')->first();

			if ($request_data) {

				$update_array = $request_data->ProductList;
				$ProductImage = '';
				$otherImages = [];

				$lhwunit = 'cm';
				$weightunit = 'kg';
				if (Input::get('measurement_unit') == 'inches_lbs') {
					$lhwunit = 'inches';
					$weightunit = 'lbs';
				}

				if (Input::hasFile('default_image')) {
					if (Input::file('default_image')->isValid()) {
						$ext = Input::file('default_image')->getClientOriginalExtension();
						$ProductImage = time() . rand(100, 9999) . ".$ext";
						if (Input::file('default_image')->move(BASEURL_FILE . 'package/', $ProductImage)) {
							$ProductImage = "package/$ProductImage";
						} else {
							$ProductImage = '';
						}
					}
				}

				$imageArray = array('package_image2', 'package_image3', 'package_image4', 'package_image5');
				foreach ($imageArray as $key => $val) {
					if (Input::hasFile($val)) {
						if (Input::file($val)->isValid()) {
							$ext = Input::file($val)->getClientOriginalExtension();
							$img = time() . rand(100, 9999) . ".$ext";

							if (Input::file($val)->move(BASEURL_FILE . 'package/', $img)) {
								$img = "package/$img";
								if (isset($otherImages[$key])) {
									// Start remove old image
									if (!empty(trim($otherImages[$key]))) {
										if (file_exists(BASEURL_FILE . $otherImages[$key])) {
											unlink(BASEURL_FILE . $otherImages[$key]);
										}
									}
									// End remove old image
									$otherImages[$key] = $img;
								} else {
									$otherImages[] = $img;
								}
							}
						}
					}
				}

				//user t stop to if mode wa sea and category diffrent
				$is_make_request = 'yes';
				$item_with_sea = false;
				$item_with_air = false;

				$category = json_decode(Input::get('category'));
				foreach ($request_data->ProductList as $key => $value) {
					if ($value['_id'] == trim(Input::get('item_id'))) {
						$update_array[$key]['product_name'] = Input::get('title');
						$update_array[$key]['productWidth'] = Input::get('width');
						$update_array[$key]['productHeight'] = Input::get('height');
						$update_array[$key]['productLength'] = Input::get('length');
						$update_array[$key]['productCost'] = trim(Input::get('package_value'));
						$update_array[$key]['productWeight'] = trim(Input::get('weight'));
						$update_array[$key]['productHeightUnit'] = trim($lhwunit);
						$update_array[$key]['ProductWeightUnit'] = trim($weightunit);
						$update_array[$key]['ProductLengthUnit'] = trim($lhwunit);
						$update_array[$key]['productCategory'] = $category->name;
						$update_array[$key]['productCategoryId'] = $category->id;
						$update_array[$key]['travelMode'] = trim(Input::get('travel_mode'));
						$update_array[$key]['InsuranceStatus'] = (Input::get('insurance') == 'yes') ? 'yes' : 'no';
						$update_array[$key]['productQty'] = (Input::get('quantity') < 1) ? 1 : Input::get('quantity');
						$update_array[$key]['ProductImage'] = ($ProductImage == '') ? $value['ProductImage'] : $ProductImage;
						//$update_array[$key]['OtherImage'] = (empty($otherImages))? $value['OtherImage'] : $otherImages;
						$update_array[$key]['QuantityStatus'] = (Input::get('quantity') > 1) ? 'multiple' : 'single';
						$update_array[$key]['BoxQuantity'] = (Input::get('quantity') == 0) ? 1 : (int) Input::get('quantity');
						$update_array[$key]['Description'] = Input::get('description');
						$update_array[$key]['PackageMaterial'] = (Input::get('need_package_material') == 'yes') ? 'yes' : 'no';
						$update_array[$key]['PackageMaterialShipped'] = "";

						if (Input::get('insurance') == 'yes') {
							$insurance = $this->get_insurance(Input::get('package_value'), Input::get('package_value'));
							if ($insurance == 0) {
								$response['msg'] = 'Sorry! We are not able to provide insurence.';
								return json_encode($response);
							}
						}
					}

					if ($value['productCategoryId'] != @$category->id && $value['travelMode']=='ship' ) {
						$is_make_request = 'no';
						$item_with_sea = true;
					}

					if (Input::get('travel_mode') == 'ship') {
						$item_with_sea = true;
					}

					if ($value['travelMode'] == 'air' || Input::get('travel_mode') == 'air') {
						$item_with_air = true;
					}

					if ($item_with_air && $item_with_sea) {
						$response['msg'] = "Sorry! You can’t have different shipping modes in the same listing. Please create a separate listing for either air or sea.";
						return response()->json($response);
						die();
					}
				}

				if ($is_make_request == 'no') {
					$response['msg'] = "If shipping by sea, all items must be in the same category Please create a new listing for items meant for Air or different Sea category.";
					return response()->json($response);
					die();
				}

				$update_data = Deliveryrequest::where(['_id' => trim(Input::get('request_id')), 'RequesterId' => new MongoId(Session::get('UserId'))])
					->update(['ProductList' => $update_array]);
				if ($update_data) {
					$response = ['success' => 1, 'msg' => 'Item has been updated successfully.'];
				}
			}
		}
		return json_encode($response);
	}

	public function remove_item($id, $requestid) {
		$response = array('success' => 0, 'msg' => 'Fail to delete request.');
		if ($requestid) {
			$res_data = Deliveryrequest::where(['_id' => trim($requestid), 'RequesterId' => new MongoId(Session::get('UserId'))])
				->select('ProductList')->first();
			if (count($res_data) > 0) {
				if (isset($res_data->ProductList)) {
					$update_array = $res_data->ProductList;
					foreach ($update_array as $key => $value) {
						if ($value['_id'] == $id) {
							unset($update_array[$key]);
						}
					}
					$update = Deliveryrequest::where(['_id' => trim($requestid), 'RequesterId' => new MongoId(Session::get('UserId'))])
						->update(['ProductList' => $update_array]);
					if ($update) {
						$response['success'] = 1;
						$response['msg'] = 'Item has been removed successfully.';
					}
				}

			}
		}
		return json_encode($response);
	}

	public function item_payment_page($requestid, $itemid) {
		$data['admin_msg'] = '';
		$config_data = Configuration::find('5673e33e6734c4f874685c84');
		if (count($config_data) > 0) {
			$data['admin_msg'] = $config_data->message;
		}
		$request = Deliveryrequest::where(array('_id' => $requestid))->select('RequestType')->first();
		if (!count($request) > 0) {

			return redirect('/my-request');
		}
		$data['cards'] = array();
		$data['RequestType'] = $request->RequestType;
		$user = User::where(array('_id' => Session()->get('UserId')))->select('StripeId')->first();
		if (count($user) > 0) {
			$data['cards'] = Payment::card_list($user->StripeId);
		}
		return view('User::send_package.item_payment', $data);
	}

	public function item_payment_page2($requestid) {
		$data['admin_msg'] = '';
		$config_data = Configuration::find('5673e33e6734c4f874685c84');
		if (count($config_data) > 0) {
			$data['admin_msg'] = $config_data->message;
		}
		$request = Deliveryrequest::where(array('_id' => $requestid))->select('RequestType','PackageNumber','ProductTitle','need_to_pay')->first();
		if (!count($request) > 0) {

			return redirect('/my-request');
		}
		$data['cards'] = array();
		$data['info'] = $request;
		$data['RequestType'] = $request->RequestType;
		$user = User::where(array('_id' => Session()->get('UserId')))->select('StripeId')->first();
		if (count($user) > 0) {
			$currency_conversion = $this->currency_conversion($data['info']->need_to_pay);

			if($user->Default_Currency == 'CAD'){
				$data['user_currency'] = $currency_conversion['canadian_cost'];
			}else if($user->Default_Currency == 'PHP'){
				$data['user_currency'] = $currency_conversion['philipins_cost'];
			}else if($user->Default_Currency == 'GBP'){
				$data['user_currency'] = $currency_conversion['philipins_cost'];
			}else{
				$data['user_currency'] = $currency_conversion['ghanian_cost'];
			}
			$data['cards'] = Payment::card_list($user->StripeId);
		}
		return view('User::send_package.item_payment', $data);
	}

	public function item_payment($requestid, $itemid, $card, $type) {
		if ($requestid != '' && $itemid != '') {
			$userinfo = User::find(Session::get('UserId'));
			$where = [
				'_id' => $requestid,
				'RequesterId' => new MongoId(Session::get('UserId')),
				'RequestType' => trim($type),
				"ProductList" => ['$elemMatch' => [
					'_id' => $itemid,
					'status' => 'not_purchased',
				]],
			];
			$req_data = Deliveryrequest::where($where)->first();

			if (count($req_data) > 0 && count($userinfo) > 0) {
				$update_array = $req_data->ProductList;
				foreach ($update_array as $key => $value) {
					if ($itemid == $value['_id']) {
						$cost = $value['after_update'] - $value['total_cost'];
						$item_name = $value['product_name'];

						$package_id = $value['package_id'];
						$update_array[$key]['PaymentStatus'] = 'yes';

						$update_array[$key]['status'] = ($type == 'delivery') ? 'purchased' : 'paid';

					}
				}

				$res = Payment::capture($userinfo, (($cost)), $card, true);

				if (isset($res['id'])) {

					Deliveryrequest::where($where)->update(['ProductList' => $update_array]);

					Transaction::insert(array(
						"SendById" => (String) @$userinfo->_id,
						"SendByName" => @$userinfo->Name,
						"SendToId" => "aquantuo",
						"RecieveByName" => "Aquantuo",
						"Description" => "Amount deposited against product '{$item_name}', PackageId: {$package_id}",
						"Credit" => floatval($cost),
						"Debit" => "",
						"Status" => "completed",
						"TransactionType" => "delivery_request",
						"request_id"=> $requestid,
						"item_id"=> $itemid,
						"EnterOn" => new MongoDate(),
					));

					RequestCard::Insert([
						'request_id' => $requestid,
						'item_id' => $itemid,
						'card_id' => $card,
						'type' => 'request',
						'payment' => $cost,
						'brand' => @$res['source']['brand'],
						'last4' => @$res['source']['last4'],
						'description' => "Payment done for item Title: '" . $item_name . "' Item Id:" . $package_id . ".",
						"EnterOn" => new MongoDate(),
					]);
					$res = Reqhelper::update_status($requestid);
					Deliveryrequest::where($where)->update(['Status' => $res]);

					if ($type == 'delivery') {
						return Redirect::to("send-package-detail/{$requestid}")->with('success', "Success! Payment for product '" . $item_name . "' has been successfully completed.Aquantuo will proceed with next steps immediately.");
					} elseif ($type == 'buy_for_me') {
						return Redirect::to("buy-for-me-detail/{$requestid}")->with('success', "Success! Payment for product '" . $item_name . "' has been successfully completed.Aquantuo will proceed with next steps immediately.");
					} elseif ($type == 'online') {
						return Redirect::to("online-request-detail/{$requestid}")->with('success', "Success! Payment for product '" . $item_name . "' has been successfully completed.Aquantuo will proceed with next steps immediately.");
					}

				}
			} else {
				return redirect::back()->with('danger', "Oops ! Something went wrong.");
			}
		}
		return redirect::back()->with('danger', "Oops ! Something went wrong.");
	}

	public function item_payment2($requestid, $card, $type) {
		if ($requestid != '') {
			$userinfo = User::find(Session::get('UserId'));
			$where = [
				'_id' => $requestid,
				'RequesterId' => new MongoId(Session::get('UserId')),
				'RequestType' => trim($type),
			];

			$req_data = Deliveryrequest::where($where)->first();

			if (count($req_data) > 0 && count($userinfo) > 0) {
				$cost = $req_data->need_to_pay;
				$item_name = $req_data->ProductTitle;
				$package_id = $req_data->PackageNumber;
				$res = Payment::capture($userinfo, (($cost)), $card, true);
				
				$TransactionType = 'delivery_request';
				if ($type == 'delivery') {
						$TransactionType = 'delivery_request';
				} elseif ($type == 'buy_for_me') {
						$TransactionType = 'buy_for_me';
				} elseif ($type == 'online') {
						$TransactionType = 'online';
				}elseif ($type == 'local_delivery') {
						$TransactionType = 'local_delivery';
				}
				
			
				if (isset($res['id'])) {
					//Deliveryrequest::where($where)->update(['ProductList' => $update_array]);
					foreach ($req_data->ProductList as $value) {
						$insertactivity = [
							"request_id" => $requestid,
							"request_type" => $type,
							"PackageNumber" => $req_data->PackageNumber,
							"item_id" => $value["_id"],
							"package_id" => $value["package_id"],
							"item_name" => $value["product_name"],
							"log_type" => "request",
							"message" => "Payment successful.",
							"status" => "ready",
							"action_user_id" => Session::get("UserId"),
							"EnterOn" => new MongoDate()
						];
						Activitylog::insert($insertactivity);
					}

					Transaction::insert(array(
						"SendById" => (String) @$userinfo->_id,
						"SendByName" => @$userinfo->Name,
						"SendToId" => "aquantuo",
						"RecieveByName" => "Aquantuo",
						"Description" => "Amount deposited against product '{$item_name}', PackageId: {$package_id}",
						"Credit" => floatval($cost),
						"Debit" => "",
						"Status" => "completed",
						"TransactionType" => $TransactionType,
						"request_id"=> $requestid,
						"EnterOn" => new MongoDate(),
					));

					RequestCard::Insert([
						'request_id' => $requestid,
						'item_id' => '',
						'card_id' => $card,
						'type' => 'request',
						'payment' => $cost,
						'brand' => @$res['source']['brand'],
						'last4' => @$res['source']['last4'],
						'description' => "Payment done for item Title: '" . $item_name . "' Item Id:" . $package_id . ".",
						"EnterOn" => new MongoDate(),
					]);
					//$res = Reqhelper::update_status($requestid);
					Deliveryrequest::where($where)->update(['need_to_pay' => 0]);

					if ($type == 'delivery') {
						return Redirect::to("send-package-detail/{$requestid}")->with('success', "Success! Payment has been successfully completed.Aquantuo will proceed with next steps immediately.");
					} elseif ($type == 'buy_for_me') {
						return Redirect::to("buy-for-me-detail/{$requestid}")->with('success', "Success! Payment has been successfully completed.Aquantuo will proceed with next steps immediately.");
					} elseif ($type == 'online') {
						return Redirect::to("online-request-detail/{$requestid}")->with('success', "Success! Payment has been successfully completed.Aquantuo will proceed with next steps immediately.");
					}elseif ($type == 'local_delivery') {
						return Redirect::to("local-delivery-detaill/{$requestid}")->with('success', "Success! Payment has been successfully completed.Aquantuo will proceed with next steps immediately.");
					}

				}
			} else {
				return redirect::back()->with('danger', "Oops ! Something went wrong.");
			}
		}
		return redirect::back()->with('danger', "Oops ! Something went wrong.");
	}

}
