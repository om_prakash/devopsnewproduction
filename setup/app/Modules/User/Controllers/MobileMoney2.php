<?php namespace App\Modules\User\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Models\CityStateCountry;
use App\Http\Models\Deliveryrequest;
use App\Http\Models\Mobilemoney;
use App\Http\Models\Configuration;
use App\Http\Models\Transaction;
use App\Http\Models\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Input;
use MongoDate;
use MongoId;
use Redirect;
use Session;
use Validator;

class MobileMoney2 extends Controller {
	public function __construct() {
		if (!session()->has('UserId')) {
			return Redirect::to('login')->send();
		}

		$user = User::where(["_id" => session()->get('UserId')])
			->where(['delete_status' => 'yes'])
			->first();
		if (count($user) > 0) {
			Session::flush();
			return Redirect::to('login');
		}
	}

	public function getMobileno($id) {
		$config_data = Configuration::find(ConfigId);
		if (count($config_data) > 0) {
			$data['admin_msg'] = $config_data->message;
		}
		$data['req_amount'] = Deliveryrequest::where('_id', '=', $id)->select('_id', 'TotalCost', 'RequestType')->first();
		$data['mobileMoney'] = Mobilemoney::where('user_id', '=', Session::get('UserId'))->get();
		return view('User::Buy.mobile_list', $data);
	}

	public function addMobileNumber($id, Request $request) {

		$validation = Validator::make($request->all(), [
			'country_code' => 'required',
			'mobile_number' => 'required',
			'type' => 'required',
		]);

		if ($validation->fails()) {
			$validation->errors()->add('field', trans('Something is wrong'));
			return $validation->errors();
		} else {

			$insertData = [
				'user_id' => Session::get('UserId'),
				'country_code' => Input::get('country_code'),
				'mobile_number' => Input::get('mobile_number'),
				'alternet_moblie' => substr(Input::get('mobile_number'), -4),
				'mobile_type' => Input::get('type'),
				'EnterOn' => new MongoDate(),
			];

			$data = Mobilemoney::insert($insertData);
			if (count($data) > 0) {
				return redirect('pay-by-mobile-money/' . $id)->withSuccess('Success! Mobile number has been added successfully.');
			} else {
				return redirect()->back()->with('danger', 'Something went wrong.');
			}

		}
	}

	public function deleteMobile($id, $reqid) {
		$data = Mobilemoney::where('_id', '=', $id)->delete();
		if (count($data) > 0) {
			return redirect('pay-by-mobile-money/' . $reqid)->withSuccess('Success! Mobile number has been Deleted successfully.');
		} else {
			return redirect()->back()->with('danger', 'Something went wrong.');
		}
	}

	public function mobilePayment($reqid, $id) {
		$reqresponce = Deliveryrequest::where('_id', '=', $reqid)->first();

		$userres = User::where('_id', '=', $reqresponce->RequesterId)->first();

		$mobileResponce = Mobilemoney::where('_id', '=', $id)->first();
		$CityStateCountry = CityStateCountry::where(['type' => 'Country', 'Content' => 'Ghana'])->select('Content', 'CurrencyCode', 'CurrencySymbol', 'CurrencyRate')->first();
		//~ if ($reqresponce['need_to_pay'] > 0) {
			//~ $initialtotal = $reqresponce['need_to_pay'];
		//~ } else {
			//~ $initialtotal = $reqresponce['TotalCost'];
		//~ }
		// Above code wrong was total cost updated
		$initialtotal = $reqresponce['TotalCost']+ $reqresponce['after_update_difference'];
		

		if ($mobileResponce['country_code'] == '+233'  || $mobileResponce['country_code'] == '233') {
			$totalamount = $CityStateCountry['CurrencyRate'] * $initialtotal;
			$CurrencyRate = $CityStateCountry['CurrencyRate'];
			$Currency = 'GHS';
		} else {
			$totalamount = $initialtotal;
			$CurrencyRate = 1;
			$Currency = 'USD';
		}

		$url = "https://community.ipaygh.com/v1/mobile_agents_v2";

		$date = getdate();

		$invoiceId = $date[0];
		$body = [
			'invoice_id' => $invoiceId,
			'total' => $totalamount,
			'currency' => $Currency,
			//'pymt_instrument' => $mobileResponce['country_code'] . $mobileResponce['mobile_number'],
			'pymt_instrument' => $mobileResponce['mobile_number'],
			'ipn_url' => 'https://aquantuo.com/api/v7/retrievePayment',
			'wallet_issuer_hint' => $mobileResponce['mobile_type'],
			'extra_name' => $reqresponce['RequesterName'],
			'extra_mobile' => $mobileResponce['mobile_number'],
			'extra_email' => $userres['Email'],
			'description' => $reqresponce['PackageNumber'],
			'buyer_email' => $userres['Email'],
			'buyer_lastname' => $reqresponce['RequesterName'],
			'buyer_phone' => $mobileResponce['mobile_number'],
			'voucher_code' => '',
			'merchant_key' => '3f3f665c-f170-11e7-9871-f23c9170642f',
		];

		$client = new \GuzzleHttp\Client();
		$response = $client->request('POST', $url, ['form_params' => $body]);
		$responser_from_api = $response->getBody();

		$response_in_array = json_decode($responser_from_api);
		/*
			echo '---------------------We are passing these parameters.--------------------';
			echo '<pre>';

			print_r($response_in_array);
			echo '---------------------Api response.---------------------------------------';
			echo '<pre>';
		*/

		if (isset($response_in_array->success) && $response_in_array->success == 1) {

			if (isset($reqresponce->mobilemoney)) {
				$mobilemoney2 = [
					'_id' => (string) new MongoId(),
					'country_code' => $mobileResponce['country_code'],
					'mobile_number_money' => $mobileResponce['mobile_number'],
					'mobile_money_status' => 'awaiting_payment',
					'CurrencyRate' => $CurrencyRate,
					'mobile_money_amount' => $totalamount,
					'alternet_moblie' => $mobileResponce['alternet_moblie'],
					'mobile_moneyId' => $invoiceId,
					'mobile_type' => $mobileResponce['mobile_type'],
				];
				Deliveryrequest::where(['_id' => trim($reqid)])->push(['mobilemoney' => $mobilemoney2]);
			} else {
				$mobilemoney[] = [
					'_id' => (string) new MongoId(),
					'country_code' => $mobileResponce['country_code'],
					'mobile_number_money' => $mobileResponce['mobile_number'],
					'mobile_money_status' => 'awaiting_payment',
					'CurrencyRate' => $CurrencyRate,
					'mobile_money_amount' => $totalamount,
					'alternet_moblie' => $mobileResponce['alternet_moblie'],
					'mobile_moneyId' => $invoiceId,
					'mobile_type' => $mobileResponce['mobile_type'],
				];
				if ($reqresponce['Status'] == 'pending') {
					Deliveryrequest::where('_id', '=', $reqid)->update([
						'Status' => 'momo_initiated',
						'PaymentDate' => new MongoDate(),
						'mobilemoney' => $mobilemoney,
					]);
				}
			}
			if ($reqresponce['Status'] == 'pending') {
				if (isset($reqresponce->ProductList)) {
					$p_array = $reqresponce->ProductList;
					foreach ($p_array as $key => $value) {
						$p_array[$key]['status'] = 'momo_initiated';
						$p_array[$key]['PaymentStatus'] = 'yes';
					}
					$update['ProductList'] = $p_array;
					$update['Status'] = 'momo_initiated';
					
				}
			}
			$update['need_to_pay'] = 0;
			$finalresult = Deliveryrequest::where('_id', '=', $reqid)->update($update);

			/*Transaction*/
			Transaction::insert(array(
				"CurrencyRate" => @$CurrencyRate,
				"momo_id" => (int) $invoiceId,
				"SendById" => (String) Session::get('UserId'),
				"SendByName" => @$reqresponce['RequesterName'],
				"SendToId" => "aquantuo",
				"RecieveByName" => "Aquantuo",
				"Description" => "Amount deposited for Mobile Money product PackageId:" . $reqresponce['PackageNumber'],
				"Credit" => floatval($totalamount),
				"Debit" => "",
				"Status" => "pending",
				"request_id"=> $reqid,
				"TransactionType" => $reqresponce['RequestType'],
				"EnterOn" => new MongoDate(),
			));
			/*End Transaction*/

			if ($finalresult) {
				if ($reqresponce['RequestType'] == 'delivery') {
					return redirect('send-package-detail/' . $reqid)->withSuccess('Success! Payment has been requested. Please check your mobile device to approve/confirm payment.');
				} else if ($reqresponce['RequestType'] == 'online') {
					return redirect('online-request-detail/' . $reqid)->withSuccess('Success! Payment has been requested. Please check your mobile device to approve/confirm payment.');
				} else if ($reqresponce['RequestType'] == 'buy_for_me') {
					return redirect('buy-for-me-detail/' . $reqid)->withSuccess('Success! Payment has been requested. Please check your mobile device to approve/confirm payment.');
				}
			} else {
				return Redirect::to('pay-by-mobile-money/' . $reqid)->with(['danger' => 'Fail!  Something went wrong.']);
			}
		} else {
			return Redirect::to('pay-by-mobile-money/' . $reqid)->with(['danger' => $response_in_array->message]);
		}
	}

	public function mobilePayment_mtn($reqid, $id) {
		$responce1 = array('success' => 0, 'msg' => 'Oops! Something went wrong.');
		if ($reqid != '' && $id != '') {
			$reqresponce = Deliveryrequest::where('_id', '=', $reqid)->first();
			$userres = User::where('_id', '=', $reqresponce->RequesterId)->first();
			$mobileResponce = Mobilemoney::where('_id', '=', $id)->first();
			$CityStateCountry = CityStateCountry::where(['type' => 'Country', 'Content' => 'Ghana'])->select('Content', 'CurrencyCode', 'CurrencySymbol', 'CurrencyRate')->first();

			if ($reqresponce['need_to_pay'] > 0) {
				$initialtotal = $reqresponce['need_to_pay'];
			} else {
				$initialtotal = $reqresponce['TotalCost'];
			}

			if ($mobileResponce['country_code'] == '+233'  || $mobileResponce['country_code'] == '233') {
				$totalamount = $CityStateCountry['CurrencyRate'] * $initialtotal;
				$CurrencyRate = $CityStateCountry['CurrencyRate'];
				$Currency = 'GHS';
			} else {
				$totalamount = $initialtotal;
				$CurrencyRate = 1;
				$Currency = 'USD';
			}

			$url = "https://community.ipaygh.com/v1/mobile_agents_v2";
			$date = getdate();
			$invoiceId = $date[0];
			$body = [
				'invoice_id' => $invoiceId,
				'total' => $totalamount,
				'currency' => $Currency,
				//'pymt_instrument' => $mobileResponce['country_code'] . $mobileResponce['mobile_number'],
				'pymt_instrument' => $mobileResponce['mobile_number'],
				'ipn_url' => 'https://aquantuo.com/api/v7/retrievePayment',
				'wallet_issuer_hint' => $mobileResponce['mobile_type'],
				'extra_name' => $reqresponce['RequesterName'],
				'extra_mobile' => $mobileResponce['mobile_number'],
				'extra_email' => $userres['Email'],
				'description' => $reqresponce['PackageNumber'],
				'buyer_email' => $userres['Email'],
				'buyer_lastname' => $reqresponce['RequesterName'],
				'buyer_phone' => $mobileResponce['mobile_number'],
				'voucher_code' => '',
				'merchant_key' => '3f3f665c-f170-11e7-9871-f23c9170642f',
			];

			$client = new \GuzzleHttp\Client();
			$response = $client->request('POST', $url, ['form_params' => $body]);
			$responser_from_api = $response->getBody();
			$response_in_array = json_decode($responser_from_api);

			if ($response_in_array->success == 1) {
				if (isset($reqresponce->mobilemoney)) {
					$mobilemoney2 = [
						'_id' => (string) new MongoId(),
						'country_code' => $mobileResponce['country_code'],
						'mobile_number_money' => $mobileResponce['mobile_number'],
						'mobile_money_status' => 'awaiting_payment',
						'CurrencyRate' => $CurrencyRate,
						'mobile_money_amount' => $totalamount,
						'alternet_moblie' => $mobileResponce['alternet_moblie'],
						'mobile_moneyId' => $invoiceId,
						'mobile_type' => $mobileResponce['mobile_type'],
					];
					Deliveryrequest::where(['_id' => trim($reqid)])->push(['mobilemoney' => $mobilemoney2]);
				} else {
					$mobilemoney[] = [
						'_id' => (string) new MongoId(),
						'country_code' => $mobileResponce['country_code'],
						'mobile_number_money' => $mobileResponce['mobile_number'],
						'mobile_money_status' => 'awaiting_payment',
						'CurrencyRate' => $CurrencyRate,
						'mobile_money_amount' => $totalamount,
						'alternet_moblie' => $mobileResponce['alternet_moblie'],
						'mobile_moneyId' => $invoiceId,
						'mobile_type' => $mobileResponce['mobile_type'],
					];

					if ($reqresponce['Status'] == 'pending') {
						Deliveryrequest::where('_id', '=', $reqid)->update([
							'Status' => 'momo_initiated',
							'PaymentDate' => new MongoDate(),
							'mobilemoney' => $mobilemoney,
						]);
					}
				}

				if ($reqresponce['Status'] == 'pending') {
					if (isset($reqresponce->ProductList)) {
						$p_array = $reqresponce->ProductList;
						foreach ($p_array as $key => $value) {
							$p_array[$key]['status'] = 'momo_initiated';
							$p_array[$key]['PaymentStatus'] = 'yes';
						}
						$update['ProductList'] = $p_array;
						$update['Status'] = 'momo_initiated';
					}
				}

				$update['need_to_pay'] = 0;
				$finalresult = Deliveryrequest::where('_id', '=', $reqid)->update($update);

				/*Transaction*/
				Transaction::insert(array(
					"CurrencyRate" => @$CurrencyRate,
					"momo_id" => (int) $invoiceId,
					"SendById" => (String) Session::get('UserId'),
					"SendByName" => @$reqresponce['RequesterName'],
					"SendToId" => "aquantuo",
					"RecieveByName" => "Aquantuo",
					"Description" => "Amount deposited for Mobile Money product PackageId:" . $reqresponce['PackageNumber'],
					"Credit" => floatval($totalamount),
					"Debit" => "",
					"Status" => "pending",
					"request_id"=> $reqid,
					"TransactionType" => $reqresponce['RequestType'],
					"EnterOn" => new MongoDate(),
				));
				/*End Transaction*/

				if ($finalresult) {
					if ($reqresponce['RequestType'] == 'delivery') {
						//return redirect('send-package-detail/' . $reqid)->withSuccess('Success! Payment has been requested. Please check your mobile device to approve/confirm payment.');
						$msg = "Success! Payment has been requested. Please check your mobile device to approve/confirm payment.";
						$url = 'send-package-detail/' . $reqid;

					} else if ($reqresponce['RequestType'] == 'online') {
						//return redirect('online-request-detail/' . $reqid)->withSuccess('Success! Payment has been requested. Please check your mobile device to approve/confirm payment.');
						$msg = "Success! Payment has been requested. Please check your mobile device to approve/confirm payment.";
						$url = 'online-request-detail/' . $reqid;
					} else if ($reqresponce['RequestType'] == 'buy_for_me') {
						//return redirect('buy-for-me-detail/' . $reqid)->withSuccess('Success! Payment has been requested. Please check your mobile device to approve/confirm payment.');
						$msg = "Success! Payment has been requested. Please check your mobile device to approve/confirm payment.";
						$url = 'buy-for-me-detail/' . $reqid;
					}
					$responce1 = ['success'=>1,'msg'=> $msg ,'url'=>$url];

				} else {
					//return Redirect::to('pay-by-mobile-money/' . $reqid)->with(['danger' => 'Fail!  Something went wrong.']);
				}
			} else {
				//return Redirect::to('pay-by-mobile-money/' . $reqid)->with(['danger' => 'Fail!  Something went wrong.']);
			}
		}
		return response()->json($responce1);
	}

	public function vouchermobilePayment($reqid) {
		$reqresponce = Deliveryrequest::where('_id', '=', $reqid)->first();
		$userres = User::where('_id', '=', $reqresponce->RequesterId)->first();
		$mobileResponce = Mobilemoney::where('_id', '=', Input::get('voucherid'))->first();

		$CityStateCountry = CityStateCountry::where(['type' => 'Country', 'Content' => 'Ghana'])->select('Content', 'CurrencyCode', 'CurrencySymbol', 'CurrencyRate')->first();
		if ($reqresponce['need_to_pay'] > 0) {
			$initialtotal = $reqresponce['need_to_pay'];
		} else {
			$initialtotal = $reqresponce['TotalCost'];
		}

		if ($mobileResponce['country_code'] == '+233' || $mobileResponce['country_code'] == '233') {
			$totalamount = $CityStateCountry['CurrencyRate'] * $initialtotal;
			$CurrencyRate = $CityStateCountry['CurrencyRate'];
			$Currency = 'GHS';
		} else {
			$totalamount = $initialtotal;
			$CurrencyRate = 1;
			$Currency = 'USD';
		}

		$url = "https://community.ipaygh.com/v1/mobile_agents_v2";

		$date = getdate();
		$invoiceId = $date[0];
		$body = [
			'invoice_id' => $invoiceId,
			'total' => $totalamount,
			'currency' => $Currency,
			//'pymt_instrument' => $mobileResponce['country_code'] . $mobileResponce['mobile_number'],
			'pymt_instrument' => $mobileResponce['mobile_number'],
			'wallet_issuer_hint' => $mobileResponce['mobile_type'],
			'extra_name' => $reqresponce['RequesterName'],
			'ipn_url' => 'apis.aquantuo.com/api/v6/retrievePayment',
			'extra_mobile' => $mobileResponce['mobile_number'],
			'extra_email' => $userres['Email'],
			'description' => $reqresponce['PackageNumber'],
			'voucher_code' => Input::get('Voucher'),
			'merchant_key' => '3f3f665c-f170-11e7-9871-f23c9170642f',
		];

		$client = new \GuzzleHttp\Client();
		$response = $client->request('POST', $url, ['form_params' => $body]);
		$responser_from_api = $response->getBody();

		$response_in_array = json_decode($responser_from_api);
		if ($response_in_array->success == 1) {

			if (isset($reqresponce->mobilemoney)) {
				$mobilemoney2 = [
					'_id' => (string) new MongoId(),
					'country_code' => $mobileResponce['country_code'],
					'mobile_number_money' => $mobileResponce['mobile_number'],
					'mobile_money_status' => 'awaiting_payment',
					'CurrencyRate' => $CurrencyRate,
					'mobile_money_amount' => $totalamount,
					'alternet_moblie' => $mobileResponce['alternet_moblie'],
					'mobile_moneyId' => $invoiceId,
					'mobile_type' => $mobileResponce['mobile_type'],
				];

				Deliveryrequest::where(['_id' => trim($reqid)])->push(['mobilemoney' => $mobilemoney2]);
			} else {
				$mobilemoney[] = [
					'_id' => (string) new MongoId(),
					'country_code' => $mobileResponce['country_code'],
					'mobile_number_money' => $mobileResponce['mobile_number'],
					'mobile_money_status' => 'awaiting_payment',
					'CurrencyRate' => $CurrencyRate,
					'mobile_money_amount' => $totalamount,
					'alternet_moblie' => $mobileResponce['alternet_moblie'],
					'mobile_moneyId' => $invoiceId,
					'mobile_type' => $mobileResponce['mobile_type'],
				];
				if ($reqresponce['Status'] == 'pending') {
					$updateData = Deliveryrequest::where('_id', '=', $reqid)->update([
						'PaymentStatus' => 'capture',
						'Status' => 'momo_initiated',
						'PaymentDate' => new MongoDate(),
						'mobilemoney' => $mobilemoney,
					]);
				}
			}

			Transaction::insert(array(
				"CurrencyRate" => @$CurrencyRate,
				"SendById" => (String) Session::get('UserId'),
				"momo_id" => (int) $invoiceId,
				"SendByName" => @$reqresponce['RequesterName'],
				"SendToId" => "aquantuo",
				"RecieveByName" => "Aquantuo",
				"Description" => "Amount deposited for Mobile Money product PackageId:" . $reqresponce['PackageNumber'],
				"Credit" => floatval($totalamount),
				"Debit" => "",
				"Status" => "pending",
				"request_id"=> $reqid,
				"TransactionType" => $reqresponce['RequestType'],
				"EnterOn" => new MongoDate(),
			));

			if ($reqresponce['Status'] == 'pending') {
				if (isset($reqresponce->ProductList)) {
					$p_array = $reqresponce->ProductList;
					foreach ($p_array as $key => $value) {
						$p_array[$key]['status'] = 'momo_initiated';
						$p_array[$key]['PaymentStatus'] = 'yes';
					}
					$update['ProductList'] = $p_array;
				}
			}
			$update['need_to_pay'] = 0;
			$update['Status'] = 'momo_initiated';
			
			$finalresult = Deliveryrequest::where('_id', '=', $reqid)->update($update);
			if ($finalresult) {
				if ($reqresponce['RequestType'] == 'delivery') {
					return redirect('send-package-detail/' . $reqid)->withSuccess('Success! Payment has been requested. Please check your mobile device to approve/confirm payment.');
				} else if ($reqresponce['RequestType'] == 'online') {
					return redirect('online-request-detail/' . $reqid)->withSuccess('Success! Payment has been requested. Please check your mobile device to approve/confirm payment.');
				} else if ($reqresponce['RequestType'] == 'buy_for_me') {
					return redirect('buy-for-me-detail/' . $reqid)->withSuccess('Success! Payment has been requested. Please check your mobile device to approve/confirm payment.');
				}
			} else {
				return Redirect::to('pay-by-mobile-money/' . $reqid)->with(['danger' => 'Fail!  Something went wrong.']);
			}
		} else {
			return Redirect::to('pay-by-mobile-money/' . $reqid)->with(['danger' => 'Fail!  Something went wrong.']);
		}
	}

}
