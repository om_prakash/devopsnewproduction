<?php namespace App\Modules\User\Controllers;
use App\Http\Controllers\Controller;
use App\Http\Models\Activitylog;
use App\Http\Models\Deliveryrequest;
use App\Http\Models\Notification;
use App\Http\Models\SendMail;
use App\Http\Models\Setting;
use App\Http\Models\User;
use App\Library\Buinessdays;
use App\Library\SapBuinessdays;
use App\Library\Notification\Pushnotification;
use App\Library\Reqhelper;
use Illuminate\Http\Request;
use Input;
use MongoDate;
use MongoId;
use Session;
use App\Library\Notify;
use App\Library\NewEmail;
use App\Http\Models\Configuration;

class NewTransporter extends Controller {

	public function request_accept_by_transporter($productid, $requestId, Request $request) {
		$response = array("success" => 0, "msg" => "Something went wrong.");
		$id = session::get('UserId');

		$where = [
			"_id" => $requestId,
			"ProductList" => [
				'$elemMatch' => [
					'_id' => $productid,
					'status' => 'ready'
				]
			]
		];

		$requestid = Deliveryrequest::where($where)->select('RequesterId', 'Status', 'RequestType', 'ProductTitle', 'PackageId', 'PackageNumber', 'shippingCost', 'TotalCost', 'PickupFullAddress', 'DeliveryFullAddress', 'ProductList')->first();

		if (count($requestid) > 0) {
			$requesterData = User::where(array('_id' => $requestid->RequesterId))->
				select('FirstName', 'LastName', 'Email', 'EmailStatus', 'NoficationStatus', 'NotificationId', 'DeviceType', 'Name')->first();

			$transporterData = User::where(array('_id' => $id))->select('FirstName', 'LastName', 'Email', 'EmailStatus', 'NoficationStatus', 'NotificationId', 'DeviceType', 'Name')->first();

			if (count($requesterData) > 0 && count($transporterData) > 0) {

				$update_array = $requestid->ProductList;

				foreach ($requestid->ProductList as $key => $value) {
					if ($value['_id'] == $productid) {
						// Expected delivery date.
						$startdate = strtotime(date("Y-m-d"));
						$Buinessdays = new SapBuinessdays();
						$result = $Buinessdays->getWorkingDays($startdate);

						if ($update_array[$key]['travelMode'] == 'air') {
							$update_array[$key]['ExpectedDate'] = new Mongodate(strtotime('+' . $result . 'days'));
						} else {
							$update_array[$key]['ExpectedDate'] = new Mongodate(strtotime('+ 63 days'));
						}
						
						$update_array[$key]['status'] = 'accepted';
						$update_array[$key]['tpid'] = $id;
						$update_array[$key]['tpName'] = $transporterData->Name;
						
						$item_title = $update_array[$key]['product_name'];
						$packageid = $update_array[$key]['package_id'];
						$total = $update_array[$key]['after_update'];
						$shippingCost = $update_array[$key]['shippingCost'];
					}
				}

				$updateData = Deliveryrequest::where(array('_id' => new MongoId($requestId)))->update(['ProductList' => $update_array]);
				/* Activity Log for Accept by transporter */
				$insertactivity = [
					'request_id' => $requestid->RequesterId,
					'request_type' => $requestid->RequestType,
					'PackageNumber' => $requestid->PackageNumber,
					'item_id' => $productid,
					'package_id' => $packageid,
					'item_name' => $item_title,
					'log_type' => 'request',
					'message' => 'Item accepted.',
					'status' => 'accepted',
					'action_user_id'=> Session::get('UserId'),
					'EnterOn' => new MongoDate(),
				];
				Activitylog::insert($insertactivity);

				if ($updateData) {
					Reqhelper::update_status2($requestId);
					//email to transporter
					$cron_mail = [
						"USERNAME" => ucfirst($transporterData->Name),
						"PACKAGETITLE" => ucfirst($item_title),
						"PACKAGEID" => $packageid,
						"SOURCE" => $requestid->PickupFullAddress,
						"DESTINATION" => $requestid->DeliveryFullAddress,
						"PACKAGENUMBER" => $packageid,
						"SHIPPINGCOST" => $shippingCost,
						"TOTALCOST" => $total,
						'email_id' => '5694ce6e5509251cd67773ed',
						'email' => $transporterData->Email,
						'status' => 'ready',
					];
					SendMail::insert($cron_mail);

					//email to requester
					$cron_mail2 = [
						"USERNAME" => ucfirst($requesterData->Name),
						"PACKAGETITLE" => ucfirst($item_title),
						"PACKAGEID" => $packageid,
						"SOURCE" => $requestid->PickupFullAddress,
						"DESTINATION" => $requestid->DeliveryFullAddress,
						"PACKAGENUMBER" => $packageid,
						"SHIPPINGCOST" => $shippingCost,
						"TOTALCOST" => $total,
						'email_id' => '57d7d4b37ac6f69c158b4569',
						'email' => $requesterData->Email,
						'status' => 'ready',
					];

					SendMail::insert($cron_mail2);
					//email to admin
					$setting = Setting::find('563b0e31e4b03271a097e1ca');
					if (count($setting) > 0) {
						$Email = new NewEmail();
						$Email->send_mail('56ab46f95509251cd67773f3', [
							"to" => $setting->SupportEmail,
							"replace" => [
								"[TRANSPORTERNAME]" => $transporterData->Name,
								"[PACKAGETITLE]" => ucfirst($item_title),
								"[PACKAGEID]" => $packageid,
								"[SOURCE]" => $requestid->PickupFullAddress,
								"[DESTINATION]" => $requestid->DeliveryFullAddress,
							],
						]);
					}

					//Notification to requester

					if ($requesterData->NoficationStatus == "on" && !empty($requesterData->NotificationId)) {
						// $Notification = new Pushnotification();
						$Notification = new Notify();
						$Notification->setValue('title', trans('lang.SEND_ACCEPT_TITLE'));
						$Notification->setValue('message', sprintf(trans('lang.SEND_ACCEPT_MSG'), $transporterData->Name, $item_title));
						$Notification->setValue('type', 'requester_delivery_detail');
						$Notification->setValue('locationkey', $requestId);
						$Notification->add_user($requesterData->NotificationId, $requesterData->DeviceType);
						$Notification->fire();
					}
					//insertNotification

					Notification::Insert([
						array(
							"NotificationTitle" => trans('lang.SEND_ACCEPT_TITLE'),
							"NotificationShortMessage" => 'Request accepted successfully.',
							"NotificationMessage" => sprintf(trans('lang.SEND_ACCEPT_MSG'), $transporterData->Name, $item_title),
							"NotificationType" => "requester_delivery_detail",
							"NotificationUserId" => array(new MongoId($requesterData->_id)),
							"Date" => new MongoDate(),
							"GroupTo" => "User",
						),
						array(
							"NotificationTitle" => 'Request was accepted',
							"NotificationMessage" => sprintf(trans('lang.SEND_ACCEPT_MSG_ADMIN'), $transporterData->Name, $requestid->PickupFullAddress, $requestid->DeliveryFullAddress),
							"NotificationUserId" => array(),
							"NotificationReadStatus" => 0,
							"location" => "requester_delivery_detail",
							"locationkey" => (string) $requestId,
							"Date" => new MongoDate(),
							"GroupTo" => "Admin",
						),
					]);

					if (count($updateData) > 0) {
						$response['success'] = 1;
						$response['status'] = 'accepted';
						$response['msg'] = "<strong>Success!</strong> Request has been accepted successfully.";
					}
				}

			}
		}

		echo json_encode($response);

	}

	public function localrequest_accept_by_transporter($productid, $requestId, Request $request) {
		$response = array("success" => 0, "msg" => "Something went wrong.");
		$id = session::get('UserId');

		$where = [
			"_id" => $requestId,
			"ProductList" => ['$elemMatch' => [
				'_id' => $productid,
				'status' => 'assign',
			],

			],
		];

		$requestid = Deliveryrequest::where($where)->select('RequesterId', 'Status', 'RequestType', 'ProductTitle', 'PackageId', 'PackageNumber', 'shippingCost', 'TotalCost', 'PickupFullAddress', 'DeliveryFullAddress', 'ProductList','AreaCharges')->first();

		//Calculation
		$configurationdata = Configuration::where(['_id' => '5673e33e6734c4f874685c84'])->first();
		$totalItemCount = count($requestid->ProductList);
		$averageAreaCharge= number_format($requestid->AreaCharges / $totalItemCount,2) ;

		foreach ($requestid->ProductList as $key2 => $value2) {
			if($value2['_id'] == $productid){
				$itemCost = $value2['after_update'] + $averageAreaCharge;
			}
			
		}

		
		$aquantuoFess = ($itemCost / 100 ) * $configurationdata->aquantuo_fees_ld;
		$transporterFess = $itemCost - $aquantuoFess;
		//endCalculation



		if (count($requestid) > 0) {
			$requesterData = User::where(array('_id' => $requestid->RequesterId))->
				select('FirstName', 'LastName', 'Email', 'EmailStatus', 'NoficationStatus', 'NotificationId', 'DeviceType', 'Name')->first();

			$transporterData = User::where(array('_id' => $id))->select('FirstName', 'LastName', 'Email', 'EmailStatus', 'NoficationStatus', 'NotificationId' . 'DeviceType', 'Name')->first();

			if (count($requesterData) > 0 && count($transporterData) > 0) {

				$update_array = $requestid->ProductList;

				foreach ($requestid->ProductList as $key => $value) {
					if ($value['_id'] == $productid) {
						$update_array[$key]['status'] = 'accepted';
						$update_array[$key]['aq_fees'] = $aquantuoFess;

						$item_title = $update_array[$key]['product_name'];
						$packageid = $update_array[$key]['package_id'];
						$total = $update_array[$key]['after_update'];
						$shippingCost = $update_array[$key]['shippingCost'];

					}
				}

				$updateData = Deliveryrequest::where(array('_id' => new MongoId($requestId)))->update(['ProductList' => $update_array]);
				/* Activity Log for Accept by transporter */
				$insertactivity = [
					'request_id' => $requestid->RequesterId,
					'request_type' => $requestid->RequestType,
					'PackageNumber' => $requestid->PackageNumber,
					'item_id' => $productid,
					'package_id' => $packageid,
					'item_name' => $item_title,
					'log_type' => 'request',
					'message' => 'Item accepted.',
					'status' => 'accepted',
					'action_user_id'=> Session::get('UserId'),
					'EnterOn' => new MongoDate(),
				];
				Activitylog::insert($insertactivity);

				if ($updateData) {
					
					Reqhelper::update_status2($requestId);
					$Email = new NewEmail();
					//email to transporter
					/*$cron_mail = [
						"USERNAME" => ucfirst($transporterData->Name),
						"PACKAGETITLE" => ucfirst($item_title),
						"PACKAGEID" => $packageid,
						"SOURCE" => $requestid->PickupFullAddress,
						"DESTINATION" => $requestid->DeliveryFullAddress,
						"PACKAGENUMBER" => $packageid,
						"SHIPPINGCOST" => $shippingCost,
						"TOTALCOST" => $total,
						'email_id' => '5694ce6e5509251cd67773ed',
						'email' => $transporterData->Email,
						'status' => 'ready',
					];
					SendMail::insert($cron_mail);*/

					$Email->send_mail('5694ce6e5509251cd67773ed', [
						"to" => $transporterData->Email,
						"replace" => [
							"[USERNAME]" => ucfirst($transporterData->Name),
							"[PACKAGETITLE]" => ucfirst($item_title),
							"[PACKAGEID]" => $packageid,
							"[SOURCE]" => $requestid->PickupFullAddress,
							"[DESTINATION]" => $requestid->DeliveryFullAddress,
							"[PACKAGENUMBER]" => $packageid,
							"[SHIPPINGCOST]" => $shippingCost,
							"[TOTALCOST]" => $total,
							"[REQUESTER]" => ucfirst($requesterData->Name)
						],
					]);

					//email to requester
					/*$cron_mail2 = [
						"USERNAME" => ucfirst($requesterData->Name),
						"PACKAGETITLE" => ucfirst($item_title),
						"PACKAGEID" => $packageid,
						"SOURCE" => $requestid->PickupFullAddress,
						"DESTINATION" => $requestid->DeliveryFullAddress,
						"PACKAGENUMBER" => $packageid,
						"SHIPPINGCOST" => $shippingCost,
						"TOTALCOST" => $total,
						'email_id' => '57d7d4b37ac6f69c158b4569',
						'email' => $requesterData->Email,
						'status' => 'ready',
					];

					SendMail::insert($cron_mail2);*/

					$Email->send_mail('5d98660f360d5c0f86530a2c', [
						"to" => $requesterData->Email,
						"replace" => [
							"[USERNAME]" => ucfirst($requesterData->Name),
							"[PACKAGETITLE]" => ucfirst($item_title),
							"[PACKAGEID]" => $packageid,
							"[SOURCE]" => $requestid->PickupFullAddress,
							"[DESTINATION]" => $requestid->DeliveryFullAddress,
							"[PACKAGENUMBER]" => $packageid,
							"[SHIPPINGCOST]" => $shippingCost,
							"[TOTALCOST]" => $total,
						],
					]);

					//email to admin
					$setting = Setting::find('563b0e31e4b03271a097e1ca');
					if (count($setting) > 0) {
						/*send_mail('56ab46f95509251cd67773f3', [
							"to" => $setting->SupportEmail,
							"replace" => [
								"[TRANSPORTERNAME]" => $transporterData->Name,
								"[PACKAGETITLE]" => ucfirst($item_title),
								"[PACKAGEID]" => $packageid,
								"[SOURCE]" => $requestid->PickupFullAddress,
								"[DESTINATION]" => $requestid->DeliveryFullAddress,
							],
						]);*/

						$Email->send_mail('56ab46f95509251cd67773f3', [
							"to" => $setting->SupportEmail,
							"replace" => [
								"[TRANSPORTERNAME]" => $transporterData->Name,
								"[PACKAGETITLE]" => ucfirst($item_title),
								"[PACKAGEID]" => $packageid,
								"[SOURCE]" => $requestid->PickupFullAddress,
								"[DESTINATION]" => $requestid->DeliveryFullAddress,
							],
						]);
					}

					//Notification to requester

					if ($requesterData->NoficationStatus == "on" && !empty($requesterData->NotificationId)) {
						$Notification = new Notify();
						$Notification->setValue('title', trans('lang.SEND_ACCEPT_TITLE'));
						$Notification->setValue('message', sprintf(trans('lang.SEND_ACCEPT_MSG'), $transporterData->Name, $item_title));
						$Notification->setValue('type', 'LOCAL_DELIVERY_USER');	// request_detail
						$Notification->setValue('locationkey', $requestId);
						$Notification->add_user($requesterData->NotificationId, $requesterData->DeviceType);
						$Notification->fire();
					}
					//insertNotification

					Notification::Insert([
						array(
							"NotificationTitle" => trans('lang.SEND_ACCEPT_TITLE'),
							"NotificationShortMessage" => 'Request accepted successfully.',
							"NotificationMessage" => sprintf(trans('lang.SEND_ACCEPT_MSG'), $transporterData->Name, $item_title),
							"NotificationType" => "LOCAL_DELIVERY_USER",
							"NotificationUserId" => array(new MongoId($requesterData->_id)),
							"Date" => new MongoDate(),
							"GroupTo" => "User",
						),
						array(
							"NotificationTitle" => 'Request was accepted',
							"NotificationMessage" => sprintf(trans('lang.SEND_ACCEPT_MSG_ADMIN'), $transporterData->Name, $requestid->PickupFullAddress, $requestid->DeliveryFullAddress),
							"NotificationUserId" => array(),
							"NotificationReadStatus" => 0,
							"location" => "request_detail",
							"locationkey" => (string) $requestId,
							"Date" => new MongoDate(),
							"GroupTo" => "Admin",
						),
					]);

					if (count($updateData) > 0) {
						$response['success'] = 1;
						$response['status'] = 'accepted';
						$response['msg'] = "<strong>Success!</strong> Request has been accepted successfully.";
					}
				}

			}
		}

		echo json_encode($response);

	}

	public function cancel_request($requestId, $product_id, Request $request) {

		$response = array("success" => 0, "msg" => "Oops! Something went wrong.");
		$id = session::get('UserId');
		$where = array("ProductList" => array('$elemMatch' => array('_id' => $product_id)));
		$requestid = Deliveryrequest::where(array('_id' => $requestId))->where($where)
			->select('RequesterId', 'ProductTitle', 'status', 'PackageId', 'PickupFullAddress', 'DeliveryFullAddress', 'PackageNumber', 'shippingCost', 'TotalCost', 'ProductList.$')->first();

		if (count($requestid) > 0) {

			if (!in_array($requestid->ProductList[0]['status'], ['accepted', 'out_for_pickup', 'assign'])) {
				$response['msg'] = 'You can cancel request only accepted or out for pickup.';
			} else {
				$requesterData = User::where(array('_id' => $requestid->RequesterId))->
					select('FirstName', 'LastName', 'Email', 'EmailStatus', 'NoficationStatus', 'NotificationId', 'DeviceType')->first();

				$transporterData = User::where(array('_id' => $id))->select('FirstName', 'LastName', 'Email', 'EmailStatus', 'NoficationStatus', 'NotificationId' . 'DeviceType', 'Name')->first();

				if (count($requesterData) > 0 && count($transporterData)) {
					$update['ProductList.$.status'] = 'ready';	// purchased
					$update['ProductList.$.tpid'] = '';
					$update['ProductList.$.tpName'] = '';
					// $updateData = Deliveryrequest::where($where)->update($update);
					$updateData = Deliveryrequest::where(array('_id' => $requestId))->where($where)->update($update);

					$response['success'] = 1;
					$response['msg'] = "Success! Request has been canceled successfully.";

					//email to transporter
					if ($transporterData->EmailStatus == "on") {
						/*send_mail('57e8c112e4b01d01c916d24a', [
							"to" => $transporterData->Email,
							"replace" => [
								"[USERNAME]" => $transporterData->Name,
								"[PACKAGETITLE]" => ucfirst($requestid->ProductTitle),
								"[PACKAGEID]" => $requestid->PackageId,
								"[SOURCE]" => $requestid->PickupFullAddress,
								"[DESTINATION]" => $requestid->DeliveryFullAddress,
								"[PACKAGENUMBER]" => $requestid->PackageNumber,
								"[SHIPPINGCOST]" => $requestid->shippingCost,
								"[TOTALCOST]" => $requestid->TotalCost,
							],
						]);*/
					}
					//email to requester
					if ($requesterData->EmailStatus == "on") {
						send_mail('57e8c3dae4b01d01c916d24c', [
							"to" => $requesterData->Email,
							"replace" => [
								"[USERNAME]" => $requesterData->FirstName . ' ' . $requesterData->LastName,
								"[PACKAGETITLE]" => ucfirst($requestid->ProductTitle),
								"[PACKAGEID]" => $requestid->PackageId,
								"[SOURCE]" => $requestid->PickupFullAddress,
								"[DESTINATION]" => $requestid->DeliveryFullAddress,
								"[PACKAGENUMBER]" => $requestid->PackageNumber,
								"[SHIPPINGCOST]" => $requestid->shippingCost,
								"[TOTALCOST]" => $requestid->TotalCost,
							],
						]);
					}

					//email to admin
					$setting = Setting::find('563b0e31e4b03271a097e1ca');

					if (count($setting) > 0) {
						/*send_mail('57e8c3f5e4b01d01c916d24e', [
							"to" => $setting->SupportEmail,
							"replace" => [
								"[USERNAME]" => $transporterData->Name,
								"[PACKAGETITLE]" => ucfirst($requestid->ProductTitle),
								"[PACKAGEID]" => $requestid->PackageNumber,
								"[TRANSPORTERNAME]" => ucfirst($transporterData->Name),
								"[DESTINATION]" => ucfirst($requestid->DeliveryFullAddress),
							],
						]);*/
					}

					$Notification = new Notify();
					/* Activity Log send a package cancel */
					$where = [
						"ProductList" => ['$elemMatch' => [
							'_id' => $product_id,
						]],
					];
					$activityLog = Deliveryrequest::where('_id', '=', $requestId)->where($where)->select('ProductList.$', 'RequestType', 'PackageNumber')->first();
					$insertactivity = [
						'request_id' => $requestId,
						'request_type' => 'delivery',
						'PackageNumber' => $activityLog->PackageNumber,
						'item_id' => $product_id,
						'package_id' => $activityLog['ProductList'][0]['package_id'],
						'item_name' => $activityLog['ProductList'][0]['product_name'],
						'log_type' => 'request',
						'message' => 'Request has been cancel.',
						'status' => 'cancel',
						'action_user_id'=> Session::get('UserId'),
						'EnterOn' => new MongoDate(),
					];

					//Activitylog::insert($insertactivity);

					//Notification to requester
					if ($requesterData->NoficationStatus == "on" && !empty($requesterData->NotificationId)) {
						$Notification->setValue('title', 'Request canceled');
						$Notification->setValue('message', sprintf('The request of send a package title: "%s", ID: %s has been canceled by transporter.', $requestid->ProductTitle, $requestid->PackageNumber));
						$Notification->setValue('type', 'requester_delivery_detail');	// request_detail
						$Notification->setValue('locationkey', $requestId);
						$Notification->add_user($requesterData->NotificationId, $requesterData->DeviceType);
						$Notification->fire();
					}
					//insertNotification
					/*Notification::Insert([
						array(
							"NotificationTitle" => 'Request canceled',
							"NotificationMessage" => sprintf('The request of send a package title: "%s", ID: %s has been canceled by transporter.', $requestid->ProductTitle, $requestid->PackageNumber),
							"NotificationType" => "request_detail",
							"NotificationUserId" => array(new MongoId($requesterData->_id)),
							"Date" => new MongoDate(),
							"GroupTo" => "User",
						),
						array(
							"NotificationTitle" => 'Request canceled',
							"NotificationMessage" => sprintf('The request of send a package title: "%s", ID: %s has been canceled by transporter.', $requestid->ProductTitle, $requestid->PackageNumber),
							"NotificationUserId" => array(),
							"NotificationReadStatus" => 0,
							"location" => "request_detail",
							"locationkey" => (string) $requestId,
							"Date" => new MongoDate(),
							"GroupTo" => "Admin",
						),
					]);*/
				}
			}
		}

		echo json_encode($response);
	}

	public function pickup_request($requestId, $product_id, Request $request) {
		$response = array("success" => 0, "msg" => "Something went wrong.");
		$where = array("_id" => $requestId, "ProductList" => array('$elemMatch' => array('_id' => $product_id)));
		$requestid = Deliveryrequest::where($where)
			->select('RequesterId', 'ProductTitle', 'Status', 'ProductList', 'PackageId', 'PickupFullAddress', 'DeliveryFullAddress', 'PackageNumber', 'shippingCost', 'TotalCost')->first();

		if (count($requestid) > 0) {

			$requesterData = User::where(array('_id' => $requestid->RequesterId))->
				select('FirstName', 'LastName', 'Email', 'EmailStatus', 'NoficationStatus', 'NotificationId', 'DeviceType', 'Name')->first();

			$transporterData = User::where(array('_id' => session::get('UserId')))->select('FirstName', 'LastName', 'Email', 'EmailStatus', 'NoficationStatus', 'NotificationId', 'DeviceType', 'Name')->first();
			if (count($requesterData) > 0 && count($transporterData)) {

				$update_array = $requestid->ProductList;
				foreach ($requestid->ProductList as $key => $value) {
					if ($value['_id'] == $product_id) {

						if ($value['travelMode'] == 'air') {
							$startdate = strtotime(date("Y-m-d"));
							$Buinessdays = new Buinessdays();
							$result = $Buinessdays->getWorkingDays($startdate);
							$ExpectedDate = new Mongodate(strtotime('+' . $result . 'days'));
						} elseif ($value['travelMode'] == 'local_ghana') {
							$ExpectedDate = new Mongodate();
						} else {
							$ExpectedDate = new Mongodate(strtotime('+ 13 days'));
						}

						$update_array[$key]['status'] = 'out_for_pickup';
						$update_array[$key]['ExpectedDate'] = $ExpectedDate;

						$item_title = $update_array[$key]['product_name'];
						$packageid = $update_array[$key]['package_id'];
						$total = $update_array[$key]['after_update'];
						$shippingCost = $update_array[$key]['shippingCost'];
					}

				}

				$updateData = Deliveryrequest::where($where)->update(['ProductList' => $update_array]);
				if ($updateData) {
					Reqhelper::update_status2($requestId);
					//email to transporter
					if ($transporterData->EmailStatus == "on") {
						/*insert mail*/
						$cron_mail = [
							"USERNAME" => ucfirst($transporterData->Name),
							"PACKAGETITLE" => ucfirst($item_title),
							"PACKAGEID" => $packageid,
							"SOURCE" => $requestid->PickupFullAddress,
							"DESTINATION" => $requestid->DeliveryFullAddress,
							"PACKAGENUMBER" => $packageid,
							"SHIPPINGCOST" => $shippingCost,
							"TOTALCOST" => $total,
							'email_id' => '5694cf3c5509251cd67773ee',
							'email' => $transporterData->Email,
							'status' => 'ready',
							'REQUESTER' => ucfirst($requesterData->Name),
						];
						SendMail::insert($cron_mail);
						/*end ins*/
					}

					//email to requester
					if ($requesterData->EmailStatus == "on") {
						/*insert mail*/
						$cron_mail2 = [
							"USERNAME" => ucfirst($requesterData->Name),
							"PACKAGETITLE" => ucfirst($item_title),
							"PACKAGEID" => $packageid,
							"SOURCE" => $requestid->PickupFullAddress,
							"DESTINATION" => $requestid->DeliveryFullAddress,
							"PACKAGENUMBER" => $packageid,
							"SHIPPINGCOST" => $shippingCost,
							"TOTALCOST" => $total,
							'email_id' => '5694cb805509251cd67773ec',
							'email' => $requesterData->Email,
							'status' => 'ready',
						];

						SendMail::insert($cron_mail2);
						/*end ins*/
					}
					//email to admin

					//Notification to requester
					if ($requesterData->NoficationStatus == "on" && !empty($requesterData->NotificationId)) {
						$Notification = new Notify();
						$Notification->setValue('title', trans('lang.SEND_OUTFORPICKUP_TITLE'));
						$Notification->setValue('message', sprintf(trans('lang.SEND_OUTFORPICKUP_MSG')));
						$Notification->setValue('type', 'requester_delivery_detail');
						$Notification->setValue('locationkey', $requestId);
						$Notification->add_user($requesterData->NotificationId, $requesterData->DeviceType);
						$Notification->fire();

						$cron_noti = [
							"title" => trans('lang.SEND_OUTFORPICKUP_TITLE'),
							"message" => trans('lang.SEND_OUTFORPICKUP_MSG'),
							"type" => 'requester_delivery_detail',
							"locationkey" => $requestId,
							"NotificationId" => $requesterData->NotificationId,
							"DeviceType" => $requesterData->DeviceType,
							'status' => 'ready',
							'by_mean' => 'notification',
						];
						SendMail::insert($cron_noti);
					}

					/* Activity Log send a package out for pickup */

					$insertactivity = [
						'request_id' => $requestid->_id,
						'request_type' => 'delivery',
						'PackageNumber' => $requestid->PackageNumber,
						'item_id' => $product_id,
						'package_id' => $packageid,
						'item_name' => $item_title,
						'log_type' => 'request',
						// 'message' => 'Item is in destination country going through customs and sorting.',
						'message' => 'Item is being picked up from originating country.',
						'status' => 'out_for_pickup',
						'action_user_id'=> Session::get('UserId'),
						'EnterOn' => new MongoDate(),
					];

					Activitylog::insert($insertactivity);
					//insertNotification
					Notification::Insert([
						array(
							"NotificationTitle" => trans('lang.SEND_OUTFORPICKUP_TITLE'),
							"NotificationShortMessage" => 'Item is in destination country going through customs and sorting.',
							"NotificationMessage" => trans('lang.SEND_OUTFORPICKUP_MSG'),
							"NotificationType" => "requester_delivery_detail",
							"NotificationUserId" => array(new MongoId($requesterData->_id)),
							"Date" => new MongoDate(),
							"GroupTo" => "User",
						),
						array(
							"NotificationTitle" => trans('lang.SEND_OUTFORPICKUP_TITLE'),
							"NotificationMessage" => sprintf(trans('lang.SEND_OUTFORPICKUP_ADMIN'), $transporterData->Name, $requestid->PickupFullAddress, $requestid->DeliveryFullAddress),
							"NotificationUserId" => array(),
							"NotificationReadStatus" => 0,
							"location" => "requester_delivery_detail",
							"locationkey" => (string) $requestId,
							"Date" => new MongoDate(),
							"GroupTo" => "Admin",
						),
					]);

					$response['success'] = 1;
					$response['status'] = 'out_for_pickup';
					$response['msg'] = "<strong>Success!</strong> Item is in destination country going through customs and sorting.";

				}

			}
		}

		echo json_encode($response);
	}

	public function localpickup_request($requestId, $product_id, Request $request) {
		$response = array("success" => 0, "msg" => "Something went wrong.");
		$where = array("_id" => $requestId, "ProductList" => array('$elemMatch' => array('_id' => $product_id)));
		$requestid = Deliveryrequest::where($where)
			->select('RequesterId', 'ProductTitle', 'Status', 'ProductList', 'PackageId', 'PickupFullAddress', 'DeliveryFullAddress', 'PackageNumber', 'shippingCost', 'TotalCost')->first();

		if (count($requestid) > 0) {

			$requesterData = User::where(array('_id' => $requestid->RequesterId))->
				select('FirstName', 'LastName', 'Email', 'EmailStatus', 'NoficationStatus', 'NotificationId', 'DeviceType','Name')->first();

			$transporterData = User::where(array('_id' => session::get('UserId')))->select('FirstName', 'LastName', 'Email', 'EmailStatus', 'NoficationStatus', 'NotificationId' . 'DeviceType', 'Name')->first();
			if (count($requesterData) > 0 && count($transporterData)) {

				$update_array = $requestid->ProductList;
				foreach ($requestid->ProductList as $key => $value) {
					if ($value['_id'] == $product_id) {

						$update_array[$key]['status'] = 'out_for_pickup';
						$update_array[$key]['ExpectedDate'] = new Mongodate(strtotime('+ 13 days'));

						$item_title = $update_array[$key]['product_name'];
						$packageid = $update_array[$key]['package_id'];
						$total = $update_array[$key]['after_update'];
						$shippingCost = $update_array[$key]['shippingCost'];
					}

				}

				$updateData = Deliveryrequest::where($where)->update(['ProductList' => $update_array]);
				if ($updateData) {
					$Email = new NewEmail();
					Reqhelper::update_status2($requestId);
					//email to transporter
					if ($transporterData->EmailStatus == "on") {
						/*insert mail*/
						/*$cron_mail = [
							"USERNAME" => ucfirst($transporterData->Name),
							"PACKAGETITLE" => ucfirst($item_title),
							"PACKAGEID" => $packageid,
							"SOURCE" => $requestid->PickupFullAddress,
							"DESTINATION" => $requestid->DeliveryFullAddress,
							"PACKAGENUMBER" => $packageid,
							"SHIPPINGCOST" => $shippingCost,
							"TOTALCOST" => $total,
							'email_id' => '5694cf3c5509251cd67773ee',
							'email' => $transporterData->Email,
							'status' => 'ready',
							'REQUESTER' => ucfirst($requesterData->Name),
						];
						SendMail::insert($cron_mail);*/

						$Email->send_mail('5694cf3c5509251cd67773ee', [
							"to" => $transporterData->Email,
							"replace" => [
								"[TRANSPORTERNAME]" => $transporterData->Name,
								"[PACKAGETITLE]" => ucfirst($item_title),
								"[PACKAGEID]" => $packageid,
								"[SOURCE]" => $requestid->PickupFullAddress,
								"[DESTINATION]" => $requestid->DeliveryFullAddress,
								"[USERNAME]" => ucfirst($transporterData->Name),
								"[PACKAGETITLE]" => ucfirst($item_title),
								"[PACKAGEID]" => $packageid,
								"[SOURCE]" => $requestid->PickupFullAddress,
								"[DESTINATION]" => $requestid->DeliveryFullAddress,
								"[PACKAGENUMBER]" => $packageid,
								"[SHIPPINGCOST]" => $shippingCost,
								"[TOTALCOST]" => $total,
								'[REQUESTER]' => ucfirst($requesterData->Name),
							],
						]);
						/*end ins*/
					}

					//email to requester
					if ($requesterData->EmailStatus == "on") {
						/*insert mail*/
						/*$cron_mail2 = [
							"USERNAME" => ucfirst($requesterData->Name),
							"PACKAGETITLE" => ucfirst($item_title),
							"PACKAGEID" => $packageid,
							"SOURCE" => $requestid->PickupFullAddress,
							"DESTINATION" => $requestid->DeliveryFullAddress,
							"PACKAGENUMBER" => $packageid,
							"SHIPPINGCOST" => $shippingCost,
							"TOTALCOST" => $total,
							'email_id' => '5694cb805509251cd67773ec',
							'email' => $requesterData->Email,
							'status' => 'ready',
						];

						SendMail::insert($cron_mail2);*/

						$Email->send_mail('5694cb805509251cd67773ec', [
							"to" => $requesterData->Email,
							"replace" => [
								"[USERNAME]" => ucfirst($requesterData->Name),
								"[PACKAGETITLE]" => ucfirst($item_title),
								"[PACKAGEID]" => $packageid,
								"[SOURCE]" => $requestid->PickupFullAddress,
								"[DESTINATION]" => $requestid->DeliveryFullAddress,
								"[PACKAGENUMBER]" => $packageid,
								"[SHIPPINGCOST]" => $shippingCost,
								"[TOTALCOST]" => $total,
							],
						]);
						/*end ins*/
					}
					//email to admin

					//Notification to requester
					if ($requesterData->NoficationStatus == "on" && !empty($requesterData->NotificationId)) {
						// $Notification = new Notify();

						/*$cron_noti = [
							"title" => trans('lang.SEND_OUTFORPICKUP_TITLE'),
							"message" => trans('lang.SEND_OUTFORPICKUP_MSG'),
							"type" => 'request_detail',
							"locationkey" => $requestId,
							"NotificationId" => $requesterData->NotificationId,
							"DeviceType" => $requesterData->DeviceType,
							'status' => 'ready',
							'by_mean' => 'notification',
						];
						SendMail::insert($cron_noti);*/

						$Notification = new Pushnotification();
						$Notification->setValue('title',trans('lang.SEND_OUTFORPICKUP_TITLE'));
						$Notification->setValue('message',trans('lang.SEND_OUTFORPICKUP_MSG'));
						$Notification->setValue('type','LOCAL_DELIVERY_USER');	// request_detail
						$Notification->setValue('locationkey',$requestId);
						$Notification->add_user($requesterData->NotificationId, $requesterData->DeviceType);
						$Notification->fire();
						// $Notification->setValue('NotificationId',$requesterData->NotificationId);
						// $Notification->setValue('DeviceType',$requesterData->DeviceType);
					}

					/* Activity Log send a package out for pickup */

					$insertactivity = [
						'request_id' => $requestid->_id,
						'request_type' => 'local_delivery',
						'PackageNumber' => $requestid->PackageNumber,
						'item_id' => $product_id,
						'package_id' => $packageid,
						'item_name' => $item_title,
						'log_type' => 'request',
						'message' => 'Request has been picked up.',
						'status' => 'out_for_pickup',
						'action_user_id'=> Session::get('UserId'),
						'EnterOn' => new MongoDate(),
					];

					Activitylog::insert($insertactivity);
					//insertNotification
					Notification::Insert([
						array(
							"NotificationTitle" => trans('lang.SEND_OUTFORPICKUP_TITLE'),
							"NotificationShortMessage" => 'Item is in destination country going through customs and sorting.',
							"NotificationMessage" => trans('lang.SEND_OUTFORPICKUP_MSG'),
							"NotificationType" => "LOCAL_DELIVERY_USER",
							"NotificationUserId" => array(new MongoId($requesterData->_id)),
							"Date" => new MongoDate(),
							"GroupTo" => "User",
						),
						array(
							"NotificationTitle" => trans('lang.SEND_OUTFORPICKUP_TITLE'),
							"NotificationMessage" => sprintf(trans('lang.SEND_OUTFORPICKUP_ADMIN'), $transporterData->Name, $requestid->PickupFullAddress, $requestid->DeliveryFullAddress),
							"NotificationUserId" => array(),
							"NotificationReadStatus" => 0,
							"location" => "request_detail",
							"locationkey" => (string) $requestId,
							"Date" => new MongoDate(),
							"GroupTo" => "Admin",
						),
					]);

					$response['success'] = 1;
					$response['status'] = 'out_for_pickup';
					$response['msg'] = "<strong>Success!</strong> Item is in destination country going through customs and sorting.";

				}

			}
		}

		echo json_encode($response);
	}

	public function delivery_request($requestId, $product_id, Request $request) {

		$response = array("success" => 0, "msg" => "Oops !Something went wrong.");
		$where = array("_id" => $requestId, "ProductList" => array('$elemMatch' => array('_id' => $product_id)));
		$requestid = Deliveryrequest::where($where)->select('RequesterId', 'ProductTitle', 'ProductList', 'Status', 'PackageId', 'PickupFullAddress', 'DeliveryFullAddress', 'PackageNumber', 'shippingCost', 'TotalCost', 'StripeChargeId', 'StripeChargeId2','ReceiverCountrycode','ReceiverMobileNo')->first();

		if (count($requestid) > 0) {
			$ReceiverMobileNo = $requestid->ReceiverMobileNo;
			$ReceiverCountrycode = $requestid->ReceiverCountrycode;
			/*if (!empty(trim($requestid->StripeChargeId))) {

				$res = RSStripe::retrive_amount($requestid->StripeChargeId);
				if (!isset($res['id'])) {
					$res = RSStripe::find_charge($requestid->StripeChargeId);

					if ($res['captured'] != 1 || $res['captured'] != false) {
						if (!isset($res['id'])) {
							$response = ['success' => 0, 'msg' => $res];
							return response()->json($response);
							die;
						}

					}
				}
			}*/

			/*if (!empty(trim($requestid->StripeChargeId2))) {

				$res = RSStripe::retrive_amount($requestid->StripeChargeId2);
				if (!isset($res['id'])) {
					$res = RSStripe::find_charge($requestid->StripeChargeId2);

					if ($res['captured'] != 1 || $res['captured'] != false) {
						if (!isset($res['id'])) {
							$response = ['success' => 0, 'msg' => $res];
							return response()->json($response);
							die;
						}

					}
				}
			}*/

			$requesterData = User::where(array('_id' => $requestid->RequesterId))
				->select('FirstName', 'LastName', 'Email', 'EmailStatus', 'NoficationStatus', 'NotificationId', 'DeviceType', 'Name')
				->first();

			$transporterData = User::where(array('_id' => session::get('UserId')))->select('FirstName', 'LastName', 'Email', 'EmailStatus', 'NoficationStatus', 'NotificationId', 'DeviceType', 'Name')->first();

			if (count($requesterData) > 0 && count($transporterData)) {

				$update_array = $requestid->ProductList;
				foreach ($requestid->ProductList as $key => $value) {
					if ($value['_id'] == $product_id) {
						$update_array[$key]['status'] = 'out_for_delivery';
						$item_title = $update_array[$key]['product_name'];
						$packageid = $update_array[$key]['package_id'];
						$total = $update_array[$key]['after_update'];
						$shippingCost = $update_array[$key]['shippingCost'];
					}

				}

				$updateData = Deliveryrequest::where($where)->update(['ProductList' => $update_array]);

				if ($updateData) {
					Reqhelper::update_status2($requestId);
					//email to transporter
					if ($transporterData->EmailStatus == "on") {
						$cron_mail = [
							"USERNAME" => ucfirst($transporterData->Name),
							"PACKAGETITLE" => ucfirst($item_title),
							"PACKAGEID" => $packageid,
							"SOURCE" => $requestid->PickupFullAddress,
							"DESTINATION" => $requestid->DeliveryFullAddress,
							"PACKAGENUMBER" => $packageid,
							"SHIPPINGCOST" => $shippingCost,
							"TOTALCOST" => $total,
							'email_id' => '5694cffb5509251cd67773ef',
							'email' => $transporterData->Email,
							'status' => 'ready',
							'REQUESTER' => ucfirst($requesterData->Name),
						];

						SendMail::insert($cron_mail);
					}
					if ($requesterData->EmailStatus == "on") {
						$cron_mail2 = [
							"USERNAME" => ucfirst($requesterData->Name),
							"PACKAGETITLE" => ucfirst($item_title),
							"PACKAGEID" => $packageid,
							"SOURCE" => $requestid->PickupFullAddress,
							"DESTINATION" => $requestid->DeliveryFullAddress,
							"PACKAGENUMBER" => $packageid,
							"SHIPPINGCOST" => $shippingCost,
							"TOTALCOST" => $total,
							'email_id' => '562228e2e4b0252ad07d07a2',
							'email' => $requesterData->Email,
							'status' => 'ready',
							'CC'=>@$ReceiverCountrycode,
							'MNO'=>@$ReceiverMobileNo,
							'by_mean'=>'email_2'
						];
						SendMail::insert($cron_mail2);
					}

					if ($requesterData->NoficationStatus == "on" && !empty($requesterData->NotificationId)) {

						$Notification = new Notify();
						$Notification->setValue('title', trans('lang.SEND_OUTFORDELIVERY_TITLE'));
						$Notification->setValue('message', sprintf(trans('lang.SEND_OUTFORDELIVERY_MSG'), ucfirst($item_title), $packageid));
						$Notification->setValue('type', 'requester_delivery_detail');
						$Notification->setValue('locationkey', $requestId);
						$Notification->add_user($requesterData->NotificationId, $requesterData->DeviceType);
						$Notification->fire();

						$cron_noti2 = [
							"title" => trans('lang.SEND_OUTFORDELIVERY_TITLE'),
							"message" => sprintf(trans('lang.SEND_OUTFORDELIVERY_MSG'), ucfirst($item_title), $packageid),
							"type" => 'requester_delivery_detail',
							"locationkey" => $requestId,
							"NotificationId" => $requesterData->NotificationId,
							"DeviceType" => $requesterData->DeviceType,
							'status' => 'ready',
							'by_mean' => 'notification',
						];
						SendMail::insert($cron_noti2);
					}
					/* Activity Log send a package out for delivery */
					$insertactivity = [
						'request_id' => $requestid->_id,
						'request_type' => 'delivery',
						'PackageNumber' => $requestid->PackageNumber,
						'item_id' => $product_id,
						'package_id' => $packageid,
						'item_name' => $item_title,
						'log_type' => 'request',
						// 'message' => 'Your package is en route to be delivered.',
						'message' => 'Package is in destination country and is out for delivery.',
						'status' => 'out_for_delivery',
						'action_user_id'=> Session::get('UserId'),
						'EnterOn' => new MongoDate(),
					];

					Activitylog::insert($insertactivity);

					//insertNotification
					Notification::insert([
						array(
							"NotificationTitle" => trans('lang.SEND_OUTFORDELIVERY_TITLE'),
							"NotificationShortMessage" => sprintf('Transporter "%s" is en route to deliver your item.', $transporterData->Name),
							"NotificationMessage" => sprintf(trans('lang.SEND_OUTFORDELIVERY_MSG'), ucfirst($item_title), $packageid),
							"NotificationType" => "requester_delivery_detail",
							"NotificationUserId" => array(new MongoId($requesterData->_id)),
							"Date" => new MongoDate(),
							"GroupTo" => "User",
						),
						array(
							"NotificationTitle" => trans('lang.SEND_OUTFORDELIVERY_TITLE'),
							"NotificationMessage" => sprintf(trans('lang.SEND_OUTFORDELIVERY_ADMIN'), ucfirst($item_title), $packageid),
							"NotificationUserId" => array(),
							"NotificationReadStatus" => 0,
							"location" => "requester_delivery_detail",
							"locationkey" => (string) $requestId,
							"Date" => new MongoDate(),
							"GroupTo" => "Admin",
						),
					]);

					$response['success'] = 1;
					$response['status'] = 'out_for_delivery';
					$response['msg'] = "<strong>Success!</strong> Your package is en route to be delivered.";
				}

			}
		}

		echo json_encode($response);
	}

	public function localdelivery_request($requestId, $product_id, Request $request) {
		$response = array("success" => 0, "msg" => "Oops !Something went wrong.");
		$where = array("_id" => $requestId, "ProductList" => array('$elemMatch' => array('_id' => $product_id)));
		$requestid = Deliveryrequest::where($where)->select('RequesterId', 'ProductTitle', 'ProductList', 'Status', 'PackageId', 'PickupFullAddress', 'DeliveryFullAddress', 'PackageNumber', 'shippingCost', 'TotalCost', 'StripeChargeId', 'StripeChargeId2')->first();

		if (count($requestid) > 0) {
			/*if (!empty(trim($requestid->StripeChargeId))) {

				$res = RSStripe::retrive_amount($requestid->StripeChargeId);
				if (!isset($res['id'])) {
					$res = RSStripe::find_charge($requestid->StripeChargeId);

					if ($res['captured'] != 1 || $res['captured'] != false) {
						if (!isset($res['id'])) {
							$response = ['success' => 0, 'msg' => $res];
							return response()->json($response);
							die;
						}

					}
				}
			}*/

			/*if (!empty(trim($requestid->StripeChargeId2))) {

				$res = RSStripe::retrive_amount($requestid->StripeChargeId2);
				if (!isset($res['id'])) {
					$res = RSStripe::find_charge($requestid->StripeChargeId2);

					if ($res['captured'] != 1 || $res['captured'] != false) {
						if (!isset($res['id'])) {
							$response = ['success' => 0, 'msg' => $res];
							return response()->json($response);
							die;
						}

					}
				}
			}*/

			$requesterData = User::where(array('_id' => $requestid->RequesterId))
				->select('FirstName', 'LastName', 'Email', 'EmailStatus', 'NoficationStatus', 'NotificationId', 'DeviceType', 'Name')
				->first();

			$transporterData = User::where(array('_id' => session::get('UserId')))->select('FirstName', 'LastName', 'Email', 'EmailStatus', 'NoficationStatus', 'NotificationId', 'DeviceType', 'Name')->first();

			if (count($requesterData) > 0 && count($transporterData)) {

				$update_array = $requestid->ProductList;
				foreach ($requestid->ProductList as $key => $value) {
					if ($value['_id'] == $product_id) {
						$update_array[$key]['status'] = 'out_for_delivery';
						$item_title = $update_array[$key]['product_name'];
						$packageid = $update_array[$key]['package_id'];
						$total = $update_array[$key]['after_update'];
						$shippingCost = $update_array[$key]['shippingCost'];
					}

				}

				$updateData = Deliveryrequest::where($where)->update(['ProductList' => $update_array]);

				if ($updateData) {
					Reqhelper::update_status2($requestId);
					$Email = new NewEmail();
					//email to transporter
					if ($transporterData->EmailStatus == "on") {
						/*$cron_mail = [
							"USERNAME" => ucfirst($transporterData->Name),
							"PACKAGETITLE" => ucfirst($item_title),
							"PACKAGEID" => $packageid,
							"SOURCE" => $requestid->PickupFullAddress,
							"DESTINATION" => $requestid->DeliveryFullAddress,
							"PACKAGENUMBER" => $packageid,
							"SHIPPINGCOST" => $shippingCost,
							"TOTALCOST" => $total,
							'email_id' => '5694cffb5509251cd67773ef',
							'email' => $transporterData->Email,
							'status' => 'ready',
							'REQUESTER' => ucfirst($requesterData->Name),
						];

						SendMail::insert($cron_mail);*/

						$Email->send_mail('5694cffb5509251cd67773ef', [
							"to" => $transporterData->Email,
							"replace" => [
								"[USERNAME]" => ucfirst($transporterData->Name),
								"[PACKAGETITLE]" => ucfirst($item_title),
								"[PACKAGEID]" => $packageid,
								"[SOURCE]" => $requestid->PickupFullAddress,
								"[DESTINATION]" => $requestid->DeliveryFullAddress,
								"[PACKAGENUMBER]" => $packageid,
								"[SHIPPINGCOST]" => $shippingCost,
								"[TOTALCOST]" => $total,
								'[REQUESTER]' => ucfirst($requesterData->Name),
							],
						]);
					}
					if ($requesterData->EmailStatus == "on") {
						/*$cron_mail2 = [
							"USERNAME" => ucfirst($requesterData->Name),
							"PACKAGETITLE" => ucfirst($item_title),
							"PACKAGEID" => $packageid,
							"SOURCE" => $requestid->PickupFullAddress,
							"DESTINATION" => $requestid->DeliveryFullAddress,
							"PACKAGENUMBER" => $packageid,
							"SHIPPINGCOST" => $shippingCost,
							"TOTALCOST" => $total,
							'email_id' => '562228e2e4b0252ad07d07a2',
							'email' => $requesterData->Email,
							'status' => 'ready',
						];
						SendMail::insert($cron_mail2);*/

						$Email->send_mail('562228e2e4b0252ad07d07a2', [
							"to" => $requesterData->Email,
							"replace" => [
								"[USERNAME]" => ucfirst($requesterData->Name),
								"[PACKAGETITLE]" => ucfirst($item_title),
								"[PACKAGEID]" => $packageid,
								"[SOURCE]" => $requestid->PickupFullAddress,
								"[DESTINATION]" => $requestid->DeliveryFullAddress,
								"[PACKAGENUMBER]" => $packageid,
								"[SHIPPINGCOST]" => $shippingCost,
								"[TOTALCOST]" => $total,
							],
						]);
					}

					if ($requesterData->NoficationStatus == "on" && !empty($requesterData->NotificationId)) {
						/*$cron_noti2 = [
							"title" => trans('lang.SEND_OUTFORDELIVERY_TITLE'),
							"message" => sprintf(trans('lang.SEND_OUTFORDELIVERY_MSG'), ucfirst($item_title), $packageid),
							"type" => 'request_detail',
							"locationkey" => $requestId,
							"NotificationId" => $requesterData->NotificationId,
							"DeviceType" => $requesterData->DeviceType,
							'status' => 'ready',
							'by_mean' => 'notification',
						];
						SendMail::insert($cron_noti2);*/

						$Notification = new Pushnotification();
						$Notification->setValue('title',trans('lang.SEND_OUTFORDELIVERY_TITLE'));
						$Notification->setValue('message',sprintf(trans('lang.SEND_OUTFORDELIVERY_MSG'), ucfirst($item_title), $packageid));
						$Notification->setValue('type','LOCAL_DELIVERY_USER');	// request_detail
						$Notification->setValue('locationkey',$requestId);
						$Notification->add_user($requesterData->NotificationId, $requesterData->DeviceType);
						$Notification->fire();
						// $Notification->setValue('NotificationId',$requesterData->NotificationId);
						// $Notification->setValue('DeviceType',$requesterData->DeviceType);
					}
					/* Activity Log send a package out for delivery */
					$insertactivity = [
						'request_id' => $requestid->_id,
						'request_type' => 'local_delivery',
						'PackageNumber' => $requestid->PackageNumber,
						'item_id' => $product_id,
						'package_id' => $packageid,
						'item_name' => $item_title,
						'log_type' => 'request',
						'message' => 'Your package is en route to be delivered.',
						'status' => 'out_for_delivery',
						'action_user_id'=> Session::get('UserId'),
						'EnterOn' => new MongoDate(),
					];

					Activitylog::insert($insertactivity);

					//insertNotification
					Notification::insert([
						array(
							"NotificationTitle" => trans('lang.SEND_OUTFORDELIVERY_TITLE'),
							"NotificationShortMessage" => sprintf('Transporter "%s" is en route to deliver your item.', $transporterData->Name),
							"NotificationMessage" => sprintf(trans('lang.SEND_OUTFORDELIVERY_MSG'), ucfirst($item_title), $packageid),
							"NotificationType" => "LOCAL_DELIVERY_USER",
							"NotificationUserId" => array(new MongoId($requesterData->_id)),
							"Date" => new MongoDate(),
							"GroupTo" => "User",
						),
						array(
							"NotificationTitle" => trans('lang.SEND_OUTFORDELIVERY_TITLE'),
							"NotificationMessage" => sprintf(trans('lang.SEND_OUTFORDELIVERY_ADMIN'), ucfirst($item_title), $packageid),
							"NotificationUserId" => array(),
							"NotificationReadStatus" => 0,
							"location" => "LOCAL_DELIVERY_USER",
							"locationkey" => (string) $requestId,
							"Date" => new MongoDate(),
							"GroupTo" => "Admin",
						),
					]);
//https://apis.aquantuo.com/local-delivery-deliver-request/5d5110928f9270f2548b4577/5d5110928f9270f2548b4576
					$response['success'] = 1;
					$response['status'] = 'out_for_delivery';
					$response['msg'] = "<strong>Success!</strong> Your package is en route to be delivered.";
				}

			}
		}

		echo json_encode($response);
	}

	public function transporter_review(Request $request) {
		$response = array("success" => 0, "msg" => "Oops! Something went wrong.");
		$where = [
			"_id" => $request->get('request_id'),
			"RequesterId" => new MongoId($request->get('user_id')),

			"ProductList" => ['$elemMatch' => [
				'_id' => $request->get('item_id'),
				'status' => 'delivered',
				"tpid" => Session::get('UserId'),
			],
			],
		];

		$del_info = Deliveryrequest::where($where)->first();
		if (count($del_info) > 0) {
			$update_array = $del_info->ProductList;

			foreach ($del_info->ProductList as $key => $value) {
				if ($value['_id'] == $request->get('item_id')) {
					$update_array[$key]['TransporterRating'] = $request->get('rating');
					$update_array[$key]['TransporterFeedbcak'] = $request->get('review');
				}
			}
			$del_info->ProductList = $update_array;
			if ($del_info->save()) {
				$requester_rating = User::find($request->get('user_id'));
				$requester_rating->RatingCount = $requester_rating->RatingCount + $request->get('rating');
				$requester_rating->RatingByCount = $requester_rating->RatingByCount + 1;
				$requester_rating->save();
				$response = array("success" => 1, "msg" => "Thanks for your feedback.");
			}
		}
		echo json_encode($response);
	}

	public function verify_delivery_code(Request $request) {
		$response = array("success" => 0, "msg" => "Oops !Something went wrong.");

		$where = array("_id" => $request->get('request_id'),
			"ProductList" => array('$elemMatch' => array(
				'_id' => $request->get('item_id'),
				'status' => 'out_for_delivery',
			)));
		$delivery_data = Deliveryrequest::where($where)->first();

		if (count($delivery_data) > 0) {
			$requesterData = User::where(array('_id' => $delivery_data->RequesterId))
				->select('FirstName', 'LastName', 'Email', 'EmailStatus', 'NoficationStatus', 'NotificationId', 'DeviceType', 'Name')
				->first();

			$transporterData = User::where(array('_id' => session::get('UserId')))->select('FirstName', 'LastName', 'Email', 'EmailStatus', 'NoficationStatus', 'NotificationId', 'DeviceType', 'Name')->first();

			if (count($requesterData) > 0 && count($transporterData) > 0) {
				$update_array = $delivery_data->ProductList;
				foreach ($delivery_data->ProductList as $key => $value) {
					if ($value['_id'] == $request->get('item_id')) {
						$update_array[$key]['status'] = 'delivered';
						$item_title = $update_array[$key]['product_name'];
						$packageid = $update_array[$key]['package_id'];
						$total = $update_array[$key]['after_update'];
						$shippingCost = $update_array[$key]['shippingCost'];
						$verifyCode = $update_array[$key]['DeliveryVerifyCode'];
					}
				}

				if ($verifyCode != $request->get('code')) {
					$response = array("success" => 0, "msg" => "The verification code you entered is invalid.");
					echo json_encode($response);die;
				}

				$updateData = Deliveryrequest::where($where)->update(['ProductList' => $update_array]);

				if ($updateData) {
					Reqhelper::update_status2($request->get('request_id'));
					if ($transporterData->EmailStatus == "on") {
						$cron_mail = [
							"USERNAME" => $transporterData->Name,
							"PACKAGETITLE" => ucfirst($item_title),
							"PACKAGEID" => $packageid,
							"SOURCE" => $delivery_data->PickupFullAddress,
							"DESTINATION" => $delivery_data->DeliveryFullAddress,
							"PACKAGENUMBER" => $packageid,
							"SHIPPINGCOST" => $shippingCost,
							"TOTALCOST" => $total,
							'email_id' => '5694d88a5509251cd67773f0',
							'email' => $transporterData->Email,
							'status' => 'ready',
							'REQUESTER' => ucfirst($requesterData->Name),
						];
						SendMail::insert($cron_mail);
					}

					if ($requesterData->EmailStatus == "on") {
						$cron_mail2 = [
							"USERNAME" => $requesterData->FirstName . ' ' . $requesterData->LastName,
							"PACKAGETITLE" => ucfirst($item_title),
							"PACKAGEID" => $packageid,
							"SOURCE" => $delivery_data->PickupFullAddress,
							"DESTINATION" => $delivery_data->DeliveryFullAddress,
							"PACKAGENUMBER" => $packageid,
							"SHIPPINGCOST" => $shippingCost,
							"TOTALCOST" => $total,
							'email_id' => '57d7d8107ac6f6e0148b4567',	// 5622492ce4b0d4bc235582bd
							'email' => $requesterData->Email,
							'status' => 'ready',
						];
						SendMail::insert($cron_mail2);
					}

					if ($requesterData->NoficationStatus == "on" && !empty(trim($requesterData->NotificationId))) {

						$Notification = new Notify();
						$Notification->setValue('title', trans('lang.REQUEST_DELIVERED'));
						$Notification->setValue('message', sprintf(trans('lang.MSG_REQUEST_DELIVERED'), $transporterData->Name, ucfirst($item_title)));
						$Notification->setValue('type', 'requester_delivery_detail');
						$Notification->setValue('locationkey', $request->get('request_id'));
						$Notification->add_user($requesterData->NotificationId, $requesterData->DeviceType);
						$Notification->fire();
						
						$cron_noti = [
							"title" => trans('lang.REQUEST_DELIVERED'),
							"message" => sprintf(trans('lang.MSG_REQUEST_DELIVERED'), $transporterData->Name, ucfirst($item_title)),
							"type" => 'requester_delivery_detail',
							"locationkey" => $request->get('request_id'),
							"NotificationId" => $requesterData->NotificationId,
							"DeviceType" => $requesterData->DeviceType,
							'status' => 'ready',
							'by_mean' => 'notification',
						];
						SendMail::insert($cron_noti);
					}

					Notification::Insert([
						array(
							"NotificationTitle" => trans('lang.REQUEST_DELIVERED'),
							"NotificationShortMessage" => sprintf(trans('lang.MSG_REQUEST_DELIVERED'), $transporterData->Name, ucfirst($item_title)),
							"NotificationMessage" => sprintf('Transporter "%s" has delivered your package "%s".', $transporterData->Name, ucfirst($item_title)),
							"NotificationType" => "requester_delivery_detail",
							"NotificationUserId" => array(new MongoId($requesterData->_id)),
							"Date" => new MongoDate(),
							"GroupTo" => "User",
						),
						array(
							"NotificationTitle" => trans('lang.SEND_DELIVERED_TITLE_ADMIN'),
							"NotificationMessage" => sprintf(trans('lang.SEND_DELIVERED_ADMIN'), $transporterData->Name, $packageid),
							"NotificationUserId" => array(),
							"NotificationReadStatus" => 0,
							"location" => "requester_delivery_detail",
							"locationkey" => (string) $request->get('request_id'),
							"Date" => new MongoDate(),
							"GroupTo" => "Admin",
						),
					]);

					$insertactivity = [
						'request_id' => $delivery_data->_id,
						'request_type' => $delivery_data->RequestType,
						'item_id' => $request->get('item_id'),
						'package_id' => $packageid,
						'item_name' => ucfirst($item_title),
						'log_type' => 'request',
						'message' => 'Package has been successfully delivered.',
						'status' => 'delivered',
						'action_user_id'=> Session::get('UserId'),
						'EnterOn' => new MongoDate(),

					];
					Activitylog::insert($insertactivity);

					$response['success'] = 1;
					$response['status'] = 'delivered';
					$response['msg'] = "Package has been successfully delivered.";
				}
			}
		}
		echo json_encode($response);
	}

	public function localverify_delivery_code(Request $request) {
		$response = array("success" => 0, "msg" => "Oops !Something went wrong.");

		$where = array("_id" => $request->get('request_id'),
			"ProductList" => array('$elemMatch' => array(
				'_id' => $request->get('item_id'),
				'status' => 'out_for_delivery',
			)));
		$delivery_data = Deliveryrequest::where($where)->first();

		if (count($delivery_data) > 0) {
			$requesterData = User::where(array('_id' => $delivery_data->RequesterId))
				->select('FirstName', 'LastName', 'Email', 'EmailStatus', 'NoficationStatus', 'NotificationId', 'DeviceType', 'Name')
				->first();

			$transporterData = User::where(array('_id' => session::get('UserId')))->select('FirstName', 'LastName', 'Email', 'EmailStatus', 'NoficationStatus', 'NotificationId', 'DeviceType', 'Name')->first();

			if (count($requesterData) > 0 && count($transporterData) > 0) {
				$update_array = $delivery_data->ProductList;
				foreach ($delivery_data->ProductList as $key => $value) {
					if ($value['_id'] == $request->get('item_id')) {
						$update_array[$key]['status'] = 'delivered';
						$item_title = $update_array[$key]['product_name'];
						$packageid = $update_array[$key]['package_id'];
						$total = $update_array[$key]['after_update'];
						$shippingCost = $update_array[$key]['shippingCost'];
						$verifyCode = $update_array[$key]['DeliveryVerifyCode'];
					}
				}

				if ($verifyCode != $request->get('code')) {
					$response = array("success" => 0, "msg" => "The verification code you entered is invalid.");
					echo json_encode($response);die;
				}

				$updateData = Deliveryrequest::where($where)->update(['ProductList' => $update_array]);

				if ($updateData) {
					Reqhelper::update_status2($request->get('request_id'));
					$Email = new NewEmail();

					if ($transporterData->EmailStatus == "on") {
						/*$cron_mail = [
							"USERNAME" => $transporterData->Name,
							"PACKAGETITLE" => ucfirst($item_title),
							"PACKAGEID" => $packageid,
							"SOURCE" => $delivery_data->PickupFullAddress,
							"DESTINATION" => $delivery_data->DeliveryFullAddress,
							"PACKAGENUMBER" => $packageid,
							"SHIPPINGCOST" => $shippingCost,
							"TOTALCOST" => $total,
							'email_id' => '5694d88a5509251cd67773f0',
							'email' => $transporterData->Email,
							'status' => 'ready',
							'REQUESTER' => ucfirst($requesterData->Name),
						];
						SendMail::insert($cron_mail);*/

						$Email->send_mail('5694d88a5509251cd67773f0', [
							"to" => $transporterData->Email,
							"replace" => [
								"[USERNAME]" => $transporterData->Name,
								"[PACKAGETITLE]" => ucfirst($item_title),
								"[PACKAGEID]" => $packageid,
								"[SOURCE]" => $delivery_data->PickupFullAddress,
								"[DESTINATION]" => $delivery_data->DeliveryFullAddress,
								"[PACKAGENUMBER]" => $packageid,
								"[SHIPPINGCOST]" => $shippingCost,
								"[TOTALCOST]" => $total,
								'[REQUESTER]' => ucfirst($requesterData->Name),
							],
						]);
					}

					if ($requesterData->EmailStatus == "on") {
						/*$cron_mail2 = [
							"USERNAME" => $requesterData->FirstName . ' ' . $requesterData->LastName,
							"PACKAGETITLE" => ucfirst($item_title),
							"PACKAGEID" => $packageid,
							"SOURCE" => $delivery_data->PickupFullAddress,
							"DESTINATION" => $delivery_data->DeliveryFullAddress,
							"PACKAGENUMBER" => $packageid,
							"SHIPPINGCOST" => $shippingCost,
							"TOTALCOST" => $total,
							'email_id' => '5622492ce4b0d4bc235582bd',
							'email' => $requesterData->Email,
							'status' => 'ready',
						];
						SendMail::insert($cron_mail2);*/

						$Email->send_mail('5622492ce4b0d4bc235582bd', [
							"to" => $requesterData->Email,
							"replace" => [
								"[USERNAME]" => $requesterData->FirstName . ' ' . $requesterData->LastName,
								"[PACKAGETITLE]" => ucfirst($item_title),
								"[PACKAGEID]" => $packageid,
								"[SOURCE]" => $delivery_data->PickupFullAddress,
								"[DESTINATION]" => $delivery_data->DeliveryFullAddress,
								"[PACKAGENUMBER]" => $packageid,
								"[SHIPPINGCOST]" => $shippingCost,
								"[TOTALCOST]" => $total,
								
							],
						]);
					}

					if ($requesterData->NoficationStatus == "on" && !empty(trim($requesterData->NotificationId))) {
						
						/*$cron_noti = [
							"title" => trans('lang.REQUEST_DELIVERED'),
							"message" => sprintf(trans('lang.MSG_REQUEST_DELIVERED'), $transporterData->Name, ucfirst($item_title)),
							"type" => 'request_detail',
							"locationkey" => $request->get('request_id'),
							"NotificationId" => $requesterData->NotificationId,
							"DeviceType" => $requesterData->DeviceType,
							'status' => 'ready',
							'by_mean' => 'notification',
						];
						SendMail::insert($cron_noti);*/

						$Notification = new Pushnotification();
						$Notification->setValue('title',trans('lang.REQUEST_DELIVERED'));
						$Notification->setValue('message',sprintf(trans('lang.MSG_REQUEST_DELIVERED'), $transporterData->Name, ucfirst($item_title)));
						$Notification->setValue('type','LOCAL_DELIVERY_USER');	// request_detail
						$Notification->setValue('locationkey',$request->get('request_id'));
						$Notification->add_user($requesterData->NotificationId, $requesterData->DeviceType);
						$Notification->fire();
						// $Notification->setValue('NotificationId',$requesterData->NotificationId);
						// $Notification->setValue('DeviceType',$requesterData->DeviceType);
					}

					Notification::Insert([
						array(
							"NotificationTitle" => trans('lang.REQUEST_DELIVERED'),
							"NotificationShortMessage" => sprintf(trans('lang.MSG_REQUEST_DELIVERED'), $transporterData->Name, ucfirst($item_title)),
							"NotificationMessage" => sprintf('Transporter "%s" has delivered your package "%s".', $transporterData->Name, ucfirst($item_title)),
							"NotificationType" => "LOCAL_DELIVERY_USER",
							"NotificationUserId" => array(new MongoId($requesterData->_id)),
							"Date" => new MongoDate(),
							"GroupTo" => "User",
						),
						array(
							"NotificationTitle" => trans('lang.SEND_DELIVERED_TITLE_ADMIN'),
							"NotificationMessage" => sprintf(trans('lang.SEND_DELIVERED_ADMIN'), $transporterData->Name, $packageid),
							"NotificationUserId" => array(),
							"NotificationReadStatus" => 0,
							"location" => "LOCAL_DELIVERY_USER",
							"locationkey" => (string) $request->get('request_id'),
							"Date" => new MongoDate(),
							"GroupTo" => "Admin",
						),
					]);

					$insertactivity = [
						'request_id' => $delivery_data->_id,
						'request_type' => $delivery_data->RequestType,
						'item_id' => $request->get('item_id'),
						'package_id' => $packageid,
						'item_name' => ucfirst($item_title),
						'log_type' => 'request',
						'message' => 'Package has been successfully delivered.',
						'status' => 'delivered',
						'action_user_id'=> Session::get('UserId'),
						'EnterOn' => new MongoDate(),

					];
					Activitylog::insert($insertactivity);

					$response['success'] = 1;
					$response['status'] = 'delivered';
					$response['msg'] = "Package has been successfully delivered.";
				}
			}
		}
		echo json_encode($response);
	}

	public function send_package_nodelivery(Request $request) {
		$response = array("success" => 0, "msg" => "Oops! Something went wrong.");
		$where = [
			"_id" => $request->get('request_id'),
			"ProductList" => ['$elemMatch' => [
				'_id' => $request->get('item_id'),
				'status' => 'out_for_delivery',
				"tpid" => Session::get('UserId'),
			],
			],
		];

		$del_info = Deliveryrequest::where($where)->first();
		$setting = Setting::find('563b0e31e4b03271a097e1ca');
		if (count($del_info) > 0) {
			$requesterData = User::where(array('_id' => $del_info->RequesterId))
				->select('Email', 'EmailStatus', 'NoficationStatus', 'NotificationId', 'DeviceType', 'Name')
				->first();
			$transporterData = User::where(array('_id' => session::get('UserId')))
				->select('Name', 'Email', 'EmailStatus', 'NoficationStatus', 'NotificationId' . 'DeviceType')
				->first();

			if (count($requesterData) == 0 | count($transporterData) == 0) {
				echo json_encode($response);die;
			}

			$update_array = $del_info->ProductList;
			foreach ($del_info->ProductList as $key => $value) {
				if ($value['_id'] == $request->get('item_id')) {
					$item_title = $update_array[$key]['product_name'];

					$update_array[$key]['status'] = 'cancel';
					$update_array[$key]['RejectBy'] = $request->get('reject_by');
					$update_array[$key]['ReturnType'] = $request->get('return_type');
					$update_array[$key]['RejectTime'] = new MongoDate();
					$update_array[$key]['TrackingNumber'] = $request->get('tracking_number');
					$update_array[$key]['TransporterMessage'] = $request->get('message');
					$update_array[$key]['UpdateOn'] = new MongoDate();
					if (isset($_FILES['image']['name']) && @$_FILES['image']['name'] != '') {
						$exts = explode('.', $_FILES['image']['name']);
						$ext = $exts[count($exts) - 1];
						$receipt = "receipt" . rand(2154, 45454) . time() . ".$ext";
						if (in_array(strtolower($ext), ['gif', 'png', 'jpg', 'jpeg'])) {
							$newpath = BASEURL_FILE . "package/{$receipt}";
							if (move_uploaded_file($_FILES['image']['tmp_name'], $newpath)) {
								$update_array[$key]['ReceiptImage'] = "package/{$receipt}";
							}
						}
					}
				}
			}

			$notifymsg = sprintf('Your package "%s" has been canceled by %s', $item_title, $transporterData->Name);
			$emailReasonforCancaletion = "Rejectby : " . ucfirst($request->get('reject_by')) . ", Retrun type: " . get_cancel_status($request->get('return_type'));
			$CancelReturnDate = '';

			if ($request->get('return_type') == 'ipayment') {

				$notifymsg = sprintf(trans('lang.BUYFORME_CANCELED_MSG_IPAY'),
					$item_title,
					@$setting->SupportEmail
				);

			} else if (strtolower($request->get('return_type')) == 'creturn' && strtolower($request->get('reject_by')) == 'requester') {

				if ($request->get('date') != '') {
					$date = date_create_from_format('M d, Y h:i:s A', $request->get('date'));
					if ($date) {

						$CancelReturnDate = new MongoDate(strtotime(date_format($date, 'd-m-Y H:i:s')));
					}
				}

				$notifymsg = sprintf(trans('lang.BUYFORME_CANCELED_MSG_CRE'), $item_title, $request->get('message'));

			} else if (Input::get('return_type') == 'osreturn' && strtolower(Input::get('reject_by')) == 'requester') {

				$notifymsg = sprintf(trans('lang.BUYFORME_CANCELED_MSG_OSCRE'),
					$item_title,
					$transporterData->Name,
					$request->get('message')
				);

			}

			foreach ($del_info->ProductList as $key => $value) {
				if ($value['_id'] == $request->get('item_id')) {
					$item_title = $update_array[$key]['product_name'];
					$packageid = $update_array[$key]['package_id'];

					$update_array[$key]['status'] = 'cancel';
					$update_array[$key]['RejectBy'] = $request->get('reject_by');
					$update_array[$key]['ReturnType'] = $request->get('return_type');
					$update_array[$key]['RejectTime'] = new MongoDate();
					$update_array[$key]['TrackingNumber'] = $request->get('tracking_number');
					$update_array[$key]['TransporterMessage'] = $request->get('message');
					$update_array[$key]['UpdateOn'] = new MongoDate();
					$update_array[$key]['CancelReturnDate'] = $CancelReturnDate;
					if (isset($_FILES['image']['name']) && @$_FILES['image']['name'] != '') {
						$exts = explode('.', $_FILES['image']['name']);
						$ext = $exts[count($exts) - 1];
						$receipt = "receipt" . rand(2154, 45454) . time() . ".$ext";
						if (in_array(strtolower($ext), ['gif', 'png', 'jpg', 'jpeg'])) {
							$newpath = BASEURL_FILE . "package/{$receipt}";
							if (move_uploaded_file($_FILES['image']['tmp_name'], $newpath)) {
								$update_array[$key]['ReceiptImage'] = "package/{$receipt}";
							}
						}
					}
				}
			}

			$updateData = Deliveryrequest::where($where)->update(['ProductList' => $update_array]);
			if ($updateData) {
				ReqHelper::update_status($deliveryData->_id);
				$response['success'] = 1;
				$response['msg'] = "Success! Request has been canceled successfully.";

				if ($transporterData->EmailStatus == "on") {
					send_mail('58762b057ac6f6f2128b4567', [
						"to" => $transporterData->Email,
						"replace" => [
							"[USERNAME]" => $transporterData->Name,
							"[PACKAGETITLE]" => $item_title,
							"[REASON]" => @$emailReasonforCancaletion,
							"[PACKAGEID]" => $packageid,
						],
					]);
				}

				if ($requesterData->EmailStatus == "on") {
					send_mail('588b56736befd90b9d7ea797', [
						"to" => $requesterData->Email,
						"replace" => [
							"[USERNAME]" => $requesterData->Name,
							'[PACKAGETITLE]' => $item_title,
							"[REASON]" => @$emailReasonforCancaletion,
							"[PACKAGEID]" => $packageid,
						],
					]);
				}

				$Notification = new Notify();

				//Notification to requester
				if ($requesterData->NoficationStatus == "on" && !empty(trim($requesterData->NotificationId))) {
					$Notification->setValue('title', trans('lang.BUYFORME_CANCELED_TITLE'));
					$Notification->setValue('message', $notifymsg);
					$Notification->setValue('type', 'delivery_detail');
					$Notification->setValue('locationkey', $request->get('request_id'));
					$Notification->add_user($requesterData->NotificationId, $requesterData->DeviceType);
					$Notification->fire();
				}
				//insertNotification
				Notification::Insert([
					array(
						"NotificationTitle" => 'Your request has been canceled',
						"NotificationShortMessage" => $notifymsg,
						"NotificationMessage" => $notifymsg,
						"NotificationType" => "canceled_delivery",
						"NotificationUserId" => array(new MongoId($requesterData->_id)),
						"Date" => new MongoDate(),
						"GroupTo" => "User",
					),
					array(
						"NotificationTitle" => 'Request canceled',
						"NotificationMessage" => $notifymsg,
						"NotificationReadStatus" => 0,
						"location" => "request_detail",
						"locationkey" => (string) $request->get('request_id'),
						"Date" => new MongoDate(),
						"GroupTo" => "Admin",
					),
				]);

			}

		}
		echo json_encode($response);
	}

	public function getInfo(Request $request) {
		$response = array("success" => 0, "msg" => "Oops! Something went wrong.");
		if ($request->get('user_id') != '') {
			$user_info = User::find($request->get('user_id'));
			if ($user_info) {
				$response = array("success" => 1, "msg" => "User Info.", 'result' => $user_info);
			}
		}
		echo json_encode($response);

	}

	public function transporterRating() {
		echo "Stop Working.........";
	}

	public function ShipmentDeparted($id, $product_id) {
		$response = ['success' => 0, 'msg' => 'Something went wrong.'];
		$where = [
			'_id' => $id,
			'ProductList' => [
				'$elemMatch' => [
					'_id' => $product_id,
					'status' => 'out_for_pickup'
				]
			]
		];
		
		$requestDetails = Deliveryrequest::where($where)->first();

		if (count($requestDetails) > 0) {
			$requesterData = User::where(array('_id' => $requestDetails->RequesterId))->select('FirstName', 'LastName', 'Email', 'EmailStatus', 'NoficationStatus', 'NotificationId', 'DeviceType', 'Name')->first();

			$transporterData = User::where(array('_id' => session::get('UserId')))->select('FirstName', 'LastName', 'Email', 'EmailStatus', 'NoficationStatus', 'NotificationId', 'DeviceType', 'Name')->first();

			$update_array = $requestDetails->ProductList;

			foreach ($requestDetails->ProductList as $key => $value) {
				if ($value['_id'] == $product_id) {
					$update_array[$key]['status'] = 'shipment_departed';

					$item_title = $update_array[$key]['product_name'];
					$packageid = $update_array[$key]['package_id'];
					$total = $update_array[$key]['after_update'];
					$shippingCost = $update_array[$key]['shippingCost'];
					$expected_date = $update_array[$key]['ExpectedDate'];
				}
			}

			// Update product list.
			Deliveryrequest::where($where)->update(['ProductList' => $update_array]);

			Reqhelper::update_status2($id);

			if ($requesterData->EmailStatus == "on") {
				$cron_mail2 = [
					'USERNAME' => ucfirst($requesterData->Name),
					'PACKAGETITLE' => ucfirst($item_title),
					'PACKAGEID' => $packageid,
					'SOURCE' => $requestDetails->PickupFullAddress,
					'DESTINATION' => $requestDetails->DeliveryFullAddress,
					'PACKAGENUMBER' => $packageid,
					'SHIPPINGCOST' => $shippingCost,
					'TOTALCOST' => $total,
					'DATE' => show_date($expected_date),
					'email_id' => '5e8af5659008c2c087ca3b6a',	// Email templete id.
					'email' => $requesterData->Email,
					'status' => 'ready',
				];
				SendMail::insert($cron_mail2);
			}

			if ($requesterData->NoficationStatus == 'on' && !empty($requesterData->NotificationId)) {
				$Notification = new Notify();
				$Notification->setValue('title', trans('lang.SHIPMENT_DEPARTED_TITLE'));
				$Notification->setValue('message', trans('lang.SHIPMENT_DEPARTED_MSG'));
				$Notification->setValue('type', 'requester_delivery_detail');	// request_detail
				$Notification->setValue('locationkey', $requestDetails->_id);
				$Notification->add_user($requesterData->NotificationId, $requesterData->DeviceType);
				$Notification->fire();
			}

			/* Activity Log send a package out for delivery */
			$insertactivity = [
				'request_id' => $requestDetails->_id,
				'request_type' => 'delivery',
				'PackageNumber' => $requestDetails->PackageNumber,
				'item_id' => $product_id,
				'package_id' => $packageid,
				'item_name' => $item_title,
				'log_type' => 'request',
				'message' => trans('lang.SHIPMENT_DEPARTED_MSG'),
				'status' => 'shipment_departed',
				'action_user_id'=> Session::get('UserId'),
				'EnterOn' => new MongoDate()
			];

			Activitylog::insert($insertactivity);

			// Store notification.
			Notification::insert([
				"NotificationTitle" => trans('lang.SHIPMENT_DEPARTED_TITLE'),
				"NotificationMessage" => trans('lang.SHIPMENT_DEPARTED_MSG'),
				"NotificationType" => "requester_delivery_detail",
				"NotificationUserId" => array(new MongoId($requesterData->_id)),
				"Date" => new MongoDate(),
				"GroupTo" => "User"
			]);

			$response = ['success' => 1, 'status' => 'shipment_departed', 'msg' => 'Success! Shipment departed originating country.'];
		}

		echo json_encode($response);
	}

}
