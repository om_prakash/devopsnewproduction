<?php namespace App\Modules\User\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Models\Activitylog;
use App\Http\Models\Category;
use App\Http\Models\CityStateCountry;
use App\Http\Models\Deliveryrequest;
use App\Http\Models\Notification;
use App\Http\Models\Configuration;
use App\Http\Models\SendMail;
use App\Http\Models\Setting;
use App\Http\Models\Trips;
use App\Http\Models\User;
use App\Library\Notification\Pushnotification;
use App\Library\Reqhelper;
use App\Library\RSStripe;
use App\Library\Utility;
use DateTime;
use Illuminate\Http\Request;
use Input;
use MongoDate;
use MongoId;
use Redirect;
use Session;
use Validator;
use App\Library\Notify;
use App\Library\NewEmail;

class Transporter extends Controller {

	public function __construct() {
		$user = User::where(["_id" => session()->get('UserId')])
			->where('delete_status', '!=', 'yes')
			->first();
		if (count($user) == 0) {
			Session::flush();
			return Redirect::to('login');
		}elseif(!session()->has('UserId') || !in_array(Session()->get('Usertype'), ['both', 'transporter'])) {
			return Redirect::to('login')->send();
		} elseif (in_array(Session()->get('Profile_Status'), ['step-one', 'step-two'])) {

			//return Redirect::to('become-transporter')->with('danger', "Your transporter account is not active because some items in your profile have not been completed. Once completed, we will review and<br> activate the Transporter section of your account")->send();

			$this->redirct_with_message('become-transporter', "Your transporter account is not active because some items in your profile have not been completed. Once completed, we will review and<br> activate the Transporter section of your account", 'danger');
		} elseif (in_array(Session()->get('Profile_Status'), ['step-third'])) {

			return Redirect::to('edit-bank-information')->with('danger', "Your transporter account is not active because some items in your profile have not been completed. Once completed, we will review and<br> activate the Transporter section of your account")->send();
		} elseif (Session()->get('Profile_Status') != 'complete') {
			return Redirect::to('dashboard')->with('danger', "Your account is being reviewed. Once completed, the Transporter features will be enabled.")
				->send();

		} elseif (session()->get('TPVerified') != 'active') {

			if (User::where(['_id' => Session()->get('UserId')])
				->where('TransporterStatus', '!=', 'active')
				->count() > 0) {
				return Redirect::to('dashboard')->with('danger', "Your account is being reviewed. Once completed, the Transporter features will be enabled.")
					->send();
			} else {
				session()->put('TPVerified', 'active');
			}
		}

	}

	public function redirct_with_message($url, $msg, $type) {
		return Redirect::to("$url")->with("$type", "$msg")->send();
	}

	public function new_request(Request $request) {
		
		// check session
		$user = User::where(["_id" => session()->get('UserId')])
			->where('delete_status', '!=', 'yes')
			->first();
			
		if (count($user) == 0) {
			Session::flush();
			return Redirect::to('login');
		}
		
		$data = ['lat' => 38.1769023, 'lng' => -107.272751];
		if (session::has('latitude')) {
			$data = [
				'lat' => floatval(session::get('latitude')),
				'lng' => floatval(session::get('longitude')),
			];
		}
		$query = Deliveryrequest::query();
		

		$query->where('RequesterId', '!=', new MongoId(Session::get('UserId')));
		$query->where(['Status' => 'ready', 'TransporterId' => '']);
		$query->whereNotIn('RequestType', ['online', 'buy_for_me', 'local_delivery']);

		//$query->where(array("ProductList" => array('$elemMatch' => array('tpid' =>Session::get('UserId')))));

		if (Input::get('search') != '') {
			$query->where('ProductTitle', 'like', '%' . trim(Input::get('search')) . '%');
		}

		if (!empty(trim(Input::get('search_address')))) {
			$query->where('PickupFullAddress', 'like', '%' . trim(Input::get('search_address')) . '%');
		}
		if (!empty(trim(Input::get('desti_address')))) {
			$query->where('DeliveryFullAddress', 'like', '%' . trim(Input::get('desti_address')) . '%');
		}

		if (!empty(trim(Input::get('lng'))) && Input::get('lng') != 0) {
			$data['lat'] = floatval(Input::get('lat'));
			$data['lng'] = floatval(Input::get('lng'));
		}
		$data['total_delivery'] = $query->count();
		$data['users'] = $query->get(['ProductTitle', 'PickupFullAddress', 'PickupDate', 'PickupLatLong', 'ProductImage', 'DeliveryFullAddress', 'DeliveryDate','ProductList']);

		// Point the map
		if (empty(trim(Input::get('lng'))) || Input::get('lng') == 0) {
			if (count($data['users']) > 0) {
				$data['lat'] = floatval($data['users'][0]['PickupLatLong'][1]);
				$data['lng'] = floatval($data['users'][0]['PickupLatLong'][0]);
			}
		}

		$data['postValue'] = "&search=" . Input::get('search');
		
		return view('User::list.new_request', $data);
	}

	public function my_deliveries() {
		$data['paginationUrl'] = "pagination/transporter/my-deliveries";
		$data['postValue'] = "&search=" . Input::get('search') . "&search_date=" . Input::get('search_date') . "&Status=" . Input::get('Status') . input::get('search_date2') . '&requester_name=' . Input::get('requester_name');
		//die;
		return view('User::list.my_deliveries', $data);
	}

	public function post_a_trip() {
		$id = Session()->get('UserId');
		$data['user_info'] = User::where(array('_id' => $id))->first();

		if (count($data['user_info']) > 0) {

			$data['country'] = CityStateCountry::where(['type' => 'Country', 'Status' => 'Active'])
				->orderBy('Content', 'Asc')
				->get(['_id', 'Content', 'state_available']);
			$data['category1'] = Category::where(['type' => 'Category', 'Status' => 'Active'])->whereIn('TravelMode', ['air'])
				->orderby('Content', 'asc')->get(['TravelMode', 'Content']);

			$data['category2'] = Category::where(['type' => 'Category', 'Status' => 'Active'])->whereIn('TravelMode', ['ship'])
				->orderby('Content', 'asc')->get(['TravelMode', 'Content']);

			return view('User::other.post_a_trip', $data);
		} else {}
	}

	public function my_individual_trips() {
		return view('User::list.individual_trips');
	}
	public function my_trips() {
		return view('User::list.my_trips');
	}

	public function edit_trip($id) {
		$data['country'] = CityStateCountry::where(['type' => 'Country', 'Status' => 'Active'])
			->orderBy('Content', 'Asc')
			->get(['_id', 'Content', 'state_available']);
		$data['category1'] = Category::where(['type' => 'Category', 'Status' => 'Active'])->whereIn('TravelMode', ['air'])->orderby('Content', 'asc')->get(['TravelMode', 'Content']);

		$data['category2'] = Category::where(['type' => 'Category', 'Status' => 'Active'])->whereIn('TravelMode', ['ship'])->orderby('Content', 'asc')->get(['TravelMode', 'Content']);
		$data['trip_data'] = Trips::find($id);
		if (count($data['trip_data']) > 0) {
			return view('User::edit.bussiness_trip', $data);
		} else {
			return redirect('transporter/bussiness-trips');
		}
	}

	public function edit_individual($id) {
		$data['country'] = CityStateCountry::where(['type' => 'Country', 'Status' => 'Active'])
			->orderBy('Content', 'Asc')
			->get(['_id', 'Content', 'state_available']);
		$data['category1'] = Category::where(['type' => 'Category', 'Status' => 'Active'])->whereIn('TravelMode', ['air'])->orderby('Content', 'asc')->get(['TravelMode', 'Content']);
		$data['category2'] = Category::where(['type' => 'Category', 'Status' => 'Active'])->whereIn('TravelMode', ['ship'])->orderby('Content', 'asc')->get(['TravelMode', 'Content']);
		$data['trip_data'] = Trips::find($id);
		if (count($data['trip_data']) > 0) {
			return view('User::edit.individual', $data);
		} else {
			return redirect('transporter/individual-trips');
		}
	}

	public function edit_individaul_trip($id) {
		$data = Input::all();
		$rules = ['address1' => 'required',
			'source_country' => 'required',
			'source_city' => 'required',
			'category1.0' => (Input::get('travel_mode') == 'air') ? 'required' : '',
			'category2.0' => (Input::get('travel_mode') == 'ship') ? 'required' : '',
		];
		$validater = Validator::make($data, $rules);

		if ($validater->fails()) {

			return redirect('transporter/edit_individual/' . $id)->withFail('Oops! Something went wrong.')->withErrors($validater);
		} else {

			$obj_source_country = json_decode(Input::get('source_country'));
			$obj_source_state = json_decode(Input::get('source_state'));
			$obj_desti_country = json_decode(Input::get('destination_country'));
			$obj_desti_state = json_decode(Input::get('destination_state'));

			$obj_source_city = json_decode(Input::get('source_city'));
			$obj_desti_city = json_decode(Input::get('destination_city'));

			$array = [
				'TransporterId' => new MongoId(Session()->get('UserId')),
				'SourceFullAddress' => Utility::formated_address([
					Input::get('address1'),
					@$obj_source_city->name,
					@$obj_source_state->name,
					@$obj_source_country->name,
				]
				),
				'SourceAddress' => Input::get('address1'),
				'SourceCountry' => @$obj_source_country->name,
				'SourceState' => @$obj_source_state->name,
				'SourceCity' => @$obj_source_city->name,
				'SourceDate' => get_utc_time(input::get('source_date')),
				'DestiAddress' => Input::get('address2'),
				'DestiCountry' => @$obj_desti_country->name,
				'DestiState' => @$obj_desti_state->name,
				'DestiCity' => @$obj_desti_city->name,
				'DestFullAddress' => Utility::formated_address([
					Input::get('address2'),
					@$obj_desti_city->name,
					@$obj_desti_state->name,
					@$obj_desti_country->name,
				]
				),
				'DestiDate' => get_utc_time(input::get('destination_date')),
				'TripType' => 'individual',
				'TravelMode' => Input::get('travel_mode'),
				'FlightNo' => Input::get('flight'),
				'Airline' => Input::get('airline'),
				'Description' => Input::get('description'),
				'Weight' => Input::get('weight'),
				'Unit' => Input::get('Unit'),
			];

			if (Input::get('travel_mode') == 'air') {
				$array['SelectCategory'] = array_map('ucfirst', (array_map('trim', Input::get('category1'))));
			} elseif (Input::get('travel_mode') == 'ship' && is_array(Input::get('category2'))) {
				$array['SelectCategory'] = array_map('ucfirst', (array_map('trim', Input::get('category2'))));
			}

			$update = Trips::where(array('_id' => $id))->update($array);

			return redirect('transporter/individual-trips')->withSuccess('Success! Trip has been updated successfully.');

		}
	}

	public function edit_bussiness_trip($id) {
		$data = Input::all();
		$rules = array
			(
			'travel_mode' => 'required',
			'description' => 'required',
			'weight' => 'required',
			'Unit' => 'required',
			//'flight' => (Input::get('travel_mode') == 'air') ? 'required' : '',
			//'airline' => (Input::get('travel_mode') == 'air') ? 'required' : '',
		);
		$validater = Validator::make($data, $rules);

		if ($validater->fails()) {

			return redirect('transporter/edit-trips/' . $id)->withFail('Oops! Something went wrong.');
		} else {
			$source_date = DateTime::createFromFormat('M d, Y h:i A', Input::get('source_date'))->format('d-m-Y h:i:s a');
			$desti_date = DateTime::createFromFormat('M d, Y h:i A', Input::get('destination_date'))->format('d-m-Y h:i:s a');

			$array['Category'] = '';

			$obj_source_country = json_decode(Input::get('source_country'));
			$obj_source_state = json_decode(Input::get('source_state'));
			$obj_desti_country = json_decode(Input::get('destination_country'));
			$obj_desti_state = json_decode(Input::get('destination_state'));
			$obj_source_city = json_decode(Input::get('source_city'));
			$obj_desti_city = json_decode(Input::get('destination_city'));

			$array = [
				'TransporterId' => new MongoId(Session()->get('UserId')),
				'SourceAddress' => Input::get('address1'),
				'SourceCountry' => @$obj_source_country->name,
				'SourceState' => @$obj_source_state->name,
				'SourceCity' => @$obj_source_city->name,
				'SourceDate' => new MongoDate(strtotime($source_date)),
				'DestiAddress' => Input::get('address2'),
				'DestiCountry' => @$obj_desti_country->name,
				'DestiState' => @$obj_desti_state->name,
				'DestiCity' => @$obj_desti_city->name,
				'DestiDate' => new MongoDate(strtotime($desti_date)),
				'TripType' => 'business',
				'TravelMode' => Input::get('travel_mode'),
				'FlightNo' => Input::get('flight'),
				'Airline' => Input::get('airline'),
				'Description' => Input::get('description'),
				'Weight' => Input::get('weight'),
				'Unit' => Input::get('Unit'),
			];

			if (Input::get('travel_mode') == 'air' && is_array(Input::get('category1'))) {
				$array['SelectCategory'] = array_map('ucfirst', (array_map('trim', Input::get('category1'))));
			} elseif (Input::get('travel_mode') == 'ship' && is_array(Input::get('category2'))) {
				$array['SelectCategory'] = array_map('ucfirst', (array_map('trim', Input::get('category2'))));
			}

			$update = Trips::where(array('_id' => $id))->update($array);

			return redirect('transporter/bussiness-trips/')->withSuccess('Success! Trip has been updated successfully.');

		}

	}

	public function insert_post_trip() {

		$data = Input::all();
		$users = User::where('_id', '=', Session()->get('UserId'))
			->select('RatingCount', 'RatingByCount')
			->first();
		if (count($users) > 0) {

			$rules = array(
				'description' => 'required',
				'weight' => 'required',
				'category1.0' => (Input::get('travel_mode') == 'air') ? 'required' : '',
				'category2.0' => (Input::get('travel_mode') == 'ship') ? 'required' : '',
				'Unit' => 'required',

			);
			$validater = Validator::make($data, $rules);

			if ($validater->fails()) {
				return redirect('transporter/post-a-trip')->withInput()
					->withErrors($validater);
			} else {

				$array['TripType'] = 'individual';
				$array['Category'] = '';

				$obj_source_country = json_decode(Input::get('source_country'));
				$obj_source_state = json_decode(Input::get('source_state'));
				$obj_source_city = json_decode(Input::get('source_city'));
				$obj_desti_country = json_decode(Input::get('destination_country'));
				$obj_desti_state = json_decode(Input::get('destination_state'));
				$obj_desti_city = json_decode(Input::get('destination_city'));

				$array = [
					'TransporterId' => new MongoId(Session()->get('UserId')),
					'TransporterName' => Session()->get('Name'),
					'TPRating' => 0,
					'TPImage' => Session()->get('Image'),
					'SourceFullAddress' => Utility::formated_address([
						Input::get('address_1'),
						@$obj_source_city->name,
						@$obj_source_state->name,
						@$obj_source_country->name,
					]
					),
					'SourceAddress' => Input::get('address_1'),
					'SourceCountry' => @$obj_source_country->name,
					'SourceState' => @$obj_source_state->name,
					'SourceCity' => @$obj_source_city->name,
					'SourceDate' => get_utc_time(Input::get('source_date')),
					'DestFullAddress' => Utility::formated_address([
						Input::get('address_2'),
						@$obj_desti_city->name,
						@$obj_desti_state->name,
						@$obj_desti_country->name,
					]
					),
					'DestiAddress' => Input::get('address_2'),
					'DestiCountry' => @$obj_desti_country->name,
					'DestiState' => @$obj_desti_state->name,
					'DestiCity' => @$obj_desti_city->name,
					'DestiDate' => get_utc_time(Input::get('destination_date')),
					'TravelMode' => Input::get('travel_mode'),
					'Status' => 'active',
					'FlightNo' => Input::get('flight'),
					'Airline' => Input::get('airline'),
					'Description' => Input::get('description'),
					'Weight' => Input::get('weight'),
					'Unit' => Input::get('Unit'),
					'SelectCategory' => [],
					'TripType' => 'individual',
					'EnterOn' => new MongoDate(),
					'UpdateDate' => new MongoDate(),
				];

				if ($users->RatingCount > 0 && $users->RatingByCount > 0) {
					$array['TPRating'] = $users->RatingCount / $users->RatingByCount;

				}

				if (Input::get('transportertype') == 'business') {
					$array['TripType'] = 'business';
				}

				if (Input::get('travel_mode') == 'air') {
					if (is_array(Input::get('category1')) && count(Input::get('category1')) > 0) {
						$array['SelectCategory'] = array_map('ucfirst', (array_map('trim', Input::get('category1'))
						));
					} else {
						return redirect('transporter/post-a-trip')->withInput();
					}
				} elseif (Input::get('travel_mode') == 'ship') {
					if (is_array(Input::get('category2')) && count(Input::get('category2')) > 0) {
						$array['SelectCategory'] = array_map('ucfirst', (array_map('trim', Input::get('category2')))
						);
					} else {
						return redirect('transporter/post-a-trip')->withInput();
					}
				}

				Trips::insert($array);
				$setting = Setting::where(["_id" => '563b0e31e4b03271a097e1ca'])->first();
				if (count($setting) > 0) {
					$adminETemplate = [
						"to" => $setting->SupportEmail,
						"replace" => [
							"[TPNAME]" => ucfirst(Session()->get('Name')),
							"[SOURCE]" => $this->get_formatted_address(array($array['SourceAddress'], $array['SourceCity'], $array['SourceState'], $array['SourceCountry'])),
							"[SOURCEDATE]" => isset($array['SourceDate']->sec) ? date('d M,Y h:i A', $array['SourceDate']->sec) : '',
							"[DESTINATION]" => $this->get_formatted_address(array($array['DestiAddress'], $array['DestiCity'],
								$array['DestiState'], $array['DestiCountry'])),
							"[DESTINATIONDATE]" => isset($array['DestiDate']->sec) ? date('d M,Y h:i A', $array['DestiDate']->sec) : '',
							"[TRAVELMODE]" => ucfirst($array['TravelMode']),
							"[CATEGORY]" => implode(",", $array['SelectCategory']),
							"[FLIGHTNO]" => isset($array['FlightNo']) ? $array['FlightNo'] : "Not Applicable",
							"[AIRLINE]" => isset($array['Airline']) ? $array['Airline'] : "Not Applicable",
							"[WEIGHT]" => isset($array['Weight']) ? $array['Weight'] . ' ' . $array['Unit'] : "Not Applicable",
							"[DESCRIPTION]" => $array['Description'],
						],
					];
					send_mail('56cd43825509251cd677740e', $adminETemplate);
				}

				if (Input::get('transportertype') == 'business') {
					return redirect('transporter/bussiness-trips')->withSuccess('Success! Trip has been added successfully.');
				} else {
					return redirect('transporter/individual-trips')->withSuccess('Success! Trip has been added successfully.');
				}

			}

		}
	}

	public function get_formatted_address($array, $zipcode = '') {
		$address = '';
		foreach ($array as $key) {
			if (!empty($key)) {
				$address .= (($address != '') ? ", $key" : $key);
			}
		}
		if (!empty($zipcode)) {$address .= " - $zipcode";}
		return $address;
	}
	public function packageDetail($id) {

		$result = Deliveryrequest::where('_id', '=', $id)->first();
		$query = Deliveryrequest::query();
		$query->where(array('_id' => $id));
		if (count($result) > 0) {
			$data['user_data'] = User::where(['_id' => $result->RequesterId])->first();
			$data['package'] = $result;
			if (!count($data['user_data']) > 0) {
				return redirect('transporter');
			}
		}
		return view('User::detail.abc', $data);
	}
	public function details($id) {
		$request_data = Deliveryrequest::where(['_id' => $id])->get();
		
		if (count($request_data) > 0 ) {

			$data['transporterDetail'] = $request_data;
			$data['user_data'] = User::where(['_id' => $data['transporterDetail'][0]->RequesterId])->first();
			if (count($data['transporterDetail']) > 0) {
				if (!count($data['user_data']) > 0) {
					return redirect('transporter/my-deliveries');
				}
				return view('User::detail.transporter_request_detail', $data);
			}
		} else {

			$query = Deliveryrequest::query();
			$where = array("ProductList" => array('$elemMatch' => array('_id' => $id)));

			$data['transporterDetail'] = $query->where($where)->select('RequesterName', 'RequestType', 'ProductList.$', 'RequesterId', 'DeliveryDate', 'DeliveryState', 'DeliveryAddress', 'DeliveryFullAddress', 'DeliveryCountry', 'DeliveryPincode', 'ReceiverCountrycode', 'ReceiverMobileNo', 'country_code', 'shippingCost', 'insurance', 'discount', 'distance', 'TransporterId', 'TransporterName', '_id', 'tpid', 'ReturnAddress', 'itemCount', 'AquantuoFees','OtherImage','Status','ProductImage')->first();
			$data['user_data'] = User::where(['_id' => $data['transporterDetail']->RequesterId])->first();
			if (count($data['transporterDetail']) > 0) {
				if (!count($data['user_data']) > 0) {
					return redirect('transporter/my-deliveries');
				}
				return view('User::send_package.transporter_request_detail', $data);
			}
			
		}

		return redirect('transporter/my-deliveries');

		/*if (count($data['transporterDetail']) > 0) {
			if (!count($data['user_data']) > 0) {
				return redirect('transporter/my-deliveries');
			}

			if(isset($data['transporterDetail']->))

			if (count($request_data) > 0) {
				return view('User::detail.transporter_request_detail', $data);
			} else {

				return view('User::send_package.transporter_request_detail', $data);
			}

		} else {
			
		}*/
	}

	public function localDetails($id) {

		$query = Deliveryrequest::query();
		$where = array("ProductList" => array('$elemMatch' => array('_id' => $id)));

		$data['transporterDetail'] = $query->where($where)->select('RequesterName', 'RequestType', 'ProductList.$', 'RequesterId', 'DeliveryDate', 'DeliveryState', 'DeliveryAddress', 'DeliveryFullAddress', 'DeliveryCountry', 'DeliveryPincode', 'ReceiverCountrycode', 'ReceiverMobileNo', 'country_code', 'shippingCost', 'insurance', 'discount', 'distance', 'TransporterId', 'TransporterName', '_id', 'tpid', 'ReturnAddress', 'itemCount', 'AquantuoFees','AreaCharges')->first();

		

		$configurationdata = Configuration::where(['_id' => '5673e33e6734c4f874685c84'])->first();
		$Deliveryrequest = Deliveryrequest::where($where)->select('ProductList')->first();
		$data['totalItemCount'] = count($Deliveryrequest->ProductList);
		$data['averageAreaCharge'] = number_format($data['transporterDetail']['AreaCharges'] / $data['totalItemCount'],2) ;
		$data['itemCost'] = $data['transporterDetail']['ProductList'][0]['after_update'] + $data['averageAreaCharge'];
		$data['aquantuoFess'] = ($data['itemCost'] / 100 ) * $configurationdata->aquantuo_fees_ld;
		$data['transporterFess'] = $data['itemCost'] - $data['aquantuoFess'];

		
		
		


		if (count($data['transporterDetail']) > 0) {
			$data['user_data'] = User::where(['_id' => $data['transporterDetail']->RequesterId])->first();
			return view('User::local_delivery.transporter_request_detail', $data);
		} else {
			return redirect('transporter/my-deliveries');
		}
	}

	public function buy_for_details($id) {

		$query = Deliveryrequest::query();

		$where = array("ProductList" => array('$elemMatch' => array('_id' => $id)));

		//$query->where(array('_id'  => $id));
		//$query->wherein('TransporterId',[new MongoId(Session()->get('UserId')),'']);
		//$query->where('RequesterId','!=',new MongoId(Session()->get('UserId')));

		$data['transporterDetail'] = $query->where($where)->select('RequesterName', 'ProductList.$', 'RequesterId', 'DeliveryDate', 'DeliveryState', 'DeliveryAddress', 'DeliveryFullAddress', 'DeliveryCountry', 'DeliveryPincode', 'ReceiverCountrycode', 'ReceiverMobileNo', 'country_code', 'shippingCost', 'insurance', 'discount', 'distance', 'TransporterId', 'TransporterName', '_id', 'tpid', 'ReturnAddress')->first();

		if (count($data['transporterDetail']) > 0) {

			$data['user_data'] = User::where(['_id' => $data['transporterDetail']->RequesterId])->first();

			if (!count($data['user_data']) > 0) {
				return redirect('transporter/my-deliveries');
			}
			return view('User::detail.transporter_buy_for_me_detail', $data);
		} else {
			return redirect('transporter/my-deliveries');
		}
	}

	public function online_details($id) {

		$query = Deliveryrequest::query();

		$where = array("ProductList" => array('$elemMatch' => array('_id' => $id)));

		$data['transporterDetail'] = $query->where($where)->select('ProductList.$', 'RequesterId', 'DeliveryDate', 'DeliveryState', 'DeliveryFullAddress', 'DeliveryCountry','ReceiverName', 'DeliveryPincode', 'ReceiverCountrycode', 'ReceiverMobileNo', 'country_code', 'shippingCost', 'insurance', 'discount', 'ReturnAddress', 'EnterOn', 'distance', 'RequesterName', 'tracking_number')->first();

		if (count($data['transporterDetail']) > 0) {

			$data['user_data'] = User::where(['_id' => (string) $data['transporterDetail']->RequesterId])->first();

			if (count($data['user_data']) > 0) {
				return view('User::detail.transporter_online_request_detail', $data);
			} else {
				return redirect('transporter/my-deliveries');
			}

		} else {
			return redirect('transporter/my-deliveries');
		}
	}

	public function request_accept_by_transporter($requestId, Request $request)
    {
        $response = array("success" => 0, "msg" => "Something went wrong.");
        $id = session::get('UserId');
        $requestid = Deliveryrequest::where(array(
            '_id' => new MongoId($requestId), 'Status' => 'ready'))->select('RequesterId', 'Status', 'ProductTitle', 'PackageId', 'PackageNumber', 'shippingCost', 'TotalCost', 'PickupFullAddress', 'DeliveryFullAddress')->first();

        if (count($requestid) > 0) {
            $requesterData = User::where(array('_id' => $requestid->RequesterId))->
                select('FirstName', 'LastName', 'Email', 'EmailStatus', 'NoficationStatus', 'NotificationId', 'DeviceType')->first();

            $transporterData = User::where(array('_id' => $id))->select('FirstName', 'LastName', 'Email', 'EmailStatus', 'NoficationStatus', 'NotificationId' . 'DeviceType', 'Name')->first();

            if (count($requesterData) > 0 && count($transporterData) > 0) {
                $update['Status'] = 'accepted';
                $update['TransporterName'] = $transporterData->FirstName . ' ' . $transporterData->LastName;
                $update['TransporterId'] = new MongoId($id);

                $updateData = Deliveryrequest::where(array('_id' => new MongoId($requestId)))->update($update);
                //email to transporter
                if ($transporterData->EmailStatus == "on") {
                    /*send_mail('5694ce6e5509251cd67773ed', [
                        "to" => $transporterData->Email,
                        "replace" => [
                            "[USERNAME]" => $transporterData->Name,
                            "[PACKAGETITLE]" => ucfirst($requestid->ProductTitle),
                            "[PACKAGEID]" => $requestid->PackageId,
                            "[SOURCE]" => $requestid->PickupFullAddress,
                            "[DESTINATION]" => $requestid->DeliveryFullAddress,
                            "[PACKAGENUMBER]" => $requestid->PackageNumber,
                            "[SHIPPINGCOST]" => $requestid->shippingCost,
                            "[TOTALCOST]" => $requestid->TotalCost,
                        ],
                    ]
                    );*/

                    $cron_mail =[
                        "USERNAME" => ucfirst($transporterData->Name),
                        "PACKAGETITLE" => ucfirst($requestid->ProductTitle),
                        "PACKAGEID" => $requestid->PackageId,
                        "SOURCE" => $requestid->PickupFullAddress,
                        "DESTINATION" => $requestid->DeliveryFullAddress,
                        "PACKAGENUMBER" => $requestid->PackageNumber,
                        "SHIPPINGCOST" => $requestid->shippingCost,
                        "TOTALCOST" => $requestid->TotalCost,
                        'email_id'=>'5694ce6e5509251cd67773ed',
                        'email'=>$transporterData->Email,
                        'status'=>'ready',
                        'REQUESTER'=>ucfirst($requesterData->Name),
                    ];

                    SendMail::insert($cron_mail);


                }
                //email to requester
                if ($requesterData->EmailStatus == "on") {
                    /*send_mail('57d7d4b37ac6f69c158b4569', [
                        "to" => $requesterData->Email,
                        "replace" => [
                            "[USERNAME]" => $requesterData->Name,
                            "[PACKAGETITLE]" => ucfirst($requestid->ProductTitle),
                            "[PACKAGEID]" => $requestid->PackageNumber,
                            "[SOURCE]" => $requestid->PickupFullAddress,
                            "[DESTINATION]" => $requestid->DeliveryFullAddress,
                            "[PACKAGENUMBER]" => $requestid->PackageNumber,
                            "[SHIPPINGCOST]" => $requestid->shippingCost,
                            "[TOTALCOST]" => $requestid->TotalCost,
                        ],
                    ]);*/

                    $cron_mail2 =[
                        "USERNAME" => ucfirst($requesterData->Name),
                        "PACKAGETITLE" => ucfirst($requestid->ProductTitle),
                        "PACKAGEID" => $requestid->PackageId,
                        "SOURCE" => $requestid->PickupFullAddress,
                        "DESTINATION" => $requestid->DeliveryFullAddress,
                        "PACKAGENUMBER" => $requestid->PackageNumber,
                        "SHIPPINGCOST" => $requestid->shippingCost,
                        "TOTALCOST" => $requestid->TotalCost,
                        'email_id'=>'57d7d4b37ac6f69c158b4569',
                        'email'=>$requesterData->Email,
                        'status'=>'ready',
                    ];

                    SendMail::insert($cron_mail2);
                }
                //email to admin
                $setting = Setting::find('563b0e31e4b03271a097e1ca');

                if (count($setting) > 0) {
                    send_mail('56ab46f95509251cd67773f3', [
                        "to" => $setting->SupportEmail,
                        "replace" => [
                            "[TRANSPORTERNAME]" => $transporterData->Name,
                            "[PACKAGETITLE]" => ucfirst($requestid->ProductTitle),
                            "[PACKAGEID]" => $requestid->PackageNumber,
                            "[SOURCE]" => $requestid->PickupFullAddress,
                            "[DESTINATION]" => $requestid->DeliveryFullAddress,
                        ],
                    ]);
                }

                //Notification to requester

                if ($requesterData->NoficationStatus == "on" && !empty($requesterData->NotificationId)) {
                    $Notification = new Notify();
                    $Notification->setValue('title', trans('lang.SEND_ACCEPT_TITLE'));
                    $Notification->setValue('message', sprintf(trans('lang.SEND_ACCEPT_MSG'), $transporterData->Name, $requestid->ProductTitle));
                    $Notification->setValue('type', 'request_detail');
                    $Notification->setValue('locationkey', $requestId);
                    $Notification->add_user($requesterData->NotificationId, $requesterData->DeviceType);
                    $Notification->fire();
                }
                //insertNotification

                Notification::Insert([
                    array(
                        "NotificationTitle" => trans('lang.SEND_ACCEPT_TITLE'),
                        "NotificationShortMessage" => 'Request accepted successfully.',
                        "NotificationMessage" => sprintf(trans('lang.SEND_ACCEPT_MSG'), $transporterData->Name, $requestid->ProductTitle),
                        "NotificationType" => "request",
                        "NotificationUserId" => array(new MongoId($requesterData->_id)),
                        "Date" => new MongoDate(),
                        "GroupTo" => "User",
                    ),
                    array(
                        "NotificationTitle" => 'Request was accepted',
                        "NotificationMessage" => sprintf(trans('lang.SEND_ACCEPT_MSG_ADMIN'), $transporterData->Name, $requestid->PickupFullAddress, $requestid->DeliveryFullAddress),
                        "NotificationUserId" => array(),
                        "NotificationReadStatus" => 0,
                        "location" => "request_detail",
                        "locationkey" => (string) $requestId,
                        "Date" => new MongoDate(),
                        "GroupTo" => "Admin",
                    ),
                ]);

                if (count($updateData) > 0) {

                    $response['success'] = 1;
                    $response['status'] = $update['Status'];
                    $response['msg'] = "<strong>Success!</strong> Request has been accepted successfully.";
                }

            }
        }

        echo json_encode($response);

    }

	public function cancel_request($requestId, Request $request) {

		$response = array("success" => 0, "msg" => "Something went wrong.");
		$id = session::get('UserId');
		$requestid = Deliveryrequest::where(array('_id' => $requestId))
			->select('RequesterId', 'ProductTitle', 'Status', 'PackageId', 'PickupFullAddress', 'DeliveryFullAddress', 'PackageNumber', 'shippingCost', 'TotalCost')->first();

		if (count($requestid) > 0) {
			if (!in_array($requestid->Status, ['accepted', 'out_for_pickup'])) {
				$response['msg'] = 'You can cancel request only accepted or out for pickup.';
			} else {
				$requesterData = User::where(array('_id' => $requestid->RequesterId))->
					select('FirstName', 'LastName', 'Email', 'EmailStatus', 'NoficationStatus', 'NotificationId', 'DeviceType')->first();

				$transporterData = User::where(array('_id' => $id))->select('FirstName', 'LastName', 'Email', 'EmailStatus', 'NoficationStatus', 'NotificationId' . 'DeviceType', 'Name')->first();

				if (count($requesterData) > 0 && count($transporterData)) {
					Deliveryrequest::where(array('_id' => new MongoId($requestId)))
						->update([
							'Status' => 'ready',
							'TransporterName' => '',
							'TransporterId' => '',
						]);
					$response['success'] = 1;
					$response['msg'] = "Success! Request has been canceled successfully.";

					//email to transporter
					if ($transporterData->EmailStatus == "on") {
						send_mail('57e8c112e4b01d01c916d24a', [
							"to" => $transporterData->Email,
							"replace" => [
								"[USERNAME]" => $transporterData->Name,
								"[PACKAGETITLE]" => ucfirst($requestid->ProductTitle),
								"[PACKAGEID]" => $requestid->PackageId,
								"[SOURCE]" => $requestid->PickupFullAddress,
								"[DESTINATION]" => $requestid->DeliveryFullAddress,
								"[PACKAGENUMBER]" => $requestid->PackageNumber,
								"[SHIPPINGCOST]" => $requestid->shippingCost,
								"[TOTALCOST]" => $requestid->TotalCost,
							],
						]);
					}
					//email to requester
					if ($requesterData->EmailStatus == "on") {
						send_mail('57e8c3dae4b01d01c916d24c', [
							"to" => $requesterData->Email,
							"replace" => [
								"[USERNAME]" => $requesterData->FirstName . ' ' . $requesterData->LastName,
								"[PACKAGETITLE]" => ucfirst($requestid->ProductTitle),
								"[PACKAGEID]" => $requestid->PackageId,
								"[SOURCE]" => $requestid->PickupFullAddress,
								"[DESTINATION]" => $requestid->DeliveryFullAddress,
								"[PACKAGENUMBER]" => $requestid->PackageNumber,
								"[SHIPPINGCOST]" => $requestid->shippingCost,
								"[TOTALCOST]" => $requestid->TotalCost,
							],
						]);
					}

					//email to admin
					$setting = Setting::find('563b0e31e4b03271a097e1ca');

					if (count($setting) > 0) {
						send_mail('57e8c3f5e4b01d01c916d24e', [
							"to" => $setting->SupportEmail,
							"replace" => [
								"[USERNAME]" => $transporterData->Name,
								"[PACKAGETITLE]" => ucfirst($requestid->ProductTitle),
								"[PACKAGEID]" => $requestid->PackageNumber,
								"[TRANSPORTERNAME]" => ucfirst($transporterData->Name),
								"[DESTINATION]" => ucfirst($requestid->DeliveryFullAddress),
							],
						]);
					}

					$Notification = new Notify();

					//Notification to requester
					if ($requesterData->NoficationStatus == "on" && !empty($requesterData->NotificationId)) {
						$Notification->setValue('title', 'Request canceled');
						$Notification->setValue('message', sprintf('The request of send a package title: "%s", ID: %s has been canceled by transporter.', $requestid->ProductTitle, $requestid->PackageNumber));
						$Notification->setValue('type', 'request_detail');
						$Notification->setValue('locationkey', $requestId);
						$Notification->add_user($requesterData->NotificationId, $requesterData->DeviceType);
						$Notification->fire();
					}
					/* Activity Log send a package cancel */
					$where = [
						"ProductList" => ['$elemMatch' => [
							'_id' => $product_id,
						]],
					];
					$activityLog = Deliveryrequest::where('_id', '=', $requestId)->where($where)->select('ProductList.$', 'RequestType', 'PackageNumber')->first();
					$insertactivity = [
						'request_id' => $requestId,
						'request_type' => 'delivery',
						'PackageNumber' => $activityLog->PackageNumber,
						'item_id' => $product_id,
						'package_id' => $activityLog['ProductList'][0]['package_id'],
						'item_name' => $activityLog['ProductList'][0]['product_name'],
						'log_type' => 'request',
						'message' => 'Request has been cancel.',
						'status' => 'cancel',
						'action_user_id'=> Session::get('UserId'),
						'EnterOn' => new MongoDate(),
					];

					Activitylog::insert($insertactivity);
					//insertNotification
					Notification::Insert([
						array(
							"NotificationTitle" => 'Request canceled',
							"NotificationMessage" => sprintf('The request of send a package title: "%s", ID: %s has been canceled by transporter.', $requestid->ProductTitle, $requestid->PackageNumber),
							"NotificationType" => "request_detail",
							"NotificationUserId" => array(new MongoId($requesterData->_id)),
							"Date" => new MongoDate(),
							"GroupTo" => "User",
						),
						array(
							"NotificationTitle" => 'Request canceled',
							"NotificationMessage" => sprintf('The request of send a package title: "%s", ID: %s has been canceled by transporter.', $requestid->ProductTitle, $requestid->PackageNumber),
							"NotificationUserId" => array(),
							"NotificationReadStatus" => 0,
							"location" => "request_detail",
							"locationkey" => (string) $requestId,
							"Date" => new MongoDate(),
							"GroupTo" => "Admin",
						),
					]);
				}
			}
		}

		echo json_encode($response);
	}

	public function pickup_request($requestId, Request $request)
    {
        $response = array("success" => 0, "msg" => "Something went wrong.");
        $id = session::get('UserId');

        $requestid = Deliveryrequest::where(array('_id' => new MongoId($requestId)))
            ->select('RequesterId', 'ProductTitle', 'Status', 'ProductList', 'PackageId', 'PickupFullAddress', 'DeliveryFullAddress', 'PackageNumber', 'shippingCost', 'TotalCost')->first();
        if (count($requestid) > 0) {
            $requesterData = User::where(array('_id' => $requestid->RequesterId))->
                select('FirstName', 'LastName', 'Email', 'EmailStatus', 'NoficationStatus', 'NotificationId', 'DeviceType')->first();

            $transporterData = User::where(array('_id' => $id))->select('FirstName', 'LastName', 'Email', 'EmailStatus', 'NoficationStatus', 'NotificationId' . 'DeviceType', 'Name')->first();
            if (count($requesterData) > 0 && count($transporterData)) {

                $updateData = Deliveryrequest::where(array('_id' => new MongoId($requestId)))
                    ->update(['TransporterName' => $transporterData->Name, 'TransporterId' => new MongoId($id), 'Status' => 'out_for_pickup']);

                //email to transporter
                if ($transporterData->EmailStatus == "on") {
                    /*send_mail('5694cf3c5509251cd67773ee', [
                        "to" => $transporterData->Email,
                        "replace" => [
                            "[USERNAME]" => ucfirst($transporterData->Name),
                            "[PACKAGETITLE]" => ucfirst($requestid->ProductTitle),
                            "[PACKAGEID]" => $requestid->PackageId,
                            "[SOURCE]" => $requestid->PickupFullAddress,
                            "[DESTINATION]" => $requestid->DeliveryFullAddress,
                            "[PACKAGENUMBER]" => $requestid->PackageNumber,
                            "[SHIPPINGCOST]" => $requestid->shippingCost,
                            "[TOTALCOST]" => $requestid->TotalCost,
                        ],
                    ]);*/
                    /*insert mail*/
                    $cron_mail =[
                        "USERNAME" => ucfirst($transporterData->Name),
                        "PACKAGETITLE" => ucfirst($requestid->ProductTitle),
                        "PACKAGEID" => $requestid->PackageId,
                        "SOURCE" => $requestid->PickupFullAddress,
                        "DESTINATION" => $requestid->DeliveryFullAddress,
                        "PACKAGENUMBER" => $requestid->PackageNumber,
                        "SHIPPINGCOST" => $requestid->shippingCost,
                        "TOTALCOST" => $requestid->TotalCost,
                        'email_id'=>'5694cf3c5509251cd67773ee',
                        'email'=>$transporterData->Email,
                        'status'=>'ready',
                        'REQUESTER'=>ucfirst($requesterData->Name),
                    ];

                    SendMail::insert($cron_mail);
                    /*end ins*/
                }
                //email to requester
                if ($requesterData->EmailStatus == "on") {
                    /*send_mail('5694cb805509251cd67773ec', [
                        "to" => $requesterData->Email,
                        "replace" => [
                            "[USERNAME]" => ucfirst($requesterData->Name),
                            "[PACKAGETITLE]" => ucfirst($requestid->ProductTitle),
                            "[PACKAGEID]" => $requestid->PackageId,
                            "[SOURCE]" => $requestid->PickupFullAddress,
                            "[DESTINATION]" => $requestid->DeliveryFullAddress,
                            "[PACKAGENUMBER]" => $requestid->PackageNumber,
                            "[SHIPPINGCOST]" => $requestid->shippingCost,
                            "[TOTALCOST]" => $requestid->TotalCost,
                        ],
                    ]);*/

                    /*insert mail*/
                    $cron_mail2 =[
                        "USERNAME" => ucfirst($requesterData->Name),
                        "PACKAGETITLE" => ucfirst($requestid->ProductTitle),
                        "PACKAGEID" => $requestid->PackageId,
                        "SOURCE" => $requestid->PickupFullAddress,
                        "DESTINATION" => $requestid->DeliveryFullAddress,
                        "PACKAGENUMBER" => $requestid->PackageNumber,
                        "SHIPPINGCOST" => $requestid->shippingCost,
                        "TOTALCOST" => $requestid->TotalCost,
                        'email_id'=>'5694cb805509251cd67773ec',
                        'email'=>$requesterData->Email,
                        'status'=>'ready',
                    ];

                    SendMail::insert($cron_mail2);
                    /*end ins*/
                }
                //email to admin
                /*send_mail('57d7d6757ac6f6b5158b4567', [
                "to" => 'admin@aquantuo.com',
                "replace" => [
                "[USERNAME]" => $transporterData->Name,
                "[PACKAGETITLE]" => ucfirst($requestid->ProductTitle),
                ],
                ]);*/

                //Notification to requester
                if ($requesterData->NoficationStatus == "on" && !empty($requesterData->NotificationId)) {
                    $Notification = new Notify();

                    //$Notification->setValue('title', trans('lang.SEND_OUTFORPICKUP_TITLE'));
                    //$Notification->setValue('message', trans('lang.SEND_OUTFORPICKUP_MSG'));
                    //$Notification->setValue('type', 'request_detail');
                    //$Notification->setValue('locationkey', $requestId);
                    //$Notification->add_user($requesterData->NotificationId, $requesterData->DeviceType);
                    //$Notification->fire();

                    $cron_noti =[
                        "title" => trans('lang.SEND_OUTFORPICKUP_TITLE'),
                        "message" => trans('lang.SEND_OUTFORPICKUP_MSG'),
                        "type" => 'request_detail',
                        "locationkey" => $requestId,
                        "NotificationId" => $requesterData->NotificationId,
                        "DeviceType" => $requesterData->DeviceType,
                        'status'=>'ready',
                        'by_mean'=>'notification'
                    ];
                    SendMail::insert($cron_noti);
                }
                //insertNotification
                Notification::Insert([
                    array(
                        "NotificationTitle" => trans('lang.SEND_OUTFORPICKUP_TITLE'),
                        "NotificationShortMessage" => 'Request pickedup successfully.',
                        "NotificationMessage" => trans('lang.SEND_OUTFORPICKUP_MSG'),
                        "NotificationType" => "request",
                        "NotificationUserId" => array(new MongoId($requesterData->_id)),
                        "Date" => new MongoDate(),
                        "GroupTo" => "User",
                    ),
                    array(
                        "NotificationTitle" => trans('lang.SEND_OUTFORPICKUP_TITLE'),
                        "NotificationMessage" => sprintf(trans('lang.SEND_OUTFORPICKUP_ADMIN'), $transporterData->Name, $requestid->PickupFullAddress, $requestid->DeliveryFullAddress),
                        "NotificationUserId" => array(),
                        "NotificationReadStatus" => 0,
                        "location" => "request_detail",
                        "locationkey" => (string) $requestId,
                        "Date" => new MongoDate(),
                        "GroupTo" => "Admin",
                    ),
                ]);

                if (count($updateData) > 0) {
                    $response['success'] = 1;
                    $response['status'] = 'out_for_pickup';
                    $response['msg'] = "<strong>Success!</strong> Request has been pickedup successfully.";
                }

            }
        }

        echo json_encode($response);
    }
	public function delivery_request($requestId, Request $request)
    {

        $response = array("success" => 0, "msg" => "Something went wrong.");
        $id = session::get('UserId');
        $requestid = Deliveryrequest::where(array(
            '_id' => new MongoId($requestId), 'Status' => 'out_for_pickup'))->select('RequesterId', 'ProductTitle', 'Status', 'PackageId', 'PickupFullAddress', 'DeliveryFullAddress', 'PackageNumber', 'shippingCost', 'TotalCost', 'StripeChargeId', 'StripeChargeId2')->first();

        if (count($requestid) > 0) {
            if (!empty(trim($requestid->StripeChargeId))) {

                $res = RSStripe::retrive_amount($requestid->StripeChargeId);
                if (!isset($res['id'])) {
                    $res = RSStripe::find_charge($requestid->StripeChargeId);

                    if ($res['captured'] != 1 || $res['captured'] != false) {
                        if (!isset($res['id'])) {
                            $response = ['success' => 0, 'msg' => $res];
                            return response()->json($response);
                            die;
                        }

                    }
                }
            }

            if (!empty(trim($requestid->StripeChargeId2))) {

                $res = RSStripe::retrive_amount($requestid->StripeChargeId2);
                if (!isset($res['id'])) {
                    $res = RSStripe::find_charge($requestid->StripeChargeId2);

                    if ($res['captured'] != 1 || $res['captured'] != false) {
                        if (!isset($res['id'])) {
                            $response = ['success' => 0, 'msg' => $res];
                            return response()->json($response);
                            die;
                        }

                    }
                }
            }
            $requesterData = User::where(array('_id' => $requestid->RequesterId))
                ->select('FirstName', 'LastName', 'Email', 'EmailStatus', 'NoficationStatus', 'NotificationId', 'DeviceType', 'Name')
                ->first();

            $transporterData = User::where(array('_id' => $id))->select('FirstName', 'LastName', 'Email', 'EmailStatus', 'NoficationStatus', 'NotificationId', 'DeviceType', 'Name')->first();

            if (count($requesterData) > 0 && count($transporterData)) {
                $update['PaymentStatus'] = 'Success';
                $update['Status'] = 'out_for_delivery';
                $update['TransporterName'] = $transporterData->FirstName . ' ' . $transporterData->LastName;
                $update['TransporterId'] = new MongoId($id);
                $updateData = Deliveryrequest::where(array('_id' => new MongoId($requestId)))
                    ->update($update);

                //email to transporter
                if ($transporterData->EmailStatus == "on") {
                    /*send_mail('5694cffb5509251cd67773ef', [
                        "to" => $transporterData->Email,
                        "replace" => [
                            "[USERNAME]" => ucfirst($transporterData->Name),
                            "[PACKAGETITLE]" => ucfirst($requestid->ProductTitle),
                            "[PACKAGEID]" => $requestid->PackageId,
                            "[SOURCE]" => $requestid->PickupFullAddress,
                            "[DESTINATION]" => $requestid->DeliveryFullAddress,
                            "[PACKAGENUMBER]" => $requestid->PackageNumber,
                            "[SHIPPINGCOST]" => $requestid->shippingCost,
                            "[TOTALCOST]" => $requestid->TotalCost,
                        ],
                    ]);*/
                    $cron_mail =[
                        "USERNAME" => ucfirst($transporterData->Name),
                        "PACKAGETITLE" =>ucfirst($requestid->ProductTitle),
                        "PACKAGEID" => $requestid->PackageId,
                        "SOURCE" => $requestid->PickupFullAddress,
                        "DESTINATION" => $requestid->DeliveryFullAddress,
                        "PACKAGENUMBER" => $requestid->PackageNumber,
                        "SHIPPINGCOST" => $requestid->shippingCost,
                        "TOTALCOST" => $requestid->TotalCost,
                        'email_id'=>'5694cffb5509251cd67773ef',
                        'email'=>$transporterData->Email,
                        'status'=>'ready',
                        'REQUESTER'=>ucfirst($requesterData->Name),
                    ];

                    SendMail::insert($cron_mail);
                }
                //email to requester
                if ($requesterData->EmailStatus == "on") {
                    /*send_mail('562228e2e4b0252ad07d07a2', [
                        "to" => $requesterData->Email,
                        "replace" => [
                            "[USERNAME]" => ucfirst($requesterData->Name),
                            "[PACKAGETITLE]" => ucfirst($requestid->ProductTitle),
                            "[PACKAGEID]" => $requestid->PackageId,
                            "[SOURCE]" => $requestid->PickupFullAddress,
                            "[DESTINATION]" => $requestid->DeliveryFullAddress,
                            "[PACKAGENUMBER]" => $requestid->PackageNumber,
                            "[SHIPPINGCOST]" => $requestid->shippingCost,
                            "[TOTALCOST]" => $requestid->TotalCost,
                        ],
                    ]);*/

                    $cron_mail2 =[
                        "USERNAME" => ucfirst($requesterData->Name),
                        "PACKAGETITLE" => ucfirst($requestid->ProductTitle),
                        "PACKAGEID" => $requestid->PackageId,
                        "SOURCE" => $requestid->PickupFullAddress,
                        "DESTINATION" => $requestid->DeliveryFullAddress,
                        "PACKAGENUMBER" => $requestid->PackageNumber,
                        "SHIPPINGCOST" => $requestid->shippingCost,
                        "TOTALCOST" => $requestid->TotalCost,
                        'email_id'=>'562228e2e4b0252ad07d07a2',
                        'email'=>$requesterData->Email,
                        'status'=>'ready',
                    ];
                    SendMail::insert($cron_mail2);
                }

                $Notification = new Notify();

                //Notification to requester
                if ($requesterData->NoficationStatus == "on" && !empty($requesterData->NotificationId)) {
                    //$Notification->setValue('title', trans('lang.SEND_OUTFORDELIVERY_TITLE'));
                    //$Notification->setValue('message', sprintf(trans('lang.SEND_OUTFORDELIVERY_MSG'), ucfirst($requestid->ProductTitle), $requestid->PackageNumber));
                    //$Notification->setValue('type', 'request_detail');
                    //$Notification->setValue('locationkey', $requestId);
                    //$Notification->add_user($requesterData->NotificationId, $requesterData->DeviceType);
                    //$Notification->fire();

                    $cron_noti2 =[
                        "title" => trans('lang.SEND_OUTFORDELIVERY_TITLE'),
                        "message" => sprintf(trans('lang.SEND_OUTFORDELIVERY_MSG'), ucfirst($requestid->ProductTitle), $requestid->PackageNumber),
                        "type" => 'request_detail',
                        "locationkey" => $requestId,
                        "NotificationId" => $requesterData->NotificationId,
                        "DeviceType" => $requesterData->DeviceType,
                        'status'=>'ready',
                        'by_mean'=>'notification'
                    ];
                    SendMail::insert($cron_noti2);
                }
                //insertNotification
                Notification::insert([
                    array(
                        "NotificationTitle" => trans('lang.SEND_OUTFORDELIVERY_TITLE'),
                        "NotificationShortMessage" => sprintf('Transporter "%s" is en route to deliver your item.', $transporterData->Name),
                        "NotificationMessage" => sprintf(trans('lang.SEND_OUTFORDELIVERY_MSG'), ucfirst($requestid->ProductTitle), $requestid->PackageNumber),
                        "NotificationType" => "request",
                        "NotificationUserId" => array(new MongoId($requesterData->_id)),
                        "Date" => new MongoDate(),
                        "GroupTo" => "User",
                    ),
                    array(
                        "NotificationTitle" => trans('lang.SEND_OUTFORDELIVERY_TITLE'),
                        "NotificationMessage" => sprintf(trans('lang.SEND_OUTFORDELIVERY_ADMIN'), ucfirst($requestid->ProductTitle), $requestid->PackageId),
                        "NotificationUserId" => array(),
                        "NotificationReadStatus" => 0,
                        "location" => "request_detail",
                        "locationkey" => (string) $requestId,
                        "Date" => new MongoDate(),
                        "GroupTo" => "Admin",
                    ),
                ]);

                if (count($updateData) > 0) {

                    $response['success'] = 1;
                    $response['status'] = $update['Status'];
                    $response['msg'] = "<strong>Success!</strong> The package is out for delivery.";
                }

            }
        }

        echo json_encode($response);
    }

	public function complete_request($requestId, Request $request) {

		$response = array("success" => 0, "msg" => "Something went wrong.");

		$requestid = Deliveryrequest::where(array(
			'_id' => new MongoId($requestId), 'Status' => 'out_for_delivery'))->select('RequesterId', 'ProductTitle', 'Status')->first();
		if (count($requestid) > 0) {
			$requesterData = User::where(array('_id' => $requestid->RequesterId))->
				select('Name', 'LastName', 'Email', 'EmailStatus', 'NoficationStatus', 'NotificationId', 'DeviceType')->first();

			$transporterData = User::where(array('_id' => session::get('UserId')))->select('Name', 'Email', 'EmailStatus', 'NoficationStatus', 'NotificationId', 'DeviceType')->first();

			if (count($requesterData) > 0 && count($transporterData) > 0) {

				$update['Status'] = 'delivered';
				$update['DeliveredTime'] = new MongoDate();
				$update['TransporterName'] = $transporterData->Name;
				$update['TransporterId'] = new MongoId(session::get('UserId'));
				$updateData = Deliveryrequest::where(array('_id' => new MongoId($requestId)))->update($update);

				//email to transporter
				if ($transporterData->EmailStatus == "on") {
					send_mail('587629a97ac6f66e128b4567', [
						"to" => $transporterData->Email,
						"replace" => [
							"[USERNAME]" => $transporterData->Name,
							"[PACKAGETITLE]" => $requestid->ProductTitle,
						],
					]);
				}
				//email to requester
				if ($requesterData->EmailStatus == "on") {
					send_mail('57d7d8107ac6f6e0148b4567', [
						"to" => $requesterData->Email,
						"replace" => [
							"[USERNAME]" => $requesterData->Name,
						],
					]);
				}

				//Notification to transporter
				$Notification = new Notify();
				if ($transporterData->NoficationStatus == "on" && !empty($transporterData->NotificationId)) {
					$Notification->setValue('title', 'Request complete');
					$Notification->setValue('message', 'Your item has been delivered successfully.');
					$Notification->setValue('type', 'request_detail');
					$Notification->setValue('locationkey', $requestId);
					$Notification->add_user($transporterData->NotificationId, $transporterData->DeviceType);
					$Notification->fire();
				}
				//Notification to requester
				if ($requesterData->NoficationStatus == "on" && !empty($requesterData->NotificationId)) {
					$Notification->setValue('title', 'Request complete');
					$Notification->setValue('message', 'Your item has been delivered successfully.');
					$Notification->setValue('type', 'request_detail');
					$Notification->setValue('locationkey', $requestId);
					$Notification->add_user($requesterData->NotificationId, $requesterData->DeviceType);
					$Notification->fire();
				}
				//insertNotification
				$insNotification = array(
					array(
						"NotificationTitle" => 'Request complete',
						"NotificationShortMessage" => 'Request completed successfully.',
						"NotificationMessage" => 'Your item has been delivered successfully.',
						"NotificationType" => "request",
						"NotificationUserId" => array(new MongoId($transporterData->_id)),
						"Date" => new MongoDate(),
						"GroupTo" => "User",
					),
					array(
						"NotificationTitle" => 'Request complete',
						"NotificationShortMessage" => 'Request completed successfully.',
						"NotificationMessage" => 'Your item has been delivered successfully.',
						"NotificationType" => "request",
						"NotificationUserId" => array(new MongoId($requesterData->_id)),
						"Date" => new MongoDate(),
						"GroupTo" => "User",
					),
					array(
						"NotificationTitle" => 'Request complete',
						"NotificationMessage" => 'Your item has been delivered successfully.',
						"NotificationUserId" => array(),
						"NotificationReadStatus" => 0,
						"location" => "request_detail",
						"locationkey" => (string) $requestId,
						"Date" => new MongoDate(),
						"GroupTo" => "Admin",
					),
				);

				Notification::Insert($insNotification);

				if (count($updateData) > 0) {

					$response['success'] = 1;
					$response['status'] = $update['Status'];
					$response['msg'] = "Success! Your item has been delivered successfully.";
				}

			}
		}

		echo json_encode($response);

	}

	public function no_delivery($requestId, Request $request) {
		$response = array("success" => 0, "msg" => "Something went wrong.");
		if (!empty($requestId)) {
			$deliveryData = Deliveryrequest::where(array('_id' => new MongoId($requestId), 'Status' => 'out_for_delivery'))->first();

			if (count($deliveryData) > 0) {
				$requesterData = User::where(array('_id' => $deliveryData->RequesterId))->
					select('FirstName', 'LastName', 'Email', 'EmailStatus', 'NoficationStatus', 'NotificationId', 'DeviceType', 'Name')->first();

				$transporterData = User::where(array('_id' => session::get('UserId')))->select('FirstName', 'LastName', 'Email', 'EmailStatus', 'NoficationStatus', 'NotificationId' . 'DeviceType', 'Name')->first();
				if (count($requesterData) > 0 && count($transporterData) > 0) {
					$updData = [
						'Status' => 'cancel',
						'RejectBy' => $request->get('reject_by'),
						'ReturnType' => $request->get('return_type'),
						"RejectTime" => new MongoDate(),
						'TrackingNumber' => $request->get('tracking_number'),
						'TransporterMessage' => $request->get('message'),
						"UpdateOn" => new MongoDate(),
					];
					if (isset($_FILES['image']['name']) && @$_FILES['image']['name'] != '') {
						$exts = explode('.', $_FILES['image']['name']);
						$ext = $exts[count($exts) - 1];
						$receipt = "receipt" . rand(2154, 45454) . time() . ".$ext";
						if (in_array(strtolower($ext), ['gif', 'png', 'jpg', 'jpeg'])) {
							$newpath = BASEURL_FILE . "package/{$receipt}";
							if (move_uploaded_file($_FILES['image']['tmp_name'], $newpath)) {
								$updData['ReceiptImage'] = "package/{$receipt}";
							}
						}
					}

					$notifymsg = sprintf('Your package "%s" has been canceled by "%s"', $deliveryData->ProductTitle, @$transporterData->Name);

					$emailReasonforCancaletion = "Rejectby : " . ucfirst(Input::get('reject_by')) . ", Retrun type: " . get_cancel_status(Input::get('return_type'));

					if (Input::get('return_type') == 'ipayment') {
						$notifymsg = sprintf('We regret to inform you, Your package delivery "%s" was not delivered. Aquantuo is continuing to look into the issue. Please contact us at support@aquantuo.com should you have any questions.', $deliveryData['ProductTitle']);

					} else if (strtolower(Input::get('return_type')) == 'creturn' && strtolower(Input::get('reject_by')) == 'requester') {
						//verifyRequiredParams(array('returnDate','returnTime'));

						if (Input::get('date') != '') {
							$date = date_create_from_format('M d, Y h:i:s A', Input::get('date'));
							if ($date) {

								$updData['CancelReturnDate'] = new MongoDate(strtotime(date_format($date, 'd-m-Y H:i:s')));
							}
						}
						$notifymsg = sprintf('Your Package "%s" failed delivery at the specified address. Your package would be returned by to the address specified at the time of the post. Additional fees may apply. Carrier Message: "%s"',
							$deliveryData['ProductTitle'],
							$updData['TransporterMessage']
						);

					} else if (Input::get('return_type') == 'osreturn') {
						//verifyRequiredParams(array('message','trackingNumber'));

						/*$notifymsg = 'Your Package "' . $deliveryData['ProductTitle'] . '" is canceled by requester.<br/><br/><b style="color:#2AABD2"> Carrier Message:</b> ' . $updData['TransporterMessage'];*/

						$notifymsg = sprintf(trans('lang.SEND_CANCELED'), $deliveryData['ProductTitle'], @$transporterData->Name);

					}

					if ($deliveryData->Status == 'out_for_delivery') {

						$updateData = Deliveryrequest::where(array('_id' => new MongoId($requestId)))->update($updData);
						if (count($updateData) > 0) {

							$response['success'] = 1;
							$response['msg'] = "Success! Request has been canceled successfully.";
						}

						//email to requester
						if ($requesterData->EmailStatus == "on") {
							send_mail('56224b45e4b0d4bc235582c0', [
								"to" => $requesterData->Email,
								"replace" => [
									"[USERNAME]" => $requesterData->FirstName . ' ' . $requesterData->LastName,
									'[PACKAGETITLE]' => $deliveryData['ProductTitle'],
									"[REASON]" => $emailReasonforCancaletion,
								],
							]);
						}

						//email to transporter
						if ($transporterData->EmailStatus == "on") {
							send_mail('588ad8316befd90b9d7ea793', [
								"to" => $transporterData->Email,
								"replace" => [
									"[USERNAME]" => $transporterData->FirstName . ' ' . $transporterData->LastName,
									"[PACKAGETITLE]" => $deliveryData->ProductTitle,
									"[REASON]" => $emailReasonforCancaletion,
								],
							]);
						}

						$Notification = new Notify();

						//Notification to requester
						if ($requesterData->NoficationStatus == "on" && !empty(trim($requesterData->NotificationId))) {
							$Notification->setValue('title', trans('lang.SEND_CANCELED'));
							$Notification->setValue('message', $notifymsg);
							$Notification->setValue('type', 'request_detail');
							$Notification->setValue('locationkey', $requestId);
							$Notification->add_user($requesterData->NotificationId, $requesterData->DeviceType);
							$Notification->fire();
						}
						//insertNotification
						Notification::Insert([
							array(
								"NotificationTitle" => 'Your request has been canceled',
								"NotificationShortMessage" => $notifymsg,
								"NotificationMessage" => $notifymsg,
								"NotificationType" => "request_detail",
								"NotificationUserId" => array(new MongoId($requesterData->_id)),
								"Date" => new MongoDate(),
								"GroupTo" => "User",
							),
							array(
								"NotificationTitle" => 'Request canceled',
								"NotificationMessage" => 'Your request has been canceled successfully .',
								"NotificationUserId" => array(),
								"NotificationReadStatus" => 0,
								"location" => "request_detail",
								"locationkey" => (string) $requestId,
								"Date" => new MongoDate(),
								"GroupTo" => "Admin",
							),
						]);

					} else {

						$response = [
							'success' => 0,
							'status' => $update['Status'],
							'msg' => "Oops! The request you have trying to canceled already canceled.",
						];
					}
				}
			}

		}
		echo json_encode($response);

	}

	public function transporter_review(Request $request)
    {
        $response = array("success" => 0, "msg" => "Oops! Something went wrong.");

        $update = Deliveryrequest::where(array('_id' => new MongoId($request->get('request_id'))))->first();

        if (count($update) > 0) {
            if ($update->TransporterRating == '') {

                $update->TransporterRating = $request->get('rating');
                $update->TransporterFeedbcak = $request->get('review');
                $update->save();

                $requester_rating = User::find($request->get('requester_id'));
                $requester_rating->RatingCount = $requester_rating->RatingCount + $request->get('rating');
                $requester_rating->RatingByCount = $requester_rating->RatingByCount + 1;
                $requester_rating->save();

                if (count($update) > 0 && count($requester_rating) > 0) {
                    $response = array("success" => 1, "msg" => "Thanks for your feedback.");
                }
            }

        }

        echo json_encode($response);
    }

	public function verify_delivery_code(Request $request) {
		$response = array("success" => 0, "msg" => "Something went wrong.");
		$delivery_data = Deliveryrequest::where(array('_id' => new MongoId($request->get('request_id')), 'Status' => 'out_for_delivery'))->first();

		if (count($delivery_data) > 0) {
			if ($delivery_data['DeliveryVerifyCode'] == (int) $request->get('code')) {
				$requesterData = User::where(array('_id' => $delivery_data->RequesterId))->
					select('FirstName', 'LastName', 'Email', 'EmailStatus', 'NoficationStatus', 'NotificationId', 'DeviceType')->first();

				$transporterData = User::where(array('_id' => session::get('UserId')))
					->select('Name', 'LastName', 'Email', 'EmailStatus', 'NoficationStatus', 'NotificationId', 'DeviceType')
					->first();

				if (count($requesterData) > 0 && count($transporterData)) {

					$update['Status'] = 'delivered';
					$update['DeliveredTime'] = new MongoDate();
					$update['TransporterName'] = $transporterData->Name;
					$update['TransporterId'] = new MongoId(session::get('UserId'));
					$updateData = Deliveryrequest::where(array('_id' => new MongoId($request->get('request_id'))))->update($update);

					//email to transporter
					if ($transporterData->EmailStatus == "on") {
						send_mail('5694d88a5509251cd67773f0', [
							"to" => $transporterData->Email,
							"replace" => [
								"[USERNAME]" => $transporterData->Name,
								"[PACKAGETITLE]" => ucfirst($delivery_data->ProductTitle),
								"[PACKAGEID]" => $delivery_data->PackageId,
								"[SOURCE]" => $delivery_data->PickupFullAddress,
								"[DESTINATION]" => $delivery_data->DeliveryFullAddress,
								"[PACKAGENUMBER]" => $delivery_data->PackageNumber,
								"[SHIPPINGCOST]" => $delivery_data->shippingCost,
								"[TOTALCOST]" => $delivery_data->TotalCost,
							],
						]);
					}
					//email to requester
					if ($requesterData->EmailStatus == "on") {
						send_mail('5622492ce4b0d4bc235582bd', [
							"to" => $requesterData->Email,
							"replace" => [
								"[USERNAME]" => $requesterData->FirstName . ' ' . $requesterData->LastName,
								"[PACKAGETITLE]" => ucfirst($delivery_data->ProductTitle),
								"[PACKAGEID]" => $delivery_data->PackageId,
								"[SOURCE]" => $delivery_data->PickupFullAddress,
								"[DESTINATION]" => $delivery_data->DeliveryFullAddress,
								"[PACKAGENUMBER]" => $delivery_data->PackageNumber,
								"[SHIPPINGCOST]" => $delivery_data->shippingCost,
								"[TOTALCOST]" => $delivery_data->TotalCost,
							],
						]);
					}

					//Notification to requester
					if ($requesterData->NoficationStatus == "on" && !empty(trim($requesterData->NotificationId))) {
						$Notification = new Notify();
						$Notification->setValue('title', trans('lang.REQUEST_DELIVERED'));
						$Notification->setValue('message', sprintf(trans('lang.MSG_REQUEST_DELIVERED'), $transporterData->Name, $delivery_data['ProductTitle']));
						$Notification->setValue('type', 'request_detail');
						$Notification->setValue('locationkey', $request->get('request_id'));
						$Notification->add_user($requesterData->NotificationId, $requesterData->DeviceType);
						$Notification->fire();
					}
					//insertNotification
					Notification::Insert([
						array(
							"NotificationTitle" => trans('lang.REQUEST_DELIVERED'),
							"NotificationShortMessage" => sprintf(trans('lang.MSG_REQUEST_DELIVERED'), $transporterData->Name, $delivery_data['ProductTitle']),
							"NotificationMessage" => sprintf('Transporter "%s" has delivered your package "%s".', $transporterData->Name, $delivery_data['ProductTitle']),
							"NotificationType" => "request_detail",
							"NotificationUserId" => array(new MongoId($requesterData->_id)),
							"Date" => new MongoDate(),
							"GroupTo" => "User",
						),
						array(
							"NotificationTitle" => trans('lang.SEND_DELIVERED_TITLE_ADMIN'),
							"NotificationMessage" => sprintf(trans('lang.SEND_DELIVERED_ADMIN'), $transporterData->Name, $delivery_data['PackageNumber']),
							"NotificationUserId" => array(),
							"NotificationReadStatus" => 0,
							"location" => "request_detail",
							"locationkey" => (string) $request->get('request_id'),
							"Date" => new MongoDate(),
							"GroupTo" => "Admin",
						),
					]);

					if (count($updateData) > 0) {
						$response['success'] = 1;
						$response['status'] = $update['Status'];
						$response['msg'] = "Request has been completed successfully.";
					}

				}
			} else {
				$response['success'] = 0;
				$response['msg'] = "The verification code you entered is invalid";
			}
		}

		echo json_encode($response);
	}

	public function buy_for_me_request($requestId, Request $request) {

		$response = array("success" => 0, "msg" => "Something went wrong.");

		if ($request->get('type') == 'pickup') {
			$old_status = 'assign';
			$new_status = 'out_for_pickup';
			$message = 'Item is in destination country going through customs and sorting.';
		} elseif ($request->get('type') == 'delivery') {
			$old_status = 'out_for_pickup';
			$new_status = 'out_for_delivery';
			$message = 'Your package is en route to be delivered.';
		} elseif ($request->get('type') == 'cancel') {
			$new_status = 'item_received';
			$message = 'Request has been cancel.';
			$where1 = array("ProductList" => array('$elemMatch' => array('_id' => $requestId, 'tpid' => session::get('UserId'))));
			$request1 = Deliveryrequest::where($where1)->select('ProductList.$.status')->first();
			if (count($request1) > 0) {
				if ($request1->ProductList[0]['status'] == 'assign') {
					$old_status = 'assign';
				} else {
					$old_status = 'out_for_pickup';
				}

			}
		}

		$where = array("ProductList" => array('$elemMatch' => array('_id' => $requestId, 'tpid' => session::get('UserId'), 'status' => $old_status)));

		$request = Deliveryrequest::where($where)
			->select('RequesterId', 'ProductTitle', 'Status', 'ProductList', 'PackageId', 'PickupAddress', 'DeliveryFullAddress', 'PackageNumber', 'shippingCost', 'TotalCost', 'PickupFullAddress', '_id', 'RequestType','ReceiverCountrycode','ReceiverMobileNo')
			->first();
		$ReceiverCountrycode = $request->ReceiverCountrycode;
		$ReceiverMobileNo = $request->ReceiverMobileNo;

		$array = [];

		if (count($request) > 0) {

			$requesterData = User::where(array('_id' => $request->RequesterId))
				->select('Name', 'Email', 'EmailStatus', 'NoficationStatus', 'NotificationId', 'DeviceType')
				->first();

			$transporterData = User::where(array('_id' => session::get('UserId')))
				->select('Name', 'Email', 'EmailStatus', 'NoficationStatus', 'NotificationId' . 'DeviceType')
				->first();

			if (count($requesterData) > 0 && count($transporterData)) {

				$array = $request->ProductList;

				foreach ($request->ProductList as $key => $value) {
					if ($requestId == $value['_id']) {
						$array[$key]['status'] = $new_status;
						$package_id = $array[$key]['package_id'];
						$items = $array[$key];

						/* Activity Log */
						$insertactivity = [
							'request_id' => $request->_id,
							'request_type' => $request->RequestType,
							'item_id' => $requestId,
							'package_id' => $array[$key]['package_id'],
							'PackageNumber' => $request->PackageNumber,
							'item_name' => $array[$key]['product_name'],
							'log_type' => 'request',
							'message' => $message,
							'status' => $new_status,
							'action_user_id'=> Session::get('UserId'),
							'EnterOn' => new MongoDate(),
						];

						Activitylog::insert($insertactivity);

					}
				}

				if (!isset($items)) {
					echo json_encode($response);
					die;
				}

				if (Input::get('type') == 'pickup') {
					$res_status = 'Out for Pickup';
					$res_msg = 'Item is in destination country going through customs and sorting.';
					$admin_id = '588ef5556befd90b2e270ce3';
					$email_id = '57d7d6757ac6f6b5158b4567';
					$tp_email_id = '5876272a7ac6f607128b4569';
					$title_message = trans('lang.BUY_OUTFORPICKUP_TITLE');
					// $message = 'The assigned Transporter is out to accept/pick up your item(s) for delivery.';
					$message = trans('lang.BUY_OUTFORPICKUP_MSG');
					$type = 'request_pickedup';
					$admin_msg = sprintf(trans('lang.BUY_OUTFORPICKUP_MSG_ADMIN'), $items['product_name'], $items['package_id']);
					$tr_msg = 'Item is in destination country going through customs and sorting.';

				} elseif (Input::get('type') == 'delivery') {

					$res_status = 'Out for Delivery';
					$res_msg = 'Your package is en route to be delivered.';
					$tp_email_id = '588f0b706befd90b2e270ce5';
					$email_id = '5e15add2982e1e19fbb7c29b';

					$title_message = trans('lang.BUY_OUTFORDELIVERY_TITLE');
					$message = sprintf(trans('lang.BUY_OUTFORDELIVERY_MSG'), $items['product_name'], $items['package_id']);

					$type = 'request_delivered';

					$admin_msg = sprintf(trans('lang.BUY_OUTFORDELIVERY_MSG_ADMIN'), $items['product_name'], $items['package_id'], $request->DeliveryFullAddress);

					$tr_msg = 'Your package is en route to be delivered.';
				} elseif (Input::get('type') == 'cancel') {
					$res_status = 'Cancel';
					$res_msg = 'Request has been canceled successfully.';
					$tp_email_id = '58ba8a5f7ac6f6d9228b4567';
					$email_id = '';
					$admin_id = '58ba8b077ac6f630238b4567';
					$title_message = 'Request is canceled.';
					$message = sprintf('Transporter has canceled request item titled:%s, ID: %s.', $items['product_name'], $items['package_id']);
					$message = 'Transporter has canceled request.';
					$type = 'request_canceled';

					$admin_msg = sprintf('Transporter has canceled request
                     item titled: %s, ID: %s to %s', $items['product_name'], $items['package_id'], $request->DeliveryFullAddress);
					$tr_msg = 'Request has been canceled successfully .';
				}

				$updateData = Deliveryrequest::where($where)->update(['ProductList' => $array]);

				if ($updateData) {
					Reqhelper::update_status($request->_id);
					$Notification = new Notify();
					$new_email = new NewEmail();
					//email to transporter
					if ($transporterData->EmailStatus == "on") {
						$new_email->send_mail($tp_email_id, [
	                        "to" => $transporterData->Email,
	                        "replace" => [
		                        "[USERNAME]" => $transporterData->Name,
		                        "[PACKAGETITLE]" => ucfirst($items['product_name']),
		                        "[PACKAGEID]" => $package_id,
		                        "[SOURCE]" => $request->PickupFullAddress,
		                        "[DESTINATION]" => $request->DeliveryFullAddress,
		                        "[PACKAGENUMBER]" => $items['package_id'],
		                        "[SHIPPINGCOST]" => $request->shippingCost,
		                        "[TOTALCOST]" => $request->TotalCost,
	                        ],
	                    ]);
						

						$cron_mail = [
							"USERNAME" => $transporterData->Name,
							"PACKAGETITLE" => ucfirst($items['product_name']),
							"PACKAGEID" => $package_id,
							"SOURCE" => $request->PickupFullAddress,
							"DESTINATION" => $request->DeliveryFullAddress,
							"PACKAGENUMBER" => $items['package_id'],
							"SHIPPINGCOST" => $request->shippingCost,
							"TOTALCOST" => $request->TotalCost,
							'email_id' => $tp_email_id,
							'email' => $transporterData->Email,
							'status' => 'ready',
							'REQUESTER' => ucfirst($requesterData->Name),
						];
						//SendMail::insert($cron_mail);
					}
					if (in_array(Input::get('type'), ['pickup', 'delivery'])) {
						//email to requester
						if ($requesterData->EmailStatus == "on") {
							$new_email->send_mail($email_id, [
	                            "to" => $requesterData->Email,
	                            "replace" => [
		                            "[USERNAME]" => $requesterData->Name,
		                            "[PACKAGETITLE]" => ucfirst($items['product_name']),
		                            "[PACKAGEID]" => $items['package_id'],
		                            "[SOURCE]" => $request->PickupFullAddress,
		                            "[DESTINATION]" => $request->DeliveryFullAddress,
		                            "[PACKAGENUMBER]" => $items['package_id'],
		                            "[SHIPPINGCOST]" => $request->shippingCost,
		                            "[TOTALCOST]" => $request->TotalCost,
	                            ],
                            ]);
							

							$cron_mail2 = [
								"USERNAME" => $requesterData->Name,
								"PACKAGETITLE" => ucfirst($items['product_name']),
								"PACKAGEID" => $items['package_id'],
								"SOURCE" => $request->PickupFullAddress,
								"DESTINATION" => $request->DeliveryFullAddress,
								"PACKAGENUMBER" => $items['package_id'],
								"SHIPPINGCOST" => $request->shippingCost,
								"TOTALCOST" => $request->TotalCost,
								'email_id' => $email_id,
								'email' => $requesterData->Email,
								'status' => 'ready',
								'CC' => $ReceiverCountrycode,
								'MNO' => $ReceiverMobileNo,
								'by_mean'=> 'email_2',
							];
							//SendMail::insert($cron_mail2);
						}

						//Notification to requester
						if ($requesterData->NoficationStatus == "on" && !empty($requesterData->NotificationId)) {
							$Notification->setValue('title', $title_message);
							$Notification->setValue('message', $message);
							$Notification->setValue('type', $type);
							$Notification->setValue('locationkey', $requestId);
							$Notification->add_user($requesterData->NotificationId, $requesterData->DeviceType);
							$Notification->fire();

							$cron_noti = [
								"title" => $title_message,
								"message" => $message,
								"type" => $type,
								"locationkey" => $requestId,
								"NotificationId" => $requesterData->NotificationId,
								"DeviceType" => $requesterData->DeviceType,
								'status' => 'ready',
								'by_mean' => 'notification',
							];
							//SendMail::insert($cron_noti);
						}

					}

					//Notification to transporter

					//insertNotification
					$insNotification = array(
						array(
							"NotificationTitle" => $title_message,
							//"NotificationShortMessage"    =>$short,
							"NotificationMessage" => $message,
							"NotificationType" => 'buy_for_me',
							"NotificationUserId" => array(new MongoId($requesterData->_id)),
							"Date" => new MongoDate(),
							"GroupTo" => "User",
						),
						array(
							"NotificationTitle" => $title_message,
							"NotificationMessage" => $admin_msg,
							"NotificationReadStatus" => 0,
							"location" => 'buy_for_me',
							"locationkey" => (string) $requestId,
							"Date" => new MongoDate(),
							"GroupTo" => "Admin",
						),
					);

					Notification::Insert($insNotification);

					$response['success'] = 1;
					$response['status'] = $res_status;
					$response['msg'] = "<strong>Success!</strong> " . $res_msg;

				}

			}
		}
		echo json_encode($response);
	}

	public function online_request($requestId, Request $request) {
		$response = array("success" => 0, "msg" => "Something went wrong.");

		if ($request->get('type') == 'pickup') {
			$old_status = 'assign';
			$new_status = 'out_for_pickup';
			$message = 'Item is in destination country going through customs and sorting.';
		} elseif ($request->get('type') == 'delivery') {
			$old_status = 'out_for_pickup';
			$new_status = 'out_for_delivery';
			$message = 'Your package is en route to be delivered.';
		} elseif ($request->get('type') == 'cancel') {
			$new_status = 'purchased';
			$message = 'Request has been cancel successfully.';
			$where1 = array("ProductList" => array('$elemMatch' => array('_id' => $requestId, 'tpid' => session::get('UserId'))));
			$request1 = Deliveryrequest::where($where1)->select('ProductList.$.status')->first();
			if (count($request1) > 0) {
				if ($request1->ProductList[0]['status'] == 'assign') {
					$old_status = 'assign';
				} else {
					$old_status = 'out_for_pickup';
				}

			}
		}

		$where = array("ProductList" => array('$elemMatch' => array('_id' => $requestId, 'tpid' => session::get('UserId'), 'status' => $old_status)));

		$request = Deliveryrequest::where($where)->select('RequesterId', 'ProductTitle', 'Status', 'ProductList', 'PickupAddress', 'DeliveryFullAddress', 'PackageNumber', 'shippingCost', 'TotalCost','ReceiverMobileNo','ReceiverCountrycode','RequestType')->first();

		$array = [];
		$ReceiverMobileNo = $request->ReceiverMobileNo;
		$ReceiverCountrycode = $request->ReceiverCountrycode;

		if (count($request) > 0) {
			$requesterData = User::where(array('_id' => $request->RequesterId))->
				select('Name', 'Email', 'EmailStatus', 'NoficationStatus', 'NotificationId', 'DeviceType')->first();

			$transporterData = User::where(array('_id' => session::get('UserId')))->select('Name', 'Email', 'EmailStatus', 'NoficationStatus', 'NotificationId','DeviceType')->first();

			if (count($requesterData) > 0 && count($transporterData)) {
				$delivery_status = $request->Status;
				$array = $request->ProductList;

				foreach ($request->ProductList as $key => $value) {
					if ($requestId == $value['_id']) {
						$array[$key]['status'] = $new_status;
						$package_id = $array[$key]['package_id'];
						$items = $array[$key];

						/* Activity Log  online */
						$insertactivity = [
							'request_id' => $request->_id,
							'request_type' => $request->RequestType,
							'item_id' => $requestId,
							'PackageNumber' => $request->PackageNumber,
							'package_id' => $array[$key]['package_id'],
							'item_name' => $array[$key]['product_name'],
							'log_type' => 'request',
							'message' => $message,
							'status' => $new_status,
							'action_user_id'=> Session::get('UserId'),
							'EnterOn' => new MongoDate(),
						];

						Activitylog::insert($insertactivity);
					}
				}

				if (!isset($items)) {
					echo json_encode($response);
					die;
				}

				if (Input::get('type') == 'pickup') {
					$res_status = 'Out for Pickup';
					$res_msg = 'Item is in destination country going through customs and sorting.';
					$email_id = '57d7d6757ac6f6b5158b4567';
					$tp_email_id = '5876272a7ac6f607128b4569';
					$title_message = 'Item is in destination country going through customs and sorting.';
					//$message = 'The assigned Transporter is out to accept/pick up your item(s) for delivery.';
					$message = trans('lang.ONLINE_OUTFORPICKUP_MSG');
					$type = 'online';
					//$admin_msg = sprintf('Transporter "%s" is out to pickup the package for delivery from "%s" to "%s"',
					//Session()->get('Name'),
					//$request->PickupFullAddress,
					//$request->DeliveryFullAddress
					//);
					$admin_msg = sprintf(trans('lang.ONLINE_OUTFORPICKUP_ADMIN'), $items['product_name'], $items['package_id']);

					$tr_msg = 'Item is in destination country going through customs and sorting.';

				} elseif (Input::get('type') == 'delivery') {

					$res_status = 'Out for Delivery';
					$res_msg = 'Your package is en route to be delivered.';
					$tp_email_id = '588f0b706befd90b2e270ce5';
					$email_id = '587863927ac6f6130c8b4568';
					$title_message = trans('lang.ONLINE_OUTFORDELIVERY_TITLE');

					$message = sprintf(trans('lang.ONLINE_OUTFORDELIVERY_MSG'), $items['product_name'], $items['package_id']);
					$type = 'online';

					$admin_msg = sprintf(trans('lang.ONLINE_OUTFORDELIVERY_ADMIN'), $items['product_name'], $items['package_id'], $request->DeliveryFullAddress);

					$tr_msg = 'Your package is en route to be delivered.';
				} elseif (Input::get('type') == 'cancel') {

					$res_status = 'Cancel';
					$res_msg = 'Request has been canceled successfully.';
					$tp_email_id = '58ba8a5f7ac6f6d9228b4567';
					$email_id = '';
					$admin_id = '58ba8b077ac6f630238b4567';
					$title_message = 'Request is canceled.';
					$message = sprintf('Transporter has canceled request item titled:%s, ID: %s.', $items['product_name'], $items['package_id']);
					$message = 'Transporter has canceled request.';
					$type = 'request_canceled';

					$admin_msg = sprintf('Transporter has canceled request
                     item titled: %s, ID: %s to %s', $items['product_name'], $items['package_id'], $request->DeliveryFullAddress);
					$tr_msg = 'Request has been canceled successfully .';

				}

				$updateData = Deliveryrequest::where($where)->update([
					'TransporterName' => $transporterData->Name,
					'TransporterId' => session::get('UserId'),
					'ProductList' => $array,
				]);

				ReqHelper::update_status($request->_id);

				//email to transporter
				if ($transporterData->EmailStatus == "on") {
					/*send_mail($tp_email_id, [
						                    "to" => $transporterData->Email,
						                    "replace" => [
						                    "[USERNAME]" => $transporterData->Name,
						                    "[PACKAGETITLE]" => ucfirst($items['product_name']),
						                    "[PACKAGEID]" => $package_id,
						                    "[SOURCE]" => $request->PickupAddress,
						                    "[DESTINATION]" => $request->DeliveryFullAddress,
						                    "[PACKAGENUMBER]" => $items['package_id'],
						                    "[SHIPPINGCOST]" => $request->shippingCost,
						                    "[TOTALCOST]" => $request->TotalCost,
						                    ],
						                    ]
					*/

					$cron_mail = [
						"USERNAME" => $transporterData->Name,
						"PACKAGETITLE" => ucfirst($items['product_name']),
						"PACKAGEID" => $package_id,
						"SOURCE" => $request->PickupAddress,
						"DESTINATION" => $request->DeliveryFullAddress,
						"PACKAGENUMBER" => $items['package_id'],
						"SHIPPINGCOST" => $request->shippingCost,
						"TOTALCOST" => $request->TotalCost,
						'email_id' => $tp_email_id,
						'email' => $transporterData->Email,
						'status' => 'ready',
						"REQUESTER" => ucfirst($requesterData->Name),
					];

					SendMail::insert($cron_mail);
				}
				$Notification = new Notify();

				if (in_array(Input::get('type'), ['pickup', 'delivery'])) {
					//email to requester
					if ($requesterData->EmailStatus == "on") {
						/*send_mail($email_id, [
							                        "to" => $requesterData->Email,
							                        "replace" => [
							                        "[USERNAME]" => $requesterData->Name,
							                        "[PACKAGETITLE]" => ucfirst($items['product_name']),
							                        "[PACKAGEID]" => $items['package_id'],
							                        "[SOURCE]" => $request->PickupAddress,
							                        "[DESTINATION]" => $request->DeliveryFullAddress,
							                        "[PACKAGENUMBER]" => $items['package_id'],
							                        "[SHIPPINGCOST]" => $request->shippingCost,
							                        "[TOTALCOST]" => $request->TotalCost,
							                        ],
							                        ]
						*/
						/*insert*/
						$cron_mail2 = [
							"USERNAME" => $requesterData->Name,
							"PACKAGETITLE" => ucfirst($items['product_name']),
							"PACKAGEID" => $items['package_id'],
							"SOURCE" => $request->PickupAddress,
							"DESTINATION" => $request->DeliveryFullAddress,
							"PACKAGENUMBER" => $items['package_id'],
							"SHIPPINGCOST" => $request->shippingCost,
							"TOTALCOST" => $request->TotalCost,
							'email_id' => $email_id,
							'email' => $requesterData->Email,
							'status' => 'ready',
							'by_mean'=>'email_2',
							"CC" => @$ReceiverCountrycode,
                            "MNO" => @$ReceiverMobileNo,
						];

						SendMail::insert($cron_mail2);
						/*end ins*/

					}

					//Notification to requester
					if ($requesterData->NoficationStatus == "on" && !empty($requesterData->NotificationId)) {

						$cron_noti = [
							"title" => $title_message,
							"message" => $message,
							"type" => $type,
							"locationkey" => $requestId,
							"NotificationId" => $requesterData->NotificationId,
							"DeviceType" => $requesterData->DeviceType,
							'status' => 'ready',
							'by_mean' => 'notification',
						];
						SendMail::insert($cron_noti);

						//$Notification->setValue('title', $title_message);
						//$Notification->setValue('message', $message);
						//$Notification->setValue('type', $type);
						//$Notification->setValue('locationkey', $requestId);
						//$Notification->add_user($requesterData->NotificationId, $requesterData->DeviceType);
						//$Notification->fire();
					}

				}

				//Notification to transporter
				if ($transporterData->NoficationStatus == "on" && !empty($transporterData->NotificationId)) {
					//$Notification->setValue('title', $title_message);
					//$Notification->setValue('message', $tr_msg);
					//$Notification->setValue('type', $type);
					//$Notification->setValue('locationkey', $requestId);
					//$Notification->add_user($transporterData->NotificationId, $transporterData->DeviceType);
					//$Notification->fire();

					$cron_noti2 = [
						"title" => $title_message,
						"message" => $tr_msg,
						"type" => $type,
						"locationkey" => $requestId,
						"NotificationId" => $transporterData->NotificationId,
						"DeviceType" => $transporterData->DeviceType,
						'status' => 'ready',
						'by_mean' => 'notification',
					];
					SendMail::insert($cron_noti2);
				}

				//insertNotification

				$insNotification = array(
					array(
						"NotificationTitle" => $title_message,
						//"NotificationShortMessage"    =>$short,
						"NotificationMessage" => $message,
						"NotificationType" => 'buy_for_me',
						"NotificationUserId" => array(new MongoId($requesterData->_id)),
						"Date" => new MongoDate(),
						"GroupTo" => "User",
					),
					array(
						"NotificationTitle" => $title_message,
						"NotificationMessage" => $admin_msg,
						"NotificationReadStatus" => 0,
						"location" => 'buy_for_me',
						"locationkey" => (string) $requestId,
						"Date" => new MongoDate(),
						"GroupTo" => "Admin",
					),
				);

				Notification::Insert($insNotification);
				if (count($updateData) > 0) {
					$response['success'] = 1;
					$response['status'] = $res_status;
					$response['msg'] = "<strong>Success!</strong> " . $res_msg;
				}
			}
		}
		echo json_encode($response);
	}

	public function buy_for_me_verify(Request $request) {

		$response = array("success" => 0, "msg" => "Something went wrong.");
		$where = array("ProductList" => array('$elemMatch' => array('_id' => $request->get('item_id'), 'status' => 'out_for_delivery')));

		$delivery_data = Deliveryrequest::where($where)->select('ProductList.$', 'RequesterId', 'PackageId', 'PickupAddress', 'DeliveryFullAddress', 'PackageNumber', 'shippingCost', 'TotalCost', '_id', 'RequestType')->first();

		if (count($delivery_data) > 0) {

			foreach ($delivery_data->ProductList as $value) {
				$verify_code = $value['verify_code'];
				$product_name = $value['product_name'];
				$package_id = $value['package_id'];
			}

			if ($verify_code == (int) $request->get('code')) {
				$requesterData = User::where(array('_id' => $delivery_data->RequesterId))->
					select('Name', 'Email', 'EmailStatus', 'NoficationStatus', 'NotificationId', 'DeviceType')->first();

				$transporterData = User::where(array('_id' => session::get('UserId')))->select('Name', 'Email', 'EmailStatus', 'NoficationStatus', 'NotificationId' . 'DeviceType')->first();

				if (count($requesterData) > 0 && count($transporterData) > 0) {

					$update['ProductList.$.status'] = 'delivered';
					$update['ProductList.$.DeliveredDate'] = new MongoDate();
					$update['ProductList.$.tpid'] = session::get('UserId');
					$update['ProductList.$.tpName'] = $transporterData->Name;

					$updateData = Deliveryrequest::where($where)->update($update);
					if ($updateData) {

						/* Activity Log */
						$insertactivity = [
							'request_id' => $delivery_data->_id,
							'request_type' => $delivery_data->RequestType,
							'item_id' => $delivery_data->ProductList[0]['_id'],
							'package_id' => $delivery_data->ProductList[0]['package_id'],
							'item_name' => $delivery_data->ProductList[0]['product_name'],
							'log_type' => 'request',
							'message' => 'Package has been successfully delivered.',
							'status' => 'delivered',
							'action_user_id'=> Session::get('UserId'),
							'EnterOn' => new MongoDate(),

						];
						Activitylog::insert($insertactivity);

						Reqhelper::update_status($delivery_data->_id);
						//email to transporter
						if ($transporterData->EmailStatus == "on") {
							/*send_mail('587629a97ac6f66e128b4567', [
								                            "to" => $transporterData->Email,
								                            "replace" => [
								                            "[USERNAME]" => $transporterData->Name,
								                            "[PACKAGETITLE]" => ucfirst($product_name),
								                            "[PACKAGEID]" => $package_id,
								                            "[SOURCE]" => $delivery_data->PickupAddress,
								                            "[DESTINATION]" => $delivery_data->DeliveryFullAddress,
								                            "[PACKAGENUMBER]" => $delivery_data->PackageNumber,
								                            "[SHIPPINGCOST]" => $delivery_data->shippingCost,
								                            "[TOTALCOST]" => $delivery_data->TotalCost,
								                            ],
								                            ]
							*/

							$cron_mail = [
								"USERNAME" => $transporterData->Name,
								"PACKAGETITLE" => ucfirst($product_name),
								"PACKAGEID" => $package_id,
								"SOURCE" => $delivery_data->PickupAddress,
								"DESTINATION" => $delivery_data->DeliveryFullAddress,
								"PACKAGENUMBER" => $delivery_data->PackageNumber,
								"SHIPPINGCOST" => $delivery_data->shippingCost,
								"TOTALCOST" => $delivery_data->TotalCost,
								'email_id' => '587629a97ac6f66e128b4567',
								'email' => $transporterData->Email,
								'status' => 'ready',
								"REQUESTER" => ucfirst($requesterData->Name),
							];

							SendMail::insert($cron_mail);
						}
						//email to requester
						if ($requesterData->EmailStatus == "on") {
							/*send_mail('57d7d8107ac6f6e0148b4567', [
								                            "to" => $requesterData->Email,
								                            "replace" => [
								                            "[USERNAME]" => $requesterData->Name,
								                            "[PACKAGETITLE]" => ucfirst($product_name),
								                            "[PACKAGEID]" => $package_id,
								                            "[SOURCE]" => $delivery_data->PickupAddress,
								                            "[DESTINATION]" => $delivery_data->DeliveryFullAddress,
								                            "[PACKAGENUMBER]" => $delivery_data->PackageNumber,
								                            "[SHIPPINGCOST]" => $delivery_data->shippingCost,
								                            "[TOTALCOST]" => $delivery_data->TotalCost,
								                            ],
								                            ]
							*/

							$cron_mail2 = [
								"USERNAME" => $requesterData->Name,
								"PACKAGETITLE" => ucfirst($product_name),
								"PACKAGEID" => $package_id,
								"SOURCE" => $delivery_data->PickupAddress,
								"DESTINATION" => $delivery_data->DeliveryFullAddress,
								"PACKAGENUMBER" => $delivery_data->PackageNumber,
								"SHIPPINGCOST" => $delivery_data->shippingCost,
								"TOTALCOST" => $delivery_data->TotalCost,
								'email_id' => '57d7d8107ac6f6e0148b4567',
								'email' => $requesterData->Email,
								'status' => 'ready',
								"REQUESTER" => ucfirst($requesterData->Name),
							];

							SendMail::insert($cron_mail2);
						}

						//Notification to transporter
						$Notification = new Notify();
						if ($transporterData->NoficationStatus == "on" && !empty($transporterData->NotificationId)) {
							$Notification->setValue('title', trans('lang.BUYFORME_COMPLETED_TITLE'));
							$Notification->setValue('message', sprintf(trans('lang.BUYFORME_COMPLETED_MSG_REQ'), ucfirst($transporterData->Name), ucfirst($product_name)));
							$Notification->setValue('type', 'request_completed');
							$Notification->setValue('locationkey', $request->get('item_id'));
							$Notification->add_user($transporterData->NotificationId, $transporterData->DeviceType);
							$Notification->fire();
						}
						//Notification to requester
						if ($requesterData->NoficationStatus == "on" && !empty($requesterData->NotificationId)) {
							$Notification->setValue('title', trans('lang.BUYFORME_COMPLETED_TITLE'));
							$Notification->setValue('message', sprintf(trans('lang.BUYFORME_COMPLETED_MSG_REQ'), ucfirst($transporterData->Name), ucfirst($product_name)));
							$Notification->setValue('type', 'request_completed');
							$Notification->setValue('locationkey', $request->get('item_id'));
							$Notification->add_user($requesterData->NotificationId, $requesterData->DeviceType);
							$Notification->fire();
						}
						//insertNotification
						$insNotification = array(
							array(
								"NotificationTitle" => trans('lang.BUYFORME_COMPLETED_TITLE'),
								"NotificationShortMessage" => sprintf(trans('lang.BUYFORME_COMPLETED_MSG_REQ'), ucfirst($transporterData->Name), ucfirst($product_name)),
								"NotificationMessage" => sprintf(trans('lang.BUYFORME_COMPLETED_MSG_REQ'), ucfirst($transporterData->Name), ucfirst($product_name)),
								"NotificationType" => "request_completed",
								"NotificationUserId" => array(new MongoId($transporterData->_id)),
								"Date" => new MongoDate(),
								"GroupTo" => "User",
							),
							array(
								"NotificationTitle" => trans('lang.BUYFORME_COMPLETED_TITLE'),
								"NotificationShortMessage" => sprintf(trans('lang.BUYFORME_COMPLETED_MSG_REQ'), ucfirst($transporterData->Name), ucfirst($product_name)),
								"NotificationMessage" => sprintf(trans('lang.BUYFORME_COMPLETED_MSG_REQ'), ucfirst($transporterData->Name), ucfirst($product_name)),
								"NotificationType" => "request_completed",
								"NotificationUserId" => array(new MongoId($requesterData->_id)),
								"Date" => new MongoDate(),
								"GroupTo" => "User",
							),
							array(
								"NotificationTitle" => trans('lang.BUYFORME_COMPLETED_TITLE_ADMIN'),
								"NotificationMessage" => sprintf(trans('lang.BUYFORME_COMPLETED_MSG_ADMIN'), ucfirst($transporterData->Name), $delivery_data->PackageNumber),
								"NotificationUserId" => array(),
								"NotificationReadStatus" => 0,
								"location" => "request_completed",
								"locationkey" => (string) $request->get('item_id'),
								"Date" => new MongoDate(),
								"GroupTo" => "Admin",
							),
						);

						Notification::Insert($insNotification);

						$response['success'] = 1;
						$response['status'] = 'Delivered';
						$response['msg'] = "Success! Package has been successfully delivered.";
					}
				}

			} else {
				$response['success'] = 0;
				$response['status'] = 'wrong';
				$response['msg'] = "Fail! You have entered an invalid code.";
			}

		}

		echo json_encode($response);

	}

	public function by_for_cancel_delivery($requestId, Request $request) {

		$response = array("success" => 0, "msg" => "Something went wrong.");
		if (!empty($requestId)) {
			$where = array("ProductList" => array('$elemMatch' => array('_id' => $requestId, 'status' => 'out_for_delivery')));

			$deliveryData = Deliveryrequest::where($where)
				->select('ProductList.$', 'ProductTitle', 'RequesterId')
				->first();

			$setting = Setting::find('563b0e31e4b03271a097e1ca');

			if (count($deliveryData) > 0) {
				$requesterData = User::where(array('_id' => $deliveryData->RequesterId))
					->select('Email', 'EmailStatus', 'NoficationStatus', 'NotificationId', 'DeviceType', 'Name')
					->first();

				$transporterData = User::where(array('_id' => session::get('UserId')))
					->select('Name', 'Email', 'EmailStatus', 'NoficationStatus', 'NotificationId' . 'DeviceType')
					->first();
				if (count($requesterData) > 0 && count($transporterData) > 0) {

					$updData = [
						'ProductList.$.status' => 'cancel',
						'ProductList.$.RejectBy' => $request->get('reject_by'),
						'ProductList.$.ReturnType' => $request->get('return_type'),
						"ProductList.$.RejectTime" => new MongoDate(),
						'ProductList.$.TrackingNumber' => $request->get('tracking_number'),
						'ProductList.$.TransporterMessage' => $request->get('message'),
						"ProductList.$.UpdateOn" => new MongoDate(),
					];
					if (isset($_FILES['image']['name']) && @$_FILES['image']['name'] != '') {
						$exts = explode('.', $_FILES['image']['name']);
						$ext = $exts[count($exts) - 1];
						$receipt = "receipt" . rand(2154, 45454) . time() . ".$ext";
						if (in_array(strtolower($ext), ['gif', 'png', 'jpg', 'jpeg'])) {
							$newpath = BASEURL_FILE . "package/{$receipt}";
							if (move_uploaded_file($_FILES['image']['tmp_name'], $newpath)) {
								$updData['ProductList.$.ReceiptImage'] = "package/{$receipt}";
							}
						}
					}

					$notifymsg = sprintf('Your package "%s" has been canceled by %s', $deliveryData->ProductList[0]['product_name'], $requesterData['Name']);

					$emailReasonforCancaletion = "Rejectby : " . ucfirst(Input::get('reject_by')) . ", Retrun type: " . get_cancel_status(Input::get('return_type'));

					if (Input::get('return_type') == 'ipayment') {

						$notifymsg = sprintf(trans('lang.BUYFORME_CANCELED_MSG_IPAY'),
							$deliveryData->ProductList[0]['product_name'],
							@$setting->SupportEmail
						);

					} else if (strtolower(Input::get('return_type')) == 'creturn' && strtolower(Input::get('reject_by')) == 'requester') {

						if (Input::get('date') != '') {
							$date = date_create_from_format('M d, Y h:i:s A', Input::get('date'));
							if ($date) {

								$updData['ProductList.$.CancelReturnDate'] = new MongoDate(strtotime(date_format($date, 'd-m-Y H:i:s')));
							}
						}

						$notifymsg = sprintf(trans('lang.BUYFORME_CANCELED_MSG_CRE'), $deliveryData->ProductList[0]['product_name'], $updData['ProductList.$.TransporterMessage']);

						//$notifymsg = 'Your Package "' . $deliveryData->ProductList[0]['product_name'] . '" failed delivery at the specified address. Your package would be returned by to the address specified at the time of the post. Additional fees may apply. Carrier Message: "' . $updData['ProductList.$.TransporterMessage'] . '"';

					} else if (Input::get('return_type') == 'osreturn' && strtolower(Input::get('reject_by')) == 'requester') {

						$notifymsg = sprintf(trans('lang.BUYFORME_CANCELED_MSG_OSCRE'),
							$deliveryData->ProductList[0]['product_name'],
							$transporterData->Name,
							$updData['ProductList.$.TransporterMessage']
						);

					}

					$updateData = Deliveryrequest::where($where)->update($updData);
					if (count($updateData) > 0) {
						ReqHelper::update_status($deliveryData->_id);
						$response['success'] = 1;
						$response['msg'] = "Success! Request has been canceled successfully.";
					}

					//email to transporter
					if ($transporterData->EmailStatus == "on") {
						send_mail('58762b057ac6f6f2128b4567', [
							"to" => $transporterData->Email,
							"replace" => [
								"[USERNAME]" => $transporterData->Name,
								"[PACKAGETITLE]" => $deliveryData->ProductList[0]['product_name'],
								"[REASON]" => $emailReasonforCancaletion,
								"[PACKAGEID]" => $deliveryData->ProductList[0]['package_id'],
							],
						]);
					}

					//email to requester
					if ($requesterData->EmailStatus == "on") {
						send_mail('588b56736befd90b9d7ea797', [
							"to" => $requesterData->Email,
							"replace" => [
								"[USERNAME]" => $requesterData->Name,
								'[PACKAGETITLE]' => $deliveryData->ProductList[0]['product_name'],
								"[REASON]" => $emailReasonforCancaletion,
								"[PACKAGEID]" => $deliveryData->ProductList[0]['package_id'],
							],
						]);
					}

					$Notification = new Notify();

					//Notification to requester
					if ($requesterData->NoficationStatus == "on" && !empty(trim($requesterData->NotificationId))) {
						$Notification->setValue('title', trans('lang.BUYFORME_CANCELED_TITLE'));
						$Notification->setValue('message', $notifymsg);
						$Notification->setValue('type', 'delivery_detail');
						$Notification->setValue('locationkey', $requestId);
						$Notification->add_user($requesterData->NotificationId, $requesterData->DeviceType);
						$Notification->fire();
					}
					//insertNotification
					Notification::Insert([
						array(
							"NotificationTitle" => 'Your request has been canceled',
							"NotificationShortMessage" => $notifymsg,
							"NotificationMessage" => $notifymsg,
							"NotificationType" => "canceled_delivery",
							"NotificationUserId" => array(new MongoId($requesterData->_id)),
							"Date" => new MongoDate(),
							"GroupTo" => "User",
						),
						array(
							"NotificationTitle" => 'Request canceled',
							"NotificationMessage" => $notifymsg,
							"NotificationReadStatus" => 0,
							"location" => "request_detail",
							"locationkey" => (string) $requestId,
							"Date" => new MongoDate(),
							"GroupTo" => "Admin",
						),
					]);

				}
			}

		}
		echo json_encode($response);

	}

	public function transporter_review_to_buy_for(Request $request) {
		$response = array("success" => 0, "msg" => "Something went wrong.");
		$where = array("ProductList" => array('$elemMatch' => array('_id' => $request->get('item_id'), 'status' => 'delivered')));
		$update = Deliveryrequest::where($where)->select('ProductList.$')->first();

		if (count($update) > 0) {

			foreach ($update->ProductList as $value) {
				$TransporterFeedbcak = $value['TransporterFeedbcak'];
				$TransporterRating = $value['TransporterRating'];
			}

			if ($TransporterRating == '' && $TransporterFeedbcak == '') {
				$update['TransporterRating'] = $request->get('rating');
				$update['TransporterFeedbcak'] = $request->get('review');

				$update = [
					'ProductList.$.TransporterFeedbcak' => $request->get('review'),
					'ProductList.$.TransporterRating' => $request->get('rating'),

				];

				$updateData = Deliveryrequest::where($where)->update($update);
				/*  Transporter to requester */
				$requester_rating = User::find($request->get('requester_id'));
				$requester_rating->RatingCount = $requester_rating->RatingCount + $request->get('rating');
				$requester_rating->RatingByCount = $requester_rating->RatingByCount + 1;
				$requester_rating->save();

				if (count($updateData) > 0 && count($requester_rating) > 0) {
					$response = array("success" => 1, "msg" => "Review has been given successfully.");
				}
			}

		}

		echo json_encode($response);
	}

	public function online_verify(Request $request)
    {

        $response = array("success" => 0, "msg" => "Something went wrong.");
        $where = array("ProductList" => array('$elemMatch' => array('_id' => $request->get('item_id'), 'status' => 'out_for_delivery')));

        $delivery_data = Deliveryrequest::where($where)->select('ProductList.$', 'RequesterId', 'PackageId', 'PickupAddress', 'DeliveryFullAddress', 'PackageNumber', 'shippingCost', 'TotalCost','RequestType')->first();

        if (count($delivery_data) > 0) {
            foreach ($delivery_data->ProductList as $value) {
                $verify_code = $value['verify_code'];
                $product_name = $value['product_name'];
                $package_id = $value['package_id'];
            }

            if ($verify_code == (int) $request->get('code')) {
                $requesterData = User::where(array('_id' => $delivery_data->RequesterId))->
                    select('Name', 'Email', 'EmailStatus', 'NoficationStatus', 'NotificationId', 'DeviceType')->first();

                $transporterData = User::where(array('_id' => session::get('UserId')))->select('Name', 'Email', 'EmailStatus', 'NoficationStatus', 'NotificationId' . 'DeviceType')->first();

                if (count($requesterData) > 0 && count($transporterData)) {

                    $update['ProductList.$.status'] = 'delivered';
                    $update['ProductList.$.tpid'] = session::get('UserId');
                    $update['ProductList.$.tpName'] = $transporterData->Name;

                    $updateData = Deliveryrequest::where($where)->update($update);

                    ReqHelper::update_status($delivery_data->_id);

                    /* Activity Log */
					$insertactivity = [
						'request_id' => $delivery_data->_id,
						'request_type' => $delivery_data->RequestType,
						'item_id' => $delivery_data->ProductList[0]['_id'],
						'package_id' => $delivery_data->ProductList[0]['package_id'],
						'item_name' => $delivery_data->ProductList[0]['product_name'],
						'log_type' => 'request',
						'message' => 'Package has been successfully delivered.',
						'status' => 'delivered',
						'action_user_id'=> Session::get('UserId'),
						'EnterOn' => new MongoDate(),

					];
					Activitylog::insert($insertactivity);

                    //email to requester
                    if ($requesterData->EmailStatus == "on") {
                        /*send_mail('57d7d8107ac6f6e0148b4567', [
                            "to" => $requesterData->Email,
                            "replace" => [
                                "[USERNAME]" => $requesterData->Name,
                                "[PACKAGETITLE]" => ucfirst($product_name),
                                "[PACKAGEID]" => $package_id,
                                "[SOURCE]" => $delivery_data->PickupAddress,
                                "[DESTINATION]" => $delivery_data->DeliveryFullAddress,
                                "[PACKAGENUMBER]" => $delivery_data->PackageNumber,
                                "[SHIPPINGCOST]" => $delivery_data->shippingCost,
                                "[TOTALCOST]" => $delivery_data->TotalCost,
                            ],
                        ]
                        );*/

                        $cron_mail = [
                            "USERNAME" => $requesterData->Name,
                            "PACKAGETITLE" => ucfirst($product_name),
                            "PACKAGEID" => $package_id,
                            "SOURCE" => $delivery_data->PickupAddress,
                            "DESTINATION" => $delivery_data->DeliveryFullAddress,
                            "PACKAGENUMBER" => $delivery_data->PackageNumber,
                            "SHIPPINGCOST" => $delivery_data->shippingCost,
                            "TOTALCOST" => $delivery_data->TotalCost,
                            'email_id' => '57d7d8107ac6f6e0148b4567',
                            'email' => $requesterData->Email,
                            'status' => 'ready',
                            "REQUESTER"=>ucfirst($requesterData->Name),
                        ];

                        SendMail::insert($cron_mail);




                    }

                    //email to transporter
                    if ($transporterData->EmailStatus == "on") {
                        /*send_mail('587629a97ac6f66e128b4567', [
                            "to" => $transporterData->Email,
                            "replace" => [
                                "[USERNAME]" => $transporterData->Name,
                                "[PACKAGETITLE]" => ucfirst($product_name),
                                "[PACKAGEID]" => $package_id,
                                "[SOURCE]" => $delivery_data->PickupAddress,
                                "[DESTINATION]" => $delivery_data->DeliveryFullAddress,
                                "[PACKAGENUMBER]" => $delivery_data->PackageNumber,
                                "[SHIPPINGCOST]" => $delivery_data->shippingCost,
                                "[TOTALCOST]" => $delivery_data->TotalCost,

                            ],
                        ]
                        );*/

                        $cron_mail2 = [
                            "USERNAME" => $transporterData->Name,
                            "PACKAGETITLE" => ucfirst($product_name),
                            "PACKAGEID" => $package_id,
                            "SOURCE" => $delivery_data->PickupAddress,
                            "DESTINATION" => $delivery_data->DeliveryFullAddress,
                            "PACKAGENUMBER" => $delivery_data->PackageNumber,
                            "SHIPPINGCOST" => $delivery_data->shippingCost,
                            "TOTALCOST" => $delivery_data->TotalCost,
                            'email_id' => '587629a97ac6f66e128b4567',
                            'email' => $transporterData->Email,
                            'status' => 'ready',
                            "REQUESTER"=>ucfirst($requesterData->Name),
                        ];

                        SendMail::insert($cron_mail2);
                        

                    }
                    

                    //Notification to transporter
                    $Notification = new Notify();
                    if ($transporterData->NoficationStatus == "on" && !empty($transporterData->NotificationId)) {
                        $Notification->setValue('title', 'Request completed');
                        $Notification->setValue('message', sprintf(trans('lang.ONLINE_DELIVERED_USER_MSG'), $transporterData->Name, @$product_name));
                        $Notification->setValue('type', 'request_completed');
                        $Notification->setValue('locationkey', $request->get('item_id'));
                        $Notification->add_user($transporterData->NotificationId, $transporterData->DeviceType);
                        $Notification->fire();
                    }

                    //Notification to requester
                    if ($requesterData->NoficationStatus == "on" && !empty($requesterData->NotificationId)) {
                        $Notification->setValue('title', trans('lang.ONLINE_DELIVERED_USER_TITLE'));
                        //$Notification->setValue('message', 'Your request has been completed successfully .');
                        $Notification->setValue('message', sprintf(trans('lang.ONLINE_DELIVERED_USER_MSG'), $transporterData->Name, @$product_name));
                        $Notification->setValue('type', 'request_completed');
                        $Notification->setValue('locationkey', $request->get('item_id'));
                        $Notification->add_user($requesterData->NotificationId, $requesterData->DeviceType);
                        $Notification->fire();
                    }
                    //insertNotification
                    $insNotification = array(
                        array(
                            "NotificationTitle" => trans('lang.ONLINE_DELIVERED_USER_TITLE'),
                            "NotificationShortMessage" => 'Package has been successfully delivered.',
                            "NotificationMessage" => sprintf(trans('lang.ONLINE_DELIVERED_USER_MSG'), $transporterData->Name, @$product_name),
                            "NotificationType" => "request_completed",
                            "NotificationUserId" => array(new MongoId($transporterData->_id)),
                            "Date" => new MongoDate(),
                            "GroupTo" => "User",
                        ),
                        array(
                            "NotificationTitle" => trans('lang.ONLINE_DELIVERED_USER_TITLE'),
                            "NotificationShortMessage" => 'Package has been successfully delivered.',
                            "NotificationMessage" => sprintf(trans('lang.ONLINE_DELIVERED_USER_MSG'), $transporterData->Name, @$product_name),
                            "NotificationType" => "request_completed",
                            "NotificationUserId" => array(new MongoId($requesterData->_id)),
                            "Date" => new MongoDate(),
                            "GroupTo" => "User",
                        ),
                        array(
                            "NotificationTitle" => trans('lang.ONLINE_DELIVERED_ADMIN_TITLE'),
                            "NotificationMessage" => sprintf(trans('lang.ONLINE_DELIVERED_USER_ADMIN'), $transporterData->Name, $delivery_data->PackageNumber),
                            "NotificationUserId" => array(),
                            "NotificationReadStatus" => 0,
                            "location" => "request_completed",
                            "locationkey" => (string) $request->get('item_id'),
                            "Date" => new MongoDate(),
                            "GroupTo" => "Admin",
                        ),
                    );

                    Notification::Insert($insNotification);

                    if (count($updateData) > 0) {
                        $response['success'] = 1;
                        $response['status'] = 'Delivered';
                        $response['msg'] = "Success! Package has been successfully delivered.";
                    }
                }

            } else {
                $response['success'] = 0;
                $response['status'] = 'wrong';
                $response['msg'] = "Fail! You have entered an invalid code.";
            }

        }

        echo json_encode($response);

    }

	public function online_cancel_delivery($requestId, Request $request) {
		$response = array("success" => 0, "msg" => "Something went wrong.");
		if (!empty($requestId)) {
			$where = array("ProductList" => array('$elemMatch' => array('_id' => $requestId, 'status' => 'out_for_delivery')));

			$deliveryData = Deliveryrequest::where($where)
				->select('ProductList.$', 'ProductTitle', 'RequesterId')
				->first();

			if (count($deliveryData) > 0) {
				$requesterData = User::where(array('_id' => $deliveryData->RequesterId))
					->select('Email', 'EmailStatus', 'NoficationStatus', 'NotificationId', 'DeviceType', 'Name')
					->first();

				$transporterData = User::where(array('_id' => session::get('UserId')))
					->select('Name', 'Email', 'EmailStatus', 'NoficationStatus', 'NotificationId' . 'DeviceType')
					->first();
				if (count($requesterData) > 0 && count($transporterData) > 0) {
					$updData = [
						'ProductList.$.status' => 'cancel',
						'ProductList.$.RejectBy' => $request->get('reject_by'),
						'ProductList.$.ReturnType' => $request->get('return_type'),
						"ProductList.$.RejectTime" => new MongoDate(),
						'ProductList.$.TrackingNumber' => $request->get('tracking_number'),
						'ProductList.$.TransporterMessage' => $request->get('message'),
						"ProductList.$.UpdateOn" => new MongoDate(),
					];
					if (isset($_FILES['image']['name']) && @$_FILES['image']['name'] != '') {
						$exts = explode('.', $_FILES['image']['name']);
						$ext = $exts[count($exts) - 1];
						$receipt = "receipt" . rand(2154, 45454) . time() . ".$ext";
						if (in_array(strtolower($ext), ['gif', 'png', 'jpg', 'jpeg'])) {
							$newpath = BASEURL_FILE . "package/{$receipt}";
							if (move_uploaded_file($_FILES['image']['tmp_name'], $newpath)) {
								$updData['ProductList.$.ReceiptImage'] = "package/{$receipt}";
							}
						}
					}

					$notifymsg = sprintf(trans('lang.ONLINE_CANCELED_MSG'), $deliveryData->ProductList[0]['product_name'], @$transporterData->Name);

					$emailReasonforCancaletion = "Rejectby : " . ucfirst(Input::get('reject_by')) . ", Retrun type: " . get_cancel_status(Input::get('return_type'));

					if (Input::get('return_type') == 'ipayment') {

						$notifymsg = sprintf('We regret to inform you, Your package delivery "%s" was not delivered. Aquantuo is continuing to look into the issue. Please contact us at support@aquantuo.com should you have any questions.', $deliveryData->ProductList[0]['product_name']);

					} else if (strtolower(Input::get('return_type')) == 'creturn' && strtolower(Input::get('reject_by')) == 'requester') {

						if (Input::get('date') != '') {
							$date = date_create_from_format('M d, Y h:i:s A', Input::get('date'));
							if ($date) {

								$updData['ProductList.$.CancelReturnDate'] = new MongoDate(strtotime(date_format($date, 'd-m-Y H:i:s')));
							}
						}

						$notifymsg = sprintf('Your Package "%s" failed delivery at the specified address. Your package would be returned by to the address specified at the time of the post. Additional fees may apply. Carrier Message: "%s"',
							$deliveryData->ProductList[0]['product_name'],
							$updData['ProductList.$.TransporterMessage']
						);

					} else if (Input::get('return_type') == 'osreturn' && strtolower(Input::get('reject_by')) == 'requester') {
						//verifyRequiredParams(array('message','trackingNumber'));

						$notifymsg = 'Your Package "' . $deliveryData->ProductList[0]['product_name'] . '" is canceled by requester. Carrier Message: ' . $updData['ProductList.$.TransporterMessage'];
					}

					$updateData = Deliveryrequest::where($where)->update($updData);
					if (count($updateData) > 0) {
						ReqHelper::update_status($deliveryData->_id);
						$response['success'] = 1;
						$response['msg'] = "Success! Request has been canceled successfully.";
					}

					//email to transporter
					if ($transporterData->EmailStatus == "on") {
						send_mail('58762b057ac6f6f2128b4567', [
							"to" => $transporterData->Email,
							"replace" => [
								"[USERNAME]" => $transporterData->Name,
								"[PACKAGETITLE]" => $deliveryData->ProductList[0]['product_name'],
								"[REASON]" => $emailReasonforCancaletion,
								"[PACKAGEID]" => $deliveryData->ProductList[0]['package_id'],
							],
						]);
					}

					//email to requester
					if ($requesterData->EmailStatus == "on") {
						send_mail('56224b45e4b0d4bc235582c0', [
							"to" => $requesterData->Email,
							"replace" => [
								"[USERNAME]" => $requesterData->Name,
								'[PACKAGETITLE]' => $deliveryData->ProductList[0]['product_name'],
								"[REASON]" => $emailReasonforCancaletion,
								"[PACKAGEID]" => $deliveryData->ProductList[0]['package_id'],
							],
						]);
					}

					$Notification = new Notify();

					//Notification to requester
					if ($requesterData->NoficationStatus == "on" && !empty(trim($requesterData->NotificationId))) {
						$Notification->setValue('title', trans('lang.ONLINE_CANCELED'));
						$Notification->setValue('message', $notifymsg);
						$Notification->setValue('type', 'ONLINE_CANCEL_DELIVERY');
						$Notification->setValue('locationkey', $requestId);
						$Notification->add_user($requesterData->NotificationId, $requesterData->DeviceType);
						$Notification->fire();
					}
					//insertNotification
					Notification::Insert([
						array(
							"NotificationTitle" => 'Request canceled',
							"NotificationShortMessage" => $notifymsg,
							"NotificationMessage" => $notifymsg,
							"NotificationType" => "online",
							"NotificationUserId" => array(new MongoId($requesterData->_id)),
							"Date" => new MongoDate(),
							"GroupTo" => "User",
						),
						array(
							"NotificationTitle" => 'Request canceled',
							"NotificationMessage" => $notifymsg,
							"NotificationReadStatus" => 0,
							"location" => "online",
							"locationkey" => (string) $requestId,
							"Date" => new MongoDate(),
							"GroupTo" => "Admin",
						),
					]);

				}
			}

		}
		echo json_encode($response);

	}

	public function transporter_review_to_online(Request $request) {
		$response = array("success" => 0, "msg" => "Something went wrong.");

		$where = array("ProductList" => array('$elemMatch' => array('_id' => $request->get('item_id'), 'status' => 'delivered')));
		$update = Deliveryrequest::where($where)->select('ProductList.$')->first();

		if (count($update) > 0) {
			foreach ($update->ProductList as $value) {
				$TransporterFeedbcak = $value['TransporterFeedbcak'];
				$TransporterRating = $value['TransporterRating'];
			}
			if ($TransporterRating == '' && $TransporterFeedbcak == '') {
				$update['TransporterRating'] = $request->get('rating');
				$update['TransporterFeedbcak'] = $request->get('review');

				$update = [
					'ProductList.$.TransporterFeedbcak' => $request->get('review'),
					'ProductList.$.TransporterRating' => $request->get('rating'),

				];

				$requester_rating = User::find($request->get('requester_id'));
				$requester_rating->RatingCount = $requester_rating->RatingCount + $request->get('rating');
				$requester_rating->RatingByCount = $requester_rating->RatingByCount + 1;

				$updateData = Deliveryrequest::where($where)->update($update);

				$requester_rating->save();

				if (count($updateData) > 0 && count($requester_rating) > 0) {
					$response = array("success" => 1, "msg" => "Review has been given successfully.");
				}
			}

		}

		echo json_encode($response);
	}

	public function view_request_trip($id) {
		return view('User::list.view_request_trip');
	}

	public function change_trip_status($id, $function) {
		if ($id != '') {
			$trip = Trips::where(['_id' => $id])->first();
			if (count($trip) > 0) {

				$trip->Status = $function;
				if ($trip->save()) {
					return redirect()->back()->withSuccess('Status has been updated successfully.');
				} else {
					return redirect()->back()->with('danger', 'Something went wrong.');
				}

			} else {
				return redirect()->back()->with('danger', 'Something went wrong.');
			}
		} else {
			return redirect()->back()->with('danger', 'Something went wrong.');
		}
	}
	public function checkSession(){
		$user = User::where(["_id" => session()->get('UserId')])
			->where('delete_status', '!=', 'yes')
			->first();
			echo count($user);die;
		if (count($user) == 0) {
			Session::flush();
			return Redirect::to('login');
		}
		
		
	}

}
