<?php namespace App\Modules\User\Controllers;


use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Session,Redirect,Input;
use App\Http\Models\Notification;

use MongoDate,MongoId;

class System extends Controller {

	public function check_system()
	{
		$lastnotify = '';
		if(Session::has('lastnotify')) {
			$lastnotify = Session::get('lastnotify');
		}
		$lastnotify = new MongoDate(strtotime($lastnotify));
		$response = ['success' => 0, 'msg' => 'Oops! Something went wrong.','result' => [], 'notify' => []];
		 
		$where = [
			"NotificationUserId" => ['$elemMatch'=>[ '$in'=> [  new MongoId(session()->get('UserId')) ] ] ],
		];

		$notify = Notification::where('Date' ,'>', $lastnotify)
						->where($where)
						->get(array('NotificationMessage','NotificationTitle'));
		if(count($notify) > 0)
		{
			$response['success'] = 1;
			$response['notify'] = $notify;
		}

		Session::put('lastnotify', date('d-m-Y H:i:s'));

		return response()->json($response);
	}


 

}// End FrontPageController
