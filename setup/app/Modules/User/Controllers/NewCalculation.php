<?php namespace App\Modules\User\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Models\Additem;
use App\Http\Models\Category;
use App\Http\Models\CityStateCountry;
use App\Http\Models\Configuration;
use App\Http\Models\Extraregion;
use App\Http\Models\User;
use App\Library\Utility;
use App\Http\Models\Deliveryrequest;
use Input;
use Session;
use App\Http\Models\Currency;

class NewCalculation extends Controller {
	
	public function GetRate($products, $data, $userinfo, $country='', $state=null) {
		$response = [
			'success' => 0,
			'msg' => 'Oops! Something went wrong.',
		];
		$total_weight = 0;
		$item_cost = 0;
		$shipping_cost_by_user = 0;
		
		foreach ($products as $key => $val) {
			$item_cost += @$val['price'] * @$val['qty'];

			if ($val['travelMode'] == 'air') {
				//$total_weight += $this->GetWeight($val['weight'], $val['weight_unit']) * (int) $val['qty'];
				$total_weight += $this->GetVolumetricWeightInLbs($val['length'], $val['height'], $val['width'], $val['weight'], $val['weight_unit']) * (int) $val['qty'];
			}
			
			if (isset($val['shipping_cost_by_user'])) {
				$shipping_cost_by_user += $val['shipping_cost_by_user'];
			}
		}
		
		// comment the code no restrication
		$dimention =  1;
		//$dimention = $this->calculation_inch_volume($products);
		
		if ($total_weight > 5000) {
			$response['msg'] = "The weight you entered is outside the limits allowed by the airlines. If possible, split your package into smaller packages or choose to ship your item by sea.";
		} else if ($dimention > 1000) {
			$response['msg'] = "The dimensions you entered are outside the limits allowed by the airlines. If possible, split your package into smaller packages or choose to ship your item by sea.";
		} else if ($data['distance']->distance <= 0) {
			$response['msg'] = "Unable to calculate distance.";
		} else {
			$Category = Category::where(['Status' => 'Active'])
			->select('Shipping','price_increase_by','weight_increase_by','weightArray','custom_duty')->get();
			$mappedCategory = [];
			foreach ($Category as $key => $value) {
				$mappedCategory[$value['_id']] = [
					'Shipping'=> $value['Shipping'],
					'price_increase_by'=> $value['price_increase_by'],
					'weight_increase_by'=> $value['weight_increase_by'],
					'weightArray' => $value['weightArray'],
					'custom_duty' => @$value['custom_duty']
				];
			}

			$information = ['Category' => $mappedCategory, 'consolidate_check' => @$data['consolidate_check']];
			$res = $this->separateByAirCategory($products, $information);

			if (!isset($data['distance_calculated'])) {
				$data['distance_calculated'] = false;
			}
			
			$information = ['Category' => $mappedCategory, 'distance_calculated' => $data['distance_calculated'], 'distance'=> $data['distance'], 'separateByAirCategory' => $res, 'is_customs_duty_applied' => @$data['is_customs_duty_applied'], 'consolidate_check' => @$data['consolidate_check']];
			$res = $this->SetRates($products, $information);
			$tot_weight = 0;
			foreach ($res['product'] as $key => $val) {
				$tot_weight += (float) $val['weight'] * (float) $val['qty'];
			}
			if ($res['success'] == 1) {
				$prod = $products[0];
				$prod['weight'] = $tot_weight;
				$prod['qty'] = 1;
				$prod['weight_unit'] = 'lbs';
				$res1 = $this->SetRates([$prod], $information);
				$response['success'] = 1;
				$response['msg'] = 'Shipping calculation.';
				$response['shippingCost'] = $res1['shippingCost'];
				$response['insurance_cost'] = $res['insurance'];
				$response['insurance'] = $res['insurance'];
				
				// Duty and custom cost calculation
				$DutyAndCustom = $res['total_custom_duty'];
				
				if ($res['total_custom_duty'] > 0 && @$data['is_customs_duty_applied']) {
					$response['DutyAndCustom'] = $DutyAndCustom;
				} else {
					$response['DutyAndCustom'] = 0.0;
				}

				// Tax Fee Calculation
				$Tax = $res['total_item_cost'] * $res['config_data']['tax'];
				$response['Tax'] =  $Tax / 100;

				$response['ProcessingFees'] = $res['ProcessingFees'];
				$response['total_item_cost'] = $res['total_item_cost'];
				
				$config = Configuration::find(CongigId);
				if ($country === 'Kenya') {
					$response['DutyAndCustom'] = 0.0;
					$response['formdata'] = $data;
					$extra = 0;
					if ($res['total_item_cost'] > 100) {
						$extra = ($res['total_item_cost'] * $config->kenya_per_item_value)/100;
						$extra = number_format($extra, 2, '.', '');
					}
					// if ($data['weight_unit'] === 'kg') {
					// 	$ratePerKg = number_format(($data['weight'] * $config->kenya_rate_per_kilogram), 2, '.', '');
					// } else {
						$ratePerKg = number_format((($total_weight * 0.453592) * $config->kenya_rate_per_kilogram), 2, '.', '');
					// }
					// echo $ratePerKg;
					$response['shippingCost'] = $ratePerKg < $config->kenya_rate_per_kilogram ? 17 : $ratePerKg;
					// $resionCharges = $this->resionCharges(['name' => $state->name, 'id' => $state->id]);
					$response['total_amount'] = $response['shippingCost'] + $response['insurance_cost'] + $extra;
					// $response['total_amount_txt'] = 'resionCharges - ' . $resionCharges . 'ratePerKg - ' .  $ratePerKg . 'extra - ' .  $extra;
					if ($response['total_amount'] < $config->kenya_rate_per_kilogram) {
						$response['total_amount'] = $config->kenya_rate_per_kilogram;
					}
					if ($data['type'] == 'buyforme') {
						$response['total_amount'] += $res['ProcessingFees'];
					}
				} else {
					$response['total_amount'] = $res1['shippingCost'] + $res['insurance'] + $response['DutyAndCustom'] + $response['Tax']+ $res['ProcessingFees'] + $shipping_cost_by_user;
				}
				if ($data['type'] == 'buyforme') {
					$response['total_amount'] += $res['total_item_cost'];
				}

				$response['total_weight'] = $tot_weight;
				$response['item_cost'] = $item_cost;
				$response['shipping_cost_by_user'] = $shipping_cost_by_user;
				$response['aq_fee'] = $res['aq_fee'];
				$De_currency = '';
				if (isset($userinfo->Default_Currency)) {
					if (in_array($userinfo->Default_Currency, ['', 'USD'])) {
						$userinfo->Default_Currency = 'GHS';
						$De_currency = 'GHS';
					}
				} else {
					$De_currency = $userinfo['Default_Currency'];
				}

				$currency = CityStateCountry::where(['CurrencyCode' => $De_currency])->first();

				if (count($currency) > 0) {
					$response['ghana_total_amount'] = $response['total_amount'] * $currency->CurrencyRate;
				}

				$response['product'] = $res['product'];
			}
		}
		return $response;
	}

	public function GetRatePE($products, $data, $userinfo, $country='', $state=null) {
		$response = [
			'success' => 0,
			'msg' => 'Oops! Something went wrong.',
		];
		$total_weight = 0;
		$item_cost = 0;
		$shipping_cost_by_user = 0;
		
		foreach ($products as $key => $val) {
			$item_cost += @$val['price'] * @$val['qty'];

			if ($val['travelMode'] == 'air') {
				//$total_weight += $this->GetWeight($val['weight'], $val['weight_unit']) * (int) $val['qty'];
				$total_weight += $this->GetVolumetricWeightInLbs($val['length'], $val['height'], $val['width'], $val['weight'], $val['weight_unit']) * (int) $val['qty'];
			}
			
			if (isset($val['shipping_cost_by_user'])) {
				$shipping_cost_by_user += $val['shipping_cost_by_user'];
			}
		}
		
		// comment the code no restrication
		$dimention =  1;
		//$dimention = $this->calculation_inch_volume($products);
		
		if ($total_weight > 5000) {
			$response['msg'] = "The weight you entered is outside the limits allowed by the airlines. If possible, split your package into smaller packages or choose to ship your item by sea.";
		} else if ($dimention > 1000) {
			$response['msg'] = "The dimensions you entered are outside the limits allowed by the airlines. If possible, split your package into smaller packages or choose to ship your item by sea.";
		} else if ($data['distance']->distance <= 0) {
			$response['msg'] = "Unable to calculate distance.";
		} else {
			$Category = Category::where(['Status' => 'Active'])
			->select('Shipping','price_increase_by','weight_increase_by','weightArray','custom_duty')->get();
			$mappedCategory = [];
			foreach ($Category as $key => $value) {
				$mappedCategory[$value['_id']] = [
					'Shipping'=> $value['Shipping'],
					'price_increase_by'=> $value['price_increase_by'],
					'weight_increase_by'=> $value['weight_increase_by'],
					'weightArray' => $value['weightArray'],
					'custom_duty' => @$value['custom_duty']
				];
			}

			$information = ['Category' => $mappedCategory, 'consolidate_check' => @$data['consolidate_check']];
			$res = $this->separateByAirCategory($products, $information);

			if (!isset($data['distance_calculated'])) {
				$data['distance_calculated'] = false;
			}
			
			$information = ['Category' => $mappedCategory, 'distance_calculated' => $data['distance_calculated'], 'distance'=> $data['distance'], 'separateByAirCategory' => $res, 'is_customs_duty_applied' => @$data['is_customs_duty_applied'], 'consolidate_check' => @$data['consolidate_check']];
			$res = $this->SetRates($products, $information);
			$tot_weight = $total_weight;
			// foreach ($res['product'] as $key => $val) {
			// 	$tot_weight += (float) $val['weight'] * (float) $val['qty'];
			// }
			if ($res['success'] == 1) {
				// $prod = $products[0];
				// $prod['weight'] = $tot_weight;
				// $prod['weight_unit'] = 'lbs';
				// $res1 = $this->SetRates([$prod], $information);
				$response['success'] = 1;
				$response['msg'] = 'Shipping calculation.';
				$response['shippingCost'] = $res['shippingCost'];
				$response['insurance_cost'] = $res['insurance'];
				$response['insurance'] = $res['insurance'];
				$response['res'] = $res;
				
				// Duty and custom cost calculation
				$DutyAndCustom = $res['total_custom_duty'];
				
				if ($res['total_custom_duty'] > 0 && @$data['is_customs_duty_applied']) {
					$response['DutyAndCustom'] = $DutyAndCustom;
				} else {
					$response['DutyAndCustom'] = 0.0;
				}

				// Tax Fee Calculation
				$Tax = $res['total_item_cost'] * $res['config_data']['tax'];
				$response['Tax'] =  $Tax / 100;

				$response['ProcessingFees'] = $res['ProcessingFees'];
				$response['total_item_cost'] = $res['total_item_cost'];
				
				$config = Configuration::find(CongigId);
				if ($country === 'Kenya') {
					$response['DutyAndCustom'] = 0.0;
					$response['formdata'] = $data;
					$extra = 0;
					if ($res['total_item_cost'] > 100) {
						$extra = ($res['total_item_cost'] * $config->kenya_per_item_value)/100;
						$extra = number_format($extra, 2, '.', '');
					}
					// if ($data['weight_unit'] === 'kg') {
					// 	$ratePerKg = number_format(($data['weight'] * $config->kenya_rate_per_kilogram), 2, '.', '');
					// } else {
						$ratePerKg = number_format((($total_weight * 0.453592) * $config->kenya_rate_per_kilogram), 2, '.', '');
					// }
					// echo $ratePerKg;
					$response['shippingCost'] = $ratePerKg < $config->kenya_rate_per_kilogram ? 17 : $ratePerKg;
					// $resionCharges = $this->resionCharges(['name' => $state->name, 'id' => $state->id]);
					$response['total_amount'] = $response['shippingCost'] + $response['insurance_cost'] + $extra;
					// $response['total_amount_txt'] = 'resionCharges - ' . $resionCharges . 'ratePerKg - ' .  $ratePerKg . 'extra - ' .  $extra;
					if ($response['total_amount'] < $config->kenya_rate_per_kilogram) {
						$response['total_amount'] = $config->kenya_rate_per_kilogram;
					}
					if ($data['type'] == 'buyforme') {
						$response['total_amount'] += $res['ProcessingFees'];
					}
				} else {
					$response['total_amount'] = $res['shippingCost'] + $res['insurance'] + $response['DutyAndCustom'] + $response['Tax']+ $res['ProcessingFees'] + $shipping_cost_by_user;
				}
				if ($data['type'] == 'buyforme') {
					$response['total_amount'] += $res['total_item_cost'];
				}

				$response['total_weight'] = $total_weight;
				$response['item_cost'] = $item_cost;
				$response['shipping_cost_by_user'] = $shipping_cost_by_user;
				$response['aq_fee'] = $res['aq_fee'];
				$De_currency = '';
				if (isset($userinfo->Default_Currency)) {
					if (in_array($userinfo->Default_Currency, ['', 'USD'])) {
						$userinfo->Default_Currency = 'GHS';
						$De_currency = 'GHS';
					}
				} else {
					$De_currency = $userinfo['Default_Currency'];
				}

				$currency = CityStateCountry::where(['CurrencyCode' => $De_currency])->first();

				if (count($currency) > 0) {
					$response['ghana_total_amount'] = $response['total_amount'] * $currency->CurrencyRate;
				}

				$response['product'] = $res['product'];
			}
		}
		return $response;
	}

	public function separateByAirCategory($products, $information) {
		$already_calculated_category = [];
		$category_set = [];

		foreach ($products as $key => $val) {
			if (!in_array($val['categoryid'], $already_calculated_category) && $val['categoryid'] != OtherCategory && $val['travelMode'] == 'air' && $val['categoryid'] != ElectronicsCategory) {
				$cat_weight = $this->category_count($products, $val['categoryid']);
				$weight = $cat_weight['weight'];
				$weight = number_format($weight,1); 
				$already_calculated_category[] = $val['categoryid'];
				$data = @$information['Category'][$val['categoryid']];
				$rate = 0;
				$custom_duty = @$data['custom_duty'];

				foreach ($data['Shipping'] as $key1) {
					if ($key1['MinDistance'] <= floatval($weight) && $key1['MaxDistance'] >= floatval($weight)) {
						//changes to day
						if (isset($information['consolidate_check'])) {
							if ($information['consolidate_check'] == 'on') {
								$rate = $key1['CONSOLIDATE_RATE'];
							} else {
								$rate = $key1['Rate'];
							}
						} else {
							$rate = $key1['Rate'];
						}
					}
				}

				if ($rate > 0) {
					$category_set[$val['categoryid']] = [
						'cat_id' => $val['categoryid'],
						'rate' => $rate,
						'custom_duty' => $custom_duty
					];
				}
			}
		}
		
		return $category_set;
	}

	public function category_count($array, $cat_id) {
		$res['count'] = 0;
		$res['weight'] = 0;
		foreach ($array as $key => $value) {
			if ($cat_id == $value['categoryid']) {
				$res['count'] += 1;
				//$res['weight'] += $this->GetWeight($value['weight'], $value['weight_unit']) * (int) $value['qty'];
				$res['weight'] += $this->GetVolumetricWeightInLbs($value['length'],$value['height'],$value['width'],$value['weight'],$value['weight_unit']);
			}
		}
		return $res;
	}

	public function SetRates($products, $information) {
		$returnProducts = $products;

		$returnData = [
			'success'=> 0,
			'msg' => 'Oops! Something went wrong.',
			'product'=>[],
			'insurance' => 0,
			'price' => 0,
			'DutyAndCustom' => 0,
			'Tax'=> 0,
			'ProcessingFees'=> 0,
			'total_weight'=> 0.0,
			'shippingCost'=> 0.0,
			'total_item_cost'=> 0.0,
			'total_custom_duty'=> 0.0,
			'aq_fee'=>0,
		];

		if (!$information['distance_calculated']) {
			$distance = $this->get_distance($information['distance']->distance);
		} else {
			// $distance = $inputData['distance']->distance;
			$distance = $information['distance']->distance;
		}
		
		$config = Configuration::find(CongigId);
		$aq_fees = 0;

		foreach ($products as $key => $val) {
			$returnData['total_item_cost'] += $val['price'] * $val['qty'];

			if ($val['travelMode'] == 'air' && $val['categoryid'] != OtherCategory && $val['categoryid'] != ElectronicsCategory) {
				$rate = $information['separateByAirCategory'][$val['categoryid']]['rate'];
				$custom_duty = $information['separateByAirCategory'][$val['categoryid']]['custom_duty'];
				$cat_count = $this->category_count($products, $val['categoryid']);
				$returnProducts[$key]['shippingCost'] = $rate / $cat_count['count'];
				$returnData['shippingCost'] += $rate / $cat_count['count'];

				//$itemWeight = $this->GetWeight($val['weight'], $val['weight_unit']) * (int) $val['qty'];
				$itemWeight = $this->GetVolumetricWeightInLbs($val['length'],$val['height'],$val['width'],$val['weight'],$val['weight_unit']) * (int) $val['qty'];
				$returnData['total_weight'] += $itemWeight;
				$returnProducts[$key]['custom_duty'] = 0.0;

				if ($custom_duty > 0) {
					$returnProducts[$key]['custom_duty'] = (($val['price'] * $val['qty'])*$custom_duty)/100;
					$returnData['total_custom_duty'] += (($val['price'] * $val['qty'])*$custom_duty)/100;
				}
			} else if ($val['travelMode'] == 'air' && ($val['categoryid'] == OtherCategory || $val['categoryid'] == ElectronicsCategory)) {
				$data = $information['Category'][$val['categoryid']];
				
				if (!isset($data['Shipping'])) {
					return $returnData;
				}

				// $itemWeight = $this->GetWeight($val['weight'], $val['weight_unit']) * (int) $val['qty'];
				$itemWeight = $this->GetVolumetricWeightInLbs($val['length'],$val['height'],$val['width'],$val['weight'],$val['weight_unit']) * (int) $val['qty'];
				
				$returnData['total_weight'] += $itemWeight;
				$is_macth = false;
				$last_price = 0;
				$weight_increase_by = $data['weight_increase_by'];
				
				if (is_array($data['weightArray'])) {
					foreach ($data['weightArray'] as $key3) {
						if ($key3['MinWeight'] < floatval($itemWeight)) {
							$weight_increase_by = $key3['Rate'];
						}
					}
				}
				
				foreach ($data['Shipping'] as $key1) {
					if ($key1['MinDistance'] <= floatval($itemWeight) && $key1['MaxDistance'] >= floatval($itemWeight)) {
						$is_macth = true;
                   		$rate = $key1['Rate'];

                   		if ($val['price'] > 1) {
                   			$p = $val['price'] - 1;
                   			$rate = $rate + ($p * $data['price_increase_by']);
                   		}

                   		$returnProducts[$key]['shippingCost'] = $rate;
                   		$returnData['shippingCost'] += $rate;
	                }
	                $last_price = $key1['Rate'];
				}

				// if weight not range then cacluate from model doc
				if (!$is_macth) {
					$itemWeight_1 = $itemWeight-2;
					$itemWeight_2 = intval($itemWeight_1/2);
					$rate_1 =  $itemWeight_2 * $weight_increase_by;
					$rate_2 = $rate_1 + $last_price;
					// Calculate price increment 
					$p = $val['price'] - 1;
	                $rate_4 = $rate_2 + ($p * $data['price_increase_by']);
					$rate = $rate_4;
										
					$returnProducts[$key]['shippingCost'] = $rate;
	                $returnData['shippingCost'] += $rate;
				}

				$returnProducts[$key]['custom_duty'] = 0.0;
				
				if ($data['custom_duty'] > 0) {
					$returnProducts[$key]['custom_duty'] = (($val['price'] * $val['qty'])*$data['custom_duty'])/100;
					$returnData['total_custom_duty'] += (($val['price'] * $val['qty'])*$data['custom_duty'])/100;
				}
			} else {
				$data = $information['Category'][$val['categoryid']];

				if (!isset($data['Shipping'])) {
					return $returnData;
				}

				$categoryConstantArray = array(AUTO_FULLSIZE_SUV, AUTO_INTERMEDIATE_SUV, AUTO_SEDAN_ECONOMY, AUTO_SEDAN_FULLSIZE, AUTO_SEDAN_INTERMEDIATE, AUTO_STANDARD_SUV, AUTOMOBILE_VAN);
				
				if ($val['travelMode'] == 'ship' && in_array($val['categoryid'], $categoryConstantArray)) {
					foreach ($data['Shipping'] as $key1) {
						if ($key1['MinDistance'] <= $distance && $key1['MaxDistance'] >= $distance) {
							if ($information["consolidate_check"] == "on") {
								$returnProducts[$key]['shippingCost'] = $key1['CONSOLIDATE_RATE'] * (int) $val['qty'];
								$returnData['shippingCost'] += $key1['CONSOLIDATE_RATE'] * (int) $val['qty'];
							} else {
								$returnProducts[$key]['shippingCost'] = $key1['Rate'] * (int) $val['qty'];
								$returnData['shippingCost'] += $key1['Rate'] * (int) $val['qty'];
							}
						}
					}
				} else {
					$cat_count = $this->category_count($products, $val['categoryid']);
					
					foreach ($data['Shipping'] as $key1) {
						if ($key1['MinDistance'] <= $distance && $key1['MaxDistance'] >= $distance) {
							if ($information["consolidate_check"] == "on") {
								$returnProducts[$key]['shippingCost'] = $key1['CONSOLIDATE_RATE'] / $cat_count['count'];
								$returnData['shippingCost'] += $key1['CONSOLIDATE_RATE'] / $cat_count['count'];
							} else {
								$returnProducts[$key]['shippingCost'] = $key1['Rate'] / $cat_count['count'];
								$returnData['shippingCost'] += $key1['Rate'] / $cat_count['count'];
							}
						}
					}
				}
				
				$returnProducts[$key]['custom_duty'] = 0.0;

				if ($data['custom_duty'] > 0) {
					$returnProducts[$key]['custom_duty'] = (($val['price'] * $val['qty'])*$data['custom_duty'])/100;
					$returnData['total_custom_duty'] += (($val['price'] * $val['qty'])*$data['custom_duty'])/100;
				}
			}
			
			if ($returnProducts[$key]['shippingCost'] == 0) {
				$returnData['msg'] = 'Item info you entered does not match with our shipping conditions, Please check package info and try again.';
				return $returnData;
			}
			// echo $returnProducts[$key]['shippingCost'] . '<br />';
			// Insurance cost calculation
			$returnProducts[$key]['insurance'] = 0;
			$returnProducts[$key]['insurance_cost'] = 0;
			
			if ($val['insurance_status'] == 'yes') {
				$returnData['price'] += $val['price'] * $val['qty'];
				$insurance = ($val['price'] * $val['qty']) * $config['main_insurance'];
				$insurance = $insurance / 100;
				$returnData['insurance'] += $insurance;
				$returnProducts[$key]['insurance_cost'] = $insurance;
				$returnProducts[$key]['insurance'] = $insurance;
			}
			
			// Processing Fee Calculation BFM Concierge 
			if (@$val['request_type'] == 'buy_for_me' || @$val['request_type'] == 'buyforme') {
				$returnProducts[$key]['ProcessingFees'] = 0;
				$ProcessingFees = ($val['price'] * $val['qty']) * $config['bfm_concierge'];
				$ProcessingFees = $ProcessingFees / 100;
				$returnData['ProcessingFees'] += $ProcessingFees;
				$returnProducts[$key]['ProcessingFees'] = $ProcessingFees;
			}
			
			// Get aquantuo fee 
			if (count($config) > 0) {
				$aq_fees = $config['aquantuoFees'];
			}

			$returnProducts[$key]['aq_fee'] = $this->get_aquantuo_fees(($val['price'] * $val['qty']),$aq_fees);
			$returnData['aq_fee'] +=  $returnProducts[$key]['aq_fee'];
		}
		
		$returnData['config_data'] = $config;
		$returnData['success'] = 1;
		$returnData['product'] = $returnProducts;
		return $returnData;
	}

	public function get_distance($distance) {
		return $distance = floatval($distance) * 0.000621371;
	} //close

	public function get_aquantuo_fees($totalcost,$fees) {
		
		$fees = ((floatval($totalcost) * $fees) / 100);
		return $fees;
	}
	
	/*
	 * TODO return weight into pounds
	 * 1 lbs = 2.20462 kg
	 * 1lbs = 0.00220462 gram
	 */
	public function GetWeight($weight, $type) {
		$type = strtolower($type);
		if ($type == 'kg') {
			return (((float) $weight) * 2.20462);
		} else if ($type == 'gram') {
			return (((float) $weight) * 0.00220462);
		} else {
			return $weight;
		}
	}
	/*
	 * TODO return Volumetric Weight in lbs
	 * 1 lbs = 2.20462 kg
	 * 1lbs = 0.00220462 gram
	 *
	 */
	public function GetVolumetricWeightInLbs($length,$height,$width,$weight,$type) {
		$VolumetricInfo = ["length"=>$length,"height"=>$height,"width"=>$width,"type"=>$type];
        $VolumetricWeight =  Utility::calculate_volumetric_weight($VolumetricInfo);
       
		if($weight>$VolumetricWeight){
			$VolumetricWeight  = $weight;
		}
        $type = strtolower($type);
		if ($type == 'kg') {
			$VolumetricWeightLbs = (((float) $VolumetricWeight) * 2.20462);
		} else if ($type == 'gram') {
			$VolumetricWeightLbs = (((float) $VolumetricWeight) * 0.00220462);
		} else {
			$VolumetricWeightLbs = $VolumetricWeight;
		}
		return $VolumetricWeightLbs;
	}

	public function calculation_inch_volume($array) {
		$intch = 0;
		foreach ($array as $key) {
			if ($key['travelMode'] == 'air') {
				$v = $this->get_size_in_inch(@$key['height'], 'inches') * $this->get_size_in_inch(@$key['length'], 'inches') * $this->get_size_in_inch(@$key['width'], 'inches');
				$v = $v * (int) $key['qty'];

				$intch += $v;
			}
		}
		return $intch;
	}

	public function get_size_in_inch($length, $unit) {
		$unit = strtolower($unit);
		switch ($unit) {
		case 'inches':
			return floatval($length);
			break;
		case 'cm':
			return floatval($length) * 0.393701;
			break;
		}
	}
	
	public function get_weight_kg($weight, $type) {
		$type = strtolower($type);
		if ($type == "kg") {
			return $weight;
		} else if ($type == "lbs") {
			return (number_format( $weight / 2.2046, 2) );
		} else {
			return $weight;
		}

		/*if ($type == 'kg') {
			return (((float) $weight) * 2.20462);
		} else if ($type == 'gram') {
			return (((float) $weight) * 0.00220462);
		} else {
			return $weight;
		}*/
	}

	public function get_weight_lbs($weight, $type) {
		$type = strtolower($type);
		if ($type == "kg") {
			return (number_format(((float) $weight) * 2.20462, 2));
		} else if ($type == "lbs") {
			return $weight;
		} else {
			return $weight;
		}
	}

	public function volumetricWeightInKg($array) {
		return ($this->get_size_in_cm($array['width'], $array['WHLUnit']) * $this->get_size_in_cm($array['height'], $array['WHLUnit']) * $this->get_size_in_cm($array['length'], $array['WHLUnit']));
	}

	public function volumetricWeightInLbs($array) {
		return ($this->getSizeInInches($array['width'], $array['WHLUnit']) * $this->getSizeInInches($array['height'], $array['WHLUnit']) * $this->getSizeInInches($array['length'], $array['WHLUnit']));
	}

	public function get_size_in_cm($height, $unit) {
		$in_cm = (float) $height;
		$unit = strtolower($unit);
		if ($unit == "inches") {
			$in_cm = $in_cm * 2.54;
		}
		return $in_cm;
	}

	public function getSizeInInches($whl, $unit) {
		$in_inches = (float) $whl;
		$unit = strtolower($unit);
		if ($unit == "cm") {
			$in_inches = $in_inches / 2.54;
		}
		return $in_inches;
	}
     
	/*
		     *Developer Name :Ajay Chaudhary 
		     *Function :Calculate shipping cost of buy for me request
		     * Before it prsent on Calulation controller
		     *Update at:11-12-2019
	*/
	public function update_buy_cal() {

		$response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		$userinfo = User::find(Session::get('UserId'));
		$aqlat = Session::get('AqLat');
		$aqlong = Session::get('AqLong');
		$user_currency = $userinfo->Default_Currency;

		$data = [
			'shippingcost' => 0,
			'insurance' => 0,
			'ghana_total_amount' => 0,
			'total_amount' => 0,
			'error' => [],
			'formated_text' => '',
			'distance_in_mile' => 0,
			'product_count' => 0,
			'ProcessingFees' => 0,
			'item_cost' => 0,
			'shipping_cost_by_user' => 0,
			'type' => 'buyforme',
			'consolidate_check' => trim(Input::get('consolidate_check')),
			"totalWeightInKg" => 0,
			"totalWeightInLbs" => 0,
			"totalVolumeInKg" => 0,
			"totalVolumeInLbs" => 0
		];

		if (count($userinfo) > 0) {
			$address = json_decode(Input::get('address'));
			$response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
			$response['total_amount'] = 0;
			$response['ghana_total_amount'] = 0;
			$response['shippingCost'] = 0;

			if (empty(trim(Input::get('request_id')))) {
				$data['product'] = Additem::where(array('user_id' => session()->get('UserId'), 'request_type' => 'buy_for_me'))->get();
			} else {
				$data['product'] = session()->get(trim(Input::get('request_id')));
			}
			
			if (count($data['product']) > 0 && count($address) > 0) {
				$totalWeightInKg = 0;
				$totalWeightInLbs = 0;
				$totalVolumeInKg = 0;
				$totalVolumeInLbs = 0;
				
				foreach ($data["product"] as $value) {
					$WHLUnit = "";
					$measurementUnit = "";
					$pWeightUnit = "";
					$pWeight = 0;
					$pWidth = 0;
					$pHeight = 0;
					$pLength = 0;
					
					if (Input::get("request_id")) {
						$measurementUnit = $value["heightUnit"];
						$pWeightUnit = $value["weight_unit"];
						$pWeight = $value["weight"];
						$pWidth = $value["width"];
						$pHeight = $value["height"];
						$pLength = $value["length"];
					} else {
						$measurementUnit = @$value->measurement_unit;
						$pWeightUnit = @$value->weight_unit;
						$pWeight = @$value->weight;
						$pWidth = @$value->width;
						$pHeight = @$value->height;
						$pLength = @$value->length;
					}

					if ($measurementUnit == "cm_kg" || $measurementUnit == "cm") {
						$WHLUnit = "cm";
					}

					if ($measurementUnit == "inches_lbs" || $measurementUnit == "inches") {
						$WHLUnit = "inches";
					}

					$totalWeightInKg += $this->get_weight_kg($pWeight, $pWeightUnit);
					$totalWeightInLbs += $this->get_weight_lbs($pWeight, $pWeightUnit);

					$totalVolumeInKg += $this->volumetricWeightInKg(array(
						"width" => $pWidth,
						"WHLUnit" => $WHLUnit,
						"height" => $pHeight,
						"WHLUnit" => $WHLUnit,
						"length" => $pLength,
						"WHLUnit" => $WHLUnit
					));

					$totalVolumeInLbs += $this->volumetricWeightInLbs(array(
						"width" => $pWidth,
						"WHLUnit" => $WHLUnit,
						"height" => $pHeight,
						"WHLUnit" => $WHLUnit,
						"length" => $pLength,
						"WHLUnit" => $WHLUnit
					));
				}
				$data['is_customs_duty_applied'] = false;
				
				if(@$address->country!="USA"){
					$data['is_customs_duty_applied'] = true;
				}

				$data['distance'] = Utility::GetDrivingDistance($aqlat, $aqlong, $address->lat, $address->lng);

				$weight = 0;
				$response = $this->GetRate($data['product'], $data, $userinfo, @$address->country);

				if ($response['success'] == 1) {
					$data['drop_address'] = $address;
					
					$response['outside_accra_charge'] = 0;
					//out side accra charges
					$response['AreaCharges'] = $this->resionCharges(['name' => $address->state, 'id' => $address->state_id]);
					$response['total_amount'] = $response['total_amount'] + $response['AreaCharges'];

					$response['shippingCost'] = $response['shippingCost'];
					//$response['total_amount'] = $response['total_amount'] + $response['ProcessingFees'];
					$response['total_amount'] = $response['total_amount'];

					$currency_conversion = $this->currency_conversion($response['total_amount'],trim($user_currency));
					
					$response['ghana_total_amount'] = $currency_conversion[trim($user_currency)];
					$response['distance_in_mile'] = 0; // this will be zero in case buyForMe and
					$response['product_count'] = count($data['product']);
					$response["totalWeightInKg"] = $totalWeightInKg;
					$response["totalWeightInLbs"] = $totalWeightInLbs;
					$response["totalVolumeInKg"] = number_format($totalVolumeInKg, 2);
					$response["totalVolumeInLbs"] = number_format($totalVolumeInLbs, 2);
					// Ftech currency Rate 
					$currencyInfo = Currency::where(['Status' => 'Active'])->select('CurrencyRate', 'FormatedText', 'CurrencyCode')->get();
					$response['USDRate'] = 1;
					$response['GBPRate'] = $response['GHSRate'] = $response['CADRate'] = $response['KESRate'] = 0;
					foreach($currencyInfo as $currencyVal){
						if($currencyVal->CurrencyCode=='CAD'){
							$response['CADRate'] = $currencyVal->CurrencyRate;
						}
						if($currencyVal->CurrencyCode=='GHS'){
							$response['GHSRate'] = $currencyVal->CurrencyRate;
						}
						if($currencyVal->CurrencyCode=='GBP'){
							$response['GBPRate'] = $currencyVal->CurrencyRate;
						}
						if($currencyVal->CurrencyCode=='KES'){
							$response['KESRate'] = $currencyVal->CurrencyRate;
						}
					}
					
					//end out side accra charges
					$response['error'] = [];
					$view = view('User::Buy.ajax.buyforme_calculation', $response);
					$response['html'] = $view->render();
					$response['product'] = $data['product'];
				}
			}
		}
		return json_encode($response);
	}
	/**
	 */
	 public function currency_conversion($shipcost,$currency_code) {

		$data = ['cost' => 0, 'GHS' => 0, 'PHP' => 0, 'GBP' => 0, 'CAD' => 0,'USD' => 0,'KES' => 0];
		$currency = Currency::where(['Status' => 'Active','CurrencyCode'=>$currency_code])->select('CurrencyRate', 'FormatedText', 'CurrencyCode')->get();

		if ($currency) {
			foreach ($currency as $key) {
				if ($key->CurrencyCode == 'GHS') {
					$data['GHS'] = str_replace('[AMT]', number_format($shipcost * $key->CurrencyRate, 2), $key->FormatedText);
				} else if ($key->CurrencyCode == 'CAD') {
					$data['CAD'] = str_replace('[AMT]', number_format($shipcost * $key->CurrencyRate, 2), $key->FormatedText);
				} else if ($key->CurrencyCode == 'PHP') {
					$data['PHP'] = str_replace('[AMT]', number_format($shipcost * $key->CurrencyRate, 2), $key->FormatedText);
				} else if ($key->CurrencyCode == 'GBP') {
					$data['GBP'] = str_replace('[AMT]', number_format($shipcost * $key->CurrencyRate, 2), $key->FormatedText);
				} else if ($key->CurrencyCode == 'USD') {
					$data['USD'] = str_replace('[AMT]', number_format($shipcost * $key->CurrencyRate, 2), $key->FormatedText);
				} else if ($key->CurrencyCode == 'KES') {
					$data['KES'] = str_replace('[AMT]', number_format($shipcost * $key->CurrencyRate, 2), $key->FormatedText);
				}
			}
		}
		return $data;

	}
	 public function resionCharges($state) {
        //print_r($state['id']); die;
        $charges = Extraregion::where(['state_id' => $state['id'], 'status' => 'Active'])->select('amount')->first();
        if ($charges) {
            return $charges->amount;
        } else {
            return 0.00;
        }
    }
	
}
