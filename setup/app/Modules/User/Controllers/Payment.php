<?php namespace App\Modules\User\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Models\User;
use Cartalyst\Stripe\Exception\BadRequestException;
use Cartalyst\Stripe\Exception\CardErrorException;
use Cartalyst\Stripe\Exception\ConnectException;
use Cartalyst\Stripe\Exception\InvalidRequestError;
use Cartalyst\Stripe\Exception\InvalidRequestException;
use Cartalyst\Stripe\Exception\MissingParameterException;
use Cartalyst\Stripe\Exception\NotFoundException;
use Cartalyst\Stripe\Exception\ServerErrorException;
use Cartalyst\Stripe\Exception\UnauthorizedException;
use Input;
use Stripe;

class Payment extends Controller
{

    public static function card_list($stripeid)
    {
        $cards = [];
        $user = [];
        $stripe = Stripe::make();
        if (!empty(trim($stripeid))) {
            try {
                $cards = $stripe->cards()->all($stripeid, ['limit' => 20]);
            } catch (BadRequestException $e) {
                $response['msg'] = $e->getMessage();
            } catch (UnauthorizedException $e) {
                $response['msg'] = $e->getMessage();
            } catch (InvalidRequestException $e) {
                $response['msg'] = $e->getMessage();
            } catch (NotFoundException $e) {
                $response['msg'] = $e->getMessage();
            } catch (ServerErrorException $e) {
                $response['msg'] = $e->getMessage();
            } catch (InvalidRequestError $e) {
                $response['msg'] = $e->getMessage();
            } catch (MissingParameterException $e) {
                $response['msg'] = $e->getMessage();
            } catch (ConnectException $e) {
                $response['msg'] = $e->getMessage();
            }
        }
        return $cards;
    }

    public static function add_card($user)
    {

        $stripe = Stripe::make();
        try
        {
            if ($user->StripeId == '') {

                $customer = $stripe->customers()->create([
                    'email' => $user->email,
                ]);

                $user->StripeId = $customer['id'];
                $user->save();
            }
            if (!empty(trim($user->StripeId))) {
                $card = $stripe->cards()->create($user->StripeId, [
                    'number' => Input::get('credit_card_number'),
                    'exp_month' => Input::get('month'),
                    'cvc' => Input::get('security_code'),
                    'exp_year' => Input::get('year'),
                    'name' => Input::get('name_on_card'),
                ]);
                if (isset($card['id'])) {
                    return 1;
                }
            }
        } catch (BadRequestException $e) {
            return $e->getMessage();
        } catch (UnauthorizedException $e) {
            return $e->getMessage();
        } catch (InvalidRequestException $e) {
            return $e->getMessage();
        } catch (NotFoundException $e) {
            return $e->getMessage();
        } catch (ServerErrorException $e) {
            return $e->getMessage();
        } catch (InvalidRequestError $e) {
            return $e->getMessage();
        } catch (MissingParameterException $e) {
            return $e->getMessage();
        } catch (CardErrorException $e) {
            return $e->getMessage();
        }
        //return 'Oops! Something went wrong.';
    }

    public static function capture($user, $amount, $cardid,$capture=false)
    {
        $stripe = Stripe::make();

        try
        {
            if (!empty(trim($user->StripeId))) {

                return $stripe->charges()->create([
                    'customer' => $user->StripeId,
                    'card' => $cardid,
                    'currency' => 'USD',
                    'amount' => $amount,
                    'capture' => $capture,
                    'metadata'=>[
                        'customer_name'=> @$user->Name,
                        'customer_email'=> @$user->Email,
                        'action'=>'user'
                    ],
                    'description'=> @$user->Name,
                ]);
            }
        } catch (BadRequestException $e) {
            return $e->getMessage();
        } catch (UnauthorizedException $e) {
            return $e->getMessage();
        } catch (InvalidRequestException $e) {
            return $e->getMessage();
        } catch (NotFoundException $e) {
            return $e->getMessage();
        } catch (ServerErrorException $e) {
            return $e->getMessage();
        } catch (InvalidRequestError $e) {
            return $e->getMessage();
        } catch (MissingParameterException $e) {
            return $e->getMessage();
        } catch (CardErrorException $e) {
            return $e->getMessage();
        }

    }
}
