<?php namespace App\Modules\User\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Models\Additem;
use App\Http\Models\Category;
use App\Http\Models\CityStateCountry;
use App\Http\Models\Configuration;
use App\Http\Models\Extraregion;
use App\Http\Models\User;
use App\Library\Utility;
use Input;
use Session;

class newCalculation extends Controller {
	/*
		     *Developer Name :Kapil Pancholi
		     *Function :Calculate shipping cost of buy for me request
		     *Update at:24-5-2017
	*/
	public function update_buy_cal() {

		$response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		$userinfo = User::find(Session::get('UserId'));
		$aqlat = Session::get('AqLat');
		$aqlong = Session::get('AqLong');

		$data = [
			'shippingcost' => 0,
			'insurance' => 0,
			'ghana_total_amount' => 0,
			'total_amount' => 0,
			'error' => [],
			'formated_text' => '',
			'distance_in_mile' => 0,
			'product_count' => 0,
			'ProcessingFees' => 0,
			'item_cost' => 0,
			'shipping_cost_by_user' => 0,
			'type' => 'buyforme',
		];

		if (count($userinfo) > 0) {
			$address = json_decode(Input::get('address'));
			$response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
			$response['total_amount'] = 0;
			$response['ghana_total_amount'] = 0;
			$response['shipping_cost'] = 0;

			if (empty(trim(Input::get('request_id')))) {
				$data['product'] = Additem::where(array('user_id' => session()->get('UserId'), 'request_type' => 'buy_for_me'))->get();
			} else {
				$data['product'] = session()->get(trim(Input::get('request_id')));
			}

			if (count($data['product']) > 0 && count($address) > 0) {
				$data['distance'] = Utility::GetDrivingDistance($aqlat, $aqlong, $address->lat, $address->lng);
				$weight = 0;

				$response = $this->getRate($data['product'], $data, $userinfo);
				if ($response['success'] == 1) {
					$data['drop_address'] = $address;
					$response['outside_accra_charge'] = 0;
					//out side accra charges
					$response['data']['AreaCharges'] = $this->resionCharges(['name' => $address->state, 'id' => $address->state_id]);
					$response['data']['total_amount'] = $response['data']['total_amount'] + $response['data']['AreaCharges'];

					//print_r($response);
					//end out side accra charges

					$view = view('User::Buy.ajax.buyforme_calculation', $response['data']);
					$response['html'] = $view->render();
					$response['product'] = $data['product'];
				}
			}
		}
		return json_encode($response);
	}

	public function resionCharges($state) {

		$charges = Extraregion::where(['state_id' => $state['id'], 'status' => 'Active'])->select('amount')->first();
		if ($charges) {
			return $charges->amount;
		} else {
			return 0.00;
		}
	}

	public function get_aquantuo_fees($totalcost) {
		$fees = 0;

		$config = Configuration::find('5673e33e6734c4f874685c84');
		if (count($config) > 0) {
			$fees = ((floatval($totalcost) * $config['aquantuoFees']) / 100);
		}
		return $fees;
	}

	public function getRate($products, $data, $userinfo) {
		$data['airItemWithWeightCount'] = 0;
		$response = [
			'success' => 0,
			'msg' => 'Oops! Something went wrong.',
			'total_amount' => 0,
			'ghana_total_amount' => 0,
			'shipping_cost' => 0,
			'total_weight' => 0,
		];

		$weight = 0;
		foreach ($products as $key => $val) {
			if (!isset($val['shipping_cost_by_user'])) {
				$val['shipping_cost_by_user'] = 0;
			}

			if ($data['type'] == 'buyforme') {
				$data['item_cost'] += $val['price'] * $val['qty'];
				if (!isset($data['userType'])) {
					if ($val['shipping_cost_by_user'] == '') {
						$val['shipping_cost_by_user'] = 5;
					}
				}
				$products[$key]['ProcessingFees'] = Utility::get_processing_fee($val['price'] * $val['qty']);
			}

			$products[$key]['shipping_cost_by_user'] = $val['shipping_cost_by_user'];
			if ($val['travelMode'] == 'air') {
				$weight += $this->GetWeight($val['weight'], $val['weight_unit']) * (int) $val['qty'];
				if ($val['weight'] > 0) {
					$data['airItemWithWeightCount'] += 1;
				}
			}

			$data['shipping_cost_by_user'] += $val['shipping_cost_by_user'];
			$data['product_count'] = $data['product_count'] + 1;

		}
		$response['total_weight'] = $weight;
		$dimention = $this->calculation_inch_volume($products);

		if ($weight > 500) {
			$response['msg'] = "The weight you entered is outside the limits allowed by the airlines. If possible, split your package into smaller packages or choose to ship your item by sea.";
		} else if ($dimention > 1000) {
			$response['msg'] = "The dimensions you entered are outside the limits allowed by the airlines. If possible, split your package into smaller packages or choose to ship your item by sea.";
		} else if ($data['distance']->distance <= 0) {
			$response['msg'] = "Unable to calculate distance.";

		} else {

			if ($data['type'] == 'buyforme') {
				$data['ProcessingFees'] = Utility::get_processing_fee($data['item_cost']);
			}

			//$AirAverageRat = $this->get_air_rate($products, $data['distance']->distance, $weight);
			//$AirAverageRat['airItemWithWeightCount'] = $data['airItemWithWeightCount'];
			//$ShipAverageRate = $this->get_ship_rate($products, $data['distance']->distance, $weight, $AirAverageRat);

			//$products = $ShipAverageRate['product'];

			/*$insurance = $this->GetInsurance($products);
				$products = $insurance['product'];
				$data['product'] = $products;
				if ($insurance['insurance'] == 0 && $insurance['existance'] == true) {
					$response['msg'] = "Sorry! We are not able to provide insurence.";
					return $response;
			*/

			$shipping_calculation = $this->get_shipping_rate($products, $data['distance']->distance);

			if ($shipping_calculation['success'] == 0) {
				$response['success'] = 0;
				$response['msg'] = $shipping_calculation['msg'];
				return $response;

			}

			$data['shipping_cost'] = $shipping_calculation['totalShippingCost'];
			$products = $shipping_calculation['product'];
			$data['product'] = $products;
			$insurance = $this->GetInsurance($products);
			if ($insurance['insurance'] == 0 && $insurance['existance'] == true) {
				$response['msg'] = "Sorry! We are not able to provide insurence.";
				return $response;
			}

			$data['insurance'] = $insurance['insurance'];
			//$data['airShippingCost'] = $AirAverageRat['rate'];
			//$data['airShippingItemQty'] = $AirAverageRat['item'];
			//$data['shipping_cost'] = $ShipAverageRate['totalShippingCost'];

			$data['total_amount'] = $data['shipping_cost'] + $data['insurance'] + $data['ProcessingFees'] + $data['shipping_cost_by_user'] + $data['item_cost'];

			/*if ($ShipAverageRate['rate'] == 0 && $ShipAverageRate['existance'] == true) {
				$response['msg'] = "Package info you entered does not match with our shipping conditions, Please check package info and try again.";
				// $response['msg'] = "Sorry! We do don't deliver items of entered .";
				return $response;
			}*/

			/*if ($AirAverageRat['rate'] == 0 && $AirAverageRat['existance'] == true) {
				$response['msg'] = "Package info you entered does not match with our shipping conditions, Please check package info and try again.";
				//$response['msg'] = "Sorry! We do don't deliver items of entered weight.";
				return $response;
			}*/

			/*if ($data['shipping_cost'] == 0) {
				$response['msg'] = "Package info you entered does not match with our shipping conditions, Please check package info and try again.";
				return $response;
			}*/
			$response['success'] = 1;

			$De_currency = '';
			if (isset($userinfo->Default_Currency)) {
				if (in_array($userinfo->Default_Currency, ['', 'USD'])) {
					$userinfo->Default_Currency = 'GHS';
					$De_currency = 'GHS';
				}
			} else {
				//$userinfo->Default_Currency = $userinfo['Default_Currency'];
				$De_currency = $userinfo['Default_Currency'];
			}

			$currency = CityStateCountry::where(['CurrencyCode' => $De_currency])->first();

			if (count($currency) > 0) {
				$response['ghana_total_amount'] = $response['total_amount'] * $currency->CurrencyRate;
				$data['ghana_total_amount'] = $response['ghana_total_amount'];
				$data['formated_text'] = $currency->FormatedText;
			}

		}

		$response['data'] = $data;
		//print_r($response['data']['total_amount']); die;
		return $response;
	}
	//Close
	/*
	     *Developer Name :Kapil Pancholi
	     *Function :This function is used to calculate shiping price of product which travel mode is air
	     *Update at:25-5-2017

*/

	public function get_shipping_rate($array, $distance) {

		$res['item'] = 0;
		$res['rate'] = 0;
		$res['existance'] = false;
		$res['success'] = 0;
		$res['totalShippingCost'] = 0;
		$res['msg'] = '';
		$distance = $this->get_distance($distance);

		foreach ($array as $key => $val) {
			if ($val['travelMode'] == 'air') {
				$data = Category::where(['_id' => $val['categoryid']])->where(['Status' => 'Active', 'TravelMode' => 'air', 'ChargeType' => 'fixed'])
					->select('Shipping')
					->first();

				$weight = $this->GetWeight($val['weight'], $val['weight_unit']) * (int) $val['qty'];
				//print_r($val['categoryid']); die;

				if (count($data) > 0) {
					$array[$key]['shippingCost'] = 0;
					foreach ($data['Shipping'] as $key1) {
						if ($key1['MinDistance'] <= floatval($weight) && $key1['MaxDistance'] >= floatval($weight)) {
							$array[$key]['shippingCost'] = $key1['Rate'];
							$res['rate'] += $key1['Rate'];
							$res['totalShippingCost'] += $key1['Rate'];
							$res['success'] = 1;

						}
					}

					if ($array[$key]['shippingCost'] == 0) {
						$res['success'] = 0;
						$res['msg'] = "Shipping conditions does not match with product " . @$val['product_name'] . "";
					}

				} else {
					$res['success'] = 0;
					$res['msg'] = "Shipping conditions does not match with product " . @$val['product_name'] . "";
				}
			} else if ($val['travelMode'] == 'ship') {
				$data = Category::where(['_id' => $val['categoryid']])->where(['Status' => 'Active', 'TravelMode' => 'ship', 'ChargeType' => 'distance'])->select('Shipping')->first();
				
				if (count($data) > 0) {
					foreach ($data['Shipping'] as $key1) {
						if ($key1['MinDistance'] <= $distance && $key1['MaxDistance'] >= $distance) {
							$res['rate'] += $key1['Rate'] * (int) $val['qty'];
							//print_r($distance); die;
							$array[$key]['shippingCost'] = $key1['Rate'] * (int) $val['qty'];
							$res['totalShippingCost'] += $key1['Rate'] * (int) $val['qty'];
							$res['success'] = 1;
						}
					}

					if ($array[$key]['shippingCost'] == 0) {
						$res['success'] = 0;
						$res['msg'] = "Shipping conditions does not match with product " . @$val['product_name'] . "";
					}
				} else {
					$res['success'] = 0;
					$res['msg'] = "Shipping conditions does not match with product " . @$val['product_name'] . "";
				}
			}
		}
		$res['product'] = $array;
		return $res;

	}

	public function get_air_rate($array, $distance, $weight) {

		// @$array is array of product
		$res['item'] = 0;
		$res['rate'] = 0;
		$res['existance'] = false;
		$res['success'] = 0;

		foreach ($array as $key) {
			if ($key['travelMode'] == 'air') {
				$res['existance'] = true;

				$data = Category::where(['_id' => $key['categoryid']])->where(['Status' => 'Active', 'TravelMode' => 'air', 'ChargeType' => 'fixed'])
					->select('Shipping')
					->first();

				if (count($data) > 0) {
					foreach ($data['Shipping'] as $key1) {
						if ($key1['MinDistance'] <= $weight && $key1['MaxDistance'] >= $weight) {
							$res['rate'] += $key1['Rate'];
							//	print_r($weight); die;
						}
					}

					$res['success'] = 1;
					$res['item'] += 1;

				}
			}
		}
		//To return average price

		if ($res['item'] > 0) {
			//$res['rate'] = number_format(($res['rate'] / $res['item']), 2, '.', '');
			$res['rate'] = $res['rate'];
			return $res;
		} else {
			return $res;
		}
	} //close
	/*
	     *Developer Name :Kapil Pancholi
	     *Function :This function is used to calculate shiping price of product which         travel mode is ship
	     *Update at:25-5-2017
*/

	public function get_ship_rate($array, $distance, $weight, $AirAverageRat) {
		//$AirAverageRat['item']
		$itemPricePerUnit = $itemPricePerLbs = 0;
		if ($AirAverageRat['item'] > 0) {
			$itemPricePerUnit = floatval(number_format(($AirAverageRat['rate'] / $AirAverageRat['item']), 2, '.', ''));
			$itemPricePerLbs = floatval(number_format(($itemPricePerUnit * $AirAverageRat['airItemWithWeightCount']), 2, '.', ''));

			if ($weight > 0) {
				$itemPricePerLbs = floatval(number_format($itemPricePerLbs / $weight, 2, '.', ''));
			}
		}

		$res['totalShippingCost'] = 0;
		$res['rate'] = 0;
		$res['existance'] = false;
		$res['success'] = 0;
		$distance = $this->get_distance($distance);

		foreach ($array as $key => $val) {
			if ($val['travelMode'] == 'ship') {
				$res['existance'] = true;
				$data = Category::where(['_id' => $val['categoryid']])->where(['Status' => 'Active', 'TravelMode' => 'ship', 'ChargeType' => 'distance'])
				//->where('Shipping.MinDistance', '<=', $distance)
				//->where('Shipping.MaxDistance', '>=', $distance)
					->select('Shipping')
					->first();
				if (count($data) > 0) {
					$res['success'] = 1;
					//

					foreach ($data['Shipping'] as $key1) {
						if ($key1['MinDistance'] <= $distance && $key1['MaxDistance'] >= $distance) {
							$res['rate'] += $key1['Rate'] * (int) $val['qty'];
							//print_r($distance); die;
							$array[$key]['shippingCost'] = $key1['Rate'] * (int) $val['qty'];
							$res['totalShippingCost'] += $key1['Rate'] * (int) $val['qty'];
						}
					}
					//
					//$res['rate'] += $data['Shipping'][0]['Rate'] * (int) $val['qty'];
					//$array[$key]['shippingCost'] = $data['Shipping'][0]['Rate'] * (int) $val['qty'];
				}
			} else {

				/*   $array[$key]['shippingCost'] = $AirAverageRat['rate'] / (int) $AirAverageRat['item'];*/
				// Start calculate air weight
				$itemWeight = $this->GetWeight($val['weight'], $val['weight_unit']) * (int) $val['qty'];

				if ($itemWeight > 0) {
					$array[$key]['shippingCost'] = floatval(number_format(($itemPricePerLbs * $itemWeight), 2, '.', ''));
					$res['totalShippingCost'] += floatval(number_format(($itemPricePerLbs * $itemWeight), 2, '.', ''));
				} else {
					$array[$key]['shippingCost'] = $itemPricePerUnit;
					$res['totalShippingCost'] += $itemPricePerUnit;
				}

				// End calculate air weight
			}
			//$res['totalShippingCost'] += $array[$key]['shippingCost'];
		}

		$res['product'] = $array;

		//print_r($res['totalShippingCost']); die;
		return $res;
	} //close
	/*
	     *Developer Name :Kapil Pancholi
	     *Function :This function is used to calculate volume in inch
	     *Update at:24-5-2017
*/

	public function calculation_inch_volume($array) {
		$intch = 0;
		foreach ($array as $key) {
			if ($key['travelMode'] == 'air') {
				$intch += $this->get_size_in_inch(@$key['height'], 'inches') + $this->get_size_in_inch(@$key['length'], 'inches') + $this->get_size_in_inch(@$key['width'], 'inches');
				$intch = $intch * (int) $key['qty'];
			}
		}
		return $intch;
	} //close
	/*
	     *Developer Name :Kapil Pancholi
	     *Function :This function is used to convert convert dimention unit
	     *Update at:24-5-2017
*/
	public function get_size_in_inch($length, $unit) {
		$unit = strtolower($unit);
		switch ($unit) {
		case 'inches':
			return floatval($length);
			break;
		case 'cm':
			return floatval($length) * 0.393701;
			break;
		}
	} //close
	/*
	     *Developer Name :Kapil Pancholi
	     *Function :This function is used to calculate insurance
	     *Update at:24-5-2017
*/
	public function GetInsurance($product) {
		$res = [
			'insurance' => 0,
			'existance' => false,
			'price' => 0,
			'product' => [],
		];

		$data = Configuration::where(array('_id' => '5673e33e6734c4f874685c84'))
			->select('Insurance')->first();

		foreach ($product as $productKey => $item) {
			$product[$productKey]['insurance'] = 0;
			if ($item['insurance_status'] == 'yes') {

				$res['price'] += $item['price'] * $item['qty'];
				if (count($data) > 0) {
					if (is_array($data->Insurance)) {
						foreach ($data->Insurance as $key) {
							if ($key['MinPrice'] <= (float) $item['price'] && $key['MaxPrice'] >= (float) $item['price']) {
								$res['insurance'] += $key['Rate'] * $item['qty'];
								$product[$productKey]['insurance'] = $key['Rate'] * $item['qty'];
								break;
							}
						}
					}
				}
			}
		}
		/* if ($res['existance'] == true) {
			        $data = Configuration::where(array('_id' => '5673e33e6734c4f874685c84'))
			        ->select('Insurance')->first();
			        if (count($data) > 0) {
			        if (is_array($data->Insurance)) {
			        foreach ($data->Insurance as $key) {
			        if ($key['MinPrice'] <= (float) $res['price'] && $key['MaxPrice'] >= (float) $res['price']) {
			        $res['insurance'] = $key['Rate'] * ;
			        break;
			        }
			        }
			        }
			        }
		*/
		$res['product'] = $product;
		return $res;
	} //close
	/*
	     *Developer Name :Kapil Pancholi
	     *Function :This function is used to convert weight in to lbs
	     *Update at:24-5-2017
*/
	public function GetWeight($weight, $type) {
		$type = strtolower($type);
		if ($type == 'kg') {
			return (((float) $weight) * 2.20462);
		} else if ($type == 'gram') {
			return (((float) $weight) * 0.00220462);
		} else {
			return $weight;
		}
	}
	/*
		     *Developer Name :Kapil Pancholi
		     *Function :This function is used to convert distance in to miles
		     *Update at:25-5-2017
	*/
	public function get_distance($distance) {
		return $distance = floatval($distance) * 0.000621371;
	} //close
}
