@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')
@section('content')
<div class="container-fluid">
	<div class="panel panel-default">
      	<div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;Trip Detail
         <a style="margin-right:5px;" class="btn btn-primary margin-top-less-7 pull-right" href="{{url($back)}}">Go Back</a>
      	</div>

      	<div class="panel-body">
         	<div class="col-sm-4 custom-table1">
                  <div class="row">
                     <b>Transporter Name</b>:
                    

                     <a href="{{ url('admin/user/detail',$info->TransporterId) }}">{{ucfirst($info->TransporterName)}}</a>
                     
                  </div>
            </div>
            <div class="col-sm-4 custom-table1">
                  <div class="row">
                     <b>Shipping Mode</b>:
                     
                     	 @if(@$index['travelMode'] == 'ship') 
					 		Sea
					 	@else
					 		{{ucfirst($info->TravelMode)}}
					 	@endif
                  </div>
            </div>

            <div class="col-sm-4 custom-table1">
                  <div class="row">
                     <b>Trip Type</b>:
                     {{ucfirst($info->TripType)}}
                  </div>
            </div>

            <div class="col-sm-4 custom-table1">
                  <div class="row">
                     <b>Flight No</b>:
                     {{ucfirst($info->FlightNo)}}
                  </div>
            </div>

            <div class="col-sm-4 custom-table1">
                  <div class="row">
                     <b>Airline</b>:
                     {{ucfirst($info->Airline)}}
                  </div>
            </div>

            <div class="col-sm-4 custom-table1">
                  <div class="row">
                     <b>Weight</b>:
                     {{$info->Weight}} {{$info->Unit}}
                  </div>
            </div>
            

            <div class="col-sm-4 custom-table1">
                  <div class="row">
                     <b>Categories</b>:
                     <?php
						if (is_array($info['SelectCategory'])) {
						    echo implode(", ", $info['SelectCategory']);
						} else {
						    echo 'n/a';
						}

					?>
                  </div>
            </div>

            <div class="col-sm-4 custom-table1">
                  <div class="row">
                     <b>Created at</b>:
                     {{  show_date(@$key->EnterOn) }}
                  </div>
            </div>

            <div class="col-sm-4 custom-table1">
                  <div class="row">
                     <b>Update Date</b>:
                     {{  show_date(@$key->UpdateDate) }}
                  </div>
            </div>

            <div class="col-sm-4 custom-table1">
                  <div class="row">
                     <b>Source Address</b>:
                     {{$info->SourceFullAddress}} 
                  </div>
            </div>

            <div class="col-sm-4 custom-table1">
                  <div class="row">
                     <b>Destination Address</b>:
                     {{$info->DestFullAddress}} 
                  </div>
            </div>

            <div class="col-sm-4 custom-table1">
                  <div class="row">
                     <b>Status</b>:
                     {{$info->Status}} 
                  </div>
            </div>

            <div class="col-sm-4 custom-table1">
                  <div class="row">
                     <b>Departure Date</b>:
                     <?php
                     print_r($key->SourceDate); die;
                     if (isset($key->SourceDate->sec)) {
                     	echo date('M d, Y h:i A',$key->SourceDate->sec)
                     }
                     ?>
                    
                      
                  </div>
            </div>

            <div class="col-sm-4 custom-table1">
                  <div class="row">
                     <b>Arrival Date</b>:
                     
                     {{  show_date(@$key->DestiDate) }} 
                  </div>
            </div>

            
            <div class="col-sm-12 custom-table1">
                  <div class="row">
                     <b>Description</b>:
                     {{ucfirst($info->Description)}} 
                  </div>
            </div>
            

            
        </div>
    </div>
</div>
@include('Admin::layouts.footer')
@stop