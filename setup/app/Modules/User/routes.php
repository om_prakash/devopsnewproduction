<?php

Route::group(['module' => 'User', 'namespace' => 'App\Modules\User\Controllers'], function () {

	Route::get('baseurl', 'Paginate@baseurl');



	Route::get('buy-for-me', 'Userctrl@buy_for_me');
	Route::post('buy-for-me', 'Ajax@address_list');
	Route::get('buy-for-me-list', 'Userctrl@buy_for_me_list');
	//Route::get('buy-for-me-detail', 'Userctrl@delivery_detail');
	Route::post('post-buy-for-me', 'Userctrl@post_buy_for_me');
	/* start buy for me section */

	Route::get('buyforme-payment/{id}', 'Userctrl@buyforme_payment');
	Route::get('buyforme-pay-card/{requestid}/{id}', 'Userctrl@buyforme_request_card_pay');
	Route::get('payment-by-for-me/{id}', 'Userctrl@buy_for_me_card_list');
	Route::get('pay-now-by-for-me/{requestid}/{id}', 'Userctrl@pay_for_buy_for_item');

	Route::post('add-online-item', 'Userctrl@add_online_item');

	Route::post('post-online-purchase', 'Userctrl@post_online_purchase');
	Route::post('validate_promocode', 'Ajax@validate_promocode');

	Route::get('my-request', 'Userctrl@my_request');
	Route::post('pagination/my-request', 'Paginate@request_list');

	Route::get('my-request-detail', 'Userctrl@delivery_detail2');
	Route::Post('/requester-review', 'Ajax@requester_review');
	//Route::Post('/transporter-review', 'Transporter@transporter_review');
	Route::get('edit-item/{id}', 'Userctrl@edit_item');
	Route::post('edit-online-item/{id?}', 'Userctrl@edit_online_item');

	//Route::get('reward', 'Userctrl@reward');
	Route::get('prepare-request/{id?}', 'Userctrl@prepare_request');
	Route::get('prepare-request-calculation', 'Ajax@prepare_request_calculation');
	Route::get('validate-promocode', 'Ajax@validate_promocode');
	Route::post('prepare-request', 'Ajax@post_prepare_request');

	Route::any('online-purchase-calculation', 'Ajax@online_purchase_calculation');
	/*Route::Post('buyforme-calculation', 'Ajax@buyforme_calculation');*/
	Route::Post('buyforme-calculation', 'NewCalculation@update_buy_cal');

	Route::get('process-card-list/{id}', 'Userctrl@request_card_list');
	Route::get('pay-now/{requestid}/{id}', 'Userctrl@pay_now');

	Route::get('online-payment/{id}', 'Userctrl@online_request_payment');
	Route::get('online-pay-card/{requestid}/{id}', 'Userctrl@online_request_card_pay');
	Route::get('online-request-detail/{requestid}', 'Userctrl@online_request_detail');

	/* end buy for me section */

	Route::post('user/check-chat-user', 'Ajax@check_chat_user');

	Route::get('my-profile', 'Userctrl@my_profile');
	Route::get('edit-profile', 'Userctrl@edit_profile');
	Route::post('post-edit-profile', 'Userctrl@post_edit_profile');
	Route::get('edit-transporter-profile', 'Userctrl@edit_transporter_profile');
	Route::post('post-edit-transporter-profile', 'Userctrl@post_edit_transporter_profile');

	Route::get('dashboard', 'Userctrl@dashboard');
	Route::get('logout', 'Userctrl@logout');
	Route::any('change-password', 'Userctrl@change_password');
	Route::post('change_password', 'Userctrl@changepassword');
	Route::get('bank-information', 'Userctrl@bank_information');
	Route::get('currency', 'Userctrl@currency');
	Route::post('edit-currency', 'Userctrl@edit_currency');
	Route::get('card-list', 'Userctrl@card_list');
	Route::get('edit-bank-information', 'Userctrl@edit_bank_information');
	Route::post('post-edit-bank-information', 'Userctrl@post_edit_bank_information');
	Route::any('delete-card/{id}', 'Userctrl@delete_card');

	Route::get('delete-request/{id}/{function}', 'Userctrl@delete_request');
	Route::get('delete_address/{id}/{function}', 'Userctrl@delete_request');
	Route::get('delete_item/{id}/{function}', 'Userctrl@delete_item');
	Route::get('delivery-details/delete-request/{id}', 'Userctrl@delete_delivery_request');
	//Route::get('edit-request/{id}','Userctrl@edit_request');
	Route::get('edit-buy-for-me/{id}', 'Userctrl@edit_buy_for_me');

	Route::get('my-aquantuo-address', 'Userctrl@aquantuo_address');
	Route::get('address', 'Userctrl@address');
	Route::post('edit_address/{id}', 'Userctrl@edit_address');
	Route::get('setting', 'Userctrl@settings');
	Route::get('new-request', 'Userctrl@new_request');
	Route::post('email-status', 'Userctrl@email_status');
	Route::post('edit-address/{id}', 'Userctrl@edit_address');
	Route::post('edit-item', 'Userctrl@edit_item');
	Route::get('buy-for-me-detail/{id}', 'Userctrl@buy_for_me_detail');

	Route::get('delivery-details/{_id}', 'Userctrl@delivery_details');
	Route::get('online/delivery-details/{_id}', 'Userctrl@online_delivery_details');
	Route::get('posts-by-transporters', 'Userctrl@posts_by_transporters');

	Route::get('near-by-transporter', 'Userctrl@near_by_transporter');
	Route::post('pagination/near-by-transporter', 'Paginate@near_by_transporter');

	Route::post('pagination/transporter', 'Paginate@new_request');

	Route::Post('pagination/transporter/my-deliveries', 'Paginate@my_deliveries');

	/*
		* Online request section
	*/
	Route::get('on-line-purchases', 'Userctrl@on_line_purchases');
	Route::Post('pagination/on-line-purchases', 'Paginate@on_line_purchases');
	Route::get('online-purchase-details/{_id}', 'Userctrl@online_purchase_details');
	Route::get('edit-online-purchase/{id}', 'Userctrl@edit_online_purchese');

	//Route::get('my-request', 'Userctrl@my_request');
	//Route::post('pagination/my-request','Paginate@request_list');

	Route::get('trip-detail/{id}', 'Userctrl@trip_detail');
	Route::post('pagination/trip-detail/{id}', 'Paginate@trip_detail');
	Route::any('add-card', 'Userctrl@add_card');
	Route::post('add-address', 'Userctrl@add_address');
	//Route::get('validate_promocode');
	Route::get('edit-item/{id}', 'Userctrl@edit_item');
	Route::any('/city', 'Userctrl@edit_profile');
	//Transporter
	Route::get('transporter', 'Transporter@new_request');
	Route::post('Paginate/new_request', 'Paginate@new_request');

	Route::get('transporter/my-deliveries', 'Transporter@my_deliveries');
	Route::get('transporter/post-a-trip', 'Transporter@post_a_trip');
	Route::post('post-trip', 'Transporter@insert_post_trip');
	Route::get('transporter/details/{id}', 'Transporter@details');
	Route::get('transporter/delivery-details/{id}', 'Transporter@details');
	Route::get('transporter/package-delivery-details/{id}', 'Transporter@details');
	Route::get('transporter/buy_for_me_detail/{id}', 'Transporter@buy_for_details');
	Route::get('transporter/online_detail/{id}','Transporter@online_details');
	Route::get('transporter/local-delivery-details/{id}', 'Transporter@localDetails');

	Route::post('/accept-request-transporter/{id}', 'Transporter@request_accept_by_transporter');
	Route::post('/cancel-request-transporter/{id}', 'Transporter@cancel_request');
	Route::post('/pickup-request-transporter/{id}', 'Transporter@pickup_request');
	Route::post('/deliver-request-transporter/{id}', 'Transporter@delivery_request');

	Route::post('/complete-request-transporter/{id}', 'Transporter@complete_request');
	Route::any('/no_delivery/{id}', 'Transporter@no_delivery');
	Route::Post('/transporter-review', 'Transporter@transporter_review');

	Route::Post('/post-requester-review', 'Ajax@buy_for_me_requester_review');
	Route::post('/online-requester-review', 'Ajax@online_requester_review');
	Route::Post('verify-delivery-code', 'Transporter@verify_delivery_code');

	Route::Post('buy-for-me-verify', 'Transporter@buy_for_me_verify');
	Route::Post('online-verify', 'Transporter@online_verify');

	Route::any('/by_for_cancel_delivery/{id}', 'Transporter@by_for_cancel_delivery');
	Route::any('/online_cancel_delivery/{id}', 'Transporter@online_cancel_delivery');
	Route::Post('/review-buy-for-me', 'Transporter@transporter_review_to_buy_for');
	Route::Post('/review-online', 'Transporter@transporter_review_to_online');

	// Transporter Trip-section
	Route::get('transporter/bussiness-trips', 'Transporter@my_trips');
	Route::Post('pagination/transporter/my-trips', 'Paginate@my_trips');
	Route::get('transporter/individual-trips', 'Transporter@my_individual_trips');
	Route::Post('pagination/transporter/individual-trips', 'Paginate@individual_trips');
	Route::get('transporter/edit-trips/{id}', 'Transporter@edit_trip');
	Route::post('transporter/edit_bussiness_trip/{id}', 'Transporter@edit_bussiness_trip');
	Route::get('transporter/edit_individual/{id}', 'Transporter@edit_individual');
	Route::post('transporter/edit_individual_trip/{id}', 'Transporter@edit_individaul_trip');
	/*
		     * Description: Post trip by transporter will show in requester side
	*/

	Route::get('post-by-transporter', 'Userctrl@post_by_transporter');
	Route::Post('pagination/transporter/post-by-transporter', 'Paginate@post_by_transporter');

	//notification
	Route::get('notification', 'Userctrl@notification');
	Route::any('pagination/notification-list', 'Paginate@notification');
	Route::get('delete-notification', 'Userctrl@delete_notification');

	//
	Route::get('edit-request/{id}', 'Userctrl@edit_prepare_request');
	Route::post('edit_prepare_request', 'Ajax@edit_prepare_request');

	Route::get('check-system', 'System@check_system');

	//transporter buy for me request
	Route::post('transporter-buy-for-me/{requestId}', 'Transporter@buy_for_me_request');
	Route::post('transporter-online/{requestId}', 'Transporter@online_request');
	/*
		     * Description: This is used for get list of requests which is related to trip
	*/

	Route::get('transporter/view-request/{id}', 'Transporter@view_request_trip');
	Route::post('pagination/view_trip_request/{id}', 'Paginate@view_trip_request');
	/*
		     * Description: This is used during edit buy-for-me and online-purchase request
	*/
	Route::post('add-product', 'Userctrl@add_product');
	// Route::post('add-online-item', 'Userctrl@add_online_item');

	/*
		     * Description: This is used during add buy-for-me and online-purchase request
	*/
	Route::post('add-Item', 'Userctrl@add_item');
	/*
		     * Description: This is used for get support page.
	*/
	Route::Get('support', 'Userctrl@support_page');
	Route::any('reward', 'Userctrl@rewords');
	/*
		     * Description: This is used for add support data.
	*/
	Route::Post('post-support', 'Userctrl@post_support');
	Route::get('statistics', 'Userctrl@statistics');
	Route::get('invite-friend', 'Userctrl@invite_friend');
	/*
		     * Description: This is used for listing of faq .
	*/
	Route::any('faq-list', 'Userctrl@faq_list');
	Route::any('pagination/faq-list', 'Paginate@faq_list');
	Route::any('json', 'Payment@json');

	Route::any('pending-request', 'Userctrl@requester_list');
	Route::post('pagination/pending-request', 'Paginate@pending_request');
	Route::any('transporter-pending-request', 'Userctrl@transporter_pending');
	Route::post('pagination/transporter-pending-request', 'Paginate@transporter_pending');

	Route::get('become-transporter', 'Userctrl@post_become_transporter');
	Route::post('post-transporter-edit-profile', 'Ajax@post_transporter_edit_profile');
	Route::post('transporter-edit-profile', 'Ajax@transporter_edit_profile');

	Route::post('update-notification-id', 'Ajax@update_information');
	Route::post('need_package_material', 'Ajax@need_package_material');

	Route::get('delete_user/{id}', 'Userctrl@delete_account');

	Route::get('transporter/change-status/{id}/{function}', 'Transporter@change_trip_status');

	Route::get('promo_code', 'Userctrl@promo_code');

//    Route::get('track_ordder', 'Userctrl@track_ordder');
	//    Route::post('post_track_ordder', 'Userctrl@post_track_ordder');

	Route::get('track-order-detail', 'Developementctrl@track_order_detail');

	Route::get('testimonial', 'Userctrl@testimonial');
	/*New Send package*/
	Route::post('prepare-request-add-item', 'SendPackage@add_sendpackage_item');
	Route::get('create-request', 'SendPackage@sendPackage');
	Route::get('prepare-request-edit-item/{id}/{requestid?}', 'SendPackage@editSendPackage');
	Route::post('prepare_request_calculation', 'SendPackage@prepare_request_calculation');
	Route::get('prepare-add-item/{id?}', 'SendPackage@add_item_page');
	Route::post('prepare-request-add-item2', 'SendPackage@post_add_item');
	Route::post('prepare_request_create', 'SendPackage@create_prepare_request');

	Route::any('new-prepare-request/{id?}', 'SendPackage@prepare_request');
	Route::get('send-package-detail/{id}', 'SendPackage@package_detail');
	Route::get('send-package-paynow/{requestid}/{id}', 'SendPackage@pay_now');
	Route::get('new-my-request', 'SendPackage@my_request');
	Route::post('pagination/new-my-request', 'Paginate@request_list2');

	Route::post('add-item-exist-request', 'SendPackage@addItemInExistRequest');
	Route::post('edit-item-exist-request', 'SendPackage@editItemInExistRequest');
	Route::post('edit_prepare_request_calculation', 'SendPackage@edit_calculation');
	Route::get('remove-item/{itemid}/{requestid}', 'SendPackage@remove_item');
	Route::get('send_package-item-pay/{itemid}/{requestid}', 'SendPackage@item_payment_page');
	Route::get('send_package-item-pay2/{requestid}', 'SendPackage@item_payment_page2');
	Route::get('item-paynow/{requestid}/{itemid}/{cardid}/{type}', 'SendPackage@item_payment');
	Route::get('item-paynow2/{requestid}/{cardid}/{type}', 'SendPackage@item_payment2');

	// local delivery
	Route::post('local-prepare-request-add-item', 'LocalDeliveryController@local_add_sendpackage_item');
	Route::get('local-create-request', 'LocalDeliveryController@localSendPackage');
	Route::get('local-prepare-request-edit-item/{id}/{requestid?}', 'LocalDeliveryController@localEditSendPackage');
	Route::post('local_prepare_request_calculation', 'LocalDeliveryController@local_prepare_request_calculation');
	Route::get('local-prepare-add-item/{id?}', 'LocalDeliveryController@local_add_item_page');
	Route::post('local-prepare-request-add-item2', 'LocalDeliveryController@local_post_add_item');
	Route::post('local_prepare_request_create', 'LocalDeliveryController@local_create_prepare_request');

	Route::any('local-new-prepare-request', 'LocalDeliveryController@local_prepare_request');
	Route::get('local-delivery-detail/{id}', 'LocalDeliveryController@local_delivery_detail');
	Route::get('local-delivery-paynow/{requestid}/{id}', 'LocalDeliveryController@local_pay_now');
	//Route::get('local-delivery-process-card-list/{id}', 'Userctrl@local_delivery_request_card_list');
	Route::get('local-delivery-process-card-list/{id}', 'LocalDeliveryController@local_delivery_request_card_list');

	Route::get('local-new-my-request', 'LocalDeliveryController@local_my_request');
	Route::post('pagination/local-new-my-request', 'LocalDeliveryController@local_request_list2');

	Route::post('local-add-item-exist-request', 'LocalDeliveryController@localAddItemInExistRequest');
	Route::post('local-edit-item-exist-request', 'LocalDeliveryController@localEditItemInExistRequest');
	Route::post('local_edit_prepare_request_calculation', 'LocalDeliveryController@local_edit_calculation');
	Route::get('local-remove-item/{itemid}/{requestid}', 'LocalDeliveryController@local_remove_item');

	Route::get('edit-local-delivery/{id}', 'Userctrl@local_edit_prepare_request');

	Route::get('local-delivery-alldetails/{_id}', 'Userctrl@local_delivery_details');

	Route::get('local-edit-request/{id}', 'Userctrl@local_edit_prepare_request');
	Route::post('local_edit_prepare_request', 'Ajax@local_edit_prepare_request');
	Route::post('local-delivery-accept-request/{id}/{requestid}', 'NewTransporter@localrequest_accept_by_transporter');
	Route::post('local-delivery-pickup-request/{id}/{product_id}', 'NewTransporter@localpickup_request');
	Route::post('local-delivery-deliver-request/{id}/{product_id}', 'NewTransporter@localdelivery_request');
	Route::Post('local-delivery-verify-delivery-code', 'NewTransporter@localverify_delivery_code');
	//
	Route::Post('/send-package-nodelivery', 'NewTransporter@send_package_nodelivery');
	Route::post('/send-package-cancel-request/{id}/{product_id}', 'NewTransporter@cancel_request');
	Route::post('/send-package-accept-request/{id}/{requestid}', 'NewTransporter@request_accept_by_transporter');
	Route::post('/send-package-pickup-request/{id}/{product_id}', 'NewTransporter@pickup_request');
	Route::post('/send-package-shipment-departed/{id}/{product_id}', 'NewTransporter@ShipmentDeparted');
	Route::post('/send-package-deliver-request/{id}/{product_id}', 'NewTransporter@delivery_request');
	Route::Post('/send-package-transporter-review', 'NewTransporter@transporter_review');
	Route::Post('send-package-verify-delivery-code', 'NewTransporter@verify_delivery_code');
	Route::Post('send-package-get-info', 'NewTransporter@verify_delivery_code');
	Route::get('clear-cache','Userctrl@clear_cache');

	Route::get('get_item_info/{id}','Developementctrl@get_item_info');
	Route::get('get_item_info2/{rid}/{id}','Developementctrl@get_item_info2');

	/* Mobile Money */
	Route::get('pay-by-mobile-money/{id}', 'MobileMoney2@getMobileno');
	Route::post('add-mobile-number/{id}', 'MobileMoney2@addMobileNumber');
	Route::get('delete-mobile/{id}/{reqid}', 'MobileMoney2@deleteMobile');
	Route::get('mobile-payment/{id}/{reqid}', 'MobileMoney2@mobilePayment');
	Route::post('Voucher-mobile-payment/{id}', 'MobileMoney2@vouchermobilePayment');
	Route::get('mobile-payment-mtn/{id}/{reqid}', 'MobileMoney2@mobilePayment_mtn');

});
