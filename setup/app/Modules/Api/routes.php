<?php
Route::group(array('prefix' => 'api/v1', 'module' => 'Api', 'namespace' =>
	'App\Modules\Api\Controllers'), function () {

	Route::get('check_mail', 'LocalDelivery@check_mail');
	Route::post('item-payment', 'Customer@item_payment');
	Route::post('local-delivery-calculation', 'LocalDelivery@calculation');
	Route::get('test-mail', 'Customer@test_mail');
	Route::post('create-local-delivery-request', 'LocalDelivery@create_local_delivery_request');
	Route::get('local-delivery-detail/{id?}', 'LocalDelivery@local_delivery_detail');
	Route::post('update-local-delivery-request', 'LocalDelivery@update_local_delivery_request');

	// Transporter side
	Route::post('delivery-process', 'Transporter@delivery_process');
	Route::get('transporter-assign-request', 'Transporter@transporterAssignrequest');
	Route::get('transporter-detail', 'Transporter@transporterDetail');
	Route::post('send-package-mobile-verify', 'Transporter@send_package_mobile_verify');
	Route::post('request-accept-by-transporter', 'Transporter@request_accept_by_transporter');
	Route::post('transporterRating', 'Transporter@transporterRating');
	Route::post('send-package-cancel-delivery', 'Transporter@sendPackageCancelDelivery');
	Route::post('package-material-status', 'Transporter@packageMaterialStatus');
	Route::post('local-request-payment', 'LocalDelivery@package_payment');
	Route::get('transporter-list', 'LocalDeliveryTransporter@transporterlist');
	Route::get('myDeliveries', 'LocalDelivery@myDeliveries');

	// local delivery transporter side
	Route::any('local-delivery-accept', 'LocalDeliveryTransporter@LocalDeliveryAccept');
	Route::post('local-delivery-process', 'LocalDeliveryTransporter@localDeliveryProcess');
	Route::post('local-delivery-mobile-verify', 'LocalDeliveryTransporter@localDeliveryMobileVerify');
	Route::get('local-delivery-transporter-detail', 'LocalDeliveryTransporter@LocalTransporterDetail');
	Route::post('local-delivery-transporterRating', 'LocalDeliveryTransporter@LocalTransporterRating');
	Route::post('local-material-status', 'LocalDeliveryTransporter@LocalMaterialStatus');
	Route::post('local-cancel-delivery', 'LocalDeliveryTransporter@localCancelDelivery');
	Route::post('insertDate', 'Transporter@insertDate');
	Route::post('cityInsert', 'Transporter@cityInsert');
	Route::get('retrievePayment', 'Customer@retrievePayment');
	Route::get('retrievePayment2', 'Customer@retrievePayment2');

	Route::post('add-mobile-number','MobileMoney2@addmobilenumber');
	Route::post('mobile-money-responce','MobileMoney2@mobile_money_responce');
	Route::post('delete-mobile-no','MobileMoney2@delete_mobile_no');

	Route::post('update-mobile-number','MobileMoney2@update_mobile_number');

});
