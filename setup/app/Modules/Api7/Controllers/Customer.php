<?php namespace App\Modules\Api7\Controllers;

use App;
use App\Http\Controllers\Controller;
use App\Http\Models\Deliveryrequest;
use App\Http\Models\RequestCard;

use App\Http\Models\Transaction;
use App\Http\Models\User;
use App\Library\Reqhelper;
use App\Library\Utility;
use App\Modules\User\Controllers\Payment;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Mail,Request;
use MongoDate;
use MongoId;
use App\Http\Models\Setting;

class Customer extends Controller
{

    function testFunction(Request $request) {
        //echo 'pp'; die;
        $response['success'] = 0;
        $supportemail = Setting::find('563b0e31e4b03271a097e1ca');

        $insert['AqAddress'] = $supportemail->AqAddress;
        $insert['AqCity'] = $supportemail->AqCity;
        $insert['AqState'] = $supportemail->AqState;
        $insert['AqZipcode'] = $supportemail->AqZipcode;
        $insert['AqCountry'] = $supportemail->AqCountry;
        $insert['AqLatLong'] = $supportemail->AqLatlong;
        $insert['Aqphone'] = $supportemail->Aqphone;
        $insert['Aqcc'] = $supportemail->Aqcc;

        $users = User::get();
        if(count($users) > 0){
            foreach ($users as $key) {
                User::where(['_id'=>$key->_id])->update($insert);
            }
        }
                
        return response()->json($response);
    }

    public function create_send_package()
    {
        $response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
        $data['cards'] = Payment::card_list('cus_CKhCqcqz0hK19B');
        return response()->json(['result' => $data['cards']]);
    }

    public function item_payment()
    {
        $response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
        $validate = Validator::make(Input::get(), [
            'requestid' => 'required',
            //'itemid' => 'required',
            'card_id' => 'required',
            'type' => 'required',
            'user_id' => 'required',
        ]);
        if ($validate->fails()) {
            $validate->errors()->add('field', trans('Oops! Something went wrong'));
            return $validate->errors();
        } else {
            $userinfo = User::find(Input::get('user_id'));
            $where = [
                '_id' => trim(Input::get('requestid')),
                'RequesterId' => new MongoId(Input::get('user_id')),
                'RequestType' => trim(Input::get('type')),
            ];
            $req_data = Deliveryrequest::where($where)->first();
            if (count($req_data) > 0 && count($userinfo) > 0) {
                $productName = $req_data->ProductTitle;
                $package_id = $req_data->PackageNumber; 
                $cost = $req_data->need_to_pay;
                $res = Payment::capture($userinfo, (($cost)), trim(Input::get('card_id')), true);

                if (isset($res['id'])) {
                    $up = Deliveryrequest::where($where)->update(['need_to_pay' => 0]);
                    Transaction::insert(array(
                        "SendById" => (String) @$userinfo->_id,
                        "SendByName" => @$userinfo->Name,
                        "SendToId" => "aquantuo",
                        "RecieveByName" => "Aquantuo",
                        "Description" => "Amount deposited against product '{$productName}', PackageId: {$package_id}",
                        "Credit" => floatval($cost),
                        "Debit" => "",
                        "Status" => "",
                        "TransactionType" => $req_data->RequestType,
                        'request_id'=> trim(Input::get('requestid')),
                        'item_id'=> trim(Input::get('itemid')),
                        "EnterOn" => new MongoDate(),
                    ));

                    RequestCard::Insert([
                        'request_id' => trim(Input::get('requestid')),
                        'item_id' => trim(Input::get('itemid')),
                        'card_id' => trim(Input::get('card_id')),
                        'type' => 'request',
                        'payment' => $cost,
                        'brand' => @$res['source']['brand'],
                        'last4' => @$res['source']['last4'],
                        'description' => "Payment done for item Title: '" . $productName . "' Item Id:" . $package_id . ".",
                        "EnterOn" => new MongoDate(),
                    ]);

                    //$req_data->RequestType
                    if($req_data->RequestType == 'local_delivery'){
                        Reqhelper::update_status2(trim(Input::get('requestid')));
                    }else{
                        Reqhelper::update_status(trim(Input::get('requestid')));
                    }

                    
                    //$status_update = Deliveryrequest::where($where)->update(['Status' => $res]);
                    $response = ['success' => 1, 'msg' => 'Payment has been successful.'];
                    $response['msg'] = "Success! Payment for package '" .$productName."' has been successfully completed.Aquantuo will proceed with next steps immediately. ";
                } else {
                    $response['msg'] = $res;
                }
            }
        }
        return response()->json($response);
    }


    /*public function online_payment()
    {
        $response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
        $validate = Validator::make(Input::get(), [
            'packageId' => 'required',
            'creditCardId' => 'required',
            'user_id' => 'required',
        ]);

        if ($validate->fails()) {
            $validate->errors()->add('field', trans('Oops! Something went wrong'));
            return $validate->errors();
        }else{
            $userinfo = User::find(Input::get('user_id'));
            $where = [
                '_id' => trim(Input::get('packageId')),
                'RequesterId' => new MongoId(Input::get('user_id')),
                'RequestType' => 'online',
                'Status'=>'pending'
                
            ];
            $req_data = Deliveryrequest::where($where)->first(); 
            if (count($req_data) > 0 && count($userinfo) > 0) {

            }
        } 
    }*/

    public function test_mail()
    {
        $time_start = microtime(true);
        $data['name'] = "<b>Hello</b>Ajay,<br/>Test this queue-19 " . date('Y-m-d h:i A');
        $message = "hello";
        Mail::send('email.welcome', $data, function ($message) {
            $message->subject("hello");
            $message->to('ajay@idealittechno.com');
        });
        $time_end = microtime(true);
        $time = $time_end - $time_start;
        echo "Process Time: {$time}";
    }

    /*
     * function for: Create local delivery
     * function created by: Deepak Pateriya
     * Created At: 28 feb 2018
     * Updated By:
     * Updated At:
     */
    public function create_local_delivery_request()
    {
        $response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
        $validate = Validator::make(Input::get(), [
            'user_id' => 'required',
            'product_list' => 'required',
        ]);

        if ($validate->fails()) {
            $validate->errors()->add('field', trans('Oops! Something went wrong'));
            return $validate->errors();
        } else {

            $userInfo = User::where(['_id' => Input::get('user_id')])->first();
            if (count($userInfo) == 0) {
                return response()->json($response);die;
            }

            //$PackageId = $this->get_packageno();
            $city = trim(Input::get('city'));
            $state = trim(Input::get('state'));
            $country = trim(Input::get('country'));

            $drop_off_city = trim(Input::get('drop_off_city'));
            $drop_off_state = trim(Input::get('drop_off_state'));
            $drop_off_country = trim(Input::get('drop_off_country'));

            $return_city = trim(Input::get('return_city'));
            $return_state = trim(Input::get('return_state'));
            $return_country = trim(Input::get('return_country'));

            $nd_return_city = trim(Input::get('nd_return_city'));
            $nd_return_state = trim(Input::get('nd_return_state'));
            $nd_return_country = trim(Input::get('nd_return_country'));

            $calculated_distance = floatval(Input::get('calculated_distance'));
            if ($calculated_distance > 0) {
                $calculated_distance = $calculated_distance * 0.000621371;
            } else {
                $calculated_distance = Utility::getDistanceBetweenPointsNew(
                    floatval(Input::get('PickupLat')),
                    floatval(Input::get('PickupLong')),
                    floatval(Input::get('DeliveryLat')),
                    floatval(Input::get('DeliveryLong'))
                );
            }

            $PackageId = Utility::sequence('Request') . time();

            $insertArray = [
                'RequesterId' => new MongoId(Input::get('user_id')),
                'RequesterName' => ucfirst($userInfo->Name),
                'ProductTitle' => '',
                'RequestType' => "local_delivery",
                'Status' => 'pending',
                'PackageId' => $PackageId,
                'device_version' => Input::get('device_version'),
                'app_version' => Input::get('app_version'),
                'device_type' => Input::get('device_type'),
                "PackageNumber" => $PackageId . time(),
                "PickupFullAddress" => $this->formated_address([
                    (trim(Input::get('address_line_1')) != '') ? trim(Input::get('address_line_1')) : '',
                    (trim(Input::get('address_line_2') != '')) ? trim(Input::get('address_line_2')) : '',
                    @$city,
                    @$state,
                    @$country,
                ], Input::get('zipcode')),
                'PickupAddress' => trim(Input::get('address_line_1')),
                'PickupAddress2' => trim(Input::get('address_line_2')),
                'PickupCity' => @$city,
                'PickupState' => @$state,
                'PickupCountry' => @$country,
                'PickupPinCode' => trim(Input::get('zipcode')),
                'PickupLatLong' => [floatval(Input::get('PickupLong')), floatval(Input::get('PickupLat'))],
                'PickupDate' => new MongoDate(strtotime(Input::get('pickup_date'))),
                'DeliveryFullAddress' => $this->formated_address([
                    (trim(Input::get('drop_off_address_line_1')) != '') ? trim(Input::get('drop_off_address_line_1')) : '',
                    (trim(Input::get('drop_off_address_line_2')) != '') ? trim(Input::get('drop_off_address_line_2')) : '',
                    @$drop_off_city,
                    @$drop_off_state,
                    @$drop_off_country,
                ], Input::get('drop_off_zipcode')),
                'DeliveryAddress' => trim(Input::get('drop_off_address_line_1')),
                'DeliveryAddress2' => trim(Input::get('drop_off_address_line_2')),
                'DeliveryCity' => @$drop_off_city,
                'DeliveryState' => @$drop_off_state,
                'DeliveryCountry' => @$drop_off_country,
                'DeliveryPincode' => trim(Input::get('drop_off_zipcode')),
                "DeliveryLatLong" => [floatval(Input::get('DeliveryLong')), floatval(Input::get('DeliveryLat'))],
                'DeliveryDate' => new MongoDate(strtotime(Input::get('drop_off_date'))),

                "ReturnFullAddress" => $this->formated_address([
                    Input::get('return_address_line_1'),
                    Input::get('return_address_line_2'),
                    @$return_city,
                    @$return_state,
                    @$return_country,
                ], Input::get('return_zipcode')),
                "ReturnAddress" => trim(Input::get('return_address_line_1')),
                "ReturnAddress2" => trim(Input::get('return_address_line_2')),
                "ReturnCityTitle" => @$return_city,
                "ReturnStateTitle" => @$return_state,
                'ReturnCountry' => @$return_country,
                'ReturnPincode' => trim(Input::get('return_zipcode')),
                "FlexibleDeliveryDate" => (Input::get('is_delivery_date_flexible') == 'yes') ? 'yes' : 'no',
                "JournyType" => (Input::get('journey_type') == 'return') ? 'return' : 'one_way',
                'NotDelReturnFullAddress' => $this->formated_address([
                    trim(Input::get('nd_return_address_line_1')),
                    trim(Input::get('nd_return_address_line_2')),
                    @$nd_return_city,
                    @$nd_return_state,
                    @$nd_return_country,
                ], Input::get('nd_return_zipcode')),
                "InCaseNotDelReturnAddress" => trim(Input::get('nd_return_address_line_1')),
                "InCaseNotDelReturnAddress2" => trim(Input::get('nd_return_address_line_2')),
                "InCaseNotDelReturnCity" => @$nd_return_city,
                "InCaseNotDelReturnState" => @$nd_return_state,
                "InCaseNotDelReturnCountry" => @$nd_return_country,
                "InCaseNotDelReturnPincode" => trim(Input::get('nd_return_zipcode')),
                "PackageMaterial" => (Input::get('need_package_material') == 'yes') ? 'yes' : 'no',
                "PackageMaterialShipped" => '',
                "PublicPlace" => (Input::get('public_palce') == 'yes') ? 'yes' : 'no',
                'ReceiverCountrycode' => trim(Input::get('country_code')),
                'ReceiverMobileNo' => trim(Input::get('phone_number')),
                'FlexibleDeliveryDate' => Input::get('is_delivery_date_flexible'),
                "Distance" => $calculated_distance,
                'ShippingCost' => trim(Input::get('ShippingCost')),
                'InsuranceCost' => trim(Input::get('InsuranceCost')),
                'TotalCost' => trim(Input::get('TotalCost')),
                'needToPay' => trim(Input::get('needToPay')),
                'promocode' => trim(Input::get('promocode')),
                'Discount' => trim(Input::get('Discount')),
                'region_charge' => trim(Input::get('RegionCost')),
                'ProductList' => [],
            ];

            // if(input::get('product_list')!='')
            //  if (count($res->product) > 0) {

            if (Input::get('product_list') != '') {

                $product = json_decode(Input::get('product_list'));

                $k = 0;
                foreach ($product as $key) {

                    $k++;
                    $insertArray["ProductTitle"] .= @$key->productName;
                    $insertArray['ShippingCost'] += @$key->shippingCost;
                    $insertArray['InsuranceCost'] += @$key->InsuranceCost;
                    $insertArray['TotalCost'] += @$key->shippingCost+@$key->InsuranceCost;

                    $insertArray['ProductList'][] = [
                        '_id' => (Input::get('request_id') == '') ? (string) new MongoId() : $key->_id,
                        "productName" => @$key->productName,
                        'package_id' => (Input::get('request_id') == '') ? $insertArray['PackageNumber'] . $k : $key->PackageNumber,
                        'status' => (Input::get('request_id') == '') ? 'pending' : $key->status,
                        "productWidth" => $key->productWidth,
                        "productHeight" => $key->productHeight,
                        "productLength" => $key->productLength,
                        "productCost" => $key->productCost,
                        "productWeight" => $key->productWeight,
                        "productHeightUnit" => $key->productHeightUnit,
                        "ProductWeightUnit" => $key->productWeightUnit,
                        "ProductLengthUnit" => $key->productLengthUnit,
                        "productCategory" => $key->category,
                        "productCategoryId" => $key->productCategoryId,
                        "travelMode" => $key->travelMode,
                        "InsuranceStatus" => $key->InsuranceStatus,
                        "productQty" => $key->productQty,
                        "ProductImage" => $key->ProductImage,
                        //"OtherImage" => $key->OtherImage,
                        "QuantityStatus" => $key->quantity,
                        "BoxQuantity" => $key->productQty,
                        "Description" => $key->description,
                        "PackageMaterial" => $key->package_material_needed,
                        //"PackageMaterialShipped" => $key->PackageMaterialShipped,
                        "shippingCost" => @$key->shippingCost,
                        'InsuranceCost' => @$key->InsuranceCost,
                        "DeliveryVerifyCode" => (Input::get('request_id') == '') ? rand(1000, 9999) : $key->DeliveryVerifyCode,
                        'tpid' => '',
                        'tpName' => '',
                        'inform_mail_sent' => 'no',
                        'PaymentStatus' => 'no',
                        'total_cost' => @$key->shippingCost+@$key->InsuranceCost,
                    ];

                }

            }

            //print_r($insertArray);die;

            $record = (String) Deliveryrequest::insertGetId($insertArray);
            if (!$record) {
                $response = ['success' => 0, 'msg' => 'Oops! Something went wrong. during insertion'];
                return response()->json($response);die;
            }

            $response = ['success' => 1, 'result' => ['local_delivery_request_id' => $record], 'msg' => 'Local delivery request created successfully.'];

            return response()->json($response);die;

        }
    }

    /*
     * function for: Edit local delivery
     * function created by: Deepak Pateriya
     * Created At: 01 march 2018
     * Updated By:
     * Updated At:
     */
    public function edit_local_delivery_request()
    {
        $response = ['success' => 0, 'msg' => 'Record not found.'];

        $result = Deliveryrequest::where(['_id' => Input::get('local_delivery_request_id'), 'RequestType' => 'local_delivery'])->first();
        if (!$result) {
            return response()->json($response);die;
        }
        $response = ['success' => 1, 'result' => $result, 'msg' => 'Local delivery data'];

        return response()->json($response);die;
    }

    /*
     * function for: Update local delivery
     * function created by: Deepak Pateriya
     * Created At: 01 march 2018
     * Updated By:
     * Updated At:
     */
    public function update_local_delivery_request()
    {
        $response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
        $validate = Validator::make(Input::get(), [
            'user_id' => 'required',
            //'product_list' => 'required',
        ]);

        if ($validate->fails()) {
            $validate->errors()->add('field', trans('Oops! Something went wrong'));
            return $validate->errors();
        } else {

            $userInfo = User::where(['_id' => Input::get('user_id')])->first();
            if (count($userInfo) == 0) {
                return response()->json($response);die;
            }

            //$PackageId = $this->get_packageno();
            $city = Input::get('city');
            $state = Input::get('state');
            $country = Input::get('country');

            $drop_off_city = Input::get('drop_off_city');
            $drop_off_state = Input::get('drop_off_state');
            $drop_off_country = Input::get('drop_off_country');

            $return_city = Input::get('return_city');
            $return_state = Input::get('return_state');
            $return_country = Input::get('return_country');

            $nd_return_city = Input::get('nd_return_city');
            $nd_return_state = Input::get('nd_return_state');
            $nd_return_country = Input::get('nd_return_country');

            $calculated_distance = floatval(Input::get('calculated_distance'));
            if ($calculated_distance > 0) {
                $calculated_distance = $calculated_distance * 0.000621371;
            } else {
                $calculated_distance = Utility::getDistanceBetweenPointsNew(
                    floatval(Input::get('PickupLat')),
                    floatval(Input::get('PickupLong')),
                    floatval(Input::get('DeliveryLat')),
                    floatval(Input::get('DeliveryLong'))
                );
            }

            $PackageId = Utility::sequence('Request') . time();

            $updateArray = [
                'RequesterId' => new MongoId(Input::get('user_id')),
                'RequesterName' => ucfirst($userInfo->Name),
                'ProductTitle' => '',
                'RequestType' => "local_delivery",
                'Status' => 'pending',
                'PackageId' => $PackageId,
                'device_version' => Input::get('device_version'),
                'app_version' => Input::get('app_version'),
                'device_type' => Input::get('device_type'),
                "PackageNumber" => $PackageId . time(),
                "PickupFullAddress" => $this->formated_address([
                    (Input::get('address_line_1') != '') ? Input::get('address_line_1') : '',
                    (Input::get('address_line_2') != '') ? Input::get('address_line_2') : '',
                    @$city,
                    @$state,
                    @$country,
                ], Input::get('zipcode')),
                'PickupAddress' => Input::get('address_line_1'),
                'PickupAddress2' => Input::get('address_line_2'),
                'PickupCity' => @$city,
                'PickupState' => @$state,
                'PickupCountry' => @$country,
                'PickupPinCode' => Input::get('zipcode'),
                'PickupLatLong' => [floatval(Input::get('PickupLong')), floatval(Input::get('PickupLat'))],
                'PickupDate' => new MongoDate(strtotime(Input::get('pickup_date'))),
                'DeliveryFullAddress' => $this->formated_address([
                    (Input::get('drop_off_address_line_1') != '') ? Input::get('drop_off_address_line_1') : '',
                    (Input::get('drop_off_address_line_2') != '') ? Input::get('drop_off_address_line_2') : '',
                    @$drop_off_city,
                    @$drop_off_state,
                    @$drop_off_country,
                ], Input::get('drop_off_zipcode')),
                'DeliveryAddress' => Input::get('drop_off_address_line_1'),
                'DeliveryAddress2' => Input::get('drop_off_address_line_2'),
                'DeliveryCity' => @$drop_off_city,
                'DeliveryState' => @$drop_off_state,
                'DeliveryCountry' => @$drop_off_country,
                'DeliveryPincode' => Input::get('drop_off_zipcode'),
                "DeliveryLatLong" => [floatval(Input::get('DeliveryLong')), floatval(Input::get('DeliveryLat'))],
                'DeliveryDate' => new MongoDate(strtotime(Input::get('drop_off_date'))),

                "ReturnFullAddress" => $this->formated_address([
                    Input::get('return_address_line_1'),
                    Input::get('return_address_line_2'),
                    @$return_city,
                    @$return_state,
                    @$return_country,
                ], Input::get('return_zipcode')),
                "ReturnAddress" => Input::get('return_address_line_1'),
                "ReturnAddress2" => Input::get('return_address_line_2'),
                "ReturnCityTitle" => @$return_city,
                "ReturnStateTitle" => @$return_state,
                'ReturnCountry' => @$return_country,
                'ReturnPincode' => Input::get('return_zipcode'),
                "FlexibleDeliveryDate" => (Input::get('is_delivery_date_flexible') == 'yes') ? 'yes' : 'no',
                "JournyType" => (Input::get('journey_type') == 'return') ? 'return' : 'one_way',
                'NotDelReturnFullAddress' => $this->formated_address([
                    Input::get('nd_return_address_line_1'),
                    Input::get('nd_return_address_line_2'),
                    @$nd_return_city,
                    @$nd_return_state,
                    @$nd_return_country,
                ], Input::get('nd_return_zipcode')),
                "InCaseNotDelReturnAddress" => Input::get('nd_return_address_line_1'),
                "InCaseNotDelReturnAddress2" => Input::get('nd_return_address_line_2'),
                "InCaseNotDelReturnCity" => @$nd_return_city,
                "InCaseNotDelReturnState" => @$nd_return_state,
                "InCaseNotDelReturnCountry" => @$nd_return_country,
                "InCaseNotDelReturnPincode" => Input::get('nd_return_zipcode'),
                "PackageMaterial" => (Input::get('need_package_material') == 'yes') ? 'yes' : 'no',
                "PackageMaterialShipped" => '',
                "PublicPlace" => (Input::get('public_palce') == 'yes') ? 'yes' : 'no',
                'ReceiverCountrycode' => Input::get('country_code'),
                'ReceiverMobileNo' => Input::get('phone_number'),
                'FlexibleDeliveryDate' => Input::get('is_delivery_date_flexible'),
                "Distance" => $calculated_distance,
                'ShippingCost' => 0,
                'InsuranceCost' => 0,
                'Discount' => 0,
                'TotalCost' => 0,
                'needToPay' => 0,
                'promocode' => Input::get('promocode'),
                'ProductList' => [],
            ];

            // if(input::get('product_list')!='')
            //  if (count($res->product) > 0) {

            if (Input::get('product_list') != '') {

                $product = json_decode(Input::get('product_list'));

                $k = 0;
                foreach ($product as $key) {

                    $k++;
                    $insertArray["ProductTitle"] .= @$key->productName;
                    $insertArray['ShippingCost'] += @$key->shippingCost;
                    $insertArray['InsuranceCost'] += @$key->InsuranceCost;
                    $insertArray['TotalCost'] += @$key->shippingCost+@$key->InsuranceCost;

                    $insertArray['ProductList'][] = [
                        '_id' => (Input::get('request_id') == '') ? (string) new MongoId() : $key->_id,
                        "productName" => @$key->productName,
                        'package_id' => (Input::get('request_id') == '') ? $insertArray['PackageNumber'] . $k : $key->PackageNumber,
                        'status' => (Input::get('request_id') == '') ? 'pending' : $key->status,
                        "productWidth" => $key->productWidth,
                        "productHeight" => $key->productHeight,
                        "productLength" => $key->productLength,
                        "productCost" => $key->productCost,
                        "productWeight" => $key->productWeight,
                        "productHeightUnit" => $key->productHeightUnit,
                        "ProductWeightUnit" => $key->productWeightUnit,
                        "ProductLengthUnit" => $key->productLengthUnit,
                        "productCategory" => $key->category,
                        "productCategoryId" => $key->productCategoryId,
                        "travelMode" => $key->travelMode,
                        "InsuranceStatus" => $key->InsuranceStatus,
                        "productQty" => $key->productQty,
                        "ProductImage" => $key->ProductImage,
                        //"OtherImage" => $key->OtherImage,
                        "QuantityStatus" => $key->quantity,
                        "BoxQuantity" => $key->productQty,
                        "Description" => $key->description,
                        "PackageMaterial" => $key->package_material_needed,
                        //"PackageMaterialShipped" => $key->PackageMaterialShipped,
                        "shippingCost" => @$key->shippingCost,
                        'InsuranceCost' => @$key->InsuranceCost,
                        "DeliveryVerifyCode" => (Input::get('request_id') == '') ? rand(1000, 9999) : $key->DeliveryVerifyCode,
                        'tpid' => '',
                        'tpName' => '',
                        'inform_mail_sent' => 'no',
                        'PaymentStatus' => 'no',
                        'total_cost' => @$key->shippingCost+@$key->InsuranceCost,
                    ];

                }

            }

            $result = (String) Deliveryrequest::where(['_id' => Input::get('local_delivery_request_id')])->update($updateArray);
            if (!$result) {
                $response = ['success' => 0, 'msg' => 'Oops! Something went wrong. during updation'];
                return response()->json($response);die;
            }

            $response = ['success' => 1, 'msg' => 'Local delivery request updated successfully.'];

            return response()->json($response);die;

        }
    }

    /*
     * function for: Common function for get formated address between two address.
     * function created by: Deepak Pateriya
     * Created At: 28 feb 2018
     * Updated By:
     * Updated At:
     */
    public function formated_address($array, $zip)
    {
        $address = '';
        foreach ($array as $ky => $val) {
            if (trim($val) != '') {
                if (trim($address) != '') {$address = "$address, ";}
                $address .= $val;
            }
        }
        if (trim($zip) != '') {$address .= "- $zip";}
        return $address;
    }

    public function retrievePayment()
    {   
        $merchant_key = '3f3f665c-f170-11e7-9871-f23c9170642f';
        if(Input::get('invoice_id') != ''){
            $requests = Deliveryrequest::where(array("mobilemoney" => array('$elemMatch' => array('mobile_moneyId' =>(int) trim(Input::get('invoice_id'))))))->first();
            
            if(count($requests) > 0){
                foreach ($requests->mobilemoney as $key2) {
                    if($key2['mobile_moneyId'] == trim(Input::get('invoice_id'))){
                        $url = "https://community.ipaygh.com/v1/gateway/status_chk?invoice_id={$key2['mobile_moneyId']}&merchant_key={$merchant_key}";
                            
                        $ch = curl_init($url);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        $output = curl_exec($ch);
                        if (strpos($output, 'paid') !== false) {
                            $this->updateRequest($requests->_id,trim(Input::get('invoice_id')));
                            
                            Transaction::where(['momo_id'=> (int) trim(Input::get('invoice_id'))])->update(['Status'=>'completed']);
                            send_mail('5b167b41360d5c10b3a24048', [
                            "to" => "support@aquantuo.com",
                                "replace" => [
                                    "[PACKAGETITLE]" => ucfirst($requests->ProductTitle),
                                    "[PACKAGEID]" => $requests->PackageNumber,
                                    "[DESTINATION]" => $requests->DeliveryFullAddress,
                                    "[PACKAGENUMBER]" => $requests->PackageNumber,
                                    "[USER]" => $requests->RequesterName,
                                    "[AMOUNT]" => number_format($requests->TotalCost,2)
                                ],
                            ]);


                        }

                        return $output;

                    }
                    
                }
            }
        }

        return 'Done';
    }

    public function retrievePayment2()
    {   
        $merchant_key = '3f3f665c-f170-11e7-9871-f23c9170642f';
        if(Input::get('invoice_id') != ''){
            $requests = Deliveryrequest::where(array("mobilemoney" => array('$elemMatch' => array('mobile_moneyId' =>(int) trim(Input::get('invoice_id'))))))->first();
            
            if(count($requests) > 0){
                foreach ($requests->mobilemoney as $key2) {
                    if($key2['mobile_moneyId'] == trim(Input::get('invoice_id'))){
                        $url = "https://community.ipaygh.com/v1/gateway/status_chk?invoice_id={$key2['mobile_moneyId']}&merchant_key={$merchant_key}";
                            
                        $ch = curl_init($url);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        $output = curl_exec($ch);
                        if (strpos($output, 'paid') !== false) {
                        $this->updateRequest($requests->_id,trim(Input::get('invoice_id')));
                        Transaction::where(['momo_id'=> (int) trim(Input::get('invoice_id'))])->update(['Status'=>'completed']);
                        //$ch = Transaction::where(['momo_id'=> (int) trim(Input::get('invoice_id'))])->first();
                        //print_r($ch); die;
                            /*send_mail('5b167b41360d5c10b3a24048', [
                            "to" => "support@aquantuo.com",
                                "replace" => [
                                    "[PACKAGETITLE]" => ucfirst($requests->ProductTitle),
                                    "[PACKAGEID]" => $requests->PackageNumber,
                                    "[DESTINATION]" => $requests->DeliveryFullAddress,
                                    "[PACKAGENUMBER]" => $requests->PackageNumber,
                                    "[USER]" => $requests->RequesterName,
                                    "[AMOUNT]" => number_format($requests->TotalCost,2)
                                ],
                            ]);*/


                        }

                        return $output;

                    }
                    
                }
            }
        }

        return 'Done';
    }

    public function updateRequest($id,$invoice){
        $data =  Deliveryrequest::where(['_id'=> $id])->first();
        $ProductList = $data->ProductList;
        $mobilemoney = $data->mobilemoney;
        if($data->Status == 'momo_initiated'){
            $data->Status = 'ready';
            foreach ($data->ProductList as $key => $value ) {
               $ProductList[$key]['status'] = 'ready';
            }
        }
        foreach ($data->mobilemoney as $key => $value) {
            if(!in_array($value['mobile_money_status'],['cancelled','expired']) ){
                if($invoice == $value['mobile_moneyId']){
                   // $key['mobile_money_status'] = 'paid';
                    $mobilemoney[$key]['mobile_money_status'] = 'paid';
                }
                
            }
        }
        $data->ProductList = $ProductList;
        $data->mobilemoney = $mobilemoney;
        $data->save();

    }


}
