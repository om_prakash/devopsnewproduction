<?php

Route::group(array('module' => 'Admin', 'namespace' => 'App\Modules\Admin\Controllers'), function () {
	//New work 2019

	Route::any('admin/received-slip-list', 'SlipController@receivedList');
	Route::any('admin/create-received-slip', 'SlipController@CreatereceivedSlip');
	Route::any('admin/test', 'SlipController@test');
	

	//end




	//Login
	Route::get('admin', 'AuthController@getLogin');
	Route::get('admin/login', 'AuthController@getLogin');
	Route::post('admin/login', 'AuthController@authenticate');
	Route::get('admin/logout', 'PageController@logout');

	Route::get('admin/near_trip', 'PageController@near_trip');

	//Dashboard
	Route::any('admin/dashboard', 'PageController@dashboard');
	Route::any('admin/update-address', 'PageController@update_address');

	Route::get('admin/edit-bank/{id}', 'PageController@edit_bank');
	Route::post('admin/post-edit-bank/{id}', 'PageController@post_edit_bank');

	Route::get('send-noti', 'NotificationController@send_noti');

	//Route::get('admin/complete-request/{id}', 'PageController@admin_complete_request');

	Route::get('admin/complete-request/{id}', 'PageController@admin_complete_request2');

	Route::get('admin/cancel-delivery-request', 'PageController@cancel_delivery_request');

	//package section
	Route::get('admin/package', 'PageController@package');
	Route::post('pagination/package', 'PaginationController@package');
	Route::get('admin/package/detail/{id}', 'PageController@package_detail');
	Route::get('admin/add-package', 'Rsrequest@add_package');
	Route::get('admin/edit-package/{id}', 'Rsrequest@edit_package');
	Route::post('admin/prepare-request', 'Rsrequest@prepare_request');
	Route::get('admin/process-card-list/{id}', 'PageController@prepare_request_make_tnx');
	Route::post('admin/process-card-list/{id}', 'PageController@post_prepare_request_make_tnx');

	// Expected date chaneg

	Route::post('admin/expected-date', 'SendpackageCtrl@ExpectedDate');
	Route::get('admin/online/expected-date/{id}', 'SendpackageCtrl@changeExpecteddate');

	// new send a package
	Route::any('admin/add-send-a-package', 'SendpackageCtrl@package');
	Route::any('admin/send-a-package-add-item', 'SendpackageCtrl@post_package');
	Route::any('admin/prepare-add-item/{id?}', 'SendpackageCtrl@prepareAddItem');
	/*Route::any('admin/prepare_request_calculation', 'SendpackageCtrl@preparecalculation');*/
	Route::any('admin/prepare_request_calculation', 'SendpackageCtrl@prepare_request_calculation');
	
	Route::any('admin/prepare_request_create', 'SendpackageCtrl@create_prepare_request');
	Route::get('admin/sendapackage/delete_item/{id}/{function}', 'SendpackageCtrl@delete_item');

	//edit send a package
	Route::post('admin/add-item-exist-request', 'SendpackageCtrl@addItemInExistRequest');
	Route::post('admin/edit-item-exist-request', 'SendpackageCtrl@editItemInExistRequest');
	Route::post('admin/post-edit-item-exist-request', 'SendpackageCtrl@posteditItemInExistRequest');
	Route::post('admin/edit_prepare_request_calculation', 'SendpackageCtrl@edit_calculation');
	Route::get('admin/remove-item/{itemid}/{requestid}', 'SendpackageCtrl@remove_item');
	Route::get('admin/package-delete-item/{id}', 'SendpackageCtrl@PackagedeleteItem');
	Route::post('admin/accept-package', 'PageController@accept_product');
	Route::post('admin/online-package-review', 'PageController@online_package_review');
	Route::get('admin/prepare-request-edit-item/{id}/{requestid?}', 'SendpackageCtrl@editSendPackage');

	Route::get('admin/delete_item2/{itemid}/{function}/{rid}', 'BuyformeController@delete_item2');

	// add new local delivery
	Route::get('admin/add-local-delivery', 'LocalDeliveryController@add_local_delivery');
	Route::any('admin/local-delivery-add-item', 'LocalDeliveryController@post_local_delivery');
	Route::any('admin/local-prepare-add-item/{id?}', 'LocalDeliveryController@localprepareAddItem');
	Route::any('admin/local_prepare_request_calculation', 'LocalDeliveryController@local_prepare_request_calculation');
	Route::any('admin/local_prepare_request_create', 'LocalDeliveryController@local_create_prepare_request');
	Route::get('admin/localdelivery/delete_item/{id}/{function}', 'LocalDeliveryController@delete_item');
	Route::get('admin/edit-local-delivery-request/{id}', 'LocalDeliveryController@edit_local_delivery_request');
	Route::post('admin/local-delivery-edit-item-exist-request', 'LocalDeliveryController@localDeliveryeditItem');

	// //edit local delivery
	Route::post('admin/local-add-item-exist-request', 'LocalDeliveryController@localaddItemInExistRequest');
	Route::post('admin/local-edit-item-exist-request', 'LocalDeliveryController@localposteditItemInExistRequest');
	Route::post('admin/local-post-edit-item-exist-request', 'LocalDeliveryController@localposteditItemInExistRequest');
	Route::post('admin/local_edit_prepare_request_calculation', 'LocalDeliveryController@local_prepare_request_calculation');
	Route::get('admin/local-remove-item/{itemid}/{requestid}', 'LocalDeliveryController@local_remove_item');
	Route::get('admin/local-delivery-delete-item/{id}', 'LocalDeliveryController@localDeliveryDeleteItem');
	Route::get('admin/local-delte-item/{itemid}/{requestid}', 'LocalDeliveryController@LocaldeleteItem');
	//Route::post('admin/accept-local-delivery', 'PageController@accept_product');
	//Route::post('admin/online-package-review', 'PageController@online_package_review');
	Route::get('admin/local-prepare-request-edit-item/{id}/{requestid?}', 'LocalDeliveryController@editLocalDelivery');
	Route::get('admin/local-edit-request/{id}', 'LocalDeliveryController@localEditRequest');

	//////////////////////////
	Route::any('package-received-all/{id}', 'SendpackageCtrl@packagereceivedall');
	Route::any('accept-all/{id}', 'PageController@accept_all_product');
	Route::post('cancel-all/{id}', 'PageController@cancelAll');
	Route::any('purchased-all/{id}', 'PageController@purchased_all');
	Route::any('received-all/{id}', 'PageController@receivedAll');
	Route::any('assign-all-tp/{id}', 'PageController@assignTp');

	Route::any('local-delivery-accept-all/{id}', 'LocalDeliveryController@acceptAllProduct');
	Route::any('local-delivery-received-all/{id}', 'LocalDeliveryController@receivedAllProduct');
	// activity log

	Route::any('admin/activity-log/{id?}', 'AjaxController@activity');
	Route::post('pagination/activity', 'PaginationController@activity');

	Route::any('cancel-request', 'PageController@cancel_request');
	Route::any('pickup', 'PageController@pickup_request');
	Route::any('request-delivery', 'PageController@delivery_request');
	Route::any('request-complete', 'PageController@complete_request');

	Route::any('assign-transporter', 'PageController@assign_transporter');
	Route::any('update-promocode/{id}', 'PageController@update_promocode');

	//online-package
	Route::any('admin/online-package', 'PageController@online_package');
	Route::post('pagination/online-package', 'PaginationController@online_package');
	Route::get('admin/online_package/detail/{id}', 'PageController@online_package_detail');

	Route::post('admin/mark_paid', 'BuyformeController@mark_as_paid');
	Route::post('admin/mark_paid_after_item_update', 'BuyformeController@mark_as_paid_after_item_update');

	// Buy for me
	Route::get('admin/buy-for-me/edit/{id}', 'PageController@edit_buy_for_me');

	Route::get('admin/buy-for-me', 'PageController@buy_for_me');
	Route::post('pagination/buy-for-me', 'PaginationController@buy_for_me');
	Route::get('admin/buy-for-me/detail/{id}', 'PageController@buy_for_me_detail');
	Route::post('admin/buy-for-me-process', 'PageController@buy_for_me_process');
	Route::post('admin/get_item_info', 'PageController@get_item_info');
	Route::post('admin/get_online_item_info', 'PageController@get_online_item_info');
	Route::post('admin/online-payment-reminder', 'PageController@online_payment_reminder');

	Route::post('admin/online-package-purchase', 'PageController@online_package_purchase');
	Route::post('admin/update_item_info_old', 'PageController@update_item_info');

	/*new bfm update*/
	Route::post('admin/update_item_info', 'RequestProcessController@update_item_info');
	/*end*/
	Route::post('admin/update_online_item_info_old', 'PageController@update_online_item_info');
	Route::post('admin/update_online_item_info','RequestProcessController@update_item_info');
	Route::post('admin/buy_for_me_assign_transporter', 'PageController@buy_for_me_assign_transporter');
	Route::post('admin/online_assign_transporter', 'PageController@onlineAssigntransporter');

	Route::post('admin/get_old_item', 'PageController@getItemInfo');

	//User list
	Route::any('admin/requester', 'PageController@requester');
	Route::post('pagination/requester', 'PaginationController@requester');
	//Route::get('admin/edit-requester/{id}','PageController@edit_requester');
	//~ Route::post('admin/edit-requester/{id}','PageController@post_edit_requester');
	Route::get('admin/user/detail/{id}', 'PageController@users_detail');
	Route::get('admin/requester/edit/{id}', 'PageController@edit_requester');
	Route::post('admin/requester/edit/{id}', 'PageController@post_edit_requester');
	Route::post('ajax/verify_user', 'AjaxController@post_verified_user');

	Route::post('admin/user-registration', 'AjaxController@user_registration');
	Route::post('admin/find-email', 'AjaxController@find_user_from_email');

	Route::get('admin/cron-mail', 'AjaxController@cronMail');
	Route::post('pagination/cron-mail', 'PaginationController@cronMail');

	//silder-image
	Route::get('admin/silder-image', 'AjaxController@sliderImages');
	Route::post('pagination/silder-image', 'PaginationController@sliderImages');
	Route::post('admin/add-images', 'AjaxController@addImages');
	Route::post('admin/update-images/{id}', 'AjaxController@updateImages');
	Route::get('admin/add-images', 'AjaxController@add_Images');
	Route::get('admin/view-image-detail/{id}', 'AjaxController@viewImageDetail');
	Route::get('admin/edit-image/{id}', 'AjaxController@editImage');
	Route::post('admin/post-images/{id}', 'AjaxController@postImages');

	// client

	Route::get('admin/client', 'AjaxController@client');
	Route::any('pagination/client', 'PaginationController@client');
	Route::get('admin/add-client', 'AjaxController@AddClient');
	Route::post('admin/post-add-client', 'AjaxController@postAddClient');
	Route::get('admin/edit-client/{id}', 'AjaxController@editClient');
	Route::post('admin/post-client/{id}', 'AjaxController@postClient');

	// Utility
	Route::get('status_activity/{id}/{status}/{function}', 'PageController@status_activity');
	Route::get('delete_activity/{id}/{function}', 'PageController@delete_activity');
	//~ Route::get('delete_activity_minumcost/{id}/{function}/{subid}','PageController@delete_activity_minumcost');
	Route::any('ajax/getCity', 'AjaxController@city');
	Route::any('ajax/getState', 'AjaxController@state');

	Route::get('admin/editcity/{id}', 'ExtraController@editcity');

	//Carrier list
	Route::any('admin/transporter', 'PageController@transporter');
	Route::any('pagination/transporter-list', 'PaginationController@transporter');

	// csv Export
	Route::get('admin/transporter/export-excel', 'PageController@Transporter_exportexcel');
	Route::get('admin/requester/export-excel', 'PageController@Requester_exportexcel');

	// Route::any('pagination/address-list','PaginationController@address');
	//~ Route::get('admin/carrier/detail/{id}','PageController@users_detail');
	Route::get('admin/edit-transporter/{id}', 'PageController@edit_transporter');
	Route::post('admin/edit-transporter/{id}', 'PageController@post_edit_transporter');
	Route::post('ajax/add_account_to_stripe', 'AjaxController@postadd_account_to_stripe');
	Route::get('ajax/transfer_fund_view', 'AjaxController@view_transfer_fund');
	Route::post('ajax/transfer_fund_to_carrier', 'AjaxController@transfer_fund_to_carrier');
	//~ Route::get('admin/transaction_history','PageController@transaction_history');
	//~ Route::post('pagination/transaction_history','PaginationController@transaction_history');
	Route::any('admin/requester-transaction-history', 'PageController@requester_transaction');
	Route::post('pagination/requester-transaction-history', 'PaginationController@requester_transaction');
	Route::post('ajax/transaction-history-driver', 'TransactionController@transaction_history_driver');
	Route::post('ajax/transaction_to_driver', 'TransactionController@transaction_to_driver');
	//~ Route::post('admin/export_carrier_transaction','PageController@export_carrier_transaction');

	//~ Route::get('ajax/get_package_information/{id}','AjaxController@get_package_information');
	//~ Route::get('ajax/get_refund_information/{id}/{refundamt}','AjaxController@get_refund_information');
	//~ Route::get('admin/cancel_package/{id}','PageController@cancel_package');
	//~ Route::post('admin/cancel_package/{id}','PageController@postcancel_package');
	Route::get('ajax/get_user_info/{id}', 'PageController@requester_detail');
	//Admin user
	Route::get('admin/admin_user', 'PageController@admin_user');
	Route::post('pagination/admin_user', 'PaginationController@admin_user');
	Route::get('admin/add_admin_user', 'PageController@add_admin_user');
	Route::post('admin/add_admin_user', 'PageController@post_add_admin_user');
	Route::get('admin/edit_admin_user/{id}', 'PageController@edit_admin_user');
	Route::post('admin/edit_admin_user/{id}', 'PageController@post_edit_admin_user');
	//~ // Old or unused code keep for demo
	//~ //Route::get('admin/users', 'PageController@user_list'); // Admin Controller
	//~ //Route::post('admin', 'PageController@create');    //save New category to database
	//~ Route::get('/user_delete/{id}/{function}','PageController@user_delete');

	//Transaction history
	Route::get('admin/transaction-history', 'TransactionController@transaction_history');
	Route::post('pagination/transaction-history', 'TransactionController@pagination_transaction_history');
	Route::any('admin/export_csv', 'TransactionController@export_csv');

	//Notification section
	Route::any('admin/notification', 'NotificationController@notification');
	Route::post('pagination/notification', 'NotificationController@pagination_notification');
	Route::any('admin/send-notification', 'NotificationController@send_notification');
	Route::any('admin/post-promocode-detail', 'NotificationController@post_promo_details');
	Route::post('pagination/send-notification', 'NotificationController@pagination_send_notification');
	Route::post('pagination/send-promocode-details', 'NotificationController@pagination_send_promocode');
	Route::post('admin/post-notificatione', 'NotificationController@post_notificatione');
	Route::any('admin/view-user/{id}', 'NotificationController@view_user');
	Route::any('pagination/view-user/{id}', 'NotificationController@pagination_view_user');

	// Country Section
	Route::any('admin/add_country', 'PageController@add_country');
	Route::any('admin/country_list', 'PageController@country_list');
	Route::resource('pagination/country_list', 'PaginationController@country_list');
	Route::post('admin/update_country/{id}', 'PageController@update_country');

	//State section
	Route::any('admin/state_list', 'PageController@state_list');
	Route::any('admin/state_list/{id}', 'PageController@state_list');
	Route::any('pagination/state_list', 'PaginationController@state_list');
	Route::post('admin/add_state', 'PageController@add_state');
	Route::post('admin/update_state/{id}', 'PageController@update_state');
	// Add Kenya State City
	Route::get('admin/countryList', 'PageController@add_kenya_state');
	//City section
	Route::any('admin/city_list', 'PageController@city_list');
	Route::resource('pagination/city_list', 'PaginationController@city_list');
	Route::post('admin/add_city', 'PageController@add_city');
	Route::post('admin/post_add_city', 'PageController@post_add_city');
	Route::post('admin/update_city/{id}', 'PageController@update_city');

	// Category Section
	Route::any('admin/add_category', 'PageController@add_category');
	Route::any('admin/category_list', 'PageController@category_list');
	Route::resource('pagination/category_list', 'PaginationController@category_list');
	Route::post('admin/update_category/{id}', 'PageController@update_category');
	Route::get('admin/add-shipping/{id}', 'PageController@shipping');
	Route::post('admin/add-shipping/{id}', 'PageController@postshipping');

	Route::post('admin/add-other-shipping/{id}', 'PageController@postOtherShipping');

	

	// item
	Route::post('admin/add-item', 'PageController@add_item');
	Route::any('admin/item', 'PageController@item');
	Route::resource('pagination/item', 'PaginationController@item');
	Route::post('admin/update-add-item/{id}', 'PageController@postAddItem');

	// local category
	Route::any('admin/add-local-category', 'PageController@addlocalCategory');
	Route::any('admin/local-category-list', 'PageController@localcategorylist');
	Route::resource('pagination/local-category-list', 'PaginationController@localCategorylist');
	Route::post('admin/update-loacl-category/{id}', 'PageController@updateLocalcategory');
	Route::get('admin/add-local-shipping/{id}', 'PageController@localshipping');
	Route::post('admin/add-local-shipping/{id}', 'PageController@localpostshipping');
	Route::any('admin/add-distance', 'AjaxController@adddistance');
	Route::any('admin/distance', 'ExtraController@distance');
	Route::post('pagination/distance', 'PaginationController@distance');
	Route::post('admin/update-distance/{id}', 'ExtraController@updatedistance');
	Route::get('admin/item-value', 'ExtraController@item_value');
	Route::post('pagination/item-value', 'PaginationController@item_value');

	// new accra management
	Route::get('admin/accra-mamagement', 'ExtraController@accraMamagement');
	Route::post('pagination/accra-management', 'PaginationController@accraManagement');
	Route::post('admin/add-accra', 'ExtraController@addAccra');
	Route::post('admin/update-region/{id}', 'ExtraController@updateRegion');
	Route::get('admin/add-region', 'ExtraController@addRegion');
	// Route::get('admin/add-kenya-region', 'ExtraController@addKenyaResionCharge');

	//App content
	Route::get('admin/app_content', 'PageController@app_content');
	Route::get('admin/edit_app_content/{id}', 'PageController@edit_app_content');
	Route::post('admin/edit_app_content/{id}', 'PageController@post_edit_app_content');
	Route::post('pagination/app_content', 'PaginationController@app_content');

	//Web content
	Route::get('admin/web_content', 'PageController@web_content');
	Route::resource('pagination/web_content', 'PaginationController@web_content');
	Route::get('admin/edit_web_content/{id}', 'PageController@edit_web_content');
	Route::post('admin/update_web_content/{id}', 'PageController@update_web_content');
	Route::get('admin/add_web_content', 'PageController@add_web_content');

	//App Tutorial
	Route::any('admin/app_tutorial', 'PageController@app_tutorial');
	Route::resource('pagination/app_tutorial', 'PaginationController@app_tutorial');
	Route::any('admin/add_app_tutorial', 'PageController@add_app_tutorial');
	Route::get('admin/edit_app_tutorial/{id}', 'PageController@edit_app_tutorial');
	Route::post('admin/update_app_tutorial/{id}', 'PageController@update_app_tutorial');
	Route::get('admin/view_app_tutorial/{id}', 'PageController@view_app_tutorial');
	Route::post('admin/post_app_tutorial', 'PageController@post_app_tutorial');

	// About App
	Route::get('admin/about_app', 'PageController@about_app');
	Route::post('admin/update_about_app/{id}', 'PageController@update_about_app');

	//Email template section
	Route::get('admin/email_template', 'PageController@email_template');
	Route::resource('pagination/email_template', 'PaginationController@email_template');
	Route::get('admin/edit_email_template/{id}', 'PageController@edit_email_template');
	Route::post('admin/edit_email_template/{id}', 'PageController@post_email_template');
	Route::get('admin/add_email_template', 'PageController@new_email_template');
	Route::post('admin/add_email_template', 'PageController@add_email_template');

	// Faq
	Route::any('admin/faq_list', 'PageController@faq_list');
	Route::resource('pagination/faq_list', 'PaginationController@faq_list');
	Route::get('admin/add_faq', 'PageController@add_faq');
	Route::post('admin/post_faq', 'PageController@post_faq');
	Route::get('admin/edit_faq/{id}', 'PageController@edit_faq');
	Route::post('admin/update_faq/{id}', 'PageController@update_faq');
	Route::any('admin/sequence', 'PageController@post_sequence');

	// section
	Route::any('admin/section', 'ExtraController@section');
	Route::any('admin/faq/{id}', 'ExtraController@faq');
	Route::get('admin/add-section', 'ExtraController@addSection');
	Route::post('admin/post-add-section', 'ExtraController@postAddSection');
	Route::post('admin/post-edit-section', 'ExtraController@PostEditSection');

	Route::get('admin/add-faq/{id}', 'ExtraController@addFaq');
	Route::post('admin/post-add-faq', 'ExtraController@PostAddFaq');
	Route::get('admin/edit-faq/{id}', 'ExtraController@EditFaq');
	Route::post('admin/post-edit-faq/{id}', 'ExtraController@PostEditfaq');
	Route::get('admin/faq-answer/{id}', 'ExtraController@faqAnswer');

	Route::get('admin/complains', 'ExtraController@complains');
	Route::post('admin/send-email', 'ExtraController@sendEmail');

	//support section
	Route::any('admin/support', 'PageController@support');
	Route::resource('pagination/support', 'PaginationController@support');
	Route::post('admin/update-support/{id}', 'PageController@update_support');
	Route::any('admin/support_view_more/{id}', 'PageController@support_view_more');
	Route::any('pagination/support_view_more/{id}', 'PaginationController@support_view_more');
	Route::post('admin/update_support_view_more/{id}', 'PageController@update_support_view_more');

	// Vehicles section
	Route::get('admin/configuration', 'PageController@configuration');
	Route::post('admin/configuration', 'PageController@postConfiguration');

	// Forgot password
	Route::get('admin/forgot_password', 'AuthController@getforgot');
	Route::post('admin/forgot_password', 'AuthController@postforgot');
	Route::get('admin/reset_password/{id}/{token}', 'AjaxController@reset_password');
	Route::post('admin/reset_password/{id}/{token}', 'AjaxController@postreset_password');

	/*   Route::get('admin', 'AdminController@dash');

		    Route::filter('auth', function()
		    {
		    if (Auth::guest()) return Redirect::guest('users/login');
		    });

		    //Route::resource('admin/dashboard', 'PageController@charts');
		    Route::get('admin', 'PageController@dashboard');

		    //Route::controller('admin','add_account_to_stripe');
	*/
	// Transaction section
	Route::any('admin/transporter-transaction-history', 'PageController@transporter_transaction');
	Route::resource('pagination/transporter-history', 'PaginationController@transporter_transaction');
	Route::get('admin/transaction_detail/{id}', 'PageController@transaction_detail');
	Route::any('admin/auto_search', 'PageController@auto_search');

/*//

//Import country section
Route::any('admin/import_country','PageController@import_country');
Route::post('admin/post_import_country','PageController@post_import_country');

//feedback section
Route::any('admin/feedback','PageController@feedback');
Route::resource('pagination/feedback','PaginationController@feedback');

Route::get('/delete/{id}','PageController@destroy');
Route::resource('edit','PageController@edit');
Route::resource('update','PageController@update');

Route::resource('category', 'CategoryController');
//Route::get('category', 'CategoryController');
//Route::resource('photos.comments', 'PhotoCommentController');
 */
	Route::get('admin/change_password', 'PageController@change_password');
	Route::post('admin/change_password_post', 'PageController@change_password_post');

	Route::any('admin/local_notificationtiming', 'PageController@local_notificationtiming');

	//Route::get('ajax/state','AjaxController@state');
	//Route::get('ajax/city','AjaxController@city');
	//Route::get('ajax/address_information','AjaxController@address_information');
	Route::get('ajax/badge', 'AjaxController@badge');
	Route::get('admin/setting', 'PageController@setting');
	Route::post('admin/setting', 'PageController@postsetting');
	Route::get('admin/admin_notification', 'PageController@admin_notification');
	Route::post('pagination/admin_notification', 'PaginationController@admin_notification');
	Route::post('admin/refund_amount/{id}', 'PageController@refund_amount');

	//Trip
	Route::get('admin/indiviual-trip', 'PageController@trip');
	Route::post('pagination/trip', 'PaginationController@trip');
	Route::get('admin/trips_request/{id}', 'PageController@trips_request');
	Route::any('pagination/trips_request/{id}', 'PaginationController@trips_request');
	Route::get('admin/business-trip', 'PageController@business_trip');
	Route::post('pagination/business-trip', 'PaginationController@business_trip');
	Route::get('admin/business_request/{id}', 'PageController@business_request');
	Route::any('pagination/business_request/{id}', 'PaginationController@business_request');
	Route::get('admin/edit-business-trip/{id}', 'PageController@edit_business_trip');
	Route::post('admin/edit-business-trip/{id}', 'PageController@post_business_trip');
	Route::get('admin/edit-individual-trip/{id}', 'PageController@edit_individual_trip');
	Route::post('admin/edit-individual-trip/{id}', 'PageController@post_individual_trip');
	Route::get('ajax/update_perpage', 'PaginationController@update_perpage');

	Route::get('admin/trip-detail/{id}', 'PageController@trip_detail');

	Route::get('admin/promocode', 'PageController@promocode');
	Route::post('pagination/promocode', 'PaginationController@promocode');
	Route::get('admin/add-promocode', 'PageController@add_promocode');
	Route::post('admin/post-promocode', 'PageController@post_promocode');
	Route::get('admin/edit-promocode/{id}', 'PageController@edit_promocode_page');
	Route::get('admin/promocode-detail/{id}', 'PageController@promocode_detail');
	Route::post('admin/promocode-detail/{id}', 'PageController@post_promocode_detail');
	Route::post('admin/update-promocode/{id}', 'PageController@update_promocode');
	Route::get('admin/address', 'PageController@address');

/*  buy for me add */
	Route::get('admin/post-buy-for-me', 'BuyformeController@post_buy_for_me');
	Route::post('admin/add-buy-for-me-item', 'BuyformeController@add_buy_for_me_item');
	Route::post('admin/edit-add-buy-for-me-item', 'BuyformeController@add_buy_for_me_item');

	Route::post('admin/edit-buy-for-me-item/{id?}', 'BuyformeController@edit_buy_for_me_item');
	Route::post('admin/buy-for-me-calculation', 'BuyformeController@buy_for_me_calculation');
	Route::post('admin/post_buy_for_me', 'BuyformeController@pre_buy_for_me');
	Route::get('admin/post_delete_item/{id}/{function}', 'BuyformeController@post_delete_item');

	Route::post('admin/add-address', 'BuyformeController@add_address');
	Route::get('admin/on-line-purchases', 'BuyformeController@on_line_purchases');
	Route::post('admin/add-online-item', 'BuyformeController@add_online_item'); // 2
	Route::post('admin/edit-online-item/{id?}', 'BuyformeController@edit_online_item');
	Route::get('admin/delete_item/{id}/{function}/{rid?}', 'BuyformeController@delete_item');
	Route::post('admin/online-purchase-calculation', 'BuyformeController@online_purchase_calculation');
	Route::post('admin/post-online-purchase', 'BuyformeController@post_online_purchase');
	//Route::post('admin/validate_promocode', 'AjaxController@validate_promocode');
	Route::post('admin/validate_promocode', 'RequestProcessController@validate_promocode');

	Route::get('admin/online_package/payment/{id}', 'BuyformeController@onlinePayment');
	Route::post('admin/online_package/payment/{id}', 'BuyformeController@postOnlinePayment');

	/*   buy for me   */
	Route::get('admin/post-buyforme/payment/{id}', 'BuyformeController@post_buyforme');
	Route::get('admin/buy-for-me/payment/{id}', 'BuyformeController@buy_for_me_payment'); //  online
	Route::post('admin/buyforme/payment', 'BuyformeController@buyforme_payment');

	Route::get('admin/user-list', 'BuyformeController@user_list');

	Route::post('admin/post-buy-for-me/payment', 'BuyformeController@post_buy_for_me_payment'); // online

	Route::get('admin/edit-online-package/{id}', 'BuyformeController@edit_online_package');

	Route::post('admin/send-upddate-mail', 'BuyformeController@send_upddate_mail');

	//Add comment
	//Route::any('admin/add-comment/{id}', 'NotificationController@add_comment');
	Route::post('admin/post-comment', 'NotificationController@post_comment');

	Route::any('admin/comment-list/{id}', 'NotificationController@comment_list');
	Route::post('pagination/comment-list/{id}', 'PaginationController@comment_list');
	
	//Add comment for user
	//Route::any('admin/add-comment/{id}', 'NotificationController@add_comment');
	Route::post('admin/post-user-comment', 'NotificationController@post_user_comment');

	Route::any('admin/user-comment-list/{id}', 'NotificationController@user_comment_list');
	Route::post('pagination/user-comment-list/{id}', 'PaginationController@user_comment_list');

	//Slip management
	Route::get('admin/slip-management', 'SlipController@index');
	Route::post('admin/slip-orderId', 'SlipController@slipOrderId');
	Route::any('admin/get-slip', 'SlipController@getSlip');
	Route::any('admin/create-slip', 'SlipController@createSlip');
	Route::any('admin/slip-list', 'SlipController@slipList');
	Route::any('admin/pagination/slip_list', 'SlipController@pagination_slip_list');

	Route::any('admin/packing-slip-list', 'SlipController@packingList');
	Route::any('admin/pagination/packing_list', 'SlipController@pagination_packing_list');
	Route::any('admin/get-packing-slip', 'SlipController@getPackingSlip');
	Route::any('admin/create-packing-slip', 'SlipController@createPackingSlip');
	Route::any('admin/delete-packing-slip', 'SlipController@deletePackingSlip');

	//admin payment for order

	Route::any('admin/mark-paid/{id}', 'AdminPayment@mark_paid');
	Route::any('admin/pay-via-card/{id}', 'AdminPayment@PayViaCard');
	Route::any('admin/add-card', 'AdminPayment@add_card');
	Route::any('admin/delete-card/{id}/{cardId}', 'AdminPayment@delete_card');
	Route::any('admin/buyforme-pay-card/{id}/{cardId}', 'AdminPayment@buyforme_request_card_pay');

	Route::any('admin/online-package-mark-paid/{id}', 'AdminPayment@online_package_mark_paid');
	Route::any('admin/online-payment/{id}', 'AdminPayment@online_request_payment');
	Route::any('admin/online-pay-card/{id}/{cardId}', 'AdminPayment@online_request_card_pay');

	Route::any('admin/package-mark-paid/{id}', 'AdminPayment@package_mark_paid');
	Route::any('admin/request-card-list/{id}', 'AdminPayment@request_card_list');
	Route::any('admin/send-package-paynow/{id}/{cardId}', 'AdminPayment@pay_now');

	Route::get('admin/edit-request/{id}', 'SendpackageCtrl@editRequest');

	//send package process
	Route::post('admin/send-package-delivery-process', 'RequestProcessController@send_package_dellivery_process');
	Route::post('admin/send-package-item-update', 'RequestProcessController@sendPackageItemUpdate');
	Route::post('admin/send-package-assign-transporter', 'RequestProcessController@sendPackageAssignTransporter');
	Route::get('admin/get-item-info', 'BuyformeController@get_item_info');
	Route::post('admin/send-package-transporter-accept', 'RequestProcessController@sendPackageTraAccept');
	Route::post('admin/send-package-transporter-process', 'RequestProcessController@sendPackageTraProcess');

	Route::post('admin/local-delivery-process', 'RequestProcessController@loaclDeliveryProcess');
	Route::post('admin/local-delivery-item-update', 'RequestProcessController@localDeliveryupdateItem');
	Route::post('admin/local-delivery-assign-transporter', 'RequestProcessController@localDeliveryassign');
	Route::any('local-assign-all-tp/{id}', 'LocalDeliveryController@localassignTp');

	Route::get('admin/get_item_info/{id}', 'BuyformeController@get_item_info1');
	Route::get('admin/get_item_info2/{rid}/{id}', 'BuyformeController@get_item_info2');

	// LOCAL DELIVERY SECTION
	Route::get('admin/local-delivery', 'PageController@local_delivery');
	Route::post('pagination/local-delivery', 'PaginationController@local_delivery');
	Route::get('admin/local-delivery-detail/{id}', 'PageController@local_delivery_detail');

	//Payment log
	Route::get('admin/payment-log/{id}', 'RequestProcessController@payment_log');
	Route::post('pagination/payment-log/{id}', 'PaginationController@payment_log');

	Route::any('admin/get-packing-slip2', 'SlipController@getPackingSlip2');

	Route::POST('admin/ajax/getIpayStatus', 'MobileMoney2@getIpayStatus');
	Route::get('admin/ajax/changeIpayStatus/{id}', 'MobileMoney2@changeIpayStatus');

	/* Mobile money */
	Route::get('admin/pay-mobile-money/{id}', 'MobileMoney2@getMobileno');
	Route::post('admin/add-mobile-number/{id}', 'MobileMoney2@addMobileNumber');
	Route::get('admin/delete-mobile/{id}/{reqid}', 'MobileMoney2@deleteMobile');
	Route::get('admin/mobile-payment/{id}/{reqid}', 'MobileMoney2@mobilePayment');
	Route::post('admin/Voucher-mobile-payment/{id}', 'MobileMoney2@vouchermobilePayment');

	Route::get('admin/cancel-request/{id}', 'RequestProcessController@cancel_package');

	Route::get('admin/get-item-info3', 'BuyformeController@get_item_info3');

	Route::get('admin/alert-msg', 'PageController@alert_msg');
	Route::post('admin/update-alert-msg', 'PageController@update_alert_msg');

	// Local Delivery Section
	// Route::get('admin/local-delivery', 'PageController@local_delivery');
	// Route::post('pagination/local-delivery', 'PaginationController@local_delivery');
	// Route::get('admin/local-delivery-detail/{id}', 'PageController@local_delivery_detail');
	// Route::get('admin/add-local-delivery', 'LocalRsrequest@add_local_delivery');
	// //Route::get('admin/edit-local-delivery/{id}', 'Rsrequest@edit_package');
	// // Route::post('admin/prepare-request', 'Rsrequest@prepare_request');
	// // Route::get('admin/process-card-list/{id}', 'PageController@prepare_request_make_tnx');
	// // Route::post('admin/process-card-list/{id}', 'PageController@post_prepare_request_make_tnx');

	Route::post('admin/local-transporter-accept', 'LocalDeliveryController@localPackageTraAccept');
	Route::post('admin/local-transporter-process', 'LocalDeliveryController@localTraProcess');
	
	// Edit request(Buy for me) Tax and Customs and Duty.
	Route::post("admin/edit-request-customsduty/{id}", "PageController@EditCustomsDuty");
	Route::post("admin/edit-request-tax/{id}", "PageController@EditTax");

	// Give admin the option to transition online purchase items to delivered.
	Route::post("admin/online-purchase-process", "PageController@OnlinePurchaseProcess");
	
	// Out for pickup all
	Route::post('admin/out-for-pickup-all', 'PageController@OutForPickupAll');
	
	// Out for delivery all
	Route::post('admin/out-for-delivery-all', 'PageController@OutForDeliveryAll');
	
	// Delivered all
	Route::post('admin/delivered-all', 'PageController@DeliveredAll');
	
	// Accept all
	Route::post('admin/accept-all', 'PageController@AcceptAll');

	// Shipment departed
	Route::post('admin/shipment-departed-all', 'PageController@ShipmentDepartedAll');
});
