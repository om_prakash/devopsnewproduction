<?php 
namespace App\Modules\Admin\Models\Setting;

use Jenssegers\Mongodb\Model as Eloquent;

class Setting extends Eloquent {

    protected $collection = 'setting';

}

