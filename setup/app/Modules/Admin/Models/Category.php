<?php 
namespace App\Modules\Admin\Models\Category;

use Jenssegers\Mongodb\Model as Eloquent;

class Category extends Eloquent {

    protected $collection = 'category';

}

