<?php 
namespace App\Modules\Admin\Models\Contact;

use Jenssegers\Mongodb\Model as Eloquent;

class Contact extends Eloquent {

    protected $collection = 'contact_us';

}

