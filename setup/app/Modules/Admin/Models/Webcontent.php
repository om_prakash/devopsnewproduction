<?php 
namespace App\Modules\Admin\Models\Webcontent;

use Jenssegers\Mongodb\Model as Eloquent;

class Webcontent extends Eloquent {

    protected $collection = 'web_content';

}

