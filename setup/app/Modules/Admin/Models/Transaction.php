<?php namespace App\Modules\Admin\Models\Transaction;

use Jenssegers\Mongodb\Model as Eloquent;

class Transaction extends Eloquent {

    protected $collection = 'transaction';

}

