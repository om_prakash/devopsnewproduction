<?php 
namespace App\Modules\Admin\Models\Subscription;

use Jenssegers\Mongodb\Model as Eloquent;

class Subscription extends Eloquent {

    protected $collection = 'subscription';

}

