<?php 
namespace App\Modules\Admin\Models\Counters;

use Jenssegers\Mongodb\Model as Eloquent;

class Counters extends Eloquent {

    protected $collection = 'counters';

}

