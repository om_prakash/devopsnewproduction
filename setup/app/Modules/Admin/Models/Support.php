<?php 
namespace App\Modules\Admin\Models\Support;

use Jenssegers\Mongodb\Model as Eloquent;

class Support extends Eloquent {

    protected $collection = 'support';

}

