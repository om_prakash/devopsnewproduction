<?php 
namespace App\Modules\Admin\Models\Userextrainfo;

use Jenssegers\Mongodb\Model as Eloquent;

class Userextrainfo extends Eloquent {

    protected $collection = 'user_additional_info';

}

