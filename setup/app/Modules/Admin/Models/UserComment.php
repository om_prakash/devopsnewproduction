

<?php 
namespace App\Modules\Admin\Models\UserComment;

use Jenssegers\Mongodb\Model as Eloquent;

class UserComment extends Eloquent {

    protected $collection = 'user_comment';

}

