<?php 
namespace App\Modules\Admin\Models\AppTutorial;

use Jenssegers\Mongodb\Model as Eloquent;

class AppTutorial extends Eloquent {

    protected $collection = 'app_tutorial';

}

