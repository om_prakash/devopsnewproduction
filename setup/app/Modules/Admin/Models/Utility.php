<?php 
namespace App\Modules\Admin\Models\Utility;

use Jenssegers\Mongodb\Model as Eloquent;

class Utility extends Eloquent {

    protected $collection = 'utility';

}

