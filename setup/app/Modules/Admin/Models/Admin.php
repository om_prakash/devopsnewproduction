<?php namespace App\Modules\Admin\Models;


use Jenssegers\Mongodb\Model as Eloquent;

class Admin extends Eloquent {

    protected $collection = 'admin';	
		
}
