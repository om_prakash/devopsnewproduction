<?php namespace App\Modules\Admin\Models\Deliveryrequest;

use Jenssegers\Mongodb\Model as Eloquent;

class Deliveryrequest extends Eloquent {

    protected $collection = 'delivery_request';    
    
}

