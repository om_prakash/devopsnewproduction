<?php 
namespace App\Modules\Admin\Models\User;

use Jenssegers\Mongodb\Model as Eloquent;

class User extends Eloquent {

    protected $collection = 'users';

}

