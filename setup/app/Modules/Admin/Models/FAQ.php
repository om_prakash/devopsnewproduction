<?php 
namespace App\Modules\Admin\Models\FAQ;

use Jenssegers\Mongodb\Model as Eloquent;

class FAQ extends Eloquent {

    protected $collection = 'faq';

}

