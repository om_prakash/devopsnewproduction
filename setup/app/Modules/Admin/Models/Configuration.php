<?php 
namespace App\Modules\Admin\Models\Configuration;

use Jenssegers\Mongodb\Model as Eloquent;

class Configuration extends Eloquent {

    protected $collection = 'configuration';

}

