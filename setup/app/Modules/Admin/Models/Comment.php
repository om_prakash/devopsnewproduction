

<?php 
namespace App\Modules\Admin\Models\Comment;

use Jenssegers\Mongodb\Model as Eloquent;

class Comment extends Eloquent {

    protected $collection = 'comment';

}

