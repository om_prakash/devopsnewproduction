<?php 
namespace App\Modules\Admin\Models\Trips;

use Jenssegers\Mongodb\Model as Eloquent;

class Trips extends Eloquent {

    protected $collection = 'trips';

}

