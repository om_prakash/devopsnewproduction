<?php 
namespace App\Modules\Admin\Models\State;

use Jenssegers\Mongodb\Model as Eloquent;

class State extends Eloquent {

    protected $collection = 'state';

}

