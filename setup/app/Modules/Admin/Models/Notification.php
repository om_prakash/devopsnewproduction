<?php 
namespace App\Modules\Admin\Models\Notification;

use Jenssegers\Mongodb\Model as Eloquent;

class Notification extends Eloquent {

    protected $collection = 'notification';

}

