<?php 
namespace App\Modules\Admin\Models\City_State_Country;

use Jenssegers\Mongodb\Model as Eloquent;

class City_State_Country extends Eloquent {

    protected $collection = 'city_state_country';

}

