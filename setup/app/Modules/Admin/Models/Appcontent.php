<?php 
namespace App\Modules\Admin\Models\Appcontent;

use Jenssegers\Mongodb\Model as Eloquent;

class Appcontent extends Eloquent {

    protected $collection = 'app_content';

}

