<?php 
namespace App\Modules\Admin\Models\Emailtemplate;

use Jenssegers\Mongodb\Model as Eloquent;

class Emailtemplate extends Eloquent {

    protected $collection = 'email_template';

}

