<?php namespace App\Modules\Admin\Controllers;

/*
 * Project: Aquantuo
 *
 */

use App\Http\Controllers\Controller;
use App\Http\Models\Activitylog;
use App\Http\Models\Admin;
use App\Http\Models\Comment;
use App\Http\Models\Deliveryrequest;
use App\Http\Models\SlipManagement;
use App\Http\Models\CityStateCountry;
use App\Http\Models\User;
use Auth;
use Illuminate\Support\Facades\Redirect;
use Input;
use MongoDate,DateTime;
use MongoId;
use PDF;

class SlipController extends Controller {

	/*
		    Function  : Checking login users.
		    Developer : Om Dangi
		    Created At: 06/02/2018
		    Metod     : Post
	*/
	public function __construct() {
		if (!(Auth::user())) {
			Redirect::to('admin/login')->send();
		}
	}

	/*
		    Function  : uses for create slip.
		    Developer : Om Dangi
		    Created At: 06/02/2018
		    Metod     : Post
	*/

	public function index() {

		$request_users = Deliveryrequest::select('RequesterId')->get();
        $users_array=[];
        foreach ($request_users as $key ) {
            $users_array[]=$key->RequesterId;
        }


        $data['res'] = $data['users'] =User::whereIn('_id',$users_array)->select('Name')->get();

		//$data['res'] = Deliveryrequest::where('RequesterName', '!=', '')->groupBy('RequesterName')->get(['RequesterName']);
		$data['title'] = 'Delivery List';
		$data['info'] = array();

		/* echo '<pre>';
        print_r($data['res']);die;*/

		return view('Admin::list.slip_management', $data);

	}

	public function receivedList(){
		if (Auth::user()->received_slip != 'yes' ) {
			return Redirect::to('/admin/dashboard');
		}

		$data['consigneeName'] = 'Aquantuo Company Limited';
		$data['consigneeAddress'] = "5 Tulip Link Nii Okaiman West Accra, Ghana";
		$data['consigneePhone'] = '0501634195';
		if (Input::get('country') === 'Kenya') {
			$data['consigneeName'] = 'Sarah Khainza';
			$data['consigneeAddress'] = "Nextgen Mall, Along Mombasa Rd. Nairobi, Kenya";
			$data['consigneePhone'] = '0720761333';
		}
		if (Input::get('country') === 'Ghana') {
			$data['consigneeName'] = 'Aquantuo Company Limited';
			$data['consigneeAddress'] = "5 Tulip Link Nii Okaiman West Accra, Ghana";
			$data['consigneePhone'] = '0501634195';
		}
		$data['countryList'] = CityStateCountry::where(['type' => 'Country', 'Status' => 'Active'])
			->get(['_id', 'Content', 'state_available']);
		$receivedItems = [];
		$startdate = Input::get('StartDate');
		$enddate = Input::get('EndDate');
		$mode = Input::get('type');
		$data['selectedCountry'] = Input::get('country');

		if (Input::get('op') === 'op') {
			$query = Deliveryrequest::query();
			$query->where('Status', 'purchased')->where("RequestType", 'online');
			if (Input::get('StartDate') != '') {
				$query->where(['EnterOn'=> ['$gte'=>new MongoDate(strtotime(DateTime::createFromFormat('d/m/Y', $startdate)->format('d-m-Y')))]]);
			}
			if (Input::get('EndDate') != '') {
				$query->where(['EnterOn'=> ['$lte'=>new MongoDate(strtotime(DateTime::createFromFormat('d/m/Y', $enddate)->format('d-m-Y 23:59:59')))]]);
			}
			if (Input::get('country') != '') {
				$query->orWhere('DeliveryCountry', Input::get('country'))->orWhere('PickupCountry', Input::get('country'));
			}
			if ($mode != '') {
				// $query->where('travelMode', $mode);
				$query->where(array("ProductList" => array('$elemMatch' => array('travelMode' => $mode))));
			}

			$query->where(array("ProductList" => array('$elemMatch' => array('status' => array('$in' => array("purchased", Input::get('pending') === 'pending' ? "pending" : ''))))));
			$data['onlineReceivedItems'] = $query->select('ProductList')->get();
			if(count($data['onlineReceivedItems']) > 0){
				foreach ($data['onlineReceivedItems'] as $key2) {
					foreach ($key2->ProductList as $key) {
						if($key['status'] == 'purchased' || ($key['status'] == 'pending' && @$key['inform_mail_sent'] != 'no')){
							$receivedItems[] = $key;
						}
					}
				}
			}
		}

		if (Input::get('bfm') === 'bfm') {
			$query2 = Deliveryrequest::query();
			$query2->where('Status', 'purchased')->whereIn("RequestType",['buy_for_me']);
			if (Input::get('StartDate') != '') {
				$query2->where(['EnterOn'=> ['$gte'=>new MongoDate(strtotime(DateTime::createFromFormat('d/m/Y', $startdate)->format('d-m-Y')))]]);
			}
			if (Input::get('EndDate') != '') {
				$query2->where(['EnterOn'=> ['$lte'=>new MongoDate(strtotime(DateTime::createFromFormat('d/m/Y', $enddate)->format('d-m-Y 23:59:59')))]]);
			}
			if (Input::get('country') != '') {
				$query2->orWhere('DeliveryCountry', Input::get('country'))->orWhere('PickupCountry', Input::get('country'));
			}
			if ($mode != '') {
				// $query2->where('travelMode', $mode);
				$query2->where(array("ProductList" => array('$elemMatch' => array('travelMode' => $mode))));
			}

			$query2->where(array("ProductList" => array('$elemMatch' => array('status' => array('$in' => array("purchased", Input::get('pending') === 'pending' ? "pending" : ''))))));

			$data['bfmReceivedItems'] = $query2->select('ProductList')->get();
			
			if(count($data['bfmReceivedItems']) > 0){
				foreach ($data['bfmReceivedItems'] as $key2) {
					foreach ($key2->ProductList as $key) {
						if($key['status'] == 'purchased' || ($key['status'] == 'pending' && @$key['inform_mail_sent'] != 'no')){
							$receivedItems[] = $key;
						}
					}
				}
			}
		}

		if (Input::get('sap') === 'sap') {
			//SAP
			$query3 = Deliveryrequest::query();
			$query3->where('Status', 'out_for_pickup')->whereIn("RequestType",['delivery']);
			if (Input::get('StartDate') != '') {
				$query3->where(['EnterOn'=> ['$gte'=>new MongoDate(strtotime(DateTime::createFromFormat('d/m/Y', $startdate)->format('d-m-Y')))]]);
			}
			if (Input::get('EndDate') != '') {
				$query3->where(['EnterOn'=> ['$lte'=>new MongoDate(strtotime(DateTime::createFromFormat('d/m/Y', $enddate)->format('d-m-Y 23:59:59')))]]);
			}
			if (Input::get('country') != '') {
				$query3->orWhere('DeliveryCountry', Input::get('country'))->orWhere('PickupCountry', Input::get('country'));
			}
			if ($mode != '') {
				// $query3->where('travelMode'=>$mode);
				$query3->where(array("ProductList" => array('$elemMatch' => array('travelMode' => $mode))));
			}

			$query3->where(array("ProductList" => array('$elemMatch' => array('status' => array('$in' => array("out_for_pickup", Input::get('pending') === 'pending' ? "pending" : ''))))));

			$data['sapReceivedItems'] = $query3->select('ProductList')->get();
			if(count($data['sapReceivedItems']) > 0){
				foreach ($data['sapReceivedItems'] as $key3) {
					foreach ($key3->ProductList as $key) {
						if($key['status'] == 'out_for_pickup' || ($key['status'] == 'pending' && $key['inform_mail_sent'] != 'no')){
							if (array_key_exists("qty",$key) && array_key_exists("price",$key))
							{
								$receivedItems[] = $key;
							}
							else
							{
								$key['qty'] = $key['productQty'];
								$key['price'] = $key['productCost'];
								$key['weight'] = $key['productWeight'];
								$receivedItems[] = $key;
							}
							
						}
					}
				}
			}
		}

		if (Input::get('op') !== 'op' && Input::get('bfm') !== 'bfm' && Input::get('sap') !== 'sap' && Input::get('pending') === 'pending') {
			$query = Deliveryrequest::query();
			
			if (Input::get('StartDate') != '') {
				$query->where(['EnterOn'=> ['$gte'=>new MongoDate(strtotime(DateTime::createFromFormat('d/m/Y', $startdate)->format('d-m-Y')))]]);
			}
			if (Input::get('EndDate') != '') {
				$query->where(['EnterOn'=> ['$lte'=>new MongoDate(strtotime(DateTime::createFromFormat('d/m/Y', $enddate)->format('d-m-Y 23:59:59')))]]);
			}
			if (Input::get('country') != '') {
				$query->orWhere('DeliveryCountry', Input::get('country'))->orWhere('PickupCountry', Input::get('country'));
			}
			if ($mode != '') {
				// $query->where('travelMode', $mode);
				$query->where(array("ProductList" => array('$elemMatch' => array('travelMode' => $mode))));
			}

			$query->where(array("ProductList" => array('$elemMatch' => array('status' => array('$in' => array(Input::get('pending') === 'pending' ? "pending" : ''))))));
			$data['onlineReceivedItems'] = $query->select('ProductList')->get();
			if(count($data['onlineReceivedItems']) > 0){
				foreach ($data['onlineReceivedItems'] as $key2) {
					foreach ($key2->ProductList as $key) {
						if(($key['status'] == 'pending' && @$key['inform_mail_sent'] != 'no')){
							$receivedItems[] = $key;
						}
					}
				}
			}
			$query2 = Deliveryrequest::query();
			if (Input::get('StartDate') != '') {
				$query2->where(['EnterOn'=> ['$gte'=>new MongoDate(strtotime(DateTime::createFromFormat('d/m/Y', $startdate)->format('d-m-Y')))]]);
			}
			if (Input::get('EndDate') != '') {
				$query2->where(['EnterOn'=> ['$lte'=>new MongoDate(strtotime(DateTime::createFromFormat('d/m/Y', $enddate)->format('d-m-Y 23:59:59')))]]);
			}
			if (Input::get('country') != '') {
				$query2->orWhere('DeliveryCountry', Input::get('country'))->orWhere('PickupCountry', Input::get('country'));
			}
			if ($mode != '') {
				// $query2->where('travelMode', $mode);
				$query2->where(array("ProductList" => array('$elemMatch' => array('travelMode' => $mode))));
			}

			$query2->where(array("ProductList" => array('$elemMatch' => array('status' => array('$in' => array(Input::get('pending') === 'pending' ? "pending" : ''))))));

			$data['bfmReceivedItems'] = $query2->select('ProductList')->get();
			
			if(count($data['bfmReceivedItems']) > 0){
				foreach ($data['bfmReceivedItems'] as $key2) {
					foreach ($key2->ProductList as $key) {
						if(($key['status'] == 'pending' && @$key['inform_mail_sent'] != 'no')){
							$receivedItems[] = $key;
						}
					}
				}
			}
			//SAP
			$query3 = Deliveryrequest::query();
			if (Input::get('StartDate') != '') {
				$query3->where(['EnterOn'=> ['$gte'=>new MongoDate(strtotime(DateTime::createFromFormat('d/m/Y', $startdate)->format('d-m-Y')))]]);
			}
			if (Input::get('EndDate') != '') {
				$query3->where(['EnterOn'=> ['$lte'=>new MongoDate(strtotime(DateTime::createFromFormat('d/m/Y', $enddate)->format('d-m-Y 23:59:59')))]]);
			}
			if (Input::get('country') != '') {
				$query3->orWhere('DeliveryCountry', Input::get('country'))->orWhere('PickupCountry', Input::get('country'));
			}
			if ($mode != '') {
				// $query3->where('travelMode'=>$mode);
				$query3->where(array("ProductList" => array('$elemMatch' => array('travelMode' => $mode))));
			}

			$query3->where(array("ProductList" => array('$elemMatch' => array('status' => array('$in' => array(Input::get('pending') === 'pending' ? "pending" : ''))))));

			$data['sapReceivedItems'] = $query3->select('ProductList')->get();
			if(count($data['sapReceivedItems']) > 0){
				foreach ($data['sapReceivedItems'] as $key3) {
					foreach ($key3->ProductList as $key) {
						if(($key['status'] == 'pending' && @$key['inform_mail_sent'] != 'no')){
							if (array_key_exists("qty",$key) && array_key_exists("price",$key))
							{
								$receivedItems[] = $key;
							}
							else
							{
								$key['qty'] = $key['productQty'];
								$key['price'] = $key['productCost'];
								$key['weight'] = $key['productWeight'];
								$receivedItems[] = $key;
							}
							
						}
					}
				}
			}
		}
		// echo "<pre>";
		// print_r(json_decode(json_encode($data['onlineReceivedItems']), true));die();

		$data['sn']=1;
		//echo '<pre>';
		//print_r($receivedItems); die;
		// if(count($receivedItems) > 0){
			$data['receivedItems'] = $receivedItems;
			return view('Admin::list.received_slip_list', $data);
		// }else{
		// 	return Redirect::back()->with('fail','Received items not found');
		// }
	}

	public function CreatereceivedSlip(){
		if (Auth::user()->received_slip != 'yes' ) {
			return Redirect::to('/admin/dashboard');
		}
		$receivedItems = [];
		$startdate = Input::get('StartDate');
		$enddate = Input::get('EndDate');
		$mode = Input::get('type');
		$data['selectedCountry'] = Input::get('country');
		if (Input::get('op') === 'op') {
			$query = Deliveryrequest::query();
			$query->where('Status', 'purchased')->where("RequestType", 'online');
			if (Input::get('StartDate') != '') {
				$query->where(['EnterOn'=> ['$gte'=>new MongoDate(strtotime(DateTime::createFromFormat('d/m/Y', $startdate)->format('d-m-Y')))]]);
			}
			if (Input::get('EndDate') != '') {
				$query->where(['EnterOn'=> ['$lte'=>new MongoDate(strtotime(DateTime::createFromFormat('d/m/Y', $enddate)->format('d-m-Y 23:59:59')))]]);
			}
			if (Input::get('country') != '') {
				$query->orWhere('DeliveryCountry', Input::get('country'))->orWhere('PickupCountry', Input::get('country'));
			}
			if ($mode != '') {
				// $query->where('travelMode', $mode);
				$query->where(array("ProductList" => array('$elemMatch' => array('travelMode' => $mode))));
			}

			$query->where(array("ProductList" => array('$elemMatch' => array('status' => array('$in' => array("purchased", Input::get('pending') === 'pending' ? "pending" : ''))))));
			$data['onlineReceivedItems'] = $query->select('ProductList')->get();
			if(count($data['onlineReceivedItems']) > 0){
				foreach ($data['onlineReceivedItems'] as $key2) {
					foreach ($key2->ProductList as $key) {
						if($key['status'] == 'purchased' || ($key['status'] == 'pending' && @$key['inform_mail_sent'] != 'no')){
							$receivedItems[] = $key;
						}
					}
				}
			}
		}

		if (Input::get('bfm') === 'bfm') {
			$query2 = Deliveryrequest::query();
			$query2->where('Status', 'purchased')->whereIn("RequestType",['buy_for_me']);
			if (Input::get('StartDate') != '') {
				$query2->where(['EnterOn'=> ['$gte'=>new MongoDate(strtotime(DateTime::createFromFormat('d/m/Y', $startdate)->format('d-m-Y')))]]);
			}
			if (Input::get('EndDate') != '') {
				$query2->where(['EnterOn'=> ['$lte'=>new MongoDate(strtotime(DateTime::createFromFormat('d/m/Y', $enddate)->format('d-m-Y 23:59:59')))]]);
			}
			if (Input::get('country') != '') {
				$query2->orWhere('DeliveryCountry', Input::get('country'))->orWhere('PickupCountry', Input::get('country'));
			}
			if ($mode != '') {
				// $query2->where('travelMode', $mode);
				$query2->where(array("ProductList" => array('$elemMatch' => array('travelMode' => $mode))));
			}

			$query2->where(array("ProductList" => array('$elemMatch' => array('status' => array('$in' => array("purchased", Input::get('pending') === 'pending' ? "pending" : ''))))));

			$data['bfmReceivedItems'] = $query2->select('ProductList')->get();
			
			if(count($data['bfmReceivedItems']) > 0){
				foreach ($data['bfmReceivedItems'] as $key2) {
					foreach ($key2->ProductList as $key) {
						if($key['status'] == 'purchased' || ($key['status'] == 'pending' && @$key['inform_mail_sent'] != 'no')){
							$receivedItems[] = $key;
						}
					}
				}
			}
		}

		if (Input::get('sap') === 'sap') {
			//SAP
			$query3 = Deliveryrequest::query();
			$query3->where('Status', 'out_for_pickup')->whereIn("RequestType",['delivery']);
			if (Input::get('StartDate') != '') {
				$query3->where(['EnterOn'=> ['$gte'=>new MongoDate(strtotime(DateTime::createFromFormat('d/m/Y', $startdate)->format('d-m-Y')))]]);
			}
			if (Input::get('EndDate') != '') {
				$query3->where(['EnterOn'=> ['$lte'=>new MongoDate(strtotime(DateTime::createFromFormat('d/m/Y', $enddate)->format('d-m-Y 23:59:59')))]]);
			}
			if (Input::get('country') != '') {
				$query3->orWhere('DeliveryCountry', Input::get('country'))->orWhere('PickupCountry', Input::get('country'));
			}
			if ($mode != '') {
				// $query3->where('travelMode'=>$mode);
				$query3->where(array("ProductList" => array('$elemMatch' => array('travelMode' => $mode))));
			}

			$query3->where(array("ProductList" => array('$elemMatch' => array('status' => array('$in' => array("out_for_pickup", Input::get('pending') === 'pending' ? "pending" : ''))))));

			$data['sapReceivedItems'] = $query3->select('ProductList')->get();
			if(count($data['sapReceivedItems']) > 0){
				foreach ($data['sapReceivedItems'] as $key3) {
					foreach ($key3->ProductList as $key) {
						if($key['status'] == 'out_for_pickup' || ($key['status'] == 'pending' && $key['inform_mail_sent'] != 'no')){
							if (array_key_exists("qty",$key) && array_key_exists("price",$key))
							{
								$receivedItems[] = $key;
							}
							else
							{
								$key['qty'] = $key['productQty'];
								$key['price'] = $key['productCost'];
								$key['weight'] = $key['productWeight'];
								$receivedItems[] = $key;
							}
							
						}
					}
				}
			}
		}

		if (Input::get('op') !== 'op' && Input::get('bfm') !== 'bfm' && Input::get('sap') !== 'sap' && Input::get('pending') === 'pending') {
			$query = Deliveryrequest::query();
			
			if (Input::get('StartDate') != '') {
				$query->where(['EnterOn'=> ['$gte'=>new MongoDate(strtotime(DateTime::createFromFormat('d/m/Y', $startdate)->format('d-m-Y')))]]);
			}
			if (Input::get('EndDate') != '') {
				$query->where(['EnterOn'=> ['$lte'=>new MongoDate(strtotime(DateTime::createFromFormat('d/m/Y', $enddate)->format('d-m-Y 23:59:59')))]]);
			}
			if (Input::get('country') != '') {
				$query->orWhere('DeliveryCountry', Input::get('country'))->orWhere('PickupCountry', Input::get('country'));
			}
			if ($mode != '') {
				// $query->where('travelMode', $mode);
				$query->where(array("ProductList" => array('$elemMatch' => array('travelMode' => $mode))));
			}

			$query->where(array("ProductList" => array('$elemMatch' => array('status' => array('$in' => array(Input::get('pending') === 'pending' ? "pending" : ''))))));
			$data['onlineReceivedItems'] = $query->select('ProductList')->get();
			if(count($data['onlineReceivedItems']) > 0){
				foreach ($data['onlineReceivedItems'] as $key2) {
					foreach ($key2->ProductList as $key) {
						if(($key['status'] == 'pending' && @$key['inform_mail_sent'] != 'no')){
							$receivedItems[] = $key;
						}
					}
				}
			}
			$query2 = Deliveryrequest::query();
			if (Input::get('StartDate') != '') {
				$query2->where(['EnterOn'=> ['$gte'=>new MongoDate(strtotime(DateTime::createFromFormat('d/m/Y', $startdate)->format('d-m-Y')))]]);
			}
			if (Input::get('EndDate') != '') {
				$query2->where(['EnterOn'=> ['$lte'=>new MongoDate(strtotime(DateTime::createFromFormat('d/m/Y', $enddate)->format('d-m-Y 23:59:59')))]]);
			}
			if (Input::get('country') != '') {
				$query2->orWhere('DeliveryCountry', Input::get('country'))->orWhere('PickupCountry', Input::get('country'));
			}
			if ($mode != '') {
				// $query2->where('travelMode', $mode);
				$query2->where(array("ProductList" => array('$elemMatch' => array('travelMode' => $mode))));
			}

			$query2->where(array("ProductList" => array('$elemMatch' => array('status' => array('$in' => array(Input::get('pending') === 'pending' ? "pending" : ''))))));

			$data['bfmReceivedItems'] = $query2->select('ProductList')->get();
			
			if(count($data['bfmReceivedItems']) > 0){
				foreach ($data['bfmReceivedItems'] as $key2) {
					foreach ($key2->ProductList as $key) {
						if(($key['status'] == 'pending' && @$key['inform_mail_sent'] != 'no')){
							$receivedItems[] = $key;
						}
					}
				}
			}
			//SAP
			$query3 = Deliveryrequest::query();
			if (Input::get('StartDate') != '') {
				$query3->where(['EnterOn'=> ['$gte'=>new MongoDate(strtotime(DateTime::createFromFormat('d/m/Y', $startdate)->format('d-m-Y')))]]);
			}
			if (Input::get('EndDate') != '') {
				$query3->where(['EnterOn'=> ['$lte'=>new MongoDate(strtotime(DateTime::createFromFormat('d/m/Y', $enddate)->format('d-m-Y 23:59:59')))]]);
			}
			if (Input::get('country') != '') {
				$query3->orWhere('DeliveryCountry', Input::get('country'))->orWhere('PickupCountry', Input::get('country'));
			}
			if ($mode != '') {
				// $query3->where('travelMode'=>$mode);
				$query3->where(array("ProductList" => array('$elemMatch' => array('travelMode' => $mode))));
			}

			$query3->where(array("ProductList" => array('$elemMatch' => array('status' => array('$in' => array(Input::get('pending') === 'pending' ? "pending" : ''))))));

			$data['sapReceivedItems'] = $query3->select('ProductList')->get();
			if(count($data['sapReceivedItems']) > 0){
				foreach ($data['sapReceivedItems'] as $key3) {
					foreach ($key3->ProductList as $key) {
						if(($key['status'] == 'pending' && @$key['inform_mail_sent'] != 'no')){
							if (array_key_exists("qty",$key) && array_key_exists("price",$key))
							{
								$receivedItems[] = $key;
							}
							else
							{
								$key['qty'] = $key['productQty'];
								$key['price'] = $key['productCost'];
								$key['weight'] = $key['productWeight'];
								$receivedItems[] = $key;
							}
							
						}
					}
				}
			}
		}

		$updated = $receivedItems;
		foreach ($receivedItems as $key => $val) {
			$updated[$key]['qty'] = Input::get('quantity_'.$val['_id']);
			$updated[$key]['price'] = Input::get('price_'.$val['_id']);
			$updated[$key]['value'] = Input::get('value_'.$val['_id']);
		}

		$data['receivedItems'] = $updated;
		$data['shipper_address'] = Input::get('shipper_address');
		$data['shipper_name'] = Input::get('shipper_name');
		$data['consignee_name'] = Input::get('consignee_name');
		$data['consignee_address'] = Input::get('consignee_address');
		$data['sn']=1;
		$pdf = PDF::loadView('Admin::list.pdf.received2', $data);
		$pdf->setPaper('a4');
			//  $pdf->setPaperNumber(1);
		return $pdf->stream('slip.pdf');

		//echo '<pre>';
		//print_r($updated);
		//die;
	}

	public function test(){
		//$pdf = PDF::loadView('Admin::list.pdf.received');
		//$pdf->setPaper('a4');
			//  $pdf->setPaperNumber(1);
		//return $pdf->stream('slip.pdf');
		return view('Admin::list.pdf.received');
	}


	/*
		    Function  : Uses for slip list.
		    Developer : Om Dangi
		    Created At: 06/02/2018
		    Metod     : Post
	*/

	public function slipList() {

		$data['title'] = "Delivery List";
		$data['paginationurl'] = "admin/pagination/slip_list";
		$data['orderType'] = "Desc";
		$data['orderby'] = "_id";
		$data['postvalue'] = "&search_value=" . Input::get('search_value') . "&search_name=" . Input::get('search_name') . "&StartDate=" . Input::get('StartDate') . "&EndDate=" . Input::get('EndDate');
		return view('Admin::list.slip_list', $data);

	}

	/*
		    Function  : Uses for pagination slip list.
		    Developer : Om Dangi
		    Created At: 07/02/2018
		    Metod     : Post
	*/

	public function pagination_slip_list() {

		$query = SlipManagement::query();
		$countQuery = SlipManagement::query();

		$query->where('type', '=', 'delivery');
		$countQuery->where('type', '=', 'delivery');

		//searching
		$search_value = trim(Input::get('search_value'));
		if (@$search_value != '') {
			$query->Where('PackageNumber', '=', $search_value);
			$countQuery->Where('PackageNumber', '=', $search_value);
		}

		$search_name = trim(Input::get('search_name'));
		if (@$search_name != '') {
			$query->where('RequesterName', 'like', "%$search_name%");
			$countQuery->where('RequesterName', 'like', "%$search_name%");
		}

		/* $StartDate = Input::get('StartDate');
			        $EndDate = Input::get('EndDate');
			        if (@$StartDate != '' && @$EndDate != '') {

			        $StartDate = date('Y-m-d', (strtotime(Input::get('StartDate'))));
			        $StartDate = new MongoDate(strtotime($StartDate));

			        $EndDate = date('Y-m-d', (strtotime(Input::get('EndDate'))));
			        $EndDate = new MongoDate(strtotime($EndDate));

			        $query->where('EnterOn', '>=', $StartDate);
			        $query->where('EnterOn', '<=', $EndDate);

			        $countQuery->where('EnterOn', '>=', $StartDate);
			        $countQuery->where('EnterOn', '<=', $EndDate);
		*/

		$data = $this->commonpaginationdata();
		$data['slip'] = $query->skip($data['start'])
			->take($data['per_page'])
			->orderBy('_id', 'desc')
			->get();

		$package_array = [];
        foreach ($data['slip'] as $key) {
            $package_array[] = trim($key->PackageNumber);
        }

        $user_ids = Deliveryrequest::whereIN('PackageNumber',$package_array)->select('RequesterId','PackageNumber')->get();
        foreach ($data['slip'] as $key) {
           $key->UserId = (string) $this->getUserId($key->PackageNumber,$user_ids);
        }

		/*echo '<pre>';
        print_r($data['slip']);die;*/

		$data['count'] = $countQuery->count();

		$data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;

		return view('Admin::list.pagination.slip_list', $data);
	}


	public function getUserId($pn,$array){
        $reqid = '';
        foreach ($array as $key) {
            if(trim($key->PackageNumber) == $pn){
                $reqid = $key->RequesterId;
            }
        }
        return $reqid;

    }

	/*
		    Function  : get package id .
		    Developer : Om Dangi
		    Created At: 07/02/2018
		    Metod     : get
	*/

	public function slipOrderId() {

		$data = Deliveryrequest::where('RequesterId', '=',New MongoId(trim(Input::get('name'))))
			/*->orderby('date', 'desc')*/
			->orderBy('EnterOn', 'desc')
			->get(['PackageNumber']);

		echo json_encode($data);
	}

	/*
		    Function  : uses for get Slip.
		    Developer : Om Dangi
		    Created At: 07/02/2018
		    Metod     : get
	*/
	public function getSlip() {
		//$data['res'] = Deliveryrequest::where('RequesterName', '!=', '')
			//->groupBy(trim('RequesterName'))
			//->get(['RequesterName']);

		$request_users = Deliveryrequest::select('RequesterId')->get();
        $users_array=[];
        foreach ($request_users as $key ) {
            $users_array[]=$key->RequesterId;
        }
        $data['res'] = User::whereIn('_id',$users_array)->select('Name')->get();

		$data['packageNum'] = [];
		if (@Input::get('RequesterName')) {
			$data['packageNum'] = Deliveryrequest::where('RequesterId', '=',new MongoId( trim(Input::get('RequesterName'))))
				->orderby('PackageNumber', 'desc')
				->get(['PackageNumber']);
		}

		$data['title'] = 'Delivery List';

		$data['info'] = Deliveryrequest::where('RequesterId', '=',new MongoId( trim(Input::get('RequesterName'))))
			->where('PackageNumber', '=', trim(Input::get('orderId')))
			->first();
		if (count($data['info']) > 0) {
			if (@Input::get('slip_id')) {

				$slip = SlipManagement::where('_id', '=', new MongoId(Input::get('slip_id')))
					->first();
				$data['info']['ProductList'] = $slip['ProductList'];
				$data['info']['dispatchDate'] = $slip['dispatchDate'];
				$data['info']['deliveryMethod'] = $slip['deliveryMethod'];
				$data['info']['packedBy'] = $slip['packedBy'];
				$data['info']['deliveredBy'] = $slip['deliveredBy'];
				$data['info']['signature'] = $slip['signature'];

			}

			$data['DeliveryNote'] = Comment::where('request_id', '=', (string) $data['info']->RequesterId)->count();
			return view('Admin::list.slip_management', $data);

		} else {
			return view('Admin::list.slip_management', $data);
		}
	}

	/*
		    Function  : uses for create slip in pdf.
		    Developer : Om Dangi
		    Created At: 08/02/2018
		    Metod     : post
	*/
	public function createSlip() {

		$insertData = [];
		$requester_data = User::where(['_id'=> trim(Input::get('RequesterName'))])->select('Name')->first();
        $insertData['RequesterName'] = @$requester_data->Name;
		
		$insertData['PackageNumber'] = Input::get('orderId');

		$Date = date('Y-m-d H:i', (strtotime(Input::get('dispatchDate'))));

		// print_r($Date);die;
		$dispatchDate = new MongoDate(strtotime($Date));

		$insertData['dispatchDate'] = $dispatchDate;
		$insertData['deliveryMethod'] = Input::get('deliveryMethod');
		$insertData['type'] = 'delivery';
		$insertData['EnterOn'] = new MongoDate();

		$insertData['packedBy'] = (@Input::get('packedBy')) ? Input::get('packedBy') : '';
		$insertData['deliveredBy'] = (@Input::get('deliveredBy')) ? Input::get('deliveredBy') : '';
		$insertData['signature'] = (@Input::get('signature')) ? Input::get('signature') : '';

		for ($i = 0; $i < count(Input::get('package_id')); $i++) {

			$orderData['package_id'] = Input::get('package_id')[$i];
			$orderData['product_name'] = Input::get('product_name')[$i];
			$orderData['qty'] = Input::get('Ordered')[$i];
			$orderData['Delivered'] = Input::get('Delivered')[$i];
			$orderData['Outstanding'] = Input::get('Outstanding')[$i];
			$orderData['weight'] = Input::get('weight')[$i];
			$orderData['weight_unit'] = Input::get('weight_unit')[$i];

			$data2[$i] = $orderData;
		}
		if (@$data2) {
			$insertData['ProductList'] = $data2;
			/*====if uses for already created slip ====*/
			if (@Input::get('slip_id')) {
				SlipManagement::where('_id', '=', new MongoId(Input::get('slip_id')))
					->update(
						['dispatchDate' => $dispatchDate, 'deliveryMethod' => Input::get('deliveryMethod'), 'packedBy' => $insertData['packedBy'], 'deliveredBy' => $insertData['deliveredBy'], 'signature' => $insertData['signature'], 'ProductList' => $data2,
						]);
			} else {
				SlipManagement::insertGetId($insertData);
			}

			$data['info'] = Deliveryrequest::where('RequesterId', '=', new MongoId(trim(Input::get('RequesterName')))) 
				->where('PackageNumber', '=', trim(Input::get('orderId')))
				->first();
			$data['info']['dispatchDate'] = $dispatchDate;
			$data['info']['deliveryMethod'] = Input::get('deliveryMethod');
			$data['info']['packedBy'] = (@Input::get('packedBy')) ? Input::get('packedBy') : '';
			$data['info']['deliveredBy'] = (@Input::get('deliveredBy')) ? Input::get('deliveredBy') : '';
			$data['info']['signature'] = (@Input::get('signature')) ? Input::get('signature') : '';

			$data['info']['ProductList2'] = $data2;

			$data['info']['DeliveryNote'] = Comment::where('request_id', '=', (string) $data['info']->RequesterId)->count();

			// echo '<pre>'; print_r($data);

			//pdf create
			$pdf = PDF::loadView('Admin::list.slip_pdf', $data);
			$pdf->setPaper('a4');
			//  $pdf->setPaperNumber(1);
			return $pdf->stream('slip.pdf');

		} else {
			$request_users = Deliveryrequest::select('RequesterId')->get();
        	$users_array=[];
	        foreach ($request_users as $key ) {
	            $users_array[]=$key->RequesterId;
	        }
        	$data['res'] = User::whereIn('_id',$users_array)->select('Name')->get();
			$data['title'] = 'Delivery List';
			//$data['res'] = Deliveryrequest::where('RequesterName', '!=', '')
				//->groupBy('RequesterName')
				//->get(['RequesterName']);
			return view('Admin::list.slip_management', $data);
		}

	}

	/*
		    Function  : Uses for packing slip list.
		    Developer : Om Dangi
		    Created At: 13/02/2018
		    Metod     : Post
	*/

	public function packingList() {

		$data['title'] = "Packing List";
		$data['paginationurl'] = "admin/pagination/packing_list";
		$data['orderType'] = "Desc";
		$data['orderby'] = "_id";
		$data['postvalue'] = "&search_value=" . Input::get('search_value')."&StartDate=".Input::get('StartDate');
		return view('Admin::list.packing_slip_list', $data);

	}

	/*
		    Function  : Uses for pagination slip list.
		    Developer : Om Dangi
		    Created At:13/02/2018
		    Metod     : Post
	*/

	public function pagination_packing_list() {

		$query = SlipManagement::query();
		$countQuery = SlipManagement::query();

		$query->where('type', '=', 'packing');
		$countQuery->where('type', '=', 'packing');

		$data = $this->commonpaginationdata();

		$search_value = Input::get('search_value');

		if ($search_value != '') {
			//$query->where('PackageNumber', 'like', "%$search_value%");
			//$countQuery->where('PackageNumber', 'like', "%$search_value%");
			$query->where(function ($query) use ($search_value) {
                $query->where('PackageNumber', 'like', "%$search_value%")->orwhere('RequesterName', 'like', "%$search_value%");
                //$countQuery->where('PackageNumber', 'like', "%$search_value%");
            });
		}
		if (Input::get('StartDate') != '') {
            $startdate = Input::get('StartDate');
            
            $query->where('EnterOn','>=', new MongoDate(strtotime(DateTime::createFromFormat('M d,Y', $startdate)->format('d-m-Y'))));
        }
		$data['slip'] = $query->skip($data['start'])->take($data['per_page'])->orderBy('_id', 'desc')->get();
		$package_array = [];
        foreach ($data['slip'] as $key) {
            $package_array[] = trim($key->PackageNumber);
        }

        $user_ids = Deliveryrequest::whereIN('PackageNumber',$package_array)->select('RequesterId','PackageNumber')->get();
        foreach ($data['slip'] as $key) {
           $key->UserId = (string) $this->getUserId($key->PackageNumber,$user_ids);
        }
		$data['count'] = $countQuery->count();

		$data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;
		return view('Admin::list.pagination.packing_slip_list', $data);
	}

	/*
		    Function  : uses for get Slip.
		    Developer : Om Dangi
		    Created At: 13/02/2018
		    Metod     : get
	*/
	public function getPackingSlip() {

		/*$data['users'] = Deliveryrequest::where('RequesterName', '!=', '')
			->groupBy('RequesterName')
			->orderby('RequesterName', 'Asc')
			->get(['RequesterName']);*/
		$request_users = Deliveryrequest::select('RequesterId')->get();
        $users_array=[];
        foreach ($request_users as $key ) {
            $users_array[]=$key->RequesterId;
        }
		$data['users'] =User::whereIn('_id',$users_array)->select('Name')->get();
		//echo '<pre>';
		//print_r($data['users']); die;

		/*$data['res'] = Deliveryrequest::where('RequesterName', '!=', '')
            ->groupBy('RequesterName')->limit(50)
            ->get(['RequesterName']);*/
		//$data['res'] = User::where(['delete_status'=>'no'])->orderby('FirstName', 'Asc')->get(['FirstName']);
		//print_r(count($data['res'])); die;

		$data['packageNum'] = [];
		if (@Input::get('RequesterName')) {
			$data['packageNum'] = Deliveryrequest::where('RequesterId', '=',New MongoId( trim(Input::get('RequesterName'))))
				//->orderby('date', 'desc')
				->orderBy('EnterOn', 'desc')
				->get(['PackageNumber']);
			$data['info'] = Deliveryrequest::where('RequesterId', '=', New MongoId(trim(Input::get('RequesterName'))))
			->where('PackageNumber', '=', trim(Input::get('orderId')))
			->first();
		}

		$data['title'] = 'Packing List';

		

		//for received date
		//if (@$data['info']['ProductList'] && @$data['info']['ProductList']['package_id']) {
		if (@$data['info']['ProductList']) {

			foreach ($data['info']['ProductList'] as $ProductList) {

				$resDate = Activitylog::where(['package_id' => @$ProductList['package_id'], 'status' => 'item_received'])->first(['EnterOn']);

				if (count($resDate) > 0) {
					$ProductList['ReceivedDate'] = $resDate->EnterOn;
				}
				$resArray[] = $ProductList;

			}
			$data['info']['ProductList'] = $resArray;
		}

		//echo '<pre>';
		//print_r($data['info']['ProductList']);die;

		if (count(@$data['info']) > 0) {
			if (@Input::get('slip_id')) {

				$slip = SlipManagement::where('_id', '=', new MongoId(Input::get('slip_id')))
					->first();

				$data['info']['ProductList'] = $slip['ProductList'];

				$data['info']['PackingSlip'] = $slip['PackingSlip'];
				$data['info']['ShippingDate'] = $slip['ShippingDate'];
				$data['info']['ArrivalDate'] = $slip['ArrivalDate'];
				$data['info']['paidStatus'] = $slip['paidStatus'];

				$data['info']['address'] = $slip['address'];
				$data['info']['comments'] = $slip['comments'];
			}

			$data['DeliveryNote'] = Comment::where('request_id', '=', (string) $data['info']->RequesterId)->count();

			return view('Admin::list.packing_slip_get', $data);

		} else {
			return view('Admin::list.packing_slip_get', $data);
		}
	}

	/*
		    Function  : uses for create slip in pdf.
		    Developer : Om Dangi
		    Created At: 08/02/2018
		    Metod     : post
	*/
	public function createPackingSlip() {

		$insertData = [];
		$requester_data = User::where(['_id'=> trim(Input::get('RequesterName'))])->select('Name')->first();
        $insertData['RequesterName'] = @$requester_data->Name;
		
		$insertData['PackageNumber'] = Input::get('orderId');

		$insertData['paidStatus'] = Input::get('paidStatus');

		$Date = date('Y-m-d H:i', (strtotime(Input::get('PackingSlip'))));
		$PackingSlip = new MongoDate(strtotime($Date));
		$insertData['PackingSlip'] = $PackingSlip;

		$Date = date('Y-m-d H:i', (strtotime(Input::get('ShippingDate'))));
		$ShippingDate = new MongoDate(strtotime($Date));
		$insertData['ShippingDate'] = $ShippingDate;

		$Date = date('Y-m-d H:i', (strtotime(Input::get('ArrivalDate'))));
		$ArrivalDate = new MongoDate(strtotime($Date));
		$insertData['ArrivalDate'] = $ArrivalDate;

		$insertData['type'] = 'packing';
		$insertData['address'] = Input::get('addressVal');
		$insertData['comments'] = Input::get('Comments');
		$insertData['EnterOn'] = new MongoDate();

		for ($i = 0; $i < count(Input::get('package_id')); $i++) {

			$orderData['package_id'] = Input::get('package_id')[$i];
			$orderData['product_name'] = Input::get('product_name')[$i];
			$orderData['qty'] = Input::get('Ordered')[$i];
			$orderData['Delivered'] = Input::get('Delivered')[$i];
			$orderData['weight'] = Input::get('weight')[$i];
			$orderData['weight_unit'] = Input::get('weight_unit')[$i];

			$orderData['ReceivedDate'] = '';
			if (@Input::get('ReceivedDate')[$i] != '') {
				$Date = date('Y-m-d H:i', (strtotime(Input::get('ReceivedDate')[$i])));
				$ReceivedDate = new MongoDate(strtotime($Date));
				$orderData['ReceivedDate'] = $ReceivedDate;
			}

			$data2[$i] = $orderData;
		}
		if (@$data2) {
			$insertData['ProductList'] = $data2;
			/*====if uses for already created slip ====*/
			if (@Input::get('slip_id')) {
				SlipManagement::where('_id', '=', new MongoId(Input::get('slip_id')))
					->update(['paidStatus' => Input::get('paidStatus'), 'PackingSlip' => $PackingSlip, 'ShippingDate' => $ShippingDate, 'ArrivalDate' => $ArrivalDate, 'address' => $insertData['address'], 'comments' => $insertData['comments'], 'ProductList' => $data2]);
			} else {
				SlipManagement::insertGetId($insertData);
			}

			//print_r(Input::get('RequesterName')); die;

			$data['info'] = Deliveryrequest::where('RequesterId', '=', new MongoId(trim(Input::get('RequesterName'))))
				->where('PackageNumber', '=', trim(Input::get('orderId')))
				->first();

			$data['info']['PackingSlip'] = $PackingSlip;
			$data['info']['ShippingDate'] = $ShippingDate;
			$data['info']['ArrivalDate'] = $ArrivalDate;
			$data['info']['paidStatus'] = Input::get('paidStatus');

			$data['info']['address'] = $insertData['address'];
			$data['info']['comments'] = $insertData['comments'];

			$data['info']['ProductList'] = $data2;

			$data['info']['DeliveryNote'] = Comment::where('request_id', '=', (string) $data['info']->RequesterId)->count();

			$pdf = PDF::loadView('Admin::list.packing_slip_pdf', $data);
			$pdf->setPaper('a4');
			return $pdf->stream('packing_slip.pdf');

		} else {
			$data['title'] = 'packing List';
			$data['res'] = Deliveryrequest::where('RequesterName', '!=', '')
				->groupBy('RequesterName')
				->get(['RequesterName']);
			return view('Admin::list.packing_slip_get', $data);
		}

	}

	/*
		    Function  :  User For create pagination.
		    Developer : Om Dangi
		    Created At: 08/02/2018
	*/
	public function commonpaginationdata() {
		//print_r(Auth::user()->PerPage); die;
		if (Auth::user()->PerPage == '') {
			Auth::user()->PerPage = 20;
		}

		$data['cur_page'] = $data['page'] = Input::get('page');
		$data['page'] -= 1;
		$data['per_page'] = Auth::user()->PerPage; //Input::get('Pnum');
		$data['orderby'] = Input::get('orderby');
		$data['orderType'] = Input::get('orderType');
		$data['previous_btn'] = true;
		$data['next_btn'] = true;
		$data['first_btn'] = true;
		$data['last_btn'] = true;
		$data['sno'] = ($data['start'] = $data['page'] * $data['per_page']) + 1;

		return $data;
	}

	/*
		    Function  : Uses for packing slip list delete.
		    Developer : Om Dangi
		    Created At: 13/02/2018
		    Metod     : Post
	*/

	public function deletePackingSlip() {

		$id = trim(Input::get('slip_id'));
		if (@$id) {
			$res = SlipManagement::find($id);
			$del = $res->delete();
			if ($del) {
				return Redirect::back()->withSuccess('Record has been deleted successfully.');
			} else {
				return Redirect::back()->withFial('Oops! Something went wrong..');
			}
		} else {
			return Redirect::back()->withFial('Oops! Something went wrong..');
		}

	}

	public function getPackingSlip2()
    {

        
        $data['res'] = Deliveryrequest::where('RequesterName', '!=', '')
            ->groupBy('RequesterName')->limit(50)
            ->get(['RequesterName']);

        //$data['res'] = User::where(['delete_status'=>'no'])->get(['Name']);
        return view('Admin::list.packing_slip_get2', $data);

        /*$data['packageNum'] = [];
        if (@Input::get('RequesterName')) {
            $data['packageNum'] = Deliveryrequest::where('RequesterName', '=', trim(Input::get('RequesterName')))
                ->orderby('date', 'desc')
                ->get(['PackageNumber']);
        }

        $data['title'] = 'Packing List';

        $data['info'] = Deliveryrequest::where('RequesterName', '=', trim(Input::get('RequesterName')))
            ->where('PackageNumber', '=', trim(Input::get('orderId')))
            ->first();*/

        //for received date
        //if (@$data['info']['ProductList'] && @$data['info']['ProductList']['package_id']) {
        /*if (@$data['info']['ProductList']) {

            foreach ($data['info']['ProductList'] as $ProductList) {

                $resDate = Activitylog::where(['package_id' => @$ProductList['package_id'], 'status' => 'item_received'])->first(['EnterOn']);

                if (count($resDate) > 0) {
                    $ProductList['ReceivedDate'] = $resDate->EnterOn;
                }
                $resArray[] = $ProductList;

            }
            $data['info']['ProductList'] = $resArray;
        }*/

        //echo '<pre>';
        //print_r($data['info']['ProductList']);die;

       /* if (count($data['info']) > 0) {
            if (@Input::get('slip_id')) {

                $slip = SlipManagement::where('_id', '=', new MongoId(Input::get('slip_id')))
                    ->first();

                $data['info']['ProductList'] = $slip['ProductList'];

                $data['info']['PackingSlip'] = $slip['PackingSlip'];
                $data['info']['ShippingDate'] = $slip['ShippingDate'];
                $data['info']['ArrivalDate'] = $slip['ArrivalDate'];
                $data['info']['paidStatus'] = $slip['paidStatus'];

                $data['info']['address'] = $slip['address'];
                $data['info']['comments'] = $slip['comments'];
            }

            $data['DeliveryNote'] = Comment::where('request_id', '=', (string) $data['info']->RequesterId)->count();

            return view('Admin::list.packing_slip_get2', $data);

        } else {
            
        }*/
    }

}
