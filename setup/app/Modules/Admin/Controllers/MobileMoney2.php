<?php namespace App\Modules\Admin\Controllers;

/*
 * Project: Aquantuo
 *
 */
use App\Http\Controllers\Controller;
use App\Http\Models\CityStateCountry;
use App\Http\Models\Deliveryrequest;
use App\Http\Models\Mobilemoney;
use App\Http\Models\Transaction;
use App\Http\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Input;
use MongoDate;
use MongoId;
use Session;

class MobileMoney2 extends Controller {

	public function getMobileno($id) {
		$data['req_amount'] = Deliveryrequest::where('_id', '=', $id)->select('_id', 'TotalCost', 'PackageNumber', 'RequesterName', 'ProductTitle')->first();
		$data['mobileMoney'] = Mobilemoney::where('user_id', '=', Session::get('adminUserId'))->get();
		return view('Admin::Buy.mobile_list', $data);
	}

	public function addMobileNumber($id, Request $request) {

		$validation = Validator::make($request->all(), [
			'country_code' => 'required',
			'mobile_number' => 'required',
			'type' => 'required',
		]);

		if ($validation->fails()) {
			return Redirect::to('admin/pay-mobile-money/' . $id)->with(['danger' => 'Fail! Your mobile number has been already registred with aquantuo.']);
		} else {

			$insertData = [
				'user_id' => Session::get('adminUserId'),
				'country_code' => Input::get('country_code'),
				'mobile_number' => Input::get('mobile_number'),
				'alternet_moblie' => substr(Input::get('mobile_number'), -4),
				'mobile_type' => Input::get('type'),
				'EnterOn' => new MongoDate(),
			];

			$data = Mobilemoney::insert($insertData);
			if (count($data) > 0) {
				return redirect('admin/pay-mobile-money/' . $id)->withSuccess('Success! Mobile number has been added successfully.');
			} else {
				return redirect()->back()->with('danger', 'Something went wrong.');
			}
		}
	}

	public function deleteMobile($id, $reqid) {
		$data = Mobilemoney::where('_id', '=', $id)->delete();
		if (count($data) > 0) {
			return redirect('admin/pay-mobile-money/' . $reqid)->withSuccess('Success! Mobile number has been Deleted successfully.');
		} else {
			return redirect()->back()->with('danger', 'Something went wrong.');
		}
	}

	public function mobilePayment($reqid, $id) {

		$reqresponce = Deliveryrequest::where('_id', '=', $reqid)->first();
		$userres = User::where('_id', '=', $reqresponce->RequesterId)->first();
		$mobileResponce = Mobilemoney::where('_id', '=', $id)->first();

		$CityStateCountry = CityStateCountry::where(['type' => 'Country', 'Content' => 'Ghana'])->select('Content', 'CurrencyCode', 'CurrencySymbol', 'CurrencyRate')->first();

		if ($reqresponce['need_to_pay'] > 0) {
			$initialtotal = $reqresponce['need_to_pay'];
		} else {
			$initialtotal = $reqresponce['TotalCost'];
		}

		if ($mobileResponce['country_code'] == '+233') {
			$totalamount = $CityStateCountry['CurrencyRate'] * $initialtotal;
			$CurrencyRate = $CityStateCountry['CurrencyRate'];
			$Currency = 'GHS';
		} else {
			$totalamount = $initialtotal;
			$CurrencyRate = 1;
			$Currency = 'USD';
		}

		$url = "https://community.ipaygh.com/v1/mobile_agents_v2";

		$date = getdate();

		$invoiceId = $date[0];

		$body = [
			'invoice_id' => $invoiceId,
			'total' => $totalamount,
			'currency' => $Currency,
			//'pymt_instrument' => $mobileResponce['country_code'] . $mobileResponce['mobile_number'],
			'pymt_instrument' => $mobileResponce['mobile_number'],
			'ipn_url' => 'https://aquantuo.com/api/v7/retrievePayment',
			'wallet_issuer_hint' => $mobileResponce['mobile_type'],
			'extra_name' => $reqresponce['RequesterName'],
			'extra_mobile' => $mobileResponce['mobile_number'],
			'extra_email' => $userres['Email'],
			'description' => $reqresponce['PackageNumber'],
			'buyer_email' => $userres['Email'],
			'buyer_lastname' => $reqresponce['RequesterName'],
			'buyer_phone' => $mobileResponce['mobile_number'],
			'voucher_code' => '',
			'merchant_key' => '3f3f665c-f170-11e7-9871-f23c9170642f',
		];

		$client = new \GuzzleHttp\Client();
		$response = $client->request('POST', $url, ['form_params' => $body]);
		$responser_from_api = $response->getBody();

		$response_in_array = json_decode($responser_from_api);
		if ($response_in_array->success == 1) {

			if (isset($reqresponce->mobilemoney)) {
				$mobilemoney2 = [
					'_id' => (string) new MongoId(),
					'country_code' => $mobileResponce['country_code'],
					'mobile_number_money' => $mobileResponce['mobile_number'],
					'mobile_money_status' => 'awaiting_payment',
					'CurrencyRate' => $CurrencyRate,
					'mobile_money_amount' => $totalamount,
					'alternet_moblie' => $mobileResponce['alternet_moblie'],
					'mobile_moneyId' => $invoiceId,
					'mobile_type' => $mobileResponce['mobile_type'],
				];

				Deliveryrequest::where(['_id' => trim($reqid)])->push(['mobilemoney' => $mobilemoney2]);
			} else {
				$mobilemoney[] = [
					'_id' => (string) new MongoId(),
					'country_code' => $mobileResponce['country_code'],
					'mobile_number_money' => $mobileResponce['mobile_number'],
					'mobile_money_status' => 'awaiting_payment',
					'CurrencyRate' => $CurrencyRate,
					'mobile_money_amount' => $totalamount,
					'alternet_moblie' => $mobileResponce['alternet_moblie'],
					'mobile_moneyId' => $invoiceId,
					'mobile_type' => $mobileResponce['mobile_type'],
				];

				if ($reqresponce['Status'] == 'pending') {
					Deliveryrequest::where('_id', '=', $reqid)->update([
						'Status' => 'momo_initiated',
						'PaymentDate' => new MongoDate(),
						'mobilemoney' => $mobilemoney,
					]);
				}
			}

			if ($reqresponce['Status'] == 'pending') {
				if (isset($reqresponce->ProductList)) {
					$p_array = $reqresponce->ProductList;
					foreach ($p_array as $key => $value) {
						$p_array[$key]['status'] = 'momo_initiated';
						$p_array[$key]['PaymentStatus'] = 'yes';
					}
					$update['ProductList'] = $p_array;
				}
			}
			$update['need_to_pay'] = 0;
			$finalresult = Deliveryrequest::where('_id', '=', $reqid)->update($update);

			/*Transaction*/
			Transaction::insert(array(
				'momo_id' => (int) $invoiceId,
				"SendById" => (String) Session::get('UserId'),
				"SendByName" => @$reqresponce['RequesterName'],
				"SendToId" => "aquantuo",
				"RecieveByName" => "Aquantuo",
				"Description" => "Amount deposited for Mobile Money product PackageId:" . $reqresponce['PackageNumber'],
				"Credit" => floatval($totalamount),
				"Debit" => "",
				"Status" => "pending",
				"TransactionType" => $reqresponce['RequestType'],
				"EnterOn" => new MongoDate(),
			));
			/*End Transaction*/

			if ($finalresult) {
				if ($reqresponce['RequestType'] == 'delivery') {
					return redirect('admin/package/detail/' . $reqid)->withSuccess('Success! Payment has been requested. Please check your mobile device to approve/confirm payment.');
				} else if ($reqresponce['RequestType'] == 'online') {
					return redirect('admin/online_package/detail/' . $reqid)->withSuccess('Success! Payment has been requested. Please check your mobile device to approve/confirm payment.');
				} else if ($reqresponce['RequestType'] == 'buy_for_me') {
					return redirect('admin/buy-for-me/detail/' . $reqid)->withSuccess('Success! Payment has been requested. Please check your mobile device to approve/confirm payment.');
				}
			} else {
				return Redirect::to('admin/pay-mobile-money/' . $reqid)->with(['danger' => 'Fail!  Something went wrong.']);
			}
		} else {
			return Redirect::to('admin/pay-mobile-money/' . $reqid)->with(['danger' => 'Fail!  Something went wrong.']);
		}
	}

	public function vouchermobilePayment($reqid) {
		$reqresponce = Deliveryrequest::where('_id', '=', $reqid)->first();
		$userres = User::where('_id', '=', $reqresponce->RequesterId)->first();
		$mobileResponce = Mobilemoney::where('_id', '=', Input::get('voucherid'))->first();

		$CityStateCountry = CityStateCountry::where(['type' => 'Country', 'Content' => 'Ghana'])->select('Content', 'CurrencyCode', 'CurrencySymbol', 'CurrencyRate')->first();
		if ($reqresponce['need_to_pay'] > 0) {
			$initialtotal = $reqresponce['need_to_pay'];
		} else {
			$initialtotal = $reqresponce['TotalCost'];
		}

		if ($mobileResponce['country_code'] == '+233') {
			$totalamount = $CityStateCountry['CurrencyRate'] * $initialtotal;
			$CurrencyRate = $CityStateCountry['CurrencyRate'];
			$Currency = 'GHS';
		} else {
			$totalamount = $initialtotal;
			$CurrencyRate = 1;
			$Currency = 'USD';
		}

		$url = "https://community.ipaygh.com/v1/mobile_agents_v2";

		$date = getdate();
		$invoiceId = $date[0];
		$body = [
			'invoice_id' => $invoiceId,
			'total' => $totalamount,
			'currency' => $Currency,
			//'pymt_instrument' => $mobileResponce['country_code'] . $mobileResponce['mobile_number'],
			'pymt_instrument' => $mobileResponce['mobile_number'],
			'wallet_issuer_hint' => $mobileResponce['mobile_type'],
			'extra_name' => $reqresponce['RequesterName'],
			'ipn_url' => 'https://aquantuo.com/api/v7/retrievePayment',
			'extra_mobile' => $mobileResponce['mobile_number'],
			'extra_email' => $userres['Email'],
			'description' => $reqresponce['PackageNumber'],
			'voucher_code' => Input::get('Voucher'),
			'merchant_key' => '3f3f665c-f170-11e7-9871-f23c9170642f',
		];

		$client = new \GuzzleHttp\Client();
		$response = $client->request('POST', $url, ['form_params' => $body]);
		$responser_from_api = $response->getBody();

		$response_in_array = json_decode($responser_from_api);
		if ($response_in_array->success == 1) {

			if (isset($reqresponce->mobilemoney)) {
				$mobilemoney2 = [
					'_id' => (string) new MongoId(),
					'country_code' => $mobileResponce['country_code'],
					'mobile_number_money' => $mobileResponce['mobile_number'],
					'mobile_money_status' => 'paid',
					'CurrencyRate' => $CurrencyRate,
					'mobile_money_amount' => $totalamount,
					'alternet_moblie' => $mobileResponce['alternet_moblie'],
					'mobile_moneyId' => $invoiceId,
					'mobile_type' => $mobileResponce['mobile_type'],
				];

				Deliveryrequest::where(['_id' => trim($reqid)])->push(['mobilemoney' => $mobilemoney2]);
			} else {
				$mobilemoney[] = [
					'_id' => (string) new MongoId(),
					'country_code' => $mobileResponce['country_code'],
					'mobile_number_money' => $mobileResponce['mobile_number'],
					'mobile_money_status' => 'paid',
					'CurrencyRate' => $CurrencyRate,
					'mobile_money_amount' => $totalamount,
					'alternet_moblie' => $mobileResponce['alternet_moblie'],
					'mobile_moneyId' => $invoiceId,
					'mobile_type' => $mobileResponce['mobile_type'],
				];

				if ($reqresponce['Status'] == 'pending') {
					$updateData = Deliveryrequest::where('_id', '=', $reqid)->update([
						'PaymentStatus' => 'capture',
						'Status' => 'ready',
						'PaymentDate' => new MongoDate(),
						'mobilemoney' => $mobilemoney,
					]);
				}

			}

			Transaction::insert(array(
				'momo_id' => (int) $invoiceId,
				"SendById" => (String) Session::get('UserId'),
				"SendByName" => @$reqresponce['RequesterName'],
				"SendToId" => "aquantuo",
				"RecieveByName" => "Aquantuo",
				"Description" => "Amount deposited for Mobile Money product PackageId:" . $reqresponce['PackageNumber'],
				"Credit" => floatval($totalamount),
				"Debit" => "",
				"Status" => "pending",
				"TransactionType" => $reqresponce['RequestType'],
				"EnterOn" => new MongoDate(),
			));

			if ($reqresponce['Status'] == 'pending') {
				if (isset($reqresponce->ProductList)) {
					$p_array = $reqresponce->ProductList;
					foreach ($p_array as $key => $value) {
						$p_array[$key]['status'] = 'ready';
						$p_array[$key]['PaymentStatus'] = 'yes';
					}
					$update['ProductList'] = $p_array;
				}
			}

			$update['need_to_pay'] = 0;
			$finalresult = Deliveryrequest::where('_id', '=', $reqid)->update($update);
			if ($finalresult) {
				if ($reqresponce['RequestType'] == 'delivery') {
					return redirect('admin/package/detail/' . $reqid)->withSuccess('Success! Payment has been requested. Please check your mobile device to approve/confirm payment.');
				} else if ($reqresponce['RequestType'] == 'online') {
					return redirect('admin/online_package/detail/' . $reqid)->withSuccess('Success! Payment has been requested. Please check your mobile device to approve/confirm payment.');
				} else if ($reqresponce['RequestType'] == 'buy_for_me') {
					return redirect('admin/buy-for-me/detail/' . $reqid)->withSuccess('Success! Payment has been requested. Please check your mobile device to approve/confirm payment.');
				}
			} else {
				return Redirect::to('admin/pay-mobile-money/' . $reqid)->with(['danger' => 'Fail!  Something went wrong.']);
			}
		} else {
			return Redirect::to('admin/pay-mobile-money/' . $reqid)->with(['danger' => 'Fail!  Something went wrong.']);
		}
	}

	public function getIpayStatus() {
		$merchant_key = '3f3f665c-f170-11e7-9871-f23c9170642f';
		$id = Input::get('id');
		$msg = '';
		$show_popup = 0;
		$responce = ['success' => 0, 'msg' => 'Oops! Something went wrong.', 'show_popup' => $show_popup, 'invoice' => '', 'status' => ''];
		if ($id != '') {
			$requestInfo = Deliveryrequest::where(['_id' => $id])->first();
			if (count($requestInfo) > 0) {
				if (isset($requestInfo->mobilemoney)) {
					foreach ($requestInfo->mobilemoney as $key) {
						if (isset($key['mobile_moneyId'])) {
							$url = "https://community.ipaygh.com/v1/gateway/status_chk?invoice_id={$key['mobile_moneyId']}&merchant_key={$merchant_key}";
							$ch = curl_init($url);
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
							$output = curl_exec($ch);
							$show_popup = 1;

							if (strpos($output, 'paid') !== false) {
								$msg .= "Status of invoice id {$key['mobile_moneyId']} is paid";
							} else if (strpos($output, 'awaiting_payment') !== false) {
								$msg .= "Status of invoice id {$key['mobile_moneyId']} is awaiting_payment";
							} else if (strpos($output, 'cancelled') !== false) {
								$msg .= "Status of invoice id {$key['mobile_moneyId']} is cancelled";
								$show_popup = 1;
							} else if (strpos($output, 'expired') !== false) {
								$msg .= "Status of invoice id {$key['mobile_moneyId']} is expired";
								$show_popup = 1;
							} else if (strpos($output, 'failed') !== false) {
								$msg .= "Status of invoice id {$key['mobile_moneyId']} is failed";
								$show_popup = 1;
							} else {
								$msg .= "Status of invoice id {$key['mobile_moneyId']} is \n\n {$output}";
								$show_popup = 1;
							}
						}

					}

					$responce = ['success' => 1, 'msg' => '',
						'msgres' => $msg,
						'show_popup' => $show_popup,

					];

				}
			}
		}
		return response()->json($responce);
	}

	public function changeIpayStatus($id) {
		if ($id != '') {
			$requestInfo = Deliveryrequest::where(['_id' => $id, 'Status' => 'momo_initiated'])->first();
			if (count($requestInfo) > 0) {
				$requestInfo->Status = 'pending';
				$ProductList = $requestInfo->ProductList;

				foreach ($requestInfo->ProductList as $key => $value) {
					$ProductList[$key]['status'] = 'pending';
				}
				$requestInfo->ProductList = $ProductList;
				$requestInfo->save();
				return Redirect::back()->withSuccess('Success! Request status has been updated successfully.');
			}
		}
		return Redirect::back();
	}
}
