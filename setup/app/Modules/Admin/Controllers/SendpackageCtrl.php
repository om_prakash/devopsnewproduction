<?php
namespace App\Modules\Admin\Controllers;

/*
 * Project: Aquantuo
 */

use App\Http\Controllers\Controller;
use App\Http\Models\Activitylog;
use App\Http\Models\Additem;
use App\Http\Models\Category;
use App\Http\Models\CityStateCountry;
use App\Http\Models\Configuration;
use App\Http\Models\Currency;
use App\Http\Models\Deliveryrequest;
use App\Modules\User\Controllers\NewCalculation;
use App\Http\Models\Notification;
use App\Http\Models\Extraregion;
use App\Http\Models\Setting;
use App\Http\Models\User;
use App\Library\Notify;
use App\Library\Promo;
use App\Library\Reqhelper;
use App\Library\Utility;
use App\Library\upsRate;
use App\Modules\Admin\Models\Admin;
use App\Traits\AdminTrait;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Input;
use MongoDate;
use MongoId;
use Session;
use App\Http\Models\Trips;
use App\Library\NewEmail;

class SendpackageCtrl extends Controller {
	use AdminTrait;

	// calculation part buy for me
	// Start buy for me section

	public function __construct() {
		if (!(Auth::user())) {
			Redirect::to('admin/login')->send();
		}
	}

	public function package() {

		$data['transporter_data'] = array();
		$data['country'] = CityStateCountry::where(['type' => 'Country', 'Status' => 'Active'])
			->orderBy('Content', 'Asc')
			->get(['_id', 'Content', 'state_available']);

		$data['user'] = User::where('delete_status', '=', 'no')->select('_id', 'Name', 'Email')->orderBy('Name', 'asc')->get();

		$data['category'] = Category::where(['type' => 'Category', 'Status' => 'Active'])->whereIn('TravelMode', ['ship', 'air'])->get(['TravelMode', 'Content']);
		
		if(Input::get('listing') != ''){
			if(Input::get('listing') == 'own'){
				$data['items'] = Additem::where(['user_type' => 'admin', 'type' => 'delivery','user_id' => Auth::user()->_id])->get();
			}else if(Input::get('listing') == 'all'){
				$data['items'] = Additem::where(['user_type' => 'admin', 'type' => 'delivery'])->get();
			}else{
				return redirect::to('admin/package');
			}
		}else{
			$data['items'] = Additem::where(['user_type' => 'admin', 'type' => 'delivery'])->get();
		}

		$data['items_count'] = count($data['items']);

		if (count($data['items']) > 0) {
			return view('Admin::sendapackage.prepare-request', $data);
		} else {
			//return view('Admin::sendapackage.prepare_add_item', $data);
			return view('Admin::sendapackage.prepareadditem', $data);
		}
	}

	public function prepareAddItem($id = "") {

		$data['transporter_data'] = array();
		$data['country'] = CityStateCountry::where(['type' => 'Country', 'Status' => 'Active'])
			->orderBy('Content', 'Asc')
			->get(['_id', 'Content', 'state_available']);
		$data['category'] = Category::where(['type' => 'Category', 'Status' => 'Active'])->whereIn('TravelMode', ['ship', 'air'])->get(['TravelMode', 'Content']);
		$data['items_count'] = Additem::where(['user_id' => Session::get('UserId'), 'type' => 'delivery'])->count();

		if ($id != '') {
			return view('Admin::sendapackage.edit.add_item', $data);
		} else {
			return view('Admin::sendapackage.prepareadditem', $data);
		}
	}

	public function editSendPackage($id, $requestid = "") {

		$data['transporter_data'] = array();
		$data['country'] = CityStateCountry::where(['type' => 'Country', 'Status' => 'Active'])
			->orderBy('Content', 'Asc')
			->get(['_id', 'Content', 'state_available']);
		$data['category'] = Category::where(['type' => 'Category', 'Status' => 'Active'])->whereIn('TravelMode', ['ship', 'air'])->get(['TravelMode', 'Content']);
		if ($requestid != '') {
			$data['delivery_data'] = Deliveryrequest::where(['_id' => $requestid])
				->select('ProductList')->first();
		} else {
			$data['delivery_data'] = Additem::where(['user_type' => 'admin', 'type' => 'delivery', '_id' => $id])->first();
		}
		$data['items_count'] = 1;
		if ($data['delivery_data']) {
			if ($requestid != '') {

				$array = [];
				foreach ($data['delivery_data']->ProductList as $key) {
					if ($key['_id'] == $id) {
						$array = [
							"_id" => $key['_id'],
							"product_name" => $key['product_name'],
							"productWidth" => $key['productWidth'],
							"productHeight" => $key['productHeight'],
							"productLength" => $key['productLength'],
							"productCost" => $key['productCost'],
							"productWeight" => $key['productWeight'],
							"productHeightUnit" => $key['productHeightUnit'],
							"ProductWeightUnit" => $key['ProductWeightUnit'],
							"ProductLengthUnit" => $key['ProductLengthUnit'],
							"productCategory" => $key['productCategory'],
							"productCategoryId" => $key['productCategoryId'],
							"travelMode" => $key['travelMode'],
							"InsuranceStatus" => $key['InsuranceStatus'],
							"productQty" => $key['productQty'],
							"ProductImage" => $key['ProductImage'],
							//"OtherImage"=>$key['OtherImage'],
							"QuantityStatus" => $key['QuantityStatus'],
							//        "BoxQuantity" => $key['BoxQuantity'],
							"Description" => $key['Description'],
							"PackageMaterial" => $key['PackageMaterial'],
							"PackageMaterialShipped" => $key['PackageMaterialShipped'],
							"inform_mail_sent" => "no",
						];
					}
				}
				$data['delivery_data'] = $array;
				return view('Admin::sendapackage.edit.edit_item', $data);
			} else {
				return view('Admin::sendapackage.edit_item', $data);
			}

		} else {
			return Redirect::back();
		}
	}

	public function post_package() {
		$response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		$ProductImage = '';
		$otherImages = [];

		if (Input::get('request_id') != '') {
			$item = Additem::where(['_id' => Input::get('request_id')])->first();
			$ProductImage = $item->ProductImage;
			$ProductImage = $item->ProductImage;
			$otherImages = $item->OtherImage;
		}

		if (Input::hasFile('default_image')) {
			if (Input::file('default_image')->isValid()) {
				$ext = Input::file('default_image')->getClientOriginalExtension();
				$ProductImage = time() . rand(100, 9999) . ".$ext";

				if (Input::file('default_image')->move(BASEURL_FILE . 'package/', $ProductImage)) {
					$ProductImage = "package/$ProductImage";
				} else {
					$ProductImage = '';
				}
			}
		}

		$imageArray = array('package_image2', 'package_image3', 'package_image4', 'package_image5');

		foreach ($imageArray as $key => $val) {
			if (Input::hasFile($val)) {
				if (Input::file($val)->isValid()) {
					$ext = Input::file($val)->getClientOriginalExtension();
					$img = time() . rand(100, 9999) . ".$ext";

					if (Input::file($val)->move(BASEURL_FILE . 'package/', $img)) {
						$img = "package/$img";
						if (isset($otherImages[$key])) {
							// Start remove old image
							if (!empty(trim($otherImages[$key]))) {
								if (file_exists(BASEURL_FILE . $otherImages[$key])) {
									unlink(BASEURL_FILE . $otherImages[$key]);
								}
							}
							// End remove old image
							$otherImages[$key] = $img;
						} else {
							$otherImages[] = $img;
						}
					}
				}
			}
		}

		$lhwunit = 'cm';
		$weightunit = 'kg';

		if (Input::get('measurement_unit') == 'inches_lbs') {
			$lhwunit = 'inches';
			$weightunit = 'lbs';
		}

		$category = json_decode(Input::get('category'));
		$add_item = [
			'product_name' => ucfirst(Input::get('package_title')),
			'user_id' => Auth::user()->_id,
			'type' => 'delivery',
			'user_type' => 'admin',
			'productWidth' => Input::get('width'),
			'productHeight' => Input::get('height'),
			'productLength' => Input::get('length'),
			'productCost' => Input::get('package_value'),
			'productWeight' => Input::get('weight'),
			'productHeightUnit' => $lhwunit,
			'ProductWeightUnit' => $weightunit,
			'ProductLengthUnit' => $lhwunit,
			'productCategory' => @$category->name,
			'productCategoryId' => @$category->id,
			'travelMode' => Input::get('travel_mode'),
			'currency' => Input::get('Default_Currency'),
			'needInsurance' => Input::get('insurance'),
			"productQty" => (Input::get('quantity') < 1) ? 1 : Input::get('quantity'),
			'ProductImage' => $ProductImage,
			//'OtherImage'=>$otherImages,
			'ProductTitle' => ucfirst(Input::get('title')),
			"QuantityStatus" => (Input::get('quantity') > 1) ? 'multiple' : 'single',
			"BoxQuantity" => (Input::get('quantity') == 0) ? 1 : (int) Input::get('quantity'),
			"Description" => Input::get('description'),
			"InsuranceStatus" => (Input::get('insurance') == 'yes') ? 'yes' : 'no',
			"PackageMaterial" => (Input::get('need_package_material') == 'yes') ? 'yes' : 'no',
			"PackageMaterialShipped" => '',
			"inform_mail_sent" => 'no',
		];

		if ($add_item['needInsurance'] == 'yes') {
			$insurance = $this->get_insurance($add_item['productCost'], $add_item['productQty']);
			if ($insurance == 0) {
				$response['msg'] = 'Sorry! We are not able to provide insurence.';
				return json_encode($response);
			}
		}

		if (Input::get('request_id') != '') {
			if (Additem::where(['_id' => Input::get('request_id')])->update($add_item)) {
				$response = ['success' => 1, 'msg' => 'Item has been updated successfully.'];
			}
		} else {
			// Stop user to add item with shipping mode sea but different category.
			$is_make_request = "yes";
			$item_with_sea = false;
			$item_with_air = false;

			$shipItemList = Additem::where(array('user_id' => Auth::user()->_id, "type" => "delivery"))->select("productCategoryId", 'travelMode')->get();	// in where clause "travelMode" => "ship",

			foreach ($shipItemList as $value) {
				if ($value->productCategoryId != @$category->id && $value->travelMode == 'ship') {
					$is_make_request = "no";
					$item_with_sea = true;
				}

				if (Input::get('travel_mode') == "ship") {
					$item_with_sea = true;
				}

				if (Input::get('travel_mode') == "air" || $value->travelMode == 'air') {
					$item_with_air = true;
				}

				if ($item_with_sea && $item_with_air) {
					$response["msg"] = "Sorry! You can’t have different shipping modes in the same listing. Please create a separate listing for either air or sea.";
					return response()->json($response);
					die();
				}
			}
			
			if ($is_make_request == "no") {
				$response["msg"] = "If shipping by Sea, all items must be of the same sea category. Please create a new listing if you need to use a different sea category.";
				return response()->json($response);
				die();
			} else {
				if (Additem::insert($add_item)) {
					$response = ["success" => 1, "msg" => "Item has been added successfully."];
				}
			}
		}
		return json_encode($response);
	}

	public function preparecalculation() {
	
		$items = Additem::where(['user_id' => Session::get('UserId'), 'type' => 'delivery'])->get();

		$rate = 0;
		$insurance = 0;
		$weight = 0;
		$volume = 0;
		$distance = $this->get_distance(Input::get('calculated_distance'));
		$drop_lat_long = ['lat' => Input::get('DeliveryLat'), 'lng' => Input::get('DeliveryLong')];

		$distance_cal = Utility::GetDrivingDistance(Input::get('PickupLat'), Input::get('PickupLong'), Input::get('DeliveryLat'), Input::get('DeliveryLong'));
		if ($distance_cal == false) {
			$response['msg'] = 'Oops! We are unable to calculate distance.';
			return json_encode($response);die;
		} else {
			$distance = $this->get_distance($distance_cal->distance);
			$distance2 = $distance_cal->distance;
		}

		$setting = Configuration::where(['_id' => '5673e33e6734c4f874685c84'])->select('accra_charge', 'accra_charge_type')->first();
		$drop_off_state = json_decode(Input::get('state11'));

		$response = ['success' => 0, 'shipping_cost' => '', 'shipping_cost' => '', 'ghana_total_amount' => '', 'formated_currency' => '', 'total_weight' => 0];

		$return_array = [];
		if ($items) {
			foreach ($items as $key) {
				$category = Category::where(['_id' => $key->productCategoryId, 'TravelMode' => $key->travelMode, 'Status' => 'Active'])->first();
				$key->InsuranceCost = 0;
				if ($category) {
					// $weight2 = $this->get_weight($key->productWeight, $key->productWeightUnit);
					$weight2 = $this->GetVolumetricWeightInLbs($key->productWidth, $key->productHeight, $key->productLength, $key->productWeight, $key->productWeightUnit);
					$weight += $weight2;
					if ($key->InsuranceStatus == 'yes') {
						$key->InsuranceCost = $this->get_insurance($key->productCost, $key->productQty);
						$insurance += $key->InsuranceCost;
					}

					$volume += $this->get_volume(array(
						"width" => $key->productWidth,
						"widthunit" => $key->ProductLengthUnit,
						"height" => $key->productHeight,
						"heightunit" => $key->productHeightUnit,
						"length" => $key->productLength,
						"lengthunit" => $key->ProductLengthUnit,
					));

					if (isset($category->Shipping) && is_array($category->Shipping)) {

						foreach ($category->Shipping as $key2) {
							if ($category->ChargeType == 'distance') {

								if ($distance >= $key2['MinDistance'] && $distance <= $key2['MaxDistance']) {
									$key->shippingCost = $key2['Rate'] * (int) $key->productQty;
									$rate += $key2['Rate'] * (int) $key->productQty;
								}
							} else if ($category->ChargeType == 'fixed') {
								if ($weight2 >= $key2['MinDistance'] && $weight2 <= $key2['MaxDistance']) {

									//$return_array['shippingCost'] = $key2['Rate'] * (int) $key->productQty;
									$key->shippingCost = $key2['Rate'] * (int) $key->productQty;
									$rate += $key2['Rate'] * (int) $key->productQty;

								}
							}
						}
					}
				}
			}
		}
		$return_array = $items;
		$response['success'] = 1;

		
		//$insert['needToPay'] = floatval($response['shipping_cost'] + $insurance);



		$response['regionCharges'] = 0;
		$response['shipping_cost'] = $rate;
		$response['insurance'] = $insurance;
		$response['total_amount'] = $response['shipping_cost'] + $insurance;
		$response['total_cost'] = $response['shipping_cost'] + $insurance;
		$response['product'] = $return_array;
		$response['AquantuoFees'] = $this->get_aquantuo_fees($response['total_amount']);
		$response['PaymentStatus'] = '';

		//extraa charge of out side accra
		$response['regionCharges'] = $this->resionCharges(['name' => $drop_off_state->name, 'id' => $drop_off_state->id]);
		$response['total_amount'] = $response['total_amount'] + $response['regionCharges'] + $response['AquantuoFees'];
		//end accra charge

		

		$currency_conversion = $this->currency_conversion($response['total_amount']);

		if (Session()->get('Default_Currency') == 'GHS') {
			$response['user_currency'] = $currency_conversion['ghanian_cost'];
		} else if (Session()->get('Default_Currency') == 'CAD') {
			$response['user_currency'] = $currency_conversion['canadian_cost'];
		} else if (Session()->get('Default_Currency') == 'PHP') {
			$response['user_currency'] = $currency_conversion['philipins_cost'];
		} else if (Session()->get('Default_Currency') == 'GBP') {
			$response['user_currency'] = $currency_conversion['uk_cost'];
		} else {
			$response['user_currency'] = $currency_conversion['ghanian_cost'];
		}

		$response['distance'] = number_format($distance,2);
		$response['volume'] = $volume;

		$response['total_weight'] = number_format($weight,2);
		return json_encode($response);
	}

	public function resionCharges($state) {
		$charges = Extraregion::where(['state_id' => $state['id'], 'status' => 'Active'])->select('amount')->first();
		if ($charges) {
			return $charges->amount;
		} else {
			return 0.00;
		}
	}

	public function get_distance($distance) {
		return floatval($distance) * 0.000621371;
	}

	public function get_weight($weight, $type) {
		$type = strtolower($type);
		if ($type == 'kg') {
			return (((float) $weight) * 2.20462);
		} else if ($type == 'gram') {
			return (((float) $weight) * 0.00220462);
		} else {
			return $weight;
		}
	}

	/*
	 * TODO return Volumetric Weight in lbs
	 * 1 lbs = 2.20462 kg
	 * 1lbs = 0.00220462 gram
	 *
	 */
	public function GetVolumetricWeightInLbs($width, $height, $length, $weight, $type) {
		$VolumetricInfo = [ "length" => $length, "height" => $height, "width" => $width, "type" => $type ];
		$VolumetricWeight = Utility::calculate_volumetric_weight($VolumetricInfo);

		if ($weight > $VolumetricWeight) {
			$VolumetricWeight = $weight;
		}

		$type = strtolower($type);
		if ($type == "kg") {
			$VolumetricWeightLbs = (((float) $VolumetricWeight) * 2.20462);
		} else if ($type == "gram") {
			$VolumetricWeightLbs = (((float) $VolumetricWeight) * 0.00220462);
		} else {
			$VolumetricWeightLbs = $VolumetricWeight;
		}
		return $VolumetricWeightLbs;
	}

	public function get_insurance($cost, $quantity) {
		$data = Configuration::where(array('_id' => '5673e33e6734c4f874685c84'))
			->select('Insurance', 'main_insurance')->first();

		$value = 0;

		if (count($data) > 0) {
			$totalPrice = $cost * (int) $quantity;
            $insurance =  $totalPrice * $data->main_insurance;
            $value =  $insurance / 100;
			/*if (is_array($data->Insurance)) {
				foreach ($data->Insurance as $key) {
					if ($key['MinPrice'] <= (float) $cost && $key['MaxPrice'] >= (float) $cost) {
						$value = $key['Rate'] * (int) $quantity;
					}
				}
			}*/
		}

		return $value;
	}
	public function package_detail($id) {
		$data['request_data'] = Deliveryrequest::where(['_id' => $id])->first();
		if (count($data['request_data']) > 0) {
			$data['total_item'] = 0;
			if (isset($data['request_data']['ProductList'])) {
				foreach ($data['request_data']['ProductList'] as $key) {
					$data['total_item'] = $data['total_item'] + 1;
				}
			}
		}
		return view('User::send_package.requester_package_detail', $data);
	}

	public function get_volume($array) {
		return ($this->get_size_in_cm($array['length'], $array['lengthunit']) * $this->get_size_in_cm($array['width'], $array['widthunit']) * $this->get_size_in_cm($array['height'], $array['heightunit']));
	}

	public function get_size_in_cm($height, $unit) {

		$in_cm = (float) $height;
		$unit = strtolower($unit);
		if ($unit == 'inches') {
			$in_cm = $in_cm * 2.54;
		}
		return $in_cm;

	}

	public function outsideOfAccraCharge($address) {
		$distance = Utility::getDistanceBetweenPointsNew(5.5913754, -0.2499413, $address['lat'], $address['lng']);
		$charge = false;
		if ($distance > 0) {
			$distance_km = $distance / 1000;
			$range = 30;
			if ($distance_km > 30) {
				$charge = true;
			}
		}
		return $charge;
	}

	public function currency_conversion($shipcost) {

		$data = ['cost' => 0, 'ghanian_cost' => 0, 'philipins_cost' => 0, 'uk_cost' => 0, 'kenya_cost' => 0, 'canadian_cost' => 0, "currency_rate" => 0];
		$currency = Currency::where(['Status' => 'Active'])->select('CurrencyRate', 'FormatedText', 'CurrencyCode')->get();

		if ($currency) {
			foreach ($currency as $key) {
				if ($key->CurrencyCode == 'GHS') {
					$data['ghanian_cost'] = str_replace('[AMT]', number_format($shipcost * $key->CurrencyRate, 2), $key->FormatedText);
					$data["currency_rate_ghs"] = $key->CurrencyRate;
					$data["FormatedText_ghs"] = $key->FormatedText;
				} else if ($key->CurrencyCode == 'CAD') {
					$data['canadian_cost'] = str_replace('[AMT]', number_format($shipcost * $key->CurrencyRate, 2), $key->FormatedText);
					$data["currency_rate_cad"] = $key->CurrencyRate;
					$data["FormatedText_cad"] = $key->FormatedText;
				} else if ($key->CurrencyCode == 'PHP') {
					$data['philipins_cost'] = str_replace('[AMT]', number_format($shipcost * $key->CurrencyRate, 2), $key->FormatedText);
					$data["currency_rate_php"] = $key->CurrencyRate;
					$data["FormatedText_php"] = $key->FormatedText;
				} else if ($key->CurrencyCode == 'GBP') {
					$data['uk_cost'] = str_replace('[AMT]', number_format($shipcost * $key->CurrencyRate, 2), $key->FormatedText);
					$data["currency_rate_gbp"] = $key->CurrencyRate;
					$data["FormatedText_gbp"] = $key->FormatedText;
				} else if ($key->CurrencyCode == 'KES') {
					$data['kenya_cost'] = str_replace('[AMT]', number_format($shipcost * $key->CurrencyRate, 2), $key->FormatedText);
					$data["currency_rate_kes"] = $key->CurrencyRate;
					$data["FormatedText_kes"] = $key->FormatedText;
				}
			}
		}
		return $data;

	}

	public function add_item_page($id = "") {

		$data['transporter_data'] = array();
		$data['country'] = CityStateCountry::where(['type' => 'Country', 'Status' => 'Active'])
			->orderBy('Content', 'Asc')
			->get(['_id', 'Content', 'state_available']);
		$data['category'] = Category::where(['type' => 'Category', 'Status' => 'Active'])->whereIn('TravelMode', ['ship', 'air'])->get(['TravelMode', 'Content']);
		$data['items_count'] = Additem::where(['user_id' => Session::get('UserId'), 'type' => 'delivery'])->count();
		if ($id != '') {
			return view('User::send_package.edit.add_item', $data);
		} else {
			return view('User::send_package.prepare_add_item', $data);
		}

	}

	private function get_packageno() {
		return Utility::sequence('Request');
	}

	public function create_prepare_request() {
		$response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];

		if (Input::get('listing') == 'own') {
			$data['items'] = Additem::where([ "user_type" => "admin", 'type' => 'delivery','user_id' => Auth::user()->_id])->get();
		} else {
			$data['items'] = Additem::where([ "user_type" => "admin", 'type' => 'delivery'])->get();
		}

		if (Input::get('user') != '') {
			$json_decode_user = json_decode(Input::get('user'));
			$requesterid = $json_decode_user->id;
			$requestername = $json_decode_user->name;
			$user = $requester = User::where(['_id' => $requesterid])->first();
		} else {
			$email_check = User::where(['Email' => strtolower(trim(Input::get('email')))])->first();
			if (count($email_check) > 0) {
				$response = [
					'success' => 0,
					'msg' => 'This email is already registered with us.',
				];
				return json_encode($response);die;
			} else {
				$pass = rand(100000, 999999);
				$UniqueNo = Input::get('first_name')[0] . Input::get('lastName')[0];
				$insert['First'] = Input::get('first_name');
				$insert['Last'] = Input::get('last_name');
				$insert['Name'] = ucfirst(Input::get('first_name')) . " " . ucfirst(Input::get('last_name'));
				$insert['Email'] = strtolower(trim(Input::get('email')));
				$insert['Password'] = md5($pass);
				$insert['temprary'] = $pass;
				$insert['TransporterType'] = '';
				$insert['UserType'] = 'requester';
				$insert['ChatName'] = "aquantuo" . Utility::sequence('user');
				$insert['CountryCode'] = "";
				$insert['PhoneNo'] = '';
				$insert['AlternateCCode'] = '';
				$insert['AlternatePhoneNo'] = '';
				$insert['RequesterStatus'] = 'active';
				$insert['Age'] = "";
				$insert['SSN'] = "";
				$insert['Street1'] = '';
				$insert['Street2'] = '';
				$insert['Country'] = '';
				$insert['delete_status'] = 'no';
				$insert['State'] = '';
				$insert['City'] = '';
				$insert['ZipCode'] = '';
				$insert['BankName'] = '';
				$insert['AccountHolderName'] = '';
				$insert['BankAccountNo'] = '';
				$insert['RoutingNo'] = '';
				$insert['StripeId'] = '';
				$insert['TransporterStatus'] = "not_registred";
				$insert['UserStatus'] = "active";
				$insert['Image'] = "";
				$insert['IDProof'] = '';
				$insert['LicenceId'] = '';
				$insert['type_of_id'] = '';
				$insert['TPTrackLocation'] = "on";
				$insert['EmailStatus'] = "on";
				$insert['consolidate_item'] = "on";
				$insert['NoficationStatus'] = "on";
				$insert['TPSetting'] = "on";
				$insert['SoundStatus'] = "on";
				$insert['VibrationStatus'] = "on";
				$insert['EnterOn'] = new MongoDate();
				$insert['UpdateOn'] = new MongoDate();
				$insert['RatingCount'] = 0;
				$insert['RatingByCount'] = 0;
				$insert['tpid'] = '';
				$insert['tpName'] = '';
				$insert['CurrentLocation'] = array();
				$insert['DeliveryAreaCountry'] = array();
				$insert['DeliveryAreaState'] = array();
				$insert['DeliveryAreaCities'] = array();
				$insert['ProfileStatus'] = 'step-one';
				$insert['StripeBankId'] = "";
				$insert['NotificationId'] = Input::get('NotificationId');
				$insert['DeviceId'] = Input::get('DeviceId');
				$insert['DeviceType'] = 'web';
				$insert['UniqueNo'] = strtoupper($UniqueNo) . Utility::sequence('user_unique');
				$insert['bank_info'] = [];
				$insert['AqAddress'] = '';
				$insert['AqCity'] = '';
				$insert['AqState'] = '';
				$insert['AqCountry'] = '';
				$insert['AqZipcode'] = '';
				$insert['AqLatLong'] = [];
				$insert['Default_Currency'] = 'USD';
				$insert['PerPage'] = 20;
				$insert['after_update_difference'] = 0;
				$insert['itemCount'] = '';

				// Get aquantuo addres
				$supportemail = Setting::find('563b0e31e4b03271a097e1ca');
				if (count($supportemail) > 0) {
					$insert['AqAddress'] = $supportemail->AqAddress;
					$insert['AqCity'] = $supportemail->AqCity;
					$insert['AqState'] = $supportemail->AqState;
					$insert['AqCountry'] = $supportemail->AqCountry;
					$insert['AqZipcode'] = $supportemail->AqZipcode;
					$insert['AqLatLong'] = $supportemail->AqLatlong;
				}

				$result['_id'] = (string) User::insertGetId($insert);

				$user = $requester = User::where(['_id' => $result['_id']])->first();

				if ($result['_id']) {
					$Email = new NewEmail();
					// email to user
					$Email->send_mail('58e4f6f77ac6f6e92d8b4567', [
						'to' => trim(Input::get('email')),
						'replace' => [
							'[USERNAME]' => $insert['Name'],
							'[PASSWORD]' => $pass,
						],
					]);

					$response = array("success" => 1, "msg" => "Requester has been Registered successfully.");
				}
				$requesterid = $result['_id'];
				$requestername = ucfirst(Input::get('first_name')) . " " . ucfirst(Input::get('last_name'));
			}
		}
		$configurationdata = Configuration::where(['_id' => '5673e33e6734c4f874685c84'])->first();
		//$PackageId = $this->get_packageno();
		$city = json_decode(Input::get('city'));
		$state = json_decode(Input::get('state10'));
		$country = json_decode(Input::get('country'));

		$drop_off_city = json_decode(Input::get('drop_off_city'));
		$drop_off_state = json_decode(Input::get('state11'));
		$drop_off_country = json_decode(Input::get('drop_off_country'));

		$return_city = json_decode(Input::get('return_city'));
		$return_state = json_decode(Input::get('state12'));
		$return_country = json_decode(Input::get('return_country'));

		$nd_return_city = json_decode(Input::get('nd_return_city'));
		$nd_return_state = json_decode(Input::get('state13'));
		$nd_return_country = json_decode(Input::get('nd_return_country'));

		if (count($requester) == 0) {
			return json_encode($response);die;
		}

		$calculated_distance = floatval(Input::get('calculated_distance'));

		if ($calculated_distance > 0) {
			$calculated_distance = $calculated_distance * 0.000621371;
		} else {
			$calculated_distance = Utility::getDistanceBetweenPointsNew(
				floatval(Input::get('PickupLat')),
				floatval(Input::get('PickupLong')),
				floatval(Input::get('DeliveryLat')),
				floatval(Input::get('DeliveryLong'))
			);
		}
		$tripid = $transporterName = $transporterId = '';
		if (Input::get('tripid') != '' && Input::get('transporter_id') != '') {
			$tripCheck = $this->checkTrip(['tripid'=>trim(Input::get('tripid')),'transporter_id'=> (String) trim(Input::get('transporter_id'))]);
			$transporterName = $tripCheck['trname'];
			$tripid = trim(Input::get('tripid'));

			if($tripCheck['success'] == 0){
				$response['msg'] = $tripCheck['msg'];
				return json_encode($response);die;
			}
		}

		if (Input::get('request_id') == '') {
			$PackageId = $this->get_packageno();
			$old_packageNumber = $PackageId . time();
		} else {
			$old_packageNumber = Deliveryrequest::where(['_id'=> Input::get('request_id')])->select('PackageNumber','PackageId')->first();
			$PackageId = $old_packageNumber->PackageId;
			$old_packageNumber = $old_packageNumber->PackageNumber;
		}

		$insert = [
			'RequesterId' => new MongoId($user->id),
			'RequesterName' => ucfirst($requester->Name),
			'ProductTitle' => Input::get('package_title'),
			'RequestType' => "delivery",
			'Status' => 'pending',
			'request_version'=>'new',
			'ReceiverIsDifferent'=> (Input::get('ReceiverIsDifferent') == 'on')?'yes':'no', 
			'ReceiverName'=> (Input::get('ReceiverIsDifferent') == 'on')? ucfirst(Input::get('ReceiverName')):'' , 
			/*Trip*/
			"TransporterName" => $transporterName,
			"TransporterId" => (Input::get('transporter_id') != '')? new MongoId(trim(Input::get('transporter_id'))) : '',
			"TripId" => $tripid,
			/*End trip*/
			'PackageId' => $PackageId,
			'itemCount' => '',
			'device_version' => Input::get('browser'),
			'app_version' => Input::get('version'),
			'device_type' => Input::get('device_type'),
			"PackageNumber" => $old_packageNumber,
			"PickupFullAddress" => $this->formated_address([
				(Input::get('address_line_1') != '') ? Input::get('address_line_1') : '',
				(Input::get('address_line_2') != '') ? Input::get('address_line_2') : '',
				$city->name,
				$state->name,
				$country->name,
			], Input::get('zipcode')),
			'PickupAddress' => Input::get('address_line_1'),
			'PickupAddress2' => Input::get('address_line_2'),
			'PickupCity' => trim($city->name),
			'PickupState' => trim($state->name),
			'PickupCountry' => trim($country->name),
			'PickupPinCode' => Input::get('zipcode'),
			'PickupLatLong' => [floatval(Input::get('PickupLong')), floatval(Input::get('PickupLat'))],
			'PickupDate' => get_utc_time(Input::get('pickup_date')),
			'DeliveryFullAddress' => $this->formated_address([
				(Input::get('drop_off_address_line_1') != '') ? Input::get('drop_off_address_line_1') : '',
				(Input::get('drop_off_address_line_2') != '') ? Input::get('drop_off_address_line_2') : '',
				$drop_off_city->name,
				$drop_off_state->name,
				$drop_off_country->name,
			], Input::get('drop_off_zipcode')),
			'DeliveryAddress' => Input::get('drop_off_address_line_1'),
			'DeliveryAddress2' => Input::get('drop_off_address_line_2'),
			'DeliveryCity' => trim($drop_off_city->name),
			'DeliveryState' => trim($drop_off_state->name) ,
			'DeliveryCountry' => trim($drop_off_country->name),
			'DeliveryPincode' => Input::get('drop_off_zipcode'),
			"DeliveryLatLong" => [floatval(Input::get('DeliveryLong')), floatval(Input::get('DeliveryLat'))],
			'DeliveryDate' => get_utc_time(Input::get('drop_off_date')),

			"DeliveryStateId" => @$drop_off_state->id,
			"DeliveryCountryId" => @$drop_off_country->id,

			"ReturnFullAddress" => $this->formated_address([
				Input::get('return_address_line_1'),
				Input::get('return_address_line_2'),
				trim(@$return_city->name),
				trim(@$return_state->name),
				trim(@$return_country->name),
			], Input::get('return_zipcode')),
			"ReturnAddress" => Input::get('return_address_line_1'),
			"ReturnAddress2" => Input::get('return_address_line_2'),
			"ReturnCityTitle" => trim(@$return_city->name) ,
			"ReturnStateTitle" => trim(@$return_state->name) ,
			'ReturnCountry' => trim(@$return_country->name) ,
			'ReturnPincode' => Input::get('return_zipcode'),
			"FlexibleDeliveryDate" => (Input::get('is_delivery_date_flexible') == 'yes') ? 'yes' : 'no',
			"JournyType" => (Input::get('journey_type') == 'return') ? 'return' : 'one_way',
			'NotDelReturnFullAddress' => $this->formated_address([
				Input::get('nd_return_address_line_1'),
				Input::get('nd_return_address_line_2'),
				@$nd_return_city->name,
				@$nd_return_state->name,
				@$nd_return_country->name,
			], Input::get('nd_return_zipcode')),
			"InCaseNotDelReturnAddress" => Input::get('nd_return_address_line_1'),
			"InCaseNotDelReturnAddress2" => Input::get('nd_return_address_line_2'),
			"InCaseNotDelReturnCity" => trim(@$nd_return_city->name) ,
			"InCaseNotDelReturnState" => trim(@$nd_return_state->name) ,
			"InCaseNotDelReturnCountry" => trim(@$nd_return_country->name) ,
			"InCaseNotDelReturnPincode" => Input::get('nd_return_zipcode'),
			"PackageMaterial" => (Input::get('need_package_material') == 'yes') ? 'yes' : 'no',
			"PackageMaterialShipped" => '',
			"PublicPlace" => (Input::get('public_palce') == 'yes') ? 'yes' : 'no',
			'ReceiverCountrycode' => Input::get('country_code'),
			'ReceiverMobileNo' => Input::get('phone_number'),
			"consolidate_item" => (Input::get('consolidate_check') == 'on') ? 'on' : 'off',
			"Distance" => $calculated_distance,
			'ShippingCost' => Input::get('shipping_cost_input'),
			'totalUpsRate' => Input::get('ups_charge_input'),
			'WarehouseTransitFee'=>0,
			'InsuranceCost' => 0,
			'Discount' => 0,
			'DutyAndCustom'=>0,
			'Tax'=>0,
			'TotalCost' => 0,
			'promocode' => Input::get('promocode'),
			'AquantuoFees' => 0,
			'region_charge'=>0,
			'after_update_difference' => 0,
			'ProductList' => [],
			'EnterOn' => new MongoDate
		];

		if (Input::get('request_id') == '') {
			$res = json_decode($this->prepare_request_calculation());
			$insert["EnterOn"] = new MongoDate();
			// $dutyAndCustoms = $res->DutyAndCustom;
		} else {
			$res = json_decode($this->edit_calculation());
		}

		if (count($res->product) > 0) {
			$k = 0;
			foreach ($res->product as $key) {
				$k++;
				if (!empty(trim($insert["ProductTitle"]))) {
					$insert["ProductTitle"] = "{$insert['ProductTitle']}, ";
				}
				$insert["ProductTitle"] .= @$key->product_name;
				// $insert['ShippingCost'] += $key->shippingCost + $key->productUpsRate;
				// $insert['totalUpsRate'] += $key->productUpsRate;
				$insert['InsuranceCost'] += $key->InsuranceCost;
				// $insert['TotalCost'] += $key->shippingCost + $key->InsuranceCost + $key->productUpsRate;

				$fees = ((floatval($key->shippingCost + $key->productUpsRate) * $configurationdata->aquantuoFees) / 100);
				$insert['AquantuoFees'] += $fees;

				if ($key->travelMode == 'air') {
					$ExpectedDate = new Mongodate(strtotime('+13 day'));
				} else {
					$ExpectedDate = new Mongodate(strtotime('+60 day'));
				}
				$insert['ProductList'][] = [
					'_id' => (Input::get('request_id') == '') ? (string) new MongoId() : $key->_id,
					"product_name" => @$key->product_name,
					'package_id' => (Input::get('request_id') == '') ? $insert['PackageNumber'] . $k : $key->package_id,
					'status' => 'pending',
					"productUpsRate" => $key->productUpsRate,
					"productWidth" => $key->productWidth,
					"productHeight" => $key->productHeight,
					"productLength" => $key->productLength,
					"productCost" => $key->productCost,
					"productWeight" => $key->productWeight,
					"productHeightUnit" => $key->productHeightUnit,
					"ProductWeightUnit" => $key->ProductWeightUnit,
					"ProductLengthUnit" => $key->ProductLengthUnit,
					"productCategory" => $key->productCategory,
					"productCategoryId" => $key->productCategoryId,
					"travelMode" => $key->travelMode,
					"InsuranceStatus" => $key->InsuranceStatus,
					"productQty" => $key->productQty,
					"ProductImage" => $key->ProductImage,
					//"OtherImage" => $key->OtherImage,
					"QuantityStatus" => $key->QuantityStatus,
					"BoxQuantity" => $key->productQty,
					//"BoxQuantity" => $key->qty,
					"Description" => $key->Description,
					"PackageMaterial" => $key->PackageMaterial,
					"PackageMaterialShipped" => $key->PackageMaterialShipped,
					"shippingCost" => $key->shippingCost + $key->productUpsRate,
					'InsuranceCost' => $key->InsuranceCost,
					"DeliveryVerifyCode" => (Input::get('request_id') == '') ? rand(1000, 9999) : $key->DeliveryVerifyCode,
					'tpid' => '',
					'tpName' => '',
					'inform_mail_sent' => 'no',
					'PaymentStatus' => 'no',
					'total_cost' => $key->shippingCost + $key->InsuranceCost + $key->productUpsRate,
					'after_update' => $key->shippingCost + $key->InsuranceCost,
					'aq_fees' => floatval($fees),
					'TransporterFeedbcak' => '',
					'TransporterRating' => '',
					'RequesterFeedbcak' => '',
					'RequesterRating' => '',
					'ReturnType' => '',
					'ReceiptImage' => '',
					'RejectTime' => '',
					'TransporterMessage' => '',
					'CancelDate' => '',
					'StripeChargeId' => '',
					'DeliveredDate' => new MongoDate(),
					'shipping_cost_by_user' => '',
					'EnterOn' => new MongoDate(),
					'ExpectedDate' => '',
					'DeliveredTime' => '',
				];
			}

			$insert['ShippingCost'] += $res->warehouse_transit_fee;

			$discount = Promo::get_validate(Input::get('promocode'), $insert['ShippingCost'], $requester->_id, $requester->Default_Currency);

			
			$insert['TotalCost'] = $insert['TotalCost'];
			$insert['region_charge'] = $this->resionCharges(['name' => $drop_off_state->name, 'id' => $drop_off_state->id]);
			$insert['TotalCost'] = $insert['TotalCost'] + $insert['region_charge'] + number_format(Input::get("dutyCustoms"), 2) + number_format(Input::get("tax"), 2) + $res->warehouse_transit_fee;
			$insert['DutyAndCustom'] = floatval(number_format(Input::get("dutyCustoms"), 2));
			$insert['WareHouseTransitFee'] = $res->warehouse_transit_fee;
			$insert['Tax'] = floatval(number_format(Input::get("tax"), 2));
			if ($discount['success'] == 1) {
				$insert['discount'] = $discount['discount'];
				$insert['TotalCost'] = $insert['TotalCost'] - $discount['discount'];
			}
		} else {
			return json_encode($response);die;
		}
		$insert['TotalCost'] = Input::get("total_amount_input");

		if (Input::get('request_id') == '') {
			$record = (String) Deliveryrequest::insertGetId($insert);

			$ActivityLog = DeliveryRequest::where('_id', '=', $record)->select('_id', 'PackageNumber', 'ProductList')->first();
			$array = $ActivityLog->ProductList;

			/* Activity Log  create request for Send a Package
				            created Aakash Tejwal 24-02-2018
			*/
			foreach ($array as $key) {
				$insertactivity = [
					'request_id' => $record,
					'request_type' => 'delivery',
					'PackageNumber' => $ActivityLog->PackageNumber,
					'item_id' => $key['_id'],
					'package_id' => $key['package_id'],
					'item_name' => $key['product_name'],
					'log_type' => 'request',
					'message' => 'Package has been created.',
					'status' => 'pending',
					'action_user_id' => Auth::user()->_id,
					'EnterOn' => new MongoDate(),
				];
				Activitylog::insert($insertactivity);
			}
		} else {

			$record = Deliveryrequest::where(['_id' => trim(Input::get('request_id'))])
				->update($insert);
		}

		if ($record) {
			if (Input::get('request_id') == '') {
				if(Input::get('listing') == 'own'){
					Additem::where(["user_type" => "admin", 'type' => 'delivery','user_id' => Auth::user()->_id])->delete();
				}else{
					Additem::where(["user_type" => "admin", 'type' => 'delivery'])->delete();
				}
				
				$response = ['success' => 1, 'msg' => 'Request has been created successfully.', 'reqid' => $record, 'type' => 'create'];
				$this->emailToRequester($record);

			} else {
				$Notification = new Notify();
				$Notification->setValue('title', "Package has been updated");
				$Notification->setValue('message',"Aquantuo has updated your package title:".$insert['ProductTitle'].".");
				$Notification->setValue('type', 'request_detail');
				$Notification->setValue('location', 'request_detail');
				$Notification->setValue('locationkey', Input::get('request_id'));
				$Notification->add_user($user->NotificationId, $user->DeviceType);
				$Notification->fire(); 
				
				$response = ['success' => 1, 'msg' => 'Request has been updated successfully.', 'reqid' => trim(Input::get('request_id')), 'type' => 'update'];
			}
		}
		return json_encode($response);

	}

	public function checkTrip($data)
	{
		$response = ['success' => 1, 'msg' => 'Something went wrong.','trname'=>''];
		if ($data['tripid'] != '' && $data['transporter_id'] != '') {
			
			$data['trip_data'] = Trips::where(['_id' => $data['tripid'],'TransporterId' => new MongoId($data['transporter_id'])])->first();
			if(count($data['trip_data']) > 0){

				if($data['trip_data']->TripType == 'individual'){
					if ($data['trip_data']->SourceDate < new MongoDate()) {
						$response = ['success' => 0, 'msg' => 'Trip has been started. You are not able to create request.'];
					}
				}

				$transporterName = @$data['trip_data']->TransporterName;
				$tripid = $data['trip_data']->_id;
				$response = ['success' => 1, 'msg' => 'Success','trname'=> $transporterName];
			}else{
				$response = ['success' => 0, 'msg' => 'Invalid trip or transporter.'];
			}
		}
		return $response;
	}

	public function emailToRequester($id) {
		$delivery = Deliveryrequest::where(array('_id' => $id))
			->select('RequesterId', 'RequesterName', 'ProductList', 'PackageId', 'PackageNumber', 'ProductTitle', 'ShippingCost', 'TotalCost', 'InsuranceCost', 'PickupFullAddress', 'DeliveryFullAddress', 'RequestType')->first();
		if ($delivery) {
			$users = User::where(['_id' => $delivery->RequesterId])->select('Email', 'Name')->first();
			$link = url('process-card-list') . '/' . (string) $delivery->_id;
			$Email = new NewEmail();

			$currency_conversion = $this->currency_conversion($delivery->TotalCost);

			if ($users->Default_Currency == 'CAD') {
				$user_currency = $currency_conversion['canadian_cost'];
			} else if ($users->Default_Currency == 'PHP') {
				$user_currency = $currency_conversion['philipins_cost'];
			} else if ($users->Default_Currency == 'GBP') {
				$user_currency = $currency_conversion['uk_cost'];
			} else {
				$user_currency = $currency_conversion['ghanian_cost'];
			}


			$Email->send_mail(strtolower($delivery->RequestType) == 'delivery' ? '5f64935656783dcb034a742c' : '5a7d7d4ce4b06005c659a45e', [
				"to" => $users->Email,
				"replace" => [
					"[TITLE]" => ucfirst($delivery->ProductTitle),
					"[USERCURRENCY]" => $user_currency,
					"[USERNAME]" => $users->Name,
					"[PACKAGETITLE]" => ucfirst($delivery->ProductTitle),
					"[PACKAGEID]" => $delivery->PackageNumber,
					"[TOTALCOST]" => $delivery->TotalCost,
					"[SHIPPINGCOST]" => $delivery->ShippingCost,
					"[INSURANCECOST]" => $delivery->InsuranceCost,
					"[DROPOFFADDRESS]" => $delivery->DeliveryFullAddress,
					"[SOURCE]" => $delivery->PickupFullAddress,
					"[LINK]" => "<a href='$link'>$link</a>",
				],
			]);

			$users->temprary = 'temp';
			$users->detail_url = $link;
			$users->save();

		}
	}

	public function formated_address($array, $zip) {
		$address = '';
		foreach ($array as $ky => $val) {
			if (trim($val) != '') {
				if (trim($address) != '') {$address = "$address, ";}
				$address .= $val;
			}
		}
		if (trim($zip) != '') {$address .= "- $zip";}
		return $address;
	}

	public function prepare_request_calculation_Old() {
		if(Input::get('listing') == 'own'){
			$items = Additem::where(['type' => 'delivery','user_type' => 'admin','user_id' => Auth::user()->_id])->get();
		}else{
			$items = Additem::where(['type' => 'delivery','user_type' => 'admin'])->get();
		}

		$rate = 0;
		$insurance = 0;
		$weight = 0;
		$volume = 0;
		$distance = $this->get_distance(Input::get('calculated_distance'));
		$drop_lat_long = ['lat' => Input::get('DeliveryLat'), 'lng' => Input::get('DeliveryLong')];

		$distance_cal = Utility::GetDrivingDistance(Input::get('PickupLat'), Input::get('PickupLong'), Input::get('DeliveryLat'), Input::get('DeliveryLong'));
		if ($distance_cal == false) {
			$response['msg'] = 'Oops! We are unable to calculate distance.';
			return json_encode($response);die;
		} else {
			$distance = $this->get_distance($distance_cal->distance);
			$distance2 = $distance_cal->distance;
		}

		$setting = Configuration::where(['_id' => '5673e33e6734c4f874685c84'])->select('accra_charge', 'accra_charge_type')->first();
		$accra_commision = 0;

		$response = ['success' => 0, 'shipping_cost' => '', 'shipping_cost' => '', 'ghana_total_amount' => '', 'formated_currency' => '', 'total_weight' => 0];

		$return_array = [];
		if ($items) {
			foreach ($items as $key) {
				$category = Category::where(['_id' => $key->productCategoryId, 'TravelMode' => $key->travelMode, 'Status' => 'Active'])->first();
				$key->InsuranceCost = 0;
				if ($category) {
					// $weight += $this->get_weight($key->productWeight, $key->productWeightUnit);
					$weight += $this->GetVolumetricWeightInLbs($key->productWidth, $key->productHeight, $key->productLength, $key->productWeight, $key->productWeightUnit);
					if ($key->InsuranceStatus == 'yes') {
						$key->InsuranceCost = $this->get_insurance($key->productCost, $key->productQty);
						$insurance += $key->InsuranceCost;
					}

					$volume += $this->get_volume(array(
						"width" => $key->productWidth,
						"widthunit" => $key->ProductLengthUnit,
						"height" => $key->productHeight,
						"heightunit" => $key->productHeightUnit,
						"length" => $key->productLength,
						"lengthunit" => $key->ProductLengthUnit,
					));

					if (isset($category->Shipping) && is_array($category->Shipping)) {

						foreach ($category->Shipping as $key2) {
							if ($category->ChargeType == 'distance') {

								if ($distance >= $key2['MinDistance'] && $distance <= $key2['MaxDistance']) {
									$key->shippingCost = $key2['Rate'] * (int) $key->productQty;
									$rate += $key2['Rate'] * (int) $key->productQty;
								}
							} else if ($category->ChargeType == 'fixed') {
								if ($weight >= $key2['MinDistance'] && $weight <= $key2['MaxDistance']) {

									//$return_array['shippingCost'] = $key2['Rate'] * (int) $key->productQty;
									$key->shippingCost = $key2['Rate'] * (int) $key->productQty;
									$rate += $key2['Rate'] * (int) $key->productQty;

								}
							}
						}
					}
				}
			}
		}
		$return_array = $items;
		$response['success'] = 1;

		$response['accra_commision'] = 0;
		$response['shipping_cost'] = $rate;
		$response['insurance'] = $insurance;
		$response['total_amount'] = $response['shipping_cost'] + $insurance;
		$response['product'] = $return_array;

		//extraa charge of out side accra
		$extraa_charge = $this->outsideOfAccraCharge($drop_lat_long);
		if ($extraa_charge != false) {
			if ($setting) {
				if ($setting->accra_charge_type == 'Percentage') {
					$accra_commision = ($response['shipping_cost'] * $setting->accra_charge) / 100;
				} else {
					$accra_commision = $setting->accra_charge;
				}

			}
			$response['shipping_cost'] = $response['shipping_cost'] + $accra_commision;
		}
		//end accra charge

		$response['accra_commision'] = $accra_commision;

		$currency_conversion = $this->currency_conversion($response['total_amount']);

		if (Session()->get('Default_Currency') == 'GHS') {
			$response['user_currency'] = $currency_conversion['ghanian_cost'];
		} else if (Session()->get('Default_Currency') == 'CAD') {
			$response['user_currency'] = $currency_conversion['canadian_cost'];
		} else if (Session()->get('Default_Currency') == 'PHP') {
			$response['user_currency'] = $currency_conversion['philipins_cost'];
		} else if (Session()->get('Default_Currency') == 'GBP') {
			$response['user_currency'] = $currency_conversion['uk_cost'];
		} else {
			$response['user_currency'] = $currency_conversion['ghanian_cost'];
		}
                    

		$response['distance'] = number_format($distance,2);


		$response['volume'] = number_format($volume,2);

		$response['total_weight'] = number_format($weight,2);
		return json_encode($response);
	}

	public function category_count($array, $cat_id) {
		$res['count'] = 0;
		$res['weight'] = 0;
		foreach ($array as $key => $value) {
			if ($cat_id == $value['productCategoryId']) {
				$res['count'] += 1;
				// $res['weight'] += $this->get_weight(($value['productWeight'] * (int) $value['productQty']), $value['ProductWeightUnit']);
				$res['weight'] += $this->GetVolumetricWeightInLbs($value['productWidth'], $value['productHeight'], $value['productLength'], $value['productWeight'], $value['ProductWeightUnit']);
			}
		}
		//print_r($res); die;
		return $res;
	}
	public function get_warehouse_transit_fee() {
		$data = Configuration::where(array('_id' => '5673e33e6734c4f874685c84'))
			->select('warehouse_transit_fee')->first();

		$warehouse_transit_fee = 0;

		if (count($data) > 0) {
			return $data->warehouse_transit_fee;
		}
		return 0;
	}
	public function set_air_category($array, $inputData) {
		$already_calculated_category = [];
		$category_set = [];
		foreach ($array as $key => $val) {
			if ($val['travelMode'] == 'air') {
				if (!in_array($val['productCategoryId'], $already_calculated_category)) {
					$cat_weight = $this->category_count($array, $val['productCategoryId']);
					$weight = $cat_weight['weight'];
					$already_calculated_category[] = $val['productCategoryId'];
					$data = Category::where(['_id' => $val['productCategoryId']])->where(['Status' => 'Active', 'TravelMode' => 'air', 'ChargeType' => 'fixed'])
						->select('Shipping', 'price_increase_by')
						->first();
					if (count($data) > 0) {
						$rate = 0;
						$is_other_category = 'no';
						if ($data["_id"] == OtherCategory) {
							foreach ($data['Shipping'] as $key1) {
								if ($key1['MinDistance'] <= floatval($weight) && $key1['MaxDistance'] >= floatval($weight)) {
									$rate = $key1['Rate'];
									
									if($val['productCost']  > 1){
											$p = $val['productCost'] - 1;
											$rate = $rate + ($p * $data['price_increase_by']);
									}
									
								}
							}
						} else {
							foreach ($data['Shipping'] as $key1) {
								if ($key1['MinDistance'] <= floatval($weight) && $key1['MaxDistance'] >= floatval($weight)) {

									if ($inputData['consolidate_check'] == 'on') {
										$rate = $key1['CONSOLIDATE_RATE'];
									} else {
										$rate = $key1['Rate'];
									}
								}
							}
						}
						
						if ($rate > 0) {
							$category_set[] = [
								'cat_id' => $val['productCategoryId'],
								'rate' => $rate,
							];
						}

					}
				}
			}
		}

		//echo '<pre>';
		//print_r($category_set); die;

		return $category_set;
	}

	public function get_category_price($category_set, $id) {
		$price = 0;
		foreach ($category_set as $key) {
			if ($key['cat_id'] == $id) {
				$price = $key['rate'];
			}
		}
		return $price;
	}

	public function kgToLb ($val) {
        return $val * 2.20462;
    }

	public function prepare_request_calculation() {
		if (Input::get('listing') == 'own') {
			$items = Additem::where(['type' => 'delivery', 'user_type' => 'admin', 'user_id' => Auth::user()->_id])->get();
		} else {
			$items = Additem::where(['type' => 'delivery', 'user_type' => 'admin'])->get();
		}

		$rate = 0;
		$insurance = 0;
		$weight = 0;
		$totalWeight = 0;
		$volume = 0;
		$distance = $this->get_distance(Input::get('calculated_distance'));
		$drop_lat_long = ['lat' => Input::get('DeliveryLat'), 'lng' => Input::get('DeliveryLong')];
		$distance_cal = Utility::GetDrivingDistance(Input::get('PickupLat'), Input::get('PickupLong'), Input::get('DeliveryLat'), Input::get('DeliveryLong'));

		if ($distance_cal == false) {
			$response['msg'] = 'Oops! We are unable to calculate distance.';
			return json_encode($response);die;
		} else {
			$distance = $this->get_distance($distance_cal->distance);
			$distance2 = $distance_cal->distance;
		}

		$drop_off_state = json_decode(Input::get('state11'));
		$response = ['success' => 0, 'shipping_cost' => '', 'shipping_cost' => '', 'ghana_total_amount' => '', 'formated_currency' => '', 'total_weight' => 0];
		$return_array = [];
		$inputData['consolidate_check'] = Input::GET('consolidate_check');
		$res['air_rate'] = 0;
		$total_productcost = 0;

		if ($items) {
			$set_air_category = $this->set_air_category($items, $inputData);
			$ups_rate = 0;
			$total_ups_rate = 0;
			$total_custom_duty_rate = 0;
			$warehouse_transit_fee = 0;
			$is_ups_rate_applied = false;
			$is_customs_duty_applied =false;
			$pickup_country = json_decode(Input::get("country"));	// Pickup.
			$drop_off_country = json_decode(Input::get("drop_off_country"));	// Drop off.
			$destination_postal_code = Input::get("drop_off_zipcode");
			$pickup_postal_code = Input::get("zipcode");
			$pickup_state = json_decode(Input::get("state10"));
			$drop_off_state = json_decode(Input::get("state11"));
			$pickup_city = json_decode(Input::get("city"));

			foreach ($items as $key) {
				// Calculate ups rate of each item.
				$last_price = 0;// is used for only other categry in air mode
				$is_macth = false;
				$key->productUpsRate = 0;
				// Get Volumetric Weight of product.
				$productVolumetricWeightInLbs = $this->GetVolumetricWeightInLbs($key->productWidth, $key->productHeight, $key->productLength, $key->productWeight, $key->ProductWeightUnit) * $key->productQty;

				if ($pickup_country->name != $drop_off_country->name) {
					$weight = $key->productWeight * $key->productQty;
					$is_customs_duty_applied =  true;

		            if ($key->ProductWeightUnit == "kg") {
		                $weight = $this->kgToLb($weight);
		            }

					if ($productVolumetricWeightInLbs > $weight) {
						$weight = $productVolumetricWeightInLbs;
					}
		            
		            if ($pickup_country->name == "USA") {
						$params = array(
							"width" => $key->productWidth,
							"height" => $key->productHeight,
							"length" => $key->productLength,
							"weight" => $weight,
							"pickupPostalCode" => $pickup_postal_code,
							"pickupStateProvinceCode" => $pickup_state->short_name,
							"pickupCity" => $pickup_city->name,
							"pickupCountryCode" => "US",
							"destinationPostalCode" => "19977",
							"service"=> "03"
						);
		                
						$ups = new upsRate();
						$ups->setCredentials();
						$ups_rate = $ups->getRate($params);
		                $key->productUpsRate = $ups_rate;
		                $total_ups_rate += $ups_rate;
		                $is_ups_rate_applied =true;
		            } else if ($drop_off_country->name == "USA") {
		            	if ($key->productCost <= 2000) {
							$is_customs_duty_applied = false;
						}
						
		                $params = array(
							"width" => $key->productWidth,
							"height" => $key->productHeight,
							"length" => $key->productLength,
							"weight" => $weight,
							"pickupPostalCode" => "19977",
							"pickupStateProvinceCode" => "DE",
							"pickupCity" => "Smyrna",
							"pickupCountryCode" => "US",
							"destinationPostalCode" => $destination_postal_code,
							"service"=> "03"
						);

		                $ups = new upsRate();
						$ups->setCredentials();
						$ups_rate = $ups->getRate($params);
		                $key->productUpsRate = $ups_rate;
		                $total_ups_rate += $ups_rate;
		                $is_ups_rate_applied = true;
		            }
		        }

		        // UPS charge also applied in USA if state is diffrent.
				if ($pickup_country->name == "USA" && $drop_off_country->name == "USA") {
					if ($pickup_state->name != $drop_off_state->name) {
						$weight = $key->productWeight * $key->productQty;

						if ($key->ProductWeightUnit == "kg") {
							$weight = $this->kgToLb($weight);
						}

						if ($productVolumetricWeightInLbs > $weight) {
							$weight = $productVolumetricWeightInLbs;
						}

						$params = array(
							"width" => $key->productWidth,
							"height" => $key->productHeight,
							"length" => $key->productLength,
							"weight" => $weight,
							"pickupPostalCode" => "19977",
							"pickupStateProvinceCode" => "DE",
							"pickupCity" => "Smyrna",
							"pickupCountryCode" => "US",
							"destinationPostalCode" => $destination_postal_code,
							"service"=> "03"
						);

						$ups = new upsRate();
						$ups->setCredentials();
						$ups_rate = $ups->getRate($params);
						$key->productUpsRate = $ups_rate;
						$total_ups_rate += $ups_rate;
						$is_ups_rate_applied = true;
					}
				}

				$key->InsuranceCost = 0;
				$key->shippingCost = 0;
				$key->customDuty = 0;
				$total_productcost += ($key->productCost * $key->productQty);
				$weight2 = $this->get_weight($key->productWeight, $key->productWeightUnit);
				// $weight2 = $this->GetVolumetricWeightInLbs($key->productWidth, $key->productHeight, $key->productLength, $key->productWeight, $key->ProductWeightUnit);
				// $weight += $weight2;
				$totalWeight += $weight;

				if ($key->InsuranceStatus == 'yes') {
					$key->InsuranceCost = $this->get_insurance($key->productCost, $key->productQty);
					$insurance += $key->InsuranceCost;
				}

				$volume += $this->get_volume(array(
					"width" => $key->productWidth,
					"widthunit" => $key->ProductLengthUnit,
					"height" => $key->productHeight,
					"heightunit" => $key->productHeightUnit,
					"length" => $key->productLength,
					"lengthunit" => $key->ProductLengthUnit,
				)) * $key->productQty;
				// match with newRequesthelper
				$category = Category::where(['_id' => $key->productCategoryId,'Status' => 'Active'])->first();
				
				if (count($category) == 0) {
					$response['msg'] = 'Oops! category not found';
					return json_encode($response);die;
				}

				// get custom duty 
				if (isset($category->custom_duty) && $is_customs_duty_applied) {
					$total_custom_duty_rate += (($key->productCost * (int) $key->productQty) * $category->custom_duty)/100;
					$key->customDuty = (($key->productCost * (int) $key->productQty) * $category->custom_duty)/100;
				}
				
				if ($category->ChargeType == 'distance') {
					$categoryConstantArray = array(AUTO_FULLSIZE_SUV, AUTO_INTERMEDIATE_SUV, AUTO_SEDAN_ECONOMY, AUTO_SEDAN_FULLSIZE, AUTO_SEDAN_INTERMEDIATE, AUTO_STANDARD_SUV, AUTOMOBILE_VAN);

					if ($key->travelMode == 'ship' && in_array($key->productCategoryId, $categoryConstantArray)) {
						foreach ($category->Shipping as $key2) {
							if ($distance >= $key2['MinDistance'] && $distance <= $key2['MaxDistance']) {
								if ($inputData['consolidate_check'] == 'on') {
									$key->shippingCost = $key2['CONSOLIDATE_RATE'] * (int) $key->productQty;
									$rate += $key2['CONSOLIDATE_RATE'] * (int) $key->productQty;
								} else {
									$key->shippingCost = $key2['Rate'] * (int) $key->productQty;
									$rate += $key2['Rate'] * (int) $key->productQty;
								}
							}		
						}
					} else {
						$cat_count = $this->category_count($items, $key->productCategoryId);

						foreach ($category->Shipping as $key2) {
							if ($distance >= $key2['MinDistance'] && $distance <= $key2['MaxDistance']) {
								if ($inputData['consolidate_check'] == 'on') {
									$key->shippingCost = $key2['CONSOLIDATE_RATE'] / $cat_count['count'];
									$rate += $key2['CONSOLIDATE_RATE'] / $cat_count['count'];
								} else {
									$key->shippingCost = $key2['Rate'] / $cat_count['count'];
									$rate += $key2['Rate'] / $cat_count['count'];
								}
							}		
						}
					}
				} elseif ($key->travelMode == 'air' && ($key->productCategoryId != OtherCategory && $key->productCategoryId != ElectronicsCategory)) {
					$air_rate = $this->get_category_price($set_air_category,$key->productCategoryId);
					$cat_count = $this->category_count($items,$key->productCategoryId);
					$key->shippingCost = $air_rate / $cat_count['count'];
					$res['air_rate'] += $air_rate / $cat_count['count'];
				} elseif ($key->travelMode == 'air' && ($key->productCategoryId == OtherCategory || $key->productCategoryId != ElectronicsCategory)) {
					foreach ($category->Shipping as $key2) {
						if ($distance >= $key2['MinDistance'] && $distance <= $key2['MaxDistance']) {
							$is_macth = true;
							$key->shippingCost = $key2['Rate'] * (int) $key->productQty;
							$rate += $key2['Rate'] * (int) $key->productQty;
							$res['air_rate'] = $rate;
						}
						$last_price = $key2['Rate'];
					}

					if (!$is_macth && ($key->productCategoryId == OtherCategory || $key->productCategoryId == ElectronicsCategory)) {
						$itemWeight_1 = $weight2-2;
						$itemWeight_2 = intval($itemWeight_1/2);
						$rate_1 =  $itemWeight_2 * $category->weight_increase_by;
						$rate_2 = $rate_1 + $last_price;

						if ($key->productCost > 1) {
							$p = $key->productCost - 1;
							$key->shippingCost = $rate_2 + ($p * $category->price_increase_by) * (int) $key->productQty;
							$rate += $rate_2 + ($p * $category->price_increase_by) * (int) $key->productQty;
							$res['air_rate'] = $rate ;
						} else {
							$rate += $rate_2 * (int) $key->productQty;
							$key->shippingCost = $rate_2 * (int) $key->productQty;
							$res['air_rate'] = $rate;
						}
					}
				} else {
					if (isset($category->Shipping) && is_array($category->Shipping)) {
						foreach ($category->Shipping as $key2) {
							if ($distance >= $key2['MinDistance'] && $distance <= $key2['MaxDistance']) {
								$key->shippingCost = $key2['Rate'] * (int) $key->productQty;
								$rate += $key2['Rate'] * (int) $key->productQty;
							}
						}
					}
				}
			}
		}

		$return_array = $items;
		$response['success'] = 1;
		$DutyAndCustom = 0;

		if ($is_customs_duty_applied) {
			$DutyAndCustom = $total_custom_duty_rate;
		}
		
		if ($is_ups_rate_applied) {
			$warehouse_transit_fee = $this->get_warehouse_transit_fee();
		}

		$Tax = $this->get_tax($total_productcost);

		$response['DutyAndCustom'] = $DutyAndCustom;
		$response['Tax'] = $Tax;
		$response['totalUpsCharge'] = $total_ups_rate;
		$response['regionCharges'] = 0;
		$response['warehouse_transit_fee'] = $warehouse_transit_fee;
		$response['shipping_cost'] = $rate + $total_ups_rate+ $warehouse_transit_fee;
		$response['insurance'] = $insurance;
		$response['total_amount'] = $response['shipping_cost'] + $insurance + $DutyAndCustom + $Tax;
		$config = Configuration::find(CongigId);
		if ($drop_off_country->name === 'Kenya') {
			$response['DutyAndCustom'] = 0.0;
			$extra = 0;
			if ($total_productcost > 100) {
				$extra = ($total_productcost * $config->kenya_per_item_value)/100;
				$extra = number_format($extra, 2, '.', '');
			}
			// if ($data['weight_unit'] === 'kg') {
			// 	$ratePerKg = number_format(($data['weight'] * $config->kenya_rate_per_kilogram), 2, '.', '');
			// } else {
				$ratePerKg = number_format((($totalWeight * 0.453592) * $config->kenya_rate_per_kilogram), 2, '.', '');
			// }
			// echo $ratePerKg;
			$response['shipping_cost'] = $ratePerKg < $config->kenya_rate_per_kilogram ? 17 : $ratePerKg;
			// $resionCharges = $this->resionCharges(['name' => $state->name, 'id' => $state->id]);
			$response['total_amount'] = $response['shipping_cost'] + $insurance + $extra;
			// $response['total_amount_txt'] = 'resionCharges - ' . $resionCharges . 'ratePerKg - ' .  $ratePerKg . 'extra - ' .  $extra;
			if ($response['total_amount'] < $config->kenya_rate_per_kilogram) {
				$response['total_amount'] = $config->kenya_rate_per_kilogram;
			}
		}
		$response['product'] = $return_array;
		$response['AquantuoFees'] = $this->get_aquantuo_fees($response['shipping_cost']);
		//extraa charge of out side accra
		$response['regionCharges'] = $this->resionCharges(['name' => $drop_off_state->name, 'id' => $drop_off_state->id]);
		$response['total_amount'] = $response['total_amount'] + $response['regionCharges'];
		//end accra charge

		$currency_conversion = $this->currency_conversion($response['total_amount']);

		if (Session()->get('Default_Currency') == 'GHS') {
			$response['user_currency'] = $currency_conversion['ghanian_cost'];
			$response["user_currency_rate"] = $currency_conversion["currency_rate_ghs"];
			$response["FormatedText"] = $currency_conversion["FormatedText_ghs"];
		} else if (Session()->get('Default_Currency') == 'CAD') {
			$response['user_currency'] = $currency_conversion['canadian_cost'];
			$response["user_currency_rate"] = $currency_conversion["currency_rate_cad"];
			$response["FormatedText"] = $currency_conversion["FormatedText_cad"];
		} else if (Session()->get('Default_Currency') == 'PHP') {
			$response['user_currency'] = $currency_conversion['philipins_cost'];
			$response["user_currency_rate"] = $currency_conversion["currency_rate_php"];
			$response["FormatedText"] = $currency_conversion["FormatedText_php"];
		} else if (Session()->get('Default_Currency') == 'GBP') {
			$response['user_currency'] = $currency_conversion['uk_cost'];
			$response["user_currency_rate"] = $currency_conversion["currency_rate_gbp"];
			$response["FormatedText"] = $currency_conversion["FormatedText_gbp"];
		} else {
			$response['user_currency'] = $currency_conversion['ghanian_cost'];
			$response["user_currency_rate"] = $currency_conversion["currency_rate_ghs"];
			$response["FormatedText"] = $currency_conversion["FormatedText_ghs"];
		}

		$response['distance'] = number_format($distance, 2);
		$response['volume'] = number_format($volume, 2);
		$response['total_weight'] = number_format($totalWeight, 2);
		return json_encode($response);
	}

	public function get_tax($cost=0) {
		$data = Configuration::where(array('_id' => '5673e33e6734c4f874685c84'))
			->select('tax')->first();
		if (count($data) > 0) {
			$Tax = $cost * $data->tax;
			return $Tax / 100;
		}
		return 0;
	}

	public function get_DutyAndCustom($cost=0) {
		$DutyAndCustom = $cost * 6.20;
		return  $DutyAndCustom / 100;
	}

	public function get_aquantuo_fees($totalcost) {
		$fees = 0;
		$configurationdata = Configuration::where(['_id' => '5673e33e6734c4f874685c84'])->first();
		if ($configurationdata) {
			$fees = ((floatval($totalcost) * $configurationdata->aquantuoFees) / 100);
		}
		return $fees;
	}

	public function editRequest($id) {

		$data['delivery_data'] = Deliveryrequest::where(array('_id' => $id))
			->whereIn('Status', ['ready', 'pending'])->first();

		if (!count($data['delivery_data']) > 0) {
			return redirect('delivery-details/' . $id);
		}

		$data['tr_data']= $data['trip_data']=[];

		if (!empty($data['delivery_data']->TripId)) {
			$data['trip_data'] = Trips::where(array('_id' => $data['delivery_data']->TripId))->first();

			if (!count($data['trip_data']) > 0) {
				return redirect()->back();
			}

			$data['tr_data'] = User::where(array('_id' => $data['trip_data']->TransporterId))->first();

			$data['category2'] = Category::where(['type' => 'Category', 'Status' => 'Active'])->whereIn('TravelMode', ['ship', 'air'])->whereIn('Content', $data['trip_data']->SelectCategory)->get(['TravelMode', 'Content']);
		}

		$data['country'] = CityStateCountry::where(['type' => 'Country', 'Status' => 'Active'])
			->orderBy('Content', 'Asc')
			->get(['_id', 'Content', 'state_available']);
		$data['category'] = Category::where(['type' => 'Category', 'Status' => 'Active'])->whereIn('TravelMode', ['ship', 'air'])->get(['TravelMode', 'Content']);

		$data['state'] = CityStateCountry::where(['type' => 'State', 'Status' => 'Active', 'SuperName' => @$data['delivery_data']['PickupCountry']])->get();

		$data['drop_state'] = CityStateCountry::where(['type' => 'State', 'Status' => 'Active', 'SuperName' => @$data['delivery_data']['DeliveryCountry']])->get();

		$data['return_state'] = CityStateCountry::where(['type' => 'State', 'Status' => 'Active', 'SuperName' => @$data['delivery_data']['ReturnCountry']])->get();

		$data['nd_return_state'] = CityStateCountry::where(['type' => 'State', 'Status' => 'Active', 'SuperName' => @$data['delivery_data']['InCaseNotDelReturnCountry']])->get();

		if ($data['state']) {
			$data['city'] = CityStateCountry::where(['type' => 'city', 'Status' => 'Active', 'SuperName' => @$data['delivery_data']['PickupState']])->get();
		}

		if ($data['drop_state']) {
			$data['drop_city'] = CityStateCountry::where(['type' => 'city', 'Status' => 'Active', 'SuperName' => @$data['delivery_data']['DeliveryState']])->get();
		}

		if ($data['return_state']) {
			$data['return_city'] = CityStateCountry::where(['type' => 'city', 'Status' => 'Active', 'SuperName' => @$data['delivery_data']['ReturnStateTitle']])->get();
		}

		if ($data['nd_return_state']) {
			$data['nd_return_city'] = CityStateCountry::where(['type' => 'city', 'Status' => 'Active', 'SuperName' => @$data['delivery_data']['InCaseNotDelReturnState']])->get();
		}
		$data['user'] = User::select('_id', 'Name', 'Email')->orderBy('Name', 'asc')->get();
		if (isset($data['delivery_data']->ProductList)) {
			return view('Admin::sendapackage.edit.edit_send_package', $data);
		} else {
			$data['user'] = User::where('_id', $data['delivery_data']->RequesterId)
            ->select(['Email'])->first();
			return view('Admin::edit.prepare_request', $data);
		}
	}

	public function addItemInExistRequest() {
		$response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];

		if (Input::get('request_id') != '') {
			$data['request_data'] = Deliveryrequest::where(['_id' => trim(Input::get('request_id'))])->select("RequesterId", "ProductList",'PackageNumber')->first();
			
			$lhwunit = 'cm';
			$weightunit = 'kg';
			if (Input::get('measurement_unit') == 'inches_lbs') {
				$lhwunit = 'inches';
				$weightunit = 'lbs';
			}

			$ProductImage = '';

			if (Input::hasFile('default_image')) {

				if (Input::file('default_image')->isValid()) {
					$ext = Input::file('default_image')->getClientOriginalExtension();
					$ProductImage = time() . rand(100, 9999) . ".$ext";

					if (Input::file('default_image')->move(BASEURL_FILE . 'package/', $ProductImage)) {
						$ProductImage = "package/$ProductImage";
					} else {
						$ProductImage = '';
					}
				}
			}
			$otherImages = [];
			$imageArray = array('package_image2', 'package_image3', 'package_image4', 'package_image5');
			foreach ($imageArray as $key => $val) {
				if (Input::hasFile($val)) {
					if (Input::file($val)->isValid()) {
						$ext = Input::file($val)->getClientOriginalExtension();
						$img = time() . rand(100, 9999) . ".$ext";

						if (Input::file($val)->move(BASEURL_FILE . 'package/', $img)) {
							$img = "package/$img";
							if (isset($otherImages[$key])) {
								// Start remove old image
								if (!empty(trim($otherImages[$key]))) {
									if (file_exists(BASEURL_FILE . $otherImages[$key])) {
										unlink(BASEURL_FILE . $otherImages[$key]);
									}
								}
								// End remove old image
								$otherImages[$key] = $img;
							} else {
								$otherImages[] = $img;
							}
						}
					}
				}
			}
			$itemCount = count($data['request_data']->ProductList);
			$itemCount = $itemCount + 1;
			$category = json_decode(Input::get('category'));
			$add_item = [
				"_id" => (String) new MongoId(),
				'product_name' => ucfirst(Input::get('title')),
				'user_id' => $data['request_data']->RequesterId,
				'package_id'=> $data['request_data']->PackageNumber.$itemCount,
				'type' => 'delivery',
				'status'=>'pending',
				'productWidth' => Input::get('width'),
				'productHeight' => Input::get('height'),
				'productLength' => Input::get('length'),
				'productCost' => Input::get('package_value'),
				'productWeight' => Input::get('weight'),
				'productHeightUnit' => $lhwunit,
				'ProductWeightUnit' => $weightunit,
				'ProductLengthUnit' => $lhwunit,
				'productCategory' => @$category->name,
				'productCategoryId' => @$category->id,
				'travelMode' => Input::get('travel_mode'),
				'currency' => Input::get('Default_Currency'),
				'needInsurance' => Input::get('insurance'),
				"productQty" => (Input::get('quantity') < 1) ? 1 : Input::get('quantity'),
				'ProductImage' => $ProductImage,
				//'OtherImage'=>$otherImages,
				'ProductTitle' => ucfirst(Input::get('title')),
				"QuantityStatus" => (Input::get('quantity') > 1) ? 'multiple' : 'single',
				"BoxQuantity" => (Input::get('quantity') == 0) ? 1 : (int) Input::get('quantity'),
				"Description" => Input::get('description'),
				"InsuranceStatus" => (Input::get('insurance') == 'yes') ? 'yes' : 'no',
				"PackageMaterial" => (Input::get('need_package_material') == 'yes') ? 'yes' : 'no',
				"PackageMaterialShipped" => '',
				'shippingCost'=>0,
				'InsuranceCost'=>0,
				"DeliveryVerifyCode" => rand(1000, 9999),
				'tpid'=>'',
				"tpName" => "", 
				'inform_mail_sent' => 'no',
				'PaymentStatus' => 'no',
				'aq_fees'=>0,
				'total_cost'=>0,
				'after_update'=>0,
				'TransporterFeedbcak' => '',
				'TransporterRating' => '',
				'RequesterFeedbcak' => '',
				'RequesterRating' => '',
				'TransporterMessage' => '',
				'ExpectedDate' => '',
				'DeliveredTime' => '',
			];

			if (isset($data['request_data']->ProductList)) {
				// Stop user to add item with shipping mode sea but different category.
				$is_make_request = "yes";
				$item_with_sea = false;
				$item_with_air = false;

				foreach ($data['request_data']->ProductList as $value) {
					if ($value['productCategoryId'] != @$category->id && $value['travelMode'] == "ship") {
						$is_make_request = "no";
						$item_with_sea = true;
					}

					if (Input::get('travel_mode') == "ship") {
						$item_with_sea = true;
					}

					if ($value['travelMode'] == "air" || Input::get('travel_mode') == "air") {
						$item_with_air = true;
					}

					if ($item_with_air && $item_with_sea) {
						$response["msg"] = "Sorry! You can’t have different shipping modes in the same listing. Please create a separate listing for either air or sea.";
						return json_encode($response);
						die();
					}
				}
				
				if ($is_make_request == "no") {
					$response["msg"] = "If shipping by Sea, all items must be of the same sea category. Please create a new listing if you need to use a different sea category.";
					return json_encode($response);
					die();
				} else {
					$update = Deliveryrequest::where(['_id' => trim(Input::get('request_id'))])->push(['ProductList' => $add_item]);
					if ($update) {
						$response = ['success' => 1, 'msg' => 'Item has been added successfully.'];
					}
				}
				
			}
		}
		return json_encode($response);
	}

	public function edit_calculation_old() {
		$response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		if (Input::get('request_id') != '') {
			$request_data = Deliveryrequest::where(['_id' => trim(Input::get('request_id'))])
				->select('ProductList')
				->first();
			if (count($request_data) == 0 && !isset($request_data->ProductList)) {
				return json_encode($response);die;
			}

			$rate = 0;
			$insurance = 0;
			$weight = 0;
			$volume = 0;
			$distance = $this->get_distance(Input::get('calculated_distance'));
			$drop_lat_long = ['lat' => Input::get('DeliveryLat'), 'lng' => Input::get('DeliveryLong')];
			$distance_cal = Utility::GetDrivingDistance(Input::get('PickupLat'), Input::get('PickupLong'), Input::get('DeliveryLat'), Input::get('DeliveryLong'));
			if ($distance_cal == false) {
				$response['msg'] = 'Oops! We are unable to calculate distance.';
				return json_encode($response);die;
			} else {
				$distance = $this->get_distance($distance_cal->distance);
				$distance2 = $distance_cal->distance;
			}

			$drop_off_state = json_decode(Input::get('state11'));
			$response = ['success' => 0, 'shipping_cost' => '', 'shipping_cost' => '', 'ghana_total_amount' => '', 'formated_currency' => '', 'total_weight' => 0];
			
			$return_array = $request_data->ProductList;

			foreach ($request_data->ProductList as $key => $val) {
				$category = Category::where(['_id' => $val['productCategoryId'], 'TravelMode' => $val['travelMode'], 'Status' => 'Active'])->first();
				$return_array[$key]['shippingCost']= 0;
				$return_array[$key]['InsuranceCost']=0;
				if ($category) {
					// $weight2 = $this->get_weight($val['productWeight'], $val['ProductWeightUnit']);
					$weight2 = $this->GetVolumetricWeightInLbs($val["productWidth"], $val["productHeight"], $val["productLength"], $val["productWeight"], $val["ProductWeightUnit"]);
					$weight += $weight2;
					if ($val['InsuranceStatus'] == 'yes') {
						$return_array[$key]['InsuranceCost'] = $this->get_insurance($val['productCost'], $val['productQty']);
						$insurance += $return_array[$key]['InsuranceCost'];
					}

					$volume += $this->get_volume(array(
						"width" => $val['productWidth'],
						"widthunit" => $val['ProductLengthUnit'],
						"height" => $val['productHeight'],
						"heightunit" => $val['productHeightUnit'],
						"length" => $val['productLength'],
						"lengthunit" => $val['ProductLengthUnit'],
					));

					if (isset($category->Shipping) && is_array($category->Shipping)) {
						foreach ($category->Shipping as $key2) {
							if ($category->ChargeType == 'distance') {
								if ($distance >= $key2['MinDistance'] && $distance <= $key2['MaxDistance']) {
									$return_array[$key]['shippingCost'] = $key2['Rate'] * (int) $val['productQty'];
									$rate += $key2['Rate'] * (int) $val['productQty'];
								}
							} else if ($category->ChargeType == 'fixed') {

								if (floatval($weight2)  >= $key2['MinDistance'] && floatval($weight2) <= $key2['MaxDistance']) {
									
									
									$return_array[$key]['shippingCost'] = $key2['Rate'] * (int) $val['productQty'];
									$rate += $key2['Rate'] * (int) $val['productQty'];

								}
								
							}
						}

					}
				}
			}
			

			/*foreach ($request_data->ProductList as $key) {
				$category = Category::where(['_id' => $key['productCategoryId'], 'TravelMode' => $key['travelMode'], 'Status' => 'Active'])->first();
				$key['InsuranceCost'] = 0;
				if ($category) {
					$weight += $this->get_weight($key['productWeight'], $key['ProductWeightUnit']);
					if ($key['InsuranceStatus'] == 'yes') {
						$key['InsuranceCost'] = $this->get_insurance($key['productCost'], $key['productQty']);
						$insurance += $key['InsuranceCost'];
					}

					$volume += $this->get_volume(array(
						"width" => $key['productWidth'],
						"widthunit" => $key['ProductLengthUnit'],
						"height" => $key['productHeight'],
						"heightunit" => $key['productHeightUnit'],
						"length" => $key['productLength'],
						"lengthunit" => $key['ProductLengthUnit'],
					));

					if (isset($category->Shipping) && is_array($category->Shipping)) {

						foreach ($category->Shipping as $key2) {
							if ($category->ChargeType == 'distance') {

								if ($distance >= $key2['MinDistance'] && $distance <= $key2['MaxDistance']) {
									$key['shippingCost'] = $key2['Rate'] * (int) $key['productQty'];
									$rate += $key2['Rate'] * (int) $key['productQty'];
								}
							} else if ($category->ChargeType == 'fixed') {
								if ($weight >= $key2['MinDistance'] && $weight <= $key2['MaxDistance']) {
									$key['shippingCost'] = $key2['Rate'] * (int) $key['productQty'];
									$rate += $key2['Rate'] * (int) $key['productQty'];

								}
							}
						}
					}
				}
			}*/

			
			$response['success'] = 1;

			
			$response['shipping_cost'] = $rate;
			$response['insurance'] = $insurance;
			$response['total_amount'] = $response['shipping_cost'];
			$response['product'] = $return_array;

			$response['AquantuoFees'] = $this->get_aquantuo_fees($response['shipping_cost']);
			$response['regionCharges'] = $this->resionCharges(['name' => $drop_off_state->name, 'id' => $drop_off_state->id]);

			
			$response['total_amount'] = $response['total_amount'] + $response['regionCharges'];
			$currency_conversion = $this->currency_conversion($response['total_amount']);

			if (Session()->get('Default_Currency') == 'GHS') {
				$response['user_currency'] = $currency_conversion['ghanian_cost'];
			} else if (Session()->get('Default_Currency') == 'CAD') {
				$response['user_currency'] = $currency_conversion['canadian_cost'];
			} else if (Session()->get('Default_Currency') == 'PHP') {
				$response['user_currency'] = $currency_conversion['philipins_cost'];
			} else if (Session()->get('Default_Currency') == 'GBP') {
				$response['user_currency'] = $currency_conversion['uk_cost'];
			} else {
				$response['user_currency'] = $currency_conversion['ghanian_cost'];
			}

			$response['distance'] = number_format($distance,2);
			$response['volume'] = number_format($volume,2);
			$response['total_weight'] = number_format($weight,2);



		}
		return json_encode($response);
	}

	public function edit_calculation() {
		$response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];

		if (Input::get('request_id') != '') {
			$request_data = Deliveryrequest::where(['_id' => trim(Input::get('request_id'))])
				->select('ProductList')
				->first();

			if (count($request_data) == 0 && !isset($request_data->ProductList)) {
				return json_encode($response);die;
			}

			$rate = 0;
			$insurance = 0;
			$weight = 0;
			$totalWeight = 0;
			$volume = 0;
			$distance = $this->get_distance(Input::get('calculated_distance'));
			$drop_lat_long = ['lat' => Input::get('DeliveryLat'), 'lng' => Input::get('DeliveryLong')];
			$distance_cal = Utility::GetDrivingDistance(Input::get('PickupLat'), Input::get('PickupLong'), Input::get('DeliveryLat'), Input::get('DeliveryLong'));

			if ($distance_cal == false) {
				$response['msg'] = 'Oops! We are unable to calculate distance.';
				return json_encode($response);die;
			} else {
				$distance = $this->get_distance($distance_cal->distance);
				$distance2 = $distance_cal->distance;
			}

			$drop_off_state = json_decode(Input::get('state11'));
			$response = ['success' => 0, 'shipping_cost' => '', 'shipping_cost' => '', 'ghana_total_amount' => '', 'formated_currency' => '', 'total_weight' => 0];
			$return_array = $request_data->ProductList;
			$inputData['consolidate_check'] = Input::GET('consolidate_check');
			$res['air_rate'] = 0;

			$set_air_category = $this->set_air_category($request_data->ProductList, $inputData);
			$ups_rate = 0;
			$total_ups_rate = 0;
			$total_custom_duty_rate = 0;
			$warehouse_transit_fee = 0;
			$is_ups_rate_applied = false;
			$is_customs_duty_applied = false;
			$total_productcost = 0;
			
			$pickup_country = json_decode(Input::get("country"));	// Pickup.
			$drop_off_country = json_decode(Input::get("drop_off_country"));	// Drop off.
			$destination_postal_code = Input::get("drop_off_zipcode");
			$pickup_postal_code = Input::get("zipcode");
			$pickup_state = json_decode(Input::get("state10"));
			$drop_off_state = json_decode(Input::get("state11"));
			$pickup_city = json_decode(Input::get("city"));
			// print_r($pickup_state);
			foreach ($request_data->ProductList as $key => $val) {
				$is_macth = false;
				// Calculate ups rate of each item.
				if ($pickup_country->name != $drop_off_country->name) {
					$weight = $val["productWeight"];
					$is_customs_duty_applied = true;
					if ($val["ProductWeightUnit"] == "kg") {
						$weight = $this->kgToLb($weight);
					}

					if ($pickup_country->name == "USA") {
						$params = array(
							"width" => $val["productWidth"],
							"height" => $val["productHeight"],
							"length" => $val["productLength"],
							"weight" => $weight,
							"pickupPostalCode" => $pickup_postal_code,
							"pickupStateProvinceCode" => @$pickup_state->short_name,
							"pickupCity" => $pickup_city->name,
							"pickupCountryCode" => "US",
							"destinationPostalCode" => "19977",
							"service"=> "03"
						);

						$ups = new upsRate();
						$ups->setCredentials();
						$ups_rate = $ups->getRate($params);
						$val["productUpsRate"] = $ups_rate;
						$total_ups_rate += $ups_rate;
						$is_ups_rate_applied = true;
					} else if ($drop_off_country->name == "USA") {
						if ($val['productCost'] <= 2000) {
							$is_customs_duty_applied = false;
						}

						$params = array(
							"width" => $val["productWidth"],
							"height" => $val["productHeight"],
							"length" => $val["productLength"],
							"weight" => $weight,
							"pickupPostalCode" => "19977",
							"pickupStateProvinceCode" => "DE",
							"pickupCity" => "Smyrna",
							"pickupCountryCode" => "US",
							"destinationPostalCode" => $destination_postal_code,
							"service"=> "03"
						);

						$ups = new upsRate();
						$ups->setCredentials();
						$ups_rate = $ups->getRate($params);
						$val["productUpsRate"] = $ups_rate;
						$total_ups_rate += $ups_rate;
						$is_ups_rate_applied = true;
					}
				}

				// UPS charge also applied in USA if state is diffrent.
				if ($pickup_country->name == "USA" && $drop_off_country->name == "USA") {
					if ($pickup_state->name != $drop_off_state->name) {
						$weight = $val["productWeight"];

						if ($val["ProductWeightUnit"] == "kg") {
							$weight = $this->kgToLb($weight);
						}

						$params = array(
							"width" => $val["productWidth"],
							"height" => $val["productHeight"],
							"length" => $val["productLength"],
							"weight" => $weight,
							"pickupPostalCode" => "19977",
							"pickupStateProvinceCode" => "DE",
							"pickupCity" => "Smyrna",
							"pickupCountryCode" => "US",
							"destinationPostalCode" => $destination_postal_code,
							"service"=> "03"
						);

						$ups = new upsRate();
						$ups->setCredentials();
						$ups_rate = $ups->getRate($params);
						$val["productUpsRate"] = $ups_rate;
						$total_ups_rate += $ups_rate;
						$is_ups_rate_applied = true;
					}
				}

				$return_array[$key]['shippingCost'] = 0;
				
				$return_array[$key]['categoryid'] = $val['productCategoryId'];
				$return_array[$key]['width'] = $val['productWidth'];
				$return_array[$key]['height'] = $val['productHeight'];
				$return_array[$key]['length'] = $val['productLength'];
				$return_array[$key]['weight'] = $val["productWeight"];
				$return_array[$key]['qty'] = $val['productQty'];
				$return_array[$key]['price'] = $val['productCost'];
				$return_array[$key]['insurance_status'] = $val['InsuranceStatus'];
				$return_array[$key]['weight_unit'] = $val["ProductWeightUnit"];
				$return_array[$key]['InsuranceCost'] = 0;
				$total_productcost += ($val['productCost']* $val['productQty']);
				// $weight2 = $this->get_weight($val['productWeight'], $val['ProductWeightUnit']);
				$weight2 = $this->GetVolumetricWeightInLbs($val["productWidth"], $val["productHeight"], $val["productLength"], $val["productWeight"], $val["ProductWeightUnit"]);
				// $weight += $weight2;
				$totalWeight += $weight*$val['productQty'];

				if ($val['InsuranceStatus'] == 'yes') {
					$return_array[$key]['InsuranceCost'] = $this->get_insurance($val['productCost'], $val['productQty']);
					$insurance += $return_array[$key]['InsuranceCost'];
				}

				$volume += $this->get_volume(array(
					"width" => $val['productWidth'],
					"widthunit" => $val['ProductLengthUnit'],
					"height" => $val['productHeight'],
					"heightunit" => $val['productHeightUnit'],
					"length" => $val['productLength'],
					"lengthunit" => $val['ProductLengthUnit'],
				))*$val['productQty'];

				// match with newRequesthelper
				$category = Category::where(['_id' => $val['productCategoryId'],'Status' => 'Active'])->first();
				// $return_array['category'] = $category;
				if (count($category) == 0) {
					$response['msg'] = 'Oops! category not found';
					return json_encode($response);die;
				}

				//get custom duty
				if (isset($category->custom_duty) && $is_customs_duty_applied) {
					$total_custom_duty_rate +=  (($val['productCost'] * (int) $val['productQty']) * $category->custom_duty)/100;
					$val['customDuty'] = (($val['productCost'] * (int) $val['productQty']) * $category->custom_duty)/100;
				}

				if ($category->ChargeType == 'distance') {
					$categoryConstantArray = array(AUTO_FULLSIZE_SUV, AUTO_INTERMEDIATE_SUV, AUTO_SEDAN_ECONOMY, AUTO_SEDAN_FULLSIZE, AUTO_SEDAN_INTERMEDIATE, AUTO_STANDARD_SUV, AUTOMOBILE_VAN);

					if ($val['travelMode'] == 'ship' && in_array($val['productCategoryId'], $categoryConstantArray)) {
						foreach ($category->Shipping as $key2) {
							if ($distance >= $key2['MinDistance'] && $distance <= $key2['MaxDistance']) {
								if ($inputData['consolidate_check'] == 'on') {
									$return_array[$key]['shippingCost'] = $key2['CONSOLIDATE_RATE'] * (int) $val['productQty'];
									$rate += $key2['CONSOLIDATE_RATE'] * (int) $val['productQty'];
								} else {
									$return_array[$key]['shippingCost'] = $key2['Rate'] * (int) $val['productQty'];
									$rate += $key2['Rate'] * (int) $val['productQty'];
								}
							}		
						}
					} else {
						$cat_count = $this->category_count($request_data->ProductList, $val['productCategoryId']);

						foreach ($category->Shipping as $key2) {
							if ($distance >= $key2['MinDistance'] && $distance <= $key2['MaxDistance']) {
								if ($inputData['consolidate_check'] == 'on') {
									$return_array[$key]['shippingCost'] = $key2['CONSOLIDATE_RATE'] / $cat_count['count'];
									$rate += $key2['CONSOLIDATE_RATE'] / $cat_count['count'];
								} else {
									$return_array[$key]['shippingCost'] = $key2['Rate'] / $cat_count['count'];
									$rate += $key2['Rate'] / $cat_count['count'];
								}
							}		
						}
					}
				} else if ($val['travelMode'] == 'air' && ($val['productCategoryId'] != OtherCategory && $val['productCategoryId'] != ElectronicsCategory)) {
					$air_rate = $this->get_category_price($set_air_category,$val['productCategoryId']);
					$cat_count = $this->category_count($request_data->ProductList,$val['productCategoryId']);
					$return_array[$key]['shippingCost'] = $air_rate / $cat_count['count'];
					$rate += $air_rate / $cat_count['count'];
				} else if ($val['travelMode'] == 'air' && ($val['productCategoryId'] == OtherCategory || $val['productCategoryId'] != ElectronicsCategory)) {
					foreach ($category->Shipping as $key2) {
						if ($distance >= $key2['MinDistance'] && $distance <= $key2['MaxDistance']) {
							$is_macth = true;
							$return_array[$key]['shippingCost'] = $key2['Rate'] * (int) $val['productQty'];
							$rate += $key2['Rate'] * (int) $val['productQty'];
						}

						$last_price = $key2['Rate'];		
					}
					if (!$is_macth && ($val['productCategoryId'] == OtherCategory || $val['productCategoryId'] == ElectronicsCategory)) {
						$itemWeight_1 = $weight2-2;
						$itemWeight_2 = intval($itemWeight_1/2);
						
						$rate_1 =  $itemWeight_2 * $category->weight_increase_by;
						$rate_2 = $rate_1 + $last_price;
						if($val['productCost']  > 1){
							$p = $val['productCost'] - 1;
							$return_array[$key]['shippingCost'] = $rate_2 + ($p * $category->price_increase_by) * (int) $val['productQty'];
							$rate += $rate_2 + ($p * $category->price_increase_by) * (int) $val['productQty'];
														
						}else{
							$rate += $rate_2 * (int) $val['productQty'];
							$return_array[$key]['shippingCost'] = $rate_2 * (int) $val['productQty'];
							
						}
								
						
					}
					
				}
				else{
					if (isset($category->Shipping) && is_array($category->Shipping)) {
						foreach ($category->Shipping as $key2) {
							if ($distance >= $key2['MinDistance'] && $distance <= $key2['MaxDistance']) {
								$return_array[$key]['shippingCost'] = $key2['Rate'] * (int) $val['productQty'];
								$rate += $key2['Rate'] * (int) $val['productQty'];
							}
							
						}
					}
					
				}
			}

			$response['success'] = 1;
			$DutyAndCustom = 0;

			if ($is_customs_duty_applied) {
				$DutyAndCustom = $total_custom_duty_rate;
			}

			$Tax = $this->get_tax($total_productcost);

			if ($is_ups_rate_applied) {
				$warehouse_transit_fee = $this->get_warehouse_transit_fee();
			}
			$data = [
				'shippingcost' => 0,
				'insurance' => 0,
				'ghana_total_amount' => 0,
				'total_amount' => 0,
				'error' => [],
				'formated_text' => '',
				'distance_in_mile' => 0,
				'product_count' => 0,
				'ProcessingFees' => 0,
				'item_cost' => 0,
				'shipping_cost_by_user' => 0,
				'type' => 'sap',
				'consolidate_check'=> (Input::get('consolidate_check') == 'on')? 'on':'off'
			];
			$data['product'] = $return_array;
			$data['distance'] = $distance_cal;
			$calculation = new NewCalculation();
			$userinfo = (Object) ['Default_Currency' => 'GHS'];
			// $response = $calculation->getRate($data['product'], $data, $userinfo);
			$res = $calculation->GetRate($return_array, $data, $userinfo, $drop_off_country->name);
			
			$response['totalUpsCharge'] = $total_ups_rate;
			$response['warehouse_transit_fee'] = $warehouse_transit_fee;
			$response['DutyAndCustom'] = $DutyAndCustom;
			$response['Tax'] = $Tax;
			$response['shipping_cost'] = $res['shippingCost']; //+ $total_ups_rate + $warehouse_transit_fee;
			$response['insurance'] = $insurance;
			$response['total_amount'] = $response['shipping_cost'] + $insurance + $DutyAndCustom + $Tax;
			$config = Configuration::find(CongigId);
			if ($drop_off_country->name === 'Kenya') {
				$response['DutyAndCustom'] = 0.0;
				$extra = 0;
				if ($total_productcost > 100) {
					$extra = ($total_productcost * $config->kenya_per_item_value)/100;
					$extra = number_format($extra, 2, '.', '');
				}
				// if ($data['weight_unit'] === 'kg') {
				// 	$ratePerKg = number_format(($data['weight'] * $config->kenya_rate_per_kilogram), 2, '.', '');
				// } else {
					$ratePerKg = number_format((($totalWeight * 0.453592) * $config->kenya_rate_per_kilogram), 2, '.', '');
				// }
				// echo $ratePerKg;
				$response['shipping_cost'] = $ratePerKg < $config->kenya_rate_per_kilogram ? 17 : $ratePerKg;
				// $resionCharges = $this->resionCharges(['name' => $state->name, 'id' => $state->id]);
				$response['total_amount'] = $response['shipping_cost'] + $insurance + $extra;
				// $response['total_amount_txt'] = 'resionCharges - ' . $resionCharges . 'ratePerKg - ' .  $ratePerKg . 'extra - ' .  $extra;
				if ($response['total_amount'] < $config->kenya_rate_per_kilogram) {
					$response['total_amount'] = $config->kenya_rate_per_kilogram;
				}
			}
			$response['product'] = $return_array;

			$response['AquantuoFees'] = $this->get_aquantuo_fees($response['shipping_cost']);

			//extraa charge of out side accra
			$response['regionCharges'] = $this->resionCharges(['name' => $drop_off_state->name, 'id' => $drop_off_state->id]);
			$response['total_amount'] = $response['total_amount'] + $response['regionCharges'];
			//end accra charge

			$currency_conversion = $this->currency_conversion($response['total_amount']);

			if (Session()->get('Default_Currency') == 'GHS') {
				$response['user_currency'] = $currency_conversion['ghanian_cost'];
				$response["user_currency_rate"] = $currency_conversion["currency_rate_ghs"];
				$response["FormatedText"] = $currency_conversion["FormatedText_ghs"];
			} else if (Session()->get('Default_Currency') == 'CAD') {
				$response['user_currency'] = $currency_conversion['canadian_cost'];
				$response["user_currency_rate"] = $currency_conversion["currency_rate_cad"];
				$response["FormatedText"] = $currency_conversion["FormatedText_cad"];
			} else if (Session()->get('Default_Currency') == 'PHP') {
				$response['user_currency'] = $currency_conversion['philipins_cost'];
				$response["user_currency_rate"] = $currency_conversion["currency_rate_php"];
				$response["FormatedText"] = $currency_conversion["FormatedText_php"];
			} else if (Session()->get('Default_Currency') == 'GBP') {
				$response['user_currency'] = $currency_conversion['uk_cost'];
				$response["user_currency_rate"] = $currency_conversion["currency_rate_gbp"];
				$response["FormatedText"] = $currency_conversion["FormatedText_gbp"];
			} else if (Session()->get('Default_Currency') == 'KES') {
				$response['user_currency'] = $currency_conversion['kenya_cost'];
				$response["user_currency_rate"] = $currency_conversion["currency_rate_kes"];
				$response["FormatedText"] = $currency_conversion["FormatedText_kes"];
			} else {
				$response['user_currency'] = $currency_conversion['ghanian_cost'];
				$response["user_currency_rate"] = $currency_conversion["currency_rate_ghs"];
				$response["FormatedText"] = $currency_conversion["FormatedText_ghs"];
			}

			// $response['currency_conversion'] = $currency_conversion;
			$response['distance'] = number_format($distance, 2);
			$response['volume'] = number_format($volume, 2);
			$response['total_weight'] = number_format($totalWeight, 2);

		}
		//print_r($response); die;
		return json_encode($response);
	}

	public function editItemInExistRequest(Request $request) {
		$response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];

		if (Input::get('request_id') != '' && Input::get('item_id') != '') {
			$request_data = Deliveryrequest::where(['_id' => trim(Input::get('request_id'))])
				->select('ProductList')->first();

			if (count($request_data) > 0) {
				$update_array = $request_data->ProductList;
				$ProductImage = '';
				$otherImages = [];
				$lhwunit = 'cm';
				$weightunit = 'kg';

				if (Input::get('measurement_unit') == 'inches_lbs') {
					$lhwunit = 'inches';
					$weightunit = 'lbs';
				}

				if (Input::hasFile('default_image')) {
					if (Input::file('default_image')->isValid()) {
						$ext = Input::file('default_image')->getClientOriginalExtension();
						$ProductImage = time() . rand(100, 9999) . ".$ext";
						if (Input::file('default_image')->move(BASEURL_FILE . 'package/', $ProductImage)) {
							$ProductImage = "package/$ProductImage";
						} else {
							$ProductImage = '';
						}
					}
				}

				$imageArray = array('package_image2', 'package_image3', 'package_image4', 'package_image5');
				foreach ($imageArray as $key => $val) {
					if (Input::hasFile($val)) {
						if (Input::file($val)->isValid()) {
							$ext = Input::file($val)->getClientOriginalExtension();
							$img = time() . rand(100, 9999) . ".$ext";

							if (Input::file($val)->move(BASEURL_FILE . 'package/', $img)) {
								$img = "package/$img";
								if (isset($otherImages[$key])) {
									// Start remove old image
									if (!empty(trim($otherImages[$key]))) {
										if (file_exists(BASEURL_FILE . $otherImages[$key])) {
											unlink(BASEURL_FILE . $otherImages[$key]);
										}
									}
									// End remove old image
									$otherImages[$key] = $img;
								} else {
									$otherImages[] = $img;
								}
							}
						}
					}
				}

				$category = json_decode(Input::get('category'));
				foreach ($request_data->ProductList as $key => $value) {
					if ($value['_id'] == trim(Input::get('item_id'))) {

						$update_array[$key]['product_name'] = Input::get('title');
						$update_array[$key]['productWidth'] = Input::get('width');
						$update_array[$key]['productHeight'] = Input::get('height');
						$update_array[$key]['productLength'] = Input::get('length');
						$update_array[$key]['productCost'] = trim(Input::get('package_value'));
						$update_array[$key]['productWeight'] = trim(Input::get('weight'));
						$update_array[$key]['productHeightUnit'] = trim($lhwunit);
						$update_array[$key]['ProductWeightUnit'] = trim($weightunit);
						$update_array[$key]['ProductLengthUnit'] = trim($lhwunit);
						$update_array[$key]['productCategory'] = $category->name;
						$update_array[$key]['productCategoryId'] = $category->id;
						$update_array[$key]['travelMode'] = trim(Input::get('travel_mode'));
						$update_array[$key]['InsuranceStatus'] = (Input::get('insurance') == 'yes') ? 'yes' : 'no';
						$update_array[$key]['productQty'] = (Input::get('quantity') < 1) ? 1 : Input::get('quantity');
						$update_array[$key]['ProductImage'] = ($ProductImage == '') ? $value['ProductImage'] : $ProductImage;
						//$update_array[$key]['OtherImage'] = (empty($otherImages))? $value['OtherImage'] : $otherImages;
						$update_array[$key]['QuantityStatus'] = (Input::get('quantity') > 1) ? 'multiple' : 'single';
						$update_array[$key]['BoxQuantity'] = (Input::get('quantity') == 0) ? 1 : (int) Input::get('quantity');
						$update_array[$key]['Description'] = Input::get('description');
						$update_array[$key]['PackageMaterial'] = (Input::get('need_package_material') == 'yes') ? 'yes' : 'no';
						$update_array[$key]['PackageMaterialShipped'] = "";

						if (Input::get('insurance') == 'yes') {
							$insurance = $this->get_insurance(Input::get('package_value'), Input::get('package_value'));
							if ($insurance == 0) {
								$response['msg'] = 'Sorry! We are not able to provide insurence.';
								return json_encode($response);
							}
						}

					}
				}

				// Stop user to add item with shipping mode sea but different category.
				$is_make_request = "yes";
				$item_with_sea = false;
				$item_with_air = false;

				if (count($request_data->ProductList) > 1) {
					foreach ($request_data->ProductList as $value) {
						if ($value['productCategoryId'] != @$category->id && $value['travelMode'] == "ship") {
							$is_make_request = "no";
							$item_with_sea = true;
						}

						if (Input::get('travel_mode') == "ship") {
							$item_with_sea = true;
						}

						if (Input::get('travel_mode') == "air" || $value['travelMode'] == "air") {
							$item_with_air = true;
						}

						if ($item_with_air && $item_with_sea) {
							$response["msg"] = "Sorry! You can’t have different shipping modes in the same listing. Please create a separate listing for either air or sea.";
							return json_encode($response);
							die();
						}
					}
				}
				
				if ($is_make_request == "no") {
					$response["msg"] = "If shipping by Sea, all items must be of the same sea category. Please create a new listing if you need to use a different sea category.";
					return json_encode($response);
					die();
				} else {
					$update_data = Deliveryrequest::where(['_id' => trim(Input::get('request_id'))])
					->update(['ProductList' => $update_array]);
					if ($update_data) {
						$response = ['success' => 1, 'msg' => 'Item has been updated successfully.'];
					}
				}
			}

		}
		return json_encode($response);
	}

	public function posteditItemInExistRequest(Request $request) {
		$response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];

		if (Input::get('item_id') != '') {
			if (Input::hasFile('default_image')) {
				if (Input::file('default_image')->isValid()) {
					$ext = Input::file('default_image')->getClientOriginalExtension();
					$ProductImage = time() . rand(100, 9999) . ".$ext";
					if (Input::file('default_image')->move(BASEURL_FILE . 'package/', $ProductImage)) {
						$updateDate['ProductImage'] = "package/$ProductImage";
					}
				}
			}

			$lhwunit = 'cm';
			$weightunit = 'kg';

			if (Input::get('measurement_unit') == 'inches_lbs') {
				$lhwunit = 'inches';
				$weightunit = 'lbs';
			}

			$category = json_decode(Input::get('category'));
			$updateDate['product_name'] = Input::get('title');
			$updateDate['productCost'] = Input::get('package_value');
			$updateDate['measurement_unit'] = Input::get('measurement_unit');
			$updateDate['productLength'] = Input::get('length');
			$updateDate['productWeight'] = Input::get('width');
			$updateDate['productHeight'] = Input::get('height');
			$updateDate['productWeight'] = Input::get('weight');
			$updateDate['productHeightUnit'] = $lhwunit;
			$updateDate['ProductWeightUnit'] = $weightunit;
			$updateDate['ProductLengthUnit'] = $lhwunit;
			$updateDate['quantity_type'] = Input::get('quantity_type');
			$updateDate['travel_mode'] = Input::get('travel_mode');
			$updateDate['travelMode'] = Input::get('travel_mode');
			$updateDate['productCategory'] = $category->name;
			$updateDate['productCategoryId'] = $category->id;
			$updateDate['description'] = Input::get('description');
			$updateDate['needInsurance'] = Input::get('insurance');
			$updateDate['InsuranceStatus'] = Input::get('insurance');
			$updateDate['need_package_material'] = Input::get('need_package_material');

			// Stop user to add item with shipping mode sea but different category.
			$is_make_request = "yes";
			$item_with_sea = false;
			$item_with_air = false;

			$shipItemList = Additem::where(array('user_id' => Auth::user()->_id, "type" => "delivery"))->select("productCategoryId")->get();	// In where clause "travelMode" => "ship", 

			if (count($shipItemList) > 1) {
				foreach ($shipItemList as $value) {
					if ($value->productCategoryId != @$category->id && $value->travelMode == 'ship') {
						$is_make_request = "no";
						$item_with_sea = true;
					}

					if (Input::get('travel_mode') == "ship") {
						$item_with_sea = true;
					}

					if (Input::get('travel_mode') == "air" || $value->travelMode == 'air') {
						$item_with_air = true;
					}

					if ($item_with_air && $item_with_sea) {
						$response["msg"] = "Sorry! You can’t have different shipping modes in the same listing. Please create a separate listing for either air or sea.";
						return response()->json($response);
						die();
					}
				}
			}
			
			if ($is_make_request == "no") {
				$response["msg"] = "If shipping by Sea, all items must be in the same category Please create a new listing for items meant for Air or different Sea category.";
				return response()->json($response);
				die();
			} else {
				$result = Additem::where('_id', '=', Input::get('item_id'))->update($updateDate);

				if (count($result) > 0) {
					$response = ['success' => 1, 'msg' => 'Item has been updated successfully.'];
				}
			}
		}
		return json_encode($response);
	}

	public function remove_item($id, $requestid) {
		$response = array('success' => 0, 'msg' => 'Fail to delete request.');

		if ($requestid) {
			$res_data = Deliveryrequest::where(['_id' => trim($requestid)])
				->select('ProductList')->first();
			if (count($res_data) > 0) {
				if (isset($res_data->ProductList)) {
					$update_array = $res_data->ProductList;
					foreach ($update_array as $key => $value) {
						if ($value['_id'] == $id) {
							unset($update_array[$key]);
						}
					}
					$update = Deliveryrequest::where(['_id' => trim($requestid)])
						->update(['ProductList' => $update_array]);
					if ($update) {
						$response['success'] = 1;
						$response['msg'] = 'Item has been removed successfully.';
					}
				}

			}
		}
		return json_encode($response);
	}

	public function PackagedeleteItem($id) {
		$result = Additem::where('_id', '=', $id)->delete();
		if (count($result) > 0) {
			return Redirect::to('admin/add-send-a-package')->withSuccess('item deleted Successfully.');
		} else {
			return Redirect::to('admin/add-send-a-package')->with('fail', 'Problem in delete.');
		}
	}

	public function addItemHistorySendPackage($data) {
		if (!empty($data['ProductList'])) {
			foreach ($data['ProductList'] as $key) {
				if ($key['_id'] == $data['item_id']) {

					$insertData = [
						'request_id' => $data['request_id'],
						'item_id' => $key['_id'],
						'product_name' => $key['product_name'],
						'package_id' => $key['package_id'],
						'status' => $key['status'],
						'productWidth' => $key['productWidth'],
						'productHeight' => $key['productHeight'],
						'productLength' => $key['productLength'],
						'productCost' => $key['productCost'],
						'productWeight' => $key['productWeight'],
						'productHeightUnit' => $key['productHeightUnit'],
						'ProductWeightUnit' => $key['ProductWeightUnit'],
						'ProductLengthUnit' => $key['ProductLengthUnit'],
						'productCategory' => $key['productCategory'],
						'productCategoryId' => $key['productCategoryId'],
						'travelMode' => $key['travelMode'],
						'InsuranceStatus' => $key['InsuranceStatus'],
						'productQty' => $key['productQty'],
						'ProductImage' => $key['ProductImage'],
						'QuantityStatus' => $key['QuantityStatus'],
						'Description' => $key['Description'],
						'PackageMaterial' => $key['PackageMaterial'],
						'PackageMaterialShipped' => $key['PackageMaterialShipped'],
						'shippingCost' => $key['shippingCost'],
						'InsuranceCost' => $key['InsuranceCost'],
						'DeliveryVerifyCode' => $key['DeliveryVerifyCode'],
					];

					Itemhistory::Insert($insertData);

				}
			}
		}
	}

	public function ExpectedDate() {
		$where = [
			"ProductList" => array('$elemMatch' => [
				'_id' => Input::get('item_id'),
			]),
		];
		//$date = new MongoDate(strtotime(str_replace('/', '-', Input::get('Expected_date'))));
		$date = get_utc_time(Input::get('Expected_date'));
		$result = Deliveryrequest::where($where)->update([
			'ProductList.$.ExpectedDate' => $date,
		]);

		if (count($result) > 0) {

			$request_info = Deliveryrequest::where($where)->select('RequesterId','ProductList.$')->first();
			if($request_info){
				$show_date = Input::get('Expected_date');
				if(isset($request_info->ProductList[0]['ExpectedDate']->sec)){
					$show_date = show_date(@$request_info->ProductList[0]['ExpectedDate']);
				}
				
				
				$requester = User::where(['_id'=>$request_info->RequesterId])->select('Email','Name')->first();
				if($requester){
					$Email = new NewEmail();
					$Email->send_mail('5b46fc44360d5c1df1e580bc', [
						"to" => $requester->Email,
						"replace" => [
							"[USERNAME]" => $requester->Name,
							'[DATE]'=> $show_date
						],
					]
					);
				}
			}
			
			if (Input::get('Requesttype') == 'buy_for_me') {
				return Redirect::to('admin/buy-for-me/detail/' . Input::get('request_id'))->withSuccess('Expected date has been updated successfully.');
			}
			if (Input::get('Requesttype') == 'delivery') {
				return Redirect::to('admin/package/detail/' . Input::get('request_id'))->withSuccess('Expected date has been updated successfully.');
			}
			if (Input::get('Requesttype') == 'online') {
				return Redirect::to('admin/online_package/detail/' . Input::get('request_id'))->withSuccess('Expected date has been updateDate successfully.');
			}
		}
	}

	public function packagereceivedall($id = '', Request $request) {

		$deliveryInfo = Deliveryrequest::where(array('_id' => $id))->first();
		$array = $deliveryInfo->ProductList;
		$i = 0;
		foreach ($array as $key) {
			if (in_array($key['_id'], Input::get('item_name'))) {
				$array[$i]['status'] = 'item_received';

				/* Activity Log */
				$insertactivity = [
					'request_id' => $id,
					'request_type' => $deliveryInfo->RequestType,
					'PackageNumber' => $deliveryInfo->PackageNumber,
					'item_id' => $key['_id'],
					'package_id' => $key['package_id'],
					'item_name' => $key['product_name'],
					'log_type' => 'request',
					'message' => 'item received.',
					'status' => 'item_received',
					'action_user_id' => Auth::user()->_id,
					'EnterOn' => new MongoDate(),
				];
				Activitylog::insert($insertactivity);
			}
			$i++;
		}

		$updation = Deliveryrequest::where('_id', '=', $id)
			->update(['ProductList' => $array]);
		Reqhelper::update_status2($deliveryInfo->_id);
		$user_data = User::where(array('_id' => $deliveryInfo->RequesterId))->first();

		if (count($user_data) > 0) {
			$Notification = new Notify();
			$Notification->setValue('title', trans('lang.ONLINE_REVIEWED_TITLE'));
			$Notification->setValue('message', sprintf(trans('lang.ONLINE_REVIEWED_MSG'), $deliveryInfo->ProductTitle));
			$Notification->setValue('type', 'online_request_accepted');
			$Notification->setValue('location', 'online_request_accepted');
			$Notification->setValue('locationkey', $id);

			$Notification->add_user($user_data->NotificationId, $user_data->DeviceType);
			$Notification->fire();

			$insNotification = array(
				array(
					"NotificationTitle" => 'Request reviewed',
					"NotificationShortMessage" => sprintf('Your request for "%s" has been reviewed.', $deliveryInfo->ProductTitle),
					"NotificationMessage" => sprintf('Your request for "%s" has been reviewed.', $deliveryInfo->ProductTitle),
					"NotificationType" => "online_request_accepted",
					"NotificationUserId" => array(new MongoId($user_data->_id)),
					"Date" => new MongoDate(),
					"GroupTo" => "User",
				),
			);

			Notification::Insert($insNotification);

			//Mail to user
			send_mail($deliveryInfo->RequestType == 'delivery' ? '5f6481e156783d77784a742c' : '57d7d4b37ac6f69c158b4569', [
				"to" => $user_data->Email,
				"replace" => [
					"[USERNAME]" => $user_data->Name,
					"[PACKAGETITLE]" => $deliveryInfo->ProductTitle,
					"[PACKAGEID]" => $deliveryInfo->PackageId,
					"[SOURCE]" => $deliveryInfo->PickupAddress,
					"[DESTINATION]" => $deliveryInfo->DeliveryFullAddress,
					"[PACKAGENUMBER]" => $deliveryInfo->PackageNumber,
					"[SHIPPINGCOST]" => $deliveryInfo->shippingCost,
					"[TOTALCOST]" => $deliveryInfo->TotalCost,
				],
			]
			);
		}
		return Redirect::to('admin/package/detail/' . $id)->withSuccess('Product received successfully.');
	}

}
