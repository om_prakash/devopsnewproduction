<?php namespace App\Modules\Admin\Controllers;

/*
 * Project: Aquantuo
 *
 */

use App\Http\Controllers\Controller;
use App\Http\Models\Activitylog;
use App\Http\Models\Admin;
use App\Http\Models\Appcontent;
use App\Http\Models\AppTutorial;
use App\Http\Models\Category;
use App\Http\Models\CityStateCountry;
use App\Http\Models\Client;
use App\Http\Models\Comment;
use App\Http\Models\UserComment;
use App\Http\Models\Deliveryrequest;
use App\Http\Models\Distance;
use App\Http\Models\Emailtemplate;
use App\Http\Models\Extraregion;
use App\Http\Models\FAQ;
use App\Http\Models\Item;
use App\Http\Models\Notification;
use App\Http\Models\Promocode;
use App\Http\Models\SilderImages;
use App\Http\Models\Support;
use App\Http\Models\Transaction;
use App\Http\Models\Trips;
use App\Http\Models\User;
use App\Http\Models\Webcontent;
use Auth;
use DateTime;
use Illuminate\Http\Request;
use Input;
use MongoDate;
use MongoId;
use MongoRegex;
use App\Http\Models\PaymentInfo;

class PaginationController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index() {
		echo "asdasdasd";
	}
	public function dashboard() {
		echo "asdasdasd";
	}

	public function payment_log($id)
	{
		$query = PaymentInfo::query();
		$query->where(['request_id'=>$id]);
		$data = $this->commonpaginationdata();
		$search_value = trim(Input::get('search_value'));

		if (Input::get('StartDate') != '') {
			$startdate = Input::get('StartDate');
			$query->where(['date'=> ['$gte'=>new MongoDate(strtotime(DateTime::createFromFormat('d/m/Y', $startdate)->format('d-m-Y')))]]);
		}
		if (Input::get('EndDate') != '') {
			$enddate = Input::get('EndDate');
			$query->where(['date'=> ['$lte'=>new MongoDate(strtotime(DateTime::createFromFormat('d/m/Y', $enddate)->format('d-m-Y 23:59:59')))]]);
		}


		$data['count'] = $query->count();
		$data['info'] = $query->skip($data['start'])->take($data['per_page'])->orderBy('_id', 'desc')->get();

		$data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;
		return view('Admin::list.pagination.payment_log', $data);
	}

	public function comment_list($id, Request $request) {

		$query = Comment::query();
		$data = $this->commonpaginationdata();

		$data['count'] = $query->where(['request_id' => $id])->count();
		$data['info'] = $query->where(['request_id' => $id])
			->skip($data['start'])
			->take($data['per_page'])
			->orderBy('_id', 'desc')
			->get();

		$data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;
		return view('Admin::list.pagination.comment_list', $data);
	}

	public function user_comment_list($id, Request $request) {

		$query = UserComment::query();
		$data = $this->commonpaginationdata();

		$data['count'] = $query->where(['request_id' => $id])->count();
		$data['info'] = $query->where(['request_id' => $id])
			->skip($data['start'])
			->take($data['per_page'])
			->orderBy('_id', 'desc')
			->get();
		
		$data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;
		return view('Admin::list.pagination.user_comment_list', $data);
	}

	public function local_delivery() {

		$query = Deliveryrequest::query();
		$countQuery = Deliveryrequest::query();
		$data = $this->commonpaginationdata();
		$search_value = trim(Input::get('search_value'));

		if (!empty($search_value)) {

			$query->Where('ProductTitle', '=', $search_value)
				->orwhere('PackageNumber', '=', $search_value)
				->orwhere('RequesterName', 'like', "%$search_value%");
		}

		if (!empty(Input::get('category'))) {
			$query->where('ProductList.productCategory', strtolower(Input::get('category')));
		}

		if (Input::get('status') != '') {
			$query->where('Status', '=', Input::get('status'));
		}

		if (Input::get('StartDate') != '') {
			$query->where('PickupDate', '>', new MongoDate(strtotime(str_replace('/', '-', Input::get('StartDate')))));
		}

		if (Input::get('EndDate') != '') {
			$query->where('DeliveryDate', '<', new MongoDate(strtotime(date('d-m-Y 23:59:59', strtotime(str_replace('/', '-', Input::get('EndDate')))))));
		}

		$data['count'] = $query->where('RequestType', '=', 'local_delivery')->count();
		$data['users'] = $query->where('RequestType', '=', 'local_delivery')->skip($data['start'])->take($data['per_page'])->orderBy('_id', 'Desc')->get();

		$data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;
		return view('Admin::list.pagination.local_delivery', $data);
	}

	public function item_value(Request $request) {

		$q = Distance::query();
		$data = $this->commonpaginationdata();

		$search_value = trim(Input::get('search_value'));
		$country = trim(Input::get('country'));

		if ($search_value != '') {
			$q->where('price', '=', (float) $search_value);
		}

		if ($country != '') {
			$q->where('country_id', '=', $country);
		}

		$data['country'] = CityStateCountry::where(array('type' => 'Country', 'Status' => 'Active'))->select('_id', 'Content', 'Status')->get();
		$data['count'] = $q->where('type', '=', 'item-value')->count();
		$data['distance'] = $q->where('type', '=', 'item-value')->skip($data['start'])->take($data['per_page'])->orderBy('_id', 'desc')->get();

		$data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;
		return view('Admin::list.pagination.itemvalue', $data);
	}

	public function distance(Request $request) {

		$q = Distance::query();
		$data = $this->commonpaginationdata();

		$search_value = trim(Input::get('search_value'));
		$country = trim(Input::get('country'));

		if ($search_value != '') {
			//$q->where('price', '=', (float) $search_value);
			$q->where(['price' => (float) $search_value]);
		}

		if ($country != '') {
			$q->where('country_id', '=', $country);
		}

		//print_r($search_value); die;
		$data['country'] = CityStateCountry::where(array('type' => 'Country', 'Status' => 'Active'))->select('_id', 'Content', 'Status')->get();
		$data['count'] = $q->where('type', '=', 'distance')->count();
		$data['distance'] = $q->where('type', '=', 'distance')->skip($data['start'])->take($data['per_page'])->orderBy('_id', 'desc')->get();

		$data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;

		return view('Admin::list.pagination.distance', $data);
	}

	public function localCategorylist() {

		$query = Category::query();
		$countQuery = Category::query();
		$data = $this->commonpaginationdata();
		$search_value = trim(Input::get('search_value'));

		if ($search_value != '') {

			$query->where('Content', 'like', "%$search_value%");
			$countQuery->where('Content', 'like', "%$search_value%");
		}
		if (Input::get('country') != '') {
			$query->where('country_id', '=', Input::get('country'));
			$countQuery->where('country_id', '=', Input::get('country'));
		}
		$data['category'] = $query->where(array('type' => 'localCategory'))->skip($data['start'])->take($data['per_page'])->orderBy('_id', 'desc')->get();
		$data['count'] = $countQuery->where(array('type' => 'localCategory'))->count();
		$data['country'] = CityStateCountry::where('type', '=', 'Country')->get();
		$data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;

		return view('Admin::list.pagination.localCategory', $data);
	}
	public function email_template() {
		$data = $this->commonpaginationdata();
		$data['email'] = Emailtemplate::skip($data['start'])->take($data['per_page'])->get();
		$data['count'] = Emailtemplate::count();
		$data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;
		return view('Admin::list.pagination.email_template', $data);
	}
	public function transporter() {

		$query = User::query();

		$data = $this->commonpaginationdata();
		$search_value = Input::get('search_value');

		if ($search_value != '') {
			$query->where('Name', 'like', "%$search_value%");

		}
		if (Input::get('transporter_type') != '') {
			$query->where('TransporterType', '=', Input::get('transporter_type'));

		}
		if (Input::get('search_email') != '') {
			$query->where('Email', 'like', "%" . Input::get('search_email') . "%");
		}

		if (Input::get('search_unit') != '') {
			$search = trim(Input::get('search_unit'));
			$query->where('UniqueNo', 'like', "%" . $search . "%");
		}

		$data['count'] = $query->whereIn('UserType', ['transporter', 'both'])->count();
		$data['users'] = $query->whereIn('UserType', ['transporter', 'both'])->skip($data['start'])->take($data['per_page'])->orderBy('_id', 'desc')->get();

		$data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;
		return view('Admin::list.pagination.transporter_list', $data);
	}
	public function address() {

		$query = User::query();

		$data = $this->commonpaginationdata();
		$search_value = Input::get('search_value');

		if ($search_value != '') {
			$query->where('Name', 'like', "%$search_value%");

		}
		if (Input::get('transporter_type') != '') {
			$query->where('TransporterType', '=', Input::get('transporter_type'));

		}
		if (Input::get('search_email') != '') {
			$query->where('Email', 'like', "%" . Input::get('search_email') . "%");

		}

		$data['count'] = $query->whereIn('UserType', ['transporter', 'both'])->count();
		$data['users'] = $query->whereIn('UserType', ['transporter', 'both'])->skip($data['start'])->take($data['per_page'])->orderBy('_id', 'desc')->get();

		$data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;
		return view('Admin::list.pagination.post_address_list', $data);
	}
	public function requester() {
		$q = User::query();
		$countq = User::query();
		$data = $this->commonpaginationdata();

		$search_value = Input::get('search_value');
		$city = json_decode(Input::get('City'));
		$state = json_decode(Input::get('State'));
		$country = json_decode(Input::get('Country'));

		$streat = trim(Input::get('streat'));
		if ($city != '') {
			$q->where('City', 'like', "%$city->name%");
		}

		if ($state != '') {
			$q->where('State', 'like', "%$state->name%");
		}

		if ($country != '') {
			$q->where('Country', 'like', "%$country->name%");
		}

		if ($streat != '') {
			$q->where('Street1', 'like', "%$streat%")
				->orwhere('Street2', 'like', '%streat%');
		}
		if ($search_value != '') {
			$q->where('Name', 'like', "%$search_value%");
			$countq->where('Name', 'like', "%$search_value%");
		}

		if (trim(Input::get('search_email')) != '') {
			$q->where('Email', 'like', "%" . Input::get('search_email') . "%");
			$countq->where('Email', 'like', "%" . Input::get('search_email') . "%");
		}

		if (Input::get('search_unit') != '') {

			$q->where('UniqueNo', 'like', "%" . trim(Input::get('search_unit')) . "%");
			$countq->where('UniqueNo', 'like', "%" . trim(Input::get('search_unit')) . "%");
		}

		$field = array('Name', 'NumberId', 'FirstName', 'LastName', 'Email', 'PhoneNo', 'Street1', 'Street2', 'ZipCode', 'EnterOn', 'Image', 'Country', 'State', 'City', 'TransporterStatus', 'RequesterStatus', 'AlternatePhoneNo', 'UniqueNo', 'delete_status');

		$data['users'] = $q->whereIn('UserType', ['requester', 'both'])->skip($data['start'])->take($data['per_page'])->orderBy('_id', 'desc')->get($field);
		$data['count'] = $countq->whereIn('UserType', ['requester', 'both'])->count();
		$data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;
		return view('Admin::list.pagination.requester', $data);
	}

	public function package_old() {

		$query = Deliveryrequest::query();
		$countQuery = Deliveryrequest::query();
		$data = $this->commonpaginationdata();
		//    $search_value     = trim(Input::get('search_value'));
		$orwhere = $where = array(
			"PickupCity" => array('$regex' => new MongoRegex("/" . Input::get('city') . "/i")),
			"PickupState" => array('$regex' => new MongoRegex("/" . Input::get('state') . "/i")),
			"PickupCountry" => array('$regex' => new MongoRegex("/" . Input::get('country') . "/i")),
			"ProductTitle" => array('$regex' => new MongoRegex("/" . Input::get('ProductName') . "/i")),
			"PackageNumber" => array('$regex' => new MongoRegex("/" . Input::get('Packageid') . "/i")),
			"ProductTitle" => array('$regex' => new MongoRegex("/" . Input::get('ProductName') . "/i")),
			"RequesterName" => array('$regex' => new MongoRegex("/" . Input::get('requester') . "/i")),
		);

		if (!empty(Input::get('category'))) {
			$orwhere['Category'] = $where['Category'] = strtolower(Input::get('category'));
		}
		/*  $search_value     = trim(Input::get('search_value'));
			        if($search_value != '') {
			        $query->where('ProductName','like',"%$search_value%")->orWhere('Packageid','like',"%$search_value%")->orWhere('country','like',"%$search_value%")->
			        orWhere('state','like',"%$search_value%")->orWhere('city','like',"%$search_value%")->get();
		*/

		if (Input::get('status') != '') {
			$orwhere['Status'] = $where['Status'] = Input::get('status');
			if (Input::get('status') == 'transporter' || Input::get('status') == 'requester') {
				$orwhere['Status'] = $where['Status'] = 'cancel';
				$orwhere['Status'] = $where['RejectBy'] = Input::get('status');
			}
		}

		if (Input::get('StartDate') != '') {
			$startdate = Input::get('StartDate');
			$orwhere['PickupDate']['$gte'] = $where['PickupDate']['$gte'] = new MongoDate(strtotime(DateTime::createFromFormat('d/m/Y', $startdate)->format('d-m-Y')));
		}
		if (Input::get('EndDate') != '') {
			$enddate = Input::get('EndDate');
			$orwhere['PickupDate']['$lte'] = $where['PickupDate']['$lte'] = new MongoDate(strtotime(DateTime::createFromFormat('d/m/Y', $enddate)->format('d-m-Y 23:59:59')));
		}

		$field = array('DeliveryVerifyCode', 'ProductTitle', 'DeliveryDate', 'RequesterName', 'Status', 'PickupDate',
			'RejectBy', 'PackageNumber', 'SenderName', 'PickupFullAddress', 'DeliveryFullAddress', 'RequestType', 'RequesterId', 'ProductList');

		$data['users'] = $query->where($where)->orwhere('Status', '=', Input::get('type'))->orwhere($orwhere)->skip($data['start'])->take($data['per_page'])
		//->orderBy('UpdateOn', 'desc')
			->orderBy('_id', 'desc')
			->get($field);

		$data['count'] = $countQuery->where($where)->orwhere('Status', '=', Input::get('type'))->orwhere($orwhere)->count();
		$data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;
		return view('Admin::list.pagination.package', $data);
	}

	public function package() {

		$query = Deliveryrequest::query();
		$countQuery = Deliveryrequest::query();
		$data = $this->commonpaginationdata();
		$search_value     = trim(Input::get('search_value'));
		
		if ($search_value != '') {
			$query->where('ProductTitle', 'like', "%$search_value%")
			->orWhere('DeliveryFullAddress', 'like', "%$search_value%")
			->orWhere('PackageNumber', 'like', "%$search_value%")
			->orWhere('RequesterName', 'like', "%$search_value%")->get();
		}

		if(Input::get('country') != ''){
			$country = json_decode(Input::get('country'));
			$query->where(['PickupCountry'=>array('$regex' => new MongoRegex("/" . $country->name . "/i"))]);
		}

		if(Input::get('state') != ''){
			$state = json_decode(Input::get('state'));
			$query->where(['PickupState'=>array('$regex' => new MongoRegex("/" . $state->name . "/i"))]);
		}

		if(Input::get('city') != ''){
			$city = json_decode(Input::get('city'));
			$query->where(['PickupCity'=>array('$regex' => new MongoRegex("/" . $city->name . "/i"))]);
		}

		if (Input::get('status') != '') {
			$query->where(['Status'=>Input::get('status') ]);
		}

		if (Input::get('StartDate') != '') {
			$startdate = Input::get('StartDate');
			$query->where(['PickupDate'=> ['$gte'=>new MongoDate(strtotime(DateTime::createFromFormat('d/m/Y', $startdate)->format('d-m-Y')))]]);
		}
		if (Input::get('EndDate') != '') {
			$enddate = Input::get('EndDate');
			$query->where(['PickupDate'=> ['$lte'=>new MongoDate(strtotime(DateTime::createFromFormat('d/m/Y', $enddate)->format('d-m-Y 23:59:59')))]]);
		}

		$query->where(['RequestType'=>'delivery']);

		$field = array('DeliveryVerifyCode', 'ProductTitle', 'DeliveryDate', 'RequesterName', 'Status', 'PickupDate','RejectBy', 'PackageNumber', 'SenderName', 'PickupFullAddress', 'DeliveryFullAddress', 'RequestType', 'RequesterId', 'ProductList');

		
		$data['count'] = $query->count();
		$data['users'] = $query->skip($data['start'])->take($data['per_page'])
		//->orderBy('UpdateOn', 'desc')
			->orderBy('_id', 'desc')
			->get($field);

		
		$data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;
		return view('Admin::list.pagination.package', $data);
	}

	public function package___() {
		$query = Deliveryrequest::query();
		$countQuery = Deliveryrequest::query();
		$data = $this->commonpaginationdata();
		$search_value = trim(Input::get('search_value'));

		$orwhere = $where = array(
			"PickupCity" => array('$regex' => new MongoRegex("/" . Input::get('city') . "/i")),
			"PickupState" => array('$regex' => new MongoRegex("/" . Input::get('state') . "/i")),
			"PickupCountry" => array('$regex' => new MongoRegex("/" . Input::get('country') . "/i")),
			"ProductTitle" => array('$regex' => new MongoRegex("/" . Input::get('ProductName') . "/i")),
			"PackageNumber" => array('$regex' => new MongoRegex("/" . Input::get('Packageid') . "/i")),
			"ProductTitle" => array('$regex' => new MongoRegex("/" . Input::get('ProductName') . "/i")),
			"RequesterName" => array('$regex' => new MongoRegex("/" . Input::get('requester') . "/i")),
		);

		if (!empty($search_value)) {

			$query->Where('ProductTitle', '=', $search_value)
				->orwhere('PackageNumber', '=', $search_value)
				->orwhere('RequesterName', 'like', "%$search_value%");
		}

		if (!empty(Input::get('category'))) {
			$orwhere['Category'] = $where['Category'] = strtolower(Input::get('category'));
		}

		if (Input::get('status') != '') {
			$query->where('Status', '=', Input::get('status'));
		}

		if (Input::get('StartDate') != '') {
			$query->where('PickupDate', '>', new MongoDate(strtotime(str_replace('/', '-', Input::get('StartDate')))));
		}

		if (Input::get('EndDate') != '') {
			$query->where('DeliveryDate', '<', new MongoDate(strtotime(date('d-m-Y 23:59:59', strtotime(str_replace('/', '-', Input::get('EndDate')))))));
		}

		$data['count'] = $query->where('RequestType', '=', 'delivery')->count();
		$data['users'] = $query->where('RequestType', '=', 'delivery')->skip($data['start'])->take($data['per_page'])->orderBy('_id', 'Desc')->get();

		$data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;
		return view('Admin::list.pagination.package', $data);
	}

	/*
		     * User For app content list
		     *
	*/
	public function app_content() {
		$query = Appcontent::query();
		$countQuery = Appcontent::query();
		$data = $this->commonpaginationdata();

		$search_value = Input::get('search_value');

		if ($search_value != '') {
			$query->where('UserName', 'like', "%$search_value%");
			$countQuery->where('UserName', 'like', "%$search_value%");
		}

		$data['appcontent'] = $query->where(array('Type' => 'app_page'))->skip($data['start'])->take($data['per_page'])->orderBy('_id', 'desc')->get(array('Title'));

		$data['count'] = $countQuery->where(array('Type' => 'app_page'))->count();
		$data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;

		return view('Admin::list.pagination.app_content', $data);
	}

	/*
		     * User For state list
		     *
	*/

	public function state_list() {
		$query = CityStateCountry::query();
		$countQuery = CityStateCountry::query();
		$data = $this->commonpaginationdata();
		$data['country'] = CityStateCountry::where(array('type' => 'Country'))->get();

		$search_value = Input::get('search_value');
		$countryName = Input::get('countryName');

		if ($search_value != '') {
			$query->where('Content', 'like', "%$search_value%");
			$countQuery->where('Content', 'like', "%$search_value%");
		}

		if ($countryName != '') {
			$query->where('SuperName', 'like', "%$countryName%");
			$countQuery->where('SuperName', 'like', "%$countryName%");
		}
		if (Input::get('country')) {
			$query->where(array('CountryId' => Input::get('country')));
			$countQuery->where(array('CountryId' => Input::get('country')));
		}
		$data['state'] = $query->where(array('type' => 'State'))->skip($data['start'])->take($data['per_page'])->orderBy('_id', 'desc')->get();
		/*$data['state'] = $query->where(array('content' => 'Uk'))->skip($data['start'])->take($data['per_page'])->orderBy('_id', 'desc')->get();
			        echo '<pre>';
		*/

		$data['count'] = $countQuery->where(array('type' => 'State'))->count();
		$data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;

		return view('Admin::list.pagination.state_list', $data);
	}

	/*
		     * User For city list
		     *
	*/

	public function city_list(Request $request) {

		$query = CityStateCountry::query();
		$countQuery = CityStateCountry::query();
		$data = $this->commonpaginationdata();

		$search_value = Input::get('search_value');
		$stateName = trim(Input::get('stateName'));

		if (Input::get('countryName') != '') {
			$countrydata = CityStateCountry::where('SuperName', 'like', "%" . Input::get('countryName') . "%")->get();
			$stateid = array();
			foreach ($countrydata as $key) {
				$stateid[] = (String) $key['_id'];
			}
			$query->whereIn('StateId', $stateid);
			$countQuery->whereIn('StateId', $stateid);
		}
		if ($search_value != '') {
			$query->where('Content', 'like', "%$search_value%");
			$countQuery->where('Content', 'like', "%$search_value%");
		}

		if ($stateName != '') {
			$query->where('SuperName', 'like', "%$stateName%");
			$countQuery->where('SuperName', 'like', "%$stateName%");
		}
		if (Input::get('id') != '') {
			$query->where(array('StateId' => Input::get('state')));
			$countQuery->where(array('StateId' => Input::get('state')));
		}
		if (!empty(trim(Input::get('state')))) {
			$query->where(array('StateId' => Input::get('state')));
		}

		$data['state'] = CityStateCountry::where(array('type' => 'State'))->get();
		$data['city'] = $query->where(array('type' => 'city'))->skip($data['start'])->take($data['per_page'])->orderBy('_id', 'desc')->get();
		if (!empty(trim(Input::get('state')))) {
			$countQuery->where(array('StateId' => Input::get('state')));
		}
		$data['count'] = $countQuery->where(array('type' => 'city'))->count();

		$data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;

		return view('Admin::list.pagination.city_list', $data);
	}

	/*
		     * User For web content list
		     *
	*/

	public function web_content() {
		$query = Webcontent::query();
		$countQuery = Webcontent::query();
		$data = $this->commonpaginationdata();

		$search_value = Input::get('search_value');

		if ($search_value != '') {

			$query->where('Title', 'like', "%$search_value%");
			$countQuery->where('Title', 'like', "%$search_value%");
		}

		$data['webContent'] = $query->skip($data['start'])->take($data['per_page'])->orderBy('_id', 'desc')->get();
		$data['count'] = $countQuery->count();
		$data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;

		return view('Admin::list.pagination.web_content', $data);
	}

	/*
		     * User For support list
		     *
	*/
	public function support() {
		$query = Support::query();
		$countQuery = Support::query();
		$data = $this->commonpaginationdata();

		$search_value = Input::get('search_value');
		$group = Input::get('group');
		$date = Input::get('StartDate');

		$where = array();

		if ($search_value != '') {
			$query->where('UserName', 'like', "%$search_value%");
			$countQuery->where('UserName', 'like', "%$search_value%");
		}
		//$query->groupBy('Token');
		$data['support'] = $query->where('Token', '>', 0)->skip($data['start'])->take((int) $data['per_page'])->orderBy('_id', 'desc')
			->get(array('Email', 'UserName', 'Query', 'title', 'Token', 'EnterOn'));

		$data['count'] = $countQuery->where('Token', '>', 0)->count();
		$data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;

		return view('Admin::list.pagination.support', $data);
	}

	public function support_view_more($id) {
		$query = Support::query();
		$countQuery = Support::query();
		$data = $this->commonpaginationdata();

		$data['id'] = $id;
		$data['support'] = $query->where(array('RepToken' => (int) $id))->skip($data['start'])->take($data['per_page'])->orderBy('_id', 1)->get();
		$data['count'] = $countQuery->where(array('RepToken' => (int) $id))->count();
		$data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;
		return view('Admin::list.pagination.support_view_more', $data);

	}

	/*
		     * User For transaction list
		     *
	*/
	public function transporter_transaction() {
		echo "hi";die;

		$query = Deliveryrequest::query();
		$countQuery = Deliveryrequest::query();
		$data = $this->commonpaginationdata();

		$search_value = Input::get('search_value');

		if ($search_value != '') {
			// $query->where('SendByName', 'like', "%$search_value%");
			// $countQuery->where('SendByName', 'like', "%$search_value%");
			//
			$query->where('RecieveByName', 'like', "%$search_value%");
			$query->orWhere('SendByName', 'like', "%$search_value%");

			$countQuery->where('RecieveByName', 'like', "%$search_value%");
			$countQuery->orWhere('SendByName', 'like', "%$search_value%");
		}

		if (Input::get('startdate') != '') {
			$query->where('PaymentDate', '>', new MongoDate(strtotime(Input::get('startdate'))));
			$countQuery->where('PaymentDate', '>', new MongoDate(strtotime(Input::get('startdate'))));
		}
		if (Input::get('endDate') != '') {
			$query->where('PaymentDate', '<', new MongoDate(strtotime(str_replace('/', '-', Input::get('enddDate') . " 23:59:59"))));
			$countQuery->where('PaymentDate', '<', new MongoDate(strtotime(str_replace('/', '-', Input::get('enddDate') . " 23:59:59"))));
		}
		if (Input::get('status') != '') {
			$query->where('PaymentStatus', '=', Input::get('status'));
			$countQuery->where('PaymentStatus', '=', Input::get('status'));
		}

		$query->where('PickupState', 'like', '%' . Input::get('state') . '%');
		$countQuery->where('PickupState', 'like', '%' . Input::get('state') . '%');

		$query->where('PickupCity', 'like', '%' . Input::get('city') . '%');
		$countQuery->where('PickupCity', 'like', '%' . Input::get('city') . '%');

		$field = array('ProductTitle', 'PackageId', 'TransporterName', 'RequesterName', 'TotalCost', 'DeliveryDate', 'PaymentStatus', 'PaymentDate', 'PickupCity', 'PickupState');
		$data['users'] = $query->whereIn('PaymentStatus', array('pending', 'capture'))->skip($data['start'])->take($data['per_page'])->orderBy('_id', 'desc')->get($field);
		$data['count'] = $countQuery->whereIn('PaymentStatus', array('done', 'capture'))->count();

		$data['no_of_paginations'] = ($data['count'] > $data['per_page']) ? ceil($data['count'] / $data['per_page']) : 0;
		return view('Admin::list.pagination.transporter_transaction', $data);
	}

	public function requester_transaction() {
		//echo 'oooooooooooooooooooooooooooooooooo'; die;
		$query = Transaction::query();
		$countQuery = Transaction::query();
		$data = $this->commonpaginationdata();

		$search_value = Input::get('search_value');

		$query->where('ToUserCity', 'like', "%" . Input::get('city') . "%");
		$countQuery->where('ToUserCity', 'like', "%" . Input::get('city') . "%");

		$query->where('ToUserState', 'like', "%" . Input::get('state') . "%");
		$countQuery->where('ToUserState', 'like', "%" . Input::get('state') . "%");

		if ($search_value != '') {
			$query->where('RecieveByName', 'like', "%$search_value%");
			$query->orWhere('SendByName', 'like', "%$search_value%");

			$countQuery->where('RecieveByName', 'like', "%$search_value%");
			$countQuery->orWhere('SendByName', 'like', "%$search_value%");
		}

		if (Input::get('startdate') != '') {
			$query->where('EnterOn', '>', new MongoDate(strtotime(str_replace('/', '-', Input::get('startdate')))));
		}
		if (Input::get('endDate') != '') {
			$query->where('EnterOn', '<', new MongoDate(strtotime(date('d-m-Y 23:59:59', strtotime(str_replace('/', '-', Input::get('endDate')))))));
		}

		$data['info'] = $query->skip($data['start'])->take($data['per_page'])->orderBy('_id', 'desc')->get(array('SendByName', 'ToUserName', 'Amount', 'EnterOn', 'ToUserCity', 'ToUserState'));
		$data['count'] = $countQuery->where('_id', '!=', Auth::user()->_id)->where('UserType', 'admin')->count();
		$data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;
		return view('Admin::list.pagination.transaction_history', $data);
	}

	/*
		     * User For app tutorail list
		     *
	*/
	public function app_tutorial() {
		$query = AppTutorial::query();
		$countQuery = AppTutorial::query();
		$data = $this->commonpaginationdata();
		$search_value = Input::get('search_value');

		if ($search_value != '') {
			$query->where('Title', 'like', "%$search_value%");
			$countQuery->where('Title', 'like', "%$search_value%");
		}

		$data['appTutorial'] = $query->skip($data['start'])->take($data['per_page'])->orderBy('_id', 'desc')->get();
		$data['count'] = $countQuery->count();
		$data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;
		return view('Admin::list.pagination.app_tutorial', $data);
	}

	/*
		     * category list
		     *
	*/
	public function category_list() {
		$query = Category::query();
		$countQuery = Category::query();
		$data = $this->commonpaginationdata();
		$search_value = Input::get('search_value');

		if ($search_value != '') {

			$query->where('Content', 'like', "%$search_value%");
			$countQuery->where('Content', 'like', "%$search_value%");
		}

		if (Input::get('TravelMode') != '') {
			$query->where('TravelMode', '=', Input::get('TravelMode'));
			$countQuery->where('TravelMode', '=', Input::get('TravelMode'));
		}
		$data['category'] = $query->where(array('type' => 'Category'))->skip($data['start'])->take($data['per_page'])->orderBy('_id', 'desc')->get();
		$data['count'] = $countQuery->where(array('type' => 'Category'))->count();
		$data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;

		return view('Admin::list.pagination.category_list', $data);
	}

	/*
		     * User country list
		     *
	*/
	public function country_list() {
		$query = CityStateCountry::query();
		$countQuery = CityStateCountry::query();
		$data = $this->commonpaginationdata();
		$search_value = Input::get('search_value');

		if ($search_value != '') {

			$query->where('Content', 'like', "%$search_value%");
			$countQuery->where('Content', 'like', "%$search_value%");
		}

		$data['country'] = $query->where(array('type' => 'Country'))->skip($data['start'])->take($data['per_page'])->orderBy('_id', 'desc')->get();
		$data['count'] = $countQuery->where(array('type' => 'Country'))->count();
		$data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;

		return view('Admin::list.pagination.country_list', $data);
	}

	/*
		     * User faq list
		     *
	*/
	public function faq_list() {
		$query = FAQ::query();
		$countQuery = FAQ::query();
		$data = $this->commonpaginationdata();
		$search_value = Input::get('search_value');

		if ($search_value != '') {
			$query->where('Question', 'like', "%$search_value%");
			$countQuery->where('Question', 'like', "%$search_value%");
		}
		$data['faq'] = $query->skip($data['start'])->take($data['per_page'])->orderBy('_id', 'desc')->get();
		$data['count'] = $countQuery->count();
		$data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;

		return view('Admin::list.pagination.faq_list', $data);
	}
	public function admin_user() {
		$query = Admin::query();
		$countQuery = Admin::query();
		$data = $this->commonpaginationdata();

		$search_value = Input::get('search_value');

		if ($search_value != '') {

			$query->where('UserName', 'like', "%$search_value%");
			$countQuery->where('UserName', 'like', "%$search_value%");
		}

		$data['user'] = $query->where('_id', '!=', Auth::user()->_id)->where('UserType', 'admin')->skip($data['start'])->take($data['per_page'])->orderBy('_id', 'desc')->get(array('UserName', 'UserEmail', 'UserStatus'));

		$data['count'] = $countQuery->where('_id', '!=', Auth::user()->_id)->where('UserType', 'admin')->count();
		$data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;

		return view('Admin::list.pagination.admin', $data);
	}

	/*
		     * User For create pagination
		     *
	*/
	public function commonpaginationdata() {
		$data['cur_page'] = $data['page'] = Input::get('page');
		$data['page'] -= 1;
		//$data['per_page'] = Auth::user()->PaginationLimit;
		$data['per_page'] = isset(Auth::user()->PerPage) ? Auth::user()->PerPage : 10; //Input::get('Pnum');
		$data['orderby'] = Input::get('orderby');
		$data['orderType'] = Input::get('orderType');
		$data['previous_btn'] = true;
		$data['next_btn'] = true;
		$data['first_btn'] = true;
		$data['last_btn'] = true;
		$data['sno'] = ($data['start'] = $data['page'] * $data['per_page']) + 1;

		return $data;
	}
	public function requester_transaction_history() {
		/*
			        Transaction::insert(array(
			        "UserId" => "",
			        "UserName" => "Ravi shukla",
			        "Description" => "Amount transfer for item delivery from indore to bhopal",
			        "Credit" => 100,
			        "Debit" => "",
			        "EnterOn" => New MongoDate(),
		*/
		$query = Transaction::query();
		$countQuery = Transaction::query();
		$data = $this->commonpaginationdata();

		$search_value = Input::get('search_value');

		if ($search_value != '') {
			$query->where('SendByName', 'like', "%$search_value%");
			$query->orWhere('ToUserName', 'like', "%$search_value%");

			$countQuery->where('SendByName', 'like', "%$search_value%");
			$countQuery->orWhere('ToUserName', 'like', "%$search_value%");
		}

		$field = array('UserName', 'Description', 'Credit', 'Debit', 'EnterOn');
		$data['info'] = $query->skip($data['start'])->take($data['per_page'])->orderBy('_id', 'desc')->get($field);
		$data['count'] = $countQuery->count();
		$data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;

		return view('Admin::list.pagination.requester_transaction_history', $data);
	}

	public function admin_notification() {
		$q = Notification::query();
		$countq = Notification::query();
		$data = $this->commonpaginationdata();
		$search_value = Input::get('search_value');

		if ($search_value != '') {
			//$q->where('UserName','like',"%$search_value%");
			//$countq->where('UserName','like',"%$search_value%");
		}
		$data['users'] = $q->where('GroupTo', '=', "Admin")->skip($data['start'])->take($data['per_page'])->orderBy('_id', 'desc')->get();
		$data['count'] = $countq->where('GroupTo', '=', "Admin")->count();
		$data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;

		return view('Admin::list.pagination.admin_notification', $data);
	}

	//Trip
	public function trip() {
		$query = Trips::query();
		$countQuery = Trips::query();
		$data = $this->commonpaginationdata();

		$search_value = Input::get('search_value');

		if ($search_value != '') {

			$query->where('Question', 'like', "%$search_value%");
			$countQuery->where('Question', 'like', "%$search_value%");
		}
		$data['trips'] = $query->where('TripType', 'individual')->skip($data['start'])->take($data['per_page'])->orderBy('_id', 'desc')->get();

		$data['count'] = $countQuery->where('TripType', 'individual')->count();
		$data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;
		return view('Admin::list.pagination.trips', $data);
	}

	public function trips_request($id, Request $request) {
		if (strlen($id) == 24) {
			$id = new MongoId($id);
		}
		$query = Deliveryrequest::query();
		$countQuery = Deliveryrequest::query();
		$data = $this->commonpaginationdata();
		$data['users'] = $query->where('TripId', $id)->skip($data['start'])->take($data['per_page'])->orderBy('_id', 'desc')->get();
		$data['count'] = $countQuery->where('TripId', $id)->count();
		$data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;
		return view('Admin::list.pagination.trips_request', $data);
	}

	public function business_trip() {
		$query = Trips::query();
		$countQuery = Trips::query();
		$data = $this->commonpaginationdata();

		$search_value = Input::get('search_value');

		if ($search_value != '') {
			$query->where('Question', 'like', "%$search_value%");
			$countQuery->where('Question', 'like', "%$search_value%");
		}
		$data['trips'] = $query->where('TripType', 'business')->skip($data['start'])->take($data['per_page'])->orderBy('_id', 'desc')->get();
		$data['count'] = $countQuery->where('TripType', 'business')->count();
		$data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;
		return view('Admin::list.pagination.business_trip', $data);
	}

	public function business_request($id, Request $request) {
		$query = Deliveryrequest::query();
		$countQuery = Deliveryrequest::query();
		$data = $this->commonpaginationdata();
		$data['users'] = $query->where('TripId', $id)->skip($data['start'])
			->take($data['per_page'])->orderBy('_id', 'desc')->get();
		$data['count'] = $countQuery->where('TripId', $id)->count();
		$data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;
		return view('Admin::list.pagination.business_request', $data);
	}
	public function update_perpage() {
		$userData = Auth::user();
		$save = Admin::find($userData->_id);
		$save->PerPage = Input::get('Pnum');
		$save->save();
	}

	public function online_package(Request $request) {
		$query = Deliveryrequest::query();
		$countQuery = Deliveryrequest::query();
		$data = $this->commonpaginationdata();
		$search_value = trim(Input::get('search_value'));

		$orwhere = $where = array(
			"RequestType" => "online",
			//"ProductList.package_id"     => array('$regex' => new  MongoRegex("/".Input::get('Packageid')."/i")),
			//"ProductTitle"         => array('$regex' => new  MongoRegex("/".Input::get('ProductName')."/i")),
			//"DeliveryFullAddress"  => array('$regex' => new  MongoRegex("/".Input::get('address')."/i")),

			// "ProductList.package_id"     => array('$regex' => new  MongoRegex("/".Input::get('search_value')."/i")),
			// "ProductTitle"         =>  array('$regex' => new  MongoRegex("/".Input::get('search_value')."/i")),
			// "DeliveryFullAddress"  => array('$regex' => new  MongoRegex("/".Input::get('search_value')."/i")),

		);

		if ($search_value != '') {
			$query->where('ProductTitle', 'like', "%$search_value%")
				->orWhere('DeliveryFullAddress', 'like', "%$search_value%")
				->orWhere('ProductList.package_id', 'like', "%$search_value%")
				->orWhere('RequesterName', 'like', "%$search_value%")
				->orWhere('Status', '=', Input::get('status'))->get();
		}

		/*    if(!empty(Input::get('category'))) {
			        //    $orwhere['Category'] = $where['Category']  = strtolower(Input::get('category'));
			        $query->where('ProductList.category','=',strtolower(Input::get('category')))->get();
		*/
		if (Input::get('category') != '') {
			$query->where('ProductList.category', '=', Input::get('category'))->get();
		}

		if (Input::get('status') != '') {

			$orwhere['Status'] = $where['Status'] = Input::get('status');
			if (Input::get('status') == 'transporter' || Input::get('status') == 'requester') {
				$orwhere['Status'] = $where['Status'] = 'cancel';
				$orwhere['Status'] = $where['RejectBy'] = Input::get('status');
			}
		}

		if (Input::get('StartDate') != '') {
			$startdate = Input::get('StartDate');

			$orwhere['EnterOn']['$gte'] = $where['EnterOn']['$gte'] = new MongoDate(strtotime(DateTime::createFromFormat('d/m/Y', $startdate)->format('d-m-Y')));
		}

		$field = array('DeliveryVerifyCode', 'ProductTitle', 'DeliveryDate', 'RequesterName', 'DeliveryDate', 'Status', 'PickupDate', 'RejectBy', 'PackageNumber', 'SenderName', 'PickupFullAddress', 'DeliveryFullAddress', 'ProductList', 'RequesterId', 'RequestType', 'EnterOn', 'category', 'UpdateOn', 'updated_at', 'Userstatus');

		$data['count'] = $query->where($where)->count();
		$data['users'] = $query->where($where)
			->skip($data['start'])
			->take($data['per_page'])
			->orderBy('EnterOn', 'desc')
			->get($field);

		$data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;
		return view('Admin::list.pagination.online_package', $data);

	}

	public function buy_for_me() {

		$query = Deliveryrequest::query();
		$data = $this->commonpaginationdata();
		$search_value = trim(Input::get('search_value'));
		$orwhere = $where = array("RequestType" => "buy_for_me");

		if ($search_value != '') {
			$query->where('ProductTitle', 'like', "%$search_value%")
				->orWhere('DeliveryFullAddress', 'like', "%$search_value%")
				->orWhere('ProductList.package_id', 'like', "%$search_value%")
				->orWhere('RequesterName', 'like', "%$search_value%")
				->orWhere('Status', '=', Input::get('status'));
		}

		if (Input::get('category') != '') {
			$query->where('ProductList.category', '=', Input::get('category'))->get();
		}

		if (Input::get('status') != '') {
			$orwhere['Status'] = $where['Status'] = Input::get('status');
			if (Input::get('status') == 'transporter' || Input::get('status') == 'requester') {
				$orwhere['Status'] = $where['Status'] = 'cancel';
				$orwhere['Status'] = $where['Status'] = Input::get('status');
			}
		}

		if (Input::get('StartDate') != '') {
			$startdate = Input::get('StartDate');
			$orwhere['DeliveryDate']['$gte'] = $where['DeliveryDate']['$gte'] = new MongoDate(strtotime(DateTime::createFromFormat('d/m/Y', $startdate)->format('d-m-Y')));
		}
		if (Input::get('EndDate') != '') {
			$enddate = Input::get('EndDate');
			$orwhere['EnterOn']['$lte'] = $where['EnterOn']['$lte'] = new MongoDate(strtotime(DateTime::createFromFormat('d/m/Y', $enddate)->format('d-m-Y 23:59:59')));
		}
		$field = array('DeliveryVerifyCode', 'ProductTitle', 'RequesterName', 'DeliveryDate', 'Status', 'PickupDate', 'RejectBy', 'PackageNumber', 'SenderName', 'PickupFullAddress', 'DeliveryFullAddress', 'RequesterId', 'EnterOn', 'ProductList', 'category', 'UpdateOn');

		$data['count'] = $query->where($where)->count();
		$data['users'] = $query->where($where)
			->skip($data['start'])
			->take($data['per_page'])
			->orderBy('_id', 'desc')
			->get($field);

		$data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;
		return view('Admin::list.pagination.buy-for-me', $data);
	}

	public function promocode() {
		$query = Promocode::query();
		$countq = Promocode::query();
		$data = $this->commonpaginationdata();
		$where = array();

		if (Input::get('code') != '') {
			$query->where('Code', 'like', "%" . Input::get('code') . "%");
			$countq->where('Code', 'like', "%" . Input::get('code') . "%");
		}
		if (Input::get('validfrom') != '') {
			/*$where['ValidFrom']['$gte'] = new MongoDate(strtotime(DateTime::createFromFormat('d/m/Y H:i:s',Input::get('validfrom').' 00:00:00')->format('d-m-Y')));*/
			$where['ValidFrom']['$gte'] = get_utc_time(Input::get('validfrom') . ' 00:00:00', false, 'M d, Y H:i:s');
		}
		if (Input::get('validtill') != '') {
			/*$where['ValidTill']['$lte'] = new MongoDate(strtotime(DateTime::createFromFormat('d/m/Y H:i:s', Input::get('validtill').' 23:59:59')->format('d-m-Y')));*/
			$where['ValidTill']['$lte'] = get_utc_time(Input::get('validtill') . ' 23:59:59', false, 'M d, Y H:i:s');
		}

		$field = array('Title', 'Code', 'MinimumOrderPrice', 'MaximumDiscount', 'DiscountAmount', 'DiscountType', 'ValidFrom', 'ValidTill', 'MaxUsesPerPerson', 'MaxUses', 'Status', 'UsesCount');

		$data['users'] = $query->where($where)->skip($data['start'])->take($data['per_page'])->orderBy('_id', 'desc')->get($field);
		$data['count'] = $countq->where($where)->count();
		$data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;

		return view('Admin::list.pagination.promocode', $data);

	}

	public function sliderImages() {
		$q = SilderImages::query();

		$data = $this->commonpaginationdata();

		$search_value = trim(Input::get('search_value'));

		if ($search_value != '') {
			$q->where('sliderContant', 'like', "%$search_value%");
		}

		$data['count'] = $q->count();

		$data['users'] = $q->skip($data['start'])->take($data['per_page'])->orderBy('_id', 'desc')->get();

		$data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;

		return view('Admin::list.pagination.images', $data);
	}

	public function client(Request $request) {
		$q = Client::query();
		$data = $this->commonpaginationdata();

		$search_value = trim(Input::get('search_value'));

		if ($search_value != '') {
			$q->where('name', 'like', "%$search_value%");
		}

		$data['count'] = $q->count();
		$data['client'] = $q->skip($data['start'])->take($data['per_page'])->orderBy('_id', 'desc')->get();

		$data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;

		return view('Admin::list.pagination.client', $data);
	}

	public function item() {
		$query = Item::query();
		$countQuery = Item::query();
		$data = $this->commonpaginationdata();
		$search_value = Input::get('search_value');

		if ($search_value != '') {

			$query->where('item_name', 'like', "%$search_value%");
			$countQuery->where('item_name', 'like', "%$search_value%");
		}

		$data['item'] = $query->skip($data['start'])->take($data['per_page'])->orderBy('_id', 'desc')->get();
		$data['count'] = $countQuery->count();
		$data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;

		return view('Admin::list.pagination.item', $data);
	}

	public function accraManagement(Request $request) {

		$q = Extraregion::query();
		$data = $this->commonpaginationdata();

		$search_value = trim(Input::get('search_value'));
		$country = json_decode(Input::get('country'));
		$state = json_decode(Input::get('state'));

		if ($search_value != '') {
			$q->where('amount', '=', (float) $search_value);
		}

		if ($country != '') {
			$q->where('country', '=', $country->name);
		}

		if ($state != '') {
			$q->where('state', '=', $state->name);
		}

		$data['country'] = CityStateCountry::where(['CountryId' => '56bdbfb4cf32079714ee89ca', 'type' => 'State', 'Status' => 'Active'])
			->get();

		$data['count'] = $q->count();
		$data['accracharge'] = $q->skip($data['start'])->take($data['per_page'])->orderBy('_id', 'desc')->get();
		$data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;
		return view('Admin::list.pagination.accracharge', $data);
	}

	public function activity(Request $request) {

		$q = Activitylog::query();
		$data = $this->commonpaginationdata();

		$package_id = trim(Input::get('package_id'));
		$request_id = trim(Input::get('request_id'));
		$requestType = Input::get('request_type');
		$status = Input::get('status');

		if ($package_id != '') {
			$q->where('package_id', '=', $package_id);
		}

		if ($status != '') {
			$q->where('status', '=', $status);
		}

		if ($request_id != '') {
			$q->where('request_id', '=', $request_id);
		}

		if ($requestType != '') {
			$q->where('request_type', '=', $requestType);
		}

		if (Input::get('StartDate') != '') {
			$q->where('EnterOn', '>', new MongoDate(strtotime(str_replace('/', '-', Input::get('StartDate')))));
		}
		if (Input::get('EndDate') != '') {
			$q->where('EnterOn', '<', new MongoDate(strtotime(date('d-m-Y 23:59:59', strtotime(str_replace('/', '-', Input::get('EndDate')))))));
		}

		$data3['count'] = $data['count'] = $q->count();
		$result = $q->skip($data['start'])->take($data['per_page'])->orderBy('_id', 'desc')
		// ->with('userInfo')
			->get();

		$data2 = [];
		for ($i = 0; $i < count($result); $i++) {
			$usr = Deliveryrequest::where('_id', new MongoId($result[$i]['request_id']))->first(['RequesterName']);
			$data2[$i] = $result[$i];
			if (count($usr) > 0) {
				$data2[$i]['RequesterName'] = $usr->RequesterName;
			} else {
				$data2[$i]['RequesterName'] = "Admin";
			}

		}
		$data['activity'] = $data2;
		$userInfo = User::get(['_id','Name']);
		$adminInfo = Admin::get(['_id','UserName']);
		foreach ($result as $key) {
			$key->action_user_info = $this->getInfoOfActionUser(@$key->action_user_id,$userInfo,$adminInfo);
		}

		$data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;

		return view('Admin::list.pagination.activity', $data);
	}

	public function getInfoOfActionUser($id,$userInfo,$adminInfo)
	{	
		$outputarray = [
			'Name'=>''
		];
		if($id != '' | $id != null){
			foreach ($userInfo as $key) {
				if($id == $key->_id){
					$outputarray['Name'] = $key->Name;
				}
			}

			if($outputarray['Name'] == ''){
				foreach ($adminInfo as $key2) {
					if($id == $key2->_id){
						$outputarray['Name'] = $key2->UserName.' (admin)';
					}
				}
			}
		}
		return $outputarray;
	}

}
