<?php
namespace App\Modules\Admin\Controllers;

/*
 * Project: Aquantuo
 *
 */

use App\Http\Controllers\Controller;
use App\Http\Models\Activitylog;
use App\Http\Models\Additem;
use App\Http\Models\Address;
use App\Http\Models\Admin;
use App\Http\Models\Appcontent;
use App\Http\Models\AppTutorial;
use App\Http\Models\Category;
use App\Http\Models\CityStateCountry;
use App\Http\Models\Client;
use App\Http\Models\Configuration;
use App\Http\Models\Currency;
use App\Http\Models\Deliveryrequest;
use App\Http\Models\Distance;
use App\Http\Models\Emailtemplate;
use App\Http\Models\Extraregion;
use App\Http\Models\FAQ;
use App\Http\Models\Item;
use App\Http\Models\Itemhistory;
use App\Http\Models\LocalNotificationTime;
use App\Http\Models\Notification;
use App\Http\Models\Package;
use App\Http\Models\Promocode;
use App\Http\Models\SendMail;
use App\Http\Models\Setting;
use App\Http\Models\SilderImages;
use App\Http\Models\State;
use App\Http\Models\Support;
use App\Http\Models\Transaction;
use App\Http\Models\Trips;
use App\Http\Models\User;
use App\Http\Models\Webcontent;
use App\Library\Buinessdays;
use App\Library\export_xls;
use App\Library\Notification\Pushnotification;
use App\Library\Notify;
use App\Library\Reqhelper;
use App\Library\Requesthelper;
use App\Library\RSStripe;
use App\Library\Stripe;
use App\Library\Utility;
use App\Library\Utility as util;
use App\Modules\User\Controllers\Calculation;
use Auth;
use DateTime;
use DB;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Image;
use Input;
use MongoDate;
use MongoId;
use Session;
use App\Http\Models\PaymentInfo;
use App\Http\Models\Comment;
use App\Library\NewEmail;
use App\Library\SapBuinessdays;

class PageController extends Controller {

	public function __construct() {
		if (!(Auth::user())) {
			Redirect::to('admin/login')->send();
		}
	}

	public function dashboard() {
		if (Input::get("year")) {
			$year = Input::get("year");
		} else {
			$year = date("Y");
		}

		$startDate = new MongoDate(strtotime($year."-01-01 00:00:00"));
		$stopDate = new MongoDate(strtotime($year."-12-31 00:00:00"));

		$data['usercount'] = User::whereBetween("EnterOn", array($startDate, $stopDate))->count();
		$data['canceldelivery'] = Deliveryrequest::where('Status', 'cancel')->whereBetween("EnterOn", array($startDate, $stopDate))->count();
		$data['completedelivery'] = Deliveryrequest::where('Status', 'delivered')->whereBetween("EnterOn", array($startDate, $stopDate))->count();
		$data['totalpayment'] = Transaction::where("Status", "complete")->whereBetween("EnterOn", array($startDate, $stopDate))->sum('Credit');

		$where["momo_id"]['$gt'] = 0;
		$where["Status"]['$in'] = ["complete", "completed"];
		$data["totalGhaniPayment"] = Transaction::where($where)->whereBetween("EnterOn", array($startDate, $stopDate))->sum("Credit");
		$data["pendingDelivery"] = Deliveryrequest::where("Status", "=", "pending")->whereBetween("EnterOn", array($startDate, $stopDate))->count();
		$data["runningDelivery"] = Deliveryrequest::whereNotIn("Status", ["delivered", "cancel", "cancel_by_admin", "pending"])->whereBetween("EnterOn", array($startDate, $stopDate))->count();

		$data['pending'] = Deliveryrequest::where('Status', '=', 'accepted')->count();
		$data['out_for_delivery'] = Deliveryrequest::where('Status', '=', 'pending')->count();
		$data['ready'] = Deliveryrequest::where('Status', '=', 'pending')->count();
		$data['on_delivery'] = Deliveryrequest::where('Status', '=', 'ready')->count();
		$data['delivered'] = Deliveryrequest::where('Status', '=', 'delivered')->count();
		$data['no_delivery'] = Deliveryrequest::where('Status', '=', 'no_delivery')->count();

		$data['totalItems'] = Deliveryrequest::whereNotIn('Status', ['cancel','cancel_by_admin'])
		->where(array("ProductList" => array('$elemMatch' => array('status' => 'delivered'))))
		->select('ProductList')->get();
		$itemcount = 0;
		$pr_list =[];

		foreach ($data['totalItems'] as $key ) {
			foreach ($key->ProductList as $value) {
				if($value['status'] == 'delivered'){
					$itemcount = $itemcount + 1;
				}
				
			}
		}

		$data["totalLocalDeliveries"] = Deliveryrequest::where("RequestType", "=", "local_delivery")->whereBetween("EnterOn", array($startDate, $stopDate))->count();
		$data["totalOnlinePurchase"] = Deliveryrequest::where("RequestType", "=", "online")->whereBetween("EnterOn", array($startDate, $stopDate))->count();
		$data["totalBuyForMe"] = Deliveryrequest::where("RequestType", "=", "buy_for_me")->whereBetween("EnterOn", array($startDate, $stopDate))->count();
		$data["totalSendAPackage"] = Deliveryrequest::where("RequestType", "=", "delivery")->whereBetween("EnterOn", array($startDate, $stopDate))->count();
		
		$data['deliver_items'] = $itemcount;

		// Year drop-down.
		$data["yearsArray"] = collect(range(7, 0))->map(function ($item) {
			return (string) date('Y') - $item;
		});

		return view('Admin::other.dashboard', $data);
	}

	public function transporter() {
		if (!(Auth::user()->UserPermission & CARRIER) == CARRIER) {
			return Redirect::to('/admin/dashboard');
		}
		$data['paginationurl'] = "pagination/transporter-list";
		$data['orderType'] = "Desc";
		$data['orderby'] = "UserId";
		$data['postvalue'] = "&search_value=" . Input::get('search_value') . "&transporter_type=" . Input::get('transporter_type') . "&City=" . Input::get('City') . "&State=" . Input::get('State') .
		"&Country=" . Input::get('Country');

		$data['country'] = CityStateCountry::where(['type' => 'Country', 'Status' => 'Active'])
			->get(['_id', 'Content', 'state_available']);
		return view('Admin::list.transporter_list', $data);
	}
	public function requester_transaction_history() {

		if (!(Auth::user()->UserPermission & TRANSACTION) == TRANSACTION) {
			return Redirect::to('/admin/dashboard');
		}
		$data['paginationurl'] = "pagination/requester-transaction-history";
		$data['orderType'] = "desc";
		$data['orderby'] = "_id";
		$data['postvalue'] = "&search_value=" . Input::get('search_value') . "&startdate=" . Input::get('StartDate') . "&endDate=" . Input::get('EndDate')
		. "&city=" . Input::get('city') . "&state=" . Input::get('state');
		return view('Admin::list.requester-transaction-history', $data);
	}

	public function edit_transporter($id) {
		if (!(Auth::user()->UserPermission & CARRIER) == CARRIER) {
			return Redirect::to('/admin/dashboard');
		}
		$data['info'] = User::find($id);
		if (count($data['info']) > 0) {
			$data['state'] = CityStateCountry::where(array('type' => array('$in' => array('State', 'Country'))))->get(array('Content', 'type'));

			return view('Admin::edit.transporter', $data);
		} else {
			return Redirect::to('admin/transporter')->with('fail', 'Invalid user id.');
		}
	}
	public function post_edit_transporter($id, Request $request) {
		if (!(Auth::user()->UserPermission & CARRIER) == CARRIER) {
			return Redirect::to('/admin/dashboard');
		}
		$v = Validator::make($request->all(),
			array('countrycode' => 'required|numeric', 'firstname' => 'required|regex:/^[A-Za-z \t]*$/i',
				'lastname' => 'required|regex:/^[A-Za-z \t]*$/i', 'state' => 'required', 'city' => 'required',
				'phone' => 'regex:/^[+0-9\- \t]*$/i', 'country' => 'required',
				'zip' => 'regex:/^[A-Za-z0-9]*$/i', 'address1' => 'regex:/^[0-9A-Za-z- \/\s]*$/i', 'address2' => 'regex:/^[0-9A-Za-z- \/\s]*$/i'));
		if ($v->fails()) {
			return Redirect::to("admin/edit-transporter/$id")->withErrors($v)->withInput();
		} else {
			$save = User::find($id);
			$save->FirstName = Input::get('firstname');
			$save->LastName = Input::get('lastname');
			$save->Name = ucfirst(Input::get('firstname')) . " " . Input::get('lastname');
			$save->CountryCode = Input::get('countrycode');
			$save->PhoneNo = Input::get('phone');
			$save->Street1 = Input::get('address1');
			$save->Street2 = Input::get('address2');
			$save->State = Input::get('state');
			$save->Country = Input::get('country');
			$save->City = Input::get('city');
			$save->ZipCode = Input::get('zip');
			$save->SSN = Input::get('ssn');

			$save->save();
			return Redirect::to('admin/transporter')->withSuccess('User updated successfully.');
		}

	}

	public function requester() {
		/*$where = [
			        "NotificationUserId" => ['$elemMatch'=>[ '$in'=> [  new MongoId('58caa19ecf3207620d0b04c7') ] ] ],
			        ];

			        $notify = Notification::where('Date' ,'>', new MongoDate(strtotime('')))
			        ->where($where)
			        ->get(array('NotificationMessage','NotificationTitle'));
			        echo '<pre>';
		*/

		/* $c =Notification::where(["_id"=>'59d7767b00bde73eb6e30a6a'])->first();
			        print_r(new MongoDate(strtotime('')));
			        echo '-----------------------';
			        print_r($c->Date);
			        die;
		*/
		if (!(Auth::user()->UserPermission & CARRIER) == CARRIER) {
			return Redirect::to('/admin/dashboard');
		}
		$data['paginationurl'] = "pagination/requester";
		$data['orderType'] = "Desc";
		$data['orderby'] = "UserId";
		$data['postvalue'] = "&type=" . Input::get('type') . "&search_value=" . Input::get('search_value') . "&class=" . Input::get('class') . "&search_email=" . Input::get('search_email') . "&search_unit=" . Input::get('search_unit') . "&City=" . Input::get('City') . "&State=" . Input::get('State') . "&Country=" . Input::get('Country') . "&streat=" . Input::get('streat');
		$data['country'] = CityStateCountry::where(['type' => 'Country', 'Status' => 'Active'])
			->get(['_id', 'Content', 'state_available']);

		return view('Admin::list.requester', $data);
	}

	public function users_detail($id) {
		if (!(Auth::user()->UserPermission & CARRIER) == CARRIER) {
			return Redirect::to('/admin/dashboard');
		}
		$data['info'] = User::find($id);
		if (count($data['info']) > 0) {
			return view('Admin::other.user', $data);
		} else {
			return Redirect::to('admin/transporter')->with('fail', 'Invalid user id.');
		}
	}

	public function requester_detail($id) {
		$response = ['success' => 0, 'msg' => "Oops! Something went wrong."];
		$info = User::find($id);
		if (count($info) > 0) {
			$response = ['success' => 1, 'msg' => "User Data", 'result' => $info];
		}
		return response()->json($response);
	}

	public function edit_requester($id) {
		if (!(Auth::user()->UserPermission & CARRIER) == CARRIER) {
			return Redirect::to('/admin/dashboard');
		}
		$data['info'] = User::find($id);
		$data['state'] = CityStateCountry::where(array('type' => array('$in' => array('State', 'Country'))))->get(array('Content', 'type'));
		$data['country'] = CityStateCountry::where(['type' => 'Country', 'Status' => 'Active'])->get(['_id', 'Content', 'state_available']);
		//echo '<pre>';
		//print_r($data['info']); die;
		if (count($data['info']) > 0) {
			return view('Admin::edit.requester', $data);
		} else {
			return Redirect::to('admin/requester')->with('fail', 'Invalid user id.');
		}

	}

	public function post_edit_requester($id, Request $request) {
		if (!(Auth::user()->UserPermission & CARRIER) == CARRIER) {
			return Redirect::to('/admin/dashboard');
		}
		///regex:/^[A-Za-z \t]*$/i
		$v = Validator::make($request->all(), array('countrycode' => 'required|numeric', 'firstname' => 'required', 'lastname' => 'required', 'drop_off_state' => 'required', 'drop_off_city' => 'required', 'phone' => 'regex:/^[+0-9\- \t]*$/i', 'zip' => 'regex:/^[A-Za-z0-9]*$/i', 'nd_return_country' => 'required', 'address1' => 'regex:/^[0-9A-Za-z- \/\s]*$/i', 'address2' => 'regex:/^[0-9A-Za-z- \/\s]*$/i',
			'AlternateCCode' => 'numeric',
			'AlternatePhoneNo' => 'numeric',
		));
		if ($v->fails()) {
			return Redirect::to("admin/requester/edit/$id")->withErrors($v)->withInput();
		} else {
			$country = json_decode(Input::get('nd_return_country'));
			$state = json_decode(Input::get('drop_off_state'));

			$city = json_decode(Input::get('drop_off_city'));

			$save = User::find($id);
			$save->FirstName = Input::get('firstname');
			$save->LastName = Input::get('lastname');
			$save->Name = ucfirst(Input::get('firstname')) . " " . Input::get('lastname');
			$save->CountryCode = Input::get('countrycode');
			$save->PhoneNo = Input::get('phone');
			$save->Street1 = Input::get('address1');
			$save->Street2 = Input::get('address2');
			$save->Country = @$country->name;
			$save->State = @$state->name;
			$save->City = @$city->name;
			$save->ZipCode = Input::get('zip');

			$save->AlternateCCode = Input::get('AlternateCCode');
			$save->AlternatePhoneNo = Input::get('AlternatePhoneNo');

			$save->save();
			return Redirect::to('admin/requester')->withSuccess('User updated successfully.');
		}

	}

	public function email_template() {
		if (!(Auth::user()->UserPermission & EMAIL_TEMPLATE) == EMAIL_TEMPLATE) {
			return Redirect::to('/admin/dashboard');
		}
		$data['title'] = "Email Management List";
		$data['paginationurl'] = "pagination/email_template";
		$data['orderType'] = "Desc";
		$data['orderby'] = "UserId";
		$data['postvalue'] = "";

		return view('Admin::list.email_template', $data);
	}

	public function edit_email_template($id) {
		if (!(Auth::user()->UserPermission & EMAIL_TEMPLATE) == EMAIL_TEMPLATE) {
			return Redirect::to('/admin/dashboard');
		}
		$data['info'] = Emailtemplate::where('_id', '=', $id)->get();
		if (count($data['info']) > 0) {
			return view('Admin::edit.email_template', $data);
		} else {
			return redirect('admin/email_template');
		}
	}

	public function post_email_template($id, Request $request) {
		if (!(Auth::user()->UserPermission & EMAIL_TEMPLATE) == EMAIL_TEMPLATE) {
			return Redirect::to('/admin/dashboard');
		}
		$v = Validator::make($request->all(), array('title' => 'required', 'subject' => 'required', 'body' => 'required', 'sendFrom' => 'required', 'sendFromName' => 'required'));
		if ($v->fails()) {
			return Redirect::to('admin/edit_email_template/' . $id)->withErrors($v)->withInput(Input::get());
		} else {

			$eTemplate = Emailtemplate::find($id);
			$eTemplate->TTitle = Input::get('title');
			$eTemplate->TFrom = Input::get('sendFrom');
			$eTemplate->TFromName = Input::get('sendFromName');
			$eTemplate->TSubject = Input::get('subject');
			$eTemplate->TBody = Input::get('body');
			$eTemplate->save();
			return Redirect::to('admin/email_template/')->withSuccess('Email template updated successfully.');
		}
	}

	public function new_email_template() {
		if (!(Auth::user()->UserPermission & EMAIL_TEMPLATE) == EMAIL_TEMPLATE) {
			return Redirect::to('/admin/dashboard');
		}
		return view('Admin::add.email_template');
	}

	public function add_email_template(Request $request) {
		if (!(Auth::user()->UserPermission & EMAIL_TEMPLATE) == EMAIL_TEMPLATE) {
			return Redirect::to('/admin/dashboard');
		}
		$v = Validator::make($request->all(), array('title' => 'required', 'subject' => 'required', 'body' => 'required', 'sendFrom' => 'required', 'sendFromName' => 'required'));
		if ($v->fails()) {
			return Redirect::to('admin/add_email_template')->withErrors($v)->withInput(Input::get());
		} else {

			// $eTemplate = Emailtemplate::find($id);
			// $eTemplate->TTitle = Input::get('title');
			// $eTemplate->TFrom = Input::get('sendFrom');
			// $eTemplate->TFromName = Input::get('sendFromName');
			// $eTemplate->TSubject = Input::get('subject');
			// $eTemplate->TBody = Input::get('body');
			// $eTemplate->save();
			$insEmailtemplate = array(
				array(
					"TTitle" => Input::get('title'),
					"TFrom" => Input::get('sendFrom'),
					"TFromName" => Input::get('sendFromName'),
					"TSubject" => Input::get('subject'),
					"TBody" => Input::get('body'),
				),
			);

			Emailtemplate::Insert($insEmailtemplate);
			return Redirect::to('admin/email_template/')->withSuccess('Email template added successfully.');
		}
	}

	public function status_activity($id, $status, $function) {
		$href = Input::get('href');
		$msg = '';

		switch ($function) {
		case 'admin_user':
			$state = Admin::find($id);
			$state->UserStatus = $status;
			$state->save();
			$msg = 'Status changed successfully.';
			break;
		case 'distance':
			$state = Distance::find($id);
			$state->Status = $status;
			$state->save();
			$msg = 'Status changed successfully.';
			break;
		case 'faq_list':
			$state = FAQ::find($id);
			$state->Status = $status;
			$state->save();
			$msg = 'Status changed successfully.';
			break;
		case 'client':
			$state = Client::find($id);
			$state->Status = $status;
			$state->save();
			$msg = 'Status changed successfully.';
			break;
		case 'app_tutorial':
			$state = AppTutorial::find($id);
			$state->Status = $status;
			$state->save();
			$msg = 'Status changed successfully.';
			break;
		case 'images':
			$state = SilderImages::find($id);
			$state->Status = $status;
			$state->save();
			$msg = 'Status changed successfully.';
			break;
		case 'extraregion':
			$state = Extraregion::find($id);
			$state->status = $status;
			$state->save();
			$msg = 'Status changed successfully.';
			break;
		case 'City_State_Country':
			$state = CityStateCountry::find($id);
			$state->Status = $status;
			$state->save();
			$msg = 'Status changed successfully.';
			break;
		case 'trips':
			$state = Trips::find($id);
			$state->status = $status;
			$state->save();
			$msg = 'Status changed successfully.';
			break;
		case 'City_State_Country_Signup':
			$state = CityStateCountry::find($id);
			if (count($state) > 0) {
				$state->Signup = ($status == 'true') ? true : false;
				$state->save();
				$msg = 'Status changed successfully.';
			}
			break;
		case 'Category':
			$state = Category::find($id);
			$state->Status = $status;
			$state->save();
			$msg = 'Status changed successfully.';
			break;

		case 'item':
			$state = Item::find($id);
			$state->Status = $status;
			$state->save();
			$msg = 'Status changed successfully.';
			break;
		case 'localCategory':
			$state = Category::find($id);
			$state->Status = $status;
			$state->save();
			$msg = 'Status changed successfully.';
			break;

		case 'Promocode':
			$state = Promocode::find($id);
			$state->Status = $status;
			$state->save();
			$msg = 'Status changed successfully.';
			break;

		case 'Requester':
			$user = User::find($id);
			if (count($user) > 0) {
				// Send notification

				if ($user->NotificationId != '') {
					$Notification = new Notify();

					if ($status == 'active') {

						$Notification->setValue('title', 'Profile activated');
						$Notification->setValue('message', 'Your profile has been activated, please login again.');

						$Notification->setValue('type', 'account_verify');
						$Notification->setValue('location', 'account_verify');

					} else {
						$Notification->setValue('title', 'Profile deactivated');
						$Notification->setValue('message', 'Your transporter profile has been deactivated, please contact us at support@aquantuo.com.');

						$Notification->setValue('location', ($user->TransporterStatus == 'inactive') ? 'logout' : 'requester_profile_deactivated');
					}

					$Notification->add_user($user->NotificationId, $user->DeviceType);
					$Notification->fire();
				}
				// End send notification
				$user->RequesterStatus = $status;
				$user->save();
				$msg = 'Status changed successfully.';
			}
			break;
		case 'Transporter':

			$user = User::find($id);
			if (count($user) > 0) {
				// Send notification

				if ($user->NotificationId != '') {
					if ($status == 'active') {

						$Notification = new Notify();
						$Notification->setValue('badge', 1);
						$Notification->setValue('message', 'Your transporter profile has been activated, please login again.');
						$Notification->setValue('title', 'Profile activated');
						$Notification->setValue('type', 'profile_activated');
						$Notification->setValue('location', 'transporter_profile_activated');
						$Notification->setValue('actionLocKey', 'transporter_profile_activated');
						$Notification->setValue('locKey', 'Profile activated');
						$Notification->setValue('actionkey', 'transporter_profile_activated');
						$Notification->add_user($user->NotificationId, $user->DeviceType);

						$Notification->fire();

						send_mail('57026b8be4b066fdce79406d', [
							"to" => $user->Email,
							"replace" => [
								"[USERNAME]" => $user->Name,
							],
						]
						);

						$insNotification = array(
							array(
								"NotificationTitle" => 'Profile activated',
								"NotificationShortMessage" => 'Transporter status has been change successfully.',
								"NotificationMessage" => 'Your transporter profile has been activated, please login again.',
								"NotificationType" => "transporter_activated",
								"NotificationUserId" => array($user->_id),
								"Date" => new MongoDate(),
								"GroupTo" => "User",
							),
							array(
								"NotificationTitle" => 'Profile activated',
								"NotificationMessage" => 'Transporter status has been change successfully.',
								"NotificationUserId" => array(),
								"NotificationReadStatus" => 0,
								"location" => "transporter_profile_activated",
								"Date" => new MongoDate(),
								"GroupTo" => "Admin",
							),
						);

						Notification::Insert($insNotification);

					} else {

						$Notification = new Notify();
						$Notification->setValue('badge', 1);
						$Notification->setValue('message', trans('lang.ACCOUNT_DEACTIVATE'));
						$Notification->setValue('title', trans('lang.ACCOUNT_DEACTIVATE_TITLE'));
						$Notification->setValue('type', 'profile_deactivated');
						$Notification->setValue('location', ($user->RequesterStatus == 'inactive') ? 'logout' : 'transporter_profile_deactivated');

						$Notification->setValue('actionLocKey', ($user->RequesterStatus == 'inactive') ? 'logout' : 'transporter_profile_deactivated');

						$Notification->setValue('locKey', 'Profile activated');
						$Notification->setValue('actionkey', ($user->RequesterStatus == 'inactive') ? 'logout' : 'transporter_profile_deactivated');
						$Notification->add_user($user->NotificationId, $user->DeviceType);

						$Notification->fire();

						send_mail('57026a29e4b066fdce79406a', [
							"to" => $user->Email,
							"replace" => [
								"[USERNAME]" => $user->Name,

							],
						]
						);

						$insNotification = array(
							array(
								"NotificationTitle" => trans('lang.ACCOUNT_DEACTIVATE_TITLE'),
								"NotificationShortMessage" => trans('lang.ACCOUNT_DEACTIVATE'),
								"NotificationMessage" => trans('lang.ACCOUNT_DEACTIVATE'),
								"NotificationType" => "profile_deactivated",
								"NotificationUserId" => array($user->_id),
								"Date" => new MongoDate(),
								"GroupTo" => "User",
							),
						);

						Notification::Insert($insNotification);

					}
				}
				// End send notification
				$user->TransporterStatus = $status;
				$user->save();
				$msg = 'Status changed successfully.';
			}
			break;
		}

		return Redirect::to($href)->withSuccess($msg);
	}

	public function delete_activity($id, $function) {
		$responce = array('success' => 0, 'msg' => 'Fail to delete record.');
		$Notification = new Notify();
		if ($function == 'requester') {

			$user = User::where('_id', $id)->first();
			$user->delete_status = 'yes';
			$user->save();

			//User::where('_id', $id)->update(['delete_status' => 'yes']);
			Deliveryrequest::where('RequesterId', new MongoId($id))
				->whereIn('Status', ['peding', 'ready'])
				->delete();
			$responce['success'] = 1;
			$responce['msg'] = 'User deleted successfully.';

			if ($user->NotificationId != '') {

				$Notification->setValue('title', 'Logout');
				$Notification->setValue('message', 'Your account is inactive, please contact Aquantuo support.');
				$Notification->setValue('location', 'login_other_place');
				$Notification->setValue('locationkey', '');
				$Notification->add_user($user->NotificationId, $user->DeviceType);
				$Notification->fire();
			}

		}

		if ($function == 'transporter') {

			Deliveryrequest::where('RequesterId', new MongoId($id))
				->whereIn('Status', ['peding', 'ready'])
				->delete();
			User::where('_id', $id)->update(['delete_status' => 'yes']);

			$responce['success'] = 1;
			$responce['msg'] = 'User deleted successfully.';
		}

		if ($function == 'transporter') {

			$user = User::where('_id', $id)->first();
			$user->delete_status = 'yes';
			$user->save();

			Deliveryrequest::where('RequesterId', new MongoId($id))
				->whereIn('Status', ['peding', 'ready'])
				->delete();
			//User::where('_id', $id)->update(['delete_status' => 'yes']);

			$responce['success'] = 1;
			$responce['msg'] = 'User deleted successfully.';

			if ($user->NotificationId != '') {

				$Notification->setValue('title', 'Logout');
				$Notification->setValue('message', 'Your account is inactive, please contact Aquantuo support.');
				$Notification->setValue('location', 'login_other_place');
				$Notification->setValue('locationkey', '');
				$Notification->add_user($user->NotificationId, $user->DeviceType);
				$Notification->fire();
			}
		}

		if ($function == 'state_list') {
			CityStateCountry::find($id)->delete();
			$responce['success'] = 1;
			$responce['msg'] = 'State deleted successfully.';
		}

		if ($function == 'city_list') {
			CityStateCountry::find($id)->delete();
			$responce['success'] = 1;
			$responce['msg'] = 'City deleted successfully.';
		}

		if ($function == 'packageItem') {
			Additem::find($id)->delete();
			$responce['success'] = 1;
			$responce['msg'] = 'Item deleted successfully.';
		}

		if ($function == 'section') {
			DB::connection('mysql')->table('section')->where('id', '=', $id)->delete();
			$responce['success'] = 1;
			$responce['msg'] = 'Section deleted successfully.';
		}

		if ($function == 'accra') {
			Extraregion::find($id)->delete();
			$responce['success'] = 1;
			$responce['msg'] = 'recored has been deleted successfully.';
		}

		if ($function == 'faqlist') {
			DB::connection('mysql')->table('faq')->where('id', '=', $id)->delete();
			$responce['success'] = 1;
			$responce['msg'] = 'Faq deleted successfully.';
		}

		if ($function == 'client') {
			Client::find($id)->delete();
			$responce['success'] = 1;
			$responce['msg'] = 'Client deleted successfully.';
		}

		if ($function == 'promocode') {
			Promocode::find($id)->delete();
			$responce['success'] = 1;
			$responce['msg'] = 'Code deleted successfully.';
		}

		if ($function == 'cronmail') {
			SendMail::find($id)->delete();
			$responce['success'] = 1;
			$responce['msg'] = 'recored deleted successfully.';
		}

		if ($function == 'distance') {
			Distance::find($id)->delete();
			$responce['success'] = 1;
			$responce['msg'] = 'recored deleted successfully.';
		}

		if ($function == 'image') {
			SilderImages::find($id)->delete();
			$responce['success'] = 1;
			$responce['msg'] = 'image deleted successfully.';
		}

		if ($function == 'country_list') {
			$user = CityStateCountry::where(array('SuperName' => $id))->get();

			if (count($user) > 0) {
				$stateid = array();
				foreach ($user as $key) {
					$stateid[] = $key['_id'];
				}

				CityStateCountry::whereIn('SuperName', $stateid)->delete();
				CityStateCountry::where(array('SuperName' => $id))->delete();

			}
			CityStateCountry::find($id)->delete();

			$responce['success'] = 1;
			$responce['msg'] = 'Country deleted successfully.';
		}

		if ($function == 'app_tutorial') {
			AppTutorial::find($id)->delete();
			$responce['success'] = 1;
			$responce['msg'] = 'Deleted successfully.';
		}

		if ($function == 'faq_list') {
			FAQ::find($id)->delete();
			$responce['success'] = 1;
			$responce['msg'] = 'Deleted successfully.';
		}
		if ($function == 'package') {
			Deliveryrequest::find($id)->delete();
			$responce['success'] = 1;
			$responce['msg'] = 'Deleted successfully.';
		}
		if ($function == 'admin_user') {
			Admin::find($id)->delete();
			$responce['success'] = 1;
			$responce['msg'] = 'Deleted successfully.';
		}
		if ($function == 'category_list') {
			Category::find($id)->delete();
			$responce['success'] = 1;
			$responce['msg'] = 'Deleted successfully.';
		}
		if ($function == 'item') {
			Item::find($id)->delete();
			$responce['success'] = 1;
			$responce['msg'] = 'Deleted successfully.';
		}

		if ($function == 'localcategory') {
			Category::find($id)->delete();
			$responce['success'] = 1;
			$responce['msg'] = 'Deleted successfully.';
		}
		if ($function == 'business-trip') {
			Trips::find($id)->delete();
			$responce['success'] = 1;
			$responce['msg'] = 'Deleted successfully.';
		}
		if ($function == 'individual-trip') {
			Trips::find($id)->delete();
			$responce['success'] = 1;
			$responce['msg'] = 'Deleted successfully.';
		}

		if ($function == 'local_delivery') {
			Deliveryrequest::find($id)->delete();
			$responce['success'] = 1;
			$responce['msg'] = 'Deleted successfully.';
		}

		echo json_encode($responce);
	}

	public function user_list() {
		if (!(Auth::user()->UserPermission & SENDER) == SENDER) {
			return Redirect::to('/admin/dashboard');
		}
		$data = $this->commonpaginationdata();
		$data['users'] = Admin::skip($data['start'])->take($data['per_page'])->get();
		$data['count'] = Admin::count();
		$data['no_of_paginations'] = ceil($data['count'] / $data['per_page']);
		return view('Admin::list.pagination.users', $data);
	}

	public function commonpaginationdata() {
		$data['cur_page'] = $data['page'] = Input::get('page');
		$data['page'] -= 1;
		$data['per_page'] = Auth::user()->PerPage; //Input::get('Pnum');
		$data['previous_btn'] = true;
		$data['next_btn'] = true;
		$data['first_btn'] = true;
		$data['last_btn'] = true;
		$data['sno'] = ($data['start'] = $data['page'] * $data['per_page']) + 1;

		return $data;
	}

	public function logout() {
		//Session::flush();
		Auth::logout();
		Session::forget('tzo');
		return redirect('admin/login');
	}

	/**
	 * Update admin password
	 *
	 */
	public function change_password() {
		return view('Admin::auth.change_password');
	}

	public function change_password_post(Request $request) {
		$userData = Auth::user();
		$Adminpassword = $userData->password;
		$oldPassword = Input::get('oldPassword');
		$v = Validator::make($request->all(), array('oldPassword' => 'required', 'NewPassword' => 'required', 'ConfirmPassword' => 'required|same:NewPassword'));
		if ($v->fails()) {
			return Redirect::to('admin/change_password')->withErrors($v)->withInput(Input::get());
		}
		//admin@123
		//admmin@123456
		$auth = Admin::where(array('UserPassword' => $oldPassword))->get(array('UserEmail'));
		//['UserEmail' => $email, 'password' => $password]
		//if (Auth::attempt(['password' => $oldPassword])) {
		//echo 'kl';
		//print_r($userData->UserEmail); die;
		if (Auth::attempt(['UserEmail' => $userData->UserEmail,'password' => $oldPassword])) {

			$save = Admin::find($userData->_id);
			$save->password = bcrypt(Input::get('NewPassword'));
			$save->save();

			return Redirect::to('admin/change_password')->withSuccess('Password Updated Successfully.');
		} else {
			//return Redirect::to('admin/change_password')->withErrors('Old Password not matched.')->withInput(Input::get());
			return Redirect::to('admin/change_password')->with('fail', 'Old Password not matched.');
		}
	}

	public function app_content() {
		if (!(Auth::user()->UserPermission & APP_CONTENT) == APP_CONTENT) {
			return Redirect::to('/admin/dashboard');
		}
		$data['title'] = "App Content Management List";
		$data['paginationurl'] = "pagination/app_content";
		$data['orderType'] = "Desc";
		$data['orderby'] = "_id";
		$data['postvalue'] = "";

		return view('Admin::list.app_content', $data);
	}

	public function edit_app_content($id) {
		if (!(Auth::user()->UserPermission & APP_CONTENT) == APP_CONTENT) {
			return Redirect::to('/admin/dashboard');
		}
		$data['edit'] = "Edit App Content";
		$data['info'] = Appcontent::find($id);
		if (count($data['info']) > 0) {
			return view('Admin::edit.app_content', $data);
		} else {
			return Redirect::to('admin/app_content');
		}
	}

	public function post_edit_app_content($id, Request $request) {
		if (!(Auth::user()->UserPermission & APP_CONTENT) == APP_CONTENT) {
			return Redirect::to('/admin/dashboard');
		}
		$v = Validator::make($request->all(), array('title' => 'required', 'content' => 'required'));
		if ($v->fails()) {
			return Redirect::to("admin/edit_app_content/$id")->withErrors($v)->withInput();

		} else {
			$save = Appcontent::find($id);
			$save->Title = Input::get('title');
			$save->Content = Input::get('content');
			$save->save();

			return Redirect::to('admin/app_content')->withSuccess('App content updated successfully.');
		}

	}

	public function package() {
		if (!(Auth::user()->UserPermission & PACKAGE) == PACKAGE) {
			return Redirect::to('/admin/dashboard');
		}
		$data['category'] = Category::where('Status', 'Active')->get(array('Content'));
		$data['country'] = CityStateCountry::where(array('type' => 'Country'))->get(array('Content'));
		$data['title'] = "Send a Package Management";
		$data['paginationurl'] = "pagination/package";
		$data['orderType'] = "Desc";
		$data['orderby'] = "UserId";
		$data['postvalue'] = "&status=" . Input::get('status') . "&StartDate=" . Input::get('StartDate') . "&EndDate=" . Input::get('EndDate') . "&ProductName=" . Input::get('ProductName') . "&type=" . Input::get('type')
		. "&city=" . Input::get('city') . "&state=" . Input::get('state') . "&country=" . Input::get('country') . "&category=" . Input::get('category') . "&search_value=" . Input::get('search_value') . "&Packageid=" . Input::get('Packageid') . "&requester=" . Input::get('requester');

		$data['country'] = CityStateCountry::where(['type' => 'Country', 'Status' => 'Active'])
			->get(['_id', 'Content', 'state_available']);
		return view('Admin::list.package', $data);
	}

	public function prepare_request_make_tnx($id) {
		$delivery_req = Deliveryrequest::find($id);
		if (count($delivery_req) > 0) {

			$data['delivery'] = $delivery_req;
			return view('Admin::edit.ajax.payment', $data);
		} else {
			return Redirect::to("admin/package");
		}
	}

	public function post_prepare_request_make_tnx($id) {
		$validation = Validator::make(Input::all(), array(
			'transaction_id' => 'required', 'description' => 'required',
		));
		if ($validation->fails()) {
			return Redirect::to("admin/process-card-list/$id")
				->withErrors($validation)->withInput();
		} else {
			$delivery_req = Deliveryrequest::find($id);
			if (count($delivery_req) > 0) {
				$status = 'ready';
				if ($delivery_req->TripId != '') {
					$status = 'assign';
				}

				$delivery_req->Status = $status;
				$delivery_req->PaymentStatus = 'paid';
				$productList = $delivery_req->ProductList;

				foreach ($productList as $key => $val) {
					$val['status'] = $status;
					$val['PaymentStatus'] = 'yes';
					$productList[$key] = $val;

					$insertactivity = [
						'request_id' => $id,
						'request_type' => $delivery_req->RequestType,
						'PackageNumber' => $delivery_req->PackageNumber,
						'item_id' => $val['_id'],
						'package_id' => $val['package_id'],
						'item_name' => $val['product_name'],
						'log_type' => 'request',
						'message' => 'Payment successful.',
						'status' => 'ready',
						'action_user_id' => Auth::user()->_id,
						'EnterOn' => new MongoDate(),
					];

					Activitylog::insert($insertactivity);
				}
				$delivery_req->ProductList = $productList;

				$old_email_notify_status = $delivery_req->email_notified_to_client;
				$delivery_req->email_notified_to_client = true;
				$delivery_req->save();

				Transaction::insert([
					'SendById' => $delivery_req->RequesterId,
					'SendByName' => $delivery_req->RequesterName,
					'TransactionType' => $delivery_req->RequestType,
					"request_id" => $delivery_req->_id, 
					'RequestType'=> $delivery_req->RequestType,
					'Description' => Input::get('description'),
					'Credit' => $delivery_req->TotalCost,
					'RecieveByName' => 'Aquantuo',
					'Debit' => '',
					'Status' => 'paid',
					'Transaction_id' => Input::get('transaction_id'),
					'EnterOn' => new MongoDate(),
				]);

				if ($old_email_notify_status != true) {
					$users = User::find($delivery_req->RequesterId);

					if (count($users) > 0) {
						$link = url("delivery-details") . "/" . $delivery_req->_id;
						if($delivery_req->RequestType == 'local_delivery'){
							$link = url("local-delivery-detail") . "/" . $delivery_req->_id;
						}
						


						if ($users->EmailStatus == "on") {
							send_mail('5694a10f5509251cd67773eb', [
								"to" => $users->Email,
								"replace" => [
									"[USERNAME]" => $users->Name,
									"[PACKAGETITLE]" => ucfirst($delivery_req->ProductTitle),
									"[PACKAGEID]" => $delivery_req->PackageId,
									"[SOURCE]" => $delivery_req->PickupFullAddress,
									"[DESTINATION]" => $delivery_req->DeliveryFullAddress,
									"[PACKAGENUMBER]" => $delivery_req->PackageNumber,
									"[SHIPPINGCOST]" => $delivery_req->ShippingCost,
									"[TOTALCOST]" => $delivery_req->TotalCost,
									'[LINK]' => "<a href='$link'>$link</a>",
								],
							]);
						}

					}
				}

				if($delivery_req->RequestType == 'local_delivery'){
					return Redirect::to("admin/local-delivery-detail/$id")->withSuccess('Transaction has been completed successfully.');
				}else{
					return Redirect::to("admin/package/detail/$id")->withSuccess('Transaction has been completed successfully.');
				}

				

			} else {

				return Redirect::to("admin/dashboard");
			}
		}

	}
	/*
		     * User For view package details
		     *
	*/
	public function online_package_detail($id) {
		if (!(Auth::user()->UserPermission & PACKAGE) == PACKAGE) {
			return Redirect::to('/admin/dashboard');
		} else {
			if (!empty($id)) {
				$transaction_id = [];
				$data['category'] = Category::where(['type' => 'Category', 'Status' => 'Active'])->whereIn('TravelMode', ['ship', 'air'])->orderBy('Content', 'asc')->get(['TravelMode', 'Content']);
				$data['info'] = Deliveryrequest::where(['_id' => $id, 'RequestType' => 'online'])->first();
				// echo "<pre>";print_r($data['info']);
				$data['tx_detail'] = [];

				$data['accept_count'] = Deliveryrequest::where(array('_id' => $id))
					->where(array("ProductList" => array('$elemMatch' => array('status' => 'ready'))))->count();
				$data['comment_count'] = Comment::where(['request_id'=>$id])->count();
				if (count($data['info']) > 0) {

					$data['transporter'] = User::where(['TransporterStatus' => 'active', 'TPSetting' => 'on'])->where('_id', '!=', $data['info']->RequesterId)->orderby('Name', 'asc')->get(['Name', '_id', 'FirstName', 'LastName']);

					$data['carrierLatitude'] = 0;
					$data['carrierLongnitude'] = 0;
					$CarrierId = $data['info']->CarrierId;
					if ($CarrierId != "") {
						$data['userinfo'] = User::find($CarrierId);
						if (isset($data['userinfo']->CarrierLocation)) {
							$data['carrierLatitude'] = $data['userinfo']->CarrierLocation[1];
							$data['carrierLongnitude'] = $data['userinfo']->CarrierLocation[0];
						}
					}

					if (isset($data['info']->request_version) && $data['info']->request_version == 'new') {
						return view('Admin::other.online_package_new', $data);
					} else {
						return view('Admin::other.online_package', $data);
					}

				} else {
					return Redirect::to("admin/online-package");
				}
			} else {
				return Redirect::to("admin/online-package");
			}
		}
	}

	/*
		     * User For view package details
		     *
	*/
	public function buy_for_me_detail($id) {

		if (!(Auth::user()->UserPermission & PACKAGE) == PACKAGE) {
			return Redirect::to('/admin/dashboard');
		} else {
			if (!empty($id)) {
				$data['category'] = Category::where(['type' => 'Category', 'Status' => 'Active'])->whereIn('TravelMode', ['ship', 'air'])->orderBy('Content', 'asc')->get(['TravelMode', 'Content']);
				$transaction_id = [];
				$data['info'] = Deliveryrequest::where(['_id' => $id, 'RequestType' => 'buy_for_me'])->first();
				$data['tx_detail'] = [];
				$data['comment_count'] = Comment::where(['request_id'=>$id])->count();

				if (count($data['info']) > 0) {
					$data['tplist'] = User::where(['TransporterStatus' => 'active', 'TPSetting' => 'on'])->where('_id', '!=', $data['info']->RequesterId)
						->orderby('Name', 'asc')
						->get(['Name']);

					$data['itemCount'] = Itemhistory::where('request_id', '=', $id)->count();
					//return view('Admin::other.but-for-me', $data);


					if (isset($data['info']->request_version) && $data['info']->request_version == 'new') {

						return view('Admin::other.but-for-me', $data);
					} else {
						return view('Admin::other.buy_for_me_old', $data);
					}
				} else {
					return Redirect::to("admin/buy-for-me");
				}
			} else {
				return Redirect::to("admin/buy-for-me");
			}
		}
	}

	public function package_detail($id) {
		if (!(Auth::user()->UserPermission & PACKAGE) == PACKAGE) {
			return Redirect::to('/admin/dashboard');
		}
		$data['info'] = Deliveryrequest::find($id);

		$data['accept_count'] = Deliveryrequest::where(array('_id' => $id))
			->where(array("ProductList" => array('$elemMatch' => array('status' => 'pending'))))->count();
		$data['comment_count'] = Comment::where(['request_id'=>$id])->count();
		if (count($data['info']) > 0) {

			if (isset($data['info']->ProductList)) {

				$data['tplist'] = User::where(['TransporterStatus' => 'active', 'TPSetting' => 'on'])->where('_id', '!=', $data['info']->RequesterId)
					->orderby('Name', 'asc')
					->get(['Name', '_id']);
				return view('Admin::sendapackage.request_detail', $data);
			} else {
				$data['carrierLatitude'] = 0;
				$data['carrierLongnitude'] = 0;
				$CarrierId = $data['info']->CarrierId;
				if ($CarrierId != "") {
					$data['userinfo'] = User::find($CarrierId);
					if (isset($data['userinfo']->CarrierLocation)) {
						$data['carrierLatitude'] = $data['userinfo']->CarrierLocation[1];
						$data['carrierLongnitude'] = $data['userinfo']->CarrierLocation[0];
					}
				}
			}

			return view('Admin::other.package', $data);
		} else {
			return Redirect::to("admin/package");
		}
	}

	/*
		     * User For notification list
		     *
	*/

	public function notification() {
		if (!(Auth::user()->UserPermission & NOTIFICATION) == NOTIFICATION) {
			return Redirect::to('/admin/dashboard');
		}

		$data['title'] = "Notification Management";
		$data['paginationurl'] = "pagination/notification";
		$data['orderType'] = "Desc";
		$data['orderby'] = "_id";
		$data['postvalue'] = "&search_value=" . Input::get('search_value') . "&StartDate=" . Input::get('StartDate') . "&EndDate=" . Input::get('EndDate');
		return view('Admin::list.notification', $data);
	}

	/*
		     * Used For load view of send notification
		     *
	*/

	public function send_notification() {
		if (!(Auth::user()->UserPermission & NOTIFICATION) == NOTIFICATION) {
			return Redirect::to('/admin/dashboard');
		}
		$data['paginationurl'] = "pagination/send_notification/";
		$data['orderType'] = "Desc";
		$data['orderby'] = "UserId";
		$data['postvalue'] = "&status=" . Input::get('status') . "&StartDate=" . Input::get('StartDate') . "&EndDate=" . Input::get('EndDate');
		return view('Admin::list.send_notification', $data);
	}

	/*
		     * Used For send notification to user
		     *
	*/
	public function post_notificatione(Request $request) {
		if (!(Auth::user()->UserPermission & NOTIFICATION) == NOTIFICATION) {
			return Redirect::to('/admin/dashboard');
		}

		$validation = Validator::make($request->all(), array('title' => 'required', 'message' => 'required'));
		if ($validation->fails()) {
			return Redirect::to("admin/send_notification")->withErrors($validation)->withInput();
		} else {
			$post = $request->all();

			$where = array();

			if (Input::get('groupto') != '') {

				if (Input::get('groupto') == 'requester') {
					$where['UserType'] = array('$in' => array('requester', 'both'));
				} else if (Input::get('groupto') == 'transporter') {
					$where['UserType'] = array('$in' => array('transporter', 'both'));
				}

			} else {
				if (is_array(Input::get('chk_del'))) {
					$userId = array();
					foreach (Input::get('chk_del') as $key => $val) {
						$where['_id']['$in'][] = new MongoId($val);

					}
				}

			}
			$userdata = User::where($where)->get(array('DeviceType', 'NotificationId', 'DeviceId'));
			if ($userdata->count() > 0) {
				$array = array('NotificationTitle' => $post['title'], 'NotificationMessage' => $post['message'], 'NotificationType' => 'Admin',
					'NotificationUserId' => array(), 'Date' => new MongoDate(), 'GroupTo' => 'all');

				$notificationids = array();
				foreach ($userdata as $key) {
					$notificationids[] = PushNotification::Device('token', array('badge' => 1));
					$array['NotificationUserId'][] = new MongoId($key['_id']);
				}

				$devices = PushNotification::DeviceCollection($notificationids);
				$message = PushNotification::Message(array('title' => $post['title'], 'message' => $post['message']));

				$collection = PushNotification::app('appNameAndroid')->to($devices)->send($message);

				$lastId = Notification::insert($array);

				return Redirect::to('admin/notification')->withSuccess('Notification Sent Successfully.');
			} else {

				return Redirect::to('admin/send-notification')->withSuccess('Oops! Something went wrong.');
			}
		}
	}

	/*
		     * Used For add new country
		     *
	*/
	public function add_country(Request $request) {
		if (!(Auth::user()->UserPermission & COUNTRY) == COUNTRY) {
			return Redirect::to('/admin/dashboard');
		}

		$post = $request->all();

		if ($post['Content'] != '') {
			$alreadyExists = CityStateCountry::where(array('Content' => trim($post['Content']), 'type' => 'Country'))->get();
			if (count($alreadyExists) > 0) {
				return Redirect::to('admin/country_list')->withSuccess('Country already exists.');
			} else {

				$array = array(
					'Content' => $post['Content'],
					'allow' => $post['allow'],
					'CurrencyCode' => $post['Currency_Code'],
					'CurrencyRate' => $post['Currency_Rate'],
					'CurrencySymbol' => $post['Currency_Symbol'],
					'FormatedText' => $post['Formated_Text'],
					'type' => 'Country',
					'Status' => $post['status'],
					'state_available' => (Input::get('state_available') == 'true') ? true : false,
				);

				$array['Signup'] = ($post['sinup_status'] == 'active') ? true : false;
				$lastId = CityStateCountry::insert($array);

				return Redirect::to('admin/country_list')->withSuccess('Country Added Successfully.');
			}

			return Redirect::to('admin/country_list')->withErrors('Country fail to add.');
		}
	}
	/*
		     * Used For list of country
		     *
	*/
	public function country_list() {
		if (!(Auth::user()->UserPermission & COUNTRY) == COUNTRY) {
			return Redirect::to('/admin/dashboard');
		}
		$data['title'] = "Country Management";
		$data['paginationurl'] = "pagination/country_list";
		$data['orderType'] = "Desc";
		$data['orderby'] = "_id";
		$data['postvalue'] = "&search_value=" . Input::get('search_value');
		return view('Admin::list.country_list', $data);
	}

	/*
		     * Used For update country
		     *
	*/
	public function update_country($id, Request $request) {
		if (!(Auth::user()->UserPermission & COUNTRY) == COUNTRY) {
			return Redirect::to('/admin/dashboard');
		}

		/*if (Input::get('state_available') == 'true') {

			        $state_available = true;
			        } elseif (Input::get('state_available') == 'false') {

			        $state_available = false;
			        }
		*/
		$content = Input::get('Content');
		$allow = Input::get('allow');
		$status = Input::get('status');
		$CurrencyCode = Input::get('Currency_Code');
		$CurrencyRate = Input::get('Currency_Rate');
		$CurrencySymbol = Input::get('Currency_Symbol');
		$FormatedText = Input::get('Formated_Text');
		$array = (Input::get('sinup_status') == 'active') ? true : false;
		if ($content != '') {
			$save = CityStateCountry::find($id);
			if (count($save) > 0) {
				$save->Content = $content;
				$save->allow = $allow;
				$save->Status = $status;
				$save->CurrencyCode = $CurrencyCode;
				$save->CurrencyRate = $CurrencyRate;
				$save->CurrencySymbol = $CurrencySymbol;
				$save->FormatedText = $FormatedText;
				$save->state_available = (Input::get('state_available') == 'true') ? true : false;
				$save->Signup = $array;
				$save->save();

				$cu = Currency::where(['CurrencyCode'=>trim(Input::get('Currency_Code'))])->first();
				if($cu){
					
					$cu->CurrencyRate = floatval(trim(Input::get('Currency_Rate')));
					$cu->save();
				}

				return Redirect::to('admin/country_list')->withSuccess('Country has been updated successfully.');
			}

		}
		return Redirect::to('admin/country_list')->withSuccess('Country fail to update.');
	}

	/*
		     * Used For state list
		     *
	*/
	public function state_list() {
		$id = Input::get('id');
		if (!(Auth::user()->UserPermission & STATE) == STATE) {
			return Redirect::to('/admin/dashboard');
		}
		$data['title'] = "State Management";
		$data['paginationurl'] = "pagination/state_list";
		$data['orderType'] = "Desc";
		$data['orderby'] = "_id";
		$data['postvalue'] = "&country=$id" . "&search_value=" . Input::get('search_value') . "&countryName=" . Input::get('countryName');
		$data['country'] = CityStateCountry::where(array('type' => 'Country'))->get(array('Content'));
		return view('Admin::list.state_list', $data);
	}

	/*
		     * Used For add new state
		     *
	*/
	public function add_state(Request $request) {
		if (!(Auth::user()->UserPermission & STATE) == STATE) {
			return Redirect::to('/admin/dashboard');
		}
		$post = $request->all();
		$id = Input::get('id');
		$post['country'] = json_decode($post['country']);

		if (isset($post['country']->name)) {
			if (trim($post['Content']) != '' && trim($post['country']->name) != '' && trim($post['country']->id) != '') {
				$post['Content'] = ucfirst(trim($post['Content']));
				$exists = CityStateCountry::where(array('CountryId' => trim($post['country']->id), 'Content' => $post['Content']))->get();

				if (count($exists) > 0) {
					return Redirect::to("admin/state_list?id=$id")->withErrors('State already exists.');
				} else {
					$array = array(
						'Content' => $post['Content'],
						"CountryId" => $post['country']->id,
						'SuperName' => $post['country']->name,
						'type' => 'State',
						'Status' => $post['state_status'],
					);
					$array['Signup'] = ($post['state_sinup_status'] == 'active') ? true : false;
					$lastId = CityStateCountry::insert($array);
					return Redirect::to("admin/state_list?id=$id")->withSuccess('State added successfully.');
				}
			}
		}
		return Redirect::to("admin/state_list?id=$id")->withErrors('Oops! Something went wrong.');
	}

	/*
		     * Used For update state
		     *
	*/
	public function update_state($stateid, Request $request) {
		if (!(Auth::user()->UserPermission & STATE) == STATE) {
			return Redirect::to('/admin/dashboard');
		}
		$id = Input::get('id');
		$content = ucfirst(trim(Input::get('Content')));
		$country = json_decode(Input::get('country'));
		$status = Input::get('state_status');
		$array = (Input::get('state_sinup_status') == 'active') ? true : false;
		if (isset($country->id)) {
			if ($content != '' && trim($country->name) != '' && trim($country->id) != '') {
				$exists = CityStateCountry::where('_id', '!=', new MongoId($stateid))
					->where(array('CountryId' => trim($country->id), 'Content' => $content))->get();

				if (count($exists) > 0) {
					return Redirect::to("admin/state_list?id=$id")->withSuccess('State already exists.');
				} else {
					$stateinfo = CityStateCountry::find($stateid);

					if (count($stateinfo) > 0) {
						$stateinfo->Content = $content;
						$stateinfo->CountryId = $country->id;
						$stateinfo->SuperName = $country->name;
						$stateinfo->Status = $status;
						$stateinfo->Signup = $array;
						$stateinfo->save();

						return Redirect::to("admin/state_list?id=$id")->withSuccess('State updated successfully.');
					}

				}
			}
		}

		return Redirect::to("admin/state_list?id=$id")->withSuccess('Oops! Something went wrong.');
	}

	public function add_kenya_state(Request $request) {
		include __DIR__ . '/excel_reader.php'; // include the class
		$excel = new \PhpExcelReader; // creates object instance of the class
		$excel->read(__DIR__ . '/country.xls'); // reads and stores the excel file data

		$id = '5ecf1fdf8f9270d16a8b4567';
		$lastId='';
		foreach ($excel->sheets[0]['cells'] as $key => $item) {
			if (trim($item[1]) != '' && trim($item[2]) != '')
			$exists = CityStateCountry::where(array('CountryId' => $id, 'Content' => trim($item[1])))->get();
			if (count($exists) === 0) {
				$array = array(
					'Content' => trim($item[1]),
					"CountryId" => $id,
					'SuperName' => 'Kenya',
					'type' => 'State',
					'Status' => 'Active',
					'Signup' => true,
				);
				$lastId = CityStateCountry::insert($array);
			}
			$exist = CityStateCountry::where(array('CountryId' => $id, 'Content' => trim($item[1])))->get();
			$lastId = $exist[0]['_id'];
			echo "last id - ".$lastId."<br>";
			$exists1 = CityStateCountry::where(array('Content' => trim($item[2])))->delete();
			$exists1 = CityStateCountry::where(array('StateId' => $lastId, 'Content' => trim($item[2])))->get();
			if (count($exists1) === 0) {
				$array = array(
					'Content' => trim($item[2]),
					"StateId" => $lastId,
					'SuperName' => trim($item[1]),
					'type' => 'city',
					'Status' => 'Active',
					'Signup' => true,
				);
				$ltId = CityStateCountry::insert($array);
			}
		}
		
		// Test to see the excel data stored in $sheets property
		echo '<pre>';
		var_export($excel->sheets[0]['cells']);
		echo '</pre>';
	}

	/*
		     * Used For city list
		     *
	*/
	public function city_list() {

		$id = Input::get('id');
		if (!(Auth::user()->UserPermission & CITY) == CITY) {
			return Redirect::to('/admin/dashboard');
		}

		$data['title'] = "City Management";
		$data['paginationurl'] = "pagination/city_list";
		$data['orderType'] = "Desc";
		$data['orderby'] = "_id";
		$data['postvalue'] = "&state=$id" . "&search_value=" . Input::get('search_value') . "&stateName=" . Input::get('stateName') . "&countryName=" . Input::get('countryName');
		$data['state'] = CityStateCountry::where(array('type' => 'State'))->get(array('Content'));

		return view('Admin::list.city_list', $data);
	}

	/*
		     * Used for add new city
		     *
	*/

	public function add_city(Request $request) {
		if (!(Auth::user()->UserPermission & STATE) == STATE) {
			return Redirect::to('/admin/dashboard');
		}
		$post = $request->all();

		$stateid = Input::get('id');
		$post['state'] = json_decode($post['state']);
		$post['Content'] = ucfirst(trim($post['Content']));
		if (isset($post['state']->name)) {
			if (trim($post['Content']) != '' && trim($post['state']->name) != '' && trim($post['state']->id) != '') {
				$exists = CityStateCountry::where(array('StateId' => trim($post['state']->id), 'Content' => $post['Content']))->get();

				if (count($exists) > 0) {
					return Redirect::to("admin/city_list?id=$stateid")->withErrors('State already exists.');
				} else {

					$array = array(
						'Content' => $post['Content'],
						"StateId" => $post['state']->id,
						'SuperName' => $post['state']->name,
						'type' => 'city',
						'Status' => $post['city_status'],
					);

					$array['Signup'] = ($post['city_sinup_status'] == 'active') ? true : false;

					$lastId = CityStateCountry::insert($array);
					return Redirect::to("admin/city_list?id=$stateid")->withSuccess('State added successfully.');
				}
			}
		}
		return Redirect::to("admin/city_list?id=$stateid")->withErrors('Oops! Something went wrong.');
	}
	public function post_add_city() {
		if (!(Auth::user()->UserPermission & STATE) == STATE) {
			return Redirect::to('/admin/dashboard');
		}

		$insert['Content'] = Input::get('Content');
		$insert['StateId'] = '58391e807ac6f63d108b4567';
		$insert['SuperName'] = '';
		$insert['type'] = 'city';
		$insert['Status'] = Input::get('city_status');
		$insert['Signup'] = Input::get('city_sinup_status');

		CityStateCountry::insert($insert);
		return Redirect::to("admin/city_list?id=58391e807ac6f63d108b4567")->withSuccess('city added successfully.');
	}

	/*
		     * Used For update city
		     *
	*/
	public function update_city($cityid, Request $request) {
		$post = $request->all();

		$stateid = Input::get('id');
		$post['state'] = json_decode($post['state']);
		$post['Content'] = ucfirst(trim($post['Content']));
		$status = Input::get('city_status');
		$array = ($post['city_sinup_status'] == 'active') ? true : false;
		if (isset($post['state']->name)) {
			if (trim($post['Content']) != '' && trim($post['state']->name) != '' && trim($post['state']->id) != '') {
				$exists = CityStateCountry::where('_id', '!=', new MongoId($cityid))
					->where(array('StateId' => trim($post['state']->id), 'Content' => $post['Content']))->get();

				if (count($exists) > 0) {
					return Redirect::to("admin/city_list?id=$stateid")->withErrors('State already exists.');
				} else {
					$citydetail = CityStateCountry::find($cityid);
					//print_r($citydetail);die;
					if (count($citydetail) > 0) {
						$citydetail->Content = $post['Content'];
						$citydetail->StateId = $post['state']->id;
						$citydetail->SuperName = $post['state']->name;
						$citydetail->Status = $status;
						$citydetail->Signup = $array;
						$citydetail->save();

						return Redirect::to("admin/city_list")->withSuccess('City updated successfully.');
					}
				}
			}
		}
		return Redirect::to("admin/city_list?id=$stateid")->withErrors('Oops! Something went wrong.');
	}

	/*
		     * Used For list fo web content
		     *
	*/
	public function web_content() {
		if (!(Auth::user()->UserPermission & WEB_CONTENT) == WEB_CONTENT) {
			return Redirect::to('/admin/dashboard');
		}
		$data['title'] = "Web Content Management";
		$data['paginationurl'] = "pagination/web_content";
		$data['orderType'] = "Desc";
		$data['orderby'] = "_id";
		$data['postvalue'] = "&search_value=" . Input::get('search_value');
		return view('Admin::list.web_content', $data);
	}

	/*
		     * Used For edit web content
		     *
	*/
	public function edit_web_content($id) {
		if (!(Auth::user()->UserPermission & WEB_CONTENT) == WEB_CONTENT) {
			return Redirect::to('/admin/dashboard');
		}
		$data['info'] = Webcontent::find($id);
		if (count($data['info']) > 0) {
			return view('Admin::edit.web_content', $data);
		} else {
			return Redirect::to('admin/web_content');
		}
	}

	/*
		     * Used For update web content
		     *
	*/
	public function update_web_content($id, Request $request) {
		if (!(Auth::user()->UserPermission & WEB_CONTENT) == WEB_CONTENT) {
			return Redirect::to('/admin/dashboard');
		}
		$validate = Validator::make($request->all(), array('title' => 'required', 'content' => 'required'));
		if ($validate->fails()) {
			return Redirect::to("admin/edit_web_content/$id")->withErrors($validate)->withInput();
		} else {
			$save = Webcontent::find($id);
			$save->Title = Input::get('title');
			$save->Content = Input::get('content');
			$save->save();
			return Redirect::to('admin/web_content')->withSuccess('Web content updated successfully.');
		}
	}

	/*
		     * Used For update Support
		     *
	*/
	public function update_support($id, Request $request) {
		if (!(Auth::user()->UserPermission & COUNTRY) == COUNTRY) {
			return Redirect::to('/admin/dashboard');
		}

		$info = Support::where(array('Token' => (int) $id))->get();

		if (count($info) > 0) {
			$insData = array(
				'UserName' => 'Admin',
				'Query' => Input::get('ReplyQuery'),
				'RepToken' => (int) $id,
				'EnterOn' => new MongoDate(),
			);

			Support::insert($insData);

			if (isset($insData['_id'])) {
				//========mail setting=====//
				$array = array(
					'to' => @$info[0]->Email,

					'replace' => array(
						'[REPLYQUERY]' => Input::get('ReplyQuery'),
					),
					'subject' => array('[QUERY]' => $info[0]->Query),
				);
				send_mail('563c4fe1e4b00dcbaba65951', $array);

				//=========mail setting====//
				return Redirect::to('admin/support')->withSuccess('Support updated successfully.');
			}
		}

		return Redirect::to('admin/support')->withSuccess('Support fail to update.');
	}

	/*
		     * Used For supportsupport list
		     *
	*/

	public function support() {
		$data['title'] = "Support Management";
		$data['paginationurl'] = "pagination/support";
		$data['orderType'] = "Desc";
		$data['orderby'] = "_id";
		$data['postvalue'] = "&search_value=" . Input::get('search_value');
		//$data['postvalue'] = "&StartDate=".Input::get('StartDate')."&status=".Input::get('status')."&group=".Input::get('group');
		return view('Admin::list.support', $data);
	}
	public function support_view_more($id) {
		$data['support'] = Support::where(array('Token' => (int) $id))->limit(1)->get();

		if (count($data['support']) > 0) {
			$data['title'] = "Admin User Communication";
			$data['paginationurl'] = "pagination/support_view_more/" . $id;
			$data['orderType'] = "Desc";
			$data['orderby'] = "_id";
			$data['postvalue'] = "&search_value=" . Input::get('search_value');

			return view('Admin::list.support_view_more', $data);
		} else {
			return Redirect::to('admin/support');
		}
	}
	public function update_support_view_more($id, Request $request) {
		if (!(Auth::user()->UserPermission & COUNTRY) == COUNTRY) {
			return Redirect::to('/admin/dashboard');
		}

		$email = '';
		$replyquery = Input::get('ReplyQuery');

		$info = Support::where(array('Token' => (int) $id))->get();

		if (count($info) > 0) {

			$insData = array(
				'UserName' => 'Admin',
				'Query' => Input::get('ReplyQuery'),
				'Email' => '',
				'RepToken' => (int) $id,
				'EnterOn' => new MongoDate(),
			);

			Support::insert($insData);

			if (isset($insData['_id'])) {
				//========mail setting=====//
				$array = array(
					'to' => @$info->Email,
					'replace' => array(
						'[REPLYQUERY]' => $replyquery,
					),
				);
				send_mail('563c4fe1e4b00dcbaba65951', $array);

				//=========mail setting====//
				return Redirect::to('admin/support_view_more/' . $id)->withSuccess('Support updated successfully.');
			}
		}
		return Redirect::to('admin/support_view_more/' . $id)->withSuccess('Support fail to update.');
	}

	/*
		     * Used For view about app
		     *
	*/
	public function about_app() {
		if (!(Auth::user()->UserPermission & ABOUT_APP) == ABOUT_APP) {
			return Redirect::to('/admin/dashboard');
		}
		$where = array('Type' => 'AboutApp');
		$data['aboutApp'] = AppContent::where($where)->get();
		return view('Admin::edit.about_app', $data);
	}

	/*
		     * Used For update about app content
		     *
	*/

	public function update_about_app($id, Request $request) {
		if (!(Auth::user()->UserPermission & ABOUT_APP) == ABOUT_APP) {
			return Redirect::to('/admin/dashboard');
		} else {
			$save = AppContent::find($id);
			$save->Content = array('Phone' => Input::get('Phone'),
				'Website' => Input::get('Website'),
				'Email' => Input::get('Email'),
				'Copyright' => Input::get('Copyright'),
				'Address' => Input::get('Address'),
			);
			$save->save();
			return Redirect::to('admin/about_app')->withSuccess('About App updated successfully.');
		}
	}

	/*
		     * Used For vehicle list
		     *
	*/

	/*
		     * Used For edit vehicle
		     *
	*/

	/*
		     * Used For  list of user notification sent
		     *
	*/
	public function view_user($id) {
		if (!(Auth::user()->UserPermission & CARRIER) == CARRIER) {
			return Redirect::to('/admin/dashboard');
		}
		$data['paginationurl'] = "pagination/view_user/" . $id;
		$data['orderType'] = "Desc";
		$data['orderby'] = "_id";
		$data['postvalue'] = "&search_value=" . Input::get('search_value');
		return view('Admin::list.view_user', $data);
	}

	/*
		     * Used For list of transaction
		     *
	*/
	public function transporter_transaction() {
		if (!(Auth::user()->UserPermission & CARRIER) == CARRIER) {
			return Redirect::to('/admin/dashboard');
		}
		$data['title'] = "Transaction History";
		$data['paginationurl'] = "pagination/transaction-history?userid=" . Input::get('userid');
		$data['orderType'] = "Desc";
		$data['orderby'] = "UserId";
		$data['postvalue'] = "&search_value=".Input::get('search_value')."&status=".Input::get('status')."&StartDate=".Input::get('StartDate')."&EndDate=".Input::get('EndDate');
		return view('Admin::list.transaction_history', $data);
	}

	/*
		     * Used For view transaction details
		     *
	*/
	public function transaction_detail($id) {
		if (!(Auth::user()->UserPermission & TRANSACTION) == TRANSACTION) {
			return Redirect::to('/admin/dashboard');
		}
		$data['info'] = Deliveryrequest::find($id);
		return view('Admin::other.transaction_detail', $data);
	}

	public function requester_transaction() {
		if (!(Auth::user()->UserPermission & TRANSACTION) == TRANSACTION) {
			return Redirect::to('/admin/dashboard');
		}
		$data['title'] = "Sender Payment";
		$data['paginationurl'] = "pagination/requester-transaction-history";
		$data['orderType'] = "Desc";
		$data['orderby'] = "_id";
		$data['postvalue'] = "&search_value=" . Input::get('search_value') . "&startdate=" . Input::get('StartDate') . "&endDate=" . Input::get('EndDate')
		. "&city=" . Input::get('city') . "&state=" . Input::get('state');
		return view('Admin::list.requester_transaction', $data);
	}
	public function export_carrier_transaction() {
		$fromDate = Input::get('FromDate');
		$todate = Input::get('ToDate');
		$searchValue = Input::get('search_value');

		$where = array();
		if ($fromDate != '') {
			$where['EnterOn']['$gt'] = new MongoDate(strtotime(DateTime::createFromFormat('d/m/Y', $fromDate)->format('d-m-Y')));
		}
		if ($todate != '') {
			$where['EnterOn']['$lt'] = new MongoDate(strtotime(DateTime::createFromFormat('d/m/Y', $todate)->format('d-m-Y 23:59:59')));
		}
		$field = array('SendByName', 'ToUserName', 'Amount', 'EnterOn');
		$info = Transaction::where('ToUserName', 'like', "%$searchValue%")->where($where)->get($field);
		$filename = time() . '.xls';
		$export_xls = new export_xls();
		$export_xls->ExportXLS($filename);

		$header[] = "Transfer By";
		$header[] = "Transfer To";
		$header[] = "Amount";
		$header[] = "Date";

		$export_xls->addHeader($header);

		foreach ($info as $key) {
			if (isset($key['EnterOn']->sec) && @$key['EnterOn']->sec > 1) {
				$key['EnterOn'] = date('d-M-Y H:i:s', $key['EnterOn']->sec);
			}
			$row = array();
			$row[] = array(0 => $key['SendByName'], 1 => $key['ToUserName'], 2 => $key['Amount'], 3 => $key['EnterOn']);

			$export_xls->addRow($row);
		}
		$export_xls->sendFile();
	}

	/*
		     * Used For export csv of transaction
		     *
	*/
	public function export_csv() {
		$q = User::query();

		$searchValue = Input::get('search_value');

		$where = array();

		if (Input::get('status') != '') {
			$where['Status'] = Input::get('status');
		}
		if (Input::get('FromDate') != '') {
			$where['EnterOn']['$gt'] = new MongoDate(strtotime(DateTime::createFromFormat('d/m/Y', Input::get('FromDate'))->format('d-m-Y')));
		}
		if (Input::get('ToDate') != '') {
			$where['EnterOn']['$lt'] = new MongoDate(strtotime(DateTime::createFromFormat('d/m/Y', Input::get('ToDate'))->format('d-m-Y 23:59:59')));
		}
		if (Input::get('transactiontype') == 'credit') {
			$where['Credit'] = array('$gt' => 0);
		}
		if (Input::get('transactiontype') == 'debit') {
			$where['Debit'] = array('$gt' => 0);
		}
		if (Input::get('depositby') == '') {
			$q->where('SendByName', 'like', "%" . Input::get('depositby') . "%");
		}
		if (Input::get('receivedby') == '') {
			$q->where('RecieveByName', 'like', "%" . Input::get('receivedby') . "%");
		}

		//
		$field = array('EnterOn', 'RecieveByName', 'Credit', 'Debit', 'Status', 'SendByName');
		$info = $q->where($where)->get($field);
		$filename = time() . '.xls';
		$export_xls = new export_xls();
		$export_xls->ExportXLS($filename);

		$header[] = "S.No.";
		$header[] = "Date";
		$header[] = "Deposit By";
		$header[] = "Receiver Name";
		$header[] = "Credit";
		$header[] = "Debit";
		$header[] = "Status";

		$export_xls->addHeader($header);

		foreach ($info as $key => $val) {
			if (isset($val['DeliveryDate']->sec)) {
				$val['EnterOn'] = date('d-M-Y H:i:s', $val['EnterOn']->sec);
			}
			$key['Status'] = ($val['Status'] == 'done') ? 'done' : 'pending';
			$row = array();
			$row[] = array(++$key, $val['EnterOn'], $val['SendByName'], $val['RecieveByName'], $val['Credit'], $val['Debit'], $val['Status']);

			$export_xls->addRow($row);
		}
		$export_xls->sendFile();
	}

	/*
		     * Used For search user for export transaction csv
		     *
	*/
	public function auto_search() {
		$searchVal = Input::get('q');
		$user = User::where('UserName', 'LIKE', "%$searchVal%")->where('UserType', '=', 'Carrier')->get(array('UserName', 'UserType'));

		for ($i = 0; $i < count($user); $i++) {
			echo $user[$i]->UserName . "\n";
		}
	}

	/*
		     * Used For load view of add app tutorial
		     *
	*/
	public function add_app_tutorial() {
		if (!(Auth::user()->UserPermission & APP_TUTORIAL) == APP_TUTORIAL) {
			return Redirect::to('/admin/dashboard');
		}
		$data['add'] = "Add App Tutorial";
		return view('Admin::add.add_app_tutorial', $data);
	}

	/*
		     * Used For add app tutorial
		     *
	*/
	public function post_app_tutorial(Request $request) {
		if (!(Auth::user()->UserPermission & APP_TUTORIAL) == APP_TUTORIAL) {
			return Redirect::to('/admin/dashboard');
		}
		$validate = Validator::make($request->all(), array('title' => 'required', 'content' => 'required'));
		if ($validate->fails()) {
			return Redirect::to("admin/add_app_tutorial")->withErrors($validate)->withInput();
		} else {
			$array = array('Title' => Input::get('title'), 'Content' => Input::get('content'), 'Status' => 'Active');
			$lastId = AppTutorial::insert($array);
			return Redirect::to('admin/app_tutorial')->withSuccess('Tutorial Added Successfully.');
		}
	}

	/*
		     * Used For list app app tutorial
		     *
	*/
	public function app_tutorial() {
		if (!(Auth::user()->UserPermission & APP_TUTORIAL) == APP_TUTORIAL) {
			return Redirect::to('/admin/dashboard');
		}
		$data['title'] = "App Tutorial Management";
		$data['paginationurl'] = "pagination/app_tutorial";
		$data['orderType'] = "Desc";
		$data['orderby'] = "_id";
		$data['postvalue'] = "&search_value=" . Input::get('search_value');
		return view('Admin::list.app_tutorial', $data);
	}

	/*
		     * Used For edit app tutorial
		     *
	*/
	public function edit_app_tutorial($id) {
		if (!(Auth::user()->UserPermission & APP_TUTORIAL) == APP_TUTORIAL) {
			return Redirect::to('/admin/dashboard');
		}
		$data['edit'] = "Edit App Tutorial";
		$data['info'] = AppTutorial::find($id);
		if (count($data['info']) > 0) {
			return view('Admin::edit.app_tutorial', $data);
		} else {
			return Redirect::to('admin/app_tutorial');
		}
	}

	/*
		     * Used For update app tutorial
		     *
	*/
	public function update_app_tutorial($id, Request $request) {
		if (!(Auth::user()->UserPermission & APP_TUTORIAL) == APP_TUTORIAL) {
			return Redirect::to('/admin/dashboard');
		}
		$validate = Validator::make($request->all(), array('title' => 'required', 'content' => 'required'));
		if ($validate->fails()) {
			return Redirect::to("admin/edit_web_content/$id")->withErrors($validate)->withInput();
		} else {
			$save = AppTutorial::find($id);
			$save->Title = Input::get('title');
			$save->Content = Input::get('content');
			$save->save();
			return Redirect::to('admin/app_tutorial')->withSuccess('App Tutorial updated successfully.');
		}
	}

	/*
		     * Used For view app tutorial
		     *
	*/
	public function view_app_tutorial($id) {
		if (!(Auth::user()->UserPermission & APP_TUTORIAL) == APP_TUTORIAL) {
			return Redirect::to('/admin/dashboard');
		}
		$data['detail'] = "App Tutorial Detail";
		$data['info'] = AppTutorial::find($id);
		if (count($data['info']) > 0) {
			return view('Admin::other.view_app_tutorial', $data);
		} else {
			return Redirect::to('admin/app_tutorial');
		}
	}

	/*
		     * Used For category
	*/

	public function category_list() {
		if (!(Auth::user()->UserPermission & COUNTRY) == COUNTRY) {
			//return Redirect::to('/admin/dashboard');
		}
		$data['title'] = "Category Management";
		$data['paginationurl'] = "pagination/category_list";
		$data['orderType'] = "Desc";
		$data['orderby'] = "_id";
		$data['postvalue'] = "&search_value=" . Input::get('search_value') . "&TravelMode=" . Input::get('TravelMode') . "&ChargeType=" . Input::get('ChargeType');
		return view('Admin::list.category_list', $data);
	}

	public function item() {
		$data['title'] = "Item Management";
		$data['paginationurl'] = "pagination/item";
		$data['orderType'] = "Desc";
		$data['orderby'] = "_id";
		$data['postvalue'] = "&search_value=" . Input::get('search_value') . "&item_name=" . Input::get('item_name') . "&ChargeType=" . Input::get('ChargeType');
		return view('Admin::list.item', $data);
	}

	public function add_item(Request $request) {

		$post = $request->all();

		if ($post['item_name'] != '') {

			$lbsWeight = '';
			$kgWeight = '';
			if ($post['weightunit'] == "lbs") {
				$lbsWeight = $post['weight'];
				$kgWeight = $post['weight'] * 1 / 2.2046;
			}

			if ($post['weightunit'] == "kg") {
				$kgWeight = $post['weight'];
				$lbsWeight = $post['weight'] / 0.45359237;
			}

			$array = [
				'item_name' => $post['item_name'],
				'kgWeight' => $kgWeight,
				'lbsWeight' => $lbsWeight,
				'price' => $post['price'],
				// 'image' => $image,
				'weightunit' => $post['weightunit'],
				'Status' => 'Active',
			];

			$lastId = Item::insert($array);

			return Redirect::to('admin/item')->withSuccess('Item Added Successfully.');
		}
		return Redirect::to('admin/item')->withErrors('item fail to add.');
	}

	public function postAddItem(Request $request, $id) {
		$post = $request->all();

		if ($post['item'] != '') {

			$updateData['item_name'] = $post['item'];
			//$updateData['weight'] = $post['weight'];

			if ($post['weightunit'] == "lbs") {
				$updateData['lbsWeight'] = $post['weight'];
				$updateData['kgWeight'] = $post['weight'] * 1 / 2.2046;
			}

			if ($post['weightunit'] == "kg") {
				$updateData['kgWeight'] = $post['weight'];
				$updateData['lbsWeight'] = $post['weight'] / 0.45359237;
			}

			$updateData['weightunit'] = $post['weightunit'];
			$updateData['price'] = $post['price'];

			$result = Item::where('_id', '=', $id)->update($updateData);

			return Redirect::to('admin/item')->withSuccess('Item updated Successfully.');
		}
		return Redirect::to('admin/item')->withErrors('item fail to updated.');
	}

	public function add_category(Request $request) {
		if (!(Auth::user()->UserPermission & COUNTRY) == COUNTRY) {
			//return Redirect::to('/admin/dashboard');
		}
		$post = $request->all();

		if ($post['Content'] != '' && $post['ChargeType'] != '' && $post['TravelMode'] != '') {
			$array = array('Content' => $post['Content'], 'TravelMode' => $post['TravelMode'], 'ChargeType' => $post['ChargeType'], 'type' => 'Category', 'Status' => 'Active', 'Shipping' => array());
			$lastId = Category::insert($array);

			return Redirect::to('admin/category_list')->withSuccess('Category Added Successfully.');
		}
		return Redirect::to('admin/category_list')->withErrors('Category fail to add.');
	}

	public function update_category($id, Request $request) {
		if (!(Auth::user()->UserPermission & COUNTRY) == COUNTRY) {
			return Redirect::to('/admin/dashboard');
		}
		$content = Input::get('Content');
		$travelmode = Input::get('TravelMode');
		$chargetype = Input::get('ChargeType');
		if ($content != '') {
			$save = category::find($id);
			$save->Content = $content;
			$save->country_id = Input::get('country');
			$save->price = (float) Input::get('price');
			$save->TravelMode = $travelmode;
			$save->ChargeType = $chargetype;
			$save->save();
			return Redirect::to('admin/local-category-list')->withSuccess('Category updated successfully.');
		}
		return Redirect::to('admin/local-category-list')->withSuccess('Category fail to update.');
	}

	/*   local Delivery  */

	public function localcategorylist() {

		if (!(Auth::user()->UserPermission & COUNTRY) == COUNTRY) {
			//return Redirect::to('/admin/dashboard');
		}

		$data['title'] = "loacl Delivery Management";
		$data['paginationurl'] = "pagination/local-category-list";
		$data['orderType'] = "Desc";
		$data['orderby'] = "_id";
		$data['postvalue'] = "&search_value=" . Input::get('search_value') . "&TravelMode=" . Input::get('TravelMode') . "&country=" . Input::get('country');

		$data['country'] = CityStateCountry::where(['type' => 'Country', 'Status' => 'Active'])
			->get(['_id', 'Content', 'state_available']);

		return view('Admin::list.localCategory', $data);
	}

	public function addlocalCategory(Request $request) {
		if (!(Auth::user()->UserPermission & COUNTRY) == COUNTRY) {
			return Redirect::to('/admin/dashboard');
		}
		$post = $request->all();

		if ($post['category_name'] != '') {

			$country = json_decode(Input::get('country'));

			$insertData =
				[
				'Content' => $post['category_name'],
				'country_id' => $country->id,
				'price' => (float) Input::get('price'),
				'ChargeType' => 'distance',
				'type' => 'localCategory',
				"TravelMode" => "ship",
				'Status' => 'Active',
				'Shipping' => array(),
			];

			$lastId = Category::insert($insertData);

			return Redirect::to('admin/local-category-list')->withSuccess('Local Category Added Successfully.');
		}
		return Redirect::to('admin/local-category-list')->withErrors('Loacl Category fail to add.');
	}

	public function faq_list() {
		if (!(Auth::user()->UserPermission & FAQ) == FAQ) {
			return Redirect::to('/admin/dashboard');
		}
		$data['title'] = "Faq Management";
		$data['paginationurl'] = "pagination/faq_list";
		$data['orderType'] = "Desc";
		$data['orderby'] = "UserId";
		$data['postvalue'] = "&search_value=" . Input::get('search_value');

		return view('Admin::list.faq_list', $data);
	}

	/*
		     * Used For load view of add faq
		     *
	*/
	public function add_faq() {
		if (!(Auth::user()->UserPermission & FAQ) == FAQ) {
			return Redirect::to('/admin/dashboard');
		}
		$data['add'] = "Add Faq";
		return view('Admin::add.add_faq', $data);
	}

	/*
		     * Used For add new faq
		     *
	*/
	public function post_faq(Request $request) {
		if (!(Auth::user()->UserPermission & FAQ) == FAQ) {
			return Redirect::to('/admin/dashboard');
		}
		$validate = Validator::make($request->all(), array('Question' => 'required', 'Answer' => 'required'));
		if ($validate->fails()) {
			return Redirect::to("admin/add_faq")->withErrors($validate)->withInput();
		} else {
			$array = array('Question' => $request['Question'], 'Answer' => $request['Answer'], 'Status' => 'Active');

			$lastId = FAQ::insert($array);
			return Redirect::to('admin/faq_list')->withSuccess('Faq Added successfully.');
		}
	}

	/*
		     * Used For edit faq
		     *
	*/
	public function edit_faq($id) {
		if (!(Auth::user()->UserPermission & FAQ) == FAQ) {
			return Redirect::to('/admin/dashboard');
		}
		$data['edit'] = "Edit Faq";
		$data['info'] = FAQ::find($id);
		if (count($data['info']) > 0) {
			return view('Admin::edit.edit_faq', $data);
		} else {
			return Redirect::to('admin/faq_list');
		}
	}

	/*
		     * Used For update faq
		     *
	*/
	public function update_faq($id, Request $request) {
		if (!(Auth::user()->UserPermission & FAQ) == FAQ) {
			return Redirect::to('/admin/dashboard');
		}
		$answer = Input::get('Answer');
		$question = Input::get('Question');

		$validate = Validator::make($request->all(), array('Question' => 'required', 'Answer' => 'required'));
		if ($validate->fails()) {
			return Redirect::to("admin/edit_faq/$id")->withErrors($validate)->withInput();
		} else {
			$save = FAQ::find($id);
			$save->Question = $question;
			$save->Answer = $answer;
			$save->save();
			return Redirect::to('admin/faq_list')->withSuccess('Faq updated successfully.');
		}
	}
	public function local_notificationtiming() {
		$where = array('Type' => 'AboutApp');
		$data['aboutApp'] = LocalNotificationTime::where($where)->get();
		return view('Admin::edit.local_notificationtiming', $data);
	}

	public function admin_user() {
		if (!(Auth::user()->UserPermission & ADMIN) == ADMIN) {
			return Redirect::to('/admin/dashboard');
		}
		$data['title'] = "Admin User Management";
		$data['paginationurl'] = "pagination/admin_user";
		$data['orderType'] = "Desc";
		$data['orderby'] = "_id";
		$data['postvalue'] = "&search_value=" . Input::get('search_value');
		return view('Admin::list.admin', $data);
	}

	public function add_admin_user() {
		if (!(Auth::user()->UserPermission & ADMIN) == ADMIN) {
			return Redirect::to('/admin/dashboard');
		}
		$this->check_permission(6);
		return view('Admin::add.admin');
	}
	public function post_add_admin_user(Request $request) {
		if (!(Auth::user()->UserPermission & ADMIN) == ADMIN) {
			return Redirect::to('/admin/dashboard');
		}
		$validate = Validator::make($request->all(), array(
			'name' => 'required',
			'email' => 'required|email',
			'password' => 'required',
			'confirm_password' => 'required|same:password',
		));
		if ($validate->fails()) {

			return Redirect::to("admin/add_admin_user")->withErrors($validate)->withInput();
		} else {
			$Auth = Admin::where(array('UserEmail' => strtolower(Input::get('email'))))->get(array('UserEmail'));
			if (count($Auth) == 0) {
				$permission = 0;
				$permission_array = array('carrier', 'sender', 'admin', 'app_content', 'email_template', 'web_content', 'app_tutorial', 'about_app_content', 'faq', 'transaction', 'country', 'state', 'city', 'vehicle', 'package', 'feedback', 'small_package_confi', 'big_package_confi', 'plane_small_package_confi', 'plane_big_package_confi', 'congiguration', 'notification', 'setting', 'transaction2', 'individual_trip', 'bussiness_trip', 'item_category', 'promocode', 'aquantuo_address', 'support', 'onlinepackage', 'buyforme');

				foreach ($permission_array as $key) {
					$permission = $permission + (int) Input::get($key);
				}
				$insData = array(
					'UserName' => Input::get('name'),
					'UserEmail' => strtolower(Input::get('email')),
					'UserPassword' => md5(Input::get('password')),
					'password' => Hash::make(Input::get('password')),
					'remember_token' => '',
					'Token' => '',
					'updated_at' => new MongoDate(),
					'UserType' => 'admin',
					'UserPermission' => $permission,
					'UserStatus' => 'Active',
					'dashbord_permission' => "no",

				);

				if (trim(Input::get('delivery_slip')) == 'yes') {
					$insData['delivery_slip'] = "yes";
				} else {
					$insData['delivery_slip'] = "no";
				}

				if (trim(Input::get('packag_slip')) == 'yes') {
					$insData['packag_slip'] = "yes";
				} else {
					$insData['packag_slip'] = "no";
				}

				if (trim(Input::get('received_slip')) == 'yes') {
					$insData['received_slip'] = "yes";
				} else {
					$insData['received_slip'] = "yes";
				}

				if (Input::get('dashbord') == "yes") {
					$insData['dashbord_permission'] = 'yes';
				}

				Admin::insert($insData);
				return Redirect::to("admin/admin_user")->withSuccess('Success! User added successfully.');
			} else {
				return Redirect::to("admin/add_admin_user")->withErrors(array('email' => 'Email already exists.'))->withInput();
			}
		}
	}
	public function edit_admin_user($id) {
		if (!(Auth::user()->UserPermission & ADMIN) == ADMIN) {
			return Redirect::to('/admin/dashboard');
		}
		$data['info'] = Admin::find($id);

		if (count($data['info']) > 0) {
			return view('Admin::edit.admin', $data);
		} else {
			return Redirect::to("admin/admin_user");
		}
	}
	public function post_edit_admin_user($id, Request $request) {
		if (!(Auth::user()->UserPermission & ADMIN) == ADMIN) {
			return Redirect::to('/admin/dashboard');
		}
		$validate = Validator::make($request->all(), array('name' => 'required', 'email' => 'required|email'));
		if ($validate->fails()) {

			return Redirect::to("admin/edit_admin_user/$id")->withErrors($validate)->withInput();
		} else {
			$Auth = Admin::where(array('UserEmail' => strtolower(Input::get('email'))))->where('_id', '!=', $id)->get(array('UserEmail'));
			if (count($Auth) == 0) {
				$permission = 0;
				$permission_array = array('carrier', 'sender', 'admin', 'app_content', 'email_template', 'web_content', 'app_tutorial', 'about_app_content', 'faq', 'country', 'state', 'city', 'package', 'feedback', 'congiguration', 'notification', 'setting', 'transaction', 'transaction2', 'individual_trip', 'bussiness_trip', 'item_category', 'promocode', 'aquantuo_address',
					'support', 'onlinepackage', 'buyforme', 'slip_management');
				foreach ($permission_array as $key) {
					$permission = $permission + (int) Input::get($key);
				}

				$save = Admin::find($id);
				$save->UserName = Input::get('name');
				$save->UserEmail = strtolower(Input::get('email'));
				$save->updated_at = new MongoDate();
				$save->UserPermission = intval($permission);

				if (Input::get('dashbord') == 'yes') {
					$save->dashbord_permission = "yes";
				} else {
					$save->dashbord_permission = "no";
				}

				// Permission for Activity Log
				if (Input::get('activitylog') == 'yes') {
					$save->activitylog = "yes";
				} else {
					$save->activitylog = "no";
				}

				// Permission slip Management
				if (trim(Input::get('delivery_slip')) == 'yes') {
					$save->delivery_slip = "yes";
				} else {
					$save->delivery_slip = "no";
				}

				if (trim(Input::get('packag_slip')) == 'yes') {
					$save->packag_slip = "yes";
				} else {
					$save->packag_slip = "no";
				}

				if (trim(Input::get('received_slip')) == 'yes') {
					$save->received_slip = "yes";
				} else {
					$save->received_slip = "no";
				}

				// Permission Region charges
				if (trim(Input::get('region_charges')) == 'yes') {
					$save->region_charges = "yes";
				} else {
					$save->region_charges = "no";
				}

				// Permission Item
				if (trim(Input::get('item')) == 'yes') {
					$save->item = "yes";
				} else {
					$save->item = "no";
				}

				// Permission local_delivery
				if (trim(Input::get('local_delivery')) == 'yes') {
					$save->local_delivery = "yes";
				} else {
					$save->local_delivery = "no";
				}

				// Permission Slider images
				if (trim(Input::get('Slider_images')) == 'yes') {
					$save->Slider_images = "yes";
				} else {
					$save->Slider_images = "no";
				}

				// Permission client testtimonials
				if (trim(Input::get('client_testtimonials')) == 'yes') {
					$save->client_testtimonials = "yes";
				} else {
					$save->client_testtimonials = "no";
				}

				// Permission Help Section
				if (trim(Input::get('help_section')) == 'yes') {
					$save->help_section = "yes";
				} else {
					$save->help_section = "no";
				}

				// Permission user feedback
				if (trim(Input::get('user_feedback')) == 'yes') {
					$save->user_feedback = "yes";
				} else {
					$save->user_feedback = "no";
				}

				if (Input::get('password') != '') {
					$save->password = bcrypt(Input::get('password'));
					$save->UserPassword = md5(Input::get('password'));
				}
				$save->save();

				return Redirect::to("admin/admin_user")->withSuccess('Success! User added successfully.');
			} else {
				return Redirect::to("admin/edit_admin_user/$id")->withErrors(array('email' => 'Email already exists.'))->withInput();
			}
		}
	}
	public function check_permission($id, $redirect = "admin/dashboard") {
		if (!(Auth::user()->UserPermission & $id) == $id) {
			return Redirect::to('/admin/dashboard');
		}
	}
	public function transfer_money_to_carrier($id) {
		return view('Admin::add.transfer');
	}
	public function cancel_package($id) {
		$data['info'] = Deliveryrequest::find($id);
		if (count($data['info']) > 0) {
			$data['country'] = CityStateCountry::where(array('type' => 'Country'))->get(array('Content'));

			$data['state'] = CityStateCountry::where(array('type' => 'State', 'SuperName' => $data['info']->DeliveryCountry))->get(array('Content'));
			$data['city'] = CityStateCountry::where(array('type' => 'city', 'SuperName' => $data['info']->DeliveryState))->get(array('Content'));

			return view('Admin::other.cancel_package', $data);
		} else {
			return Redirect::to('/admin/package');
		}
	}
	public function postcancel_package($id) {
		$update = Deliveryrequest::find($id);
		$senderid = $update->SenderId;
		$stripe = new Stripe();
		if (count($update) > 0) {
			if (isset($_FILES['receipt']['name']) && @$_FILES['receipt']['name'] != '') {
				$image = Input::file('receipt');
				$filename = 'Signature_' . rand(1000, 9999) . md5(time()) . '.' . $image->getClientOriginalExtension();

				if (move_uploaded_file($_FILES['receipt']['tmp_name'], BASEURL_FILE . "package/$filename")) {
					$update->ReceiptImage = "package/$filename";
				}
			}
			if (Input::get('return_type') == 'bycreturn' && Input::get('reject_by') == 'receiver' && $update->Status != 'ready') {
				$res = $this->create_new_request($id, $update);
				if ($res['success'] == 0) {
					return Redirect::to("admin/cancel_package/$id")->with('fail', $res['msg'])->withInput();
				}
			}
			$update->RejectBy = Input::get('reject_by');
			$update->ReturnType = Input::get('return_type');
			$update->CarrierMessage = Input::get('comment');
			$update->TrackingNumber = Input::get('tracking_number');
			$update->RejectTime = new MongoDate();
			$update->Status = 'cancel';

			// For penalty
			if (Input::get('penalty') > 0) {
				$error = '';
				$penalty = ((float) Input::get('penalty') * 100);
				$SenderUser = User::find($senderid);
				if (count($SenderUser) > 0) {
					if ($SenderUser->UserStripeId != '') {
						try {

							$res = $stripe->charge_customer($penalty, $SenderUser->UserStripeId, '');
							if (isset($res->id)) {
								$update->Penalty = $penalty;
								$update->PenaltyStatus = 'Done';

							} else {
								$error = $res->error->message;
							}
						} catch (Exception $e) {
							$error = $e->getMessage();
						}

					} else {
						$error = 'Sender does not have stripe account.';
					}
				}
			}

			// End penalty

			$update->save();

			return Redirect::to("admin/package")->withSuccess('Package cancel successfully.')->with('fail', $error);

		} else {
			return Redirect::to("admin/cancel_package/$id");
		}
	}
	public function admin_notification() {
		if (!(Auth::user()->UserPermission & CARRIER) == CARRIER) {
			return Redirect::to('/admin/dashboard');
		}
		Notification::where(array('GroupTo' => 'Admin', 'NotificationReadStatus' => 0))->update(array('NotificationReadStatus' => 1));

		$data['title'] = "Admin Notification";
		$data['paginationurl'] = "pagination/admin_notification";
		$data['orderType'] = "Desc";
		$data['orderby'] = "UserId";
		$data['postvalue'] = "&type=" . Input::get('type') . "&search_value=" . Input::get('search_value') . "&class=" . Input::get('class');

		return view('Admin::list.admin_notification', $data);
	}
	public function setting() {
		$data['info'] = Setting::find('563b0e31e4b03271a097e1ca');
		return view('Admin::list.setting', $data);
	}
	public function postsetting(Request $request) {

		$validate = Validator::make($request->all(), array('support_email' => 'required|email',
			'privacyemail' => 'required|email',
			'financeemail' => 'required|email',
			'PaginationLimit' => 'required|numeric'));
		if ($validate->fails()) {
			return Redirect::to("admin/setting")->withErrors($validate)->withInput();
		} else {

			$save = Setting::find('563b0e31e4b03271a097e1ca');
			if (count($save) > 0) {
				$save->SupportEmail = Input::get('support_email');
				$save->FinanceEmail = Input::get('financeemail');
				$save->PrivacyEmail = Input::get('privacyemail');
				$save->save();
			}

			Admin::where(array('_id' => Auth::user()->_id))->update(array('PaginationLimit' => (int) Input::get('PaginationLimit')));

			return Redirect::to('admin/setting')->withSuccess('Setting has been updated successfully.');
		}

	}

	public function refund_amount($id, Request $request) {
		$penalty = Input::get('penalty');
		if ($penalty != '') {
			$update = Deliveryrequest::find($id);
			$senderid = $update->SenderId;
			// For penalty
			if (Input::get('penalty') > 0) {
				$penalty = ((float) Input::get('penalty') * 100);
				$SenderUser = User::find($senderid);
				if (count($SenderUser) > 0) {
					if ($SenderUser->UserStripeId != '') {
						$stripe = new Stripe();
						try {

							$res = $stripe->charge_customer($penalty, $SenderUser->UserStripeId, '');
							if (isset($res->id)) {
								$update->Penalty = $penalty;
								$update->PenaltyStatus = 'Done';

							} else {
								//Redirect::with('fail',$res->error->message);
							}
						} catch (Exception $e) {
							//    Redirect::with('fail',$e->getMessage());
						}

					} else {
						$fail = 'Sender does not have stripe account.';
					}
				}
			}

			// End penalty
			$update->save();

			return Redirect::to("admin/package")->withSuccess('Penalty applied  successfully.');
		}
		return Redirect::to('admin/package')->withErrors(['credential_error' => 'Fail ! You have entered wrong penalty.']);

	}

	public function configuration() {
		$data["info"] = Configuration::find("5673e33e6734c4f874685c84");
		return view("Admin::list.configuration", $data);
	}

	public function postConfiguration(Request $request) {
		$validate = Validator::make(
			$request->all(),
			array(
				'aquantuo_fees' => 'required|numeric|between:0,99.9',
				'CoverageArea' => 'required|numeric',
				'aquantuo_fees_ld' => 'required|numeric|between:0,99.9',
				"dutyCustoms" => "required|numeric|between:0,99.9",
				"tax" => "required|numeric|between:0,99.9",
				"main_insurance" => "required|numeric|between:0,99.9",
				"bfm_concierge" => "required|numeric|between:0,99.9",
				"warehouse_transit_fee" => "required|numeric",
				"kenya_minimum_item_value" => "required|numeric",
				"kenya_per_item_value" => "required|numeric|between:0,99.9",
				"kenya_rate_per_kilogram" => "required|numeric"
			)
		);
		if ($validate->fails()) {
			return Redirect::to("admin/configuration")->withErrors($validate)->withInput();
		} else {
			if (is_array(Input::get('MaximumCost'))) {

				foreach (Input::get('MaximumCost') as $key => $val) {

					$array[] = array('MaxPrice' => (float) Input::get('MaximumCost')[$key],
						'MinPrice' => (float) Input::get('MinimumCost')[$key],
						'Rate' => (float) Input::get('InsuranceCost')[$key],
					);
				}
			} else {
				$array[] = array('MaxPrice' => 0, 'MinPrice' => 0, 'Rate' => 0);
			}

			$save = Configuration::find('5673e33e6734c4f874685c84');
			$save->aquantuoFees = Input::get('aquantuo_fees');
			$save->aquantuo_fees_ld = Input::get('aquantuo_fees_ld');
			$save->bfm_concierge = Input::get('bfm_concierge');
			$save->main_insurance = Input::get('main_insurance');
			$save->CoverageArea = Input::get('CoverageArea');
			$save->accra_charge_type = Input::get('accra_charge_type');
			$save->accra_charge = Input::get('accra_charge');
			$save->ProcessingFees = floatval(Input::get('processing_fees'));
			$save->duty_customs = floatval(Input::get('dutyCustoms'));
			$save->tax = floatval(Input::get('tax'));
			$save->Insurance = $array;
			$save->message = ucfirst(Input::get('msg'));
			$save->warehouse_transit_fee = Input::get("warehouse_transit_fee");
			$save->kenya_minimum_item_value = Input::get("kenya_minimum_item_value");
			$save->kenya_per_item_value = Input::get("kenya_per_item_value");
			$save->kenya_rate_per_kilogram = Input::get("kenya_rate_per_kilogram");
			// echo '<pre>';
			// print_r(json_decode($save));die();
			$save->save();

			return Redirect::to('admin/configuration')->withSuccess('Aquntuo fees has been updated successfully.');

		}
	}
//add-shipping
	public function GetWeightArray($id) {
		$data['info'] = category::find($id);

		$where = [
			"_id" => new MongoId($id),
		];
		$res = category::raw(function ($collection) use ($where, $data) {
			return $collection->aggregate([
				[
					'$project' => [
						'_id' => '$_id',
						'weightArray' => '$weightArray'
					],
				],
				[
					'$match' => $where,
				],

				[
					'$unwind' => '$weightArray',
				],

				[
					'$sort' => [
						'weightArray.MinWeight' => 1,
					],
				],
			]);
		});

		return $res['result'];
	}

	public function shipping($id) {
		$data['info'] = category::find($id);

		$where = [
			"_id" => new MongoId($id),
		];
		$res = category::raw(function ($collection) use ($where, $data) {
			return $collection->aggregate([
				[
					'$project' => [
						'_id' => '$_id',
						'Shipping' => '$Shipping',
						'weightArray' => '$weightArray'
					],
				],
				[
					'$match' => $where,
				],

				[
					'$unwind' => '$Shipping',
				],

				[
					'$sort' => [
						'Shipping.MinDistance' => 1,
					],
				],
			]);
		});
		$data['mul'] = $res['result'];
		$data["weightArrayInfo"] = $this->GetWeightArray($id);
		// echo "<pre>";
		// print_r($data);
		if($id == OtherCategory || $id == ElectronicsCategory){
			// $data["airCategory"] = OtherCategory;
			// dd($data);
			return view('Admin::list.otherShipping', $data);
		}else{
			//echo "string";die;
			return view('Admin::list.shipping', $data);
		}
	}

	public function postOtherShipping($id, Request $request) {
		$validation = Validator::make($request->all(), array(
			'weight_increase_by' => 'required|numeric',
			'price_increase_by' => 'required|numeric',
		));
		if ($validation->fails()) {
			return Redirect::back()->withErrors($validation)->withInput();
		} else {
			if (is_array(Input::get('MaximumDistance'))) {
				foreach (Input::get('MaximumDistance') as $key => $val) {

					$array[] = array('MaxDistance' =>  Input::get('MaximumDistance')[$key],
						'MinDistance' => (float) Input::get('MinimumDistance')[$key],
						'Rate' => (float) Input::get('Rate')[$key],
					);
				}
			}else {
				$array[] = array('MaxDistance' => 0, 'MinDistance' => 0,
					'Rate' => 0);
			}

			if (is_array(Input::get("MinWeight"))) {
				foreach (Input::get("MinWeight") as $key => $value) {
					$weightArray[] = array(
						"MaxWeight" => Input::get("MaxWeight")[$key],
						"MinWeight" => Input::get("MinWeight")[$key],
						"Rate" => Input::get("weight_rate")[$key]
					);
				}
			} else {
				$weightArray[] = array("MaxWeight" => 0, "MinWeight" => 0, "Rate" => 0);
			}

			$save = category::find($id);
			$save->Shipping = $array;
			$save->weightArray = $weightArray;
			$save->custom_duty = floatval(Input::get("custom_duty"));
			$save->weight_increase_by = floatval(Input::get('weight_increase_by'));
			$save->price_increase_by = floatval(Input::get('price_increase_by'));
			$save->save();
			return Redirect::back()->withSuccess('shipping Added Successfully');
		}
	}

	

	public function postshipping($id, Request $request) {
		if (is_array(Input::get('MaximumDistance'))) {
			foreach (Input::get('MaximumDistance') as $key => $val) {
				$array[] = array('MaxDistance' =>  Input::get('MaximumDistance')[$key],
					'MinDistance' => (float) Input::get('MinimumDistance')[$key],
					'Rate' => (float) Input::get('Rate')[$key],
					'CONSOLIDATE_RATE' => (float) Input::get('CONSOLIDATE_RATE')[$key],
				);
			}
		} else {
			$array[] = array('MaxDistance' => 0, 'MinDistance' => 0,
				'Rate' => 0);
		}

		$save = category::find($id);
		$save->Shipping = $array;
		$save->custom_duty = floatval(Input::get("custom_duty"));
		$save->save();
		return Redirect::to("admin/add-shipping/$id")->withSuccess('shipping Added Successfully');

	}

	// local shipping

	public function localshipping($id) {
		$data['info'] = category::find($id);

		$where = [
			"_id" => new MongoId($id),
		];
		$res = category::raw(function ($collection) use ($where, $data) {
			return $collection->aggregate([
				[
					'$project' => [
						'_id' => '$_id',
						'Shipping' => '$Shipping',

					],
				],
				[
					'$match' => $where,
				],

				[
					'$unwind' => '$Shipping',
				],
				[
					'$sort' => [
						'Shipping.MinDistance' => 1,
					],
				],
			]);
		});

		$data['mul'] = $res['result'];

		return view('Admin::list.localshipping', $data);
	}
	public function localpostshipping($id, Request $request) {
		if (is_array(Input::get('MaximumDistance'))) {
			foreach (Input::get('MaximumDistance') as $key => $val) {

				$array[] = array('MaxDistance' => (float) Input::get('MaximumDistance')[$key],
					'MinDistance' => (float) Input::get('MinimumDistance')[$key],
					'Rate' => (float) Input::get('Rate')[$key],
				);
			}
		} else {
			$array[] = array('MaxDistance' => 0, 'MinDistance' => 0,
				'Rate' => 0);
		}

		$save = category::find($id);
		$save->Shipping = $array;

		$save->save();

		return Redirect::to("admin/add-local-shipping/$id")->withSuccess('shipping Added Successfully');

	}

	//Trip
	public function trip() {
		if (!(Auth::user()->UserPermission & INDIVIDUAL_TRIP) == INDIVIDUAL_TRIP) {
			return Redirect::to('/admin/dashboard');
		}
		$data['title'] = " Indiviual Trip";
		$data['paginationurl'] = "pagination/trip";
		$data['orderType'] = "Desc";
		$data['orderby'] = "_id";
		$data['postvalue'] = "&search_value=" . Input::get('search_value');

		return view('Admin::list.trips', $data);

	}
	public function trips_request($id) {
		if (!(Auth::user()->UserPermission & INDIVIDUAL_TRIP) == INDIVIDUAL_TRIP) {
			return Redirect::to('/admin/dashboard');
		}
		$data['title'] = "Indiviual Trips Request List";
		$data['paginationurl'] = "pagination/trips_request/{$id}";
		$data['orderType'] = "Desc";
		$data['orderby'] = "_id";
		$data['postvalue'] = "&search_value=" . Input::get('search_value');

		$data['info'] = Trips::find($id);

		if (count($data['info']) > 0) {
			return view('Admin::list.trips_request', $data);
		} else {
			return view('Admin::list.trips_request', $data);
		}

	}
	public function business_trip() {
		if (!(Auth::user()->UserPermission & BUSSINESS_TRIP) == BUSSINESS_TRIP) {
			return Redirect::to('/admin/dashboard');
		}
		$data['title'] = "Business Trip";
		$data['paginationurl'] = "pagination/business-trip";
		$data['orderType'] = "Desc";
		$data['orderby'] = "_id";
		$data['postvalue'] = "&search_value=" . Input::get('search_value');
		return view('Admin::list.business_trip', $data);

	}

	public function business_request($id, Request $request) {
		if (!(Auth::user()->UserPermission & BUSSINESS_TRIP) == BUSSINESS_TRIP) {
			return Redirect::to('/admin/dashboard');
		}
		$data['title'] = "Business Trip Request";
		$data['paginationurl'] = "pagination/business_request/{$id}";
		$data['orderType'] = "Desc";
		$data['orderby'] = "_id";
		$data['postvalue'] = "&search_value=" . Input::get('search_value');
		$data['info'] = Trips::find($id);

		if (count($data['info']) > 0) {
			return view('Admin::list.business_request', $data);

		} else {
			return view('Admin::list.business_request', $data);
		}

	}
	public function edit_business_trip($id) {
		if (!(Auth::user()->UserPermission & BUSSINESS_TRIP) == BUSSINESS_TRIP) {
			return Redirect::to('/admin/dashboard');
		}
		$data['info'] = Trips::find($id);
		if (count($data['info']) > 0) {

			$where = array('type' => 'Country');
			$data['country'] = CityStateCountry::where($where)->get();

			$where = array('type' => 'Category');
			$data['category'] = category::where($where)->get();

			return view('Admin::edit.business_trip', $data);
		} else {
			return Redirect::to('admin/business-trip');
		}

	}

	public function post_individual_trip($id, Request $request) {
		if (!(Auth::user()->UserPermission & INDIVIDUAL_TRIP) == INDIVIDUAL_TRIP) {
			return Redirect::to('/admin/dashboard');
		}
		$validation = Validator::make($request->all(), array('TransporterName' => 'required', 'SourceAddress' => 'required', 'DestiAddress' => 'required', 'Weight' => 'required|numeric', 'description' => 'required'));
		if ($validation->fails()) {
			return Redirect::to("admin/edit-individual-trip/$id")->withErrors($validation)->withInput();
		} else {
			$save = Trips::find($id);
			if (count($save) > 0) {
				$source_country = json_decode(Input::get('SourceCountry'));
				$SourceState = json_decode(Input::get('SourceState'));
				$SourceCity = json_decode(Input::get('SourceCity'));

				$des_country = json_decode(Input::get('nd_return_country'));
				$des_state = json_decode(Input::get('drop_off_state'));
				$des_city = json_decode(Input::get('drop_off_city'));

				$save->TransporterName = ucfirst(Input::get('TransporterName'));
				$save->sourceAddress = ucfirst(Input::get('sourceAddress'));
				$save->SourceCountry = ucfirst(@$source_country->name);
				$save->SourceState = ucfirst(@$SourceState->name);
				$save->SourceCity = ucfirst(@$SourceCity->name);
				$save->DestiAddress = ucfirst(Input::get('DestiAddress'));
				$save->DestiCountry = ucfirst(@$des_country->name);
				$save->DestiState = ucfirst(@$des_state->name);
				$save->DestiCity = ucfirst(@$des_city->name);
				$save->Weight = ucfirst(Input::get('Weight'));
				$save->TravelMode = Input::get('TravelMode');
				$save->Description = Input::get('description');
				if (input::get('TravelMode') == 'air') {
					$save->SelectCategory = array_map('ucfirst', (array_map('trim', Input::get('aircategory'))));
				} else {

					$save->SelectCategory = array_map('ucfirst', (array_map('trim', Input::get('shipcategory'))));
				}

				$save->save();
			}

			return Redirect::to('admin/indiviual-trip')->withSuccess('business trip updated successfully.');
		}

	}

	public function post_business_trip($id, Request $request) {
		if (!(Auth::user()->UserPermission & BUSSINESS_TRIP) == BUSSINESS_TRIP) {
			return Redirect::to('/admin/dashboard');
		}
		$validation = Validator::make($request->all(), array('TransporterName' => 'required',
			'Weight' => 'required|numeric',
			'Description' => 'required',
			'SourceAddress' => 'required',
			'DestiAddress' => 'required',
		));
		if ($validation->fails()) {
			return Redirect::to("admin/edit-business-trip/$id")->withErrors($validation)->withInput();
		} else {
			$save = Trips::find($id);
			if (count($save) > 0) {

				$source_country = json_decode(Input::get('SourceCountry'));
				$SourceState = json_decode(Input::get('SourceState'));
				$SourceCity = json_decode(Input::get('SourceCity'));

				$des_country = json_decode(Input::get('nd_return_country'));
				$des_state = json_decode(Input::get('drop_off_state'));
				$des_city = json_decode(Input::get('drop_off_city'));

				$save->TransporterName = ucfirst(Input::get('TransporterName'));
				$save->SourceCountry = trim(Input::get('country'));
				if (input::get('TravelMode') == 'air') {
					$save->Categories = array_map('ucfirst', (array_map('trim', Input::get('aircategory'))));
				} else {
					$save->Categories = array_map('ucfirst', (array_map('trim', Input::get('shipcategory'))));
				}
				$save->Weight = ucfirst(Input::get('Weight'));
				$save->TravelMode = Input::get('TravelMode');
				$save->Description = ucfirst(Input::get('Description'));
				$save->SourceAddress = ucfirst(Input::get('SourceAddress'));
				$save->DestiAddress = ucfirst(Input::get('DestiAddress'));

				$save->SourceCountry = ucfirst(@$source_country->name);
				$save->SourceState = ucfirst(@$SourceState->name);
				$save->SourceCity = ucfirst(@$SourceCity->name);

				$save->DestiCountry = ucfirst(@$des_country->name);
				$save->DestiState = ucfirst(@$des_state->name);
				$save->DestiCity = ucfirst(@$des_city->name);

				$save->save();
			}

			return Redirect::to('admin/business-trip')->withSuccess('business trip updated successfully.');

		}
	}

	public function edit_individual_trip($id) {
		if (!(Auth::user()->UserPermission & INDIVIDUAL_TRIP) == INDIVIDUAL_TRIP) {
			return Redirect::to('/admin/dashboard');
		}
		//$data['info'] = Trips::find($id);
		$data['info'] = Trips::where(['_id' => $id])->first();
		/*echo '<pre>';
        print_r($data['info']); die;*/

		if (count($data['info']) > 0) {
			//~ $where = array('type'=>'Country');
			//~ $data['country'] = CityStateCountry::where($where)->get();

			$where = array('type' => 'Category');
			$data['category'] = category::where($where)->get();

			$data['state'] = CityStateCountry::where(array('type' => array('$in' => array('State', 'Country'))))->get(array('Content', 'type', 'state_available'));
			$data['country'] = CityStateCountry::where(['type' => 'Country', 'Status' => 'Active'])->get(['_id', 'Content', 'state_available']);
			/*echo '<pre>';
            print_r( $data['country'] ); die;*/

			return view('Admin::edit.individual', $data);
		} else {
			return Redirect::to('admin/indiviual-trip');
		}

	}

	//End Trip
	public function accept_product(Request $request) {
		$responce = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];

		$where = [
			"ProductList" => ['$elemMatch' => [
				'_id' => $request->get('product_id'),
				'status' => 'ready',
			]],
		];

		$deliveryInfo = Deliveryrequest::where(array('_id' => $request->get('request_id')))
			->where($where)->select('ProductList.$', 'RequesterId', 'PackageId', 'PackageId', 'DeliveryFullAddress', 'PackageNumber', 'shippingCost', 'TotalCost')->first();

		if (count($deliveryInfo) > 0) {

			$updation = Deliveryrequest::where(array('_id' => $request->get('request_id')))
				->where($where)
				->update(['ProductList.$.status' => 'accepted', 'Status' => 'accepted']);

			if ($updation) {

				$count = Deliveryrequest::where(array("ProductList" => array('$elemMatch' => array('status' => 'ready'))))
					->where(array('_id' => $request->get('request_id')))
					->count();

				$responce = ['success' => 1, 'msg' => 'Product accepted successfully.', 'count' => $count];

				$user_data = User::where(array('_id' => $deliveryInfo->RequesterId))->first();

				if (count($user_data) > 0) {
					Reqhelper::update_status($deliveryInfo->_id);

					foreach ($deliveryInfo->ProductList as $key) {
						$productName = $key['product_name'];
					}
					//notification

					$Notification = new Notify();
					$Notification->setValue('title', 'Request accepted');
					$Notification->setValue('message', 'Aquantuo has accepted your online request for ' . $productName . '.');
					$Notification->setValue('type', 'online_request_accepted');
					$Notification->setValue('location', 'online_request_accepted');
					$Notification->setValue('itemId', $request->get('product_id'));
					$Notification->setValue('locationkey', $request->get('request_id'));
					$Notification->add_user($user_data->NotificationId, $user_data->DeviceType);
					$Notification->fire();

					$insNotification = array(
						array(
							"NotificationTitle" => 'Request accepted',
							"NotificationMessage" => 'Aquantuo has accepted your online request for ' . $productName . '.',
							"NotificationType" => "online_request_accepted",
							"NotificationUserId" => array(new MongoId($user_data->_id)),
							"Date" => new MongoDate(),
							"GroupTo" => "User",
						),
					);

					Notification::Insert($insNotification);

					send_mail('57d7d4b37ac6f69c158b4569', [
						"to" => $user_data->Email,
						"replace" => [
							"[USERNAME]" => $user_data->Name,
							"[PACKAGETITLE]" => $productName,
							"[PACKAGEID]" => $deliveryInfo->ProductList[0]['package_id'],
							"[SOURCE]" => $deliveryInfo->PickupAddress,
							"[DESTINATION]" => $deliveryInfo->DeliveryFullAddress,
							"[PACKAGENUMBER]" => $deliveryInfo->ProductList[0]['package_id'],
							"[SHIPPINGCOST]" => $deliveryInfo->ProductList[0]['shippingCost'],
						],
					]
					);
				}
			}

		}

		return json_encode($responce);

	}

	public function online_package_review(Request $request) {
		$responce = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		$where = [
			"ProductList" => ['$elemMatch' => [
				'_id' => $request->get('product_id'),
				'status' => 'ready',
			]],
		];

		$deliveryInfo = Deliveryrequest::where(array('_id' => $request->get('request_id')))
			->where($where)->select('ProductList.$', 'RequesterId', 'PackageId', 'PackageId', 'DeliveryFullAddress', 'PackageNumber', 'shippingCost', 'TotalCost', 'RequestType', 'package_id', 'PickupAddress')->first();

		if (count($deliveryInfo) > 0) {
			$updation = Deliveryrequest::where(array('_id' => $request->get('request_id')))
				->where($where)
				->update(['ProductList.$.status' => 'reviewed',
					'Status' => 'reviewed',
				]);

			/* Activity Log */

			$insertactivity = [
				'request_id' => $request->get('request_id'),
				'request_type' => $deliveryInfo->RequestType,
				'PackageNumber' => $deliveryInfo->PackageNumber,
				'item_id' => $request->get('product_id'),
				'package_id' => $deliveryInfo->ProductList[0]['package_id'],
				'item_name' => $deliveryInfo->ProductList[0]['product_name'],
				'log_type' => 'request',
				'message' => 'Listing Reviewed.',
				'status' => 'reviewed',
				'action_user_id' => Auth::user()->_id,
				'EnterOn' => new MongoDate(),

			];

			Activitylog::insert($insertactivity);

			if ($updation) {

				$count = Deliveryrequest::where(array("ProductList" => array('$elemMatch' => array('status' => 'ready'))))
					->where(array('_id' => $request->get('request_id')))
					->count();
				$responce = ['success' => 1, 'msg' => 'Product reviewed successfully.', 'count' => $count];
				$user_data = User::where(array('_id' => $deliveryInfo->RequesterId))->first();
				if (count($user_data) > 0) {
					Reqhelper::update_status($deliveryInfo->_id);

					foreach ($deliveryInfo->ProductList as $key) {
						$productName = $key['product_name'];
					}

					if ($deliveryInfo->RequestType == 'online') {
						$noti_msg = trans('lang.ITEM_REVIEW_MEG_ONLINE');
						$notification_type = 'requester_online_detail';
						$notification_location = 'requester_online_detail';
					} else {
						$noti_msg = trans('lang.ITEM_REVIEW_MEG_BUY');
						$notification_type = 'BUY_FOR_ME_DELIVERY_DETAIL_FOR_REQUESTER';
						$notification_location = 'BUY_FOR_ME_DELIVERY_DETAIL_FOR_REQUESTER';
					}

					// notification
					$Notification = new Notify();
					$Notification->setValue('title', trans('lang.ITEM_REVIEW_TITLE'));
					$Notification->setValue('message', sprintf($noti_msg, $productName));
					$Notification->setValue('type', $notification_type);
					$Notification->setValue('location', $notification_location);
					$Notification->setValue('itemId', $request->get('product_id'));
					$Notification->setValue('locationkey', $request->get('request_id'));
					$Notification->add_user($user_data->NotificationId, $user_data->DeviceType);
					$Notification->fire();

					$insNotification = array(
						array(
							"NotificationTitle" => trans('lang.ITEM_REVIEW_TITLE'),
							"NotificationMessage" => sprintf($noti_msg, $productName),
							"NotificationType" => "requester_online_detail",
							"NotificationUserId" => array(new MongoId($user_data->_id)),
							"Date" => new MongoDate(),
							"GroupTo" => "User",
						),
					);

					Notification::Insert($insNotification);
					
					$cron_mail = [
						"USERNAME" => ucfirst($user_data->Name),
						"PACKAGETITLE" => ucfirst($productName),
						"PACKAGEID" => $deliveryInfo->ProductList[0]["package_id"],
						"SOURCE" => $deliveryInfo->PickupAddress,
						"DESTINATION" => $deliveryInfo->DeliveryFullAddress,
						"PACKAGENUMBER" => $deliveryInfo->ProductList[0]["package_id"],
						"SHIPPINGCOST" => $deliveryInfo->ProductList[0]["shippingCost"],
						"email_id" => "589c485f7ac6f6c1248b4567",
						"email" => $user_data->Email,
						"status" => "ready"
					];
					SendMail::insert($cron_mail);
				}
			}
		}
		return json_encode($responce);
	}

	public function accept_all_product($id = '', Request $request) {

		$deliveryInfo = Deliveryrequest::where(array('_id' => $id))->first();
		$array = $deliveryInfo->ProductList;

		if (is_array(Input::get('item_name')) && !empty(Input::get('item_name'))) {
			foreach ($deliveryInfo->ProductList as $key => $value) {
				if (in_array($value['_id'], Input::get('item_name'))) {
					$array[$key]['status'] = 'reviewed';
					$insertactivity = [
						'request_id' => $id,
						'request_type' => $deliveryInfo->RequestType,
						'PackageNumber' => $deliveryInfo->PackageNumber,
						'item_id' => $value['_id'],
						'package_id' => $value['package_id'],
						'item_name' => $value['product_name'],
						'log_type' => 'request',
						'message' => 'Product reviewed.',
						'status' => 'reviewed',
						'action_user_id' => Auth::user()->_id,
						'EnterOn' => new MongoDate(),
					];
					Activitylog::insert($insertactivity);
				}

			}

			$updation = Deliveryrequest::where('_id', '=', $id)
				->update(['ProductList' => $array]);

			if ($deliveryInfo->RequestType == 'delivery') {
				Reqhelper::update_status2($deliveryInfo->_id);
			} else {
				Reqhelper::update_status($deliveryInfo->_id);
			}
			$user_data = User::where(array('_id' => $deliveryInfo->RequesterId))->first();

			if (count($user_data) > 0) {
				$Notification = new Notify();
				$Notification->setValue('title', trans('lang.ONLINE_REVIEWED_TITLE'));
				$Notification->setValue('message', sprintf(trans('lang.ONLINE_REVIEWED_MSG'), $deliveryInfo->ProductTitle));
				$Notification->setValue('type', 'online_request_accepted');
				$Notification->setValue('location', 'online_request_accepted');
				$Notification->setValue('locationkey', $id);

				$Notification->add_user($user_data->NotificationId, $user_data->DeviceType);
				$Notification->fire();

				$insNotification = array(
					array(
						"NotificationTitle" => 'Request reviewed',
						"NotificationShortMessage" => sprintf('Your request for "%s" has been reviewed.', $deliveryInfo->ProductTitle),
						"NotificationMessage" => sprintf('Your request for "%s" has been reviewed.', $deliveryInfo->ProductTitle),
						"NotificationType" => "online_request_accepted",
						"NotificationUserId" => array(new MongoId($user_data->_id)),
						"Date" => new MongoDate(),
						"GroupTo" => "User",
					),
				);

				Notification::Insert($insNotification);
				$Email = new NewEmail();
				//Mail to user
				$Email->send_mail('57d7d4b37ac6f69c158b4569', [
					"to" => $user_data->Email,
					"replace" => [
						"[USERNAME]" => $user_data->Name,
						"[PACKAGETITLE]" => $deliveryInfo->ProductTitle,
						"[PACKAGEID]" => $deliveryInfo->PackageId,
						"[SOURCE]" => $deliveryInfo->PickupAddress,
						"[DESTINATION]" => $deliveryInfo->DeliveryFullAddress,
						"[PACKAGENUMBER]" => $deliveryInfo->PackageNumber,
						"[SHIPPINGCOST]" => $deliveryInfo->shippingCost,
						"[TOTALCOST]" => $deliveryInfo->TotalCost,
					],
				]
				);
			}

			if ($deliveryInfo->RequestType == 'buy_for_me') {
				return Redirect::to('admin/buy-for-me/detail/' . $id)->withSuccess('Product accepted successfully.');
			} elseif ($deliveryInfo->RequestType == 'delivery') {
				return Redirect::to('admin/package/detail/' . $id)->withSuccess('Product accepted successfully.');
			} elseif ($deliveryInfo->RequestType == 'online') {
				return Redirect::to('admin/online_package/detail/' . $id)->withSuccess('Product accepted successfully.');
			}
		} else {
			return Redirect::back();
		}
	}

	public function cancelAll($id = '', Request $request) {
		$deliveryInfo = Deliveryrequest::where(['_id' => $id])->first();
		$array = $deliveryInfo->ProductList;
		$i = 0;
		foreach ($array as $key) {
			if (in_array($key['_id'], Input::get('item_name'))) {
				$array[$i]['status'] = 'cancel_by_admin';

				/* Activity Log */
				$insertactivity = [
					'request_id' => $id,
					'request_type' => $deliveryInfo->RequestType,
					'PackageNumber' => $deliveryInfo->PackageNumber,
					'item_id' => $key['_id'],
					'package_id' => $key['package_id'],
					'item_name' => $key['product_name'],
					'log_type' => 'request',
					'message' => 'Product has been cancel.',
					'status' => 'cancel_by_admin',
					'action_user_id' => Auth::user()->_id,
					'EnterOn' => new MongoDate(),
				];
				Activitylog::insert($insertactivity);
			}
			$i++;
		}

		$updation = Deliveryrequest::where('_id', '=', $id)
			->update(['ProductList' => $array]);

		$user_data = User::where(array('_id' => $deliveryInfo->RequesterId))->first();

		if (count($user_data) > 0) {
			$Notification = new Notify();
			$Notification->setValue('title', 'Request canceled');
			$Notification->setValue('message', 'Your  request has been canceled.');
			$Notification->setValue('type', 'online_request_canceled');
			$Notification->setValue('location', 'online_request_canceled');
			$Notification->setValue('locationkey', $request->get('request_id'));
			$Notification->setValue('itemId', $request->get('product_id'));
			$Notification->add_user($user_data->NotificationId, $user_data->DeviceType);
			$Notification->fire();

			$insNotification = array(
				array(
					"NotificationTitle" => 'Request canceled',
					"NotificationShortMessage" => 'Your  request has been canceled.',
					"NotificationMessage" => 'Your  request has been canceled.',
					"NotificationType" => "online_request_canceled",
					"NotificationUserId" => array($user_data->_id),
					"Date" => new MongoDate(),
					"GroupTo" => "User",
				),
			);

			Notification::Insert($insNotification);

			//Mail
			foreach ($deliveryInfo->ProductList as $key) {
				$productName = $key['product_name'];
			}

			//Mail to user
			send_mail('57d7d8bc7ac6f69c158b456a', [
				"to" => $user_data->Email,
				"replace" => [
					"[USERNAME]" => $user_data->Name,
					"[PACKAGETITLE]" => $productName,
					"[PACKAGEID]" => $deliveryInfo->PackageId,
					"[SOURCE]" => $deliveryInfo->PickupAddress,
					"[DESTINATION]" => $deliveryInfo->DeliveryFullAddress,
					"[PACKAGENUMBER]" => $deliveryInfo->PackageNumber,
					"[SHIPPINGCOST]" => $deliveryInfo->shippingCost,
					"[TOTALCOST]" => $deliveryInfo->TotalCost,
				],
			]
			);
		}

		if ($deliveryInfo->RequestType == 'buy_for_me') {
			return Redirect::to('admin/buy-for-me/detail/' . $id)->withSuccess('All product accepted successfully.');
		} else {
			return Redirect::to('admin/online_package/detail/' . $id)->withSuccess('All product accepted successfully.');
		}
	}

	public function purchased_all($id = '', Request $request) {

		$deliveryInfo = Deliveryrequest::where(array('_id' => $id))->first();
		$array = $deliveryInfo->ProductList;
		$product_name = '';
		$i = 0;
		foreach ($array as $key) {
			if (in_array($key['_id'], Input::get('item_name'))) {
				$array[$i]['status'] = 'purchased';

				/* Activity Log */
				$insertactivity = [
					'request_id' => $id,
					'request_type' => $deliveryInfo->RequestType,
					'PackageNumber' => $deliveryInfo->PackageNumber,
					'item_id' => $key['_id'],
					'package_id' => $key['package_id'],
					'item_name' => $key['product_name'],
					'log_type' => 'request',
					'message' => 'item Purchased.',
					'status' => 'purchased',
					'action_user_id'=>Auth::user()->_id,
					'EnterOn' => new MongoDate(),
				];
				Activitylog::insert($insertactivity);
				if($product_name != ''){
					$product_name .= ','.$key['product_name'];
				}else{
					$product_name .= $key['product_name'];
				}

			}
			$i++;

			$product_detail = $key;
		}

		$updation = Deliveryrequest::where('_id', '=', $id)
			->update(['ProductList' => $array]);
		Reqhelper::update_status($id);
		$user_data = User::where(array('_id' => $deliveryInfo->RequesterId))->first();

		$message = sprintf(trans('lang.ITEM_PURCHASED_MSG2'), $product_name);
		$title = trans('lang.ITEM_PURCHASED_TITLE');
		$type = 'requester_buy_for_me_detail';
		$email_id = '58a1ba586befd910a15c1c2e';

		if (count($user_data) > 0) {
			$Notification = new Notify();
			$Notification->setValue('title', $title);
			$Notification->setValue('message', $message);
			$Notification->setValue('type', $type);
			$Notification->setValue('location', $type);
			$Notification->setValue('itemId', Input::get('itemid'));
			$Notification->setValue('locationkey', Input::get('requestid'));
			$Notification->add_user($user_data->NotificationId, $user_data->DeviceType);
			$Notification->fire();

			$insNotification = array(
				array(
					"NotificationTitle" => $title,
					"NotificationShortMessage" => $message,
					"NotificationMessage" => $message,
					"NotificationType" => $type,
					"NotificationUserId" => array(new MongoId($user_data->_id)),
					"Date" => new MongoDate(),
					"GroupTo" => "User",
				),
			);

			Notification::Insert($insNotification);
			$Email = new NewEmail();
			$Email->send_mail($email_id, [
				"to" => $user_data->Email,
				"replace" => [
					"[USERNAME]" => $user_data->Name,
					"[PACKAGETITLE]" => $product_name,
					"[ITEM_PRICE]" => $product_detail['price'],
					"[SHIPPINGCOST]" => $product_detail['shippingCost'],
					"[PACKAGEID]" => $deliveryInfo->PackageId,
					"[SOURCE]" => $deliveryInfo->PickupAddress,
					"[DESTINATION]" => $deliveryInfo->DeliveryFullAddress,
					"[PACKAGENUMBER]" => $deliveryInfo->PackageNumber,
					"[TOTALCOST]" => $deliveryInfo->TotalCost,
					"[ACCEPT_BY]" => "Aquantuo",
				],
			]
			);
		}
		if ($deliveryInfo->RequestType == 'buy_for_me') {
			return Redirect::to('admin/buy-for-me/detail/' . $id)->withSuccess('All product accepted successfully.');
		} else {
			return Redirect::to('admin/online_package/detail/' . $id)->withSuccess('All product accepted successfully.');
		}

		if ($deliveryInfo->RequestType == 'buy_for_me') {
			return Redirect::to('admin/buy-for-me/detail/' . $id)->withFail('Somthing went wrong.');
		}

		return Redirect::to('admin/online_package/detail/' . $id)->withFail('Somthing went wrong.');
	}

	public function receivedAll($id = '', Request $request) {

		$deliveryInfo = Deliveryrequest::where(array('_id' => $id))->first();
		$array = $deliveryInfo->ProductList;
		$product_name = '';
		$email_id = '';
		if ($deliveryInfo->RequestType == 'buy_for_me') {

			foreach ($deliveryInfo->ProductList as $key => $value) {
				if (in_array($value['_id'], Input::get('item_name'))) {
					if ($value['travelMode'] == 'air') {
						$startdate = strtotime(date("Y-m-d"));
						$Buinessdays = new Buinessdays();
						$result = $Buinessdays->getWorkingDays($startdate);
						$ExpectedDate = new Mongodate(strtotime('+' . $result . 'days'));
					} else {
						$ExpectedDate = new Mongodate(strtotime('+ 65 days'));
					}

					$array[$key]['status'] = 'item_received';
					$array[$key]['ExpectedDate'] = $ExpectedDate;
					Activitylog::insert([
						'request_id' => $id,
						'request_type' => $deliveryInfo->RequestType,
						'PackageNumber' => $deliveryInfo->PackageNumber,
						'item_id' => $value['_id'],
						'package_id' => $value['package_id'],
						'item_name' => $value['product_name'],
						'log_type' => 'request',
						'message' => 'Your item has been received.',
						'status' => 'item_received',
						'action_user_id' => Auth::user()->_id,
						'EnterOn' => new MongoDate(),
					]);

					if($product_name != ''){
						$product_name .= ','.$value['product_name'];
					}else{
						$product_name .= $value['product_name'];
					}
					
				}
			}
			Deliveryrequest::where('_id', '=', $id)->update(['ProductList' => $array]);
			$email_id = '594cadb06befd91cd70addb6';
		}

		if ($deliveryInfo->RequestType == 'online') {
			foreach ($deliveryInfo->ProductList as $key => $value) {
				if (in_array($value['_id'], Input::get('item_name'))) {
					if ($value['travelMode'] == 'air') {
						$startdate = strtotime(date("Y-m-d"));
						$Buinessdays = new Buinessdays();
						$result = $Buinessdays->getWorkingDays($startdate);
						$ExpectedDate = new Mongodate(strtotime('+' . $result . 'days'));
					} else {
						$ExpectedDate = new Mongodate(strtotime('+ 65 days'));
					}

					$array[$key]['status'] = 'purchased';
					$array[$key]['ExpectedDate'] = $ExpectedDate;

					Activitylog::insert([
						'request_id' => $id,
						'request_type' => $deliveryInfo->RequestType,
						'PackageNumber' => $deliveryInfo->PackageNumber,
						'item_id' => $value['_id'],
						'package_id' => $value['package_id'],
						'item_name' => $value['product_name'],
						'log_type' => 'request',
						'message' => 'Your item has been received.',
						'status' => 'item_received',
						'action_user_id' => Auth::user()->_id,
						'EnterOn' => new MongoDate(),
					]);

					if($product_name != ''){
						$product_name .= ','.$value['product_name'];
					}else{
						$product_name .= $value['product_name'];
					}
				}

			}

			Deliveryrequest::where('_id', '=', $id)->update(['ProductList' => $array]);
			$email_id = '590191857ac6f6a2188b4567';
		}

		if (count($deliveryInfo) > 0) {
			Reqhelper::update_status($deliveryInfo->_id);
			$user_data = User::where(array('_id' => $deliveryInfo->RequesterId))->first();

			$message = sprintf(trans('lang.ITEM_RECEIVED_MSG2'), $product_name);
			$title = trans('lang.ITEM_RECEIVED');
			$type = 'requester_buy_for_me_detail';
			

			if (count($user_data) > 0) {
				$Notification = new Notify();
				$Notification->setValue('title', $title);
				$Notification->setValue('message', $message);
				$Notification->setValue('type', $type);
				$Notification->setValue('location', $type);
				$Notification->setValue('itemId', Input::get('itemid'));
				$Notification->setValue('locationkey', Input::get('requestid'));
				$Notification->add_user($user_data->NotificationId, $user_data->DeviceType);
				$Notification->fire();

				$insNotification = array(
					array(
						"NotificationTitle" => $title,
						"NotificationShortMessage" => $message,
						"NotificationMessage" => $message,
						"NotificationType" => $type,
						"NotificationUserId" => array(new MongoId($user_data->_id)),
						"Date" => new MongoDate(),
						"GroupTo" => "User",
					),
				);

				Notification::Insert($insNotification);
				$where = [
					"ProductList" => ['$elemMatch' => ['_id' => Input::get('item_name')[0]]],
					'_id' => $id,
				];

				$req_info = Deliveryrequest::where($where)->select('ProductList.$')->first();
				$show_date = '';
				if(isset($req_info->ProductList[0]['ExpectedDate']->sec)){
					$show_date = show_date(@$req_info->ProductList[0]['ExpectedDate']);
				}

				$Email = new NewEmail();
				$Email->send_mail($email_id, [
					"to" => $user_data->Email,
					"replace" => [
						"[USERNAME]" => $user_data->Name,
						"[PACKAGETITLE]" => $product_name,
						"[ITEM_PRICE]" => floatval($deliveryInfo->TotalCost),
						"[SHIPPINGCOST]" => floatval($deliveryInfo->shippingCost),
						"[PACKAGEID]" => $deliveryInfo->PackageId,
						"[SOURCE]" => $deliveryInfo->PickupAddress,
						"[DESTINATION]" => $deliveryInfo->DeliveryFullAddress,
						"[PACKAGENUMBER]" => $deliveryInfo->PackageNumber,
						"[TOTALCOST]" => $deliveryInfo->TotalCost,
						"[ACCEPT_BY]" => "Aquantuo",
						"[DATE]"=>$show_date,
					],
				]
				);

			}
			if ($deliveryInfo->RequestType == 'buy_for_me') {
				return Redirect::to('admin/buy-for-me/detail/' . $id)->withSuccess('All product accepted successfully.');
			} else {
				return Redirect::to('admin/online_package/detail/' . $id)->withSuccess('All product accepted successfully.');
			}

			if ($deliveryInfo->RequestType == 'buy_for_me') {
				return Redirect::to('admin/buy-for-me/detail/' . $id)->withFail('Somthing went wrong.');
			}
		}

		return Redirect::to('admin/online_package/detail/' . $id)->withFail('Somthing went wrong.');

	}
	/* cancel request for online case */
	public function cancel_request(Request $request) {
		$responce = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		$where = array("ProductList" => array('$elemMatch' => array('_id' => $request->get('product_id'))));

		$deliveryInfo = Deliveryrequest::where(array('_id' => $request->get('request_id')))->where($where)->select('ProductList.$', 'PackageId', 'PickupAddress', 'DeliveryFullAddress', 'PackageNumber', 'shippingCost', 'TotalCost', '_id', 'RequestType')->first();
		if (count($deliveryInfo) > 0) {

			/* Activity Log */
			$insertactivity = [
				'request_id' => $deliveryInfo->_id,
				'request_type' => $deliveryInfo->RequestType,
				'PackageNumber' => $deliveryInfo->PackageNumber,
				'item_id' => $deliveryInfo->ProductList[0]['_id'],
				'package_id' => $deliveryInfo->ProductList[0]['package_id'],
				'item_name' => $deliveryInfo->ProductList[0]['product_name'],
				'log_type' => 'request',
				'message' => 'request has been cancel.',
				'status' => 'cancel_by_admin',
				'action_user_id' => Auth::user()->_id,
				'EnterOn' => new MongoDate(),
			];

			Activitylog::insert($insertactivity);

			$updation = Deliveryrequest::where(array('_id' => $request->get('request_id')))->where($where)
				->update(['ProductList.$.status' => 'cancel_by_admin']);

			//To check that all request have canceled
			if ($updation) {

				$count = Deliveryrequest::where(array('_id' => $request->get('request_id')))
					->where(array("ProductList" => array('$elemMatch' => array('status' => 'pending'))))->count();
				Deliveryrequest::where(array('_id' => $request->get('request_id')))
					->where($where)->update(['ProductList.$.reason' => $request->get('reason'), 'ProductList.$.RejectBy' => 'aquantuo']);
				Reqhelper::update_status($deliveryInfo->_id);
				$responce = ['success' => 1, 'msg' => 'Product rejected successfully.', 'count' => $count];
			}

			$user_data = User::where(array('_id' => $deliveryInfo->RequesterId))->first();
			if (count($user_data) > 0) {
				$Notification = new Notify();
				$Notification->setValue('title', 'Request canceled');
				$Notification->setValue('message', 'Your  request has been canceled.');
				$Notification->setValue('type', 'online_request_canceled');
				$Notification->setValue('location', 'online_request_canceled');
				$Notification->setValue('locationkey', $request->get('request_id'));
				$Notification->setValue('itemId', $request->get('product_id'));
				$Notification->add_user($user_data->NotificationId, $user_data->DeviceType);
				$Notification->fire();
				$insNotification = array(
					array(
						"NotificationTitle" => 'Request canceled',
						"NotificationShortMessage" => 'Your  request has been canceled.',
						"NotificationMessage" => 'Your  request has been canceled.',
						"NotificationType" => "online_request_canceled",
						"NotificationUserId" => array($user_data->_id),
						"Date" => new MongoDate(),
						"GroupTo" => "User",
					),
					array(
						"NotificationTitle" => 'Request accepted',
						"NotificationMessage" => 'Your  request has been accepted.',
						"NotificationUserId" => array(),
						"NotificationReadStatus" => 0,
						"location" => "request_detail",
						"locationkey" => (string) $request->get('request_id'),
						"Date" => new MongoDate(),
						"GroupTo" => "Admin",
					),
				);

				Notification::Insert($insNotification);

				//Mail
				foreach ($deliveryInfo->ProductList as $key) {
					$productName = $key['product_name'];
				}
				send_mail('57d7d8bc7ac6f69c158b456a', [
					"to" => $user_data->Email,
					"replace" => [
						"[USERNAME]" => $user_data->Name,
						"[PACKAGETITLE]" => $productName,
						"[PACKAGEID]" => $deliveryInfo->PackageId,
						"[SOURCE]" => $deliveryInfo->PickupAddress,
						"[DESTINATION]" => $deliveryInfo->DeliveryFullAddress,
						"[PACKAGENUMBER]" => $deliveryInfo->PackageNumber,
						"[SHIPPINGCOST]" => $deliveryInfo->shippingCost,
						"[TOTALCOST]" => $deliveryInfo->TotalCost,

					],
				]
				);

			}

		}

		return json_encode($responce);
	}

	public function pickup_request(Request $request) {
		$responce = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];

		$where = array("ProductList" => array('$elemMatch' => array('_id' => $request->get('product_id'))), "ProductList" => array('$elemMatch' => array('status' => 'accepted')));

		$deliveryInfo = Deliveryrequest::where(array('_id' => $request->get('request_id')))->where($where)->select('ProductList.$', 'RequesterId')->first();

		if (count($deliveryInfo) > 0) {

			$updation = Deliveryrequest::where(array('_id' => $request->get('request_id')))->where($where)
				->update(['ProductList.$.status' => 'pickedup']);

			if ($updation) {
				$responce = ['success' => 1, 'msg' => 'Product pickup successfully.'];
			}

			$user_data = User::where(array('_id' => $deliveryInfo->RequesterId))->first();
			$tp_data = User::where(array('_id' => $deliveryInfo->ProductList[0]->tpid))->first();

			if (count($user_data) > 0 && count($tp_data) > 0) {
				$Notification = new Notify();
				$Notification->setValue('title', 'Out for pickup');
				$Notification->setValue('message', 'Unless you shipped your item to the Transporter, please have it ready. The Transporter is out to pick up your package or to meet you at the agreed location.');
				$Notification->setValue('type', 'online_request_pickedup');
				$Notification->setValue('location', 'online_request_pickedup');
				$Notification->setValue('locationkey', $request->get('request_id'));
				$Notification->setValue('itemId', $request->get('product_id'));
				$Notification->add_user($user_data->NotificationId, $user_data->DeviceType);
				$Notification->fire();

				$insNotification = array(
					array(
						"NotificationTitle" => 'Out for pickup',
						"NotificationShortMessage" => 'Unless you shipped your item to the Transporter, please have it ready. The Transporter is out to pick up your package or to meet you at the agreed location.',
						"NotificationMessage" => 'Your request has been pickedup.',
						"NotificationType" => "online_request_pickedup",
						"NotificationUserId" => array($user_data->_id),
						"Date" => new MongoDate(),
						"GroupTo" => "User",
					),
					array(
						"NotificationTitle" => 'Transporter is out for pickup',
						"NotificationMessage" => sprintf('Transporter "%s" is out to pickup the package for delivery.', $tp_data->Name),
						"NotificationUserId" => array(),
						"NotificationReadStatus" => 0,
						"location" => "online_request_pickedup",
						"locationkey" => (string) $request->get('request_id'),
						"Date" => new MongoDate(),
						"GroupTo" => "Admin",
					),
				);

				Notification::Insert($insNotification);

				//Mail
				foreach ($deliveryInfo->ProductList as $key) {
					$productName = $key['product_name'];
				}

				send_mail('57d7d6757ac6f6b5158b4567', [
					"to" => $user_data->Email,
					"replace" => [
						"[USERNAME]" => $user_data->Name,
						"[PACKAGETITLE]" => $productName,

					],
				]);
			}
		}
		return json_encode($responce);

	}

	public function delivery_request(Request $request) {
		$responce = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];

		$where = array("ProductList" => array('$elemMatch' => array('_id' => $request->get('product_id'))), "ProductList" => array('$elemMatch' => array('status' => 'pickedup')));

		$deliveryInfo = Deliveryrequest::where(array('_id' => $request->get('request_id')))->where($where)->select('ProductList.$', 'RequesterId')->first();

		if (count($deliveryInfo) > 0) {
			$postArray = [];

			foreach ($deliveryInfo->ProductList as $key) {
				$key['status'] = ($key['status'] == 'pickedup') ? 'delivered' : $key['status'];
				$postArray[] = $key;
			}

			$updation = Deliveryrequest::where(array('_id' => $request->get('request_id')))->where($where)
				->update(['ProductList.$.status' => 'delivered']);
			if ($updation) {
				$responce = ['success' => 1, 'msg' => 'Product pickup successfully.'];
			}

			$user_data = User::where(array('_id' => $deliveryInfo->RequesterId))->first();

			if (count($user_data) > 0) {
				$Notification = new Notify();
				$Notification->setValue('title', 'Out for delivery');
				$Notification->setValue('message', 'Your request has been delivered successfully .');
				$Notification->setValue('type', 'online_request_delivered');
				$Notification->setValue('location', 'online_request_delivered');
				$Notification->setValue('locationkey', $request->get('request_id'));
				$Notification->setValue('itemId', $request->get('product_id'));
				$Notification->add_user($user_data->NotificationId, $user_data->DeviceType);
				$Notification->fire();

				$insNotification = array(
					array(
						"NotificationTitle" => 'Out for delivery',
						"NotificationShortMessage" => 'Your request has been delivered.',
						"NotificationMessage" => 'Your request has been delivered.',
						"NotificationType" => "online_request_delivered",
						"NotificationUserId" => array($user_data->_id),
						"Date" => new MongoDate(),
						"GroupTo" => "User",
					),
					array(
						"NotificationTitle" => 'Out for delivery',
						"NotificationMessage" => 'Your request has been delivered.',
						"NotificationUserId" => array(),
						"NotificationReadStatus" => 0,
						"location" => "online_request_delivered",
						"locationkey" => (string) $request->get('request_id'),
						"Date" => new MongoDate(),
						"GroupTo" => "Admin",
					),
				);

				Notification::Insert($insNotification);

				//Mail
				foreach ($deliveryInfo->ProductList as $key) {
					$productName = $key['product_name'];
				}

				send_mail('57d7d75f7ac6f6d9158b4568', [
					"to" => $user_data->Email,
					"replace" => [
						"[USERNAME]" => $user_data->Name,
						"[PACKAGETITLE]" => $productName,

					],
				]
				);

			}

		}

		return json_encode($responce);

	}

	public function complete_request(Request $request) {
		$responce = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];

		$where = array("ProductList" => array('$elemMatch' => array('_id' => $request->get('product_id'))), "ProductList" => array('$elemMatch' => array('status' => 'delivered')));

		$deliveryInfo = Deliveryrequest::where(array('_id' => $request->get('request_id')))->where($where)->select('ProductList.$', 'RequesterId')->first();

		if (count($deliveryInfo) > 0) {
			$postArray = [];

			foreach ($deliveryInfo->ProductList as $key) {
				$key['status'] = ($key['status'] == 'delivered') ? 'completed' : $key['status'];
				$postArray[] = $key;
			}

			$updation = Deliveryrequest::where(array('_id' => $request->get('request_id')))->where($where)
				->update(['ProductList.$.status' => 'completed', 'ProductList.$.completed_date_time' => new MongoDate()]);
			if ($updation) {
				$responce = ['success' => 1, 'msg' => 'Product completed  successfully.'];
			}

			$user_data = User::where(array('_id' => (String) $deliveryInfo->RequesterId))->first();

			if (count($user_data) > 0) {
				$Notification = new Notify();
				$Notification->setValue('title', 'Request complete');
				$Notification->setValue('message', 'Your request has been completed successfully .');
				$Notification->setValue('type', 'online_request_completed');
				$Notification->setValue('location', 'online_request_completed');
				$Notification->setValue('locationkey', $request->get('request_id'));
				$Notification->setValue('itemId', $request->get('product_id'));
				$Notification->add_user($user_data->NotificationId, $user_data->DeviceType);
				$Notification->fire();

				$insNotification = array(
					array(
						"NotificationTitle" => 'Request complete',
						"NotificationShortMessage" => 'Your request has been completed successfully .',
						"NotificationMessage" => 'Your request has been completed successfully .',
						"NotificationType" => "online_request_completed",
						"NotificationUserId" => array($user_data->_id),
						"Date" => new MongoDate(),
						"GroupTo" => "User",
					),
					array(
						"NotificationTitle" => 'Request complete',
						"NotificationMessage" => 'Your request has been completed successfully .',
						"NotificationUserId" => array(),
						"NotificationReadStatus" => 0,
						"location" => "online_request_completed",
						"locationkey" => (string) $request->get('request_id'),
						"Date" => new MongoDate(),
						"GroupTo" => "Admin",
					),
				);

				Notification::Insert($insNotification);

				//Mail
				foreach ($deliveryInfo->ProductList as $key) {
					$productName = $key['product_name'];
				}

				send_mail('57d7d8107ac6f6e0148b4567', [
					"to" => $user_data->Email,
					"replace" => [
						"[USERNAME]" => $user_data->Name,
						"[PACKAGETITLE]" => $productName,

					],
				]
				);

			}

		}

		return json_encode($responce);

	}
	public function buy_for_me() {
		if (!(Auth::user()->UserPermission & PACKAGE) == PACKAGE) {
			return Redirect::to('/admin/dashboard');
		}
		$data['category'] = Category::where('Status', 'Active')->get(array('Content'));
		$data['country'] = CityStateCountry::where(array('type' => 'Country'))->get(array('Content'));
		$data['title'] = "Buy for Me";
		$data['Request_type'] = 'buy_for_me';
		$data['paginationurl'] = "pagination/buy-for-me";
		$data['orderType'] = "Desc";
		$data['orderby'] = "UserId";
		$data['postvalue'] = "&status=" . Input::get('status') . "&StartDate=" . Input::get('StartDate') . "&EndDate=" . Input::get('EndDate') . "&ProductName=" . Input::get('ProductName') . "&type=" . Input::get('type')
		. "&city=" . Input::get('city') . "&state=" . Input::get('state') . "&country=" . Input::get('country') . "&category=" . Input::get('category') . "&Packageid=" . Input::get('Packageid') . "&address=" . Input::get('address') . "&search_value=" . Input::get('search_value');
		return view('Admin::list.online_package', $data);
	}
	public function online_package() {
		if (!(Auth::user()->UserPermission & PACKAGE) == PACKAGE) {
			return Redirect::to('/admin/dashboard');
		}
		$data['category'] = Category::where('Status', 'Active')->get(array('Content'));
		$data['country'] = CityStateCountry::where(array('type' => 'Country'))->get(array('Content'));
		$data['title'] = "Online Package Management";
		$data['Request_type'] = 'online';
		$data['paginationurl'] = "pagination/online-package";
		$data['orderType'] = "Desc";
		$data['orderby'] = "UserId";
		$data['postvalue'] = "&status=" . Input::get('status') . "&StartDate=" . Input::get('StartDate') . "&EndDate=" . Input::get('EndDate') . "&ProductName=" . Input::get('ProductName') . "&type=" . Input::get('type')
		. "&city=" . Input::get('city') . "&state=" . Input::get('state') . "&country=" . Input::get('country') . "&category=" . Input::get('category') . "&Packageid=" . Input::get('Packageid') . "&address=" . Input::get('address') . "&search_value=" . Input::get('search_value');

		return view('Admin::list.online_package', $data);
	}

	public function online_payment_reminder() {
		$responce = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		$valid = Validator::make(Input::get(), [
			'requestid' => 'required',
		]);
		if (!$valid->fails()) {
			$delinfo = Deliveryrequest::where('_id', Input::get('requestid'))->first();
			if (count($delinfo) > 0) {
				$user_data = User::where(array('_id' => $delinfo->RequesterId))->first();

				if (count($user_data) > 0) {
					//Mail
					$link = url("online-request-detail/{$delinfo->_id}");
					// send_mail('5886f3986befd91046cf465d', [
					// 	"to" => $user_data->Email,
					// 	"replace" => [
					// 		"[USERNAME]" => $user_data->Name,
					// 		"[PACKAGETITLE]" => $delinfo->ProductTitle,
					// 		"[PACKAGEID]" => @$delinfo->PackageNumber,
					// 		"[URL]" => "<a href='{$link}' >{$link}</a>",
					// 	],
					// ]);

					$cron_mail = [
						"USERNAME" => ucfirst($user_data->Name),
						"PACKAGETITLE" => ucfirst($delinfo->ProductTitle),
						"PACKAGEID" => @$delinfo->PackageNumber,
						"URL" => "<a href='{$link}' >{$link}</a>",
						'email_id' => '5886f3986befd91046cf465d',
						'email' => $user_data->Email,
						'status' => 'ready',

					];

					SendMail::insert($cron_mail);

					$Notification = new Notify();
					$Notification->setValue('title', 'Pending payment');
					$Notification->setValue('message', "Additional payment is required on your item prior to purchase.");
					$Notification->setValue('type', 'requester_online_detail');
					$Notification->setValue('location', 'requester_online_detail');
					$Notification->setValue('locationkey', Input::get('requestid'));
					$Notification->setValue('itemid', Input::get('requestid'));
					$Notification->add_user($user_data->NotificationId, $user_data->DeviceType);
					$Notification->fire();

					Notification::Insert([
						"NotificationTitle" => 'Pending payment',
						"NotificationShortMessage" => "Additional payment is required on your item prior to purchase.",
						"NotificationMessage" => "Additional payment is required on your item prior to purchase.",
						"NotificationType" => 'online_request_reminder',
						"NotificationUserId" => array($user_data->_id),
						"Date" => new MongoDate(),
						"GroupTo" => "User",
					]);

					return json_encode(['success' => 1, 'msg' => "Reminder has been sent successfully."]);
				}
			}
		}
		return json_encode($responce);
	}

	public function buy_for_me_process() {
		
		$responce = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		$valid = Validator::make(Input::get(), [
			'type' => 'required',
			'requestid' => 'required',
			'itemid' => 'required',
		]);

		if (!$valid->fails()) {
			// Start reminder email
			if (Input::get('type') == 'payment_reminder') {
				$delinfo = Deliveryrequest::where('_id', Input::get('requestid'))->first();

				if (count($delinfo) > 0) {
					$user_data = User::where(array('_id' => $delinfo->RequesterId))->first();

					if (count($user_data) > 0) {
						// Mail
						$link = url("buy-for-me-detail/{$delinfo->_id}");
						/*send_mail('5886f3986befd91046cf465d', [
								"to" => $user_data->Email,
								"replace" => [
									"[USERNAME]" => $user_data->Name,
									"[PACKAGETITLE]" => $delinfo->ProductTitle,
									"[PACKAGEID]" => @$delinfo->PackageNumber,
									"[URL]" => "<a href='{$link}' >{$link}</a>",
								],
							]);
						*/
						$cron_mail = [
							"USERNAME" => ucfirst($user_data->Name),
							"PACKAGETITLE" => ucfirst($delinfo->ProductTitle),
							"PACKAGEID" => @$delinfo->PackageNumber,
							'URL' => "<a href='{$link}' >{$link}</a>",
							'email_id' => '5886f3986befd91046cf465d',
							'email' => $user_data->Email,
							'status' => 'ready',
						];

						SendMail::insert($cron_mail);

						$Notification = new Notify();
						$Notification->setValue('title', trans('lang.ITEM_REMINDER_TITLE'));
						$Notification->setValue('message', trans('lang.ITEM_REMINDER_MSG'));
						$Notification->setValue('type', 'buy_for_me_request_accepted');
						$Notification->setValue('location', 'buy_for_me_request_accepted');
						$Notification->setValue('locationkey', Input::get('requestid'));
						$Notification->setValue('itemId', Input::get('requestid'));
						$Notification->add_user($user_data->NotificationId, $user_data->DeviceType);
						$Notification->fire();

						Notification::Insert([
							"NotificationTitle" => trans('lang.ITEM_REMINDER_TITLE'),
							"NotificationShortMessage" => trans('lang.ITEM_REMINDER_MSG'),
							"NotificationMessage" => trans('lang.ITEM_REMINDER_MSG'),
							"NotificationType" => 'buy_for_me',
							"NotificationUserId" => array(new MongoId($user_data->_id)),
							"Date" => new MongoDate(),
							"GroupTo" => "User",
						]);
						return json_encode(['success' => 1, 'msg' => "Reminder has been sent successfully."]);
					}

				}
			}
			// End reminder email

			// Prepare where condition
			$where = [
				"ProductList" => ['$elemMatch' => ['_id' => Input::get('itemid')]],
				'_id' => Input::get('requestid'),
			];
			if (trim(Input::get('type')) == 'accept') {
				$where['ProductList']['$elemMatch']['status'] = 'ready';
			} elseif (trim(Input::get('type')) == 'cancel') {
				$where['ProductList']['$elemMatch']['status'] = ['$in' => ['ready', 'accepted', 'reviewed', 'paid']];
			} elseif (trim(Input::get('type')) == 'purchased') {
				$where['ProductList']['$elemMatch']['status'] = ['$in' => ['paid']];
			} elseif (trim(Input::get('type')) == 'item_received') {
				$where['ProductList']['$elemMatch']['status'] = ['$in' => ['purchased']];
			} else if (trim(Input::get("type")) == "out_for_pickup") {
				$where['ProductList']['$elemMatch']['status'] = ['$in' => ['assign']];
			} else if (trim(Input::get("type")) == "out_for_delivery") {
				$where['ProductList']['$elemMatch']['status'] = ['$in' => ['out_for_pickup']];
			} else if (trim(Input::get("type")) == "delivered") {
				$where['ProductList']['$elemMatch']['status'] = ['$in' => ['out_for_delivery']];
			}
			//item_received
			$delinfo = Deliveryrequest::where($where)->first();

			if (count($delinfo) > 0) {
				// Find product detail on which we want to work
				$productlist = $delinfo['ProductList'];
				$product_detail = [];
				$package_id = '';
				$item_name = '';

				foreach ($productlist as $key => $val) {

					if ($val['_id'] == trim(Input::get('itemid'))) {

						$package_id = $val['package_id'];
						$item_name = $val['product_name'];

						if (Input::get('type') == 'accepted') {
							$productlist[$key]['status'] = 'accepted';
						} else if (Input::get('type') == 'cancel') {
							$productlist[$key]['status'] = 'cancel_by_admin';
						} else if (Input::get('type') == 'purchased') {
							$productlist[$key]['status'] = 'purchased';
						} else if (Input::get('type') == 'item_received') {
							$productlist[$key]['status'] = 'item_received';

							if ($productlist[$key]['travelMode'] == 'air') {

								$startdate = strtotime(date("Y-m-d"));
								$endDate = strtotime("+13 day", $startdate);
								$Buinessdays = new Buinessdays();
								$result = $Buinessdays->getWorkingDays($startdate, $endDate);
								$daycount = 13 - $result;
								$finalCount = 13 + $daycount;

								$productlist[$key]['ExpectedDate'] = new Mongodate(strtotime('+' . $finalCount . ''));
							} else {
								$productlist[$key]['ExpectedDate'] = new Mongodate(strtotime('+ 65 days'));
							}
						} else if (Input::get('type') == 'out_for_pickup') {
							$productlist[$key]['status'] = 'out_for_pickup';
						} else if (Input::get('type') == 'out_for_delivery') {
							$productlist[$key]['status'] = 'out_for_delivery';
						} else if (Input::get('type') == 'delivered') {
							$productlist[$key]['status'] = 'delivered';
						}
						$product_detail = $productlist[$key];
					}
				}

				if (count($product_detail) > 0) {

					if (trim(Input::get('type')) == 'accept') {
						$updatedata = ['ProductList.$.status' => 'accepted'];
					} elseif (trim(Input::get('type')) == 'cancel') {
						$updatedata = ['ProductList.$.status' => 'cancel_by_admin',
							'ProductList.$.cancel_date' => new MongoDate(),
						];
					} elseif (trim(Input::get('type')) == 'purchased') {

						if ($delinfo->PaymentStatus != 'paid'  && $delinfo->PaymentStatus != 'capture') {

							$res = RSStripe::retrive_amount($delinfo->StripeChargeId2);
							$res = RSStripe::retrive_amount($delinfo->StripeChargeId);
							if (!isset($res['id'])) {
								$res2 = RSStripe::charge_detail($delinfo->StripeChargeId);
								if (@$res2['captured'] == 1) {
									$res = $res2;
								}
							}
							if (!isset($res['id'])) {
								$responce['msg'] = $res;
								return json_encode($responce);
								die;
							} else {
								Transaction::insert([
									"SendById" => $delinfo->RequesterId,
									"SendByName" => $delinfo->RequesterName,
									"SendToId" => "aquantuo",
									"RecieveByName" => "Aquantuo",
									"Description" => sprintf("Amount deposited against delivery request for %s, PackageId: %u to %s", $delinfo->ProductTitle, $delinfo->PackageNumber, $delinfo->DeliveryFullAddress
									),
									"Credit" => $delinfo->TotalCost,
									"Debit" => "",
									"Status" => "paid",
									"TransactionType" => "buy_for_me",
									"EnterOn" => new MongoDate(),
								]);

								$delinfo->PaymentStatus = 'paid';
								$delinfo->save();
							}

						}

						$updatedata = [
							'ProductList.$.status' => 'purchased',
							'ProductList.$.purchased_item_date' => new MongoDate(),
						];

						/* Activity Log */
						$insertactivity = [
							'request_id' => Input::get('requestid'),
							'request_type' => $delinfo->RequestType,
							'PackageNumber' => $delinfo->PackageNumber,
							'item_id' => Input::get('itemid'),
							//'package_id' => $delinfo->ProductList[0]['package_id'],
							//'item_name' => $delinfo->ProductList[0]['product_name'],
							'package_id' => $package_id,
							'item_name' => $item_name,
							'log_type' => 'request',
							'message' => 'Your Item has been purchased',
							'status' => 'purchased',
							'action_user_id'=>Auth::user()->_id,
							'EnterOn' => new MongoDate(),
						];
						Activitylog::insert($insertactivity);

					} elseif (trim(Input::get('type')) == 'item_received') {

						if ($productlist[$key]['travelMode'] == 'air') {

							$startdate = strtotime(date("Y-m-d"));

							$Buinessdays = new Buinessdays();
							$result = $Buinessdays->getWorkingDays($startdate);

							$ExpectedDate = new Mongodate(strtotime('+' . $result . 'days'));
						} else {

							$ExpectedDate = new Mongodate(strtotime('+ 65 days'));
						}

						//print_r(expression)

						$updatedata = [
							'ProductList.$.status' => 'item_received',
							'ProductList.$.ExpectedDate' => $ExpectedDate,
							'ProductList.$.item_received_date' => new MongoDate(),
						];

						//print_r($updatedata); die;

						/* Activity Log */
						$insertactivity = [
							'request_id' => Input::get('requestid'),
							'request_type' => $delinfo->RequestType,
							'PackageNumber' => $delinfo->PackageNumber,
							'item_id' => Input::get('itemid'),
							//'package_id' => $delinfo->ProductList[0]['package_id'],
							//'item_name' => $delinfo->ProductList[0]['product_name'],
							'package_id' => $package_id,
							'item_name' => $item_name,
							'log_type' => 'request',
							'message' => 'Your item has been received.',
							'status' => 'item_received',
							'action_user_id'=>Auth::user()->_id,
							'EnterOn' => new MongoDate(),
						];
						Activitylog::insert($insertactivity);

					}elseif (trim(Input::get('type')) == 'out_for_pickup') {
						
						/* Activity Log */
						$insertactivity = [
							'request_id' => Input::get('requestid'),
							'request_type' => $delinfo->RequestType,
							'PackageNumber' => $delinfo->PackageNumber,
							'item_id' => Input::get('itemid'),
							//'package_id' => $delinfo->ProductList[0]['package_id'],
							//'item_name' => $delinfo->ProductList[0]['product_name'],
							'package_id' => $package_id,
							'item_name' => $item_name,
							'log_type' => 'request',
							'message' => 'Item is in destination country going through customs and sorting.',
							'status' => 'out_for_pickup',
							'action_user_id'=>Auth::user()->_id,
							'EnterOn' => new MongoDate(),
						];
						Activitylog::insert($insertactivity);
						
						$updatedata = [
							'ProductList.$.status' => 'out_for_pickup',
						];
					} else if (trim(Input::get('type')) == "out_for_delivery") {
						/* Activity Log */
						$insertactivity = [
							'request_id' => Input::get('requestid'),
							'request_type' => $delinfo->RequestType,
							'PackageNumber' => $delinfo->PackageNumber,
							'item_id' => Input::get('itemid'),
							//'package_id' => $delinfo->ProductList[0]['package_id'],
							//'item_name' => $delinfo->ProductList[0]['product_name'],
							'package_id' => $package_id,
							'item_name' => $item_name,
							'log_type' => 'request',
							'message' => 'Your package is en route to be delivered.',
							'status' => 'out_for_delivery',
							'action_user_id'=>Auth::user()->_id,
							'EnterOn' => new MongoDate(),
						];
						Activitylog::insert($insertactivity);
						
						$updatedata = [
							'ProductList.$.status' => 'out_for_delivery',
						];
					}  else if (trim(Input::get('type')) == "delivered") {
						/* Activity Log */
						$insertactivity = [
							'request_id' => Input::get('requestid'),
							'request_type' => $delinfo->RequestType,
							'PackageNumber' => $delinfo->PackageNumber,
							'item_id' => Input::get('itemid'),
							//'package_id' => $delinfo->ProductList[0]['package_id'],
							//'item_name' => $delinfo->ProductList[0]['product_name'],
							'package_id' => $package_id,
							'item_name' => $item_name,
							'log_type' => 'request',
							'message' => 'Package has been successfully delivered.',
							'status' => 'delivered',
							'action_user_id'=>Auth::user()->_id,
							'EnterOn' => new MongoDate(),
						];
						Activitylog::insert($insertactivity);
						
						$updatedata = [
							'ProductList.$.status' => 'delivered',
						];
					}
					$updatedata['UpdateOn'] = new MongoDate();
					$res = Deliveryrequest::where($where)->update($updatedata);

					if ($res) {
						Reqhelper::update_status($delinfo->_id);

						if (trim(Input::get('type')) == 'accept') {
							$responce = ['success' => 1, 'msg' => 'Success! Request has been accepted successfully.'];
							$message = 'Aquantuo has been accepted your request "' . $product_detail['product_name'] . '".';
							$title = 'Request accepted';
							$type = 'buy_for_me_request_accepted';
							$email_id = '562226c0e4b0252ad07d079f';
						} elseif (trim(Input::get('type')) == 'cancel') {
							$responce = ['success' => 1, 'msg' => 'Success! Request has been canceled successfully.'];
							$message = 'Your request has been canceled.';
							$title = 'Request canceled';
							$type = 'buy_for_me_request_canceled';
							$email_id = '56224b45e4b0d4bc235582c0';
						} elseif (trim(Input::get('type')) == 'purchased') {
							$responce = ['success' => 1, 'msg' => 'Success! Request has been purchased successfully.'];
							$message = sprintf(trans('lang.ITEM_PURCHASED_MSG'), $product_detail['product_name']);
							$title = trans('lang.ITEM_PURCHASED_TITLE');
							$type = 'buy_for_me_request_purchased';
							$email_id = '58a1ba586befd910a15c1c2e';
						} elseif (trim(Input::get('type')) == 'item_received') {
							$responce = ['success' => 1, 'msg' => 'Success! Item has been received successfully.'];
							$message = sprintf(trans('lang.ITEM_RECEIVED_MSG'), $product_detail['product_name']);
							$title = trans('lang.ITEM_RECEIVED');
							$type = 'BUY_FOR_ME_DELIVERY_DETAIL_FOR_REQUESTER';
							$email_id = '594cadb06befd91cd70addb6';
						} elseif (trim(Input::get('type')) == 'out_for_pickup') {
							$responce = ['success' => 1, 'msg' => 'Success! Item is in destination country going through customs and sorting.'];
							$message = sprintf(trans('lang.BUY_OUTFORPICKUP_MSG'), $product_detail['product_name']);
							$title = trans('lang.BUY_OUTFORPICKUP_TITLE');
							$type = 'BUY_FOR_ME_DELIVERY_DETAIL_FOR_REQUESTER';
							$email_id = '57d7d6757ac6f6b5158b4567';
						} else if (trim(Input::get('type')) == "out_for_delivery") {
							$responce = ['success' => 1, 'msg' => 'Success! Your package is en route to be delivered.'];
							$message = sprintf(trans('lang.BUY_OUTFORDELIVERY_MSG'), $product_detail['product_name'], $product_detail['_id']);
							$title = trans('lang.BUY_OUTFORDELIVERY_TITLE');
							$type = 'BUY_FOR_ME_DELIVERY_DETAIL_FOR_REQUESTER';
							$email_id = '587863927ac6f6130c8b4568';
						} else if (trim(Input::get('type')) == "delivered") {
							$responce = ['success' => 1, 'msg' => 'Success! Package has been successfully delivered.'];
							$message = sprintf(trans('lang.MSG_REQUEST_DELIVERED'), "Aquantuo", $product_detail['product_name']);
							$title = trans('lang.REQUEST_DELIVERED');
							$type = 'BUY_FOR_ME_DELIVERY_DETAIL_FOR_REQUESTER';
							$email_id = '5e15add2982e1e19fbb7c29b';
						}

						$user_data = User::where(array('_id' => $delinfo->RequesterId))->first();

						if (count($user_data) > 0) {

							/* $Notification = new Notify();
								                            $Notification->setValue('title', $title);
								                            $Notification->setValue('message', $message);
								                            $Notification->setValue('type', $type);
								                            $Notification->setValue('location', $type);
								                            $Notification->setValue('itemId', Input::get('itemid'));
								                            $Notification->setValue('locationkey', Input::get('requestid'));
								                            $Notification->add_user($user_data->NotificationId, $user_data->DeviceType);
							*/

							$cron_noti = [
								"title" => $title,
								"message" => $message,
								"type" => $type,
								"locationkey" => Input::get('requestid'),
								'location' => $type,
								"NotificationId" => $user_data->NotificationId,
								"DeviceType" => $user_data->DeviceType,
								"itemId" => Input::get('itemid'),
								'ITEM_PRICE' => @$product_detail['price'],
								'SHIPPINGCOST2' => @$product_detail['shipping_cost_by_user'],
								'status' => 'ready',
								'by_mean' => 'notification',
							];
							SendMail::insert($cron_noti);

							$insNotification = array(
								array(
									"NotificationTitle" => $title,
									"NotificationShortMessage" => $message,
									"NotificationMessage" => $message,
									"NotificationType" => $type,
									"NotificationUserId" => array(new MongoId($user_data->_id)),
									"Date" => new MongoDate(),
									"GroupTo" => "User",
								),
							);

							Notification::Insert($insNotification);

							//Mail

							/*send_mail($email_id, [
								                            "to" => $user_data->Email,
								                            "replace" => [
								                            "[USERNAME]" => $user_data->Name,
								                            "[PACKAGETITLE]" => $product_detail['product_name'],
								                            "[ITEM_PRICE]" => $product_detail['price'],
								                            "[SHIPPINGCOST]" => $product_detail['shippingCost'],
								                            "[PACKAGEID]" => $delinfo->PackageId,
								                            "[SOURCE]" => $delinfo->PickupAddress,
								                            "[DESTINATION]" => $delinfo->DeliveryFullAddress,
								                            "[PACKAGENUMBER]" => $delinfo->PackageNumber,
								                            "[TOTALCOST]" => $delinfo->TotalCost,
								                            "[ACCEPT_BY]" => "Aquantuo",
								                            ],
								                            ]
							*/
							// $Email = new NewEmail();
							if (trim(Input::get('type')) == 'item_received') {	

								$where2 = [
									"ProductList" => ['$elemMatch' => ['_id' => Input::get('itemid')]],
									'_id' => Input::get('requestid'),
								];
								
								$req_info = Deliveryrequest::where($where2)->select('ProductList.$')->first();
								$show_date = '';
								if(isset($req_info->ProductList[0]['ExpectedDate']->sec)){
									$show_date = show_date(@$req_info->ProductList[0]['ExpectedDate']);
								}

								send_mail('594cadb06befd91cd70addb6', [
									"to" => $user_data->Email,
									"replace" => [
										"[USERNAME]" => $user_data->Name,
										"[PACKAGETITLE]" => $product_detail['product_name'],
										"[DATE]" => $show_date
									],
								]);
							} else {
								send_mail($email_id, [
									"to" => $user_data->Email,
									"replace" => [
										"[USERNAME]" => $user_data->Name,
										"[PACKAGETITLE]" => $product_detail['product_name'],
										"[SHIPPINGCOST]" => $product_detail['shippingCost'],
										"[PACKAGEID]" => $product_detail["package_id"],
										"[SOURCE]" => $delinfo->PickupAddress,
										"[DESTINATION]" => $delinfo->DeliveryFullAddress,
										"[PACKAGENUMBER]" => $delinfo->PackageNumber,
										"[TOTALCOST]" => $delinfo->TotalCost,
										"[ACCEPT_BY]" => "Aquantuo",
										'[ITEM_PRICE]' => @$product_detail['price'],
										'[QUANTITY]' => @$product_detail['qty'],
										'[TOTAL_ITEM_PRICE]'=> @$product_detail['price'] * (int) @$product_detail['qty'], 
										'[SHIPPINGCOST2]' => @$product_detail['shipping_cost_by_user']
									],
								]);

								$cron_mail2 = [
									"USERNAME" => $user_data->Name,
									"PACKAGETITLE" => $product_detail['product_name'],
									"ITEM_PRICE" => $product_detail['price'],
									"SHIPPINGCOST" => $product_detail['shippingCost'],
									"PACKAGEID" => $product_detail["package_id"],
									"SOURCE" => $delinfo->PickupAddress,
									"DESTINATION" => $delinfo->DeliveryFullAddress,
									"PACKAGENUMBER" => $delinfo->PackageNumber,
									"TOTALCOST" => $delinfo->TotalCost,
									"ACCEPT_BY" => "Aquantuo",
									'email_id' => $email_id,
									'email' => $user_data->Email,
									'status' => 'ready'
								];

								SendMail::insert($cron_mail2);
							}
						}
					}
				}
			}
		}
		return json_encode($responce);
	}

	public function get_item_info() {
		$responce = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		$valid = Validator::make(Input::get(), [
			'requestid' => 'required', 'itemid' => 'required',
		]);

		if (!$valid->fails()) {
			// Prepare where condition
			$where = [
				"ProductList" => ['$elemMatch' => ['_id' => Input::get('itemid')]],
				'_id' => Input::get('requestid'),
			];

			$delinfo = Deliveryrequest::where($where)->select('ProductList.$')->first();
			if (count($delinfo) > 0) {
				$responce = ['success' => 1, 'msg' => 'Item detail.', 'ProductList' => $delinfo->ProductList[0]];

			}

		}
		return json_encode($responce);
	}

	public function getItemInfo() {
		$responce = ['success' => 0, 'msg' => 'Item information has been not found.'];
		$valid = Validator::make(Input::get(), [
			'requestid' => 'required', 'itemid' => 'required',
		]);

		if (!$valid->fails()) {

			$delinfo = Itemhistory::where('item_id', '=', Input::get('itemid'))->first();

			if (count($delinfo) > 0) {
				$responce = ['success' => 1,
					'msg' => 'Item detail.',
					'ProductList' => $delinfo,
				];

			}

		}
		return json_encode($responce);
	}

	public function get_online_item_info() {
		$responce = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		$valid = Validator::make(Input::get(), [
			'requestid' => 'required', 'itemid' => 'required',
		]);

		if (!$valid->fails()) {
			// Prepare where condition
			$where = [
				"ProductList" => ['$elemMatch' => ['_id' => Input::get('itemid')]],
				'_id' => Input::get('requestid'),
			];

			$delinfo = Deliveryrequest::where($where)->select('ProductList.$')->first();

			if (count($delinfo) > 0) {
				$responce = ['success' => 1, 'msg' => 'Item detail.', 'ProductList' => $delinfo->ProductList[0]];
			}

		}
		return json_encode($responce);
	}

	public function update_item_info(Request $request) {

		$responce = [
			'success' => 0,
			'msg' => 'Oops! Something went wrong.',
			'reminder_button' => false,
			'assing_tp_button' => false,
		];
		$msg = '';

		$valid = Validator::make(Input::get(), [
			'requestid' => 'required', 'itemid' => 'required',
			'item_price' => 'required',
		]);

		if (!$valid->fails()) {

			$where = [
				"ProductList" => ['$elemMatch' => [
					'_id' => $request->get('itemid'),
				]],
			];
			$deliveryInfo2 = Deliveryrequest::where(array('_id' => Input::get('requestid')))
				->where($where)->select('ProductList.$')->first();

			$where = ['_id' => Input::get('requestid')];
			$delinfo = Deliveryrequest::where($where)
				->select('ProductList', 'RequesterId', 'consolidate_item', 'discount_in_percent', 'ProductTitle', 'distance', 'RequestType', 'PackageNumber')->first();

			if (count($delinfo) > 0) {

				$userInfo = User::where(array('_id' => $delinfo->RequesterId))->first();
				if (count($userInfo) <= 0) {
					return ['success' => 0, 'msg' => 'Oops! Issue with user'];
				} else {

					$productList = [];
					foreach ($delinfo->ProductList as $productKey) {

						if ($productKey['_id'] == trim(Input::get('itemid'))) {
							$metric_unit = 'cm';
							$imperial_unit = 'kg';
							if (Input::get('measurement_units') == 'inches_lbs') {
								$metric_unit = 'inches';
								$imperial_unit = 'lbs';
							}

							$result = Deliveryrequest::where('_id', '=', Input::get('requestid'))->select('_id', 'ProductList')->first();

							$insertData = [
								'request_id' => $result['_id'],
								'item_id' => $productKey['_id'],
								'package_id' => $productKey['package_id'],
								'tpid' => $productKey['tpid'],
								'tpName' => $productKey['tpName'],
								'marketid' => $productKey['marketid'],
								'marketname' => $productKey['marketname'],
								'txnid' => $productKey['txnid'],
								'product_name' => $productKey['product_name'],
								'url' => $productKey['url'],
								'price' => $productKey['price'],
								'qty' => $productKey['qty'],
								'description' => $productKey['description'],
								'travelMode' => $productKey['travelMode'],
								'shipping_cost_by_user' => $productKey['shipping_cost_by_user'],
								'weight' => $productKey['weight'],
								'weight_unit' => $productKey['weight_unit'],
								'height' => $productKey['height'],
								'heightUnit' => $productKey['heightUnit'],
								'length' => $productKey['length'],
								'lengthUnit' => $productKey['lengthUnit'],
								'width' => $productKey['width'],
								'widthUnit' => $productKey['widthUnit'],
								'category' => $productKey['category'],
								'categoryid' => $productKey['categoryid'],
								'insurance_status' => $productKey['insurance_status'],
								'TransporterFeedbcak' => $productKey['TransporterFeedbcak'],
								'TransporterRating' => $productKey['TransporterRating'],
								'RequesterRating' => $productKey['RequesterRating'],
								'RejectBy' => $productKey['RejectBy'],
								'ReturnType' => $productKey['ReturnType'],
								'ReceiptImage' => $productKey['ReceiptImage'],
								'RejectTime' => $productKey['RejectTime'],
								'TrackingNumber' => $productKey['TrackingNumber'],
								'TransporterMessage' => $productKey['TransporterMessage'],
								'CancelDate' => $productKey['CancelDate'],
								'StripeChargeId' => $productKey['StripeChargeId'],
								'DeliveredDate' => $productKey['DeliveredDate'],
								'discount' => $productKey['discount'],
								'insurance' => $productKey['insurance'],
								'shippingCost' => $productKey['shippingCost'],
								'aq_fee' => $productKey['aq_fee'],
								'status' => $productKey['status'],
								'PaymentStatus' => $productKey['PaymentStatus'],
								'verify_code' => $productKey['verify_code'],
								'updated_at' => new MongoDate(),
							];

							Itemhistory::Insert($insertData);

							$productKey["price"] = floatval(Input::get('item_price'));
							$productKey["product_name"] = Input::get('item_name');
							$productKey["length"] = Input::get('item_length');
							$productKey["lengthUnit"] = $metric_unit;
							$productKey["width"] = Input::get('item_width');
							$productKey["widthUnit"] = $metric_unit;
							$productKey["height"] = Input::get('item_height');
							$productKey["heightUnit"] = $metric_unit;
							$productKey["weight"] = Input::get('item_weight');
							$productKey["weight_unit"] = $imperial_unit;
							$productKey["marketname"] = Input::get('purchase_from');
							$productKey["status"] = "not_purchased";
							$productKey["shipping_cost_by_user"] = floatval(Input::get('shipping_cost_by_user'));
							$productKey["qty"] = Input::get('qty1');

						}
						$productList[] = $productKey;
					}

					$Notification = new Notify();
					$calculation = new Calculation();

					$data = [
						'shippingcost' => 0,
						'insurance' => 0,
						'ghana_total_amount' => 0,
						'total_amount' => 0,
						'error' => [],
						'formated_text' => '',
						'distance_in_mile' => 0,
						'product_count' => 0,
						'ProcessingFees' => 0,
						'item_cost' => 0,
						'shipping_cost_by_user' => 0,
						'type' => 'buyforme',
						'userType' => 'admin',
						'distance' => (object) ['distance' => $delinfo->distance],
					];

					$info = $calculation->getRate($productList, $data, $userInfo);

					$productList = $info['data']['product'];

					foreach ($productList as $productKey => $productVal) {
						$productList[$productKey]['aq_fee'] = Reqhelper::get_aquantuo_fees($productVal['shippingCost']);
					}

					$res = Deliveryrequest::where($where)->update([
						'ProductList' => $productList,
						'shippingCost' => $info['shipping_cost'],
						'TotalCost' => $info['total_amount'] - $delinfo->discount,
						'shipping_cost_by_user' => $info['data']['shipping_cost_by_user'],
						'GhanaTotalCost' => $info['ghana_total_amount'],
					]);

					if ($res) {
						$res = Reqhelper::update_status(Input::get('requestid'));
						$responce = [
							'success' => 1,
							'msg' => 'Item has been updated successfully.',
							'current' => $res,
						];

						if ($res == 'not_purchased') {

							$delinfo_all = Deliveryrequest::where($where)->select('ProductList', 'BeforePurchaseTotalCost', 'ProcessingFees', 'discount', 'TotalCost', 'ProductTitle')->first();
							$TotalCost = $insurance = $shippingCost = 0;
							$ProductList = [];
							if (count($delinfo_all) > 0) {
								$ProductList = $delinfo_all->ProductList;
								foreach ($delinfo_all->ProductList as $key) {
									$insurance = $insurance + $key['insurance'];
									$shippingCost = $shippingCost + $key['shippingCost'];
									$TotalCost += ($key['shipping_cost_by_user'] + $key['shippingCost'] + $key['insurance'] + ($key['price'] * $key['qty']));
								}

								if ($TotalCost > $delinfo_all->discount) {
									$TotalCost = $TotalCost - $delinfo_all->discount;
								}
								$TotalCost += $delinfo_all->ProcessingFees;
								$delinfo_all->TotalCost = $TotalCost;
								$delinfo_all->insurance = $insurance;
								$delinfo_all->shippingCost = $shippingCost;
								$delinfo_all->AquantuoFees = $this->get_aquantuo_fees($shippingCost);

								if (($TotalCost - 0.50) > $delinfo_all->BeforePurchaseTotalCost) {

									if (count($delinfo_all->ProductList) > 1) {
										$msg = trans('lang.BUY_UPDATE_MSG');
										$title = trans('lang.BUY_UPDATE_TITLE');

									} else {
										$msg = sprintf(trans('lang.ITEM_PURCHASED'), $delinfo_all->ProductTitle);
										$title = trans('lang.ITEM_PURCHASED_TITLE');
									}
									$Notification->setValue('message', $msg);

									if (count($userInfo) > 0) {

										Notification::insert([
											"NotificationTitle" => $title,
											"NotificationMessage" => $msg,
											"NotificationType" => "buy_for_me",
											"NotificationUserId" => array(new MongoId($userInfo->_id)),
											"Date" => new MongoDate(),
											"GroupTo" => "User",
										]);

										$Notification->setValue('title', trans('lang.ITEM_PURCHASED_TITLE'));
										$Notification->setValue('type', 'requester_buy_for_me_detail');
										$Notification->setValue('location', 'requester_buy_for_me_detail');
										$Notification->setValue('locationkey', Input::get('requestid'));

										$Notification->add_user($userInfo->NotificationId, $userInfo->DeviceType);
										//$Notification->fire();

										$link = url("buy-for-me-detail/{$delinfo->_id}");
									}
									/*  Activity Log */
									$insertactivity = [
										'request_id' => Input::get('requestid'),
										'request_type' => $delinfo->RequestType,
										'PackageNumber' => $delinfo->PackageNumber,
										'item_id' => Input::get('itemid'),
										'package_id' => $deliveryInfo2->ProductList[0]['package_id'],
										'item_name' => $deliveryInfo2->ProductList[0]['product_name'],
										'log_type' => 'request',
										'message' => 'Product has been updated.',
										'status' => 'not_purchased',
										'EnterOn' => new MongoDate(),

									];

								} else {
									$delinfo_all->Status = 'paid';
									foreach ($delinfo_all->ProductList as $key => $item) {
										$ProductList[$key]['status'] = 'paid';
									}
									/*  Activity Log */
									$insertactivity = [
										'request_id' => Input::get('requestid'),
										'request_type' => $delinfo->RequestType,
										'PackageNumber' => $delinfo->PackageNumber,
										'item_id' => Input::get('itemid'),
										'package_id' => $deliveryInfo2->ProductList[0]['package_id'],
										'item_name' => $deliveryInfo2->ProductList[0]['product_name'],
										'log_type' => 'request',
										'message' => 'Product has been updated.',
										'status' => 'paid',
										'action_user_id' => Auth::user()->_id,
										'EnterOn' => new MongoDate(),

									];

									Activitylog::insert($insertactivity);

								}

								$delinfo_all->ProductList = $ProductList;
								$delinfo_all->save();
							}

						}

					}

				}

			}
		}

		return json_encode($responce);
	}

	public function update_online_item_info(Request $request) {
		$responce = [
			'success' => 0,
			'msg' => 'Oops! Something went wrong.',
			'reminder_button' => false,
			'assing_tp_button' => false,
		];
		$msg = '';
		$valid = Validator::make(Input::get(), [
			'requestid' => 'required', 'itemid' => 'required',
			'item_price' => 'required',
		]);

		if (!$valid->fails()) {

			$where = [
				"ProductList" => ['$elemMatch' => [
					'_id' => $request->get('itemid'),
				]],
			];
			$deliveryInfo2 = Deliveryrequest::where(array('_id' => Input::get('requestid')))
				->where($where)->select('ProductList.$')->first();

			$where = ['_id' => Input::get('requestid')];
			$delinfo = Deliveryrequest::where($where)
				->select('ProductList', 'RequesterId', 'discount_in_percent', 'ProductTitle', 'distance', 'RequestType')->first();

			if (count($delinfo) > 0) {

				$userInfo = User::where(array('_id' => $delinfo->RequesterId))->first();
				if (count($userInfo) <= 0) {
					return ['success' => 0, 'msg' => 'Oops! Issue with user'];
				} else {

					$productList = [];
					foreach ($delinfo->ProductList as $productKey) {

						if ($productKey['_id'] == trim(Input::get('itemid'))) {

							$metric_unit = 'cm';
							$imperial_unit = 'kg';
							if (Input::get('measurement_units') == 'inches_lbs') {
								$metric_unit = 'inches';
								$imperial_unit = 'lbs';
							}

							$productKey["price"] = floatval(Input::get('item_price'));
							$productKey["product_name"] = Input::get('item_name');
							$productKey["length"] = Input::get('item_length');
							$productKey["lengthUnit"] = $metric_unit;
							$productKey["width"] = Input::get('item_width');
							$productKey["widthUnit"] = $metric_unit;
							$productKey["height"] = Input::get('item_height');
							$productKey["heightUnit"] = $metric_unit;
							$productKey["weight"] = Input::get('item_weight');
							$productKey["weight_unit"] = $imperial_unit;
							$productKey["marketname"] = Input::get('purchase_from');
							$productKey["status"] = "not_purchased";
							$productKey["shipping_cost_by_user"] = floatval(Input::get('shipping_cost_by_user'));
							$productKey["qty"] = Input::get('qty1');

						}

						$productList[] = $productKey;
					}

					$Notification = new Notify();
					$calculation = new Calculation();

					$data = [
						'shippingcost' => 0,
						'insurance' => 0,
						'ghana_total_amount' => 0,
						'total_amount' => 0,
						'error' => [],
						'formated_text' => '',
						'distance_in_mile' => 0,
						'product_count' => 0,
						'ProcessingFees' => 0,
						'item_cost' => 0,
						'shipping_cost_by_user' => 0,
						'type' => 'online',
						'userType' => 'admin',
						'distance' => (object) ['distance' => $delinfo->distance],
					];

					$info = $calculation->getRate($productList, $data, $userInfo);
					if (@$info['success'] == 0) {
						$responce = [
							'success' => 0,
							'msg' => $info['msg'],
							'reminder_button' => false,
							'assing_tp_button' => false,
						];
						return json_encode($responce);
						die;
					}

					$productList = @$info['data']['product'];

					foreach ($productList as $productKey => $productVal) {
						$productList[$productKey]['aq_fee'] = Reqhelper::get_aquantuo_fees($productVal['shippingCost']);
					}

					$res = Deliveryrequest::where($where)->update([
						'ProductList' => $productList,
						'shippingCost' => $info['shipping_cost'],
						'TotalCost' => $info['total_amount'] - $delinfo->discount,
						'GhanaTotalCost' => $info['ghana_total_amount'],
						'AquantuoFees' => $this->get_aquantuo_fees($info['shipping_cost']),
					]);

					if ($res) {
						$res = Reqhelper::update_status(Input::get('requestid'));
						$responce = [
							'success' => 1,
							'msg' => 'Item has been updated successfully.',
						];

						if ($res == 'not_purchased') {

							/*  Activity Log */
							/*    $insertactivity = [
								                            'request_id' => Input::get('requestid'),
								                            'request_type' => $delinfo->RequestType,
								                            'PackageNumber' => $delinfo->PackageNumber,
								                            'item_id' => Input::get('itemid'),
								                            'package_id' => $deliveryInfo2->ProductList[0]['package_id'],
								                            'item_name' => $deliveryInfo2->ProductList[0]['product_name'],
								                            'log_type' => 'request',
								                            'message' => 'Product has been updated.',
								                            'status' => 'not_purchased',
								                            'EnterOn' => new MongoDate(),

								                            ];

							*/

							$delinfo_all = Deliveryrequest::where($where)->select('ProductList', 'BeforePurchaseTotalCost', 'ProcessingFees', 'discount', 'TotalCost')->first();
							$TotalCost = $insurance = $shippingCost = 0;
							$ProductList = [];
							if (count($delinfo_all) > 0) {
								$ProductList = $delinfo_all->ProductList;

								if (($delinfo_all->TotalCost - 0.50) > $delinfo_all->BeforePurchaseTotalCost) {

									$msg = 'Your item, Item has been updated. Please proceed for the next steps.';

									if (count($delinfo_all->ProductList) > 1) {
										$msg = 'Your all item Item has been updated. Please process to pay for next process.';
									}
									$Notification->setValue('message', $msg);

									if (count($userInfo) > 0) {

										Notification::insert([
											"NotificationTitle" => 'Item Updated',
											"NotificationMessage" => $msg,
											"NotificationType" => "online",
											"NotificationUserId" => array(new MongoId($userInfo->_id)),
											"Date" => new MongoDate(),
											"GroupTo" => "User",
										]);
									}

								} else {
									$delinfo_all->Status = 'paid';
									foreach ($delinfo_all->ProductList as $key => $item) {
										$ProductList[$key]['status'] = 'paid';
									}

									/*  Activity Log */
									$insertactivity = [
										'request_id' => Input::get('requestid'),
										'request_type' => $delinfo->RequestType,
										'PackageNumber' => $delinfo->PackageNumber,
										'item_id' => Input::get('itemid'),
										'package_id' => $deliveryInfo2->ProductList[0]['package_id'],
										'item_name' => $deliveryInfo2->ProductList[0]['product_name'],
										'log_type' => 'request',
										'message' => 'Product has been updated.',
										'status' => 'paid',
										'action_user_id' => Auth::user()->_id,
										'EnterOn' => new MongoDate(),

									];

									Activitylog::insert($insertactivity);
								}

								$delinfo_all->ProductList = $ProductList;
								$delinfo_all->save();

							}

						}

					}

				}

			}
		}

		return json_encode($responce);
	}

	public function update_online_item_info_old_unused() {
		$responce = [
			'success' => 0,
			'msg' => 'Oops! Something went wrong.',
			'reminder_button' => false,
			'assing_tp_button' => false,
		];
		$msg = '';
		$valid = Validator::make(Input::get(), [
			'requestid' => 'required', 'itemid' => 'required',
			'item_price' => 'required',
		]);

		if (!$valid->fails()) {
			$where = ['_id' => Input::get('requestid')];

			$delinfo = Deliveryrequest::where($where)->select('ProductList.$', 'RequesterId', 'discount_in_percent', 'ProductTitle', 'DeliveryFullAddress')->first();

			if (count($delinfo) > 0) {
				$user_data = User::where(array('_id' => $delinfo->RequesterId))->first();
				$Notification = new Notify();

				$metric_unit = 'cm';
				$imperial_unit = 'kg';
				if (Input::get('measurement_units') == 'inches_lbs') {
					$metric_unit = 'inches';
					$imperial_unit = 'lbs';
				}

				$productName = $delinfo->ProductList[0]['product_name'];
				$requesthelper = new Requesthelper(
					[
						"needInsurance" => $delinfo->ProductList[0]['insurance_status'],
						"productQty" => $delinfo->ProductList[0]['qty'],
						"productCost" => Input::get('item_price'),
						"productWidth" => Input::get('item_width'),
						"productWidthUnit" => $metric_unit,
						"productHeight" => Input::get('item_height'),
						"productHeightUnit" => $metric_unit,
						"productLength" => Input::get('item_length'),
						"productLengthUnit" => $metric_unit,
						"productWeight" => Input::get('item_weight'),
						"productWeightUnit" => $imperial_unit,
						"productCategory" => $delinfo->ProductList[0]['category'],
						"productCategoryId" => @$delinfo->ProductList[0]['categoryid'],
						"distance" => $delinfo->distance,
						"travelMode" => $delinfo->ProductList[0]['travelMode'],

					]
				);

				$requesthelper->calculate();
				$calculationinfo = $requesthelper->get_information();

				if (!isset($calculationinfo->error)) {
					$discount = 0;
					$udpate = [
						"ProductList.$.price" => floatval(Input::get('item_price')),
						"ProductList.$.product_name" => Input::get('item_name'),
						"ProductList.$.length" => Input::get('item_length'),
						"ProductList.$.lengthUnit" => $metric_unit,
						"ProductList.$.width" => Input::get('item_width'),
						"ProductList.$.widthUnit" => $metric_unit,
						"ProductList.$.height" => Input::get('item_height'),
						"ProductList.$.heightUnit" => $metric_unit,
						"ProductList.$.weight" => Input::get('item_weight'),
						"ProductList.$.weight_unit" => $imperial_unit,
						"ProductList.$.marketname" => Input::get('purchase_from'),
						"ProductList.$.status" => "not_purchased",
						"ProductList.$.shipping_cost_by_user" => floatval(Input::get('shipping_cost_by_user')),
						"ProductList.$.shippingCost" => $calculationinfo->shippingcost,
						"ProductList.$.insurance" => $calculationinfo->insurance,
						"ProductList.$.aq_fee" => $this->get_aquantuo_fees($calculationinfo->shippingcost),

					];

					$res = Deliveryrequest::where($where)->update($udpate);
					if ($res) {
						$responce = ['success' => 1,
							'msg' => 'Item has been updated successfully.',
							'item_purchase_bt' => false,
							'reminder_button' => false,
						];

						$delinfo_all = Deliveryrequest::where($where)->select('ProductList', 'BeforePurchaseTotalCost')->first();
						$all_items_purchased_status = true;
						$TotalCost = $insurance = $shippingCost = 0;
						$ProductList = $delinfo_all->ProductList;

						foreach ($delinfo_all->ProductList as $key) {

							$insurance = $insurance + $key['insurance'];
							$shippingCost = $shippingCost + $key['shippingCost'];
							$discount += $key['discount'];
							$TotalCost += ($key['shippingCost'] + $key['insurance']) - $key['discount'];

							if (!in_array($key['status'], ['not_purchased', 'cancel'])) {
								$all_items_purchased_status = false;
							}
						}
						$msg = sprintf('Your item "%s" has been purchased.', $productName);
						$Notification->setValue('message', $msg);

						if ($all_items_purchased_status) {
							$status = 'paid';
							if ($delinfo_all->BeforePurchaseTotalCost < $TotalCost) {
								$status = 'not_purchased';
								if (count($delinfo_all->ProductList) > 1) {
									$msg = 'Your all item Item has been received. Please process to pay for next process.';

								} else {
									$msg = 'Your item Item has been received. Please process to pay for next process.';
								}
								$Notification->setValue('message', $msg);

								$link = url("online-request-detail/{$delinfo->_id}");
								send_mail('5886f3986befd91046cf465d', [
									"to" => $user_data->Email,
									"replace" => [
										"[USERNAME]" => $user_data->Name,
										"[PACKAGETITLE]" => $delinfo->ProductTitle,
										"[PACKAGEID]" => @$delinfo->PackageNumber,
										"[URL]" => "<a href='{$link}' >{$link}</a>",
									],
								]);

							}
//
							if ($status == 'paid') {

								$responce['item_purchase_bt'] = true;
								foreach ($delinfo_all->ProductList as $key => $val) {
									$ProductList[$key]['status'] = 'paid';
								}

								send_mail('58774ed77ac6f6881c8b4567', [
									"to" => $user_data->Email,
									"replace" => [
										"[USERNAME]" => $user_data->Name,
										"[PACKAGETITLE]" => $delinfo->ProductTitle,
										"[PACKAGEID]" => @$delinfo->PackageNumber,
										"[DESTINATION]" => @$delinfo->DeliveryFullAddress,

									],
								]);

							} else {
								$responce['reminder_button'] = true;
							}

							Deliveryrequest::where($where)->update([
								'Status' => $status,
								'TotalCost' => $TotalCost,
								'insurance' => $insurance,
								'shippingCost' => $shippingCost,
								'GhanaTotalCost' => 0,
								'ProductList' => $ProductList,
								'AquantuoFees' => $this->get_aquantuo_fees($shippingCost),
							]);
						}
					}

				} else {
					$responce['msg'] = $calculationinfo->error;
				}
			}

		}
		return json_encode($responce);

	}

	public function online_package_purchase(Request $request) {

		$responce = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		$where = [
			"ProductList" => ['$elemMatch' => ['_id' => Input::get('product_id'), 'status' => 'paid']],
			'_id' => Input::get('request_id'),
		];

		$deliveryInfo = Deliveryrequest::where($where)->first();
		$deliveryInfo2 = Deliveryrequest::where($where)->select('_id', 'ProductList.$')->first();

		if (count($deliveryInfo) > 0) {

			if ($deliveryInfo2['ProductList'][0]['travelMode'] == 'air') {
				$startdate = strtotime(date("Y-m-d"));
				$Buinessdays = new Buinessdays();
				$result = $Buinessdays->getWorkingDays($startdate);

				$ExpectedDate = new Mongodate(strtotime('+' . $result . 'days'));
			} else {
				$ExpectedDate = new Mongodate(strtotime('+ 56 days'));
			}

			//print_r($ExpectedDate); die;

			$updation = Deliveryrequest::where($where)
				->update(['ProductList.$.status' => 'purchased',
					'ProductList.$.ExpectedDate' => $ExpectedDate,
				]);

			if ($updation) {

				/* Activity Log  reviewed*/
				$insertactivity = [
					'request_id' => $deliveryInfo2->_id,
					'request_type' => $deliveryInfo->RequestType,
					'PackageNumber' => $deliveryInfo->PackageNumber,
					'item_id' => $deliveryInfo2->ProductList[0]['_id'],
					'package_id' => $deliveryInfo2->ProductList[0]['package_id'],
					'item_name' => $deliveryInfo2->ProductList[0]['product_name'],
					'log_type' => 'request',
					'message' => 'Your item has been received.',
					'status' => 'item_received',
					'action_user_id' => Auth::user()->_id,
					'EnterOn' => new MongoDate(),
				];
				Activitylog::insert($insertactivity);

				$data['purchase_count'] = Deliveryrequest::where(array('_id' => Input::get('request_id')))
					->where(array("ProductList" => array('$elemMatch' => array('status' => 'paid'))))->count();
				if ($data['purchase_count'] == 0) {
					Deliveryrequest::where(['_id' => Input::get('request_id')])->update(['Status' => 'purchased']);
				}

				$responce = ['success' => 1, 'msg' => 'Product has been received successfully.'];
				$user_data = User::where(array('_id' => $deliveryInfo->RequesterId))->first();
				$productName = '';
				if (count($user_data) > 0) {
					Reqhelper::update_status($deliveryInfo->_id);

					foreach ($deliveryInfo->ProductList as $key) {
						if($key['_id'] == Input::get('product_id')){
							$productName = $key['product_name'];
						}
						
					}
					//notification

					$Notification = new Notify();
					$Notification->setValue('title', trans('lang.ONLINE_RECEIVED_TITLE'));

					$Notification->setValue('message', sprintf('Aquantuo has received your online request for %s from the Retailer.', $productName));

					$Notification->setValue('type', 'requester_online_detail');
					$Notification->setValue('locationkey', $request->get('request_id'));
					$Notification->setValue('itemId', $request->get('request_id'));
					$Notification->add_user($user_data->NotificationId, $user_data->DeviceType);
					$Notification->fire();

					Notification::Insert(array(
						"NotificationTitle" => trans('lang.ONLINE_RECEIVED_TITLE'),
						"NotificationMessage" => sprintf(trans('lang.ONLINE_RECEIVED_MSG'), $productName),
						"NotificationType" => "online",
						"NotificationUserId" => array(new MongoId($user_data->_id)),
						"Date" => new MongoDate(),
						"GroupTo" => "User",
					));




					$where2 = [
						"ProductList" => ['$elemMatch' => ['_id' => Input::get('product_id')]],
						'_id' => Input::get('request_id'),
					];
					$req_info = Deliveryrequest::where($where2)->select('ProductList.$')->first();
					$show_date = '';
					if(isset($req_info->ProductList[0]['ExpectedDate']->sec)){
						$show_date = show_date(@$req_info->ProductList[0]['ExpectedDate']);
					}
					

					// $Email = new NewEmail();
					send_mail('590191857ac6f6a2188b4567', [
						"to" => $user_data->Email,
						"replace" => [
							"[USERNAME]" => $user_data->Name,
							"[PACKAGETITLE]" => $productName,
							"[PACKAGEID]" => $deliveryInfo->ProductList[0]['package_id'],
							"[SOURCE]" => $deliveryInfo->PickupAddress,
							"[DESTINATION]" => $deliveryInfo->DeliveryFullAddress,
							"[PACKAGENUMBER]" => $deliveryInfo->ProductList[0]['package_id'],
							"[SHIPPINGCOST]" => $deliveryInfo->ProductList[0]['shippingCost'],
							"[DATE]" => $show_date,
						],
					]);

				}
			}

		}

		return json_encode($responce);

	}

	public function get_ghana_cost($totalAmt) {
		$currencydata = Currency::where(['CountryCode' => 'GHS'])->first();
		if (count($currencydata) > 0) {

			return $totalAmt * (float) @$currencydata->Rate;
		}

		return $totalAmt;
	}
	public function get_aquantuo_fees($totalcost) {
		$fees = 0;
		$configurationdata = Configuration::find('5673e33e6734c4f874685c84');
		if ($configurationdata->count() > 0) {
			$fees = ((floatval($totalcost) * $configurationdata->aquantuoFees) / 100);
		}
		return $fees;
	}

	public function assignTp($id, Request $request) {

		$result = Deliveryrequest::where(['_id' => $id])->first();
		$obj = json_decode($request->get('tp_name'));

		if (!empty($obj)) {

			if ($result->RequestType == 'online') {
				$where = array("ProductList" => array('$elemMatch' => array('_id' => $request->get('product_id'), 'status' => 'purchased')));
			} elseif ($result->RequestType == 'delivery') {
				$where = array("ProductList" => array('$elemMatch' => array('_id' => $request->get('product_id'), 'status' => 'purchased')));
			} elseif ($result->RequestType == 'buy_for_me') {
				$where = array("ProductList" => array('$elemMatch' => array('_id' => $request->get('product_id'), 'status' => 'item_received')));
			}

			$deliveryInfo = Deliveryrequest::where(array('_id' => $request->get('request_id')))->where($where)->select('ProductList.$', 'RequesterId', 'PaymentStatus', 'RequesterName', 'ProductTitle', 'PackageNumber', 'DeliveryFullAddress', 'TotalCost', 'Status', 'StripeChargeId', 'StripeChargeId2', 'RequestType')->first();

			if ($result->RequestType == 'online') {
				$where = array("ProductList" => array('$elemMatch' => array('status' => 'purchased')));
			}

			if ($result->RequestType == 'buy_for_me') {
				$where = array("ProductList" => array('$elemMatch' => array('status' => 'item_received')));
			}

			if ($result->RequestType == 'delivery') {
				$where = array("ProductList" => array('$elemMatch' => array('status' => 'purchased')));
			}

			$deliveryInfo = Deliveryrequest::where(array('_id' => $request->get('request_id')))->where($where)->select('ProductList.$', 'RequesterId', 'PaymentStatus', 'RequesterName', 'ProductTitle', 'PackageNumber', 'DeliveryFullAddress', 'TotalCost', 'Status', 'StripeChargeId', 'StripeChargeId2')->first();

			if (count($deliveryInfo) > 0) {
				$userinfo = User::find($deliveryInfo->RequesterId);
				if (count($userinfo) <= 0) {
					$responce['msg'] = 'Requester not found';
					return json_encode($responce);
					die;
				}
			}

			/*if ($deliveryInfo->PaymentStatus != 'paid') {
				if (!empty(trim($deliveryInfo->StripeChargeId))) {
					$res = RSStripe::retrive_amount($deliveryInfo->StripeChargeId);
					if (!isset($res['id'])) {

						$res = RSStripe::find_charge($deliveryInfo->StripeChargeId);
						if ($res['captured'] != 1) {
							$responce['msg'] = $res;
							return json_encode($responce);
							die;
						}

					}
				}
				if (!empty(trim($deliveryInfo->StripeChargeId2))) {
					$res = RSStripe::retrive_amount($deliveryInfo->StripeChargeId2);
					if (!isset($res['id'])) {

						$res = RSStripe::find_charge($deliveryInfo->StripeChargeId2);
						if ($res['captured'] != 1) {
							$responce['msg'] = $res;
							return json_encode($responce);
							die;
						}
					}
				}

				Transaction::insert([
					"SendById" => $deliveryInfo->RequesterId,
					"SendByName" => $deliveryInfo->RequesterName,
					"SendToId" => "aquantuo",
					"RecieveByName" => "Aquantuo",
					"Description" => sprintf("Amount deposited against delivery request for %s, PackageId: %u to %s", $deliveryInfo->ProductTitle, $deliveryInfo->PackageNumber, $deliveryInfo->DeliveryFullAddress
					),
					"Credit" => $deliveryInfo->TotalCost,
					"Debit" => "",
					"Status" => "paid",
					"TransactionType" => "online",
					"EnterOn" => new MongoDate(),
				]);

				$deliveryInfo->PaymentStatus = 'paid';
				$deliveryInfo->save();
			}*/

			$deliveryInfo = Deliveryrequest::where(['_id' => $id])->first();
			$deliveryInfo2 = Deliveryrequest::where(array('_id' => $request->get('request_id')))->where($where)->select('ProductList.$', 'RequesterId', 'PaymentStatus', 'RequesterName', 'ProductTitle', 'PackageNumber', 'DeliveryFullAddress', 'TotalCost', 'Status', 'StripeChargeId', 'StripeChargeId2', 'RequestType')->first();

			$array = $deliveryInfo->ProductList;

			$i = 0;
			foreach ($array as $key) {
				if (in_array($key['_id'], Input::get('item_name'))) {
					$array[$i]['tpid'] = (string) $obj->id;
					$array[$i]['tpName'] = $obj->name;
					$array[$i]['status'] = 'assign';

					/* Activity Log */
					$insertactivity = [
						'request_id' => $request->get('request_id'),
						'request_type' => $deliveryInfo->RequestType,
						'PackageNumber' => $deliveryInfo->PackageNumber,
						'item_id' => $key['_id'],
						'package_id' => $key['package_id'],
						'item_name' => $key['product_name'],
						'tpid' => (string) $obj->id,
						'tpName' => $obj->name,
						'log_type' => 'request',
						'message' => 'Transporter has been assigned.',
						'status' => 'assign',
						'action_user_id' => Auth::user()->_id,
						'EnterOn' => new MongoDate(),
					];
					Activitylog::insert($insertactivity);
				}
				$i++;
				$product_detail = $key;
			}

			$updation = Deliveryrequest::where('_id', '=', $id)
				->update(['ProductList' => $array]);

			if ($updation) {
				if ($result->RequestType == 'delivery') {
					Reqhelper::update_status2($request->get('request_id'));
				} else {
					Reqhelper::update_status($request->get('request_id'));
				}

				$responce = ['success' => 1, 'msg' => 'Transporter has been assigned.', 'count' => 0];
			}

			$user_data = User::where(array('_id' => $deliveryInfo->RequesterId))->first();

			$transporter_data = User::where(array('_id' => $obj->id))->first();

			if (count($user_data) > 0 && count($transporter_data) > 0) {

				$productName = $deliveryInfo->ProductList[0]['product_name'];

				//notification to user
				$Notification = new Notify();
				$Notification->setValue('title', trans('lang.ITEM_ASSIGN_TP_TITLE'));

				$Notification->setValue('message', sprintf(trans('lang.ITEM_ASSIGN_TP_USER'), $productName,
					$deliveryInfo->ProductList[0]['package_id']));

				$Notification->setValue('type', 'BUY_FOR_ME_TRANSPORTER_ASSIGN_FOR_USER');
				$Notification->setValue('location', 'BUY_FOR_ME_TRANSPORTER_ASSIGN_FOR_USER');
				$Notification->setValue('locationkey', $request->get('request_id'));
				$Notification->setValue('itemId', $request->get('product_id'));
				$Notification->add_user($user_data->NotificationId, $user_data->DeviceType);
				$Notification->fire();
				$Email = new NewEmail();
				//Mail to user

				$Email->send_mail('57daa7fa7ac6f6a3218b4567', [
					"to" => $user_data->Email,
					"replace" => [
						"[USERNAME]" => $user_data->Name,
						"[PACKAGETITLE]" => $productName,
						"[TRANSPORTER]" => $transporter_data->Name,
						"[DATE]"=> show_date(@$deliveryInfo->ProductList[0]['ExpectedDate'])

					],
				]
				);

				//Mail to transporter
				if ($deliveryInfo->RequestType == 'delivery') {
					$productprice = number_format($deliveryInfo->ProductList[0]['productCost'], 2);
					$insuranceStatus = 'No';
				} else {
					$productprice = number_format($deliveryInfo->ProductList[0]['price'], 2);
					$insuranceStatus = ucfirst($deliveryInfo->ProductList[0]['insurance_status']);
				}

				$Email->send_mail('587396736befd9212ec39eba', [
					"to" => $transporter_data->Email,
					"replace" => [
						"[USERNAME]" => $transporter_data->Name,
						"[PACKAGETITLE]" => $productName,
						"[PRICE]" => $productprice,
						"[INSURANCE]" => $insuranceStatus,

					],
				]
				);

				//notification to transporter
				$Notification = new Notify();
				$Notification->setValue('title', trans('lang.ITEM_ASSIGN_TP_TITLE'));
				$Notification->setValue('message', sprintf(trans('lang.ITEM_ASSIGN_TP_TRP'), $productName, $deliveryInfo->ProductList[0]['package_id']));

				$Notification->setValue('type', 'BUY_FOR_ME_TRANSPORTER_ASSIGN');
				$Notification->setValue('location', 'BUY_FOR_ME_TRANSPORTER_ASSIGN');
				$Notification->setValue('locationkey', $request->get('request_id'));
				$Notification->setValue('itemId', $request->get('product_id'));
				$Notification->add_user($transporter_data->NotificationId, $transporter_data->DeviceType);
				$Notification->fire();

				$insNotification = array(
					array(
						"NotificationTitle" => trans('lang.ITEM_ASSIGN_TP_TITLE'),
						/*"NotificationShortMessage"    =>'You have assigned transporter for '.$productName.'.',*/

						"NotificationMessage" => sprintf(trans('lang.ITEM_ASSIGN_TP_USER'), $productName,
							$deliveryInfo->ProductList[0]['package_id']),
						"NotificationType" => "BUY_FOR_ME_TRANSPORTER_ASSIGN_FOR_USER.",
						"NotificationUserId" => array(new MongoId($user_data->_id)),
						"Date" => new MongoDate(),
						"GroupTo" => "User",
					),
					array(
						"NotificationTitle" => trans('lang.ITEM_ASSIGN_TP_TITLE'),
						"NotificationMessage" => sprintf(trans('lang.ITEM_ASSIGN_TP_TRP'), $productName,
							$deliveryInfo->ProductList[0]['package_id']),
						"NotificationType" => "BUY_FOR_ME_TRANSPORTER_ASSIGN.",
						"NotificationUserId" => array(new MongoId($transporter_data->_id)),
						"Date" => new MongoDate(),
						"GroupTo" => "User",
					),
				);

				Notification::Insert($insNotification);
			}

		}

		if ($result->RequestType == 'online') {
			return Redirect::to('admin/online_package/detail/' . $id)->withSuccess('All Trnsporter Assign successfully.');
		} elseif ($result->RequestType == 'delivery') {
			return Redirect::to('admin/package/detail/' . $id)->withSuccess('All Trnsporter Assign successfully.');
		} elseif ($result->RequestType == 'buy_for_me') {
			return Redirect::to('admin/buy-for-me/detail/' . $request->get('request_id'))->withSuccess('All Trnsporter Assign successfully.');
		}

	}

	//asign transporter
	public function buy_for_me_assign_transporter(Request $request) {
		$responce = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		$obj = json_decode($request->get('tp_name'));
		
		if (!empty($obj)) {
			$where = array("ProductList" => array('$elemMatch' => array('_id' => $request->get('product_id'), 'status' => 'item_received')));

			$deliveryInfo = Deliveryrequest::where(array('_id' => $request->get('request_id')))->where($where)->select('ProductList.$', 'RequesterId', 'PaymentStatus', 'RequesterName', 'ProductTitle', 'PackageNumber', 'DeliveryFullAddress', 'TotalCost', 'Status', 'StripeChargeId', 'StripeChargeId2', 'RequestType')->first();

			if (count($deliveryInfo) == 0) {
				$where = array("ProductList" => array('$elemMatch' => array('_id' => $request->get('product_id'), 'status' => 'assign')));
				$deliveryInfo = Deliveryrequest::where(array('_id' => $request->get('request_id')))->where($where)->select('ProductList.$', 'RequesterId', 'PaymentStatus', 'RequesterName', 'ProductTitle', 'PackageNumber', 'DeliveryFullAddress', 'TotalCost', 'Status', 'StripeChargeId', 'StripeChargeId2', 'RequestType')->first();
			}

			if (count($deliveryInfo) > 0) {
				$userinfo = User::find($deliveryInfo->RequesterId);
				if (count($userinfo) <= 0) {
					$responce['msg'] = 'Requester not found';
					return json_encode($responce);
					die;
				}

				if ($deliveryInfo->PaymentStatus != 'paid') {
					if (!empty(trim($deliveryInfo->StripeChargeId))) {
						$res = RSStripe::retrive_amount($deliveryInfo->StripeChargeId);
						
						if (!isset($res['id'])) {
							$res = RSStripe::find_charge($deliveryInfo->StripeChargeId);
							
							if ($res['captured'] != 1) {
								$responce['msg'] = $res;
								return json_encode($responce);
								die;
							}
						}
					}
					
					if (!empty(trim($deliveryInfo->StripeChargeId2))) {
						$res = RSStripe::retrive_amount($deliveryInfo->StripeChargeId2);
						
						if (!isset($res['id'])) {
							$res = RSStripe::find_charge($deliveryInfo->StripeChargeId2);
							
							if ($res['captured'] != 1) {
								$responce['msg'] = $res;
								return json_encode($responce);
								die;
							}
						}
					}

					Transaction::insert([
						"SendById" => $deliveryInfo->RequesterId,
						"SendByName" => $deliveryInfo->RequesterName,
						"SendToId" => "aquantuo",
						"RecieveByName" => "Aquantuo",
						"Description" => sprintf("Amount deposited against delivery request for %s, PackageId: %u to %s", $deliveryInfo->ProductTitle, $deliveryInfo->PackageNumber, $deliveryInfo->DeliveryFullAddress
						),
						"Credit" => $deliveryInfo->TotalCost,
						"Debit" => "",
						"Status" => "paid",
						"TransactionType" => "online",
						"EnterOn" => new MongoDate(),
					]);

					$deliveryInfo->PaymentStatus = 'paid';
					$deliveryInfo->save();
				}

				$updation = Deliveryrequest::where($where)
					->where(array('_id' => $request->get('request_id')))
					->update([
						'ProductList.$.tpid' => (string) $obj->id,
						'ProductList.$.tpName' => $obj->name,
						'ProductList.$.status' => 'assign',
						'UpdateOn' => new MongoDate(),
					]);

				/* Activity Log */
				$insertactivity = [
					'request_id' => $request->get('request_id'),
					'request_type' => $deliveryInfo->RequestType,
					'PackageNumber' => $deliveryInfo->PackageNumber,
					'item_id' => $deliveryInfo->ProductList[0]['_id'],
					'package_id' => $deliveryInfo->ProductList[0]['package_id'],
					'item_name' => $deliveryInfo->ProductList[0]['product_name'],
					'tpid' => (string) $obj->id,
					'tpName' => $obj->name,
					'log_type' => 'request',
					// 'message' => 'Transporter has been assigned.',
					'message' => 'Shipment departed originating country.',
					'status' => 'assign',
					'action_user_id'=>Auth::user()->_id,
					'EnterOn' => new MongoDate(),
				];

				Activitylog::insert($insertactivity);

				if ($updation) {
					Reqhelper::update_status($request->get('request_id'));
					$responce = ['success' => 1, 'msg' => 'Shipment departed originating country.', 'count' => 0];
				}

				$user_data = User::where(array('_id' => $deliveryInfo->RequesterId))->first();
				$transporter_data = User::where(array('_id' => $obj->id))->first();

				if (count($user_data) > 0 && count($transporter_data) > 0) {
					$productName = $deliveryInfo->ProductList[0]['product_name'];

					//notification to user

					/* $Notification = new Notify();
						                    $Notification->setValue('title', trans('lang.ITEM_ASSIGN_TP_TITLE'));

						                    $Notification->setValue('message', sprintf(trans('lang.ITEM_ASSIGN_TP_USER'), $productName,
						                    $deliveryInfo->ProductList[0]['package_id']));

						                    $Notification->setValue('type', 'BUY_FOR_ME_TRANSPORTER_ASSIGN_FOR_USER');
						                    $Notification->setValue('location', 'BUY_FOR_ME_TRANSPORTER_ASSIGN_FOR_USER');
						                    $Notification->setValue('locationkey', $request->get('request_id'));
						                    $Notification->setValue('itemId', $request->get('product_id'));
					*/
					//$Notification->fire();

					$cron_noti = [
						//~ "title" => trans('lang.ITEM_ASSIGN_TP_TITLE'),
						//~ "message" => sprintf(trans('lang.ITEM_ASSIGN_TP_USER'), $productName, $deliveryInfo->ProductList[0]['package_id']),
						'title' => trans('lang.SHIPMENT_DEPARTED_TITLE'),
						'message' => trans('lang.SHIPMENT_DEPARTED_MSG'),
						"type" => 'BUY_FOR_ME_TRANSPORTER_ASSIGN_FOR_USER',
						"locationkey" => $request->get('request_id'),
						"NotificationId" => $user_data->NotificationId,
						"DeviceType" => $user_data->DeviceType,
						'status' => 'ready',
						'by_mean' => 'notification',
					];
					SendMail::insert($cron_noti);

					// Mail to user
					$Email = new NewEmail();
					$Email->send_mail('57daa7fa7ac6f6a3218b4567', [
	                    "to" => $user_data->Email,
	                    "replace" => [
	                    "[USERNAME]" => $user_data->Name,
	                    "[PACKAGETITLE]" => $productName,
	                    "[TRANSPORTER]" => $transporter_data->Name,
	                    "[DATE]"=> show_date(@$deliveryInfo->ProductList[0]['ExpectedDate'])

	                    ],
                    ]);
					

					$cron_mail = [
						"USERNAME" => ucfirst($user_data->Name),
						"PACKAGETITLE" => ucfirst($productName),
						"PACKAGEID" => '',
						"SOURCE" => '',
						"DESTINATION" => '',
						"PACKAGENUMBER" => '',
						"SHIPPINGCOST" => '',
						"TOTALCOST" => '',
						'email_id' => '57daa7fa7ac6f6a3218b4567',
						'email' => $user_data->Email,
						'status' => 'ready',
						'TRANSPORTER' => $transporter_data->Name,
					];

					//SendMail::insert($cron_mail);

					//Mail to transporter

					/*send_mail('587396736befd9212ec39eba', [
						                    "to" => $transporter_data->Email,
						                    "replace" => [
						                    "[USERNAME]" => $transporter_data->Name,
						                    "[PACKAGETITLE]" => $productName,
						                    "[PRICE]" => number_format($deliveryInfo->ProductList[0]['price'], 2),
						                    "[INSURANCE]" => ucfirst($deliveryInfo->ProductList[0]['insurance_status']),

						                    ],
						                    ]
					*/

					$cron_mail2 = [
						"USERNAME" => ucfirst($transporter_data->Name),
						"PACKAGETITLE" => ucfirst($productName),
						"PACKAGEID" => '',
						"SOURCE" => '',
						"DESTINATION" => '',
						"PACKAGENUMBER" => '',
						"SHIPPINGCOST" => '',
						"TOTALCOST" => '',
						'email_id' => '587396736befd9212ec39eba',
						'email' => $transporter_data->Email,
						'status' => 'ready',
						'TRANSPORTER' => $transporter_data->Name,
						"PRICE" => number_format($deliveryInfo->ProductList[0]['price'], 2),
						"INSURANCE" => ucfirst($deliveryInfo->ProductList[0]['insurance_status']),
					];

					SendMail::insert($cron_mail2);

					//notification to transporter
					//$Notification = new Notify();
					/*$Notification->setValue('title', trans('lang.ITEM_ASSIGN_TP_TITLE'));
	                    $Notification->setValue('message', sprintf(trans('lang.ITEM_ASSIGN_TP_TRP'), $productName, $deliveryInfo->ProductList[0]['package_id']));

	                    $Notification->setValue('type', 'BUY_FOR_ME_TRANSPORTER_ASSIGN');
	                    $Notification->setValue('location', 'BUY_FOR_ME_TRANSPORTER_ASSIGN');
	                    $Notification->setValue('locationkey', $request->get('request_id'));
	                    $Notification->setValue('itemId', $request->get('product_id'));
	                    $Notification->add_user($transporter_data->NotificationId, $transporter_data->DeviceType);
					*/

					$cron_noti2 = [
						//~ "title" => trans('lang.ITEM_ASSIGN_TP_TITLE'),
						//~ "message" => sprintf(trans('lang.ITEM_ASSIGN_TP_TRP'), $productName, $deliveryInfo->ProductList[0]['package_id']),
						'title' => trans('lang.SHIPMENT_DEPARTED_TITLE'),
						'message' => trans('lang.SHIPMENT_DEPARTED_MSG'),
						"type" => 'BUY_FOR_ME_TRANSPORTER_ASSIGN',
						"locationkey" => $request->get('request_id'),
						"NotificationId" => $transporter_data->NotificationId,
						"DeviceType" => $transporter_data->DeviceType,
						'status' => 'ready',
						'by_mean' => 'notification',
						'itemId' => $request->get('product_id'),
					];
					SendMail::insert($cron_noti2);

					$insNotification = array(
						array(
							//~ "NotificationTitle" => trans('lang.ITEM_ASSIGN_TP_TITLE'),
							//~ "NotificationMessage" => sprintf(trans('lang.ITEM_ASSIGN_TP_USER'), $productName, $deliveryInfo->ProductList[0]['package_id']),
							'NotificationTitle' => trans('lang.SHIPMENT_DEPARTED_TITLE'),
							'NotificationMessage' => trans('lang.SHIPMENT_DEPARTED_MSG'),
							"NotificationType" => "BUY_FOR_ME_TRANSPORTER_ASSIGN_FOR_USER.",
							"NotificationUserId" => array(new MongoId($user_data->_id)),
							"Date" => new MongoDate(),
							"GroupTo" => "User",
						),
						array(
							//~ "NotificationTitle" => trans('lang.ITEM_ASSIGN_TP_TITLE'),
							//~ "NotificationMessage" => sprintf(trans('lang.ITEM_ASSIGN_TP_TRP'), $productName, $deliveryInfo->ProductList[0]['package_id']),
							'NotificationTitle' => trans('lang.SHIPMENT_DEPARTED_TITLE'),
							'NotificationMessage' => trans('lang.SHIPMENT_DEPARTED_MSG'),
							"NotificationType" => "BUY_FOR_ME_TRANSPORTER_ASSIGN.",
							"NotificationUserId" => array(new MongoId($transporter_data->_id)),
							"Date" => new MongoDate(),
							"GroupTo" => "User",
						),
					);

					Notification::Insert($insNotification);
				}
				//  }
			}
		}

		return json_encode($responce);
	}

	public function onlineAssigntransporter(Request $request) {

		$responce = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		$obj = json_decode($request->get('tp_name'));

		if (!empty($obj)) {

			$where = array("ProductList" => array('$elemMatch' => array('_id' => $request->get('product_id'), 'status' => 'purchased')));

			$deliveryInfo = Deliveryrequest::where(array('_id' => $request->get('request_id')))->where($where)
				->select('ProductList.$', 'RequesterId', 'PaymentStatus', 'RequesterName', 'ProductTitle', 'PackageNumber', 'DeliveryFullAddress', 'TotalCost', 'Status', 'StripeChargeId', 'StripeChargeId2','RequestType')->first();

			if (count($deliveryInfo) == 0) {
				//orwhere
				$where = array("ProductList" => array('$elemMatch' => array(
					'_id' => $request->get('product_id'),
					'status' => 'assign',

				)));
				$deliveryInfo = Deliveryrequest::where(array('_id' => $request->get('request_id')))->where($where)
					->select('ProductList.$', 'RequesterId', 'PaymentStatus', 'RequesterName', 'ProductTitle', 'PackageNumber', 'DeliveryFullAddress', 'TotalCost', 'Status', 'StripeChargeId', 'StripeChargeId2','RequestType')->first();
				if (count($deliveryInfo) == 0) {
					return json_encode($responce);die;
				}
			}

			if (count($deliveryInfo) > 0) {
				$userinfo = User::find($deliveryInfo->RequesterId);
				if (count($userinfo) <= 0) {
					$responce['msg'] = 'Requester not found';
					return json_encode($responce);die;
				}
			}

			if ($deliveryInfo->PaymentStatus != 'paid') {
				if (!empty(trim($deliveryInfo->StripeChargeId))) {
					$res = RSStripe::retrive_amount($deliveryInfo->StripeChargeId);
					if (!isset($res['id'])) {

						$res = RSStripe::find_charge($deliveryInfo->StripeChargeId);
						if ($res['captured'] != 1) {
							$responce['msg'] = $res;
							return json_encode($responce);
							die;
						}

					}
				}
				if (!empty(trim($deliveryInfo->StripeChargeId2))) {
					$res = RSStripe::retrive_amount($deliveryInfo->StripeChargeId2);
					if (!isset($res['id'])) {

						$res = RSStripe::find_charge($deliveryInfo->StripeChargeId2);
						if ($res['captured'] != 1) {
							$responce['msg'] = $res;
							return json_encode($responce);
							die;
						}
					}
				}

				Transaction::insert([
					"SendById" => $deliveryInfo->RequesterId,
					"SendByName" => $deliveryInfo->RequesterName,
					"SendToId" => "aquantuo",
					"RecieveByName" => "Aquantuo",
					"Description" => sprintf("Amount deposited against delivery request for %s, PackageId: %u to %s", $deliveryInfo->ProductTitle, $deliveryInfo->PackageNumber, $deliveryInfo->DeliveryFullAddress
					),
					"Credit" => $deliveryInfo->TotalCost,
					"Debit" => "",
					"Status" => "paid",
					"TransactionType" => "online",
					"EnterOn" => new MongoDate(),
				]);

				$deliveryInfo->PaymentStatus = 'paid';
				$deliveryInfo->save();
			}

			$updation = Deliveryrequest::where($where)
				->where(array('_id' => $request->get('request_id')))
				->update([
					'ProductList.$.tpid' => (string) $obj->id,
					'ProductList.$.tpName' => $obj->name,
					'ProductList.$.status' => 'assign',
					'UpdateOn' => new MongoDate(),
					'status' => 'assign',
				]);

			if ($updation) {

				/* Activity Log */
				$insertactivity = [
					'request_id' => $deliveryInfo->_id,
					'request_type' => $deliveryInfo->RequestType,
					'PackageNumber' => $deliveryInfo->PackageNumber,
					'item_id' => $deliveryInfo->ProductList[0]['_id'],
					'package_id' => $deliveryInfo->ProductList[0]['package_id'],
					'item_name' => $deliveryInfo->ProductList[0]['product_name'],
					'tpid' => (string) $obj->id,
					'tpName' => $obj->name,
					'log_type' => 'request',
					// 'message' => 'Transporter has been assigned.',
					'message' => 'Shipment departed originating country.',
					'status' => 'assign',
					'action_user_id' => Auth::user()->_id,
					'EnterOn' => new MongoDate(),
				];

				Activitylog::insert($insertactivity);

				Reqhelper::update_status($request->get('request_id'));
				$responce = ['success' => 1, 'msg' => 'Shipment departed originating country.', 'count' => 0];
			}

			$user_data = User::where(array('_id' => $deliveryInfo->RequesterId))->first();
			$transporter_data = User::where(array('_id' => $obj->id))->first();
			
			if (count($user_data) > 0 && count($transporter_data) > 0) {
				$productName = $deliveryInfo->ProductList[0]['product_name'];

				// notification to user
				$Notification = new Notify();
				//~ $Notification->setValue('title', trans('lang.ITEM_ASSIGN_TP_TITLE'));
				//~ $Notification->setValue('message', sprintf(trans('lang.ITEM_ASSIGN_TP_USER'), $productName,
					//~ $deliveryInfo->ProductList[0]['package_id']));
				$Notification->setValue('title', trans('lang.SHIPMENT_DEPARTED_TITLE'));
				$Notification->setValue('message', trans('lang.SHIPMENT_DEPARTED_MSG'));
				$Notification->setValue('TransporterName', $transporter_data->Name);
				$Notification->setValue('TransporterImage', $transporter_data->Image);
				
				if ($deliveryInfo->RequestType == 'buy_for_me') {
					$Notification->setValue('type', 'BUY_FOR_ME_TRANSPORTER_ASSIGN_FOR_USER');
					$Notification->setValue('location', 'BUY_FOR_ME_TRANSPORTER_ASSIGN_FOR_USER');
				} else {
					$Notification->setValue('type', 'online_transporter_assigned_for_user');
					$Notification->setValue('location', 'ONLINE_TRANSPORTER_ASSIGN_FOR_USER');
				}

				$Notification->setValue('locationkey', $request->get('request_id'));
				$Notification->setValue('itemId', $request->get('product_id'));
				$Notification->add_user($user_data->NotificationId, $user_data->DeviceType);
				$Notification->fire();

				// Mail to user
				$Email = new NewEmail();
				$Email->send_mail('57daa7fa7ac6f6a3218b4567', [
					"to" => $user_data->Email,
					"replace" => [
						"[USERNAME]" => $user_data->Name,
						"[PACKAGETITLE]" => $productName,
						"[TRANSPORTER]" => $transporter_data->Name,
						"[DATE]"=> show_date(@$deliveryInfo->ProductList[0]['ExpectedDate'])
					]
				]);

				$cron_mail = [
					"USERNAME" => ucfirst($user_data->Name),
					"PACKAGETITLE" => ucfirst($productName),
					"TRANSPORTER" => $transporter_data->Name,
					'email_id' => '57daa7fa7ac6f6a3218b4567',
					'email' => $user_data->Email,
					'status' => 'ready',
				];
				//SendMail::insert($cron_mail);

				//Mail to transporter

				/*send_mail('587396736befd9212ec39eba', [
						"to" => $transporter_data->Email,
						"replace" => [
							"[USERNAME]" => $transporter_data->Name,
							"[PACKAGETITLE]" => $productName,
							"[PRICE]" => number_format($deliveryInfo->ProductList[0]['price'], 2),
							"[INSURANCE]" => ucfirst($deliveryInfo->ProductList[0]['insurance_status']),

						],
					]);*/

				// requester Email
				$cron_mail2 = [
					"USERNAME" => ucfirst($transporter_data->Name),
					"PACKAGETITLE" => ucfirst($productName),
					"PRICE" => number_format($deliveryInfo->ProductList[0]['price'], 2),
					"INSURANCE" => ucfirst($deliveryInfo->ProductList[0]['insurance_status']),
					'email_id' => '587396736befd9212ec39eba',
					'email' => $transporter_data->Email,
					'status' => 'ready',
				];
				SendMail::insert($cron_mail2);

				//notification to transporter
				$Notification = new Notify();
				//~ $Notification->setValue('title', trans('lang.ITEM_ASSIGN_TP_TITLE'));
				//~ $Notification->setValue('message', sprintf(trans('lang.ITEM_ASSIGN_TP_TRP'), $productName, $deliveryInfo->ProductList[0]['package_id']));
				$Notification->setValue('title', trans('lang.SHIPMENT_DEPARTED_TITLE'));
				$Notification->setValue('message', trans('lang.SHIPMENT_DEPARTED_MSG'));

				if ($deliveryInfo->RequestType == 'buy_for_me') {
					$Notification->setValue('type', 'BUY_FOR_ME_TRANSPORTER_ASSIGN');
					$Notification->setValue('location', 'BUY_FOR_ME_TRANSPORTER_ASSIGN');
				} else {
					$Notification->setValue('type', 'online_transporter_assigned');
					$Notification->setValue('location', 'ONLINE_TRANSPORTER_ASSIGN');
				}

				$Notification->setValue('locationkey', $request->get('request_id'));
				$Notification->setValue('itemId', $request->get('product_id'));
				$Notification->add_user($transporter_data->NotificationId, $transporter_data->DeviceType);
				$Notification->fire();

				$insNotification = array(
					array(
						//~ "NotificationTitle" => trans('lang.ITEM_ASSIGN_TP_TITLE'),
						//~ "NotificationMessage" => sprintf(trans('lang.ITEM_ASSIGN_TP_USER'), $productName, $deliveryInfo->ProductList[0]['package_id']),
						"NotificationTitle" => trans('lang.SHIPMENT_DEPARTED_TITLE'),
						"NotificationMessage" => trans('lang.SHIPMENT_DEPARTED_MSG'),
						"NotificationType" => "BUY_FOR_ME_TRANSPORTER_ASSIGN_FOR_USER.",
						"NotificationUserId" => array(new MongoId($user_data->_id)),
						"Date" => new MongoDate(),
						"GroupTo" => "User",
					),
					array(
						//~ "NotificationTitle" => trans('lang.ITEM_ASSIGN_TP_TITLE'),
						//~ "NotificationMessage" => sprintf(trans('lang.ITEM_ASSIGN_TP_TRP'), $productName, $deliveryInfo->ProductList[0]['package_id']),
						"NotificationTitle" => trans('lang.SHIPMENT_DEPARTED_TITLE'),
						"NotificationMessage" => trans('lang.SHIPMENT_DEPARTED_MSG'),
						"NotificationType" => "BUY_FOR_ME_TRANSPORTER_ASSIGN.",
						"NotificationUserId" => array(new MongoId($transporter_data->_id)),
						"Date" => new MongoDate(),
						"GroupTo" => "User",
					),
				);

				Notification::Insert($insNotification);
			}

		}

		return json_encode($responce);
	}

	/* transporter csv */
	public function Transporter_exportexcel(Request $request) {

		$q = User::query();

		$search_value = trim(Input::get('search_value'));
		$city = json_decode(Input::get('City'));
		$state = json_decode(Input::get('State'));
		$country = json_decode(Input::get('Country'));

		if ($city != '') {
			$q->where('City', 'like', "%$city->name%");
		}

		if ($state != '') {
			$q->where('State', 'like', "%$state->name%");
		}

		if ($country != '') {
			$q->where('Country', 'like', "%$country->name%");
		}

		if ($search_value != '') {

			$q->where('Name', 'like', "%$search_value%")
				->orWhere('Email', 'like', "%$search_value%")
				->orWhere('UniqueNo', 'like', "%$search_value%");
		}

		if (Input::get('transporter_type') != '') {
			$q->where('TransporterType', '=', Input::get('transporter_type'));

		}

		$info = $q->whereIn('UserType', ['transporter', 'both'])->get();

		$filename = time() . '.xls';
		$export_xls = new export_xls();
		$export_xls->ExportXLS($filename);

		$header[] = "S.No.";
		$header[] = "TransporterType";
		$header[] = "Transporter Name";
		$header[] = "Email";
		$header[] = "CountryCode";
		$header[] = "PhoneNo";
		$header[] = "AlternateCCode";
		$header[] = "AlternatePhoneNo";
		$header[] = "City";
		$header[] = "State";
		$header[] = "Country";
		$header[] = "Street1";
		$header[] = "Street2";
		$header[] = "Unit #";
		$header[] = "Licence Image";
		$header[] = "Profile Image";
		$header[] = "type of id";
		$header[] = "UserType";
		$header[] = "BankName";
		$header[] = "AccountHolderName";
		$header[] = "BankAccountNo";
		$header[] = "RoutingNo";
		$header[] = "PaypalId";
		$header[] = "SSN No.";
		$header[] = "UserStatus";
		$header[] = "TransporterStatus";
		$header[] = "registration date";

		$export_xls->addHeader($header);

		foreach ($info as $key => $val) {

			$ssn = ($val['SSN'] != '') ? 'XXXXX-' . substr($val['SSN'], -4) : '';
			if ($val['TransporterStatus'] == 'not_verify') {
				$TransporterStatus = 'Not Verified';
			} else {
				$TransporterStatus = 'Verified';
			}

			if ($val['LicenceId'] != '') {
				$LicenceImage = 'Yes';
			} else {
				$LicenceImage = 'No';
			}

			if ($val['Image'] != '') {
				$Image = 'Yes';
			} else {
				$Image = 'No';
			}

			$row = array();
			$row[] = array(++$key,
				$val['TransporterType'],
				$val['Name'],
				$val['Email'],
				$val['CountryCode'],
				$val['PhoneNo'],
				$val['AlternateCCode'],
				$val['AlternatePhoneNo'],
				$val['City'],
				$val['State'],
				$val['Country'],
				$val['Street1'],
				$val['Street2'],
				$val['UniqueNo'],
				$LicenceImage,
				$Image,
				$val['type_of_id'],
				$val['UserType'],
				$val['BankName'],
				$val['AccountHolderName'],
				$val['BankAccountNo'],
				$val['RoutingNo'],
				$val['PaypalId'],
				$ssn,
				$val['UserStatus'],
				$TransporterStatus,
				show_date($val['EnterOn']),
			);
			$export_xls->addRow($row);
		}

		$export_xls->sendFile();
	}

	/* Requester Export Csv  */
	public function Requester_exportexcel() {

		$q = User::query();

		$search_value = trim(Input::get('search_value'));
		$city = json_decode(Input::get('City'));
		$state = json_decode(Input::get('State'));
		$country = json_decode(Input::get('Country'));

		$streat = trim(Input::get('streat'));
		$q->select('_id','Name','CountryCode','Email','PhoneNo','AlternateCCode','AlternatePhoneNo','City','State','Country','Street1','Street2','UniqueNo','UserType','UserType','SSN','EnterOn');
		if ($city != '') {
			$q->where('City', 'like', "%$city->name%");
		}

		if ($state != '') {
			$q->where('State', 'like', "%$state->name%");
		}

		if ($country != '') {
			$q->where('Country', 'like', "%$country->name%");
		}

		if ($streat != '') {
			$q->where('Street1', 'like', "%$streat%")
				->orwhere('Street2', 'like', '%streat%');
		}

		if ($search_value != '') {
			$q->where('Name', 'like', "%$search_value%")
				->orWhere('Email', 'like', "%$search_value%")
				->orWhere('UniqueNo', 'like', "%$search_value%");

		}

		$info = $q->whereIn('UserType', ['requester', 'both'])->get();

		$filename = time() . '.xls';
		$export_xls = new export_xls();
		$export_xls->ExportXLS($filename);

		$header[] = "S.No.";
		$header[] = "Requester Name";
		$header[] = "Email";
		$header[] = "CountryCode";
		$header[] = "PhoneNo";
		$header[] = "AlternateCCode";
		$header[] = "AlternatePhoneNo";
		$header[] = "City";
		$header[] = "State";
		$header[] = "Country";
		$header[] = "Street1";
		$header[] = "Street2";
		$header[] = "Unit #";
		$header[] = "UserType";
		$header[] = "SSN No.";
		$header[] = "registration date";

		$export_xls->addHeader($header);

		foreach ($info as $key => $val) {

			$row = array();
			$row[] = array(++$key,
				$val['Name'],
				$val['Email'],
				$val['CountryCode'],
				$val['PhoneNo'],
				$val['AlternateCCode'],
				$val['AlternatePhoneNo'],
				$val['City'],
				$val['State'],
				$val['Country'],
				$val['Street1'],
				$val['Street2'],
				$val['UniqueNo'],
				$val['UserType'],
				$val['SSN'],
				show_date($val['EnterOn']),
			);
			$export_xls->addRow($row);
		}

		$export_xls->sendFile();

	}

	public function assign_transporter(Request $request) {

		$responce = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		$obj = json_decode($request->get('tpid'));

		if (!empty($obj)) {
			$where = array("ProductList" => array('$elemMatch' => array('_id' => $request->get('product_id'), 'status' => 'purchased'),
			),
			);

			$deliveryInfo = Deliveryrequest::where(array('_id' => $request->get('request_id')))
				->where($where)->select('ProductList.$', 'RequesterId', 'Status', 'payment_status', 'DeliveryFullAddress', 'PackageNumber', 'ProductTitle', 'TotalCost', 'StripeChargeId', 'PickupAddress', 'PackageId', 'shippingCost', 'PaymentStatus', 'StripeChargeId2')->first();

			if (count($deliveryInfo) > 0) {

				if ($deliveryInfo->PaymentStatus != 'paid') {

					if (!empty(trim($deliveryInfo->StripeChargeId))) {
						$res = RSStripe::retrive_amount($deliveryInfo->StripeChargeId);
						if (!isset($res['id'])) {

							$res = RSStripe::find_charge($deliveryInfo->StripeChargeId);
							if ($res['captured'] != 1) {
								$responce['msg'] = $res;
								return json_encode($responce);
								die;
							}

						}
					}
					if (!empty(trim($deliveryInfo->StripeChargeId2))) {
						$res = RSStripe::retrive_amount($deliveryInfo->StripeChargeId2);
						if (!isset($res['id'])) {

							$res = RSStripe::find_charge($deliveryInfo->StripeChargeId2);
							if ($res['captured'] != 1) {
								$responce['msg'] = $res;
								return json_encode($responce);
								die;
							}
						}
					}

					Transaction::insert([
						"SendById" => $deliveryInfo->RequesterId,
						"SendByName" => $deliveryInfo->RequesterName,
						"SendToId" => "aquantuo",
						"RecieveByName" => "Aquantuo",
						"Description" => sprintf("Amount deposited against delivery request for %s, PackageId: %u to %s", $deliveryInfo->ProductTitle, $deliveryInfo->PackageNumber, $deliveryInfo->DeliveryFullAddress
						),
						"Credit" => $deliveryInfo->TotalCost,
						"Debit" => "",
						"Status" => "paid",
						"TransactionType" => "buy_for_me",
						"EnterOn" => new MongoDate(),
					]);

					$deliveryInfo->PaymentStatus = 'paid';
					$deliveryInfo->save();
				}

				$updation = Deliveryrequest::where(array('_id' => $request->get('request_id')))
					->where($where)
					->update([
						'ProductList.$.tpid' => (string) $obj->id,
						'ProductList.$.tpName' => $obj->name,
						'ProductList.$.status' => 'assign',
						'UpdateOn' => new MongoDate(),
					]);

				/* Activity Log */
				$insertactivity = [
					'request_id' => $request->get('request_id'),
					'request_type' => $deliveryInfo->RequestType,
					'PackageNumber' => $deliveryInfo->PackageNumber,
					'item_id' => $request->get('product_id'),
					'package_id' => $deliveryInfo->ProductList[0]['package_id'],
					'item_name' => $deliveryInfo->ProductList[0]['product_name'],
					'tpid' => (string) $obj->id,
					'tpName' => $obj->name,
					'log_type' => 'request',
					'message' => 'Transporter has been assigned.',
					'status' => 'assign',
					'action_user_id' => Auth::user()->_id,
					'EnterOn' => new MongoDate(),
				];

				Activitylog::insert($insertactivity);

				if ($updation) {
					Reqhelper::update_status($deliveryInfo->_id);

					$count = Deliveryrequest::where(array('_id' => $request->get('request_id')))
						->where(array("ProductList" => array('$elemMatch' => array('status' => 'ready'))))
						->count();

					$responce = [
						'success' => 1,
						'msg' => 'Transporter has been assigned.',
						'count' => $count,
					];
				}

				$user_data = User::where(array('_id' => $deliveryInfo->RequesterId))->first();
				$transporter_data = User::where(array('_id' => $obj->id))->first();

				if (count($user_data) > 0 && count($transporter_data) > 0) {
					//echo 'checking'; die;
					foreach ($deliveryInfo->ProductList as $key) {
						$productName = $key['product_name'];
					}
					//notification to user

					/*$Notification = new Notify();
						                    $Notification->setValue('title', trans('lang.ONLINE_ASSIGN_TP_TITLE'));

						                    $Notification->setValue('message', sprintf(trans('lang.ONLINE_ASSIGN_TP_USER'), $deliveryInfo->ProductList[0]['product_name'], $deliveryInfo->ProductList[0]['package_id']));
						                    $Notification->setValue('type', 'online_transporter_assigned_for_user');
						                    $Notification->setValue('location', 'online_transporter_assigned_for_user');
						                    $Notification->setValue('locationkey', $request->get('request_id'));
						                    $Notification->setValue('itemId', $request->get('product_id'));
						                    $Notification->add_user($user_data->NotificationId, $user_data->DeviceType);
					*/

					$cron_noti = [
						"title" => trans('lang.ONLINE_ASSIGN_TP_TITLE'),
						"message" => sprintf(trans('lang.ONLINE_ASSIGN_TP_USER'), $deliveryInfo->ProductList[0]['product_name'], $deliveryInfo->ProductList[0]['package_id']),
						"type" => 'online_transporter_assigned_for_user',
						"locationkey" => $request->get('request_id'),
						"NotificationId" => $user_data->NotificationId,
						"DeviceType" => $user_data->DeviceType,
						"itemId" => $request->get('product_id'),
						'status' => 'ready',
						'by_mean' => 'notification',
					];
					SendMail::insert($cron_noti);

					//Mail to user

					/*send_mail('57daa7fa7ac6f6a3218b4567', [
						                    "to" => $user_data->Email,
						                    "replace" => [
						                    "[USERNAME]" => $user_data->Name,
						                    "[PACKAGETITLE]" => $productName,
						                    "[TRANSPORTER]" => $transporter_data->Name,
						                    "[PACKAGEID]" => $deliveryInfo->PackageId,
						                    "[SOURCE]" => $deliveryInfo->PickupAddress,
						                    "[DESTINATION]" => $deliveryInfo->DeliveryFullAddress,
						                    "[PACKAGENUMBER]" => $deliveryInfo->PackageNumber,
						                    "[SHIPPINGCOST]" => $deliveryInfo->shippingCost,
						                    "[TOTALCOST]" => $deliveryInfo->TotalCost,
						                    ],
						                    ]
					*/

					$cron_mail = [
						"USERNAME" => ucfirst($user_data->Name),
						"PACKAGETITLE" => ucfirst($productName),
						"TRANSPORTER" => $transporter_data->Name,
						"PACKAGEID" => $deliveryInfo->PackageId,
						"SOURCE" => $deliveryInfo->PickupAddress,
						"DESTINATION" => $deliveryInfo->DeliveryFullAddress,
						"PACKAGENUMBER" => $deliveryInfo->PackageNumber,
						"SHIPPINGCOST" => $deliveryInfo->shippingCost,
						"TOTALCOST" => $deliveryInfo->TotalCost,
						'email_id' => '57daa7fa7ac6f6a3218b4567',
						'email' => $user_data->Email,
						'status' => 'ready',
					];

					SendMail::insert($cron_mail);

					//notification to transporter

					/*$Notification = new Notify();
						                    $Notification->setValue('title', trans('lang.ONLINE_ASSIGN_TP_TITLE'));
						                    $Notification->setValue('message', sprintf(trans('lang.ONLINE_ASSIGN_TP_TRP'), $deliveryInfo->ProductList[0]['product_name'], $deliveryInfo->ProductList[0]['package_id']));

						                    $Notification->setValue('type', 'online_transporter_assigned');
						                    $Notification->setValue('location', 'online_transporter_assigned');
						                    $Notification->setValue('locationkey', $request->get('request_id'));
						                    $Notification->setValue('itemId', $request->get('product_id'));
						                    $Notification->add_user($transporter_data->NotificationId, $transporter_data->DeviceType);
						                    $Notification->fire();
					*/
					$cron_noti2 = [
						"title" => trans('lang.ONLINE_ASSIGN_TP_TITLE'),
						"message" => sprintf(trans('lang.ONLINE_ASSIGN_TP_TRP'), $deliveryInfo->ProductList[0]['product_name'], $deliveryInfo->ProductList[0]['package_id']),
						"type" => 'online_transporter_assigned',
						"locationkey" => $request->get('request_id'),
						"NotificationId" => $transporter_data->NotificationId,
						"DeviceType" => $transporter_data->DeviceType,
						'itemId' => $request->get('product_id'),
						'status' => 'ready',
						'by_mean' => 'notification',
					];
					SendMail::insert($cron_noti2);

					//Mail to transporter

					/*send_mail('5875d4e17ac6f63e038b4567', [
						                    "to" => $transporter_data->Email,
						                    "replace" => [
						                    "[USERNAME]" => $transporter_data->Name,
						                    "[PACKAGETITLE]" => ucfirst($productName),
						                    "[PRICE]" => number_format($deliveryInfo->ProductList[0]['price'], 2),
						                    "[INSURANCE]" => ucfirst($deliveryInfo->ProductList[0]['insurance_status']),
						                    "[PACKAGEID]" => $deliveryInfo->PackageId,
						                    "[BY]" => $user_data->Name,
						                    "[SOURCE]" => $deliveryInfo->PickupAddress,
						                    "[DESTINATION]" => $deliveryInfo->DeliveryFullAddress,
						                    "[PACKAGENUMBER]" => $deliveryInfo->PackageNumber,
						                    "[SHIPPINGCOST]" => $deliveryInfo->shippingCost,
						                    "[TOTALCOST]" => $deliveryInfo->TotalCost,

						                    ],
						                    ]
					*/

					$cron_mail2 = [
						"USERNAME" => ucfirst($transporter_data->Name),
						"PACKAGETITLE" => ucfirst($productName),
						"PRICE" => number_format($deliveryInfo->ProductList[0]['price'], 2),
						"INSURANCE" => ucfirst($deliveryInfo->ProductList[0]['insurance_status']),
						"PACKAGEID" => $deliveryInfo->PackageId,
						"BY" => $user_data->Name,
						"SOURCE" => $deliveryInfo->PickupAddress,
						"DESTINATION" => $deliveryInfo->DeliveryFullAddress,
						"PACKAGENUMBER" => $deliveryInfo->PackageNumber,
						"SHIPPINGCOST" => $deliveryInfo->shippingCost,
						"TOTALCOST" => $deliveryInfo->TotalCost,
						'email_id' => '5875d4e17ac6f63e038b4567',
						'email' => $transporter_data->Email,
						'status' => 'ready',
					];

					SendMail::insert($cron_mail2);

					$insNotification = array(
						array(
							"NotificationTitle" => trans('lang.ONLINE_ASSIGN_TP_TITLE'),
							"NotificationMessage" => sprintf(trans('lang.ONLINE_ASSIGN_TP_USER'), $deliveryInfo->ProductList[0]['product_name'], $deliveryInfo->ProductList[0]['package_id']),
							"NotificationType" => "transporter_assigned.",
							"NotificationUserId" => array(new MongoId($user_data->_id)),
							"Date" => new MongoDate(),
							"GroupTo" => "User",
						),
						array(
							"NotificationTitle" => trans('lang.ONLINE_ASSIGN_TP_TITLE'),
							"NotificationMessage" => sprintf(trans('lang.ONLINE_ASSIGN_TP_ADMIN'), $user_data->Name, $productName),
							"NotificationUserId" => array(),
							"NotificationReadStatus" => 0,
							"location" => "request_detail",
							"locationkey" => (string) $request->get('request_id'),
							"Date" => new MongoDate(),
							"GroupTo" => "Admin",
						),
						array(
							"NotificationTitle" => trans('lang.ONLINE_ASSIGN_TP_TITLE'),
							"NotificationMessage" => sprintf(trans('lang.ONLINE_ASSIGN_TP_TRP'), $deliveryInfo->ProductList[0]['product_name'], $deliveryInfo->ProductList[0]['package_id']),
							"NotificationType" => "transporter_assigned.",
							"NotificationUserId" => array(new MongoId($transporter_data->_id)),
							"Date" => new MongoDate(),
							"GroupTo" => "User",
						),
					);

					Notification::Insert($insNotification);

				}

			}
		}

		return json_encode($responce);
	}

	public function promocode() {
		$data['title'] = "Promocode";
		$data['paginationurl'] = "pagination/promocode";
		$data['orderType'] = "Desc";
		$data['orderby'] = "UserId";
		$data['postvalue'] = "&code=" . Input::get('code') . "&validfrom=" . Input::get('validfrom') . "&validtill=" . Input::get('validtill');

		return view('Admin::list.promocode', $data);
	}

	public function add_promocode() {
		return view('Admin::add.promocode');
	}

	public function post_promocode(Request $request) {

		$a = Input::get('MaxUses');
		$validate = Validator::make($request->all(), array(
			'title' => 'required',
			'code' => 'required|AlphaNum|',
			'minimumorderprice' => 'required|Integer',
			'maximumDiscount' => 'required|Integer',
			'DiscountAmount' => (Input::get('DiscountType') == 'fixed') ? 'required|Integer' : 'required|Integer|max:100',
			'validfrom' => 'required',
			'validtill' => 'required',
			'MaxUses' => 'required|Integer',
			'MaxUsesPerPerson' => "required|Integer|max:$a"));
		if ($validate->fails()) {
			return Redirect::to('admin/add-promocode')->withErrors($validate)->withInput();
		} else {

			$array = array(
				'Title' => ucfirst(Input::get('title')),
				'Code' => strtoupper(Input::get('code')),
				'MinimumOrderPrice' => Input::get('minimumorderprice'),
				'MaximumDiscount' => Input::get('maximumDiscount'),
				'DiscountAmount' => Input::get('DiscountAmount'),
				'DiscountType' => Input::get('DiscountType'),
				'ValidFrom' => get_utc_time(Input::get('validfrom') . ' 00:00:00', false, 'M d, Y H:i:s'),
				'ValidTill' => get_utc_time(Input::get('validtill') . ' 23:59:59', false, 'M d, Y H:i:s'),
				'MaxUses' => (int) Input::get('MaxUses'),
				'UsesCount' => 0,
				'MaxUsesPerPerson' => (int) Input::get('MaxUsesPerPerson'),
				'Status' => 'active');

			$lastId = Promocode::insert($array);

			return Redirect::to('admin/promocode')->withSuccess('Promocode added successfully.');
		}
	}

	public function get_mongodate($date, $format = 'M d, Y H:i:s') {
		$date = DateTime::createFromFormat($format, $date);
		if (is_object($date)) {
			return new MongoDate(strtotime($date->format('d-m-Y H:i:s')));
		}
	}

	public function edit_promocode_page($id) {

		$data['promocode'] = Promocode::where('_id', '=', $id)->select()->first();
		if (count($data['promocode']) > 0) {
			return view('Admin::edit.promocode_edit', $data);
		} else {
			return redirect::back();
		}

	}

	public function promocode_detail($id) {

		$data['user'] = User::select('_id', 'FirstName', 'LastName')->get();
		$data['old_user'] = Promocode::where(['_id' => $id])->select('assign_promocode_users')->first();
		return view('Admin::list.promocode_detail', $data);

	}
	public function post_promocode_detail($id) {
		//return Redirect::to('admin/promocode-detail/'.$id)->with('fail', 'Please choose at least one user.');
		if (!empty(Input::get('promocodelistbox'))) {

			Promocode::raw(function ($collection) {
				return $collection->update(
					[
						'assign_promocode_users' => [
							'$elemMatch' => ['$in' => Input::get('promocodelistbox')],
						],
					],
					[
						'$pullAll' => [
							'assign_promocode_users' => Input::get('promocodelistbox'),
						],
					],
					[
						'multi' => true,
					]
				);
			});
		}

		$update['assign_promocode_users'] = Input::get('promocodelistbox');
		Promocode::where('_id', '=', $id)->update($update);
		return Redirect::to('admin/promocode')->withSuccess('Promocode has benn assign successfully.');
	}

	public function update_promocode($id, Request $request) {
		$a = Input::get('MaxUses');
		$validate = Validator::make($request->all(), array('title' => 'required', 'code' => 'required|AlphaNum|', 'minimumorderprice' => 'required|Integer', 'maximumDiscount' => 'required|Integer', 'DiscountAmount' => 'required|Integer', 'validfrom' => 'required', 'validtill' => 'required', 'MaxUses' => 'required|Integer', 'MaxUsesPerPerson' => "required|Integer|max:$a"));
		if ($validate->fails()) {
			return Redirect::to("admin/edit-promocode/{$id}")->withErrors($validate)->withInput();
		} else {
			$Title = ucfirst(Input::get('title'));
			$Code = strtoupper(Input::get('code'));
			$MinimumOrderPrice = Input::get('minimumorderprice');
			$MaximumDiscount = Input::get('maximumDiscount');
			$DiscountAmount = Input::get('DiscountAmount');
			$DiscountType = Input::get('DiscountType');
			$ValidFrom = get_utc_time(Input::get('validfrom') . ' 00:00:00', false, 'M d, Y H:i:s');
			$ValidTill = get_utc_time(Input::get('validtill') . ' 23:59:59', false, 'M d, Y H:i:s');
			$MaxUses = (int) Input::get('MaxUses');
			$MaxUsesPerPerson = (int) Input::get('MaxUsesPerPerson');
			$save = PROMOCODE::find($id);
			$save->Title = $Title;
			$save->Code = $Code;
			$save->MinimumOrderPrice = $MinimumOrderPrice;
			$save->MaximumDiscount = $MaximumDiscount;
			$save->DiscountAmount = $DiscountAmount;
			$save->DiscountType = $DiscountType;
			$save->ValidFrom = $ValidFrom;
			$save->ValidTill = $ValidTill;
			$save->MaxUses = $MaxUses;
			$save->MaxUsesPerPerson = $MaxUsesPerPerson;

			$save->save();
			return Redirect::to("admin/promocode")->withSuccess('promocode updated successfully.');
		}
	}
	public function address() {
		$data['setting'] = Setting::select()->first();
		return view('Admin::list.address', $data);
	}

	public function update_address(Request $request) {
		$validation = Validator::make($request->all(), [
			'address' => 'required',
			'city' => 'required',
			'state' => 'required',
			'country' => 'required',
			'zipcode' => 'required',
			'phone' => 'required',
			'cc' => 'required',
		]);
		if ($validation->fails()) {
			return Redirect::to("admin/address")->withErrors($validation)->withInput(Input::get());

		} else {
			$latlong = util::getLatLong(Input::get('address') .
				Input::get('city') .
				Input::get('state') .
				Input::get('country') .
				Input::get('zipcode')
			);
			if ($latlong) {
				$update['AqAddress'] = Input::get('address');
				$update['AqCity'] = Input::get('city');
				$update['AqState'] = Input::get('state');
				$update['AqCountry'] = Input::get('country');
				$update['AqZipcode'] = Input::get('zipcode');
				$update['AqLatlong'] = [floatval($latlong['lng']),
					floatval($latlong['lat'])];
				$update['Aqphone'] = Input::get('phone');
				$update['Aqcc'] = Input::get('cc');

				Setting::where('_id', '=', '563b0e31e4b03271a097e1ca')->update($update);

				unset($update['AqLatlong']);
				$update['AqLatLong'] = [floatval($latlong['lng']),
					floatval($latlong['lat'])];

				User::where('RequesterStatus', '!=', 'test')->update($update);
				return Redirect::to("admin/address")->withSuccess('Your aquantuo address has been updated successfully.');
			} else {
				return Redirect::to("admin/address")->with('fail', 'Your aquantuo address has incorrect.');
			}

		}
	}

	public function on_line_purchases() {
		$data['country'] = CityStateCountry::where(['type' => 'Country', 'Status' => 'Active'])
			->get(['_id', 'Content', 'state_available']);

		$data['category'] = Category::where(['type' => 'Category', 'Status' => 'Active'])->whereIn('TravelMode', ['ship', 'air'])->orderBy('Content', 'asc')->get(['TravelMode', 'Content']);
		$field = ['country', 'state', 'city', 'address_line_1', 'address_line_2', 'zipcode', 'lat', 'lng'];
		$data['address'] = Address::where('user_id', '=', Auth::user()->_id)->get($field);

		$data['additem'] = Additem::where('user_id', '=', Auth::user()->_id)->where(array('request_type' => 'online'))->limit(5)->get();

		return view('Admin::Buy.on-line-purchases', $data);
	}

	public function add_online_item(Request $request) {
		$response = array("success" => 0, "msg" => "Oops! Something went wrong.");
		$validation = Validator::make($request->all(), [
			'item_name' => 'required',
			'purchase_from' => 'required',
			'length' => 'required',
			'width' => 'required',
			'height' => 'required',
			'weight' => 'required',
			'item_price' => 'required',
			'category' => 'required',
			'description' => 'required',

		]);
		if ($validation->fails()) {
			$response['error'] = $validation->errors();
			return response()->json($response);
		} else {
			$lhwunit = 'inches';
			$weightunit = 'lbs';
			if (Input::get('measurement_unit') == 'cm_kg') {
				$lhwunit = 'cm';
				$weightunit = 'kg';
			}

			$json_decode_category = json_decode(Input::get('category'));

			$insert = [
				'item_image' => '',
				'image' => '',
				'item_name' => Input::get('item_name'),
				'product_name' => Input::get('item_name'),
				'add_item_url' => Input::get('purchase_from'),
				'url' => Input::get('purchase_from'),
				'measurement_unit' => Input::get('measurement_unit'),
				'weight_unit' => (Input::get('measurement_unit') == 'cm_kg') ? 'kg' : 'lbs',
				'length' => Input::get('length'),
				'width' => Input::get('width'),
				'height' => Input::get('height'),
				'weight' => Input::get('weight'),
				'item_price' => Input::get('item_price'),
				'price' => Input::get('item_price'),
				'quentity' => Input::get('quentity'),
				'qty' => Input::get('quentity'),
				'travel_mode' => Input::get('travel_mode'),
				'travelMode' => Input::get('travel_mode'),
				'category' => @$json_decode_category->name,
				'category_id' => @$json_decode_category->id,
				'categoryid' => @$json_decode_category->id,
				'description' => Input::get('description'),
				'insurance' => Input::get('insurance'),
				'insurance_status' => Input::get('insurance'),
				'request_type' => 'online',
				'user_id' => Auth::user()->_id,
			];

			// Calculate value
			$requesthelper = new Requesthelper(
				[
					"needInsurance" => Input::get('insurance'),
					"productQty" => (Input::get('quentity') < 1) ? 1 : Input::get('quentity'),
					"productCost" => $insert['price'],
					"productWidth" => $insert['width'],
					"productWidthUnit" => $lhwunit,
					"productHeight" => $insert['height'],
					"productHeightUnit" => $lhwunit,
					"productLength" => $insert['length'],
					"productLengthUnit" => $lhwunit,
					"productWeight" => $insert['weight'],
					"productWeightUnit" => $weightunit,
					"productCategory" => @$json_decode_category->name,
					'productCategoryId' => @$json_decode_category->id,
					"distance" => 0,
					"travelMode" => $insert['travelMode'],
				]
			);

			$requesthelper->calculate();
			$calculationinfo = $requesthelper->get_information();

			if (isset($calculationinfo->error)) {

				$response['msg'] = $calculationinfo->error;
				return response()->json($response);

			}

			if (Input::file('item_image')) {
				$extension = Input::file('item_image')->getClientOriginalExtension(); // getting image extension

				$fileName = time() . rand(100, 999) . '.' . $extension; // Rename img image
				if (Input::file('item_image')->move(BASEURL_FILE . 'package/', $fileName)) {
					$insert['image'] = "package/" . $fileName;
				}
			}
			$data['items'] = [];

			if (!empty(trim(Input::get('request_id')))) {
				// This will execute on edit reqeust
				$insert['_id'] = (String) new MongoId();
				$data['items'][] = $insert;

				foreach (Auth::user()->request_id as $key) {
					$data['items'][] = $key;
				}
				session::put(trim(Input::get('request_id')), $data['items']);
			} else {
				$response = Additem::insert($insert);
				$data['items'] = Additem::where(['user_id' => Auth::user()->_id, 'request_type' => 'online'])->get();
			}

			$response = array("success" => 1, "msg" => "Item has been added successfully.");
			$view = view('Admin::Buy.ajax.edit_online_item', $data);

			$response['html'] = $view->render();

		}
		return response()->json($response);
	}

	/*public function add_address(Request $request)
		    {
		    $validation = Validator::make($request->all(), [
		    'address_line_1'     => 'required',
		    'country'            => 'required',
		    'city'               => 'required'

		    ]);
		    if ($validation->fails())
		    {

		    $validation->errors()->add('field', trans('Something is wrong'));
		    return $validation->errors();
		    }
		    else
		    {

		    $addlat = true;
		    $lat = floatval(Input::get('lat'));
		    $lng = floatval(Input::get('lng'));
		    $json_decode_country      =   json_decode(Input::get('country'));
		    $json_decode_state      =   json_decode(Input::get('state'));
		    $json_decode_city      =   json_decode(Input::get('city'));

		    if(empty($lng)){
		    $addlat = Utility::getLatLong(Input::get('address_line_1'),
		    Input::get('address_line_2'),
		    $json_decode_city->name,
		    @$json_decode_state->name,
		    $json_decode_country->name
		    );
		    if($addlat != false)
		    {
		    $lat = $addlat['lat'];
		    $lng = $addlat['lng'];
		    }
		    }

		    if($addlat == false)
		    {
		    $response = array("success" => 0, "msg" => "We are unable to find your location. Please  correct it.");
		    } else {

		    $insert = [
		    'country'             => $json_decode_country->name,
		    'country_id'        => $json_decode_country->id,
		    'state'             => @$json_decode_state ->name,
		    'state_id'            => @$json_decode_state ->id,
		    'city'                 => $json_decode_city->name,
		    'city_id'             => $json_decode_city->id,
		    'state_available'     => $json_decode_country->state_available,
		    'address_line_1'       => Input::get('address_line_1'),
		    'address_line_2'       => Input::get('address_line_2'),
		    'zipcode'             => Input::get('zipcode'),
		    'user_id'           => Auth::user()->_id,
		    'lat'                => $lat,
		    'lng'                => $lng
		    ];

		    Address::insert($insert);
		    $field = ['country','state','city','address_line_1','address_line_2','zipcode','lat','lng','state_available'];
		    $address = Address::where(['user_id' => Auth::user()->_id])->get();
		    $response = array("success" => 1, "msg" => "Address has been added successfully.",'address_html' => '<option value="">Select address</option>');

		    foreach($address as $key) {
		    $response['address_html'] .= "<option value='".json_encode($key)."'>".ucfirst($key->address_line_1)." ".$key->city." ".$key->state." ". $key->country." ".$key->zipcode."</option>";
		    }

		    $view = view('Admin::Buy.ajax.add_address',['address' => $address] );
		    $response['html'] = $view->render();
		    }

		    }
		    return response()->json($response);
	*/

	public function edit_online_item(Request $request) {
		$response = array("success" => 0, "msg" => "Oops! Something went wrong.");

		$online_item = [];
		if (empty(trim(Input::get('request_id')))) {
			$online_item[Input::get('item_id')] = Additem::where([
				'_id' => Input::get('item_id'),
				'user_id' => Auth::user()->_id,
				'request_type' => 'online',
			])->select('image')->first();
		} else {
			$product_list = Auth::user()->request_id;
			foreach ($product_list as $key => $val) {
				$online_item[(String) $val['_id']] = $val;
			}
		}

		if (isset($online_item[Input::get('item_id')])) {

			$validation = Validator::make($request->all(), [
				'item_name' => 'required',
				'purchase_from' => 'required',
				'measurement_unit' => 'required',
				'length' => 'required',
				'width' => 'required',
				'height' => 'required',
				'weight' => 'required',
				'item_price' => 'required',
				'quentity' => 'required',
				'travel_mode' => 'required',
				'category' => 'required',
				'description' => 'required',
				'insurance' => 'required',
			]);

			if ($validation->fails()) {
				$validation->errors()->add('field', trans('Something is wrong'));
				return $validation->errors();
			} else {
				$json_decode_category = json_decode(Input::get('category'));
				$lhwunit = 'inches';
				$weightunit = 'lbs';
				if (Input::get('measurement_unit') == 'cm_kg') {
					$lhwunit = 'cm';
					$weightunit = 'kg';
				}

				$data['items'] = [];
				$update = [
					'item_image' => '',
					'item_name' => Input::get('item_name'),
					'product_name' => Input::get('item_name'),
					'add_item_url' => Input::get('purchase_from'),
					'url' => Input::get('purchase_from'),
					'measurement_unit' => Input::get('measurement_unit'),
					'weight_unit' => (Input::get('measurement_unit') == 'cm_kg') ? 'kg' : 'lbs',
					'length' => Input::get('length'),
					'width' => Input::get('width'),
					'height' => Input::get('height'),
					'weight' => Input::get('weight'),
					'item_price' => Input::get('item_price'),
					'price' => Input::get('item_price'),
					'quentity' => Input::get('quentity'),
					'qty' => Input::get('quentity'),
					'travel_mode' => Input::get('travel_mode'),
					'travelMode' => Input::get('travel_mode'),
					'category' => @$json_decode_category->name,
					'category_id' => @$json_decode_category->id,
					'categoryid' => @$json_decode_category->id,
					'description' => Input::get('description'),
					'insurance' => Input::get('insurance'),
					'insurance_status' => Input::get('insurance'),
					'request_type' => 'online',

				];

				// Calculate value
				$requesthelper = new Requesthelper(
					[
						"needInsurance" => Input::get('insurance'),
						"productQty" => (Input::get('quentity') < 1) ? 1 : Input::get('quentity'),
						"productCost" => $update['price'],
						"productWidth" => $update['width'],
						"productWidthUnit" => $lhwunit,
						"productHeight" => $update['height'],
						"productHeightUnit" => $lhwunit,
						"productLength" => $update['length'],
						"productLengthUnit" => $lhwunit,
						"productWeight" => $update['weight'],
						"productWeightUnit" => $weightunit,
						"productCategory" => @$json_decode_category->name,
						'productCategoryId' => @$json_decode_category->id,
						"distance" => 0,
						"travelMode" => $update['travelMode'],
					]
				);

				$requesthelper->calculate();
				$calculationinfo = $requesthelper->get_information();

				if (isset($calculationinfo->error)) {

					$response['msg'] = $calculationinfo->error;
					return response()->json($response);

				}

				if (Input::file('item_image')) {
					$extension = Input::file('item_image')->getClientOriginalExtension(); // getting image extension
					$fileName = time() . rand(100, 999) . '.' . $extension; // Rename img image
					if (Input::file('item_image')->move(BASEURL_FILE . 'package/', $fileName)) {

						if (isset($online_item[Input::get('item_id')]['image']) && ($online_item[Input::get('item_id')]['image']) != "") {
							if (file_exists(BASEURL_FILE . $online_item[Input::get('item_id')]['image'])) {
								unlink(BASEURL_FILE . $online_item[Input::get('item_id')]['image']);
							}
						}

						$update['image'] = "package/" . $fileName;
					}
				}
				if (empty(trim(Input::get('request_id')))) {
					$result = Additem::where(array('_id' => Input::get('item_id'), 'user_id' => Auth::user()->_id, 'request_type' => 'online'))->update($update);

					$data['items'] = Additem::where(['user_id' => Auth::user()->_id, 'request_type' => 'online'])->get();

				} else {
					$update['_id'] = Input::get('item_id');
					$online_item[Input::get('item_id')] = $update;
					foreach ($online_item as $okey => $oval) {
						$data['items'][] = $oval;
					}

					session::put(trim(Input::get('request_id')), $data['items']);
				}

				$response = array("success" => 1, "msg" => "Item has been updated successfully.");
				$view = view('User::Buy.ajax.edit_online_item', $data);
				$response['edit_html'] = $view->render();
			}
		}
		return response()->json($response);
	}

	public function delete_item($id, $function) {

		$responce = array('success' => 0, 'msg' => 'Fail to delete request.');

		if ($function == 'DeleteItem') {
			if (!empty(trim(Input::get('request_id'))) && session()->has(trim(Input::get('request_id')))) {
				$product_list = [];
				if (is_array(session()->get(trim(Input::get('request_id'))))) {
					foreach (session()->get(trim(Input::get('request_id'))) as $key => $val) {
						$product_list[(String) $val['_id']] = $val;
					}
					if (isset($product_list[$id])) {
						unset($product_list[$id]);
						session()->put(trim(Input::get('request_id')), []);
						foreach ($product_list as $key) {
							session()->push(trim(Input::get('request_id')), $key);
						}
						$responce['success'] = 1;
						$responce['msg'] = 'Item has been removed successfully.';
					}
				}

			} else {
				$item = Additem::find($id);

				if (count($item) > 0) {
					if (!empty(trim($item->image))) {
						if (file_exists(BASEURL_FILE . $item->image)) {
							unlink(BASEURL_FILE . $item->image);
						}
					}
					$item->delete();
					$responce['success'] = 1;
					$responce['msg'] = 'Item has been removed successfully.';
				}
			}
		}

		echo json_encode($responce);
	}

	// calculation part

	public function online_purchase_calculation() {

		$aqlat = Auth::user()->AqLat;
		$aqlong = Auth::user()->AqLong;

		$data = ['shippingcost' => 0, 'insurance' => 0, 'ghana_total_amount' => 0,
			'total_amount' => 0, 'error' => [], 'formated_text' => '', 'distance_in_mile' => 0];

		$address = json_decode(Input::get('address'));
		//print_r($address);die;
		$response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		$response['total_amount'] = 0;
		$response['ghana_total_amount'] = 0;
		$response['shipping_cost'] = 0;

		if (empty(trim(Input::get('request_id')))) {
			$data['product'] = Additem::where(array('user_id' => Auth::user()->_id, 'request_type' => 'online'))->get();
		} else {
			$data['product'] = Auth::user()->request_id;
		}

		if (count($data['product']) > 0 && count($address) > 0) {

			$data['distance'] = Utility::GetDrivingDistance($aqlat, $aqlong, $address->lat, $address->lng);

			if ($data['distance']->distance > 0) {

				foreach ($data['product'] as $value) {
					$lhwunit = 'cm';
					$weightunit = 'kg';
					if ($value['weight_unit'] == 'lbs') {
						$lhwunit = 'inches';
						$weightunit = 'lbs';
					}

					$requesthelper = new Requesthelper(
						[
							"needInsurance" => $value['insurance_status'],
							"productQty" => ($value['qty'] < 1) ? 1 : $value['qty'],
							"productCost" => $value['price'],
							"productWidth" => $value['width'],
							"productWidthUnit" => $lhwunit,
							"productHeight" => $value['height'],
							"productHeightUnit" => $lhwunit,
							"productLength" => $value['length'],
							"productLengthUnit" => $lhwunit,
							"productWeight" => $value['weight'],
							"productWeightUnit" => $weightunit,
							"productCategory" => @$value['category'],
							"productCategoryId" => @$value['categoryid'],
							"distance" => $data['distance']->distance,
							"travelMode" => $value['travelMode'],
						]
					);

					$requesthelper->calculate();
					$calculationinfo = $requesthelper->get_information();

					if (isset($calculationinfo->error)) {
						$data['error'][] = $calculationinfo->error;
					}
					$response['shipping_cost'] += $calculationinfo->shippingcost;
					$response['total_amount'] += $calculationinfo->shippingcost + $calculationinfo->insurance;
					$response['ghana_total_amount'] += $calculationinfo->costInCurrency;
					$response['distance'] = $calculationinfo->distance;
					$data['distance_in_mile'] = $calculationinfo->distance;

					$data['insurance'] = $calculationinfo->insurance + $data['insurance'];
					$data['shippingcost'] = $calculationinfo->shippingcost + $data['shippingcost'];
				}

				$currency = CityStateCountry::where(['CurrencyCode' => $userinfo->Default_Currency])->first();

				if (count($currency) > 0) {
					$response['ghana_total_amount'] = $response['total_amount'] * $currency->CurrencyRate;
					$data['ghana_total_amount'] = $response['ghana_total_amount'];
					$data['formated_text'] = $currency->FormatedText;
				}

			}

			if (!count($data['error']) > 0) {
				$response['success'] = 1;
			}

			$view = view('Admin::Buy.ajax.online_purchase', $data);

			$response['html'] = $view->render();

		}

		return json_encode($response);

	}

	public function post_online_purchase(Request $request) {

		$validation = Validator::make($request->all(), [
			'country_code' => 'required',
			'phone_number' => 'required',
			// 'desired_delivery_date'  => 'required'
		]);

		if ($validation->fails()) {

			$validation->errors()->add('field', trans('Something is wrong'));
			return $validation->errors();
		} else {

			$discount_in_percent = 0;
			$distance_in_mile = floatval(Input::get('distance'));
			$productlist = [];
			if (empty(trim(Input::get('request_id')))) {
				$productlist = Additem::where('user_id', '=', Auth::user()->_id)->where(array('request_type' => 'online'))->get();
			} else {
				$productlist = Auth::user()->request_id;
			}

			$DeliveryDate = '';
			if (!Input::get('desired_delivery_date') == '') {
				$Date = DateTime::createFromFormat('M d, Y H:i:s A', Input::get('desired_delivery_date'))->format('d-m-Y h:i:s a');
				$DeliveryDate = new MongoDate(strtotime($Date));
			}

			$json_decode_category = json_decode(Input::get('category'));
			$address = json_decode(Input::get('address'));

			$addlat = true;
			$lat = floatval(Input::get('lat'));
			$lng = floatval(Input::get('lng'));
			$json_decode_country = json_decode(Input::get('country'));
			$json_decode_state = json_decode(Input::get('state'));
			$json_decode_city = json_decode(Input::get('city'));

			$addlat = Utility::getLatLong(Input::get('address_line_1') . ' ' .
				Input::get('address_line_2') . ' ' .
				$json_decode_city->name . ' ' .
				@$json_decode_state->name . ' ' .
				$json_decode_country->name
			);

			if ($addlat != false) {
				$lat = $addlat['lat'];
				$lng = $addlat['lng'];
			}

			if ($addlat == false) {
				$response = array("success" => 0, "msg" => "We are unable to find your location. Please  correct it.");
			}

			$insert = [
				'RequesterId' => new MongoId(Session::get('UserId')),
				'RequesterName' => Session::get('Name'),
				'TransporterId' => '',
				'TransporterName' => '',
				'PackageNumber' => Utility::sequence('Request') . time(),
				'ProductTitle' => '',
				'RequestType' => 'online',
				'Status' => 'pending',
				'DeliveryFullAddress' => Input::get('address_line_1') . ', ' . Input::get('address_line_2') . ' ' . Input::get('city') . ' ' . Input::get('state') . ' ' . Input::get('country') . ' ' . Input::get('zipcode'),
				'DeliveryAddress' => Input::get('address_line_1'),
				'DeliveryCity' => $json_decode_city->name,
				'DeliveryState' => $json_decode_state->name,
				'DeliveryCountry' => $json_decode_country->name,
				'DeliveryPincode' => Input::get('zipcode'),
				'ReturnToAquantuo' => (Input::get('return_to_aq') == 'yes') ? true : false,
				'ReturnAddress' => Session::get('AqAddress'),
				'PaymentStatus' => '',
				'country_code' => '+' . Input::get('country_code'),
				'ReceiverMobileNo' => Input::get('phone_number'),
				'DeliveryDate' => $DeliveryDate,
				'PromoCode' => Input::get('promo_code'),
				'discount' => 0,
				'distance' => $distance_in_mile,
				'distanceType' => 'Miles',
				'shippingCost' => 0,
				'insurance' => 0,
				'TotalCost' => 0,
				'GhanaTotalCost' => 0,
				'PickupAddress' => '',
				"consolidate_item" => (Input::get('consolidate_check') == 'on') ? 'on' : 'off',
				'UpdateOn' => new MongoDate(),
				'EnterOn' => new MongoDate(),
				'PickupLatLong' => [floatval(Session::get('AqLong')), floatval(Session::get('AqLat'))],
				'DeliveryLatLong' => [floatval($lng), floatval($lat)],
			];

			foreach ($productlist as $k => $key) {
				$lhwunit = 'cm';
				$weightunit = 'kg';
				if ($key['weight_unit'] == 'lbs') {
					$lhwunit = 'inches';
					$weightunit = 'lbs';
				}
				$requesthelper = new Requesthelper(
					[
						"needInsurance" => $key['insurance_status'],
						"productQty" => ($key['qty'] < 1) ? 1 : $key['qty'],
						"productCost" => $key['price'],
						"productWidth" => $key['width'],
						"productWidthUnit" => $lhwunit,
						"productHeight" => $key['height'],
						"productHeightUnit" => $lhwunit,
						"productLength" => $key['length'],
						"productLengthUnit" => $lhwunit,
						"productWeight" => $key['weight'],
						"productWeightUnit" => $weightunit,
						"productCategory" => @$key['category'],
						"productCategoryId" => @$key['categoryid'],
						"distance" => $distance_in_mile,
						"travelMode" => $key['travelMode'],
					]
				);

				$requesthelper->calculate();
				$calculationinfo = $requesthelper->get_information();

				if (isset($calculationinfo->error)) {

					$response['msg'] = $calculationinfo->error;
					return response()->json($response);
					die();
				}

				$cost = $calculationinfo->insurance + $calculationinfo->shippingcost;

				$insert['shippingCost'] += $calculationinfo->shippingcost;
				$insert['insurance'] += $calculationinfo->insurance;
				$insert['GhanaTotalCost'] += $calculationinfo->costInCurrency;

				if (!empty(trim($insert["ProductTitle"]))) {
					$insert["ProductTitle"] = "{$insert['ProductTitle']}, ";
				}

				$insert["ProductTitle"] .= $key['product_name'];

				$insert["ProductList"][] = [
					'_id' => (string) new MongoId(),
					'marketid' => '',
					'marketname' => '',
					'tpid' => '',
					'tpName' => '',
					'package_id' => $insert['PackageNumber'] . $k,
					'product_name' => @$key['product_name'],
					'url' => @$key['url'],
					'description' => @$key['description'],
					'travelMode' => @$key['travelMode'],
					'weight' => @$key['weight'],
					'weight_unit' => $weightunit,
					'height' => @$key['height'],
					'heightUnit' => $lhwunit,
					'length' => @$key['length'],
					'lengthUnit' => $lhwunit,
					'width' => @$key['width'],
					'widthUnit' => $lhwunit,
					'category' => @$key['category'],
					'categoryid' => @$key['categoryid'],
					'price' => @$key['price'],
					'qty' => @$key['qty'],
					'insurance_status' => @$key['insurance_status'],
					'image' => @$key['image'],
					///'shipping_cost_by_user' => '',
					'status' => 'pending',
					'verify_code' => rand(1000, 9999),
					'txnid' => '',
					'shippingCost' => $calculationinfo->shippingcost,
					'insurance' => $calculationinfo->insurance,
					'discount' => 0,
					'aq_fee' => Reqhelper::get_aquantuo_fees($calculationinfo->shippingcost),
					'TransporterFeedbcak' => '',
					'TransporterRating' => '',
					'RequesterFeedbcak' => '',
					'RequesterRating' => '',
					'RejectBy' => '',
					'ReturnType' => '',
					'ReceiptImage' => '',
					'RejectTime' => '',
					'TrackingNumber' => '',
					'TransporterMessage' => '',
					'CancelDate' => '',
					'StripeChargeId' => '',
					'DeliveredDate' => '',
				];
			}

			$insert['TotalCost'] = (($insert['shippingCost'] + $insert['insurance']) - $insert['discount']);
			if ($insert['TotalCost'] <= 0) {
				$insert['Status'] = 'ready';
				foreach ($insert["ProductList"] as $key => $val) {
					$insert["ProductList"][$key]['status'] = 'ready';
				}
			}

			if (empty(trim(Input::get('request_id')))) {
				$response = array("success" => 1, "msg" => "Success! Request has been created successfully.");
				$response['reqid'] = (String) Deliveryrequest::insertGetId($insert);

				$PackageNumber = Deliveryrequest::where('_id', '=', $response['reqid'])->select('_id', 'PackageNumber')->first();
				/* Activity Log */
				$insertactivity = [
					'request_id' => $response['reqid'],
					'PackageNumber' => $PackageNumber,
					'request_type' => 'online',
					'item_id' => '',
					'package_id' => '',
					'item_name' => '',
					'log_type' => 'request',
					'message' => 'Package has been created.',
					'status' => 'pending',
					'action_user_id' => Auth::user()->_id,
					'EnterOn' => new MongoDate(),
				];

				Activitylog::insert($insertactivity);

				if (count($response['reqid']) > 0) {
					Additem::where('user_id', '=', Auth::user()->_id)
						->where(['request_type' => 'online'])->delete();
				}
				$admin = Setting::where(['_id' => '563b0e31e4b03271a097e1ca'])->first();
				$user_data = User::where(['_id' => Auth::user()->_id])->first();
				$request = Deliveryrequest::where(['_id' => $response['reqid']])->first();
				if (count($admin) > 0 && count($user_data)) {
					if (count($request) > 0) {
						if ($user_data->EmailStatus == "on") {
							send_mail('587750587ac6f6e61c8b4567', [
								"to" => $user_data->Email,
								"replace" => [
									"[USERNAME]" => $user_data->Name,
									"[PACKAGETITLE]" => ucfirst($request->ProductTitle),
									"[DESTINATION]" => $request->DeliveryFullAddress,
									"[SHIPPINGCOST]" => $request->ShippingCost,
									"[TOTALCOST]" => $request->TotalCost,

								],
							]
							);
						}

						send_mail('587751d47ac6f6e61c8b4568', [
							"to" => 'admin@aquantuo.com',
							"replace" => [
								"[USERNAME]" => $user_data->Name,
								"[DESTINATION]" => $request->DeliveryFullAddress,
								"[SHIPPINGCOST]" => $request->ShippingCost,
								"[TOTALCOST]" => $request->TotalCost,
								"[REQUEST]" => 'Online',
								"[EMAIL]" => $user_data->Email,
								"[TITLE]" => ucfirst($insert["ProductTitle"]),
								"[USERTYPE]" => $user_data->UserType,
								"[DEVICETYPE]" => $user_data->DeviceType,

							],
						]
						);
					}
				}

				Notification::insert([
					"NotificationTitle" => "Online request request created",
					"NotificationMessage" => sprintf('The online request titled:"%s", ID:%s has created.', ucfirst($request->ProductTitle), $request->PackageNumber),
					"NotificationUserId" => [],
					"NotificationReadStatus" => 0,
					"location" => "online",
					"locationkey" => (String) @$request->_id,
					"Date" => new MongoDate(),
					"GroupTo" => "Admin",
				]);

			} else {
				$response = array("success" => 1, "msg" => "Success! Request has been updated successfully.");
				$update = Deliveryrequest::where(['_id' => Input::get('request_id')])->update($insert);
				$response['reqid'] = Input::get('request_id');
				$admin = Setting::where(['_id' => '563b0e31e4b03271a097e1ca'])->first();
				$user_data = User::where(['_id' => Auth::user()->_id])->first();
				if ($update) {
					$request = Deliveryrequest::where(['_id' => Input::get('request_id')])->first();
					if (count($request) > 0 && count($user_data) > 0) {

						if ($user_data->EmailStatus == "on") {
							send_mail('58774ed77ac6f6881c8b4567', [
								"to" => $user_data->Email,
								"replace" => [
									"[USERNAME]" => $user_data->Name,
									"[PACKAGETITLE]" => ucfirst($request->ProductTitle),
									"[DESTINATION]" => $request->DeliveryFullAddress,
									"[SHIPPINGCOST]" => $request->ShippingCost,
									"[TOTALCOST]" => $request->TotalCost,

								],
							]
							);
						}

						send_mail('587751447ac6f6151d8b4567', [
							"to" => 'admin@aquantuo.com',
							"replace" => [
								"[DESTINATION]" => $request->DeliveryFullAddress,
								"[SHIPPINGCOST]" => $request->ShippingCost,
								"[TOTALCOST]" => $request->TotalCost,
								"[REQUEST]" => 'Online',
								"[USERNAME]" => $user_data->Name,
								"[EMAIL]" => $user_data->Email,
								"[TITLE]" => ucfirst($insert["ProductTitle"]),
								"[USERTYPE]" => $user_data->UserType,
								"[DEVICETYPE]" => $user_data->DeviceType,

							],
						]
						);

					}
				}

				Notification::insert([
					"NotificationTitle" => "Online request request updated",
					"NotificationMessage" => sprintf('The online request titled:"%s", ID:%s has updated.', ucfirst($request->ProductTitle), $request->PackageNumber),
					"NotificationUserId" => [],
					"NotificationReadStatus" => 0,
					"location" => "online",
					"locationkey" => (String) @$request->_id,
					"Date" => new MongoDate(),
					"GroupTo" => "Admin",
				]);

			}
			$url = "promocode=" . Input::get('promo_code');
			if ($insert['TotalCost'] <= 0) {

				$response['redirect_to'] = url("online-request-detail/{$response['reqid']}?$url");
			} else {
				$response['redirect_to'] = url("online-payment/{$response['reqid']}?$url");
			}

		}
		return response()->json($response);
	}
	public function edit_buy_for_me($id) {
		$data['request'] = Deliveryrequest::where(['_id' => $id, 'RequestType' => 'buy_for_me'])->first();
		if (count($data['request']) > 0) {
			session()->set($id, $data['request']['ProductList']);
			$data['additem'] = $data['request']['ProductList'];
			$data['product_list'] = $data['request']['ProductList'];
			$data['user'] = User::select('_id', 'Name', 'Email')->orderBy('Name', 'asc')->get();
			$data['country'] = CityStateCountry::where(['type' => 'Country', 'Status' => 'Active'])->get(['_id', 'Content', 'state_available']);

			$data['state'] = CityStateCountry::where(['type' => 'State', 'Status' => 'Active', 'SuperName' => trim($data['request']->DeliveryCountry)])->get(['_id', 'Content', 'state_available']);

			$data['city'] = CityStateCountry::where(['type' => 'city', 'Status' => 'Active', 'SuperName' => trim($data['request']->DeliveryState)])->get(['_id', 'Content', 'state_available']);

			$data['category'] = Category::where(['type' => 'Category', 'Status' => 'Active'])->whereIn('TravelMode', ['ship', 'air'])->orderBy('Content', 'asc')->get(['TravelMode', 'Content']);
			$data['address'] = Address::where('user_id', '=', Session::get('UserId'))->select('address_line_1', 'city', 'state', 'country', 'zipcode', '_id', 'lat', 'lng')->get();
			return view('Admin::edit.buy-for-me', $data);
		} else {
			return Redirect::to("admin/buy-for-me")->with('danger', "Fail! Something is worng");
		}
	}

	public function edit_bank(Request $request) {
		$data['user'] = User::find($request->segment(3));
		$data['country'] = CityStateCountry::whereIn('ShortCode', ['UK', 'US', 'CA'])
			->where(array('type' => 'Country'))
			->get(array('Content'));

		return view('Admin::edit.bank_info', $data);
	}
	public function post_edit_bank(Request $request) {

		$response = ["success" => 0, "msg" => "Oops! Something went wrong."];

		$validation = Validator::make($request->all(), [
			'bank_country' => 'required',
			'bank_name' => 'required',
			'account_holder_name' => 'required',
			'account_no' => 'required',
			'routing_no' => 'required',
			'PaypalId' => 'email',
		]);

		if ($validation->fails()) {

			return Redirect::to('admin/edit-bank/' . $request->segment(3))
				->withErrors($validation)->withInput();
		} else {
			$data = User::find($request->segment(3));

			if (count($data) > 0) {
				$bank_country = json_decode(Input::get('bank_country'));
				if (empty($bank_country)) {

					return Redirect::to('admin/edit-bank/' . $request->segment(3))
						->with('danger', 'Bank country is required.');
				}

				$legal_entity = [
					'type' => 'individual',
					'first_name' => trim($data->FirstName),
					'last_name' => trim($data->LastName),
				];

				$res = RSStripe::create_account([
					"legal_entity" => $legal_entity,
					"external_account" => [
						'object' => 'bank_account',
						'account_number' => trim(Input::get('account_no')),
						'routing_number' => trim(Input::get('routing_no')),
						'country' => @$bank_country->ShortCode,
						'currency' => @$bank_country->CurrencyCode,
						'AccountHolderName' => trim(Input::get('account_holder_name')),
					],
				], @$bank_country->ShortCode, $data->StripeBankId);

				if (isset($res['id'])) {
					$data->BankCountryCode = $bank_country->ShortCode;
					$data->BankName = trim(Input::get('bank_name'));
					$data->AccountHolderName = trim(Input::get('account_holder_name'));
					$data->BankAccountNo = trim(Input::get('account_no'));
					$data->RoutingNo = trim(Input::get('routing_no'));
					$data->StripeBankId = $res['id'];

					if ($data->save()) {
						$response = array(
							"success" => 1,
							"msg" => "Success! Bank information has been change successfully.",
						);
					}
				} else {
					$response['msg'] = $res;
				}
			}
		}
		if ($response['success'] == 0) {
			return Redirect::to("edit-bank-information")->with('danger', $response['msg'])->withInput();
		}

		return Redirect::to('admin/edit-bank/' . $request->segment(3))
			->withSuccess($response['msg']);
	}

	public function cancel_delivery_request(Request $request) {
		$response = ['success' => 0, 'msg' => "Oops! Something went wrong."];

		if ($request->get('request_id') != '') {
			$req_detail = Deliveryrequest::where(['_id' => trim($request->get('request_id')), 'RequestType' => 'delivery'])->first();

			if (count($req_detail) > 0) {
				$req_detail->Status = 'cancel';
				$req_detail->save();

				$user_data = User::where(['_id' => $req_detail->RequesterId])->first();

				$transporterData = User::where(array('_id' => $req_detail->TransporterId))->select('FirstName', 'LastName', 'Email', 'EmailStatus', 'NoficationStatus', 'NotificationId' . 'DeviceType', 'Name')->first();

				$response = ['success' => 1, 'msg' => "Request has been canceled successfully."];
			}
		}
		return response()->json($response);

	}

	public function admin_complete_request2($id) {
		$req_detail = Deliveryrequest::where(['_id' => trim($id)])->first();
		if (count($req_detail) > 0) {
			$requesterData = User::where(array('_id' => $req_detail->RequesterId))->
				select('FirstName', 'LastName', 'Email', 'EmailStatus', 'NoficationStatus', 'NotificationId', 'DeviceType', 'Name')->first();

			if ($req_detail->RequestType == 'delivery' AND $req_detail->request_version == 'old') {
				$req_detail->Status = "delivered";
				$req_detail->complete_by_admin = "yes";
				$req_detail->save();

				if ($requesterData->EmailStatus == "on") {
					send_mail('5622492ce4b0d4bc235582bd', [
						"to" => $requesterData->Email,
						"replace" => [
							"[USERNAME]" => $requesterData->Name,
							"[PACKAGETITLE]" => ucfirst($req_detail->ProductTitle),
							"[PACKAGEID]" => $req_detail->PackageId,
							"[SOURCE]" => $req_detail->PickupFullAddress,
							"[DESTINATION]" => $req_detail->DeliveryFullAddress,
							"[PACKAGENUMBER]" => $req_detail->PackageNumber,
							"[SHIPPINGCOST]" => $req_detail->shippingCost,
							"[TOTALCOST]" => $req_detail->TotalCost,
						],
					]);
				}
			}

			if (count($requesterData) > 0 && isset($req_detail->ProductList)) {
				$update_array = $req_detail->ProductList;
				$Notification = new Pushnotification();
				$Email = new NewEmail();
				foreach ($req_detail->ProductList as $key => $val) {
					$update_array[$key]['status'] = 'delivered';

					if ($requesterData->EmailStatus == "on") {
						$Email->send_mail('5622492ce4b0d4bc235582bd', [
							"to" => $requesterData->Email,
							"replace" => [
								"[USERNAME]" => $requesterData->Name,
								"[PACKAGETITLE]" => ucfirst($val['product_name']),
								"[PACKAGEID]" => @$val['package_id'],
								"[SOURCE]" => $req_detail->PickupFullAddress,
								"[DESTINATION]" => $req_detail->DeliveryFullAddress,
								"[PACKAGENUMBER]" => @$val['package_id'],
								"[SHIPPINGCOST]" => @$val['shippingCost'],
								"[TOTALCOST]" => @$val['after_update'],
							],
						]);

						//Notification to requester
						if ($requesterData->NoficationStatus == "on" && !empty(trim($requesterData->NotificationId))) {

							$Notification->setValue('title', trans('lang.REQUEST_DELIVERED'));
							$Notification->setValue('message', "Product '" . ucfirst(@$val['product_name']) . "' has been delivered.");
							$Notification->setValue('type', 'request_detail');
							$Notification->setValue('locationkey', $id);
							$Notification->add_user($requesterData->NotificationId, $requesterData->DeviceType);

						}

						Notification::Insert([
							array(
								"NotificationTitle" => trans('lang.REQUEST_DELIVERED'),
								"NotificationShortMessage" => "Product '" . ucfirst(@$val['product_name']) . "' has been delivered.",
								"NotificationMessage" => "Product '" . ucfirst(@$val['product_name']) . "' has been delivered.",
								"NotificationType" => "request_detail",
								"NotificationUserId" => array(new MongoId($requesterData->_id)),
								"Date" => new MongoDate(),
								"GroupTo" => "User",
							),
							array(
								"NotificationTitle" => trans('lang.SEND_DELIVERED_TITLE_ADMIN'),
								"NotificationMessage" => "Product '" . ucfirst(@$val['product_name']) . "' has been delivered.",
								"NotificationUserId" => array(),
								"NotificationReadStatus" => 0,
								"location" => "request_detail",
								"locationkey" => (string) $id,
								"Date" => new MongoDate(),
								"GroupTo" => "Admin",
							),
						]);
					}

				}
				$Notification->fire();
				$req_detail->ProductList = $update_array;
				$req_detail->complete_by_admin = "yes";
				$req_detail->save();
				if ($req_detail->RequestType == 'delivery' || $req_detail->RequestType == 'local_delivery') {
					Reqhelper::update_status2($id);
				} else {
					Reqhelper::update_status($id);
				}

				return Redirect::back()->withSuccess("Request has been completed successfully.");

			}
		}
		return Redirect::back()->with('danger', "Fail! Something is worng");
	}

	public function admin_complete_request($id) {
		$req_detail = Deliveryrequest::where(['_id' => trim($id)])->first();
		if (count($req_detail) > 0) {
			$requesterData = User::where(array('_id' => $req_detail->RequesterId))->
				select('FirstName', 'LastName', 'Email', 'EmailStatus', 'NoficationStatus', 'NotificationId', 'DeviceType', 'Name')->first();
			if (count($requesterData) > 0) {

				if ($req_detail->RequestType == 'delivery') {
					$req_detail->Status = "delivered";
					$req_detail->complete_by_admin = "yes";
					$req_detail->save();

					if ($requesterData->EmailStatus == "on") {
						send_mail('5622492ce4b0d4bc235582bd', [
							"to" => $requesterData->Email,
							"replace" => [
								"[USERNAME]" => $requesterData->Name,
								"[PACKAGETITLE]" => ucfirst($req_detail->ProductTitle),
								"[PACKAGEID]" => $req_detail->PackageId,
								"[SOURCE]" => $req_detail->PickupFullAddress,
								"[DESTINATION]" => $req_detail->DeliveryFullAddress,
								"[PACKAGENUMBER]" => $req_detail->PackageNumber,
								"[SHIPPINGCOST]" => $req_detail->shippingCost,
								"[TOTALCOST]" => $req_detail->TotalCost,
							],
						]);
					}
				} elseif ($req_detail->RequestType == 'online') {
					$req_detail->Status = "delivered";
					$req_detail->complete_by_admin = "yes";
					$req_detail->save();
					$update['ProductList.$.status'] = 'delivered';
					$update['ProductList.$.DeliveredDate'] = new MongoDate();

					foreach ($req_detail->ProductList as $value) {
						$updateData = Deliveryrequest::where(["_id" => trim($id)])
							->where(array("ProductList" => array('$elemMatch' => array('_id' => $value['_id']))))
							->update($update);
					}

					if ($requesterData->EmailStatus == "on") {
						send_mail('57d7d8107ac6f6e0148b4567', [
							"to" => $requesterData->Email,
							"replace" => [
								"[USERNAME]" => $requesterData->Name,
								"[PACKAGETITLE]" => ucfirst($req_detail->ProductTitle),
								"[PACKAGEID]" => $req_detail->PackageNumber,
								"[SOURCE]" => $req_detail->PickupAddress,
								"[DESTINATION]" => $req_detail->DeliveryFullAddress,
								"[PACKAGENUMBER]" => $req_detail->PackageNumber,
								"[SHIPPINGCOST]" => $req_detail->shippingCost,
								"[TOTALCOST]" => $req_detail->TotalCost,
							],
						]
						);
					}

				} elseif ($req_detail->RequestType == 'buy_for_me') {
					$req_detail->Status = "delivered";
					$req_detail->complete_by_admin = "yes";
					$req_detail->save();
					$update['ProductList.$.status'] = 'delivered';
					$update['ProductList.$.DeliveredDate'] = new MongoDate();

					foreach ($req_detail->ProductList as $value) {
						$updateData = Deliveryrequest::where(["_id" => trim($id)])
							->where(array("ProductList" => array('$elemMatch' => array('_id' => $value['_id']))))
							->update($update);
					}
					if ($requesterData->EmailStatus == "on") {
						send_mail('57d7d8107ac6f6e0148b4567', [
							"to" => $requesterData->Email,
							"replace" => [
								"[USERNAME]" => $requesterData->Name,
								"[PACKAGETITLE]" => ucfirst($req_detail->ProductTitle),
								"[PACKAGEID]" => $req_detail->PackageNumber,
								"[SOURCE]" => $req_detail->PickupAddress,
								"[DESTINATION]" => $req_detail->DeliveryFullAddress,
								"[PACKAGENUMBER]" => $req_detail->PackageNumber,
								"[SHIPPINGCOST]" => $req_detail->shippingCost,
								"[TOTALCOST]" => $req_detail->TotalCost,
							],
						]
						);

					}
				}

				return Redirect::back()->withSuccess("Request has been completed successfully.");

			} else {
				return Redirect::back()->with('danger', "Fail! Something is worng");
			}
		}
		return Redirect::back()->with('danger', "Fail! Something is worng");
	}

	public function trip_detail($id) {
		if ($id != '') {
			$data['info'] = Trips::find(trim($id));
			if (count($data['info']) > 0) {
				$data['back'] = "admin/business-trip";
				if ($data['info']->TripType != 'business') {
					$data['back'] = "admin/indiviual-trip";
				}

				return view('Admin::other.trip_detail', $data);
			}
		}
	}

	public function near_trip() {
		/*$data['info']=Trips::where('SourceDate','>=', new MongoDate)->get();
			        print_r(count($data['info']));
		*/

		User::where("delete_status", '!=', "yes")->update(['delete_status' => 'no']);
		echo 'okk';
		die;
	}

	/* public function section()
		    {
		    if (!(Auth::user()->UserPermission & SECTION) == SECTION) {
		    return Redirect::to('/admin/dashboard');
		    }

		    $searchvalue = Input::get('search_value');
		    if (Input::get('search_value') != '') {
		    $data['section'] = DB::connection('mysql')->table('section')->where('section_name', 'LIKE', "%$searchvalue%")->orderBy('id', 'Asc')->get();
		    } else {
		    $data['section'] = DB::connection('mysql')->table('section')->orderBy('id', 'Asc')->get();
		    }

		    return view('Admin::list.section', $data);

		    }

		    public function addSection()
		    {
		    if (!(Auth::user()->UserPermission & SECTION) == SECTION) {
		    return Redirect::to('/admin/dashboard');
		    }
		    return view('Admin::add.section');
		    }

		    public function postAddSection(Request $request)
		    {
		    if (!(Auth::user()->UserPermission & SECTION) == SECTION) {
		    return Redirect::to('/admin/dashboard');
		    }

		    $validate = Validator::make($request->all(), array('section_name' => 'required'));
		    if ($validate->fails()) {
		    return Redirect::to("admin/add-section")->withErrors($validate)->withInput();
		    } else {

		    if ($request->file('image') != "") {
		    $extension = $request->file('image')->getClientOriginalExtension();
		    $imageName = "section_" . time() . ".$extension";
		    $request->file('image')->move(BASEURL_FILE . 'section/', $imageName);
		    $section_image = $imageName;
		    } else {
		    $section_image = '';
		    }

		    $insertData = [
		    'section_name' => Input::get('section_name'),
		    'image' => $section_image,
		    'created_at' => date('Y-m-d'),
		    ];

		    $users = DB::connection('mysql')->table('section')->Insert($insertData);

		    if (count($users) > 0) {
		    return Redirect::to('admin/section')->withSuccess('Section Added successfully.');
		    } else {
		    return Redirect::to('admin/section')->withSuccess('Problem in Added.');
		    }
		    }
		    }

		    public function faq($id)
		    {

		    $data['section'] = DB::connection('mysql')->table('section')->where('id', '=', $id)->select('id', 'section_name')->first();
		    $searchvalue = Input::get('search_value');

		    if (Input::get('search_value') != '') {
		    $data['faq'] = DB::connection('mysql')->table('faq')->where('question', 'LIKE', "%$searchvalue%")->orderBy('id', 'Asc')->get();
		    } else {

		    $data['faq'] = DB::connection('mysql')->table('faq')->where('section_id', '=', $id)->orderBy('sort', 'Asc')->get();
		    }
		    return view('Admin::list.faq', $data);
		    }

		    public function addFaq($id)
		    {

		    $data['redirect_id'] = $id;
		    $data['section'] = DB::connection('mysql')->table('section')->orderBy('id', 'Asc')->get();
		    $data['question'] = DB::connection('mysql')->table('faq')->select('id', 'question','section_id')->paginate(10);

		    return view('Admin::add.faq', $data);
		    }

		    public function EditFaq($id)
		    {
		    $data['faq'] = DB::connection('mysql')->table('faq')->where('id', $id)->first();
		    $data['section'] = DB::connection('mysql')->table('section')->orderBy('id', 'Asc')->get();
		    $data['question'] = DB::connection('mysql')->table('faq')->select('id', 'question','related_question')->get();
		    return view('Admin::edit.faq', $data);
		    }

		    public function PostAddFaq(Request $request)
		    {

		    $validate = Validator::make($request->all(), array(
		    'section_name' => 'required',
		    'Question' => 'required',
		    'Answer' => 'required',
		    ));
		    if ($validate->fails()) {
		    return Redirect::to('admin/add-faq/'.Input::get('related_page'))->withErrors($validate)->withInput();
		    } else {

		    if (Input::get('related_question') != '') {
		    $string = implode(',', Input::get('related_question'));
		    } else {
		    $string = '';
		    }

		    $insertData = [
		    'section_id' => Input::get('section_name'),
		    'question' => Input::get('Question'),
		    'answer' => Input::get('Answer'),
		    'related_question' => $string,
		    'status' => 0,
		    'created_at' => date('Y-m-d'),
		    ];

		    $users = DB::connection('mysql')->table('faq')->Insert($insertData);

		    if (count($users) > 0) {
		    return Redirect::to('admin/faq/'.Input::get('related_page'))->withSuccess('Faq Added successfully.');
		    } else {
		    return Redirect::to('admin/faq/'.Input::get('related_page'))->withSuccess('Problem in Added.');
		    }
		    }
		    }

		    public function PostEditSection(Request $request)
		    {
		    $validate = Validator::make($request->all(), array('section_name' => 'required'));
		    if ($validate->fails()) {
		    return Redirect::to("admin/section")->withErrors($validate)->withInput();
		    } else {

		    if ($request->file('image') != "") {
		    $extension = $request->file('image')->getClientOriginalExtension();
		    $imageName = "section_" . time() . ".$extension";
		    $request->file('image')->move(BASEURL_FILE . 'section/', $imageName);
		    $updateData['image'] = $imageName;
		    }

		    $updateData['section_name'] = Input::get('section_name');

		    $users = DB::connection('mysql')->table('section')->where('id', '=', Input::get('sectionid'))->update($updateData);

		    if (count($users) > 0) {
		    return Redirect::to('admin/section')->withSuccess('Section updated successfully.');
		    } else {
		    return Redirect::to('admin/section')->withSuccess('Problem in updated.');
		    }
		    }
		    }

		    public function PostEditfaq(Request $request, $id)
		    {
		    $validate = Validator::make($request->all(), array(
		    'section_name' => 'required',
		    'Question' => 'required',
		    'Answer' => 'required',
		    ));
		    if ($validate->fails()) {
		    return Redirect::to("admin/edit-faq/" . Input::get('sectionId'))->withErrors($validate)->withInput();
		    } else {

		    if (Input::get('related_question') != '') {
		    $updateData['related_question'] = implode(',', Input::get('related_question'));
		    }

		    $updateData['section_id'] = Input::get('section_name');
		    $updateData['question'] = Input::get('Question');
		    $updateData['answer'] = Input::get('Answer');
		    $updateData['updated_at'] = date('Y-m-d');

		    $users = DB::connection('mysql')->table('faq')->where('id', '=', $id)->update($updateData);

		    if (count($users) > 0) {
		    return Redirect::to('admin/faq/' . Input::get('sectionId'))->withSuccess('Faq updated successfully.');
		    } else {
		    return Redirect::to('admin/faq/' . Input::get('sectionId'))->withSuccess('Problem in updation.');
		    }
		    }
		    }

		    public function post_sequence()
		    {

		    $response = array('success' => 0, 'msg' => 'Oops! We are unable to update.');

		    if (is_array(Input::get('newposition'))) {

		    foreach (Input::get('newposition') as $key => $val) {
		    $query = DB::connection('mysql')->table('faq')->where(['id' => $key])->update(['sort' => $val]);
		    }
		    $response = array('success' => 1, 'msg' => 'Record updated successfully.');
		    }
		    echo json_encode($response);
		    }

		    public function faqAnswer($id)
		    {
		    $data['result'] = DB::connection('mysql')->table('faq')->where('id', '=', $id)->first();
		    $final = explode(',', $data['result']->related_question);
		    $data['question'] = DB::connection('mysql')->table('faq')->whereIn('id', $final)->select('id', 'question', 'related_question')->get();
		    return view('Admin::other.answer', $data);
	*/

	public function add_comment($id) {
		$data['title'] = "Add Comment";

		$query = Deliveryrequest::query();

		$field = array('DeliveryVerifyCode', 'ProductTitle', 'DeliveryDate', 'RequesterName', 'Status', 'PickupDate',
			'RejectBy', 'PackageNumber', 'SenderName', 'PickupFullAddress', 'DeliveryFullAddress', 'RequestType', 'RequesterId', 'Street1', 'Street2', 'Country', 'State', 'City', 'ZipCode', 'EnterOn');

		$data['users'] = $query->where($where)
			->orwhere('Status', '=', Input::get('type'))
			->orwhere($orwhere)
			->get($field);

		return view('Admin::add.add_comment');
	}

	public function post_comment(Request $request) {
		if (!(Auth::user()->UserPermission & ADMIN) == ADMIN) {
			return Redirect::to('/admin/dashboard');
		}
		$validate = Validator::make($request->all(), array(
			'comment' => 'required',
		));
		if ($validate->fails()) {
			return Redirect::to("admin/add-comment")->withErrors($validate)->withInput();
		} else {

		}
	}

	// Local Delivery Section

	public function local_delivery() {
		if (!(Auth::user()->UserPermission & PACKAGE) == PACKAGE) {
			return Redirect::to('/admin/dashboard');
		}
		$data['category'] = Category::where(['type' => 'localCategory', 'Status' => 'Active', "country_id" => "56bdbfb4cf32079714ee89ca"])->get();
		$data['country'] = CityStateCountry::where(array('type' => 'Country'))->get(array('Content'));
		$data['title'] = "Local Delivery Management";
		$data['paginationurl'] = "pagination/local-delivery";
		$data['orderType'] = "Desc";
		$data['orderby'] = "UserId";
		$data['postvalue'] = "&status=" . Input::get('status') . "&StartDate=" . Input::get('StartDate') . "&EndDate=" . Input::get('EndDate') . "&ProductName=" . Input::get('ProductName') . "&type=" . Input::get('type')
		. "&city=" . Input::get('city') . "&state=" . Input::get('state') . "&country=" . Input::get('country') . "&category=" . Input::get('category') . "&search_value=" . Input::get('search_value') . "&Packageid=" . Input::get('Packageid') . "&requester=" . Input::get('requester');

		$data['country'] = CityStateCountry::where(['type' => 'Country', 'Status' => 'Active'])
			->get(['_id', 'Content', 'state_available']);

		return view('Admin::list.local_delivery', $data);
	}

	public function local_delivery_detail($id) {
		if (!(Auth::user()->UserPermission & PACKAGE) == PACKAGE) {
			return Redirect::to('/admin/dashboard');
		}
		$data['info'] = Deliveryrequest::find($id);
		$data['comment_count'] = Comment::where(['request_id'=>$id])->count();
		$data['accept_count'] = Deliveryrequest::where(array('_id' => $id))
			->where(array("ProductList" => array('$elemMatch' => array('status' => 'pending'))))->count();
		if (count($data['info']) > 0) {
			$data['tplist'] = User::where(['TransporterStatus' => 'active', 'TPSetting' => 'on'])->where('_id', '!=', $data['info']->RequesterId)
				->orderby('Name', 'asc')
				->get(['Name']);

			return view('Admin::localdelivery.request_detail', $data);

		} else {
			return Redirect::to("admin/local-delivery");
		}
	}

	public function alert_msg() {
		$data['alertMsg'] = Setting::where('_id', '=', '5c57fefccab743651e132659')->select()->first();
		if (count($data['alertMsg']) > 0) {
			return view('Admin::edit.alert_msg', $data);
		} else {
			return redirect::back();
		}
	}

	public function update_alert_msg(Request $request) {

		$validate = Validator::make($request->all(), array('Status' => 'required', 'Content' => 'required'));
		if ($validate->fails()) {
			return Redirect::to("admin/alert-msg")->withErrors($validate)->withInput();
		} else {
			$id = '5c57fefccab743651e132659';
			$update['status'] = Input::get('Status');
			$update['content'] = Input::get('Content');

			Setting::where('_id', '=', $id)->update($update);
			return Redirect::to("admin/alert-msg")->withSuccess('alert message updated successfully.');
		}
	}

	public function addItemHistory($data) {
		if (!empty($data['ProductList'])) {
			foreach ($data['ProductList'] as $key) {
				if ($key['_id'] == $data['item_id']) {
					$insertData = [
						'request_id' => $data['request_id'],
						'item_id' => $key['_id'],
						'request_type' => $data['type'],
						'package_id' => $key['package_id'],
						'tpid' => $key['tpid'],
						'tpName' => $key['tpName'],
						'marketid' => @$key['marketid'],
						'marketname' => @$key['marketname'],
						'txnid' => @$key['txnid'],
						'product_name' => $key['product_name'],
						'url' => @$key['url'],
						'price' => @$key['price'],
						'qty' => @$key['qty'],
						'description' => @$key['description'],
						'travelMode' => $key['travelMode'],
						'shipping_cost_by_user' => @$key['shipping_cost_by_user'],
						'weight' => @$key['weight'],
						'weight_unit' => @$key['weight_unit'],
						'height' => @$key['height'],
						'heightUnit' => @$key['heightUnit'],
						'length' => @$key['length'],
						'lengthUnit' => @$key['lengthUnit'],
						'width' => @$key['width'],
						'widthUnit' => @$key['widthUnit'],
						'category' => @$key['category'],
						'categoryid' => @$key['categoryid'],
						'insurance_status' => @$key['insurance_status'],
						'TransporterFeedbcak' => @$key['TransporterFeedbcak'],
						'TransporterRating' => @$key['TransporterRating'],
						'RequesterRating' => @$key['RequesterRating'],
						'RejectBy' => @$key['RejectBy'],
						'ReturnType' => @$key['ReturnType'],
						'ReceiptImage' => @$key['ReceiptImage'],
						'RejectTime' => @$key['RejectTime'],
						'TrackingNumber' => @$key['TrackingNumber'],
						'TransporterMessage' => @$key['TransporterMessage'],
						'CancelDate' => @$key['CancelDate'],
						'StripeChargeId' => @$key['StripeChargeId'],
						'DeliveredDate' => @$key['DeliveredDate'],
						'discount' => @$key['discount'],
						'insurance' => @$key['insurance'],
						'shippingCost' => @$key['shippingCost'],
						'aq_fee' => @$key['aq_fee'],
						'status' => @$key['status'],
						'PaymentStatus' => @$key['PaymentStatus'],
						'verify_code' => @$key['verify_code'],
						'updated_at' => new MongoDate(),
						'update_difference'=> $data['update_difference'],
					];

					Itemhistory::Insert($insertData);

				}
			}
		}
	}

	public function EditCustomsDuty(Request $request, $id) {
		$validate = Validator::make(
			$request->all(),
			array("edit_customsduty_input" => "required")
		);
		if ($validate->fails()) {
			// return Redirect::to("admin/buy-for-me/detail/".$id)->withErrors($validate)->withInput();
		} else {
			$data = Deliveryrequest::find($id);
			$req_data = [
				'request_id' => $data->_id,
				'item_id' => $data->ProductList[0]["_id"],
				'requester_id' => $data->RequesterId,
				'ProductList' => $data->ProductList,
				'type' => $data->RequestType,
				'update_difference'=> 0
			];

			$customsDutyDifference = $data->DutyAndCustom - floatval(number_format(Input::get("edit_customsduty_input"), 2));
			$previousTotalCost = $data->TotalCost;
			$totalCost = $data->TotalCost - $data->DutyAndCustom;
			$newTotalCost = floatval(number_format($totalCost, 2)) + floatval(number_format(Input::get("edit_customsduty_input"), 2));
			$difference = floatval(number_format($newTotalCost, 2)) - floatval(number_format($previousTotalCost, 2));
			
			if ($difference > 0.1) {
				$oldDifference = $data->after_update_difference;
				$newDifference = $oldDifference + $difference;
				$req_data['update_difference'] = $newDifference - $oldDifference;
				$data->after_update_difference = $data->after_update_difference + $difference;
				$data->need_to_pay = $data->need_to_pay + $req_data['update_difference'];

				$this->addItemHistory($req_data);
			}

			PaymentInfo::insert([
				'request_id' => $data->_id,
				'user_id'=> $data->RequesterId,
				'action_user_id'=> Auth::user()->_id,
				'RequestType'=> $data->RequestType,
				'action' => 'Update Duty/Customs Clearing',
				'item_id'=> $data->ProductList[0]["_id"],
				'item_unic_number' => $data->ProductList[0]["package_id"],
				'TotalCost'=> floatval($data->TotalCost),
				'shippingCost'=> floatval($data->shippingCost),
				'insurance'=> floatval($data->insurance),
				'AreaCharges'=> floatval($data->AreaCharges),
				'ProcessingFees'=> floatval($data->ProcessingFees),
				'shipping_cost_by_user'=> floatval($data->shipping_cost_by_user),
				'item_cost'=> floatval($data->total_item_price),
				'discount'=> floatval($data->discount),
				'PromoCode'=> $data->PromoCode,
				'difference'=> floatval($difference),
				'difference_before'=> floatval($data->after_update_difference - $difference),
				'date'=> New MongoDate(),
			]);

			Activitylog::insert([
				'request_id' => $data->_id,
				'request_type' => $data->RequestType,
				'PackageNumber' => $data->PackageNumber,
				'item_id' => $data->ProductList[0]["_id"],
				'package_id' => $data->ProductList[0]["package_id"],
				'item_name' => $data->ProductTitle,
				'log_type' => 'request',
				// 'message' => "Update Duty/Customs Clearing ($".$customsDutyDifference.")",
				'message' => "Update Duty/Customs Clearing from ($".$data->DutyAndCustom.") to ($".Input::get("edit_customsduty_input").")",
				'status' => "pending",
				'action_user_id' => Auth::user()->_id,
				'EnterOn' => new MongoDate(),
			]);

			$data->DutyAndCustom = floatval(number_format(Input::get("edit_customsduty_input"), 2));
			$data->save();

			
			if ($data->RequestType === "buy_for_me") {
				return Redirect::to("admin/buy-for-me/detail/".$id)->withSuccess('Duty/Customs Clearing updated successfully.');
			} else if ($data->RequestType === "online") {
				return Redirect::to("admin/online_package/detail/".$id)->withSuccess('Duty/Customs Clearing updated successfully.');
			} else if ($data->RequestType === "delivery") {
				return Redirect::to("admin/package/detail/".$id)->withSuccess('Duty/Customs Clearing updated successfully.');
			}
		}
	}

	public function EditTax(Request $request, $id) {
		$validate = Validator::make(
			$request->all(),
			array("edit_tax_input" => "required")
		);
		if ($validate->fails()) {
			// return Redirect::to("admin/buy-for-me/detail/".$id)->withErrors($validate)->withInput();
		} else {
			$data = Deliveryrequest::find($id);
			$req_data = [
				'request_id' => $data->_id,
				'item_id' => $data->ProductList[0]["_id"],
				'requester_id' => $data->RequesterId,
				'ProductList' => $data->ProductList,
				'type' => $data->RequestType,
				'update_difference'=> 0
			];
			$taxDifference = $data->Tax - floatval(number_format(Input::get("edit_tax_input"), 2));
			$previousTotalCost = $data->TotalCost;
			$totalCost = $data->TotalCost - $data->Tax;
			$newTotalCost = floatval(number_format($totalCost, 2)) + floatval(number_format(Input::get("edit_tax_input"), 2));
			$difference = floatval(number_format($newTotalCost, 2)) - floatval(number_format($previousTotalCost, 2));
			
			if ($difference > 0.1) {
				$oldDifference = $data->after_update_difference;
				$newDifference = $oldDifference + $difference;
				$req_data['update_difference'] = $newDifference - $oldDifference;
				$data->after_update_difference = $data->after_update_difference + $difference;
				$data->need_to_pay = $data->need_to_pay + $req_data['update_difference'];

				$this->addItemHistory($req_data);
			}

			PaymentInfo::insert([
				'request_id' => $data->_id,
				'user_id'=> $data->RequesterId,
				'action_user_id'=> Auth::user()->_id,
				'RequestType'=> $data->RequestType,
				'action' => 'Update Tax',
				'item_id'=> $data->ProductList[0]["_id"],
				'item_unic_number' => $data->ProductList[0]["package_id"],
				'TotalCost'=> floatval($data->TotalCost),
				'shippingCost'=> floatval($data->shippingCost),
				'insurance'=> floatval($data->insurance),
				'AreaCharges'=> floatval($data->AreaCharges),
				'ProcessingFees'=> floatval($data->ProcessingFees),
				'shipping_cost_by_user'=> floatval($data->shipping_cost_by_user),
				'item_cost'=> floatval($data->total_item_price),
				'discount'=> floatval($data->discount),
				'PromoCode'=> $data->PromoCode,
				'difference'=> floatval($difference),
				'difference_before'=> floatval($data->after_update_difference - $difference),
				'date'=> New MongoDate(),
			]);

			Activitylog::insert([
				'request_id' => $data->_id,
				'request_type' => $data->RequestType,
				'PackageNumber' => $data->PackageNumber,
				'item_id' => $data->ProductList[0]["_id"],
				'package_id' => $data->ProductList[0]["package_id"],
				'item_name' => $data->ProductTitle,
				'log_type' => 'request',
				// 'message' => "Update Tax ($".$taxDifference.")",
				'message' => "Update Tax from ($".$data->Tax.") to ($".Input::get("edit_tax_input").")",
				'status' => "pending",
				'action_user_id' => Auth::user()->_id,
				'EnterOn' => new MongoDate(),
			]);

			$data->Tax = floatval(number_format(Input::get("edit_tax_input"), 2));
			$data->save();

			if ($data->RequestType === "buy_for_me") {
				return Redirect::to("admin/buy-for-me/detail/".$id)->withSuccess('Tax updated successfully.');
			} else if ($data->RequestType === "online") {
				return Redirect::to("admin/online_package/detail/".$id)->withSuccess('Tax updated successfully.');
			} else if ($data->RequestType === "delivery") {
				return Redirect::to("admin/package/detail/".$id)->withSuccess('Tax updated successfully.');
			}
		}
	}

	public function OnlinePurchaseProcess(Request $request) {
		$response = ["success" => 0, "msg" => "Whoops!, Something went wrong."];
		$validator = Validator::make($request->all(), [
			"status" => "required",
			"requestId" => "required",
			"productId" => "required"
		]);

		if ($validator->fails()) {
			return json_encode($response);
		} else {
			$where = [
				"ProductList" => ['$elemMatch' => ["_id" => Input::get("productId")]],
				"_id" => Input::get("requestId")
			];

			if (trim(Input::get("status")) == "out_for_pickup") {
				$where["ProductList"]['$elemMatch']["status"] = ['$in' => ["assign"]];
			} else if (trim(Input::get("status")) == "out_for_delivery") {
				$where["ProductList"]['$elemMatch']["status"] = ['$in' => ["out_for_pickup"]];
			} else if (trim(Input::get("status")) == "delivered") {
				$where["ProductList"]['$elemMatch']["status"] = ['$in' => ["out_for_delivery"]];
			}
			
			$delinfo = Deliveryrequest::where($where)->first();

			if (count($delinfo) > 0) {
				$productlist = $delinfo["ProductList"];
				$product_detail = [];
				$package_id = "";
				$item_name = "";

				foreach ($productlist as $key => $value) {
					if ($value["_id"] == trim(Input::get("productId"))) {
						$package_id = $value["package_id"];
						$item_name = $value["product_name"];

						if (Input::get("status") == "out_for_pickup") {
							$productlist[$key]["status"] = "out_for_pickup";
						} else if (Input::get("status") == "out_for_delivery") {
							$productlist[$key]["status"] = "out_for_delivery";
						} else if (Input::get("status") == "delivered") {
							$productlist[$key]["status"] = "delivered";
						}

						$product_detail = $productlist[$key];
					}
				}

				if (count($product_detail) > 0) {
					if (trim(Input::get("status")) == "out_for_pickup") {
						$insertactivity = [
							"request_id" => Input::get("requestId"),
							"request_type" => $delinfo->RequestType,
							"PackageNumber" => $delinfo->PackageNumber,
							"item_id" => Input::get("productId"),
							"package_id" => $package_id,
							"item_name" => $item_name,
							"log_type" => "request",
							"message" => "Item is being picked up by a Transporter.",
							"status" => "out_for_pickup",
							"action_user_id" => Auth::user()->_id,
							"EnterOn" => new MongoDate(),
						];

						Activitylog::insert($insertactivity);
						$updatedata = [ 'ProductList.$.status' => "out_for_pickup" ];
					} else if (trim(Input::get("status")) == "out_for_delivery") {
						$insertactivity = [
							"request_id" => Input::get("requestId"),
							"request_type" => $delinfo->RequestType,
							"PackageNumber" => $delinfo->PackageNumber,
							"item_id" => Input::get("productId"),
							"package_id" => $package_id,
							"item_name" => $item_name,
							"log_type" => "request",
							'message' => "Your package is en route to be delivered.",
							"status" => "out_for_delivery",
							"action_user_id" => Auth::user()->_id,
							"EnterOn" => new MongoDate(),
						];
						Activitylog::insert($insertactivity);
						$updatedata = [ 'ProductList.$.status' => "out_for_delivery" ];
					} else if (trim(Input::get("status")) == "delivered") {
						$insertactivity = [
							"request_id" => Input::get("requestId"),
							"request_type" => $delinfo->RequestType,
							"PackageNumber" => $delinfo->PackageNumber,
							"item_id" => Input::get("productId"),
							"package_id" => $package_id,
							"item_name" => $item_name,
							"log_type" => "request",
							"message" => "Package has been successfully delivered.",
							"status" => "delivered",
							"action_user_id" => Auth::user()->_id,
							"EnterOn" => new MongoDate(),
						];

						Activitylog::insert($insertactivity);
						$updatedata = [ 'ProductList.$.status' => "delivered" ];
					}

					$updatedata["UpdateOn"] = new MongoDate();
					$res = Deliveryrequest::where($where)->update($updatedata);

					if ($res) {
						Reqhelper::update_status($delinfo->_id);

						if (trim(Input::get("status")) == "out_for_pickup") {
							$response = ["success" => 1, "msg" => "Success! Request has been picked up."];
							$message = sprintf(trans("lang.BUY_OUTFORPICKUP_MSG"), $product_detail["product_name"]);
							$title = trans("lang.BUY_OUTFORPICKUP_TITLE");
							$type = "requester_online_detail";
							$email_id = "57d7d6757ac6f6b5158b4567";
							$tp_email_id = "5876272a7ac6f607128b4569";
							$tr_msg = "Request has been picked up.";
						} else if (trim(Input::get("status")) == "out_for_delivery") {
							$response = ["success" => 1, "msg" => "Success! Your package is en route to be delivered."];
							$message = sprintf(trans("lang.ONLINE_OUTFORDELIVERY_MSG"), $product_detail["product_name"], $product_detail["_id"]);
							$title = trans("lang.ONLINE_OUTFORDELIVERY_TITLE");
							$type = "requester_online_detail";
							$email_id = "587863927ac6f6130c8b4568";
							$tp_email_id = "588f0b706befd90b2e270ce5";
							$tr_msg = "Your package is en route to be delivered.";
						} else if (trim(Input::get("status")) == "delivered") {
							$response = ["success" => 1, "msg" => "Success! Package has been successfully delivered."];
							$message = sprintf(trans("lang.MSG_REQUEST_DELIVERED"), "Aquantuo", $product_detail["product_name"]);
							$title = trans("lang.REQUEST_DELIVERED");
							$type = "requester_online_detail";
							$email_id = "5e15add2982e1e19fbb7c29b";
							$tp_email_id = "587629a97ac6f66e128b4567";
							$tr_msg = "Package has been successfully delivered.";
						}

						$user_data = User::where(array("_id" => $delinfo->RequesterId))->first();

						$requesterData = User::where(array("_id" => $delinfo->RequesterId))->
						select("Name", "Email", "EmailStatus", "NoficationStatus", "NotificationId", "DeviceType")->first();

						$transporterData = User::where(array("_id" => $product_detail["tpid"]))->select("Name", "Email", "EmailStatus", "NoficationStatus", "NotificationId", "DeviceType")->first();

						if (count($requesterData) > 0 && count($transporterData)) {
							$Notification = new Notify();
							if (in_array(trim(Input::get("status")), ["out_for_pickup", "out_for_delivery", "delivered"])) {
								// Notification to requester.
								if ($requesterData->NoficationStatus == "on" && !empty($requesterData->NotificationId)) {
									$cron_noti = [
										"title" => $title,
										"message" => $message,
										"type" => $type,
										"locationkey" => Input::get("requestId"),
										"NotificationId" => $requesterData->NotificationId,
										"DeviceType" => $requesterData->DeviceType,
										"status" => "ready",
										'by_mean' => "notification"
									];
									SendMail::Insert($cron_noti);
								}

								// Email to requester.
								if ($requesterData->EmailStatus == "on") {
									$cron_mail2 = [
										"USERNAME" => $requesterData->Name,
										"PACKAGETITLE" => ucfirst($product_detail["product_name"]),
										"PACKAGEID" => $product_detail["package_id"],
										"SOURCE" => $delinfo->PickupAddress,
										"DESTINATION" => $delinfo->DeliveryFullAddress,
										"PACKAGENUMBER" => $product_detail["package_id"],
										"SHIPPINGCOST" => $delinfo->shippingCost,
										"TOTALCOST" => $delinfo->TotalCost,
										"email_id" => $email_id,
										"email" => $requesterData->Email,
										"status" => "ready",
										"by_mean" => "email_2",
										"CC" => $delinfo->ReceiverCountrycode,
										"MNO" => $delinfo->ReceiverMobileNo
									];
									SendMail::Insert($cron_mail2);
								}
							}

							// Notification to transporter.
							if ($transporterData->NoficationStatus == "on" && !empty($transporterData->NotificationId)) {
								$cron_noti2 = [
									"title" => $title,
									"message" => $tr_msg,
									"type" => $type,
									"locationkey" => Input::get("requestId"),
									"NotificationId" => $transporterData->NotificationId,
									"DeviceType" => $transporterData->DeviceType,
									"status" => "ready",
									"by_mean" => "notification"
								];
								SendMail::Insert($cron_noti2);
							}

							// Email to transporter.
							if ($transporterData->EmailStatus == "on") {
								$cron_mail = [
									"USERNAME" => $transporterData->Name,
									"PACKAGETITLE" => ucfirst($product_detail["product_name"]),
									"PACKAGEID" => $product_detail["package_id"],
									"SOURCE" => $delinfo->PickupAddress,
									"DESTINATION" => $delinfo->DeliveryFullAddress,
									"PACKAGENUMBER" => $product_detail["package_id"],
									"SHIPPINGCOST" => $delinfo->shippingCost,
									"TOTALCOST" => $delinfo->TotalCost,
									"email_id" => $tp_email_id,
									"email" => $transporterData->Email,
									"status" => "ready",
									"REQUESTER" => ucfirst($requesterData->Name)
								];

								SendMail::Insert($cron_mail);
							}

							$insNotification = array(
								array(
									"NotificationTitle" => $title,
									"NotificationMessage" => $message,
									"NotificationType" => $type,
									"NotificationUserId" => array(new MongoId($user_data->_id)),
									"Date" => new MongoDate(),
									"GroupTo" => "User",
								),
							);

							Notification::Insert($insNotification);
						}
					}
				}
			}

			return json_encode($response);
		}
	}
	
	public function OutForPickupAll(Request $request) {
		$deliveryInfo = Deliveryrequest::where(array('_id' => $request->input('requestid')))->first();
		$array = $deliveryInfo->ProductList;
		$productName = '';
		$packageId = '';

		if (is_array($request->input('item_name')) && !empty($request->input('item_name'))) {
			foreach ($deliveryInfo->ProductList as $key => $value) {
				if (in_array($value['_id'], $request->input('item_name'))) {
					$array[$key]['status'] = 'out_for_pickup';
					$productName .= $value['product_name'].', ';
					$packageId .= $value['package_id'].', ';

					$insertactivity = [
						'request_id' => $request->input('requestid'),
						'request_type' => $deliveryInfo->RequestType,
						'PackageNumber' => $deliveryInfo->PackageNumber,
						'item_id' => $value['_id'],
						'package_id' => $value['package_id'],
						'item_name' => $value['product_name'],
						'log_type' => 'request',
						'message' => 'Item is in destination country going through customs and sorting.',
						'status' => 'out_for_pickup',
						'action_user_id' => Auth::user()->_id,
						'EnterOn' => new MongoDate(),
					];
					Activitylog::insert($insertactivity);
				}
			}

			$productName = rtrim($productName, ', ');
			$packageId = rtrim($packageId, ', ');
			$updation = Deliveryrequest::where('_id', '=', $request->input('requestid'))->update(['ProductList' => $array]);

			if ($deliveryInfo->RequestType == 'delivery') {
				Reqhelper::update_status2($deliveryInfo->_id);
			} else {
				Reqhelper::update_status($deliveryInfo->_id);
			}

			if ($deliveryInfo->RequestType == 'online') {
				$title = trans('lang.ONLINE_OUTFORPICKUP_TITLE');
				$message = trans('lang.ONLINE_OUTFORPICKUP_MSG');
				$type = 'requester_online_detail';
				$emailTemplateId = '57d7d6757ac6f6b5158b4567';
			} else if ($deliveryInfo->RequestType == 'buy_for_me') {
				$title = trans('lang.BUY_OUTFORPICKUP_TITLE');
				$message = trans('lang.BUY_OUTFORPICKUP_MSG');
				$type = 'requester_buy_for_me_detail';
				$emailTemplateId = '57d7d6757ac6f6b5158b4567';
			} else if ($deliveryInfo->RequestType == 'delivery') {
				$title = trans('lang.SEND_OUTFORPICKUP_TITLE');
				$message = trans('lang.SEND_OUTFORPICKUP_MSG');
				$type = 'requester_delivery_detail';
				$emailTemplateId = '5694cb805509251cd67773ec';
			}

			$user_data = User::where(array('_id' => $deliveryInfo->RequesterId))->first();

			if (count($user_data) > 0) {
				$Notification = new Notify();
				$Notification->setValue('title', $title);
				$Notification->setValue('message', $message);
				$Notification->setValue('type', $type);
				$Notification->setValue('locationkey', $request->input('requestid'));
				$Notification->add_user($user_data->NotificationId, $user_data->DeviceType);
				$Notification->fire();

				$insNotification = array(
					"NotificationTitle" => $title,
					"NotificationMessage" => $message,
					"NotificationType" => $type,
					"NotificationUserId" => array(new MongoId($user_data->_id)),
					"Date" => new MongoDate(),
					"GroupTo" => "User"
				);

				Notification::Insert($insNotification);
				$Email = new NewEmail();

				// Mail to user
				$Email->send_mail($emailTemplateId, [
					"to" => $user_data->Email,
					"replace" => [
						"[USERNAME]" => $user_data->Name,
						"[PACKAGETITLE]" => $productName,
						"[PACKAGEID]" => $packageId,
						"[SOURCE]" => $deliveryInfo->PickupAddress,
						"[DESTINATION]" => $deliveryInfo->DeliveryFullAddress
					],
				]);
			}

			if ($deliveryInfo->RequestType == 'buy_for_me') {
				return Redirect::to('admin/buy-for-me/detail/' . $request->input('requestid'))->withSuccess('Success! Item is in destination country going through customs and sorting.');
			} else if ($deliveryInfo->RequestType == 'delivery') {
				return Redirect::to('admin/package/detail/' . $request->input('requestid'))->withSuccess('Success! Item is in destination country going through customs and sorting.');
			} else if ($deliveryInfo->RequestType == 'online') {
				return Redirect::to('admin/online_package/detail/' . $request->input('requestid'))->withSuccess('Success! Item is in destination country going through customs and sorting.');
			}
		} else {
			return Redirect::back();
		}
	}
	
	public function OutForDeliveryAll(Request $request) {
		$deliveryInfo = Deliveryrequest::where(array('_id' => $request->input('requestid')))->first();
		$array = $deliveryInfo->ProductList;
		$productName = '';
		$packageId = '';

		if (is_array($request->input('item_name')) && !empty($request->input('item_name'))) {
			foreach ($deliveryInfo->ProductList as $key => $value) {
				if (in_array($value['_id'], $request->input('item_name'))) {
					$array[$key]['status'] = 'out_for_delivery';
					$productName .= $value['product_name'].', ';
					$packageId .= $value['package_id'].', ';

					$insertactivity = [
						'request_id' => $request->input('requestid'),
						'request_type' => $deliveryInfo->RequestType,
						'PackageNumber' => $deliveryInfo->PackageNumber,
						'item_id' => $value['_id'],
						'package_id' => $value['package_id'],
						'item_name' => $value['product_name'],
						'log_type' => 'request',
						'message' => 'Your package is en route to be delivered.',
						'status' => 'out_for_delivery',
						'action_user_id' => Auth::user()->_id,
						'EnterOn' => new MongoDate(),
					];
					Activitylog::insert($insertactivity);
				}
			}

			$productName = rtrim($productName, ', ');
			$packageId = rtrim($packageId, ', ');
			$updation = Deliveryrequest::where('_id', '=', $request->input('requestid'))->update(['ProductList' => $array]);

			if ($deliveryInfo->RequestType == 'delivery') {
				Reqhelper::update_status2($deliveryInfo->_id);
			} else {
				Reqhelper::update_status($deliveryInfo->_id);
			}

			if ($deliveryInfo->RequestType == 'online') {
				$title = trans('lang.ONLINE_OUTFORDELIVERY_TITLE');
				$message = sprintf(trans('lang.ONLINE_OUTFORDELIVERY_MSG'), $productName, $packageId);
				$type = 'requester_online_detail';
				$emailTemplateId = '587863927ac6f6130c8b4568';
			} else if ($deliveryInfo->RequestType == 'buy_for_me') {
				$title = trans('lang.BUY_OUTFORDELIVERY_TITLE');
				$message = sprintf(trans('lang.BUY_OUTFORDELIVERY_MSG'), $productName, $packageId);
				$type = 'requester_buy_for_me_detail';
				$emailTemplateId = '587863927ac6f6130c8b4568';
			} else if ($deliveryInfo->RequestType == 'delivery') {
				$title = trans('lang.SEND_OUTFORDELIVERY_TITLE');
				$message = sprintf(trans('lang.SEND_OUTFORDELIVERY_MSG'), $productName, $packageId);
				$type = 'requester_delivery_detail';
				$emailTemplateId = '562228e2e4b0252ad07d07a2';
			}

			$user_data = User::where(array('_id' => $deliveryInfo->RequesterId))->first();

			if (count($user_data) > 0) {
				$Notification = new Notify();
				$Notification->setValue('title', $title);
				$Notification->setValue('message', $message);
				$Notification->setValue('type', $type);
				$Notification->setValue('locationkey', $request->input('requestid'));
				$Notification->add_user($user_data->NotificationId, $user_data->DeviceType);
				$Notification->fire();

				$insNotification = array(
					"NotificationTitle" => $title,
					"NotificationMessage" => $message,
					"NotificationType" => $type,
					"NotificationUserId" => array(new MongoId($user_data->_id)),
					"Date" => new MongoDate(),
					"GroupTo" => "User"
				);

				Notification::Insert($insNotification);
				$Email = new NewEmail();

				// Mail to user
				$Email->send_mail($emailTemplateId, [
					"to" => $user_data->Email,
					"replace" => [
						"[USERNAME]" => $user_data->Name,
						"[PACKAGETITLE]" => $productName,
						"[PACKAGEID]" => $packageId,
						"[SOURCE]" => $deliveryInfo->PickupAddress,
						"[DESTINATION]" => $deliveryInfo->DeliveryFullAddress,
						"[CC]" => $deliveryInfo->ReceiverCountrycode,
						"[MNO]" => $deliveryInfo->ReceiverMobileNo
					],
				]);
			}

			if ($deliveryInfo->RequestType == 'buy_for_me') {
				return Redirect::to('admin/buy-for-me/detail/'.$request->input('requestid'))->withSuccess('Success! Your package is en route to be delivered.');
			} else if ($deliveryInfo->RequestType == 'delivery') {
				return Redirect::to('admin/package/detail/'.$request->input('requestid'))->withSuccess('Success! Your package is en route to be delivered.');
			} else if ($deliveryInfo->RequestType == 'online') {
				return Redirect::to('admin/online_package/detail/'.$request->input('requestid'))->withSuccess('Success! Your package is en route to be delivered.');
			}
		} else {
			return Redirect::back();
		}
	}
	
	public function DeliveredAll(Request $request) {
		$deliveryInfo = Deliveryrequest::where(array('_id' => $request->input('requestid')))->first();
		$array = $deliveryInfo->ProductList;
		$productName = '';
		$packageId = '';

		if (is_array($request->input('item_name')) && !empty($request->input('item_name'))) {
			foreach ($deliveryInfo->ProductList as $key => $value) {
				if (in_array($value['_id'], $request->input('item_name'))) {
					$array[$key]['status'] = 'delivered';
					$productName .= $value['product_name'].', ';
					$packageId .= $value['package_id'].', ';

					$insertactivity = [
						'request_id' => $request->input('requestid'),
						'request_type' => $deliveryInfo->RequestType,
						'PackageNumber' => $deliveryInfo->PackageNumber,
						'item_id' => $value['_id'],
						'package_id' => $value['package_id'],
						'item_name' => $value['product_name'],
						'log_type' => 'request',
						'message' => 'Package has been successfully delivered.',
						'status' => 'delivered',
						'action_user_id' => Auth::user()->_id,
						'EnterOn' => new MongoDate(),
					];
					Activitylog::insert($insertactivity);
				}
			}

			$productName = rtrim($productName, ', ');
			$packageId = rtrim($packageId, ', ');
			$updation = Deliveryrequest::where('_id', '=', $request->input('requestid'))->update(['ProductList' => $array]);

			if ($deliveryInfo->RequestType == 'delivery') {
				Reqhelper::update_status2($deliveryInfo->_id);
			} else {
				Reqhelper::update_status($deliveryInfo->_id);
			}

			if ($deliveryInfo->RequestType == 'online') {
				$title = trans('lang.REQUEST_DELIVERED');
				$message = sprintf(trans('lang.MSG_REQUEST_DELIVERED'), 'Aquantuo', $productName);
				$type = 'requester_online_detail';
				$emailTemplateId = '5e15add2982e1e19fbb7c29b';
			} else if ($deliveryInfo->RequestType == 'buy_for_me') {
				$title = trans('lang.REQUEST_DELIVERED');
				$message = sprintf(trans('lang.MSG_REQUEST_DELIVERED'), 'Aquantuo', $productName);
				$type = 'requester_buy_for_me_detail';
				$emailTemplateId = '5e15add2982e1e19fbb7c29b';
			} else if ($deliveryInfo->RequestType == 'delivery') {
				$title = trans('lang.REQUEST_DELIVERED');
				$message = sprintf(trans('lang.MSG_REQUEST_DELIVERED'), 'Aquantuo', $productName);
				$type = 'requester_delivery_detail';
				$emailTemplateId = '5622492ce4b0d4bc235582bd';
			}

			$user_data = User::where(array('_id' => $deliveryInfo->RequesterId))->first();

			if (count($user_data) > 0) {
				$Notification = new Notify();
				$Notification->setValue('title', $title);
				$Notification->setValue('message', $message);
				$Notification->setValue('type', $type);
				$Notification->setValue('locationkey', $request->input('requestid'));
				$Notification->add_user($user_data->NotificationId, $user_data->DeviceType);
				$Notification->fire();

				$insNotification = array(
					"NotificationTitle" => $title,
					"NotificationMessage" => $message,
					"NotificationType" => $type,
					"NotificationUserId" => array(new MongoId($user_data->_id)),
					"Date" => new MongoDate(),
					"GroupTo" => "User"
				);

				Notification::Insert($insNotification);
				$Email = new NewEmail();

				// Mail to user
				$Email->send_mail($emailTemplateId, [
					"to" => $user_data->Email,
					"replace" => [
						"[USERNAME]" => $user_data->Name,
						"[PACKAGETITLE]" => $productName,
						"[PACKAGEID]" => $packageId,
						"[SOURCE]" => $deliveryInfo->PickupAddress,
						"[DESTINATION]" => $deliveryInfo->DeliveryFullAddress
					],
				]);
			}

			if ($deliveryInfo->RequestType == 'buy_for_me') {
				return Redirect::to('admin/buy-for-me/detail/'.$request->input('requestid'))->withSuccess('Success! Package has been successfully delivered.');
			} else if ($deliveryInfo->RequestType == 'delivery') {
				return Redirect::to('admin/package/detail/'.$request->input('requestid'))->withSuccess('Success! Package has been successfully delivered.');
			} else if ($deliveryInfo->RequestType == 'online') {
				return Redirect::to('admin/online_package/detail/'.$request->input('requestid'))->withSuccess('Success! Package has been successfully delivered.');
			}
		} else {
			return Redirect::back();
		}
	}
	
	public function AcceptAll(Request $request) {
		$deliveryInfo = Deliveryrequest::where(array('_id' => $request->input('requestid')))->first();
		$array = $deliveryInfo->ProductList;
		$productName = '';
		$packageId = '';

		if (is_array($request->input('item_name')) && !empty($request->input('item_name'))) {
			foreach ($deliveryInfo->ProductList as $key => $value) {
				if (in_array($value['_id'], $request->input('item_name'))) {
					// Expected delivery date.
					$startdate = strtotime(date("Y-m-d"));
					$Buinessdays = new SapBuinessdays();
					$result = $Buinessdays->getWorkingDays($startdate);

					if ($value['travelMode'] == 'air') {
						$expectedDate = new Mongodate(strtotime('+' . $result . 'days'));
					} else {
						$expectedDate = new Mongodate(strtotime('+ 63 days'));
					}

					$array[$key]['status'] = 'accepted';
					$array[$key]['ExpectedDate'] = $expectedDate;
					$productName .= $value['product_name'].', ';
					$packageId .= $value['package_id'].', ';

					$insertactivity = [
						'request_id' => $request->input('requestid'),
						'request_type' => $deliveryInfo->RequestType,
						'PackageNumber' => $deliveryInfo->PackageNumber,
						'item_id' => $value['_id'],
						'package_id' => $value['package_id'],
						'item_name' => $value['product_name'],
						'log_type' => 'request',
						'message' => 'Item accepted.',
						'status' => 'accepted',
						'action_user_id' => Auth::user()->_id,
						'EnterOn' => new MongoDate(),
					];
					Activitylog::insert($insertactivity);
				}
			}

			$productName = rtrim($productName, ', ');
			$packageId = rtrim($packageId, ', ');
			$updation = Deliveryrequest::where('_id', '=', $request->input('requestid'))->update(['ProductList' => $array]);

			if ($deliveryInfo->RequestType == 'delivery') {
				Reqhelper::update_status2($deliveryInfo->_id);
			} else {
				Reqhelper::update_status($deliveryInfo->_id);
			}

			if ($deliveryInfo->RequestType == 'delivery') {
				$title = trans('lang.SEND_ACCEPT_TITLE');
				$message = sprintf(trans('lang.SEND_ACCEPT_MSG'), 'Aquantuo', $productName);
				$type = 'requester_delivery_detail';
				$emailTemplateId = '57d7d4b37ac6f69c158b4569';
			}

			$user_data = User::where(array('_id' => $deliveryInfo->RequesterId))->first();

			if (count($user_data) > 0) {
				$Notification = new Notify();
				$Notification->setValue('title', $title);
				$Notification->setValue('message', $message);
				$Notification->setValue('type', $type);
				$Notification->setValue('locationkey', $request->input('requestid'));
				$Notification->add_user($user_data->NotificationId, $user_data->DeviceType);
				$Notification->fire();

				$insNotification = array(
					"NotificationTitle" => $title,
					"NotificationMessage" => $message,
					"NotificationType" => $type,
					"NotificationUserId" => array(new MongoId($user_data->_id)),
					"Date" => new MongoDate(),
					"GroupTo" => "User"
				);

				Notification::Insert($insNotification);
				$Email = new NewEmail();

				// Mail to user
				$Email->send_mail($emailTemplateId, [
					"to" => $user_data->Email,
					"replace" => [
						"[USERNAME]" => $user_data->Name,
						"[PACKAGETITLE]" => $productName,
						"[PACKAGEID]" => $packageId,
						"[SOURCE]" => $deliveryInfo->PickupAddress,
						"[DESTINATION]" => $deliveryInfo->DeliveryFullAddress
					],
				]);
			}

			if ($deliveryInfo->RequestType == 'delivery') {
				return Redirect::to('admin/package/detail/'.$request->input('requestid'))->withSuccess('Success! Product has been accepted successfully.');
			}
		} else {
			return Redirect::back();
		}
	}

	public function ShipmentDepartedAll(Request $request) {
		$deliveryInfo = Deliveryrequest::where(array('_id' => $request->input('requestid')))->first();
		$array = $deliveryInfo->ProductList;
		$productName = '';
		$packageId = '';
		$expectedDelivryDate = '';

		if (is_array($request->input('item_name')) && !empty($request->input('item_name'))) {
			foreach ($deliveryInfo->ProductList as $key => $value) {
				if (in_array($value['_id'], $request->input('item_name'))) {
					$array[$key]['status'] = 'shipment_departed';
					$productName .= $value['product_name'].', ';
					$packageId .= $value['package_id'].', ';
					$expectedDelivryDate .= show_date($value['ExpectedDate']).', ';

					$insertactivity = [
						'request_id' => $request->input('requestid'),
						'request_type' => $deliveryInfo->RequestType,
						'PackageNumber' => $deliveryInfo->PackageNumber,
						'item_id' => $value['_id'],
						'package_id' => $value['package_id'],
						'item_name' => $value['product_name'],
						'log_type' => 'request',
						'message' => 'Shipment Departed originating country.',
						'status' => 'shipment_departed',
						'action_user_id' => Auth::user()->_id,
						'EnterOn' => new MongoDate(),
					];
					Activitylog::insert($insertactivity);
				}
			}

			$productName = rtrim($productName, ', ');
			$packageId = rtrim($packageId, ', ');
			$expectedDelivryDate = rtrim($expectedDelivryDate, ', ');
			$updation = Deliveryrequest::where('_id', '=', $request->input('requestid'))->update(['ProductList' => $array]);

			if ($deliveryInfo->RequestType == 'delivery') {
				Reqhelper::update_status2($deliveryInfo->_id);
			} else {
				Reqhelper::update_status($deliveryInfo->_id);
			}

			if ($deliveryInfo->RequestType == 'delivery') {
				$title = trans('lang.SHIPMENT_DEPARTED_TITLE');
				$message = trans('lang.SHIPMENT_DEPARTED_MSG');
				$type = 'requester_delivery_detail';
				$emailTemplateId = '5e8af5659008c2c087ca3b6a';
			}

			$user_data = User::where(array('_id' => $deliveryInfo->RequesterId))->first();

			if (count($user_data) > 0) {
				$Notification = new Notify();
				$Notification->setValue('title', $title);
				$Notification->setValue('message', $message);
				$Notification->setValue('type', $type);
				$Notification->setValue('locationkey', $request->input('requestid'));
				$Notification->add_user($user_data->NotificationId, $user_data->DeviceType);
				$Notification->fire();

				$insNotification = array(
					"NotificationTitle" => $title,
					"NotificationMessage" => $message,
					"NotificationType" => $type,
					"NotificationUserId" => array(new MongoId($user_data->_id)),
					"Date" => new MongoDate(),
					"GroupTo" => "User"
				);

				Notification::Insert($insNotification);
				$Email = new NewEmail();

				// Mail to user
				$Email->send_mail($emailTemplateId, [
					"to" => $user_data->Email,
					"replace" => [
						"[USERNAME]" => $user_data->Name,
						"[PACKAGETITLE]" => $productName,
						"[PACKAGEID]" => $packageId,
						"[SOURCE]" => $deliveryInfo->PickupAddress,
						"[DESTINATION]" => $deliveryInfo->DeliveryFullAddress,
						"[DATE]" => $expectedDelivryDate
					],
				]);
			}

			if ($deliveryInfo->RequestType == 'delivery') {
				return Redirect::to('admin/package/detail/'.$request->input('requestid'))->withSuccess('Success! Shipment departed originating country.');
			}
		} else {
			return Redirect::back();
		}
	}
}
