<?php namespace App\Modules\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Models\Activitylog;
use App\Http\Models\Deliveryrequest;
use App\Http\Models\Notification;
use App\Http\Models\SendMail;
use App\Http\Models\Setting;
use App\Http\Models\Transaction;
use App\Http\Models\User;
use App\Library\Reqhelper;
use App\Library\RSStripe;
use Auth;
use Input;
use MongoDate;
use MongoId;
use Redirect;
use Request;
use Stripe;
use Validator;
use App\Http\Models\Trips;
use App\Http\Models\RequestCard;

class AdminPayment extends Controller {

	/*
		    Function  : Construct.
		    Developer : Om Dangi
		    Created At: 14/02/2018
	*/

	public function __construct() {
		if (!(Auth::user())) {
			Redirect::to('admin/login')->send();
		}
	}

	/*
		    Function  : mark as paid.
		    Developer : Om Dangi
		    Created At: 14/02/2018
	*/

	public function mark_paid($id) {

		$delivery = Deliveryrequest::find($id);
		if (count($delivery) > 0) {

			$delivery->Status = 'ready';
			$delivery->PaymentStatus = 'paid';

			$productList = $delivery->ProductList;

			foreach ($productList as $key => $val) {
				$val['status'] = 'ready';
				$val['PaymentStatus'] = 'yes';
				$productList[$key] = $val;
			}

			$delivery->ProductList = $productList;
			$delivery->save();

			$user_data = User::where(['_id' => $delivery->RequesterId])->first();

			$insert = [
				'SendById' => new MongoId($delivery->RequesterId),
				'SendByName' => $delivery->RequesterName,
				'TransactionType' => 'buy_for_me',
				'Description' => "Mark as paid",
				'Credit' => $delivery->TotalCost,
				'RecieveByName' => 'Aquantuo',
				'Debit' => '',
				'Status' => 'paid',
				'Transaction_id' => "",
				'EnterOn' => new MongoDate(),
			];

			$result = Transaction::insert($insert);

			return Redirect::to("admin/buy-for-me/detail/" . $id)->withSuccess('Mark as paid has been successfully.');
		}

	}

	/*
		    Function  : Pay Via Card.
		    Developer : Om Dangi
		    Created At: 14/02/2018
	*/
	public function PayViaCard($id) {
		$data = array();
		$data['admin_msg'] = '';
		/*$config_data = Configuration::find('5673e33e6734c4f874685c84');
			        if (count($config_data) > 0) {
			        $data['admin_msg'] = $config_data->message;
		*/
		$data['Reques_id'] = $id;
		$delivery = Deliveryrequest::where('_id', '=', $id)->first(['RequesterId', 'RequesterName', 'ProductTitle', 'PackageNumber']);
		$user = User::where(['_id' => $delivery->RequesterId])->select('_id', 'StripeId')->first();

		if (count($user) > 0) {
			$data['cards'] = Payment::card_list($user->StripeId);
		}

		$data['delivery'] = $delivery;

		return view('Admin::Buy.buyforme_request_card', $data);

	}

	/*
		    Function  : add card.
		    Developer : Om Dangi
		    Created At: 14/02/2018
	*/

	public function add_card() {
		$validate = Validator::make(input::all(), [
			'name_on_card' => 'required',
			'credit_card_number' => 'required',
			'month' => 'required',
			'year' => 'required',
			'security_code' => 'required',
		]);

		if ($validate->fails()) {
			return Redirect::back()->withFail('Oops! Something went wrong.')->withInput();
		} else {

			$delivery = Deliveryrequest::where('_id', '=', Input::get('requestid'))->first(['RequesterId']);
			$user = User::where(['_id' => $delivery->RequesterId])->select('_id', 'StripeId')->first();
			if (count($user) > 0) {

				$res = Payment::add_card($user);

				if ($res == 1) {
					return Redirect::back()
						->withSuccess('Credit card added successfully.');
				} else {
					return Redirect::back()->withFail($res);
				}
			}

			return Redirect::back()->withFail('Oops! Something went wrong.');

		}
	}

	/*
		    Function  : delete card.
		    Developer : Om Dangi
		    Created At: 14/02/2018
	*/

	public function delete_card($id, $cardId, Request $request) {

		$delivery = Deliveryrequest::where('_id', '=', new MongoId($id))->first(['RequesterId']);
		$user = User::where(['_id' => $delivery->RequesterId])->first();
		if (count($user) > 0) {
			$stripe = Stripe::make();
			if ($user->StripeId != '') {
				try {
					$StripeResponse = $stripe->cards()->delete($user->StripeId, $cardId);
					$response['success'] = 1;
					$response['msg'] = "Credit card deleted successfully.";
					return Redirect::back()->withSuccess('Success! Card has been deleted successfully.');
				} catch (Exception $e) {
					$response['msg'] = $e->getMessage();
				}
			}
		} else {
			return Redirect::back()->withFail('Oops! Something went wrong.');
		}

	}

	/*
		    Function  : buy for me request card pay.
		    Developer : Om Dangi
		    Created At: 14/02/2018
	*/

	public function buyforme_request_card_pay($id, $cardid, Request $request) {

		$update['Status'] = 'ready';

		$delivery = Deliveryrequest::where('_id', '=', new MongoId($id))->first(['RequesterId']);
		$userinfo = User::where(['_id' => $delivery->RequesterId])->first();
		if (count($userinfo) > 0) {

			$where = [
				'RequesterId' => $delivery->RequesterId,
				'_id' => $id,
			];

			$data = Deliveryrequest::where($where)->whereIn('Status', ['pending', 'ready'])->first();

			$array = $data['ProductList'];

			foreach ($array as $key) {
				$package_id[] = $key['package_id'];
			}

			if (count($data) > 0) {

				$porductlist = $data->ProductList;
				foreach ($porductlist as $key => $val) {
					$porductlist[$key]['status'] = 'ready';
					$porductlist[$key]['PaymentStatus'] = 'yes';

					/* Start discount section  */
					/*if (!empty(trim(Input::get('promocode')))) {
						$res = Promo::get_validate(Input::get('promocode'),
							$val['shippingCost'],
							$val['shippingCost'],
							Session::get('UserId')
						);
						if ($res['success'] == 1) {

							$data->discount += $res['discount'];
							$porductlist[$key]['discount'] = $res['discount'];
						}
					}*/

					/* End discount section  */
				}

				//print_r($data->TotalCost - $data->discount); die;

				$res = Payment::capture($userinfo, (($data->TotalCost)), $cardid,true);
				if (isset($res['id'])) {

					if ($userinfo->EmailStatus == "on") {
						send_mail('587748397ac6f63c1a8b456d', [
							"to" => $userinfo->Email,
							"replace" => [
								"[USERNAME]" => $userinfo->Name,
								"[PACKAGETITLE]" => ucfirst($data->ProductTitle),
								"[SOURCE]" => $data->PickupFullAddress,
								"[DESTINATION]" => $data->DeliveryFullAddress,
								"[PACKAGENUMBER]" => $data->PackageNumber,
								"[SHIPPINGCOST]" => $data->ShippingCost,
								"[TOTALCOST]" => $data->TotalCost,
							],
						]);
					}

					if (Deliveryrequest::where($where)->update([
						'ProductList' => $porductlist,
						'Status' => 'ready',
						'StripeCard' => $cardid,
						'discount' => $data->discount,
						'ProcessingFees' => $data->ProcessingFees,
						'TotalCost' => $data->TotalCost,
						'BeforePurchaseTotalCost' => $data->TotalCost,
						//'PromoCode' => trim(Input::get('promocode')),
						'StripeChargeId' => $res['id'],
					])) {

						$admin = Setting::where(['_id' => '563b0e31e4b03271a097e1ca'])->first();
						if (count($admin) > 0) {

							send_mail('583967f17ac6f6b21a8b4567', [
								"to" => $admin->SupportEmail,
								"replace" => [
									"[PACKAGETITLE]" => ucfirst($data->ProductTitle),
									"[USERNAME]" => $userinfo->Name,
									"[PACKAGEID]" => $data->PackageNumber,
									"[DESTINATION]" => $data->DeliveryFullAddress,
									"[DELIVERY_TYPE]" => 'Buy for Me',
									"[DISTANCE]" => sprintf('%s Mile', number_format($data->distance, 2)),
									"[EMAIL]" => $userinfo->Email,
									"[USERTYPE]" => ucfirst($userinfo->UserType),
									"[TITLE]" => ucfirst($data->ProductTitle),
								],
							]);

						}

						Notification::insert([
							"NotificationTitle" => trans('lang.BUYFORME_REQUEST_CREATED_TITLE'),
							"NotificationMessage" => sprintf(trans('lang.BUYFORME_REQUEST_CREATED'), ucfirst($data->ProductTitle), $data->PackageNumber),
							"NotificationReadStatus" => 0,
							"location" => "buy_for_me",
							"locationkey" => (String) @$data->_id,
							"Date" => new MongoDate(),
							"GroupTo" => "Admin",
						]);

						RequestCard::Insert([
	                        'request_id' => (String) @$data->_id,
	                        'item_id' => "",
	                        'card_id' => trim($cardid),
	                        'type' => 'buy_for_me',
	                        'payment' => $data->TotalCost,
	                        'brand' => @$res['source']['brand'],
	                        'last4' => @$res['source']['last4'],
	                        'description' => "Payment done for request Title: '" . $data->ProductTitle . "' Package Id:" . $data->PackageNumber . " by admin.",
	                        "EnterOn" => new MongoDate(),
	                    ]);

						/* Activity Log */
						if ($data->Status == 'pending') {
							$insertactivity = [
								'request_id' => $data->_id,
								'request_type' => $data->RequestType,
								'PackageNumber' => $data->PackageNumber,
								'item_id' => '',
								'package_id' => '',
								'item_name' => '',
								'log_type' => 'request',
								'message' => 'Payment has been successfully.',
								'status' => 'ready',
								'action_user_id' => Auth::user()->_id,
								'EnterOn' => new MongoDate(),
							];

							Activitylog::insert($insertactivity);
						}
						return Redirect::to("admin/buy-for-me/detail/{$id}")->with('success', "Success! Your Buy for me request has been successfully submitted. Aquantuo will review and proceed with next steps immediately.");
						//Your Buy for me request has been successfully submitted. Aquantuo will review and proceed with next steps immediately.
					}
				} else {
					return Redirect::to("admin/pay-via-card/{$requestid}")->with('danger', $res);
				}
			}
		}
		return Redirect::to("admin/pay-via-card/{$requestid}")->with('danger', "Fail! Something is worng");
	}

	/*
		    Function  : online_package_mark_paid.
		    Developer : Om Dangi
		    Created At: 15/02/2018
	*/

	public function online_package_mark_paid($id) {

		$delivery = Deliveryrequest::find($id);
		if (count($delivery) > 0) {

			$delivery->Status = 'ready';
			$delivery->PaymentStatus = 'paid';

			$productList = $delivery->ProductList;

			foreach ($productList as $key => $val) {
				$val['status'] = 'ready';
				$productList[$key] = $val;
			}

			$delivery->ProductList = $productList;
			$delivery->save();

			$user_data = User::where(['_id' => $delivery->RequesterId])->first();

			$insert = [
				'SendById' => new MongoId($delivery->RequesterId),
				'SendByName' => $delivery->RequesterName,
				'TransactionType' => 'buy_for_me',
				'Description' => "Mark as paid",
				'Credit' => $delivery->TotalCost,
				'RecieveByName' => 'Aquantuo',
				'Debit' => '',
				'Status' => 'paid',
				'Transaction_id' => "",
				'EnterOn' => new MongoDate(),
			];

			$result = Transaction::insert($insert);

			return Redirect::to("admin/online_package/detail/" . $id)->withSuccess('Mark as paid has been successfully.');
		}

	}

	/*
		    Function  : online payment.
		    Developer : Om Dangi
		    Created At: 14/02/2018
	*/
	public function online_request_payment($id) {
		$data['admin_msg'] = '';
		/* $config_data = Configuration::find('5673e33e6734c4f874685c84');
			        if (count($config_data) > 0) {
			        $data['admin_msg'] = $config_data->message;
		*/
		$data['delivery'] = $data['info'] = Deliveryrequest::where(array('_id' => $id))->first();

		if (!count($data['info']) > 0) {
			return redirect('admin/online-package');
		}
		$data['cards'] = array();
		$user = User::where(array('_id' => $data['info']->RequesterId))->select('StripeId')->first();
		if (count($user) > 0) {
			$data['cards'] = Payment::card_list($user->StripeId);
		}
		return view('Admin::Buy.online_request_card', $data);
	}

	/*
		    Function  : online request card pay.
		    Developer : Om Dangi
		    Created At: 14/02/2018
	*/

	public function online_request_card_pay($id, $cardid, Request $request) {
		$requestid = $id;

		$delivery = Deliveryrequest::where('_id', '=', new MongoId($id))->first(['RequesterId']);
		$userinfo = User::where(['_id' => $delivery->RequesterId])->first();

		if (count($userinfo) > 0) {

			$where = [
				'RequesterId' => $delivery->RequesterId,
				'_id' => $requestid,
			];

			$data = Deliveryrequest::where($where)->first();
			if (count($data) > 0) {
				$porductlist = $data->ProductList;
				$email_sent = 'no';

				if ($data->Status == 'not_purchased') {
					$andwhere = [
						'RequesterId' => $delivery->RequesterId,
						'_id' => $requestid,
					];
					$status = 'paid';
				} else {
					$andwhere = [
						'RequesterId' => $delivery->RequesterId,
						'Status' => 'pending',
						'_id' => $requestid,
					];
					$status = 'ready';

				}

				foreach ($porductlist as $key => $val) {
					/* Start discount section  */
					/*if ($data->Status != 'not_purchased') {
						if (!empty(trim(Input::get('promocode')))) {
							$res = Promo::get_validate(Input::get('promocode'),
								$val['shippingCost'],
								$val['shippingCost'],
								Session::get('UserId')
							);
							if ($res['success'] == 1) {

								$data->discount += $res['discount'];
								$porductlist[$key]['discount'] = $res['discount'];
							}
						}
					}*/

					$porductlist[$key]['status'] = $status;
					$porductlist[$key]['inform_mail_sent'] = $email_sent;
					$porductlist[$key]['PaymentStatus'] = 'yes';

				}

				/*if ($data->Status == 'not_purchased') {
					$cost = $data->TotalCost - $data->BeforePurchaseTotalCost;
				} else {
					$cost = $data->TotalCost;
					$data->BeforePurchaseTotalCost = $cost;
				}*/

				$cost = $data->TotalCost;
				//$data->BeforePurchaseTotalCost = $cost;

				//print_r($cost); die;

				$res = Payment::capture($userinfo, $cost, $cardid,true);

				if (!isset($res['id'])) {
					return Redirect::to("admin/online-payment/{$requestid}")
						->with('fail', $res);
				}

				/*if ($data->Status == 'not_purchased') {
					$data->StripeChargeId2 = $res['id'];
				} else {
					$data->StripeChargeId = $res['id'];
					$data->PaymentStatus = 'capture';
					$data->PromoCode = Input::get('promocode');
					$data->TotalCost = $data->TotalCost - $data->discount;
				}*/

				$data->StripeChargeId = $res['id'];
				$data->PaymentStatus = 'capture';
				$data->PromoCode = Input::get('promocode');
				$data->TotalCost = $data->TotalCost;

				$data->Status = $status;
				$data->ProductList = $porductlist;
				$data->PaymentDate = new MongoDate();

				if ($data->save()) {

					RequestCard::Insert([
                        'request_id' => (String) @$data->_id,
                        'item_id' => "",
                        'card_id' => trim($cardid),
                        'type' => 'online',
                        'payment' => $data->TotalCost,
                        'brand' => @$res['source']['brand'],
                        'last4' => @$res['source']['last4'],
                        'description' => "Payment done for request Title: '" . $data->ProductTitle . "' Package Id:" . $data->PackageNumber . ". by admin",
                        "EnterOn" => new MongoDate(),
                    ]);

					/* Activity Log */

					$insertactivity = [
						'request_id' => $data->_id,
						'request_type' => $data->RequestType,
						'PackageNumber' => $data->PackageNumber,
						'item_id' => '',
						'package_id' => '',
						'item_name' => '',
						'log_type' => 'request',
						'message' => 'Payment has been successfully.',
						'status' => 'ready',
						'action_user_id' => Auth::user()->_id,
						'EnterOn' => new MongoDate(),
					];

					Activitylog::insert($insertactivity);

					$this->online_notify($data, $userinfo);

					return Redirect::to("admin/online_package/detail/{$requestid}")->with('success', "Success! Your Online Purchase request has been successfully submitted. Aquantuo will review and proceed with next steps immediately.");
				}
			}
		}
		return Redirect::to("admin/online-payment/{$requestid}")->with('fial', "Fail! Something is worng");
	}

	/*
		    Function  : online notify.
		    Developer : Om Dangi
		    Created At: 14/02/2018
	*/

	public function online_notify($request, $user_data) {
		if ($user_data->EmailStatus == "on") {
			send_mail('587750587ac6f6e61c8b4567', [
				"to" => $user_data->Email,
				"replace" => [
					"[USERNAME]" => $user_data->Name,
					"[PACKAGETITLE]" => ucfirst($request->ProductTitle),
					"[DESTINATION]" => $request->DeliveryFullAddress,
					"[SHIPPINGCOST]" => $request->ShippingCost,
					"[TOTALCOST]" => $request->TotalCost,
				],
			]);
		}

		Notification::insert([
			"NotificationTitle" => trans('lang.ONLINE_REQUEST_CREATED_TITLE'),
			"NotificationMessage" => sprintf(trans('lang.ONLINE_REQUEST_CREATED_MSG'), ucfirst($request->ProductTitle), $request->PackageNumber),
			"NotificationUserId" => [],
			"NotificationReadStatus" => 0,
			"location" => "online",
			"locationkey" => (String) @$request->_id,
			"Date" => new MongoDate(),
			"GroupTo" => "Admin",
		]);

		$admin = Setting::where(['_id' => '563b0e31e4b03271a097e1ca'])->first();
		if (count($admin) > 0) {
			send_mail('587751d47ac6f6e61c8b4568', [
				"to" => $admin->SupportEmail,
				"replace" => [
					"[USERNAME]" => $user_data->Name,
					"[DESTINATION]" => $request->DeliveryFullAddress,
					"[SHIPPINGCOST]" => $request->ShippingCost,
					"[TOTALCOST]" => $request->TotalCost,
					"[REQUEST]" => 'Online',
					"[EMAIL]" => $user_data->Email,
					"[TITLE]" => ucfirst($request["ProductTitle"]),
					"[USERTYPE]" => $user_data->UserType,
					"[DEVICETYPE]" => $user_data->DeviceType,
					"[PACKAGETITLE]" => ucfirst($request->ProductTitle),
					"[PACKAGEID]" => ucfirst($request["PackageNumber"]),
				],
			]);
		}

	}

	/*
	    Function  : online_package_mark_paid.
	    Developer : Om Dangi
	    Created At: 15/02/2018
	*/

	public function package_mark_paid($id) {

		$delivery = Deliveryrequest::find($id);
		if (count($delivery) > 0) {
			$status = 'ready';

			if($delivery->TripId != ''){
				$status = 'accepted';
			}

			$delivery->Status = $status;
			$delivery->PaymentStatus = 'paid';

			$productList = $delivery->ProductList;

			foreach ($productList as $key => $val) {
				$val['status'] = $status;
				$val['PaymentStatus'] = "yes";
				$productList[$key] = $val;
			}

			$delivery->ProductList = $productList;
			$delivery->save();

			$ActivityLog = DeliveryRequest::where('_id', '=', $id)->select('_id', 'PackageNumber', 'ProductList','RequestType')->first();
			$array = $ActivityLog->ProductList;

			/* Activity Log  create request for Send a package */
			foreach ($array as $key) {
				$insertactivity = [
					'request_id' => $id,
					'request_type' => ($ActivityLog->RequestType == 'local_delivery')? 'local_delivery':'delivery',
					'PackageNumber' => $ActivityLog->PackageNumber,
					'item_id' => $key['_id'],
					'package_id' => $key['package_id'],
					'item_name' => $key['product_name'],
					'log_type' => 'request',
					'message' => 'Payment successful.',
					'status' => 'ready',
					'action_user_id' => Auth::user()->_id,
					'EnterOn' => new MongoDate(),

				];
				Activitylog::insert($insertactivity);
			}

			$user_data = User::where(['_id' => $delivery->RequesterId])->first();

			$insert = [
				'SendById' => new MongoId($delivery->RequesterId),
				'SendByName' => $delivery->RequesterName,
				'TransactionType' => ($ActivityLog->RequestType == 'local_delivery')? 'local_delivery':'delivery',
				'Description' => "Mark as paid",
				'Credit' => $delivery->TotalCost,
				'RecieveByName' => 'Aquantuo',
				'Debit' => '',
				'Status' => 'paid',
				'Transaction_id' => "",
				'EnterOn' => new MongoDate(),
			];

			$result = Transaction::insert($insert);

			if($ActivityLog->RequestType == 'local_delivery'){
				return Redirect::to("admin/local-delivery-detail/" . $id)->withSuccess('Mark as paid has been successfully.');
			}else{
				return Redirect::to("admin/package/detail/" . $id)->withSuccess('Mark as paid has been successfully.');
			}

			
		}

	}

	/*
		    Function  : request_card_list.
		    Developer : Om Dangi
		    Created At: 15/02/2018
	*/

	public function request_card_list($id, Request $request) {
		$data['admin_msg'] = '';
		/* $config_data = Configuration::find('5673e33e6734c4f874685c84');
			        if (count($config_data) > 0) {
			        $data['admin_msg'] = $config_data->message;
		*/

		$del_request = Deliveryrequest::where(array('_id' => $id))->first();
		if (!count($del_request) > 0) {
			return redirect('admin/package');
		}
		/*if ($del_request->Status == 'ready') {
			        return redirect('admin/package/detail/' . $id);
		*/
		$data['cards'] = array();
		$user = User::where(array('_id' => $del_request->RequesterId))->select('StripeId')->first();
		if (count($user) > 0) {
			$data['cards'] = Payment::card_list($user->StripeId);
		}

		$data['delivery'] = $del_request;

		return view('Admin::Buy.card_list', $data);
	}

	public function checkTrip($data)
	{
		$response = ['success' => 1, 'msg' => 'Something went wrong.','trname'=>''];
		if ($data['tripid'] != '' && $data['transporter_id'] != '') {
			
			$data['trip_data'] = Trips::where(['_id' => $data['tripid'],'TransporterId' => new MongoId($data['transporter_id'])])->first();
			if(count($data['trip_data']) > 0){

				if($data['trip_data']->TripType == 'individual'){
					if ($data['trip_data']->SourceDate < new MongoDate()) {
						$response = ['success' => 0, 'msg' => 'Trip has been started. You are not able to create request.'];
					}
				}

				$transporterName = @$data['trip_data']->TransporterName;
				$tripid = $data['trip_data']->_id;
				$response = ['success' => 1, 'msg' => 'Success','trname'=> $transporterName];
			}else{
				$response = ['success' => 0, 'msg' => 'Invalid trip or transporter.'];
			}
		}
		return $response;
	}

	/*
		    Function  : pay_now for package.
		    Developer : Om Dangi
		    Created At: 15/02/2018
	*/
	public function pay_now($id, $cardid, Request $request) {
		$requestid = $id;

		$delivery = Deliveryrequest::where('_id', '=', new MongoId($id))->first(['RequesterId']);
		$user = User::where(['_id' => $delivery->RequesterId])->first();

		
		$status = 'ready';
		$where = [
			'RequesterId' => $delivery->RequesterId,
			'_id' => $requestid,
		];

		$del_detail = Deliveryrequest::where($where)->whereIn('Status', ['pending', 'ready'])->first();
		if (count($del_detail) <= 0 || count($user) <= 0) {
			return Redirect::to("admin/request-card-list/{$requestid}")->with('fail', "Fail! Something is worng");
		}

		if (count($del_detail) > 0 && count($user) > 0) {

			if($del_detail->TripId != ''){
				$tripCheck = $this->checkTrip(['tripid'=>$del_detail->TripId,'transporter_id'=> (String) $del_detail->TransporterId]);

				if($tripCheck['success'] == 0){
					return Redirect::to("admin/request-card-list/{$requestid}")->with('fail', $tripCheck['msg']);
				}else{
					$status = 'accepted';
					$tpname = $del_detail->TransporterName;
					$tpid = (String) $del_detail->TransporterId;
				}
			}



			if (!empty($del_detail->StripeChargeId)) {
				RSStripe::refund($del_detail->StripeChargeId);
			}

			$res = Payment::capture($user, $del_detail->TotalCost + $del_detail->AquantuoFees, $cardid,true);

			if (isset($res['id'])) {
				$update['StripeChargeId'] = $res['id'];
				Transaction::insert(array(
					"SendById" => (String) $user->_id,
					"SendByName" => @$user->RequesterName,
					"SendToId" => "aquantuo",
					"RecieveByName" => "Aquantuo",
					"Description" => "Amount deposited against delivery request for {$del_detail->ProductTitle}, PackageId: {$del_detail->PackageNumber} from {$del_detail->PickupFullAddress} to {$del_detail->DeliveryFullAddress}",
					"Credit" => floatval($del_detail->TotalCost),
					"Debit" => "",
					"Status" => "pending",
					"TransactionType" => "delivery_request",
					"EnterOn" => new MongoDate(),
				));

				if (isset($del_detail->ProductList)) {
					$p_array = $del_detail->ProductList;
					foreach ($p_array as $key => $value) {
						$p_array[$key]['status'] = $status;
					}
					$update['ProductList'] = $p_array;
				}

				$update['Status'] = $status;

				if (Deliveryrequest::where($where)->update($update)) {

					$ActivityLog = DeliveryRequest::where('_id', '=', $id)->select('_id', 'PackageNumber', 'ProductList')->first();
					$array = $ActivityLog->ProductList;

					/* Activity Log  Payment case for Send a Package
						                created Aakash Tejwal 24-02-2018
					*/
					foreach ($array as $key) {
						$insertactivity = [
							'request_id' => $id,
							'request_type' => 'delivery',
							'PackageNumber' => $ActivityLog->PackageNumber,
							'item_id' => $key['_id'],
							'package_id' => $key['package_id'],
							'item_name' => $key['product_name'],
							'log_type' => 'request',
							'message' => 'Payment successful.',
							'status' => 'ready',
							'action_user_id' => Auth::user()->_id,
							'EnterOn' => new MongoDate(),
						];
						Activitylog::insert($insertactivity);
					}

					Reqhelper::send_notification_to_tp(['id' => $requestid,
						'request_type' => Input::get('request_type')]);

					Notification::Insert([
						array(
							"NotificationTitle" => trans('lang.SEND_CREATE_TITLE'),
							"NotificationMessage" => sprintf(trans('lang.SEND_CREATE_MSG'), $del_detail->ProductTitle, $del_detail->PackageNumber),
							"NotificationUserId" => array(),
							"NotificationReadStatus" => 0,
							"location" => "request_detail",
							"locationkey" => (string) $requestid,
							"Date" => new MongoDate(),
							"GroupTo" => "Admin",
						),
					]);

					if ($user->EmailStatus == "on") {
						$cron_mail = [
							"USERNAME" => ucfirst($user->Name),
							"PACKAGETITLE" => ucfirst($del_detail->ProductTitle),
							"PACKAGEID" => $del_detail->PackageId,
							"SOURCE" => $del_detail->PickupFullAddress,
							"DESTINATION" => $del_detail->DeliveryFullAddress,
							"PACKAGENUMBER" => $del_detail->PackageNumber,
							"SHIPPINGCOST" => $del_detail->ShippingCost,
							"TOTALCOST" => $del_detail->TotalCost,
							'email_id' => '5694a10f5509251cd67773eb',
							'email' => $user->Email,
							'status' => 'ready',
						];
						SendMail::insert($cron_mail);
					}

					$setting = Setting::find('563b0e31e4b03271a097e1ca');
					if (count($setting) > 0) {
						$cron_mail2 = [
							"USERNAME" => ucfirst($user->Name),
							"PACKAGETITLE" => ucfirst($del_detail->ProductTitle),
							"PACKAGEID" => $del_detail->PackageId,
							"SOURCE" => $del_detail->PickupFullAddress,
							"DESTINATION" => $del_detail->DeliveryFullAddress,
							"PACKAGENUMBER" => $del_detail->PackageNumber,
							"SHIPPINGCOST" => $del_detail->ShippingCost,
							"TOTALCOST" => $del_detail->TotalCost,
							'email_id' => '56cd3e1f5509251cd677740c',
							'email' => $setting->SupportEmail,
							'status' => 'ready',
						];
						SendMail::insert($cron_mail2);
					}

					return Redirect::to("admin/package/detail/{$requestid}")->with('success', "Success! Your item has been successfully posted. Once a transporter reviews and accepts it, you will be notified.");

				}

			}
		}
	}

} # End class
