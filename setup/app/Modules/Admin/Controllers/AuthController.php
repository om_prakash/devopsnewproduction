<?php namespace App\Modules\Admin\Controllers;

/*
 * Project: Aquantuo
 *
 */

use App\Http\Controllers\Controller;
use App\Http\Models\Admin;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Input;
use MongoId;
use Redirect;

class AuthController extends Controller
{

    public function __construct()
    {
        if ((Auth::user('web'))) {
            Redirect::to('admin/dashboard')->send();
        }
    }

    public function getLogin(Request $request)
    {
        return view('Admin::auth.login');
    }
    public function authenticate(Request $request)
    {
        $response = array("success" => 0, "msg" => "Something went wrong.");
        $v = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required']);
        if ($v->fails()) {
            return Redirect::to('admin/login')->withErrors($v)->withCookie(cookie('email', Input::get('email')));
        } else {

            $email = Input::get('email');
            $password = Input::get('password');

            if (Auth::attempt(['UserEmail' => $email, 'password' => $password], (Input::get('remember') == 'on') ? true : false)) {

                if (Auth::user()->UserStatus == 'Active') {
                    if (Input::get('remember') == 'on') {
                        setcookie('useremail', $email);
                        setcookie('password', $password);
                        // Session::set('Image', $user_info->Image);
                        //Auth::login(Auth::user()->_id,true);
                    } else {
                        setcookie("useremail", "", time() - 3600);
                        setcookie("password", "", time() - 3600);
                    }
                    return redirect()->intended('admin/dashboard');
                } else {
                    Auth::logout();
                    return Redirect::to('admin/login')->with('loginfail', 'You are inactive.');
                }
            } else {
                return Redirect::to('admin/login')->with('loginfail', 'You have entered wrong credentials.');
            }
        }
    }

    public function getforgot()
    {
        return view('Admin::auth.forgot_password');
    }
    public function postforgot(Request $request)
    {
        $email = Input::get('email');
        $v = Validator::make($request->all(), array('email' => 'required|email'));
        if ($v->fails()) {
            return Redirect::to('admin/forgot_password')->withErrors($v)->withInput(Input::get());
        }
        $auth = Admin::where(array('UserEmail' => $email))->get(array('UserEmail', 'UserName'));
        if ($auth->count() > 0) {
            $to = $auth[0]->UserEmail;
            $token = rand(1000, 9999);
            $link = url("admin/reset_password") . "/" . $auth[0]->_id . "/$token";

            $admin = Admin::find($auth[0]->_id);
            $admin->Token = $token;
            $admin->save();

            $array = array(
                'to' => $email,
                'replace' => array(
                    '[USERNAME]' => $auth[0]->UserName,
                    '[LINK]' => "<a href='$link'>$link</a>",
                ),
            );
            send_mail('560f774ce4b0e8a48085522c', $array);

            //return redirect('admin/login')->withSuccess('Please check your email and follow the step given in mail.');
            return redirect('admin/login')->with('loginsuccessmail', 'Please check your email and follow the step given in mail.');
        } else {
            return Redirect::to('admin/forgot_password')->with('forgotfail', 'Invalid email.');

        }
    }
    public function reset_password($id, $token, Request $request)
    {
        $id = (strlen($request->segment(3)) == 24) ? new MongoId($request->segment(3)) : $request->segment(3);

        $auth = Admin::where(array('_id' => $id, 'Token' => (int) $request->segment(4)))->get(array('UserEmail', 'Token'));

        if (count($auth) > 0) {
            return view('Admin::auth.reset_password');
        } else {
            return redirect('admin/login')->withError('You are not authorize to reset password.	');
        }
    }
    public function postreset_password($id, $token, Request $request)
    {

        $v = Validator::make($request->all(), array('conf_password' => 'required', 'password' => 'required'));
        if ($v->fails()) {
            return Redirect::to('admin/reset_password/' . $request->segment(3) . "/" . $request->segment(4))->withErrors($v)->withInput(Input::get());
        }
        $auth = Admin::find($request->segment(3));

        $auth->password = bcrypt(Input::get('password'));
        $auth->Token = '';

        $auth->save();
        return redirect('admin/login')->withSuccess('Password reset successfully.');
    }

}
