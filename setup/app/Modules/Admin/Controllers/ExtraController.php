<?php namespace App\Modules\Admin\Controllers;

/*
 * Project: Aquantuo
 *
 */
use App\Http\Controllers\Controller;
use App\Http\Models\Admin;
use App\Http\Models\CityStateCountry;
use App\Http\Models\Distance;
use App\Http\Models\Extraregion;
use App\Http\Models\FAQ;
use App\Http\Models\SilderImages;
use App\Http\Models\User;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Input;
use MongoDate;

class ExtraController extends Controller {

	public function __construct() {
		if (!(Auth::user())) {
			Redirect::to('admin/login')->send();
		}
	}

	public function commonpaginationdata() {
		$data['cur_page'] = $data['page'] = Input::get('page');
		$data['page'] -= 1;
		$data['per_page'] = Auth::user()->PerPage; //Input::get('Pnum');
		$data['previous_btn'] = true;
		$data['next_btn'] = true;
		$data['first_btn'] = true;
		$data['last_btn'] = true;
		$data['sno'] = ($data['start'] = $data['page'] * $data['per_page']) + 1;

		return $data;
	}

	public function faq_list() {
		if (!(Auth::user()->UserPermission & FAQ) == FAQ) {
			return Redirect::to('/admin/dashboard');
		}
		$data['title'] = "Faq Management";
		$data['paginationurl'] = "pagination/faq_list";
		$data['orderType'] = "Desc";
		$data['orderby'] = "UserId";
		$data['postvalue'] = "&search_value=" . Input::get('search_value');

		return view('Admin::list.faq_list', $data);
	}

		
	public function editcity($id) {
		$data['state'] = CityStateCountry::where(array('type' => 'State'))->get();
		$data['lists'] = CityStateCountry::where('_id', '=', $id)->first();
		return view('Admin::edit.city', $data);
	}


	/*
		     * Used For load view of add faq
		     *
	*/
	public function add_faq() {
		if (!(Auth::user()->UserPermission & FAQ) == FAQ) {
			return Redirect::to('/admin/dashboard');
		}
		$data['add'] = "Add Faq";
		return view('Admin::add.add_faq', $data);
	}

	/*
		     * Used For add new faq
		     *
	*/
	public function post_faq(Request $request) {
		if (!(Auth::user()->UserPermission & FAQ) == FAQ) {
			return Redirect::to('/admin/dashboard');
		}
		$validate = Validator::make($request->all(), array('Question' => 'required', 'Answer' => 'required'));
		if ($validate->fails()) {
			return Redirect::to("admin/add_faq")->withErrors($validate)->withInput();
		} else {
			$array = array('Question' => $request['Question'], 'Answer' => $request['Answer'], 'Status' => 'Active');

			$lastId = FAQ::insert($array);
			return Redirect::to('admin/faq_list')->withSuccess('Faq Added successfully.');
		}
	}

	/*
		     * Used For edit faq
		     *
	*/
	public function edit_faq($id) {
		if (!(Auth::user()->UserPermission & FAQ) == FAQ) {
			return Redirect::to('/admin/dashboard');
		}
		$data['edit'] = "Edit Faq";
		$data['info'] = FAQ::find($id);
		if (count($data['info']) > 0) {
			return view('Admin::edit.edit_faq', $data);
		} else {
			return Redirect::to('admin/faq_list');
		}
	}

	/*
		     * Used For update faq
		     *
	*/
	public function update_faq($id, Request $request) {
		if (!(Auth::user()->UserPermission & FAQ) == FAQ) {
			return Redirect::to('/admin/dashboard');
		}
		$answer = Input::get('Answer');
		$question = Input::get('Question');

		$validate = Validator::make($request->all(), array('Question' => 'required', 'Answer' => 'required'));
		if ($validate->fails()) {
			return Redirect::to("admin/edit_faq/$id")->withErrors($validate)->withInput();
		} else {
			$save = FAQ::find($id);
			$save->Question = $question;
			$save->Answer = $answer;
			$save->save();
			return Redirect::to('admin/faq_list')->withSuccess('Faq updated successfully.');
		}
	}

	public function section() {
		if (!(Auth::user()->UserPermission & SECTION) == SECTION) {
			return Redirect::to('/admin/dashboard');
		}

		$searchvalue = Input::get('search_value');
		if (Input::get('search_value') != '') {
			$data['section'] = DB::connection('mysql')->table('section')->where('section_name', 'LIKE', "%$searchvalue%")->orderBy('id', 'Asc')->get();
		} else {
			$data['section'] = DB::connection('mysql')->table('section')->orderBy('id', 'Asc')->get();
		}

		return view('Admin::list.section', $data);

	}

	public function addSection() {
		if (!(Auth::user()->UserPermission & SECTION) == SECTION) {
			return Redirect::to('/admin/dashboard');
		}
		return view('Admin::add.section');
	}

	public function postAddSection(Request $request) {
		if (!(Auth::user()->UserPermission & SECTION) == SECTION) {
			return Redirect::to('/admin/dashboard');
		}

		$validate = Validator::make($request->all(), array('section_name' => 'required'));
		if ($validate->fails()) {
			return Redirect::to("admin/add-section")->withErrors($validate)->withInput();
		} else {

			if ($request->file('image') != "") {
				$extension = $request->file('image')->getClientOriginalExtension();
				$imageName = "section_" . time() . ".$extension";
				$request->file('image')->move(BASEURL_FILE . 'section/', $imageName);
				$section_image = $imageName;
			} else {
				$section_image = '';
			}

			$insertData = [
				'section_name' => Input::get('section_name'),
				'image' => $section_image,
				'created_at' => date('Y-m-d'),
			];

			$users = DB::connection('mysql')->table('section')->Insert($insertData);

			if (count($users) > 0) {
				return Redirect::to('admin/section')->withSuccess('Section Added successfully.');
			} else {
				return Redirect::to('admin/section')->withSuccess('Problem in Added.');
			}
		}
	}

	public function faq($id) {

		$data['section'] = DB::connection('mysql')->table('section')->where('id', '=', $id)->select('id', 'section_name')->first();
		$searchvalue = Input::get('search_value');

		if (Input::get('search_value') != '') {
			$data['faq'] = DB::connection('mysql')->table('faq')->where('question', 'LIKE', "%$searchvalue%")->orderBy('id', 'Asc')->get();
		} else {

			$data['faq'] = DB::connection('mysql')->table('faq')->where('section_id', '=', $id)->orderBy('id', 'Asc')->get();
		}
		return view('Admin::list.faq', $data);
	}

	public function addFaq($id) {

		$data['redirect_id'] = $id;
		$data['section'] = DB::connection('mysql')->table('section')->orderBy('id', 'Asc')->get();
		$data['question'] = DB::connection('mysql')->table('faq')->select('id', 'question', 'section_id')->paginate(10);

		return view('Admin::add.faq', $data);
	}

	public function EditFaq($id) {
		$data['faq'] = DB::connection('mysql')->table('faq')->where('id', $id)->first();

		$data['section'] = DB::connection('mysql')->table('section')->orderBy('id', 'Asc')->get();
		$data['question'] = DB::connection('mysql')->table('faq')->select('id', 'question', 'related_question')->get();

		return view('Admin::edit.faq', $data);
	}

	public function PostAddFaq(Request $request) {

		$validate = Validator::make($request->all(), array(
			'section_name' => 'required',
			'Question' => 'required',
			'Answer' => 'required',
		));
		if ($validate->fails()) {
			return Redirect::to('admin/add-faq/' . Input::get('related_page'))->withErrors($validate)->withInput();
		} else {

			if (Input::get('chk_del') != '') {
				$string = implode(',', Input::get('chk_del'));
			} else {
				$string = '';
			}

			$insertData = [
				'section_id' => Input::get('section_name'),
				'question' => Input::get('Question'),
				'answer' => Input::get('Answer'),
				'related_question' => $string,
				'status' => 0,
				'created_at' => date('Y-m-d'),
			];

			
			$users = DB::connection('mysql')->table('faq')->Insert($insertData);

			if (count($users) > 0) {
				return Redirect::to('admin/faq/' . Input::get('related_page'))->withSuccess('Faq Added successfully.');
			} else {
				return Redirect::to('admin/faq/' . Input::get('related_page'))->withSuccess('Problem in Added.');
			}
		}
	}

	public function PostEditSection(Request $request) {
		$validate = Validator::make($request->all(), array('section_name' => 'required'));
		if ($validate->fails()) {
			return Redirect::to("admin/section")->withErrors($validate)->withInput();
		} else {

			if ($request->file('image') != "") {
				$extension = $request->file('image')->getClientOriginalExtension();
				$imageName = "section_" . time() . ".$extension";
				$request->file('image')->move(BASEURL_FILE . 'section/', $imageName);
				$updateData['image'] = $imageName;
			}

			$updateData['section_name'] = Input::get('section_name');

			$users = DB::connection('mysql')->table('section')->where('id', '=', Input::get('sectionid'))->update($updateData);

			if (count($users) > 0) {
				return Redirect::to('admin/section')->withSuccess('Section updated successfully.');
			} else {
				return Redirect::to('admin/section')->withSuccess('Problem in updated.');
			}
		}
	}

	public function PostEditfaq(Request $request, $id) {
		$validate = Validator::make($request->all(), array(
			'section_name' => 'required',
			'Question' => 'required',
			'Answer' => 'required',
		));
		if ($validate->fails()) {
			return Redirect::to("admin/edit-faq/" . Input::get('sectionId'))->withErrors($validate)->withInput();
		} else {

			if (Input::get('related_question') != '') {
				$updateData['related_question'] = implode(',', Input::get('related_question'));
			}

			$updateData['section_id'] = Input::get('section_name');
			$updateData['question'] = Input::get('Question');
			$updateData['answer'] = Input::get('Answer');
			$updateData['updated_at'] = date('Y-m-d');

			$users = DB::connection('mysql')->table('faq')->where('id', '=', $id)->update($updateData);

			if (count($users) > 0) {
				return Redirect::to('admin/faq/' . Input::get('sectionId'))->withSuccess('Faq updated successfully.');
			} else {
				return Redirect::to('admin/faq/' . Input::get('sectionId'))->withSuccess('Problem in updation.');
			}
		}
	}

	public function post_sequence() {

		$response = array('success' => 0, 'msg' => 'Oops! We are unable to update.');

		if (is_array(Input::get('newposition'))) {

			foreach (Input::get('newposition') as $key => $val) {
				$query = DB::connection('mysql')->table('faq')->where(['id' => $key])->update(['sort' => $val]);
			}
			$response = array('success' => 1, 'msg' => 'Record updated successfully.');
		}
		echo json_encode($response);
	}

	public function faqAnswer($id) {
		$data['result'] = DB::connection('mysql')->table('faq')->where('id', '=', $id)->first();
		$final = explode(',', $data['result']->related_question);
		$data['question'] = DB::connection('mysql')->table('faq')->whereIn('id', $final)->select('id', 'question', 'related_question')->get();
		return view('Admin::other.answer', $data);
	}

	public function complains() {

		$search = trim(Input::get('search_value'));
		if ($search != '') {
			$data['result'] = DB::connection('mysql')->table('support')->where('email', 'Like', $search)->paginate(10);
		} else {
			$data['result'] = DB::connection('mysql')->table('support')->paginate(10);
		}

		return view('Admin::list.complain', $data);
	}

	public function sendEmail() {
		if (Input::get('message') != '') {
			send_mail('5a73f9cb360d5c20c5a7a3a2', [
				"to" => Input::get('email'),
				"replace" => [
					"[MESSAGE]" => ucfirst(Input::get('message')),
				],
			]
			);
			return Redirect::to('admin/complains')->withSuccess('Mail send successfully.');

		}
		return Redirect::to('admin/complains')->withSuccess('Mail send successfully.');

	}

	public function postImages(Request $request, $id) {

		if (Input::file('image')) {
			$extension = Input::file('image')->getClientOriginalExtension();
			$fileName = time() . rand(100, 999) . '.' . $extension;
			if (Input::file('image')->move(BASEURL_FILE . 'section/', $fileName)) {
				$updateData['image'] = "section/" . $fileName;
			}
		}

		if (Input::file('slideImage')) {
			$extension2 = Input::file('slideImage')->getClientOriginalExtension();
			$fileName2 = time() . rand(100, 999) . '.' . $extension2;
			if (Input::file('slideImage')->move(BASEURL_FILE . 'section/', $fileName2)) {
				$updateData['slideImage'] = "section/" . $fileName2;
			}
		}

		$updateData['sliderContant'] = Input::get('description');
		$lastId = SilderImages::where('_id', '=', $id)->update($updateData);
		return Redirect::to('admin/silder-image')->withSuccess('Image updated Successfully.');
	}

	public function accraMamagement(Request $request) {
		$data['paginationurl'] = "pagination/accra-management";
		$data['orderType'] = "Desc";
		$data['orderby'] = "UserId";

		$data['country'] = CityStateCountry::where(['type' => 'Country', 'Status' => 'Active'])
			->get(['_id', 'Content', 'state_available']);

		$data['postvalue'] = "&type=" . Input::get('type') . "&search_value=" . Input::get('search_value') . "&class=" . Input::get('class') . "&search_email=" . Input::get('search_email') . "&package_id=" . Input::get('package_id') . "&state=" . Input::get('state') . "&country=" . Input::get('country') . "&request_id=" . Input::get('request_id') . "&streat=" . Input::get('streat');
		return view('Admin::list.accracharge', $data);
	}

	public function addAccra(Request $request) {
		$validate = Validator::make($request->all(), array(
			'country' => 'required',
			'state' => 'required',
			'amount' => 'required'));
		if ($validate->fails()) {
			return Redirect::to("admin/accra-mamagement")->withErrors($validate)->withInput();
		} else {
			$country = json_decode(Input::get('country'));
			$state = json_decode(Input::get('state'));

			$array = [
				'country' => $country->name,
				'country_id' => $country->id,
				'state' => $state->name,
				'state_id' => $state->id,
				'status' => 'Active',
				'amount' => (float) Input::get('amount'),
				'Enteron' => new MongoDate(),
			];

			$result = Extraregion::insert($array);

			if (count($result) > 0) {
				return Redirect::to('admin/accra-mamagement')->withSuccess('Accra Charge has been added successfully.');
			} else {
				return Redirect::to('admin/accra-mamagement')->withErrors('Problem in added.');
			}
		}
	}

	// public function addKenyaResionCharge() {
    //     $country = CityStateCountry::where(['SuperName' => 'Kenya'])
    //         ->orderBy('Content', 'Asc')
	// 		->get();
	// 	echo "<pre>";
    //     print_r(json_decode($country));
	// 	foreach ($country as $key => $value) {
	// 		$array = [
	// 			'country' => $value->SuperName,
	// 			'country_id' => $value->CountryId,
	// 			'state' => $value->Content,
	// 			'state_id' => $value->_id,
	// 			'status' => 'Active',
	// 			'amount' => 1,
	// 			'Enteron' => new MongoDate(),
	// 		];

	// 		$result = Extraregion::insert($array);
	// 	}
	// }

	public function updateRegion($id) {
		if (Input::get('Charges') != '') {
			$value = Input::get('Charges');
		} else {
			$value = 0.0;
		}

		$updateDate['amount'] = $value;
		$result = Extraregion::where('_id', '=', $id)->update($updateDate);
		if (count($result) > 0) {
			return Redirect::to('admin/accra-mamagement')->withsuccess('region is updated Successfully.');
		} else {
			return Redirect::to('admin/accra-mamagement')->withErrors('Problem in update.');
		}
	}

	public function addRegion() {
		$data['country'] = CityStateCountry::where(['type' => 'Country', 'Status' => 'Active'])
			->get(['_id', 'Content', 'state_available']);
		return view('Admin::add.addregion', $data);
	}

	public function distance(Request $request) {
		$data['paginationurl'] = "pagination/distance";
		$data['orderType'] = "Desc";
		$data['orderby'] = "UserId";
		$data['country'] = CityStateCountry::where(array('type' => 'Country', 'Status' => 'Active'))->select('_id', 'Content', 'Status')->get();
		$data['postvalue'] = "&type=" . Input::get('type') . "&search_value=" . Input::get('search_value') . "&class=" . Input::get('class') . "&search_email=" . Input::get('search_email') . "&package_id=" . Input::get('package_id') . "&City=" . Input::get('City') . "&notification=" . Input::get('notification') . "&request_id=" . Input::get('request_id') . "&country=" . Input::get('country');
		return view('Admin::list.distance', $data);

	}

	public function item_value() {
		$data['paginationurl'] = "pagination/item-value";
		$data['orderType'] = "Desc";
		$data['orderby'] = "UserId";
		$data['country'] = CityStateCountry::where(array('type' => 'Country', 'Status' => 'Active'))->select('_id', 'Content', 'Status')->get();
		$data['postvalue'] = "&type=" . Input::get('type') . "&search_value=" . Input::get('search_value') . "&class=" . Input::get('class') . "&search_email=" . Input::get('search_email') . "&package_id=" . Input::get('package_id') . "&City=" . Input::get('City') . "&notification=" . Input::get('notification') . "&request_id=" . Input::get('request_id') . "&country_id=" . Input::get('country_id');
		return view('Admin::list.itemvalue', $data);
	}

	public function updatedistance($id) {
		$validation = Validator::make(Input::all(), array(
			'to' => 'required',
			'from' => 'required',
			'price' => 'required',
		));
		if ($validation->fails()) {
			return Redirect::to("admin/distance/")
				->withErrors($validation)->withInput();
		} else {
			if (trim(Input::get('type') == 'item-value')) {
				$type = 'item-value';
			} else {
				$type = 'distance';
			}
			$updateData['to'] = (float) Input::get('to');
			$updateData['from'] = (float) Input::get('from');
			$updateData['price'] = (float) Input::get('price');
			$updateData['country_id'] = Input::get('country');

			$result = Distance::where('_id', '=', $id)->update($updateData);
			if (count($result) > 0) {
				return Redirect::to('admin/' . $type)->withSuccess('update successfully.');
			} else {
				return Redirect::to('admin/' . $type)->withErrors('Problem in added');
			}
		}
	}

}
