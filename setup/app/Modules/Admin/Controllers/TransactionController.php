<?php namespace App\Modules\Admin\Controllers;

/*
 * Package: Aquantuo
 *
 */

use App\Http\Controllers\Controller;
use App\Http\Models\Deliveryrequest;
use App\Http\Models\Transaction;
use App\Http\Models\User;
use App\Library\export_xls;
use App\Library\RSStripe;
use Auth;
use DateTime;
use Illuminate\Support\Facades\Redirect;
use Input;
use MongoDate;
use MongoId;
use DB;

class TransactionController extends Controller
{

    public function __construct()
    {
        if (!(Auth::user())) {
            Redirect::to('admin/login')->send();
        }
    }
    public function commonpaginationdata()
    {
        if (Auth::user()->PerPage == '') {
            Auth::user()->PerPage = 20;
        }

        $data['cur_page'] = $data['page'] = Input::get('page');
        $data['page'] -= 1;
        $data['per_page'] = Auth::user()->PerPage; //Input::get('Pnum');

        $data['previous_btn'] = true;
        $data['next_btn'] = true;
        $data['first_btn'] = true;
        $data['last_btn'] = true;
        $data['sno'] = ($data['start'] = $data['page'] * $data['per_page']) + 1;
        return $data;
    }

    public function transaction_history()
    {
        if (!(Auth::user()->UserPermission & TRANSACTION) == TRANSACTION) {
            return Redirect::to('/admin/dashboard');
        }
        $data['paginationurl'] = "pagination/transaction-history";
        $data['orderType'] = "desc";
        $data['orderby'] = "_id";
        $data['postvalue'] = $this->array_to_attributes(Input::get());
        return view('Admin::list.transaction-history', $data);
    }
    public function pagination_transaction_history()
    {
        $query = Transaction::query();
        $selectQuery = Deliveryrequest::query();

        $countQuery = Transaction::query();
        $data = $this->commonpaginationdata();

        $where = array();
        $orwhere = array();
        if (Input::get('userid') != '') {
            $where['SendById'] = Input::get('userid');
            $orwhere['SendToId'] = Input::get('userid');
        }
        if (Input::get('search_value') != '') {
            $query->where('SendByName', 'like', "%" . Input::get('search_value') . "%");
            $query->orWhere('RecieveByName', 'like', "%" . Input::get('search_value') . "%");
        }

        if (Input::get('StartDate') != '') {
            $where['EnterOn']['$gt'] = new MongoDate(strtotime(DateTime::createFromFormat('d/m/Y', Input::get('StartDate'))->format('d-m-Y')));
        }

        if (Input::get('EndDate') != '') {
            $where['EnterOn']['$lt'] = new MongoDate(strtotime(DateTime::createFromFormat('d/m/Y', Input::get('EndDate'))->format('d-m-Y 23:59:59')));
        }

        if (Input::get('status') != '' && Input::get('status') != "all") {
            $where['Status'] = Input::get('status');
        }

        $field = array('RecieveByName', 'SendByName', 'request_id','momo_id', 'Description', 'Credit', 'Debit', 'EnterOn', 'Status','CurrencyRate');
        $data['info'] = $query->where($where)->orWhere($orwhere)->skip($data['start'])->take($data['per_page'])->orderBy('_id', 'desc')->get($field);

        $requestIdArray = [];
        foreach ($data['info'] as $value) {
            if ($value->request_id) {
                array_push($requestIdArray, new MongoId($value->request_id));
            }
        }

        $requestwhere['$in'] = $requestIdArray;

        $requestDetail = $selectQuery->where(["_id" => $requestwhere])->get(["_id", "RequestType", "PackageNumber", "shippingCost", "ShippingCost", "shipping_cost_by_user", "insurance", "InsuranceCost", "DutyAndCustom", "Tax", "TotalCost", "total_item_price", "AquantuoFees", "ProcessingFees", "AreaCharges", "region_charge", "after_update_difference"]);

        for ($i=0; $i < count($data['info']); $i++) {
            $data['info'][$i]['request_detail'] = "";
            for ($j=0; $j < count($requestDetail); $j++) {
               if ((string)$requestDetail[$j]['_id'] == $data['info'][$i]['request_id']) {
                       $data['info'][$i]['request_detail'] = $requestDetail[$j];
                }
            }
        }
       
        $data['count'] = $countQuery->where($where)->orWhere($orwhere)->count();
        $data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;

        return view('Admin::list.pagination.transaction_history', $data);
    }
    /*
     * Used For export csv of transaction
     *
     */
    public function export_csv()
    {
        $q = Transaction::query();

        $searchValue = Input::get('search_value');

        $where = array();

        if (Input::get('status') != '') {
            $where['Status'] = Input::get('status');
        }
        if (Input::get('FromDate') != '') {
            $where['EnterOn']['$gt'] = new MongoDate(strtotime(DateTime::createFromFormat('d/m/Y', Input::get('FromDate'))->format('d-m-Y')));
        }
        if (Input::get('ToDate') != '') {
            $where['EnterOn']['$lt'] = new MongoDate(strtotime(DateTime::createFromFormat('d/m/Y', Input::get('ToDate'))->format('d-m-Y 23:59:59')));
        }
        if (Input::get('transactiontype') == 'credit') {
            $where['Credit'] = array('$gt' => 0);
        }
        if (Input::get('transactiontype') == 'debit') {
            $where['Debit'] = array('$gt' => 0);
        }
        if (Input::get('depositby') != '') {
            $q->where('SendByName', 'like', "%" . Input::get('depositby') . "%");
        }
        if (Input::get('depositto') != '') {
            $q->where('RecieveByName', 'like', "%" . Input::get('depositto') . "%");
        }
        //
        $field = array('EnterOn', 'RecieveByName', 'Credit', 'Debit', 'Status', 'SendByName');
        $info = $q->where($where)->get($field);

        $filename = time() . '.xls';
        $export_xls = new export_xls();
        $export_xls->ExportXLS($filename);

        $header[] = "S.No.";
        $header[] = "Date";
        $header[] = "Deposit By";
        $header[] = "Receiver Name";
        $header[] = "Credit";
        $header[] = "Debit";
        $header[] = "Status";

        $export_xls->addHeader($header);

        foreach ($info as $key => $val) {
            if (isset($val['EnterOn']->sec)) {
                $val['EnterOn'] = date('d-M-Y H:i:s', $val['EnterOn']->sec);
            }
            $val['Status'] = ($val['Status'] == 'done') ? 'done' : 'pending';
            $row = array();
            $row[] = array(++$key, $val['EnterOn'], $val['SendByName'], $val['RecieveByName'], $val['Credit'], $val['Debit'], $val['Status']);

            $export_xls->addRow($row);
        }
        $export_xls->sendFile();
    }

    public function array_to_attributes($array_attributes)
    {

        $attributes_str = null;
        foreach ($array_attributes as $attribute => $value) {
            $attributes_str .= "&$attribute=$value";
        }
        return $attributes_str;
    }
    public function transaction_to_driver()
    {
        $response = array('success' => 0, 'msg' => 'Invalid Transporter information');
        $amount = (float) Input::get('amount');
        $driverid = Input::get('deliveryBoyId');

        $auth = User::find(new MongoId($driverid));
        //print_r($auth);die;
        if (count($auth) > 0) {
            if (!empty(trim(@$auth->StripeBankId))) {
                try {

                    $res = RSStripe::transfer($amount, $auth->StripeBankId);

                    if (isset($res['id'])) {

                        $insData = array(
                            'SendById' => Auth::user('admin')->_id,
                            'SendByName' => Auth::user('admin')->UserName,
                            'SendToId' => $auth->id,
                            'RecieveByName' => $auth->Name,
                            'TransactionId' => $res['id'],
                            // 'Comment' => $comment,
                            "Description" => "Amount transfer to " . $auth->Name,
                            "Credit" => 0,
                            "Debit" => $amount,
                            "Status" => "done",
                            "TransactionType" => "transfer",
                            'EnterOn' => new MongoDate(),
                        );

                        Transaction::insert($insData);
                        $response['success'] = 1;
                        $response['msg'] = "Transfer successfully.";
                    } else {
                        $response['msg'] = $res;
                    }
                } catch (Exception $e) {
                    $response['msg'] = $e - getMessage();
                }
            } else {
                $response['msg'] = 'Transporter bank account not attach to stripe.';
            }
        }
        echo json_encode($response);
    }

    public function transaction_history_driver()
    {
        $response = array("success" => 0);
        $deliveryboyId = Input::get('deliveryboyId');
        $data['user'] = User::find(new MongoId($deliveryboyId));

        $data['info_2'] = DeliveryRequest::where([
            'ProductList.tpid' => $deliveryboyId,
            'ProductList.status' => 'delivered',
            //  'ProductList.PaymentStatus' => 'yes', 58c12d277ac6f6a1248b4568
            'RequestType' => 'buy_for_me',
        ])->select('ProductList.$')->first();

        $data['info_3'] = DeliveryRequest::where([
            'ProductList.tpid' => $deliveryboyId,
            'ProductList.status' => 'delivered',
            //  'ProductList.PaymentStatus' => 'yes', 58c12d277ac6f6a1248b4568
            'RequestType' => 'online',
        ])->select('ProductList.$')->first();

        $data['info'] = DeliveryRequest::where([
            'TransporterId' => new Mongoid($deliveryboyId),
            //'TransporterId' => $deliveryboyId,
            'Status' => 'delivered',
            'PaymentStatus' => 'yes',
            'RequestType' => 'delivery',
        ])->get();

        return view('Admin::other.popup.transaction_history', $data);
    }

}
