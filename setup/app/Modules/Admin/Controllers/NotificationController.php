<?php namespace App\Modules\Admin\Controllers;

/*
 * Project: Aquantuo
 *
 */

use App\Http\Controllers\Controller;
use App\Http\Models\Admin;
use App\Http\Models\Notification;
use App\Http\Models\User;
use App\Library\Notify;
use Auth;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Input;
use MongoDate;
use MongoId;
use App\Http\Models\Comment;
use App\Http\Models\UserComment;
use App\Http\Models\Deliveryrequest;
use DB;

class NotificationController extends Controller
{

    public function __construct()
    {
        if (!(Auth::user())) {
            Redirect::to('admin/login')->send();
        }
    }
    public function notification()
    {
        if (!(Auth::user()->UserPermission & NOTIFICATION) == NOTIFICATION) {
            return Redirect::to('/admin/dashboard');
        }

        $data['title'] = "Notification Management List";
        $data['paginationurl'] = "pagination/notification";
        $data['orderType'] = "Desc";
        $data['orderby'] = "_id";
        $data['postvalue'] = "&search_value=" . Input::get('search_value') . "&StartDate=" . Input::get('StartDate') . "&EndDate=" . Input::get('EndDate');
        return view('Admin::list.notification', $data);
    }

    /*
     * User notification list sent to users
     *
     */

    public function pagination_notification()
    {
        $query = Notification::query();
        $countQuery = Notification::query();
        $data = $this->commonpaginationdata();

        $search_value = Input::get('search_value');

        if ($search_value != '') {
            $query->where('NotificationTitle', 'like', "%$search_value%");
            $countQuery->where('NotificationTitle', 'like', "%$search_value%");
        }

        $where = array();
        if (Input::get('StartDate') != '') {
            $startdate = Input::get('StartDate');
            $where['Date']['$gt'] = new MongoDate(strtotime(DateTime::createFromFormat('d/m/Y', $startdate)->format('d-m-Y')));

        }
        if (Input::get('EndDate') != '') {
            $enddate = Input::get('EndDate');
            $where['Date']['$lt'] = new MongoDate(strtotime(DateTime::createFromFormat('d/m/Y', $enddate)->format('d-m-Y 23:59:59')));

        }

        $data['notification'] = $query->where($where)->skip($data['start'])->take($data['per_page'])->orderBy('_id', 'desc')->get();

        $data['count'] = $countQuery->where($where)->count();

        $data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;
        return view('Admin::list.pagination.notification', $data);
    }

    /*
     * Used For load view of send notification
     *
     */

    public function send_notification()
    {
        if (!(Auth::user()->UserPermission & NOTIFICATION) == NOTIFICATION) {
            return Redirect::to('/admin/dashboard');
        }
        $data['paginationurl'] = "pagination/send-notification";
        $data['orderType'] = "Desc";
        $data['orderby'] = "UserId";
        $data['postvalue'] = "&status=" . Input::get('status') . "&StartDate=" . Input::get('StartDate') . "&EndDate=" . Input::get('EndDate');
        return view('Admin::list.send_notification', $data);
    }

    /*
     * User For send notification to users
     *
     */

    public function pagination_send_notification()
    {

        $query = User::query();
        $countQuery = User::query();
        $data = $this->commonpaginationdata();
        $search_value = Input::get('user_name');
        $where = array();

        if (Input::get('userGroup') == 'requester') {
            $where['UserType'] = array('$in' => array('both', 'requester'));

            if (!empty($search_value)) {
                $query->where($where)->where('Email', 'like', "%$search_value%")->orwhere('Name', 'like', "%$search_value%");
            }

            $data['users'] = $query->skip($data['start'])->take($data['per_page'])->orderBy('_id', 'desc')->get(array('Name', '_id', 'Email'));

            $data['count'] = $countQuery->count();
            $data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;

            return view('Admin::list.pagination.send_notification', $data);

        } else if (Input::get('userGroup') == 'transporter') {
            $where['UserType'] = array('$in' => array('both', 'transporter'));

            if (!empty($search_value)) {
                $query->where($where)->where('Email', 'like', "%$search_value%")->orwhere('Name', 'like', "%$search_value%");
            }

            $data['users'] = $query->skip($data['start'])->take($data['per_page'])->orderBy('_id', 'desc')->get(array('Name', '_id', 'Email'));

            $data['count'] = $countQuery->count();
            $data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;

            return view('Admin::list.pagination.send_notification', $data);
        } else {
            $where['UserType'] = array('$in' => array('both', 'transporter', 'requester'));

            if (!empty($search_value)) {
                $query->where($where)->where('Email', 'like', "%$search_value%")->orwhere('Name', 'like', "%$search_value%");
            }

            $data['users'] = $query->skip($data['start'])->take($data['per_page'])->orderBy('_id', 'desc')->get(array('Name', '_id', 'Email'));

            $data['count'] = $countQuery->count();
            $data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;

            return view('Admin::list.pagination.send_notification', $data);

        }

/*      $data['users'] = $query->where('NotificationId', '!=', '')->skip($data['start'])->take($data['per_page'])->orderBy('_id', 'desc')->get(array('FirstName', 'LastName', '_id', 'Email'));
$data['count'] = $countQuery->where('NotificationId', '!=', '')->count();
$data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;

return view('Admin::list.pagination.send_notification', $data);*/
    }

    public function pagination_send_promocode()
    {
        $query = User::query();
        $countQuery = User::query();
        $data = $this->commonpaginationdata();
        $search_value = Input::get('search_value');
        $where = array();

        if (Input::get('userGroup') == 'requester') {
            $where['UserType'] = array('$in' => array('both', 'requester'));
        } else if (Input::get('userGroup') == 'transporter') {
            $where['UserType'] = array('$in' => array('both', 'transporter'));
        }

        $data['users'] = $query->skip($data['start'])->take($data['per_page'])->orderBy('_id', 'desc')->get(array('FirstName', 'LastName', '_id'));
        $data['count'] = $countQuery->count();
        $data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;

        return view('Admin::list.pagination.promocode_detail', $data);
    }
    /*
     * Used For send notification to user
     *
     */
    public function post_notificatione(Request $request)
    {

        if (!(Auth::user()->UserPermission & NOTIFICATION) == NOTIFICATION) {
            return Redirect::to('/admin/dashboard');
        }

        $validation = Validator::make($request->all(), array('title' => 'required', 'message' => 'required'));
        if ($validation->fails()) {
            return Redirect::to("admin/send-notification")->withErrors($validation)->withInput();
        } else {
            $post = $request->all();

            $where = array();

            if (Input::get('groupto') == 'requester' && Input::get('chk_all_del') == 'all') {
                $where['UserType'] = array('$in' => array('requester', 'both'));
            } else if (Input::get('groupto') == 'transporter' && Input::get('chk_all_del') == 'all') {
                $where['UserType'] = array('$in' => array('transporter', 'both'));
            } else {
                if (Input::get('chk_all_del') != 'all') {
                    if (is_array(Input::get('chk_del'))) {
                        $userId = array();
                        foreach (Input::get('chk_del') as $key => $val) {
                            $where['_id']['$in'][] = new MongoId($val);
                        }
                    }
                }
            }


            //$where['NotificationId'] = array('$ne' => '');
            //echo '<pre>';
            // print_r($where); die;
            
            $userdata = User::where($where)
            ->where('NotificationId','!=','')
            ->get(array('DeviceType', 'NotificationId', 'DeviceId'));
            //echo "<pre>";
            //print_r($where); 

           // echo "<pre>";
           // print_r($userdata); die;

            $Notification = new Notify();

            $Notification->setValue('title', ucfirst($post['title']));
            $Notification->setValue('message', ucfirst($post['message']));
            $Notification->setValue('location', 'admin_information');
            $Notification->setValue('type', 'admin_information');

            /*$Notification->add_user("APA91bFWRjcs0BWC-83ffqV9ZMqaxP9yDhobKR6FuNcHc-rezlqx_fENPp9H15N2H1IrBQ9adzlQteAnj4TZIMGxbjbQF1sa3WusmuPG4RBmSvWoU6UnB3w", "android");

            $Notification->fire();
            echo 'send'; die();*/

            if (count($userdata) > 0) {
                $array = array(
                    'name' => 'aquantuo',
                    'image' => '',
                    'sender_id' => '',
                    'NotificationTitle' => ucfirst($post['title']),
                    'NotificationMessage' => ucfirst($post['message']),
                    'NotificationType' => 'ADMIN',
                    'NotificationUserId' => array(),
                    'Date' => new MongoDate(),
                );

                $iphonenotificationids = $androidnotificationids = array();
                foreach ($userdata as $key) {
                    if($key['NotificationId'] != ''){
                        $Notification->add_user($key['NotificationId'], $key['DeviceType']);
                        $array['NotificationUserId'][] = new MongoId($key['_id']);
                    }
                }


                try
                {
                    $Notification->fire();
                } catch (ApnsPHP_Message_Exception $e) {

                }

                if (count($array['NotificationUserId']) > 0) {
                    Notification::insert($array);
                }

                return Redirect::to('admin/notification')->withSuccess('Notification Sent Successfully.');
            } else {

                return Redirect::to('admin/send-notification')->withSuccess('Oops! Something went wrong.');
            }
        }
    }

    /*
     * Used For  list of user notification sent
     *
     */
    public function view_user($id)
    {
        if (!(Auth::user()->UserPermission & CARRIER) == CARRIER) {
            return Redirect::to('/admin/dashboard');
        }
        $data['paginationurl'] = "pagination/view-user/" . $id;
        $data['orderType'] = "Desc";
        $data['orderby'] = "_id";
        $data['postvalue'] = "&search_value=" . Input::get('search_value');
        return view('Admin::list.view_user', $data);
    }
    /*
     * User For user list for notification
     *
     */
    public function pagination_view_user($id)
    {
        $query = User::query();
        $countQuery = User::query();
        $data = $this->commonpaginationdata();
        $userId = Notification::where(array('_id' => $id))->select('NotificationUserId')->first();

        $search_value = Input::get('search_value');

        if ($search_value != '') {
            $query->where('Name', 'like', "%$search_value%");
            $countQuery->where('Name', 'like', "%$search_value%");
        }
        $data['users'] = [];
        $data['count'] = 0;
        if (count($userId->NotificationUserId) > 0) {

            $field = array('Email', 'Name', 'PhoneNo');
            $data['users'] = $query->whereIn('_id', $userId->NotificationUserId)
                ->skip($data['start'])->take($data['per_page'])
                ->orderBy('_id', 'desc')->get($field);
            $data['count'] = $countQuery->whereIn('_id', $userId->NotificationUserId)->count();
        }

        $data['no_of_paginations'] = ($data['count'] > 0) ? ceil($data['count'] / $data['per_page']) : 0;
        return view('Admin::list.pagination.view_user', $data);
    }
    /*
     * User For create pagination
     *
     */
    public function commonpaginationdata()
    {
        //print_r(Auth::user()->PerPage); die;
        if (Auth::user()->PerPage == '') {
            Auth::user()->PerPage = 20;
        }

        $data['cur_page'] = $data['page'] = Input::get('page');
        $data['page'] -= 1;
        $data['per_page'] = Auth::user()->PerPage; //Input::get('Pnum');
        $data['orderby'] = Input::get('orderby');
        $data['orderType'] = Input::get('orderType');
        $data['previous_btn'] = true;
        $data['next_btn'] = true;
        $data['first_btn'] = true;
        $data['last_btn'] = true;
        $data['sno'] = ($data['start'] = $data['page'] * $data['per_page']) + 1;

        return $data;
    }

    public function comment_list($id) {

        $data['comment'] = Deliveryrequest::where(['_id' => $id])->first();
        $data['detail_url'] = '#';
        if($data['comment']->RequestType == 'online'){
            $data['detail_url'] = 'admin/online_package/detail/'.$id;
        }else if($data['comment']->RequestType =='buy_for_me'){
            $data['detail_url'] = 'admin/buy-for-me/detail/'.$id;
        }else if($data['comment']->RequestType =='delivery'){
            $data['detail_url'] = 'admin/package/detail/'.$id;
        }
        
        $data['id'] = $id;
        $data['title'] = "Comment List";
        $data['paginationurl'] = "pagination/comment-list/" . $id;
        $data['orderType'] = "Desc";
        $data['orderby'] = "_id";
        $data['postvalue'] = "&search_value=" . Input::get('search_value') . "&StartDate=" . Input::get('StartDate') . "&EndDate=" . Input::get('EndDate');

        return view('Admin::list.comment_list', $data);
    }

    public function post_comment(Request $request) {
        if (!(Auth::user()->UserPermission & ADMIN) == ADMIN) {
            return Redirect::to('/admin/dashboard');
        }
        $validate = Validator::make($request->all(), array(
            'comment' => 'required',
        ));
        if ($validate->fails()) {
            return Redirect::back()->withErrors($validate)->withInput();
        } else {
            $userInfo = Auth::user();
            $insertArray = [
                "user_id" => $request['user_id'],
                'request_id' => $request['request_id'],
                'admin_id' => $userInfo->_id,
                'by_comment' => $userInfo->UserName,
                'product_title' => $request['product_title'],
                'comment' => $request['comment'],
                "Date" => new MongoDate(),
            ];

            Comment::insert($insertArray);
            return Redirect::back()->withSuccess('Comment added successfully.');
        }
    }

    public function user_comment_list($id) {
        $data['comment'] = User::where(['_id' => $id])->first();
        $data['id'] = $id;
        $data['title'] = "Comment List";
        $data['paginationurl'] = "pagination/user-comment-list/" . $id;
        $data['orderType'] = "Desc";
        $data['orderby'] = "_id";
        $data['postvalue'] = "";

        return view('Admin::list.user_comment_list', $data);
    }

    public function post_user_comment(Request $request) {
        if (!(Auth::user()->UserPermission & ADMIN) == ADMIN) {
            return Redirect::to('/admin/dashboard');
        }
        $validate = Validator::make($request->all(), array(
            'comment' => 'required',
        ));
        if ($validate->fails()) {
            return Redirect::back()->withErrors($validate)->withInput();
        } else {
            $userInfo = Auth::user();
            $insertArray = [
                "user_id" => $request['user_id'],
                'request_id' => $request['request_id'],
                'admin_id' => $userInfo->_id,
                'by_comment' => $userInfo->UserName,
                'product_title' => $request['product_title'],
                'comment' => $request['comment'],
                "Date" => new MongoDate(),
            ];

            UserComment::insert($insertArray);
            return Redirect::back()->withSuccess('Comment added successfully.');
        }
    }

}
