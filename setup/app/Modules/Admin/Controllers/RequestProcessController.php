<?php
namespace App\Modules\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Models\Activitylog;
use App\Http\Models\Admin;
use App\Http\Models\Category;
use App\Http\Models\Configuration;
use App\Http\Models\Deliveryrequest;
use App\Http\Models\Distance;
use App\Http\Models\Itemhistory;
use App\Http\Models\Notification;
use App\Http\Models\SendMail;
use App\Http\Models\User;
use App\Library\Notification\Pushnotification;
use App\Library\Notify;
use App\Library\Reqhelper;
use App\Library\Requesthelper;
use App\Library\NewRequesthelper;
use App\Library\Utility;
use App\Library\WebActivityLog;
use App\Modules\User\Controllers\Calculation;
use App\Modules\User\Controllers\NewCalculation;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Input;
use MongoDate;
use MongoId,Session;
use App\Http\Models\PaymentInfo;
use App\Library\Promo;
use App\Library\NewEmail;
use App\Library\SapBuinessdays;

class RequestProcessController extends Controller {
	public function __construct() {
		if (!(Auth::user())) {
			Redirect::to('admin/login')->send();
		}
	}

	public function validate_promocode() 
	{
		$response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		$res = Promo::get_validate(Input::get('promocode'), Input::get('shipping_cost'), Input::get('total_amount'), Session::get('UserId'), Session::get('Default_Currency'));
		$response['msg'] = $res['msg'];
		if ($res['success'] == 1) {
			$response['success'] = 1;
			$response['result'] = $res;
		}
		return response()->json($response);
	}


	public function update_item_info(Request $request){
		$responce = [
			'success' => 0,
			'msg' => 'Oops! Something went wrong.',
			'reminder_button' => false,
			'assing_tp_button' => false,
		];

		$valid = Validator::make(Input::get(), [
			'requestid' => 'required', 'itemid' => 'required',
			'item_price' => 'required',
		]);

		if (!$valid->fails()) {
			$where = [
				"ProductList" => ['$elemMatch' => [
					'_id' => $request->get('itemid'),
				]],
			];
			$configurationdata = Configuration::where(['_id' => '5673e33e6734c4f874685c84'])->first();
			$delinfo = Deliveryrequest::where($where)
				->select('ProductList','DeliveryCountry', 'RequesterId', 'discount_in_percent', 'ProductTitle', 'distance', 'RequestType', 'PackageNumber','BeforePurchaseTotalCost','TotalCost','consolidate_item','AreaCharges','after_update_difference','need_to_pay','discount','PromoCode','shippingCost','insurance','ProcessingFees','shipping_cost_by_user','total_item_price')
				->first();
			$category = Category::where(['_id'=> trim(Input::get('category'))])->first();

			if (count($delinfo) > 0 && count($category) > 0) {
				$userInfo = User::where(array('_id' => $delinfo->RequesterId))->first();
				if (count($userInfo) <= 0) {
					return ['success' => 0, 'msg' => 'Oops! Issue with user'];
				} else {
					$req_data = [
						'request_id' => trim($request->get('requestid')),
						'item_id' => trim($request->get('itemid')),
						'requester_id' => trim($delinfo->RequesterId),
						'ProductList' => $delinfo->ProductList,
						'type' => $delinfo->RequestType,
						'update_difference'=> 0
					];
					$metric_unit = 'cm';
					$imperial_unit = 'kg';
					if (Input::get('measurement_units') == 'inches_lbs') {
						$metric_unit = 'inches';
						$imperial_unit = 'lbs';
					}
					$update_list = $delinfo->ProductList;
					foreach ($delinfo->ProductList as $productKey) {
						if ($productKey['_id'] == trim($request->get('itemid'))) {
							/*Before  Updation item total cost*/
							//$old_cost = $productKey["total_cost"];
							/*End*/
							$package_id = $productKey["package_id"];
							$productKey['actual_count'] = Input::get('actual_count');
							$productKey["price"] = floatval(Input::get('item_price'));
							$productKey["product_name"] = Input::get('item_name');
							$productKey["length"] = Input::get('item_length');
							$productKey["lengthUnit"] = $metric_unit;
							$productKey["width"] = Input::get('item_width');
							$productKey["widthUnit"] = $metric_unit;
							$productKey["height"] = Input::get('item_height');
							$productKey["heightUnit"] = $metric_unit;
							$productKey["weight"] = Input::get('item_weight');
							$productKey["weight_unit"] = $imperial_unit;
							$productKey["marketname"] = Input::get('purchase_from');
							$productKey["url"] = Input::get('purchase_from');
							$productKey["shipping_cost_by_user"] = floatval(Input::get('shipping_cost_by_user'));
							$productKey["qty"] = Input::get('qty1');
							$productKey["travelMode"] = trim(Input::get('travel_mode'));
							$item_name = Input::get('item_name');
							$productKey["category"] = $category->Content;
							$productKey["categoryid"] = $category->_id;
							if (Input::file('item_image')) {
								$extension = Input::file('item_image')->getClientOriginalExtension(); // getting image extension
								$fileName = time() . rand(100, 999) . '.' . $extension; // Rename img image
								if (Input::file('item_image')->move(BASEURL_FILE . 'package/', $fileName)) {
									$productKey['image'] = "package/" . $fileName;
								}
							}
						}
						$productList[] = $productKey;
					}
					

					$calculation = new NewCalculation();
					$data = [
						'shippingcost' => 0,
						'insurance' => 0,
						'ghana_total_amount' => 0,
						'total_amount' => 0,
						'error' => [],
						'formated_text' => '',
						'distance_in_mile' => 0,
						'product_count' => 0,
						'ProcessingFees' => 0,
						'item_cost' => 0,
						'shipping_cost_by_user' => 0,
						'type' => ($delinfo->RequestType == 'buy_for_me') ? 'buyforme' : $delinfo->RequestType,
						'userType' => 'admin',
						'distance' => (object) ['distance' => $delinfo->distance],
						'distance_calculated'=> true,
						'consolidate_check'=> trim($delinfo->consolidate_item),
						'is_customs_duty_applied' =>false,
					];
					
					if (in_array($delinfo->RequestType, ['buy_for_me', 'online'])) {
						if (@$delinfo->DeliveryCountry != "USA") {
							$data["is_customs_duty_applied"] = true;
						}
					}
					
					$info = $calculation->GetRate($productList, $data, $userInfo);
					
					
					if ($info['success'] == 1) {
						$productList2 = $info['product'];
						$shippingCost = 0; $insurance = 0; $ProcessingFees =0; $updateTotalCost = 0; $shipping_cost_by_user = 0;
						$newShippingCost = 0;
						$newinsurance = 0;
						if ($delinfo->RequestType == 'buy_for_me') {
							$ProcessingFees = $delinfo->ProcessingFees;
							$shipping_cost_by_user = $delinfo->shipping_cost_by_user;
							$updateTotalCost = $delinfo->total_item_price + $delinfo->shipping_cost_by_user + $delinfo->shippingCost + $delinfo->insurance + $delinfo->ProcessingFees;
						} else {
							$updateTotalCost =  $delinfo->shippingCost + $delinfo->insurance;
						}
						foreach ($productList2 as $key => $val) {
							$shippingCost += $val['shippingCost'];
							$insurance += $val['insurance'];

							
							//$aq_fee = Reqhelper::get_aquantuo_fees(@$val['shippingCost']);

							
							if ($delinfo->RequestType == 'buy_for_me') {
								// $ProcessingFees += $val['ProcessingFees'];
								// $shipping_cost_by_user += @$val['shipping_cost_by_user'];
								// $updateTotalCost += (floatval(@$val['price']) * $val['qty']) + @$val['shipping_cost_by_user'] + $val['shippingCost'] + $val['insurance'] + $val['ProcessingFees'] ;
								
							}else{
								// $updateTotalCost +=  $val['shippingCost'] + $val['insurance'];
							} 
							
							if ($val['_id'] == trim($request->get('itemid'))) {
								if ($val['status'] != "purchased" && $val['status'] != "item_received") {
									$productList2[$key]['status'] = 'paid';
									$productList2[$key]['PaymentStatus'] = 'yes';
								}
								$newShippingCost = $val['shippingCost'];
								$newinsurance = $val['insurance'];
							}
						}
						
						
						$updateTotalCost += $delinfo->AreaCharges+$info['DutyAndCustom']+$info['Tax'];
						$updateTotalCost = $updateTotalCost - $delinfo->discount;
						$currentTotalCost = $delinfo->TotalCost + $delinfo->after_update_difference;
						$diference = $updateTotalCost - $currentTotalCost;
						
						if($diference > 0.1){
							
							$old_diff =  $delinfo->after_update_difference;
							$new_diff =  $old_diff + $diference;

							$req_data['update_difference'] = $new_diff - $old_diff;
							$delinfo->need_to_pay = $delinfo->need_to_pay + $req_data['update_difference'];
                           	

							$delinfo->after_update_difference =  $delinfo->after_update_difference + $diference;


							
							if ($delinfo->RequestType == 'buy_for_me') {
								$link = url("buy-for-me-detail/{$delinfo->_id}");
							}else{
								$link = url("online-request-detail/{$delinfo->_id}");
							}

                            send_mail('5886f3986befd91046cf465d', [
                                "to" => @$userInfo->Email,
                                "replace" => [
                                "[USERNAME]" => @$userInfo->Name,
                                "[PACKAGETITLE]" => @$item_name,
                                "[PACKAGEID]" => @$package_id,
                                "[URL]" => "<a href='{$link}' >{$link}</a>",
                                ],
                            ]);

                            User::where(array('_id' => $userInfo->_id))->update(['detail_url'=> $link,'temprary'=>'temprary']);
						}

						PaymentInfo::insert([
							'request_id' => $delinfo->_id,
							'user_id'=> $userInfo->_id,
							'action_user_id'=> Auth::user()->_id,
							'RequestType'=> $delinfo->RequestType,
							'action' => 'item_update',
							'item_id'=> trim($request->get('itemid')),
							'item_unic_number' => $package_id,
							'TotalCost'=> floatval($currentTotalCost),
							'shippingCost'=> floatval($newShippingCost),
							'insurance'=> floatval($newinsurance),
							'AreaCharges'=> floatval($delinfo->AreaCharges),
							'ProcessingFees'=> floatval($delinfo->ProcessingFees),
							'shipping_cost_by_user'=> floatval(@$shipping_cost_by_user),
							'item_cost'=> floatval(@$delinfo->total_item_price),
							'discount'=> floatval($delinfo->discount),
							'PromoCode'=> $delinfo->PromoCode,
							'difference'=> floatval($diference),
							'difference_before'=> floatval($delinfo->after_update_difference - $diference),
							'date'=> New MongoDate(),
						]);

						$delinfo->ProductList = $productList2;
						$delinfo->save();
						$msg_activity = 'Aquantuo will purchase your item shortly.';
						if ($delinfo->RequestType == 'online') {
							$msg_activity = 'Aquantuo reviewed your listing to reflect the correct weight.';
							
						}
						$insertactivity = [
							'request_id' => Input::get('requestid'),
							'request_type' => $delinfo->RequestType,
							'PackageNumber' => $delinfo->PackageNumber,
							'item_id' => Input::get('itemid'),
							'package_id' => $package_id,
							'item_name' => $item_name,
							'log_type' => 'request',
							'message' => $msg_activity,
							'status' => "paid",
							'action_user_id' => Auth::user()->_id,
							'EnterOn' => new MongoDate(),

						];

						Activitylog::insert($insertactivity);
						

						$this->addItemHistory($req_data);
						Reqhelper::update_status(Input::get('requestid'));
						$responce = [
							'success' => 1,
							'msg' => 'Item has been updated successfully.',
							'current' => '',
						];

						Notification::insert([
							"NotificationTitle" => "Item Updated",
							"NotificationMessage" => sprintf('Item titled:"%s", ID:%s has been updated.', ucfirst(@$item_name), @$package_id),
							"NotificationUserId" => [],
							"NotificationReadStatus" => 0,
							"location" => "delivery",
							"locationkey" => (String) @$delinfo->_id,
							"Date" => new MongoDate(),
							"GroupTo" => "Admin",
						]);
						

					}else {
						$responce['msg'] = $info['msg'];
					}
				}
			}
		}
		return response()->json($responce);
	}

	public function update_item_info__(Request $request) {
		$responce = [
			'success' => 0,
			'msg' => 'Oops! Something went wrong.',
			'reminder_button' => false,
			'assing_tp_button' => false,
		];

		$valid = Validator::make(Input::get(), [
			'requestid' => 'required', 'itemid' => 'required',
			'item_price' => 'required',
		]);

		if (!$valid->fails()) {
			$where = [
				"ProductList" => ['$elemMatch' => [
					'_id' => $request->get('itemid'),
				]],
			];
			$configurationdata = Configuration::where(['_id' => '5673e33e6734c4f874685c84'])->first();

			$delinfo = Deliveryrequest::where($where)
				->select('ProductList', 'RequesterId', 'discount_in_percent', 'ProductTitle', 'distance', 'RequestType', 'PackageNumber')
				->first();
			if (count($delinfo) > 0) {
				$userInfo = User::where(array('_id' => $delinfo->RequesterId))->first();
				if (count($userInfo) <= 0) {
					return ['success' => 0, 'msg' => 'Oops! Issue with user'];
				} else {
					$req_data = [
						'request_id' => trim($request->get('requestid')),
						'item_id' => trim($request->get('itemid')),
						'requester_id' => trim($delinfo->RequesterId),
						'ProductList' => $delinfo->ProductList,
						'type' => $delinfo->RequestType,
					];

					$metric_unit = 'cm';
					$imperial_unit = 'kg';
					if (Input::get('measurement_units') == 'inches_lbs') {
						$metric_unit = 'inches';
						$imperial_unit = 'lbs';
					}
					$productList = [];

					$update_list = $delinfo->ProductList;
					foreach ($delinfo->ProductList as $productKey) {
						if ($productKey['_id'] == trim($request->get('itemid'))) {
							/*Before  Updation item total cost*/
							$old_cost = $productKey["total_cost"];
							/*End*/

							$item_name = $productKey["product_name"];
							$package_id = $productKey["package_id"];

							$productKey["price"] = floatval(Input::get('item_price'));
							$productKey["product_name"] = Input::get('item_name');
							$productKey["length"] = Input::get('item_length');
							$productKey["lengthUnit"] = $metric_unit;
							$productKey["width"] = Input::get('item_width');
							$productKey["widthUnit"] = $metric_unit;
							$productKey["height"] = Input::get('item_height');
							$productKey["heightUnit"] = $metric_unit;
							$productKey["weight"] = Input::get('item_weight');
							$productKey["weight_unit"] = $imperial_unit;
							$productKey["marketname"] = Input::get('purchase_from');
							$productKey["shipping_cost_by_user"] = floatval(Input::get('shipping_cost_by_user'));
							$productKey["qty"] = Input::get('qty1');
							$productList[] = $productKey;
						}
					}

					$Notification = new Notify();
					$calculation = new Calculation();
					$data = [
						'shippingcost' => 0,
						'insurance' => 0,
						'ghana_total_amount' => 0,
						'total_amount' => 0,
						'error' => [],
						'formated_text' => '',
						'distance_in_mile' => 0,
						'product_count' => 0,
						'ProcessingFees' => 0,
						'item_cost' => 0,
						'shipping_cost_by_user' => 0,
						'type' => ($delinfo->RequestType == 'buy_for_me') ? 'buyforme' : $delinfo->RequestType,
						'userType' => 'admin',
						'distance' => (object) ['distance' => $delinfo->distance],
						'consolidate_check'=>''
					];

					$info = $calculation->getRate($productList, $data, $userInfo);

					if ($info['success'] == 1) {
						$productList2 = $info['data']['product'];

						foreach ($productList2 as $productKey => $productVal) {
							if ($productVal['_id'] == trim($request->get('itemid'))) {

								if ($delinfo->RequestType == 'buy_for_me') {
									$fees = $productVal["ProcessingFees"];
								} else {
									$fees = ((floatval($productVal['insurance'] + $productVal['shippingCost']) * $configurationdata->aquantuoFees) / 100);
								}

								$shippingCost = $productVal["shippingCost"];
								$insurance = $productVal["insurance"];
								if ($delinfo->RequestType == 'buy_for_me') {
									$shipping_cost_by_user = $productVal["shipping_cost_by_user"];
									$ProcessingFees = $productVal["ProcessingFees"];
									$after_update = $productVal["ProcessingFees"] + $productVal["shippingCost"] + $productVal["shipping_cost_by_user"] + $productVal["insurance"] + ($productVal["price"] * $productVal["qty"]);
									$after_update = number_format($after_update,2);
								} else {
									$productList2[$productKey]['after_update'] = $productVal["shippingCost"] + $productVal["insurance"] + $fees;
									$after_update = $productVal["shippingCost"] + $productVal["insurance"] + $fees;
									$after_update = number_format($after_update,2);
								}
								$dif2 = $after_update - $productVal['total_cost'];
								if ($dif2 > 0.1) {
									$update_status = 'not_purchased';
									$PaymentStatus = 'no';
								} else {
									$update_status = 'paid';
									$PaymentStatus = 'yes';
								}

							}
						}

						foreach ($delinfo->ProductList as $Key => $v) {
							if ($v['_id'] == trim($request->get('itemid'))) {

								$update_list[$Key]['aq_fee'] = $fees;

								$update_list[$Key]['status'] = $update_status;
								$update_list[$Key]['PaymentStatus'] = $PaymentStatus;
								$update_list[$Key]['after_update'] = $after_update;

								$update_list[$Key]['after_update'] = $after_update;
								$update_list[$Key]['shippingCost'] = $shippingCost;
								$update_list[$Key]['insurance'] = $insurance;

								if ($delinfo->RequestType == 'buy_for_me') {
									$update_list[$Key]['shipping_cost_by_user'] = $productVal["shipping_cost_by_user"];
									$update_list[$Key]['ProcessingFees'] = $ProcessingFees;
								}

								$update_list[$Key]["price"] = floatval(Input::get('item_price'));
								$update_list[$Key]["product_name"] = Input::get('item_name');
								$update_list[$Key]["length"] = Input::get('item_length');
								$update_list[$Key]["lengthUnit"] = $metric_unit;
								$update_list[$Key]["width"] = Input::get('item_width');
								$update_list[$Key]["widthUnit"] = $metric_unit;
								$update_list[$Key]["height"] = Input::get('item_height');
								$update_list[$Key]["heightUnit"] = $metric_unit;
								$update_list[$Key]["weight"] = Input::get('item_weight');
								$update_list[$Key]["weight_unit"] = $imperial_unit;
								$update_list[$Key]["marketname"] = Input::get('purchase_from');
								$update_list[$Key]["shipping_cost_by_user"] = floatval(Input::get('shipping_cost_by_user'));
								$update_list[$Key]["qty"] = Input::get('qty1');

							}
						}

						$res = Deliveryrequest::where($where)->update([
							'ProductList' => $update_list,
						]);

						if ($res) {

							$this->addItemHistory($req_data);
							Reqhelper::update_status(Input::get('requestid'));
							$delinfo_all = Deliveryrequest::where($where)->select('after_update_difference')->first();
							$dif = $after_update - $old_cost;
							if ($dif > 0.1) {
								$new = $delinfo_all->after_update_difference + $dif;
								$delinfo_all->after_update_difference = floatval($new);

								if ($delinfo->RequestType == 'buy_for_me') {
									$link = url("buy-for-me-detail/{$delinfo_all->_id}");
								}else{
									$link = url("online-request-detail/{$delinfo_all->_id}");
								}

                                send_mail('5886f3986befd91046cf465d', [
                                    "to" => @$userInfo->Email,
                                    "replace" => [
                                    "[USERNAME]" => @$userInfo->Name,
                                    "[PACKAGETITLE]" => @$item_name,
                                    "[PACKAGEID]" => @$package_id,
                                    "[URL]" => "<a href='{$link}' >{$link}</a>",
                                    ],
                                ]);

                                User::where(array('_id' => $userInfo->_id))->update(['detail_url'=> $link,'temprary'=>'temprary']);
								
							}
							$delinfo_all->save();
							$msg_activity = 'Aquantuo will purchase your item shortly.';
							if ($delinfo->RequestType == 'online') {
								$msg_activity = 'Aquantuo reviewed your listing to reflect the correct weight.';
								
							}

							$insertactivity = [
								'request_id' => Input::get('requestid'),
								'request_type' => $delinfo->RequestType,
								'PackageNumber' => $delinfo->PackageNumber,
								'item_id' => Input::get('itemid'),
								'package_id' => $package_id,
								'item_name' => $item_name,
								'log_type' => 'request',
								'message' => $msg_activity,
								'status' => $update_status,
								'action_user_id' => Auth::user()->_id,
								'EnterOn' => new MongoDate(),

							];

							Activitylog::insert($insertactivity);
							$responce = [
								'success' => 1,
								'msg' => 'Item has been updated successfully.',
								'current' => $res,
							];
						}

					} else {
						$responce['msg'] = $info['msg'];
					}
				}
			}
		}

		return response()->json($responce);
	}

	public function addItemHistory($data) {
		if (!empty($data['ProductList'])) {
			foreach ($data['ProductList'] as $key) {
				if ($key['_id'] == $data['item_id']) {

					$insertData = [
						'request_id' => $data['request_id'],
						'item_id' => $key['_id'],
						'request_type' => $data['type'],
						'package_id' => $key['package_id'],
						'tpid' => $key['tpid'],
						'tpName' => $key['tpName'],
						'marketid' => @$key['marketid'],
						'marketname' => @$key['marketname'],
						'txnid' => @$key['txnid'],
						'product_name' => $key['product_name'],
						'url' => @$key['url'],
						'price' => @$key['price'],
						'qty' => @$key['qty'],
						'description' => @$key['description'],
						'travelMode' => $key['travelMode'],
						'shipping_cost_by_user' => @$key['shipping_cost_by_user'],
						'weight' => @$key['weight'],
						'weight_unit' => @$key['weight_unit'],
						'height' => @$key['height'],
						'heightUnit' => @$key['heightUnit'],
						'length' => @$key['length'],
						'lengthUnit' => @$key['lengthUnit'],
						'width' => @$key['width'],
						'widthUnit' => @$key['widthUnit'],
						'category' => @$key['category'],
						'categoryid' => @$key['categoryid'],
						'insurance_status' => @$key['insurance_status'],
						'TransporterFeedbcak' => @$key['TransporterFeedbcak'],
						'TransporterRating' => @$key['TransporterRating'],
						'RequesterRating' => @$key['RequesterRating'],
						'RejectBy' => @$key['RejectBy'],
						'ReturnType' => @$key['ReturnType'],
						'ReceiptImage' => @$key['ReceiptImage'],
						'RejectTime' => @$key['RejectTime'],
						'TrackingNumber' => @$key['TrackingNumber'],
						'TransporterMessage' => @$key['TransporterMessage'],
						'CancelDate' => @$key['CancelDate'],
						'StripeChargeId' => @$key['StripeChargeId'],
						'DeliveredDate' => @$key['DeliveredDate'],
						'discount' => @$key['discount'],
						'insurance' => @$key['insurance'],
						'shippingCost' => @$key['shippingCost'],
						'aq_fee' => @$key['aq_fee'],
						'status' => @$key['status'],
						'PaymentStatus' => @$key['PaymentStatus'],
						'verify_code' => @$key['verify_code'],
						'updated_at' => new MongoDate(),
						'update_difference'=> $data['update_difference'],
					];

					Itemhistory::Insert($insertData);

				}
			}
		}
	}

	public function addItemHistory2($data) {
		if (!empty($data['ProductList'])) {
			foreach ($data['ProductList'] as $key) {
				if ($key['_id'] == $data['item_id']) {
					$insertData = [
						'request_id' => $data['request_id'],
						'item_id' => $key['_id'],
						'request_type' => $data['type'],
						"product_name" => $key['product_name'],
						"package_id" => $key['package_id'],
						"status" => $key['status'],
						"productWidth" => $key['productWidth'],
						"productHeight" => $key['productHeight'],
						"productLength" => $key['productLength'],
						"productCost" => $key['productCost'],
						"productWeight" => $key['productWeight'],
						"productHeightUnit" => $key['productHeightUnit'],
						"ProductWeightUnit" => $key['ProductWeightUnit'],
						"ProductLengthUnit" => $key['ProductLengthUnit'],
						"productCategory" => $key['productCategory'],
						"productCategoryId" => $key['productCategoryId'],
						"travelMode" => @$key['travelMode'],
						"InsuranceStatus" => $key['InsuranceStatus'],
						"productQty" => $key['productQty'],
						"ProductImage" => $key['ProductImage'],
						"QuantityStatus" => $key['QuantityStatus'],
						"BoxQuantity" => @$key['BoxQuantity'],
						"Description" => $key['Description'],
						"PackageMaterial" => $key['PackageMaterial'],
						//	"PackageMaterialShipped" => $key['PackageMaterialShipped'],
						"shippingCost" => $key['shippingCost'],
						"InsuranceCost" => $key['InsuranceCost'],
						"DeliveryVerifyCode" => $key['DeliveryVerifyCode'],
						"tpid" => $key['tpid'],
						"tpName" => $key['tpName'],
						"inform_mail_sent" => $key['inform_mail_sent'],
						"PaymentStatus" => $key['PaymentStatus'],
						"total_cost" => $key['total_cost'],
						"after_update" => $key['after_update'],
						"EnterOn" => @$key['EnterOn'],
						'updated_at' => new MongoDate(),
						'update_difference'=> $data['update_difference'],
						//	"ExpectedDate" => $key['ExpectedDate'],
					];
					Itemhistory::Insert($insertData);
				}
			}
		}
	}

	public function loaclDeliveryProcess(Request $request) {
		$responce = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		$valid = Validator::make(Input::get(), [
			'requestid' => 'required', 'itemid' => 'required', 'type' => 'required',
		]);

		if (!$valid->fails()) {
			$where = ["ProductList" => ['$elemMatch' => ['_id' => $request->get('itemid')]], '_id' => $request->get('requestid')];

			$delinfo = Deliveryrequest::where($where)
				->select('_id', 'ProductList', 'RequesterId', 'ProductTitle', 'RequestType', 'PackageNumber','PickupAddress','DeliveryFullAddress')
				->first();

			$userInfo = User::where(['_id' => $delinfo->RequesterId])->first();
			if (!$userInfo) {
				$responce['msg'] = 'Oops! Problem with requester.';
				return response()->json($responce);die;
			}

			if (count($delinfo) > 0) {
				if ($request->get('type') == 'review') {
					$update_status = 'reviewed';
					$msg = 'Item has been reviewed successfully.';
					$Activitymsg = 'Listing Reviewed.';
				} else if ($request->get('type') == 'cancel') {
					$update_status = 'cancel_by_admin';
					$msg = 'Item has been cancel successfully.';
					$Activitymsg = 'Item has been cancel';
				} else if ($request->get('type') == 'received') {
					$update_status = 'item_received';
					$msg = 'Item has been received.';
					$Activitymsg = 'Item received';
				}
				$update_array = $delinfo->ProductList;
				$productName = '';
				//echo $request->get('itemid');die;
				foreach ($update_array as $key => $value) {
					if ($value['_id'] == trim($request->get('itemid'))) {
						$productName = $value['product_name'];
						$update_array[$key]['status'] = $update_status;

						/* Activity Log  send a package review */
						$insertactivity = [
							'request_id' => $request->get('requestid'),
							'request_type' => 'local_delivery',
							'PackageNumber' => $delinfo->PackageNumber,
							'item_id' => $request->get('itemid'),
							'package_id' => $value['package_id'],
							'item_name' => $value['product_name'],
							'log_type' => 'request',
							'message' => $Activitymsg,
							'status' => $update_status,
							'action_user_id' => Auth::user()->_id,
							'EnterOn' => new MongoDate(),
						];
						Activitylog::insert($insertactivity);

					}
				}
				$update = Deliveryrequest::where($where)->update(['ProductList' => $update_array]);
				if ($update) {
					Reqhelper::update_status2($request->get('requestid'));

					// $Email = New NewEmail();
					if ($request->get('type') == 'review') {
						send_mail('589c485f7ac6f6c1248b4567', [
							"to" => $userInfo->Email,
							"replace" => [
								"[USERNAME]" => $userInfo->Name,
								"[PACKAGETITLE]" => @$productName,
								"[PACKAGEID]" => @$delinfo->ProductList[0]['package_id'],
								"[SOURCE]" => @$delinfo->PickupAddress,
								"[DESTINATION]" => @$delinfo->DeliveryFullAddress,
								"[PACKAGENUMBER]" => @$delinfo->ProductList[0]['package_id'],
								"[SHIPPINGCOST]" => @$delinfo->ProductList[0]['shippingCost'],
							],
						]);
					} elseif ($request->get('type') == 'received') {
						send_mail('5d5a3e39360d5c142a2135f9', [
							"to" => $userInfo->Email,
							"replace" => [
								"[USERNAME]" => $userInfo->Name,
								"[PACKAGETITLE]" => $productName,
								"[SOURCE]" => @$delinfo->PickupAddress,
								"[DESTINATION]" => @$delinfo->DeliveryFullAddress,
							],
						]);
					}
					
					$responce['msg'] = $msg;
					$responce['success'] = 1;
				}
			}
			return response()->json($responce);die;
		}
	}

	public function send_package_dellivery_process(Request $request) {
		$responce = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		$valid = Validator::make(Input::get(), [
			'requestid' => 'required', 'itemid' => 'required', 'type' => 'required',
		]);

		if (!$valid->fails()) {
			$where = ["ProductList" => ['$elemMatch' => ['_id' => $request->get('itemid')]], '_id' => $request->get('requestid')];

			$delinfo = Deliveryrequest::where($where)
				->select('ProductList', 'RequesterId', 'ProductTitle', 'RequestType', 'PackageNumber')
				->first();

			$userInfo = User::where(['_id' => $delinfo->RequesterId])->first();

			if (!$userInfo) {
				$responce['msg'] = 'Oops! Problem with requester.';
				return response()->json($responce);die;
			}

			if (count($delinfo) > 0) {
				if ($request->get('type') == 'review') {
					$update_status = 'reviewed';
					$msg = 'Item has been reviewed successfully.';
					$Activitymsg = 'Product reviewed';
				} else if ($request->get('type') == 'cancel') {
					$update_status = 'ready';	// cancel_by_admin
					$msg = 'Item has been cancel successfully.';
					$Activitymsg = 'Item has been cancel';
					$tpid = '';
					$tpName = '';
				} else if ($request->get('type') == 'item_received') {
					$update_status = 'item_received';
					$msg = 'Item has been received successfully.';
					$Activitymsg = 'Item received';
				}
				$update_array = $delinfo->ProductList;
				foreach ($update_array as $key => $value) {
					if ($value['_id'] == trim($request->get('itemid'))) {
						$update_array[$key]['status'] = $update_status;
						$update_array[$key]['tpid'] = $tpid;
						$update_array[$key]['tpName'] = $tpName;

						/* Activity Log  send a package review */
						$insertactivity = [
							'request_id' => $request->get('requestid'),
							'request_type' => 'delivery',
							'PackageNumber' => $delinfo->PackageNumber,
							'item_id' => $request->get('itemid'),
							'package_id' => $value['package_id'],
							'item_name' => $value['product_name'],
							'log_type' => 'request',
							'message' => $Activitymsg,
							'status' => $update_status,
							'action_user_id' => Auth::user()->_id,
							'EnterOn' => new MongoDate(),
						];

						Activitylog::insert($insertactivity);
					}
				}

				$update = Deliveryrequest::where($where)->update(['ProductList' => $update_array]);
				if ($update) {
					Reqhelper::update_status2($request->get('requestid'));
					$responce['msg'] = $msg;
					$responce['success'] = 1;
				}
			}
			return response()->json($responce);die;
		}
	}

	public function sendPackageItemUpdate(Request $request) {
		$responce = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		$valid = Validator::make(Input::get(), [
			'requestid' => 'required', 'itemid' => 'required',
		]);

		if (!$valid->fails()) {
			$where = ["ProductList" => ['$elemMatch' => ['_id' => $request->get('itemid')]], '_id' => $request->get('requestid')];
			$delinfo = Deliveryrequest::where($where)
				->select('ProductList', 'RequesterId', 'ProductTitle', 'RequestType', 'PackageNumber', 'PickupLatLong', 'DeliveryLatLong', 'after_update_difference', 'need_to_pay', 'AquantuoFees', 'TotalCost')
				->first();

			$userInfo = User::where(['_id' => $delinfo->RequesterId, 'delete_status' => 'no'])->first();
			if (!$userInfo) {
				$responce['msg'] = 'Oops! Problem with requester.';
				return response()->json($responce);die;
			}

			$lhwunit = 'cm';
			$weightunit = 'kg';
			if (Input::get('measurement_units') == 'inches_lbs') {
				$lhwunit = 'inches';
				$weightunit = 'lbs';
			}
			$configurationdata = Configuration::where(['_id' => '5673e33e6734c4f874685c84'])->first();

			$distance = Utility::getDistanceBetweenPointsNew($delinfo['PickupLatLong'][1], $delinfo['PickupLatLong'][0], $delinfo['DeliveryLatLong'][1], $delinfo['DeliveryLatLong'][0]);
			if ($delinfo) {

				$req_data = [
					'request_id' => trim($request->get('requestid')),
					'item_id' => trim($request->get('itemid')),
					'requester_id' => trim($delinfo->RequesterId),
					'ProductList' => $delinfo->ProductList,
					'type' => 'delivery',
					'update_difference'=>0,
				];

				$total_before_update = $delinfo->TotalCost;
				foreach ($delinfo->ProductList as $key => $value) {
					if ($value['_id'] == trim($request->get('itemid'))) {
						$travelMode = $value['travelMode'];
						$productCategoryId = $value['productCategoryId'];
						$productCategory = $value['productCategory'];
						// $total_before_update = $value['total_cost'];

						/* Activity Log  send a package update item */
						$insertactivity = [
							'request_id' => $request->get('requestid'),
							'request_type' => 'delivery',
							'PackageNumber' => $delinfo->PackageNumber,
							'item_id' => $request->get('itemid'),
							'package_id' => $value['package_id'],
							'item_name' => $value['product_name'],
							'log_type' => 'request',
							'message' => 'Item Updated.',
							'status' => $value['status'],
							'action_user_id' => Auth::user()->_id,
							'EnterOn' => new MongoDate(),

						];
						Activitylog::insert($insertactivity);
					}
				}
				// Requesthelper
				$requesthelper = new NewRequesthelper(
					[
						"needInsurance" => (Input::get('InsuranceStatus') == 'yes') ? true : false,
						"productQty" => (Input::get('productQty') < 1) ? 1 : Input::get('productQty'),
						"productCost" => Input::get('productCost'),
						"productWidth" => Input::get('productWidth'),
						"productWidthUnit" => $lhwunit,
						"productHeight" => Input::get('productHeight'),
						"productHeightUnit" => $lhwunit,
						"productLength" => Input::get('productLength'),
						"productLengthUnit" => $lhwunit,
						"productWeight" => Input::get('productWeight'),
						"productWeightUnit" => $weightunit,
						"productCategory" => $productCategory,
						"productCategoryId" => $productCategoryId,
						"distance" => $distance,
						"travelMode" => $travelMode,
						"currency" => "GHS",
					]
				);

				$requesthelper->calculate();
				$response['calculationinfo'] = $requesthelper->get_information();
				
				if (isset($response['calculationinfo']->error)) {
					$responce['msg'] = $response['calculationinfo']->error;
					return response()->json($responce);die;
				}

				$total_after_updation = $response['calculationinfo']->shippingcost + $response['calculationinfo']->insurance;

				//aq fees
				$fees = ((floatval($response['calculationinfo']->shippingcost + $response['calculationinfo']->insurance) * $configurationdata->aquantuoFees) / 100);
				$total_after_updation = $total_after_updation + $fees;
				$diference = 0;
				
				if ($total_after_updation > $total_before_update) {
					$diference = $total_after_updation - $total_before_update;
					$status_update = 'not_purchased';
					$payment_status = 'no';
					$inform_mail = 'no';
				} else {
					$status_update = 'purchased';
					$inform_mail = 'yes';
					$payment_status = 'yes';
				}

				$update_array = $delinfo->ProductList;
				foreach ($update_array as $key => $value) {
					if ($value['_id'] == trim($request->get('itemid'))) {

						// if ($diference > 0) {
							$update_array[$key]['product_name'] = trim($request->get('product_name'));
							$update_array[$key]['productCost'] = trim($request->get('productCost'));
							$update_array[$key]['productWeight'] = trim($request->get('productWeight'));
							$update_array[$key]['productHeight'] = trim($request->get('productHeight'));
							$update_array[$key]['productLength'] = trim($request->get('productLength'));
							$update_array[$key]['productWidth'] = trim($request->get('productWidth'));
							$update_array[$key]['InsuranceStatus'] = trim($request->get('InsuranceStatus'));
							$update_array[$key]['productHeightUnit'] = $lhwunit;
							$update_array[$key]['ProductWeightUnit'] = $weightunit;
							$update_array[$key]['ProductLengthUnit'] = $lhwunit;
							$update_array[$key]['shippingCost'] = floatval($response['calculationinfo']->shippingcost);
							$update_array[$key]['InsuranceCost'] = floatval($response['calculationinfo']->insurance);
							$update_array[$key]['after_update'] = $response['calculationinfo']->shippingcost + $response['calculationinfo']->insurance + $fees;
							$update_array[$key]['aq_fees'] = floatval($fees);
						// }

						// $update_array[$key]['status'] = $status_update;
						$update_array[$key]['status'] = $value["status"];
						$update_array[$key]['PaymentStatus'] = $payment_status;
						$update_array[$key]['actual_count'] = Input::get('actual_count');
					}
				}

				$after_update_difference = $delinfo->after_update_difference;
				$AquantuoFees = $delinfo->AquantuoFees;
				$update_fees = 0;
				/*After updation Aquantuo fees*/
				if ($diference > 0) {
					$after_update_difference = $delinfo->after_update_difference + $diference;
					$delinfo->need_to_pay = $delinfo->need_to_pay + $diference;
					$total_cost = $after_update_difference + $delinfo->TotalCost;
					$AquantuoFees = $delinfo->AquantuoFees + $fees;
					$req_data['update_difference'] = $diference;
				}
				/*End*/

				if (Deliveryrequest::where($where)->update(['ProductList' => $update_array, 'after_update_difference' => floatval($after_update_difference), 'AquantuoFees' => floatval($AquantuoFees), "need_to_pay" => floatval($delinfo->need_to_pay)])) {
					$this->addItemHistory2($req_data);
					Reqhelper::update_status2($request->get('requestid'));
					$responce = ['success' => 1, 'msg' => 'Item has been updated successfully.'];

				}
			}

		}
		return response()->json($responce);die;
	}

	public function localDeliveryupdateItem(Request $request) {
		$responce = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		$valid = Validator::make(Input::get(), [
			'requestid' => 'required', 'itemid' => 'required',
		]);

		if (!$valid->fails()) {
			$where = ["ProductList" => ['$elemMatch' => ['_id' => $request->get('itemid')]], '_id' => $request->get('requestid')];

			$delinfo = Deliveryrequest::where($where)
				->select('ProductList', 'RequesterId', 'ProductTitle', 'RequestType', 'PackageNumber', 'PickupLatLong', 'DeliveryLatLong', 'after_update_difference', 'AquantuoFees', 'TotalCost','need_to_pay')
				->first();

			$userInfo = User::where(['_id' => $delinfo->RequesterId, 'delete_status' => 'no'])->first();
			if (!$userInfo) {
				$responce['msg'] = 'Oops! Problem with requester.';
				return response()->json($responce);die;
			}

			$lhwunit = 'cm';
			$weightunit = 'kg';
			if (Input::get('measurement_units') == 'inches_lbs') {
				$lhwunit = 'inches';
				$weightunit = 'lbs';
			}

			$req_data = [
				'request_id' => trim($request->get('requestid')),
				'item_id' => trim($request->get('itemid')),
				'requester_id' => trim($delinfo->RequesterId),
				'ProductList' => $delinfo->ProductList,
				'type' => 'local_delivery',
				'update_difference'=>0
			];

			if ($delinfo) {

				$distance = Utility::getDistanceBetweenPointsNew($delinfo['PickupLatLong'][1], $delinfo['PickupLatLong'][0], $delinfo['DeliveryLatLong'][1], $delinfo['DeliveryLatLong'][0]);

				foreach ($delinfo->ProductList as $key => $value) {
					if ($value['_id'] == trim($request->get('itemid'))) {

						$productCategoryId = $value['productCategoryId'];
						$productCategory = $value['productCategory'];
						$old_total = $value['total_cost'];
						$item_name = $value['product_name'];
						$packageId = $value['package_id'];
					}
				}

				$in_array = [
					'categoryid' => $productCategoryId,
					'productCost' => trim($request->get('productCost')),
					'productQty' => trim($request->get('productQty')),
					'distance' => $distance,
					'countryid' => '56bdbfb4cf32079714ee89ca',
					'insurance' => trim($request->get('InsuranceStatus')),

				];

				$res = $this->localDeliveryPriceEstimator($in_array);
				if ($res['success'] == 1) {

					$update_array = $delinfo->ProductList;
					foreach ($delinfo->ProductList as $key => $value) {
						if ($request->get('itemid') == $value['_id']) {
							//if ($res['total_cost'] - $old_total > 0) {
								//$update_array[$key]['status'] = 'not_purchased';
								//$update_array[$key]['PaymentStatus'] = 'no';
								//$update_array[$key]['inform_mail_sent'] = 'yes';
							//} else {
								//$update_array[$key]['status'] = 'purchased';
								//$update_array[$key]['PaymentStatus'] = 'yes';
								//$update_array[$key]['inform_mail_sent'] = 'no';
							//}
							//$update_array[$key]['status'] = 'purchased';

							/*if ($res['total_cost'] - $old_total > 0) {*/
								$update_array[$key]['product_name'] = trim($request->get('product_name'));
								$update_array[$key]['productCost'] = trim($request->get('productCost'));
								$update_array[$key]['productWeight'] = trim($request->get('productWeight'));
								$update_array[$key]['productHeight'] = trim($request->get('productHeight'));
								$update_array[$key]['productLength'] = trim($request->get('productLength'));
								$update_array[$key]['productWidth'] = trim($request->get('productWidth'));
								$update_array[$key]['InsuranceStatus'] = trim($request->get('InsuranceStatus'));
								$update_array[$key]['productHeightUnit'] = $lhwunit;
								$update_array[$key]['ProductWeightUnit'] = $weightunit;
								$update_array[$key]['ProductLengthUnit'] = $lhwunit;
								$update_array[$key]['shippingCost'] = $res['ShippingCost'];
								if ($request->get('InsuranceStatus') == 'yes') {
									$update_array[$key]['InsuranceCost'] = $res['InsuranceCost'];
								}
								$update_array[$key]['after_update'] = $res['total_cost'];
								$update_array[$key]['actual_count'] = $request->get('actual_count');
							/*}*/
						}
					}

					$difference = $res['total_cost'] - $old_total;
					$delinfo->ProductList = $update_array;
					
					if ($difference > 0.1) {
						$old_diff =  $delinfo->after_update_difference;
						$new_diff =  $old_diff + $difference;
						$update_difference = $new_diff - $old_diff;
						$delinfo->after_update_difference = $delinfo->after_update_difference + $difference;
						$delinfo->need_to_pay = $delinfo->need_to_pay + $update_difference;
						$req_data['update_difference'] = $update_difference;
						$link = url("local-delivery-detail/{$delinfo->_id}");
					
						// $Email = New NewEmail();
		                send_mail('5886f3986befd91046cf465d', [
		                    "to" => @$userInfo->Email,
		                    "replace" => [
		                    "[USERNAME]" => @$userInfo->Name,
		                    "[PACKAGETITLE]" => @$item_name,
		                    "[PACKAGEID]" => @$packageId,
		                    "[URL]" => "<a href='{$link}' >{$link}</a>",
		                    ],
		                ]);

		                User::where(array('_id' => $userInfo->_id))->update(['detail_url'=> $link,'temprary'=>'temprary']);
					}


					$delinfo->save();

					$insertactivity = [
						'request_id' => $delinfo->_id,
						'request_type' => 'local_delivery',
						'PackageNumber' => $delinfo->PackageNumber,
						'item_id' => $request->get('itemid'),
						'package_id' => $packageId,
						'item_name' => $item_name,
						'log_type' => 'request',
						'message' => 'Item Updated.',
						'status' => $update_array[$key]['status'],
						'action_user_id' => Auth::user()->_id,
						'EnterOn' => new MongoDate(),
					];

					Activitylog::insert($insertactivity);

					$this->addItemHistory2($req_data);
					Reqhelper::update_status2($delinfo->_id);


					

					$responce = ['success' => 1, 'msg' => 'Item has been updated successfully.'];
					return response()->json($responce);
				} else {
					$responce['msg'] = $res['msg'];
					return response()->json($responce);die;
				}
			}
		}
		return response()->json($responce);die;
	}

	public function localDeliveryPriceEstimator($data) {
		$response['success'] = 0;
		$response['msg'] = '';
		//Category calculation
		if ($data['categoryid']) {
			$match_category = Category::where(['_id' => $data['categoryid'], 'country_id' => $data['countryid'], 'Status' => 'Active', 'type' => 'localCategory'])
				->select('price')
				->first();

			if ($match_category) {
				$response['success'] = 1;
				$response['category_price'] = $match_category->price;
			} else {
				$response = ['success' => 0, 'msg' => 'The shipping category you have specified is unsupported.'];
			}
		}

		//Item value calculation
		if ($data['productCost'] != '' && $response['success'] == 1) {
			$match_value = Distance::Where('from', '<=', (float) $data['productCost'] * $data['productQty'])->where('to', '>=', (float) $data['productCost'] * $data['productQty'])->where(['Status' => 'Active', 'country_id' => $data['countryid'], "type" => "item-value"])
				->select('price')
				->first();
			//	print_r($match_value);die;
			if ($match_value) {
				$response['success'] = 1;
				$response['item_price'] = $match_value->price;
			} else {
				$response = ['success' => 0, 'msg' => 'The item value you have specified is unsupported.'];
			}
		} else { $response = ['success' => 0, 'msg' => 'The item value you have specified is unsupported.'];}

		//Distance calculation
		if ($data['distance'] > 0 != '' && $response['success'] == 1) {
			//Distance in to miles
			$data['distance'] = $data['distance'] * 0.000621371;

			$match_distance = Distance::Where('from', '<=', (float) $data['distance'])->where('to', '>=', (float) $data['distance'])
				->where(['Status' => 'Active', 'country_id' => $data['countryid'], "type" => "distance"])
				->select('price')
				->first();

			if ($match_distance) {
				$response['success'] = 1;
				$response['distance_price'] = $match_distance->price;
			} else {
				$response = ['success' => 0, 'msg' => 'The shipping address/distance you have specified is unsupported.'];
			}
		} else { $response = ['success' => 0, 'msg' => 'The shipping address/distance you have specified is unsupported.'];}

		if ($response['success'] == 1) {
			$response['ShippingCost'] = $response['distance_price'] + $response['category_price'] + $response['item_price'];
			$response['InsuranceCost'] = 0;
			if ($data['insurance'] == 'yes') {
				$response['InsuranceCost'] = $this->get_insurance($data['productCost'], $data['productQty']);
			}

			$response['total_cost'] = $response['ShippingCost'] + $response['InsuranceCost'];

		}

		return $response;
	}

	public function get_insurance($cost, $quantity) {
		$data = Configuration::where(array('_id' => '5673e33e6734c4f874685c84'))
			->select('Insurance')->first();
		$value = 0;
		if (count($data) > 0) {
			if (is_array($data->Insurance)) {
				foreach ($data->Insurance as $key) {
					if ($key['MinPrice'] <= (float) $cost && $key['MaxPrice'] >= (float) $cost) {
						$value = $key['Rate'] * (int) $quantity;
					}
				}
			}
		}
		return $value;
	}

	public function get_aquantuo_fees($totalcost) {
		$fees = 0;
		$configurationdata = Configuration::where(['_id' => '5673e33e6734c4f874685c84'])->first();
		if ($configurationdata) {
			$fees = ((floatval($totalcost) * $configurationdata->aquantuoFees) / 100);
		}
		return $fees;
	}

	public function sendPackageAssignTransporter(Request $request) {

		$responce = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		$obj = json_decode($request->get('tp_name'));
		if (!empty($obj)) {

			$where = array("ProductList" => array('$elemMatch' => array('_id' => $request->get('product_id'))));

			$deliveryInfo = Deliveryrequest::where(array('_id' => $request->get('request_id')))->where($where)->select('ProductList', 'RequesterId', 'PaymentStatus', 'RequesterName', 'ProductTitle', 'PackageNumber', 'DeliveryFullAddress', 'TotalCost', 'Status', 'StripeChargeId', 'StripeChargeId2', 'RequestType')->first();

			$transporter_data = User::find($obj->id);

			if (count($deliveryInfo) > 0 && count($transporter_data) > 0) {
				$userinfo = User::find($deliveryInfo->RequesterId);
				if (count($userinfo) <= 0) {
					$responce['msg'] = 'Requester not found';
					return json_encode($responce);
					die;
				}

				$update_array = $deliveryInfo->ProductList;
				foreach ($update_array as $key => $value) {
					if ($value['_id'] == $request->get('product_id')) {
						$productName = $value['product_name'];
						$productprice = $value['productCost'];
						$insuranceStatus = $value['InsuranceCost'];

						$update_array[$key]['tpid'] = (string) $obj->id;
						$update_array[$key]['tpName'] = $obj->name;
						$update_array[$key]['status'] = 'assign';

						/* Activity Log */
						$insertactivity = [
							'request_id' => $request->get('request_id'),
							'request_type' => 'delivery',
							'PackageNumber' => $deliveryInfo->PackageNumber,
							'item_id' => $value['_id'],
							'package_id' => $value['package_id'],
							'item_name' => $value['product_name'],
							'tpid' => (string) $obj->id,
							'tpName' => $obj->name,
							'log_type' => 'request',
							'message' => 'Transporter assign.',
							'status' => 'assign',
							'action_user_id' => Auth::user()->_id,
							'EnterOn' => new MongoDate(),
						];
						Activitylog::insert($insertactivity);
					}
				}

				$updation = Deliveryrequest::where($where)
					->where(array('_id' => $request->get('request_id')))
					->update(['ProductList' => $update_array]);
				Reqhelper::update_status2($deliveryInfo->_id);
				/*		send_mail('57daa7fa7ac6f6a3218b4567', [
						"to" => $userinfo->Email,
						"replace" => [
							"[USERNAME]" => $userinfo->Name,
							"[PACKAGETITLE]" => $productName,
							"[TRANSPORTER]" => $transporter_data->Name,

						],
					]
					);

					send_mail('587396736befd9212ec39eba', [
						"to" => $transporter_data->Email,
						"replace" => [
							"[USERNAME]" => $transporter_data->Name,
							"[PACKAGETITLE]" => $productName,
							"[PRICE]" => $productprice,
							"[INSURANCE]" => $insuranceStatus,

						],
					]
					);
				*/
				if ($updation) {
					$responce = ['success' => 1, 'msg' => 'Transporter assigned successfully.', 'count' => 0];
				}

			}
		}
		return json_encode($responce);
	}

	public function localDeliveryassign(Request $request) {

		$responce = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		$obj = json_decode($request->get('tp_name'));
		if (!empty($obj)) {


			$where = array("ProductList" => array('$elemMatch' => array('_id' => $request->get('product_id'), 'status' => 'reviewed')));

			$deliveryInfo = Deliveryrequest::where($where)->select('ProductList', 'RequesterId', 'PaymentStatus', 'RequesterName', 'ProductTitle', 'PackageNumber', 'DeliveryFullAddress', 'TotalCost', 'Status', 'StripeChargeId', 'StripeChargeId2', 'RequestType')->first();

			$transporter_data = User::find($obj->id);

			//echo count($deliveryInfo);die;

			if (count($deliveryInfo) > 0 && count($transporter_data) > 0) {
				//echo "''string34''";die;
				$userinfo = User::find($deliveryInfo->RequesterId);
				if (count($userinfo) <= 0) {
					$responce['msg'] = 'Requester not found';
					return json_encode($responce);
					die;
				}

				$update_array = $deliveryInfo->ProductList;
				foreach ($update_array as $key => $value) {
					if ($value['_id'] == $request->get('product_id')) {
						$productName = $value['product_name'];
						$productprice = $value['productCost'];
						$insuranceStatus = $value['InsuranceCost'];
						$package_id = $value['package_id'];

						$update_array[$key]['tpid'] = (string) $obj->id;
						$update_array[$key]['tpName'] = $obj->name;
						$update_array[$key]['status'] = 'assign';

						/* Activity Log */
						$insertactivity = [
							'request_id' => $request->get('request_id'),
							'request_type' => 'local_delivery',
							'PackageNumber' => $deliveryInfo->PackageNumber,
							'item_id' => $value['_id'],
							'package_id' => $value['package_id'],
							'item_name' => $value['product_name'],
							'tpid' => (string) $obj->id,
							'tpName' => $obj->name,
							'log_type' => 'request',
							'message' => 'Transporter assign.',
							'status' => 'assign',
							'action_user_id' => Auth::user()->_id,
							'EnterOn' => new MongoDate(),
						];
						Activitylog::insert($insertactivity);
					}
				}

				$updation = Deliveryrequest::where($where)
					->where(['_id' => $request->get('request_id')])
					->update(['ProductList' => $update_array]);


				

				if (count($updation) > 0) {
					Reqhelper::update_status2($deliveryInfo->_id);

					//Inform to  transporter
					$Notification = new Pushnotification();
					$Email = new NewEmail();

					$Notification->setValue('title', trans('lang.ITEM_ASSIGN_TP_TITLE'));
		            $Notification->setValue('message', sprintf(trans('lang.ITEM_ASSIGN_TP_TRP'), $productName, $package_id));
		            $Notification->setValue('type', 'LOCAL_TRANSPORTER_ASSIGN');
		            $Notification->setValue('location', 'LOCAL_TRANSPORTER_ASSIGN');
		            $Notification->setValue('locationkey', $deliveryInfo->_id);
		            $Notification->setValue('itemId', $request->get('product_id'));
		            $Notification->add_user($transporter_data->NotificationId, $transporter_data->DeviceType);
		            $Notification->fire();

		            $Email->send_mail('587396736befd9212ec39eba', [
						"to" => $transporter_data->Email,
						"replace" => [
							"[USERNAME]" => $transporter_data->Name,
							"[PACKAGETITLE]" => $productName,
							"[PRICE]" => number_format($productprice, 2),
							"[INSURANCE]" => ucfirst($insuranceStatus),

						],
					]);
		            //
					

					//inform to user

					$Email->send_mail('57daa7fa7ac6f6a3218b4567', [
						"to" => $userinfo->Email,
						"replace" => [
							"[USERNAME]" => $userinfo->Name,
							"[PACKAGETITLE]" => $productName,
							"[TRANSPORTER]" => $transporter_data->Name,
						],
					]);
					
					$Notification->setValue('title', trans('lang.ITEM_ASSIGN_TP_TITLE'));
					$Notification->setValue('message', sprintf(trans('lang.ITEM_ASSIGN_TP_USER'), $productName,
						$package_id));

					$Notification->setValue('type', 'LOCAL_TRANSPORTER_ASSIGN_FOR_USER');
					$Notification->setValue('location', 'LOCAL_TRANSPORTER_ASSIGN_FOR_USER');
					$Notification->setValue('locationkey', $deliveryInfo->_id);
					$Notification->setValue('itemId', $request->get('product_id'));
					$Notification->add_user($userinfo->NotificationId, $userinfo->DeviceType);
					$Notification->fire();
						
					//


					$responce = ['success' => 1, 'msg' => 'Transporter assigned successfully.', 'count' => 0];
				}

			}
		}

		return json_encode($responce);
	}

	public function sendPackageTraProcess() {
		$responce = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		if (Input::get('type') != '' && Input::get('itemid') != '' && Input::get('requestid') != '') {
			$res_msg = 'Status has been changed successfully';
			if (Input::get('type') == 'out_for_pickup') {
				$old_status = 'accepted';
				$update_status = 'out_for_pickup';
				$requester_email = '5f6366b656783d0a30eaa1f8';
				$res_msg = "Success! Pick up Scheduled/Package enroute to Aquantuo’s facility."; //Item is in destination country going through customs and sorting.";
			} else if (Input::get('type') == 'shipment_departed') {
				$old_status = 'out_for_pickup';
				$update_status = 'shipment_departed';
				$requester_email = '5f637c2756783d0040eaa1f5';
				$res_msg = trans('lang.SHIPMENT_DEPARTED_MSG');
			} else if (Input::get('type') == 'out_for_delivery') {
				$old_status = 'shipment_departed';
				$update_status = 'out_for_delivery';
				$requester_email = '5f63688a56783d2934eaa1f4';
				$res_msg = "Success! Your package is en route to be delivered.";
			} else if (Input::get('type') == 'delivered') {
				$old_status = 'out_for_delivery';
				$update_status = 'delivered';
				$requester_email = '5f63690256783d2934eaa1f5';
				$res_msg = "Success! Package has been successfully delivered.";
			}

			$where = [
				'_id' => trim(Input::get('requestid')),
				"ProductList" => ['$elemMatch' => [
					'_id' => Input::get('itemid'),
					'status' => $old_status,
				]],
			];

			$deliveryInfo = Deliveryrequest::where($where)->first();
			if ($deliveryInfo) {
				$requester = User::where('_id', '=', $deliveryInfo->RequesterId)->first();
				if ($requester) {
					$packageid = '';
					$item_title = '';
					$shippingCost = 0;
					$total = 0;
					foreach ($deliveryInfo->ProductList as $key => $value) {
						if ($value['_id'] == trim(Input::get('itemid'))) {
							$packageid = $value['package_id'];
							$item_title = $value['product_name'];
							$shippingCost = $value['shippingCost'];
							$total = $value['total_cost'];
						}
					}

					$DeliveryStatus = Deliveryrequest::where($where)
						->update(['ProductList.$.status' => $update_status]);

					if ($DeliveryStatus) {
						Reqhelper::update_status2(Input::get('requestid'));
						$where2 = [
							'_id' => trim(Input::get('requestid')),
							"ProductList" => ['$elemMatch' => [
								'_id' => Input::get('itemid'),
							]],
						];

						$deliveryRequest = Deliveryrequest::where(['_id' => Input::get('requestid')])->where($where2)->select('_id', 'ProductList.$', 'RequesterId','RequestType','PackageNumber')->first();
						$deliveryRequest->action_user_id = Auth::user()->_id;
						// dd($deliveryRequest);
						$Activity = new WebActivityLog();
						$Activity->ActivityLog($deliveryRequest);

						if ($requester->EmailStatus == "on") {
							$cron_mail = [
								"USERNAME" => ucfirst($requester->Name),
								"PACKAGETITLE" => ucfirst($deliveryRequest['ProductList'][0]['product_name']),
								"PACKAGEID" => $deliveryRequest['ProductList'][0]['package_id'],
								"SOURCE" => $deliveryInfo->PickupAddress,
								"DESTINATION" => $deliveryInfo->DeliveryAddress,
								"DROPOFFADDRESS" => $deliveryInfo->DeliveryFullAddress,
								"PICKUPOFFADDRESS" => $deliveryInfo->PickupFullAddress,
								"PACKAGENUMBER" => $packageid,
								"SHIPPINGCOST" => $shippingCost,
								"TOTALCOST" => $total,
								'DATE' => show_date($deliveryRequest['ProductList'][0]['ExpectedDate']),
								'email_id' => $requester_email,
								'email' => $requester->Email,
								'status' => 'ready',
							];
							SendMail::insert($cron_mail);
						}

						if ($requester->NoficationStatus == "on" && !empty($requester->NotificationId)) {
							$Notification = new Pushnotification();
							if (Input::get('type') == 'out_for_pickup') {
								$Notification->setValue('title', trans('lang.SEND_OUTFORPICKUP_TITLE'));
								$Notification->setValue('message', trans('lang.SEND_OUTFORPICKUP_MSG'));
								$Notification->setValue('type', 'requester_delivery_detail');
								$Notification->setValue('locationkey', trim(Input::get('requestid')));
								$Notification->add_user($requester->NotificationId, $requester->DeviceType);
								$Notification->fire();
							} else if (Input::get('type') == 'shipment_departed') {
								$Notification->setValue('title', trans('lang.SHIPMENT_DEPARTED_TITLE'));
								$Notification->setValue('message', trans('lang.SHIPMENT_DEPARTED_MSG'));
								$Notification->setValue('type', 'requester_delivery_detail');
								$Notification->setValue('locationkey', trim(Input::get('requestid')));
								$Notification->add_user($requester->NotificationId, $requester->DeviceType);
								$Notification->fire();
							} else if (Input::get('type') == 'out_for_delivery') {
								$Notification->setValue('title', trans('lang.SEND_OUTFORDELIVERY_TITLE'));
								$Notification->setValue('message', sprintf(trans('lang.SEND_OUTFORDELIVERY_MSG'), ucfirst($deliveryRequest['ProductList'][0]['product_name']), $deliveryRequest->PackageId));
								$Notification->setValue('type', 'requester_delivery_detail');
								$Notification->setValue('locationkey', trim(Input::get('requestid')));
								$Notification->add_user($requester->NotificationId, $requester->DeviceType);
								$Notification->fire();
							} else if (Input::get('type') == 'delivered') {
								$Notification->setValue('title', trans('lang.REQUEST_DELIVERED'));
								$Notification->setValue('message', sprintf(trans('lang.MSG_REQUEST_DELIVERED'), "Aquantuo", ucfirst($deliveryRequest['ProductList'][0]['product_name'])));
								$Notification->setValue('type', 'requester_delivery_detail');
								$Notification->setValue('locationkey', trim(Input::get('requestid')));
								$Notification->add_user($requester->NotificationId, $requester->DeviceType);
								$Notification->fire();
							}
						}
						$responce = ['success' => 1, 'msg' => $res_msg];
					}
				}
			}
		}
		return json_encode($responce);
	}

	public function sendPackageTraAccept() {
		$responce = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		if (Input::get('itemid') != '' && Input::get('requestid') != '') {
			$where = [
				'_id' => trim(Input::get('requestid')),
				"ProductList" => ['$elemMatch' => [
					'_id' => Input::get('itemid'),
					'status' => "ready",
				]],
			];

			$deliveryInfo = Deliveryrequest::where($where)->first();

			if ($deliveryInfo) {
				$requester = User::where('_id', '=', $deliveryInfo->RequesterId)->first();
				if ($requester) {

					$packageid = '';
					$item_title = '';
					$shippingCost = 0;
					$total = 0;
					$expectedDate = '';

					foreach ($deliveryInfo->ProductList as $key => $value) {
						if ($value['_id'] == trim(Input::get('itemid'))) {
							// Expected delivery date.
							$startdate = strtotime(date("Y-m-d"));
							$Buinessdays = new SapBuinessdays();
							$result = $Buinessdays->getWorkingDays($startdate);

							if (@$value['travelMode'] == 'air') {
								$expectedDate = new Mongodate(strtotime('+' . $result . 'days'));
							} else {
								$expectedDate = new Mongodate(strtotime('+ 63 days'));
							}
							

							$packageid = $value['package_id'];
							$item_title = $value['product_name'];
							$shippingCost = $value['shippingCost'];
							$total = $value['total_cost'];
						}
					}

					$DeliveryStatus = Deliveryrequest::where($where)
						->update(['ProductList.$.status' => "accepted", 'ProductList.$.tpid' => "admin", 'ProductList.$.tpName' => "admin", 'ProductList.$.ExpectedDate' => $expectedDate]);

					if ($DeliveryStatus) {
						Reqhelper::update_status2(Input::get('requestid'));
						$insertactivity = [
							'request_id' => $deliveryInfo->RequesterId,
							'request_type' => $deliveryInfo->RequestType,
							'PackageNumber' => $deliveryInfo->PackageNumber,
							'item_id' => Input::get('itemid'),
							'package_id' => $packageid,
							'item_name' => $item_title,
							'log_type' => 'request',
							'message' => 'Item accepted.',
							'status' => 'accepted',
							'action_user_id' => Auth::user()->_id,
							'EnterOn' => new MongoDate(),
						];
						Activitylog::insert($insertactivity);

						$cron_mail = [
							"USERNAME" => ucfirst($requester->Name),
							"PACKAGETITLE" => ucfirst($item_title),
							"PACKAGEID" => $packageid,
							"SOURCE" => $deliveryInfo->PickupFullAddress,
							"DESTINATION" => $deliveryInfo->DeliveryFullAddress,
							"PACKAGENUMBER" => $packageid,
							"SHIPPINGCOST" => $shippingCost,
							"TOTALCOST" => $total,
							'email_id' => '57d7d4b37ac6f69c158b4569',
							'email' => $requester->Email,
							'status' => 'ready',
						];

						SendMail::insert($cron_mail);

						//Notification to requester

						if ($requester->NoficationStatus == "on" && !empty($requester->NotificationId)) {
							$Notification = new Pushnotification();
							$Notification->setValue('title', trans('lang.SEND_ACCEPT_TITLE'));
							$Notification->setValue('message', sprintf(trans('lang.SEND_ACCEPT_MSG'), "Admin", $item_title));
							$Notification->setValue('type', 'requester_delivery_detail');	// request_detail
							$Notification->setValue('itemId', Input::get('itemid'));
							$Notification->setValue('locationkey', Input::get('requestid'));
							$Notification->add_user($requester->NotificationId, $requester->DeviceType);
							$Notification->fire();
						}

						Notification::Insert([
							array(
								"NotificationTitle" => trans('lang.SEND_ACCEPT_TITLE'),
								"NotificationShortMessage" => 'Request accepted successfully.',
								"NotificationMessage" => sprintf(trans('lang.SEND_ACCEPT_MSG'), "Admin", $item_title),
								"NotificationType" => "request",
								"NotificationUserId" => array(new MongoId($requester->_id)),
								"Date" => new MongoDate(),
								"GroupTo" => "User",
							),
						]);

						$responce = ['success' => 1, 'msg' => 'Product has been accepted successfully.'];
					}

				}

			}
		}
		return json_encode($responce);
	}

	public function payment_log($id)
	{
		$data['paginationurl'] = "pagination/payment-log/".$id;
		$data['orderType'] = "Desc";
		$data['orderby'] = "UserId";
		$data['postvalue'] = "&search_value=" . Input::get('search_value')."&StartDate=".Input::get('StartDate')."&EndDate=".Input::get('EndDate');
		$data['request_info'] =  Deliveryrequest::where(['_id'=>$id])->select('RequesterId','PackageNumber','ProductTitle','RequestType')->first();
		if(count($data['request_info']) > 0){
			$data['user_info'] = User::where(['_id'=>$data['request_info']->RequesterId])->first();
			$data['detail_url'] = '#';
			if($data['request_info']->RequestType == 'online'){
				$data['detail_url'] = 'admin/online_package/detail/'.$id;
			}else if($data['request_info']->RequestType =='buy_for_me'){
				$data['detail_url'] = 'admin/buy-for-me/detail/'.$id;
			}else if($data['request_info']->RequestType =='delivery'){
				$data['detail_url'] = 'admin/package/detail/'.$id;
			}
		}

		return view('Admin::list.payment_log', $data);
	}

	public function cancel_package($id)
	{
		$request_info =  Deliveryrequest::where(['_id'=>$id])->first();
		if(count($request_info) > 0){
			$request_info->Status = 'cancel_by_admin';
			if(isset($request_info->ProductList)){
				$ProductList = $request_info->ProductList;
				foreach ($request_info->ProductList as $key => $value) {
					$ProductList[$key]['status'] = 'cancel_by_admin';
				}
				$request_info->ProductList = $ProductList;
			}
			if($request_info->save()){

				$insertactivity = [
					'request_id' => $request_info->_id,
					'request_type' => $request_info->RequestType,
					'PackageNumber' => $request_info->PackageNumber,
					'item_id' => '',
					'package_id' => $request_info->PackageNumber,
					'item_name' => '',
					'log_type' => 'request',
					'message' => 'request has been cancel by admin',
					'status' => 'cancel_by_admin',
					'action_user_id' => Auth::user()->_id,
					'EnterOn' => new MongoDate(),
				];

				Activitylog::insert($insertactivity);
				return Redirect::back()->withSuccess('Package has been canceled successfully.');
			}
			
		}
		return Redirect::back()->with(['danger','Oops! Something went wrong.']);
	}

}
