<?php namespace App\Modules\Admin\Controllers;

/*
 * Project: Aquantuo
 *
 */

use App\Http\Controllers\Controller;
use App\Http\Models\Admin;
use App\Http\Models\CityStateCountry;
use App\Http\Models\Deliveryrequest;
use App\Http\Models\Notification;
use App\Http\Models\Setting;
use App\Http\Models\Transaction;
use App\Http\Models\User;
use App\Library\Notify;
use App\Library\Stripe;
use App\Library\UserReg;
use Auth;
use Illuminate\Http\Request;
use Input;
use MongoDate;
use MongoId;
use Validator;
use Redirect;
use App\Http\Models\SilderImages;
use App\Http\Models\Client;
use App\Http\Models\Distance;

class AjaxController extends Controller
{

    public function adddistance() {
        $validation = Validator::make(Input::all(), array(
            'from' => 'required',
            'country' => 'required',
            'to' => 'required',
            'price' => 'required',
        ));
        if ($validation->fails()) {
            return Redirect::to("admin/distance")
                ->withErrors($validation)->withInput();
        } else {
            if (trim(Input::get('type')) == 'item') {
                $type = 'item-value';
            } else {
                $type = 'distance';
            }

            $country = json_decode(Input::get('country'));
            $insertData = [
                'country_id' => $country->id,
                'to' => (float) Input::get('to'),
                'from' => (float) Input::get('from'),
                'price' => (float) Input::get('price'),
                'type' => $type,
                'Status' => 'Active',
                'EnterOn' => new MongoDate(),
            ];

            $result = Distance::Insert($insertData);

            if (count($result) > 0) {
                return Redirect::to("admin/" . $type)->withSuccess('Added successfully.');
            } else {
                return Redirect::to('admin/' . $type)->withErrors('Problem in added');
            }
        }

    }
    public function postadd_account_to_stripe()
    {
        $id = Input::get('id');
        $response = array('success' => 0, 'msg' => 'Invalid information');
        $user = User::find($id);

        if (count($user) > 0) {
            if ($user->StripeBankId == '') {
                $stripe = new Stripe();
                if (!empty($user->SSN)) {
                    try {
                        $param = array(
                            "name" => $user->Name,
                            "type" => "individual",
                            "tax_id" => $user->SSN,
                            "bank_account" => array(
                                'country' => 'US',
                                'routing_number' => $user->RoutingNo,
                                'account_number' => $user->BankAccountNo,
                            ),
                        );

                        $res = $stripe->add_bank($param);
                        if (isset($res->id)) {
                            $user->StripeBankId = $res->id;
                            $user->save();
                            $response['success'] = 1;
                            $response['msg'] = 'Bank account added successfully';
                        } else {

                            $response['msg'] = $res->error->message;
                        }
                    } catch (Exception $e) {
                        $response['msg'] = $e->getMessage();
                    }
                } else {
                    $response['msg'] = "The SSN number is required.";
                }
            } else {
                $response['msg'] = "Bank account already added to stripe.";
            }
        }
        echo json_encode($response);
    }
    public function city()
    {
        $response = array('success' => 1);
        $dropbox = array('' => "<option value=''>Select City</option>");
        $State = Input::get('superid');
        $data['info'] = CityStateCountry::where(array('type' => 'city', 'Status' => 'Active', 'SuperName' => $State))->get(array('Content'));

        foreach ($data['info'] as $key) {
            $selected = (strtolower(Input::get('city')) == strtolower($key->Content)) ? 'selected="selected"' : '';
            $dropbox[] = "<option value='$key->Content' $selected >$key->Content</option>";
        }

        $response['html'] = implode(' ', $dropbox);
        echo json_encode($response);
    }
    public function state()
    {
        $response = array('success' => 1);
        $dropbox = array('' => "<option value=''>Select State</option>");
        $State = Input::get('superid');

        $data['info'] = CityStateCountry::where(array('type' => 'State', 'Status' => 'Active', 'SuperName' => $State))->get(array('Content'));
        foreach ($data['info'] as $key) {
            $selected = (strtolower(Input::get('state')) == strtolower($key->Content)) ? 'selected="selected"' : '';
            $dropbox[] = "<option  data-id='$key->Content' value='$key->Content' $selected >$key->Content</option>";
        }

        $response['html'] = implode(' ', $dropbox);
        echo json_encode($response);
    }
    public function post_verified_user()
    {
        $response = array('success' => 0, 'msg' => 'Oops! We are unable to verify transporter account.');
        $id = Input::get('id');
        $user = User::find($id);
        if (count($user) > 0) {
            $array = array(
                'to' => $user->Email,
                'replace' => array(
                    '[USERNAME]' => $user->Name,
                ),
            );
            send_mail('56b453a55509251cd67773f5', $array);

            $user->TransporterStatus = 'active';
            $user->ProfileStatus = 'complete';

            if ($user->NotificationId != '') {
                $Notification = new Notify();
                $Notification->setValue('title', 'Account verify');
                $Notification->setValue('message', 'Your transporter account has been verified, please logout and login again to access transporter features.');
                $Notification->setValue('type', 'account_verify');
                $Notification->setValue('location', 'account_verify');
                $Notification->add_user($user->NotificationId, $user->DeviceType);
                $Notification->fire();

            }
            $user->save();
            $supportemail = Setting::find('563b0e31e4b03271a097e1ca');
            //Email to admin
            if (count($supportemail) > 0) {
                send_mail('58b6992e7ac6f67a228b4567', [
                    'to' => $supportemail->RegEmail,

                    'replace' => [
                        '[USERNAME]' => $user->Name,
                        '[EMAIL]' => $user->Email,
                        '[USERTYPE]' => $user->UserType,
                    ],
                ]);
            }

            Notification::insert([
                "NotificationTitle" => "Bussiness Account Activation",
                "NotificationMessage" => sprintf('%s has verified.', ucfirst($user->Name)),
                "NotificationReadStatus" => 0,
                "location" => "",
                "locationkey" => $user->_id,
                "Date" => new MongoDate(),
                "GroupTo" => "Admin",
            ]);

            $response = array('success' => 1, 'msg' => 'Transporter verified successfully.');
        }
        echo json_encode($response);
    }


    public function cronMail() {

        $data['paginationurl'] = "pagination/cron-mail";
        $data['orderType'] = "Desc";
        $data['orderby'] = "UserId";
        $data['postvalue'] = "&type=" . Input::get('type') . "&search_value=" . Input::get('search_value') . "&class=" . Input::get('class') . "&search_email=" . Input::get('search_email') . "&search_unit=" . Input::get('search_unit') . "&City=" . Input::get('City') . "&notification=" . Input::get('notification') . "&Country=" . Input::get('Country') . "&streat=" . Input::get('streat');

        return view('Admin::list.cronmail', $data);
    }

    public function get_package_information($id)
    {
        $response = array('success' => 0, 'msg' => 'Information not found.');

        $data['info'] = Deliveryrequest::find($id);

        return view('Admin::other.package_information', $data);
    }
    public function get_refund_information($id, $refundamt)
    {
        $deliveryrequestdetail = Deliveryrequest::find($id);
        $StripeChargeId = $deliveryrequestdetail->StripeChargeId;

        $stripeobj = new Stripe();
        $res = $stripeobj->charge_refund($StripeChargeId, $refundamt);
        echo json_encode($res);
    }

    public function view_transfer_fund()
    {
        $id = Input::get('id');
        $data['info'] = User::find($id);
        return view('Admin::other.popup.transfer_fund', $data);
    }
    public function transfer_fund_to_carrier()
    {
        if ((Auth::user()->UserPermission & CARRIER) == CARRIER) {
            $response = array('success' => 0, 'msg' => 'Invalid carrier information');
            $amount = (float) Input::get('transfer_amount');
            $carrierid = Input::get('transfer_to');
            $auth = User::find($carrierid);
            if (count($auth) > 0) {
                if (isset($auth->StripeBankId) and $auth->StripeBankId != '') {
                    try {
                        $amount = $amount * 100;
                        $stripeobj = new Stripe();
                        $ac_info = array(
                            "amount" => $amount,
                            "currency" => "usd",
                            "recipient" => $auth->StripeBankId,
                            "description" => "",
                        );
                        $res = $stripeobj->transfer_amount_to_bank($ac_info);
                        if (isset($res->id)) {
                            $insData = array(
                                "SendById" => "aquantuo",
                                "SendToId" => Auth::user()->_id,
                                "SendByName" => Auth::user()->UserName,
                                "RecieveByName" => $auth->Name,
                                "Description" => "Amount transfer to " . $auth->Name,
                                "Credit" => 0,
                                "Debit" => $amount,
                                "Status" => "done",
                                "TransactionType" => "transfer",
                                "EnterOn" => new MongoDate(),
                            );
                            Transaction::insert($insData);
                            $response['success'] = 1;
                            $response['msg'] = "Transfer successfully.";
                        } else {
                            $response['msg'] = $res->error->message;
                        }
                    } catch (Exception $e) {
                        $response['msg'] = $e - getMessage();
                    }
                } else {
                    $response['msg'] = 'Carrier bank account not attach to stripe.';
                }
            }
        }
        echo json_encode($response);
    }
    public function address_information()
    {
        $response = array('success' => 1, 'msg' => '', 'distance' => 0, 'lat' => 0, 'long' => 0);
        $from = urlencode(Input::get('startpoint'));
        $to = urlencode(Input::get('endpoint'));

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://maps.googleapis.com/maps/api/distancematrix/json?origins=$from&destinations=$to&language=en-EN&sensor=false");
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $res = curl_exec($ch);
        curl_close($ch);

        $res = json_decode($res);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://maps.google.com/maps/api/geocode/json?address=$to&sensor=false");
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $reslatlong = json_decode(curl_exec($ch));

        curl_close($ch);
        if (isset($reslatlong->results[0]->geometry->location->lat)) {
            $response['lat'] = $reslatlong->results[0]->geometry->location->lat;
            $response['long'] = $reslatlong->results[0]->geometry->location->lng;
        }
        foreach ($res->rows[0]->elements as $road) {
            if (isset($road->distance)):
                $response['distance'] += $road->distance->value;
            endif;
        }

        echo json_encode($response);
    }
    public function badge()
    {
        $html = "";
        $data['info'] = Notification::where(array('NotificationReadStatus' => 0, 'GroupTo' => "Admin"))->orderBy('_id', 'desc')->get();
        foreach ($data['info'] as $key => $value) {
            $html .= '<a href="#" class="media list-group-item dark">
                    <div class="">
                      <div class="media-left">
                          <i class="glyphicon glyphicon-bell font-40"></i>
                      </div>
                      <div class="media-body">
                          <h4 class="media-heading">' . $value->NotificationTitle . '</h4>'
            . $value->NotificationMessage .
                '</div>
                      </div>
                  </a>';

        }
        $view = "";
        if ($html !== '') {
            $view = '<a class="list-group-item active text-center" href="' . url("admin/admin_notification") . '"><div class="custom-view-btn">View</div></a>';
        }

        $response = array("success" => 0, 'html' => $html, 'count' => $data['info']->count(), 'view' => $view);

        echo json_encode($response);
    }
    public function reset_password($id, $token, Request $request)
    {
        Auth::logout();
        $id = (strlen($request->segment(3)) == 24) ? new MongoId($request->segment(3)) : $request->segment(3);

        $auth = Admin::where(array('_id' => $id))->get(array('UserEmail', 'Token'));

        if (count($auth) > 0) {
            if ($auth[0]->Token == (int) $request->segment(4)) {
                return view('Admin::auth.reset_password');
            } else {
                return redirect('admin/login')->with('fail', 'Your link is expired.');
            }
        } else {
            return redirect('admin/login')->with('fail', 'You are not authorize to reset password.');
        }
    }
    public function postreset_password($id, $token, Request $request)
    {

        $v = Validator::make($request->all(), array('conf_password' => 'required', 'password' => 'required'));
        if ($v->fails()) {
            return Redirect::to('admin/reset_password/' . $request->segment(3) . "/" . $request->segment(4))->withErrors($v)->withInput(Input::get());
        }
        $auth = Admin::find($request->segment(3));

        $auth->password = bcrypt(Input::get('password'));
        $auth->Token = '';

        $auth->save();
        return redirect('admin/login')->withSuccess('Password reset successfully.');
    }
    public function user_registration()
    {

        $response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
        $v = Validator::make([
            'Email' => strtolower(trim(Input::get('user_email'))),
        ], array('Email' => 'required|email|unique:users'));
        if ($v->fails()) {
            if ($v->errors()->has('Email')) {
                $response['msg'] = $v->errors()->first('Email');
            }
        } else {
            $pass = rand(100000, 999999);

            $UserReg = new UserReg([
                "FirstName" => Input::get('first_name'),
                "LastName" => Input::get('last_name'),
                "Email" => Input::get('user_email'),
                "Password" => md5($pass),
                "temprary" => $pass,
            ]);
            $response = $UserReg->_reg();
        }
        return response()->json($response);
    }
    public function find_user_from_email()
    {
        $response = ['success' => 0, 'msg' => 'Oops! The email you have entered is not registred with us.'];
        $user = User::where(['Email' => strtolower(trim(Input::get('existing_user_email')))])
            ->first();
        if (count($user) > 0) {
            $response['success'] = 1;
            $response['result'] = ['_id' => $user->_id];
        }

        return response()->json($response);
    }

    public function sliderImages() {

        //if (!(Auth::user()->UserPermission & EMAIL_TEMPLATE) == EMAIL_TEMPLATE) {
           // return Redirect::to('/admin/dashboard');
       // }

        $data['paginationurl'] = "pagination/silder-image";
        $data['orderType'] = "Desc";
        $data['orderby'] = "UserId";
        $data['postvalue'] = "&type=" . Input::get('type') . "&search_value=" . Input::get('search_value') . "&class=" . Input::get('class') . "&search_email=" . Input::get('search_email') . "&search_unit=" . Input::get('search_unit') . "&City=" . Input::get('City') . "&State=" . Input::get('State') . "&Country=" . Input::get('Country') . "&streat=" . Input::get('streat');

        return view('Admin::list.images', $data);
    }

    public function addImages(Request $request) {

        $v = Validator::make($request->all(), array('image' => 'required'));
        if ($v->fails()) {
            return Redirect::to('admin/add-images')->withErrors($v)->withInput(Input::get());
        } else {

            $post = $request->all();

            if ($post['image'] != '') {

                if (Input::file('image')) {
                    $extension = Input::file('image')->getClientOriginalExtension();
                    $fileName = time() . rand(100, 999) . '.' . $extension;
                    if (Input::file('image')->move(BASEURL_FILE . 'section/', $fileName)) {
                        $image = "section/" . $fileName;
                    }
                } else {
                    $image = '';
                }

                if (Input::file('slideImage')) {
                    $extension2 = Input::file('slideImage')->getClientOriginalExtension();
                    $fileName2 = time() . rand(100, 999) . '.' . $extension2;
                    if (Input::file('slideImage')->move(BASEURL_FILE . 'section/', $fileName2)) {
                        $image2 = "section/" . $fileName2;
                    }
                } else {
                    $image2 = '';
                }

                $array = [
                    'sliderContant' => $post['description'],
                    'image' => $image,
                    'slideImage' => $image2,
                    'Status' => 'active',
                    'EnterOn' => new MongoDate(),
                ];

                $lastId = SilderImages::insert($array);

                return Redirect::to('admin/silder-image')->withSuccess('Image Added Successfully.');
            }
            return Redirect::to('admin/silder-image')->withErrors('image fail to add.');
        }
    }

    public function add_Images() {
        return view('Admin::add.Images');
    }

    public function editImage($id) {

        $data['images'] = SilderImages::where('_id', '=', $id)->first();
        return view('Admin::edit.images', $data);
    }

    public function updateImages(Request $request, $id) {

        if (Input::file('image')) {
            $extension = Input::file('image')->getClientOriginalExtension();
            $fileName = time() . rand(100, 999) . '.' . $extension;
            if (Input::file('image')->move(BASEURL_FILE . 'section/', $fileName)) {
                $updateData['image'] = "section/" . $fileName;
            }
        }

        $updateDate['sliderContant'] = Input::get('title');

        $lastId = SilderImages::where('_id', '=', $id)->update($updateDate);

        return Redirect::to('admin/silder-image')->withSuccess('Image has been updated successfully.');

    }

    public function postImages(Request $request, $id) {

        if (Input::file('image')) {
            $extension = Input::file('image')->getClientOriginalExtension();
            $fileName = time() . rand(100, 999) . '.' . $extension;
            if (Input::file('image')->move(BASEURL_FILE . 'section/', $fileName)) {
                $updateData['image'] = "section/" . $fileName;
            }
        }

        if (Input::file('slideImage')) {
            $extension2 = Input::file('slideImage')->getClientOriginalExtension();
            $fileName2 = time() . rand(100, 999) . '.' . $extension2;
            if (Input::file('slideImage')->move(BASEURL_FILE . 'section/', $fileName2)) {
                $updateData['slideImage'] = "section/" . $fileName2;
            }
        }

        $updateData['sliderContant'] = Input::get('description');

        $lastId = SilderImages::where('_id', '=', $id)->update($updateData);

        return Redirect::to('admin/silder-image')->withSuccess('Image has been updated successfully.');
    }

    public function viewImageDetail($id) {

        $data['images'] = SilderImages::where('_id', '=', $id)->first();
        return view('Admin::other.images', $data);
    }

    public function client() {
        $data['paginationurl'] = "pagination/client";
        $data['orderType'] = "Desc";
        $data['orderby'] = "UserId";
        $data['postvalue'] = "&type=" . Input::get('type') . "&search_value=" . Input::get('search_value');
        return view('Admin::list.client', $data);
    }

    public function AddClient() {

        return view('Admin::add.client');
    }

    public function postAddClient(Request $request) {

        $v = Validator::make($request->all(), array(
            'name' => 'required',
            'position' => 'required',
            'description' => 'required',
        ));
        if ($v->fails()) {
            return Redirect::to('admin/add-client')->withErrors($v)->withInput(Input::get());
        } else {

            if (Input::file('image') != '') {
                $extension = Input::file('image')->getClientOriginalExtension();
                $fileName = time() . rand(100, 999) . '.' . $extension;
                if (Input::file('image')->move(BASEURL_FILE . 'section/', $fileName)) {
                    $image = "section/" . $fileName;
                }
            } else {
                $image = '';
            }

            $array = [
                'name' => Input::get('name'),
                'position' => Input::get('position'),
                'description' => Input::get('description'),
                'Status' => 'active',
                'image' => $image,
                'EnterOn' => new MongoDate(),
            ];

            $lastId = Client::insert($array);

            return Redirect::to('admin/client')->withSuccess('Client Added Successfully.');
        }
    }

    public function editClient($id) {

        $data['client'] = Client::where('_id', '=', $id)->first();
        return view('Admin::edit.client', $data);
    }

    public function postClient(Request $request, $id) {

        $validation = Validator::make(Input::all(), array(
            'name' => 'required',
            'position' => 'required',
            'description' => 'required',
        ));
        if ($validation->fails()) {
            return Redirect::to("admin/edit-client/.$id")
                ->withErrors($validation)->withInput();
        } else {

            if (Input::file('image') != '') {
                $extension = Input::file('image')->getClientOriginalExtension();
                $fileName = time() . rand(100, 999) . '.' . $extension;
                if (Input::file('image')->move(BASEURL_FILE . 'section/', $fileName)) {
                    $updateData['image'] = "section/" . $fileName;
                }
            }

            $updateData['name'] = trim(Input::get('name'));
            $updateData['position'] = trim(Input::get('position'));
            $updateData['description'] = trim(Input::get('description'));

            $result = Client::where('_id', '=', $id)->update($updateData);

            if (count($result) > 0) {
                return Redirect::to("admin/client")->withSuccess('Added successfully.');
            } else {
                return Redirect::to('admin/client')->withErrors('Problem in added');
            }
        }
    }

    public function activity(Request $request) {
        $data['paginationurl'] = "pagination/activity";
        $data['orderType'] = "Desc";
        $data['orderby'] = "UserId";
        $data['postvalue'] = "&StartDate=" . Input::get('StartDate') . "&search_value=" . Input::get('search_value') . "&EndDate=" . Input::get('EndDate') . "&package_id=" . Input::get('package_id') . "&request_type=" . Input::get('request_type') . "&status=" . Input::get('status') . "&request_id=" . Input::get('request_id');

        return view('Admin::list.activity', $data);
    }


}
