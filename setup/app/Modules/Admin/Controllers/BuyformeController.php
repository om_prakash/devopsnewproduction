<?php
namespace App\Modules\Admin\Controllers;

/*
 * Project: Aquantuo
 */

use App\Http\Controllers\Controller;
use App\Http\Models\Activitylog;
use App\Http\Models\Additem;
use App\Http\Models\Address;
use App\Http\Models\Category;
use App\Http\Models\CityStateCountry;
use App\Http\Models\Configuration;
use App\Http\Models\Deliveryrequest;
use App\Http\Models\Extraregion;
use App\Http\Models\Setting;
use App\Http\Models\Transaction;
use App\Http\Models\User;
use App\Library\Requesthelper;
use App\Library\Utility;
use App\Modules\Admin\Models\Admin;
use App\Modules\User\Controllers\Calculation;
use App\Modules\User\Controllers\NewCalculation;
use App\Traits\AdminTrait;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Image;
use Input;
use MongoDate;
use MongoId;
use Session;
use App\Library\NewEmail;
use App\Http\Models\Currency;
use App\Http\Models\PaymentInfo;
use App\Library\Notify;

class BuyformeController extends Controller {
	use AdminTrait;
	// calculation part buy for me
	// Start buy for me section

	public function __construct() {
		if (!(Auth::user())) {
			Redirect::to('admin/login')->send();
		}
	}

	public function latlongapi2($address) {
		

		$flag = false;
		$i = 0;
		while ($flag == false && $i < 19) {
			$i = $i + 1;
			$res = Utility::getLatLong($address);
			if ($res != false) {
				$flag = true;
				break;
			}
		}
		return $res;
	}

	public function buy_for_me_calculation() {
		$response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		$aqGeoLocation = $this->getAqLatLong();

		$data = [
			'shippingcost' => 0,
			'insurance' => 0,
			'ghana_total_amount' => 0,
			'total_amount' => 0,
			'error' => [],
			'formated_text' => '',
			'distance_in_mile' => 0,
			'product_count' => 0,
			'ProcessingFees' => 0,
			'item_cost' => 0,
			'shipping_cost_by_user' => 0,
			'type' => 'buyforme',
			'consolidate_check'=> (Input::get('consolidate_check') == 'on')? 'on':'off'
		];

		$address = json_decode(Input::get('address'));
		$data["is_customs_duty_applied"] = false;
		$addlat = true;
		$lat = floatval(Input::get('lat'));
		$lng = floatval(Input::get('lng'));
		$json_decode_country = json_decode(Input::get('country'));
		$json_decode_state = json_decode(Input::get('state3'));
		$json_decode_city = json_decode(Input::get('city'));
		
		if ($json_decode_country->name != "USA" && $json_decode_country->name != "Kenya") {
			$data["is_customs_duty_applied"] = true;
		}

		$addlat = $this->latlongapi2(@$json_decode_city->name .' ,' . @$json_decode_state->name .' ,'.@$json_decode_country->name);

		if ($addlat != false) {
			$lat = $addlat['lat'];
			$lng = $addlat['lng'];
		}

		if ($addlat == false) {
			$response = array("success" => 0, "msg" => "We are unable to find your location. Please  correct it.");
		}

		if (empty(trim(Input::get('request_id')))) {
			if (Input::get('listing') == 'own') {
				$data['product'] = Additem::where(array('user_id' => Auth::user()->_id, 'request_type' => 'buy_for_me',"user_type" => "admin"))->get();
			} else {
				$data['product'] = Additem::where(['request_type' => 'buy_for_me', "user_type" => "admin"])->get();
			}
			$data['product'] = $data['product']->toArray();
		} else {
			$data['product'] = Session::get(trim(Input::get('request_id')));
			foreach ($data['product'] as $key => $value) {
				$data['product'][$key]['request_type'] = 'buy_for_me';
			}
		}

		if (count($data['product']) > 0 && count($addlat) > 0) {

			$data['distance'] = Utility::GetDrivingDistance(
				$aqGeoLocation->aqlat, $aqGeoLocation->aqlong, $lat, $lng);

			if ($data['distance']->distance > 0) {
				$calculation = new NewCalculation();
				$userinfo = (Object) ['Default_Currency' => 'GHS'];
				// $response = $calculation->getRate($data['product'], $data, $userinfo);
				$response = $calculation->GetRate($data['product'], $data, $userinfo, $json_decode_country->name);

				$state = json_decode(Input::get('state3')); // update 08/03/2018

				$response['AreaCharges'] = $this->resionCharges(['name' => $state->name, 'id' => $state->id]);
				$response['total_amount'] = $response['total_amount'] + $response['AreaCharges'];

				$response['shippingCost'] = $response['shippingCost'];

				$response['total_amount'] = $response['total_amount']; // + $response['ProcessingFees'];
				$currency_conversion = $this->currency_conversion($response['total_amount']);
				$response['ghana_total_amount'] = $currency_conversion['ghanian_cost'];
				$response["user_currency_rate"] = $currency_conversion["currency_rate_ghs"];
				$response["FormatedText"] = $currency_conversion["FormatedText_ghs"];
				$response['error'] = [];
				$response['product_count'] = 0;
				$response['insurance'] = $response['insurance_cost'];

				if ($response['success'] == 1) {
					$view = view('Admin::Buy.ajax.buy_for_me', $response);
					$response['html'] = $view->render();
				}

			}

		}

		return json_encode($response);
	}

	public function post_buy_for_me() {
		$data['country'] = CityStateCountry::where(['type' => 'Country', 'Status' => 'Active'])
			->get(['_id', 'Content', 'state_available']);
		$data['user'] = User::select('_id', 'Name', 'Email')->orderBy('Name', 'asc')->get();
		$data['category'] = Category::where(['type' => 'Category', 'Status' => 'Active'])->whereIn('TravelMode', ['ship', 'air'])->orderBy('Content', 'asc')->get(['TravelMode', 'Content']);
		$field = ['country', 'state', 'city', 'address_line_1', 'address_line_2', 'zipcode', 'lat', 'lng'];
		$data['address'] = Address::where('user_id', '=', Auth::user()->_id)->get($field);
		
		if(Input::get('listing') != ''){
			if(Input::get('listing') == 'own'){
				$data['additem'] = Additem::where('user_id', '=', Auth::user()->_id)->where(array('request_type' => 'buy_for_me', 'user_type' => 'admin'))->get();
			}else if(Input::get('listing') == 'all'){
				$data['additem'] = Additem::where(['request_type' => 'buy_for_me', 'user_type' => 'admin'])->get();
			}else{
				return redirect::to('admin/buy-for-me');
			}
			
		}else{
			$data['additem'] = Additem::where(['request_type' => 'buy_for_me', 'user_type' => 'admin'])->get();
		}
		return view('Admin::Buy.buy_for_me', $data);
	}

	public function add_buy_for_me_item(Request $request) {
		$response = array("success" => 0, "msg" => "Oops! Something went wrong.");
		$validation = Validator::make($request->all(), [
			'item_name' => 'required',
			'purchase_from' => 'required',
			'item_price' => 'required',
			'category' => 'required',
			'description' => 'required'
		]);

		if ($validation->fails()) {
			$response['error'] = $validation->errors();
			return response()->json($response);
		} else {
			$lhwunit = 'inches';
			$weightunit = 'lbs';

			if (Input::get('measurement_unit') == 'cm_kg') {
				$lhwunit = 'cm';
				$weightunit = 'kg';
			}

			$json_decode_category = json_decode(Input::get('category'));

			// Stop user to add item with shipping mode sea but different category.
			$is_make_request = "yes";
			$item_with_sea = false;
			$item_with_air = false;

			if (empty(trim(Input::get("request_id")))) {	// "travelMode" => "ship", 
				$shipItemList = Additem::where(array("user_id" => Auth::user()->_id, "request_type" => "buy_for_me"))->select("category_id", 'travelMode')->get();
				foreach ($shipItemList as $value) {
					if ($value->category_id != @$json_decode_category->id && $value->travelMode == 'ship') {
						$is_make_request = "no";
						$item_with_sea = true;
					}

					if (Input::get("travel_mode") == "ship") {
						$item_with_sea = true;
					}

					if (Input::get("travel_mode") == "air" || $value->travelMode == 'air') {
						$item_with_air = true;
					}

					if ($item_with_sea && $item_with_air) {
						$response["msg"] = "Sorry! You can’t have different shipping modes in the same listing. Please create a separate listing for either air or sea.";
						return response()->json($response);
						die();
					}
				}
			} else {
				$item_list = Session::get(trim(Input::get("request_id")));
				foreach ($item_list as $key) {
					if ($key["categoryid"] != @$json_decode_category->id && $key["travelMode"] == "ship") {
						$is_make_request = "no";
						$item_with_sea = true;
					}

					if (Input::get("travel_mode") == "ship") {
						$item_with_sea = true;
					}

					if ($key["travelMode"] == "air" || Input::get("travel_mode") == "air") {
						$item_with_air = true;
					}

					if ($item_with_sea && $item_with_air) {
						$response["msg"] = "Sorry! You can’t have different shipping modes in the same listing. Please create a separate listing for either air or sea.";
						return response()->json($response);
						die();
					}
				}
			}
			
			if ($is_make_request == "no") {
				$response["msg"] = "If shipping by Sea, all items must be of the same sea category. Please create a new listing if you need to use a different sea category.";
				return response()->json($response);
				die();
			}

			$insert = [
				'item_name' => Input::get('item_name'),
				'product_name' => Input::get('item_name'),
				'add_item_url' => Input::get('purchase_from'),
				'url' => Input::get('purchase_from'),
				'measurement_unit' => Input::get('measurement_unit'),
				'weight_unit' => (Input::get('measurement_unit') == 'cm_kg') ? 'kg' : 'lbs',
				'length' => Input::get('length'),
				'width' => Input::get('width'),
				'height' => Input::get('height'),
				'weight' => Input::get('weight'),
				'item_price' => Input::get('item_price'),
				'price' => Input::get('item_price'),
				'qty' => Input::get('quentity'),
				'shipping_cost_by_user' => Input::get('shipping_cost'),
				'travel_mode' => Input::get('travel_mode'),
				'travelMode' => Input::get('travel_mode'),
				'category' => @$json_decode_category->name,
				'category_id' => @$json_decode_category->id,
				'categoryid' => @$json_decode_category->id,
				'description' => Input::get('description'),
				'insurance' => Input::get('insurance'),
				'insurance_status' => Input::get('insurance'),
				'request_type' => 'buy_for_me',
				'user_type' => 'admin',
				'user_id' => Auth::user()->_id,
				'buy_for_me_dimensions' => Input::get('buy_for_me_dimensions'),
				'ExpectedDate' => '',
				'EnterOn' => new MongoDate,
			];

			// Calculate value
			$requesthelper = new Requesthelper(
				[
					"needInsurance" => Input::get('insurance'),
					"productQty" => (Input::get('quentity') < 1) ? 1 : Input::get('quentity'),
					"productCost" => $insert['price'],
					"productWidth" => $insert['width'],
					"productWidthUnit" => $lhwunit,
					"productHeight" => $insert['height'],
					"productHeightUnit" => $lhwunit,
					"productLength" => $insert['length'],
					"productLengthUnit" => $lhwunit,
					"productWeight" => $insert['weight'],
					"productWeightUnit" => $weightunit,
					"productCategory" => @$json_decode_category->name,
					'productCategoryId' => @$json_decode_category->id,
					"distance" => 0,
					"travelMode" => $insert['travelMode'],
				]
			);

			$requesthelper->calculate();
			$calculationinfo = $requesthelper->get_information();

			if (isset($calculationinfo->error)) {
				$response['msg'] = $calculationinfo->error;
				return response()->json($response);
			}

			if (Input::file('item_image')) {
				$extension = Input::file('item_image')->getClientOriginalExtension(); // getting image extension

				$fileName = time() . rand(100, 999) . '.' . $extension; // Rename img image
				if (Input::file('item_image')->move(BASEURL_FILE . 'package/', $fileName)) {
					$insert['image'] = "package/" . $fileName;
				}
			}
			$data['items'] = [];

			if (!empty(trim(Input::get('request_id')))) {

				$old_item = Deliveryrequest::where(['_id' => trim(Input::get('request_id')), 'RequestType' => 'buy_for_me'])
					->select('ProductList')
					->first();

				if (isset($old_item->ProductList)) {
					foreach ($old_item->ProductList as $key) {
						$data['items'][] = $key;
					}
				}
				$data['items'] = session()->get(trim(Input::get('request_id')));
				$insert['_id'] = (String) new MongoId();
				$data['items'][] = $insert;

				session::put(trim(Input::get('request_id')), $data['items']);

			} else {
				$response = Additem::insert($insert);
				$data['items'] = Additem::where(["user_id" => Auth::user()->_id, "request_type" => "buy_for_me"])->get();
			}
			$data['form_type'] = Input::get('form_type');
			$response = array("success" => 1, "msg" => "Item has been added successfully.");
			$view = view('Admin::Buy.ajax.edit_buy_for_me', $data);

			$response['html'] = $view->render();

		}
		return response()->json($response);
	}

	/*   buy for me    second part*/
	public function pre_buy_for_me(Request $request) {
		$dutyCustoms = Input::get("dutyCustom");
		$tax = Input::get("tax");
		$response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];

		$aqGeoLocation = $this->getAqLatLong();

		$validation = Validator::make($request->all(), [
			'country_code' => 'required',
			'phone_number' => 'required',
		]);

		if ($validation->fails()) {

			$validation->errors()->add('field', trans('Something is wrong'));
			return $validation->errors();
		} else {

			$discount_in_percent = 0;
			$distance_in_mile = floatval(Input::get('distance'));
			$productlist = [];
			/*if (empty(trim(Input::get('request_id')))) {
				$productlist = Additem::where(array('request_type' => 'buy_for_me',"user_type"=> "admin"))->get();
				$productlist = $productlist->toArray();
			} else {
				$productlist = session::get(Input::get('request_id'));
				// update by aakash  15/03/2017
			}
*/
			$DeliveryDate = '';
			if (!Input::get('desired_delivery_date') == '') {
				$DeliveryDate = get_utc_time(Input::get('desired_delivery_date'));

			}

			$json_decode_category = json_decode(Input::get('category'));
			$address = json_decode(Input::get('address'));

			$addlat = true;
			$lat = floatval(Input::get('lat'));
			$lng = floatval(Input::get('lng'));
			$json_decode_country = json_decode(Input::get('country'));
			$json_decode_state = json_decode(Input::get('state'));
			$json_decode_city = json_decode(Input::get('city'));

			
			$requesterid = '';
			if (Input::get('user') != '') {
				$json_decode_user = json_decode(Input::get('user'));
				$requesterid = @$json_decode_user->id;
				$requestername = @$json_decode_user->name;
			} else {
				$response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
				$email_check = User::where(['Email' => strtolower(trim(Input::get('email')))])->first();
				if (count($email_check) > 0) {
					$response = ['success' => 0, 'msg' => 'This email is already registered with us.'];

				} else {
					if(!isset(Input::get('first_name')[0])){
						$response = array("success" => 0, "msg" => "Please choose user.");
						echo json_encode($response);
						die;
					}
					

					$pass = rand(100000, 999999);
					$UniqueNo = Input::get('first_name')[0] . Input::get('lastName')[0];
					$insert['FirstName'] = Input::get('first_name');
					$insert['LastName'] = Input::get('last_name');
					$insert['Name'] = ucfirst(Input::get('first_name')) . " " . ucfirst(Input::get('last_name'));

					$insert['Email'] = strtolower(trim(Input::get('email')));
					$insert['Password'] = md5($pass);
					$insert['temprary'] = $pass;
					$insert['TransporterType'] = '';
					$insert['UserType'] = 'requester';
					$insert['ChatName'] = "aquantuo" . Utility::sequence('user');
					$insert['CountryCode'] = "";
					$insert['PhoneNo'] = '';
					$insert['AlternateCCode'] = '';
					$insert['AlternatePhoneNo'] = '';
					$insert['RequesterStatus'] = 'active';
					$insert['Age'] = "";
					$insert['SSN'] = "";
					$insert['Street1'] = '';
					$insert['Street2'] = '';
					$insert['Country'] = '';
					$insert['delete_status'] = 'no';
					$insert['State'] = '';
					$insert['City'] = '';
					$insert['ZipCode'] = '';
					$insert['BankName'] = '';
					$insert['AccountHolderName'] = '';
					$insert['BankAccountNo'] = '';
					$insert['RoutingNo'] = '';
					$insert['StripeId'] = '';
					$insert['TransporterStatus'] = "not_registred";
					$insert['UserStatus'] = "active";
					$insert['Image'] = "";
					$insert['IDProof'] = '';
					$insert['LicenceId'] = '';
					$insert['type_of_id'] = '';
					$insert['TPTrackLocation'] = "on";
					$insert['EmailStatus'] = "on";
					$insert['consolidate_item'] = "on";
					$insert['NoficationStatus'] = "on";
					$insert['TPSetting'] = "on";
					$insert['SoundStatus'] = "on";
					$insert['VibrationStatus'] = "on";
					$insert['EnterOn'] = new MongoDate();
					$insert['UpdateOn'] = new MongoDate();
					$insert['RatingCount'] = 0;
					$insert['RatingByCount'] = 0;
					$insert['CurrentLocation'] = array();
					$insert['DeliveryAreaCountry'] = array();
					$insert['DeliveryAreaState'] = array();
					$insert['DeliveryAreaCities'] = array();
					$insert['ProfileStatus'] = 'step-one';
					$insert['StripeBankId'] = "";
					$insert['NotificationId'] = Input::get('NotificationId');
					$insert['DeviceId'] = Input::get('DeviceId');
					$insert['DeviceType'] = 'web';
					$insert['UniqueNo'] = strtoupper($UniqueNo) . Utility::sequence('user_unique');
					$insert['bank_info'] = [];
					$insert['AqAddress'] = '';
					$insert['AqCity'] = '';
					$insert['AqState'] = '';
					$insert['AqCountry'] = '';
					$insert['AqZipcode'] = '';
					$insert['AqLatLong'] = [];
					$insert['Default_Currency'] = 'USD';
					//  $insert['PerPage'] = 20;

					// Get aquantuo addres
					$supportemail = Setting::find('563b0e31e4b03271a097e1ca');
					if (count($supportemail) > 0) {
						$insert['AqAddress'] = $supportemail->AqAddress;
						$insert['AqCity'] = $supportemail->AqCity;
						$insert['AqState'] = $supportemail->AqState;
						$insert['AqCountry'] = $supportemail->AqCountry;
						$insert['AqZipcode'] = $supportemail->AqZipcode;
						$insert['AqLatLong'] = $supportemail->AqLatlong;
					}

					$result['_id'] = (string) User::insertGetId($insert);

					if ($result['_id']) {
						$Email = new NewEmail();
						// email to user
						$Email->send_mail('58e4f6f77ac6f6e92d8b4567', [
							'to' => trim(Input::get('email')),
							'replace' => [
								'[USERNAME]' => $insert['Name'],
								'[PASSWORD]' => $pass,
							],
						]);

						$response = array("success" => 1, "msg" => "Requester has been Registered successfully.");
					}
					$requesterid = $result['_id'];
					$requestername = ucfirst(Input::get('first_name')) . " " . ucfirst(Input::get('last_name'));
				}
			}

			if($requesterid == ''){
				$response = array("success" => 0, "msg" => "Please choose user.");
				echo json_encode($response);
				die;
			}

			$result = Setting::where('_id', '563b0e31e4b03271a097e1ca')->select('AqLatlong')->first();

			$aqlat = isset($result['AqLatlong'][1]) ? $result['AqLatlong'][1] : '';
			$aqlong = isset($result['AqLatlong'][0]) ? $result['AqLatlong'][0] : '';

			// Get distance
			$data['distance'] = Utility::GetDrivingDistance($aqlat, $aqlong, @$address->lat, @$address->lng);

			// End get distance
			$configurationdata = Configuration::where(['_id' => '5673e33e6734c4f874685c84'])->first();
			// $calculation = new Calculation();
			$calculation = new NewCalculation();

			$data = [
				'shippingcost' => 0,
				'insurance' => 0,
				'ghana_total_amount' => 0,
				'total_amount' => 0,
				'error' => [],
				'formated_text' => '',
				'distance_in_mile' => 0,
				'product_count' => 0,
				'ProcessingFees' => 0,
				'item_cost' => 0,
				'shipping_cost_by_user' => 0,
				'type' => 'buyforme',
				'consolidate_check'=> (Input::get('consolidate_check') == 'on')? 'on':'off'
			];
			$addlat = true;
			$lat = floatval(Input::get('lat'));
			$lng = floatval(Input::get('lng'));
			$json_decode_country = json_decode(Input::get('country'));
			$json_decode_state = json_decode(Input::get('state3'));
			$json_decode_city = json_decode(Input::get('city'));

			$addlat = $this->latlongapi2($json_decode_city->name .' ,' . @$json_decode_state->name .' ,'.$json_decode_country->name);

			if ($addlat != false) {
				$lat = $addlat['lat'];
				$lng = $addlat['lng'];
			}

			if ($addlat == false) {
				$response = array("success" => 0, "msg" => "We are unable to find your location. Please  correct it.");
				echo json_encode($response);
				die;
			}

			$response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
			$response['total_amount'] = 0;
			$response['ghana_total_amount'] = 0;
			$response['shipping_cost'] = 0;

			if (empty(trim(Input::get('request_id')))) {
				$old_packageNumber = Utility::sequence('Request') . time();
				//$data['product'] = Additem::where(array('request_type' => 'buy_for_me',"user_type"=> "admin"))->get();
				if(Input::get('listing') == 'own'){
					$data['product'] = Additem::where(array('user_id' => Auth::user()->_id, 'request_type' => 'buy_for_me',"user_type" => "admin"))->get();
				}else{
					$data['product'] = Additem::where(array('request_type' => 'buy_for_me', "user_type" => "admin"))->get();
				}

				$data['product'] = $data['product']->toArray();
			} else {
				$data['product'] = session()->get(trim(Input::get('request_id')));
				if(count($data['product']) == 0){
					$response = ['success' => 0, 'msg' => 'Product not found.'];
				}
				$old_packageNumber = Deliveryrequest::where(['_id'=> Input::get('request_id')])->select('PackageNumber')->first();
				$old_packageNumber = $old_packageNumber->PackageNumber;
			}



			if (count($data['product']) > 0) {
				$data['distance'] = Utility::GetDrivingDistance($aqlat, $aqlong, $lat, $lng);
				$weight = 0;
				$userinfo = (Object) ['Default_Currency' => 'GHS'];


				// $response = $calculation->getRate($data['product'], $data, $userinfo);
				$response = $calculation->GetRate($data['product'], $data, $userinfo, $json_decode_country->name);


				if ($response['success'] == 1) {

					$DeliveryDate = '';
					/*if (!Input::get('desired_delivery_date') == '') {*/
						//$DeliveryDate = get_utc_time(Input::get('desired_delivery_date'));
						$DeliveryDate ='';
						if(!empty(Input::get('desired_delivery_date'))){
							$DeliveryDate =get_utc_time(Input::get('desired_delivery_date'));
						}

						$insert = [
							'RequesterId' => new MongoId($requesterid),
							'RequesterName' => $requestername,
							'PackageNumber' => $old_packageNumber,
							'ProductTitle' => '',
							'RequestType' => 'buy_for_me',
							'request_version'=>'new',
							'ReceiverIsDifferent'=> (Input::get('ReceiverIsDifferent') == 'on')?'yes':'no', 
							'ReceiverName'=> (Input::get('ReceiverIsDifferent') == 'on')? ucfirst(Input::get('ReceiverName')):'' ,
							'Status' => 'pending',
							'DeliveryFullAddress' => Utility::formated_address([
								Input::get('address_line_1'),
								Input::get('address_line_2'),
								$json_decode_city->name,
								@$json_decode_state->name,
								$json_decode_country->name,
							], Input::get('zipcode')),
							'DeliveryAddress' => Input::get('address_line_1'),
							'DeliveryAddress2' => Input::get('address_line_2'),
							'DeliveryCity' => trim(@$json_decode_city->name) ,
							'DeliveryState' => trim(@$json_decode_state->name) ,
							'DeliveryCountry' => trim(@$json_decode_country->name) ,
							'DeliveryPincode' => Input::get('zipcode'),
							'ReturnToAquantuo' => (Input::get('return_to_aq') == 'yes') ? true : false,
							'ReturnAddress' => Auth::user()->AqAddress,
							'PaymentStatus' => 'paid',
							'ReceiverCountrycode' => '+' . Input::get('country_code'),
							'ReceiverMobileNo' => Input::get('phone_number'),
							'DeliveryDate' => $DeliveryDate,

							"DeliveryStateId" => @$json_decode_state->id,
							"DeliveryCountryId" => @$json_decode_country->id,
							
							'PromoCode' => Input::get('promo_code'),
							'discount' => 0,
							'BeforePurchaseTotalCost' => 0,
							'shipping_cost_by_user' => 0,
							'AquantuoFees' => 0,
							'ProcessingFees' => 0,
							'after_update_difference' => 0,
							'distance' => $calculation->get_distance($data['distance']->distance),
							'distanceType' => 'Miles',
							'shippingCost' => $response['shippingCost'],
							'insurance' => 0,
							'DutyAndCustom' => floatval($dutyCustoms),
							'Tax' => floatval($tax),
							'TotalCost' => 0,
							'total_item_price' => 0,
							'GhanaTotalCost' => 0,
							'PickupAddress' => '',
							"consolidate_item" => (Input::get('consolidate_check') == 'on') ? 'on' : 'off',
							'UpdateOn' => new MongoDate(),
							'EnterOn' => new MongoDate(),
							'PickupLatLong' => [floatval($aqlat), floatval($aqlong)],
							'DeliveryLatLong' => [floatval($lng), floatval($lat)],
							'Userstatus' => 'admin',
							'ProductList' => [],
						];
						$productList = $response['product'];
						$k = 0;
						foreach ($productList as $productKey => $productValue) {
							$lhwunit = 'cm';
							$weightunit = 'kg';
							if ($productValue['weight_unit'] == 'lbs') {
								$lhwunit = 'inches';
								$weightunit = 'lbs';
							}

							$cost = $productValue['insurance'] + $productValue['shippingCost'];

							// $insert['shippingCost'] += $productValue['shippingCost'];
							$insert['insurance'] += $productValue['insurance'];

							if (!empty(trim($insert["ProductTitle"]))) {
								$insert["ProductTitle"] = "{$insert['ProductTitle']}, ";
							}
							$total_item_price = floatval(@$productValue['price']) * $productValue['qty'];
							$insert["ProductTitle"] .= @$productValue['product_name'];
							$insert['shipping_cost_by_user'] += $productValue['shipping_cost_by_user'];

							$insert['ProcessingFees'] += $productValue['ProcessingFees'];
							$insert['AquantuoFees'] += @$productValue['aq_fee'];
							$insert["TotalCost"] += (floatval(@$productValue['price']) * $productValue['qty']) + $productValue['shipping_cost_by_user'] + $productValue['shippingCost'] + $productValue['insurance'];
							$k++;

							$insert["ProductList"][] = [
								'_id' => (string) new MongoId(),
								'marketid' => '',
								'marketname' => '',
								'tpid' => '',
								'tpName' => '',
								'package_id' => $insert['PackageNumber'] . $k,
								'product_name' => @$productValue['product_name'],
								'url' => @$productValue['url'],
								'description' => @$productValue['description'],
								'travelMode' => @$productValue['travelMode'],
								'weight' => @$productValue['weight'],
								'weight_unit' => $weightunit,
								'height' => @$productValue['height'],
								'heightUnit' => $lhwunit,
								'length' => @$productValue['length'],
								'lengthUnit' => $lhwunit,
								'width' => @$productValue['width'],
								'widthUnit' => $lhwunit,
								'category' => @$productValue['category'],
								'categoryid' => @$productValue['categoryid'],
								'price' => @$productValue['price'],
								'qty' => @$productValue['qty'],
								'insurance_status' => @$productValue['insurance_status'],
								'image' => @$productValue['image'],
								///'shipping_cost_by_user' => '',
								'status' => 'pending',
								'verify_code' => rand(1000, 9999),
								'txnid' => '',
								'shippingCost' => floatval(@$productValue['shippingCost']),
								'shipping_cost_by_user' => floatval(@$productValue['shipping_cost_by_user']),
								'insurance' => $productValue['insurance'],
								'discount' => 0,
								'aq_fee' => @$productValue['aq_fee'],
								'ProcessingFees' => @$productValue['ProcessingFees'],
								'TransporterFeedbcak' => '',
								'TransporterRating' => 0,
								'RequesterFeedbcak' => '',
								'RequesterRating' => 0,
								'RejectBy' => '',
								'ReturnType' => '',
								'ReceiptImage' => '',
								'RejectTime' => '',
								'TrackingNumber' => '',
								'TransporterMessage' => '',
								'CancelDate' => '',
								'StripeChargeId' => '',
								'DeliveredDate' => '',
								'tracking_number' => @$productValue['tracking_number'],

								'total_cost' => (floatval(@$productValue['price']) * $productValue['qty']) + $productValue['shipping_cost_by_user'] + $productValue['shippingCost'] + $productValue['insurance'] ,
								'after_update' => (floatval(@$productValue['price']) * $productValue['qty']) + $productValue['shipping_cost_by_user'] + $productValue['shippingCost'] + $productValue['insurance'],
								'PaymentStatus' => 'no',
								'inform_mail_sent' => 'no',
								'ExpectedDate' => '',
								'EnterOn' => new MongoDate(),
							];
						}
						if (count($insert["ProductList"]) <= 0) {
							return response()->json($response);
						}
						$state = json_decode(Input::get('state3'));

						$insert['AreaCharges'] = $this->resionCharges(['name' => $state->name, 'id' => $state->id]);
						//end accra charge
						$insert['discount'] = floatval(Input::get('discount'));
						
						
						$insert['TotalCost'] = $insert['TotalCost'] + $insert['AreaCharges']-$insert['discount'] + $insert['ProcessingFees'] + $insert['DutyAndCustom'] + $insert['Tax'];
						$insert['BeforePurchaseTotalCost'] = $insert['TotalCost'];

						$insert['total_item_price'] = floatval($total_item_price);

						if ($insert['TotalCost'] <= 0) {
							$insert['Status'] = 'ready';
							foreach ($insert["ProductList"] as $key => $val) {
								$insert["ProductList"][$key]['status'] = 'ready';
							}
						}

						if (empty(trim(Input::get('request_id')))) {
							$response = array("success" => 1, "msg" => "Success! Request has been created successfully.");
							$response['reqid'] = (String) Deliveryrequest::insertGetId($insert);

							$ActivityLog = DeliveryRequest::where('_id', '=', $response['reqid'])->select('_id', 'PackageNumber', 'ProductList', 'RequesterId')->first();
							
							if(Input::get('listing') == 'own'){
								Additem::where(['request_type' => 'buy_for_me', "user_type" => "admin",'user_id' => Auth::user()->_id])->delete();
							}else{
								Additem::where(['request_type' => 'buy_for_me', "user_type" => "admin"])->delete();
							}
							PaymentInfo::insert([
								'request_id' => $response['reqid'],
								'user_id'=> (String) $insert['RequesterId'],
								'action_user_id'=> Auth::user()->_id,
								'RequestType'=> 'buy_for_me',
								'action' => 'create',
								'item_id'=> '',
								'item_unic_number' => '',
								'TotalCost'=> floatval($insert['TotalCost']),
								'shippingCost'=> floatval($insert['shippingCost']),
								'insurance'=> floatval($insert['insurance']),
								'AreaCharges'=> floatval($insert['AreaCharges']),
								'ProcessingFees'=> floatval($insert['ProcessingFees']),
								'shipping_cost_by_user'=> floatval($insert['shipping_cost_by_user']),
								'item_cost'=> floatval($total_item_price),
								'discount'=> floatval($insert['discount']),
								'PromoCode'=> $insert['PromoCode'],
								'difference'=> floatval($insert['after_update_difference']),
								'difference_before'=> floatval($insert['after_update_difference']),
								'date'=> New MongoDate(),
							]);

							//$this->emailToRequester($response['reqid']);

							$array = $ActivityLog->ProductList;

							/* Activity Log  Create online purchase */
							foreach ($array as $key) {
								$insertactivity = [
									'request_id' => $response['reqid'],
									'request_type' => 'buy_for_me',
									'PackageNumber' => $ActivityLog->PackageNumber,
									'item_id' => $key['_id'],
									'package_id' => $key['package_id'],
									'item_name' => $key['product_name'],
									'log_type' => 'request',
									'message' => 'Package has been created.',
									'status' => 'pending',
									'action_user_id' => Auth::user()->_id,
									'EnterOn' => new MongoDate(),
								];

								Activitylog::insert($insertactivity);
							}

							$insert['insert'] = $response['reqid'];
							if ($response['reqid']) {
								Additem::where('user_id', '=', Auth::user()->_id)
									->where(['request_type' => 'buy_for_me'])->delete();
							}

						} else {
							$response = array("success" => 1, "msg" => "Success! Request has been updated successfully.");
							$update = Deliveryrequest::where(['_id' => Input::get('request_id')])->update($insert);
							$insert['insert'] = Input::get('request_id');
							$response['reqid'] = Input::get('request_id');
							PaymentInfo::insert([
								'request_id' => Input::get('request_id'),
								'user_id'=> (String) $insert['RequesterId'],
								'action_user_id'=> Auth::user()->_id,
								'RequestType'=> 'buy_for_me',
								'action' => 'update_request',
								'item_id'=> '',
								'item_unic_number' => '',
								'TotalCost'=> floatval($insert['TotalCost']),
								'shippingCost'=> floatval($insert['shippingCost']),
								'insurance'=> floatval($insert['insurance']),
								'AreaCharges'=> floatval($insert['AreaCharges']),
								'ProcessingFees'=> floatval($insert['ProcessingFees']),
								'shipping_cost_by_user'=> floatval($insert['shipping_cost_by_user']),
								'item_cost'=> floatval($total_item_price),
								'discount'=> floatval($insert['discount']),
								'PromoCode'=> $insert['PromoCode'],
								'difference'=> floatval($insert['after_update_difference']),
								'difference_before'=> floatval($insert['after_update_difference']),
								'date'=> New MongoDate(),
							]);

							$users = User::where(['_id' => $insert['RequesterId']])->select('NotificationId','DeviceType')->first();

							$Notification = new Notify();
							$Notification->setValue('title', "Package has been updated");
							$Notification->setValue('message',"Aquantuo has updated your package title:".ucfirst($insert['ProductTitle']).".");
							$Notification->setValue('type', 'request_detail');
							$Notification->setValue('location', 'request_detail');
							$Notification->setValue('locationkey', Input::get('request_id'));
							$Notification->add_user(@$users->NotificationId, @$users->DeviceType);
							$Notification->fire();

						}
						$url = "promocode=" . Input::get('promo_code');
						if ($insert['TotalCost'] <= 0) {
							$response['redirect_to'] = url("admin/buy-for-me/detail/{$response['reqid']}?$url");
						} else {
							$response['redirect_to'] = url("admin/post-buyforme/payment/{$response['reqid']}?$url");
						}
					/*}*/

				}
			}
		}
		return response()->json($response);
	}

	public function emailToRequester($id) {
		$delivery = Deliveryrequest::where(array('_id' => $id))
			->select('RequesterId', 'RequesterName', 'ProductList', 'PackageId', 'PackageNumber', 'ProductTitle', 'shippingCost', 'TotalCost', 'insurance', 'PickupFullAddress', 'DeliveryFullAddress', 'RequestType')->first();
		if ($delivery) {
			$users = User::where(['_id' => $delivery->RequesterId])->select('Email', 'Name')->first();
			if($delivery->RequestType == 'buy_for_me'){
				$link = url('buyforme-payment') . '/' . (string) $delivery->_id;
			}else if($delivery->RequestType == 'online'){
				$link = url('online-payment') . '/' . (string) $delivery->_id;
			}

			send_mail('5a7d7d4ce4b06005c659a45e', [
				"to" => $users->Email,
				"replace" => [
					"[TITLE]"=> ucfirst($delivery->ProductTitle),
					"[USERNAME]" => $users->Name,
					"[PACKAGETITLE]" => ucfirst($delivery->ProductTitle),
					"[PACKAGEID]" => $delivery->PackageNumber,
					"[TOTALCOST]" => $delivery->TotalCost,
					"[SHIPPINGCOST]" => $delivery->shippingCost,
					"[INSURANCECOST]" => $delivery->insurance,
					"[DROPOFFADDRESS]" => $delivery->DeliveryFullAddress,
					"[LINK]" => "<a href='$link'>$link</a>",
				],
			]);

			$users->temprary = 'temp';
			$users->detail_url = $link;
			$users->save();

		}
	}

	public function post_buyforme($id) {
		/* sent mail */
		$delivery = Deliveryrequest::where(array('_id' => $id))
			->select('RequesterId', 'RequesterName', 'ProductList', 'PackageId', 'PackageNumber', 'ProductTitle', 'shippingCost', 'TotalCost', 'insurance', 'PickupFullAddress', 'DeliveryFullAddress', 'RequestType')->first();
		if ($delivery) {
			$array = $delivery->ProductList;
			$product_name = '';

			foreach ($delivery->ProductList as $key => $value) {
				if (trim(Input::get('product_id')) == $value['_id']) {
					$array[$key]['inform_mail_sent'] = "yes";
					$product_name = $array[$key]['product_name'];
				}
			}
			$updateData = Deliveryrequest::where(array('_id' => $id))->update(['ProductList' => $array]);

			$users = User::where(['_id' => $delivery->RequesterId])->select('Name', 'Email','Default_Currency','NotificationId','DeviceType')->first();
			if (strtolower($delivery->RequestType) == 'online') {
				$sublink = 'online-payment';
			} elseif (strtolower($delivery->RequestType) == 'buy_for_me') {
				$sublink = 'buyforme-payment';
			}
			$link = url($sublink) . '/' . (string) $delivery->_id;
			$currency_conversion = $this->currency_conversion($delivery->TotalCost);

			if ($users->Default_Currency == 'CAD') {
				$user_currency = $currency_conversion['canadian_cost'];
			} else if ($users->Default_Currency == 'PHP') {
				$user_currency = $currency_conversion['philipins_cost'];
			} else if ($users->Default_Currency == 'GBP') {
				$user_currency = $currency_conversion['uk_cost'];
			} else {
				$user_currency = $currency_conversion['ghanian_cost'];
			}
			if ($users) {
				$Email = new NewEmail();

				

				/*$Email->send_mail('5a7d7d4ce4b06005c659a45e', [
					"to" => $users->Email,
					"replace" => [
						"[TITLE]"=> ucfirst($delivery->ProductTitle),
						"[USERNAME]" => $users->Name,
						"[PACKAGETITLE]" => ucfirst($product_name),
						"[PACKAGEID]" => $delivery->PackageNumber,
						"[TOTALCOST]" => $delivery->TotalCost,
						"[SHIPPINGCOST]" => $delivery->shippingCost,
						"[INSURANCECOST]" => $delivery->insurance,
						"[DROPOFFADDRESS]" => $delivery->DeliveryFullAddress,
						"[USERCURRENCY]" => $user_currency,
						"[LINK]" => "<a href='$link'>$link</a>",
					],
				]);*/
				
				$users = User::where(['_id' => $delivery->RequesterId])->update(['link' => $link, 'packagePayment' => 'yes']);
			}
		}
/* sent mail */
		$data['delivery'] = $delivery;
		return view('Admin::Buy.post_buyforme_payment', $data);
	}
	public function buyforme_payment(Request $request) {

		$validation = Validator::make($request->all(), [
			'transaction' => 'required',
			'description' => 'required',
		]);
		if ($validation->fails()) {
			return Redirect::to("admin/post-buyforme/payment/" . Input::get('url_id'))->withErrors($validation)->withInput();
		} else {
			$delivery = Deliveryrequest::find(Input::get('url_id'));
			if (count($delivery) > 0) {

				$delivery->Status = 'ready';
				$oldEmailNotifiedStatus = $delivery->email_notified_to_client;
				$delivery->email_notified_to_client = true;
				$productList = $delivery->ProductList;
				foreach ($productList as $key => $val) {
					$val['status'] = 'ready';
					$productList[$key] = $val;
				}
				$delivery->ProductList = $productList;
				$delivery->save();
				$user_data = User::where(['_id' => $delivery->RequesterId])->first();

				$insert = [
					'SendById' => new MongoId($delivery->RequesterId),
					'SendByName' => $delivery->RequesterName,
					'TransactionType' => 'buy_for_me',
					'Description' => Input::get('description'),
					'Credit' => $delivery->TotalCost,
					'RecieveByName' => 'Aquantuo',
					'Debit' => '',
					'Status' => 'paid',
					'Transaction_id' => Input::get('transaction'),
					'EnterOn' => new MongoDate(),
				];

				$result = Transaction::insert($insert);

				if (count($user_data)) {

					$productList = $delivery->ProductList;
					foreach ($productList as $key => $val) {
						$insertactivity = [
							'request_id' => Input::get('url_id'),
							'request_type' => 'buy_for_me',
							'PackageNumber' => $delivery->PackageNumber,
							'item_id' => $val['_id'],
							'package_id' => $val['package_id'],
							'item_name' => $val['product_name'],
							'log_type' => 'request',
							'message' => 'Payment successful.',
							'status' => 'ready',
							'action_user_id' => Auth::user()->_id,
							'EnterOn' => new MongoDate(),
						];

						Activitylog::insert($insertactivity);
					}

					if ($oldEmailNotifiedStatus != true) {
						$link = url("buy-for-me-detail") . "/" . $delivery->_id;
						if ($user_data->EmailStatus == "on") {
							send_mail('587748397ac6f63c1a8b456d', [
								"to" => $user_data->Email,
								"replace" => [
									"[USERNAME]" => $user_data->Name,
									"[PACKAGETITLE]" => ucfirst($delivery->ProductTitle),
									"[DESTINATION]" => $delivery->DeliveryFullAddress,
									"[SHIPPINGCOST]" => $delivery->ShippingCost,
									"[TOTALCOST]" => $delivery->TotalCost,
									'[LINK]' => "<a href='$link'>$link</a>",
									"[PRFEE]"=> @$delivery->ProcessingFees,
								],
							]);
						}
					}
				}

				return Redirect::to("admin/buy-for-me/detail/" . $delivery->_id)->withSuccess('Transaction has been successfully.');
			} else {
				return Redirect::to("admin/buy-for-me")
					->with('fail', 'Something went wrong.');
			}

		}
	}

	// End buy for me section
	public function on_line_purchases() {
		$data['country'] = CityStateCountry::where(['type' => 'Country', 'Status' => 'Active'])
			->get(['_id', 'Content', 'state_available']);
		$data['user'] = User::select('_id', 'Name', 'Email')->orderBy('Name', 'asc')->get();
		$data['category'] = Category::where(['type' => 'Category', 'Status' => 'Active'])->whereIn('TravelMode', ['ship', 'air'])->orderBy('Content', 'asc')->get(['TravelMode', 'Content']);
		$field = ['country', 'state', 'city', 'address_line_1', 'address_line_2', 'zipcode', 'lat', 'lng'];
		$data['address'] = Address::where('user_id', '=', Auth::user()->_id)->get($field);

		//$data['additem'] = Additem::where('user_id', '=', Auth::user()->_id)->where(array('request_type' => 'online'))->limit(5)->get();
		if(Input::get('listing')!= ''){
			if(Input::get('listing') == 'own'){
				$data['additem'] = Additem::where('user_id', '=', Auth::user()->_id)->where(array('request_type' => 'online','user_type' => 'admin'))->limit(5)->get();
			}else if(Input::get('listing') == 'all'){
				$data['additem'] = Additem::where(['request_type' => 'online', 'user_type' => 'admin'])->limit(5)->get();
			}else{
				return Redirect::to("admin/online-package");
			}
			
		}else{
			$data['additem'] = Additem::where(['request_type' => 'online', 'user_type' => 'admin'])->limit(5)->get();
		}
		return view('Admin::Buy.on-line-purchases', $data);
	}

	public function add_online_item(Request $request) {
		$response = array("success" => 0, "msg" => "Oops! Something went wrong.");
		$validation = Validator::make($request->all(), [
			'item_name' => 'required',
			'purchase_from' => 'required',
			'item_price' => 'required',
			'category' => 'required',
			'description' => 'required'
		]);

		if ($validation->fails()) {
			$response['error'] = $validation->errors();
			return response()->json($response);
		} else {
			// $lhwunit = 'inches';
			// $weightunit = 'lbs';

			// if (Input::get('measurement_unit') == 'cm_kg') {
			// 	$lhwunit = 'cm';
			// 	$weightunit = 'kg';
			// }

			// $json_decode_category = json_decode(Input::get('category'));
			// $cat_name = Category::where(['_id'=> $json_decode_category->id])->first();

			// if(count($cat_name) == 0){
			// 	return response()->json($response); die;
			// }

			// // Stop user to add item with shipping mode sea but different category.
			// $is_make_request = "yes";
			// $item_with_sea = false;
			// $item_with_air = false;

			// if (empty(trim(Input::get("request_id")))) {	// "travelMode" => "ship", 
			// 	$shipItemList = Additem::where(array("user_id" => Auth::user()->_id, "request_type" => "online"))->select("category_id", 'travelMode')->get();

			// 	foreach ($shipItemList as $value) {
			// 		if ($value->category_id != @$json_decode_category->id && $value->travelMode == 'ship') {
			// 			$is_make_request = "no";
			// 			$item_with_sea = true;
			// 		}

			// 		if (Input::get("travel_mode") == "ship") {
			// 			$item_with_sea = true;
			// 		}

			// 		if ($value->travelMode == 'air' || Input::get('travel_mode') == 'air') {
			// 			$item_with_air = true;
			// 		}

			// 		if ($item_with_air && $item_with_sea) {
			// 			$response['msg'] = "Sorry! You can’t have different shipping modes in the same listing. Please create a separate listing for either air or sea.";
			// 			return response()->json($response);
			// 			die();
			// 		}
			// 	}
			// } else {
			// 	$item_list = Session::get(trim(Input::get("request_id")));
			// 	foreach ($item_list as $key) {
			// 		if ($key["categoryid"] != @$json_decode_category->id && $key["travelMode"] == "ship") {
			// 			$is_make_request = "no";
			// 			$item_with_sea = true;
			// 		}

			// 		if (Input::get('travel_mode') == 'ship') {
			// 			$item_with_sea = true;
			// 		}

			// 		if ($key['travelMode'] == 'air' || Input::get('travel_mode') == 'air') {
			// 			$item_with_air = true;
			// 		}

			// 		if ($item_with_air && $item_with_sea) {
			// 			$response['msg'] = "Sorry! You can’t have different shipping modes in the same listing. Please create a separate listing for either air or sea.";
			// 			return response()->json($response);
			// 			die();
			// 		}
			// 	}
			// }
			
			// if ($is_make_request == "no") {
			// 	$response["msg"] = "If shipping by Sea, all items must be of the same sea category. Please create a new listing if you need to use a different sea category.";
			// 	return response()->json($response);
			// 	die();
			// }

			// // Calculate value
			// $requesthelper = new Requesthelper([
			// 	"needInsurance" => Input::get('insurance'),
			// 	"productQty" => (Input::get('quentity') < 1) ? 1 : Input::get('quentity'),
			// 	"productCost" => $insert['price'],
			// 	"productWidth" => $insert['width'],
			// 	"productWidthUnit" => $lhwunit,
			// 	"productHeight" => $insert['height'],
			// 	"productHeightUnit" => $lhwunit,
			// 	"productLength" => $insert['length'],
			// 	"productLengthUnit" => $lhwunit,
			// 	"productWeight" => $insert['weight'],
			// 	"productWeightUnit" => $weightunit,
			// 	"productCategory" => @$json_decode_category->name,
			// 	'productCategoryId' => @$json_decode_category->id,
			// 	"distance" => 0,
			// 	"travelMode" => Input::get('travel_mode'),
			// ]);

			$lhwunit = 'inches';
			$weightunit = 'lbs';
			if (Input::get('measurement_unit') == 'cm_kg') {
				$lhwunit = 'cm';
				$weightunit = 'kg';
			}

			$json_decode_category = json_decode(Input::get('category'));
			$cat_name = Category::where(['_id'=> $json_decode_category->id])->first();

			if(count($cat_name) == 0){
				return response()->json($response); die;
			}
			//user t stop to if mode wa sea and category diffrent
			$is_make_request = 'yes';
			$item_with_sea = false;
			$item_with_air = false;

			if (empty(trim(Input::get('request_id')))) { // in where 'travel_mode'=>'ship',
				$shipItemList = Additem::where(array('user_id' => Auth::user()->_id, 'request_type' => 'online'))->select('categoryid', 'travelMode')->get();
				
				foreach ($shipItemList as $value) {
					if ($value->categoryid != $json_decode_category->id && $value->travelMode == 'ship') {
						$is_make_request = 'no';
						$item_with_sea = true;
					}

					if (Input::get('travel_mode') == 'ship') {
						$item_with_sea = true;
					}

					if ($value->travelMode == 'air' || Input::get('travel_mode') == 'air') {
						$item_with_air = true;
					}

					if ($item_with_air && $item_with_sea) {
						$response['msg'] = "Sorry! You can’t have different shipping modes in the same listing. Please create a separate listing for either air or sea.";
						return response()->json($response);
						die();
					}
				}
			} else {
				$item_list = Session::get(trim(Input::get('request_id')));
				foreach ($item_list as $key) {
					if ($key['categoryid'] != $json_decode_category->id && $key['travelMode'] == 'ship') {
						$is_make_request = 'no';
						$item_with_sea = true;
					}

					if (Input::get('travel_mode') == 'ship') {
						$item_with_sea = true;
					}

					if ($key['travelMode'] == 'air' || Input::get('travel_mode') == 'air') {
						$item_with_air = true;
					}

					if ($item_with_air && $item_with_sea) {
						$response['msg'] = "Sorry! You can’t have different shipping modes in the same listing. Please create a separate listing for either air or sea.";
						return response()->json($response);
						die();
					}
				}	
			}

			if ($is_make_request == 'no') {
				$response['msg'] = "If shipping by sea, all items must be of the same sea category. Please create a new listing if you need to use a different sea category.";
				return response()->json($response);
				die();
			}

			$insert = [
				'item_image' => '',
				'image' => '',
				'item_name' => Input::get('item_name'),
				'product_name' => Input::get('item_name'),
				'add_item_url' => Input::get('purchase_from'),
				'url' => Input::get('purchase_from'),
				'measurement_unit' => Input::get('measurement_unit'),
				'weight_unit' => (Input::get('measurement_unit') == 'cm_kg') ? 'kg' : 'lbs',
				'length' => Input::get('length'),
				'width' => Input::get('width'),
				'height' => Input::get('height'),
				'weight' => Input::get('weight'),
				'item_price' => Input::get('item_price'),
				'price' => Input::get('item_price'),
				'quentity' => Input::get('quentity'),
				'qty' => Input::get('quentity'),
				'travel_mode' => Input::get('travel_mode'),
				'travelMode' => Input::get('travel_mode'),
				'category' => @$cat_name->Content,
				'category_id' => @$json_decode_category->id,
				'categoryid' => @$json_decode_category->id,
				'description' => Input::get('description'),
				'insurance' => Input::get('insurance'),
				'insurance_status' => Input::get('insurance'),
				'request_type' => 'online',
				'user_type' => 'admin',
				'user_id' => Auth::user()->_id,
				'buy_for_me_dimensions' => Input::get('buy_for_me_dimensions'),
				'ExpectedDate' => '',
				'EnterOn' => new MongoDate,
			];

			// Calculate value
			$requesthelper = new Requesthelper(
				[
					"needInsurance" => Input::get('insurance'),
					"productQty" => (Input::get('quentity') < 1) ? 1 : Input::get('quentity'),
					"productCost" => $insert['price'],
					"productWidth" => $insert['width'],
					"productWidthUnit" => $lhwunit,
					"productHeight" => $insert['height'],
					"productHeightUnit" => $lhwunit,
					"productLength" => $insert['length'],
					"productLengthUnit" => $lhwunit,
					"productWeightUnit" => $weightunit,
					"productCategory" => @$json_decode_category->name,
					'productCategoryId' => @$json_decode_category->id,
					"distance" => 0,
					"travelMode" => $insert['travelMode'],
				]
			);
			$requesthelper->calculate();
			$calculationinfo = $requesthelper->get_information();

			if (isset($calculationinfo->error)) {

				$response['msg'] = $calculationinfo->error;
				$response['calculationinfo'] = $calculationinfo;
				$response['$cat_name'] = $cat_name;
				return response()->json($response);

			}

			if (Input::file('item_image')) {
				$extension = Input::file('item_image')->getClientOriginalExtension(); // getting image extension
				$fileName = time() . rand(100, 999) . '.' . $extension; // Rename img image
				if (Input::file('item_image')->move(BASEURL_FILE . 'package/', $fileName)) {
					$insert['image'] = "package/" . $fileName;
				}
			}
			$data['items'] = [];

			if (!empty(trim(Input::get('request_id')))) {
				// This will execute on edit reqeust
				$insert['_id'] = (String) new MongoId();
				$data['items'][] = $insert;

				foreach (session::get(trim(Input::get('request_id'))) as $key) {
					$data['items'][] = $key;
				}
				session::put(trim(Input::get('request_id')), $data['items']);

			} else {
				$response = Additem::insert($insert);
				$data['items'] = Additem::where(["user_id" => Auth::user()->_id, "request_type" => "online"])->get();
			}
			$data['form_type'] = Input::get('form_type');
			$response = array("success" => 1, "msg" => "Item has been added successfully.");
			$view = view('Admin::Buy.ajax.edit_online_item', $data);

			$response['html'] = $view->render();

		}
		return response()->json($response);
	}

	public function edit_buy_for_me_item(Request $request) {
		$response = array("success" => 0, "msg" => "Oops! Something went wrong.");

		$online_item = [];
		if (empty(trim(Input::get('request_id')))) {
			$online_item[Input::get('item_id')] = Additem::where([
				'_id' => Input::get('item_id'),
				'user_id' => Auth::user()->_id,
				'request_type' => 'buy_for_me',
			])->select('image')->first();
		} else {
			$product_list = Session::get(trim(Input::get('request_id')));
			foreach ($product_list as $key) {
				$online_item[(String) $key['_id']] = $key;
			}
		}

		if (isset($online_item[Input::get('item_id')])) {

			$validation = Validator::make($request->all(), [
				'item_name' => 'required',
				'purchase_from' => 'required',
				'measurement_unit' => 'required',
				'item_price' => 'required',
				'quentity' => 'required',
				'travel_mode' => 'required',
				'description' => 'required',
				'insurance' => 'required',
			]);

			if ($validation->fails()) {
				$validation->errors()->add('field', trans('Something is wrong'));
				return $validation->errors();
			} else {
				$data['items'] = [];
				$json_decode_category = json_decode(Input::get('category'));

				// Stop user to add item with shipping mode sea but different category.
				$is_make_request = "yes";
				$item_with_sea = false;
				$item_with_air = false;

				if (empty(trim(Input::get("request_id")))) {	// "travelMode" => "ship", 
					$shipItemList = Additem::where(array("user_id" => Auth::user()->_id, "request_type" => "buy_for_me"))->select("category_id", 'travelMode')->get();

					if (count($shipItemList) > 1) {
						foreach ($shipItemList as $value) {
							if ($value->category_id != @$json_decode_category->id && $value->travelMode == 'ship') {
								$is_make_request = "no";
								$item_with_sea = true;
							}

							if (Input::get("travel_mode") == "ship") {
								$item_with_sea = true;
							}

							if (Input::get("travel_mode") == "air" || $value->travelMode == 'air') {
								$item_with_air = true;
							}

							if ($item_with_sea && $item_with_air) {
								$response["msg"] = "Sorry! You can’t have different shipping modes in the same listing. Please create a separate listing for either air or sea.";
								return response()->json($response);
								die();
							}
						}
					}
				} else {
					$item_list = Session::get(trim(Input::get("request_id")));

					if (count($item_list) > 1) {
						foreach ($item_list as $key) {
							if ($key["categoryid"] != @$json_decode_category->id && $key["travelMode"] == "ship") {
								$is_make_request = "no";
								$item_with_sea = true;
							}

							if (Input::get("travel_mode") == "ship") {
								$item_with_sea = true;
							}

							if ($key["travelMode"] == "air" || Input::get("travel_mode") == "air") {
								$item_with_air = true;
							}

							if ($item_with_sea && $item_with_air) {
								$response["msg"] = "Sorry! You can’t have different shipping modes in the same listing. Please create a separate listing for either air or sea.";
								return response()->json($response);
								die();
							}
						}
					}
				}
				
				if ($is_make_request == "no") {
					$response["msg"] = "If shipping by Sea, all items must be of the same sea category. Please create a new listing if you need to use a different sea category.";
					return response()->json($response);
					die();
				}

				$lhwunit = 'inches';
				$weightunit = 'lbs';
				
				if (Input::get('measurement_unit') == 'cm_kg') {
					$lhwunit = 'cm';
					$weightunit = 'kg';
				}

				$update = [
					'image' => Input::get('old_image'),
					'item_name' => Input::get('item_name'),
					'product_name' => Input::get('item_name'),
					'add_item_url' => Input::get('purchase_from'),
					'url' => Input::get('purchase_from'),
					'measurement_unit' => Input::get('measurement_unit'),
					'weight_unit' => (Input::get('measurement_unit') == 'cm_kg') ? 'kg' : 'lbs',
					'length' => Input::get('length'),
					'width' => Input::get('width'),
					'height' => Input::get('height'),
					'weight' => Input::get('weight'),
					'item_price' => Input::get('item_price'),
					'price' => Input::get('item_price'),
					'quentity' => Input::get('quentity'),
					'qty' => Input::get('quentity'),
					'travel_mode' => Input::get('travel_mode'),
					'travelMode' => Input::get('travel_mode'),
					'category' => @$json_decode_category->name,
					'category_id' => @$json_decode_category->id,
					'categoryid' => @$json_decode_category->id,
					'description' => Input::get('description'),
					'insurance' => Input::get('insurance'),
					'insurance_status' => Input::get('insurance'),
					'request_type' => 'buy_for_me',
					'buy_for_me_dimensions' => Input::get('buyforme_dimensions'),
					'shipping_cost_by_user' => Input::get('shipping_cost'),

				];

				// Calculate value
				//~ $requesthelper = new Requesthelper(
					//~ [
						//~ "needInsurance" => Input::get('insurance'),
						//~ "productQty" => (Input::get('quentity') < 1) ? 1 : Input::get('quentity'),
						//~ "productCost" => $update['price'],
						//~ "productWidth" => $update['width'],
						//~ "productWidthUnit" => $lhwunit,
						//~ "productHeight" => $update['height'],
						//~ "productHeightUnit" => $lhwunit,
						//~ "productLength" => $update['length'],
						//~ "productLengthUnit" => $lhwunit,
						//~ "productWeight" => $update['weight'],
						//~ "productWeightUnit" => $weightunit,
						//~ "productCategory" => @$json_decode_category->name,
						//~ 'productCategoryId' => @$json_decode_category->id,
						//~ "distance" => 0,
						//~ "travelMode" => $update['travelMode'],
					//~ ]
				//~ );

				//~ $requesthelper->calculate();
				//~ $calculationinfo = $requesthelper->get_information();

				//~ if (isset($calculationinfo->error)) {
					//~ $response['msg'] = $calculationinfo->error;
					//~ return response()->json($response);
				//~ }

				if (Input::hasfile('item_image') != '') {
					$extension = Input::file('item_image')->getClientOriginalExtension(); // getting image extension
					$fileName = time() . rand(100, 999) . '.' . $extension; // Rename img image
					if (Input::file('item_image')->move(BASEURL_FILE . 'package/', $fileName)) {

						if (isset($online_item[Input::get('item_id')]['image']) && ($online_item[Input::get('item_id')]['image']) != "") {
							if (file_exists(BASEURL_FILE . $online_item[Input::get('item_id')]['image'])) {
								unlink(BASEURL_FILE . $online_item[Input::get('item_id')]['image']);
							}
						}
						$update['image'] = "package/" . $fileName;
					}
				}

				if (empty(trim(Input::get('request_id')))) {
					$result = Additem::where(array('_id' => Input::get('item_id'), 'user_id' => Auth::user()->_id, 'request_type' => 'buy_for_me'))->update($update);

					$data['items'] = Additem::where(['user_id' => Auth::user()->_id, 'request_type' => 'buy_for_me'])->get();

				} else {
					$update['_id'] = Input::get('item_id');
					$online_item[Input::get('item_id')] = $update;
					foreach ($online_item as $okey => $oval) {
						$data['items'][] = $oval;
					}

					session::put(trim(Input::get('request_id')), $data['items']);
				}
				$data['form_type']=Input::get('form_type');
				$response = array("success" => 1, "msg" => "Item has been updated successfully.");
				$view = view('Admin::Buy.ajax.edit_buy_for_me', $data);
				$response['edit_html'] = $view->render();
			}
		} else {
			$response = array("success" => 1, "msg" => "You have no permission.");
		}
		return response()->json($response);
	}

	public function edit_online_item(Request $request) {
		$response = array("success" => 0, "msg" => "Oops! Something went wrong.");
		$online_item = [];
		if (empty(trim(Input::get('request_id')))) {
			$online_item[Input::get('item_id')] = Additem::where([
				'_id' => Input::get('item_id'),
				'user_id' => Auth::user()->_id,
				'request_type' => 'online',
			])->select('image')->first();
		} else {
			$product_list = Session::get(Input::get('request_id'));
			foreach ($product_list as $key => $val) {
				$online_item[(String) $val['_id']] = $val;
			}
		}

		if (isset($online_item[Input::get('item_id')])) {
			$validation = Validator::make($request->all(), [
				'item_name' => 'required',
				'purchase_from' => 'required',
				'measurement_unit' => 'required',
				'item_price' => 'required',
				'quentity' => 'required',
				'travel_mode' => 'required',
				'description' => 'required',
				'insurance' => 'required',
			]);

			if ($validation->fails()) {
				$validation->errors()->add('field', trans('Something is wrong'));
				return $validation->errors();
			} else {
				$json_decode_category = json_decode(Input::get('category'));
				$cat_name = Category::where(['_id'=> $json_decode_category->id])->first();
				if(count($cat_name) == 0){
					return response()->json($response); die;
				}

				// Stop user to add item with shipping mode sea but different category.
				$is_make_request = "yes";
				$item_with_sea = false;
				$item_with_air = false;

				if (empty(trim(Input::get("request_id")))) {	// "travelMode" => "ship", 
					$shipItemList = Additem::where(array("user_id" => Auth::user()->_id, "request_type" => "online"))->select("category_id", 'travelMode')->get();

					if (count($shipItemList) > 1) {
						foreach ($shipItemList as $value) {
							if ($value->category_id != @$json_decode_category->id && $value->travelMode == 'ship') {
								$is_make_request = "no";
								$item_with_sea = true;
							}

							if (Input::get("travel_mode") == "ship") {
								$item_with_sea = true;
							}

							if ($value->travelMode == 'air' || Input::get('travel_mode') == 'air') {
								$item_with_air = true;
							}

							if ($item_with_air && $item_with_sea) {
								$response['msg'] = "Sorry! You can’t have different shipping modes in the same listing. Please create a separate listing for either air or sea.";
								return response()->json($response);
								die();
							}
						}
					}
				} else {
					$item_list = Session::get(trim(Input::get("request_id")));
					
					if (count($item_list) > 1) {
						foreach ($item_list as $key) {
							if ($key["categoryid"] != @$json_decode_category->id && $key["travelMode"] == "ship") {
								$is_make_request = "no";
								$item_with_sea = true;
							}

							if (Input::get('travel_mode') == 'ship') {
								$item_with_sea = true;
							}

							if ($key['travelMode'] == 'air' || Input::get('travel_mode') == 'air') {
								$item_with_air = true;
							}

							if ($item_with_air && $item_with_sea) {
								$response['msg'] = "Sorry! You can’t have different shipping modes in the same listing. Please create a separate listing for either air or sea.";
								return response()->json($response);
								die();
							}
						}
					}
				}
				
				if ($is_make_request == "no") {
					$response["msg"] = "If shipping by Sea, all items must be of the same sea category. Please create a new listing if you need to use a different sea category.";
					return response()->json($response);
					die();
				}

				$lhwunit = 'inches';
				$weightunit = 'lbs';
				if (Input::get('measurement_unit') == 'cm_kg') {
					$lhwunit = 'cm';
					$weightunit = 'kg';
				}

				$data['items'] = [];
				$update = [
					'image'=>Input::get('old_image'),
					'item_image' => '',
					'item_name' => Input::get('item_name'),
					'product_name' => Input::get('item_name'),
					'add_item_url' => Input::get('purchase_from'),
					'url' => Input::get('purchase_from'),
					'measurement_unit' => Input::get('measurement_unit'),
					'weight_unit' => (Input::get('measurement_unit') == 'cm_kg') ? 'kg' : 'lbs',
					'length' => Input::get('length'),
					'width' => Input::get('width'),
					'height' => Input::get('height'),
					'weight' => Input::get('weight'),
					'item_price' => Input::get('item_price'),
					'price' => Input::get('item_price'),
					'quentity' => Input::get('quentity'),
					'qty' => Input::get('quentity'),
					'travel_mode' => Input::get('travel_mode'),
					'travelMode' => Input::get('travel_mode'),
					'category' => @$cat_name->Content,
					'category_id' => @$json_decode_category->id,
					'categoryid' => @$json_decode_category->id,
					'description' => Input::get('description'),
					'insurance' => Input::get('insurance'),
					'insurance_status' => Input::get('insurance'),
					'request_type' => 'online',
					'buy_for_me_dimensions' => Input::get('buyforme_dimensions'),

				];

				// Calculate value
				//~ $requesthelper = new Requesthelper(
					//~ [
						//~ "needInsurance" => Input::get('insurance'),
						//~ "productQty" => (Input::get('quentity') < 1) ? 1 : Input::get('quentity'),
						//~ "productCost" => $update['price'],
						//~ "productWidth" => $update['width'],
						//~ "productWidthUnit" => $lhwunit,
						//~ "productHeight" => $update['height'],
						//~ "productHeightUnit" => $lhwunit,
						//~ "productLength" => $update['length'],
						//~ "productLengthUnit" => $lhwunit,
						//~ "productWeight" => $update['weight'],
						//~ "productWeightUnit" => $weightunit,
						//~ "productCategory" => @$json_decode_category->name,
						//~ 'productCategoryId' => @$json_decode_category->id,
						//~ "distance" => 0,
						//~ "travelMode" => $update['travelMode'],
					//~ ]
				//~ );

				//~ $requesthelper->calculate();
				//~ $calculationinfo = $requesthelper->get_information();

				//~ if (isset($calculationinfo->error)) {

					//~ $response['msg'] = $calculationinfo->error;
					//~ return response()->json($response);

				//~ }

				if (Input::file('item_image')) {
					$extension = Input::file('item_image')->getClientOriginalExtension(); // getting image extension
					$fileName = time() . rand(100, 999) . '.' . $extension; // Rename img image
					if (Input::file('item_image')->move(BASEURL_FILE . 'package/', $fileName)) {

						if (isset($online_item[Input::get('item_id')]['image']) && ($online_item[Input::get('item_id')]['image']) != "") {
							if (file_exists(BASEURL_FILE . $online_item[Input::get('item_id')]['image'])) {
								unlink(BASEURL_FILE . $online_item[Input::get('item_id')]['image']);
							}
						}

						$update['image'] = "package/" . $fileName;
					}
				}
				if (empty(trim(Input::get('request_id')))) {
					$result = Additem::where(array('_id' => Input::get('item_id'), 'user_id' => Auth::user()->_id, 'request_type' => 'online'))->update($update);

					$data['items'] = Additem::where(['user_id' => Auth::user()->_id, 'request_type' => 'online'])->get();
				} else {
					$update['_id'] = Input::get('item_id');
					$online_item[Input::get('item_id')] = $update;
					foreach ($online_item as $okey => $oval) {
						$data['items'][] = $oval;
					}

					session::put(trim(Input::get('request_id')), $data['items']);
				}

				$response = array("success" => 1, "msg" => "Item has been updated successfully.");
				$view = view('User::Buy.ajax.edit_online_item', $data);
				$response['edit_html'] = $view->render();
			}
		}
		return response()->json($response);
	}

	public function delete_item(Request $request, $id, $function, $rid='') {
		$responce = array('success' => 0, 'msg' => 'Fail to delete request.');

		if ($function == 'DeleteItem') {
		
			
			if (!empty(trim(Input::get('request_id'))) && session()->has(trim(Input::get('request_id')))) {
				$product_list = [];

				if (is_array(session()->get('request_id'))) {

					foreach (session()->get(trim(Input::get('request_id'))) as $key => $val) {
						$product_list[(String) $val['_id']] = $val;
					}
					if (isset($product_list[$id])) {
						unset($product_list[$id]);
						session()->put(trim(Input::get('request_id')), []);
						foreach ($product_list as $key) {
							session()->push(trim(Input::get('request_id')), $key);
						}
						$responce['success'] = 1;
						$responce['msg'] = 'Item has been removed successfully.';
					}
				}

			} else {
				$item = Additem::find($id);

				if (count($item) > 0) {
					if (!empty(trim($item->image))) {
						if (file_exists(BASEURL_FILE . $item->image)) {
							unlink(BASEURL_FILE . $item->image);
						}
					}
					$item->delete();
					$responce['success'] = 1;
					$responce['msg'] = 'Item has been removed successfully.';
				}
			}
		}

		if ($function == 'edit_send_package_item') {
			$requestid = $request->segment(5);
			if (isset($requestid)) {
				$res_data = Deliveryrequest::where(['_id' => trim($requestid)])->select('ProductList')->first();

				if (count($res_data) > 0) {
					if (isset($res_data->ProductList)) {
						$update_array = $res_data->ProductList;

						foreach ($update_array as $key => $value) {
							if ($value['_id'] == $id) {
								unset($update_array[$key]);
							}
						}

						$update = Deliveryrequest::where(['_id' => trim($requestid)])->update(['ProductList' => $update_array]);
						$responce['success'] = 1;
						$responce['msg'] = 'Item has been removed successfully.';
					}
				}
			}

			$item = Additem::find($id);
			if (count($item) > 0) {
				$item->delete();
				$responce['success'] = 1;
				$responce['msg'] = 'Item has been removed successfully.';
			}
		}
		echo json_encode($responce);
	}

	public function delete_item2(Request $request, $id, $function,$rid) {
		$responce = array('success' => 0, 'msg' => 'Fail to delete request.');
		if ($function == 'DeleteItem') {
			if (!empty(trim($rid)) && session()->has(trim($rid))) {
				//print_r($rid); die;
				$product_list = [];

				if (is_array(session()->get($rid))) {

					foreach (session()->get(trim($rid)) as $key => $val) {
						$product_list[(String) $val['_id']] = $val;
					}
					if (isset($product_list[$id])) {
						unset($product_list[$id]);
						session()->put(trim($rid), []);
						foreach ($product_list as $key) {
							session()->push(trim($rid), $key);
						}
						$responce['success'] = 1;
						$responce['msg'] = 'Item has been removed successfully.';
					}
				}

			} else {
				$item = Additem::find($id);

				if (count($item) > 0) {
					if (!empty(trim($item->image))) {
						if (file_exists(BASEURL_FILE . $item->image)) {
							unlink(BASEURL_FILE . $item->image);
						}
					}
					$item->delete();
					$responce['success'] = 1;
					$responce['msg'] = 'Item has been removed successfully.';
				}
			}
		}
		

		echo json_encode($responce);
	}

	public function post_delete_item($id, $function) {

		$responce = array('success' => 0, 'msg' => 'Fail to delete request.');

		if ($function == 'DeleteItem') {
			if (!empty(trim(Input::get('request_id'))) && session()->has(trim(Input::get('request_id')))) {
				$product_list = [];
				if (is_array(session()->get(trim(Input::get('request_id'))))) {
					foreach (session()->get(trim(Input::get('request_id'))) as $key => $val) {
						$product_list[(String) $val['_id']] = $val;
					}
					if (isset($product_list[$id])) {
						unset($product_list[$id]);
						session()->put(trim(Input::get('request_id')), []);
						foreach ($product_list as $key) {
							session()->push(trim(Input::get('request_id')), $key);
						}
						$responce['success'] = 1;
						$responce['msg'] = 'Item has been removed successfully.';
					}
				}

			} else {
				$item = Additem::find($id);

				if (count($item) > 0) {
					if (!empty(trim($item->image))) {
						if (file_exists(BASEURL_FILE . $item->image)) {
							unlink(BASEURL_FILE . $item->image);
						}
					}
					$item->delete();
					$responce['success'] = 1;
					$responce['msg'] = 'Item has been removed successfully.';
				}
			}
		}

		echo json_encode($responce);
	}

	// start online calculation section

	public function online_purchase_calculation() {

		/*$email_check = User::where(['Email' => strtolower(trim(Input::get('email')))])->count();
			if ($email_check > 0) {
				$response = ['success' => 0, 'msg' => 'This email is already registered with us.'];
				echo json_encode($response);
				die;
		*/

		$result = Setting::where('_id', '563b0e31e4b03271a097e1ca')->select('AqLatlong')->first();

		$aqlat = isset($result['AqLatlong'][1]) ? $result['AqLatlong'][1] : '';
		$aqlong = isset($result['AqLatlong'][0]) ? $result['AqLatlong'][0] : '';
    
		// $calculation = new Calculation();
		$calculation = new NewCalculation();

		$data = [
			'shippingcost' => 0,
			'insurance' => 0,
			'ghana_total_amount' => 0,
			'total_amount' => 0,
			'error' => [],
			'formated_text' => '',
			'distance_in_mile' => 0,
			'product_count' => 0,
			'ProcessingFees' => 0,
			'item_cost' => 0,
			'shipping_cost_by_user' => 0,
			'type' => 'online',
			'consolidate_check'=> (Input::get('consolidate_check') == 'on')? 'on':'off'
		];
		$data['is_customs_duty_applied'] = false;
		$addlat = true;
		$lat = floatval(Input::get('lat'));
		$lng = floatval(Input::get('lng'));
		$json_decode_country = json_decode(Input::get('country'));
		$json_decode_state = json_decode(Input::get('state10'));
		$json_decode_city = json_decode(Input::get('city'));

		if ($json_decode_country->name != "USA") {
			$data['is_customs_duty_applied'] = true;
		}

		$addlat = $this->latlongapi2($json_decode_city->name .' ,' . @$json_decode_state->name .' ,'.$json_decode_country->name);

		if ($addlat != false) {
			$lat = $addlat['lat'];
			$lng = $addlat['lng'];
		}

		if ($addlat == false) {
			$response = array("success" => 0, "msg" => "We are unable to find your location. Please  correct it.");
			echo json_encode($response);
			die;
		}

		$response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		$response['total_amount'] = 0;
		$response['ghana_total_amount'] = 0;
		$response['shipping_cost'] = 0;

		if (empty(trim(Input::get('request_id')))) {
			if(Input::get('listing') == 'own'){
				$data['product'] = Additem::where(array('user_id' => Auth::user()->_id, 'request_type' => 'online',"user_type" => "admin"))->get();
			}else{
				$data['product'] = Additem::where(['request_type' => 'online', "user_type" => "admin"])->get();
			}
		} else {

			$data['product'] = session()->get(trim(Input::get('request_id')));
		}
		$configurationdata = Configuration::where(['_id' => '5673e33e6734c4f874685c84'])->first();
		//print_r(count($data['product'])); die;
		if (count($data['product']) > 0) {
			$total_weight = 0;
			$data['distance'] = Utility::GetDrivingDistance($aqlat, $aqlong, $lat, $lng);
			$weight = 0;
			$userinfo = (Object) ['Default_Currency' => 'GHS'];
			// $response = $calculation->getRate($data['product'], $data, $userinfo);
			$response = $calculation->GetRate($data['product'], $data, $userinfo, $json_decode_country->name);
			foreach ($response['product'] as $key => $val) {
				$total_weight += (float) $val['weight'] * (float) $val['qty'];
			}
			if ($response['success'] == 1) {
				$response['outside_accra_charge'] = 0;
				$state = json_decode(Input::get('state10'));
				//out side accra charges
				$response['AreaCharges'] = $this->resionCharges(['name' => $state->name, 'id' => $state->id]);
				// $response['ProcessingFees'] = ((floatval($response['total_amount']) * $configurationdata->aquantuoFees) / 100);
				$response['total_amount'] = $response['total_amount'] + $response['AreaCharges'] - $response['ProcessingFees'];
				$response['total_weight'] = $response['total_weight'];

				$currency_conversion = $this->currency_conversion($response['total_amount']);
				$response['ghana_total_amount'] = $currency_conversion['ghanian_cost'];
				$response["user_currency_rate"] = $currency_conversion["currency_rate_ghs"];
				$response["FormatedText"] = $currency_conversion["FormatedText_ghs"];
				$response['error'] = [];
				$response['shippingCost'] = $response['shippingCost'];
				$response['insurance'] = $response['insurance_cost'];

				$view = view('Admin::Buy.ajax.online_purchase', $response);
				$response['html'] = $view->render();

				$response['product'] = $data['product'];
			}
		}

		return json_encode($response);
	}

	public function currency_conversion($shipcost) {

		$data = ['cost' => 0, 'ghanian_cost' => 0, 'philipins_cost' => 0, 'uk_cost' => 0, 'canadian_cost' => 0, 'kenya_cost' => 0];
		$currency = Currency::where(['Status' => 'Active'])->select('CurrencyRate', 'FormatedText', 'CurrencyCode')->get();

		if ($currency) {
			foreach ($currency as $key) {
				if ($key->CurrencyCode == 'GHS') {
					$data['ghanian_cost'] = str_replace('[AMT]', number_format($shipcost * $key->CurrencyRate, 2), $key->FormatedText);
					$data["currency_rate_ghs"] = $key->CurrencyRate;
					$data["FormatedText_ghs"] = $key->FormatedText;
				} else if ($key->CurrencyCode == 'CAD') {
					$data['canadian_cost'] = str_replace('[AMT]', number_format($shipcost * $key->CurrencyRate, 2), $key->FormatedText);
					$data["currency_rate_cad"] = $key->CurrencyRate;
					$data["FormatedText_cad"] = $key->FormatedText;
				} else if ($key->CurrencyCode == 'PHP') {
					$data['philipins_cost'] = str_replace('[AMT]', number_format($shipcost * $key->CurrencyRate, 2), $key->FormatedText);
					$data["currency_rate_php"] = $key->CurrencyRate;
					$data["FormatedText_php"] = $key->FormatedText;
				} else if ($key->CurrencyCode == 'GBP') {
					$data['uk_cost'] = str_replace('[AMT]', number_format($shipcost * $key->CurrencyRate, 2), $key->FormatedText);
					$data["currency_rate_gbp"] = $key->CurrencyRate;
					$data["FormatedText_gbp"] = $key->FormatedText;
				} else if ($key->CurrencyCode == 'KES') {
					$data['kenya_cost'] = str_replace('[AMT]', number_format($shipcost * $key->CurrencyRate, 2), $key->FormatedText);
					$data["currency_rate_kes"] = $key->CurrencyRate;
					$data["FormatedText_kes"] = $key->FormatedText;
				}
			}
		}
		return $data;

	}

	public function post_online_purchase(Request $request) {
		$dutyCustoms = Input::get("dutyCustoms");
		$tax = Input::get("tax");

		$response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];

		if (Input::get('user') != '') {
		
			$json_decode_user = json_decode(Input::get('user'));
			$requesterid = $json_decode_user->id;
			$requestername = $json_decode_user->name;
		} else {

	
			$email_check = User::where(['Email' => strtolower(trim(Input::get('email')))])->first();
			if (count($email_check) > 0) {
				$response = [
					'success' => 0,
					'msg' => 'This email is already registered with us.',
				];

			} else {


				$pass = rand(100000, 999999);
				$UniqueNo = Input::get('first_name')[0] . Input::get('lastName')[0];
				$insert['FirstName'] = Input::get('first_name');
				$insert['LastName'] = Input::get('last_name');
				$insert['Name'] = ucfirst(Input::get('first_name')) . " " . ucfirst(Input::get('last_name'));

				$insert['Email'] = strtolower(trim(Input::get('email')));
				$insert['Password'] = md5($pass);
				$insert['temprary'] = $pass;
				$insert['TransporterType'] = '';
				$insert['UserType'] = 'requester';
				$insert['ChatName'] = "aquantuo" . Utility::sequence('user');
				$insert['CountryCode'] = "";
				$insert['PhoneNo'] = '';
				$insert['AlternateCCode'] = '';
				$insert['AlternatePhoneNo'] = '';
				$insert['RequesterStatus'] = 'active';
				$insert['Age'] = "";
				$insert['SSN'] = "";
				$insert['Street1'] = '';
				$insert['Street2'] = '';
				$insert['Country'] = '';
				$insert['delete_status'] = 'no';
				$insert['State'] = '';
				$insert['City'] = '';
				$insert['ZipCode'] = '';
				$insert['BankName'] = '';
				$insert['AccountHolderName'] = '';
				$insert['BankAccountNo'] = '';
				$insert['RoutingNo'] = '';
				$insert['StripeId'] = '';
				$insert['TransporterStatus'] = "not_registred";
				$insert['UserStatus'] = "active";
				$insert['Image'] = "";
				$insert['IDProof'] = '';
				$insert['LicenceId'] = '';
				$insert['type_of_id'] = '';
				$insert['TPTrackLocation'] = "on";
				$insert['EmailStatus'] = "on";
				$insert['consolidate_item'] = "on";
				$insert['NoficationStatus'] = "on";
				$insert['TPSetting'] = "on";
				$insert['SoundStatus'] = "on";
				$insert['VibrationStatus'] = "on";
				$insert['EnterOn'] = new MongoDate();
				$insert['UpdateOn'] = new MongoDate();
				$insert['RatingCount'] = 0;
				$insert['RatingByCount'] = 0;
				$insert['CurrentLocation'] = array();
				$insert['DeliveryAreaCountry'] = array();
				$insert['DeliveryAreaState'] = array();
				$insert['DeliveryAreaCities'] = array();
				$insert['ProfileStatus'] = 'step-one';
				$insert['StripeBankId'] = "";
				$insert['NotificationId'] = Input::get('NotificationId');
				$insert['DeviceId'] = Input::get('DeviceId');
				$insert['DeviceType'] = 'web';
				$insert['UniqueNo'] = strtoupper($UniqueNo) . Utility::sequence('user_unique');
				$insert['bank_info'] = [];
				$insert['AqAddress'] = '';
				$insert['AqCity'] = '';
				$insert['AqState'] = '';
				$insert['AqCountry'] = '';
				$insert['AqZipcode'] = '';
				$insert['AqLatLong'] = [];
				$insert['Default_Currency'] = 'USD';
				$insert['PerPage'] = 20;

				// Get aquantuo addres
				$supportemail = Setting::find('563b0e31e4b03271a097e1ca');
				if (count($supportemail) > 0) {
					$insert['AqAddress'] = $supportemail->AqAddress;
					$insert['AqCity'] = $supportemail->AqCity;
					$insert['AqState'] = $supportemail->AqState;
					$insert['AqCountry'] = $supportemail->AqCountry;
					$insert['AqZipcode'] = $supportemail->AqZipcode;
					$insert['AqLatLong'] = $supportemail->AqLatlong;
				}

				$result['_id'] = (string) User::insertGetId($insert);

				if ($result['_id']) {
					$Email = new NewEmail();
					// email to user
					$Email->send_mail('58e4f6f77ac6f6e92d8b4567', [
						'to' => trim(Input::get('email')),
						'replace' => [
							'[USERNAME]' => $insert['Name'],
							'[PASSWORD]' => $pass,
						],
					]);

					$response = array("success" => 1, "msg" => "Requester has been Registered successfully.");
				}
				$requesterid = $result['_id'];
				$requestername = ucfirst(Input::get('first_name')) . " " . ucfirst(Input::get('last_name'));

			}
		}

		$result = Setting::where('_id', '563b0e31e4b03271a097e1ca')->select('AqLatlong')->first();

		$aqlat = isset($result['AqLatlong'][1]) ? $result['AqLatlong'][1] : '';
		$aqlong = isset($result['AqLatlong'][0]) ? $result['AqLatlong'][0] : '';
		// Get distance
		$data['distance'] = Utility::GetDrivingDistance($aqlat, $aqlong, @$address->lat, @$address->lng);

		// End get distance
		$configurationdata = Configuration::where(['_id' => '5673e33e6734c4f874685c84'])->first();
		// $calculation = new Calculation();
		$calculation = new NewCalculation();

		$data = [
			'shippingcost' => 0,
			'insurance' => 0,
			'ghana_total_amount' => 0,
			'total_amount' => 0,
			'error' => [],
			'formated_text' => '',
			'distance_in_mile' => 0,
			'product_count' => 0,
			'ProcessingFees' => 0,
			'item_cost' => 0,
			'shipping_cost_by_user' => 0,
			'type' => 'online',
			'consolidate_check'=> (Input::get('consolidate_check') == 'on')? 'on':'off'
		];

		

		$addlat = true;
		$lat = floatval(Input::get('lat'));
		$lng = floatval(Input::get('lng'));
		$json_decode_country = json_decode(Input::get('country'));
		$json_decode_state = json_decode(Input::get('state10'));
		$json_decode_city = json_decode(Input::get('city'));


		$addlat = $this->latlongapi2($json_decode_city->name .' ,' . @$json_decode_state->name .' ,'.$json_decode_country->name);

		if ($addlat != false) {
			$lat = $addlat['lat'];
			$lng = $addlat['lng'];
		}

		if ($addlat == false) {
			$response = array("success" => 0, "msg" => "We are unable to find your location. Please  correct it.");
			echo json_encode($response);
			die;
		}

		$response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		$response['total_amount'] = 0;
		$response['ghana_total_amount'] = 0;
		$response['shipping_cost'] = 0;

		if (empty(trim(Input::get('request_id')))) {
			$old_packageNumber = Utility::sequence('Request') . time();
			if(Input::get('listing') == 'own'){
				$data['product'] = Additem::where(array('user_id' => Auth::user()->_id, 'request_type' => 'online',"user_type" => "admin"))->get();
			}else{
				$data['product'] = Additem::where(['request_type' => 'online', "user_type" => "admin"])->get();
			}

			$data['product'] = $data['product']->toArray();
		} else {
			$data['product'] = session()->get(trim(Input::get('request_id')));
			$old_packageNumber = Deliveryrequest::where(['_id'=> Input::get('request_id')])->select('PackageNumber')->first();
			$old_packageNumber = $old_packageNumber->PackageNumber;
		}
		if (count($data['product']) > 0) {
			$data['distance'] = Utility::GetDrivingDistance($aqlat, $aqlong, $lat, $lng);
			$weight = 0;
			$userinfo = (Object) ['Default_Currency' => 'GHS'];
			// $response = $calculation->getRate($data['product'], $data, $userinfo);
			$response = $calculation->GetRate($data['product'], $data, $userinfo, trim(@$json_decode_country->name));

			if ($response['success'] == 1) {
				$DeliveryDate = '';
				if (!Input::get('desired_delivery_date') == '') {
					$DeliveryDate = get_utc_time(Input::get('desired_delivery_date'));

				}

				if (@$requesterid == '') {
					$response = array("success" => 0, "msg" => "Please choose user");
					echo json_encode($response);
					die;
				}

				 if (empty(trim(Input::get('request_id')))) { 
					$newdate = new MongoDate();
				}else{
                    $date = Deliveryrequest::where('_id' ,'=',Input::get('request_id'))->select('EnterOn')->first();
					$newdate = $date['EnterOn'];
				}

				
				$insert = [
					'RequesterId' => new MongoId($requesterid),
					'RequesterName' => $requestername,
					'PackageNumber' => $old_packageNumber,
					'ProductTitle' => '',
					'RequestType' => 'online',
					'request_version'=>'new',
					'ReceiverIsDifferent'=> (Input::get('ReceiverIsDifferent') == 'on')?'yes':'no', 
					'ReceiverName'=> (Input::get('ReceiverIsDifferent') == 'on')? ucfirst(Input::get('ReceiverName')):'' ,
					'Status' => 'pending',
					'DeliveryFullAddress' => Utility::formated_address([
						Input::get('address_line_1'),
						Input::get('address_line_2'),
						$json_decode_city->name,
						@$json_decode_state->name,
						$json_decode_country->name,
					], Input::get('zipcode')),
					'DeliveryAddress' => Input::get('address_line_1'),
					'DeliveryAddress2' => Input::get('address_line_2'),
					'DeliveryCity' => trim(@$json_decode_city->name) ,
					'DeliveryState' => trim(@$json_decode_state->name) ,
					'DeliveryCountry' => trim(@$json_decode_country->name),
					'DeliveryPincode' => Input::get('zipcode'),

					"DeliveryStateId" => @$json_decode_state->id,
					"DeliveryCountryId" => @$json_decode_country->id,

					'ReturnToAquantuo' => (Input::get('return_to_aq') == 'yes') ? true : false,
					'ReturnAddress' => Auth::user()->AqAddress,
					'PaymentStatus' => 'paid',
					'ReceiverCountrycode' => '+' . Input::get('country_code'),
					'ReceiverMobileNo' => Input::get('phone_number'),
					'DeliveryDate' => $DeliveryDate,
					'PromoCode' => Input::get('promo_code'),
					'discount' => 0,
					'BeforePurchaseTotalCost' => 0,
					'shipping_cost_by_user' => 0,
					'AquantuoFees' => 0,
					'after_update_difference' => 0,
					'distance' => $calculation->get_distance($data['distance']->distance),
					'distanceType' => 'Miles',
					'shippingCost' => $response['shippingCost'],
					'DutyAndCustom' => floatval($dutyCustoms),	// $response['DutyAndCustom'],
					'Tax' => floatval($tax),	// $response['Tax'],
					'insurance' => 0,
					'TotalCost' => 0,
					'GhanaTotalCost' => @$response['ghana_total_amount'],
					'PickupAddress' => '',
					'consolidate_item'=> (Input::get('consolidate_check') == 'on')? 'on':'off',
					'UpdateOn' => new MongoDate(),
					'EnterOn' => $newdate,
					'PickupLatLong' => [floatval($aqlat), floatval($aqlong)],
					'DeliveryLatLong' => [floatval($lng), floatval($lat)],
					'Userstatus' => 'admin',
					'ProductList' => [],
				];

				$productList = $response['product'];

				$k = 0;
				foreach ($productList as $productKey => $productValue) {

					$lhwunit = 'cm';
					$weightunit = 'kg';
					if ($productValue['weight_unit'] == 'lbs') {
						$lhwunit = 'inches';
						$weightunit = 'lbs';
					}

					$cost = $productValue['insurance'] + $productValue['shippingCost'];

					// $insert['shippingCost'] += $productValue['shippingCost'];
					$insert['insurance'] += $productValue['insurance'];

					if (!empty(trim($insert["ProductTitle"]))) {
						$insert["ProductTitle"] = "{$insert['ProductTitle']}, ";
					}

					$insert["ProductTitle"] .= @$productValue['product_name'];
					$k++;

					$fees = ((floatval($productValue['insurance'] + $productValue['shippingCost']) * $configurationdata->aquantuoFees) / 100);
					$insert['AquantuoFees'] += $fees;

					$insert["ProductList"][] = [
						'_id' => (string) new MongoId(),
						'tpid' => '',
						'tpName' => '',
						'package_id' => $insert['PackageNumber'] . $k,
						'product_name' => @$productValue['product_name'],
						'url' => @$productValue['url'],
						'description' => @$productValue['description'],
						'travelMode' => @$productValue['travelMode'],
						'weight' => @$productValue['weight'],
						'weight_unit' => $weightunit,
						'height' => @$productValue['height'],
						'heightUnit' => $lhwunit,
						'length' => @$productValue['length'],
						'lengthUnit' => $lhwunit,
						'width' => @$productValue['width'],
						'widthUnit' => $lhwunit,
						'category' => @$productValue['category'],
						'categoryid' => @$productValue['categoryid'],
						'price' => @$productValue['price'],
						'qty' => @$productValue['qty'],
						'insurance_status' => @$productValue['insurance_status'],
						'image' => @$productValue['image'],
						///'shipping_cost_by_user' => '',
						'status' => 'pending',
						'verify_code' => rand(1000, 9999),
						'txnid' => '',
						'shippingCost' => $productValue['shippingCost'],
						'insurance' => $productValue['insurance'],
						'discount' => 0,
						'aq_fee' => floatval($fees),
						'TransporterFeedbcak' => '',
						'TransporterRating' => 0,
						'RequesterFeedbcak' => '',
						'RequesterRating' => 0,
						'RejectBy' => '',
						'ReturnType' => '',
						'ReceiptImage' => '',
						'RejectTime' => '',
						'TrackingNumber' => '',
						'TransporterMessage' => '',
						'CancelDate' => '',
						'StripeChargeId' => '',
						'DeliveredDate' => '',
						'tracking_number' => @$productValue['tracking_number'],
						'total_cost' => $productValue['shippingCost'] + $productValue['insurance'] ,
						'after_update' => $productValue['shippingCost'] + $productValue['insurance'] ,
						'PaymentStatus' => 'no',
						'inform_mail_sent' => 'no',
						'ExpectedDate' => '',
						'EnterOn' => new MongoDate(),
					];
				}
				if (count($insert["ProductList"]) <= 0) {
					return response()->json($response);
				}
				$state = json_decode(Input::get('state10'));

				$insert['AreaCharges'] = $this->resionCharges(['name' => $state->name, 'id' => $state->id]);
				//end accra charge
				$insert['TotalCost'] = $insert['TotalCost'] + $insert['AreaCharges'] + $insert['DutyAndCustom'] + $insert['Tax'];
				$insert['discount'] = floatval(Input::get('discount'));
				$insert['TotalCost'] = $insert['TotalCost'] + (($insert['shippingCost'] + $insert['insurance']) - $insert['discount']);
				$insert['BeforePurchaseTotalCost'] = $insert['TotalCost'];

				if ($insert['TotalCost'] <= 0) {
					$insert['Status'] = 'ready';
					foreach ($insert["ProductList"] as $key => $val) {
						$insert["ProductList"][$key]['status'] = 'ready';
					}
				}

				

				if (empty(trim(Input::get('request_id')))) {
					$response = array("success" => 1, "msg" => "Success! Request has been created successfully.");
					$response['reqid'] = (String) Deliveryrequest::insertGetId($insert);

					PaymentInfo::insert([
						'request_id' => $response['reqid'],
						'user_id'=> (string) $insert['RequesterId'],
						'action_user_id'=> Auth::user()->_id,
						'RequestType'=> 'online',
						'action' => 'create',
						'item_id'=> '',
						'TotalCost'=> floatval($insert['TotalCost']),
						'shippingCost'=> floatval($insert['shippingCost']),
						'insurance'=> floatval($insert['insurance']),
						'AreaCharges'=> floatval($insert['AreaCharges']),
						'ProcessingFees'=> 0.0,
						'discount'=> floatval($insert['discount']),
						'PromoCode'=> $insert['PromoCode'],
						'shipping_cost_by_user'=> 0.0,
						'item_cost'=> 0.0,
						'difference'=> floatval($insert['after_update_difference']),
						'difference_before'=> floatval($insert['after_update_difference']),
						'date'=> New MongoDate(),
					]);

					//Additem::where('user_id', '=', Auth::user()->_id)
						//->where(['request_type' => 'online'])->delete();

					$ActivityLog = DeliveryRequest::where('_id', '=', $response['reqid'])->select('_id', 'PackageNumber', 'ProductList')->first();
					//$this->emailToRequester($response['reqid']);

					$array = $ActivityLog->ProductList;

					/* Activity Log  Create online purchase */
					foreach ($array as $key) {
						$insertactivity = [
							'request_id' => $response['reqid'],
							'request_type' => 'online',
							'PackageNumber' => $ActivityLog->PackageNumber,
							'item_id' => $key['_id'],
							'package_id' => $key['package_id'],
							'item_name' => $key['product_name'],
							'log_type' => 'request',
							'message' => 'Package has been created.',
							'status' => 'pending',
							'action_user_id' => Auth::user()->_id,
							'EnterOn' => new MongoDate(),
						];

						Activitylog::insert($insertactivity);
					}

					$insert['insert'] = $response['reqid'];
					if ($response['reqid']) {
						
						if(Input::get('listing') == 'own'){
							Additem::where(['request_type' => 'online', "user_type" => "admin",'user_id' => Auth::user()->_id])->delete();
						}else{
							Additem::where(['request_type' => 'online', "user_type" => "admin"])->delete();
						}
					}

				} else {
					$response = array("success" => 1, "msg" => "Success! Request has been updated successfully.");
					$update = Deliveryrequest::where(['_id' => Input::get('request_id')])->update($insert);
					$insert['insert'] = Input::get('request_id');
					$response['reqid'] = Input::get('request_id');
					PaymentInfo::insert([
						'request_id' => $response['reqid'],
						'user_id'=> (string) $insert['RequesterId'],
						'action_user_id'=> Auth::user()->_id,
						'RequestType'=> 'online',
						'action' => 'update_request',
						'item_id'=> '',
						'TotalCost'=> floatval($insert['TotalCost']),
						'shippingCost'=> floatval($insert['shippingCost']),
						'insurance'=> floatval($insert['insurance']),
						'AreaCharges'=> floatval($insert['AreaCharges']),
						'ProcessingFees'=> 0.0,
						'shipping_cost_by_user'=> 0.0,
						'item_cost'=> 0.0,
						'discount'=> floatval($insert['discount']),
						'PromoCode'=> $insert['PromoCode'],
						'difference'=> floatval($insert['after_update_difference']),
						'difference_before'=> floatval($insert['after_update_difference']),
						'date'=> New MongoDate(),
					]);
					//echo 'here------';
					//print_r(Input::get('request_id')); die;

				}
				$url = "promocode=" . Input::get('promo_code');
				if ($insert['TotalCost'] <= 0) {
					$response['redirect_to'] = url("admin/online_package/detail/{$insert['insert']}?$url");
				} else {
					$response['redirect_to'] = url("admin/online_package/payment/{$insert['insert']}?$url");
				}
			}
		}

		return response()->json($response);
	}

	public function onlinePayment($id) {
		/*sent mail*/
		$delivery = Deliveryrequest::where(array('_id' => $id))
			->select('RequesterId', 'RequesterName', 'ProductList', 'PackageId', 'PackageNumber', 'ProductTitle', 'shippingCost', 'TotalCost', 'insurance', 'PickupFullAddress', 'DeliveryFullAddress', 'RequestType')->first();
		if ($delivery) {
			$array = $delivery->ProductList;
			$product_name = '';

			foreach ($delivery->ProductList as $key => $value) {
				if (trim(Input::get('product_id')) == $value['_id']) {
					$array[$key]['inform_mail_sent'] = "yes";
					$product_name = $array[$key]['product_name'];
				}
			}
			$updateData = Deliveryrequest::where(array('_id' => $id))->update(['ProductList' => $array]);

			$users = User::where(['_id' => $delivery->RequesterId])->select('Name', 'Email','Default_Currency')->first();
			if (strtolower($delivery->RequestType) == 'online') {
				$sublink = 'online-payment';
			} elseif (strtolower($delivery->RequestType) == 'buy_for_me') {
				$sublink = 'buyforme-payment';
			}
			$link = url($sublink) . '/' . (string) $delivery->_id;

			$currency_conversion = $this->currency_conversion($delivery->TotalCost);

			if ($users->Default_Currency == 'CAD') {
				$user_currency = $currency_conversion['canadian_cost'];
			} else if ($users->Default_Currency == 'PHP') {
				$user_currency = $currency_conversion['philipins_cost'];
			} else if ($users->Default_Currency == 'GBP') {
				$user_currency = $currency_conversion['uk_cost'];
			} else {
				$user_currency = $currency_conversion['ghanian_cost'];
			}

			if ($users) {
				$Email = new NewEmail();
				/*$Email->send_mail('5a7d7d4ce4b06005c659a45e', [
					"to" => $users->Email,
					"replace" => [
						"[TITLE]"=> ucfirst($delivery->ProductTitle),
						"[USERNAME]" => $users->Name,
						"[PACKAGETITLE]" => ucfirst($product_name),
						"[PACKAGEID]" => $delivery->PackageNumber,
						"[TOTALCOST]" => $delivery->TotalCost,
						"[SHIPPINGCOST]" => $delivery->shippingCost,
						"[INSURANCECOST]" => $delivery->insurance,
						"[DROPOFFADDRESS]" => $delivery->DeliveryFullAddress,
						"[USERCURRENCY]" => $user_currency,
						"[LINK]" => "<a href='$link'>$link</a>",
					],
				]);*/
				$users = User::where(['_id' => $delivery->RequesterId])->update(['link' => $link, 'packagePayment' => 'yes']);
			}
		}
		/*sent mail*/
		$data['delivery'] = $delivery;
		return view('Admin::Buy.online-payment', $data);
	}
	public function postOnlinePayment(Request $request) {

		$validation = Validator::make($request->all(), [
			'transaction' => 'required',
			'description' => 'required',
		]);
		if ($validation->fails()) {
			return Redirect::to("admin/online_package/payment/" . Input::get('url_id'))->withErrors($validation)->withInput();
		} else {
			$delivery = Deliveryrequest::find(Input::get('url_id'));

			if (count($delivery) > 0) {

				$delivery->Status = 'ready';
				$oldEmailNotifiedStatus = $delivery->email_notified_to_client;
				$delivery->email_notified_to_client = true;
				$productList = $delivery->ProductList;
				foreach ($productList as $key => $val) {
					$val['status'] = 'ready';
					$productList[$key] = $val;
				}
				$delivery->ProductList = $productList;
				$delivery->save();
				$user_data = User::where(['_id' => $delivery->RequesterId])->first();

				$insert = [
					'SendById' => new MongoId($delivery->RequesterId),
					'SendByName' => $delivery->RequesterName,
					'TransactionType' => 'buy_for_me',
					'Description' => Input::get('description'),
					'Credit' => $delivery->TotalCost,
					'RecieveByName' => 'Aquantuo',
					'Debit' => '',
					'Status' => 'paid',
					'Transaction_id' => Input::get('transaction'),
					'EnterOn' => new MongoDate(),
				];

				$result = Transaction::insert($insert);

				if (count($user_data)) {
					$productList = $delivery->ProductList;
					foreach ($productList as $key => $val) {
						$insertactivity = [
							'request_id' => Input::get('url_id'),
							'request_type' => 'online',
							'PackageNumber' => $delivery->PackageNumber,
							'item_id' => $val['_id'],
							'package_id' => $val['package_id'],
							'item_name' => $val['product_name'],
							'log_type' => 'request',
							'message' => 'Payment successful.',
							'status' => 'ready',
							'action_user_id' => Auth::user()->_id,
							'EnterOn' => new MongoDate(),
						];

						Activitylog::insert($insertactivity);
					}

					if ($oldEmailNotifiedStatus != true) {
						$link = url("online-request-detail") . "/" . $delivery->_id;
						if ($user_data->EmailStatus == "on") {

							send_mail('587750587ac6f6e61c8b4567', [
								"to" => $user_data->Email,
								"replace" => [
									"[USERNAME]" => $user_data->Name,
									"[PACKAGETITLE]" => ucfirst($delivery->ProductTitle),
									"[DESTINATION]" => $delivery->DeliveryFullAddress,
									"[SHIPPINGCOST]" => $delivery->ShippingCost,
									"[TOTALCOST]" => $delivery->TotalCost,
									'[LINK]' => "<a href='$link'>$link</a>",
								],
							]);
						}
					}
				}

				return Redirect::to("admin/online_package/detail/" . $delivery->_id)->withSuccess('Transaction has been successfully.');
			}
		}
		return Redirect::to("admin/online-package")
			->with('fail', 'Something went wrong.');
	}

	public function buy_for_me_payment($id) {
		return view('Admin::Buy.buy_for_me_payment');
	}

	public function post_buy_for_me_payment(Request $request) {

		$validation = Validator::make($request->all(), [
			'transaction' => 'required',
			'description' => 'required',
		]);
		if ($validation->fails()) {
			return Redirect::to("admin/buy-for-me/payment/" . Input::get('url_id'))->withErrors($validation)->withInput();
		} else {

			$delivery = Deliveryrequest::where(['_id' => Input::get('url_id')])->first();

			if (count($delivery) > 0) {
				$delivery->Status = 'ready';
				$delivery->Save();
				$postArray = [];
				foreach ($delivery->ProductList as $key) {
					$key['status'] = 'ready';
					$postArray[] = $key;
				}

				Deliveryrequest::where(['_id' => Input::get('url_id')])
					->update([
						'ProductList' => $postArray,
					]);

				$insert = [
					'SendById' => new MongoId($delivery->RequesterId),
					'SendByName' => $delivery->RequesterName,
					'TransactionType' => 'online',
					'Description' => Input::get('description'),
					'Credit' => $delivery->TotalCost,
					'RecieveByName' => 'Aquantuo',
					'Debit' => '',
					'Status' => 'paid',
					'Transaction_id' => Input::get('transaction'),
					'EnterOn' => new MongoDate(),
				];

				$result = Transaction::insert($insert);
				return Redirect::to("admin/online_package/detail/" . $delivery->_id)->withSuccess('Transaction has been successfully.');
			} else {

				return Redirect::back()->withFail('danger', "Fail! Something is worng.");
			}
		}
	}

	public function mark_as_paid() {
		if (Input::get('requestid')) {
			$delivery = Deliveryrequest::where([
				'_id' => Input::get('requestid'),
				'RequestType' => Input::get('request_type'),
				'Status' => 'pending',
			])->first();

			if (count($delivery) > 0) {
				$delivery->Status = 'ready';
				$delivery->marked_description = Input::get('description');
				$oldEmailNotifiedStatus = $delivery->email_notified_to_client;
				$delivery->email_notified_to_client = true;
				if (Input::get('request_type') == 'online' || Input::get('request_type') == 'buy_for_me') {

					$postArray = [];
					foreach ($delivery->ProductList as $key) {
						$key['status'] = 'ready';
						$key['inform_mail_sent'] = 'no';
						$key['PaymentStatus'] = 'yes';

						$postArray[] = $key;
					}
					$delivery->ProductList = $postArray;
					$delivery->Save();
					$user_data = User::where('_id', $delivery->RequesterId)
						->select('Email', 'Name', 'EmailStatus')->first();

					
					if (isset($delivery->ProductList)) {
						foreach ($delivery->ProductList as $key) {
							Activitylog::insert(['request_id' => Input::get('requestid'),
								'request_type' => $delivery->RequestType,
								'PackageNumber' => $delivery->PackageNumber,
								'log_type' => 'request',
								'status' => $delivery->Status,
								'message' => 'Request has been marked as paid.',
								'package_id' => $key['package_id'],
								'item_name' => $key['product_name'],
								// 'action_user_id'=> Session::get('UserId'),
								'action_user_id'=> Auth::user()->_id,
								'EnterOn' => new MongoDate]
							);
						}
					}

					if (count($user_data)) {
						if ($oldEmailNotifiedStatus != true) {
							if (Input::get('request_type') == 'online') {
								$link = url("online-request-detail") . "/" . $delivery->_id;
								$emailTemplateID = '5b40b5f2360d5c1e0dbcbaaa';
							} else {
								$link = url("buy-for-me-detail") . "/" . $delivery->_id;
								$emailTemplateID = '5b40b5f2360d5c1e0dbcbaaa';
							}

							if ($user_data->EmailStatus == "on") {
								$Email = new NewEmail();
								$Email->send_mail($emailTemplateID, [
									"to" => $user_data->Email,
									//"to" => "kapil@idealittechno.com",
									"replace" => [
										"[USERNAME]" => $user_data->Name,
										"[PACKAGETITLE]" => ucfirst($delivery->ProductTitle),
										"[DESTINATION]" => $delivery->DeliveryFullAddress,
										"[SHIPPINGCOST]" => $delivery->shippingCost,
										"[TOTALCOST]" => $delivery->TotalCost,
										"[SOURCE]" => $delivery->PickupFullAddress,
										"[PACKAGENUMBER]" => $delivery->PackageNumber,
										'[LINK]' => "<a href='$link'>$link</a>",
									],
								]);
							}
						}
						return Redirect::back()->withSuccess('Request has been marked as paid  successfully.');
					}

				} elseif (Input::get('request_type') == 'delivery') {
					if(isset($delivery->ProductList)){
						$postArray = [];
						foreach ($delivery->ProductList as $key) {
							$key['status'] = 'ready';
							$key['inform_mail_sent'] = 'no';
							$key['PaymentStatus'] = 'yes';

							$postArray[] = $key;
						}
						$delivery->ProductList = $postArray;
					}

					$delivery->Save();
					if (isset($delivery->ProductList)) {
						foreach ($delivery->ProductList as $key) {
							Activitylog::insert(['request_id' => Input::get('requestid'),
								'request_type' => $delivery->RequestType,
								'PackageNumber' => $delivery->PackageNumber,
								'log_type' => 'request',
								'status' => $delivery->Status,
								'message' => 'Request has been marked as paid.',
								'package_id' => $key['package_id'],
								'item_name' => $key['product_name'],
								// 'action_user_id'=> Session::get('UserId'),
								'action_user_id'=> Auth::user()->_id,
								'EnterOn' => new MongoDate]
							);
						}
					}
					if ($oldEmailNotifiedStatus != true) {
						$users = User::find($delivery->RequesterId);

						if (count($users) > 0) {
							$link = url("delivery-details") . "/" . $delivery->_id;
							if ($users->EmailStatus == "on") {
								$Email = new NewEmail();
								$Email->send_mail('5b40b5f2360d5c1e0dbcbaaa', [
									"to" => $users->Email,
									"replace" => [
										"[USERNAME]" => $users->Name,
										"[PACKAGETITLE]" => ucfirst($delivery->ProductTitle),
										"[PACKAGEID]" => $delivery->PackageId,
										"[SOURCE]" => $delivery->PickupFullAddress,
										"[DESTINATION]" => $delivery->DeliveryFullAddress,
										"[PACKAGENUMBER]" => $delivery->PackageNumber,
										"[SHIPPINGCOST]" => $delivery->ShippingCost,
										"[TOTALCOST]" => $delivery->TotalCost,
										'[LINK]' => "<a href='$link'>$link</a>",
									],
								]);
							}

						}
					}

					return Redirect::back()->withSuccess('Request has been marked as paid  successfully.');
				} elseif (Input::get('request_type') == 'local_delivery'){
					$postArray = [];
					foreach ($delivery->ProductList as $key) {
						$key['status'] = 'ready';
						$key['inform_mail_sent'] = 'no';
						$key['PaymentStatus'] = 'yes';

						$postArray[] = $key;
					}
					$delivery->ProductList = $postArray;
					$delivery->Save();

					foreach ($delivery->ProductList as $key) {
						Activitylog::insert(['request_id' => Input::get('requestid'),
							'request_type' => $delivery->RequestType,
							'PackageNumber' => $delivery->PackageNumber,
							'log_type' => 'request',
							'status' => $delivery->Status,
							'message' => 'Request has been marked as paid.',
							'package_id' => $key['package_id'],
							'item_name' => $key['product_name'],
							'action_user_id'=> Auth::user()->_id,
							'EnterOn' => new MongoDate]
						);
					}

					if ($oldEmailNotifiedStatus != true) {
						$users = User::find($delivery->RequesterId);

						if (count($users) > 0) {
							$link = url("local-delivery-detail") . "/" . $delivery->_id;
							if ($users->EmailStatus == "on") {
								$Email = new NewEmail();
								$Email->send_mail('5b40b5f2360d5c1e0dbcbaaa', [
									"to" => $users->Email,
									"replace" => [
										"[USERNAME]" => $users->Name,
										"[PACKAGETITLE]" => ucfirst($delivery->ProductTitle),
										"[PACKAGEID]" => $delivery->PackageId,
										"[SOURCE]" => $delivery->PickupFullAddress,
										"[DESTINATION]" => $delivery->DeliveryFullAddress,
										"[PACKAGENUMBER]" => $delivery->PackageNumber,
										"[SHIPPINGCOST]" => $delivery->ShippingCost,
										"[TOTALCOST]" => $delivery->TotalCost,
										'[LINK]' => "<a href='$link'>$link</a>",
									],
								]);
							}

						}
					}
					return Redirect::back()->withSuccess('Request has been marked as paid  successfully.');
				}

			}
			return Redirect::back()->withFail('danger', "Fail! Something is worng.");
		}
		return Redirect::back()->withFail('danger', "Fail! Something is worng.");

	}

	public function mark_as_paid_after_item_update() {

		if (Input::get('requestid')) {
			$delivery = Deliveryrequest::where([
				'_id' => Input::get('requestid'),
				'RequestType' => Input::get('request_type'),
				'Status' => 'not_purchased',
			])->first();

			$user_data = User::where('_id', '=', $delivery->RequesterId)->select('_id', 'Name', 'Email')->first();

			if (count($delivery) > 0) {
				$ActivityLog = Deliveryrequest::where('_id', '=', Input::get('requestid'))->select('_id', 'RequestType', 'PackageNumber')->first();
				$delivery->Status = 'paid';
				$delivery->PaymentStatus = 'paid';
				$delivery->marked_description = Input::get('description');
				if (Input::get('request_type') == 'online' || Input::get('request_type') == 'buy_for_me' || Input::get('request_type') == 'delivery') {

					$postArray = [];
					foreach ($delivery->ProductList as $key) {
						$key['status'] = 'paid';
						$key['inform_mail_sent'] = 'no';
						if ($ActivityLog->RequestType == 'buy_for_me') {
							$insertactivity = [
								'request_id' => Input::get('requestid'),
								'request_type' => 'buy_for_me',
								'PackageNumber' => $ActivityLog->PackageNumber,
								'item_id' => $key['_id'],
								'package_id' => $key['package_id'],
								'item_name' => $key['product_name'],
								'log_type' => 'request',
								'message' => 'Request has been marked as paid.',
								'status' => 'paid',
								'action_user_id' => Auth::user()->_id,
								'EnterOn' => new MongoDate(),
							];
							Activitylog::insert($insertactivity);
						}
						if ($ActivityLog->RequestType == 'online') {
							$insertactivity = [
								'request_id' => Input::get('requestid'),
								'request_type' => 'online',
								'PackageNumber' => $ActivityLog->PackageNumber,
								'item_id' => $key['_id'],
								'package_id' => $key['package_id'],
								'item_name' => $key['product_name'],
								'log_type' => 'request',
								'message' => 'Request has been marked as paid.',
								'status' => 'paid',
								'action_user_id' => Auth::user()->_id,
								'EnterOn' => new MongoDate(),
							];
							Activitylog::insert($insertactivity);
						}

						$postArray[] = $key;
					}

					//Mail to user as mark as paid
					send_mail('5ab33bc7e4b0d652a3100b0d', [
						"to" => $user_data->Email,
						"replace" => [
							"[USERNAME]" => $user_data->Name,
							"[PACKAGETITLE]" => $delivery->ProductTitle,
						],
					]
					);

					$delivery->ProductList = $postArray;
					$delivery->Save();
					return Redirect::back()->withSuccess('Request has been marked as paid  successfully.');
				}
			}
			return Redirect::back()->withFail('danger', "Fail! Something is worng.");
		}
		return Redirect::back()->withFail('danger', "Fail! Something is worng.");

	}

	public function edit_online_package($id) {
		$data['request'] = Deliveryrequest::where(['_id' => $id, 'RequestType' => 'online'])->first();
		if (count($data['request']) > 0) {

			session()->set($id, $data['request']['ProductList']);
			$data['product_list'] = $data['request']['ProductList'];
			//  echo "<pre>"; print_r($data['product_list']);  die;
			$data['country'] = CityStateCountry::where(['type' => 'Country', 'Status' => 'Active'])
				->get(['_id', 'Content', 'state_available']);

			$data['state'] = CityStateCountry::where(['type' => 'State', 'Status' => 'Active','SuperName'=> $data['request']->DeliveryCountry])
				->get(['_id', 'Content', 'state_available']);

			

			$data['city'] = CityStateCountry::where(['type' => 'city', 'Status' => 'Active','SuperName'=> $data['request']->DeliveryState])
				->get(['_id', 'Content', 'state_available']);

			$data['user'] = User::select('_id', 'Name', 'Email')->orderBy('Name', 'asc')->get();
			$data['category'] = Category::where(['type' => 'Category', 'Status' => 'Active'])->whereIn('TravelMode', ['ship', 'air'])->orderBy('Content', 'asc')->get(['TravelMode', 'Content']);
			$field = ['country', 'state', 'city', 'address_line_1', 'address_line_2', 'zipcode', 'lat', 'lng'];
			$data['address'] = Address::where('user_id', '=', Auth::user()->_id)->get($field);

			$data['additem'] = Additem::where('user_id', '=', Auth::user()->_id)->where(array('request_type' => 'online'))->limit(5)->get();
			
			return view('Admin::Buy.edit_online_purchase', $data);
		} else {
			return Redirect::to("admin/online-package")->with('danger', "Fail! Something is worng");
		}
	}

	public function user_list() {
		$search = Input::get('value');

		$data['user'] = User::whereIn("UserType", ['requester', 'both'])
			->where("Name", "like", "%$search%")->select('_id', 'Name', 'Email')->orderBy('Name', 'asc')->get();
		return view('Admin::user_list', $data);
	}

	public function send_upddate_mail() {
		$response = ['success' => 0, 'msg' => "Oops! Something went wrong."];
		if (Input::get('request_id') != '' && Input::get('product_id') != '') {

			$delivery = Deliveryrequest::where(array('_id' => Input::get('request_id')))
				->where(array("ProductList" => array('$elemMatch' => array('_id' => Input::get('product_id')))))
				->select('RequesterId', 'ProductList', 'PackageId', 'PackageNumber', 'ProductTitle', 'shippingCost', 'TotalCost', 'insurance', 'PickupFullAddress', 'DeliveryFullAddress', 'RequestType','request_version','shippingCost')->first();
			if ($delivery) {
				$array = $delivery->ProductList;
				$product_name = '';
				$package_id = '';

				foreach ($delivery->ProductList as $key => $value) {
					if (trim(Input::get('product_id')) == $value['_id']) {
						$array[$key]['inform_mail_sent'] = "yes";
						$product_name = (isset($array[$key]['product_name'])) ? $array[$key]['product_name'] : @$array[$key]['item_name'];
						$package_id = $array[$key]['package_id'];
					}
				}
				$updateData = Deliveryrequest::where(array('_id' => Input::get('request_id')))->update(['ProductList' => $array]);

				$users = User::where(['_id' => $delivery->RequesterId])->select('Name', 'Email','Default_Currency')->first();

				$emailID = "5a7d7d4ce4b06005c659a45e";
				if (strtolower($delivery->RequestType) == 'online') {
					//$sublink = 'online-payment';
					$sublink = 'online-request-detail';
				} elseif (strtolower($delivery->RequestType) == 'buy_for_me') {
					//$sublink = 'buyforme-payment';
					$sublink = 'buy-for-me-detail';
				} elseif (strtolower($delivery->RequestType) == 'delivery') {
					$emailID = "5f6345ab56783ddf19eaa1f5";
					if(isset($delivery->request_version) && $delivery->request_version == 'new'){
						$sublink = 'send-package-detail';
					}else{
						$sublink = 'delivery-details';
					}
				}elseif (strtolower($delivery->RequestType) == 'local_delivery') { 
					$sublink = 'local-delivery-detail';
				}

				$link = url($sublink) . '/' . (string) $delivery->_id;

				$currency_conversion = $this->currency_conversion($delivery->TotalCost);

				if ($users->Default_Currency == 'CAD') {
					$user_currency = $currency_conversion['canadian_cost'];
				} else if ($users->Default_Currency == 'PHP') {
					$user_currency = $currency_conversion['philipins_cost'];
				} else if ($users->Default_Currency == 'GBP') {
					$user_currency = $currency_conversion['uk_cost'];
				} else if ($users->Default_Currency == 'KES') {
					$user_currency = $currency_conversion['kenya_cost'];
				} else {
					$user_currency = $currency_conversion['ghanian_cost'];
				}

				$insertactivity = [
					'request_id' => Input::get('request_id'),
					'request_type' => strtolower($delivery->RequestType),
					'PackageNumber' => $delivery->PackageNumber,
					'item_id' => trim(Input::get('product_id')),
					'package_id' => $package_id,
					'item_name' => $product_name,
					'log_type' => 'request',
					'message' => 'Requester has been informed for payment.',
					'status' => 'pending',
					'action_user_id'=> Auth::user()->_id,
					'EnterOn' => new MongoDate(),

				];
				
				Activitylog::insert($insertactivity);


				if ($users) {
					$Email = new NewEmail();
					//echo $users->Email;
					$Email->send_mail($emailID, [
						"to" => $users->Email,
						"replace" => [
							"[TITLE]" => ucfirst($product_name),
							"[USERNAME]" => $users->Name,
							"[PACKAGETITLE]" => ucfirst($product_name),
							"[PACKAGEID]" => $package_id,
							"[TOTALCOST]" => number_format($delivery->TotalCost, 2),
							"[USERCURRENCY]" => $user_currency,
							"[SHIPPINGCOST]" => $delivery->shippingCost,
							"[INSURANCECOST]" => $delivery->insurance,
							"[DROPOFFADDRESS]" => $delivery->DeliveryFullAddress,
							"[PICKUPOFFADDRESS]" => $delivery->PickupFullAddress,
							"[LINK]" => "<a href='$link'>$link</a>",
						],
					]);
					$users = User::where(['_id' => $delivery->RequesterId])->update(['link' => $link, 'packagePayment' => 'yes']);
					$response = ['success' => 1, 'msg' => "Mail has been sent successfully."];
				}
			}
		}
		return response()->json($response);
	}

	public function get_item_info() {
		$response = ['success' => 0, 'msg' => "Oops! Something went wrong."];

		if (Input::get('requestid') != '' && Input::get('id') != '') {
			$where = ["ProductList" => ['$elemMatch' => ['_id' => Input::get('id')]]];
			$delinfo = Deliveryrequest::where($where)->where(['_id' => Input::get('requestid')])
				->select('ProductList')
				->first();

			if (count($delinfo) > 0) {
				$array = [];
				foreach ($delinfo->ProductList as $key => $value) {
					if ($value['_id'] == Input::get('id')) {
						$array = $value;
					}
				}

				$response['success'] = 1;
				$response['msg'] = "Product data";
				$response['result'] = $array;
			}

		}
		return response()->json($response);
	}

	public function resionCharges($state) {

		$charges = Extraregion::where(['state_id' => $state['id'], 'status' => 'Active'])->select('amount')->first();
		if ($charges) {
			return $charges->amount;
		} else {
			return 0.00;
		}
	}

	public function get_item_info1($id){
		$response = array("success" => 0, "msg" => "Oops! Something went wrong.");
		if($id){
			$data = Additem::where(['_id'=>$id])->first();
			if(count($data) > 0){
				$response = array("success" => 1, "msg" => "result.",'result' => $data);
			}
		}
		return response()->json($response);
	}

	public function get_item_info2($rid,$id){
		$response = array("success" => 0, "msg" => "Oops! Something went wrong.");
		if($id != '' && $rid != ''){

			$request_info = Deliveryrequest::where(['_id'=>$rid])->select('ProductList')->first();
			if($request_info){
				$product_list = session::get(trim($rid));
				$data = [];
				foreach ($product_list as $key => $value) {
					if($value['_id'] == $id){
						$data = $value;
					}
				}
				if(empty($data)){
					$data = Additem::where(['_id'=>$id])->first();
				}

				if(count($data) > 0){

					$response = array("success" => 1, "msg" => "result.",'result' => $data);
				}
			}
			
		}
		return response()->json($response);
	}

	public function get_item_info3() {
		$response = ['success' => 0, 'msg' => "Oops! Something went wrong."];

		if (Input::get('id') != '') {
			$where = ['_id' => Input::get('id')];
			$delinfo = Additem::where($where)->first();

			if (count($delinfo) > 0) {
				$response['success'] = 1;
				$response['msg'] = "Product data";
				$response['result'] = $delinfo;
			}

		}
		return response()->json($response);
	}

	/*
	 * TODO return Volumetric Weight in lbs
	 * 1 lbs = 2.20462 kg
	 * 1lbs = 0.00220462 gram
	 *
	 */
	public function GetVolumetricWeightInLbs($length,$height,$width,$weight,$type) {
		$VolumetricInfo = ["length"=>$length,"height"=>$height,"width"=>$width,"type"=>$type];
        $VolumetricWeight =  Utility::calculate_volumetric_weight($VolumetricInfo);
       
		if($weight>$VolumetricWeight){
			$VolumetricWeight  = $weight;
		}
        $type = strtolower($type);
		if ($type == 'kg') {
			$VolumetricWeightLbs = (((float) $VolumetricWeight) * 2.20462);
		} else if ($type == 'gram') {
			$VolumetricWeightLbs = (((float) $VolumetricWeight) * 0.00220462);
		} else {
			$VolumetricWeightLbs = $VolumetricWeight;
		}
		return $VolumetricWeightLbs;
	}

}
