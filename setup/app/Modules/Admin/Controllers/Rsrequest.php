<?php namespace App\Modules\Admin\Controllers;

/*
 * Project: Aquantuo
 *
 */

use App\Http\Controllers\Controller;
use App\Http\Models\Category;
use App\Http\Models\CityStateCountry;
use App\Http\Models\Deliveryrequest;
use App\Http\Models\User;
use App\Library\Reqhelper;
use Input;

class Rsrequest extends Controller
{
    public function edit_package($id)
    {

        $data['delivery_data'] = Deliveryrequest::where(array('_id' => $id))
            ->whereIn('Status', ['ready', 'pending'])->first();

        if (!count($data['delivery_data']) > 0) {
            return redirect('delivery-details/' . $id);
        }
        $data['user'] = User::where('_id', $data['delivery_data']->RequesterId)
            ->select(['Email'])->first();

        $data['country'] = CityStateCountry::where(['type' => 'Country', 'Status' => 'Active'])
            ->get(['_id', 'Content', 'state_available']);
        $data['category'] = Category::where(['type' => 'Category', 'Status' => 'Active'])->whereIn('TravelMode', ['ship', 'air'])->get(['TravelMode', 'Content']);
        return view('Admin::edit.prepare_request', $data);
    }

    public function add_package()
    {

        $data['country'] = CityStateCountry::where(['type' => 'Country', 'Status' => 'Active'])
            ->get(['_id', 'Content', 'state_available']);
        $data['category'] = Category::where(['type' => 'Category', 'Status' => 'Active'])->whereIn('TravelMode', ['ship', 'air'])->get(['TravelMode', 'Content']);
        return view('Admin::add.prepare_request', $data);
    }

    public function prepare_request()
    {
        $response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
        $users = User::find(Input::get('user_id'));
        if (count($users) > 0) {

            $req = new Reqhelper();
            $res = $req->request_create($users);
            if ($res['success'] == 1) {
                $response['success'] = 1;
                $response['type'] = $res['type'];
                $request = Deliveryrequest::where(['_id' => $res['reqid']])->first();
                if (count($request) > 0) {
                    $request->Status = 'pending';
                    //$request->PerPage = 20;
                    $request->save();
                }
                if ($res['type'] == 'update') {
                    $response['msg'] = "Your request has been updated, Please proceed to payment method.";

                } else {
                    $response['msg'] = "Your request has been created, Please proceed to payment method.";
                }
                $response['reqid'] = $res['reqid'];
            } else {
                $response['msg'] = $res['msg'];
            }
        }

        return json_encode($response);
    }
}
