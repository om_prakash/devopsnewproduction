<?php
namespace App\Modules\Admin\Controllers;

/*
 * Project: Aquantuo
 */

use App\Http\Controllers\Controller;
use App\Http\Models\Activitylog;
use App\Http\Models\Additem;
use App\Http\Models\Category;
use App\Http\Models\CityStateCountry;
use App\Http\Models\Configuration;
use App\Http\Models\Currency;
use App\Http\Models\Deliveryrequest;
use App\Http\Models\Distance;
use App\Http\Models\Extraregion;
use App\Http\Models\Notification;
use App\Http\Models\Setting;
use App\Http\Models\User;
use App\Library\Notify;
use App\Library\Promo;
use App\Library\Reqhelper;
use App\Library\Notification\Pushnotification;
use App\Library\Utility;
use App\Modules\Admin\Models\Admin;
use App\Traits\AdminTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Input;
use MongoDate;
use MongoId;
use Session;
use Auth;
use App\Library\WebActivityLog;
use App\Library\NewEmail;
class LocalDeliveryController extends Controller {
	use AdminTrait;

	// calculation part buy for me
	// Start buy for me section
	public function add_local_delivery() {

		$data['transporter_data'] = array();
		$data['RequestType'] = Input::get("local_delivery");

		$data['country'] = CityStateCountry::where(['type' => 'Country', 'Status' => 'Active', 'Content' => 'Ghana'])
			->orderBy('Content', 'Asc')
			->get(['_id', 'Content', 'state_available']);

		$data['state'] = CityStateCountry::where(['type' => 'State', 'Status' => 'Active', 'CountryId' => $data['country'][0]['_id']])
			->orderBy('Content', 'Asc')
			->get(['_id', 'Content', 'CountryId']);

		$data['category'] = Category::where(['type' => 'localCategory', 'Status' => 'Active', "country_id" => $data['country'][0]['_id']])->get();

		$data['user'] = User::where('delete_status', '=', 'no')->select('_id', 'Name', 'Email')->orderBy('Name', 'asc')->get();

		$data['items'] = Additem::where(['user_id' => Session::get('UserId'), 'type' => 'local_delivery'])->get();
		$data['items_count'] = count($data['items']);
		
		if (count($data['items']) > 0) {
			return view('Admin::localdelivery.prepare-request', $data);
		} else {
			//return view('Admin::sendapackage.prepare_add_item', $data);
			return view('Admin::localdelivery.prepareadditem', $data);
		}
	}

	public function localprepareAddItem($id = "") {

		// $data['transporter_data'] = array();
		// $data['country'] = CityStateCountry::where(['type' => 'Country', 'Status' => 'Active'])
		//     ->orderBy('Content', 'Asc')
		//     ->get(['_id', 'Content', 'state_available']);

		// $data['category'] = Category::where(['type' => 'Category', 'Status' => 'Active'])->whereIn('TravelMode', ['ship', 'air'])->get(['TravelMode', 'Content']);

		// $data['items_count'] = Additem::where(['user_id' => Session::get('UserId'), 'type' =>
		//     'local_delivery'])->count();

		$data['transporter_data'] = array();
		$data['RequestType'] = Input::get("local_delivery");

		$data['country'] = CityStateCountry::where(['type' => 'Country', 'Status' => 'Active'])
			->orderBy('Content', 'Asc')
			->get(['_id', 'Content', 'state_available']);

		$data['state'] = CityStateCountry::where(['type' => 'State', 'Status' => 'Active', 'CountryId' => '56bdbfb4cf32079714ee89ca'])
			->orderBy('Content', 'Asc')
			->get(['_id', 'Content', 'CountryId']);

		$data['category'] = Category::where(['type' => 'localCategory', 'Status' => 'Active', "country_id" => "56bdbfb4cf32079714ee89ca"])->get();

		$data['items'] = Additem::where(['user_id' => Session::get('UserId'), 'type' => 'local_delivery'])->get();
		$data['items_count'] = count($data['items']);

		if ($id != '') {
			return view('Admin::localdelivery.edit.add_item', $data);
		} else {
			return view('Admin::localdelivery.prepareadditem', $data);
		}
	}

	public function editLocalDelivery($id, $requestid = "") {
		$data['transporter_data'] = array();
		$data['country'] = CityStateCountry::where(['type' => 'Country', 'Status' => 'Active', 'Content' => 'Ghana'])
			->orderBy('Content', 'Asc')
			->get(['_id', 'Content', 'state_available']);

		$data['category'] = Category::where(['type' => 'localCategory', 'Status' => 'Active', "country_id" => "56bdbfb4cf32079714ee89ca"])->get();

		if ($requestid != '') {
			$data['delivery_data'] = Deliveryrequest::where(['_id' => $requestid])
				->select('ProductList')->first();
		} else {
			$data['delivery_data'] = Additem::where(['user_id' => Session::get('UserId'), 'type' => 'local_delivery', '_id' => $id])->first();
		}
		$data['items_count'] = 1;
		if ($data['delivery_data']) {
			if ($requestid != '') {
				$array = [];
				foreach ($data['delivery_data']->ProductList as $key) {
					if ($key['_id'] == $id) {
						$array = [
							"_id" => $key['_id'],
							"product_name" => @$key['product_name'],
							"productWidth" => $key['productWidth'],
							"productHeight" => $key['productHeight'],
							"productLength" => $key['productLength'],
							"productCost" => $key['productCost'],
							"productWeight" => $key['productWeight'],
							"productHeightUnit" => $key['productHeightUnit'],
							"ProductWeightUnit" => $key['ProductWeightUnit'],
							"ProductLengthUnit" => $key['ProductLengthUnit'],
							"productCategory" => $key['productCategory'],
							"productCategoryId" => $key['productCategoryId'],
							"travelMode" => $key['travelMode'],
							"InsuranceStatus" => $key['InsuranceStatus'],
							"productQty" => $key['productQty'],
							"ProductImage" => $key['ProductImage'],
							"QuantityStatus" => $key['QuantityStatus'],
							//"BoxQuantity" => $key['BoxQuantity'],
							"InsuranceCost" => @$key['InsuranceCost'],
							"Description" => $key['Description'],
							"PackageMaterial" => $key['PackageMaterial'],
							"PackageMaterialShipped" => @$key['PackageMaterialShipped'],
							"inform_mail_sent" => "no",
						];
					}
				}
				$data['delivery_data'] = $array;
				return view('Admin::localdelivery.edit.edit_item', $data);
			} else {
				return view('Admin::localdelivery.edit_item', $data);
			}

		} else {
			return Redirect::back();
		}
	}
     
  

	public function post_local_delivery() {
		$response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		$ProductImage = '';
		$otherImages = [];
		if (Input::get('request_id') != '') {
			$item = Additem::where(['_id' => Input::get('request_id')])->first();
			$ProductImage = $item->ProductImage;
			$ProductImage = $item->ProductImage;
			$otherImages = $item->OtherImage;
		}
		if (Input::hasFile('default_image')) {
			if (Input::file('default_image')->isValid()) {
				$ext = Input::file('default_image')->getClientOriginalExtension();
				$ProductImage = time() . rand(100, 9999) . ".$ext";

				if (Input::file('default_image')->move(BASEURL_FILE . 'package/', $ProductImage)) {
					$ProductImage = "package/$ProductImage";
				} else {
					$ProductImage = '';
				}
			}
		}
		$imageArray = array('package_image2', 'package_image3', 'package_image4', 'package_image5');
		foreach ($imageArray as $key => $val) {
			if (Input::hasFile($val)) {
				if (Input::file($val)->isValid()) {
					$ext = Input::file($val)->getClientOriginalExtension();
					$img = time() . rand(100, 9999) . ".$ext";

					if (Input::file($val)->move(BASEURL_FILE . 'package/', $img)) {
						$img = "package/$img";
						if (isset($otherImages[$key])) {
							// Start remove old image
							if (!empty(trim($otherImages[$key]))) {
								if (file_exists(BASEURL_FILE . $otherImages[$key])) {
									unlink(BASEURL_FILE . $otherImages[$key]);
								}
							}
							// End remove old image
							$otherImages[$key] = $img;
						} else {
							$otherImages[] = $img;
						}
					}
				}
			}
		}

		$lhwunit = 'cm';
		$weightunit = 'kg';
		if (Input::get('measurement_unit') == 'inches_lbs') {
			$lhwunit = 'inches';
			$weightunit = 'lbs';
		}
		$category = json_decode(Input::get('category'));

		$add_item = [
			'product_name' => ucfirst(Input::get('package_title')),
			'user_id' => Session::get('UserId'),
			'type' => 'local_delivery',
			'productWidth' => Input::get('width'),
			'productHeight' => Input::get('height'),
			'productLength' => Input::get('length'),
			'productCost' => (Input::get('package_value') == '')?0:Input::get('package_value') ,
			'productWeight' => Input::get('weight'),
			'productHeightUnit' => $lhwunit,
			'ProductWeightUnit' => $weightunit,
			'ProductLengthUnit' => $lhwunit,
			'productCategory' => @$category->name,
			'productCategoryId' => @$category->id,
			'travelMode' => Input::get('travel_mode'),
			'currency' => Input::get('Default_Currency'),
			'needInsurance' => Input::get('insurance'),
			"productQty" => (Input::get('quantity') < 1) ? 1 : Input::get('quantity'),
			'ProductImage' => $ProductImage,
			'ProductTitle' => ucfirst(Input::get('title')),
			"QuantityStatus" => (Input::get('quantity') > 1) ? 'multiple' : 'single',
			"BoxQuantity" => (Input::get('quantity') == 0) ? 1 : (int) Input::get('quantity'),
			"Description" => Input::get('description'),
			"InsuranceStatus" => (Input::get('insurance') == 'yes') ? 'yes' : 'no',
			"PackageMaterial" => (Input::get('need_package_material') == 'yes') ? 'yes' : 'no',
			"PackageMaterialShipped" => '',
			"inform_mail_sent" => 'no',
			"InsuranceCost" => 0,
		];

		if ($add_item['needInsurance'] == 'yes') {
			$insurance = $this->get_insurance($add_item['productCost'], $add_item['productQty']);
			if ($insurance == 0) {
				$response['msg'] = 'Sorry! We are not able to provide insurence.';
				return json_encode($response);
			}
		}
		if (Input::get('request_id') != '') {
			// print_r(Input::get('request_id')); die;
			if (Additem::where(['_id' => Input::get('request_id')])->update($add_item)) {
				$response = ['success' => 1, 'msg' => 'Item has been updated successfully.'];
			}
		} else {
			if (Additem::insert($add_item)) {
				$response = ['success' => 1, 'msg' => 'Item has been added successfully.'];
			}
		}
		return json_encode($response);
	}

	public function local_prepare_calculation() {

		$response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		$response = ['success' => 0, 'shipping_cost' => '', 'shipping_cost' => '', 'ghana_total_amount' => '', 'formated_currency' => '', 'total_weight' => 0, 'insurance_cost' => '', 'reasion_charge' => '', 'item_price'];
		if (Input::get('request_id') == '') {
			$items = Additem::where(['user_id' => Session::get('UserId'), 'type' => 'local_delivery'])->get();
		} else {
			$items = Deliveryrequest::where(['_id' => Input::get('request_id')])->select('ProductList')->first();
			$items = $items->ProductList;
		}

		$data['item_price'] = 0;
		$data['category_price'] = 0;
		$data['distance_price'] = 0;
		$data['region_price'] = 0;
		$data['insurance_price'] = 0;
		$data['shipping_cost'] = 0;
		$data['total_weight'] = 0;
		$data['volume'] = 0;
		$data['distance'] = 0;

		$return_array = $items;

		if (count($items) == 0) {
			return json_encode($response);
		}
		$rate = $insurance = $weight = $volume = 0;

		$distance = Utility::getDistanceBetweenPointsNew(Input::get('PickupLat'), Input::get('PickupLong'), Input::get('DeliveryLat'), Input::get('DeliveryLat'));

		if ($distance == false) {
			$response['msg'] = 'Oops! We are unable to calculate distance.';
			return json_encode($response);die;
		}

		$distance = $distance * 0.000621371;
		$country2 = json_decode(Input::get('drop_off_country'));
		$state2 = json_decode(Input::get('drop_off_state'));

		$match_distance = Distance::Where('from', '<=', (float) $distance)->where('to', '>=', (float) $distance)
			->where(['Status' => 'Active', 'country_id' => $country2->id, "type" => "distance"])
			->select('price')
			->first();
		if (count($match_distance) == 0) {
			$response = ['success' => 0, 'msg' => 'The shipping address/distance you have specified is unsupported.'];
			return response()->json($response);
		}

		$match_value = Distance::where(['Status' => 'Active', 'country_id' => $country2->id, "type" => "item-value"])
			->select('price', 'from', 'to')->get();
		if (count($match_distance) == 0) {
			return response()->json($response);
		}

		if ($items) {
			foreach ($items as $key => $value) {
				# code...
				$value['shippingCost'] = 0;
				//Weight
				$weight += $this->get_weight($value['productWeight'], $value['ProductWeightUnit']);
				//Volume
				$volume += $this->get_volume(array(
					"width" => $value['productWidth'],
					"widthunit" => $value['productHeightUnit'],
					"height" => $value['productHeight'],
					"heightunit" => $value['productHeightUnit'],
					"length" => $value['productLength'],
					"lengthunit" => $value['productHeightUnit'],
				));
				//Insurance
				if ($value['InsuranceStatus'] == 'yes') {
					$value['InsuranceCost'] = $this->get_insurance($value['productCost'], $value['productQty']);
					$data['insurance_price'] += $value['InsuranceCost'];
				}
				//Price Item category
				$match_category = Category::where(['_id' => $value['productCategoryId'], 'country_id' => $country2->id, 'Status' => 'Active', 'type' => 'localCategory'])->select('price')
					->first();

				if ($match_category) {
					$data['category_price'] += $match_category->price;
					//$return_array[$key]['shippingCost'] = $match_category->price;
					$value['shippingCost'] = $match_category->price;
				} else {
					$response = ['success' => 0, 'msg' => "The shipping category you have specified is unsupported for product '" . $value['product_name'] . '.'];
					return response()->json($response);
				}

				foreach ($match_value as $key) {
					//print_r($key->to); die;
					$flag1 = false;
					$flag2 = false;
					if ($key->from <= $value['productCost']) {
						$flag1 = true;
					}

					if ($key->to >= $value['productCost']) {
						$flag2 = true;
					}

					if ($flag1 == true && $flag2 == true) {
						$data['item_price'] += $key->price;
						$value['shippingCost'] += $key->price;
						//$return_array[$key]['shippingCost'] += $match_category->price;
					} else {
						$response = ['success' => 0, 'msg' => "The item value you have specified is unsupported for product '" . $value['product_name'] . '.'];
						return response()->json($response);
					}
				}

				$data['distance_price'] += $match_distance->price;
				$value['shippingCost'] += $match_distance->price;

			}
		}

		//extraa charge of out side accra
		$data['region_price'] = $this->resionCharges(['name' => $state2->name, 'id' => $state2->id]);
		//end accra charge
		$data['shipping_cost'] = $data['category_price'] + $data['item_price'] + $data['distance_price'];
		$data['total_amount'] = $data['shipping_cost'] + $data['region_price'] + $data['insurance_price'];
		// print_r($data['category_price']); die;
		$data['volume'] = $volume;
		$data['total_weight'] = $weight;
		$data['distance'] = $distance;
		$data['product'] = $items;
		$data['item_count'] = count($items);
		$currency_conversion = $this->currency_conversion($data['total_amount']);

		if (Session::get('Default_Currency') == 'GHS') {
			$data['user_currency'] = $currency_conversion['ghanian_cost'];
		} else if (Session()->get('Default_Currency') == 'CAD') {
			$data['user_currency'] = $currency_conversion['canadian_cost'];
		} else if (Session()->get('Default_Currency') == 'PHP') {
			$data['user_currency'] = $currency_conversion['philipins_cost'];
		} else if (Session()->get('Default_Currency') == 'GBP') {
			$data['user_currency'] = $currency_conversion['uk_cost'];
		} else {
			$data['user_currency'] = $currency_conversion['ghanian_cost'];
		}

		//$data['product_list'] = $items;

		$response = ['success' => 1, 'msg' => 'Calculation', 'result' => $data, 'product' => $return_array];
		//print_r($data);die;
		//return response()->json($response);
		return json_encode($response);

	}

	public function get_distance($distance) {
		return floatval($distance) * 0.000621371;
	}

	public function get_weight($weight, $type) {
		$type = strtolower($type);
		if ($type == 'kg') {
			return (((float) $weight) * 2.20462);
		} else if ($type == 'gram') {
			return (((float) $weight) * 0.00220462);
		} else {
			return $weight;
		}
	}

	public function get_insurance($cost, $quantity) {
		$data = Configuration::where(array('_id' => '5673e33e6734c4f874685c84'))
			->select('Insurance')->first();

		$value = 0;

		if (count($data) > 0) {
			if (is_array($data->Insurance)) {
				foreach ($data->Insurance as $key) {
					if ($key['MinPrice'] <= (float) $cost && $key['MaxPrice'] >= (float) $cost) {
						$value = $key['Rate'] * (int) $quantity;
					}
				}
			}
		}

		return $value;
	}
	public function local_delivery_detail($id) {
		$data['request_data'] = Deliveryrequest::where(['_id' => $id])->first();
		if (count($data['request_data']) > 0) {
			$data['total_item'] = 0;
			if (isset($data['request_data']['ProductList'])) {
				foreach ($data['request_data']['ProductList'] as $key) {
					$data['total_item'] = $data['total_item'] + 1;
				}
			}
		}
		return view('User::send_package.requester_package_detail', $data);
	}

	public function get_volume($array) {
		return ($this->get_size_in_cm($array['length'], $array['lengthunit']) * $this->get_size_in_cm($array['width'], $array['widthunit']) * $this->get_size_in_cm($array['height'], $array['heightunit']));
	}

	public function get_size_in_cm($height, $unit) {

		$in_cm = (float) $height;
		$unit = strtolower($unit);
		if ($unit == 'inches') {
			$in_cm = $in_cm * 2.54;
		}
		return $in_cm;

	}

	public function outsideOfAccraCharge($address) {
		$distance = Utility::getDistanceBetweenPointsNew(5.5913754, -0.2499413, $address['lat'], $address['lng']);
		$charge = false;
		if ($distance > 0) {
			$distance_km = $distance / 1000;
			$range = 30;
			if ($distance_km > 30) {
				$charge = true;
			}
		}
		return $charge;
	}

	public function currency_conversion($shipcost) {

		$data = ['cost' => 0, 'ghanian_cost' => 0, 'philipins_cost' => 0, 'uk_cost' => 0, 'canadian_cost' => 0];
		$currency = Currency::where(['Status' => 'Active'])->select('CurrencyRate', 'FormatedText', 'CurrencyCode')->get();

		if ($currency) {
			foreach ($currency as $key) {
				if ($key->CurrencyCode == 'GHS') {
					$data['ghanian_cost'] = str_replace('[AMT]', number_format($shipcost * $key->CurrencyRate, 2), $key->FormatedText);
				} else if ($key->CurrencyCode == 'CAD') {
					$data['canadian_cost'] = str_replace('[AMT]', number_format($shipcost * $key->CurrencyRate, 2), $key->FormatedText);
				} else if ($key->CurrencyCode == 'PHP') {
					$data['philipins_cost'] = str_replace('[AMT]', number_format($shipcost * $key->CurrencyRate, 2), $key->FormatedText);
				} else if ($key->CurrencyCode == 'GBP') {
					$data['uk_cost'] = str_replace('[AMT]', number_format($shipcost * $key->CurrencyRate, 2), $key->FormatedText);
				}
			}
		}
		return $data;

	}

	public function local_add_item_page($id = "") {

		$data['transporter_data'] = array();
		$data['country'] = CityStateCountry::where(['type' => 'Country', 'Status' => 'Active'])
			->orderBy('Content', 'Asc')
			->get(['_id', 'Content', 'state_available']);
		$data['category'] = Category::where(['type' => 'Category', 'Status' => 'Active'])->whereIn('TravelMode', ['ship', 'air'])->get(['TravelMode', 'Content']);
		$data['items_count'] = Additem::where(['user_id' => Session::get('UserId'), 'type' => 'delivery'])->count();
		if ($id != '') {
			return view('User::send_package.edit.add_item', $data);
		} else {
			return view('User::send_package.prepare_add_item', $data);
		}

	}

	private function get_packageno() {
		return Utility::sequence('Request');
	}

	public function local_create_prepare_request() {
		$response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		$data['items'] = Additem::where(['user_id' => Session::get('UserId'), 'type' => 'local_delivery'])->get();

		if (Input::get('user') != '') {
			$json_decode_user = json_decode(Input::get('user'));
			$requesterid = $json_decode_user->id;
			$requestername = $json_decode_user->name;
			$user = $requester = User::where(['_id' => $requesterid])->first();
		} else {
			$email_check = User::where(['Email' => strtolower(trim(Input::get('email')))])->first();
			if (count($email_check) > 0) {
				$response = [
					'success' => 0,
					'msg' => 'This email is already registered with us.',
				];
				return json_encode($response);die;
			} else {
				$pass = rand(100000, 999999);
				$UniqueNo = Input::get('first_name')[0] . Input::get('lastName')[0];
				$insert['First'] = Input::get('first_name');
				$insert['Last'] = Input::get('last_name');
				$insert['Name'] = ucfirst(Input::get('first_name')) . " " . ucfirst(Input::get('last_name'));
				$insert['Email'] = strtolower(trim(Input::get('email')));
				$insert['Password'] = md5($pass);
				$insert['temprary'] = $pass;
				$insert['TransporterType'] = '';
				$insert['UserType'] = 'requester';
				$insert['ChatName'] = "aquantuo" . Utility::sequence('user');
				$insert['CountryCode'] = "";
				$insert['PhoneNo'] = '';
				$insert['AlternateCCode'] = '';
				$insert['AlternatePhoneNo'] = '';
				$insert['RequesterStatus'] = 'active';
				$insert['Age'] = "";
				$insert['SSN'] = "";
				$insert['Street1'] = '';
				$insert['Street2'] = '';
				$insert['Country'] = '';
				$insert['delete_status'] = 'no';
				$insert['State'] = '';
				$insert['City'] = '';
				$insert['ZipCode'] = '';
				$insert['BankName'] = '';
				$insert['AccountHolderName'] = '';
				$insert['BankAccountNo'] = '';
				$insert['RoutingNo'] = '';
				$insert['StripeId'] = '';
				$insert['TransporterStatus'] = "not_registred";
				$insert['UserStatus'] = "active";
				$insert['Image'] = "";
				$insert['IDProof'] = '';
				$insert['LicenceId'] = '';
				$insert['type_of_id'] = '';
				$insert['TPTrackLocation'] = "on";
				$insert['EmailStatus'] = "on";
				$insert['consolidate_item'] = "on";
				$insert['NoficationStatus'] = "on";
				$insert['TPSetting'] = "on";
				$insert['SoundStatus'] = "on";
				$insert['VibrationStatus'] = "on";
				$insert['EnterOn'] = new MongoDate();
				$insert['UpdateOn'] = new MongoDate();
				$insert['RatingCount'] = 0;
				$insert['RatingByCount'] = 0;
				$insert['tpid'] = '';
				$insert['tpName'] = '';
				$insert['CurrentLocation'] = array();
				$insert['DeliveryAreaCountry'] = array();
				$insert['DeliveryAreaState'] = array();
				$insert['DeliveryAreaCities'] = array();
				$insert['ProfileStatus'] = 'step-one';
				$insert['StripeBankId'] = "";
				$insert['NotificationId'] = Input::get('NotificationId');
				$insert['DeviceId'] = Input::get('DeviceId');
				$insert['DeviceType'] = 'web';
				$insert['UniqueNo'] = strtoupper($UniqueNo) . Utility::sequence('user_unique');
				$insert['bank_info'] = [];
				$insert['AqAddress'] = '';
				$insert['AqCity'] = '';
				$insert['AqState'] = '';
				$insert['AqCountry'] = '';
				$insert['AqZipcode'] = '';
				$insert['AqLatLong'] = [];
				$insert['Default_Currency'] = 'USD';
				$insert['PerPage'] = 20;
				$insert['after_update_difference'] = 0;
				$insert['itemCount'] = '';

				// Get aquantuo addres
				$supportemail = Setting::find('563b0e31e4b03271a097e1ca');
				if (count($supportemail) > 0) {
					$insert['AqAddress'] = $supportemail->AqAddress;
					$insert['AqCity'] = $supportemail->AqCity;
					$insert['AqState'] = $supportemail->AqState;
					$insert['AqCountry'] = $supportemail->AqCountry;
					$insert['AqZipcode'] = $supportemail->AqZipcode;
					$insert['AqLatLong'] = $supportemail->AqLatlong;
				}

				$result['_id'] = (string) User::insertGetId($insert);

				$user = $requester = User::where(['_id' => $result['_id']])->first();

				if ($result['_id']) {
					// email to user
					send_mail('58e4f6f77ac6f6e92d8b4567', [
						'to' => trim(Input::get('email')),
						'replace' => [
							'[USERNAME]' => $insert['Name'],
							'[PASSWORD]' => $pass,
						],
					]);

					$response = array("success" => 1, "msg" => "Requester has been Registered successfully.");
				}
				$requesterid = $result['_id'];
				$requestername = ucfirst(Input::get('first_name')) . " " . ucfirst(Input::get('last_name'));
			}
		}

		$PackageId = $this->get_packageno();
		$city = json_decode(Input::get('city'));
		$state = json_decode(Input::get('state'));
		$country = json_decode(Input::get('country'));

		$drop_off_city = json_decode(Input::get('drop_off_city'));
		$drop_off_state = json_decode(Input::get('drop_off_state'));
		$drop_off_country = json_decode(Input::get('drop_off_country'));

		$return_city = json_decode(Input::get('return_city'));
		$return_state = json_decode(Input::get('return_state'));
		$return_country = json_decode(Input::get('return_country'));

		$nd_return_city = json_decode(Input::get('nd_return_city'));
		$nd_return_state = json_decode(Input::get('nd_return_state'));
		$nd_return_country = json_decode(Input::get('nd_return_country'));

		if (count($requester) == 0) {
			return json_encode($response);die;
		}

		$calculated_distance = floatval(Input::get('calculated_distance'));

		if ($calculated_distance > 0) {
			$calculated_distance = $calculated_distance * 0.000621371;
		} else {
			$calculated_distance = Utility::getDistanceBetweenPointsNew(
				floatval(Input::get('PickupLat')),
				floatval(Input::get('PickupLong')),
				floatval(Input::get('DeliveryLat')),
				floatval(Input::get('DeliveryLong'))
			);
		}

		/*if (!empty(trim(Input::get('request_id')))) {
			$requestId = Deliveryrequest::where('_id', '=', Input::get('request_id'))->select('_id', 'PackageNumber')->first();
			$PackageNumber = $requestId->PackageNumber;
		} else {
			$PackageNumber = Utility::sequence('Request') . time();
		}*/


		if (Input::get('request_id') == '') {
			$PackageId = $this->get_packageno();
			$old_packageNumber = $PackageId . time();
		} else {
			$old_packageNumber = Deliveryrequest::where(['_id'=> Input::get('request_id')])->select('PackageNumber','PackageId')->first();
			$PackageId = $old_packageNumber->PackageId;
			$old_packageNumber = $old_packageNumber->PackageNumber;
		}

		$user = json_decode(Input::get('user'));
		$insert = [
			'RequesterId' => new MongoId($user->id),
			'RequesterName' => ucfirst($requester->Name),
			'ProductTitle' => Input::get('package_title'),
			'RequestType' => "local_delivery",
			'Status' => 'pending',
			'ReceiverIsDifferent'=> (Input::get('ReceiverIsDifferent') == 'on')?'yes':'no', 
			'ReceiverName'=> (Input::get('ReceiverIsDifferent') == 'on')? ucfirst(Input::get('ReceiverName')):'' ,
			'SenderMobileNo' => Input::get('SenderMobileNo'),
			'PackageId' => $PackageId,
			'device_version' => Input::get('browser'),
			//'itemCount' => '',
			'device_type' => Input::get('device_type'),
			'app_version' => Input::get('version'),
			"PackageNumber" => $old_packageNumber,
			"PickupFullAddress" => $this->formated_address([
				(Input::get('address_line_1') != '') ? Input::get('address_line_1') : '',
				(Input::get('address_line_2') != '') ? Input::get('address_line_2') : '',
				$city->name,
				$state->name,
				$country->name,
			], Input::get('zipcode')),
			'PickupAddress' => Input::get('address_line_1'),
			'PickupAddress2' => Input::get('address_line_2'),
			'PickupCity' => $city->name,
			'PickupState' => $state->name,
			'PickupCountry' => $country->name,
			'PickupPinCode' => Input::get('zipcode'),
			'PickupLatLong' => [floatval(Input::get('PickupLong')), floatval(Input::get('PickupLat'))],
			'PickupDate' => get_utc_time(Input::get('pickup_date')),
			'DeliveryFullAddress' => $this->formated_address([
				(Input::get('drop_off_address_line_1') != '') ? Input::get('drop_off_address_line_1') : '',
				(Input::get('drop_off_address_line_2') != '') ? Input::get('drop_off_address_line_2') : '',
				$drop_off_city->name,
				$drop_off_state->name,
				$drop_off_country->name,
			], Input::get('drop_off_zipcode')),
			'DeliveryAddress' => Input::get('drop_off_address_line_1'),
			'DeliveryAddress2' => Input::get('drop_off_address_line_2'),
			'DeliveryCity' => $drop_off_city->name,
			'DeliveryState' => $drop_off_state->name,
			'DeliveryCountry' => $drop_off_country->name,
			'DeliveryPincode' => Input::get('drop_off_zipcode'),
			"DeliveryLatLong" => [floatval(Input::get('DeliveryLong')), floatval(Input::get('DeliveryLat'))],
			'DeliveryDate' => get_utc_time(Input::get('drop_off_date')),

			"ReturnFullAddress" => $this->formated_address([
				Input::get('return_address_line_1'),
				Input::get('return_address_line_2'),
				@$return_city->name,
				@$return_state->name,
				@$return_country->name,
			], Input::get('return_zipcode')),
			"ReturnAddress" => Input::get('return_address_line_1'),
			"ReturnAddress2" => Input::get('return_address_line_2'),
			"ReturnCityTitle" => @$return_city->name,
			"ReturnStateTitle" => @$return_state->name,
			'ReturnCountry' => @$return_country->name,
			'ReturnPincode' => Input::get('return_zipcode'),
			"FlexibleDeliveryDate" => (Input::get('is_delivery_date_flexible') == 'yes') ? 'yes' : 'no',
			"JournyType" => (Input::get('journey_type') == 'return') ? 'return' : 'one_way',
			'NotDelReturnFullAddress' => $this->formated_address([
				Input::get('nd_return_address_line_1'),
				Input::get('nd_return_address_line_2'),
				@$nd_return_city->name,
				@$nd_return_state->name,
				@$nd_return_country->name,
			], Input::get('nd_return_zipcode')),
			"InCaseNotDelReturnAddress" => Input::get('nd_return_address_line_1'),
			"InCaseNotDelReturnAddress2" => Input::get('nd_return_address_line_2'),
			"InCaseNotDelReturnCity" => @$nd_return_city->name,
			"InCaseNotDelReturnState" => @$nd_return_state->name,
			"InCaseNotDelReturnCountry" => @$nd_return_country->name,
			"InCaseNotDelReturnPincode" => Input::get('nd_return_zipcode'),
			"PackageMaterial" => (Input::get('need_package_material') == 'yes') ? 'yes' : 'no',
			"PackageMaterialShipped" => '',
			"PublicPlace" => (Input::get('public_palce') == 'yes') ? 'yes' : 'no',
			'ReceiverCountrycode' => Input::get('country_code'),
			'ReceiverMobileNo' => Input::get('phone_number'),
			'FlexibleDeliveryDate' => Input::get('is_delivery_date_flexible'),
			"Distance" => $calculated_distance,
			'ShippingCost' => 0,
			'InsuranceCost' => 0,
			'InsuranceCost' => 0,
			'Discount' => 0,
			'TotalCost' => 0,
			'promocode' => Input::get('promocode'),
			'AquantuoFees' => 0,
			'after_update_difference' => 0,
			'AreaCharges' => 0,
			'ProductList' => [],
			'EnterOn' => new MongoDate,

		];



		if (Input::get('request_id') == '') {

			$res = json_decode($this->local_prepare_request_calculation());
			$insert["EnterOn"] = new MongoDate();
		} else {
			//echo 'op';die;
			$res = json_decode($this->local_prepare_request_calculation());
		}
		///echo '<pre>';
		//print_r($res); die;

		if (count($res->product) > 0) {
			$k = 0;

			foreach ($res->product as $key) {
				$k++;

				$insert["ProductTitle"] .= @$key->product_name;
				$insert['ShippingCost'] += $key->shippingCost;
				$insert['InsuranceCost'] += @$key->InsuranceCost;
				$insert['TotalCost'] += @$key->shippingCost+@$key->InsuranceCost;

				$insert['ProductList'][] = [
					'_id' => (Input::get('request_id') == '') ? (string) new MongoId() : $key->_id,
					"product_name" => @$key->product_name,

					
					'package_id' => (Input::get('request_id') == '') ? $insert['PackageNumber'] . $k : $key->package_id,
					'status' => (Input::get('request_id') == '') ? 'pending' : $key->status,
					"productWidth" => $key->productWidth,
					"productHeight" => $key->productHeight,
					"productLength" => $key->productLength,
					"productCost" => $key->productCost,
					"productWeight" => $key->productWeight,
					"productHeightUnit" => $key->productHeightUnit,
					"ProductWeightUnit" => $key->ProductWeightUnit,
					"ProductLengthUnit" => $key->ProductLengthUnit,
					"productCategory" => $key->productCategory,
					"productCategoryId" => $key->productCategoryId,
					"travelMode" => $key->travelMode,
					"InsuranceStatus" => $key->InsuranceStatus,
					"productQty" => $key->productQty,
					"ProductImage" => $key->ProductImage,
					"QuantityStatus" => $key->QuantityStatus,
					"BoxQuantity" => $key->BoxQuantity,
					"Description" => $key->Description,
					"PackageMaterial" => $key->PackageMaterial,
					"PackageMaterialShipped" => $key->PackageMaterialShipped,
					"shippingCost" => $key->shippingCost,
					'InsuranceCost' => @$key->InsuranceCost, // error
					"DeliveryVerifyCode" => (Input::get('request_id') == '') ? rand(1000, 9999) : $key->DeliveryVerifyCode,
					'tpid' => '',
					'tpName' => '',
					'inform_mail_sent' => 'no',
					'PaymentStatus' => 'no',
					'total_cost' => $key->shippingCost + @$key->InsuranceCost,
					'after_update' => $key->shippingCost + @$key->InsuranceCost,
					'TransporterFeedbcak' => '',
					'TransporterRating' => '',
					'RequesterFeedbcak' => '',
					'RequesterRating' => '',
					'EnterOn' => '',
					'ExpectedDate' => '',
					'aq_fees'=>0,
				];
			}

			$discount = Promo::get_validate(Input::get('promocode'), $insert['ShippingCost'], $requester->_id, $requester->Default_Currency);
			$insert['AreaCharges'] = $this->resionCharges(['name' => $drop_off_state->name, 'id' => $drop_off_state->id]);
			$insert['TotalCost'] = $insert['TotalCost'] + $insert['AreaCharges'];

			//$insert['AquantuoFees'] = $this->get_aquantuo_fees($insert['TotalCost']);
			$insert['TotalCost'] = $insert['TotalCost'] + $insert['AquantuoFees'];

			if ($discount['success'] == 1) {
				$insert['discount'] = $discount['discount'];
				$insert['TotalCost'] = $insert['TotalCost'] - $discount['discount'];
			}
		} else {
			return json_encode($response);die;
		}

		if (Input::get('request_id') == '') {
			$record = (String) Deliveryrequest::insertGetId($insert);

			$ActivityLog = DeliveryRequest::where('_id', '=', $record)->select('_id', 'PackageNumber', 'ProductList')->first();
			$array = $ActivityLog->ProductList;

			/* Activity Log  create request for Send a Package
				created Aakash Tejwal 24-02-2018
			*/
			foreach ($array as $key) {
				$insertactivity = [
					'request_id' => $record,
					'request_type' => 'local_delivery',
					'PackageNumber' => $ActivityLog->PackageNumber,
					'item_id' => $key['_id'],
					'package_id' => $key['package_id'],
					'item_name' => $key['product_name'],
					'log_type' => 'request',
					'message' => 'Package has been created.',
					'status' => 'pending',
					'action_user_id'=> Auth::user()->_id,
					'EnterOn' => new MongoDate(),
				];
				Activitylog::insert($insertactivity);
			}
		} else {

			$record = Deliveryrequest::where(['_id' => trim(Input::get('request_id'))])
				->update($insert);
		}

		if ($record) {
			if (Input::get('request_id') == '') {
				Additem::where(['user_id' => Session::get('UserId'), 'type' => 'local_delivery'])->delete();
				$response = ['success' => 1, 'msg' => 'Request has been created successfully.', 'reqid' => $record, 'type' => 'create'];
			} else {
				$response = ['success' => 1, 'msg' => 'Request has been updated successfully.', 'reqid' => trim(Input::get('request_id')), 'type' => 'update'];
			}
		}
		return json_encode($response);

	}

	public function formated_address($array, $zip) {
		$address = '';
		foreach ($array as $ky => $val) {
			if (trim($val) != '') {
				if (trim($address) != '') {$address = "$address, ";}
				$address .= $val;
			}
		}
		if (trim($zip) != '') {$address .= "- $zip";}
		return $address;
	}

	public function local_prepare_request_calculation0() {

		$response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		$response = ['success' => 0, 'shipping_cost' => '', 'shipping_cost' => '', 'ghana_total_amount' => '', 'formated_currency' => '', 'total_weight' => 0, 'insurance_cost' => '', 'reasion_charge' => '', 'item_price'];
		if (Input::get('request_id') == '') {
			$items = Additem::where(['user_id' => Session::get('UserId'), 'type' => 'local_delivery'])->get();
		} else {
			$items = Deliveryrequest::where(['_id' => Input::get('request_id')])->select('ProductList')->first();
			$items = $items->ProductList;
		}
		$data['item_price'] = 0;
		$data['category_price'] = 0;
		$data['distance_price'] = 0;
		$data['region_price'] = 0;
		$data['insurance_price'] = 0;
		$data['shipping_cost'] = 0;
		$data['total_weight'] = 0;
		$data['volume'] = 0;
		$data['distance'] = 0;

		$return_array = $items;

		if (count($items) == 0) {
			return json_encode($response);
		}
		$rate = $insurance = $weight = $volume = 0;

		$distance = Utility::getDistanceBetweenPointsNew(Input::get('PickupLat'), Input::get('PickupLong'), Input::get('DeliveryLat'), Input::get('DeliveryLong'));
		if ($distance == false) {
			$response['msg'] = 'Oops! We are unable to calculate distance.dsfsdfdsfdsfs';
			return json_encode($response);die;
		}
		$distance = $distance * 0.000621371;
		$country2 = json_decode(Input::get('drop_off_country'));
		$state2 = json_decode(Input::get('drop_off_state'));

		print_r($distance); die;

		$match_distance = Distance::Where('from', '<=', (float) $distance)->where('to', '>=', (float) $distance)
			->where(['Status' => 'Active', 'country_id' => $country2->id, "type" => "distance"])
			->select('price')
			->first();
		if (count($match_distance) == 0) {
			$response = ['success' => 0, 'msg' => 'The shipping address/distance you have specified is unsupported.'];
			return response()->json($response);
		}

		$match_value = Distance::where(['Status' => 'Active', 'country_id' => $country2->id, "type" => "item-value"])
			->select('price', 'from', 'to')->get();
		if (count($match_distance) == 0) {
			return response()->json($response);
		}

		if ($items) {
			foreach ($items as $key => $value) {
				# code...
				$value['shippingCost'] = 0;
				//Weight
				$weight += $this->get_weight($value['productWeight'], $value['ProductWeightUnit']);
				//Volume
				$volume += $this->get_volume(array(
					"width" => $value['productWidth'],
					"widthunit" => $value['productHeightUnit'],
					"height" => $value['productHeight'],
					"heightunit" => $value['productHeightUnit'],
					"length" => $value['productLength'],
					"lengthunit" => $value['productHeightUnit'],
				));
				//Insurance
				if ($value['InsuranceStatus'] == 'yes') {
					$value['InsuranceCost'] = $this->get_insurance($value['productCost'], $value['productQty']);
					$data['insurance_price'] += $value['InsuranceCost'];
				}
				//Price Item category
				$match_category = Category::where(['_id' => $value['productCategoryId'], 'country_id' => $country2->id, 'Status' => 'Active', 'type' => 'localCategory'])->select('price')
					->first();

				if ($match_category) {
					$data['category_price'] += $match_category->price;
					$return_array[$key]['shippingCost'] += $match_category->price;
					$value['shippingCost'] = $match_category->price;
				} else {
					$response = ['success' => 0, 'msg' => "The shipping category you have specified is unsupported for product '" . $value['product_name'] . '.'];
					return response()->json($response);
				}

				foreach ($match_value as $key) {
					//print_r($key->to); die;
					$flag1 = false;
					$flag2 = false;
					if ($key->from <= $value['productCost']) {
						$flag1 = true;
					}

					if ($key->to >= $value['productCost']) {
						$flag2 = true;
					}

					if ($flag1 == true && $flag2 == true) {
						$data['item_price'] += $key->price;
						$value['shippingCost'] += $key->price;
						$return_array[$key]['shippingCost'] += $match_category->price;
					} else {
						$response = ['success' => 0, 'msg' => "The item value you have specified is unsupported for product '" . $value['product_name'] . '.'];
						return response()->json($response);
					}
				}

				$data['distance_price'] += $match_distance->price;
				$value['shippingCost'] += $match_distance->price;

			}
		}

		//extraa charge of out side accra
		$data['region_price'] = $this->resionCharges(['name' => $state2->name, 'id' => $state2->id]);
		//end accra charge
		$data['shipping_cost'] = $data['category_price'] + $data['item_price'] + $data['distance_price'];
		$data['total_amount'] = $data['shipping_cost'] + $data['region_price'] + $data['insurance_price'];
		// print_r($data['category_price']); die;
		$data['volume'] = $volume;
		$data['total_weight'] = $weight;
		$data['distance'] = $distance;
		$data['product'] = $items;
		$data['item_count'] = count($items);
		$currency_conversion = $this->currency_conversion($data['total_amount']);

		if (Session::get('Default_Currency') == 'GHS') {
			$data['user_currency'] = $currency_conversion['ghanian_cost'];
		} else if (Session()->get('Default_Currency') == 'CAD') {
			$data['user_currency'] = $currency_conversion['canadian_cost'];
		} else if (Session()->get('Default_Currency') == 'PHP') {
			$data['user_currency'] = $currency_conversion['philipins_cost'];
		} else if (Session()->get('Default_Currency') == 'GBP') {
			$data['user_currency'] = $currency_conversion['uk_cost'];
		} else {
			$data['user_currency'] = $currency_conversion['ghanian_cost'];
		}

		//$data['product_list'] = $items;

		$response = ['success' => 1, 'msg' => 'Calculation', 'result' => $data, 'product' => $return_array];
		//print_r($data);die;
		//return response()->json($response);
		return json_encode($response);
	}

	public function local_prepare_request_calculation_old() {

		$response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		$response = ['success' => 0, 'shipping_cost' => '', 'shipping_cost' => '', 'ghana_total_amount' => '', 'formated_currency' => '', 'total_weight' => 0, 'insurance_cost' => '', 'reasion_charge' => '', 'item_price'];
		if (Input::get('request_id') == '') {
			$items = Additem::where(['user_id' => Session::get('UserId'), 'type' => 'local_delivery'])->get();
		} else {
			$items = Deliveryrequest::where(['_id' => Input::get('request_id')])->select('ProductList')->first();
			$items = $items->ProductList;
		}
		$data['item_price'] = 0;
		$data['category_price'] = 0;
		$data['distance_price'] = 0;
		$data['region_price'] = 0;
		$data['insurance_price'] = 0;
		$data['shipping_cost'] = 0;
		$data['total_weight'] = 0;
		$data['volume'] = 0;
		$data['distance'] = 0;

		$return_array = $items;

		if (count($items) == 0) {
			return json_encode($response);
		}
		$rate = $insurance = $weight = $volume = 0;

		$distance = Utility::getDistanceBetweenPointsNew(Input::get('PickupLat'), Input::get('PickupLong'), Input::get('DeliveryLat'), Input::get('DeliveryLong'));
		

		if ($distance == false) {
			$response['msg'] = 'Oops! We are unable to calculate distance.dsfsdfdsfdsfs';
			return json_encode($response);die;
		}
		$distance = $distance * 0.000621371;
		$country2 = json_decode(Input::get('drop_off_country'));
		$state2 = json_decode(Input::get('drop_off_state'));

		$match_distance = Distance::Where('from', '<=', (float) $distance)->where('to', '>=', (float) $distance)
			->where(['Status' => 'Active', 'country_id' => $country2->id, "type" => "distance"])
			->select('price')
			->first();
		if (count($match_distance) == 0) {
			$response = ['success' => 0, 'msg' => 'The shipping address/distance you have specified is unsupported.'];
			return response()->json($response);
		}

		$match_value = Distance::where(['Status' => 'Active', 'country_id' => $country2->id, "type" => "item-value"])
			->select('price', 'from', 'to')->get();
		if (count($match_distance) == 0) {
			return response()->json($response);
		}

		

		if ($items) {
			foreach ($items as $key => $value) {
				# code...
				$value['shippingCost'] = 0;
				//Weight
				$weight += $this->get_weight($value['productWeight'], $value['ProductWeightUnit']);
				//Volume
				$volume += $this->get_volume(array(
					"width" => $value['productWidth'],
					"widthunit" => $value['productHeightUnit'],
					"height" => $value['productHeight'],
					"heightunit" => $value['productHeightUnit'],
					"length" => $value['productLength'],
					"lengthunit" => $value['productHeightUnit'],
				));
				//Insurance
				$return_array[$key]['InsuranceCost'] = 0;
				if ($value['InsuranceStatus'] == 'yes') {
					$value['InsuranceCost'] = $this->get_insurance($value['productCost'], $value['productQty']);
					$data['insurance_price'] += $value['InsuranceCost'];
					$return_array[$key]['InsuranceCost'] = $value['InsuranceCost'];
				}
				//Price Item category
				$match_category = Category::where(['_id' => $value['productCategoryId'], 'country_id' => $country2->id, 'Status' => 'Active', 'type' => 'localCategory'])->select('price')
					->first();

				if ($match_category) {
					$data['category_price'] += $match_category->price;
					$return_array[$key]['shippingCost'] = $match_category->price;
					$value['shippingCost'] = +$match_category->price;
				} else {
					$response = ['success' => 0, 'msg' => "The shipping category you have specified is unsupported for product '" . $value['product_name'] . '.'];
					return response()->json($response);
				}
				$temp = 0;
				foreach ($match_value as $key2) {
					//print_r($key->to); die;
					if ($key2->from <= (float) trim($value['productCost']) && $key2->to >= (float) trim($value['productCost'])) {
						$data['item_price'] += $key2->price;
						$value['shippingCost'] += $key2->price;
						$temp = $key2->price;
						$return_array[$key]['shippingCost'] += $match_category->price;
					}
				}

				if($temp == 0){
					$response = ['success' => 0, 'msg' => "The item value you have specified is unsupported for product '" . $value['product_name'] . '.'];
					return response()->json($response);
				}

				$data['distance_price'] += $match_distance->price;
				$value['shippingCost'] += $match_distance->price;
				$return_array[$key]['shippingCost'] += $match_distance->price;

			}
		}

		//extraa charge of out side accra
		$data['region_price'] = $this->resionCharges(['name' => $state2->name, 'id' => $state2->id]);
		//end accra charge
		$data['shipping_cost'] = $data['category_price'] + $data['item_price'] + $data['distance_price'];
		$data['total_amount'] = $data['shipping_cost'] + $data['region_price'] + $data['insurance_price'];
		// print_r($data['category_price']); die;
		$data['volume'] = $volume;
		$data['total_weight'] = $weight;
		$data['distance'] = $distance;
		$data['product'] = $items;
		$data['item_count'] = count($items);
		$currency_conversion = $this->currency_conversion($data['total_amount']);

		if (Session::get('Default_Currency') == 'GHS') {
			$data['user_currency'] = $currency_conversion['ghanian_cost'];
		} else if (Session()->get('Default_Currency') == 'CAD') {
			$data['user_currency'] = $currency_conversion['canadian_cost'];
		} else if (Session()->get('Default_Currency') == 'PHP') {
			$data['user_currency'] = $currency_conversion['philipins_cost'];
		} else if (Session()->get('Default_Currency') == 'GBP') {
			$data['user_currency'] = $currency_conversion['uk_cost'];
		} else {
			$data['user_currency'] = $currency_conversion['ghanian_cost'];
		}

		//$data['product_list'] = $items;
		

		$response = ['success' => 1, 'msg' => 'Calculation', 'result' => $data, 'product' => $return_array];
		//echo '<pre>';
		//print_r($return_array);die;
		//return response()->json($response);
		return json_encode($response);
	}

	public function local_prepare_request_calculation() {

		$response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		$response = ['success' => 0, 'shipping_cost' => '', 'shipping_cost' => '', 'ghana_total_amount' => '', 'formated_currency' => '', 'total_weight' => 0, 'insurance_cost' => '', 'reasion_charge' => '', 'item_price'];
		if (Input::get('request_id') == '') {
			$items = Additem::where(['user_id' => Session::get('UserId'), 'type' => 'local_delivery'])->get();
		} else {
			$items = Deliveryrequest::where(['_id' => Input::get('request_id')])->select('ProductList')->first();
			$items = $items->ProductList;
		}
		$data['item_price'] = 0;
		$data['category_price'] = 0;
		$data['distance_price'] = 0;
		$data['region_price'] = 0;
		$data['insurance_price'] = 0;
		$data['shipping_cost'] = 0;
		$data['total_weight'] = 0;
		$data['volume'] = 0;
		$data['distance'] = 0;

		$return_array = $items;
		//echo '<pre>';
		//print_r($return_array); 

		if (count($items) == 0) {
			return json_encode($response);
		}
		$rate = $insurance = $weight = $volume = 0;
		$distance = Utility::getDistanceBetweenPointsNew(Input::get('PickupLat'), Input::get('PickupLong'), Input::get('DeliveryLat'), Input::get('DeliveryLong'));
		if ($distance == false) {
			$response['msg'] = 'Oops! We are unable to calculate distance.';
			return json_encode($response);die;
		}
		$distance = $distance * 0.000621371;
		$country2 = json_decode(Input::get('drop_off_country'));
		$state2 = json_decode(Input::get('drop_off_state'));

		//print_r($distance); die;

		$match_distance = Distance::Where('from', '<=', (float) $distance)->where('to', '>=', (float) $distance)
			->where(['Status' => 'Active', 'country_id' => $country2->id, "type" => "distance"])
			->select('price')
			->first();
		if (count($match_distance) == 0) {
			$response = ['success' => 0, 'msg' => 'The shipping address/distance you have specified is unsupported.'];
			return response()->json($response);
		}

		$match_value = Distance::where(['Status' => 'Active', 'country_id' => $country2->id, "type" => "item-value"])
			->select('price', 'from', 'to')->get();
		if (count($match_distance) == 0) {
			return response()->json($response);
		}

		if ($items) {
			foreach ($items as $key => $value) {
				if(Input::get('request_id') != ''){
					$return_array[$key]['shippingCost'] = 0;
				}
				# code...
				$value['shippingCost'] = 0;
				//Weight
				$weight += $this->get_weight($value['productWeight'], $value['ProductWeightUnit']);
				//Volume
				$volume += $this->get_volume(array(
					"width" => $value['productWidth'],
					"widthunit" => $value['productHeightUnit'],
					"height" => $value['productHeight'],
					"heightunit" => $value['productHeightUnit'],
					"length" => $value['productLength'],
					"lengthunit" => $value['productHeightUnit'],
				));
				//Insurance
				if ($value['InsuranceStatus'] == 'yes') {
					$value['InsuranceCost'] = $this->get_insurance($value['productCost'], $value['productQty']);
					$data['insurance_price'] += $value['InsuranceCost'];
				}
				//Price Item category
				$match_category = Category::where(['_id' => $value['productCategoryId'], 'country_id' => $country2->id, 'Status' => 'Active', 'type' => 'localCategory'])->select('price')
					->first();

				if ($match_category) {
					$data['category_price'] += $match_category->price;
					
					$value['shippingCost'] += $match_category->price;
					if(Input::get('request_id') != ''){
						$return_array[$key]['shippingCost'] += $match_category->price;
					}
				} else {
					$response = ['success' => 0, 'msg' => "The shipping category you have specified is unsupported for product '" . $value['product_name'] . '.'];
					return response()->json($response);
				}

				$temp = 0;
				foreach ($match_value as $key2) {
					
					if ($key2->from <= (float) trim($value['productCost']) && $key2->to >= (float) trim($value['productCost'])) {
						$data['item_price'] += $key2->price;
						$value['shippingCost'] += $key2->price;
						$temp = $key2->price;
						
						if(Input::get('request_id') != ''){
							$return_array[$key]['shippingCost'] += $key2->price;
						}
					}
					 
				}

				if($temp == 0){
					$response = ['success' => 0, 'msg' => "The item value you have specified is unsupported for product '" . $value['product_name'] . '.'];
					return response()->json($response);
				}

				

				$data['distance_price'] += $match_distance->price;
				$value['shippingCost'] += $match_distance->price;
				if(Input::get('request_id') != ''){
					$return_array[$key]['shippingCost'] += $match_distance->price;
				}
				//echo '09';
				//print_r($return_array[$key]['shippingCost']);
			}
		}

		//extraa charge of out side accra
		$data['region_price'] = $this->resionCharges(['name' => $state2->name, 'id' => $state2->id]);
		//end accra charge
		$data['shipping_cost'] = $data['category_price'] + $data['item_price'] + $data['distance_price'];
		$data['total_amount'] = $data['shipping_cost'] + $data['region_price'] + $data['insurance_price'];
		// print_r($data['category_price']); die;
		$data['volume'] = $volume;
		$data['total_weight'] = $weight;
		$data['distance'] = $distance;
		$data['product'] = $return_array;
		$data['item_count'] = count($return_array);
		$currency_conversion = $this->currency_conversion($data['total_amount']);

		if (Session::get('Default_Currency') == 'GHS') {
			$data['user_currency'] = $currency_conversion['ghanian_cost'];
		} else if (Session()->get('Default_Currency') == 'CAD') {
			$data['user_currency'] = $currency_conversion['canadian_cost'];
		} else if (Session()->get('Default_Currency') == 'PHP') {
			$data['user_currency'] = $currency_conversion['philipins_cost'];
		} else if (Session()->get('Default_Currency') == 'GBP') {
			$data['user_currency'] = $currency_conversion['uk_cost'];
		} else {
			$data['user_currency'] = $currency_conversion['ghanian_cost'];
		}

		$data['shipping_cost_in_ghs'] = $this->currency_conversion_without_format_text($data['shipping_cost']);
		$data['region_price_in_ghs'] = $this->currency_conversion_without_format_text($data['region_price']);
		$data['total_amount_in_ghs'] = $this->currency_conversion_without_format_text($data['total_amount']);
		$data['insurance_price_in_ghs'] = $this->currency_conversion_without_format_text($data['insurance_price']);
		$data['total_amount_in_ghs'] = $this->currency_conversion_without_format_text($data['total_amount']);


		//$data['product_list'] = $items;
		//echo '<pre>';
		

		$response = ['success' => 1, 'msg' => 'Calculation', 'result' => $data, 'product' => $return_array];
		//print_r($data);die;
		//return response()->json($response);
		//print_r($return_array); die;
		return json_encode($response);
	}

	public function currency_conversion_without_format_text($shipcost,$currency="GHS") {
		$currency = Currency::where(['Status' => 'Active','CurrencyCode'=>'GHS'])->select('CurrencyRate','CurrencyCode')->first();
		$cost = 0.0;
		if ($currency) {
			$cost =  number_format($shipcost * $currency->CurrencyRate, 2);
		}
		return $cost;
	}

	public function get_aquantuo_fees($totalcost) {
		$fees = 0;
		$configurationdata = Configuration::where(['_id' => '5673e33e6734c4f874685c84'])->first();
		if ($configurationdata) {
			$fees = ((floatval($totalcost) * $configurationdata->aquantuoFees) / 100);
		}
		return $fees;
	}

	public function edit_local_delivery_request($id) {

		$data['delivery_data'] = Deliveryrequest::where(array('_id' => $id, 'RequestType' => 'local_delivery'))
			->whereIn('Status', ['ready', 'pending'])->first();

		if (!count($data['delivery_data']) > 0) {
			return redirect('local-delivery-details/' . $id);
		}

		if (!empty($data['delivery_data']->TripId)) {
			$data['trip_data'] = Trips::where(array('_id' => $data['delivery_data']->TripId))->first();

			if (!count($data['trip_data']) > 0) {
				return redirect()->back();
			}

			$data['tr_data'] = User::where(array('_id' => $data['trip_data']->TransporterId))->first();

			// $data['category2'] = Category::where(['type' => 'Category', 'Status' => 'Active'])->whereIn('TravelMode', ['ship', 'air'])->whereIn('Content', $data['trip_data']->SelectCategory)->get(['TravelMode', 'Content']);

			$data['category2'] = Category::where(['type' => 'localCategory', 'Status' => 'Active'])->whereIn('TravelMode', ['ship', 'air'])->whereIn('Content', $data['trip_data']->SelectCategory)->get(['TravelMode', 'Content']);
		}

		$data['all_state'] = CityStateCountry::where(['type' => 'State', 'Status' => 'Active', 'CountryId' => '56bdbfb4cf32079714ee89ca'])
			->orderBy('Content', 'Asc')
			->get(['_id', 'Content', 'CountryId']);

		$data['category'] = Category::where(['type' => 'localCategory', 'Status' => 'Active', "country_id" => "56bdbfb4cf32079714ee89ca"])->get();

		$data['country'] = CityStateCountry::where(['type' => 'Country', 'Status' => 'Active'])
			->orderBy('Content', 'Asc')
			->get(['_id', 'Content', 'state_available']);

		// $data['category'] = Category::where(['type' => 'localCategory', 'Status' => 'Active'])->whereIn('TravelMode', ['ship', 'air'])->get(['TravelMode', 'Content']);

		$data['state'] = CityStateCountry::where(['type' => 'State', 'Status' => 'Active', 'Content' => @$data['delivery_data']['PickupState']])->first();

		$data['drop_state'] = CityStateCountry::where(['type' => 'State', 'Status' => 'Active', 'Content' => @$data['delivery_data']['DeliveryState']])->first();

		$data['return_state'] = CityStateCountry::where(['type' => 'State', 'Status' => 'Active', 'Content' => @$data['delivery_data']['ReturnStateTitle']])->first();

		$data['nd_return_state'] = CityStateCountry::where(['type' => 'State', 'Status' => 'Active', 'Content' => @$data['delivery_data']['InCaseNotDelReturnState']])->first();

		if ($data['state']) {
			$data['city'] = CityStateCountry::where(['type' => 'city', 'Status' => 'Active', 'Content' => @$data['delivery_data']['PickupCity']])->first();
		}

		if ($data['drop_state']) {
			$data['drop_city'] = CityStateCountry::where(['type' => 'city', 'Status' => 'Active', 'Content' => @$data['delivery_data']['DeliveryCity']])->first();
		}

		if ($data['return_state']) {
			$data['return_city'] = CityStateCountry::where(['type' => 'city', 'Status' => 'Active', 'Content' => @$data['delivery_data']['ReturnCityTitle']])->first();
		}

		if ($data['nd_return_state']) {
			$data['nd_return_city'] = CityStateCountry::where(['type' => 'city', 'Status' => 'Active', 'Content' => @$data['delivery_data']['InCaseNotDelReturnCity']])->first();
		}
		$data['user'] = User::select('_id', 'Name', 'Email')->orderBy('Name', 'asc')->get();

		// echo "<pre>";
		//print_r($data);die;
		if (isset($data['delivery_data']->ProductList)) {
			return view('Admin::localdelivery.edit.edit_local_delivery', $data);
		} else {
			return view('Admin::localdelivery.edit.local_delivery_prepare_request', $data);
		}

	}

	public function edit_local_delivery_request111($id) {
		$data['delivery_data'] = Deliveryrequest::where(array('_id' => $id))
			->whereIn('Status', ['ready', 'pending'])->first();

		if (!count($data['delivery_data']) > 0) {
			return redirect('local-delivery-details/' . $id);
		}

		if (!empty($data['delivery_data']->TripId)) {
			$data['trip_data'] = Trips::where(array('_id' => $data['delivery_data']->TripId))->first();

			if (!count($data['trip_data']) > 0) {
				return redirect()->back();
			}

			$data['tr_data'] = User::where(array('_id' => $data['trip_data']->TransporterId))->first();

			$data['category2'] = Category::where(['type' => 'localCategory', 'Status' => 'Active'])->whereIn('TravelMode', ['ship', 'air'])->whereIn('Content', $data['trip_data']->SelectCategory)->get(['TravelMode', 'Content']);
		}

		$data['country'] = CityStateCountry::where(['type' => 'Country', 'Status' => 'Active'])
			->orderBy('Content', 'Asc')
			->get(['_id', 'Content', 'state_available']);

		$data['state'] = CityStateCountry::where(['type' => 'State', 'Status' => 'Active', 'CountryId' => '56bdbfb4cf32079714ee89ca'])
			->orderBy('Content', 'Asc')
			->get(['_id', 'Content', 'CountryId']);

		$data['category'] = Category::where(['type' => 'localCategory', 'Status' => 'Active', "country_id" => "56bdbfb4cf32079714ee89ca"])->get();

		$data['drop_state'] = CityStateCountry::where(['type' => 'State', 'Status' => 'Active', 'Content' => @$data['delivery_data']['DeliveryState']])->first();

		$data['return_state'] = CityStateCountry::where(['type' => 'State', 'Status' => 'Active', 'Content' => @$data['delivery_data']['ReturnStateTitle']])->first();

		$data['nd_return_state'] = CityStateCountry::where(['type' => 'State', 'Status' => 'Active', 'Content' => @$data['delivery_data']['InCaseNotDelReturnState']])->first();

		if ($data['state']) {
			$data['city'] = CityStateCountry::where(['type' => 'city', 'Status' => 'Active', 'Content' => @$data['delivery_data']['PickupCity']])->first();
		}

		if ($data['drop_state']) {
			$data['drop_city'] = CityStateCountry::where(['type' => 'city', 'Status' => 'Active', 'Content' => @$data['delivery_data']['DeliveryCity']])->first();
		}

		if ($data['return_state']) {
			$data['return_city'] = CityStateCountry::where(['type' => 'city', 'Status' => 'Active', 'Content' => @$data['delivery_data']['ReturnCityTitle']])->first();
		}

		if ($data['nd_return_state']) {
			$data['nd_return_city'] = CityStateCountry::where(['type' => 'city', 'Status' => 'Active', 'Content' => @$data['delivery_data']['InCaseNotDelReturnCity']])->first();
		}
		$data['user'] = User::select('_id', 'Name', 'Email')->orderBy('Name', 'asc')->get();

		// echo "<pre>";
		//print_r($data);die;
		if (isset($data['delivery_data']->ProductList)) {
			return view('Admin::localdelivery.edit.edit_local_delivery', $data);
		} else {
			return view('Admin::localdelivery.edit.local_delivery_prepare_request', $data);
		}
	}

	public function editLocalItem() {
		echo "sdfdsfdssd";die;
	}
	public function localaddItemInExistRequest() {
		$response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];

		if (Input::get('request_id') != '') {
			$data['request_data'] = Deliveryrequest::where(['_id' => trim(Input::get('request_id'))])->select('ProductList','PackageNumber')->first();
			$lhwunit = 'cm';
			$weightunit = 'kg';
			if (Input::get('measurement_unit') == 'inches_lbs') {
				$lhwunit = 'inches';
				$weightunit = 'lbs';
			}

			$ProductImage = '';

			if (Input::hasFile('default_image')) {

				if (Input::file('default_image')->isValid()) {
					$ext = Input::file('default_image')->getClientOriginalExtension();
					$ProductImage = time() . rand(100, 9999) . ".$ext";

					if (Input::file('default_image')->move(BASEURL_FILE . 'package/', $ProductImage)) {
						$ProductImage = "package/$ProductImage";
					} else {
						$ProductImage = '';
					}
				}
			}
			$otherImages = [];
			$imageArray = array('package_image2', 'package_image3', 'package_image4', 'package_image5');
			foreach ($imageArray as $key => $val) {
				if (Input::hasFile($val)) {
					if (Input::file($val)->isValid()) {
						$ext = Input::file($val)->getClientOriginalExtension();
						$img = time() . rand(100, 9999) . ".$ext";

						if (Input::file($val)->move(BASEURL_FILE . 'package/', $img)) {
							$img = "package/$img";
							if (isset($otherImages[$key])) {
								// Start remove old image
								if (!empty(trim($otherImages[$key]))) {
									if (file_exists(BASEURL_FILE . $otherImages[$key])) {
										unlink(BASEURL_FILE . $otherImages[$key]);
									}
								}
								// End remove old image
								$otherImages[$key] = $img;
							} else {
								$otherImages[] = $img;
							}
						}
					}
				}
			}
			$category = json_decode(Input::get('category'));
			$itemCount = count($data['request_data']->ProductList);
			$package_id = $data['request_data']->PackageNumber.(int) $itemCount+1;
			$add_item = [
				"_id" => (String) new MongoId(),
				'product_name' => ucfirst(Input::get('title')),
				'user_id' => Session::get('UserId'),
				'type' => 'local_delivery',
				"package_id" => (String) $package_id, 
				"status" => "pending", 
				'productWidth' => Input::get('width'),
				'productHeight' => Input::get('height'),
				'productLength' => Input::get('length'),
				'productCost' => (Input::get('package_value') == '')?0:Input::get('package_value') ,
				'productWeight' => Input::get('weight'),
				'productHeightUnit' => $lhwunit,
				'ProductWeightUnit' => $weightunit,
				'ProductLengthUnit' => $lhwunit,
				'productCategory' => @$category->name,
				'productCategoryId' => @$category->id,
				'travelMode' => Input::get('travel_mode'),
				'currency' => Input::get('Default_Currency'),
				'needInsurance' => Input::get('insurance'),
				"productQty" => (Input::get('quantity') < 1) ? 1 : Input::get('quantity'),
				'ProductImage' => $ProductImage,
				//'OtherImage'=>$otherImages,
				'ProductTitle' => ucfirst(Input::get('title')),
				"QuantityStatus" => (Input::get('quantity') > 1) ? 'multiple' : 'single',
				"BoxQuantity" => (Input::get('quantity') == 0) ? 1 : (int) Input::get('quantity'),
				"Description" => Input::get('description'),
				"InsuranceStatus" => (Input::get('insurance') == 'yes') ? 'yes' : 'no',
				"InsuranceCost" => 0,
				"PackageMaterial" => (Input::get('need_package_material') == 'yes') ? 'yes' : 'no',
				"PackageMaterialShipped" => '',
				"DeliveryVerifyCode" => rand(1000, 9999),
			];

			if (isset($data['request_data']->ProductList)) {
				$update = Deliveryrequest::where(['_id' => trim(Input::get('request_id'))])->push(['ProductList' => $add_item]);
				if ($update) {
					$response = ['success' => 1, 'msg' => 'Item has been added successfully.'];
				}
			}
		}
		return json_encode($response);
	}

	public function local_edit_calculation() {
		$response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		if (Input::get('request_id') != '') {
			$request_data = Deliveryrequest::where(['_id' => trim(Input::get('request_id')), 'RequesterId' => new MongoId(Session::get('UserId'))])
				->select('ProductList')
				->first();
			if (count($request_data) == 0 && !isset($request_data->ProductList)) {
				return json_encode($response);die;
			}
			$rate = 0;
			$insurance = 0;
			$weight = 0;
			$volume = 0;
			$distance = $this->get_distance(Input::get('calculated_distance'));
			$drop_lat_long = ['lat' => Input::get('DeliveryLat'), 'lng' => Input::get('DeliveryLong')];
			$distance_cal = Utility::GetDrivingDistance(Input::get('PickupLat'), Input::get('PickupLong'), Input::get('DeliveryLat'), Input::get('DeliveryLong'));
			if ($distance_cal == false) {
				$response['msg'] = 'Oops! We are unable to calculate distance dffdgfd.';
				return json_encode($response);die;
			} else {
				$distance = $this->get_distance($distance_cal->distance);
				$distance2 = $distance_cal->distance;
			}

			$setting = Configuration::where(['_id' => '5673e33e6734c4f874685c84'])->select('accra_charge', 'accra_charge_type')->first();
			$accra_commision = 0;
			$response = ['success' => 0, 'shipping_cost' => '', 'shipping_cost' => '', 'ghana_total_amount' => '', 'formated_currency' => '', 'total_weight' => 0];
			$return_array = [];

			foreach ($request_data->ProductList as $key) {
				$category = Category::where(['_id' => $key['productCategoryId'], 'TravelMode' => $key['travelMode'], 'Status' => 'Active'])->first();
				$key['InsuranceCost'] = 0;
				if ($category) {
					$weight += $this->get_weight($key['productWeight'], $key['ProductWeightUnit']);
					if ($key['InsuranceStatus'] == 'yes') {
						$key['InsuranceCost'] = $this->get_insurance($key['productCost'], $key['productQty']);
						$insurance += $key['InsuranceCost'];
					}

					$volume += $this->get_volume(array(
						"width" => $key['productWidth'],
						"widthunit" => $key['ProductLengthUnit'],
						"height" => $key['productHeight'],
						"heightunit" => $key['productHeightUnit'],
						"length" => $key['productLength'],
						"lengthunit" => $key['ProductLengthUnit'],
					));

					if (isset($category->Shipping) && is_array($category->Shipping)) {

						foreach ($category->Shipping as $key2) {
							if ($category->ChargeType == 'distance') {

								if ($distance >= $key2['MinDistance'] && $distance <= $key2['MaxDistance']) {
									$key['shippingCost'] = $key2['Rate'] * (int) $key['productQty'];
									$rate += $key2['Rate'] * (int) $key['productQty'];
								}
							} else if ($category->ChargeType == 'fixed') {
								if ($weight >= $key2['MinDistance'] && $weight <= $key2['MaxDistance']) {
									$key['shippingCost'] = $key2['Rate'] * (int) $key['productQty'];
									$rate += $key2['Rate'] * (int) $key['productQty'];

								}
							}
						}
					}
				}
			}

			$return_array = $request_data->ProductList;
			$response['success'] = 1;

			$response['accra_commision'] = 0;
			$response['shipping_cost'] = $rate;
			$response['insurance'] = $insurance;
			$response['total_amount'] = $response['shipping_cost'] + $insurance;
			$response['product'] = $return_array;
			$currency_conversion = $this->currency_conversion($response['total_amount']);

			if (Session()->get('Default_Currency') == 'GHS') {
				$response['user_currency'] = $currency_conversion['ghanian_cost'];
			} else if (Session()->get('Default_Currency') == 'CAD') {
				$response['user_currency'] = $currency_conversion['canadian_cost'];
			} else if (Session()->get('Default_Currency') == 'PHP') {
				$response['user_currency'] = $currency_conversion['philipins_cost'];
			} else if (Session()->get('Default_Currency') == 'GBP') {
				$response['user_currency'] = $currency_conversion->uk_cost;
			} else {
				$response['user_currency'] = $currency_conversion['ghanian_cost'];
			}

			$response['distance'] = $distance;
			$response['volume'] = $volume;

			$response['total_weight'] = $weight;

		}
		return json_encode($response);
	}

	public function prepare_request_calculation() {
		$response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		$response = ['success' => 0, 'shipping_cost' => '', 'shipping_cost' => '', 'ghana_total_amount' => '', 'formated_currency' => '', 'total_weight' => 0, 'insurance_cost' => '', 'reasion_charge' => '', 'item_price'];
		$items = Additem::where(['user_id' => Session::get('UserId'), 'type' => 'local_delivery'])->get();

		$data['item_price'] = 0;
		$data['category_price'] = 0;
		$data['distance_price'] = 0;
		$data['region_price'] = 0;
		$data['insurance_price'] = 0;
		$data['shipping_cost'] = 0;
		$data['total_weight'] = 0;
		$data['volume'] = 0;
		$data['distance'] = 0;

		$return_array = $items;

		if (count($items) == 0) {
			return json_encode($response);
		}
		$rate = $insurance = $weight = $volume = 0;
		$distance = Utility::getDistanceBetweenPointsNew(Input::get('PickupLat'), Input::get('PickupLong'), Input::get('DeliveryLat'), Input::get('DeliveryLong'));
		if ($distance == false) {
			$response['msg'] = 'Oops! We are unable to calculate distance.4353453453';
			return json_encode($response);die;
		}
		$distance = $distance * 0.000621371;
		$country2 = json_decode(Input::get('drop_off_country'));

		$match_distance = Distance::Where('from', '<=', (float) $distance)->where('to', '>=', (float) $distance)
			->where(['Status' => 'Active', 'country_id' => $country2->id, "type" => "distance"])
			->select('price')
			->first();
		if (count($match_distance) == 0) {
			$response = ['success' => 0, 'msg' => 'Sorry! Shipping condition does not match according to choose addresses.'];
			return response()->json($response);
		}

		$match_value = Distance::where(['Status' => 'Active', 'country_id' => $country2->id, "type" => "item-value"])
			->select('price', 'from', 'to')->get();
		if (count($match_distance) == 0) {
			return response()->json($response);
		}

		if ($items) {
			foreach ($items as $key => $value) {
				# code...
				$value['shippingCost'] = 0;
				//Weight
				$weight += $this->get_weight($value['productWeight'], $value['productWeightUnit']);
				//Volume
				$volume += $this->get_volume(array(
					"width" => $value['productWidth'],
					"widthunit" => $value['productHeightUnit'],
					"height" => $value['productHeight'],
					"heightunit" => $value['productHeightUnit'],
					"length" => $value['productLength'],
					"lengthunit" => $value['productHeightUnit'],
				));
				//Insurance
				if ($value['InsuranceStatus'] == 'yes') {

					$value['InsuranceCost'] = $this->get_insurance($value['productCost'], $value['productQty']);
					$data['insurance_price'] += $value['InsuranceCost'];

				}
				//Price Item category
				$match_category = Category::where(['_id' => "5a8e50b37ac6f6ba0e8b4568", 'Status' => 'Active', 'type' => 'localCategory'])->select('price')
					->first();

				if ($match_category) {
					$data['category_price'] += $match_category->price;
					//$return_array[$key]['shippingCost'] = $match_category->price;
					$value['shippingCost'] = $match_category->price;
				} else {
					$response = ['success' => 0, 'msg' => 'Category not found.'];
					return response()->json($response);
				}

				foreach ($match_value as $key) {
					//print_r($key->to); die;
					$flag = false;
					if ($key->from <= 150) {
						$flag = true;
					}

					if ($key->to >= 150) {
						$flag = true;
					}

					if ($flag == true) {
						$data['item_price'] += $key->price;
						$value['shippingCost'] += $key->price;
						//$return_array[$key]['shippingCost'] += $match_category->price;
					} else {

						$response = ['success' => 0, 'msg' => "Sorry! Shipping condition does not match according to given item value for product '" . $value['product_name'] . '.'];
						return response()->json($response);
					}
				}

				$data['distance_price'] += $match_distance->price;
				$value['shippingCost'] += $match_distance->price;

			}
		}

		//extraa charge of out side accra
		$data['region_price'] = $this->resionCharges(['name' => Input::get('state_name'), 'id' => Input::get('state_id')]);
		//end accra charge
		$data['shipping_cost'] = $data['category_price'] + $data['item_price'] + $data['distance_price'];
		$data['total_amount'] = $data['shipping_cost'] + $data['region_price'] + $data['insurance_price'];
		// print_r($data['category_price']); die;
		$data['volume'] = $volume;
		$data['total_weight'] = $weight;
		$data['distance'] = $distance;
		$data['product'] = $items;

		//$return_array = $items;
		// $response['success'] = 1;

		// $response['accra_commision'] = 0;
		// $response['shipping_cost'] = $rate;
		// $response['insurance'] = $insurance;
		// $response['total_amount'] = $response['shipping_cost'] + $insurance;
		// $response['product'] = $return_array;

		$currency_conversion = $this->currency_conversion($data['total_amount']);

		if (Session::get('Default_Currency') == 'GHS') {
			$data['user_currency'] = $currency_conversion['ghanian_cost'];
		} else if (Session()->get('Default_Currency') == 'CAD') {
			$data['user_currency'] = $currency_conversion['canadian_cost'];
		} else if (Session()->get('Default_Currency') == 'PHP') {
			$data['user_currency'] = $currency_conversion['philipins_cost'];
		} else if (Session()->get('Default_Currency') == 'GBP') {
			$data['user_currency'] = $currency_conversion['uk_cost'];
		} else {
			$data['user_currency'] = $currency_conversion['ghanian_cost'];
		}

		//$data['product_list'] = $items;

		$response = ['success' => 1, 'msg' => 'Calculation', 'result' => $data, 'product' => $return_array];
		//print_r($data);die;
		//return response()->json($response);
		return json_encode($response);
	}

	public function LocaldeleteItem($itemId, $requestId) {
		$response = array('success' => 0, 'msg' => 'Fail to delete request.');
		if (isset($requestId)) {

			$res_data = Deliveryrequest::where(['_id' => trim($requestId)])
				->select('ProductList')->first();
			if (count($res_data) > 0) {
				if (isset($res_data->ProductList)) {
					$update_array = $res_data->ProductList;
					foreach ($update_array as $key => $value) {
						if ($value['_id'] == $itemId) {
							unset($update_array[$key]);
						}
					}
					$update = Deliveryrequest::where(['_id' => trim($requestId)])
						->update(['ProductList' => $update_array]);
					if ($update) {
						$response['success'] = 1;
						$response['msg'] = 'Item has been removed successfully.';
					}
				}

			}
		}
		return Redirect::to('admin/edit-local-delivery-request/' . $requestId)->withSuccess('item deleted Successfully.');
	}

	public function localDeliveryeditItem(Request $request) {
		$response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		if (Input::get('item_id') != '') {
			if (Input::hasFile('default_image')) {
				if (Input::file('default_image')->isValid()) {
					$ext = Input::file('default_image')->getClientOriginalExtension();
					$ProductImage = time() . rand(100, 9999) . ".$ext";
					if (Input::file('default_image')->move(BASEURL_FILE . 'package/', $ProductImage)) {
						$updateDate['ProductImage'] = "package/$ProductImage";
					}
				}
			}

			$where = [
				"ProductList" => ['$elemMatch' => [
					'_id' => $request->get('item_id'),
				]],
			];
			$category = json_decode(Input::get('category'));
			$result = Deliveryrequest::where($where)->update(
				[
					'ProductList.$.product_name' => Input::get('title'),
					'ProductList.$.productCost' => (Input::get('package_value') == '')?0:Input::get('package_value') ,
					'ProductList.$.measurement_unit' => Input::get('measurement_unit'),
					'ProductList.$.productLength' => Input::get('length'),
					'ProductList.$.productWeight' => Input::get('width'),
					'ProductList.$.productHeight' => Input::get('height'),
					'ProductList.$.productWeight' => Input::get('weight'),
					'ProductList.$.quantity_type' => Input::get('quantity_type'),
					'ProductList.$.productCategory' => $category->name,
					'ProductList.$.productCategoryId' => $category->id,
					'ProductList.$.description' => Input::get('description'),
					'ProductList.$.needInsurance' => Input::get('insurance'),
					'ProductList.$.need_package_material' => Input::get('need_package_material'),

				]);

			if (count($result) > 0) {
				$response = ['success' => 1, 'msg' => 'Item has been updated successfully.'];
			}
		}
		return json_encode($response);

	}
	public function localposteditItemInExistRequest(Request $request) {
		$response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		if (Input::get('item_id') != '') {

			if (Input::hasFile('default_image')) {
				if (Input::file('default_image')->isValid()) {
					$ext = Input::file('default_image')->getClientOriginalExtension();
					$ProductImage = time() . rand(100, 9999) . ".$ext";
					if (Input::file('default_image')->move(BASEURL_FILE . 'package/', $ProductImage)) {
						$updateDate['ProductImage'] = "package/$ProductImage";
					}
				}
			}

			$category = json_decode(Input::get('category'));
			$updateDate['product_name'] = Input::get('title');
			$updateDate['productCost'] = (Input::get('package_value') == '')? 0:Input::get('package_value') ;
			$updateDate['measurement_unit'] = Input::get('measurement_unit');
			$updateDate['productLength'] = Input::get('length');
			$updateDate['productWeight'] = Input::get('width');
			$updateDate['productHeight'] = Input::get('height');
			$updateDate['productWeight'] = Input::get('weight');
			$updateDate['quantity_type'] = Input::get('quantity_type');
			$updateDate['travel_mode'] = Input::get('travel_mode');
			$updateDate['productCategory'] = $category->name;
			$updateDate['productCategoryId'] = $category->id;
			$updateDate['description'] = Input::get('description');
			$updateDate['needInsurance'] = Input::get('insurance');
			$updateDate['need_package_material'] = Input::get('need_package_material');

			$result = Additem::where('_id', '=', Input::get('item_id'))->update($updateDate);

			if (count($result) > 0) {
				$response = ['success' => 1, 'msg' => 'Item has been updated successfully.'];
			}
		}
		return json_encode($response);
	}

	public function local_remove_item($id, $requestid) {
		echo "dsfdsfds";die;
		$response = array('success' => 0, 'msg' => 'Fail to delete request.');

		if ($requestid) {
			$res_data = Deliveryrequest::where(['_id' => trim($requestid)])
				->select('ProductList')->first();
			if (count($res_data) > 0) {
				if (isset($res_data->ProductList)) {
					$update_array = $res_data->ProductList;
					foreach ($update_array as $key => $value) {
						if ($value['_id'] == $id) {
							unset($update_array[$key]);
						}
					}
					$update = Deliveryrequest::where(['_id' => trim($requestid)])
						->update(['ProductList' => $update_array]);
					if ($update) {
						$response['success'] = 1;
						$response['msg'] = 'Item has been removed successfully.';
					}
				}

			}
		}
		return json_encode($response);
	}

	public function localDeliveryDeleteItem($id) {
		$result = Additem::where('_id', '=', $id)->delete();
		if (count($result) > 0) {
			return Redirect::to('admin/add-local-delivery')->withSuccess('item deleted Successfully.');
		} else {
			return Redirect::to('admin/add-local-delivery')->with('fail', 'Problem in delete.');
		}
	}

	public function addItemHistorySendPackage($data) {
		if (!empty($data['ProductList'])) {
			foreach ($data['ProductList'] as $key) {
				if ($key['_id'] == $data['item_id']) {

					$insertData = [
						'request_id' => $data['request_id'],
						'item_id' => $key['_id'],
						'product_name' => $key['product_name'],
						'package_id' => $key['package_id'],
						'status' => $key['status'],
						'productWidth' => $key['productWidth'],
						'productHeight' => $key['productHeight'],
						'productLength' => $key['productLength'],
						'productCost' => $key['productCost'],
						'productWeight' => $key['productWeight'],
						'productHeightUnit' => $key['productHeightUnit'],
						'ProductWeightUnit' => $key['ProductWeightUnit'],
						'ProductLengthUnit' => $key['ProductLengthUnit'],
						'productCategory' => $key['productCategory'],
						'productCategoryId' => $key['productCategoryId'],
						'travelMode' => $key['travelMode'],
						'InsuranceStatus' => $key['InsuranceStatus'],
						'productQty' => $key['productQty'],
						'ProductImage' => $key['ProductImage'],
						'QuantityStatus' => $key['QuantityStatus'],
						'Description' => $key['Description'],
						'PackageMaterial' => $key['PackageMaterial'],
						'PackageMaterialShipped' => $key['PackageMaterialShipped'],
						'shippingCost' => $key['shippingCost'],
						'InsuranceCost' => $key['InsuranceCost'],
						'DeliveryVerifyCode' => $key['DeliveryVerifyCode'],
					];

					Itemhistory::Insert($insertData);

				}
			}
		}
	}

	public function localEditRequest($id) {

		// $data['delivery_data'] = Deliveryrequest::where(array('_id' => $id))
		//     ->whereIn('Status', ['ready', 'pending'])->first();

		// if (!count($data['delivery_data']) > 0) {
		//     return redirect('delivery-details/' . $id);
		// }

		// if (!empty($data['delivery_data']->TripId)) {
		//     $data['trip_data'] = Trips::where(array('_id' => $data['delivery_data']->TripId))->first();

		//     if (!count($data['trip_data']) > 0) {
		//         return redirect()->back();
		//     }

		//     $data['tr_data'] = User::where(array('_id' => $data['trip_data']->TransporterId))->first();

		//     $data['category2'] = Category::where(['type' => 'localCategory', 'Status' => 'Active'])->whereIn('TravelMode', ['ship', 'air'])->whereIn('Content', $data['trip_data']->SelectCategory)->get(['TravelMode', 'Content']);
		// }

		// $data['country'] = CityStateCountry::where(['type' => 'Country', 'Status' => 'Active'])
		//     ->orderBy('Content', 'Asc')
		//     ->get(['_id', 'Content', 'state_available']);
		// $data['category'] = Category::where(['type' => 'Category', 'Status' => 'Active'])->whereIn('TravelMode', ['ship', 'air'])->get(['TravelMode', 'Content']);

		// $data['state'] = CityStateCountry::where(['type' => 'State', 'Status' => 'Active', 'Content' => @$data['delivery_data']['PickupState']])->first();

		// $data['drop_state'] = CityStateCountry::where(['type' => 'State', 'Status' => 'Active', 'Content' => @$data['delivery_data']['DeliveryState']])->first();

		// $data['return_state'] = CityStateCountry::where(['type' => 'State', 'Status' => 'Active', 'Content' => @$data['delivery_data']['ReturnStateTitle']])->first();

		// $data['nd_return_state'] = CityStateCountry::where(['type' => 'State', 'Status' => 'Active', 'Content' => @$data['delivery_data']['InCaseNotDelReturnState']])->first();

		// if ($data['state']) {
		//     $data['city'] = CityStateCountry::where(['type' => 'city', 'Status' => 'Active', 'Content' => @$data['delivery_data']['PickupCity']])->first();
		// }

		// if ($data['drop_state']) {
		//     $data['drop_city'] = CityStateCountry::where(['type' => 'city', 'Status' => 'Active', 'Content' => @$data['delivery_data']['DeliveryCity']])->first();
		// }

		// if ($data['return_state']) {
		//     $data['return_city'] = CityStateCountry::where(['type' => 'city', 'Status' => 'Active', 'Content' => @$data['delivery_data']['ReturnCityTitle']])->first();
		// }

		// if ($data['nd_return_state']) {
		//     $data['nd_return_city'] = CityStateCountry::where(['type' => 'city', 'Status' => 'Active', 'Content' => @$data['delivery_data']['InCaseNotDelReturnCity']])->first();
		// }
		// $data['user'] = User::select('_id', 'Name', 'Email')->orderBy('Name', 'asc')->get();
		// if (isset($data['delivery_data']->ProductList)) {
		//     return view('Admin::localdelivery.edit.edit_local_delivery', $data);
		// } else {
		//     return view('Admin::localdelivery.edit.local_delivery_prepare_request', $data);
		// }

		$data['delivery_data'] = Deliveryrequest::where(array('_id' => $id, 'RequestType' => 'local_delivery'))
			->whereIn('Status', ['ready', 'pending'])->first();

		if (!count($data['delivery_data']) > 0) {
			return redirect('local-delivery-details/' . $id);
		}

		if (!empty($data['delivery_data']->TripId)) {
			$data['trip_data'] = Trips::where(array('_id' => $data['delivery_data']->TripId))->first();

			if (!count($data['trip_data']) > 0) {
				return redirect()->back();
			}

			$data['tr_data'] = User::where(array('_id' => $data['trip_data']->TransporterId))->first();

			// $data['category2'] = Category::where(['type' => 'Category', 'Status' => 'Active'])->whereIn('TravelMode', ['ship', 'air'])->whereIn('Content', $data['trip_data']->SelectCategory)->get(['TravelMode', 'Content']);

			$data['category2'] = Category::where(['type' => 'localCategory', 'Status' => 'Active'])->whereIn('TravelMode', ['ship', 'air'])->whereIn('Content', $data['trip_data']->SelectCategory)->get(['TravelMode', 'Content']);
		}

		$data['all_state'] = CityStateCountry::where(['type' => 'State', 'Status' => 'Active', 'CountryId' => '56bdbfb4cf32079714ee89ca'])
			->orderBy('Content', 'Asc')
			->get(['_id', 'Content', 'CountryId']);

		$data['category'] = Category::where(['type' => 'localCategory', 'Status' => 'Active', "country_id" => "56bdbfb4cf32079714ee89ca"])->get();

		$data['country'] = CityStateCountry::where(['type' => 'Country', 'Status' => 'Active'])
			->orderBy('Content', 'Asc')
			->get(['_id', 'Content', 'state_available']);

		// $data['category'] = Category::where(['type' => 'localCategory', 'Status' => 'Active'])->whereIn('TravelMode', ['ship', 'air'])->get(['TravelMode', 'Content']);

		$data['state'] = CityStateCountry::where(['type' => 'State', 'Status' => 'Active', 'Content' => @$data['delivery_data']['PickupState']])->first();

		$data['drop_state'] = CityStateCountry::where(['type' => 'State', 'Status' => 'Active', 'Content' => @$data['delivery_data']['DeliveryState']])->first();

		$data['return_state'] = CityStateCountry::where(['type' => 'State', 'Status' => 'Active', 'Content' => @$data['delivery_data']['ReturnStateTitle']])->first();

		$data['nd_return_state'] = CityStateCountry::where(['type' => 'State', 'Status' => 'Active', 'Content' => @$data['delivery_data']['InCaseNotDelReturnState']])->first();

		if ($data['state']) {
			$data['city'] = CityStateCountry::where(['type' => 'city', 'Status' => 'Active', 'Content' => @$data['delivery_data']['PickupCity']])->first();
		}

		if ($data['drop_state']) {
			$data['drop_city'] = CityStateCountry::where(['type' => 'city', 'Status' => 'Active', 'Content' => @$data['delivery_data']['DeliveryCity']])->first();
		}

		if ($data['return_state']) {
			$data['return_city'] = CityStateCountry::where(['type' => 'city', 'Status' => 'Active', 'Content' => @$data['delivery_data']['ReturnCityTitle']])->first();
		}

		if ($data['nd_return_state']) {
			$data['nd_return_city'] = CityStateCountry::where(['type' => 'city', 'Status' => 'Active', 'Content' => @$data['delivery_data']['InCaseNotDelReturnCity']])->first();
		}
		$data['user'] = User::select('_id', 'Name', 'Email')->orderBy('Name', 'asc')->get();
		$data['user'] = User::select('_id', 'Name', 'Email')->orderBy('Name', 'asc')->get();
		if (isset($data['delivery_data']->ProductList)) {
			return view('Admin::localdelivery.edit.edit_local_delivery', $data);
		} else {
			return view('Admin::localdelivery.edit.local_delivery_prepare_request', $data);
		}

	}

	// Region charge
	public function resionCharges($state) {
		//print_r($state['id']); die;
		$charges = Extraregion::where(['state_id' => $state['id'], 'status' => 'Active'])->select('amount')->first();
		if ($charges) {
			return $charges->amount;
		} else {
			return 0.00;
		}
	}

	public function acceptAllProduct($id = '', Request $request) {

		$deliveryInfo = Deliveryrequest::where(array('_id' => $id))->first();
		$array = $deliveryInfo->ProductList;
		$i = 0;
		foreach ($array as $key) {
			if (in_array($key['_id'], Input::get('item_name'))) {

				$array[$i]['status'] = 'reviewed';

				/* Activity Log */
				$insertactivity = [
					'request_id' => $id,
					'request_type' => $deliveryInfo->RequestType,
					'PackageNumber' => $deliveryInfo->PackageNumber,
					'item_id' => $key['_id'],
					'package_id' => $key['package_id'],
					'item_name' => $key['product_name'],
					'log_type' => 'request',
					'message' => 'Product reviewed.',
					'status' => 'reviewed',
					'EnterOn' => new MongoDate(),
				];
				Activitylog::insert($insertactivity);
			}
			$i++;
		}

		$updation = Deliveryrequest::where('_id', '=', $id)
			->update(['ProductList' => $array]);
		Reqhelper::update_status2($id);

		$user_data = User::where(array('_id' => $deliveryInfo->RequesterId))->first();

		if (count($user_data) > 0) {
			$Notification = new Notify();
			$Notification->setValue('title', trans('lang.ONLINE_REVIEWED_TITLE'));
			$Notification->setValue('message', sprintf(trans('lang.ONLINE_REVIEWED_MSG'), $deliveryInfo->ProductTitle));
			$Notification->setValue('type', 'online_request_accepted');
			$Notification->setValue('location', 'online_request_accepted');
			$Notification->setValue('locationkey', $id);

			$Notification->add_user($user_data->NotificationId, $user_data->DeviceType);
			$Notification->fire();

			$insNotification = array(
				array(
					"NotificationTitle" => 'Request reviewed',
					"NotificationShortMessage" => sprintf('Your request for "%s" has been reviewed.', $deliveryInfo->ProductTitle),
					"NotificationMessage" => sprintf('Your request for "%s" has been reviewed.', $deliveryInfo->ProductTitle),
					"NotificationType" => "localdelivery_reviwed",
					"NotificationUserId" => array(new MongoId($user_data->_id)),
					"Date" => new MongoDate(),
					"GroupTo" => "User",
				),
			);

			Notification::Insert($insNotification);

			//Mail to user
			send_mail('57d7d4b37ac6f69c158b4569', [
				"to" => $user_data->Email,
				"replace" => [
					"[USERNAME]" => $user_data->Name,
					"[PACKAGETITLE]" => $deliveryInfo->ProductTitle,
					"[PACKAGEID]" => $deliveryInfo->PackageId,
					"[SOURCE]" => $deliveryInfo->PickupAddress,
					"[DESTINATION]" => $deliveryInfo->DeliveryFullAddress,
					"[PACKAGENUMBER]" => $deliveryInfo->PackageNumber,
					"[SHIPPINGCOST]" => $deliveryInfo->shippingCost,
					"[TOTALCOST]" => $deliveryInfo->TotalCost,
				],
			]
			);
		}
		return Redirect::to('admin/local-delivery-detail/' . $id)->withSuccess('Product accepted successfully.');
	}

	public function receivedAllProduct($id = '', Request $request) {

		$deliveryInfo = Deliveryrequest::where(array('_id' => $id))->first();
		$array = $deliveryInfo->ProductList;
		$i = 0;
		foreach ($array as $key) {
			if (in_array($key['_id'], Input::get('item_name'))) {

				$array[$i]['status'] = 'item_received';

				/* Activity Log */
				$insertactivity = [
					'request_id' => $id,
					'request_type' => $deliveryInfo->RequestType,
					'PackageNumber' => $deliveryInfo->PackageNumber,
					'item_id' => $key['_id'],
					'package_id' => $key['package_id'],
					'item_name' => $key['product_name'],
					'log_type' => 'request',
					'message' => 'Product reviewed.',
					'status' => 'item_received',
					'EnterOn' => new MongoDate(),
				];
				Activitylog::insert($insertactivity);
			}
			$i++;
		}

		$updation = Deliveryrequest::where('_id', '=', $id)
			->update(['ProductList' => $array]);
		Reqhelper::update_status2($id);

		$user_data = User::where(array('_id' => $deliveryInfo->RequesterId))->first();

		if (count($user_data) > 0) {
			$Notification = new Notify();
			$Notification->setValue('title', trans('lang.LOCAL_DELIVERY_RECEIVED'));
			$Notification->setValue('message', sprintf(trans('lang.ONLINE_REVIEWED_MSG'), $deliveryInfo->ProductTitle));
			$Notification->setValue('type', 'online_request_accepted');
			$Notification->setValue('location', 'online_request_accepted');
			$Notification->setValue('locationkey', $id);

			$Notification->add_user($user_data->NotificationId, $user_data->DeviceType);
			$Notification->fire();

			$insNotification = array(
				array(
					"NotificationTitle" => 'Request reviewed',
					"NotificationShortMessage" => sprintf('Your request for "%s" has been reviewed.', $deliveryInfo->ProductTitle),
					"NotificationMessage" => sprintf('Your request for "%s" has been reviewed.', $deliveryInfo->ProductTitle),
					"NotificationType" => "localdelivery_reviwed",
					"NotificationUserId" => array(new MongoId($user_data->_id)),
					"Date" => new MongoDate(),
					"GroupTo" => "User",
				),
			);

			Notification::Insert($insNotification);

			//Mail to user
			send_mail('57d7d4b37ac6f69c158b4569', [
				"to" => $user_data->Email,
				"replace" => [
					"[USERNAME]" => $user_data->Name,
					"[PACKAGETITLE]" => $deliveryInfo->ProductTitle,
					"[PACKAGEID]" => $deliveryInfo->PackageId,
					"[SOURCE]" => $deliveryInfo->PickupAddress,
					"[DESTINATION]" => $deliveryInfo->DeliveryFullAddress,
					"[PACKAGENUMBER]" => $deliveryInfo->PackageNumber,
					"[SHIPPINGCOST]" => $deliveryInfo->shippingCost,
					"[TOTALCOST]" => $deliveryInfo->TotalCost,
				],
			]
			);
		}
		return Redirect::to('admin/local-delivery-detail/' . $id)->withSuccess('Product accepted successfully.');
	}

	public function localassignTp($id, Request $request) {

		$result = Deliveryrequest::where(['_id' => $id])->first();
		$obj = json_decode($request->get('tp_name'));

		if (!empty($obj)) {

			if (count($result) > 0) {
				$userinfo = User::find($result->RequesterId);
				if (count($userinfo) <= 0) {
					$responce['msg'] = 'Requester not found';
					return json_encode($responce);
					die;
				}
			}

			/*dd($result);
				echo $result->PaymentStatus;die;
				if ($result->PaymentStatus != 'yes') {
					if (!empty(trim($result->StripeChargeId))) {
						$res = RSStripe::retrive_amount($result->StripeChargeId);
						if (!isset($res['id'])) {

							$res = RSStripe::find_charge($result->StripeChargeId);
							if ($res['captured'] != 1) {
								$responce['msg'] = $res;
								return json_encode($responce);
								die;
							}

						}
					}
					if (!empty(trim($result->StripeChargeId2))) {
						$res = RSStripe::retrive_amount($result->StripeChargeId2);
						if (!isset($res['id'])) {

							$res = RSStripe::find_charge($result->StripeChargeId2);
							if ($res['captured'] != 1) {
								$responce['msg'] = $res;
								return json_encode($responce);
								die;
							}
						}
					}

					Transaction::insert([
						"SendById" => $result->RequesterId,
						"SendByName" => $result->RequesterName,
						"SendToId" => "aquantuo",
						"RecieveByName" => "Aquantuo",
						"Description" => sprintf("Amount deposited against delivery request for %s, PackageId: %u to %s", $result->ProductTitle, $result->PackageNumber, $result->DeliveryFullAddress
						),
						"Credit" => $result->TotalCost,
						"Debit" => "",
						"Status" => "paid",
						"TransactionType" => "local_delivery",
						"EnterOn" => new MongoDate(),
					]);

					$result->PaymentStatus = 'paid';
					$result->save();
			*/

			$deliveryInfo = Deliveryrequest::where(array('_id' => $request->get('request_id')))->select('ProductList', 'RequesterId', 'PaymentStatus', 'RequesterName', 'ProductTitle', 'PackageNumber', 'DeliveryFullAddress', 'TotalCost', 'Status', 'StripeChargeId', 'StripeChargeId2', 'RequestType')->first();

			$array = $result->ProductList;

			$i = 0;
			foreach ($array as $key) {
				if (in_array($key['_id'], Input::get('item_name'))) {
					$array[$i]['tpid'] = (string) $obj->id;
					$array[$i]['tpName'] = $obj->name;
					$array[$i]['status'] = 'assign';

					/* Activity Log */
					$insertactivity = [
						'request_id' => $request->get('request_id'),
						'request_type' => $result->RequestType,
						'PackageNumber' => $result->PackageNumber,
						'item_id' => $key['_id'],
						'package_id' => $key['package_id'],
						'item_name' => $key['product_name'],
						'tpid' => (string) $obj->id,
						'tpName' => $obj->name,
						'log_type' => 'request',
						'message' => 'Transporter has been assigned.',
						'status' => 'assign',
						'EnterOn' => new MongoDate(),
					];
					Activitylog::insert($insertactivity);
				}
				$i++;
				$product_detail = $key;
			}

			$updation = Deliveryrequest::where('_id', '=', $id)
				->update(['ProductList' => $array]);

			if ($updation) {
				Reqhelper::update_status2($request->get('request_id'));
				$responce = ['success' => 1, 'msg' => 'Transporter has been assigned.', 'count' => 0];
			}

			$user_data = User::where(array('_id' => $deliveryInfo->RequesterId))->first();

			$transporter_data = User::where(array('_id' => $obj->id))->first();
			if (count($user_data) > 0 && count($transporter_data) > 0) {

				$productName = $deliveryInfo->ProductList[0]['product_name'];

				//notification to user
				$Notification = new Notify();
				$Notification->setValue('title', trans('lang.ITEM_ASSIGN_TP_TITLE'));

				$Notification->setValue('message', sprintf(trans('lang.ITEM_ASSIGN_TP_USER'), $productName,
					$deliveryInfo->ProductList[0]['package_id']));

				$Notification->setValue('type', 'BUY_FOR_ME_TRANSPORTER_ASSIGN_FOR_USER');
				$Notification->setValue('location', 'BUY_FOR_ME_TRANSPORTER_ASSIGN_FOR_USER');
				$Notification->setValue('locationkey', $request->get('request_id'));
				$Notification->setValue('itemId', $request->get('product_id'));
				$Notification->add_user($user_data->NotificationId, $user_data->DeviceType);
				$Notification->fire();

				//Mail to user

				/*	send_mail('57daa7fa7ac6f6a3218b4567', [
						"to" => $user_data->Email,
						"replace" => [
							"[USERNAME]" => $user_data->Name,
							"[PACKAGETITLE]" => $productName,
							"[TRANSPORTER]" => $transporter_data->Name,

						],
					]
				*/

				//Mail to transporter
				/*	if ($deliveryInfo->RequestType == 'delivery') {
					$productprice = number_format($deliveryInfo->ProductList[0]['productCost'], 2);
					$insuranceStatus = 'No';
				} else {
					$productprice = number_format($deliveryInfo->ProductList[0]['price'], 2);
					$insuranceStatus = ucfirst($deliveryInfo->ProductList[0]['insurance_status']);
				}

				send_mail('587396736befd9212ec39eba', [
					"to" => $transporter_data->Email,
					"replace" => [
						"[USERNAME]" => $transporter_data->Name,
						"[PACKAGETITLE]" => $productName,
						"[PRICE]" => $productprice,
						"[INSURANCE]" => $insuranceStatus,

					],
				]
				);*/
				//notification to transporter
				$Notification = new Notify();
				$Notification->setValue('title', trans('lang.ITEM_ASSIGN_TP_TITLE'));
				$Notification->setValue('message', sprintf(trans('lang.ITEM_ASSIGN_TP_TRP'), $productName, $deliveryInfo->ProductList[0]['package_id']));

				$Notification->setValue('type', 'BUY_FOR_ME_TRANSPORTER_ASSIGN');
				$Notification->setValue('location', 'BUY_FOR_ME_TRANSPORTER_ASSIGN');
				$Notification->setValue('locationkey', $request->get('request_id'));
				$Notification->setValue('itemId', $request->get('product_id'));
				$Notification->add_user($transporter_data->NotificationId, $transporter_data->DeviceType);
				$Notification->fire();

				$insNotification = array(
					array(
						"NotificationTitle" => trans('lang.ITEM_ASSIGN_TP_TITLE'),
						/*"NotificationShortMessage"    =>'You have assigned transporter for '.$productName.'.',*/

						"NotificationMessage" => sprintf(trans('lang.ITEM_ASSIGN_TP_USER'), $productName,
							$deliveryInfo->ProductList[0]['package_id']),
						"NotificationType" => "BUY_FOR_ME_TRANSPORTER_ASSIGN_FOR_USER.",
						"NotificationUserId" => array(new MongoId($user_data->_id)),
						"Date" => new MongoDate(),
						"GroupTo" => "User",
					),
					array(
						"NotificationTitle" => trans('lang.ITEM_ASSIGN_TP_TITLE'),
						"NotificationMessage" => sprintf(trans('lang.ITEM_ASSIGN_TP_TRP'), $productName,
							$deliveryInfo->ProductList[0]['package_id']),
						"NotificationType" => "BUY_FOR_ME_TRANSPORTER_ASSIGN.",
						"NotificationUserId" => array(new MongoId($transporter_data->_id)),
						"Date" => new MongoDate(),
						"GroupTo" => "User",
					),
				);

				Notification::Insert($insNotification);
			}
		}
		return Redirect::to('admin/local-delivery-detail/' . $id)->withSuccess('All Trnsporter Assign successfully.');
	}

	public function localPackageTraAccept() {
		$responce = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
		if (Input::get('itemid') != '' && Input::get('requestid') != '') {
			$where = [
				'_id' => trim(Input::get('requestid')),
				"ProductList" => ['$elemMatch' => [
					'_id' => Input::get('itemid'),
					'status' => 'purchased',
				]],
			];

			$deliveryInfo = Deliveryrequest::where($where)->first();

			if ($deliveryInfo) {


				$requester = User::where('_id', '=', $deliveryInfo->RequesterId)->first();
				if ($requester) {

					//Calculation
					$configurationdata = Configuration::where(['_id' => '5673e33e6734c4f874685c84'])->first();
					$totalItemCount = count($deliveryInfo->ProductList);
					$averageAreaCharge= number_format($deliveryInfo->AreaCharges / $totalItemCount,2) ;

					foreach ($deliveryInfo->ProductList as $key2 => $value2) {
						if($value2['_id'] == Input::get('itemid')){
							$itemCost = $value2['after_update'] + $averageAreaCharge;
						}
						
					}

					
					$aquantuoFess = ($itemCost / 100 ) * $configurationdata->aquantuo_fees_ld;
					$transporterFess = $itemCost - $aquantuoFess;
					//endCalculation



					$packageid = '';
					$item_title = '';
					$shippingCost = 0;
					$total = 0;

					foreach ($deliveryInfo->ProductList as $key => $value) {
						if ($value['_id'] == trim(Input::get('itemid'))) {
							$packageid = $value['package_id'];
							$item_title = $value['product_name'];
							$shippingCost = $value['shippingCost'];
							$total = $value['total_cost'];
						}
					}
					

					$DeliveryStatus = Deliveryrequest::where($where)
						->update(['ProductList.$.status' => "accepted", 'ProductList.$.tpid' => "admin", 'ProductList.$.tpName' => "admin","ProductList.$.aq_fees"=>$aquantuoFess]);

					if ($DeliveryStatus) {
						Reqhelper::update_status2(Input::get('requestid'));

						$insertactivity = [
							'request_id' => $deliveryInfo->RequesterId,
							'request_type' => $deliveryInfo->RequestType,
							'PackageNumber' => $deliveryInfo->PackageNumber,
							'item_id' => Input::get('itemid'),
							'package_id' => $packageid,
							'item_name' => $item_title,
							'log_type' => 'request',
							'message' => 'item accepted.',
							'status' => 'accepted',
							'action_user_id' => Auth::user()->_id,
							'EnterOn' => new MongoDate(),
						];
						Activitylog::insert($insertactivity);

						$Email = new NewEmail();

						$Email->send_mail('57d7d4b37ac6f69c158b4569', [
							"to" => $requester->Email,
							"replace" => [
								"[USERNAME]" => ucfirst($requester->Name),
								"[PACKAGETITLE]" => ucfirst($item_title),
								"[PACKAGEID]" => $packageid,
								"[SOURCE]" => $deliveryInfo->PickupFullAddress,
								"[DESTINATION]" => $deliveryInfo->DeliveryFullAddress,
								"[PACKAGENUMBER]" => $packageid,
								"[SHIPPINGCOST]" => $shippingCost,
								"[TOTALCOST]" => $total,
							],
						]);

						//Notification to requester

						if ($requester->NoficationStatus == "on" && !empty($requester->NotificationId)) {
							$Notification = new Notify();
							$Notification->setValue('title', trans('lang.SEND_ACCEPT_TITLE'));
							$Notification->setValue('message', sprintf(trans('lang.SEND_ACCEPT_MSG'), 'Aquantuo', $item_title));
							$Notification->setValue('type', 'request_detail');
							$Notification->setValue('locationkey', Input::get('requestId'));
							$Notification->add_user($requester->NotificationId, $requester->DeviceType);
							$Notification->fire();
						}

						

						Notification::Insert([
							array(
								"NotificationTitle" => trans('lang.SEND_ACCEPT_TITLE'),
								"NotificationShortMessage" => 'Request accepted successfully.',
								"NotificationMessage" => sprintf(trans('lang.SEND_ACCEPT_MSG'), "Admin", $item_title),
								"NotificationType" => "request",
								"NotificationUserId" => array(new MongoId($requester->_id)),
								"Date" => new MongoDate(),
								"GroupTo" => "User",
							),
						]);

						$responce = ['success' => 1, 'msg' => 'Product has been updated successfully.'];
					}

				}

			}
		}
		return json_encode($responce);
	}

	public function localTraProcess() {
		$responce = ["success" => 0, "msg" => "Whoops! Something went wrong."];

		if (Input::get("type") != "" && Input::get("itemid") != "" && Input::get("requestid") != "") {
			$res_msg = "Status has been changed successfully.";

			if (Input::get("type") == "out_for_pickup") {
				$old_status = "assign";
				$update_status = "out_for_pickup";
				$requester_email = "5694cb805509251cd67773ec";
				$res_msg = "Request has been picked up.";
			} else if (Input::get("type") == "out_for_delivery") {
				$old_status = "out_for_pickup";
				$update_status = "out_for_delivery";
				$requester_email = "562228e2e4b0252ad07d07a2";
				$res_msg = "Your package is en route to be delivered.";
			} else if (Input::get("type") == "delivered") {
				$old_status = "out_for_delivery";
				$update_status = "delivered";
				$requester_email = "5622492ce4b0d4bc235582bd";
				$res_msg = "Package has been successfully delivered.";
			}

			$where = [
				"_id" => trim(Input::get("requestid")),
				"ProductList" => [
					'$elemMatch' => [
						"_id" => Input::get("itemid"),
						"status" => $old_status
					]
				],
			];

			$deliveryInfo = Deliveryrequest::where($where)->first();

			if ($deliveryInfo) {
				$requester = User::where('_id', '=', $deliveryInfo->RequesterId)->first();
				if ($requester) {
					$packageid = '';
					$item_title = '';
					$shippingCost = 0;
					$total = 0;
					foreach ($deliveryInfo->ProductList as $key => $value) {
						if ($value['_id'] == trim(Input::get('itemid'))) {
							$packageid = $value['package_id'];
							$item_title = $value['product_name'];
							$shippingCost = $value['shippingCost'];
							$total = $value['total_cost'];
						}
					}

					$DeliveryStatus = Deliveryrequest::where($where)->update(['ProductList.$.status' => $update_status]);

					if ($DeliveryStatus) {
						Reqhelper::update_status2(Input::get('requestid'));
						$where2 = [
							'_id' => trim(Input::get('requestid')),
							"ProductList" => ['$elemMatch' => [
								'_id' => Input::get('itemid'),
							]],
						];

						$deliveryRequest = Deliveryrequest::where(['_id' => Input::get('requestid')])->where($where2)->select('_id', 'ProductList.$', 'RequesterId','RequestType','PackageNumber')->first();
						$deliveryRequest->action_user_id = Auth::user()->_id;
						$Activity = new WebActivityLog();
						$Activity->ActivityLog($deliveryRequest);
						// $Email = new NewEmail();

						if ($requester->EmailStatus == "on") {
							send_mail($requester_email, [
								"to" => $requester->Email,
								"replace" => [
									"[USERNAME]" => ucfirst($requester->Name),
									"[PACKAGETITLE]" => ucfirst($deliveryRequest['ProductList'][0]['product_name']),
									"[PACKAGEID]" => $deliveryRequest['ProductList'][0]['package_id'],
									"[SOURCE]" => $deliveryInfo->PickupFullAddress,
									"[DESTINATION]" => $deliveryInfo->DeliveryFullAddress,
									"[PACKAGENUMBER]" => $packageid,
									"[SHIPPINGCOST]" => $shippingCost,
									"[TOTALCOST]" => $total
								],
							]);
						}

						if ($requester->NoficationStatus == "on" && !empty($requester->NotificationId)) {
							$Notification = new Pushnotification();
							if (Input::get('type') == 'out_for_pickup') {
								$Notification->setValue('title',trans('lang.SEND_OUTFORPICKUP_TITLE'));
								$Notification->setValue('message',trans('lang.SEND_OUTFORPICKUP_MSG'));
								$Notification->setValue('type','request_detail');
								$Notification->setValue('locationkey', trim(Input::get('requestid')));
								$Notification->add_user($requester->NotificationId, $requester->DeviceType);
								$Notification->fire();

								Notification::Insert([
									"NotificationTitle" => trans('lang.SEND_OUTFORPICKUP_TITLE'),
									"NotificationShortMessage" => trans('lang.SEND_OUTFORPICKUP_MSG'),
									"NotificationMessage" => trans('lang.SEND_OUTFORPICKUP_MSG'),
									"NotificationType" => "request_detail",
									"NotificationUserId" => array(new MongoId($requester->_id)),
									"Date" => new MongoDate(),
									"GroupTo" => "User",
								]);
							} else if (Input::get('type') == 'out_for_delivery') {
								$Notification->setValue('title',trans('lang.SEND_OUTFORDELIVERY_TITLE'));
								$Notification->setValue('message',sprintf(trans('lang.SEND_OUTFORDELIVERY_MSG'), ucfirst($deliveryRequest['ProductList'][0]['product_name']), $deliveryRequest->PackageId));
								$Notification->setValue('type','request_detail');
								$Notification->setValue('locationkey', trim(Input::get('requestid')));
								$Notification->add_user($requester->NotificationId, $requester->DeviceType);
								$Notification->fire();

								Notification::Insert([
									"NotificationTitle" => trans('lang.SEND_OUTFORDELIVERY_TITLE'),
									"NotificationShortMessage" => sprintf(trans('lang.SEND_OUTFORDELIVERY_MSG'), ucfirst($deliveryRequest['ProductList'][0]['product_name']), $deliveryRequest->PackageId),
									"NotificationMessage" => sprintf(trans('lang.SEND_OUTFORDELIVERY_MSG'), ucfirst($deliveryRequest['ProductList'][0]['product_name']), $deliveryRequest->PackageId),
									"NotificationType" => "request_detail",
									"NotificationUserId" => array(new MongoId($requester->_id)),
									"Date" => new MongoDate(),
									"GroupTo" => "User",
								]);
							} else if (Input::get('type') == 'delivered') {
								$Notification->setValue('title',trans('lang.REQUEST_DELIVERED'));
								$Notification->setValue('message',sprintf(trans('lang.MSG_REQUEST_DELIVERED'), "Aquantuo", ucfirst($deliveryRequest['ProductList'][0]['product_name'])));
								$Notification->setValue('type','request_detail');
								$Notification->setValue('locationkey',trim(Input::get('requestid')));
								$Notification->add_user($requester->NotificationId, $requester->DeviceType);
								$Notification->fire();

								Notification::Insert([
									"NotificationTitle" => trans('lang.REQUEST_DELIVERED'),
									"NotificationShortMessage" => sprintf(trans('lang.MSG_REQUEST_DELIVERED'), "Aquantuo", ucfirst($deliveryRequest['ProductList'][0]['product_name'])),
									"NotificationMessage" => sprintf(trans('lang.MSG_REQUEST_DELIVERED'), "Aquantuo", ucfirst($deliveryRequest['ProductList'][0]['product_name'])),
									"NotificationType" => "request_detail",
									"NotificationUserId" => array(new MongoId($requester->_id)),
									"Date" => new MongoDate(),
									"GroupTo" => "User",
								]);
							}
						}
						$responce = ['success' => 1, 'msg' => $res_msg];
					}
				}
			}
		}
		return json_encode($responce);
	}
}
