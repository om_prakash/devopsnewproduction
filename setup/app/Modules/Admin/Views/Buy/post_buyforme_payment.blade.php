@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')
@section('content')

{!! Html::script('theme/admin/custome/js/validation.js') !!}

<div class="container">
   <h3 class="text-primary">Buy For Me Transaction</h3>

   <div class="box-shadow" style="padding-top: 15px; padding-bottom: 10px">

         <div class="col-sm-4">
        <label>Package Id: </label>
         {{@$delivery->PackageNumber}}
       </div>

        <div class="col-sm-4">
        <label>Requester Name: </label>
        {{@$delivery->RequesterName}}
       </div>

        <div class="col-sm-4">
        <label>Product Title: </label>
         {{@$delivery->ProductTitle}}
       </div>
   </div>

   <div class="box-shadow">
   <br><br>
  {!! Form::model('', ['name' => 'buy_for_me_payment',  'url' => 'admin/buyforme/payment', 'id' => 'buy_for_me_payment', 'method' => 'POST']) !!}
  <div class="col-sm-8">
  <p>Please fill the transaction id and description if you have or <a href="{{ url('admin/buy-for-me/detail/'.Request::segment(4)) }}">Skip</a>.</p>
  <div class="form-group">
    <label for="transaction">Transaction ID</label>
    <input type="text" class="form-control required" id="transaction" name="transaction" placeholder="Transaction Id">
    <p class="help-block red" style="color:red;"  >{{$errors->first('transaction')}} </p>
  </div>
  </div>
  <div class="col-sm-8">
  <div class="form-group">

    <label for="description">Description</label>
    <textarea row="5" class="form-control required" id="description" name="description" placeholder="Description"> </textarea>
    <p class="help-block red" style="color:red;"  >{{$errors->first('description')}} </p>
  </div>
  </div>
  <div class="col-sm-8">
  <input type="hidden" value="{{ Request::segment(4) }}"  name="url_id">
  <button type="submit" class="btn btn-primary">Submit</button>
  <a href="{{ url('admin/buy-for-me/detail/'.Request::segment(4)) }}" class="btn btn-primary">Skip</a>

   <a href="{{ url('admin/pay-via-card/'.Request::segment(4)) }}?promocode={{@Input::get('promocode')}}" class="btn btn-primary">pay via card</a>

  <a href="{{ url('admin/mark-paid/'.Request::segment(4)) }}" class="btn btn-primary" onclick="return confirm('Are you sure you want to mark as paid.');">Mark as paid</a>

   <a href="{{ url('admin/pay-mobile-money/'.Request::segment(4)) }}" class="btn btn-primary" >Pay by Mobile Money</a>

  </div>
  <div class="clearfix"></div>
 {!! Form::close() !!}
</div>
</div>
@include('Admin::layouts.footer')

<script>
new Validate({
   FormName :  'buy_for_me_payment',
   ErrorLevel : 1,

    });
</script>
@endsection
