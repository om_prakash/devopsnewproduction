@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')
@section('content')


<div class="container">
	<div class="row">
    <div class="col-xs-6 col-sm-6">
		<h2 class="color-blue mainHeading" style="text-align:left;">Card List</h2>
       </div>
       <div class="col-xs-6 col-sm-6">
        <small class="pull-right color-blue mainHeading" style="text-align:right;">
        <div class="clearfix"></div>
		 <a class="btn btn-primary" href="{{url('admin/online_package/payment')}}/{{Request::segment(3)}}">
		 <i class="fa fa-arrow-left"> Back</i></a>
		</small>
        </div>
        </div>
       {{--  <br/>
        <input type="checkbox" value='' name="check"  id="exist" >&nbsp;
        <small>
        {{$admin_msg}}
		</small>
		<br/> --}}<br/>

	<div class="clearfix"></div>
		<span id="card_msg"></span>
		<div class="row">

			<div class="col-md-12 col-sm-12 col-xs-12">

   <div class="box-shadow" style="padding-top: 15px; padding-bottom: 10px">
		    <div class="col-sm-4">
		    <label>Package Id: </label>
		     {{@$delivery->PackageNumber}}
		   </div>

		   <div class="col-sm-4">
		    <label>Requester Name: </label>
		    {{@$delivery->RequesterName}}
		   </div>

		    <div class="col-sm-4">
		    <label>Product Title: </label>
		     {{@$delivery->ProductTitle}}
		   </div>
   </div>
				<div class="box-shadow clearfix">
				<br/>
{{--
				<div class="col-md-12 col-sm-6 col-xs-12" >
                <a href="{{url('admin/pay-mobile-money/'.Request::segment(3))}}"> Pay by Mobile Money</a>
                 </div>
                 <div class="clearfix"> </div> --}}


				<input type="hidden" name="test" value="0" id="test"/>
				@if(isset($cards['data'])) @if(count($cards['data']) > 0)
					@foreach($cards['data'] as $key)
					<div class="col-md-4 col-sm-6 col-xs-12" id="card_{{$key['id']}}">
						<div class="card-wrapper">
							<div class="custom-btn">
	                         <a class="btn" href="{{url('admin/online-pay-card/'.Request::segment(3))}}/{{$key['id']}}?promocode={{Input::get('promocode')}}" onclick="return show_btn()">PAY NOW</a>

							</div>
	                       <a class="deletebtn" title="Delete card" onclick="return confirm('Are you sure you want to delete this card?')" href="{{url('admin/delete-card/'.Request::segment(3).'/'.@$key['id'])}}"><i class="fa fa-trash"></i></a>

								<div class="media">
									<div class="media-left">
										<a href="#">
											@if($key['brand'] == 'Visa')
											<img height="30px" width="50px" src="{{ImageUrl}}/webcard/visa.png">
											@elseif($key['brand'] == 'Diners Club')
											<img height="30px" width="50px" src="{{ImageUrl}}/webcard/dinsers.jpeg">
											@elseif($key['brand'] == 'Discover')
											<img height="30px" width="50px" src="{{ImageUrl}}/webcard/discover.jpeg">
											@elseif($key['brand'] == 'MasterCard')
											<img height="30px" width="50px" src="{{ImageUrl}}/webcard/mastercard.jpeg">
											@elseif($key['brand'] == 'American Express')
											<img height="30px" width="50px" src="{{ImageUrl}}/webcard/ae.jpeg">
											@elseif($key['brand'] == 'JCB')
											<img height="30px" width="50px" src="{{ImageUrl}}/webcard/jcb.jpeg">
											@endif
										</a>
									</div>
									<div class="media-body">
										<p class="media-heading color-blue" style="font-weight:bold">xxxx-xxxx-xxxx-{{$key['last4']}}</p>
										<p>Pricing may change if the actual item weight was not entered.</p>

									</div>
								</div>
							</div>
						</div>
					@endforeach @else
							<div class="col-md-12">
								<!-- <div class="panel panel-default">
								<center>
								<br>
									<p class="text-center" >No card added yet.</p>
								</center>
								<br>
								</div> -->
							</div>
						  @endif

				@endif

				<div class="col-md-4 col-sm-6 col-xs-12">
                 	<div class="card-wrapper gray-card">
						<a data-whatever="@mdo" data-target="#modal2" data-toggle="modal" href="" ><span><i class="fa fa-plus"></i>Add card</span></a>
                   </div>
				</div>
			</div>
		</div>

		<div class="col-md-12 col-sm-6 col-xs-12 text-center" >
   			 <a class="btn btn-lg btn-primary" href="{{url('admin/pay-mobile-money/'.Request::segment(3))}}"><i class="fa fa-mobile" style="font-size:26px; margin-right:10px"></i> Pay by Mobile Money</a>
   			 <br/><br/>
        </div>

    </div>
</div>

@include('Admin::layouts.footer')
@endsection
<!--card modal-->
	<div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title color-white" id="myModalLabel">&nbsp;&nbsp;&nbsp;Add Card</h4>
          </div><div class="clearfix"></div>
          {!! Form::model('', ['name' => 'Addcard', 'id' => 'Addcard', 'method' => 'POST','onsubmit' => 'return validateForm()', 'url' => 'admin/add-card']) !!}
          <input type="hidden" name="requestid" value="{{Request::segment(3)}}">
          <div class="modal-body">

          		<div class=col-md-12 id="succesdiv"></div>
				<div id='errordiv' class="col-sm-12"></div>

			<div class="form-group clearfix">
				<div class="col-sm-12">

					{!! Form::label('name_on_card', 'Card Holder Name',['class'=>'color-black']) !!}
					{!! Form::text('name_on_card', '', ['class'=>'form-control required','maxlength'=>'25' ,'placeholder'=> 'Card Holder Name','id' =>'name_card']) !!}
					<p class="help-block red" id='er_name_card'></p>
				</div>
			</div>


			<div class="form-group clearfix">
				<div class="col-sm-12">
				   {!! Form::label('credit_card_number', 'Card Number',['class'=>'color-black']) !!}
					  {!! Form::text('credit_card_number', '', ['class'=>'form-control required numeric','maxlength'=>'16	' , 'placeholder'=> 'Card Number','id' =>'credit_card_number','maxlength'=>'16']) !!}
					  <p class="help-block red" id='er_credit_card_number'></p>
				</div>
			</div>

			<div class="">
            <div class="col-sm-12">

               <div class="row">
                <div class="col-sm-6">
                <div class="form-group">
                	{!! Form::label('expiration_date', 'Exp. Month',['class'=>'color-black']) !!}
                 {!! Form::select('month', [''=>'Exp. Month','1'=>'01','2'=>'02','3'=>'03','4'=>'04','5'=>'05','6'=>'06','7'=>'07','8'=>'08','9'=>'09','10'=>'10','11'=>'11','12'=>'12'],Input::get('month'),['class'=>'form-control required numeric usename-#month#"','id'=>'month']) !!}
                 <p class="help-block red" id='er_month'></p>
                </div>
                </div>

                 <div class="col-sm-6">
                 <div class="form-group">
                 {!! Form::label('year', 'Year ',['class'=>'color-black']) !!}
                 {{ Form::selectRange('year', date('Y'), date('Y',strtotime('+15 year')), Input::get('year'), ['class'=>'form-control required numeric usename-#year#"', 'id'=>"year",'placeholder'=>'Year']) }}
                 <p class="help-block red" id='er_year'></p>
                </div>
                </div>
                </div>
            </div>
            </div>

            <div class="form-group">
				<div class="col-sm-12">
				   {!! Form::label('security_code', 'CVV',['class'=>'color-black']) !!}

					{!! Form::password('security_code',['class'=>'form-control required numeric usename-#CVV#', 'placeholder'=> 'CVV', 'maxlenght'=>'4', 'id' =>'security_code','maxlength'=>'4']) !!}

					  <p class="help-block red" id='er_security_code'></p>

				</div>

			</div>


          </div><div class="clearfix"></div>
          <br />
			<div class="modal-footer">
			  <div class="col-sm-12 text-right">
				{!! Form::button('Close', ['class' => 'btn custom-btn1', 'data-dismiss'=>'modal']) !!}
				{!! Form::button('Submit', ['class' => 'btn custom-btn1','id'=>'addbutton','type'=>'submit']) !!}
			  </div>
			</div>

			{!! Form::close() !!}
        </div>
      </div>
    </div>

<script>
function show_btn()
{
	checked =$('#exist').prop('checked'); // true
	//checked = $("input[type=checkbox]:checked").length;

	if(checked == 0){
		alert('Please accept agreement.');
		 flage=false;
	}else if(checked == 1)
	{
		flage=true;
	}

	return flage;
}

function validateForm() {

    var flag = true;

    try
    {
        if(document.getElementById('name_card').value == '') {
            document.getElementById('er_name_card').innerHTML = 'The card holder name field is required';
            flag = false;
        }
        if (document.getElementById('credit_card_number').value == '') {
             document.getElementById('er_credit_card_number').innerHTML = 'The credit card number field is required';
             flag = false;
             }
             if (document.getElementById('month').value == '') {
             document.getElementById('er_month').innerHTML = 'The month field is required';
             flag = false;
             }
             if (document.getElementById('year').value == '') {
             document.getElementById('er_year').innerHTML = 'The year field is required';
             flag = false;
             }
            if (document.getElementById('security_code').value == '') {
             document.getElementById('er_security_code').innerHTML = 'The CVV field is required';
             flag = false;
             }

    }catch(E) {
        console.log(E);
    }
   return flag;
}


</script>
