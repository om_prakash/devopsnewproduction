@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')

@section('content')
{!! Html::style('theme/admin/choose-multiple/bootstrap-chosen.css') !!}
{!! Html::script('theme/admin/choose-multiple/chosen.jquery.js') !!}
{!! Html::style('theme/admin/custome/css/style.min.css') !!}


<div class="modal fade" id="item_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title color-white" id="myModalLabel">&nbsp;&nbsp;Edit Item</h4>
         </div>
         <div class="clearfix"></div>
         {!! Form::model('', ['name' => 'edit_item', 'id' =>'edit_item', 'method' => 'post']) !!}
          <input type="hidden" value="{{Request::segment(4)}}" name="request_id">
         <div class="modal-body">
            <div class="row">
               <div class="col-sm-12">
                  <div class="form-group">
                     <label class="control-label">Item Name</label>
                     <input type="hidden" value="" name="item_id" id="item_id">
                     <input class="form-control required"   placeholder="Item Name"  name="item_name" id="item_name" maxlength="110" >
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-sm-12">
                  <div class="form-group">
                     <label class="control-label">Purchased From</label>
                     <input type="text" class="form-control required username-#purchase from#"  name="purchase_from"  id="add_item_url" placeholder="Purchase From" maxlength="700">
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-sm-6">
                  <div class="form-group">
                     <label class="control-label">Item Cost</label>
                     <input class="form-control required float maxlength-9 " placeholder="$" name="item_price" id="item_price" maxlength="9">
                  </div>
               </div>
               <div class="col-sm-6">
                  <div class="form-group">
                     <label class="control-label">Quantity</label>
                     <div style="width:160px;" class="input-group number-spinner">
                        <span class="input-group-btn data-dwn">
                        <button class="btn btn-primary" data-dir="dwn" onclick="return false;">
                        <span class="glyphicon glyphicon-minus"></span>
                        </button>
                        </span>
                        <input type="text" class="form-control text-center" style="height:37px;" value="1" min="1" max="40"  id="quentity" name="quentity" >
                        <input type="hidden" value="">
                        <span class="input-group-btn data-up">
                        <button class="btn btn-primary" data-dir="up" onclick="return false;">
                        <span class="glyphicon glyphicon-plus"></span>
                        </button>
                        </span>
                     </div>
                  </div>
               </div>
            </div>

            <div class="col-sm-12 col-xs-12 row">
               <div class="form-group">

                  <div class="">
                    <span class="pull-left">
                    <input name="buyforme_dimensions" id="buyforme_dimensions" value="buy_for_me_dimensions" type="checkbox">
                    </span>
                    <span class="Insurance_check">
                    <p>
                     &nbsp;&nbsp;I know item dimensions and weight
                     </p>
                     </span>

                  </div>
               </div>
            </div>
            <div class="row" id="dimention_div">
              <div class="col-sm-12">
                  <div class="form-group">
                     <label class="control-label">Measurement Units</label>
                     <div class="radio">
                        <label class="">
                        <input type="radio" name="measurement_unit" id="edit_measurement_unit" value="cm_kg">
                        </label>&nbsp;Metric (Cm/Kg)
                        <label class="">
                        <input type="radio" name="measurement_unit" id="edit_measurement_unit2" value="inches_lbs" checked >
                        </label>&nbsp;Imperial (Inches/lbs)
                     </div>
                  </div>
               </div>
              <div class="col-sm-12">
                <label class="control-label">Item Specification</label>
              </div>
               <div class="col-sm-3">
                  <div class="form-group">
                     <label class="control-label">Length</label>
                     <input class="form-control required  float maxlength-9" placeholder="Length"  id="length" name="length" maxlength="9" />
                  </div>
               </div>
               <div class="col-sm-3">
                  <div class="form-group">
                     <label class="control-label">Width</label>
                     <input class="form-control required float maxlength-9" placeholder="Width" name="width"  id="width" maxlength="9">
                  </div>
               </div>
               <div class="col-sm-3">
                  <div class="form-group">
                     <label class="control-label">Height</label>
                     <input class="form-control required float maxlength-9" placeholder="Height" name="height" id="height" maxlength="9">
                  </div>
               </div>
               <div class="col-sm-3">
                  <div class="form-group">
                     <label class="control-label">Weight</label>
                     <input class="form-control required float maxlength-9" placeholder="weight" name="weight" id="weight" maxlength="9">
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-sm-6">
                  <div class="form-group">
                     <label class="control-label">Shipping Mode</label>
                     <div class="radio">
                        <label class="">
                        <input type="radio" name="travel_mode" id="travel_mode_air" value="air" onclick="toggle_category('.travel-mode-air','.travel-mode-ship','#package_category')" checked="checked">
                        </label>&nbsp;By Air
                        <label class="">
                        <input type="radio" name="travel_mode" id="travel_mode_ship" value="ship" onclick="toggle_category('.travel-mode-ship','.travel-mode-air','#package_category')">
                        </label>&nbsp;By Sea
                     </div>
                  </div>
               </div>
                <div class="col-sm-6">
                  <div class="form-group">
                     <label class="control-label">Insurance</label>
                     <div class="radio">
                        <label>
                        <input type="radio" name="insurance" id="insurance" value="yes" checked>
                        </label>&nbsp; Yes
                        &nbsp; &nbsp;
                        <label>
                        <input type="radio" name="insurance"  id="insurance2" value="no">
                        </label> &nbsp;<span style="">No</span>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-sm-6">
               <div class="row">
               <div class="col-sm-12">
                  <div class="form-group">
                     <label class="control-label">Select Package Category</label>
                     <select class="form-control required" id="package_category" name="category">
                        <option value="" >Select Category</option>
                        <?php foreach ($category as $key) {?>
                        <option  value='{"id":"{{$key->_id}}"}' class="travel-mode-{{$key->TravelMode}}" >{{$key->Content}}</option>
                        <?php }?>
                     </select>
                  </div>
               </div>
               <div class="col-sm-12">
                  <div class="form-group">
                     <label class="control-label">Description/Instructions</label>
                     <textarea class="form-control required" name="description" rows="3" id="description" maxlength="500" ></textarea>
                  </div>
               </div>
            </div>
            </div>
               <div class="col-sm-6">
                   <div class="selected-pic">
                        <div>
                           <a class="fancybox" rel="group" href="" >
                           <img id="online_purchase_preview" name="item_image" src=""  width="200px" height="150px" />
                          </a>
                        </div>
                        <br>
                        <label class="custom-input-file">
                        {!! Form::file('item_image', ['class'=> 'custom-input-file','id'=>'senior_image','onchange'=>"image_preview(this,'online_purchase_preview',event)"]) !!}
                        </label>
                     </div>
                  </div>
            </div>
         </div>
         <input type="hidden" value="" name="address_id" id="address_id">
         <div class="clearfix"></div>
         <div class="modal-footer">

            {!! Form::button('Update', ['class' => 'btn custom-btn1 pull-left','id'=>'item_loader','type'=>'submit']) !!} &nbsp;
            {!! Form::button('Close', ['class' => 'btn custom-btn1 pull-left', 'data-dismiss'=>'modal' ,'id' => 'edit_modal']) !!}
         </div>
         {!! Form::close() !!}
      </div>
   </div>
</div>

<div class="container">
<div class="row">
<div class="col-sm-12">
   <h2 class="color-blue">Online Purchases</h2>
   <br />
</div>
<div class="col-sm-12" id="sec1">
  <div class="box-shadow">
      <h3>Please complete the following fields</h3>
      <hr />
      <!------------------step1------------------>
      <div class="row">
        <div class="col-sm-12">
          <div class="step_box three_step clearfix">
            <div class="step first selected">
                  <div>1</div>
                  <p class="text-center colol-black">Package Details</p>
            </div>
            <div class="step inner" id="step2-header">
                  <div>2</div>
                  <p class="text-center colol-black">Drop Off Address</p>
            </div>
            <div class="step last " id="step3-header">
                  <div>3</div>
                  <p class="text-center colol-black">Payment</p>
            </div>
          </div>
        </div>

        <div class="col-sm-10 col-sm-offset-1">
        </div>
        <div id="table_hide" style="@if(count($additem) <= 0) display:none; @endif">
            <div class="col-md-10 col-sm-offset-1">
               <div class="col-sm-12 text-right">
                  <button class="btn btn-primary"  onclick="$('#add_item').show();$('#table_hide').hide();"  id="add_item_button">Add Item</button>
                  @if(Input::get('listing') != 'own')
                  <a class="btn btn-primary"  id="" href="{{url('admin/on-line-purchases')}}?listing=own">Create Own Listing</a>
                  @endif
                  @if(Input::get('listing') == 'own')
                  <a class="btn btn-primary"  id="" href="{{url('admin/on-line-purchases')}}?listing=all">Listing of All</a>
                  @endif
                  </br>
                  </br>
               </div>
            </div>

            <div class="col-md-10 col-sm-offset-1">
                <div class="col-sm-12"  >
                  <table class="table table-bordered table-striped table-highlight table-list" name="item_list" id="item_list">
                     <thead>
                        <tr class="heading">
                           <th>S.No. </th>
                           <th>Item Name</th>
                           <th>Item Cost</th>
                           <th>Item Weight</th>
                           <th>Quantity</th>
                           <th>Description</th>
                           <th>Action</th>
                        </tr>
                     </thead>
                     <tbody>
                           <?php $sno = 1;?>
                        @foreach($additem as $value)
                        <?php
$weight_unit = 'lbs';
if ($value->measurement_unit == 'cm_kg') {
	$weight_unit = 'kg';
}
?>
                        <tr id="row-{{$value->_id}}">
                           <td>{{$sno++}}</td>
                           <td>{{ucfirst($value->item_name)}}</td>
                           <td>${{number_format((float)$value->item_price,2)}}</td>
                           <td>{{ucfirst($value->weight)}}&nbsp;{{ucfirst($weight_unit)}}</td>
                           <td>{{ucfirst($value->quentity)}}&nbsp;</td>
                           <td>{{ucfirst($value->description)}}</td>
                           <td colspan="2" >&nbsp;&nbsp;
                              <span><a type=""  data-toggle="modal" onclick="get_item_info('{{$value['_id']}}')" id="Edit" title="Edit" data-whatever="@mdo" href="#item_modal"><img src="http://aquantuo.com/upload/edit.svg"  width="20px" height="25px" ></a></span>&nbsp;&nbsp;
                              <a title="delete" id="Delete"  onclick="remove_record('admin/delete_item/{{$value->_id}}/DeleteItem','{{$value->_id}}')" href="javascript:void(0)"><img src="http://aquantuo.com/upload/delete.png"  width="20px" height="25px"></a>
                           </td>
                        </tr>
                        @endforeach
                        </tr>
                     </tbody>
                  </table>
                  <div class="">
                     <div class="">
                        <div class="form-group">
                           <button class="custom-btn1 btn"  onclick="$('#sec1').hide();$('#sec2').show();">
                           Next
                           	<div class="custom-btn-h"></div>
                           </button>
                        </div>
                     </div>
                  </div>
                </div>
            </div>
        </div>

        <div class="col-md-10 col-sm-offset-1" style="@if(count($additem) > 0) display:none; @endif" id="add_item" >
            {!! Form::open(['class'=>'form-vertical','name'=>'buy_for_me_form_add','id' => 'buy_for_me_form_add','file' => true ]) !!}
               <input type="hidden" value="" name="item_id" id="item_id">
               <input type="hidden" value="add" name="form_type" id="form_type">

              <div class="col-sm-6 col-xs-12">
                 <div class="form-group">
                    <label class="control-label">Item Name</label><span class="red-star"> *</span>
                    <input class="form-control required"   placeholder="Item Name"  name="item_name" id="item_name" maxlength="110" >
                 </div>
              </div>

              <div class="col-sm-6 col-xs-12">
                 <div class="form-group">
                    <label class="control-label">Purchased From</label><span class="red-star"> *</span>
                    <input type="text" class="form-control required username-#purchase from "  name="purchase_from"  id="add_item_url" placeholder="Purchase From" maxlength="700">
                 </div>
              </div>

              <div class="col-sm-6">
                 <div class="form-group">
                    <label class="control-label">Item Cost</label><span class="red-star"> *</span>
                    <div class="input-group error-input">
                     <span class="input-group-addon">$</span>
                    <input class="form-control required float maxlength-9 " placeholder="Item Cost" name="item_price" id="item_price" maxlength="9">
                    </div>
                 </div>
              </div>

              <div class="col-sm-6 col-xs-12">
                 <div class="form-group">
                    <label class="control-label">Quantity</label>
                    <div style="width:160px;" class="input-group number-spinner">
                       <span class="input-group-btn data-dwn">
                       <button class="btn btn-primary" data-dir="dwn" onclick="return false;">
                       <span class="glyphicon glyphicon-minus"></span>
                       </button>
                       </span>
                       <input type="text" class="form-control text-center" style="height:38px;" value="1" min="1" max="40"  id="quentity" name="quentity" >
                       <input type="hidden" value="">
                       <span class="input-group-btn data-up">
                       <button class="btn btn-primary" data-dir="up" onclick="return false;">
                       <span class="glyphicon glyphicon-plus"></span>
                       </button>
                       </span>
                    </div>
                 </div>
              </div>

              <div class="col-sm-12 col-xs-12">
                  <div class="form-group">
                    <div class="">
                        <span class="pull-left">
                        <input name="buy_for_me_dimensions" id="buy_for_me_dimensions"  value="buy_for_me_dimensions" type="checkbox">
                        </span>
                        <span class="Insurance_check">
                        <p>
                         &nbsp;&nbsp;I know item dimensions and weight
                         </p>
                         </span>

                    </div>
                  </div>

                 <div id="measurment" style="display:none;" >
                  <div class="col-sm-12 col-xs-12 row">
                     <div class="form-group">
                        <label class="control-label">Measurement Units</label>
                        <div class="radio">
                           <label class="">
                           <input type="radio" name="measurement_unit" id="measurement_unit"  value="cm_kg">
                           </label>&nbsp;Metric (Cm/Kg)
                           <label class="">
                           <input type="radio" name="measurement_unit" id="measurement_unit2" value="inches_lbs" checked >
                           </label>&nbsp;Imperial (Inches/lbs)
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-12 col-xs-12 row">
                     <label class="control-label">Item Specification</label>
                  </div>
                  <div class="col-sm-3 row">
                     <div class="form-group">
                        <label class="control-label">Length</label><span class="red-star"> *</span>
                        <input class="form-control required  float maxlength-9" placeholder="Length"  id="length" name="length" maxlength="9" />
                     </div>
                  </div>
                  <div class="col-sm-3">
                     <div class="form-group">
                        <label class="control-label">Width</label><span class="red-star"> *</span>
                        <input class="form-control required float maxlength-9" placeholder="Width" name="width"  id="width" maxlength="9">
                     </div>
                  </div>
                  <div class="col-sm-3">
                     <div class="form-group">
                        <label class="control-label">Height</label><span class="red-star"> *</span>
                        <input class="form-control required float maxlength-9" placeholder="Height" name="height" id="height" maxlength="9">
                     </div>
                  </div>
                  <div class="col-sm-3">
                     <div class="form-group">
                        <label class="control-label">Weight</label><span class="red-star"> *</span>
                        <input class="form-control required float maxlength-9" placeholder="weight" name="weight" id="weight" maxlength="9">
                     </div>
                  </div>
              </div>


              <div class="col-sm-12 row">
                 <div class="form-group">
                    <label class="control-label">Shipping Mode</label>
                    <div class="radio">
                       <label class="">
                       <input type="radio" name="travel_mode" id="travel_mode_air" value="air" onclick="toggle_category('.travel-mode-air','.travel-mode-ship','#package_category_add')" checked="checked">
                       </label>&nbsp;By Air
                       <label class="">
                       <input type="radio" name="travel_mode" id="travel_mode_ship" value="ship" onclick="toggle_category('.travel-mode-ship','.travel-mode-air','#package_category_add')">
                       </label>&nbsp;By Sea
                    </div>
                 </div>
              </div>

              <div class="col-sm-12 row">
                 <div class="form-group">
                    <label class="control-label">Select Package Category</label><span class="red-star"> *</span>
                    <select class="form-control required" id="package_category_add" name="category">
                       <option value="">Select Category</option>
                       <?php foreach ($category as $key) {?>
                       <option value='{"id":"{{$key->_id}}"}' class="travel-mode-{{$key->TravelMode}}">{{$key->Content}}</option>
                       <?php }?>
                    </select>
                 </div>
              </div>

              <div class="col-sm-12 row">
                 <div class="form-group">
                    <label class="control-label">Package Description / Instructions</label><span class="red-star"> *</span>
                    <textarea class="form-control required" name="description" rows="3" id="description" maxlength="500" ></textarea>
                 </div>
              </div>

              <div class="col-sm-6 row">
                   <div class="form-group">
                    <label class="control-label">Insurance</label>
                    <div class="radio">
                       <label>
                       <input type="radio" name="insurance" id="insurance" value="yes" checked>
                       </label>&nbsp;Yes
                       &nbsp; &nbsp;
                       <label>
                       <input type="radio" name="insurance"  id="insurance2" value="no">
                       </label>&nbsp; No
                    </div>
                    </div>
              </div>

              <div class="col-sm-6">
                 <div class="form-group">
                    <label class="control-label">Item Image</label>
                    <input type="file" name="item_image" id="item_image" class="valid-filetype-jpg,png,gif,jpeg" />
                 </div>
              </div>


                 <div class="">
                    <hr />
                 </div>

                   <div class="">
               <div class="row">
                  <div class="col-xs-6">
                     <div class="form-group" id="add_item_loader">
                        <a  class="custom-btn1 btn"  onclick="$('#add_item').hide();$('#table_hide').show();"   >Back
                        <div class="custom-btn-h"></div>
                        </a>

                        <button type="submit"  id="add_item_loader1" name="add_item" class="custom-btn1 btn">Add
                        <div class="custom-btn-h"></div>
                        </button>

                     </div>
                  </div>
               </div>
            </div>
              </div>
            {!! Form::close() !!}

         </div>
      </div>
   </div>
</div>
<!------------------step1------------------>
<!------------------step2------------------>


{!! Form::open(['class'=>'form-vertical','name'=>'buy_for_me_form','id' => 'buy_for_me_form' ]) !!}
<input type="hidden" name="discount" id="olp_discount" value="0">
<input type="hidden" value="{{Input::get('listing')}}" name="listing" id="">
<div class="col-sm-12" id="sec2" style="display:none;">
   <div class="box-shadow">
      <h3>Please complete the following fields</h3>
      <hr />
      <!------------------step1------------------>
      <div class="step_box three_step clearfix">
         <div class="step first selected">
            <div>1</div>
            <p class="text-center colol-black">Package Details</p>
         </div>
         <div class="step inner selected">
            <div>2</div>
            <p class="text-center colol-black">Drop Off Address</p>
         </div>
         <div class="step last ">
            <div>3</div>
            <p class="text-center colol-black">Payment</p>
         </div>
      </div>

      <div class="row">
         <div class="col-md-10 col-sm-offset-1">

            <h2 class="col-sm-12 text-primary row" >Customer Detail</h2>
            <div class="row">
              <div class="col-sm-6">
                  <div class="form-group">
                     <label class="control-label">User</label>
                     <div class="radio">
                        <label class="">
                        <input type="radio" name="travel_mode" id="existing" value="air"  checked="checked">
                        </label>&nbsp;Existing
                        <label class="">
                        <input type="radio" name="travel_mode" id="newuser" value="ship" >
                        </label>&nbsp;New
                     </div>
                  </div>
               </div>
            </div>

             {{--  <div class="col-sm-12 row">
                  <div class="form-group">
                     <label class="control-label">Filter</label>
                     <input type="text" name="search" class="form-control"  id="search_id" placeholder="Search by name">
                  </div>
              </div> --}}

              <script type="text/javascript">
                $("#search_id").keyup(function(){
                    var length = $("#search_id").val().length;
                    if(length >2){
                      $('#loading').show();
                      $.ajax({
                        url: SITEURL + 'admin/user-list',
                        type: 'Get',
                        data: "value="+$("#search_id").val(),
                        success: function(obj) {
                          $('#loading').hide();
                          $("#user").html(obj);

                        }
                      });
                    }
                });
              </script>

              <div class="col-sm-12 row" id="Existing_user">
                  <div class="form-group">
                     <label class="control-label">Select user</label><span class="red-star"> *</span>
                     <select class="form-control required chosen-select" id="user" name="user" >
                        <option value="" >Select User</option>
                        <?php foreach ($user as $key) {?>
                        <option  value='{"id":"{{$key->_id}}","name":"{{$key->Name}}","email":"{{$key->Email}}"}' class="travel-mode-{{$key->TravelMode}}" >{{$key->Name}} ({{$key->Email}})</option>
                        <?php }?>
                     </select>
                  </div>
              </div>

               <div class="row" id="name" style="display:none;">
                 <div class="col-sm-6">
                  <div class="form-group">
                     <label>First Name</label><span class="red-star"> *</span>
                     <input type="text" placeholder="First Name" name="first_name" id="first_name"  maxlength= "125" class="form-control required">
                  </div>
               </div>

                 <div class="col-sm-6">
                  <div class="form-group">
                     <label>Last Name</label><span class="red-star"> *</span>
                     <input type="text" placeholder="Last Name" name="last_name" id="last_name"  maxlength= "125" class="form-control required">
                  </div>
               </div>
               </div>
               <div class="row" id="new_email" style="display:none;">
                 <div class="col-sm-6">
                  <div class="form-group">
                     <label>Email</label><span class="red-star"> *</span>
                     <input type="text" placeholder="Email" name="email" id="email"  maxlength= "125" class="form-control required">
                     <p class="help-block" id="er_email"> </p>
                  </div>
               </div>
              </div>
            <h2 class="col-sm-12 text-primary row" >Drop Off Address</h2>

            <div class="row">
            <div class="col-sm-6">
                  <div class="form-group">
                     <label>Address Line 1</label><span class="red-star"> *</span>
                     <input type="text" placeholder="Address" name="address_line_1" id="address_line_1"  maxlength= "125" class="form-control required">
                  </div>
               </div>
               <div class="col-sm-6">
                  <div class="form-group">
                     <label>Address Line 2</label>
                     <input type="text" placeholder="Address" name="address_line_2" id="address_line_2"  maxlength= "125" class="form-control">
                  </div>
               </div>
               </div>
               <div class="row">
               <div class="col-sm-6">
                  <div class="form-group">
                     <label>Country</label><span class="red-star"> *</span>
                     <select name="country" class="form-control required" id="pp_pickup_country10" onchange="get_state2('pp_pickup_country10','pp_pickup_state10','pp_pickup_city10','pp_pickup_state10','','','','10','')">


                        <option value="" id="country">Select Country</option>
                        @foreach($country as $key)
                        <option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}","state_available":"{{$key->state_available}}"}'>{{$key->Content}}</option>
                        @endforeach
                     </select>
                  </div>
               </div>
               <div class="col-sm-6">
                  <div class="form-group">
                     <label>State</label><span class="red-star"> *</span>
                     <span id="ap_id10">
                     <select name="state10" class="form-control required left-disabled chosen-select" id="pp_pickup_state10" onchange="get_city('pp_pickup_state10','pp_pickup_city10','pp_pickup_city10','')">
                        <option value="">Select State</option>
                     </select>
                     </span>
                  </div>
               </div>
               </div>
               <div class="row">
               <div class="col-sm-6">
                  <div class="form-group">
                     <label>City</label><span class="red-star"> *</span>
                     <select  name="city" class="form-control required chosen-select" id="pp_pickup_city10">
                        <option value="">Select City</option>
                     </select>
                  </div>
               </div>
               <div class="col-sm-6">
                  <div class="form-group">
                     <label>Zip Code/Postcode</label>
                     <input type="text" placeholder="Zip Code/Postcode" name="zipcode" id="zipcode"  maxlength="8" class="form-control alpha-numeric">
                  </div>
               </div>
               </div>
            <div class="row">


            <div class="col-sm-12 col-xs-12" >
               <div class="form-group">
                  <div class="checkbox">
                     <label>
                        <input type="checkbox" name="ReceiverIsDifferent"  id="ReceiverIsDifferent" onclick=" return receiver_name()">
                     </label>Receiver is different from Requester
                  </div>
               </div>
            </div>

            <div class="col-sm-4 col-xs-4" id="ReceiverName_in" style="display: none;">
               <div class="form-group">
                  <label class="control-label">Receiver Name</label><span class="red-star"> *</span>

                    <input type="text" class="form-control required  usename-#name#" name="ReceiverName" placeholder="Receiver Name" maxlength="100" value="">

               </div>
            </div>
            <div class="col-sm-12 col-xs-12"></div>



            <div class="col-sm-6">
               <div class="form-group">
                  <label class="control-label">Receiver's Phone Number</label><span class="red-star"> *</span>

                  <div class="row">
                     <div class="col-xs-4">
                        <input type="text" class="form-control required numeric usename-#country_code#" placeholder="Country Code" name="country_code" maxlength="4">
                     </div>
                     <div class="col-xs-8">
                        <input type="text" class="form-control required numeric usename-#phone_number# between-8-12" name="phone_number" placeholder="Phone Number" maxlength="12">
                     </div>
                  </div>
               </div>
            </div>

            <!-- <div class="col-sm-4">
              <div class="form-group">
                  <label class="control-label">Desired Delivery Date</label>
                  <input class="form-control" placeholder="Date" name="desired_delivery_date" id="desired_delivery_date" readonly style="background-color:white !important;">
               </div>
            </div> -->
           </div>

            <div class="col-sm-12 row">
               <div class="form-group">
                  <label class="control-label">Return Address (if item is not delivered)</label>
                  <div class="checkbox">
                     <label>
                     <input type="checkbox" name="return_to_aq" value="yes">
                     </label> Return to Aquantuo
                  </div>
               </div>
            </div>

            <div class="col-sm-12 row">
               <div class="form-group">
                  <label class="control-label">Consolidate my shipping and ship them together when possible.</label>
                  <div class="checkbox curtomlabel">
                     <label>
                     <input type="checkbox" name="consolidate_check" value="on" checked="checked" >
                     </label> Yes consolidate my shipping?
                  </div>
               </div>
            </div>

            <div class="">
               <div class="row">
                  <div class="col-xs-6">
                     <div class="form-group">
                        <a class="custom-btn1 btn text-center" onclick="switch_request_header('#sec1','#sec2',0)" href="javascript:void(0)">
                        Back
                        <div class="custom-btn-h"></div>
                        </a>


                        <button class="custom-btn1 btn" id="calculate_loader">
                        Next
                        <div class="custom-btn-h"></div>
                        </button>



                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div class="row" id="sec3" style="display:none;">
   <div class="box-shadow">
      <h3>Please complete the following fields</h3>
      <hr />
      <!------------------ step1 ------------------>
      <div class="step_box three_step clearfix">
         <div class="step first selected">
            <div>1</div>
            <p class="text-center colol-black">Package Details</p>
         </div>
         <div class="step inner selected">
            <div>2</div>
            <p class="text-center colol-black">Drop Off Address</p>
         </div>
         <div class="step last selected" >
            <div>3</div>
            <p class="text-center colol-black">Payment</p>
         </div>
      </div>

      <div class="row">
         <div class="col-md-10 col-sm-offset-1">
            <div class="col-sm-12">
               <div id="shipping_detail">
                  Calculating! Please wait...
               </div>
               <div class="">
                  <div class="row">
                     <div class="col-xs-6">
                        <div class="form-group">
                            <a class="custom-btn1 btn text-center" onclick="switch_request_header('#sec2','#sec3',0)" href="javascript:void(0)">
                           Back
                           <div class="custom-btn-h"></div>
                           </a>

                           <button class="custom-btn1 btn" id="creating_req_btn">
                              Next
                              <div class="custom-btn-h"></div>
                           </button>

                        </div>
                     </div>
                     <input type="hidden" name="distance" id="distance">
                     <input type="hidden" name="promo_code" id="promo_code_olp_promo">
                  </div>
               </div>
            </div>
         </div>
      </div>

      <!------------------step2------------------>
      <!------------------step3------------------>
      <div class="col-sm-12" id="sec3" style="display:none;">
         <div class="box-shadow">
            <h3>Please complete the following fields</h3>
            <hr />
            <!------------------step1------------------>
            <div class="row" >
               <div class="step_box three_step clearfix">
                  <div class="step first selected">
                     <div>1</div>
                     <p class="text-center colol-black">Package Details</p>
                  </div>
                  <div class="step inner selected">
                     <div>2</div>
                     <p class="text-center colol-black">Drop Off Address</p>
                  </div>
                  <div class="step last selected">
                     <div>3</div>
                     <p class="text-center colol-black">Payment</p>
                  </div>
               </div>
            </div>
            <div class="col-md-10 col-sm-offset-1">
               <div class="col-sm-12">
                  <div class="row custom-row">
                     b
                     <div class="col-sm-6"><b>Total Weight -</b></div>
                     <div class="col-sm-6">50lbs</div>
                  </div>
                  <div class="row custom-row">
                     <div class="col-sm-6"><b>Item Price -</b></div>
                     <div class="col-sm-6">$50.00</div>
                  </div>
                  <div class="row custom-row">
                     <div class="col-sm-6"><b>Retailer Shipping -</b></div>
                     <div class="col-sm-6">50lbs</div>
                  </div>
                  <div class="row custom-row">
                     <div class="col-sm-6"><b>Aquantuo Shipping -</b></div>
                     <div class="col-sm-6">50lbs</div>
                  </div>
                  <div class="row custom-row">
                     <div class="col-sm-6"><b>Delivery within Ghana -</b></div>
                     <div class="col-sm-6">50lbs</div>
                  </div>
                  <div class="row">
                     <hr />
                  </div>
                  <div class="col-sm-8 col-sm-offset-2">
                     <div class="form-group">
                        <button class="custom-btn1 btn-block" >
                           Next
                           <div class="custom-btn-h"></div>
                        </button>
                     </div>
                  </div>
               </div>
            </div>
            <div class="clearfix"></div>
            <div class="list-footer">
               <div class="row">
                  <div class="col-sm-6">
                     <h4><b>Total Amount- $250.00</b></h4>
                     <small>(In Ghananian cedi GHS 315,8656.00)</small>
                  </div>
                  <div class="col-sm-6">
                     <div class="form-group reward-group">
                        <input class="form-control pull-left" placeholder="Reward Code" type="text" />
                        <button class="btn default-btn">
                      {{--   <img src="theme/web/promo/images/green_check.png" /> --}}
                        Apply
                        </button>
                        <span>$21.00</span>
                     </div>
                  </div>
               </div>
               <small class="color-red">You wil be billed or refunded any differences in price or shipping at time of purchase</small>
            </div>
         </div>
      </div>
   </div>
</div>
{!! Form::close() !!}

{!! Html::script('theme/admin/custome/js/validation1.js') !!}
<!-- {!! Html::script('theme/admin/custome/js/utility.js') !!} -->
{!! Html::script('theme/admin/custome/js/online_purchase.js') !!}
{!! Html::style('theme/date-time-picker/jquery.datetimepicker.css') !!}
{!! Html::script('theme/date-time-picker/jquery.datetimepicker.js') !!}

{!! Html::style('theme/admin/choose-multiple/bootstrap-chosen.css') !!}
{!! Html::script('theme/admin/choose-multiple/chosen.jquery.js') !!}
{!! Html::script('theme/web/js/utility.js') !!}

@include('Admin::layouts.footer')

<style type="text/css">
  #user_chosen{
   width: 100% !important;
  }
</style>


<script type="text/javascript">



jQuery(document).ready(function ($) {
$(".chosen-select").chosen({no_results_text: "Oops, nothing found!"})
});


   $(function() {
       var action;
       $(".number-spinner button").mousedown(function () {
           btn = $(this);
           input = btn.closest('.number-spinner').find('input');
           btn.closest('.number-spinner').find('button').prop("disabled", false);

           if (btn.attr('data-dir') == 'up') {
               action = setInterval(function(){
                   if ( input.attr('max') == undefined || parseInt(input.val()) < parseInt(input.attr('max')) ) {
                       input.val(parseInt(input.val())+1);
                   }else{
                       btn.prop("disabled", true);
                       clearInterval(action);
                   }
               }, 50);
           } else {
               action = setInterval(function(){
                   if ( input.attr('min') == undefined || parseInt(input.val()) > parseInt(input.attr('min')) ) {
                       input.val(parseInt(input.val())-1);
                   }else{
                       btn.prop("disabled", true);
                       clearInterval(action);
                   }
               }, 50);
           }
       }).mouseup(function(){
           clearInterval(action);
       });
   });

   $('#desired_delivery_date').datetimepicker({

      format:'M d, Y h:i A',
      formatTime:'h:i A',
      timepicker:true,
      datepicker:true,
      minDate : new Date(),
   });

new Validate({
  FormName :  'new_address',
  ErrorLevel : 1,
  callback: function() {
      $("#address_loader").addClass("spinning");
       $.ajax({
           url: 'add-address',
           data: {     "address_line_1": $('#address_line_1').val() ,
                       "address_line_2": $('#address_line_2').val(),
                       "country": $('#pp_pickup_country').val(),
                       "state": $('#pp_pickup_state').val(),
                       "city":$('#pp_pickup_city').val(),
                       "zipcode": $('#zipcode').val()},
           type : 'post',
           dataType: 'json',
           success : function(obj) {
                 $("#address_loader").removeClass("spinning");

                if(obj.success == 1) {
                    document.getElementById("address_loader").value = "Submit";
                    document.getElementById("Addaddress").reset();
                    $('#dropoff_address').html(obj.address_html);
                     $("#exampleModal").modal("hide");
                }
                alert(obj.msg);
           }
        });
   }
});



new Validate({
  FormName :  'buy_for_me_form_add',
  ErrorLevel : 1,
  validateHidden : false,
  callback: function() {
      $("#add_item_loader1").addClass("spinning");
       $.ajax({
            url: 'add-online-item',
            data: new FormData(document.getElementById('buy_for_me_form_add')),
            type : 'post',
            processData: false,
            contentType: false,
            dataType: "json",
            success : function(obj) {
                $("#add_item_loader1").removeClass("spinning");
                if(obj.success == 1)
                {
                  try {
                    document.getElementById("buy_for_me_form_add").reset();
                    //document.getElementById("add_item_loader").value = "Submit";
                  }catch(e) {}
                    $('#item_list').html(obj.html);
                    $('#table_hide').show();
                    $('#add_item').hide();
                }
                alert(obj.msg);

           }
        });
    }
});

   function remove_record(url,rowid)
   {
    if(confirm('Are you sure? You want to delete this record.') == true)
    {
        $('#row-'+rowid).addClass('relative-pos spinning');

        url = SITEURL+url;
        $.ajax
            ({
                url: url,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(res)
                {
                    var obj = JSON.parse(res);
                    $('#row-'+rowid).removeClass('relative-pos spinning');
                    if(obj.success == 1){

                        $('#row-'+rowid).css({'background-color':'red'});
                        $('#row-'+rowid).fadeOut('slow');
                        $('#error_msg_section').html('<div class="alert alert-success">'+ obj.msg + '</div>');
                    }else{
                        $('#row-'+rowid).css({'background-color':'white'});
                        $('#error_msg_section').html('<div class="alert alert-danger">'+ obj.msg + '</div>');
                        alert(obj.msg);
                    }
                }
            });
    }
    return false;
   }

   $('#add_item_button').click(function(){
      $('#buy_for_me_dimensions').prop('checked', false);
      $("#measurment").hide();
   });


   function get_item_info(id){
    $.ajax({
       url: SITEURL+'admin/get_item_info/'+id,
       type : 'get',
       dataType: 'json',
       success : function(obj) {
          //alert(obj.result.item_name);
          get_edit_item(obj.result);


       }
    });
    return false;
  }

   function get_edit_item(obj)
   {
    $('#edit_item').trigger('reset');
      //obj = eval('('+obj+')');

    $('#item_name').val(obj.item_name);
    $('#add_item_url').val(obj.add_item_url);


    $('#buyforme_dimensions').prop('checked', false);
    $("#dimention_div").hide();
     document.getElementById('edit_measurement_unit2').checked='checked';
    if(obj.weight != ''){
      $('#buyforme_dimensions').prop('checked', true);
      if(obj.measurement_unit == 'cm_kg') {
        document.getElementById('edit_measurement_unit').checked='checked';
      }
      $('#length').val(obj.length);
       $('#width').val(obj.width);
       $('#height').val(obj.height);
       $('#weight').val(obj.weight);
       $("#dimention_div").show();
    }

    $('#item_price').val(obj.item_price);
    $('#quentity').val(obj.quentity);

    if(obj.image != '') {
         $('#online_purchase_preview').attr('src',SITEURL+'upload/'+obj.image);
        } else {
         $('#online_purchase_preview').attr('src',SITEURL+'upload/no-image.jpg');
         }


    if(obj.travel_mode == 'air') {
      document.getElementById('travel_mode_air').checked='checked';
      toggle_category('.travel-mode-air','.travel-mode-ship','#package_category');
    } else {
      document.getElementById('travel_mode_ship').checked='checked';
      toggle_category('.travel-mode-ship','.travel-mode-air','#package_category');
    }
    //$('#package_category').val('{"id":"'+obj.category_id+'","name":"'+obj.category+'"}');
    $('#package_category').val('{"id":"'+obj.category_id+'"}');
    $('#description').val(obj.description);

    console.log(obj.insurance);
     if(obj.insurance == 'yes') {
      document.getElementById('insurance').checked='checked';
      }else{
      document.getElementById('insurance2').checked='checked';

    }
    $('#item_id').val(obj._id);

   }

new Validate({
    FormName :  'edit_item',
    ErrorLevel : 1,
    validateHidden: false,
    callback: function() {
       $("#item_loader").addClass("spinning");
       $.ajax({
           url: 'edit-online-item/{$value->_id}',
           data: $('#edit_item').serialize(),
           type : 'post',
           data: new FormData(document.getElementById('edit_item')),
           processData: false,
           contentType: false,
           dataType: 'json',
           success : function(obj) {
               $("#item_loader").removeClass("spinning");

               if(obj.success == 1) {
                  document.getElementById("item_loader").value = "Submit";
                  document.getElementById("edit_item").reset();
                  $('#item_list').html(obj.edit_html);
                  $("#edit_modal").trigger("click");

               }
               alert(obj.msg);
           }
        });
    }
});

function image_preview(obj,previewid,evt)
{
    var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
    if ($.inArray($(obj).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
       $(obj).val('');
       $('#'+previewid).attr('src', 'user-no-image.jpg');
        alert("Only "+fileExtension.join(', ')+" formats are allowed.");
     } else {

       var file = evt.target.files[0];

       if (file)
       {
         var reader = new FileReader();

         reader.onload = function (e) {
             $('#'+previewid).attr('src', e.target.result)
         };
         reader.readAsDataURL(file);
       }
     }
}

  $(document).ready(function(){
    $('#buy_for_me_dimensions').prop('checked', false);
    $("#newuser").click(function(){
        $("#Existing_user").hide();
        $("#name").show();
        $("#new_email").show();
    });

    $("#existing").click(function(){
        $("#Existing_user").show();
        $("#name").hide();
        $("#new_email").hide();
    });

});
$("#buy_for_me_dimensions").click(function(){
   // If checked
   if ($("#buy_for_me_dimensions").is(":checked"))
   {
      //show the hidden div
      $("#measurment").show();
   }
   else
   {
      //otherwise, hide it
      $("#measurment").hide();
   }
});

$("#buyforme_dimensions").click(function(){
   // If checked
   if ($("#buyforme_dimensions").is(":checked"))
   {
      //show the hidden div
      $("#dimention_div").show();
   } else {
      //otherwise, hide it
      $("#dimention_div").hide();
   }
});


function receiver_name(){

  if($("#ReceiverIsDifferent").prop('checked') == true){
    $('#ReceiverName_in').show();
  }else{
    $('#ReceiverName_in').hide();
  }

}
</script>

@endsection

