<!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Item List</h4>
        </div>
        <div class="modal-body">
           <table id="example" class="table table-striped table-bordered" width="100%" cellspacing="0">
             <tr>
                <th>S.No.</th>
                <th>Item Name</th>
                <th>Price</th>
                <th>Qty</th>
                <th>Aquantuo shipping</th>
             <tr>
             <?php $sno = 1;$qty = 0;?>
            @foreach($product as $value)
              <tr>
                <td><?php echo $sno++; $qty = $qty + $value['qty']; ?> </td>
                <td>{{ucfirst($value['product_name'])}}</td>
                <td>${{number_format($value['price'],2)}}</td>
                <td>{{ $value['qty'] }}</td>
                <td>${{number_format(@$value['shippingCost'],2)}}</td>
             <tr>
             @endforeach
           </table>
            <div class="row">
             <div class="col-sm-4">
            	<label>Total item cost: </label>
             </div>
             <div class="col-sm-8">
             	${{number_format($item_cost,2)}}<br>
             </div>
            </div>

            <div class="row">
            <div class="col-sm-4">
            	<label>Aquantuo Charge: </label>
            </div>
            <div class="col-sm-8">
             	${{number_format($shippingCost,2)}}
            </div>
            </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>






@if(count($error) > 0)

<div class="row custom-row">
	@foreach($error as $key)
   	<div class="col-sm-12"><p class="color-red">{{$key}}</p></div>
   	<p></p><p></p>
   	@endforeach
</div>

@else

{{-- <div class="row custom-row">
   <div class="col-sm-6"><b>Distance -</b></div>
   <div class="col-sm-6">{{number_format($distance_in_mile,2)}} Miles</div>
</div> --}}

<div class="row custom-row">
   <div class="col-sm-6"><b> No. of Items: </b></div>
   <div class="col-sm-6">{{ $qty }}</div>
   {{-- <div class="col-sm-6">{{$product_count}}</div> --}}
</div>

<div class="row custom-row">
   <div class="col-sm-6"><b> Int'l Shipping Cost: </b></div>
   <div class="col-sm-6" id="shippingCost">${{number_format($shippingCost,2)}}</div>
</div>

<div class="row custom-row">
   <div class="col-sm-6">
   		<b> Shipping Cost: </b> <br />
   		<small>(Retailer to Aquantuo's facility)</small>
   	</div>
   <div class="col-sm-6" id="shippingCostByUser">${{number_format($shipping_cost_by_user,2)}}</div>
</div>

<div class="row custom-row">
   <div class="col-sm-6"><b> Insurance: </b></div>
   <div class="col-sm-6" id="insuranceCost">${{number_format($insurance,2)}}</div>
</div>

<div class="row custom-row">
  <div class="col-sm-6"><b> Duty/Customs Clearing: </b></div>
  <div class="col-sm-6">
    $<input type="text" id="dutyCustom" name="dutyCustom" value="{{ number_format($DutyAndCustom, 2) }}" />
  </div>
</div>

<div class="row custom-row">
  <div class="col-sm-6"><b> Tax: </b></div>
  <div class="col-sm-6">
    $<input type="text" id="tax" name="tax" value="{{ number_format($Tax, 2) }}" />
  </div>
</div>

<div class="row custom-row">

   <div class="col-sm-6"><b><a data-toggle="modal" style="cursor:pointer" data-target="#myModal"> Total Item Cost: </a></b></div>
   <div class="col-sm-6" id="totalItemCost">${{number_format($item_cost,2)}}</div>
</div>

<div class="row custom-row">
   <div class="col-sm-6">
      <b> Processing Cost: </b> <br />

    </div>
   <div class="col-sm-6" id="processingCost">${{number_format($ProcessingFees,2)}}</div>
</div>

<div class="row custom-row">
   <div class="col-sm-6">
      <b> Area Charge: </b> <br />
    </div>
     <div class="col-sm-6" id="areaCharge">${{number_format($AreaCharges,2)}}</div>
</div>

<div class="row"> <hr /> </div>
<div class="row custom-row">
   <div class="col-sm-6"><b> Total Amount: </b></br>
   <small id="olp_promo_ghana">{{$ghana_total_amount}}</small>

    </div>
   <div class="col-sm-6" id="totalAmount">${{number_format(($total_amount),2)}}</div>
</div>


<div class="row">
	<hr>
	<div class="col-sm-12">
		<div class="form-group reward-group" id="olp_promo_input">
			<input type="text" placeholder="Promotion Code" class="form-control pull-left" id="promocode" name="promocode" >
			<a class="btn default-btn" onclick="check_promocode('promocode',shipping_cost,'olp_promo',total_amount)" href="javascript:void(0)">
				<img src="{{url('theme/web/promo/images/green_check.png')}}" />Apply
			</a>
			<div class="clearfix"></div>
			<div  id="er_promocode" class="color-red"></div>
		</div>
		<div class="form-group reward-group" id="olp_promo" style='display: none;'>
			<span class="" onclick="remove_promocode('olp_promo','promocode')"><i class="fa fa-trash"></i></span><span id="olp_promo_msg"></span>
		</div>
	</div>
</div>
</div>

	<div class="row"> <hr /> </div>
		<div class="row custom-row">
			<div class="col-sm-6"><b> Payable: </b></br>
		<small><div id="olp_promo_payable_ghana">{{$ghana_total_amount}}</div></small>
		</div>
		<div class="col-sm-6"><div id="olp_promo_payable">${{number_format(($total_amount),2)}}</div></div>
</div>

	{{-- <small class="color-red">You wil be billed or refunded any differences in price or shipping at time of purchase</small> --}}
  <hr/>
  <small class="color-red">If actual weight wasn’t provided, you may be billed or refunded any differences in price or shipping upon review</small>

<div class="row"> <hr /> </div>
@endif
<script type="text/javascript">
  $("#dutyCustom").keyup(function () {
    var shippingCost = $("#shippingCost").text().split("$");
    var shippingCostByUser = $("#shippingCostByUser").text().split("$");
    var insuranceCost = $("#insuranceCost").text().split("$");
    var tax = $("#tax").val();
    var totalItemCost = $("#totalItemCost").text().split("$");
    var processingCost = $("#processingCost").text().split("$");
    var areaCharge = $("#areaCharge").text().split("$");
    var newAmt = parseFloat(shippingCost[1]) + parseFloat(shippingCostByUser[1]) + parseFloat(insuranceCost[1]) + parseFloat(tax) + parseFloat(totalItemCost[1]) + parseFloat(processingCost[1]) + parseFloat(areaCharge[1]) + parseFloat(this.value);
    $("#totalAmount").html("$" + newAmt);
    $("#olp_promo_payable").html("$" + newAmt);
    var user_currency_value = (newAmt*currency_rate).toFixed(2);
    var user_currency_text = formated_text.replace("[AMT]", user_currency_value);
    $("#olp_promo_ghana").html(user_currency_text);
    $("#olp_promo_payable_ghana").html(user_currency_text);
  });

  $("#tax").keyup(function () {
    var shippingCost = $("#shippingCost").text().split("$");
    var shippingCostByUser = $("#shippingCostByUser").text().split("$");
    var insuranceCost = $("#insuranceCost").text().split("$");
    var dutyCustom = $("#dutyCustom").val();
    var totalItemCost = $("#totalItemCost").text().split("$");
    var processingCost = $("#processingCost").text().split("$");
    var areaCharge = $("#areaCharge").text().split("$");
    var newAmt = parseFloat(shippingCost[1]) + parseFloat(shippingCostByUser[1]) + parseFloat(insuranceCost[1]) + parseFloat(dutyCustom) + parseFloat(totalItemCost[1]) + parseFloat(processingCost[1]) + parseFloat(areaCharge[1]) + parseFloat(this.value);
    $("#totalAmount").html("$" + newAmt);
    $("#olp_promo_payable").html("$" + newAmt);
    var user_currency_value = (newAmt*currency_rate).toFixed(2);
    var user_currency_text = formated_text.replace("[AMT]", user_currency_value);
    $("#olp_promo_ghana").html(user_currency_text);
    $("#olp_promo_payable_ghana").html(user_currency_text);
  });
</script>