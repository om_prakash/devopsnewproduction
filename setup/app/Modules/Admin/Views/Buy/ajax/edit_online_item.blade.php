
<tr >
<th>S.No.</th>
<th>Purchase Form</th>
<th>Item Cost</th>
<th>Item Weight</th>
<th>Quantity</th>
<th>Description</th>
<th>Action</th>
</tr>
<tr>
<?php $sno = 1;?>
@foreach($items as $key)

   <tr id="row-{{$key['_id']}}">
      <td>{{$sno++}}</td>
      <td>{{ucfirst($key['product_name'])}}</td>

      <td>${{number_format((float)$key['price'],2)}}</td>

      <td>{{ucfirst($key['weight'])}}&nbsp;{{ucfirst($key['weight_unit'])}}</td>

      <td>{{$key['qty']}}</td>

      <td>{{ucfirst($key['description'])}}</td>
      <td colspan='2' >&nbsp;&nbsp;<a type=""  data-toggle="modal" onclick="get_item_info('{{$key['_id']}}')" id="Edit" title="Edit" data-whatever="@mdo" href="#item_modal"><img src="http://aquantuo.com/upload/edit.svg"  width="20px" height="25px" ></a>&nbsp;
                <!-- <a title="delete" id="Delete"  onclick="remove_record('delete_item/<?php //echo $key['_id']; ?>/DeleteItem','<?php //echo $key['_id']; ?>','<?php //echo Input::get('request_id'); ?>')" href="javascript:void(0)"><img src="http://aquantuo.com/upload/delete.png"  width="20px" height="25px"></a> -->

               

          @if($form_type == 'edit')
                 <a title="delete" id="Delete"  onclick="remove_record2('{{$key['_id']}}','<?php echo Input::get('request_id'); ?>')" href="javascript:void(0)"><img src="http://aquantuo.com/upload/delete.png"  width="20px" height="25px"></a>
          @else
            <a title="delete" id="Delete"  onclick="remove_record('admin/delete_item/{{$key['_id']}}/DeleteItem','{{$key['_id']}}')" href="javascript:void(0)"><img src="http://aquantuo.com/upload/delete.png"  width="20px" height="25px"></a>
          @endif

            </td>
   </tr>

@endforeach
