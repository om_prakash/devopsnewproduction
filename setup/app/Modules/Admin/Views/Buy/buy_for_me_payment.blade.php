@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')
@section('content')

{!! Html::script('theme/admin/custome/js/validation.js') !!}

<div class="container">
   <h3 class="text-primary">Online Transaction</h3>
   <div class="box-shadow">
   <br><br>
  {!! Form::model('', ['name' => 'buy_for_me_payment',  'url' => 'admin/post-buy-for-me/payment', 'id' => 'buy_for_me_payment', 'method' => 'POST']) !!}
  <div class="col-sm-8">
    <p>Please fill the transaction id and description if you have or <a href="{{ url('admin/online_package/detail/'.Request::segment(4)) }}">Skip</a>.</p>

  <div class="form-group">
    <label for="transaction">Transaction ID</label>
    <input type="text" class="form-control required" id="transaction" name="transaction" placeholder="Transaction Id">
    <p class="help-block red" style="color:red;"  >{{$errors->first('transaction')}} </p>
  </div>
  </div>
  <div class="col-sm-8">
  <div class="form-group">
    <label for="description">Description</label>
    <textarea row="5" class="form-control required" id="description" name="description" placeholder="Description"> </textarea>
    <p class="help-block red" style="color:red;"  >{{$errors->first('description')}} </p>
  </div>
  </div>
  <div class="col-sm-8">
  <input type="hidden" value="{{ Request::segment(4) }}"  name="url_id">
  <button type="submit" class="btn btn-primary">Submit</button>
  <a href="{{ url('admin/online_package/detail/'.Request::segment(4)) }}" class="btn btn-primary">Skip</a>
  </div>
  <div class="clearfix"></div>
 {!! Form::close() !!}
</div>
</div>
@include('Admin::layouts.footer')

<script>
new Validate({
   FormName :  'buy_for_me_payment',
   ErrorLevel : 1,

    });
</script>
@endsection
