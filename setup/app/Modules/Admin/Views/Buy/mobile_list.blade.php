@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')
@section('content')
{!!Html::style('theme/web/css/parsley.css') !!}
<style>
.parsley-required {
    color: #a94442;
    font-weight: bold;
    font-size: 13px;
}
input.parsley-error, select.parsley-error, textarea.parsley-error {
    background-color: #FFF;
     color: black;
}

</style>


<div class="container">

	<div class="row">
    	<div class="col-xs-6 col-sm-6">
		<h2 class="color-blue mainHeading" style="text-align:left;">Mobile Money Payment</h2>
        </div>
        <div class="col-xs-6 col-sm-6">
        <small class="pull-right color-blue mainHeading" style="text-align:right;">
		<div class="clearfix"></div>
		 <a class="btn btn-primary" href={{url('admin/process-card-list')}}/{{Request::segment(3)}} style="margin-right:0;">
		 <i class="fa fa-arrow-left"> Back</i></a>
		</small></div>
        </div>
        {{-- <br/>
        <input type="checkbox" value='' name="check"  id="exist">&nbsp;

        <small>
        {{$admin_msg}}
		</small>
		<br/> --}}<br/>

	<div class="clearfix"></div>
		<span id="card_msg"></span>
		<div class="row">

			<div class="col-md-12 col-sm-12 col-xs-12">

            <div class="box-shadow" style="padding-top: 15px; padding-bottom: 10px">

			    <div class="col-sm-4">
			    <label>Package Id: </label>
			     {{@$req_amount->PackageNumber}}
			     </div>

                 <div class="col-sm-4">
			    <label>Requester Name: </label>
			    {{@$req_amount->RequesterName}}
			   </div>

			    <div class="col-sm-4">
			    <label>Product Title: </label>
			     {{ucfirst(@$req_amount->ProductTitle)}}
			   </div>

			</div>

				<div class="box-shadow clearfix">
				<br/>

				@if(count($mobileMoney) > 0)
					@foreach($mobileMoney as $key)

					<div class="col-md-4 col-sm-6 col-xs-12" id="card_{{$key['id']}}">
						<div class="card-wrapper">
                        <div class="custom-btn">
							 @if($key['mobile_type'] == 'vodafone')
                              <a data-whatever="@mdo" mobileid="{{$key['id']}}"  onclick="return Vodaphone('{{$key['id']}}')" id="vodafoneid"  data-target="#modal3" data-toggle="modal" href="" ><span>PAY NOW</span></a>
                            @else
								<a class="btn"  href="{{url('admin/mobile-payment/'.Request::segment(3))}}/{{$key['id']}}" onclick="return show_btn()">PAY NOW</a>
                            @endif
						</div>
						<a class="deletebtn" title="Delete card" onclick="return confirm('Are you sure you want to delete this Mobile number?')" href="{{url('admin/delete-mobile/'.$key['_id'].'/'.Request::segment(3))}}"><i class="fa fa-trash"></i></a>

								<div class="media">
									<div class="media-left">
										<a href="#">
											@if($key['mobile_type'] == 'airtel')
											<img height="30px" width="50px" src="{{ImageUrl}}/webcard/airtel_money.png">
											@elseif($key['mobile_type'] == 'mtn')
											<img height="30px" width="50px" src="{{ImageUrl}}/webcard/mtn.png">
											@elseif($key['mobile_type'] == 'tigo')
											<img height="30px" width="50px" src="{{ImageUrl}}/webcard/tigo.png">
											@elseif($key['mobile_type'] == 'vodafone')
											<img height="30px" width="50px" src="{{ImageUrl}}/webcard/vodafone.png">
											@endif
										</a>
									</div>
									<div class="media-body">
										<p class="media-heading color-blue" style="font-weight:bold">xxxxxx-{{$key['alternet_moblie']}}</p>
										<p>Pricing may change if the actual item weight was not entered.</p>

									</div>
								</div>
							</div>
						</div>
					@endforeach
				@else
							<div class="col-md-12">
								<!-- <div class="panel panel-default">
								<center>
								<br>
									<p class="text-center" >No card added yet.</p>
								</center>
								<br>
								</div> -->
							</div>
					@endif



				<div class="col-md-4 col-sm-6 col-xs-12">
                 	<div class="card-wrapper gray-card">
						<a data-whatever="@mdo" data-target="#modal2" data-toggle="modal" href="" ><span><i class="fa fa-plus"></i>Add Mobile No.</span></a>
                   </div>
				</div>




			</div>
		</div>

    </div>
</div>


<div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title color-white" id="myModalLabel">&nbsp;&nbsp;&nbsp;Add Mobile Number</h4>
          </div><div class="clearfix"></div>
          {!! Form::model('', ['name' => 'Addcard', 'id' => 'Addcard', 'method' => 'POST', 'url' => 'admin/add-mobile-number/'.Request::segment(3),'data-parsley-validate']) !!}

          <input type="hidden" name="requestid" value="{{Request::segment(3)}}">
          <div class="modal-body">
          		<div class=col-md-12 id="succesdiv"></div>
				<div id='errordiv' class="col-sm-12"></div>

			<div class="form-group clearfix">
				<div class="col-sm-12">
					{!! Form::label('Select Country Code', 'Select Country code',['class'=>'color-black']) !!}
					{!! Form::select('country_code', ['233'=>'+233','1'=>'+1','44'=>'+44'],Input::get('country_code'),['class'=>'form-control required usename-#country_code#"','id'=>'country_code']) !!}
                	 @if($errors->has('country_code'))
                     <p class="controll-error help-block"><i class="fa fa-times-circle-o"></i>&nbsp;
                        {{ $errors->first('country_code') }}
                     </p>
                     @endif
				</div>
			</div>


			<div class="col-sm-12">
               <div class="row">
                <div class="col-sm-12">
                <div class="form-group">
               {!! Form::label('Select Mobile Carrier', 'Select Mobile Carrier',['class'=>'color-black']) !!}
                 {!! Form::select('type', [''=>'Select Mobile Carrier','mtn'=>'MTN','airtel'=>'Airtel','tigo'=>'Tigo','vodafone' => 'Vodafone'],Input::get('type'),['class'=>'form-control required usename-#type#"','id'=>'type']) !!}
               		 <p class="help-block white" id='type'></p>
                </div>
                </div>

                </div>
            </div>

			<div class="form-group clearfix">
				<div class="col-sm-12">
				   {!! Form::label('Mobile Number', 'Mobile Number',['class'=>'color-black']) !!}
					  {!! Form::text('mobile_number', '', ['class'=>'form-control required numeric','maxlength'=>'10	' , 'placeholder'=> 'Mobile Number','id' =>'mobile_number', 'required' , 'data-parsley-required-message' =>'Mobile Number is required.']) !!}
					  @if($errors->has('mobile_number'))
                     <p class="controll-error help-block"><i class="fa fa-times-circle-o"></i>&nbsp;
                        {{ $errors->first('mobile_number') }}
                     </p>
                     @endif
				</div>
			</div>


          </div><div class="clearfix"></div>
          <br />
			<div class="modal-footer">
			  <div class="col-sm-12 text-right">
			  <div class="">
				{!! Form::button('Close ', ['class' => 'custom-btn1 btn', 'data-dismiss'=>'modal']) !!}
                {!! Form::button('Sumbit', ['class' => 'custom-btn1 btn','id'=>'addbutton','type'=>'submit']) !!}
				</div>
			  </div>
			</div>

			{!! Form::close() !!}
        </div>
      </div>
    </div>


    <div class="modal fade" id="modal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title color-white" id="myModalLabel2">&nbsp;&nbsp;&nbsp;Voucher Code</h4>
          </div><div class="clearfix"></div>

           {!! Form::model('', ['name' => 'AddVoucher', 'id' => 'AddVoucher', 'method' => 'post', 'url'=> ['admin/Voucher-mobile-payment/'. Request::segment(3)],'data-parsley-validate']) !!}

          <div class="modal-body">
      		<div class=col-md-12 id="succesdiv"></div>
			<div id='errordiv' class="col-sm-12"></div>

			<div class="form-group clearfix">
				<div class="col-sm-12">
					Dial *110# on your Vodafone Cash phone,  select Generate Voucher and follow the prompts
				 <br/><br/>
				 <input type="hidden" value="" name="voucherid" id="vodafoneId">
				   {!! Form::label('Voucher', 'Voucher',['class'=>'color-black']) !!}
					  {!! Form::text('Voucher', '', ['class'=>'form-control required numeric','maxlength'=>'10	' , 'placeholder'=> 'Voucher','id' =>'Voucher','required' => '','data-parsley-required-message' =>'Voucher code is required.']) !!}
					   @if($errors->has('Voucher'))
                     <p class="controll-error help-block"><i class="fa fa-times-circle-o"></i>&nbsp;
                        {{ $errors->first('Voucher') }}
                     </p>
                     @endif
				</div>
			</div>

          </div><div class="clearfix"></div>
          <br />
			<div class="modal-footer">
			  <div class="col-sm-12 text-right">
			  <div class="">
				{!! Form::button('Close ', ['class' => 'custom-btn1 btn', 'data-dismiss'=>'modal']) !!}
                {!! Form::button('Sumbit', ['class' => 'custom-btn1 btn','id'=>'addbutton2','type'=>'submit']) !!}

				</div>
			  </div>
			</div>

			{!! Form::close() !!}
        </div>
      </div>
    </div>

{!! Html::script('theme/admin/custome/js/validation.js') !!}
{!! Html::script('theme/web/js/validation.js') !!}
{!! Html::script('theme/web/js/parsley.min.js') !!}
{!! Html::script('theme/web/js/utility.js') !!}

@include('Admin::layouts.footer')
@endsection
<!--card modal-->


<script>
function show_btn()
{
	checked =$('#exist').prop('checked'); // true
	//checked = $("input[type=checkbox]:checked").length;

	if(checked == 0){
		alert('Please accept agreement.');
		//return false;
		flage=false;
	}else if(checked == 1)
	{
		//return true;
		flage=true;
	}

	return flage;

}

new Validate({
	FormName : 'Addcard',
	ErrorLevel : 1,
	});

$('#AddVoucher').parsley().subscribe('parsley:form:validated', function (parsleyForm) {
  	if (true === parsleyForm.validationResult ){
      $('#addbutton2').addClass('spinning');
   	}else{
   		$('#addbutton2').removeClass('spinning');
   	}

});


	 function Vodaphone(id){
	 	$('#vodafoneId').val('');
        $('#vodafoneId').val(id);
	 }

</script>
