@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')

@section('content')
{!! Html::style('theme/admin/custome/css/style.min.css') !!}


<div class="container-fluid">
<div class="panel panel-default">
  <div class="panel-heading"><i class="fa fa-plus"><b>Add Region</b></i>&nbsp;</div>
  <div class="panel-body">

  	<div class="row">
  		<div class="col-md-8">
		{!! Form::model('', ['name' => 'itemForm', 'id' => 'itemForm', 'method' => 'POST', 'url' => ['admin/add-accra']]) !!}
        <div class="modal-body">

          <div class="col-md-12">
             <div class="form-group" id="cityDiv">
            {!! Form::label('Country', 'Country',['class' => 'control-label']) !!}
               <select name="country" class="form-control required usename-#country#" id="pp_pickup_country" onchange="get_state('pp_pickup_country','pp_pickup_state','pp_pickup_city','pp_pickup_state','','')">

                              <option value="">Select Country</option>
                              @foreach($country as $key)
                               <option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}","state_available":"{{$key->state_available}}"}' >{{$key->Content}}</option>
                              @endforeach

                          </select>
            <p class="help-block red" id="er_country" style="color:#a94442"></p>
          </div>
        </div>
            <div class="clearfix"> </div>
                    <div class="col-sm-12">
                       <div class="form-group">
                          <label class="control-label">Region</label><span class="red-star"> *</span>
                             <select name="state" class="form-control required left-disabled" id="pp_pickup_state" onchange="get_city('pp_pickup_state','pp_pickup_city','pp_pickup_city','')" >
                              <option value="">Select State</option>
                          </select>
                       </div>
                    </div>
                    <div class="clearfix"> </div>

                <div class="col-md-12">
                      <div class="form-group">
                            <label class="control-label">Amount</label>
                            <div class="input-group error-input">
                            <span class="input-group-addon">$</span>
                            <input class="form-control required float" placeholder="Amount" name="amount"  >
                            <p class="help-block red" id="er_amount" style="color:#ab4442"></p>
                            </div>
                      </div>
                  </div>

        <div class ="clearfix"></div>
        <div class="col-sm-12">
        <br/>
        <a class="btn btn-primary" href="{{ url('admin/accra-mamagement') }}" >Back</a>
        {!! Form::submit('Add', ['class' => 'btn btn-primary']) !!}
        </div>
        </div>
      {!! Form::close() !!}
			 </div>
		</div>
	</div>
</div>
  </div>






  </div>
</div>

</div>

{!! Html::script('theme/admin/custome/js/utility.js') !!}
{!! Html::script('theme/admin/custome/js/validation1.js') !!}

<style>
.text-color
{
	color:#a94442;
}


</style>


<script>



new Validate({
  FormName : 'itemForm',
  ErrorLevel : 1,

});

</script>

@include('Admin::layouts.footer')
@stop
