@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')

@section('content')
{!! Html::style('theme/admin/custome/css/style.min.css') !!}

<div class="container">
   <div class="row">
   	  <div class="col-sm-12"  id="send_a_package_start_position">
      <h2 class="color-blue">SEND A PACKAGE</h2>
      <br>
      </div>
        <style type="text/css">
                  .Insurance_check{float: left; margin-top: 3px; margin-left: 5px;}

                </style>
      <div class="col-sm-12">
      	<div class="box-shadow">
        <h3>Please complete the following fields</h3>
        <hr>
        <div class="row">
        <!-- Fillable -->
          <div class="col-sm-12">
             <div class="step_box four_step clearfix">
             	<div class="step first selected" id="step0-header" style="width:20%;">
                   <div>1</div>
                   <p class="text-center colol-black">Select User</p>
                </div>
                <div class="step inner" id="step1-header" style="width:20%;">
                   <div>2</div>
                   <p class="text-center colol-black">Pickup Address</p>
                </div>
                <div class="step inner" id="step2-header" style="width:20%;">
                   <div>3</div>
                   <p class="text-center colol-black">Drop Off Address</p>
                </div>
                <div class="step inner" id="step3-header" style="width:20%;">
                   <div>4</div>
                   <p class="text-center colol-black">Package Details</p>
                </div>
                <div class="step last " id="step4-header" style="width:20%;">
                   <div>5</div>
                   <p class="text-center colol-black">Payment</p>
                </div>
             </div>
          </div>

             <!-- <div id="sec1" >  -->
        {!! Form::open(['class'=>'form-vertical','name'=>'prepare_request_form','id' => 'prepare_request_form']) !!}
        <input type="hidden" name="request_type" id="request_type">
        <input type="hidden" name="request_id" id="request_id" value="">
        <input type="hidden" name="user_id" id="user_id">
            <div class="col-md-10 col-sm-offset-1" id="sec0" >

            	<h2 class="col-sm-12 text-primary row" >Customer Detail</h2>
            	<div class="row">
	              <div class="col-sm-6">
	                  <div class="form-group">
	                     <label class="control-label">User</label>
	                     <div class="radio">
	                        <label class="">
	                        <input type="radio" name="user_type" id="existing" value="old_user" checked >
	                        </label>&nbsp;Existing
	                        <label class="">
	                        <input type="radio" name="user_type" id="newuser" value="new_user" >
	                        </label>&nbsp;New
	                     </div>
	                  </div>
	               </div>
	            </div>
	            <div class="col-sm-12 row" id="Existing_user">
                  <div class="form-group">
                    	<div class="col-sm-6">
		                  <div class="form-group">
		                     <label>Email</label>
		                     <input type="text" placeholder="Email" name="existing_user_email" id="existing_user_email"  maxlength= "105" class="form-control required" value="">
		                  </div>
		               </div>
		               {{-- <div class="col-sm-1">
		               		<label> &nbsp;</label>
		               		<button type="button" class="custom-btn1" id="search_user">
					           Search
					           <div class="custom-btn-h"></div>
					        </button>
		               </div> --}}
                  </div>
               </div>
                <div class="row" id="name" style="display:none;">
                 <div class="col-sm-6">
                  <div class="form-group">
                     <label>First Name</label><span class="red-star"> *</span>
                     <input type="text" placeholder="First Name" name="first_name" id="first_name"  maxlength= "125" class="form-control required">
                  </div>
               </div>

                 <div class="col-sm-6">
                  <div class="form-group">
                     <label>Last Name</label><span class="red-star"> *</span>
                     <input type="text" placeholder="Last Name" name="last_name" id="last_name"  maxlength= "125" class="form-control required">
                  </div>
               </div>
               <div class="col-sm-6">
                  <div class="form-group">
                     <label>Email</label><span class="red-star"> *</span>
                     <input type="text" placeholder="Email" name="user_email" id="user_email"  maxlength= "125" class="form-control required">
                     <p class="help-block" id="er_email"> </p>
                  </div>
               </div>
               </div>

			  <div class="">
			     <hr>
			  </div>
			  <div class="">
			     <div class="form-group">
			        <button class="custom-btn1" id="select_user_btn">
			           Next
			           <div class="custom-btn-h"></div>
			        </button>
			     </div>
			  </div>
			</div>

              <div class="col-md-10 col-sm-offset-1" id="sec1" style="display: none;" >
              	<div class="row">
	                  <div class="col-sm-8">
	                     <div class="form-group">
	                        <label>Package Title</label><span class="red-star"> *</span>
	                        {!! Form::text('title','',['class'=>"form-control required usename-#package title#", 'placeholder'=>"Package Title",'maxlength' => 80]) !!}
	                        <input type="hidden" name="PickupLat" id="PickupLat">
	                        <input type="hidden" name="PickupLong" id="PickupLong">
	                        <input type="hidden" name="DeliveryLat" id="DeliveryLat">
	                        <input type="hidden" name="DeliveryLong" id="DeliveryLong">
	                     </div>
	                  </div>
	            </div>
	            <div class="row">
                  <div class="col-md-12">
                    <h3 class="color-blue">Pickup Address</h3>
                  </div>

                  <div class="col-sm-12">
                      <div class="checkbox">
                          <label>
                          <span class="pull-left"><input type="checkbox" name="public_place" checked="checked" ></span>
                           <span class="Insurance_check">  This is public place </span>
                          </label>
                      </div>
                  </div>
	                  <div class="col-sm-6">
	                     <div class="form-group">
	                        <label>Address Line 1</label><span class="red-star"> *</span>
	                        {!! Form::text('address_line_1','',['class'=>"form-control required",'placeholder'=>"Address",'maxlength' => 100,'id' => 'address_line_1'])  !!}
	                     </div>
	                  </div>
	                  <div class="col-sm-6">
	                     <div class="form-group">
	                        <label>Address Line 2</label>
	                        {!! Form::text('address_line_2','',['class'=>"form-control",'placeholder'=>"Address",'maxlength' => 80,'id'=>'address_line_2'])  !!}
	                     </div>
	                  </div>
	              </div>
                  <div class="row">
	                  <div class="col-sm-4">
	                     <div class="form-group">
	                        <label class="control-label">Country</label><span class="red-star"> *</span>
	                        <select name="country" class="form-control required usename-#country#" id="pp_pickup_country" onchange="get_state('pp_pickup_country','pp_pickup_state','pp_pickup_city','pp_pickup_state','','')">

	                            <option value="">Select Country</option>

	                            @foreach($country as $key)
	                             <option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}","state_available":"{{$key->state_available}}"}' >{{$key->Content}}</option>
	                            @endforeach

	                        </select>
	                     </div>
	                  </div>
	                  <div class="col-sm-4">
	                     <div class="form-group">
	                        <label class="control-label">State</label><span class="red-star"> *</span>
	                        <select name="state" class="form-control required left-disabled" id="pp_pickup_state" onchange="get_city('pp_pickup_state','pp_pickup_city','pp_pickup_city','')">
	                            <option value="">Select State</option>
	                        </select>
	                     </div>
	                  </div>
	                  <div class="col-sm-4">
	                     <div class="form-group">
	                        <label class="control-label">City</label><span class="red-star"> *</span>
	                        <select  name="city" class="form-control required" id="pp_pickup_city">
	                            <option value="">Select City</option>
	                        </select>
	                     </div>
	                  </div>
	               </div>
                  <div class="row">
	                  <div class="col-sm-4">
	                     <div class="form-group">
	                        <label>Zip Code/Postcode</label>
	                        {!! Form::text('zipcode','',['class'=>"form-control alpha-numeric  maxlength-8",'placeholder'=>"Zip Code/Postcode",'maxlength' => 8])  !!}
	                     </div>
	                  </div>

	                  <div class="col-sm-4">
	                     <div class="form-group">
	                        <label>Pickup Date</label><span class="red-star"> *</span>
	                        {!! Form::text('pickup_date','',['class'=>"form-control required",'placeholder'=>"Date","id"=>"pickup_date",'readonly'=>'true', 'readonly' => 'true','style'=>"background-color:white !important;"])  !!}
	                     </div>
	                  </div>
	              </div>


                  <div class="">
                     <hr>
                  </div>
                  <div class="">
                    	<div class="row">
	                  		<div class="col-xs-6">
			                     <div class="form-group">
			                        <button class="custom-btn1 btn" id="step1-next-btn">
			                           Next
			                           <div class="custom-btn-h"></div>
			                        </button>
			                        <a class="custom-btn1 btn text-center" onclick="switch_request_header('#sec0','#sec1',0)" href="javascript:void(0)">
			                           Back
                                       <div class="custom-btn-h"></div>
			                        </a>
			                     </div>
			                </div>
	                  	</div>
                    </div>

              </div>
              <!-- End of first section -->
              <!-- Start second section -->
              <div class="" id="sec2" style="display:none;">
                 <div class="col-md-10 col-sm-offset-1">

                 	<div class="">
	                    <div class="col-md-12 row">
	                       <h3 class="color-blue">Drop Off Address</h3>
	                    </div>
	                    <div class="col-sm-6 row">
	                       <div class="form-group">
	                          <label class="control-label">Address Line 1</label><span class="red-star"> *</span>
	                          <input name="drop_off_address_line_1" class="form-control required usename-#address#" placeholder="Address" name="drop_address" maxlength="100" id="drop_off_address_line_1" value="">
	                       </div>
	                    </div>

	                    <div class="col-sm-6">
	                       <div class="form-group">
	                          <label class="control-label">Address Line 2</label>
	                          <input name="drop_off_address_line_2" class="form-control usename-#address#" placeholder="Address" name="drop_address2"  maxlength="100" id="drop_off_address_line_2" value="">
	                       </div>
	                    </div>

	                </div>
	                <div class="clearfix"></div>
                    <div class="row">
	                  <div class="col-sm-4">
	                     <div class="form-group">
	                        <label class="control-label">Country</label><span class="red-star"> *</span>
	                        <select name="drop_off_country" class="form-control required usename-#country#" id="pp_dropoff_country" onchange="get_state('pp_dropoff_country','pp_dropoff_state','pp_dropoff_city','pp_dropoff_state','','')">

	                            <option value="">Select Country</option>
	                            @foreach($country as $key)
	                             <option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}","state_available":"{{$key->state_available}}"}' >{{$key->Content}}</option>
	                            @endforeach
	                        </select>
	                     </div>
	                  </div>
	                  <div class="col-sm-4">
	                     <div class="form-group">
	                        <label class="control-label">State</label><span class="red-star"> *</span>
	                        <select name="drop_off_state" class="form-control required usename-#state# left-disabled" id="pp_dropoff_state" onchange="get_city('pp_dropoff_state','pp_dropoff_city','pp_dropoff_city','')">
	                            <option value="">Select State</option>
	                        </select>
	                     </div>
	                  </div>
	                  <div class="col-sm-4">
	                     <div class="form-group">
	                        <label class="control-label">City</label><span class="red-star"> *</span>
	                        <select  name="drop_off_city" class="form-control required usename-#city#" id="pp_dropoff_city">
	                            <option value="">Select City</option>
	                        </select>
	                     </div>
	                  </div>
	               </div>
	               <div class="">
	                    <div class="col-sm-4 row">
	                       <div class="form-group">
	                          <label class="control-label">Zip Code/Postcode</label>
	                          <input class="form-control alpha-numeric usename-#zipcode#" placeholder="Zip Code/Postcode" name="drop_off_zipcode" maxlength="8" value="">
	                       </div>
	                    </div>
	                    <div class="col-sm-4">
	                       <div class="form-group">
	                          <label class="control-label">Date</label><span class="red-star"> *</span>
	                          <input class="form-control required usename-#date#" placeholder="Date" name="drop_off_date" id="drop_off_date" readonly value="" style="background-color:white !important;">
	                       </div>
	                    </div>
	                </div>
	                <div class="">
	                    <div class="col-sm-12 row">
	                       <div class="form-group">
	                          <label class="control-label">Is Delivery Date Flexible?</label>
	                          <div class="checkbox">
	                             <label>
	                             <span class="pull-left row"><input type="radio" name="is_delivery_date_flexible" value="yes" ></span>  <span class="Insurance_check" >  Yes </span>
	                             </label>
	                             &nbsp; &nbsp;
	                             <label>
	                             <span class="pull-left"><input type="radio" name="is_delivery_date_flexible" value="no" checked="checked" ></span>  <span class="Insurance_check"  >  No </span>
	                             </label>
	                          </div>
	                       </div>
	                    </div>
	                    <div class="col-sm-12 row">
	                       <div class="form-group">
	                          <label class="control-label">Journy Type</label>
	                          <div class="checkbox">
	                             <label>
	                             <div class="pull-left row"><input type="radio" value="one_way" name="journey_type" checked="checked" onclick="$('#return_jurney_section').hide()" id="one_way_journey"></div>  <span class="Insurance_check">  One Way </span>
	                             </label>
	                             &nbsp; &nbsp;
	                             <label>
	                             <span class="pull-left"><input type="radio" value="return" name="journey_type" onclick="$('#return_jurney_section').show()" id="return_journy"></span>  <span class="Insurance_check">  Return  </span>
	                             </label>
	                          </div>
	                       </div>
	                    </div>
	                </div>
	                <div id="return_jurney_section"  style="display:none;">
	                	<div class="row">
		                	<div class="col-sm-12">
		                       <div class="form-group">
		                       		<h3 class="color-blue">Return Address</h3>
		                          <div class="checkbox">
		                             <label>
		                             	<span class="pull-left"><input type="checkbox" id="return_same_as_pickup" name="return_same_as_pickup" ></span>
		                             	     <span class="Insurance_check">  Same as pickup  adddress </span>
 		                             </label>
		                          </div>
		                       </div>
		                    </div>
		                </div>
	                   	<div id="return_jurney_address">
			               	<div class="row">
			                    <div class="col-sm-6">
			                       <div class="form-group">
			                          <label class="control-label">Address Line 1</label><span class="red-star"> *</span>
			                        	<input name="return_address_line_1" class="form-control required usename-#address line 1#" placeholder="Address" name="return_address" maxlength="100" value="">
			                       </div>
			                    </div>
			                    <div class="col-sm-6">
			                       <div class="form-group">
			                          <label class="control-label">Address Line 2</label>
			                          <input name="return_address_line_2" class="form-control usename-#address line 2#" placeholder="Address" name="return_address2" maxlength="100" value="">
			                       </div>
			                    </div>
			                </div>
		                    <div class="row">
			                  <div class="col-sm-4">
			                     <div class="form-group">
			                        <label class="control-label">Country</label><span class="red-star"> *</span>
			                        <select name="return_country" class="form-control required usename-#country#" id="pp_return_country" onchange="get_state('pp_return_country','pp_return_state','pp_return_city','pp_return_state','','')">
			                            <option value="">Select Country</option>
			                            @foreach($country as $key)
			                             <option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}","state_available":"{{$key->state_available}}"}' >{{$key->Content}}</option>
			                            @endforeach


			                        </select>
			                     </div>
			                  </div>
			                  <div class="col-sm-4">
			                     <div class="form-group">
			                        <label class="control-label">State</label><span class="red-star"> *</span>
			                        <select name="return_state" class="form-control required usename-#state# left-disabled" id="pp_return_state" onchange="get_city('pp_return_state','pp_return_city','pp_return_city','')">
			                            <option value="">Select State</option>
			                        </select>
			                     </div>
			                  </div>
			                  <div class="col-sm-4">
			                     <div class="form-group">
			                        <label class="control-label">City</label><span class="red-star"> *</span>
			                        <select  name="return_city" class="form-control required usename-#city#" id="pp_return_city">
			                            <option value="">Select City</option>
			                        </select>
			                     </div>
			                  </div>
			               </div>
			               <div class="row">
			                    <div class="col-sm-4">
			                       <div class="form-group">
			                          <label class="control-label">Zip Code/Postcode</label>
			                          <input class="form-control alpha-numeric usename-#zipcode#" placeholder="Zip Code/Postcode" name="return_zipcode" maxlength="8" value="">
			                       </div>
			                    </div>
			                </div>
		                </div>
	             	</div>

	                <div class="row">
	                	<div class="col-sm-12">
	                       <div class="form-group">
	                       		<h3 class="color-blue">Return address(If item is not delivered)</h3>
	                          <div class="checkbox curtomlabel">
	                             <label>
	                             	<input type="checkbox" id="same_as_pickup" name="same_as_pickup" checked="checked" >
	                             	Same as pickup adddress
	                             </label>
	                          </div>
	                       </div>
	                    </div>
	                </div>

	                <div id="return_address_action" style="display: none;"  >
		               	<div class="row">
		                    <div class="col-sm-6">
		                       <div class="form-group">
		                          <label class="control-label">Address Line 1</label>
		                          <input value='' name="nd_return_address_line_1" class="form-control required usename-#address line 1#" placeholder="Address" maxlength="100" >


		                       </div>
		                    </div>
		                    <div class="col-sm-6">
		                       <div class="form-group">
		                          <label class="control-label">Address Line 2</label>
		                          <input value="" name="nd_return_address_line_2" class="form-control usename-#address line 2#" placeholder="Address" maxlength="100">



		                       </div>
		                    </div>
		                </div>
	                    <div class="row">
		                  <div class="col-sm-4">
		                     <div class="form-group">
		                        <label class="control-label">Country</label>
		                        <select name="nd_return_country" class="form-control required usename-#country#" id="pp_nd_return_country" onchange="get_state('pp_nd_return_country','pp_nd_return_state','pp_nd_return_city','pp_nd_return_state','','')">
		                            <option value="">Select Country</option>			                            @foreach($country as $key)
			                            <option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}","state_available":"{{$key->state_available}}"}' >{{$key->Content}}</option>
			                        @endforeach
		                        </select>
		                     </div>
		                  </div>
		                  <div class="col-sm-4">
		                     <div class="form-group">
		                        <label class="control-label">State</label>
		                        <select name="nd_return_state" class="form-control required usename-#state# left-disabled" id="pp_nd_return_state" onchange="get_city('pp_nd_return_state','pp_nd_return_city','pp_nd_return_city','')">
		                            <option value="">Select State</option>
		                        </select>
		                     </div>
		                  </div>
		                  <div class="col-sm-4">
		                     <div class="form-group">
		                        <label class="control-label">City</label>
		                        <select  name="nd_return_city" class="form-control required usename-#city#" id="pp_nd_return_city">
		                            <option value="">Select City</option>
		                        </select>
		                     </div>
		                  </div>
		               </div>
		               <div class="row">
		                    <div class="col-sm-4">
		                       <div class="form-group">
		                          <label class="control-label">Zip Code/Postcode</label>
		                          <input class="form-control alpha-numeric usename-#zipcode#" placeholder="Zip Code/Postcode" name="nd_return_zipcode" maxlength="8" value="">
		                       </div>
		                    </div>
		                </div>
	             	</div>
                    <div class="">
                       <hr>
                    </div>
                    <div class="">
                    	<div class="row">
	                  		<div class="col-xs-6">
			                     <div class="form-group">
			                        <button class="custom-btn1 btn" id="step2-next-btn">
			                           Next
			                           <div class="custom-btn-h"></div>
			                        </button>
			                        <a class="custom-btn1 btn text-center" onclick="switch_request_header('#sec1','#sec2',1)" href="javascript:void(0)">
			                           Back
                                       <div class="custom-btn-h"></div>
			                        </a>
			                     </div>
			                </div>
	                  	</div>
                    </div>

                 </div>
              </div>

              <!-- End section2 -->
              <!-- Start section3 -->

              <div class="row" id="sec3" style="display: none;">

                 <div class="col-md-10 col-sm-offset-1">
                    <!--  <form class="form-vertical">    -->


                    <div class="col-sm-6 col-xs-6">
	                    <div class="form-group">
	                          <label class="control-label">Package Value</label>
	                          <div class="input-group error-input">
	                          <span class="input-group-addon">$</span>
	                          <input class="form-control required float maxlength-7 " placeholder="Package Value" name="package_value" maxlength="9" >
	                          </div>
	                    </div>
	                </div>


                    <div class="col-sm-12">
                       <div class="form-group">
                          <label class="control-label">Measurement Unit</label>
                          <div class="clearfix">
                             <label class="radio-inline">
                             	<input type="radio" name="measurement_unit"  value="cm_kg"  onclick="return unit_show();">Metric (Cm/Kg)
                             </label>
                             <label class="radio-inline">
                             	<input type="radio" name="measurement_unit" value="inches_lbs" onclick="return unit_show();" checked >Imperial (Inches/lbs)
                             </label>
                          </div>
                       </div>
                    </div>

                    <div class="col-md-2 col-sm-3 col-xs-12">
                       <div class="form-group">
                          <label class="control-label">Length</label><span class="red-star"> *</span>
                          <div class="input-group error-cm-input">
                          <input class="form-control required  float maxlength-9" placeholder="Length" name="length" maxlength="9">
                          <span class="input-group-addon" id="length_unit"></span>
                          </div>
                       </div>
                    </div>

                    <div class="col-md-2 col-sm-3 col-xs-12">
                       <div class="form-group">
                          <label class="control-label">Width</label><span class="red-star"> *</span>
                          <div class="input-group error-cm-input">
                          <input class="form-control required float maxlength-9" placeholder="Width" name="width" maxlength="9">
                          <span class="input-group-addon" id="width_unit"></span>
                          </div>
                       </div>
                    </div>
                    <div class="col-md-2 col-sm-3 col-xs-12">
                       <div class="form-group">
                          <label class="control-label">Height</label><span class="red-star"> *</span>
                          <div class="input-group error-cm-input">
                          <input class="form-control required float maxlength-9" placeholder="Height" name="height" maxlength="9">
                          <span class="input-group-addon" id="height_unit"></span>
                          </div>
                       </div>
                    </div>
                    <div class="col-md-2 col-sm-3 col-xs-12">
                       <div class="form-group">
                          <label class="control-label">Weight</label><span class="red-star"> *</span>
                          <div class="input-group error-cm-input">
                          <input class="form-control required float maxlength-9" placeholder="Weight" name="weight" maxlength="9">
                          <span class="input-group-addon" id="weight_unit"></span>
                          </div>
                       </div>
                    </div>


	                <div class="col-sm-12 col-xs-12">
                       	<div class="form-group">
                          <label class="control-label">Quantity</label>


                          	<div class="radio">
	                            <label class="">
	                             	<input type="radio" name="quantity_type" value="single" onclick="show_quantity();" checked="true">
	                            </label>&nbsp;Single
                             	<label class="">
                             		<input type="radio" name="quantity_type" value="multiple" onclick="show_quantity();">
                             	</label>&nbsp;Multiple
                  			</div>


                       	</div>
		            </div>

		            <div class="col-sm-12 col-xs-12" id="quantity_input">
                       		<div class="form-group">
		              		<input type="text" class="form-control required numeric" name="quantity" placeholder="Quantity" >
		              		</div>
		            </div>





                    <div class="col-sm-6">
                       <div class="form-group">
                          <label class="control-label">Shipping Mode</label>
                          <div class="clearfix">
                             <label class="radio-inline">
                             	<span class="pull-left"><input type="radio" name="travel_mode" value="air" onclick="toggle_category('.travel-mode-air','.travel-mode-ship','#package_category')" checked="checked" ></span> <span class="Insurance_check">  By Air </span>
                             </label>
                             <label class="radio-inline">
                             	<span class="pull-left"><input type="radio" name="travel_mode" value="ship" onclick="toggle_category('.travel-mode-ship','.travel-mode-air','#package_category')">
                             	  </span> <span class="Insurance_check"> By Sea  </span>
                             </label>
                          </div>
                       </div>
                    </div>
                    <div class="col-sm-6">
                       <div class="form-group">
                          <label class="control-label">Select Package Category</label><span class="red-star"> *</span>
                          <select class="form-control required" id="package_category" name="category">
                             <option value="">Select Category</option>

                             <?php foreach ($category as $key) {?>

                             	<option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}"}'  class="travel-mode-{{$key->TravelMode}}">{{$key->Content}}
                             	</option>

                             <?php }?>
                          </select>
                       </div>
                    </div>
                    <div class="col-sm-12">
                       <div class="form-group">
                          <label class="control-label ">Package Content/Description/Instructions</label>
                          <textarea rows="3" class="form-control" name="description"></textarea>
                       </div>
                    </div>

					<!-- <div class="col-sm-12">
						<label class="control-label">Upload Default Package Image</label><span class="red-star"> *</span>
						<div class="selected-pic"> <div>
				           <img id="senior-preview" src="{{ ImageUrl}}/no-image.jpg"  width="200px" height="150px" />
						</div>
					    <label class="custom-input-file">
					    	{!! Form::file('default_image', ['class'=> 'required custom-input-file','id'=>'senior_image','onchange'=>"image_preview(this,'senior-preview',event)"]) !!}
					    </label>
					    <br>
					</div> -->
					<div class="col-sm-12">
                       	<div class="form-group">
                          <label class="control-label">Upload Default Package Image</label><span class="red-star"> *</span>
                          <input type="file" placeholder="Browse" name="default_image" class="required valid-filetype-jpg,png,gif,jpeg,JPEG,JPG,PNG,GIF">
                       	</div>
                    </div>


                  		<div class="col-sm-12">
                       <div class="form-group">
                          <label class="control-label">Upload Extra Image(s)(Optional)</label>
                          <div class="row">
	                          <div class="col-sm-3">
	                          	<input  type="file" placeholder="Browse" name="package_image2" class="valid-filetype-jpg,png,gif,jpeg,JPG,PNG,GIF,JPEG">

	                          </div>
	                          <div class="col-sm-3">
	                          	<input type="file" placeholder="Browse" name="package_image3" class="valid-filetype-jpg,png,gif,jpeg,JPG,PNG,GIF,JPEG">
	                          </div>
	                          <div class="col-sm-3">
	                          	<input type="file" placeholder="Browse" name="package_image4" class="valid-filetype-jpg,png,gif,jpeg,JPG,PNG,GIF,JPEG">
	                          </div>
	                          <div class="col-sm-3">
	                          	<input type="file" placeholder="Browse" name="package_image5" class="valid-filetype-jpg,png,gif,jpeg,JPG,PNG,GIF,JPEG">
	                          </div>
	                      </div>
                       </div>
                    </div>

                    <div class="col-sm-6">
                       <div class="form-group">
                          <label class="control-label">Receiver's Phone number</label><span class="red-star"> *</span>
                          <div class="row">
                             <div class="col-xs-4">
                                <input type="text" class="form-control required numeric maxlength-3" placeholder="Country Code" name="country_code" maxlength="3" value="">
                             </div>
                             <div class="col-xs-8">
                                <input type="text" class="form-control required numeric between-8-12" name="phone_number" placeholder="Phone Number" value="" pattern=".{8,12}" title="8 to 12 numbers">
                             </div>
                          </div>
                       </div>
                    </div>



                    <div class="col-sm-12">
                       <div class="form-group">
                          <label class="control-label">Insurance</label>
                          <div class="checkbox">
                             <label>
                             	<span class="pull-left row"><input type="radio" name="insurance" value="yes" ></span> <span class="Insurance_check"> Yes </span>
                             </label>
                             &nbsp; &nbsp;
                             <label>
                             	<span class="pull-left"><input type="radio" name="insurance" value="no" checked ></span> <span class="Insurance_check"> No </span>
                             </label>
                          </div>
                       </div>
                       <div class="form-group">
                          <div class="checkbox">
                             <label>
                             	<span class="pull-left"><input type="checkbox" value="yes" name="need_package_material" ></span> <span class="Insurance_check"> Need package material </span>
                             </label>
                          </div>
                       </div>

                        <div class="form-group">
                          <div class="checkbox">
                             <label>
                             	<span class="pull-left"><input type="checkbox" id="agree" name="terms_conditions"></span> <span class="Insurance_check"> I Accept  <a href="{{url('terms-and-conditions')}}" target="_blank"> Terms and Conditions </a> </span>
                             </label>
                          </div>
                       </div>
                    </div>
                    <div class="col-md-12">
                       <hr>
                    </div>
                    <div class="">
                    	<div class="">

	                  		<div class="col-xs-12">
			                     <div class="form-group">
			                        <button class="custom-btn1 btn"  id="calculate_loader">
			                           Next
			                           <div class="custom-btn-h"></div>
			                        </button>
			                         <a class="custom-btn1 btn  text-center" onclick="switch_request_header('#sec2','#sec3',2)" href="javascript:void(0)">
			                           Back
                                       <div class="custom-btn-h"></div>
			                        </a>
			                     </div>
			                </div>

	                  </div>
                    </div>
                 </div>
              </div>
              <!-- End section 3 -->

              <!-- Start section 4 -->
              <div class="row" id="sec4" style="display:none;">
                <div class="col-md-10 col-sm-offset-1">
                   <div class="col-sm-12">
                   		<div id="shipping_detail">
                   			Calculating! Please wait...
                   		</div>

                      <div class="">
                    	<div class="">
	                  		<div class="row">
			                     <div class="form-group">
			                        <button class="custom-btn1 btn" id="item_loader">
			                           Next
			                           <div class="custom-btn-h"></div>
			                        </button>
			                         <a class="custom-btn1 btn text-center" onclick="switch_request_header('#sec3','#sec4',3)" href="javascript:void(0)">
			                           Back
                                       <div class="custom-btn-h"></div>
			                        </a>
			                     </div>
			                </div>

	                  	</div>
                       </div>
                	</div>
                </div>
              </div>

                <!-- End section 4 -->
             {!! Form::close() !!}
        </div>
     </div>
  </div>
</div>


{!! Form::close() !!}
<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyCT0uj-bffWWdOdwDp15LvrMko-OuoZUoY"></script>

{!! Html::script('theme/admin/custome/js/validation1.js') !!}
{!! Html::script('theme/admin/custome/js/utility.js') !!}
{!! Html::script('theme/admin/custome/js/request.js') !!}
{!! Html::style('theme/date-time-picker/jquery.datetimepicker.css') !!}
{!! Html::script('theme/date-time-picker/jquery.datetimepicker.js') !!}

@include('Admin::layouts.footer')


<script>

$('#pp_pickup_country').trigger('change');
$('#pp_dropoff_country').trigger('change');
$('#pp_nd_return_country').trigger('change');
$('#pp_return_country').trigger('change');


$(document).ready(function() {

    $(".fancybox").fancybox();

  });

$(function() {
       var action;
       $(".number-spinner a").mousedown(function () {
           btn = $(this);
           input = btn.closest('.number-spinner').find('input');
           btn.closest('.number-spinner').find('a').prop("disabled", false);

           if (btn.attr('data-dir') == 'up') {
               action = setInterval(function(){
                   if ( input.attr('max') == undefined || parseInt(input.val()) < parseInt(input.attr('max')) ) {
                       input.val(parseInt(input.val())+1);
                   }else{
                       btn.prop("disabled", true);
                       clearInterval(action);
                   }
               }, 50);
           } else {
               action = setInterval(function(){
                   if ( input.attr('min') == undefined || parseInt(input.val()) > parseInt(input.attr('min')) ) {
                       input.val(parseInt(input.val())-1);
                   }else{
                       btn.prop("disabled", true);
                       clearInterval(action);
                   }
               }, 50);
           }
       }).mouseup(function(){
           clearInterval(action);
       });
   });

$('#pickup_date').datetimepicker({

   format:'M d, Y h:i A',
   formatTime:'h:i A',
   timepicker:true,
   datepicker:true,
   minDate : new Date(),
   onChangeDateTime:function( ct ){
       $('#drop_off_date').datetimepicker({  minDate:ct  })
   },
});

$('#drop_off_date').datetimepicker({
   format:'M d, Y h:i A',
   formatTime:'h:i A',
   timepicker:true,
   datepicker:true,
   minDate : new Date(),
   onChangeDateTime:function( ct ){
       $('#pickup_date').datetimepicker({  maxDate:ct  })
   },
});


</script>

<script type="text/javascript">



function journey_show()
{
	if($('[name="journey_type"]:checked').val() == "return")
		{
			$("#return_journy").trigger("onclick");
		}
}

journey_show();

function image_preview(obj,previewid,evt)
{
    var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
    if ($.inArray($(obj).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
       $(obj).val('');
       $('#'+previewid).attr('src', '{{ImageUrl}}/user-no-image.jpg');
        alert("Only "+fileExtension.join(', ')+" formats are allowed.");
     } else {

       var file = evt.target.files[0];
       if (file)
       {
         var reader = new FileReader();

         reader.onload = function (e) {
             $('#'+previewid).attr('src', e.target.result)
         };
         reader.readAsDataURL(file);
       }
     }
}

function toggle_category(showid, hideid, id) {
	$(id).val('');
	$(showid).attr('disabled', false);
	$(showid).show();
	$(hideid).attr('disabled', true);
	$(hideid).hide();
}


toggle_category('.travel-mode-air','.travel-mode-ship','#package_category');


function show_quantity()
{
	var type =$('input[name=quantity_type]:checked').val();
	if(type == 'multiple'){
		$('#quantity_input').show();
	}else{
		$('#quantity_input').hide();
	}

}
show_quantity();


function unit_show()
{
	var unit =$('input[name=measurement_unit]:checked').val();

	if(unit == 'cm_kg'){
		$('#length_unit').html('Cm');
		$('#width_unit').html('Cm');
		$('#height_unit').html('Cm');
		$('#weight_unit').html('Kg');

	}else{
		$('#length_unit').html('Inches');
		$('#width_unit').html('Inches');
		$('#height_unit').html('Inches');
		$('#weight_unit').html('lbs');
	}

}

unit_show();
</script>

@endsection
