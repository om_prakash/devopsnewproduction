@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')

@section('content')

<div class="container-fluid">
<div class="panel panel-default">
  <div class="panel-heading"><i class="fa fa-pencil-square-o"></i>&nbsp;Add Admin User</div>
  <div class="panel-body">

  	<div class="row">
  		<div class="col-md-12">
		{!! Form::model('', ['name'=>'add_admin_user','method' => 'POST', 'url' => ['admin/add_admin_user']]) !!}

     	  <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}"  >
		     {!! Form::label('name', 'Name',['class' => 'col-md-2 text-right']) !!}
		     <div class="col-md-6" style="color:#a44423">
				{!! Form::text('name', Input::old('name'), ['class'=>'form-control required', 'placeholder'=> 'Name','id' =>'name']) !!}
				@if ($errors->has('name')) <p class="help-block" style="color:#a44423">{{ $errors->first('name') }}</p> @endif
			</div>
		     <div class="clearfix"></div>
         </div>
         <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}"  >
		     {!! Form::label('email', 'Email',['class' => 'col-md-2 text-right']) !!}
		     <div class="col-md-6" style="color:#a44423">
				{!! Form::text('email', Input::old('email'), ['class'=>'form-control required valid-email', 'placeholder'=> 'Email','id' =>'email']) !!}
				@if ($errors->has('email')) <p class="help-block">{{ $errors->first('email') }}</p> @endif
			</div>
		     <div class="clearfix"></div>
         </div>
         <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}"  >
		     {!! Form::label('password', 'Password',['class' => 'col-md-2 text-right']) !!}
		     <div class="col-md-6" style="color:#a44423">
				{!! Form::password('password',['class'=>'form-control required', 'placeholder'=> 'Password','id' =>'password']) !!}
				@if ($errors->has('password')) <p class="help-block">{{ $errors->first('password') }}</p> @endif
			</div>
		     <div class="clearfix"></div>
         </div>
         <div class="form-group {{ $errors->has('conf_password') ? ' has-error' : '' }}"  >
		     {!! Form::label('Conf_password', 'Confirm Password',['class' => 'col-md-2 text-right']) !!}
		     <div class="col-md-6" style="color:#a44423">
				{!! Form::password('confirm_password', ['class'=>'form-control required', 'placeholder'=> 'Confirm Password','id' =>'conf_password']) !!}
				@if ($errors->has('confirm_password')) <p class="help-block">{{ $errors->first('confirm_password') }}</p> @endif
			</div>
		     <div class="clearfix"></div>
         </div>

        <div class="row">
         <div class="col-md-2 text-right">
         	{!! Form::label('Permission', 'Permission') !!}
         </div>
         <div class="col-md-8">
			 <b><a href="javascript:void(0)" onclick="check_all()" id="check_control">
			 	Check All</a></b>
			 <br />

			<div class="row">
			 	<div class="col-md-4">
			       <div><h4 class="headclass">User</h4></div>
			     	<input type="checkbox" class="permission"  name="sender" value="{{SENDER}}"  > &nbsp;{!! Form::label('sender', 'Transporter') !!}
					<br />

					<input type="checkbox" class="permission"  name="carrier" value="{{CARRIER}}" > &nbsp;{!! Form::label('carrier', 'Requester') !!}
					<br />

					<input type="checkbox" class="permission" name="admin" value="{{ADMIN}}" >
					&nbsp;{!! Form::label('admin', 'Admin') !!}
					<br />
	           </div>

	           <div class="col-md-4">
		        <h4 class="headclass">Package</h4>
				<input type="checkbox" class="permission" name="package" value="{{PACKAGE}}"  > &nbsp;{!! Form::label('package', 'Send a Package') !!}
				<br />
				<input type="checkbox" class="permission" name="onlinepackage" value="{{ONLINEPACKAGE}}"  > &nbsp;{!! Form::label('onlinepackage', 'Online Package') !!}
				<br />

				<input type="checkbox" class="permission" name="buyforme" value="{{BUYFORME}}"  > &nbsp;{!! Form::label('buyforme', 'Buy For Me') !!}
				<br />

			</div>

			<div class="col-md-4">
		    <div><h4 class="headclass">Transaction</h4></div>
			<input type="checkbox" class="permission" name="transaction" value="{{TRANSACTION}}"  > &nbsp;{!! Form::label('transaction',
			'Transporter') !!}
				<br />

		   <input type="checkbox" class="permission" name="transaction2" value="{{TRANSACTION2}}"  > &nbsp;{!! Form::label('transaction',
		   'Requester') !!}
				<br />
		  </div>
     </div>

           	<div class="row">
           		<div class="col-md-4">
		         <h4 class="headclass">Content</h4>
				<input type="checkbox" class="permission" name="app_content" value="{{APP_CONTENT}}"  > &nbsp;{!! Form::label('app_content', 'App Content') !!}
				<br />

				<input type="checkbox" class="permission" name="web_content" value="{{WEB_CONTENT}}"  > &nbsp;{!! Form::label('web_content', 'Web Content') !!}
				<br />
				<input type="checkbox" class="permission" name="app_tutorial" value="{{APP_TUTORIAL}}"  > &nbsp;{!! Form::label('app_tutorial', 'App Tutorial') !!}
				<br />
				<input type="checkbox" class="permission" name="about_app_content" value="{{ABOUT_APP}}" > &nbsp;{!! Form::label('about_app_content', 'About App') !!}
				<br />

			    <input type="checkbox" class="permission"  name="email_template" value="{{EMAIL_TEMPLATE}}" > &nbsp;{!! Form::label('email_template', 'Email Template') !!}
				<br />

				<input type="checkbox" class="permission" name="faq" value="{{FAQ}}"  > &nbsp;{!! Form::label('faq', 'Faq') !!}
                 <br/>

					<input type="checkbox" class="permission" name="aquantuo_address" value="{{AQUANTUO_ADDRESS}}"  > &nbsp;{!! Form::label('aquantuo_address', 'Aquantuo Address') !!}
			</div>

			  <div class="col-md-4">
              <div><h4 class="headclass">Management</h4></div>

				<input type="checkbox" class="permission" name="country" value="{{COUNTRY}}"  > &nbsp;{!! Form::label('country', 'Country') !!}
				<br />
				<input type="checkbox" class="permission" name="state" value="{{STATE}}"  > &nbsp;{!! Form::label('state', 'State') !!}
				<br />
				<input type="checkbox" class="permission" name="city" value="{{CITY}}" > &nbsp;{!! Form::label('city', 'City') !!}
				<br />

	           <input type="checkbox" class="permission" name="item_category" value="{{ITEMCATEGORY}}" > &nbsp;{!! Form::label('itemcategory', 'Item Category') !!}
				<br />

				<input type="checkbox" class="permission" name="promocode" value="{{PROMOCODE}}"  > &nbsp;{!! Form::label('promocode', 'Promocode') !!}
				<br />
			</div>

			<div class="row">
			 <div class="col-md-4">
		       <div><h4 class="headclass">Trip</h4></div>
		     		<input type="checkbox" class="permission" name="bussiness_trip" value="{{BUSSINESS_TRIP}}" > &nbsp;{!! Form::label('bussinesstrip', 'Bussiness Trip') !!}
				<br />
				<input type="checkbox" class="permission" name="individual_trip" value="{{INDIVIDUAL_TRIP}}" > &nbsp;{!! Form::label('individualtrip', 'Individual Trip') !!}
				<br>
           </div>
          </div>

			<div class="col-md-4">
				 <div><h4 class="headclass">Notification</h4></div>
				<input type="checkbox" class="permission" name="notification" value="{{NOTIFICATION}}"  > &nbsp;{!! Form::label('notification', 'Notification') !!}
				<br />
				</div>

              <div class="col-md-4">
				 <div><h4 class="headclass">Configuration</h4></div>
					<input type="checkbox" class="permission" name="congiguration" value="{{CONFIGURATION}}" > &nbsp;{!! Form::label('congiguration', 'Configuration') !!}
				<br />
				</div>


				<div class="col-md-4">
				 <div><h4 class="headclass">Support</h4></div>
					<input type="checkbox" class="permission" name="support" value="{{SUPPORT}}" > &nbsp;{!! Form::label('support',
					'Support') !!}
				<br />
				</div>
		    </div>

            <div class="row">
			    <div class="col-md-4">
				 <div><h4 class="headclass">Setting</h4></div>
				 <input type="checkbox" class="permission" name="setting" value="{{SETTING}}">
				 &nbsp;{!! Form::label('setting', 'Setting') !!}
				<br />
				</div>


				<div class="col-md-4">
				 <div><h4 class="headclass">Dashboard</h4></div>
				 <input type="checkbox" class="permission" name="dashbord" value="yes">
				 &nbsp;{!! Form::label('Dashbord', 'Dashboard') !!}
				<br />
				</div>

				<div class="col-md-4">
				 	<div><h4 class="headclass">Slip Management</h4></div>
				 	<input type="checkbox" class="permission" name="delivery_slip" value="yes"  > &nbsp;{!! Form::label('Delivery Slip', 'Delivery Slip') !!}
					<br />
					<input type="checkbox" class="permission" name="packag_slip" value="yes" > &nbsp;{!! Form::label('Packing Slip', 'Packing Slip') !!}
					<br />
					<input type="checkbox" class="permission" name="received_slip" value="yes"  > &nbsp;{!! Form::label('Received Slip', 'Received Slip') !!}
				</div>
		   </div>

		     <div class="clearfix"></div>
         </div>
          <div class="form-group">
			  <div class="col-md-10 col-md-offset-2">
				{!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
				<a href="{{'admin_user'}}" class='btn btn-danger'>Cancel</a>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>



      <!-- Pagination Section-->




  </div> <!-- Panel Body -->
</div>


</div>

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&key=AIzaSyCT0uj-bffWWdOdwDp15LvrMko-OuoZUoY"></script>



{!! Html::script('theme/web/js/utility.js') !!}
{!! Html::script('theme/admin/custome/js/validate.js') !!}
{!! Html::script('theme/admin/custome/js/request.js') !!}
<script type="text/javascript">
var status = 'true';
function check_all()
{
	if(status == 'true'){
		$('.permission').prop('checked',true);
		status = 'false';
		$('#check_control').html('Uncheck All');
	}else{
		$('.permission').prop('checked',false);
		$('#check_control').html('Check All');
		status = 'true';
	}
}
</script>

@include('Admin::layouts.footer')
@stop
