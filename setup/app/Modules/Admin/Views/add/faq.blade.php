@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')

@section('content')
{!! Html::style('theme/admin/custome/css/sumoselect.css') !!}

<style>
.hideOverflow {
	width: 100%;
	overflow: hidden;
	max-width: 100%;
	white-space: nowrap;
	text-overflow: ellipsis;
	display: block;
	padding-left:30px;
	font-weight: normal;
}
.span3 .checkbox input {
	margin-left: 3px;
	margin-top: 2px;
}

</style>
<div class="container-fluid">
<div class="panel panel-default">
  <div class="panel-heading"><i class="fa fa-plus"></i>&nbsp;Faq</div>
  <div class="panel-body">

  	<div class="row">
  		<div class="col-md-12">
		{!! Form::model('', ['method' => 'POST', 'url' => ['admin/post-add-faq']]) !!}

     	  <div class="form-group {{ $errors->has('section_name') ? ' has-error' : '' }}"  >
		     {!! Form::label('Select Section', '',['class' => 'col-md-2 text-right']) !!}
		     <div class="col-md-10">
              <select class="form-control" name="section_name">
              	@foreach($section as $value)
              	 <option value="{{$value->id}}" @if($value->id == $redirect_id)  selected @endif>{{ucfirst($value->section_name)}}</option>
              	 @endforeach
              </select>
			</div>
		     <div class="clearfix"></div>
         </div>
         <div>
          <input type="hidden" name="related_page" value="{{$redirect_id}}">



     	  <div class="form-group {{ $errors->has('Question') ? ' has-error' : '' }}"  >
		     {!! Form::label('Question', '',['class' => 'col-md-2 text-right']) !!}
		     <div class="col-md-10">
				{!! Form::textarea('Question', '', ['class'=>'form-control', 'placeholder'=> 'Question','id' =>'inputError1','rows'=>'2']) !!}
				@if ($errors->has('Question')) <p class="help-block">{{ $errors->first('Question') }}</p> @endif
			</div>
		     <div class="clearfix"></div>
         </div>

         <div class="form-group {{ $errors->has('Answer') ? ' has-error' : '' }}">
		     {!! Form::label('Answer', 'Answer',['class' => 'col-md-2 text-right']) !!}
		     <div class="col-md-10" style="color:#a94442">
				 {!! Form::textarea('Answer', '', ['class'=>'form-control', 'placeholder'=> 'Answer','id'=>'TBody']) !!}

				@if ($errors->has('Answer')) <p class="help-block">{{ $errors->first('Answer') }}</p> @endif
				</span>
			</div>
			<div class="clearfix"></div>
         </div><br/>

         {!! Form::label('Related Question', 'Related Question',['class' => 'col-md-2 text-right']) !!}
		 <div class="col-md-10" style="color:#a94442">
				<select id="aircategory" class="form-control testselect3" name="related_question[]" multiple="multiple" >
					@foreach($question as $value)
					<option value="{{$value->id}}" >{{$value->question}}  </option>
					@endforeach
				</select>
			</div>
			<div class="clearfix"></div>
		</div><br/>

          {!! Form::label('Related Question', 'Related Question',['class' => 'col-md-2 text-right']) !!}
         <div class="col-md-10">
         	<div class="row">
           	@foreach($question as $value)
            <div class="col-sm-6">
        		  <div class="span3 userSpan2" data-toggle="tooltip" data-placement="top"  id="NoficationBox{{$value->id}}">
                <label class="checkbox" >
               <?php echo Form::checkbox('chk_del[]', "$value->id", false, ['class' => 'chk_del', 'id' => "chk_del"]); ?>

            		<span for="name"  class="hideOverflow">{{$value->question}}</span>
            		</label>
        		</div>
		    	</div>
        		@endforeach
        </div>
    </div>

         {!! Form::label('', '',['class' => 'col-md-2 text-right']) !!}
         <div class="col-md-10">
               <span>{!! $question->render() !!}</span>
            </div>


          <div class="form-group">
			  <div class="col-md-10 col-md-offset-2">
				{!! Form::submit('Add', ['class' => 'btn btn-primary']) !!}
				<a href = "{{url('admin/faq/'.Request::segment(3))}}" class = 'btn btn-danger'>Cancel</a>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
      <!-- Pagination Section-->
  </div> <!-- Panel Body -->
</div>
</div>
   {!! Html::script('theme/tinymce/tinymce.min.js') !!}
   {!! Html::script('theme/admin/custome/js/jquery.sumoselect.min.js') !!}

<script>

tinymce.init({
	selector: "#TBody",

	plugins: [
		"advlist autolink lists link image charmap print preview anchor","searchreplace visualblocks code fullscreen",
		"insertdatetime media table contextmenu paste jbimages"	],

	toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | jbimages",

	relative_urls: false,
    remove_script_host: false,
    convert_urls : true,
});

  $('.testselect3').SumoSelect();

</script>
@include('Admin::layouts.footer')
@stop
