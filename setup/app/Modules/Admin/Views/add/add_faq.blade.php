@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')

@section('content')

<div class="container-fluid"> 
<div class="panel panel-default">
  <div class="panel-heading"><i class="fa fa-pencil-square-o"></i>&nbsp;{{$add}}</div>
  <div class="panel-body">
 
  	<div class="row">
  		<div class="col-md-12">
		{!! Form::model('', ['method' => 'POST', 'url' => ['admin/post_faq']]) !!}
		  		  	
     	  <div class="form-group {{ $errors->has('Question') ? ' has-error' : '' }}"  >
		     {!! Form::label('Question', '',['class' => 'col-md-2 text-right']) !!}
		     <div class="col-md-10">
				{!! Form::textarea('Question', '', ['class'=>'form-control', 'placeholder'=> 'Question','id' =>'inputError1','rows'=>'2']) !!}
				@if ($errors->has('Question')) <p class="help-block">{{ $errors->first('Question') }}</p> @endif
			</div>
		     
		     <div class="clearfix"></div>
         </div>
         <div class="form-group {{ $errors->has('Answer') ? ' has-error' : '' }}">
		     {!! Form::label('Answer', '',['class' => 'col-md-2 text-right']) !!}
		     <div class="col-md-10">
				{!! Form::textarea('Answer', '', ['class'=>'form-control', 'placeholder'=> 'Answer','id'=>'inputError1']) !!}
				@if ($errors->has('Answer')) <p class="help-block">{{ $errors->first('Answer') }}</p> @endif
			</div>
			<div class="clearfix"></div>
         </div>
          <div class="form-group">
			  <div class="col-md-10 col-md-offset-2">
				{!! Form::submit('Add', ['class' => 'btn btn-primary']) !!}
				<a href = "{{URL::previous()}}" class = 'btn btn-danger'>Cancel</a>
				{!! Form::close() !!}
			</div>       
		</div>
	</div>
</div>
      <!-- Pagination Section-->
  </div> <!-- Panel Body -->
</div>
</div>

@include('Admin::layouts.footer')
@stop
