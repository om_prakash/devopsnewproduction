@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')

@section('content')
<?php //print_r($_POST); ?>
<div class="container-fluid"> 
<div class="panel panel-default">
  <div class="panel-heading"><i class="fa fa-pencil-square-o"></i>&nbsp;Add Vehicle</div>
  <div class="panel-body">
  	<div class="row">
  		<div class="col-md-12">
		{!! Form::model('',['method' => 'POST', 'url' => ['admin/post_vehicle']]) !!}
		  
		  <!-- Vehicle -->
		  <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}"  >
		     {!! Form::label('Vehicle', 'Vehicle:',['class' => 'col-md-2 text-right']) !!}

            <div class="col-md-5">
             <select name="name" id="inputError1"  class="form-control">
				<option>Select Vehicle</option> 
			    <option value="Bike">Bike</option>
			    <option value="Motor Cycle">Motor Cycle</option>
			    <option value="Car">Car</option>
			    <option value="Truck">Truck</option>
			    <option value="Plane">Plane</option>
			    <option value="Drone">Drone</option>
				<option value="Boat">Boat</option>
			 </select>
             </div>
             @if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
             
             
		    
		     <div class="clearfix"></div>
         </div>
		
		<!-- Weight -->
		  
		 <div class="{{ $errors->has('weightValue') ? ' has-error' : '' }} {{ $errors->has('weightUnit') ? ' has-error' : '' }}"  >
         	<div class="form-group">
		     {!! Form::label('weight', 'Max. Weight:',['class' => 'col-md-2 text-right']) !!}
		     <div class="col-md-5">
				{!! Form::text('weightValue', '', ['class'=>'form-control', 'placeholder'=> 'Weight','id' =>'inputError1']) !!}
				@if ($errors->has('weightValue')) <p class="help-block">{{ $errors->first('weightValue') }}</p> @endif
				<span>Note: Weight in pound</span>
			 </div>
             </div>		  
		     <div class="clearfix"></div>
         </div>
        
		<!-- Volume -->
		  <div class="{{ $errors->has('volumeValue') ? ' has-error' : '' }} {{ $errors->has('volumeUnit') ? ' has-error' : '' }}"  >
          
        <div class="form-group ">

		     {!! Form::label('volume', 'Max. Volume:',['class' => 'col-md-2 text-right']) !!}
		     <div class="col-md-1">
				{!! Form::text('volumeValue', '', ['class'=>'form-control', 'placeholder'=> 'Volume','id' =>'inputError1']) !!}
				
			 </div>
             </div>
			  
		      
         </div>
        
		
		<!-- Distance -->
		  <div class="{{ $errors->has('distanceValue') ? ' has-error' : '' }} {{ $errors->has('distanceUnit') ? ' has-error' : '' }}"  >
           
		  <br />
		  <!-- delivery cost -->
		  <div class="col-md-10 col-md-offset-2 field_wrapper clearfix">
          
			  <div class="inline-form">
              	<div class="form-group">
				  {!! Form::text('StartDistance[0]', '', ['class'=>'form-control', 'placeholder'=> 'Start Distance','id' =>'inputError1']) !!}
                  </div>
                  </div>
                   <div class="inline-form">
                  <div class="form-group">
				  {!! Form::text('EndDistance[0]', '', ['class'=>'form-control', 'placeholder'=> 'End Distance','id' =>'inputError1']) !!}
                  </div>
                  </div>
                   <div class="inline-form">
                  <div class="form-group">
				  {!! Form::text('Cost[0]', '', ['class'=>'form-control', 'placeholder'=> 'Min. Cost','id' =>'inputError1']) !!}
                  </div>
                  </div>
                   <div class="inline-form">
                  <div class="form-group">
				  {!! Form::text('UrgentCost[0]', '', ['class'=>'form-control', 'placeholder'=> 'Urgent Cost','id' =>'inputError1']) !!}
                  </div>
                  </div>
			  </div>
              <div class="add-more"></div>
           
		  <div class="inline-form col-md-10 col-md-offset-2">
              <div class="form-group">
           <a href="javascript:void(0);" class="add_button btn btn-primary" title="Add field">Add More</a>
           </div>  
           </div>
           <div class="inline-form col-md-10 col-md-offset-2">
              	<div class="form-group">
				{!! Form::submit('Add', ['class' => 'btn btn-primary']) !!}
              
          		<a href = "{{URL::previous()}}" class = 'btn btn-danger'>Cancel</a>
				</div>
                </div>
           {!! Form::close() !!}
                
			       
		</div>
	</div>
</div>
  
  </div> <!-- Panel Body -->
</div> 
</script>
@include('Admin::layouts.footer')
@stop
