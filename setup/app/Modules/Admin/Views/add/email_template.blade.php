@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')
@section('content')

<div class="container-fluid"> 
<div class="panel panel-default">
  <div class="panel-heading"><i class="fa fa-plus-circle"></i>&nbsp;ADD New Email Template</div>
  <div class="panel-body">
 
  	<div class="row">
  		<div class="col-md-12">
  		  	{!! Form::model([], ['method' => 'POST', 'url' => ['admin/add_email_template']]) !!}
  		  	
     	  <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}"  >
		     {!! Form::label('UserFirstName', 'Title',['class' => 'col-md-2 text-right']) !!}
		     <div class="col-md-10">
				{!! Form::text('title', '', ['class'=>'form-control', 'placeholder'=> 'First Name','id' =>'inputError1']) !!}
				@if ($errors->has('title')) <p class="help-block">{{ $errors->first('title') }}</p> @endif
			</div>		     
		     <div class="clearfix"></div>
         </div>
         
         <div class="form-group {{ $errors->has('subject') ? ' has-error' : '' }}">
		     {!! Form::label('Subject', 'Subject',['class' => 'col-md-2 text-right']) !!}
		     <div class="col-md-10">
				{!! Form::text('subject', '', ['class'=>'form-control', 'placeholder'=> 'Subject']) !!}
				@if ($errors->has('subject')) <p class="help-block">{{ $errors->first('subject') }}</p> @endif
				
			</div>
			<div class="clearfix"></div>
         </div>
          <div class="form-group {{ $errors->has('sendFromName') ? ' has-error' : '' }}">
		     {!! Form::label('Send from Name', 'Send From Name',['class' => 'col-md-2 text-right']) !!}
		     <div class="col-md-10">
				{!! Form::text('sendFromName', '', ['class'=>'form-control', 'placeholder'=> 'Send From Name']) !!}
				@if ($errors->has('sendFromName')) <p class="help-block">{{ $errors->first('sendFromName') }}</p> @endif
			</div>
			
			<div class="clearfix"></div>
         </div>
         <div class="form-group {{ $errors->has('sendFrom') ? ' has-error' : '' }}">
		     {!! Form::label('Send from', 'Send From',['class' => 'col-md-2 text-right']) !!}
		     <div class="col-md-10">
				{!! Form::text('sendFrom', '', ['class'=>'form-control', 'placeholder'=> 'Send From']) !!}
				@if ($errors->has('sendFrom')) <p class="help-block">{{ $errors->first('sendFrom') }}</p> @endif
			</div>
			
			<div class="clearfix"></div>
         </div>
         <div class="form-group {{ $errors->has('body') ? ' has-error' : '' }}">
		     {!! Form::label('UserLastName', 'Email Body',['class' => 'col-md-2 text-right']) !!}
		     <div class="col-md-10">
				{!! Form::textarea('body', '', ['class'=>'form-control', 'placeholder'=> 'Email Body','id'=>'TBody']) !!}
				@if ($errors->has('body')) <p class="help-block">{{ $errors->first('body') }}</p> @endif
			</div>
			<div class="clearfix"></div>
         </div>
          <div class="form-group">
			  <div class="col-md-10 col-md-offset-2">
				{!! Form::submit('ADD', ['class' => 'btn btn-primary']) !!}
				<a href = "{{URL::previous()}}" class = 'btn btn-danger'>Cancel</a>
				{!! Form::close() !!}
			</div>       
		</div>
	</div>
</div>
 
      
      
      <!-- Pagination Section-->
    
  

  
  </div> <!-- Panel Body -->
</div>


</div>
{!! Html::script('theme/tinymce/tinymce.min.js') !!}
<script>
tinymce.init({
	selector: "#TBody",

	plugins: [
		"advlist autolink lists link image charmap print preview anchor","searchreplace visualblocks code fullscreen",
		"insertdatetime media table contextmenu paste jbimages"	],
		
	toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | jbimages",

	relative_urls: false
});
</script>
@include('Admin::layouts.footer')
@stop
