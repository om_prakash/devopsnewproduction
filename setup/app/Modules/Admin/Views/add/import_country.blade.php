@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')

@section('content')

<div class="container-fluid"> 
<div class="panel panel-default">
  <div class="panel-heading"><i class="fa fa-pencil-square-o"></i>&nbsp;Import Country</div>
  <div class="panel-body">
 
  	<div class="row">
  		<div class="col-md-12">
		{!! Form::model('', ['method' => 'POST', 'url' => ['admin/post_import_country'],'enctype' => 'multipart/form-data']) !!}
		  		  	
     	  <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}"  >
		     {!! Form::label('title', 'File',['class' => 'col-md-2 text-right']) !!}
		     <div class="col-md-10">
				{!! Form::file('title', '', ['class'=>'form-control', 'placeholder'=> 'Title','id' =>'inputError1']) !!}
				@if ($errors->has('title')) <p class="help-block">{{ $errors->first('title') }}</p> @endif
			</div>
		     
		     <div class="clearfix"></div>
         </div>
        
          <div class="form-group">
			  <div class="col-md-10 col-md-offset-2">
				{!! Form::submit('Upload', ['class' => 'btn btn-primary']) !!}
				{!! Form::close() !!}
			</div>       
		</div>
	</div>
</div>
 
      
      
      <!-- Pagination Section-->
    
  

  
  </div> <!-- Panel Body -->
</div>


</div>
{!! Html::script('theme/tinymce/tinymce.min.js') !!}
<script>
tinymce.init({
	selector: "#TBody",

	plugins: [
		"advlist autolink lists link image charmap print preview anchor","searchreplace visualblocks code fullscreen",
		"insertdatetime media table contextmenu paste jbimages"	],
		
	toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | jbimages",

	relative_urls: false
});
</script>
 
@include('Admin::layouts.footer')
@stop
