@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')

@section('content')
<style>
.help-block{
color: #a94442;
}
</style>
<div class="container-fluid"> 
<div class="panel panel-default">
  <div class="panel-heading"><i class="fa fa-plus"></i>&nbsp;Section</div>
  <div class="panel-body">
 
  	<div class="row">
  		<div class="col-md-12">
		{!! Form::model('', ['method' => 'POST', 'files' => true, 'url' => ['admin/post-add-section']]) !!}
		  		  	
     	  <div class="form-group {{ $errors->has('Question') ? ' has-error' : '' }}"  >
		     {!! Form::label('Section Name', '',['class' => 'col-md-2 text-right']) !!}
		     <div class="col-md-6">
				{!! Form::text('section_name', '', ['class'=>'form-control required', 'placeholder'=> 'Section Name']) !!}
				@if ($errors->has('section_name')) <p class="help-block">{{ $errors->first('section_name') }}</p> @endif
			</div>
		     
		     <div class="clearfix"></div>
         </div>
         <div class="form-group {{ $errors->has('Answer') ? ' has-error' : '' }}">
				{!! Form::label('Section Image', '',['class' => 'col-md-2 text-right']) !!}
		     <div class="col-md-6">
                 <input type="file" name="image" id="image"><br/>
			</div>
			<div class="clearfix"></div>
         </div>
          <div class="form-group">
			  <div class="col-md-10 col-md-offset-2">
				{!! Form::submit('Add', ['class' => 'btn btn-primary']) !!}
				<a href = "{{url('admin/section')}}" class = 'btn btn-danger'>Cancel</a>
				{!! Form::close() !!}
			</div>       
		</div>
	</div>
</div>
      <!-- Pagination Section-->
  </div> <!-- Panel Body -->
</div>
</div>

@include('Admin::layouts.footer')
@stop
