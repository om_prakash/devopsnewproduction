@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')

@section('content')

	<div class="container-fluid">
		<div class="panel panel-default">
			<div class="panel-heading"><i class="fa fa-plus"></i>&nbsp;Add Client</div>

			<div class="panel-body">
				<div class="row">
					<div class="col-md-12">
						{!! Form::model('', ['name'=>'add_comment','method' => 'POST', 'url' => ['admin/post-add-client'],'files' => true]) !!}

						<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}"  >
							{!! Form::label('Name', 'Name',['class' => 'col-md-2 text-right']) !!}
							<div class="col-md-6" style="color:#a44423">
								{!! Form::text('name', '', ['class'=>'form-control required', 'placeholder'=> 'Name','id' =>'name',]) !!}
								@if ($errors->has('name')) <p class="help-block" style="color:#a44423">{{ $errors->first('name') }}</p> @endif
							</div>
							<div class="clearfix"></div>
						</div>

						<div class="form-group {{ $errors->has('position') ? ' has-error' : '' }}"  >
							{!! Form::label('Position', 'Position',['class' => 'col-md-2 text-right']) !!}
							<div class="col-md-6" style="color:#a44423">
								{!! Form::text('position', '', ['class'=>'form-control required', 'placeholder'=> 'Position','id' =>'position',]) !!}
								@if ($errors->has('position')) <p class="help-block" style="color:#a44423">{{ $errors->first('position') }}</p> @endif
							</div>
							<div class="clearfix"></div>
						</div>


						<div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}"  >
							{!! Form::label('Description', 'Description',['class' => 'col-md-2 text-right']) !!}
							<div class="col-md-6" style="color:#a44423">
								{!! Form::textarea('description', '', ['class'=>'form-control required', 'placeholder'=> 'Description','id' =>'description','rows' => 5,'maxlength' => 1000]) !!}
								@if ($errors->has('description')) <p class="help-block" style="color:#a44423">{{ $errors->first('description') }}</p> @endif
							</div>
							<div class="clearfix"></div>
						</div>


						<div class=""  >
							  {!! Form::label('Image','Image',['class' => 'col-md-2 text-right']) !!}
							<div class="col-md-6" >
							 <input type="file" name="image" id="image" >
							</div>
							<div class="clearfix"></div>
						</div>
					</div>


						<div class="form-group">
							<div class="col-md-10 col-md-offset-2">
								{!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
								<a href="{{'client'}}" class='btn btn-danger'>Cancel</a>
								{!! Form::close() !!}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

{!! Html::script('theme/admin/custome/js/validate.js') !!}
<script type="text/javascript"></script>

@include('Admin::layouts.footer')
@stop
