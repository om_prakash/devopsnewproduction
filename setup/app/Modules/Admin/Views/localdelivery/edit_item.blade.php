@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')
@section('content')
{!! Html::style('theme/admin/custome/css/style.min.css') !!}

<div class="container">
  <div class="row">
      <div class="col-sm-12" id="send_a_package_start_position">
      <h2 class="color-blue mainHeading">Local Delivery</h2>
      <br>
      </div>
      <div class="col-sm-12">
        <div class="box-shadow">
          <h3 class="midHeading">Please complete the following fields</h3>
          <br>
          <hr>


          <div class="col-sm-12">
              <div class="step_box four_step clearfix">

                <div class="step first selected" id="step1-header">
                     <div>1</div>
                     <p class="text-center colol-black">Package Details</p>
                  </div>

                  <div class="step inner" id="step2-header">
                     <div>2</div>
                     <p class="text-center colol-black">Pickup Address</p>
                  </div>

                  <div class="step inner" id="step3-header ">
                     <div>3</div>
                     <p class="text-center colol-black">Drop Off Address</p>
                  </div>

                  <div class="step last " id="step4-header">
                     <div>4</div>
                     <p class="text-center colol-black">Payment</p>
                  </div>

              </div>
          </div>


          <form name="add_item" id="add_item">
          <input type="hidden" name="item_id" id="item_id" value="{{$delivery_data['_id']}}">
          <input type="hidden" name="request_id" id="request_id" value="{{Request::segment(4)}}">
          <div class="row" id="sec3">

             <div class="col-md-10 col-sm-offset-1">

                <div class="col-sm-6 col-xs-6">
                  <div class="form-group">
                    <label class="control-label">Product Title</label>
                    <input class="form-control required  usename-#title#" placeholder="Product Title" name="title" maxlength="50" value="{{$delivery_data['product_name']}}">
                  </div>
                </div>

                <div class="col-sm-6 col-xs-6">
                  <div class="form-group">
                    <label class="control-label">Package Value</label>
                    <div class="input-group error-input">
                    <span class="input-group-addon">$</span>
                    <input class="form-control  float maxlength-7 " placeholder="Package Value" name="package_value" maxlength="9" value="{{$delivery_data['productCost']}}" >
                  </div>
                </div>
              </div>



                <div class="col-sm-12 col-xs-12">
                <br>
                   <div class="form-group">
                      <label class="control-label">Measurement Unit </label>

                      <div class="clearfix">
                          <div class="radio">
                           <label class="">

                            <input type="radio" name="measurement_unit"  value="cm_kg" @if($delivery_data['ProductWeightUnit'] == 'kg') checked @endif onclick="return unit_show();" >

                           </label>&nbsp;Metric (Cm/Kg)
                           <label class="">

                            <input type="radio" name="measurement_unit" value="inches_lbs" @if($delivery_data['ProductWeightUnit'] == 'lbs') checked @endif onclick="return unit_show();">
                           </label>&nbsp;Imperial (Inches/Lbs)
                          </div>

                      </div>
                   </div>
                </div>

                <div class="col-md-3 col-sm-3 col-xs-12">
                   <div class="form-group">
                      <label class="control-label">Length</label>
                      <div class="input-group error-cm-input">
                      <input class="form-control   float maxlength-9" placeholder="Length" name="length" maxlength="9" value="{{$delivery_data['productLength']}}">
                      <span class="input-group-addon" id="length_unit"></span>
                      </div>
                   </div>
                </div>

                <div class="col-md-3 col-sm-3 col-xs-12">
                   <div class="form-group">
                      <label class="control-label">Width</label>
                      <div class="input-group error-cm-input">
                      <input class="form-control  float maxlength-9" placeholder="Width" name="width" maxlength="9" value="{{$delivery_data['productWidth']}}">
                      <span class="input-group-addon" id="width_unit"></span>
                      </div>
                   </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                   <div class="form-group">
                      <label class="control-label">Height</label>
                      <div class="input-group error-cm-input">
                      <input class="form-control  float maxlength-9" placeholder="Height" name="height" maxlength="9" value="{{$delivery_data['productHeight']}}">
                      <span class="input-group-addon" id="height_unit"></span>
                      </div>
                   </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                   <div class="form-group">
                      <label class="control-label">Weight</label>
                      <div class="input-group error-cm-input">
                      <input class="form-control  float maxlength-9" placeholder="Weight" name="weight" maxlength="9" value="{{$delivery_data['productWeight']}}">
                      <span class="input-group-addon" id="weight_unit"></span>
                      </div>
                   </div>
                </div>
                <div class="col-sm-12 col-xs-12">
                       <div class="form-group">

                          <label class="control-label">Quantity</label>


                            <div class="radio">
                                <label class="">
                                    <input type="radio" name="quantity_type" value="single" onclick="show_quantity();" @if($delivery_data['QuantityStatus'] == 'single') checked @endif>
                                  </label>&nbsp;Single
                                <label class="">
                                  <input type="radio" name="quantity_type" value="multiple" onclick="show_quantity();" @if($delivery_data['QuantityStatus'] == 'multiple') checked @endif>
                                </label>&nbsp;Multiple
                            </div>


                       </div>
                    </div>
                <div class="col-sm-12 col-xs-12" id="quantity_input">
                      <div class="form-group">
                  <input type="text" class="form-control required numeric" name="quantity" placeholder="Quantity" value="{{$delivery_data['productQty']}}">
                  </div>
                </div>

                  @if(count($transporter_data) > 0)
                    <div class="col-sm-12 col-xs-12">
                       <div class="form-group">
                          <label class="control-label">Select Package Category</label>
                          <select class="form-control required" id="package_category" name="category">
                             <option value="">Select Category</option>

                             <?php foreach ($category2 as $key) {?>
                              <option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}"}' class="travel-mode-air">{{$key->Content}}</option>
                             <?php }?>


                          </select>
                       </div>
                    </div>

                @else
 <!-- <?php //echo  strtolower($delivery_data['productCategory']); die;?> -->
                    <div class="col-sm-12 col-xs-12">
                      <div class="form-group">
                          <label class="control-label">Select Package Category</label>
                          <select class="form-control required" id="package_category" name="category">
                             <option value="">Select Category</option>
                             <?php foreach ($category as $key) {?>
                              <option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}"}'  class="travel-mode-{{$key->TravelMode}}" <?php if (in_array(strtolower($key->Content), [strtolower($delivery_data['productCategory'])])) {echo 'selected="selected"';}?> >{{$key->Content}}
                              </option>
                             <?php }?>
                          </select>
                      </div>
                    </div>

                @endif

                <div class="col-sm-12 col-xs-12">
                   <div class="form-group">
                      <label class="control-label">Package Content/Description/Instructions</label>
                      <textarea rows="3" class="form-control" name="description" value="">{{$delivery_data['Description']}}</textarea>
                   </div>
                </div>
               <!-- <div class="col-sm-6">
                   <div class="form-group">
                      <label class="control-label">Package care note</label>
                      <textarea rows="3" class="form-control required" name="care_note"></textarea>
                   </div>
                </div>-->

                <div class="col-sm-12">
                  <label class="control-label">Upload Default Package Image</label>
                  <div class="selected-pic"> <div>
                         @if($delivery_data['ProductImage'] != '')
                           @if(file_exists(BASEURL_FILE.$delivery_data['ProductImage']))
                            <a class="fancybox" rel="group" href="{{ ImageUrl.$delivery_data['ProductImage']}}" >
                           <img id="senior-preview" src="{{ ImageUrl.$delivery_data['ProductImage']}}"  width="200px" height="150px" /><p> </p>
                           </a>
                           @else
                           <img id="senior-preview" src="{{ ImageUrl}}/no-image.jpg"  width="200px" height="150px" />
                           @endif
                         @endif
                  </div>
                    <label class="custom-input-file">
                      {!! Form::file('default_image', ['class'=> 'custom-input-file','id'=>'senior_image','onchange'=>"image_preview(this,'senior-preview',event)"]) !!}
                    </label>
                </div>

                <div class="col-sm-12">
                       <div class="form-group">
                          <label class="control-label">Insurance</label>
                          <div class="checkbox">
                             <label>
                              <span class="pull-left row"><input type="radio" name="insurance" value="yes" <?php if ($delivery_data['InsuranceStatus'] == "yes") {echo 'checked=="checked"';}?> ></span> <span class="Insurance_check"> Yes </span>
                             </label>
                             &nbsp; &nbsp;
                             <label>
                              <span class="pull-left"><input type="radio" name="insurance" value="no" <?php if ($delivery_data['InsuranceStatus'] == "no") {echo 'checked=="checked"';}?> ></span> <span class="Insurance_check"> No </span>
                             </label>
                          </div>
                       </div>
                       <div class="form-group">
                          <div class="checkbox">
                             <label>
                              <span class="pull-left"><input type="checkbox" value="yes" name="need_package_material" <?php if ($delivery_data['PackageMaterial'] == "yes") {echo 'checked=="checked"';}?>></span> <span class="Insurance_check"> Need package material </span>
                             </label>
                          </div>
                       </div>

                        <div class="form-group">
                          <div class="checkbox">
                             <!-- <label>
                              <span class="pull-left"><input type="checkbox" id="agree" name="terms_conditions"></span> <span class="Insurance_check"> I Accept the  <a href="{{url('terms-and-conditions')}}" target="_blank"> Terms and Conditions </a> .</span>
                             </label> -->
                          </div>
                       </div>
                    </div>
                <div class="col-md-12 col-xs-12">
                   <hr>
                </div>

                   <div class="col-sm-8 col-xs-12">
                      <div class="">
                          <div class="form-group">
                            <button class="custom-btn1 btn" id="calculate_loader">Next
                              <div class="custom-btn-h"></div>
                            </button><div class="error-msg"></div>
                          </div>
                          <div class="col-xs-6 col-xs-12">
                             <div class="form-group">
                                <a class="custom-btn1 btn text-center" href="{{url('admin/add-local-delivery')}}">Back
                                    <div class="custom-btn-h"></div>
                                </a>
                             </div>
                          </div>
                      </div>
                  </div>




             </div>
          </div>
          </form>

        </div>
      </div>
  </div>
</div>


{!! Html::script('theme/web/js/utility.js') !!}
{!! Html::script('theme/admin/custome/js/validation1.js') !!}

<script type="text/javascript">

function unit_show()
{
  var unit =$('input[name=measurement_unit]:checked').val();

  if(unit == 'cm_kg'){
    $('#length_unit').html('Cm');
    $('#width_unit').html('Cm');
    $('#height_unit').html('Cm');
    $('#weight_unit').html('Kg');

  }else{
    $('#length_unit').html('Inches');
    $('#width_unit').html('Inches');
    $('#height_unit').html('Inches');
    $('#weight_unit').html('Lbs');
  }

}

unit_show();

new Validate({
    FormName : 'add_item',
    ErrorLevel : 1,
    validateHidden: false,
    callback : function(){
      $.ajax({
          url: SITEURL+"admin/local-post-edit-item-exist-request",
          method: 'post',
          data: new FormData(document.getElementById('add_item')),
          processData: false,
          contentType: false,
          dataType: "json",

          success: function(res){
            if(res.success == 1){
              alert(res.msg);
              document.location.href = SITEURL + 'admin/add-local-delivery';
            }else{
              alert(res.msg);
            }

          }
      });
    }

});

function toggle_category(showid, hideid, id) {
  $(id).val('');
  $(showid).attr('disabled', false);
  $(showid).show();
  $(hideid).attr('disabled', true);
  $(hideid).hide();
}

@if($delivery_data['travelMode'] == "air")
  toggle_category('.travel-mode-air','.travel-mode-ship','#package_category');
@else
  toggle_category('.travel-mode-ship','.travel-mode-air','#package_category');
@endif
$('#package_category').val('{"id":"{{$delivery_data['productCategoryId']}}","name":"{{$delivery_data['productCategory']}}"}');
function show_quantity()
{
  var type =$('input[name=quantity_type]:checked').val();
  if(type == 'multiple'){
    $('#quantity_input').show();
  }else{
    $('#quantity_input').hide();
  }

}
show_quantity();
// for selected true for package category
/*$('#package_category').find('option[selected="true"]').each(function(){
    $(this).prop('selected', true);
});*/

function image_preview(obj,previewid,evt)
{
    var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
    if ($.inArray($(obj).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
       $(obj).val('');
       $('#'+previewid).attr('src', '{{ImageUrl}}/user-no-image.jpg');
        alert("Only "+fileExtension.join(', ')+" formats are allowed.");
     } else {

       var file = evt.target.files[0];
       if (file)
       {
         var reader = new FileReader();

         reader.onload = function (e) {
             $('#'+previewid).attr('src', e.target.result)
         };
         reader.readAsDataURL(file);
       }
     }
}

</script>
@endsection
