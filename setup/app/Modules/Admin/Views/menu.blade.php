<script type="text/javascript">
 window.setTimeout(function() {
    $("#success-alert").fadeTo(3000, 0).slideUp(100, function(){
        $(this).close(); 
    });
}, 5000);               
 </script>
@if(Auth::user())

<div style="position:fixed; display:none; width:70px; height:100%; background:#303030;" class="fixed-back"></div>
<div class="side-menu">
  <ul class="nav navbar-nav nav-left">
   <li><a class="side-menu1 side-nav" onclick="manage_side_menu('big-side-bar')"><span class="text-left"></span><i class="fa fa-bars text-right"></i></a></li>
   <!--<li><a class="side-menu1 main-nav" onclick="manage_side_menu('')" ><i class="fa fa-bars"></i></a></li>-->

   <!--<li><a class="side-menu1 main-nav" onclick="manage_side_menu('')" ><i class="fa fa-bars"></i></a></li>-->

      <li class="dropdown custom_drop show-sm">
         <a >
         <i class="fa fa-home inline-fa fa-width"></i>
         <span>Home</span>
         </a>
      </li>
      <li class="dropdown custom_drop show-sm">
         <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="fa fa-user"></i> Welcome Admin &nbsp; <span class="caret"></span> </a>
         <ul class="dropdown-menu">
            <li>
               <a href="#">
                  <!--<i class="fa fa-cogs"></i>--> Setting
               </a>
            </li>
            <li>
               <a href="{{ url('admin','change_password')}}">
                  <!--<i class="fa fa-exchange"></i>--> Change Password
               </a>
            </li>
            <li>
               <a href="#">
                  <!--<i class="fa fa-sign-out"></i>--> Logout
               </a>
            </li>
         </ul>
      </li>
      <li>
          <a class="change_view" onclick="manage_side_menu('small-bar')"><span>Welcome Admin &nbsp;</span><i class="fa fa-bars pull-right inline-fa fa-width"></i>
         
        
         </a>
         <li><a class="side-menu1 side-nav" onclick="manage_side_menu('big-side-bar')"><span class="text-left"></span><i class="fa fa-bars text-right"></i></a></li>
      </li>
      <li>
            	<a href="{{ url('admin','dashboard')}}">
					<i class="fa fa-home inline-fa fa-width"></i>
					<span>Home</span>
				</a>
			</li>
              <!--<li class="hover-drop">-->
              @if(((Auth::user()->UserPermission & ADMIN) == ADMIN) OR ((Auth::user()->UserPermission & SENDER) == SENDER) OR ((Auth::user()->UserPermission & CARRIER) == CARRIER))
              <li>
            	<a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#">
					<i class="fa fa-user inline-fa"></i>
					<span>User</span>
                    <span class="caret"></span>
				</a>
                
                <ul class="dropdown-menu">
					@if((Auth::user()->UserPermission & CARRIER) == CARRIER)
						<li>
							<a href="{{ url('admin','transporter')}}">Transporter</a>
						</li>
					@endif
					@if((Auth::user()->UserPermission & SENDER) == SENDER)
						<li><a href="{{ url('admin','requester')}}"></i>Requester</a></li>
					@endif
					@if((Auth::user()->UserPermission & ADMIN) == ADMIN)
						<li><a href="{{ url('admin','admin_user')}}"></i>Admin</a></li>
					@endif
				</ul>
			</li>	
			@endif

		  @if((Auth::user()->UserPermission & PACKAGE) == PACKAGE)
				<li class="<?php if(in_array(Request::segment(2),array('package'))) { echo 'nav-active'; } ?>">
				  <a href="{{ url('admin','package')}}">
			  <i class="fa fa-archive inline-fa"></i>
			  <span>Package</span>
			</a>
		  </li>
		  @endif

		  @if((Auth::user()->UserPermission & PACKAGE) == PACKAGE)
            <li class="<?php if(in_array(Request::segment(2),array('online-package','package/detail'))) { echo 'nav-active'; } ?>">
				<a href="{{ url('admin','online-package')}}">
					<i class="fa fa-archive inline-fa"></i>
					<span> Online Package</span>
				</a>
			</li>
			@endif
    
            
			@if((Auth::user()->UserPermission & TRANSACTION) == TRANSACTION)
			<li>
            	<a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#">
					<i class="fa fa-credit-card inline-fa"></i>
					<span>Transaction</span>
					<span class="caret"></span>	
				</a>
				<ul class="dropdown-menu">					 
					<li><a href="{{ url('admin/transporter_transaction') }}">Transporter</a></li>					 
					<li><a href="{{ url('admin/requester_transaction') }}">Requester</a></li>
                </ul>  
			</li>
			@endif
            <!--<li class="hover-drop">-->
            @if((Auth::user()->UserPermission & NOTIFICATION) == NOTIFICATION)
            <li>
            	<a href="{{ url('admin/notification') }}">
					<i class="fa fa-bell inline-fa"></i>
					<span>Notification</span>
                    
				</a>
                    
			</li>
			@endif

      @if(((Auth::user()->UserPermission & COUNTRY) == COUNTRY)  OR ((Auth::user()->UserPermission & STATE) == STATE)  OR ((Auth::user()->UserPermission & CITY) == CITY))
            <li>
            	<a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#">
					<i class="fa fa-cogs inline-fa"></i>
					<span>Management m</span>
					<span class="caret"></span>	
				</a>
				<ul class="dropdown-menu">
					@if((Auth::user()->UserPermission & COUNTRY) == COUNTRY)
						<li><a href="{{ url('admin/country_list') }}">Country</a></li>
					@endif
					@if((Auth::user()->UserPermission & STATE) == STATE)
						<li><a href="{{ url('admin/state_list') }}">State</a></li>	
					@endif
					@if((Auth::user()->UserPermission & CITY) == CITY)
						<li><a href="{{ url('admin/city_list') }}">City</a></li>
					@endif
          @if((Auth::user()->UserPermission & CITY) == CITY)
            <li> <a href="{{ url('admin/category_list') }}">Item Category</a></li>
          @endif					
        </ul>  
			</li>
			@endif
			


 <!--<li class="hover-drop">-->
             @if(((Auth::user()->UserPermission & APP_CONTENT) == APP_CONTENT) OR ((Auth::user()->UserPermission & EMAIL_TEMPLATE) == EMAIL_TEMPLATE) OR ((Auth::user()->UserPermission & WEB_CONTENT) == WEB_CONTENT) OR ((Auth::user()->UserPermission & APP_TUTORIAL) == APP_TUTORIAL) OR ((Auth::user()->UserPermission & ABOUT_APP) == ABOUT_APP) OR ((Auth::user()->UserPermission & FAQ) == FAQ))
             <li>
              <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#">
          <i class="fa fa-folder inline-fa"></i>
          <span>Content</span>
                    <span class="caret"></span>
        </a>
                
                <ul class="dropdown-menu">
          @if((Auth::user()->UserPermission & APP_CONTENT) == APP_CONTENT)
            <li><a href="{{ url('admin','app_content')}}">App Content</a></li>
          @endif
          @if((Auth::user()->UserPermission & EMAIL_TEMPLATE) == EMAIL_TEMPLATE)
            <li><a href="{{ url('admin','email_template')}}">Email Template</a></li>
          @endif
          @if((Auth::user()->UserPermission & WEB_CONTENT) == WEB_CONTENT)
            <li><a href="{{ url('admin','web_content')}}">Web Content</a></li>
          @endif
          @if((Auth::user()->UserPermission & APP_TUTORIAL) == APP_TUTORIAL)
            <li><a href="{{ url('admin','app_tutorial')}}">App Tutorial</a></li>
          @endif
          @if((Auth::user()->UserPermission & ABOUT_APP) == ABOUT_APP)
            <li><a href="{{ url('admin','about_app')}}">About App</a></li>
          @endif
          @if((Auth::user()->UserPermission & FAQ) == FAQ)
            <li><a href="{{ url('admin','faq_list')}}">FAQ</a></li> 
          @endif
                </ul>
      </li> 
      @endif
        @if((Auth::user()->UserPermission & PACKAGE) == PACKAGE)
            <li>
              <a href="{{ url('admin','trip')}}">
          <i class="fa fa-plane inline-fa"></i>
          <span>Trip</span>
        </a>
      </li>
      @endif
			@if((Auth::user()->UserPermission & FEEDBACK) == FEEDBACK)
            <li>
            	<a href="{{ url('admin','support')}}">
					<i class="fa fa-question-circle inline-fa"></i>
					<span>Support</span>
				</a>
			</li>
			@endif
      @if((Auth::user()->UserPermission & FEEDBACK) == FEEDBACK)
      <li>
          <a href="{{ url('admin','configuration')}}">
          <i class="fa fa-cog inline-fa"></i>
          <span>Configuration</span>
        </a>
      </li>
      @endif
	
      </li>
   </ul>
</div>
@endif
<div class="side-wrap-custom">
<nav class="navbar navbar-default navbar-inverse margin-bottom-0 border-radious nav_bg hidden-xs">
   <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
         <!-- <a aria-expanded="false" data-target="#bs-example-navbar-collapse-1" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
<i class="fa fa-cog color-white"></i> 
          </a>-->
          <a href="{{ url('admin/dashboard')}}" class="navbar-brand visible">{!! Html::image('theme/admin/custome/images/logo_1242.png') !!}</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div id="bs-example-navbar-collapse-1" class="collapse navbar-collapse">
         
           @if(Auth::user())
          <!--<ul class="nav navbar-nav navbar-right show-sm">
          
          	<li class="dropdown">
              <!--<a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="fa fa-user"></i> Welcome Admin </a>-->
				 <!--</li>
                <li><a href="#"><i class="fa fa-cogs"></i> Setting</a></li>
                <li><a href="{{ url('admin','change_password')}}"><i class="fa fa-exchange"></i> Change Password</a></li>
                <li class="divider" role="separator"></li>
                <li><a href="#"><i class="fa fa-sign-out"></i> Logout</a></li>
          </ul>-->
        

          
          <ul class="nav navbar-nav navbar-right hide-sm">
            <li class="dropdown custom_drop">
              <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="fa fa-user"></i> Welcome Admin <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="{{ url('admin','change_password')}}"><i class="fa fa-exchange blue"></i> Change Password</a></li>
                <li class="divider" role="separator"></li>
                <li><a href="{{ url('admin/setting') }}"><i class="fa fa-cogs blue"></i> Settings</a></li>
                <li><a class="side-menu1 side-nav" onclick="manage_side_menu('big-side-bar')"><i class="fa fa-bars"></i> Side navbar</a></li>
                  <li><a class="side-menu1 main-nav" onclick="manage_side_menu('')" ><i class="fa fa-bars"></i> Main navbar</a></li>
                <li><a href="{{ url('admin/logout') }}"><i class="fa fa-sign-out red"></i> Logout</a></li>
              </ul>
            </li>
          </ul>

          <ul class="nav navbar-nav navbar-right hide-sm">
            <li class="dropdown custom_drop">
              <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle newbadge" href="#"><i style="font-size:16px" class="fa fa-bell"></i> <span style="background:#F00;" class="badge" id="badgecount">0</span><span class="caret"></span></a>
              
            <ul class="noti-dropd dropdown-menu">
            	
            
            
              <div id="badge_html" class="list-group bg">
              
              </div>
              <div id="view_btn"></div>
           <!--  <a class="active text-center" href="{{ url('admin/admin_notification') }}"><div class="custom-view-btn">View</div></a> -->
            </ul>

            </li>
          </ul>

           @endif
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
      </nav>
      
      @if(Auth::user())
      <nav class="navbar navbar-default navbar-inverse border-radious custom-navbar">
       <div class="container-fluid subnavbar">
        <!-- Brand and toggle get grouped for better mobile display -->
        
        <div class="pull-left visible-xs-block custom-brand">
        <a href="http://192.168.11.101:8000/admin"><img src="{{url('theme/admin/custome/images/logo_1242-sm.png')}}"></a>
        </div>
        
        <div class="navbar-header">
          <button aria-expanded="false" data-target="#bs-example-navbar-collapse-2" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div id="bs-example-navbar-collapse-2" class="collapse navbar-collapse subnav-collapse pad-0 ">
         <div>
		 <a href="{{ url('admin/dashboard')}}">{!! Html::image('theme/admin/custome/images/logo_1242.png','',['class'=>'fixed-nav-logo fixed-nav-logo1']) !!}</a>
         
          <ul class="nav navbar-nav nav-left">
           @if(Auth::user())
          
         
          
          	<li class="dropdown custom_drop show-sm">
              <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="fa fa-user"></i> Welcome Admin &nbsp; <span class="caret"></span> </a>
              <ul class="dropdown-menu">
                <li><a href="#"><!--<i class="fa fa-cogs"></i>--> Setting</a></li>
                <li><a href="{{ url('admin','change_password')}}"><!--<i class="fa fa-exchange"></i>--> Change Password</a></li>
                
                <li><a href="#"><!--<i class="fa fa-sign-out"></i>--> Logout</a></li>
                </ul>
          	</li> 
           @endif
            <li>
            	<a href="{{ url('admin','dashboard')}}">
					<i class="fa fa-home inline-fa fa-width"></i>
					<span>Home</span>
				</a>
			</li>
              <!--<li class="hover-drop">-->
              @if(((Auth::user()->UserPermission & ADMIN) == ADMIN) OR ((Auth::user()->UserPermission & SENDER) == SENDER) OR ((Auth::user()->UserPermission & CARRIER) == CARRIER))
              <li>
            	<a id="dropdown-toggle1" aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#">
					<i class="fa fa-user inline-fa"></i>
					<span>User</span>
                    <span class="caret"></span>
				</a>
                
                <ul id="menu1" class="dropdown-menu">
					@if((Auth::user()->UserPermission & CARRIER) == CARRIER)
						<li><a href="{{ url('admin','transporter')}}">Transporter</a></li>
					@endif
					@if((Auth::user()->UserPermission & SENDER) == SENDER)
						<li><a href="{{ url('admin','requester')}}"></i>Requester</a></li>
					@endif
					@if((Auth::user()->UserPermission & ADMIN) == ADMIN)
						<li><a href="{{ url('admin','admin_user')}}"></i>Admin</a></li>
					@endif
				</ul>
			</li>	
			@endif
      @if((Auth::user()->UserPermission & PACKAGE) == PACKAGE)
        <li>
              <a id="dropdown-toggle2" aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#">
                <i class="fa fa-archive inline-fa"></i>
                <span>Package</span>
                <span class="caret"></span> 
              </a>
              <ul id="menu2" class="dropdown-menu">
                  <li><a href="{{ url('admin','package')}}">Package</a></li>
                  <li><a href="{{ url('admin/online-package') }}">Online Package</a></li>  
                  <li><a href="{{ url('admin/buy-for-me') }}">Buy for Me</a></li>  
                 
              </ul>  
        </li>
      @endif
 
			 

			@if((Auth::user()->UserPermission & TRANSACTION) == TRANSACTION)
			<li>
				<a href="{{ url('admin/transaction-history') }}" class="dropdown-toggle">
					<i class="fa fa-credit-card inline-fa"></i>
					<span>Transaction</span>
				</a>
			</li>
			@endif

			@if((Auth::user()->UserPermission & NOTIFICATION) == NOTIFICATION)
			<li>
				<a href="{{ url('admin/notification') }}">
						  <i class="fa fa-bell inline-fa"></i>
							<span>Notification</span>
				</a>            
			</li>
			@endif

      @if(((Auth::user()->UserPermission & COUNTRY) == COUNTRY)  OR ((Auth::user()->UserPermission & STATE) == STATE)  OR ((Auth::user()->UserPermission & CITY) == CITY))
            <li>
            	<a id="dropdown-toggle2" aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#">
					<i class="fa fa-cogs inline-fa"></i>
					<span>Management</span>
					<span class="caret"></span>	
				</a>
				<ul id="menu2" class="dropdown-menu">
					@if((Auth::user()->UserPermission & COUNTRY) == COUNTRY)
						<li><a href="{{ url('admin/country_list') }}">Country</a></li>
					@endif
					@if((Auth::user()->UserPermission & STATE) == STATE)
						<li><a href="{{ url('admin/state_list') }}">State</a></li>	
					@endif
					@if((Auth::user()->UserPermission & CITY) == CITY)
						<li><a href="{{ url('admin/city_list') }}">City</a></li>
					@endif
          @if((Auth::user()->UserPermission & CITY) == CITY)
            <li> <a href="{{ url('admin/category_list') }}">Item Category</a></li>
          @endif	
          @if((Auth::user()->UserPermission & PROMOCODE) == PROMOCODE)
            <li> <a href="{{ url('admin/promocode') }}">Promocode</a></li>
          @endif  				
                </ul>  
			</li>
			@endif
		
      <!--<li class="hover-drop">-->
             @if(((Auth::user()->UserPermission & APP_CONTENT) == APP_CONTENT) OR ((Auth::user()->UserPermission & EMAIL_TEMPLATE) == EMAIL_TEMPLATE) OR ((Auth::user()->UserPermission & WEB_CONTENT) == WEB_CONTENT) OR ((Auth::user()->UserPermission & APP_TUTORIAL) == APP_TUTORIAL) OR ((Auth::user()->UserPermission & ABOUT_APP) == ABOUT_APP) OR ((Auth::user()->UserPermission & FAQ) == FAQ))
             <li>
              <a id="dropdown-toggle3" aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#">
          <i class="fa fa-folder inline-fa"></i>
          <span>Content</span>
                    <span class="caret"></span>
        </a>
                
                <ul id="menu3" class="dropdown-menu">
          @if((Auth::user()->UserPermission & APP_CONTENT) == APP_CONTENT)
            <li><a href="{{ url('admin','app_content')}}">App Content</a></li>
          @endif          
          @if((Auth::user()->UserPermission & WEB_CONTENT) == WEB_CONTENT)
            <li><a href="{{ url('admin','web_content')}}">Web Content</a></li>
          @endif
          @if((Auth::user()->UserPermission & APP_TUTORIAL) == APP_TUTORIAL)
            <li><a href="{{ url('admin','app_tutorial')}}">App Tutorial</a></li>
          @endif
          @if((Auth::user()->UserPermission & ABOUT_APP) == ABOUT_APP)
            <li><a href="{{ url('admin','about_app')}}">About App</a></li>
          @endif          
          @if((Auth::user()->UserPermission & EMAIL_TEMPLATE) == EMAIL_TEMPLATE)
            <li><a href="{{ url('admin','email_template')}}">Email Template</a></li>
          @endif
          @if((Auth::user()->UserPermission & FAQ) == FAQ)
            <li><a href="{{ url('admin','faq_list')}}">FAQ</a></li> 
          @endif
          </ul>
      </li> 
      @endif
        
         


        @if((Auth::user()->UserPermission & PACKAGE) == PACKAGE)
        <li>
              <a id="dropdown-toggle4" aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#">
         <i class="fa fa-plane inline-fa"></i>
          <span>Trip</span>
                    <span class="caret"></span>
        </a>
         <ul id="menu4" class="dropdown-menu">
          <li><a href="{{ url('admin','business-trip')}}">Business Trip</a></li>
          <li><a href="{{ url('admin','indiviual-trip')}}">Individual Trip</a></li>
          </ul>
		</li>
		@endif

			@if((Auth::user()->UserPermission & FEEDBACK) == FEEDBACK)
            <li>
            	<a href="{{ url('admin','support')}}">
					<i class="fa fa-question-circle inline-fa"></i>
					<span>Support</span>
				</a>
			</li>
			@endif
		  @if((Auth::user()->UserPermission & FEEDBACK) == FEEDBACK)
				<li>
				  <a href="{{ url('admin','configuration')}}">
			  <i class="fa fa-cog inline-fa"></i>
			  <span>Configuration</span>
			</a>
		  </li>
		  @endif

	
          </ul>

			
             @if(Auth::user())
             
          <ul class="nav navbar-nav navbar-right fixed-nav-dropd">
            <li class="dropdown custom_drop">
              <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="fa fa-user"></i><span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="#"><i class="fa fa-cogs blue"></i> Setting</a></li>
                <li><a href="{{ url('admin','change_password')}}"><i class="fa fa-exchange blue"></i> Change Password</a></li>
                <li class="divider" role="separator"></li>
                <li><a href="{{ url('admin/logout') }}"><i class="fa fa-sign-out red"></i> Logout</a></li>
              </ul>
            </li>
          </ul>
           @endif 
            
          </div>
          
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </nav>
 @endif   
    
    
<div class="container-fluid" id='error_msg_section'>

   @if(Session::has('success'))
    <div class="alert alert-success" id="success-alert">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<strong>Success!</strong> {{Session::get('success')}}
    </div>
    @endif
    
   @if(Session::has('fail'))
    <div class="alert alert-danger" id="success-alert">
        <ul>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<strong>Fail!</strong> {{Session::get('fail')}}
        </ul>
    </div>
	@endif
</div>
<div class="wrapper">
	<div id="loading" style="display: none;"></div>

