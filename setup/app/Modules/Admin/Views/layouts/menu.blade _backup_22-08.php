   <nav class="navbar navbar-default navbar-inverse margin-bottom-0 border-radious">
      <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <a aria-expanded="false" data-target="#bs-example-navbar-collapse-1" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
<i class="fa fa-cog"></i> 
          </a>
          <a href="#" class="navbar-brand">Brand</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div id="bs-example-navbar-collapse-1" class="collapse navbar-collapse">
         
         
          <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
              <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="fa fa-user"></i> Welcome Admin <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="#"><i class="fa fa-cogs"></i> Setting</a></li>
                <li><a href="#"><i class="fa fa-exchange"></i> Change Password</a></li>
                <li class="divider" role="separator"></li>
                <li><a href="#"><i class="fa fa-sign-out"></i> Logout</a></li>
              </ul>
            </li>
          </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
      </nav>
      <nav class="navbar navbar-default navbar-inverse border-radious">
       <div class="container-fluid subnavbar">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button aria-expanded="false" data-target="#bs-example-navbar-collapse-2" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div id="bs-example-navbar-collapse-2" class="collapse navbar-collapse subnav-collapse pad-0 ">
         
         
          <ul class="nav navbar-nav navbar-center mainnav">
            <li>
            	<a href="#">
							<i class="fa fa-home"></i>
							<span>Home</span>
				</a>
			</li>			
              <li>
            	<a href="#">
					<i class="fa fa-folder"></i>
					<span>Category</span>
				</a>
			</li>	
             <li>
            	<a href="#">
					<i class="fa fa-folder"></i>
					<span>Pages</span>
				</a>
			</li>	
             <li>
            	<a href="#">
					<i class="fa fa-folder"></i>
					<span>Section</span>
				</a>
			</li>	

			  <li>
            	<a href="{{ url('admin/email_template') }}">
					<i class="fa fa-folder"></i>
					<span>Content</span>
				</a>
			</li>	
          </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </nav>
<div class="container-fluid" id='error_msg_section'> 

   @if(Session::has('success'))
    <div class="alert alert-success">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<strong>Success!</strong> {{Session::get('success')}}
    </div>
    @endif
    
   @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
	@endif
</div>
