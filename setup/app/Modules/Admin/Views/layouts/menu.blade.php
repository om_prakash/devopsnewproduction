<script type="text/javascript">
 window.setTimeout(function() {
    $("#success-alert").fadeTo(3000, 0).slideUp(100, function(){
        $(this).close();
    });
}, 5000);
 </script>
@if(Auth::user())

<div style="position:fixed; display:none; width:70px; height:100%; background:#303030;" class="fixed-back"></div>
<div class="side-menu">
  <ul class="nav navbar-nav nav-left">
   <li><a class="side-menu1 side-nav" onclick="manage_side_menu('big-side-bar')"><span class="text-left"></span><i class="fa fa-bars text-right"></i></a></li>
   <!--<li><a class="side-menu1 main-nav" onclick="manage_side_menu('')" ><i class="fa fa-bars"></i></a></li>-->

   <!--<li><a class="side-menu1 main-nav" onclick="manage_side_menu('')" ><i class="fa fa-bars"></i></a></li>-->

      <li class="dropdown custom_drop show-sm">
         <a >
         <i class="fa fa-home inline-fa fa-width"></i>
         <span>Home</span>
         </a>
      </li>
      <li class="dropdown custom_drop show-sm">
         <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="fa fa-user"></i>
         Welcome {{Auth::user()->UserName}} &nbsp; <span class="caret"></span> </a>
         <ul class="dropdown-menu">
            <li>
               <a href="#">
                  <!--<i class="fa fa-cogs"></i>--> Setting
               </a>
            </li>
            <li>
               <a href="{{ url('admin','change_password')}}">
                  <!--<i class="fa fa-exchange"></i>--> Change Password
               </a>
            </li>
            <li>
               <a href="#">
                  <!--<i class="fa fa-sign-out"></i>--> Logout
               </a>
            </li>
         </ul>
      </li>
      <li>
          <a class="change_view" onclick="manage_side_menu('small-bar')"><span>Welcome {{Auth::user()->UserName}}</span><i class="fa fa-bars pull-right inline-fa fa-width"></i>


         </a>
         <li><a class="side-menu1 side-nav" onclick="manage_side_menu('big-side-bar')"><span class="text-left"></span><i class="fa fa-bars text-right"></i></a></li>
      </li>

     <li class="<?php if (in_array(Request::segment(2), array('dashboard'))) {echo 'nav-active';}?>">
            	<a href="{{ url('admin','dashboard')}}">
					<i class="fa fa-home inline-fa fa-width"></i>
					<span>Home</span>
				</a>
			</li>


        <!--<li class="hover-drop">-->
          @if(((Auth::user()->UserPermission & ADMIN) == ADMIN) OR ((Auth::user()->UserPermission & SENDER) == SENDER) OR ((Auth::user()->UserPermission & CARRIER) == CARRIER))
            <li class="<?php if (in_array(Request::segment(2), array('transporter', 'edit-transporter', 'edit_admin_user', 'user', 'edit-bank', 'requester', 'admin_user', 'transaction-history', 'add_admin_user'))) {echo 'nav-active';}?>">
              <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#">
                <i class="fa fa-user inline-fa"></i>
                <span>User</span>
                <span class="caret"></span>
				      </a>

              <ul class="dropdown-menu">
                @if((Auth::user()->UserPermission & SENDER) == SENDER)
                  <li><a href="{{ url('admin','transporter')}}">Transporter</a></li>
                @endif

                @if((Auth::user()->UserPermission & CARRIER) == CARRIER)
                  <li><a href="{{ url('admin','requester')}}"></i>Requester</a></li>
                @endif

                @if((Auth::user()->UserPermission & ADMIN) == ADMIN)
                  <li><a href="{{ url('admin','admin_user')}}"></i>Admin</a></li>
                @endif
              </ul>
			      </li>
			    @endif


          @if(((Auth::user()->UserPermission & PACKAGE) == PACKAGE) OR ((Auth::user()->UserPermission & ONLINEPACKAGE) == ONLINEPACKAGE) OR ((Auth::user()->UserPermission & BUYFORME) == BUYFORME))
            <li class="<?php if (in_array(Request::segment(2), array('on-line-purchases', 'package', 'post-buy-for-me', 'edit-online-package', 'online_package', 'online-package', 'buy-for-me', 'process-card-list', 'add-package', 'edit-package', 'post-buyforme', 'comment-list', 'add-send-a-package', 'edit-request'))) {echo 'nav-active';}?>">
              <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#">
                <i class="fa fa-archive inline-fa"></i>
                <span>Package</span>
                <span class="caret"></span>
              </a>
              <ul class="dropdown-menu">
                @if((Auth::user()->UserPermission & PACKAGE) == PACKAGE)
                  <li><a href="{{ url('admin','package')}}">Send a Package</a></li>
                @endif

                @if((Auth::user()->UserPermission & ONLINEPACKAGE) == ONLINEPACKAGE)
                  <li><a href="{{ url('admin/online-package') }}">Online Package</a></li>
                @endif

                @if((Auth::user()->UserPermission & BUYFORME) == BUYFORME)
                  <li><a href="{{ url('admin/buy-for-me') }}">Buy for Me</a></li>
                @endif

                @if((Auth::user()->UserPermission & PACKAGE) == PACKAGE)
                     <li><a href="{{ url('admin','local-delivery')}}">Local Delivery</a></li> 
                @endif

              </ul>
            </li>
          @endif

          @if(((Auth::user()->UserPermission & TRANSACTION) == TRANSACTION) )
            <li class="<?php if (in_array(Request::segment(2), array('transporter-transaction-history', 'requester-transaction-history'))) {echo 'nav-active';}?>">
              <a class="dropdown-toggle" href="{{ url('admin/transporter-transaction-history') }}" >
                <i class="fa fa-credit-card inline-fa"></i>
                <span>Transaction</span>
                {{--  <span class="caret"></span> --}}
              </a>
            </li>
          @endif

            <!--<li class="hover-drop">-->
          @if((Auth::user()->UserPermission & NOTIFICATION) == NOTIFICATION)
            <li class="<?php if (in_array(Request::segment(2), array('notification', 'send-notification', 'view-user'))) {echo 'nav-active';}?>" >
            	<a href="{{ url('admin/notification') }}">
					      <i class="fa fa-bell inline-fa"></i>
					      <span>Notification</span>
				      </a>
			      </li>
			    @endif

        @if(((Auth::user()->UserPermission & COUNTRY) == COUNTRY)  OR ((Auth::user()->UserPermission & STATE) == STATE)  OR ((Auth::user()->UserPermission & CITY) == CITY) OR ((Auth::user()->UserPermission & PROMOCODE) == PROMOCODE) OR ((Auth::user()->UserPermission & ITEMCATEGORY) == ITEMCATEGORY) OR ((Auth::user()->activitylog  == "yes")) OR (Auth::user()->region_charges  == "yes") OR (Auth::user()->item  == "yes") OR ((Auth::user()->local_delivery  == "yes")))
          <li class="<?php if (in_array(Request::segment(2), array('country_list', 'promocode-detail', 'edit-promocode', 'state_list', 'city_list', 'category_list', 'add-promocode', 'promocode', 'local-category-list', 'item', 'activity-log', 'distance', 'cron-mail', 'item-value', 'accra-mamagement', 'add-region','editcity','alert-msg'))) {echo 'nav-active';}?>">
            <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#">
              <i class="fa fa-cogs inline-fa"></i>
              <span>Management</span>
              <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
               @if((Auth::user()->UserPermission & COUNTRY) == COUNTRY)
              <li class="submenu"><a href="{{ url('admin/country_list') }}">City/State/Country
              <span class="caret-right"></span>
              </a>
              <ul class="submenuul">
              <li><a href="{{ url('admin/country_list') }}">Country</a></li>
               <li><a href="{{ url('admin/city_list') }}">City</a></li>
                 <li><a href="{{ url('admin/state_list') }}">State</a></li>
              </ul>
              </li>
              @endif
              @if(Auth::user()->item  == "yes")
                <li> <a href="{{ url('admin/item') }}">Suggested Item Weights</a></li>
              @endif
              @if((Auth::user()->UserPermission & ITEMCATEGORY) == ITEMCATEGORY)
                <li> <a href="{{ url('admin/category_list') }}">Item Category</a></li>
              @endif


              <!-- @if(Auth::user()->local_delivery  == "yes") -->
              <li class="submenu"><a href="{{ url('admin/local-category-list')}}">Local Delivery
              <span class="caret-right"></span>
              </a>
              <ul class="submenuul">
               <li><a href="{{ url('admin/local-category-list')}}">Category</a></li>
                <li> <a href="{{ url('admin/distance') }}">Distance</a></li>
                <li> <a href="{{ url('admin/item-value') }}">Item value</a></li>
              </ul>
              </li>
              <!-- @endif -->

             <!--  @if(Auth::user()->region_charges  == "yes") -->
                <li><a href="{{ url('admin/accra-mamagement')}}">Region Charges</a></li>
                <!-- @endif -->

              <!-- @if((Auth::user()->UserPermission & PROMOCODE) == PROMOCODE)
                <li> <a href="{{ url('admin/cron-mail') }}">Cron mail</a></li>
              @endif -->
              @if((Auth::user()->UserPermission & PROMOCODE) == PROMOCODE)
                <li> <a href="{{ url('admin/promocode') }}">Promocode</a></li>
              @endif
               @if(Auth::user()->activitylog  == "yes")
                <li> <a href="{{ url('admin/activity-log') }}">Activity Log</a></li>
              @endif
              <li> <a href="{{ url('admin/alert-msg') }}">Alert Message</a></li>
            </ul>
			    </li>
			  @endif



      <!--<li class="hover-drop">-->
      @if(((Auth::user()->UserPermission & APP_CONTENT) == APP_CONTENT) OR ((Auth::user()->UserPermission & EMAIL_TEMPLATE) == EMAIL_TEMPLATE) OR ((Auth::user()->UserPermission & WEB_CONTENT) == WEB_CONTENT) OR ((Auth::user()->UserPermission & APP_TUTORIAL) == APP_TUTORIAL) OR ((Auth::user()->UserPermission & ABOUT_APP) == ABOUT_APP) OR ((Auth::user()->UserPermission & FAQ) == FAQ) OR ((Auth::user()->Slider_images  == "yes")) OR ((Auth::user()->client_testtimonials  == "yes")))
        <li class="<?php if (in_array(Request::segment(2), array('app_content', 'web_content', 'app_tutorial', 'about_app', 'email_template', 'edit_email_template', 'faq_list', 'edit_faq', 'edit_app_tutorial', 'edit_web_content', 'edit_app_content', 'add_faq', 'silder-image', 'address', 'view_app_tutorial', 'client', 'add-client', 'edit-client', 'view-image-detail', 'edit-image'))) {echo 'nav-active';}?>">
          <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#">
            <i class="fa fa-folder inline-fa"></i>
            <span>Content</span>
            <span class="caret"></span>
          </a>

          <ul class="dropdown-menu">
            @if((Auth::user()->UserPermission & APP_CONTENT) == APP_CONTENT)
              <li><a href="{{ url('admin','app_content')}}">App Content</a></li>
            @endif
            @if((Auth::user()->UserPermission & EMAIL_TEMPLATE) == EMAIL_TEMPLATE)
              <li><a href="{{ url('admin','email_template')}}">Email Template</a></li>
            @endif
            @if(Auth::user()->Slider_images  == "yes")
              <li><a href="{{ url('admin','silder-image')}}">Slider images</a></li>
            @endif
            @if(Auth::user()->client_testtimonials  == "yes")
              <li><a href="{{ url('admin/client')}}">Client Testimonials</a></li>
            @endif
            @if((Auth::user()->UserPermission & WEB_CONTENT) == WEB_CONTENT)
              <li><a href="{{ url('admin','web_content')}}">Web Content</a></li>
            @endif
            @if((Auth::user()->UserPermission & APP_TUTORIAL) == APP_TUTORIAL)
              <li><a href="{{ url('admin','app_tutorial')}}">App Tutorial</a></li>
            @endif
            @if((Auth::user()->UserPermission & ABOUT_APP) == ABOUT_APP)
              <li><a href="{{ url('admin','about_app')}}">About App</a></li>
            @endif
            <!--@if((Auth::user()->UserPermission & FAQ) == FAQ)
              <li><a href="{{ url('admin','faq_list')}}">FAQ</a></li>
            @endif -->
            @if((Auth::user()->UserPermission & AQUANTUO_ADDRESS) == AQUANTUO_ADDRESS)
              <li><a href="{{ url('admin','address')}}">Aquantuo address </a></li>
            @endif

            <!--@if((Auth::user()->UserPermission & SECTION) == SECTION)
              <li><a href="{{ url('admin','section')}}">Section</a></li>
            @endif-->
          </ul>
        </li>
      @endif

      @if(((Auth::user()->UserPermission & INDIVIDUAL_TRIP) == INDIVIDUAL_TRIP) OR ((Auth::user()->UserPermission & BUSSINESS_TRIP) == BUSSINESS_TRIP))
        <li class="<?php if (in_array(Request::segment(2), ['business-trip', 'indiviual-trip', 'edit-individual-trip', 'edit-business-trip', 'trips_request', 'trip-detail'])) {echo 'nav-active';}?>">
          <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#">
            <i class="fa fa-plane inline-fa"></i>
            <span>Trip</span>
            <span class="caret"></span>
          </a>
          <ul class="dropdown-menu">
            @if((Auth::user()->UserPermission & BUSSINESS_TRIP) == BUSSINESS_TRIP)
              <li><a href="{{ url('admin','business-trip')}}">Business Trip</a></li>
            @endif
            @if((Auth::user()->UserPermission & INDIVIDUAL_TRIP) == INDIVIDUAL_TRIP)
              <li><a href="{{ url('admin','indiviual-trip')}}">Individual Trip</a></li>
            @endif
          </ul>
        </li>
    @endif



    @if((Auth::user()->UserPermission & SUPPORT) == SUPPORT)
      <li class="<?php if (in_array(Request::segment(2), ['support_view_more', 'support', 'section', 'faq', 'faq-answer', 'edit-faq', 'faq-answer', 'add-section', 'add-faq'])) {echo 'nav-active';}?>">
        <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#">
          <i class="fa fa-question-circle inline-fa"></i>
          <span>Support</span>
          <span class="caret"></span>
        </a>
        <ul class="dropdown-menu">
          <li><a href="{{ url('admin','support')}}">Support</a></li>
          @if(Auth::user()->help_section  == "yes")
            <li><a href="{{ url('admin','section')}}">Help Section</a></li>
          @endif
          
          @if(Auth::user()->user_feedback  == "yes")
            <li><a href="{{ url('admin/complains')}}">User Feedback</a></li>
          @endif
        </ul>
      </li>
    @endif


    @if((Auth::user()->UserPermission & CONFIGURATION) == CONFIGURATION)
      <li class="<?php if (in_array(Request::segment(2), ['configuration'])) {echo 'nav-active';}?>">
        <a href="{{ url('admin','configuration')}}">
          <i class="fa fa-cog inline-fa"></i>
          <span>Configuration</span>
        </a>
      </li>
    @endif

       <li class="<?php if (in_array(Request::segment(2), ['slip-list', 'packing-list', 'received-slip-list', 'editcity', 'slip-management', 'get-slip', 'get-packing-slip', 'packing-slip-list'])) {echo 'nav-active';}?>">
        <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#">
          <i class="  fa fa-file-text-o"></i>
          <span>Slip Management</span>
          <span class="caret"></span>
        </a>
        <ul class="dropdown-menu">
          @if(Auth::user()->delivery_slip  == "yes")
            <li><a href="{{url('admin','slip-list')}}"> Delivery List </a></li>
          @endif
          @if(Auth::user()->packag_slip  == "yes") 
            <li><a href="{{url('admin','packing-slip-list')}}"> Packing List </a></li>
          @endif

          @if(Auth::user()->received_slip  == "yes") 
          <li><a href="{{url('admin','received-slip-list')}}?op=op"> Received Items List </a></li>
          @endif

        </ul>
      </li>

      </li>
   </ul>
</div>
@endif
<div class="side-wrap-custom">
<nav class="navbar navbar-default navbar-inverse margin-bottom-0 border-radious nav_bg hidden-xs">
   <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
         <!-- <a aria-expanded="false" data-target="#bs-example-navbar-collapse-1" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
<i class="fa fa-cog color-white"></i>
          </a>-->
          <a href="{{ url('admin/dashboard')}}" class="navbar-brand visible">{!! Html::image('theme/admin/custome/images/logo_1242.png') !!}</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div id="bs-example-navbar-collapse-1" class="collapse navbar-collapse">

           @if(Auth::user())
          <!--<ul class="nav navbar-nav navbar-right show-sm">

          	<li class="dropdown">
              <!--<a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="fa fa-user"></i> Welcome Admin </a>-->
				 <!--</li>
                <li><a href="#"><i class="fa fa-cogs"></i> Setting</a></li>
                <li><a href="{{ url('admin','change_password')}}"><i class="fa fa-exchange"></i> Change Password</a></li>
                <li class="divider" role="separator"></li>
                <li><a href="#"><i class="fa fa-sign-out"></i> Logout</a></li>
          </ul>-->

          <ul class="nav navbar-nav navbar-right hide-sm">
            <li class="dropdown custom_drop">
              <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="fa fa-user"></i> Welcome {{Auth::user()->UserName}} <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="{{ url('admin','change_password')}}"><i class="fa fa-exchange blue"></i> Change Password</a></li>
                <li class="divider" role="separator"></li>
                @if((Auth::user()->UserPermission & SETTING) == SETTING)
                <li><a href="{{ url('admin/setting') }}"><i class="fa fa-cogs blue"></i>
                   <!--    <a href="{{ url('admin','setting')}}">  -->
                Settings</a></li>
                @endif
                <li><a class="side-menu1 side-nav" onclick="manage_side_menu('big-side-bar')"><i class="fa fa-bars"></i> Side navbar</a></li>
                  <li><a class="side-menu1 main-nav" onclick="manage_side_menu('')" ><i class="fa fa-bars"></i> Main navbar</a></li>
                <li><a href="{{ url('admin/logout') }}"><i class="fa fa-sign-out red"></i> Logout</a></li>
              </ul>
            </li>
          </ul>

          <ul class="nav navbar-nav navbar-right hide-sm">
            <li class="dropdown custom_drop">
              <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle newbadge" href="#"><i style="font-size:16px" class="fa fa-bell"></i> <span style="background:#F00;" class="badge" id="badgecount">0</span><span class="caret"></span></a>

            <ul class="noti-dropd dropdown-menu">
              <div id="badge_html" class="list-group bg">
              </div>
              <div id="view_btn"></div>
           <!--  <a class="active text-center" href="{{ url('admin/admin_notification') }}"><div class="custom-view-btn">View</div></a> -->
            </ul>

            </li>
          </ul>

           @endif
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
      </nav>

      @if(Auth::user())
      <nav class="navbar navbar-default navbar-inverse border-radious custom-navbar">
       <div class="container-fluid subnavbar">
        <!-- Brand and toggle get grouped for better mobile display -->

        <div class="pull-left visible-xs-block custom-brand">
        <a href="http://192.168.11.101:8000/admin"><img src="{{url('theme/admin/custome/images/logo_1242-sm.png')}}"></a>
        </div>

        <div class="navbar-header">
          <button aria-expanded="false" data-target="#bs-example-navbar-collapse-2" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div id="bs-example-navbar-collapse-2" class="collapse navbar-collapse subnav-collapse pad-0 ">
         <div>
		 <a href="{{ url('admin/dashboard')}}">{!! Html::image('theme/admin/custome/images/logo_1242.png','',['class'=>'fixed-nav-logo fixed-nav-logo1']) !!}</a>

          <ul class="nav navbar-nav nav-left">
           @if(Auth::user())



          	<li class="dropdown custom_drop show-sm">
              <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="fa fa-user"></i>
               Welcome {{Auth::user()->UserName}} &nbsp; <span class="caret"></span> </a>
              <ul class="dropdown-menu">
                <li><a href="#"><!--<i class="fa fa-cogs"></i>--> Setting</a></li>
                <li><a href="{{ url('admin','change_password')}}"><!--<i class="fa fa-exchange"></i>--> Change Password</a></li>

                <li><a href="#"><!--<i class="fa fa-sign-out"></i>--> Logout</a></li>
                </ul>
          	</li>
           @endif
            <li class="<?php if (in_array(Request::segment(2), array('dashboard'))) {echo 'nav-active';}?>">
            	<a href="{{ url('admin','dashboard')}}">
					<i class="fa fa-home inline-fa fa-width"></i>
					<span>Home</span>
				</a>
			</li>
              <!--<li class="hover-drop">-->
              @if(((Auth::user()->UserPermission & ADMIN) == ADMIN) OR ((Auth::user()->UserPermission & SENDER) == SENDER) OR ((Auth::user()->UserPermission & CARRIER) == CARRIER))
              <li class="<?php if (in_array(Request::segment(2), array('transporter', 'edit-transporter', 'edit_admin_user', 'user', 'edit-bank', 'requester', 'admin_user', 'transaction-history', 'add_admin_user'))) {echo 'nav-active';}?>">
            	<a id="dropdown-toggle1" aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#">
					<i class="fa fa-user inline-fa"></i>
					<span>User</span>
                    <span class="caret"></span>
				</a>

                <ul id="menu1" class="dropdown-menu">
					@if((Auth::user()->UserPermission &  SENDER) == SENDER)
						<li><a href="{{ url('admin','transporter')}}">Transporter</a></li>
					@endif
					@if((Auth::user()->UserPermission & CARRIER) == CARRIER)
						<li><a href="{{ url('admin','requester')}}"></i>Requester</a></li>
					@endif
					@if((Auth::user()->UserPermission & ADMIN) == ADMIN)
						<li><a href="{{ url('admin','admin_user')}}"></i>Admin</a></li>
					@endif
				</ul>
			</li>
			@endif

         @if(((Auth::user()->UserPermission & PACKAGE) == PACKAGE) OR ((Auth::user()->UserPermission & ONLINEPACKAGE) == ONLINEPACKAGE) OR ((Auth::user()->UserPermission & BUYFORME) == BUYFORME))
        <li class="<?php if (in_array(Request::segment(2), array('on-line-purchases', 'package', 'post-buy-for-me', 'edit-online-package', 'online_package', 'post-buyforme', 'online-package', 'buy-for-me', 'process-card-list', 'add-package', 'edit-package', 'comment-list', 'add-send-a-package', 'edit-request'))) {echo 'nav-active';}?>">
              <a id="dropdown-toggle2" aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#">
                <i class="fa fa-archive inline-fa"></i>
                <span>Package</span>
                <span class="caret"></span>
              </a>
              <ul id="menu2" class="dropdown-menu">

              @if((Auth::user()->UserPermission & PACKAGE) == PACKAGE)
                      <li><a href="{{ url('admin','package')}}">Send a Package</a></li>
                 @endif
                @if((Auth::user()->UserPermission & ONLINEPACKAGE) == ONLINEPACKAGE)
                  <li><a href="{{ url('admin/online-package') }}">Online Package</a></li>
                @endif

                 @if((Auth::user()->UserPermission & BUYFORME) == BUYFORME)
                     <li><a href="{{ url('admin/buy-for-me') }}">Buy for Me</a></li>
                  @endif

                  @if((Auth::user()->UserPermission & PACKAGE) == PACKAGE)
                      <li><a href="{{ url('admin','local-delivery')}}">Local Delivery</a></li> 
                @endif

              </ul>
        </li>
      @endif


      @if(((Auth::user()->UserPermission & TRANSACTION) == TRANSACTION) )

      <li class="<?php if (in_array(Request::segment(2), array('transporter-transaction-history', 'requester-transaction-history'))) {echo 'nav-active';}?>">
          <a class="dropdown-toggle" href="{{ url('admin/transporter-transaction-history') }}">
          <i class="fa fa-credit-card inline-fa"></i>
          <span>Transaction</span>
        </a>

      </li>
      @endif

			@if((Auth::user()->UserPermission & NOTIFICATION) == NOTIFICATION)
			<li class="<?php if (in_array(Request::segment(2), array('notification', 'send-notification', 'view-user'))) {echo 'nav-active';}?>" >
				<a href="{{ url('admin/notification') }}">
						  <i class="fa fa-bell inline-fa"></i>
							<span>Notification</span>
				</a>
			</li>
			@endif

      @if(((Auth::user()->UserPermission & COUNTRY) == COUNTRY)  OR ((Auth::user()->UserPermission & STATE) == STATE)  OR ((Auth::user()->UserPermission & CITY) == CITY) OR ((Auth::user()->UserPermission & PROMOCODE) == PROMOCODE) OR ((Auth::user()->UserPermission & ITEMCATEGORY) == ITEMCATEGORY) OR ((Auth::user()->activitylog  == "yes")) OR (Auth::user()->region_charges  == "yes") OR (Auth::user()->item  == "yes") OR (Auth::user()->local_delivery  == "yes"))
        <li class="<?php if (in_array(Request::segment(2), array('country_list', 'promocode-detail', 'edit-promocode', 'state_list', 'city_list', 'category_list', 'add-promocode', 'promocode', 'local-category-list', 'item', 'activity-log', 'distance', 'cron-mail', 'item-value', 'accra-mamagement', 'add-region','alert-msg'))) {echo 'nav-active';}?>">
          <a id="dropdown-toggle5" aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#">
				    <i class="fa fa-cogs inline-fa"></i>
					  <span>Management</span>
					  <span class="caret"></span>
          </a>

				  <ul id="menu5" class="dropdown-menu">
             @if((Auth::user()->UserPermission & COUNTRY) == COUNTRY)
              <li class="submenu"><a href="{{ url('admin/country_list') }}">City/State/Country
              <span class="caret-right"></span>
              </a>
              <ul class="submenuul">
              <li><a href="{{ url('admin/country_list') }}">Country</a></li>
               <li><a href="{{ url('admin/city_list') }}">City</a></li>
                 <li><a href="{{ url('admin/state_list') }}">State</a></li>
              </ul>
              </li>
              @endif
           
            @if(Auth::user()->item  == "yes")
              <li> <a href="{{ url('admin/item') }}">Suggested Item Weights</a></li>
            @endif
            @if((Auth::user()->UserPermission & ITEMCATEGORY) == ITEMCATEGORY)
              <li> <a href="{{ url('admin/category_list') }}">Item Category</a></li>
            @endif


            <!-- @if(Auth::user()->local_delivery  == "yes") -->
              <li class="submenu"><a href="{{ url('admin/local-category-list')}}">Local Delivery
              <span class="caret-right"></span>
              </a>
              <ul class="submenuul">
               <li><a href="{{ url('admin/local-category-list')}}">Category</a></li>
                <li> <a href="{{ url('admin/distance') }}">Distance</a></li>
                <li> <a href="{{ url('admin/item-value') }}">Item value</a></li>
              </ul>
              </li>
            <!-- @endif -->
            <!-- <li class="submenu"><a href="{{ url('admin/local-category-list')}}">Local Delivery
              <span class="caret-right"></span>
              </a>
              <ul class="submenuul">
               <li><a href="{{ url('admin/local-category-list')}}">Category</a></li>
                <li> <a href="{{ url('admin/distance') }}">Distance</a></li>
                <li> <a href="{{ url('admin/item-value') }}">Item value</a></li>
              </ul>
              </li> -->

            @if(Auth::user()->region_charges  == "yes")
            <li><a href="{{ url('admin/accra-mamagement')}}">Region Charges</a></li>
             @endif
             

         <!--    @if((Auth::user()->UserPermission & PROMOCODE) == PROMOCODE)
                <li> <a href="{{ url('admin/cron-mail') }}">Cron mail</a></li>
            @endif -->
            @if((Auth::user()->UserPermission & PROMOCODE) == PROMOCODE)
              <li> <a href="{{ url('admin/promocode') }}">Promocode</a></li>
            @endif
             @if(Auth::user()->activitylog  == "yes")
                <li> <a href="{{ url('admin/activity-log') }}">Activity Log</a></li>
            @endif
            <li> <a href="{{ url('admin/alert-msg') }}">Alert Message</a></li>
          </ul>
		    </li>
			@endif

      <!--<li class="hover-drop">-->
      @if(((Auth::user()->UserPermission & APP_CONTENT) == APP_CONTENT) OR ((Auth::user()->UserPermission & EMAIL_TEMPLATE) == EMAIL_TEMPLATE) OR ((Auth::user()->UserPermission & WEB_CONTENT) == WEB_CONTENT) OR ((Auth::user()->UserPermission & APP_TUTORIAL) == APP_TUTORIAL) OR ((Auth::user()->UserPermission & ABOUT_APP) == ABOUT_APP) OR ((Auth::user()->UserPermission & FAQ) == FAQ) OR (Auth::user()->Slider_images  == "yes") OR ((Auth::user()->client_testtimonials  == "yes")))
        <li class="<?php if (in_array(Request::segment(2), array('app_content', 'web_content', 'app_tutorial', 'about_app', 'email_template', 'edit_email_template', 'faq_list', 'edit_faq', 'edit_app_tutorial', 'edit_web_content', 'edit_app_content', 'add_faq', 'silder-image', 'address', 'view_app_tutorial', 'client', 'add-client', 'edit-client', 'view-image-detail', 'edit-image'))) {echo 'nav-active';}?>">
          <a id="dropdown-toggle3" aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#">
            <i class="fa fa-folder inline-fa"></i>
            <span>Content</span>
            <span class="caret"></span>
          </a>

          <ul id="menu3" class="dropdown-menu">
            @if((Auth::user()->UserPermission & APP_CONTENT) == APP_CONTENT)
              <li><a href="{{ url('admin','app_content')}}">App Content</a></li>
            @endif
            @if((Auth::user()->UserPermission & WEB_CONTENT) == WEB_CONTENT)
              <li><a href="{{ url('admin','web_content')}}">Web Content</a></li>
            @endif
            @if((Auth::user()->UserPermission & APP_TUTORIAL) == APP_TUTORIAL)
              <li><a href="{{ url('admin','app_tutorial')}}">App Tutorial</a></li>
            @endif
            @if((Auth::user()->UserPermission & ABOUT_APP) == ABOUT_APP)
              <li class="submenu"><a href="{{ url('admin','about_app')}}">About App</a></li>
            @endif
            @if((Auth::user()->UserPermission & EMAIL_TEMPLATE) == EMAIL_TEMPLATE)
              <li><a href="{{ url('admin','email_template')}}">Email Template</a></li>
            @endif
            @if(Auth::user()->Slider_images  == "yes")
              <li><a href="{{ url('admin','silder-image')}}">Slider images</a></li>
            @endif
            @if(Auth::user()->client_testtimonials  == "yes")
              <li><a href="{{ url('admin/client')}}">Client Testimonials</a></li>
            @endif

     <!-- @if((Auth::user()->UserPermission & FAQ) == FAQ)
            <li><a href="{{ url('admin','faq_list')}}">FAQ</a></li>
          @endif -->
          @if((Auth::user()->UserPermission & AQUANTUO_ADDRESS) == AQUANTUO_ADDRESS)
            <li><a href="{{ url('admin','address')}}">Aquantuo address</a></li>
          @endif


      <!--@if((Auth::user()->UserPermission & SECTION) == SECTION)
            <li><a href="{{ url('admin','section')}}">Section</a></li>
          @endif -->
          </ul>
      </li>
      @endif

      @if(((Auth::user()->UserPermission & INDIVIDUAL_TRIP) == INDIVIDUAL_TRIP) OR ((Auth::user()->UserPermission & BUSSINESS_TRIP) == BUSSINESS_TRIP))
        <li class="<?php if (in_array(Request::segment(2), ['business-trip', 'indiviual-trip', 'edit-individual-trip', 'edit-business-trip', 'trips_request', 'trip-detail'])) {echo 'nav-active';}?>">
          <a id="dropdown-toggle4" aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#">
            <i class="fa fa-plane inline-fa"></i>
            <span>Trip</span>
            <span class="caret"></span>
          </a>

          <ul id="menu4" class="dropdown-menu">
            @if((Auth::user()->UserPermission & BUSSINESS_TRIP) == BUSSINESS_TRIP)
              <li><a href="{{ url('admin','business-trip')}}">Business Trip</a></li>
            @endif
            @if((Auth::user()->UserPermission & INDIVIDUAL_TRIP) == INDIVIDUAL_TRIP)
              <li><a href="{{ url('admin','indiviual-trip')}}">Individual Trip</a></li>
            @endif
          </ul>
		    </li>
		  @endif

      @if((Auth::user()->UserPermission & SUPPORT) == SUPPORT)
        <li class="<?php if (in_array(Request::segment(2), ['support_view_more', 'support', 'section', 'faq', 'faq-answer', 'edit-faq', 'faq-answer', 'add-section', 'add-faq'])) {echo 'nav-active';}?>">
          <a id="dropdown-toggle6" aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#">
            <i class="fa fa-question-circle inline-fa"></i>
            <span>Support</span>
            <span class="caret"></span>
          </a>
          <ul id="menu6" class="dropdown-menu">
            <li><a href="{{ url('admin','support')}}">Support</a></li>
            @if(Auth::user()->help_section  == "yes")
              <li><a href="{{ url('admin','section')}}">Help Section</a></li>
            @endif
            @if(Auth::user()->user_feedback  == "yes")
            <li><a href="{{ url('admin/complains')}}">User Feedback</a></li>
            @endif
          </ul>
        </li>
      @endif

		  @if((Auth::user()->UserPermission & CONFIGURATION) == CONFIGURATION)
				<li class="<?php if (in_array(Request::segment(2), ['configuration'])) {echo 'nav-active';}?>">
				  <a href="{{ url('admin','configuration')}}">
			      <i class="fa fa-cog inline-fa"></i>
			      <span>Configuration</span>
			    </a>
		    </li>
		  @endif

        <li class="<?php if (in_array(Request::segment(2), ['slip-list', 'packing-list', 'received-slip-list', 'slip-management', 'get-slip', 'get-packing-slip', 'packing-slip-list'])) {echo 'nav-active';}?>">

        <a id="dropdown-toggle6" aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#">
          <i class="fa fa-file-text-o inline-fa"></i>
          <span>Slip Management</span>
          <span class="caret"></span>
        </a>
        <ul class="dropdown-menu">
          @if(Auth::user()->delivery_slip  == "yes")
            <li><a href="{{url('admin','slip-list')}}"> Delivery List </a></li>
          @endif
          @if(Auth::user()->packag_slip  == "yes") 
            <li><a href="{{url('admin','packing-slip-list')}}"> Packing List </a></li>
          @endif

          @if(Auth::user()->received_slip  == "yes") 
          <li><a href="{{url('admin','received-slip-list')}}?op=op"> Received Items List </a></li>
          @endif
        </ul>
      </li>

    </ul>

      @if(Auth::user())
        <ul class="nav navbar-nav navbar-right fixed-nav-dropd">
          <li class="dropdown custom_drop">
            <a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="fa fa-user"></i><span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="#"><i class="fa fa-cogs blue"></i> Setting</a></li>
              <li><a href="{{ url('admin','change_password')}}"><i class="fa fa-exchange blue"></i> Change Password</a></li>
              <li class="divider" role="separator"></li>
              <li><a href="{{ url('admin/logout') }}"><i class="fa fa-sign-out red"></i> Logout</a></li>
            </ul>
          </li>
        </ul>
      @endif
    </div>

    </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
  </nav>
 @endif


<div class="container-fluid" id='error_msg_section'>

   @if(Session::has('success'))
    <div class="alert alert-success" id="success-alert">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<strong>Success!</strong> {{Session::get('success')}}
    </div>
    @endif

   @if(Session::has('fail'))
    <div class="alert alert-danger" id="success-alert">

			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<strong>Fail!</strong> {{Session::get('fail')}}

    </div>
	@endif
</div>
<div class="wrapper">
	<div id="loading" style="display: none;"></div>

