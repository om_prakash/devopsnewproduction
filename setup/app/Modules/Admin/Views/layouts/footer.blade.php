</div>
</div>
</div>
<div class="footer">

<div class="container-fluid">

<div class="row">

<div class="col-md-6 col-sm-6 col-xs-6">

Copyright © <?php echo date("Y"); ?> Aquantuo.

</div>

<div class="col-md-6 col-sm-6 col-xs-6 text-right res-right">

<!-- Designed & Developed by <a href="http://idealittechno.com/" target="_blank">Ideal IT Techno</a> -->

</div>

</div>

</div>

</div>
<style>

html, body {
	height: 100%;
}
.wrapper {
	margin: 0 auto; /* the bottom margin is the negative value of the footer's height */
}
.footer {
	height: 55px; /* .push must be the same height as .footer */
}

</style>

<script type="text/javascript">
$(window).scroll(function()
{
    if ($(window).scrollTop() > 100)
    {
        $('.fading-ef').css('opacity','1');
    }
    else
    {
        $('.fading-ef').css('opacity','0');
    }
});

		/*setInterval(function(){
				$.ajax({
					url: SITEURL+"ajax/badge",
					type:"GET",
					success: function(results){
		           		results = eval("("+results+")");
		  				$('#badgecount').html(results.count);
		            	$('#badge_html').html(results.html);
		        	}
		    	});
		}, 10000);*/


		$(document).ready(function(e) {
            $('.change_view').click(function(){
				$('.side-menu.change-side-menu > .nav.navbar-nav.nav-left > li > .navbar-brand1').css('display','block');
			});
        });



function open_sidebar()
	{
		if(screen.width > 1024)
		{
			if(window.name == 'big-side-bar')
			{
				$('.side-menu').removeClass('change-side-menu');
				$('.wrapper').css({'min-height':(($(window).height())-122)+'px'});
				$('.custom-navbar').css('display','none');
				$('.side-wrap-custom,.footer').css('padding-left','250px');
				$('.side-wrap-custom,.footer').css('width','100%');
				$('.side-wrap-custom,.footer').css('float','right');
				$('.footer').css('right','0px');
				$('.side-menu').css('transform','translateX(0%)');
				$('.dropdown-menu').css('box-shadow','none');
				$('.side-nav,.navbar-brand1').css('display','none');
				$('.main-nav').css('display','block');
				$('.navbar-brand').css('display','block');
				$('.fixed-back').css('width','250px');
				$('.fixed-back').css('display','block');
				$('.navbar.navbar-default.navbar-inverse.margin-bottom-0.border-radious.nav_bg.hidden-xs').css('margin-bottom','15px');



			} else if(window.name == 'small-bar')
			{
				$('.wrapper').css({'min-height':(($(window).height())-231)+'px'});
				$('.custom-navbar').css('display','none');
				$('.side-wrap-custom,.footer').css('padding-left','70px');
				$('.side-wrap-custom,.footer').css('float','right');
				$('.side-wrap-custom,.footer').css('width','100%');
				$('.fixed-back').css('width','70px');
				$('.footer').css('right','0px');
				$('.side-menu').css('transform','translateX(0%)');
				$('.dropdown-menu').css('box-shadow','none');
				$('.side-nav').css('display','none');
				$('.main-nav,.navbar-brand1').css('display','block');
				$('.navbar.navbar-default.navbar-inverse.margin-bottom-0.border-radious.nav_bg.hidden-xs').css('margin-bottom','15px');
				$('.side-menu').addClass('change-side-menu');
				$('.side-nav,.navbar-brand').css('display','none');
				$('.main-nav,.visible').css('display','block');
				$('.fixed-back').css('display','block');
			}
			else
			{
				$('.side-wrap-custom,.footer').css('padding-left','0px');
				$('.wrapper').css({'min-height':(($(window).height())-231)+'px'});
				$('.fading-ef').css('display','block');
				$('.custom-navbar').css('display','block');
				$('.side-wrap-custom,.footer').css('width','100%'),('padding-left','0px');
				$('.side-menu').css('transform','translateX(-100%)');
				$('.side-nav').css('display','block');
				$('.fixed-back').css('display','block');
				$('.main-nav,.navbar-brand1').css('display','none');
$('.fixed-back').css('display','none');
				$('.navbar.navbar-default.navbar-inverse.margin-bottom-0.border-radious.nav_bg.hidden-xs').css('margin-bottom','0px');
			}
		}
	}


	function manage_side_menu(type)
	{
		if(window.name == 'small-bar' & type == 'small-bar'){ type = 'big-side-bar'; }

		window.name = type;
		open_sidebar();

	}
	open_sidebar();



</script>

<script type="text/javascript">
function badgeNotify(){
			$.ajax({
				url: SITEURL+"ajax/badge",
				type:"GET",
				success: function(results){
	           		results = eval("("+results+")");
	  				$('#badgecount').html(results.count);
	            	$('#badge_html').html(results.html);
	            	$('#view_btn').html(results.view);
	        	}
	    	});
	}
	setInterval(badgeNotify, 10000);
	badgeNotify();

@if (!Session::has('tzo'))
   if (navigator.cookieEnabled) {
   	 document.location.href = SITEURL+'set-datetime?tzo=' + (- new Date().getTimezoneOffset()) + '&href=' + document.location.href;
   }
@endif

</script>

