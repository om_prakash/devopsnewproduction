<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
    <meta name="_token" content="{!! csrf_token() !!}"/>
    <meta name="csrf-token" content="{{{ Session::token() }}}">

    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <link rel="icon" type="image/png" href="{{ url('theme/web/promo/images/favicon.png')}}"/>
    <title>Aquantuo</title>
		<!-- Bootstrap 3.3.4 Style-->
		{!! Html::style('theme/admin/bootstrap_335/css/bootstrap.min.css') !!}
		{!! Html::style('theme/admin/bootstrap_335/css/bootstrap-theme.css') !!}
		
		<!-- Font Awesome Icons -->
		{!! Html::style('theme/admin/fontawesome/css/font-awesome.min.css') !!}

		<!-- Custome Default Style -->
		{!! Html::style('theme/admin/custome/css/style.css') !!}

		{!! Html::style('theme/admin/custome/css/jquery.autocomplete.css') !!}
		<!-- Search Autoload -->
{!! Html::script('theme/web/js/jquery.js') !!}
		<!-- Custome Responsive Style -->
		{!! Html::script('theme/admin/custome/js/jquery.js') !!}
		{!! Html::style('theme/admin/custome/css/responsive.css') !!}

		{!! Html::script('theme/admin/custome/js/min.js') !!}
		<!-- Bootstrap 3.3.4 Script -->
		{!! Html::script('theme/admin/bootstrap_335/js/bootstrap.min.js') !!}
		{!! Html::script('theme/admin/bootstrap_335/js/jquery.min.js') !!}
	<style type="text/css">

.red-star{
			color:#a94442;
		}

		.input-group.error-cm-input .error-msg {
    bottom: -15px;
    left: 0;
    position: absolute;
    width: 179px;
    font-size: 12px;
}

.input-group.error-cm-input {
    position: relative !important;
}
	</style>


    <script>
		var SITEURL 		= '{{url('')}}/';
		var PAGE_LIMIT = '{{isset(Auth::user()->PaginationLimit)? Auth::user()->PaginationLimit : 10 }}';


    </script>

<?php
echo <<<EOE
 <script type="text/javascript">
   if (navigator.cookieEnabled)
     document.cookie = "tzo="+ (- new Date().getTimezoneOffset());
 </script>
EOE;
if (!isset($_COOKIE['tzo'])) {
    echo <<<EOE
    <script type="text/javascript">
      if (navigator.cookieEnabled) window.location.reload();
      else alert("Cookies must be enabled!");
    </script>
EOE;
}
?>

  </head>
  <body>

@yield('content')

<!-- Custome Default JS -->
{!! Html::script('theme/admin/custome/js/pagination.js') !!}

  </body>
</html>
