{!! Html::script('theme/admin/custome/js/piejquery.min.js') !!}
{!! Html::script('theme/admin/custome/js/highcharts.js') !!}
{!! Html::script('theme/admin/custome/js/exporting.js') !!}


            
<script type="text/javascript">

  $(function () {

    // Make monochrome colors and set them as default for all pies
    Highcharts.getOptions().plotOptions.pie.colors = (function () {
        var colors = [],
            base = Highcharts.getOptions().colors[0],
            i;

        for (i = 0; i < 10; i += 1) {
            // Start out with a darkened base color (negative brighten), and end
            // up with a much brighter color
            colors.push(Highcharts.Color(base).brighten((i - 3) / 7).get());
        }
        return colors;
    }());

    // Build the chart
    $('#container').highcharts({
        chart: {
            plotBackgroundColor	: '#F1F2F7',
            plotBorderWidth		: null,
            plotShadow			: false,
            type				: 'pie'
        },
        title: {
            text: ''
        },
        credits: {
    		enabled: false
  		},
  		exporting: { 
  			enabled: false 
  		},
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect	: true,
                		cursor		: 'pointer',
                dataLabels: {
                    		enabled	: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    },
                }
            }
        },
        series: [{
            name: 'Brands',
            data: [
                { name: 'Pending',          y: {{ $pending }}},
                { name: 'Out for Delivery', y: {{ $out_for_delivery }} },
                { name: 'Ready',            y: {{ $ready}} },
                { name: 'On Delivery',      y: {{ $on_delivery }} },
                { name: 'Delivered',        y: {{ $delivered }} },
                { name: 'Not Delivered',    y: {{ $no_delivery}} }
            ]
        }]
    });
});
</script>

