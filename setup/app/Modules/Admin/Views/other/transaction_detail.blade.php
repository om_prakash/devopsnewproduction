@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')

@section('content')

<div class="container-fluid">
	<div class="panel panel-default">
		<div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;Product Detail
		
		<a class="btn btn-primary margin-top-less-7 pull-right" onclick="window.history.back()">Go Back</a>
		</div>
		<div class="panel-body">
			 <div class="col-md-6">
				 <b>Carrier Name</b> : {{$info->CarrierName}}
			</div>
			<div class="col-md-6">
				 <b>Paid Amount</b> : {{$info->DeliveryCost}}
			</div>
			
			<div class="col-md-6">
				<b>Date</b>: <?php
							if(isset($info->DeliveryDate->sec)){
								echo date('d-M-Y',$info->DeliveryDate->sec);
							}
					?>
			</div>
			<div class="col-md-6">				
				<b>Payment Status</b> : {{$info->PaymentStatus}}
			</div>
			<div class="clearfix"></div>
		</div> <!-- Panel Body -->
	</div>

@include('Admin::layouts.footer')

@stop
