@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')

@section('content')

<div class="container-fluid">

<div class="panel panel-default">
  <div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;Send Notification</div>
  <div class="panel-body">
  		 
      <!-- Pagination Section-->
	<input type="hidden" name="urlvalue" id="urlvalue" value="{{$paginationurl}}" />
	<input type="hidden" name="postvalue" id="postvalue" value="{{$postvalue}}" />
	<input type="hidden" name="orderby" id="orderby" value="{{$orderby}}" />
	<input type="hidden" name="orderType" id="orderType" value="{{$orderType}}" />

	{!! Form::model('', ['name' => 'actionform', 'method' => 'POST', 'url' => ['admin/post-notificatione'], 'id' => 'actionform']) !!}
		<input type="hidden" id="sendAll" name="sendAll">
		<input type="hidden" name="user_list" value="" id="user_list" >
		<div class="control-group" >
			
			<div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}"  >
				{!! Form::label('Title', 'Notification Title',['class' => 'col-md-2 text-right']) !!}
				<div class="col-md-10">
					{!! Form::text('title', '', ['class'=>'form-control', 'id' =>'inputError1']) !!}
					@if ($errors->has('title')) <p class="help-block">{{ $errors->first('title') }}</p> @endif
				</div>				
				<div class="clearfix"></div>
			</div>

			<div class="form-group {{ $errors->has('message') ? ' has-error' : '' }}"  >
				{!! Form::label('Message', 'Notification Message',['class' => 'col-md-2 text-right']) !!}
				<div class="col-md-10">
					{!! Form::textarea('message', '', ['class'=>'form-control', 'id' =>'inputError1']) !!}
					@if ($errors->has('message')) <p class="help-block">{{ $errors->first('message') }}</p> @endif
				</div>				
				<div class="clearfix"></div>
			</div>					

			<div class="form-group ">
				{!! Form::label('UserGroup', 'User Group',['class' => 'col-md-2 text-right']) !!}
				<div class="col-md-10">
					{!! Form::select('groupto', array(''=>'Select Group','requester' => 'All Requester', 'transporter' => 'All Transporter'),Input::get('class'),['class' => 'form-control','id'=>'groupto','onchange'=>'prepare_data()']); !!}
				</div>
				<div class="clearfix"></div>
			</div> 	
			<div class="clearfix"></div>	
			<div class="col-md-10 col-md-offset-2">
	            <div class="panel panel-default">
	                <div class="panel-body">
	                    <div class="chosen-container chosen-container-multi chosen-container-active" style="" title="">
	                        <ul class="chosen-choices" id="add_li">
	                            <p id="add_note"><strong>Note: </strong> Notifification will be sent to this slected users.</p>
	                        </ul>
	                    </div>
	            	</div>
	        	</div>
	    	</div>
			<div class="clearfix"></div>
			<div class="form-group ">
				{!! Form::checkbox('chk_all_del', '', false, ['id' => 'chk_all_del'] ) !!}&nbsp			
				{!! Form::label('name', 'Select all user (For current page only)',['class' => 'notificationlable']) !!}
				<hr/>

			</div>
		</div>
    
	<div id="containerdata"></div>
	
		<input type="hidden" name="totalnoti" id="totalnotiid" value="">
		<?php echo Form::submit('Submit', ['name' => 'submit', 'class' => 'btn btn-primary', 'id' => 'save_add_new']) ?>
	{!! Form::close() !!}
	<div class="clearfix"></div>
    
	
  	
  </div> <!-- Panel Body -->
</div>


</div>


@include('Admin::layouts.footer')
<script>
var oldPostData = $("#postvalue").val();

function prepare_data()
{
	var userGroup = $('#groupto').val();
	alert($('$chk_all_del').attr('checked'));
	var newPostData = oldPostData + '&userGroup='+userGroup;

	$("#postvalue").val(newPostData);
	var howpage = $("#howpage").val();
	loadData(0,howpage);
}
	function showMe()
	{
	 	urlvalue 	= $("#urlvalue").val();
	 	urlvalue 	= SITEURL+urlvalue;
	 	search_val = $("#search_val").val()
	 	user_type 	= $("#user_type").val()
	 	page = 1;
	 	postvalue 	=  "&type=&search_value="+search_val+"&userType="+user_type;
	 	urisegmnt 	= '';
	 	$("#postvalue").val(postvalue)
		 	$.ajax
			({
				type    : "POST",
				url     :  urlvalue,
				data    : "page="+page+postvalue+"&uri="+urisegmnt+"&orderby=UserId&orderType=Desc&Pnum=10",
				headers :  {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
				success :  function(msg)
				{	
					
					$('#loading').hide();				
					$("#containerdata").html(msg);
					$('#actionform').unbind('submit');				
				}
			});
	}
  
	 function check_action_notification(a,b)
	 {	
	 	var userlist =  $("#user_list").val();
	 	var getVal 	 = userlist.length;
		
		$("#chk_all_del").attr('checked',false);
		
	 	
	 	if(document.getElementById('chk_del_'+a).checked)
	 	
	 	{
	 		$("#add_note").hide();	
				
				if(getVal!=0)
			 	{
			 		valdata = userlist+','+ a;
			 	}
			 	else
			 	{
			 		valdata = a;
			 	}
			 	$("#user_list").val(valdata);

			 
			 	$("#add_li").append("<li id='remove_"+a+"'  class='search-choice glyphicon glyphicon-remove' onclick=remove_li('"+a+"')><span fa fa-remove>"+ b.FirstName+"</span></li>");
			 	
		}
	 	else
	 	{
	 		remove_li(a)
	 	}

	 }
	 function remove_li(removeId)
	 {

	 		$("#remove_"+removeId).remove();
		 	myString 	= $("#user_list").val();
		 	$("#chk_del_"+removeId).attr('checked',false);
		 	var userLen = myString.length;
		 	if(userLen > 24)
		 	{
		 		var userlist =  myString.replace(','+removeId,"");   
		 	}
		 	else
		 	{
		 		var userlist =  myString.replace(removeId,"");   
		 	}
		 	
		 	$("#user_list").val(userlist);
	 	
	 		var userlist =  $("#user_list").val();
	 		var userLen = userlist.length;
	 	if(userLen == 0)
	 	{
	 		$("#add_note").show();	
	 	}
	 }

</script>
@stop


<style>

.chosen-container-multi .chosen-choices li.search-choice {
    background-clip: padding-box;
    background-color: #eeeeee;
    background-image: linear-gradient(#f4f4f4 20%, #f0f0f0 50%, #e8e8e8 52%, #eeeeee 100%);
    background-repeat: repeat-x;
    background-size: 100% 19px;
    border: 1px solid #aaa;
    border-radius: 3px;
    box-shadow: 0 0 2px white inset, 0 1px 0 rgba(0, 0, 0, 0.05);
    color: #333;
    cursor: default;
    line-height: 13px;
    margin: 3px 5px 3px 0;
    max-width: 100%;
    padding: 3px 20px 3px 5px;
    position: relative;

}
.chosen-container-multi .chosen-choices li {
    float: left;
    list-style: outside none none;
}



</style>
