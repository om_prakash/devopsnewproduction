@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')

@section('content')

<style>
.truncate {
  max-width: 100%;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
}
.slider-content .play-store-btn {
  text-align: left;
  display: inline-flex;
  display: -webkit-inline-flex;
  float: left;
}
.slider-content .play-store-btn > div{margin-right: 10px;}
</style>
<div class="container-fluid">

  <!-- Customer Information -->
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;Detail</div>
          <div class="panel-body">
        <div class="col-sm-6">
        <div class="slider-content">
        <h4 ><?php echo ucfirst($images->sliderContant); ?></h4>
        </div>
        </div>
        <div class="col-sm-6">
        @if ($images->image != '')
          <img  width="600px" height="300px" src="<?php echo ImageUrl . $images->image; ?>" tag="Silder image" />
        @else
          <img  src="<?php echo ImageUrl . '/no-image.jpg'; ?>"  />
        @endif
        </div>

    </div>
    </div>
    </div>
    </div>




    <div class="clearfix"></div>
          </div>
          <!-- Panel Body -->
        </div>
      </div>

        <!-- Driver  Information -->

  </div>



@include('Admin::layouts.footer')
@stop
<style type="text/css">
.blue-text{color:rgb(41, 184, 229)}
.list-group-item{border:1px solid #ddd !important; padding: 10px 15px !important;}
}
</style>