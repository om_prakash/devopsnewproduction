{!! Form::model('', ['method' => 'POST', 'url' => ['admin/login']]) !!}

{!! Form::label('Email', 'Email',['class' => 'col-md-2 text-right']) !!}
{!! Form::text('email', '', ['class'=>'form-control', 'placeholder'=> 'Email','id' =>'inputError1']) !!}
{!! Form::label('password', 'Password',['class' => 'col-md-2 text-right']) !!}
{!! Form::text('password', '', ['class'=>'form-control', 'placeholder'=> 'Password','id' =>'inputError1']) !!}

{!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
{!! Form::submit('Cancel', ['class' => 'btn btn-danger']) !!}
{!! Form::close() !!}