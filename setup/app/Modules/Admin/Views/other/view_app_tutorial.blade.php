@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')

@section('content')

<div class="container-fluid">
	<div class="panel panel-default">
		<div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;{{$detail}}
		
		<a class="btn btn-primary margin-top-less-7 pull-right" onclick="window.history.back()">Go Back</a>
		</div>
		<div class="panel-body">
			 <div class="col-md-6">
				 Title : {{$info->Title}}
			</div>
			<br/>
			<div class="col-md-6">
				 Content : <?php echo $info->Content; ?>
			</div>
			<div class="clearfix"></div>
		</div> <!-- Panel Body -->
	</div>

@extends('Admin::category.editModal')
@include('Admin::layouts.footer')

@stop
