@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')

@section('content')

<style>
.truncate {
  max-width: 100%;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
}
</style>
<div class="container-fluid">
	
  <!-- Customer Information -->
  <div class="row">
    <div class="col-md-8">
      <div class="panel panel-default">
        <div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;Answer</div>
          <div class="panel-body">
            
    <h4 class="blue-text">{{ucfirst($result->question)}}</h4>
        <?php  $value = explode(',', $result->related_question) ?>
        <?php echo $result->answer; ?>

    </div>
    </div>
    </div>


      <div class="col-md-4">
      <div class="panel panel-default">
        <div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;Related Question</div>
          <div class="panel-body">
             <ul class="list-group">
          
       @if(count($question) > 0)      
      @foreach($question as $value)
      <li class="list-group-item truncate" style="max-width:500px;"><a href="{{url('admin/faq-answer/'.$value->id)}}">{{ucfirst($value->question)}}</a></li>
      @endforeach
      @else
       <li class="list-group-item truncate" align="center">Question Not Found</li>
      @endif
      
    </ul>
    </div>
    </div>
    </div>
    </div>
    </div>




    <div class="clearfix"></div>
          </div>
          <!-- Panel Body -->
        </div>
      </div>

        <!-- Driver  Information -->
  
  </div>



@include('Admin::layouts.footer')
@stop
<style type="text/css">
.blue-text{color:rgb(41, 184, 229)}
.list-group-item{border:1px solid #ddd !important; padding: 10px 15px !important;}
}
</style>