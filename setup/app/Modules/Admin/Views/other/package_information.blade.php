<div class="modal-body">
{!! Form::model('', ['method' => 'POST', 'url' => ['admin/post_app_tutorial'],'id'=>'formtarget']) !!}
<br />
<div class="form-group"  >
     {!! Form::label('title', 'Amount Charged:',['class' => 'col-md-4 text-right']) !!}
     <div class="col-md-8">
     	{!! Form::label('title', '$'.number_format($info->TotalCost,2),['class' => 'col-md-4']) !!}
	</div>
     <div class="clearfix"></div>
 </div>
 <div id="refund_amountdiv" class="form-group {{ $errors->has('title') ? ' has-error' : '' }}"  >
     {!! Form::label('title', 'Refund Amount:',['class' => 'col-md-4 text-right']) !!}
     <div class="col-md-8">
		{!! Form::text('refund_amount', '', ['class'=>'form-control', 'placeholder'=> 'Refund Amount','id'=>'refund_amount']) !!}
		@if ($errors->has('refund_amount')) <p class="help-block">{{ $errors->first('refund_amount') }}</p> @endif
        <p class="help-block" id="refund_amountError"></p>
	</div>
     <div class="clearfix"></div>
 </div>

<div class="modal-footer">
   
	<button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
    <button type="button" class="btn btn-primary" onclick="make_refund('{{ $info->_id }}')">Refund</button>
 <!--   {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}-->
    {!! Form::close() !!}
</div>
</div>
 
<script>

$(document).ready(function(){
   
        $("#formtarget").submit(function(){
		    var refundAmount =$("#refund_amount").val();
		    
        if(refundAmount =="")
            { 
                 flag = false;
                 $("#refund_amountdiv").addClass("has-error");
                 $("#refund_amountError").html('Refund amount field is required.');
            }
            else
            {
              
              if(Number(refundAmount)<0){
				$("#refund_amountdiv").addClass("has-error");
                $("#refund_amountError").html('Refund amount is only positive number.');

              }
               else{
                    $("#refund_amountError").html('');
                }
            }
            return false;
    });
});

</script>
