@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')
@section('content')

{!! Html::script('theme/admin/fancybox/source/jquery.fancybox.js') !!}
{!! Html::style('theme/admin/fancybox/source/jquery.fancybox.css') !!}
{!! Html::script('theme/admin/custome/js/validation.js')!!}

<div class="container-fluid">
<?php
$total_item_cost = 0;
$total_item = 0 ;
$total_weight = 0;
foreach ($info->ProductList as $key => $value) {
	$total_item = $total_item+1 ;
    $total_item_cost = $total_item_cost + ($value['price'] * $value['qty']);
    $total_weight += GetWeight($value['weight']* $value['qty'],$value['weight_unit']);
}
?>

	<div id="msg_div" class=""></div>
   <div class="panel panel-default">
      <div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;Package Detail
         <a style="margin-right:5px;" class="btn btn-primary margin-top-less-7 pull-right" href="{{url('admin/buy-for-me')}}">Go Back</a>
      </div>
      <div class="panel-body">
         <div class="col-xs-12">
            <h3>Other Detail</h3>
            <div class="row">
               <div class="col-sm-4 custom-table1">
                  <label>Package Id:</label>
                     {{ucfirst($info->PackageNumber)}}
               </div>
               <div class="col-sm-4 custom-table1">
					<label>Distance:</label>
                     @if(!$info->distance == '')
                     		{{number_format($info->distance,2)}} Miles

                     @else
                     	0.00 Miles
                     @endif
               </div>
               <div class="col-sm-4 custom-table1">
                 <label>Status:</label>
	                <?php echo get_status_title($info->Status)['status']; ?>
               </div>
               <div class="col-sm-4 custom-table1">
                  <label>Requester Name:</label>
                  <a data-toggle="modal" href="#myModal" onclick="return get_user_info('{{$info->RequesterId}}')">{{ucfirst($info->RequesterName)}}</a>
                      
                </div>
               <div class="col-sm-4 custom-table1">
				  <label>Receiver Mobile No:</label>
					{{$info->ReceiverCountrycode}}-{{$info->ReceiverMobileNo}}
		       </div>
		       <div class="col-sm-4 custom-table1">
				  <label>Consolidate Shipping: </label>
					@if($info->consolidate_item == 'on') Yes @else No @endif
		       </div>

		       <div class="col-sm-4 custom-table1">
				  <label>Created Date: </label>
				  {{ show_date(@$info->EnterOn) }}
		       </div>

		       <div class="col-sm-4 custom-table1">
				  <label>Item Total: </label>
					<?php echo $total_item;?>
		       </div>

		       <div class="col-sm-4 custom-table1">
				  <label>Device Type: </label>
					{{ ucfirst(@$info->device_type)}}
		       	</div>

		       	<div class="col-sm-4 custom-table1">
				  <label>@if(@$info->device_type == 'website')Browser @else Device Version @endif: </label>
					{{ @$info->device_version}}
		       	</div>

		       	<div class="col-sm-4 custom-table1">
				  <label>@if(@$info->device_type == 'website')
                           Version 
                        @else 
                           App Version 
                        @endif: </label>
					{{ @$info->app_version}}
		       	</div>

		       	<div class="col-sm-4 custom-table1">
				  <label>Total Weight:</label>
					{{ @$total_weight}} lbs
		       	</div>

		       
               <div class="col-sm-12 custom-table1">
				  <label>Drop Address: </label>
				    {{ucfirst($info->DeliveryFullAddress)}}
               </div>

               @if($info->marked_description)
               <div class="col-sm-12 custom-table1">
				 <label>Description</label>(Marked as Paid)<label>:</label>
					{{ucfirst($info->marked_description)}}
               </div>
               @endif
            </div>
         </div>
         <div class="clearfix"></div>
      </div>
      <!-- Panel Body -->
		</div>
<!--panel body product-->

<div class="panel panel-default">
	<div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;Products Detail </div>
		<div class="panel-body">
					@if(count($info->ProductList) > 0)
						@foreach($info->ProductList as $index)

					<div class="panel panel-default panel-body">

							@if($index['tpName'] != '')
							<div class="col-sm-4 custom-table1">
								 <b>Transporter Name</b>:
								 {{ucfirst(@$index['tpName'])}}
							</div>
							@endif
							<div class="col-sm-4 custom-table1">
								 <b>Quantity</b>:
								 {{$index['qty']}} Box(es)
							</div>

							<div class="col-sm-4 custom-table1">
								 <b>Insurance Status</b>: {{ucfirst($index['insurance_status'])}}
							</div>

							<!-- <div class="col-sm-4 custom-table1">
								 <b>Verification</b>:{{@$index['verify_code']}}
							</div>
 -->
		<div class="col-sm-4 custom-table1">
			 <b>Shipping mode</b>:
			 @if(@$index['travelMode'] == 'ship') 
			 	Sea
			 @else
			 	{{ucfirst(@$index['travelMode'])}}
			 @endif

			  
		</div>

		<div class="col-sm-4 custom-table1">
			 <b>Status</b>: {{ get_status_title($index['status'])['status'] }}
		</div>

		<div class="col-sm-4 custom-table1">
						        <label>Shipping Cost</label>:
					               
					               	
					               	${{number_format(@$index['shippingCost'],2)}}
								     <br>
							</div>



							<div class="col-md-12">

									<label>Website:</label>
									&nbsp;<a target="_blank" href="{{url($index['url'])}}">{{ucfirst(@$index['url'])}}</a>
									<div>
										<label>Description:&nbsp;</label>
										&nbsp;{{ucfirst(@$index['description'])}}
									</div>
									<div>
										<label>Package id:&nbsp;</label>
										&nbsp;{{ucfirst(@$index['package_id'])}}
									</div>

							</div>
							<div class="col-md-12">
								<table class="custom-table table table-bordered " >
								<thead>
									<tr >
										<th>Product Image</th>
										<th>Category</th>
										<th >Name</th>
										<th >Weight</th>
										<th >Height</th>
										<th >Width</th>
										<th>Length</th>

										<th>Price</th>
										<th>Verification Code</th>
										<th>Shipping <br /> <small>(Retailer to Aquantuo's facility)</small></th>
									</tr>

								</thead>
								<tbody>
									<tr>
										<td><?php
$fileexists = false;
if (isset($index['image'])  &&  $index['image'] != '') {
    if (file_exists(BASEURL_FILE . $index['image'])) {
        $fileexists = true;
        ?>
												<a class="fancybox" rel="group" href="{{ ImageUrl.$index['image'] }}" >
						                           <img id="senior-preview" src="{{ ImageUrl.$index['image'] }}" class="margin-top" width="120px" height="100px" >
						                         </a><?php
}
}
if ($fileexists == false) {?>
											<img src="{{ImageUrl}}no-image.jpg" class="margin-top" width="120px" height="100px">
										<?php }?>
										</td>
										<td>
											{{ucfirst($index['category'])}}
										</td>
										<td>
											@if(isset($index['product_name']))
												{{ucfirst($index['product_name'])}}
											@endif
										</td>
										<td>
										@if($index['weight'] != '')
											{{ucfirst($index['weight'])}}&nbsp;{{$index['weight_unit']}}
										@else
												N/A
										@endif
										</td>
										<td>
										@if($index['height'] != '')
											{{ucfirst($index['height'])}}&nbsp;{{$index['heightUnit']}}
										@else
												N/A
										@endif
										</td>
										<td>
										@if($index['width'] != '')
											&nbsp;{{$index['width']}}{{$index['widthUnit']}}
										@else
												N/A
										@endif
										</td>
										<td>
										@if($index['length'] != '')
											&nbsp;{{$index['length']}}{{$index['lengthUnit']}}
										@else
												N/A
										@endif
										</td>

										<td>
											${{number_format($index['price'],2)}}

										</td>
										<td>
											{{$index['verify_code']}}
										</td>
										<td>
											${{number_format(floatval(@$index['shipping_cost_by_user']),2)}}
										</td>

									</tr>
										<tr><?php
$value = @$index['shippingCost'] - $index['aq_fee'];
?>



										</tr>
									</tbody>
								</table>
							</div>
					@if(@$index['status'] == 'cancel')
						<h2>Cancel Information</h2>

						<div class="col-sm-4 custom-table1">
							<b>Transporter Message</b>:
							{{ucfirst(@$index['TransporterMessage'])}}
						</div>
						<div class="col-sm-4 custom-table1">
							<b>Reject By</b>:
							{{ucfirst(@$index['RejectBy'])}}
						</div>

						<div class="col-sm-4 custom-table1">
							<b>Reject Time</b>:
							{{date('f m,Y h:i A',$index['RejectTime']->sec)}}
						</div>

						<div class="col-sm-4 custom-table1">
							<b>Return Type</b>:
							{{@$index['ReturnType']}}
						</div>
						@if(!empty(trim($index['TrackingNumber'])))
							<div class="col-sm-4 custom-table1">
								<b>Tracking Number</b>:
								{{@$index['TrackingNumber']}}
							</div>
						@endif

						<div class="col-sm-4 custom-table1">
							<b>Receipt Image</b>:<br />
							<img src="{{ImageUrl.@$index['ReceiptImage']}}" width="80" height="80">
						</div>
					@endif

									@if(!@$index['TransporterFeedbcak'] == '')
									<div class="">
								      <div class="col-sm-6">
								         <div class="panel panel-default">
								            <div class="panel-heading">&nbsp;&nbsp;&nbsp;Transporter Ratting</div>
								            <div class="panel-body">
								               <div class="col-md-12 col-sm-12 custom-table1 row">

								                  	<table><tr>
														<td><label style="font-size:14px;">Ratting:&nbsp;</label></td>
															<td><div class="ratingAdjustmentcss" style="margin-bottom:10px;">
																<div class="graph" style="width:<?php echo $index['TransporterRating'] * 20; ?>%">
																	<img class="rate" src="{{ImageUrl}}/5star.png" >
																</div>
															</div></td>
															</tr></table>
													@if(!@$index['TransporterFeedbcak'] == '')
															<label>Feedback:&nbsp;</label>
															&nbsp;{{ucfirst(@$index['TransporterFeedbcak'])}}
													@endif


								               </div>

								            </div>
								         </div>
								      </div>
								   </div>
								   	@endif
								   	@if(!@$index['RequesterFeedbcak'] == '')
								  	 <div class="">
								      <div class="col-sm-6">
								         <div class="panel panel-default">
								            <div class="panel-heading">&nbsp;&nbsp;&nbsp;Requester Ratting</div>
								            <div class="panel-body">

								               <div class="col-md-12 col-sm-12 custom-table1 row">


								                  <table><tr>
														<td><label style="font-size:14px;">Ratting:&nbsp;</label></td>
															<td><div class="ratingAdjustmentcss" style="margin-bottom:10px;">
																<div class="graph" style="width:<?php echo $index['RequesterRating'] * 20; ?>%">
																	<img class="rate" src="{{ImageUrl}}/5star.png" >
																</div>
															</div></td>
													</tr></table>
													@if(!@$index['RequesterFeedbcak'] == '')
															<label>Feedback:&nbsp;</label>
															&nbsp;{{ucfirst(@$index['RequesterFeedbcak'])}}
													@endif



								               </div>
								            </div>
								         </div>
								      </div>
								   </div>
								  @endif

							<div class="col-sm-6"><br />
							</div>

							<div class="col-sm-6">
								<div class="pull-right" style="margin-top:0px;;">

<?php $btn_status = 'display:none';
if ($index['status'] == 'ready') {$btn_status = 'display:inline;';}?>

<a  type="button" class="btn btn-primary" title="Item Reviewed"   style=" {{$btn_status}}" id="reviewButton{{@$index['_id']}}" onclick="return confirm('Are you sure, you have review this item?')? status_review('{{@$index['_id']}}'):'';" >Review</a>

<?php $btn_status = 'display:none';
if ($index['status'] == 'reviewed') {$btn_status = 'display:inline;';}?>
<a  type="button" class="btn btn-primary" title="Update Item" data-toggle="modal"  style="{{$btn_status}}" id="item_update-{{$index['_id']}}"
	data-target="#edit_item" onclick="item_purchased('{{$index['_id']}}','{{$info['_id']}}',this)" >Update Item</a>

<?php $btn_status = 'display:none';
if ($index['status'] == 'not_purchased') {$btn_status = 'display:inline';}?>

<span id="waiting-for-payment-{{$index['_id']}}" style="{{$btn_status}}" class="btn" >Waiting for a requester to review</span>

<?php $btn_status = 'display:none';
if ($index['status'] == 'paid') {$btn_status = 'display:inline;';}?>
<a  type="button" class="btn btn-primary purchase_button" title="Purchased" style="{{$btn_status}}" id="purchased-{{$index['_id']}}" onclick="return delivery_process('purchased','{{@$index['_id']}}',this,'Are you sure you have purchased this item.');" >Purchased</a>


<?php $btn_status = 'display:none';
if ($index['status'] == 'purchased') {$btn_status = 'display:block';}?>

<a  type="button" class="btn btn-primary purchase_button" title="Item Received" style="{{$btn_status}}" id="item_received-{{$index['_id']}}" onclick="return delivery_process('item_received','{{@$index['_id']}}',this,'Are you sure you have received this item.');" >Item Received</a>


<?php $btn_status = 'display:none';
if ($index['status'] == 'item_received') {$btn_status = 'display:block';}?>
<a  type="button" class="btn btn-primary assign-btn" title="Assign Transporter" data-toggle="modal"  style="{{$btn_status}}" id="assign-{{$index['_id']}}" data-target="#assign_tp" onclick="$('#tp_itemid').val('{{$index['_id']}}')" >Assign Transporter</a>

<?php $btn_status = 'display:none';
if (in_array($index['status'], ['accepted', 'ready', 'not_purchased', 'reviewed', 'paid'])) {
    $btn_status = 'display:inline;';
}?>

<a type="button" data-placement="top" style="{{$btn_status}}" class="btn btn-danger" title="Cancel" onclick="return delivery_process('cancel','{{@$index['_id']}}',this,'Are you sure? You want to cancel this request.');" id="cancel-{{$index['_id']}}" >Cancel</a>

		</div>
	</div>
</div>
<!--cancel-model-->

				@endforeach

				<div class="col-sm-12 custom-table1"><hr>
					<div class="row">

							<?php
$total_aq_fee = 0;
$total_shippingCost = 0;
foreach ($info->ProductList as $key => $value) {
    $total_aq_fee += @$value['aq_fee'];
    $total_shippingCost += @$value['shippingCost'];
}
$value = $total_shippingCost - $total_aq_fee;

?>
                                <div class="col-sm-12">
                                  <div class="col-sm-4">
                   <h3><b>Payment Information</b></h3>
                     <table class="table table-bordered">
  					<tbody bgcolor="#F1F1F1">

	                <tr>
	                   <td class="">
	                      <div class="col-xs-8 text-left">
	                        <label class="" for="exampleInputEmail1"><b>Shipping Cost:</b> </label>
	                      </div>
	                      <div class="col-xs-4 text-right">
	                        ${{number_format($info->shippingCost,2)}}
	                      </div>
	                    </td>
	               </tr>
	               <tr>
	                   <td class="">
	                      <div class="col-xs-8 text-left">
	                        <label class="" for="exampleInputEmail1"><b> Shipping Cost:
								(Retailer to Aquantuo's facility): </b> </label>
	                      </div>
	                      <div class="col-xs-4 text-right">
	                        	(+)${{number_format($info->shipping_cost_by_user,2)}}
	                      </div>
	                    </td>
	               </tr>
			       <tr>
                      <td class="">
                        <div class="col-xs-8 text-left">
                           <label class="" for="exampleInputEmail1"><b>Item(s) Cost:</b> </label>
                        </div>
                          <div class="col-xs-4 text-right">
                            (+)${{number_format($total_item_cost,2)}}
                          </div>
                       </td>
                    </tr>

                  @if($info->insurance > 0)
                  <tr>
                    <td class="">
                      <div class="col-xs-8 text-left">
                        <label class="" for="exampleInputEmail1"><b>Insurance:</b> </label>
                      </div>
                      <div class="col-xs-4 text-right">
                          (+)${{number_format($info->insurance,2)}}
                      </div>
                    </td>
                  </tr>
                  @endif
                  @if($info->discount > 0)
                  <tr>
                    <td class="">
                        <div class="col-xs-8 text-left">
                          <label class="" for="exampleInputEmail1"><b>Aquantuo Discount:</b>
                           </label>
                                @if(!$info->PromoCode == '')
                                 <small>(<b>Promocode:</b>{{$info->PromoCode }}) </small>
                                 @endif
                        </div>
                        <div class="col-xs-4 text-right">
                            (-)${{number_format($info->discount,2)}}
                        </div>
                    </td>
                  </tr>
                  @endif
                   <tr>
                       <td class="">
                              <div class="col-xs-8 text-left">
                                 <label class="" for="exampleInputEmail1">
                                    <b>Processing Fee: <br />
                                    </b>
                                 </label>
                              </div>
                              <div class="col-xs-4 text-right">
                                 @if($info['ProcessingFees'] != '')
                                 (+)${{number_format($info['ProcessingFees'],2)}}
                                 @else
                                 $0.00
                                 @endif
                              </div>
                           </td>
                        </tr>
                    <tr>
					 <td class="">
						<div class="col-xs-8 text-left">
							<label class="" for="exampleInputEmail1"><b>Total Price:</b> </label>
						</div>
							<div class="col-xs-4 text-right">
    							${{ number_format($info->TotalCost,2)}}
							</div>
					    </td>
					</tr>
                   </tbody>
				</table>
               </div>
               <div class="col-sm-4">

               </div>
               <div class="col-sm-4 text-right">
                <h3><b>Fee Details</b></h3>
               <table class="table table-bordered">
  					<tbody bgcolor="#F1F1F1">
               <!--     -->
	                <tr>
	                   <td class="">
	                      <div class="col-xs-8 text-left">
	                        <label class="" for="exampleInputEmail1"><b>Shipping Cost:</b> </label>
	                      </div>
	                      <div class="col-xs-4 text-right">
	                        ${{number_format($total_shippingCost,2)}}
	                      </div>
	                    </td>
	               </tr>


			       <tr>
                      <td class="">
                        <div class="col-xs-8 text-left">
                           <label class="" for="exampleInputEmail1"><b>Aquantuo Fee:</b> </label>
                        </div>
                          <div class="col-xs-4 text-right">
                           (-)${{number_format($total_aq_fee,2)}}
                          </div>
                       </td>
                    </tr>


                  <tr>
                    <td class="">
                      <div class="col-xs-8 text-left">
                        <label class="" for="exampleInputEmail1"><b>Transporter Earning:</b> </label>
                      </div>
                      <div class="col-xs-4 text-right">
                         ${{number_format($value,2)}}
                      </div>
                    </td>
                  </tr>


                   </tbody>
				</table>




				    </div>
				</div>
				<div class="col-sm-12 custom-table1 text-right"><hr>
					@if($info->Status == 'ready')
     				<a style="" class="btn btn-primary" href="{{url('accept-all',[$info->_id])}}"  id="accept_all_button" onclick="return confirm('Are you sure, you reviewed all items?')">Review All</a>
     				@endif
     				<?php $btn_status = 'display:none';
				if ($info->Status == 'not_purchased') {$btn_status = 'display:inline-block';}?>
					<span id="reminder_button" style="{{$btn_status}}"  >
     					<a class="btn btn-primary" title="Send payment reminder"
						onclick="delivery_process('payment_reminder','{{@$info->_id}}',this,'Are you sure, you want to send reminder.');" >Reminder for Payment</a>&nbsp;&nbsp;
						
					</span>

					@if($info->Status == 'not_purchased')
				<a data-toggle="modal" data-target="#paid_mark"  title="Mark as Paid" class="btn btn-primary"><i class="fa fa-check-square-o"></i>&nbsp;Mark as Paid
					</a>
					@endif

				</div>

			@endif

			<div class="clearfix"></div>

	</div>

	</div>

<!--end-->


   <?php if (@$info->Penalty > 0): ?>
   <div class="row">
      <div class="col-sm-12">
         <div class="panel panel-default">
            <div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;Penalty</div>
            <div class="panel-body">
               <div class="col-md-6 col-sm-6 custom-table1 row">
                  <div class="row">
                     <div class="col-md-6 col-sm-6"><b>Penalty Status</b> :</div>
                     <div class="col-md-4 col-sm-4"> {{ucfirst($info->PenaltyStatus)}}</div>
                  </div>
               </div>
               <div class="col-md-6 col-sm-6 custom-table1 row">
                  <div class="row">
                     <div class="col-md-6 col-sm-6 text-right"><b>Penalty Status</b> :</div>
                     <div class="col-md-4 col-sm-4 text-right"> ${{number_format(($info->Penalty/100),2)}}</div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <?php endif;?>
   <div class="row">


      <?php if ($info['ProductImage'] != "") {
    ?>
      <div class="col-sm-12">
         <div class="panel panel-default">
            <div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;Package Image</div>
            <div class="panel-body">
               <?php

    if ($info['ProductImage'] != '') {
        if (file_exists(BASEURL_FILE . $info['ProductImage'])) {?>
               <div class="col-md-2">
                  <a class="fancybox" href="{{ImageUrl}}{{ $info->ProductImage}}" data-fancybox-group="gallery" title="Product Image">
                  <img src="<?php echo ImageUrl . $info['ProductImage']; ?>" tag="product image" style="width:178px; height:178px;"/></a>
               </div>
               <?php }
    }
    if (is_array($info["OtherImage"])) {

        foreach ($info['OtherImage'] as $key) {
            if ($key != '') {
                if (file_exists(BASEURL_FILE . $key)) {?>
               <div class="col-md-2">
                  <a class="fancybox" href="{{ImageUrl}}{{$key}}" data-fancybox-group="gallery" title="Product Image">
                  <img src="<?php echo ImageUrl . $key; ?>" tag="product image" style="width:178px; height:178px;"/></a>
               </div>
               <?php }
            }
        }

    }?>
               <div class="clearfix"></div>
            </div>
            <!-- Panel Body -->
         </div>
      </div>
      <?php }?>
   </div>

   <!--  Modal content for the mixer image example -->
   <div class="modal fade pop-up-map" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel-1" aria-hidden="true">
      <div class="modal-dialog modal-lg">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
               <div id="map1"></div>
               <p id="error"></p>
            </div>
         </div>
         <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
   </div>
   <!-- /.modal mixer image -->
</div>


<!-- Modal -->
<div id="edit_item" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" id="edit_item_close">&times;</button>
        <h4 class="modal-title">Edit Item</h4>
      </div>
      <form id="item_purchased_form">
      	<div class="modal-body">
	        <div class="row">
	        	<div class="col-md-12">
	        		<div class="form-group">
					    <label for="item_name">Item Name:</label>
					    <input type="text" class="form-control" id="item_name" name="item_name">
					    <div class="red" id="er_item_name"></div>
					</div>
	        	</div>
	        </div>
	        <div class="row">
	        	<div class="col-md-6">
	        		<div class="form-group">
					    <label for="item_name">Purchased From:</label>
					    <!-- <select class="form-control" id="purchase_from" name="purchase_from">
					    	<option value="">Select Market</option>
					    	<option value='Amazon'>Amazon</option>
					    	<option value='eBay'>eBay</option>
					    	<option value='Walmart'>Walmart</option>
					    	<option value='FlipKart'>FlipKart</option>
					    	<option value='Snapdeal'>Snapdeal</option>


					    </select> -->
					    <input type="text" class="form-control" id="purchase_from" name="purchase_from" placeholder="Purchased From">
					    <div class="red" id="er_purchase_from"></div>
					</div>
	        	</div>

	        	<div class="col-md-6">
	        		<div class="form-group">
					    <label for="item_name">Shipping:
					    	<small>(Retailer to Aquantuo's facility)</small></label>
					    <input type="text" class="form-control" id="shipping_cost_by_user" name="shipping_cost_by_user" placeholder="Shipping">
					    <div class="red" id="er_shipping_cost_by_user"></div>
					</div>
	        	</div>
	        	<div class="col-md-6">
	        		<div class="form-group">
					    <label for="item_name">Item Price:</label>
					    <input type="text" class="form-control" id="item_price" name="item_price">
					    <div class="red" id="er_item_price"></div>
					</div>
	        	</div>
	        </div>
	        <div class="row">
	        	<div class="col-md-6">
	        		<h4>Measurement Units</h4>
	        		<div class="clearfix"></div>
	        		<input type="radio" name="measurement_units" id="measurement_units_cm_kg" value="cm_kg" checked>&nbsp;Metric(cm/kg)&nbsp;&nbsp;
	        		<input type="radio" name="measurement_units" id="measurement_units_inches_lbs" value="inches_lbs">&nbsp;Imperial(inches/lbs)
	        	</div>

	        	<div class="col-md-6">
	        		<h4>Quantity</h4>
	        		<input type="text" class="form-control" id="qty1" name="qty1">
	        		
	        		<div class="red" id="er_qty1"></div>
	        	</div>

	        	<div class="col-md-12"><h4>Item Specification</h4></div>
	        	<div class="col-md-6">
	        		<div class="clearfix"></div>
	        		<div class="form-group">
					    <label for="item_name">Length:</label>
					    <input type="text" class="form-control" id="item_length" name="item_length">
					    <div class="red" id="er_item_length"></div>
					</div>
	        	</div>
	        	<div class="col-md-6">
	        		<div class="form-group">
					    <label for="item_name">Width:</label>
					    <input type="text" class="form-control" id="item_width" name="item_width">
					    <div class="red" id="er_item_width"></div>
					</div>
	        	</div>
	        </div>
	        <div class="row">
	        	<div class="col-md-6">
	        		<div class="form-group">
					    <label for="item_name">Height:</label>
					    <input type="text" class="form-control" id="item_height" name="item_height">
					    <div class="red" id="er_item_height"></div>
					</div>
	        	</div>
	        	<div class="col-md-6">
	        		<div class="form-group">
					    <label for="item_name">Weight:</label>
					    <input type="text" class="form-control" id="item_weight" name="item_weight">
					    <div class="red" id="er_item_weight"></div>
					</div>
	        	</div>
	        </div>
	    	<input type="hidden" name="itemid" id="itemid" >
	    	<input type="hidden" name="requestid" id="requestid" >
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" id="item_sub_button">Submit</button>
      </div>
      </form>
    </div>

  </div>
</div>




<!-- Modal for assign transporter -->
<div id="assign_tp" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" id="assign_tp_close">&times;</button>
        <h4 class="modal-title">Assign Transporter</h4>
      </div>
      <form id="assing_tp_form">
      	<div class="modal-body">
	        <div class="row">
	        	<div class="col-md-12">
	        		<div class="form-group">
					    <label for="item_name">Transporter:</label>
					    <select class="form-control" id="tp_name" name="tp_name">
					    	<option value="">Select Transporter</option>
					    	@foreach($tplist as $key)
					    		<option value='{"id":"{{$key['_id']}}","name":"{{$key['Name']}}"}' >{{$key['Name']}}</option>
					    	@endforeach
					    </select>
					    <div class="red" id="er_tp_name"></div>
					</div>
	        	</div>
	        </div>

	    	<input type="hidden" name="product_id" id="tp_itemid" >
	    	<input type="hidden" name="request_id" id="tp_requestid" value="{{Request::segment(4)}}">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" id="assing_tp_btn" >Submit</button>
      </div>
      </form>
    </div>

  </div>
</div>




<!--requester detail-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="close_modal"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">User Detail</h4>
      </div>

      <div class="modal-body my-modal-body">

         
        <div id="profile_image">
         <p>Profile Image</p>
        </div>
        <script type="text/javascript">
         $(document).ready(function() {
            $(".fancybox").fancybox();
         })
      </script>



         <div class="table-responsive">
            <table class="table table-hover table-striped my-table">
               <tr>
                  <td>Name:</td>
                    <td><p id="name"></p></td>
                </tr>
               <tr>
                  <td>Email:</td>
                    <td><p id="email"></p></td>
                </tr>
                <tr>
                  <td>Phone Number:</td>
                    <td><p id="phone"></p></td>
                </tr>
                <tr>
                  <td>Alternate Phone Number:</td>
                    <td> <p id="phone2"></p></td>
                </tr>
                <tr>
                  <td>Address line-1:</td>
                    <td><p id="street1"></p></td>
                </tr>
                <tr>
                  <td>Address line-2:</td>
                    <td> <p id="street2"></p></td>
                </tr>
                <tr>
                  <td>Country:</td>
                    <td><p id="detail_country"></p></td>
                </tr>
                <tr>
                  <td>State:</td>
                    <td><p id="detail_state"></p></td>
                </tr>
                <tr>
                  <td>City:</td>
                    <td> <p id="detail_city"></p></td>
                </tr>
                 <tr>
                  <td>Zip Code:</td>
                    <td><p id="detail_zipcode"></p></td>
                </tr>
                 
            </table>
      </div>
      <div id="no_user" style="display:none;"><center>User not found.</center></div>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
<!-- end -->



<!-- mark paid after item update -->
<div class="modal fade" id="paid_mark" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title" id="exampleModalLabel">Mark as Paid</h4>
	  </div>

	  {!! Form::model('', ['name' => 'paid_payment2', 'id' => 'paid_payment2', 'method' => 'POST', 'url' => ['admin/mark_paid_after_item_update']]) !!}
	  <input type="hidden" name="requestid" value="{{$info->_id}}" id="requestid">
	  <input type="hidden" name="request_type" value="buy_for_me" id="request_type">

	  <div class="modal-body">
			<div class="form-group" id="">
			<label>Description/Reason</label>
				<textarea class="form-control" name="description" placeholder="Description/Reason"></textarea>
				<p class="help-block" id="description" style="display: none;"></p>
			</div>

	  </div>
	  <div class="modal-footer">
		{!! Form::button('Close', ['class' => 'btn btn-primary', 'data-dismiss'=>'modal']) !!}
		{!! Form::submit('Submit', ['class' => 'btn btn-primary','onclick' => "return confirm('Are you sure? You want to mark as paid to this request!');"]) !!}
	  </div>
		<div class="clearfix"></div>
		{!! Form::close() !!}

	</div>
  </div>
</div>
<!-- end mark paid -->


<?php
function get_feet($InFeet, $unit)
{
    $InFeet = (double) $InFeet;
    $unit = strtolower($unit);
    if ($unit == 'inch') {
        $InFeet = $InFeet * 0.0833333;
    } else if ($unit == 'meter') {
        $InFeet = $InFeet * 3.28084;
    } else if ($unit == 'centimeter' || $unit == 'centimetre') {
        $InFeet = $InFeet * 0.0328084;
    }
    return $InFeet;
}
?>
<script type="text/javascript">
   $(document).ready(function() {
   	$(".fancybox").fancybox();
   });

function get_user_info(id)
{
   var url = SITEURL+'ajax/get_user_info/'+id;
   $('#loading').show();
   $.ajax({
      type:"GET",
      url:url,
      //headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      success: function(res)
      {  $('#loading').hide();
         if(res.success == 0){
            
            

            $(".table-responsive").hide();
            $("#profile_image").html('');
            $("#no_user").show();
            
         }else{
            $(".table-responsive").show();
            $("#no_user").hide();
            $("#name").html(res.result.Name);
            $("#email").html(res.result.Email);
            $("#phone").html(res.result.PhoneNo);
            $("#phone2").html(res.result.AlternatePhoneNo);
            $("#street1").html(res.result.Street1);
            $("#street2").html(res.result.Street2);
            $("#detail_country").html(res.result.Country);
            $("#detail_state").html(res.result.State);
            $("#detail_city").html(res.result.City);
            $("#detail_zipcode").html(res.result.ZipCode);
            if(res.result.Image == ''){
               $("#profile_image").html('<img class="thumbnail" src="{{ImageUrl}}/profile/noImageAvailable.jpg" height="100px" width="100px" alt="Profile image">');
            }else{
               $("#profile_image").html('<img class="thumbnail" src="{{ImageUrl}}/'+res.result.Image+'" height="100px" width="100px" alt="Profile image">');
            }
            
            //$("#detail_reg_date").html('{{ show_date('+res.result.EnterOn+') }}');
            //$('#detail_modal').trigger('click');
            
            
         }
         
         //$("#refund-data").html(msg);
      }
   });
}


var requestid = '{{Request::segment(4)}}';
	function delivery_process(type,itemid,obj,msg)
	{
		if(confirm(msg) == true)
		{
			$(obj).addClass('spinning');

			$.ajax({
				url     : '{{url("admin/buy-for-me-process")}}',
				type    : 'post',
				data    : {type:type,itemid:itemid,requestid:requestid},
				dataType: 'json',
				success : function(res)
				{
					$(obj).removeClass('spinning');
					$("#accept_all_button").hide();
					alert(res.msg);
					if(res.success == 1)
					{
						location.reload();
					}
				}
			});
		}
		return false;
	}

function item_purchased(itemid,requestid,obj)
{
	$('#itemid').val(itemid);
	$('#requestid').val(requestid);
	$(obj).addClass('spinning');
	$.ajax({
		url     : '{{url("admin/get_item_info")}}',
		type    : 'post',
		data    : {itemid:itemid,requestid:requestid},
		dataType: 'json',
		success : function(res)
		{
			$(obj).removeClass('spinning');
			if(res.success == 1)
			{
				$('#item_name').val(res.ProductList.product_name);
				if(res.ProductList.marketname != '') {
					$('#purchase_from').val(res.ProductList.marketname);
				}
				$('#shipping_cost_by_user').val(res.ProductList.shipping_cost_by_user);
				$('#item_price').val(res.ProductList.price);
				$('#item_length').val(res.ProductList.length);
				$('#item_width').val(res.ProductList.width);
				$('#item_height').val(res.ProductList.height);
				$('#item_weight').val(res.ProductList.weight);
				$('#qty1').val(res.ProductList.qty);
				if(res.ProductList.heightUnit == 'cm') {
					$('#measurement_units_cm_kg').attr('checked','checked');
				} else {
					$('#measurement_units_inches_lbs').attr('checked','checked');
				}
			}
			else
			{
				alert(res.msg);
			}
		}
	});
}
$('#item_purchased_form').submit(function()
{
	var flag = true;

	if(valid.required('item_name','item name') == false) { flag = false; }
	if(valid.required('item_price','item price') == false) { flag = false; } else {
		if(valid.float('item_price','item price') == false) { flag = false; }
	}
	if(valid.required('item_length','item length') == false) { flag = false; } else {
		if(valid.float('item_length','item length') == false) { flag = false; }
	}
	if(valid.required('item_width','item width') == false) { flag = false; } else {
		if(valid.float('item_width','item width') == false) { flag = false; }
	}
	if(valid.required('item_height','item height') == false) { flag = false; } else {
		if(valid.float('item_height','item height') == false) { flag = false; }
	}
	if(valid.required('item_weight','item weight') == false) { flag = false; } else {
		if(valid.float('item_weight','item weight') == false) { flag = false; }
	}
	if(valid.required('shipping_cost_by_user','shipping') == false) { flag = false; } else {
		if(valid.float('shipping_cost_by_user','shipping') == false) { flag = false; }
	}

	if(valid.required('qty1','quantity') == false) { flag = false; }else {
		if(valid.numeric('qty1','quantity') == false) { flag = false; }
	}

	if(flag)
	{

		$('#item_sub_button').addClass('spinning');

		$.ajax({
		url     : '{{url("admin/update_item_info")}}',
		type    : 'post',
		data    : $('#item_purchased_form').serialize(),
		dataType: 'json',
		success : function(res)
		{
			console.log(res);
			$('#item_sub_button').removeClass('spinning');
			if(res.success == 1)
			{
				$('#item_purchased_form')[0].reset();
				$('#edit_item_close').trigger('click');
				$('#item_update-'+$('#itemid').val()).hide();
				if(res.reminder_button == true) {
					$('#reminder_button').show();
				}
				if(res.purchase_button == true) {
					$('.purchase_button').show();
				} else {
					$('#waiting-for-payment-'+$('#itemid').val()).show();
				}
				alert(res.msg);
				location.reload();
				/*if(res.assing_tp_button == true) {
					$('.assign-btn').show();
				}*/

			}
			alert(res.msg);
		}
	});
	}
	return false;
});

$('#assing_tp_form').submit(function(){

	var flag = true;

	if(valid.required('tp_name','transprter') == false) { flag = false; }

	if(flag)
	{

		$('#assing_tp_btn').addClass('spinning');

		$.ajax({
		url     : '{{url("admin/buy_for_me_assign_transporter")}}',
		type    : 'post',
		data    : $('#assing_tp_form').serialize(),
		dataType: 'json',
		success : function(res)
		{
			$('#assing_tp_btn').removeClass('spinning');
			if(res.success == 1)
			{
				$('#assing_tp_form')[0].reset();
				$('#assign_tp_close').trigger('click');

				$('#assign-'+$('#tp_itemid').val()).hide();
				alert(res.msg);
				location.reload();
			}
			alert(res.msg);
		}
	});
	}
	return false;

});


function status_review(prid)
{
	$("#reviewButton"+prid).addClass('spinning');
	$.ajax({
		url     : '{{url("admin/online-package-review")}}',
		type    : 'post',
		data    : "request_id={{Request::segment(4)}}"+"&product_id="+prid,
		success : function(res){

			$("#reviewButton"+prid).removeClass('spinning');
			var obj = eval("("+res+")");
			if(obj.success == 1)
			{
				$("#item_update-"+prid).show();
				$("#reviewButton"+prid).hide();
				$("#updated-"+prid).show();
				alert(obj.msg);
				location.reload();
			}
			alert(obj.msg);
		}
	});
}
</script>
@include('Admin::layouts.footer')

@stop
<style>
   .modal-header .close {
   margin-top: -9px !important;
   }

   .custom-table.table th {
    border-bottom: medium none !important;
    vertical-align: middle;
    height: 60px!important;
    background: #F1F1F1;
    /* text-align:center;*/
}
.custom-table.table > tbody td {
    /*text-align:center !important;*/

    vertical-align: middle;
}

.table-bordered td,.table-bordered th{border:1px solid #ddd!important}

.ratingAdjustmentcss {
background-color: #8d887a;
float: left;
margin-top: 3px !important;
width: 65px;
}

.graph {
background-color: #ecb90d;
height: 14px;
}

</style>

