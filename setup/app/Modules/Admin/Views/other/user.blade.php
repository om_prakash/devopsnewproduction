@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')

@section('content')
 {!! Html::script('theme/admin/fancybox/source/jquery.fancybox.js') !!}
 {!! Html::style('theme/admin/fancybox/source/jquery.fancybox.css') !!}
<div class="container-fluid">
	<div class="panel panel-default">
		<div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;User Detail
		<a class="btn btn-primary margin-top-less-7 pull-right" style="margin-top: -5px;" onclick="window.history.back()">Go Back</a>
		</div>
		<div class="panel-body">
			<div class="col-md-12 div-table-view">
			<div class="row">
				<div class="col-md-6">
					<p>Profile Image</p>
						<?php if ($info->Image == "") {?>
						<img class="thumbnail" src="{{ImageUrl}}/profile/noImageAvailable.jpg" height="100px" width="100px" alt="Profile image">
						<?php } else {?>
							<a class="fancybox" href="{{ImageUrl}}{{ $info->Image}}" data-fancybox-group="gallery" title="{{$info->Name}} ">
								<img class="thumbnail" src="{{ImageUrl}}{{ $info->Image }}" height="100px" width="100px" alt="Profile image">
							</a>
						<?php }?>
						<!--  Modal content for the mixer image example -->
								  <div class="modal fade pop-up-1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel-1" aria-hidden="true">
								    <div class="modal-dialog modal-lg">
								      <div class="modal-content">

								        <div class="modal-header">
								          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
								        </div>
								        <div class="modal-body">
								        <img src="{{ImageUrl}}{{ $info->Image }}" class="img-responsive">
								        </div>
								      </div><!-- /.modal-content -->
								    </div><!-- /.modal-dialog -->
								  </div><!-- /.modal mixer image -->
												</div>
				<div class="col-md-6">
					<div class="row">
						<div class="col-md-6">
							<p>Licence Image</p>
							<?php if ($info->LicenceId == "") {?>
								<img class="thumbnail" src="{{ImageUrl}}/profile/noImageAvailable.jpg" height="100px" width="100px" alt="Licence image">
							<?php } else {?>
								<a class="fancybox" href="{{ImageUrl}}{{ $info->LicenceId}}" data-fancybox-group="gallery" title="Licence">
	                            <img class="thumbnail" src="{{ImageUrl}}{{$info->LicenceId }}" height="100px" width="100px" alt="Licence image"></a>
							<?php }?>
								 <!--  Modal content for the lion image example -->
							  <div class="modal fade pop-up-2" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel-2" aria-hidden="true">
							    <div class="modal-dialog modal-lg">
							      <div class="modal-content">

							        <div class="modal-header">
							          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

							        </div>
							        <div class="modal-body">
							        <img src="{{ImageUrl}}{{ $info->LicenceId }}" class="img-responsive">
							        </div>
							      </div><!-- /.modal-content -->
							    </div><!-- /.modal-dialog -->
							  </div><!-- /.modal mixer image -->
						</div>
						<div class="col-md-6">
							<?php if ($info->TransporterType == 'business') {?>
							<p>Business ID</p>
							<?php if (@$info->IDProof == "") {?>
								<img class="thumbnail" src="{{ImageUrl}}/profile/noImageAvailable.jpg" height="100px" width="100px" alt="Licence image">
							<?php } else {?>
								<a class="fancybox" href="{{ImageUrl}}{{ $info->IDProof}}" data-fancybox-group="gallery" >
	                            <img class="thumbnail" src="{{ImageUrl}}{{ $info->IDProof }}" height="100px" width="100px" alt="Licence image"></a>
	                        <?php }?>
	                        <?php }?>

						</div>
					</div>
				</div>
			</div>
			</div>

			<div class="col-md-6 div-table-view">

			  	Name : {{$info->Name}}
			</div>
			<div class="col-md-6 div-table-view">
			  	Transporter Type : <?php echo ucfirst($info->TransporterType); ?>
			</div>
			<div class="col-md-6 div-table-view">
				 Email : {{$info->Email}}
			</div>
			<div class="col-md-6 div-table-view">
				 Phone Number : @if(@$lists->BillingInfo['CountryPhoneCode'] != ''){
						{{@$lists->BillingInfo['CountryPhoneCode']}}-
						@endif
						{{$info->PhoneNo}}
			</div>
			<div class="col-md-6 div-table-view">
				 Alternate Phone No: <?php echo $info->AlternatePhoneNo; ?>
			</div>
			<div class="col-md-6 div-table-view dropdown">
				 Address : {{$info->Street1}} {{$info->Street2}}
			</div>
			<div class="col-md-6 div-table-view">
				 Country : {{@$info->Country}}
			</div>
			<div class="col-md-6 div-table-view">
				 State : {{$info->State}}
			</div>
			<div class="col-md-6 div-table-view">
				 City : {{$info->City}}
			</div>
			<div class="col-md-6 div-table-view">
				 Zip : @if($info->ZipCode != '') {{$info->ZipCode}} @else N/A @endif
			</div>
			<div class="col-md-6 div-table-view">
				<?php if (isset($info->SSN)): ?>
					SSN : <?php echo ($info->SSN != '') ? 'XXXXX-' . substr($info->SSN, -4) : ''; ?>
				<?php endif;?>
			</div>

			<div class="col-md-6 div-table-view">
				 Registration Date : <?php
				if (isset($info->EnterOn->sec)) {
				    echo show_date($info->EnterOn);
				}
				?>
			</div>


			<div class="col-md-6 div-table-view">

				<?php

					if(isset($info->type_of_id) && $info->type_of_id != ''){
				?>
						Type of Id above : {{$info->type_of_id}}
				<?php
					}elseif (isset($info->id_type) && $info->id_type != '') {
				?>
						Type of Id above : {{$info->id_type}}
				<?php
				}
				?>
				
			</div>

			<div class="clearfix"></div>
		</div> <!-- Panel Body -->
	</div>
	@if($info->ProfileStatus == 'complete')
	<div class="panel panel-default">
		<div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;Account Information</div>
		<div class="panel-body">
			 <div class="col-md-6 div-table-view">
				 Bank Name : {{$info->BankName}}
			</div>
			<div class="col-md-6 div-table-view">
				 Account Holder Name : {{$info->AccountHolderName}}
			</div>

			<div class="col-md-6 div-table-view">
				 Account Number: {{$info->BankAccountNo}}
			</div>
			<div class="col-md-6 div-table-view">
				 Routing Number: {{$info->RoutingNo}}
			</div>
			<div class="col-md-6 div-table-view">
				 Paypal ID: {{$info->PaypalID}}
			</div>
			<div class="clearfix"></div>
		</div> <!-- Panel Body -->
	</div>
	@endif

</div>
<script type="text/javascript">
	$(document).ready(function() {
		$(".fancybox").fancybox();
	})
</script>

@include('Admin::layouts.footer')
@stop
<style>
	.modal-header .close {
    margin-top: -9px !important;
}
 .dropdown {
    word-break: break-all;
}


</style>
