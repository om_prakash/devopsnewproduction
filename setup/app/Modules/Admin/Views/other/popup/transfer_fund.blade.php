<?php if(count($info) > 0){ ?>
<div class="modal-body" >
	<div id="fund-error"></div>
	<div class="form-group">
		<label class="control-label col-sm-3" for="email">User Name:</label>
	  	<div class="col-sm-8">
	    	<span ><?php echo $info->Name; ?></span>
	  	</div>
	</div>
	<div class="form-group">
		<label class="control-label col-sm-3" for="email">Amount:</label>
	  	<div class="col-sm-8" style="color:#a94442">
	    	<input type="text" class="form-control" name="transfer_amount" placeholder="amount" id="transfer_amount">
	    	<span id="er_transfer_amount" class="error" ></span>
	  	</div>
	</div>
	<input type="hidden" value="<?php echo $info->id; ?>" id="transfer_to" name="transfer_to">
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
	<button type="submit" class="btn btn-primary">Transfer</button>
</div>
<?php }else{ ?>
<div class="modal-body" >
	<div class="row">
		<div class="col-md-12 text-center">No Record Found</div>
	</div>
</div>
<?php } ?>
