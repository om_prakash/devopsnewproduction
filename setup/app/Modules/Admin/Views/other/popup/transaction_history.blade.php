
<div class="table-responsive">
     <div class="col-md-6 div-table-view"><b>User Name</b> : <i><strong>{{$user->Name }}</strong></i></div>
	      		<div class="col-md-6 div-table-view"><b>Email</b> : <i><strong>{{ $user->Email }}</strong></i></div>
	      		<input type="hidden" name="driverEmail" value="{{ $user->Email }}">
	 			<input type="hidden" id="deliveryBoyId" name="deliveryBoyId" value="<?php echo $user['_id']; ?>">

            @if(count($user) > 0)
             <br/>
             <div class="col-sm-12"><h4>Send A Package</h4></div>
	      	<div class="col-md-13 div-table-view">
	      	<table  class="table table-hover table-striped my-table">
	      		<tr>
	      			<th></th>
	      			<th>Title</th>
	      			<th style="text-align: right;">Bid Amount</th>
	      			 <th style="text-align: right;">Transporter Income</th>
	      		    <th style="text-align: right;">Admin Income</th>
	      		</tr>
                   <?php $Amount = $userIncome = $adminIncome = 0;?>
	      		@foreach($info as $order )
	      			<?php
$systemcharge = $order->AquantuoFees;
$Amount = $Amount + $order->TotalCost;
$userIncome = $userIncome + $order->TotalCost - $systemcharge;
$adminIncome = $adminIncome + $systemcharge;
?>
	      		<tr>

	      			<td><input type="checkbox" name="addTotal[]" id="addTotal" value="<?php echo (String) $order->_id; ?>" onclick="add_total('<?php echo (String) $order->_id; ?>')" class="addTotal" checked></td>
	      			<td>{{ $order->product_name }}</td>
	      			<td style="text-align: right;">${{number_format($order->TotalCost,2)}}</td>
	      			 <td style="text-align: right;">${{number_format(($order->TotalCost - $order->AquantuoFees),2)}}</td>

	      			<input type="hidden" name="driverValue_<?php echo (String) $order['_id']; ?>" id="driverValue_<?php echo (String) $order['_id']; ?>" value="{{($order->TotalCost - $order->AquantuoFees)}}">
	      			 <td style="text-align: right;">${{number_format($systemcharge,2)}}</td>
	      		</tr>
	      		@endforeach

	      		<tr style="text-align: right;">
	      			<td></td>
	      			<td><b>Total</b></td>
	      			<td ><b><?php echo '$' . number_format($Amount, 2); ?></b></td>
	      			 <td><b><?php echo '$' . number_format($userIncome, 2); ?></b></td>
	      			 <td><b><?php echo '$' . number_format($adminIncome, 2); ?></b></td>
	      		</tr>
	      	</table>
	      	</div>
	      	  <br/>
                <div class="col-sm-12"><h4>Online Purchase</h4></div>
               <div class="col-md-13 div-table-view">
	      	<table  class="table table-hover table-striped my-table">
	      		<tr>
	      			<th></th>
	      			<th>Title</th>
	      			<th style="text-align: right;">Bid Amount</th>
	      			 <th style="text-align: right;">Transporter Income</th>
	      		    <th style="text-align: right;">Admin Income</th>
	      		</tr>
                   <?php $Amount = $userIncome = $adminIncome = 0;?>
                @if(count($info_2) > 0)
	      		@foreach($info_2->ProductList as $order)
	      			<?php
$systemcharge = $order['aq_fee'];
$Amount = $Amount + $order['shippingCost'];
$userIncome = $order['shippingCost'] - $order['aq_fee'];
$adminIncome = $adminIncome + $systemcharge;
?>
	      		<tr>

	      			<td><input type="checkbox" name="addTotal[]" id="addTotal" value="<?php echo (String) $order['_id']; ?>" onclick="add_total('<?php echo (String) $order['_id']; ?>')" class="addTotal" checked></td>
	      			<td>{{ $order['product_name'] }}</td>
	      			<td style="text-align: right;">${{number_format($order['shippingCost'],2)}}</td>
	      			 <td style="text-align: right;">${{number_format(($order['shippingCost'] - $order['aq_fee']),2)}}</td>

	      			<input type="hidden" name="driverValue_<?php echo (String) $order['_id']; ?>" id="driverValue_<?php echo (String) $order['_id']; ?>" value="{{($order['shippingCost'] - $order['aq_fee'])}}">
	      			 <td style="text-align: right;">${{number_format($order['aq_fee'],2)}}</td>
	      		</tr>
	      		@endforeach
                @endif
	      		<tr style="text-align: right;">
	      		<td></td>
	      		<td><b>Total</b></td>
	      		<td ><b><?php echo '$' . number_format($Amount, 2); ?></b></td>
	      		<td><b><?php echo '$' . number_format($userIncome, 2); ?></b></td>
	      		<td><b><?php echo '$' . number_format($adminIncome, 2); ?></b></td>
	      		</tr>
	      	</table>
	      	<br/>

                <div class="col-sm-12"><h4>Buy For Me</h4></div>
               <div class="col-md-13 div-table-view">
	      	<table  class="table table-hover table-striped my-table">
	      		<tr>
	      			<th></th>
	      			<th>Title</th>
	      			<th style="text-align: right;">Bid Amount</th>
	      			 <th style="text-align: right;">Transporter Income</th>
	      		    <th style="text-align: right;">Admin Income</th>
	      		</tr>
                   <?php $Amount = $userIncome = $adminIncome = 0;?>
                @if(count($info_3) > 0)
	      		@foreach($info_3->ProductList as $order)
	      			<?php
$systemcharge = $order['aq_fee']; // admin income
$Amount = $Amount + $order['shippingCost'];
$userIncome = $order['shippingCost'] - $order['aq_fee'];
$adminIncome = $adminIncome + $systemcharge;
?>
	      		<tr>
	      			<td>

	      				<input type="checkbox" name="addTotal[]" id="addTotal" value="<?php echo (String) $order['_id']; ?>" onclick="add_total('<?php echo (String) $order['_id']; ?>')" class="addTotal" checked>
	      			</td>
	      			<td>{{ $order['product_name'] }}</td>
	      			<td style="text-align: right;">${{number_format($order['shippingCost'],2)}}</td>
	      			 <td style="text-align: right;">${{number_format(($order['shippingCost'] - $order['aq_fee']),2)}}</td>

	      			<input type="hidden" name="driverValue_<?php echo (String) $order['_id']; ?>" id="driverValue_<?php echo (String) $order['_id']; ?>" value="{{($order['shippingCost'] - $order['aq_fee'])}}">
	      			 <td style="text-align: right;">${{number_format($order['aq_fee'],2)}}</td>
	      		</tr>
	      		@endforeach
                @endif
	      		<tr style="text-align: right;">
	      		<td></td>
	      		<td><b>Total</b></td>
	      		<td ><b><?php echo '$' . number_format($Amount, 2); ?></b></td>
	      		<td><b><?php echo '$' . number_format($userIncome, 2); ?></b></td>
	      		<td><b><?php echo '$' . number_format($adminIncome, 2); ?></b></td>
	      		</tr>
	      	</table>
	      	</div>
             @else
         	<div class="col-md-13 div-table-view"><span style="margin-top: 50px; margin-left: 200px;font-size: 20">Transporter Not found</span></div>
             @endif
	      </div>