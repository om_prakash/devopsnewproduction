@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')

@section('content')

<div class="container-fluid">
<div class="panel panel-default">
  <div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;<b>Cancel Delivery Request</b>
  </div>
  <div class="panel-body">		
		{!! Form::model('', ['method' => 'POST', 'url' => ['admin/cancel_package',$info->_id], 'class'=>"form-horizontal",'files'=>true,'onsubmit' => 'return validate_cancel_package()', 'id' => 'validate_cancel_package_form' ]) !!}
			<div class="form-group">
				<label class="control-label col-sm-2" for="email">Package Title:</label>
				<span class="col-sm-10" for="email"><?php echo $info->ProductTitle;?></span>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2" for="email">Package Id:</label>
				<span class="col-sm-10" for="email"><?php echo $info->PackageNumber;?></span>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2" for="email">Penalty Amount:</label>
				<div class="col-sm-3" id="er_container_penalty">
					<input type="text" class="form-control" name="penalty" id="penalty" value="" placeholder="Penalty Amount" >
					<div id="er_penalty" class="help-block"></div>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2" for="email">Rejected By:</label>
				<div class="col-sm-8">
					<label class="radio-inline"><input type="radio" name="reject_by" value="you" checked="checked"  onclick="switch_section('.receiver','.you');make_checked('you_osreturn');switch_section('.option_hide','.osreturn')" >Requester</label>
					<label class="radio-inline"><input type="radio" name="reject_by" value="receiver"  onclick="switch_section('.you','.receiver');make_checked('receiver_oscreturn');switch_section('.option_hide','.receiver_osreturn')" >Receiver</label>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2" for="email">Return Type:</label>
				<div class="col-sm-8 you">
					<label class="radio-inline"><input type="radio" name="return_type" value="osreturn" checked="checked" onclick="switch_section('.option_hide','.osreturn')" id="you_osreturn" >OSReturn</label>
					<!--
					<label class="radio-inline"><input type="radio" name="return_type" value="bycreturn" onclick="switch_section('.option_hide','.bycreturn')">ByCReturn</label>
					-->
					<label class="radio-inline"><input type="radio" name="return_type" value="ipayment" onclick="switch_section('.option_hide','.ipayment')">IPayment</label>
				</div>
				<div class="col-sm-8 receiver"  style="display:none;">
					<!--
					<label class="radio-inline"><input type="radio" name="return_type" value="bycreturn" onclick="switch_section('.option_hide','.receiver_bycreturn');"  id="receiver_bycreturn">ByCReturn</label>
					-->
					<label class="radio-inline"><input  id="receiver_oscreturn" type="radio" checked="checked" name="return_type" value="osreturn" onclick="switch_section('.option_hide','.receiver_osreturn')">OSReturn</label>
				</div>
			</div>
			<div class=" option_hide receiver_bycreturn" style="display:none;">
				<div class="form-group" >
					<label class="control-label col-sm-2" for="email">Return Address:</label>
					<div class="col-sm-3">
						<input type="text" class="form-control" name="ReturnAddress" id="ReturnAddress" value="{{$info->PickupAddress}}" placeholder="Address" >
					</div>
				</div>				
				<div class="form-group" >
					<label class="control-label col-sm-2" for="email">Country:</label>
					<div class="col-sm-3">
						<select name="ReturnCountry" class="form-control" id="country"  onchange="get_country_city_state('ajax/state','country','state')">
							<option value="">Select Country</option>
							<?php foreach($country as $key){ ?>
								<option value="{{$key->Content}}" <?php if($info->PickupCountry == $key->Content) { echo 'selected="selected"'; } ?> >{{$key->Content}}</option>
							<?php } ?>
						</select>
						<div id="er_country"></div>
					</div>
				</div>								
				<div class="form-group" >
					<label class="control-label col-sm-2" for="email">State:</label>
					<div class="col-sm-3">
						<select name="ReturnState" class="form-control" id="state" onchange="get_country_city_state('ajax/city','state','city')">
							<option value="">Select State</option>
							<?php foreach($state as $key){ ?>
								<option value="{{$key->Content}}" <?php if($info->PickupState == $key->Content) { echo 'selected="selected"'; } ?> >{{$key->Content}}</option>
							<?php } ?>
						</select>
						<div id="er_state"></div>
					</div>
				</div>
				<div class="form-group" >
					<label class="control-label col-sm-2" for="email">City:</label>
					<div class="col-sm-3">
						<select name="ReturnCity" class="form-control" id="city" >
							<option value="">Select City</option>
							<?php foreach($city as $key){ ?>
								<option value="{{$key->Content}}" <?php if($info->PickupCity == $key->Content) { echo 'selected="selected"'; } ?> >{{$key->Content}}</option>
							<?php } ?>
						</select>
						<div id="er_city"></div>
					</div>
				</div>
				<div class="form-group" >
					<label class="control-label col-sm-2" for="email">Zip Code:</label>
					<div class="col-sm-3">
						<input type="text" class="form-control" name="ReturnZip" id="zip" value="{{$info->PickupPinCode}}" placeholder="Zip Code" >
						<div id="er_zip"></div>
					</div>
				</div>
				<input type="hidden" name="ReturnLongitude" id="ReturnLongitude" value="{{$info->PickupLatLong[0]}}">
				<input type="hidden" name="ReturnLatitude" id="ReturnLatitude" value="{{$info->PickupLatLong[1]}}">
				<input type="hidden" name="distance" id="distance" value="">
			</div>
			<div class="form-group option_hide bycreturn receiver_bycreturn"  style="display:none;">
				<label class="control-label col-sm-2" for="email">Return Date & Time:</label>
				<div class="col-sm-2">
					<input type="text" class="form-control datepicker" name="return_date" value="" placeholder="Date" readonly>
				</div>
				<div class="col-sm-2">
					<input type="text" class="form-control timepicker" name="return_time" value="" placeholder="Time" readonly>
				</div>
			</div>
			<div class="form-group option_hide osreturn bycreturn ipayment receiver_bycreturn receiver_osreturn">
				<label class="control-label col-sm-2" for="email">Comment:</label>
				<div class="col-sm-8">
					<textarea class="form-control" rows="5" name="comment"></textarea>
				</div>
			</div>
        	<div class="form-group option_hide osreturn receiver_osreturn">
				<label class="control-label col-sm-2" for="email">Tracking Number:</label>
				<div class="col-sm-8">
					<input type="text" class="form-control" placeholder="Tracking Number" name="tracking_number" />
				</div>
			</div>
			<div class="form-group option_hide receiver_osreturn osreturn">
				<label class="control-label col-sm-2" for="email">Receipt:</label>
				<div class="col-sm-8">
					<input type="file" name="receipt" />
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-8 col-sm-offset-2">
					{!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
					<a href="{{ url('admin/package') }}" data-placement="top" title="Add" class="btn btn-primary btn-action">Cancel</a>
				</div>
			</div>
		{!! Form::close() !!}		
	<div class="clearfix"></div>
  </div> <!-- Panel Body -->
</div>
</div>
{!! Html::style('theme/date-time-picker/jquery.datetimepicker.css') !!}
			
{!! Html::script('theme/date-time-picker/jquery.datetimepicker.js') !!}

{!! Html::script('theme/admin/custome/js/validation.js') !!}

<script>
function switch_section(hide,show)
{
	$(hide).hide();
	$(show).show();
	
}
function make_checked(obj){
	$('#'+obj).attr('checked',true);
}
console.log((new Date()).getFullYear()+'/'+(new Date()).getMonth()+'/'+(new Date()).getDate());
$(function(){
	$('.datepicker').datetimepicker({			
		format:'d/m/Y',
		startDate:(new Date()).getFullYear()+'/'+(new Date()).getMonth()+'/'+(new Date()).getDate(),
		timepicker:false,
	}); 
	$('.timepicker').datetimepicker({			
		format:'H:i',
		datepicker:false,
	}); 
});

function get_country_city_state(url,superId,PutOnId)
{
	
	$.ajax({
		url:SITEURL+url,
		type:'GET',
		data:'&superid='+$('#'+superId).val(),
		success:function(res){
			var obj = eval("("+res+")");
			if(obj.success == 1){
				$('#'+PutOnId).html(obj.html);
			}
		}
	});
}
var validation_status = true;
function validate_cancel_package()
{
	var flag = true;
	var reject_by = $('input[name=reject_by]:checked').val();	
 
	if(reject_by == 'receiver' & validation_status != 'success')
	{
		validation_status = false;		
		
		if(valid.required('country','country') == false) { flag = false; }
		if(valid.required('state','state') == false) { flag = false; }
		if(valid.required('city','city') == false) { flag = false; }
		if(valid.required('zip','zip code') == false) { flag = false; }
		if(flag == true)
		{
			var startpoint = "{{$info->DeliveryAddress}} {{$info->DeliveryCity}} {{$info->DeliveryState}} {{$info->DeliveryCountry}} {{$info->DeliveryPincode}}";
			var endpoint = $('#ReturnAddress').val()+" "+$('#city').val()+" "+$('#state').val()+" "+$('#country').val()+" "+$('#zip').val(); 
			$.ajax({
				url:SITEURL+'ajax/address_information',
				type:'GET',
				data:'&startpoint='+startpoint+'&endpoint='+endpoint,
				success:function(res){
					
					var obj = eval("("+res+")");
					$("#distance").val(obj.distance);
					if(obj.distance > 0){
						
						 validation_status = 'success';
						 $("#ReturnLongitude").val(obj.long);
						 $("#ReturnLatitude").val(obj.lat);
						 
						 $('#validate_cancel_package_form').submit();
					}else{
						alert('Invalid return address.');
						return false;
					}			
				}
			}); 
			
		}else{ 
			validation_status = true;
		} 
	}else{
		 validation_status = true;
	}
	if(valid.numeric('penalty','penalty amount') == false) { flag = false; validation_status = false;} 
	return validation_status;
}

function get_address_information()
{
	var startpoint = "{{$info->DeliveryAddress}} {{$info->DeliveryCity}} {{$info->DeliveryState}} {{$info->DeliveryCountry}} {{$info->DeliveryPincode}}";
	var endpoint = $('#ReturnAddress').val()+" "+$('#city').val()+" "+$('#state').val()+" "+$('#country').val()+" "+$('#zip').val(); 
	$.ajax({
		url:SITEURL+'ajax/address_information',
		type:'GET',
		data:'&startpoint='+startpoint+'&endpoint='+endpoint,
		success:function(res){
			
			var obj = eval("("+res+")");
			$("#distance").val(obj.distance);
			if(obj.distance > 0){
				 validation_status = true;
				 $("#ReturnLongitude").val();
				 $("#ReturnLatitude").val();
				 return true;
			}else{
				alert('Invalid return address.');
				return false;
			}			
		}
	});
}

</script>


@include('Admin::layouts.footer')
@stop
