@extends('Admin::layouts.master')


@section('content')
@include('Admin::layouts.menu')

   {!! Html::script('theme/admin/custome/js/Chart.js') !!}
<div class="col-md-10">
   <h2>Dashboard</h2> 
  

      <?php  /*  echo Auth::user()->_id;    */   ?>
   <br />
</div>
<div class="col-md-2">
    <select class="form-control" id="yearDropDown">
      <option value=""> -- Select year -- </option>
      @foreach($yearsArray as $value)
        <option value="{{ $value }}"> {{ $value }} </option>
      @endforeach
    </select>
  </div>
@if(Auth::user()->dashbord_permission  == "yes")
<div class="col-md-3 col-xs-12">
  <div class="dashboard-bx blue-bg">
      <div class="dashb-icon">
        <i class="fa fa-user"></i>
      </div>
  	  <div class="dashb-txt">
      	<a href="javasacript::void(0)" data-toggle="modal" data-target="#myModal">
      	  <h1>{{$usercount}}</h1>
          <p>Users</p>
        </a>
      </div>    
  </div>
</div>

<div class="col-md-3 col-xs-12">
	<div class="dashboard-bx green-bg">    
      <div class="dashb-icon"><i class="fa fa-check"></i></div>
    	<div class="dashb-txt">
        <a href="javasacript::void(0)" data-toggle="modal" data-target="#myModal_request">
        	<h1>{{$completedelivery}}</h1>
            <p>Completed Delivery</p>
        </a>
      </div>   
    </div>
</div>

<div class="col-md-3 col-xs-12">
	<div class="dashboard-bx red-bg">    
        <div class="dashb-icon"><i class="fa fa-times"></i></div>
    	<div class="dashb-txt">
        <a href="javasacript::void(0)" data-toggle="modal" data-target="#myModal_cancel">
        	<h1>{{$canceldelivery}}</h1>
            <p>Cancelled Delivery</p>
         </a>
        </div>   
    </div>
</div>

<div class="col-md-3 col-xs-12">
  <div class="dashboard-bx panding-bg">    
    <div class="dashb-icon"><i class="fa fa-clock-o"></i></div>
    <div class="dashb-txt">
      <a href="#">
        <h1> {{ $pendingDelivery }} </h1>
        <p> Pending Delivery </p>
      </a>
    </div>   
  </div>
</div>

<div class="col-md-3 col-xs-12">
  <div class="dashboard-bx running-bg">    
    <div class="dashb-icon"><i class="fa fa-refresh"></i></div>
    <div class="dashb-txt">
      <a href="#">
        <h1> {{ $runningDelivery }} </h1>
        <p> Running Delivery </p>
      </a>
    </div>   
  </div>
</div>

<div class="col-md-3 col-xs-12">
  <div class="dashboard-bx yellow-bg">
    <div class="dashb-icon"><i class="fa fa-credit-card"></i></div>
    <div class="dashb-txt">
      <a href="javasacript::void(0)" data-toggle="modal" data-target="#myModal_transaction">
        <h3 style="font-size: 19px">${{number_format($totalpayment,2)}} | &#8373;{{ number_format($totalGhaniPayment, 2) }}</h3>
        <p>Total Payment</p>
      </a>
    </div>
  </div>
</div>

<div class="col-md-3 col-xs-12">
  <div class="dashboard-bx blue-bg">
    <div class="dashb-icon">
      <i class="fa fa-shopping-cart" aria-hidden="true"></i>
    </div>
    <div class="dashb-txt">
      <a href="#">
        <h3> {{ $totalOnlinePurchase }} </h3>
        <p> Online Purchase </p>
      </a>
    </div>
  </div>
</div>

<div class="col-md-3 col-xs-12">
  <div class="dashboard-bx green-bg">
    <div class="dashb-icon">
      <i class="fa fa-archive" aria-hidden="true"></i>
    </div>
    <div class="dashb-txt">
      <a href="#">
        <h3> {{ $totalBuyForMe }} </h3>
        <p> Buy For Me </p>
      </a>
    </div>
  </div>
</div>

<div class="col-md-3 col-xs-12">
  <div class="dashboard-bx red-bg">
    <div class="dashb-icon">
      <i class="fa fa-th-large" aria-hidden="true"></i>
    </div>
    <div class="dashb-txt">
      <a href="#">
        <h3> {{ $totalSendAPackage }} </h3>
        <p> Send A Package </p>
      </a>
    </div>
  </div>
</div>

<div class="col-md-3 col-xs-12">
  <div class="dashboard-bx panding-bg">
    <div class="dashb-icon">
      <i class="fa fa-truck" aria-hidden="true"></i>
    </div>
    <div class="dashb-txt">
      <a href="#">
        <h3> {{ $totalLocalDeliveries }} </h3>
        <p> Local Delivery </p>
      </a>
    </div>
  </div>
</div>

<!-- <div class="col-md-3 col-xs-12">
  <div class="dashboard-bx green-bg">
    
        <div class="dashb-icon"><i class="fa fa-check"></i></div>
       <div class="dashb-txt">
          <a href="javasacript::void(0)" data-toggle="modal" data-target="#myModal_request">
          <h1>{{$deliver_items}}</h1> 
            <p>Completed Items</p>
          </a>
        </div>
   
  </div>
</div> -->
@endif
 <!--<div class="col-md-6" style="margin-bottom:20px":>

  <div id="container" style="min-width: 310px; height: 400px;margin-bottom:20px; max-width: 600px; margin: 0 auto"></div>  
</div>   -->

 <style>
 	body{background:#F1F2F7;}
	.footer{position:fixed; bottom:0;}
 </style>
     
    
@include('Admin::other.charts')
@include('Admin::layouts.footer')




<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document" style="width:300px;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Select User</h4>
      </div>
      <div class="modal-body new-pop clearfix">
          <div class="light-btn"><a href="{{url('admin/transporter')}}">Transporter</a></div>
          <div class="dark-btn"><a href="{{url('admin/requester')}}">Requester</a></div>
      </div>
    </div>
  </div>
</div>


<!-- Requester Modal -->
<div class="modal fade" id="myModal_request" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document" style="width:300px;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Select Request</h4>
      </div>
      <div class="modal-body new-pop clearfix">
          <div class="light-btn2"><a href="{{url('admin/package?status=complete')}}">Package</a></div>
          <div class="dark-btn2"><a href="{{url('admin/online-package?status=complete')}}">Online Package</a></div>
          <div class="light-btn3"><a href="{{url('admin/buy-for-me?status=complete')}}">Buy for Me</a></div>
      </div>
    </div>
  </div>
</div>


<!-- Requester Modal cancel -->
<div class="modal fade" id="myModal_cancel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document" style="width:300px;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel2">Select Request</h4>
      </div>
      <div class="modal-body new-pop clearfix">
          <div class="light-btn2"><a href="{{url('admin/package?status=cancel')}}">Package</a></div>
          <div class="dark-btn2"><a href="{{url('admin/online-package?status=cancel')}}">Online Package</a></div>
          <div class="light-btn3"><a href="{{url('admin/buy-for-me?status=cancel')}}">Buy for Me</a></div>
      </div>
    </div>
  </div>
</div>

<!-- Transcction Modal cancel -->
<div class="modal fade" id="myModal_transaction" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document" style="width:300px;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel3">Select Request</h4>
      </div>
      <div class="modal-body new-pop clearfix">
          <div class="light-btn"><a href="{{url('admin/transporter-transaction-history')}}">Transporter</a></div>
          <div class="dark-btn"><a href="{{url('admin/requester-transaction-history')}}">Requester</a></div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $("#yearDropDown").on('change', function () {
    window.location.href = SITEURL + "admin/dashboard?year=" + this.value;
  });
</script>
@endsection