@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')
@section('content')

{!! Html::script('theme/admin/fancybox/source/jquery.fancybox.js') !!}
{!! Html::style('theme/admin/fancybox/source/jquery.fancybox.css') !!}
{!! Html::style('theme/admin/custome/css/adminsumoselect.css') !!}
{!! Html::script('theme/admin/custome/js/validation.js')!!}
{!! Html::script('theme/admin/custome/js/jquery.sumoselect.min.js') !!}
{!! Html::script('theme/admin/custome/css/parsley.css') !!}
{!! Html::script('theme/admin/custome/js/parsley.min.js')!!}
{!! Html::style('theme/date-time-picker/jquery.datetimepicker.css') !!}
{!! Html::script('theme/date-time-picker/jquery.datetimepicker.js') !!}
<style>

#parsley-id-multiple-item_name\[\] {
	position: absolute;
	bottom: -16px;
	padding-left: 13px;
	list-style-type: none;
	left: 3px;
	color: RED;
	font-size: 12px;
}
#parsley-id-11{
	position: absolute;
	bottom: -16px;
	padding-left: 13px;
	list-style-type: none;
	left: 3px;
	color: RED;
	font-size: 12px;
}
.SumoSelect {
	width: 100%;
}
</style>
<div class="container-fluid">
<?php
$total_item_cost = 0;
$total_item = 0;
$total_item_cost_by_user = 0;
$total_weight = 0;
foreach ($info->ProductList as $key => $value) {
	$total_item = $total_item + 1;
	$total_item_cost = $total_item_cost + ($value['price'] * $value['qty']);
	$total_item_cost_by_user = $total_item_cost_by_user+ $value['shipping_cost_by_user'];
	$total_weight += GetWeight($value['weight'] * $value['qty'], $value['weight_unit']);
}
?>

	<div id="msg_div" class=""></div>
   <div class="panel panel-default">
      <div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;Package Detail
         <a style="margin-right:5px;" class="btn btn-primary margin-top-less-7 pull-right" href="{{url('admin/buy-for-me')}}">Go Back</a>
      </div>
      <div class="panel-body">
         <div class="col-xs-12">
            <h3>Other Detail</h3>
            <div class="row">
               <div class="col-sm-4 custom-table1">
                  <label>Package Id:</label>
                     {{ucfirst($info->PackageNumber)}}
               </div>
               <div class="col-sm-4 custom-table1">
					<label>Distance:</label>
                     @if(!$info->distance == '')
                     		{{number_format($info->distance,2)}} Miles

                     @else
                     	0.00 Miles
                     @endif
               </div>
               <div class="col-sm-4 custom-table1">
                 <label>Status:</label>
	                <?php echo get_status_title($info->Status)['status']; ?>
               </div>
               <div class="col-sm-4 custom-table1">
                  <label>Requester Name:</label>
                  <a data-toggle="modal" href="#myModal" onclick="return get_user_info('{{$info->RequesterId}}')">{{ucfirst($info->RequesterName)}}</a>

                </div>
               <div class="col-sm-4 custom-table1">
				  <label>Receiver Mobile No:</label>
					{{$info->ReceiverCountrycode}}-{{$info->ReceiverMobileNo}}
		       </div>
		       <div class="col-sm-4 custom-table1">
				 <label>Receiver Name:</label>
				@if(@$info->ReceiverName != '')
					{{ucfirst(@$info->ReceiverName)}}
				@else
					N/A
				@endif
			    </div>
		       <div class="col-sm-4 custom-table1">
				  <label>Consolidate Shipping: </label>
					@if($info->consolidate_item == 'on') Yes @else No @endif
		       </div>

		       <div class="col-sm-4 custom-table1">
				  <label>Device Type: </label>
					{{ ucfirst(@$info->device_type)}}
		       	</div>

		       	<div class="col-sm-4 custom-table1">
				  <label>@if(@$info->device_type == 'website')Browser @else Device Version @endif: </label>
					{{ @$info->device_version}}
		       	</div>

		       	<div class="col-sm-4 custom-table1">
				  <label>@if(@$info->device_type == 'website')
                           Version
                        @else
                           App Version
                        @endif: </label>
					{{ @$info->app_version}}
		       	</div>


		       <div class="col-sm-4 custom-table1">
				  <label>Created Date: </label>
				  {{ show_date(@$info->EnterOn) }}
		       </div>

		       <div class="col-sm-4 custom-table1">
				  <label>Item Total: </label>
					<?php echo $total_item; ?>
		       </div>

		       <div class="col-sm-4 custom-table1">
				  <label>Total Weight:</label>
					{{ @$total_weight}} lbs
		       	</div>

		       <div class="col-sm-4 custom-table1">
				  <label>Comments on Listing:</label>
					{{ @$comment_count}} <u><a href="{{url('admin/comment-list',[$info->_id])}}" class="" title="Add/View Comment" style="cursor: pointer;background-color:#1FA0D9;color:white; ">Add/View Comment</a></u>
		       	</div>


		       @if($info->Status == 'delivered')
		       <div class="col-sm-4 custom-table1">
				  <label>Complete by Admin: </label>
				  	@if(isset($info->complete_by_admin))
				  		@if($info->complete_by_admin == 'yes')
				  		Yes
				  		@endif
				  	@else
				  	Not
				  	@endif
		       </div>
		       @endif


               <div class="col-sm-12 custom-table1">
				  <label>Drop Address: </label>
				    {{ucfirst($info->DeliveryFullAddress)}}
               </div>

               @if($info->marked_description)
               <div class="col-sm-12 custom-table1">
				 <label>Description</label>(Marked as Paid)<label>:</label>
					{{ucfirst($info->marked_description)}}
               </div>
               @endif
            </div>
         </div>
         <div class="clearfix"></div>
      </div>
      <!-- Panel Body -->
		</div>
<!--panel body product-->

<div class="panel panel-default">
	<div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;Products Detail </div>
		<div class="panel-body">
					@if(count($info->ProductList) > 0)
						@foreach($info->ProductList as $index)

					<div class="panel panel-default panel-body">

							@if($index['tpName'] != '')
							<div class="col-sm-4 custom-table1">
								 <b>Transporter Name</b>:
								 {{ucfirst(@$index['tpName'])}}
							</div>
							@endif
							<div class="col-sm-4 custom-table1">
								 <b>Quantity</b>:
								{{$index['qty']}} Unit(s) <br>
								@if (@$index['actual_count'])
									(Actual Quantity: {{ @$index['actual_count'] }})
								@endif
							</div>

							<div class="col-sm-4 custom-table1">
								 <b>Insurance Status</b>: {{ucfirst($index['insurance_status'])}}
							</div>

		<div class="col-sm-4 custom-table1">
			 <b>Shipping mode</b>:
			 @if(@$index['travelMode'] == 'ship')
			 	Sea
			 @else
			 	{{ucfirst(@$index['travelMode'])}}
			 @endif


		</div>

		<div class="col-sm-4 custom-table1">
			 <b>Status</b>: {{ get_status_title($index['status'])['status'] }}
		</div>

		<!-- <div class="col-sm-4 custom-table1">
						        <label>Shipping Cost</label>:
					               	${{number_format(@$index['shippingCost'],2)}}
								     <br>
							</div> -->

							<div class="col-sm-4 custom-table1">
							<label>Expected Date</label>:
							@if(isset($index['ExpectedDate']->sec))
					           {{ show_date(@$index['ExpectedDate']) }}
					        @else
					            N/A
					        @endif
							</div>

							<div class="col-sm-4 custom-table1">
								<label>Inform Mail Sent</label>:
								{{ucfirst(@$index['inform_mail_sent'])}}
							</div>



							<div class="col-md-12">
									<label>Website:</label>
									&nbsp;<a target="_blank" href="{{url($index['url'])}}">{{ucfirst(@$index['url'])}}</a>
									<div>
										<label>Description:&nbsp;</label>
										&nbsp;{{ucfirst(@$index['description'])}}
									</div>
									<div>
										<label>Package id:&nbsp;</label>
										&nbsp;{{ucfirst(@$index['package_id'])}}
									</div>

							    <div align="right">
							   <a class="btn btn-primary" href="{{url('admin/activity-log?package_id='.$index['package_id'])}}" >Activity log</a>

							@if($itemCount > 0)
								<?php $btn_status = 'display:none';
if ($index['status'] == in_array($index['status'], ['not_purchased', 'paid', 'purchased', 'item_received', 'assign'])) {$btn_status = 'display:inline-block;';}?>
								<a class="btn btn-primary" title="Item detail" data-toggle="modal" style="{{$btn_status}}" id="item_update-{{$index['_id']}}"
									data-target="#old_item" onclick="old_item('{{$index['_id']}}','{{$info['_id']}}',this)" >Old Detail</a>
							@endif
								</div>
							<br/>
							</div>




							<div class="col-md-12">
								<table class="custom-table table table-bordered " >
								<thead>
									<tr >
										<th>Product Image</th>
										<th >Name</th>
										<th>Category</th>
										<th >Weight</th>
										<th >Height</th>
										<th >Width</th>
										<th>Length</th>

										<th>Price</th>
										<th>Verification Code</th>
										<th>Shipping <br /> <small>(Retailer to Aquantuo's facility)</small></th>
									</tr>

								</thead>
								<tbody>
									<tr>
										<td><?php
$fileexists = false;
if (isset($index['image']) && $index['image'] != '') {
	if (file_exists(BASEURL_FILE . $index['image'])) {
		$fileexists = true;
		?>
												<a class="fancybox" rel="group" href="{{ ImageUrl.$index['image'] }}" >
						                           <img id="senior-preview" src="{{ ImageUrl.$index['image'] }}" class="margin-top" width="120px" height="100px" >
						                         </a><?php
}
}
if ($fileexists == false) {?>
											<img src="{{ImageUrl}}no-image.jpg" class="margin-top" width="120px" height="100px">
										<?php }?>
										</td>
										<td>
											@if(isset($index['product_name']))
												{{ucfirst($index['product_name'])}}
											@endif
										</td>
										<td>
											{{ucfirst($index['category'])}}
										</td>
										<td>
										@if($index['weight'] != '')
											{{$index['weight']* $index['qty']}}&nbsp;{{$index['weight_unit']}}
										@else
												N/A
										@endif
										</td>
										<td>
										@if($index['height'] != '')
											{{ucfirst($index['height'])}}&nbsp;{{$index['heightUnit']}}
										@else
												N/A
										@endif
										</td>
										<td>
										@if($index['width'] != '')
											&nbsp;{{$index['width']}}{{$index['widthUnit']}}
										@else
												N/A
										@endif
										</td>
										<td>
										@if($index['length'] != '')
											&nbsp;{{$index['length']}}{{$index['lengthUnit']}}
										@else
												N/A
										@endif
										</td>

										<td>
											${{number_format($index['price'] * $index['qty'],2)}}

										</td>
										<td>
											{{$index['verify_code']}}
										</td>
										<td>
											${{number_format(floatval(@$index['shipping_cost_by_user']),2)}}
										</td>

									</tr>
										<tr><?php
$value = @$index['shippingCost'] - $index['aq_fee'];
?>



										</tr>
									</tbody>
								</table>
							</div>
					@if(@$index['status'] == 'cancel')
						<h2 class="text-primary">Cancel Information</h2>

						<div class="col-sm-4 custom-table1">
							<b>Transporter Message</b>:
							{{ucfirst(@$index['TransporterMessage'])}}
						</div>
						<div class="col-sm-4 custom-table1">
							<b>Reject By</b>:
							{{ucfirst(@$index['RejectBy'])}}
						</div>

						<div class="col-sm-4 custom-table1">
							<b>Reject Time</b>:
							{{date('d m,Y h:i A',@$index['RejectTime']->sec)}}
						</div>

						<div class="col-sm-4 custom-table1">
							<b>Return Type</b>:
							{{@$index['ReturnType']}}
						</div>
						@if(!empty(trim($index['TrackingNumber'])))
							<div class="col-sm-4 custom-table1">
								<b>Tracking Number</b>:
								{{@$index['TrackingNumber']}}
							</div>
						@endif

						<div class="col-sm-4 custom-table1">
							<b>Receipt Image</b>:<br />
							@if($index['ReceiptImage'] != '')
							<img src="{{ImageUrl.@$index['ReceiptImage']}}" width="80" height="80">
							@endif
						</div>
					@endif

									@if(!@$index['TransporterFeedbcak'] == '')
									<div class="">
								      <div class="col-sm-6">
								         <div class="panel panel-default">
								            <div class="panel-heading">&nbsp;&nbsp;&nbsp;Transporter Ratting</div>
								            <div class="panel-body">
								               <div class="col-md-12 col-sm-12 custom-table1 row">

								                  	<table><tr>
														<td><label style="font-size:14px;">Ratting:&nbsp;</label></td>
															<td><div class="ratingAdjustmentcss" style="margin-bottom:10px;">
																<div class="graph" style="width:<?php echo $index['TransporterRating'] * 20; ?>%">
																	<img class="rate" src="{{ImageUrl}}/5star.png" >
																</div>
															</div></td>
															</tr></table>
													@if(!@$index['TransporterFeedbcak'] == '')
															<label>Feedback:&nbsp;</label>
															&nbsp;{{ucfirst(@$index['TransporterFeedbcak'])}}
													@endif
								               </div>
								            </div>
								         </div>
								      </div>
								   </div>
								   	@endif
								   	@if(!@$index['RequesterFeedbcak'] == '')
								  	 <div class="">
								      <div class="col-sm-6">
								         <div class="panel panel-default">
								            <div class="panel-heading">&nbsp;&nbsp;&nbsp;Requester Ratting</div>
								            <div class="panel-body">
								               <div class="col-md-12 col-sm-12 custom-table1 row">
								                  <table><tr>
														<td><label style="font-size:14px;">Ratting:&nbsp;</label></td>
															<td><div class="ratingAdjustmentcss" style="margin-bottom:10px;">
																<div class="graph" style="width:<?php echo $index['RequesterRating'] * 20; ?>%">
																	<img class="rate" src="{{ImageUrl}}/5star.png" >
																</div>
															</div></td>
													</tr></table>
													@if(!@$index['RequesterFeedbcak'] == '')
															<label>Feedback:&nbsp;</label>
															&nbsp;{{ucfirst(@$index['RequesterFeedbcak'])}}
													@endif
								               </div>
								            </div>
								         </div>
								      </div>
								   </div>
								  @endif

							<div class="col-sm-6"><br />
							@if(isset($index['ExpectedDate']))
							@if($index['ExpectedDate'] != '')
								<button type="button" class="btn btn-primary assign-btn" data-toggle="modal" data-target="#myModaldate{{ $index['_id'] }}">Expected Date</button>
                            @endif
                            @endif
							</div>

							<div class="col-sm-6">
								<div class="text-right" style="margin-top:0px;">



<?php $btn_status = 'display:none';
if ($index['status'] == 'ready') {$btn_status = 'display:inline;';}?>

<a  type="button" class="btn btn-primary" title="Item Reviewed"   style=" {{$btn_status}}" id="reviewButton{{@$index['_id']}}" onclick="return confirm('Are you sure, you have review this item?')? status_review('{{@$index['_id']}}'):'';" >Review</a>


<?php
	$btn_status = 'display:none';
	if ($index['status'] == 'reviewed' || $index['status'] == 'paid' || $index['status'] == 'purchased' || $index['status'] == "item_received") { $btn_status = 'display:inline-block;'; }
?>
<a type="button" class="btn btn-primary" title="Update Item" data-toggle="modal"  style="{{$btn_status}}" id="item_update-{{$index['_id']}}" data-target="#edit_item" onclick="item_purchased('{{$index['_id']}}','{{$info['_id']}}',this)"> Update Item </a>

<?php $btn_status = 'display:none';
if ($index['status'] == 'not_purchased') {$btn_status = 'display:inline';}?>

<span id="waiting-for-payment-{{$index['_id']}}" style="{{$btn_status}}" class="btn" >Waiting for a requester to review</span>


<!-- <a  type="button" class="btn btn-primary" title="Inform Requester" data-toggle="modal"  style="{{$btn_status}} " id="inform-{{@$index['_id']}}" onclick="send_upddate_mail('{{@$index['_id']}}','{{$info['_id']}}','{{@$index['product_name']}}')" >Inform Requester</a> -->

<?php $btn_status = 'display:none';
if ($index['status'] == 'paid') {$btn_status = 'display:inline-block;';}?>
<a  type="button" class="btn btn-primary purchase_button" title="Purchased" style="{{$btn_status}}" id="purchased-{{$index['_id']}}" onclick="return delivery_process('purchased','{{@$index['_id']}}',this,'Are you sure you have purchased this item.');" >Purchased</a>


<?php $btn_status = 'display:none';
if ($index['status'] == 'purchased') {$btn_status = 'display:inline-block';}?>

<a  type="button" class="btn btn-primary purchase_button" title="Purchased" style="{{$btn_status}}" id="item_received-{{$index['_id']}}" onclick="return delivery_process('item_received','{{@$index['_id']}}',this,'Are you sure you have received this item.');" >Item Received</a>






<?php $btn_status = 'display:none';
if ($index['status'] == 'item_received' | $index['status'] == 'assign') {$btn_status = 'display:inline-block';}
$title = "Shipment Departed";

if ($index['status'] == 'assign') {
	$title = "Reassign Transporter";
}

?>
<a  type="button" class="btn btn-primary assign-btn" title="Shipment Departed" data-toggle="modal"  style="{{$btn_status}}" id="assign-{{$index['_id']}}" data-target="#assign_tp" onclick="$('#tp_itemid').val('{{$index['_id']}}')" >{{$title}}</a>

@if($index['status'] == 'assign')
	<a  type="button" class="btn btn-primary" title="Out for Pickup" style="{{$btn_status}}" id="item_received-{{$index['_id']}}" onclick="return delivery_process('out_for_pickup','{{@$index['_id']}}',this,'Are you sure! you are ready to pickup this item');" >Out for Pickup</a>
@endif

@if($index['status'] == 'out_for_pickup')
	<a  type="button" class="btn btn-primary" title="Out for Delivery" onclick="return delivery_process('out_for_delivery','{{$index['_id']}}',this,'Are you sure! you have picked up this item');">Out for Delivery</a>
@endif

@if($index['status'] == 'out_for_delivery')
	<a  type="button" class="btn btn-primary" title="Delivered" onclick="return delivery_process('delivered','{{$index['_id']}}',this, 'Are you sure! you have delivered this item');">Delivered</a>
@endif

<?php $btn_status = 'display:none';
if (in_array($index['status'], ['accepted', 'ready', 'not_purchased', 'reviewed', 'paid'])) {
	$btn_status = 'display:inline-block;';
}?>

<a type="button" data-placement="top" style="{{$btn_status}}" class="btn btn-danger" title="Cancel" onclick="return delivery_process('cancel','{{@$index['_id']}}',this,'Are you sure? You want to cancel this request.');" id="cancel-{{$index['_id']}}" >Cancel</a>

@if(@$index['status'] == 'pending')
		@if(isset($index['inform_mail_sent']) && @$index['inform_mail_sent'] != 'yes')
			<a  type="button" class="btn btn-primary" title="Inform Requester" data-toggle="modal"  style="display:inline-block" id="inform2-{{@$index['_id']}}" onclick="send_upddate_mail('{{@$index['_id']}}','{{$info['_id']}}','')" >Inform Requester</a>
		@endif
	@endif

		</div>
	</div>
</div>
<!--cancel-model-->

				@endforeach

				<div class="col-sm-12 custom-table1"><hr>
					<div class="row">

					<?php
$total_aq_fee = 0;
$total_shippingCost = 0;
foreach ($info->ProductList as $key => $value) {
	$total_aq_fee += @$value['aq_fee'];
	$total_shippingCost += @$value['shippingCost'];
	$status[] = $value['status'];
}
$value = $total_shippingCost - $total_aq_fee;
?>
                                <div class="col-sm-12">
                                  <div class="col-sm-4">
                   <h3><b>Payment Information</b></h3>
                     <table class="table table-bordered">
  					<tbody bgcolor="#F1F1F1">

	                <tr>
	                   <td class="">
	                      <div class="col-xs-8 text-left">
	                        <label class="" for="exampleInputEmail1"><b>Shipping Cost:</b> </label>
	                      </div>
	                      <div class="col-xs-4 text-right">
	                        ${{number_format($info->shippingCost,2)}}
	                      </div>
	                    </td>
	               </tr>
	               <tr>
	                   <td class="">
	                      <div class="col-xs-8 text-left">
	                        <label class="" for="exampleInputEmail1"><b> Shipping Cost:
								(Retailer to Aquantuo's facility): </b> </label>
	                      </div>
	                      <div class="col-xs-4 text-right">
	                        	(+)${{number_format($total_item_cost_by_user,2)}}
	                      </div>
	                    </td>
	               </tr>

			       <tr>
                      <td class="">
                        <div class="col-xs-8 text-left">
                           <label class="" for="exampleInputEmail1"><b>Item(s) Cost:</b> </label>
                        </div>
                          <div class="col-xs-4 text-right">
                            (+)${{number_format($total_item_cost,2)}}
                          </div>
                       </td>
                    </tr>

					@if($info->insurance > 0)
					<tr>
						<td class="">
							<div class="col-xs-8 text-left">
								<label class="" for="exampleInputEmail1"><b>Insurance:</b> </label>
							</div>
							<div class="col-xs-4 text-right">
								(+)${{number_format($info->insurance,2)}}
							</div>
						</td>
					</tr>
					@endif

				<tr>
					<td>
						<div class="col-xs-8 text-left">
							<label> <b> Duty/Customs Clearing: </b> </label>
						</div>
						<div class="col-xs-4 text-right">
							(+)${{ number_format($info->DutyAndCustom, 2) }}
							
							<small><span data-toggle="modal" data-target="#edit_customs_duty_div"  class="glyphicon glyphicon-pencil cursor-p"></span></small> 
						</div>
						
					</td>
				</tr>

				<tr>
					<td>
						<div class="col-xs-8 text-left">
							<label> <b> Tax: </b> </label>
						</div>
						<div class="col-xs-4 text-right">
							(+)${{ number_format($info->Tax, 2) }}
						<small><span data-toggle="modal" data-target="#edit_tax_div" class="glyphicon glyphicon-pencil cursor-p"></span></small>
						
						
						</div>
					</td>
				</tr>

                  @if($info->discount > 0)
                  <tr>
                    <td class="">
                        <div class="col-xs-8 text-left">
                          <label class="" for="exampleInputEmail1"><b>Aquantuo Discount:</b>
                           </label>
                                @if(!$info->PromoCode == '')
                                 <small>(<b>Promocode:</b>{{$info->PromoCode }}) </small>
                                 @endif
                        </div>
                        <div class="col-xs-4 text-right">
                            (-)${{number_format($info->discount,2)}}
                        </div>
                    </td>
                  </tr>
                  @endif
                   <tr>
                       <td class="">
                              <div class="col-xs-8 text-left">
                                 <label class="" for="exampleInputEmail1">
                                    <b>Processing Fee: <br />
                                    </b>
                                 </label>
                              </div>
                              <div class="col-xs-4 text-right">
                                 @if($info['ProcessingFees'] != '')
                                 (+)${{number_format($info['ProcessingFees'],2)}}
                                 @else
                                 $0.00
                                 @endif
                              </div>
                           </td>
                        </tr>
                    <tr>

                    <tr>
					 <td class="">
						<div class="col-xs-8 text-left">
							<label class="" for="exampleInputEmail1"><b>Region Cost:</b> </label>
						</div>
							<div class="col-xs-4 text-right">
    							${{ number_format($info->AreaCharges,2)}}
							</div>
					    </td>
					</tr>


                    <tr>
					 <td class="">
						<div class="col-xs-8 text-left">
							<label class="" for="exampleInputEmail1"><b>After Item Review:</b> </label>
						</div>
							<div class="col-xs-4 text-right">
    							${{ number_format($info->after_update_difference,2)}}
							</div>
					    </td>
					</tr>



					 <td class="">
						<div class="col-xs-8 text-left">
							<label class="" for="exampleInputEmail1"><b><u>Total Price:</u></b> </label>
						</div>
							<div class="col-xs-4 text-right">
    							${{ number_format($info->TotalCost + $info->after_update_difference,2)}}
							</div>
					    </td>
					</tr>
                   </tbody>
				</table>
				<a style="" class="btn btn-primary" href="{{url('admin/payment-log',[$info->_id])}}">Payment Log</a>
               </div>
                 <div class="col-sm-4">

            <style>
          .card_info{backgroung:#fff; display:flex; display:-webkit-flex; align-items:center; padding:10px; border:1px solid #eee;}
          .card_box{float:left; width:80px; height:50px; display:flex; display:-webkit-flex; align-items:center; background:#eee; border:1px solid #eee; overflow:hidden;}
          .card_box img{max-width:100%;}
          .card_info_text{ padding-left:15px; display:flex; display:-webkit-flex; flex:1;}
          .justify-right{justify-content:end; padding-right:10px;}
         </style>

         @if(count($info->mobilemoney) > 0)
          <h3><b>Mobile Payment</b></h3>
         @foreach($info->mobilemoney as $data)
         <div class="card_info">
          <div class="card_box">
          	 @if($data['mobile_type'] == 'airtel')
            <img  src="{{ImageUrl}}/webcard/airtel_money.png">
          @elseif($data['mobile_type'] == 'mtn')
            <img  src="{{ImageUrl}}/webcard/mtn.png">
          @elseif($data['mobile_type'] == 'tigo')
            <img src="{{ImageUrl}}/webcard/tigo.png">
          @elseif($data['mobile_type'] == 'vodafone')
            <img src="{{ImageUrl}}/webcard/vodafone.png">
          @endif
          </div>
          <div class="card_info_text">
            <h5>XXXX-XXXX-{{ $data['alternet_moblie'] }}</span></h5>
          </div>
          <div class="card_info_text">
            <h5><span>
              @if($data['mobile_money_status'] == 'awaiting_payment')
              	Awaiting Payment
              @else
 				Completed
              @endif
            </span></h5>
          </div>
             <?php
if (isset($data['CurrencyRate'])) {
	$CurrencyRate = $data['CurrencyRate'];
} else {
	$CurrencyRate = 1;
}

?>

          <div class="card_info_text justify-right">
            <b class="pull-right">${{ number_format($data['mobile_money_amount']/$data['CurrencyRate'],2) }}</b>
          </div>
         </div>
         @endforeach
         @endif


                                   </div>
               <div class="col-sm-4 text-right">
                <h3><b>Fee Details</b></h3>
               	<table class="table table-bordered">
  					<tbody bgcolor="#F1F1F1">
               		<!--     -->
	                <tr>
	                   <td class="">
	                      <div class="col-xs-8 text-left">
	                        <label class="" for="exampleInputEmail1"><b>Shipping Cost:</b> </label>
	                      </div>
	                      <div class="col-xs-4 text-right">
						  ${{number_format($info->shippingCost,2)}}
	                      </div>
	                    </td>
	               </tr>


			       <tr>
                      <td class="">
                        <div class="col-xs-8 text-left">
                           <label class="" for="exampleInputEmail1"><b>Aquantuo Fee:</b> </label>
                        </div>
                          <div class="col-xs-4 text-right">
                           (-)${{number_format($total_aq_fee,2)}}
                          </div>
                       </td>
                    </tr>


                  <tr>
                    <td class="">
                      <div class="col-xs-8 text-left">
                        <label class="" for="exampleInputEmail1"><b>Transporter Earning:</b> </label>
                      </div>
                      <div class="col-xs-4 text-right">
                         ${{number_format($total_shippingCost - $total_aq_fee,2)}}
                      </div>
                    </td>
                  </tr>


                   </tbody>
				</table>




				    </div>
				</div>
				<div class="col-sm-12 custom-table1 text-right"><hr>
					<!-- @if($info->Status == 'ready')
     				<a  class="btn btn-primary" href="{{url('accept-all',[$info->_id])}}"  id="accept_all_button" onclick="return confirm('Are you sure, you reviewed all items?')">Review All</a>
     				@endif -->

                           @foreach($info->ProductList as $value)
                    			@if('ready' == $value['status'])
                                   <?php $check_review = "true";?>
                                @endif
                           @endforeach
 							@if(isset($check_review))
                           <a  type="button" class="btn btn-primary assign-btn" title="Select Item" data-toggle="modal"  id="review-{{$index['_id']}}" data-target="#review_select" >Review All</a>
                           @endif


                            @foreach($info->ProductList as $value)
                    			@if($value['status'] == in_array($value['status'],['ready','not_purchased','reviewed']))
                    			<?php $check_calcel = "true";?>
                                @endif
                           @endforeach

                                @if(isset($check_calcel))
                                     <a  type="button" class="btn btn-danger assign-btn" title="Select Item" data-toggle="modal"  id="cancel-{{$index['_id']}}" data-target="#cancel_all" >Cancel All</a>
                                @endif


     				<!-- Purchased all button  -->

<!--  				 	@if (in_array("paid", $status))
					   <a  class="btn btn-primary" href="{{url('purchased-all',[$info->_id])}}" title="Purchased All" id="purchased_all" onclick="return confirm('Are you sure, you have purchased all item.');" >Purchased All</a>
					@endif -->

                    @if (in_array("paid", $status))
					 <a  type="button" class="btn btn-primary assign-btn" title="Select Item" data-toggle="modal"  id="review-{{$index['_id']}}" data-target="#purchased_select" >Purchased All</a>
					 @endif


     				@if(in_array("purchased", $status))
 						<a  class="btn btn-primary" title="select Item" data-toggle="modal"  title="Received All" id="purchased-{{$index['_id']}}" data-target="#itemreceived_select" >Received All</a>
     				@endif


     				@if(in_array('item_received',$status))
                       <a  type="button" class="btn btn-primary assign-btn" title="Assign All" data-toggle="modal"  id="assign-{{$index['_id']}}" data-target="#assign_tp_again" onclick="$('#tp_itemid').val('{{$index['_id']}}')" >Assign All</a>
     				@endif
     				
     				@if (in_array('assign', $status))
						<button class="btn btn-primary" type="button" title="Out for Pickup All" data-toggle="modal" data-target="#out_for_pickup_all_modal"> Out for Pickup All </button>
					@endif
					
					@if (in_array('out_for_pickup', $status))
						<button class="btn btn-primary" type="button" title="Out for Delivery All" data-toggle="modal" data-target="#out_for_delivery_all_modal"> Out for Delivery All </button>
					@endif
					
					@if (in_array('out_for_delivery', $status))
						<button class="btn btn-primary" type="button" title="Delivered All" data-toggle="modal" data-target="#delivered_all_modal"> Delivered All </button>
					@endif

     				<?php $btn_status = 'display:none';
if ($info->Status == 'pending' | @$info->need_to_pay > 0.1) {$btn_status = 'display:inline-block';}?>
					<span id="reminder_button" style="{{$btn_status}}"  >
     					<a class="btn btn-primary" title="Send payment reminder"
						onclick="delivery_process('payment_reminder','{{@$info->_id}}',this,'Are you sure, you want to send reminder.');" >Reminder for Payment</a>&nbsp;&nbsp;

					</span>

					@if($info->Status == 'not_purchased')
				<a data-toggle="modal" data-target="#paid_mark"  title="Mark as Paid" class="btn btn-primary"><i class="fa fa-check-square-o"></i>&nbsp;Mark as Paid
					</a>
					@endif
				</div>
			@endif
			<div class="clearfix"></div>
	</div>
	</div>

<!--end-->


   <?php if (@$info->Penalty > 0): ?>
   <div class="row">
      <div class="col-sm-12">
         <div class="panel panel-default">
            <div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;Penalty</div>
            <div class="panel-body">
               <div class="col-md-6 col-sm-6 custom-table1 row">
                  <div class="row">
                     <div class="col-md-6 col-sm-6"><b>Penalty Status</b> :</div>
                     <div class="col-md-4 col-sm-4"> {{ucfirst($info->PenaltyStatus)}}</div>
                  </div>
               </div>
               <div class="col-md-6 col-sm-6 custom-table1 row">
                  <div class="row">
                     <div class="col-md-6 col-sm-6 text-right"><b>Penalty Status</b> :</div>
                     <div class="col-md-4 col-sm-4 text-right"> ${{number_format(($info->Penalty/100),2)}}</div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <?php endif;?>
   <div class="row">


      <?php if ($info['ProductImage'] != "") {
	?>
      <div class="col-sm-12">
         <div class="panel panel-default">
            <div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;Package Image</div>
            <div class="panel-body">
               <?php

	if ($info['ProductImage'] != '') {
		if (file_exists(BASEURL_FILE . $info['ProductImage'])) {?>
               <div class="col-md-2">
                  <a class="fancybox" href="{{ImageUrl}}{{ $info->ProductImage}}" data-fancybox-group="gallery" title="Product Image">
                  <img src="<?php echo ImageUrl . $info['ProductImage']; ?>" tag="product image" style="width:178px; height:178px;"/></a>
               </div>
               <?php }
	}
	if (is_array($info["OtherImage"])) {

		foreach ($info['OtherImage'] as $key) {
			if ($key != '') {
				if (file_exists(BASEURL_FILE . $key)) {?>
               <div class="col-md-2">
                  <a class="fancybox" href="{{ImageUrl}}{{$key}}" data-fancybox-group="gallery" title="Product Image">
                  <img src="<?php echo ImageUrl . $key; ?>" tag="product image" style="width:178px; height:178px;"/></a>
               </div>
               <?php }
			}
		}

	}?>
               <div class="clearfix"></div>
            </div>
            <!-- Panel Body -->
         </div>
      </div>
      <?php }?>
   </div>

   <!--  Modal content for the mixer image example -->
   <div class="modal fade pop-up-map" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel-1" aria-hidden="true">
      <div class="modal-dialog modal-lg">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
               <div id="map1"></div>
               <p id="error"></p>
            </div>
         </div>
         <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
   </div>
   <!-- /.modal mixer image -->
</div>




<!-- Modal for cancel select -->
<div id="cancel_all" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" id="assign_tp_close">&times;</button>
        <h4 class="modal-title">Select Item for Cancel</h4>
      </div>
      {!! Form::model('', ['name'=>'add-appcontent','data-parsley-validate','id' => 'cancel_form', 'method' => 'POST', 'url' => ['cancel-all/'.Request::segment(4)]]) !!}

      	<div class="modal-body">
	        <div class="row">
	        	<input type="hidden" name="requestid" value="{{Request::segment(4)}}">
	        	<div class="col-md-12">
	        		<div class="form-group">
					    <label for="item_name">Item:</label>
					    <select class="form-control testselect3 required" data-parsley-required-message = 'Please select: At least one item is required.' required  multiple="" id="tp_name" name="item_name[]">
					   @foreach($info->ProductList as $index)
					   	@if($index['status'] == in_array($index['status'],['ready','reviewed','not_purchased']))
					    <option value='{{$index['_id']}}' >{{ucfirst($index['product_name'])}}</option>
					    @endif
					   @endforeach
					    </select>
					    <div class="red" id="er_tp_name"></div>
					</div>
	        	</div>
	        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" id="assing_tp_btn" >Submit</button>
      </div>
      </form>
    </div>

  </div>
</div>
<!-- Model end -->




<!-- Modal for Item received -->
<div id="itemreceived_select" class="modal fade" role="dialog">
  <div class="modal-dialog modal-sm">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" id="assign_tp_close">&times;</button>
        <h4 class="modal-title">Select Item received</h4>
      </div>
      {!! Form::model('', ['name'=>'add-appcontent','data-parsley-validate', 'id' => 'received_from', 'method' => 'POST', 'url' => ['received-all/'.Request::segment(4)]]) !!}
      	<div class="modal-body">
	        <div class="row">
	        	<input type="hidden" name="requestid" value="{{Request::segment(4)}}">
	        	<div class="col-md-12">
	        		<div class="form-group">
					    <label for="item_name">Item:</label>
					    <select class="form-control testselect3 required" data-parsley-required-message = 'Please select atlist one  item is required.' required multiple="" id="tp_name" name="item_name[]">
					   @foreach($info->ProductList as $index)
					   	@if($index['status'] == 'purchased')
					    <option value='{{$index['_id']}}' >{{ucfirst($index['product_name'])}}</option>
					    @endif
					   @endforeach
					    </select>
					    <div class="red" id="er_tp_name"></div>
					</div>
	        	</div>
	        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" id="received_id" >Submit</button>
      </div>
      </form>
    </div>

  </div>
</div>
<!-- Model end -->



<!-- Modal for purchas select -->
<div id="purchased_select" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" id="assign_tp_close">&times;</button>
        <h4 class="modal-title">Select Item for purchase</h4>
      </div>
      {!! Form::model('', ['name'=>'add-appcontent','id'=>'purchase_select_from', 'data-parsley-validate', 'method' => 'POST', 'url' => ['purchased-all/'.Request::segment(4)]]) !!}
      	<div class="modal-body">
	        <div class="row">
	        	<input type="hidden" name="requestid" value="{{Request::segment(4)}}">
	        	<div class="col-md-12">
	        		<div class="form-group">
					    <label for="item_name">Item:</label>
					    <select class="form-control testselect3 required" data-parsley-required-message = 'Please select atlist one  item is required.' required multiple="" id="tp_name" name="item_name[]">
					   @foreach($info->ProductList as $index)
					   	@if($index['status'] == 'paid')
					    <option value='{{$index['_id']}}' >{{ucfirst($index['product_name'])}}</option>
					    @endif
					   @endforeach
					    </select>
					    <div class="red" id="er_tp_name"></div>
					</div>
	        	</div>
	        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" id="purchase_id" >Submit</button>
      </div>
      </form>
    </div>

  </div>
</div>
<!-- Model end -->




<!-- Modal for review select -->
<div id="review_select" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" id="assign_tp_close">&times;</button>
        <h4 class="modal-title">Select Item for review</h4>
      </div>
      {!! Form::model('', ['name'=>'add-appcontent','id' => 'review_from', 'method' => 'POST', 'data-parsley-validate', 'url' => ['accept-all/'.Request::segment(4)]]) !!}
      	<div class="modal-body">
	        <div class="row">
	        	<input type="hidden" name="requestid" value="{{Request::segment(4)}}">
	        	<div class="col-md-12">
	        		<div class="form-group">
					    <label for="item_name">Item:</label>
					    <select class="form-control testselect3 required" data-parsley-required-message = 'Please select atlist one  item is required.' required multiple="" id="tp_name" name="item_name[]">
					   @foreach($info->ProductList as $index)
					   	@if($index['status'] == 'ready')
					    <option value='{{$index['_id']}}' >{{ucfirst($index['product_name'])}}</option>
					    @endif
					   @endforeach
					    </select>
					    @if($errors->has('item_name'))
                            <p class="controll-error help-block"><i class="fa fa-times-circle-o"></i>&nbsp;
                                {{ $errors->first('item_name') }}
                            </p>
                            @endif
					</div>

	        	</div>
	        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" id="review_id"  >Submit</button>
      </div>
      </form>
    </div>

  </div>
</div>
<!-- Model end -->

<!-- Model tax update open -->

<div id="edit_tax_div" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" id="edit_tax_model">&times;</button>
        <h4 class="modal-title">Update Tax</h4>
      </div>
      {!! Form::model('', ['name'=>'edit-request-tax','id' => 'edit-request-tax-form', 'method' => 'POST', 'data-parsley-validate', 'url' => ['admin/edit-request-tax/'.Request::segment(4)]]) !!}
      	<div class="modal-body">
	        <div class="row">
	        	<input type="hidden" name="requestid" value="{{Request::segment(4)}}">
	        	<div class="col-md-12">
	        		<div class="form-group">
					    <label for="item_name">Tax in ($):</label>
					    <input type="text" value="{{$info->Tax}}" class="form-control" id="edit_tax_input" name="edit_tax_input">
					    @if($errors->has('edit_tax_input'))
                            <p class="controll-error help-block"><i class="fa fa-times-circle-o"></i>&nbsp;
                                {{ $errors->first('edit_tax_input') }}
                            </p>
                        @endif
					</div>

	        	</div>
	        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" id="review_id"  >Submit</button>
      </div>
      </form>
    </div>

  </div>
</div>
<!-- Model end -->


<!-- Model tax update open -->

<div id="edit_customs_duty_div" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" id="edit_tax_model">&times;</button>
        <h4 class="modal-title">Update Duty/Customs Clearing</h4>
      </div>
      {!! Form::model('', ['name'=>'edit-request-tax','id' => 'edit-request-customduty-form', 'method' => 'POST', 'data-parsley-validate', 'url' => ['admin/edit-request-customsduty/'.Request::segment(4)]]) !!}
      	<div class="modal-body">
	        <div class="row">
	        	<input type="hidden" name="requestid" value="{{Request::segment(4)}}">
	        	<div class="col-md-12">
	        		<div class="form-group">
					    <label for="item_name">Duty/Customs Clearing ($):</label>
					    <input type="text" value="{{$info->DutyAndCustom}}" class="form-control" id="edit_customsduty_input" name="edit_customsduty_input" required=""/>
					    @if($errors->has('edit_customsduty_input'))
                            <p class="controll-error help-block"><i class="fa fa-times-circle-o"></i>&nbsp;
                                {{ $errors->first('edit_customsduty_input') }}
                            </p>
                        @endif
					</div>

	        	</div>
	        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" id="review_id"  >Submit</button>
      </div>
      </form>
    </div>

  </div>
</div>
<!-- Model end -->
 

<!-- Modal -->
<div id="edit_item" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" id="edit_item_close">&times;</button>
        <h4 class="modal-title">Edit Item</h4>
      </div>
      <form id="item_purchased_form">
      	<div class="modal-body">
	        <div class="row">
	        	<div class="col-md-12">
	        		<div class="form-group">
					    <label for="item_name">Item Name:</label>
					    <input type="text" class="form-control" id="item_name" name="item_name">
					    <div class="red" id="er_item_name"></div>
					</div>
	        	</div>
	        </div>
	        <div class="row">
	        	<div class="col-md-6">
	        		<div class="form-group">
					    <label for="item_name">Purchased From:</label>
					    <!-- <select class="form-control" id="purchase_from" name="purchase_from">
					    	<option value="">Select Market</option>
					    	<option value='Amazon'>Amazon</option>
					    	<option value='eBay'>eBay</option>
					    	<option value='Walmart'>Walmart</option>
					    	<option value='FlipKart'>FlipKart</option>
					    	<option value='Snapdeal'>Snapdeal</option>


					    </select> -->
					    <input type="text" class="form-control" id="purchase_from" name="purchase_from" placeholder="Purchased From">
					    <div class="red" id="er_purchase_from"></div>
					</div>
	        	</div>

	        	<div class="col-md-6">
	        		<div class="form-group">
					    <label for="item_name">Shipping:
					    	<small>(Retailer to Aquantuo's facility)</small></label>
					    <input type="text" class="form-control" id="shipping_cost_by_user" name="shipping_cost_by_user" placeholder="Shipping">
					    <div class="red" id="er_shipping_cost_by_user"></div>
					</div>
	        	</div>
	        	<div class="col-md-6">
	        		<div class="form-group">
					    <label for="item_name">Item Price:</label>
					    <input type="text" class="form-control" id="item_price" name="item_price">
					    <div class="red" id="er_item_price"></div>
					</div>
	        	</div>

	        	<div class="col-sm-6">
                  <div class="form-group">
                     <label class="control-label">Shipping Mode</label>
                     <div class="radio">
                        <label class="">
                        <input type="radio" name="travel_mode" id="travel_mode_air" value="air" onclick="toggle_category('.travel-mode-air','.travel-mode-ship','#package_category','edit_category')">
                        </label>&nbsp;By Air
                        <label class="">
                        <input type="radio" name="travel_mode" id="travel_mode_ship" value="ship" onclick="toggle_category('.travel-mode-ship','.travel-mode-air','#package_category','edit_category')">
                        </label>&nbsp;By Sea
                     </div>
                  </div>
               	</div>

	        	<div class="col-md-6">
	        		<div class="form-group">
					    <label class="control-label">Select Package Category</label>
                     <select class="form-control required" id="package_category" name="category">
                        <option value="">Select Category</option>
                        <?php
						foreach ($category as $key) {?>
                        <option value='{{$key->_id}}' class="travel-mode-{{$key->TravelMode}}">{{$key->Content}}</option>
                        <?php }?>
                     </select>
					</div>
	        	</div>
	        </div>

	        <div class="row">
	        	<div class="col-md-6">
	        		<h4>Measurement Units</h4>
	        		<div class="clearfix"></div>
	        		<input type="radio" name="measurement_units" id="measurement_units_cm_kg" value="cm_kg" checked>&nbsp;Metric(cm/kg)&nbsp;&nbsp;
	        		<input type="radio" name="measurement_units" id="measurement_units_inches_lbs" value="inches_lbs">&nbsp;Imperial(inches/lbs)
	        	</div>
	        	
	        	<div class="col-md-3" id="actual_count_div" style="display: none;">
	        		<h4> Actual Count </h4>
	        		<input type="text" class="form-control" id="actual_count" name="actual_count"/>
	        		<div class="red" id="er_actual_count"></div>
	        	</div>

	        	<div class="col-md-3">
	        		<h4>Quantity</h4>
	        		<input type="text" class="form-control" id="qty1" name="qty1">
	        		<div class="red" id="er_qty1"></div>
	        	</div>

	        	<div class="col-md-12"><h4>Item Specification</h4></div>
	        	<div class="col-md-6">
	        		<div class="clearfix"></div>
	        		<div class="form-group">
					    <label for="item_name">Length:</label>
					    <input type="text" class="form-control" id="item_length" name="item_length">
					    <div class="red" id="er_item_length"></div>
					</div>
	        	</div>

	        	<div class="col-md-6">
	        		<div class="form-group">
					    <label for="item_name">Width:</label>
					    <input type="text" class="form-control" id="item_width" name="item_width">
					    <div class="red" id="er_item_width"></div>
					</div>
	        	</div>

	        </div>

	        <div class="row">
	        	<div class="col-md-6">
	        		<div class="form-group">
					    <label for="item_name">Height:</label>
					    <input type="text" class="form-control" id="item_height" name="item_height">
					    <div class="red" id="er_item_height"></div>
					</div>
	        	</div>

	        	<div class="col-md-6">
	        		<div class="form-group">
					    <label for="item_name">Weight:</label>
					    <input type="text" class="form-control" id="item_weight" name="item_weight">
					    <div class="red" id="er_item_weight"></div>
					</div>
	        	</div>

	        	<div class="">
	        	<div class="col-md-6">
	        		<div class="form-group">
                        <div class="" >
                          	<a class="fancybox" rel="group" href="{{ImageUrl}}no-image.jpg" id="anchor">
								<img  src="{{ImageUrl}}no-image.jpg" width="200px" height="150px" id="product_image"/>
							</a>
                        </div>

                       	<br>

					    <label class="custom-input-file">
					    	<input type="file" name="item_image" class="custom-input-file" id="senior_image" onchange="image_preview(this,'online_purchase_preview',event)">

                        </label>

					</div>
	        	</div>
	        </div>
	        </div>
	    	<input type="hidden" name="itemid" id="itemid" >
	    	<input type="hidden" name="requestid" id="requestid" >
      </div>
      <div class="modal-footer">
		<input type="hidden" name="item_status" id="item_status"/>
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" id="item_sub_button">Submit</button>
      </div>
      </form>
    </div>

  </div>
</div>



<!-- Modal -->
<div id="old_item" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" id="edit_item_close">&times;</button>
        <h4 class="modal-title">Old Item Detail</h4>
      </div>
      	<div class="modal-body">
	        <div class="row">
	        	<div class="col-md-12">
	        		<div class="form-group">
					    <label for="item_name">Item Name:</label>
					    <span  id="item_name2" > </span>
					</div>
	        	</div>
	        </div>
	        <div class="row">
	        	<div class="col-md-12">
	        		<div class="form-group">
					    <label for="item_name">Purchased From:</label>
					    <span id="purchase_from2" ></span>

					</div>
	        	</div>

	        	<div class="col-md-12">
	        		<div class="form-group">
					    <label for="item_name">Shipping:
					    	<small>(Retailer to Aquantuo's facility):</small></label>
					    <span  id="shipping_cost_by_user2" > </span>
					</div>
	        	</div>
	        	<div class="col-md-6">
	        		<div class="form-group">
					    <label for="item_name">Item Price:</label>
					    <span id="item_price2"> </span>
					</div>
	        	</div>
	        	<div class="col-md-6">
	        		<label>Quantity:</label>
	        		<span id="qty12" > </span>
	        	</div>
	        </div>

	        <div class="row">
	        	<div class="col-md-6">
	        		<h4>Measurement Units</h4>
	        		<div class="clearfix"></div>
	        		<input type="radio" name="measurement_units" id="measurement_units_cm_kg2" value="cm_kg" checked>&nbsp;Metric(cm/kg)&nbsp;&nbsp;
	        		<input type="radio" name="measurement_units" id="measurement_units_inches_lbs2" value="inches_lbs">&nbsp;Imperial(inches/lbs)
	        	</div>



	        	<div class="col-md-12 text-primary" ><h4>Item Specification</h4></div>
	        	<div class="col-md-6">
	        		<div class="clearfix"></div>
	        		<div class="form-group">
					    <label for="item_name">Length:</label>
					    <span id="item_length2"> </span>&nbsp;<span id="lengthUnit2"> </span>
					</div>
	        	</div>

	        	<div class="col-md-6">
	        		<div class="form-group">
					    <label for="item_name">Width:</label>
					    <span id="item_width2"> </span>&nbsp;<span id="widthUnit2"> </span>
					</div>
	        	</div>
	        </div>

	        <div class="row">
	        	<div class="col-md-6">
	        		<div class="form-group">
					    <label for="item_name">Height:</label>
					    <span id="item_height2" > </span>&nbsp;<span id="heightUnit2"> </span>
					</div>
	        	</div>

	        	<div class="col-md-6">
	        		<div class="form-group">
					    <label for="item_name">Weight:</label>
					    <span id="item_weight2" > </span>&nbsp;<span id="weight_unit2"> </span>
					</div>
	        	</div>
	        </div>
      </div>
    </div>

  </div>
</div>




<!-- Modal for assign transporter -->
<div id="assign_tp" class="modal fade" role="dialog">
  <div class="modal-dialog modal-sm">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" id="assign_tp_close">&times;</button>
        <h4 class="modal-title">Shipment Departed</h4>
      </div>
      <form id="assing_tp_form">
      	<div class="modal-body">
	        <div class="row">
	        	<div class="col-md-12">
	        		<div class="form-group">
					    <label for="item_name">Transporter:</label>
					    <select class="form-control" id="tp_name" name="tp_name">
					    	<option value="">Select Transporter</option>
					    	@foreach($tplist as $key)
					    		<option value='{"id":"{{$key['_id']}}","name":"{{$key['Name']}}"}' >{{$key['Name']}}</option>
					    	@endforeach
					    </select>
					    <div class="red" id="er_tp_name"></div>
					</div>
	        	</div>
	        </div>

	    	<input type="hidden" name="product_id" id="tp_itemid" >
	    	<input type="hidden" name="request_id" id="tp_requestid" value="{{Request::segment(4)}}">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" id="assing_tp_btn" >Submit</button>
      </div>
      </form>
    </div>

  </div>
</div>

<!-- Out for Pickup All Modal -->
<div id="out_for_pickup_all_modal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"> Select Item Out for Pickup </h4>
			</div>
			{!! Form::model('', ['name' => 'out_for_pickup_all_form', 'method' => 'POST', 'url' => ['admin/out-for-pickup-all'], 'data-parsley-validate']) !!}
				<div class="modal-body">
					<div class="row">
						<input type="hidden" name="requestid" value="{{Request::segment(4)}}">
						<div class="col-md-12">
							<div class="form-group">
								<label for="item_name">Item:</label>
								<select class="form-control testselect3 required" data-parsley-required-message='Select at least one item.' id="tp_name" name="item_name[]" multiple="" required>
									@foreach($info->ProductList as $index)
										@if($index['status'] == 'assign')
											<option value="{{ $index['_id'] }}">
												{{ ucfirst($index['product_name']) }}
											</option>
										@endif
									@endforeach
								</select>
								<div class="red" id="er_tp_name"></div>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal"> Close </button>
					<button type="submit" class="btn btn-primary"> Submit </button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>

<!-- Out for Delivery All Modal -->
<div id="out_for_delivery_all_modal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"> Select Item Out for Delivery </h4>
			</div>
			{!! Form::model('', ['name' => 'out_for_delivery_all_form', 'method' => 'POST', 'url' => ['admin/out-for-delivery-all'], 'data-parsley-validate']) !!}
				<div class="modal-body">
					<div class="row">
						<input type="hidden" name="requestid" value="{{Request::segment(4)}}">
						<div class="col-md-12">
							<div class="form-group">
								<label for="item_name">Item:</label>
								<select class="form-control testselect3 required" data-parsley-required-message='Select at least one item.' id="tp_name" name="item_name[]" multiple="" required>
									@foreach($info->ProductList as $index)
										@if($index['status'] == 'out_for_pickup')
											<option value="{{ $index['_id'] }}">
												{{ ucfirst($index['product_name']) }}
											</option>
										@endif
									@endforeach
								</select>
								<div class="red" id="er_tp_name"></div>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal"> Close </button>
					<button type="submit" class="btn btn-primary"> Submit </button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>

<!-- Delivered All Modal -->
<div id="delivered_all_modal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"> Select Item for Delivered All </h4>
			</div>
			{!! Form::model('', ['name' => 'delivered_all_form', 'method' => 'POST', 'url' => ['admin/delivered-all'], 'data-parsley-validate']) !!}
				<div class="modal-body">
					<div class="row">
						<input type="hidden" name="requestid" value="{{Request::segment(4)}}">
						<div class="col-md-12">
							<div class="form-group">
								<label for="item_name">Item:</label>
								<select class="form-control testselect3 required" data-parsley-required-message='Select at least one item.' id="tp_name" name="item_name[]" multiple="" required>
									@foreach($info->ProductList as $index)
										@if($index['status'] == 'out_for_delivery')
											<option value="{{ $index['_id'] }}">
												{{ ucfirst($index['product_name']) }}
											</option>
										@endif
									@endforeach
								</select>
								<div class="red" id="er_tp_name"></div>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal"> Close </button>
					<button type="submit" class="btn btn-primary"> Submit </button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>

<!-- Modal for assign transporter -->
<div id="assign_tp_again" class="modal fade" role="dialog">
  <div class="modal-dialog modal-sm">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" id="assign_tp_close">&times;</button>
        <h4 class="modal-title">Shipment Departed</h4>
      </div>
      	 {!! Form::model('', ['name' => 'assignalltp','id' => 'assing_tp2_from', 'data-parsley-validate', 'method' => 'post', 'url'=> ['assign-all-tp/'.Request::segment(4)]]) !!}

      	<div class="modal-body">
	        <div class="row">

	        	<div class="col-md-12">
	        		<div class="form-group">
					    <label for="item_name">Item:</label>
					    <select class="form-control testselect3 required" data-parsley-required-message = 'Please select atlist one  item is required.' required multiple="" id="tp_name" name="item_name[]">
					   @foreach($info->ProductList as $index)
					   	@if($index['status'] == 'item_received')
					    <option value='{{$index['_id']}}' >{{ucfirst($index['product_name'])}}</option>
					    @endif
					   @endforeach
					    </select>
					    <div class="red" id="er_tp_name"></div>
					</div>
	        	</div>

                <div class="clearfix"> </div>
                <br/>
	        	<div class="col-md-12">
	        		<div class="form-group">
					    <label for="item_name">Transporter:</label>
					    <select class="form-control" id="tp_name" data-parsley-required-message = 'Please select atlist one Transporter is required.' required name="tp_name">
					    	<option value="">Select Transporter</option>
					    	@foreach($tplist as $key)
					    		<option value='{"id":"{{$key['_id']}}","name":"{{$key['Name']}}"}' >{{$key['Name']}}</option>
					    	@endforeach
					    </select>
					    <div class="red" id="er_tp_name"></div>
					</div>
	        	</div>
	        </div>

	    	<input type="hidden" name="product_id" id="123tp_itemid" >
	    	<input type="hidden" name="request_id" id="tp_requestid" value="{{Request::segment(4)}}">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" id="assing_tp2" >Submit</button>
      </div>
      {!! Form::close() !!}
    </div>

  </div>
</div>




<!--requester detail-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="close_modal"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">User Detail</h4>
      </div>

      <div class="modal-body my-modal-body">
	  	<div class="row">
          <div class="col-sm-4">
            <div id="profile_image">
               <p>Profile Image</p>
            </div>
            <script type="text/javascript">
               $(document).ready(function() {
                  $(".fancybox").fancybox();
               })
            </script>
          </div>
          <div class="col-sm-8" style="position: relative;text-align: center;vertical-align: middle;line-height: 90px;">
            <a href="{{ url('admin/user-comment-list',$info->RequesterId) }}" data-placement="top" title="View Notes/Comments" class="btn btn-small btn-primary btn-action"><i class="fa fa-th-list"></i></a>
            <a href="{{ url('admin/requester/edit',$info->RequesterId) }}" data-placement="top" title="Edit" class="btn btn-small btn-primary btn-action" style="margin-right: 10;"><i class="fa fa-edit"></i></a><br />
            <a href="#" data-toggle="modal" data-target="#myCommentModal{{$info->RequesterId}}" data-placement="top" title="Add Notes/Comments" class="btn btn-small btn-info"><i class="fa fa-plus"></i> Add Notes/Comments</a>
          </div>
        </div>
         <div class="table-responsive">
            <table class="table table-hover table-striped my-table">
               <tr>
                  <td>Name:</td>
                    <td><p id="name"></p></td>
                </tr>
               <tr>
                  <td>Email:</td>
                    <td><p id="email"></p></td>
                </tr>
                <tr>
                  <td>Phone Number:</td>
                    <td><p id="phone"></p></td>
                </tr>
                <tr>
                  <td>Alternate Phone Number:</td>
                    <td> <p id="phone2"></p></td>
                </tr>
                <tr>
                  <td>Address line-1:</td>
                    <td><p id="street1"></p></td>
                </tr>
                <tr>
                  <td>Address line-2:</td>
                    <td> <p id="street2"></p></td>
                </tr>
                <tr>
                  <td>Country:</td>
                    <td><p id="detail_country"></p></td>
                </tr>
                <tr>
                  <td>State:</td>
                    <td><p id="detail_state"></p></td>
                </tr>
                <tr>
                  <td>City:</td>
                    <td> <p id="detail_city"></p></td>
                </tr>
                 <tr>
                  <td>Zip Code:</td>
                    <td><p id="detail_zipcode"></p></td>
                </tr>
				<tr>
                	<td>Unit#:</td>
                	<td style="color: #f00;"> <p id="unitHash"></p> </td>
                </tr>
            </table>
      </div>
      <div id="no_user" style="display:none;"><center>User not found.</center></div>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
<!-- end -->

<div id="myCommentModal{{$info->RequesterId}}" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="exampleModalLabel">Add Comment</h4>
			</div>
			<div class="col-sm-12 ">
				{!! Form::model('', ['name' => 'commentForm', 'id' => 'commentForm', 'method' => 'POST', 'url' => ['admin/post-user-comment']]) !!}
			
					<div class="modal-body clearfix">
						<div class="form-group" id="cityDiv">
							{!! Form::label('Comment', 'Comment:',['class' => 'control-label']) !!}
							{!! Form::textarea('comment', '', ['class'=>'form-control', 'placeholder'=> 'Comment','id' =>'comment','rows'=>'3']) !!}
							<p class="help-block red" id="er_comment"></p>
						</div>
					</div>
					
					<input type="hidden" name="user_id" id="user_id" value="{{$info->RequesterId}} " />
					<input type="hidden" name="request_id" id="request_id" value="{{$info->RequesterId}}" />
					<input type="hidden" name="product_title" id="product_title" value="{{$info->RequesterName}}" />

					<div class="modal-footer">
						{!! Form::button('Close', ['class' => 'btn btn-primary', 'data-dismiss'=>'modal']) !!}
						{!! Form::submit('Add', ['class' => 'btn btn-primary']) !!}
					</div>

				{!! Form::close() !!}
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>

<!-- mark paid after item update -->
<div class="modal fade" id="paid_mark" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title" id="exampleModalLabel">Mark as Paid</h4>
	  </div>

	  {!! Form::model('', ['name' => 'paid_payment2', 'id' => 'paid_payment2', 'method' => 'POST', 'url' => ['admin/mark_paid_after_item_update']]) !!}
	  <input type="hidden" name="requestid" value="{{$info->_id}}" id="requestid">
	  <input type="hidden" name="request_type" value="buy_for_me" id="request_type">

	  <div class="modal-body">
			<div class="form-group" id="">
			<label>Description/Reason</label>
				<textarea class="form-control" name="description" placeholder="Description/Reason"></textarea>
				<p class="help-block" id="description" style="display: none;"></p>
			</div>

	  </div>
	  <div class="modal-footer">
		{!! Form::button('Close', ['class' => 'btn btn-primary', 'data-dismiss'=>'modal']) !!}
		{!! Form::submit('Submit', ['class' => 'btn btn-primary','onclick' => "return confirm('Are you sure? You want to mark as paid to this request!');"]) !!}
	  </div>
		<div class="clearfix"></div>
		{!! Form::close() !!}

	</div>
  </div>
</div>
<!-- end mark paid -->

<!-- Modal -->
 @foreach($info->ProductList as $index)
  <div class="modal fade" id="myModaldate{{ $index['_id'] }}" role="dialog">
    <div class="modal-dialog modal-sm">
      <!-- Modal content-->
     {!! Form::model('', ['name' => 'expected_date','method' => 'post', 'url'=> ['admin/expected-date'], 'data-parsley-validate']) !!}
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Expected Date</h4>
        </div>
        <input type="hidden" value="{{ Request::segment(4) }}" name="request_id">
        <input type="hidden" value="{{$index['_id'] }}" name="item_id">
        <input type="hidden" value="buy_for_me"  name="Requesttype">
        <div class="modal-body">
          <p>Expected Date :-</p>
          @if(isset($index['ExpectedDate']))
            {!! Form::text('Expected_date', show_date($index['ExpectedDate'],'M d, Y'), ['class'=>'form-control required DatePicker', 'placeholder'=> 'Expected date','id' => 'Expected_date']) !!}
           @endif
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary" >Submit</button>
        </div>
      </div>
         {!! Form::close() !!}

    </div>
  </div>
 @endforeach

<?php
function get_feet($InFeet, $unit) {
	$InFeet = (double) $InFeet;
	$unit = strtolower($unit);
	if ($unit == 'inch') {
		$InFeet = $InFeet * 0.0833333;
	} else if ($unit == 'meter') {
		$InFeet = $InFeet * 3.28084;
	} else if ($unit == 'centimeter' || $unit == 'centimetre') {
		$InFeet = $InFeet * 0.0328084;
	}
	return $InFeet;
}
?>
<script type="text/javascript">
   $(document).ready(function() {
   	$(".fancybox").fancybox();
   });

$('.DatePicker').datetimepicker({
			format:'M d, Y',
			scrollInput:false,
			timepicker:false,
		});
function get_user_info(id)
{
   var url = SITEURL+'ajax/get_user_info/'+id;
   $('#loading').show();
   $.ajax({
      type:"GET",
      url:url,
      //headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      success: function(res)
      {  $('#loading').hide();
         if(res.success == 0){
            $(".table-responsive").hide();
            $("#profile_image").html('');
            $("#no_user").show();

         }else{
            $(".table-responsive").show();
            $("#no_user").hide();
            $("#name").html(res.result.Name);
            $("#email").html(res.result.Email);
            $("#unitHash").html(res.result.UniqueNo);
            $("#phone").html(res.result.PhoneNo);
            $("#phone2").html(res.result.AlternatePhoneNo);
            $("#street1").html(res.result.Street1);
            $("#street2").html(res.result.Street2);
            $("#detail_country").html(res.result.Country);
            $("#detail_state").html(res.result.State);
            $("#detail_city").html(res.result.City);
            $("#detail_zipcode").html(res.result.ZipCode);
            if(res.result.Image == ''){
               $("#profile_image").html('<img class="thumbnail" src="{{ImageUrl}}/profile/noImageAvailable.jpg" height="100px" width="100px" alt="Profile image">');
            }else{
               $("#profile_image").html('<img class="thumbnail" src="{{ImageUrl}}/'+res.result.Image+'" height="100px" width="100px" alt="Profile image">');
            }

            //$("#detail_reg_date").html('{{ show_date('+res.result.EnterOn+') }}');
            //$('#detail_modal').trigger('click');
         }

         //$("#refund-data").html(msg);
      }
   });
}


function send_upddate_mail(itemid,requestid,item_name)
{
	//alert(itemid);
	//alert(requestid);
	//alert(item_name);
	$("#inform-"+itemid).addClass('spinning');

		$.ajax({
		url     : '{{url("admin/send-upddate-mail")}}',
		type    : 'post',
		data    : "request_id="+requestid+"&product_id="+itemid+"&product_name="+item_name,
		success : function(res){

			$("#inform-"+itemid).removeClass('spinning');
			if(res.success == 1)
			{
				alert('Mail has been sent successfully.');
				location.reload();
			}
			else
			{
				alert(res.msg);
			}
		}
	});
}


var requestid = '{{Request::segment(4)}}';
	function delivery_process(type,itemid,obj,msg)
	{
		if(confirm(msg) == true)
		{
			$(obj).addClass('spinning');

			$.ajax({
				url     : '{{url("admin/buy-for-me-process")}}',
				type    : 'post',
				data    : {type:type,itemid:itemid,requestid:requestid},
				dataType: 'json',
				success : function(res)
				{
					$(obj).removeClass('spinning');
					$("#accept_all_button").hide();
					alert(res.msg);
					if(res.success == 1)
					{
						location.reload();
					}
				}
			});
		}
		return false;
	}

function toggle_category(showid, hideid, id,state) {
	$("div."+state+" select").val("");
	$(id).val('');
	$(showid).attr('disabled', false);
	$(showid).show();
	$(hideid).attr('disabled', true);
	$(hideid).hide();
}

function item_purchased(itemid,requestid,obj)
{
	$('#itemid').val(itemid);
	$('#requestid').val(requestid);
	$(obj).addClass('spinning');
	$.ajax({
		url     : '{{url("admin/get_item_info")}}',
		type    : 'post',
		data    : {itemid:itemid,requestid:requestid},
		dataType: 'json',
		success : function(res)
		{
			$(obj).removeClass('spinning');
			if(res.success == 1)
			{
				$('#item_name').val(res.ProductList.product_name);
				if(res.ProductList.marketname != '') {
					$('#purchase_from').val(res.ProductList.marketname);
				}
				$('#purchase_from').val(res.ProductList.url);
				$('#shipping_cost_by_user').val(res.ProductList.shipping_cost_by_user);
				$('#item_price').val(res.ProductList.price);
				$('#item_length').val(res.ProductList.length);
				$('#item_width').val(res.ProductList.width);
				$('#item_height').val(res.ProductList.height);
				$('#item_weight').val(res.ProductList.weight);
				$('#qty1').val(res.ProductList.qty);
				$('#item_status').val(res.ProductList.status);
				
				if (res.ProductList.status == 'item_received') {
					$('#actual_count_div').show();
					if (res.ProductList.actual_count) {
						$('#actual_count').val(res.ProductList.actual_count);
					} else {
						$('#actual_count').val(0);
					}
				}
				
				var path = "{{ImageUrl}}"+res.ProductList.image;
				if(res.ProductList.image != '' && res.ProductList.image != null){
					$("#product_image").attr('src', path);
					$("#anchor").attr("href", path);
				}
				if(res.ProductList.heightUnit == 'cm') {
					$('#measurement_units_cm_kg').attr('checked','checked');
				} else {
					$('#measurement_units_inches_lbs').attr('checked','checked');
				}

				if(res.ProductList.travelMode == 'air') {
					document.getElementById('travel_mode_air').checked='checked';
			      toggle_category('.travel-mode-air','.travel-mode-ship','#package_category');
			    } else {
			    	document.getElementById('travel_mode_ship').checked='checked';
			      toggle_category('.travel-mode-ship','.travel-mode-air','#package_category');
			    }

			    $('#package_category').val(res.ProductList.categoryid);
			}
			else
			{
				alert(res.msg);
			}
		}
	});
}

/* old item detail */

function old_item(itemid,requestid,obj)
{
	$('#itemid').val(itemid);
	$('#requestid').val(requestid);
	$(obj).addClass('spinning');
	$.ajax({
		url     : '{{url("admin/get_old_item")}}',
		type    : 'post',
		data    : {itemid:itemid,requestid:requestid},
		dataType: 'json',
		success : function(res)
		{
			$(obj).removeClass('spinning');
			if(res.success == 1)
			{

				$('#item_name2').html(res.ProductList.product_name);
				if(res.ProductList.marketname != '') {
					$('#purchase_from2').html(res.ProductList.marketname);
				}
				$('#shipping_cost_by_user2').html(res.ProductList.shipping_cost_by_user);
				$('#item_price2').html(res.ProductList.price);
				$('#item_length2').html((res.ProductList.length == '')? 0.0 :res.ProductList.length);
				$('#lengthUnit2').html(res.ProductList.lengthUnit);
				$('#item_width2').html((res.ProductList.width == '')? 0.0 :res.ProductList.width);
				$('#widthUnit2').html(res.ProductList.widthUnit);
				$('#item_height2').html((res.ProductList.height == '')? 0.0 :res.ProductList.height);
				$('#heightUnit2').html(res.ProductList.heightUnit);
				$('#item_weight2').html((res.ProductList.weight == '')? 0.0 :res.ProductList.weight);
				$('#weight_unit2').html(res.ProductList.weight_unit);
				$('#qty12').html(res.ProductList.qty);
				if(res.ProductList.heightUnit == 'cm') {
					$('#measurement_units_cm_kg2').attr('checked','checked');
				} else {
					$('#measurement_units_inches_lbs2').attr('checked','checked');
				}
			}
			else
			{
				alert(res.msg);
			}
		}
	});
}
/* end old item detail */
$('#item_purchased_form').submit(function()
{
	var flag = true;

	if(valid.required('item_name','item name') == false) { flag = false; }
	if(valid.required('item_price','item price') == false) { flag = false; } else {
		if(valid.float('item_price','item price') == false) { flag = false; }
	}
	if(valid.required('item_length','item length') == false) { flag = false; } else {
		if(valid.float('item_length','item length') == false) { flag = false; }
	}
	if(valid.required('item_width','item width') == false) { flag = false; } else {
		if(valid.float('item_width','item width') == false) { flag = false; }
	}
	if(valid.required('item_height','item height') == false) { flag = false; } else {
		if(valid.float('item_height','item height') == false) { flag = false; }
	}
	if(valid.required('item_weight','item weight') == false) { flag = false; } else {
		if(valid.float('item_weight','item weight') == false) { flag = false; }
	}
	if(valid.required('shipping_cost_by_user','shipping') == false) { flag = false; } else {
		if(valid.float('shipping_cost_by_user','shipping') == false) { flag = false; }
	}

	if(valid.required('qty1','quantity') == false) { flag = false; }else {
		if(valid.numeric('qty1','quantity') == false) { flag = false; }
	}
	
	if ($('#item_status').val() == 'item_received') {
		if(valid.required('actual_count','actual count') == false) { flag = false; }else {
			if(valid.numeric('actual_count','actual count') == false) { flag = false; }
		}
	}

	if(flag)
	{

		$('#item_sub_button').addClass('spinning');

		$.ajax({
		url     : '{{url("admin/update_item_info")}}',
		type    : 'post',
		data    : $('#item_purchased_form').serialize(),
		dataType: 'json',
		success : function(res)
		{
			console.log(res);
			$('#item_sub_button').removeClass('spinning');
			if(res.success == 1)
			{
				$('#item_purchased_form')[0].reset();
				$('#edit_item_close').trigger('click');
				$('#item_update-'+$('#itemid').val()).hide();
				if(res.reminder_button == true) {
					$('#reminder_button').show();
				}
				if(res.purchase_button == true) {
					$('.purchase_button').show();
				} else {
					$('#waiting-for-payment-'+$('#itemid').val()).show();
				}
				alert(res.msg);
				location.reload();
				/*if(res.assing_tp_button == true) {
					$('.assign-btn').show();
				}*/

			}
			alert(res.msg);
		}
	});
	}
	return false;
});

$('#assing_tp_form').submit(function(){

	var flag = true;

	if(valid.required('tp_name','transprter') == false) { flag = false; }

	if(flag)
	{

		$('#assing_tp_btn').addClass('spinning');

		$.ajax({
		url     : '{{url("admin/buy_for_me_assign_transporter")}}',
		type    : 'post',
		data    : $('#assing_tp_form').serialize(),
		dataType: 'json',
		success : function(res)
		{
			$('#assing_tp_btn').removeClass('spinning');
			if(res.success == 1)
			{
				$('#assing_tp_form')[0].reset();
				$('#assign_tp_close').trigger('click');

				$('#assign-'+$('#tp_itemid').val()).hide();
				alert(res.msg);
				location.reload();
			}
			alert(res.msg);
		}
	});
	}
	return false;

});


function status_review(prid)
{
	$("#reviewButton"+prid).addClass('spinning');
	$.ajax({
		url     : '{{url("admin/online-package-review")}}',
		type    : 'post',
		data    : "request_id={{Request::segment(4)}}"+"&product_id="+prid,
		success : function(res){

			$("#reviewButton"+prid).removeClass('spinning');
			var obj = eval("("+res+")");
			if(obj.success == 1)
			{
				$("#item_update-"+prid).show();
				$("#reviewButton"+prid).hide();
				$("#updated-"+prid).show();
				alert(obj.msg);
				location.reload();
			}
			alert(obj.msg);
		}
	});
}
  $('.testselect3').SumoSelect();


  $('#review_from').parsley().subscribe('parsley:form:validated', function (parsleyForm) {
  if (true === parsleyForm.validationResult ){
      $('#review_id').addClass('spinning');
    }

  else{
      $('#review_id').removeClass('spinning');
  }
});

     $('#assing_tp2_from').parsley().subscribe('parsley:form:validated', function (parsleyForm) {
  if (true === parsleyForm.validationResult ){
      $('#assing_tp2').addClass('spinning');
    }
  else{
      $('#assing_tp2').removeClass('spinning');
  }
});

  $('#purchase_select_from').parsley().subscribe('parsley:form:validated', function (parsleyForm) {
  if (true === parsleyForm.validationResult ){
      $('#purchase_id').addClass('spinning');
    }
  else{
      $('#purchase_id').removeClass('spinning');
  }
});

  $('#cancel_from').parsley().subscribe('parsley:form:validated', function (parsleyForm) {
  if (true === parsleyForm.validationResult ){
      $('#cancel_id').addClass('spinning');
    }
  else{
      $('#cancel_id').removeClass('spinning');
  }
});

    $('#received_from').parsley().subscribe('parsley:form:validated', function (parsleyForm) {
  if (true === parsleyForm.validationResult ){
      $('#received_id').addClass('spinning');
    }
  else{
      $('#received_id').removeClass('spinning');
  }
});

</script>
@include('Admin::layouts.footer')

@stop
<style>
   .modal-header .close {
   margin-top: -9px !important;
   }

   .custom-table.table th {
    border-bottom: medium none !important;
    vertical-align: middle;
    height: 60px!important;
    background: #F1F1F1;
    /* text-align:center;*/
}
.custom-table.table > tbody td {
    /*text-align:center !important;*/

    vertical-align: middle;
}

.table-bordered td,.table-bordered th{border:1px solid #ddd!important}

.ratingAdjustmentcss {
background-color: #8d887a;
float: left;
margin-top: 3px !important;
width: 65px;
}

.graph {
background-color: #ecb90d;
height: 14px;
}

</style>

