@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')
@section('content')
{!! Html::script('theme/admin/fancybox/source/jquery.fancybox.js') !!}
{!! Html::style('theme/admin/fancybox/source/jquery.fancybox.css') !!}
<div class="container-fluid">
<!--
	@if(Session::has('success'))
		<div class="alert alert-success alert-dismissable text-left">
			<button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
			<h4><i class="icon fa fa-check"></i>Success! </h4>{{Session::get('success')}}
		</div>
	@endif
	@if(Session::has('fail'))
		<div class="alert alert-danger alert-dismissable text-left">
			<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
				<h4><i class="icon fa fa-check"></i>Fail! </h4>{{Session::get('fail')}}
		</div>
	@endif
-->
	<div id="msg_div" class=""></div>
   <div class="panel panel-default">
      <div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;Package Detail1
         <a style="margin-right:5px;" class="btn btn-primary margin-top-less-7 pull-right" href="{{url('admin/package')}}">Go Back</a>
      </div>
      <div class="panel-body">
         <div class="col-xs-12 admin-list">
            <h3>Package Detail</h3>
            <div class="row">
               <div class="col-sm-4 custom-table1">
                  <div class="row">
                     <div class="col-sm-5"><b>Package Id</b>:</div>
                     <div class="col-sm-7">{{ucfirst($info->PackageNumber)}}</div>
                  </div>
               </div>
               <div class="col-sm-4 custom-table1">
                  <div class="row">
                     <div class="col-sm-5"><b>Package Title</b>:</div>
                     <div class="col-sm-7">{{ucfirst($info->ProductTitle)}}</div>
                  </div>
               </div>
               <div class="col-sm-4 custom-table1">
                  <div class="row">
                     <div class="col-sm-5"><b>Status</b>:</div>
                     <div class="col-sm-7">
                        {{ get_status_title($info->Status)['status'] }}
                     </div>
                  </div>
               </div>
               <div class="col-sm-4 custom-table1">
                  <div class="row">
                     <div class="col-sm-5"><b>Requester Name</b>:</div>
                     <div class="col-sm-7">
                     <a data-toggle="modal" href="#myModal" onclick="return get_user_info('{{$info->RequesterId}}')">{{ucfirst($info->RequesterName)}}</a>

                     </div>
                  </div>
               </div>
               <div class="col-sm-4 custom-table1">
                  <div class="row">
                     <div class="col-sm-5"><b>Width</b>:</div>
                     <div class="col-sm-7">{{$info->ProductWidth}} {{$info->ProductWidthUnit}}</div>
                  </div>
               </div>
               <div class="col-sm-4 custom-table1">
                  <div class="row">
                     <div class="col-sm-5"><b>Transporter Name</b>:</div>
                     <div class="col-sm-7">{{$info->TransporterName or '--'}}</div>
                  </div>
               </div>
               <div class="col-sm-4 custom-table1">
                  <div class="row">
                     <div class="col-sm-5"><b>Length</b>:</div>
                     <div class="col-sm-7">{{$info->ProductLength}} {{$info->ProductLengthUnit}}</div>
                  </div>
               </div>
               <div class="col-sm-4 custom-table1">
                  <div class="row">
                     <div class="col-sm-5"><b>Package Value</b>:</div>
                     <div class="col-sm-7">{{ '$'}}{{number_format($info->ProductCost,2)}}</div>
                  </div>
               </div>
               <div class="col-sm-4 custom-table1">
                  <div class="row">
                     <div class="col-sm-5"><b>Weight</b>:</div>
                     <div class="col-sm-7">{{$info->ProductWeight}} {{$info->ProductWeightUnit}}</div>
                  </div>
               </div>
               <div class="col-sm-4 custom-table1">
                  <div class="row">
                     <div class="col-sm-5"><b>Height</b>:</div>
                     <div class="col-sm-7 ucfirst">{{$info->ProductHeight}} {{$info->ProductHeightUnit}}</div>
                  </div>
               </div>
               <div class="col-sm-4 custom-table1">
                  <div class="row">
                     <div class="col-sm-5"><b>Distance</b>:</div>
                     <div class="col-sm-7">{{number_format($info->Distance,2)}} miles</div>
                  </div>
               </div>
               <div class="col-sm-4 custom-table1">
                  <div class="row">
                     <div class="col-sm-5"><b>Volume</b>:</div>
                     <div class="col-sm-7"><?php echo number_format(($info->ProductHeight * $info->ProductLength * $info->ProductWidth), 2); ?> Cu. {{ucfirst($info->ProductWidthUnit)}}</div>
                  </div>
               </div>
               <div class="col-sm-4 custom-table1">
                  <div class="row">
                     <div class="col-sm-5"><b>Pickup Date</b>:</div>
                     <div class="col-sm-7">
                        {{ show_date(@$info->PickupDate) }}
                     </div>
                  </div>
               </div>
               <div class="col-sm-4 custom-table1">
                  <div class="row">
                     <div class="col-sm-5"><b>Journey Type</b>:</div>
                     <div class="col-sm-7" id="from">
                        {{ ucfirst(str_replace('_',' ',$info->JournyType))}}
                     </div>
                  </div>
               </div>

               <div class="col-sm-4 custom-table1">
                  <div class="row">
                     <div class="col-sm-5"><b>Public Place</b>:</div>
                     <div class="col-sm-7" id="from">{{ ucfirst($info->PublicPlace)}}</div>
                  </div>
               </div>

               <div class="col-sm-4 custom-table1">
                  <div class="row">
                     <div class="col-sm-5"><b>Drop Date</b>:</div>
                     <div class="col-sm-7">
                      {{ show_date(@$info->DeliveryDate) }}
                     </div>
                  </div>
               </div>

               <div class="col-sm-4 custom-table1">
                  <div class="row">
                     <div class="col-sm-5"><b>Shipping Mode</b>:</div>
                     <div class="col-sm-7" id="from">{{ ucfirst($info->TravelMode)}}</div>
                  </div>
               </div>

               <div class="col-sm-4 custom-table1">
                  <div class="row">
                     <div class="col-sm-5"><b>Created Date</b>:</div>
                     <div class="col-sm-7" id="from">{{ show_date(@$info->EnterOn) }}</div>
                  </div>
               </div>

               <div class="col-sm-4 custom-table1">
                  <div class="row">
                     <div class="col-sm-5"><b>Device Type</b>:</div>
                     <div class="col-sm-7" id="from">{{ucfirst(@$info->device_type) }}</div>
                  </div>
               </div>

               <div class="col-sm-4 custom-table1">
                  <div class="row">
                     <div class="col-sm-5"><b> @if(@$info->device_type == 'website')Browser @else Device Version @endif</b>:</div>
                     <div class="col-sm-7" id="from">{{ @$info->device_version}}</div>
                  </div>
               </div>

               <div class="col-sm-4 custom-table1">
                  <div class="row">
                     <div class="col-sm-5"><b>
                        @if(@$info->device_type == 'website')
                           Version
                        @else
                           App Version
                        @endif

                     </b>:</div>
                     <div class="col-sm-7" id="from">{{ @$info->app_version}}</div>
                  </div>
               </div>


            </div>
         </div>
         <div class="col-xs-12 admin-list">
            <h3>Other Detail</h3>
            <div class="row">
               <div class="col-sm-4 custom-table1">
                  <div class="row">
                     <div class="col-sm-5"><b>Receiver Mobile No</b>:</div>
                     <div class="col-sm-7">
                        {{$info->ReceiverMobileNo}}
                     </div>
                  </div>
               </div>
               <div class="col-sm-4 custom-table1">
                  <div class="row">
                     <div class="col-sm-5"><b>Insurance</b>:</div>
                     <div class="col-sm-7">
                        <?php if ($info->InsuranceStatus == "yes") {
	echo "Yes";
} else {
	echo "No";
}?>
                     </div>
                  </div>
               </div>
               <div class="col-sm-4 custom-table1">
                  <div class="row">
                     <div class="col-sm-5"><b>Description</b>:</div>
                     <div class="col-sm-7">
                        {{ucfirst($info->Description)}}
                     </div>
                  </div>
               </div>
               <div class="col-sm-4 custom-table1">
                  <div class="row">
                     <div class="col-sm-5"><b>Quatity</b>:</div>
                     <div class="col-sm-7" id="from">{{ ucfirst($info->QuantityStatus)}}</div>
                  </div>
               </div>
               <div class="col-sm-4 custom-table1">
                  <div class="row">
                     <div class="col-sm-5">
                        <b>Quatity Value</b> :
                     </div>
                     <div class="col-sm-7" id="from">
                        {{ ucfirst($info->BoxQuantity)}}
                     </div>
                  </div>
               </div>
               <div class="col-sm-4 custom-table1">
                  <div class="row">
                     <div class="col-sm-5"><b>Is Delivery Date Flexible</b>:</div>
                     <div class="col-sm-7" id="from">
                        {{ ucfirst($info->FlexibleDeliveryDate) }}
                     </div>
                  </div>
               </div>
               <div class="col-sm-4 custom-table1">
                  <div class="row">
                     <div class="col-sm-5"><b>Package Material</b>:</div>
                     <div class="col-sm-7">
                        {{ ucfirst($info->PackageMaterial)}}
                     </div>
                  </div>
               </div>
               <div class="col-sm-4 custom-table1">
                  <div class="row">
                     <div class="col-sm-5">
                        <b>Package Category</b> :
                     </div>
                     <div class="col-sm-7" id="from">
                        {{ ucfirst($info->Category)}}
                     </div>
                  </div>
               </div>
               <div class="col-sm-4 custom-table1">
                  <div class="row">
                     <div class="col-sm-5"><b>Verification Code </b>:</div>
                     <div class="col-sm-7">{{$info->DeliveryVerifyCode}}
                     </div>
                  </div>
               </div>

               <div class="col-sm-4 custom-table1">
                  <div class="row">
                     <div class="col-sm-5"><b>Package Care Note </b>:</div>
                     <div class="col-sm-7" id="from">{{ucfirst($info->PackageCareNote)}}
                     </div>
                  </div>
               </div>

               @if($info->Status == 'delivered')
                <div class="col-sm-4 custom-table1">
                 <label>Complete by Admin: </label>
                  @if(isset($info->complete_by_admin))
                     @if($info->complete_by_admin == 'yes')
                     Yes
                     @endif
                  @else
                  No
                  @endif
                </div>
                @endif

               @if($info->marked_description)
               <div class="col-sm-12 custom-table1">
                  <label>Description</label>(Marked as Paid)<label>:</label>
                  {{ucfirst($info->marked_description)}}
               </div>
               @endif

            </div>
         </div>
         <div class="col-xs-12 admin-list">
            <h3>Delivery Address</h3>
            <div class="row">
               <div class="col-sm-3 custom-table1">
                  <div><b>Pickup Address</b>:</div>
                  <div id="from">{{ucfirst($info->PickupFullAddress)}}
                  </div>
               </div>
               <div class="col-sm-3 custom-table1">
                  <div><b>Drop Address</b>:</div>
                  <div id="to">
                     {{ucfirst($info->DeliveryFullAddress)}}
                  </div>
               </div>
               <div class="col-sm-3 custom-table1">
                  <div><b>Return Address</b>:</div>
                  <div id="from">
                     {{ucfirst($info->ReturnFullAddress)}}
                  </div>
               </div>
               <div class="col-sm-3 custom-table1">
                  <div><b>Return Address (In Case Not Delivered) </b>:</div>
                  <div id="from">{{ucfirst($info->NotDelReturnFullAddress)}}
                  </div>
               </div>
            </div>
         </div>
         <div class="clearfix"></div>
      </div>
      <!-- Panel Body -->
		</div>



   <?php if (@$info->Penalty > 0): ?>
   <div class="row">
      <div class="col-sm-12">
         <div class="panel panel-default">
            <div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;Penalty</div>
            <div class="panel-body">
               <div class="col-md-6 col-sm-6 custom-table1 row">
                  <div class="row">
                     <div class="col-md-6 col-sm-6"><b>Penalty Status</b> :</div>
                     <div class="col-md-4 col-sm-4"> {{ucfirst($info->PenaltyStatus)}}</div>
                  </div>
               </div>
               <div class="col-md-6 col-sm-6 custom-table1 row">
                  <div class="row">
                     <div class="col-md-6 col-sm-6 text-right"><b>Penalty Status</b> :</div>
                     <div class="col-md-4 col-sm-4 text-right"> ${{number_format(($info->Penalty/100),2)}}</div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <?php endif;?>
   <div class="row">
      <div class="col-sm-6">
         <div class="panel panel-default">
            <div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;Payment Information</div>
            <div class="panel-body height">
               <div class="col-md-6 col-sm-6 custom-table1 row">
                  <div class="row">
                     <div class="col-md-6 col-sm-6"><b>Payment Status</b> :</div>
                     <div class="col-md-4 col-sm-4"> {{ucfirst($info->PaymentStatus)}}</div>
                  </div>
               </div>
               <div class="col-md-6 col-sm-6 custom-table1 row">
                  <div class="row">
                     <div class="col-md-8 col-sm-6 text-right"><b>Shipping Cost</b> :</div>
                     <div class="col-md-4 col-sm-4 text-right"> ${{number_format($info->ShippingCost,2)}}</div>
                  </div>
                  <div class="row">
                     <div class="col-md-8 col-sm-6 text-right"><b>Insurance Cost</b> :</div>
                     <div class="col-md-4 col-sm-4 text-right">${{number_format($info->InsuranceCost,2)}}</div>
                  </div>
                  <div class="row">
                     <div class="col-md-8 col-sm-6 text-right"><b>Total</b> :</div>
                     <div class="col-md-4 col-sm-4 text-right"> ${{number_format($info->TotalCost,2)}}</div>
                  </div>
                  <div class="row">
                  <div class="col-md-8 col-sm-6 text-right"><b>Aquantuo Discount</b> :
                   @if(!$info->promocode == '')
                        <small>(<b>Promocode:</b>{{ $info->promocode }}) </small>
                        @endif
                  </div>

                  <div class="col-md-4 col-sm-4 text-right"> ${{number_format($info->discount,2)}}</div>
               </div>
               </div>
               <div class="clearfix"></div>
            </div>
            <!-- Panel Body -->
         </div>
      </div>
      <div class="col-sm-6">
         <div class="panel panel-default">
            <div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;Feedback</div>
            <div class="panel-body">
               <div class="col-md-12 col-sm-12 custom-table1 row">
               <div class="col-md-4 col-sm-4">
                     <b>Requester Rating:</b>
                  </div>
                  <div class="col-md-8 col-sm-8">
                     <div class='rating_bar'>
                        <div  class='rating' style='width:{{@$info->RequesterRating * 20}}%'></div>
                     </div>
                  </div>
               </div>
               <div class="col-md-12 col-sm-12 custom-table1 row">
                  <div class="col-md-4 col-sm-4">
                     <b>Requester Comment:</b>
                  </div>

                  <div class="col-md-8 col-sm-8">
                     {{@$info->RequesterFeedbcak}}
                  </div>
               </div>
               <div class="col-md-12 col-sm-12 custom-table1 row">
                  <div class="col-md-4 col-sm-4">
                     <b>Transporter Rating:</b>
                  </div>
                  <div class="col-md-8 col-sm-8">
                     <div class='rating_bar'>
                        <div  class='rating' style='width:{{@$info->TransporterRating * 20}}%'></div>
                     </div>
                  </div>
               </div>
               <div class="clearfix"></div>
            </div>
            <!-- Panel Body -->
         </div>
      </div>
      <?php if ($info['ProductImage'] != "") {
	?>
      <div class="col-sm-12">
         <div class="panel panel-default">
            <div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;Package Image</div>
            <div class="panel-body">
               <?php

	if ($info['ProductImage'] != '') {
		if (file_exists(BASEURL_FILE . $info['ProductImage'])) {?>
               <div class="col-md-2">
                  <a class="fancybox" href="{{ImageUrl}}{{ $info->ProductImage}}" data-fancybox-group="gallery" title="Product Image">
                  <img src="<?php echo ImageUrl . $info['ProductImage']; ?>" tag="product image" style="width:178px; height:178px;"/></a>
               </div>
               <?php }
	}
	if (is_array($info["OtherImage"])) {

		foreach ($info['OtherImage'] as $key) {
			if ($key != '') {
				if (file_exists(BASEURL_FILE . $key)) {?>
               <div class="col-md-2">
                  <a class="fancybox" href="{{ImageUrl}}{{$key}}" data-fancybox-group="gallery" title="Product Image">
                  <img src="<?php echo ImageUrl . $key; ?>" tag="product image" style="width:178px; height:178px;"/></a>
               </div>
               <?php }
			}
		}

	}?>
               <div class="clearfix"></div>
            </div>
            <!-- Panel Body -->
         </div>
      </div>
      <?php }?>
   </div>

   <div class="">
      <div class="panel panel-default">
         <div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;Track Location</div>
         <div class="panel-body">
            <div id="map" style="height: 400px;">
            </div>
            <div class="clearfix"></div>
         </div>
         <!-- Panel Body -->
      </div>
   </div>
   <!--{!! HTML::script('theme/admin/custome/js/map_api.js') !!}-->
   <!-- {!! HTML::script('theme/admin/custome/js/jquery-1.8.0.min.js') !!}-->
   <!-- {!! HTML::script('theme/admin/custome/js/map-api.js') !!} -->
   <script>
      function calculateAndDisplayRoute(directionsService, directionsDisplay,map) {
         //alert((document.getElementById('to').innerHTML).trim());
         //alert((document.getElementById('from').innerHTML).trim());
        directionsService.route({
          origin: new google.maps.LatLng(parseFloat('{{$info->PickupLatLong[1]}}'), parseFloat('{{$info->PickupLatLong[0]}}')),//'<?php echo $info->PickupFullAddress; ?>',
          destination: new google.maps.LatLng(parseFloat('{{$info->DeliveryLatLong[1]}}'), parseFloat('{{$info->DeliveryLatLong[0]}}')),
          travelMode: google.maps.TravelMode.DRIVING
        }, function(response, status) {
          if (status === google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
          } else {

               new google.maps.Marker({
                  position: new google.maps.LatLng(parseFloat('{{$info->DeliveryLatLong[1]}}'), parseFloat('{{$info->DeliveryLatLong[0]}}')),
                  map: map,
                  title: 'Drop Location'
               });
               new google.maps.Marker({
                  position: new google.maps.LatLng(parseFloat('{{$info->PickupLatLong[1]}}'), parseFloat('{{$info->PickupLatLong[0]}}')),
                  map: map,
                  title: 'Pickup Location'
               });

              new google.maps.Polyline({
                  path: [
                           new google.maps.LatLng(parseFloat('{{$info->DeliveryLatLong[1]}}'), parseFloat('{{$info->DeliveryLatLong[0]}}')),
                           new google.maps.LatLng(parseFloat('{{$info->PickupLatLong[1]}}'), parseFloat('{{$info->PickupLatLong[0]}}'))
                        ],
                  strokeColor: "#FF0000",
                  strokeOpacity: 1.0,
                  strokeWeight: 2
              }).setMap(map);

          }
        });
      }

      function initMap() {
        var directionsService = new google.maps.DirectionsService;

        var directionsDisplay = new google.maps.DirectionsRenderer({ polylineOptions: { strokeColor: "#FF0000" } });
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 7,
          center: {lat: parseFloat('{{$info->PickupLatLong[1]}}'), lng: parseFloat('{{$info->PickupLatLong[0]}}')}
        });
        if($("#pstatus").val() == "to_pickup" || $("#pstatus").val() == "on_delivery" ){
      	var mlat = {{ @$carrierLatitude }};
      	var mlng = {{ @$carrierLongnitude }};

      	var marker = new google.maps.Marker({
      		position: {lat:mlat, lng:mlng},
      		map: map,
      		title: 'Carrier Location'
      	});
        }



        directionsDisplay.setMap(map);
        calculateAndDisplayRoute(directionsService, directionsDisplay,map);

      }

   </script>
   <script src="https://maps.googleapis.com/maps/api/js?&signed_in=true&callback=initMap" async defer></script>

   <!--  Modal content for the mixer image example -->
   <div class="modal fade pop-up-map" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel-1" aria-hidden="true">
      <div class="modal-dialog modal-lg">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
               <div id="map1"></div>
               <p id="error"></p>
            </div>
         </div>
         <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
   </div>
   <!-- /.modal mixer image -->
</div>

<!--requester detail-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="close_modal"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">User Detail</h4>
      </div>

      <div class="modal-body my-modal-body">
         <div class="row">
          <div class="col-sm-4">
            <div id="profile_image">
               <p>Profile Image</p>
            </div>
            <script type="text/javascript">
               $(document).ready(function() {
                  $(".fancybox").fancybox();
               })
            </script>
          </div>
          <div class="col-sm-8" style="position: relative;text-align: center;vertical-align: middle;line-height: 90px;">
            <a href="{{ url('admin/user-comment-list',$userinfo->_id) }}" data-placement="top" title="View Notes/Comments" class="btn btn-small btn-primary btn-action"><i class="fa fa-th-list"></i></a>
            <a href="{{ url('admin/requester/edit',$userinfo->_id) }}" data-placement="top" title="Edit" class="btn btn-small btn-primary btn-action" style="margin-right: 10;"><i class="fa fa-edit"></i></a><br />
            <a href="#" data-toggle="modal" data-target="#myCommentModal{{$userinfo->_id}}" data-placement="top" title="Add Notes/Comments" class="btn btn-small btn-info"><i class="fa fa-plus"></i> Add Notes/Comments</a>
          </div>
        </div>



         <div class="table-responsive">
            <table class="table table-hover table-striped my-table">
               <tr>
                  <td>Name:</td>
                    <td><p id="name"></p></td>
                </tr>
               <tr>
                  <td>Email:</td>
                    <td><p id="email"></p></td>
                </tr>
                <tr>
                  <td>Phone Number:</td>
                    <td><p id="phone"></p></td>
                </tr>
                <tr>
                  <td>Alternate Phone Number:</td>
                    <td> <p id="phone2"></p></td>
                </tr>
                <tr>
                  <td>Address line-1:</td>
                    <td><p id="street1"></p></td>
                </tr>
                <tr>
                  <td>Address line-2:</td>
                    <td> <p id="street2"></p></td>
                </tr>
                <tr>
                  <td>Country:</td>
                    <td><p id="detail_country"></p></td>
                </tr>
                <tr>
                  <td>State:</td>
                    <td><p id="detail_state"></p></td>
                </tr>
                <tr>
                  <td>City:</td>
                    <td> <p id="detail_city"></p></td>
                </tr>
                 <tr>
                  <td>Zip Code:</td>
                    <td><p id="detail_zipcode"></p></td>
                </tr>

            </table>
      </div>
      <div id="no_user" style="display:none;"><center>User not found.</center></div>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
<!-- end -->

<div id="myCommentModal{{$userinfo->_id}}" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="exampleModalLabel">Add Comment</h4>
			</div>
			<div class="col-sm-12 ">
				{!! Form::model('', ['name' => 'commentForm', 'id' => 'commentForm', 'method' => 'POST', 'url' => ['admin/post-user-comment']]) !!}
			
					<div class="modal-body clearfix">
						<div class="form-group" id="cityDiv">
							{!! Form::label('Comment', 'Comment:',['class' => 'control-label']) !!}
							{!! Form::textarea('comment', '', ['class'=>'form-control', 'placeholder'=> 'Comment','id' =>'comment','rows'=>'3']) !!}
							<p class="help-block red" id="er_comment"></p>
						</div>
					</div>
					
					<input type="hidden" name="user_id" id="user_id" value="{{$userinfo->_id}} " />
					<input type="hidden" name="request_id" id="request_id" value="{{$userinfo->_id}}" />
					<input type="hidden" name="product_title" id="product_title" value="{{$userinfo->Name}}" />

					<div class="modal-footer">
						{!! Form::button('Close', ['class' => 'btn btn-primary', 'data-dismiss'=>'modal']) !!}
						{!! Form::submit('Add', ['class' => 'btn btn-primary']) !!}
					</div>

				{!! Form::close() !!}
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>

<?php
function get_feet($InFeet, $unit) {
	$InFeet = (double) $InFeet;
	$unit = strtolower($unit);
	if ($unit == 'inch') {
		$InFeet = $InFeet * 0.0833333;
	} else if ($unit == 'meter') {
		$InFeet = $InFeet * 3.28084;
	} else if ($unit == 'centimeter' || $unit == 'centimetre') {
		$InFeet = $InFeet * 0.0328084;
	}
	return $InFeet;
}
?>
<script type="text/javascript">

function get_user_info(id)
{
   var url = SITEURL+'ajax/get_user_info/'+id;
   $('#loading').show();
   $.ajax({
      type:"GET",
      url:url,
      //headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      success: function(res)
      {  $('#loading').hide();
         if(res.success == 0){



            $(".table-responsive").hide();
            $("#profile_image").html('');
            $("#no_user").show();

         }else{
            $(".table-responsive").show();
            $("#no_user").hide();
            $("#name").html(res.result.Name);
            $("#email").html(res.result.Email);
            $("#phone").html(res.result.PhoneNo);
            $("#phone2").html(res.result.AlternatePhoneNo);
            $("#street1").html(res.result.Street1);
            $("#street2").html(res.result.Street2);
            $("#detail_country").html(res.result.Country);
            $("#detail_state").html(res.result.State);
            $("#detail_city").html(res.result.City);
            $("#detail_zipcode").html(res.result.ZipCode);
            if(res.result.Image == ''){
               $("#profile_image").html('<img class="thumbnail" src="{{ImageUrl}}/profile/noImageAvailable.jpg" height="100px" width="100px" alt="Profile image">');
            }else{
               $("#profile_image").html('<img class="thumbnail" src="{{ImageUrl}}/'+res.result.Image+'" height="100px" width="100px" alt="Profile image">');
            }

            //$("#detail_reg_date").html('{{ show_date('+res.result.EnterOn+') }}');
            //$('#detail_modal').trigger('click');


         }

         //$("#refund-data").html(msg);
      }
   });
}




   $(document).ready(function() {
   	$(".fancybox").fancybox();
   });


	function accept_package(prid)
	{
		$.ajax({
		url     : '{{url("accept-package")}}',
		type    : 'post',
		data    : "detail_id={{Request::segment(4)}}"+"&product_id="+prid,
		success : function(res){
		 $("#ratingButton").removeClass('spinning');
				var obj = eval("("+res+")");

		if(obj.success == 1)
		{

			alert('Request has been accepted successfully.');
			$("#acceptButton").hide();
			$("#cancelButton2").hide();
			$("#pickupButton").show();
		}
		else
			{}


		 }
		});
	}

	function cancel_package(prid)
	{

		$.ajax({
		url     : '{{url("cancel-request")}}',
		type    : 'post',
		data    : "request_id={{Request::segment(4)}}"+"&product_id="+prid,
		success : function(res){
			$("#ratingButton").removeClass('spinning');
			var obj = eval("("+res+")");

			if(obj.success == 1)
			{

				alert('Request has been canceled successfully.');
				$("#cancelButton").hide();
			}
			else
				{}


		 }
		});
	}

	function pickup(prid)
	{
		$("#accept_all_button").addClass('spinning');
		$.ajax({
		url     : '{{url("pickup")}}',
		type    : 'post',
		data    : "request_id={{Request::segment(4)}}"+"&product_id="+prid,
		success : function(res){
			$("#accept_all_button").removeClass('spinning');
			var obj = eval("("+res+")");
			if(obj.success == 1)
			{
				alert('Request has been pickedup successfully.');
				$("#pickupButton").hide();
				$("#pickupButton2").hide();
				$("#deliveryButton").show();

			}
			else
				{}


			 }
			});
	}

	function delivery(prid)
	{
		$("#accept_all_button").addClass('spinning');
		$.ajax({
		url     : '{{url("request-delivery")}}',
		type    : 'post',
		data    : "request_id={{Request::segment(4)}}"+"&product_id="+prid,
		success : function(res){
			$("#accept_all_button").removeClass('spinning');
			var obj = eval("("+res+")");
			if(obj.success == 1)
			{
				alert('Request has been delivered successfully.');
				$("#deliveryButton").hide();
				$("#deliveryButton2").hide();
				$("#completeButton").show();
				$("#cancelButton").show();
			}
			else
				{}


			 }
			});
	}

	function complete(prid)
	{
		$("#accept_all_button").addClass('spinning');
		$.ajax({
		url     : '{{url("request-complete")}}',
		type    : 'post',
		data    : "request_id={{Request::segment(4)}}"+"&product_id="+prid,
		success : function(res){
			$("#accept_all_button").removeClass('spinning');
			var obj = eval("("+res+")");
			if(obj.success == 1)
			{
				alert('Request has been completed successfully.');
				$("#completeButton").hide();
				$("#cancelButton").hide();
				$("#completeButton2").hide();
				$("#cancelButton2").hide();
			}
			else
				{}


			 }
			});
	}
</script>
@include('Admin::layouts.footer')
@stop
<style>
   .modal-header .close {
   margin-top: -9px !important;
   }
</style>