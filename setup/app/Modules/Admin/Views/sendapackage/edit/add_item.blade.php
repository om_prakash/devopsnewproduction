@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')
@section('content')
{!! Html::style('theme/admin/custome/css/style.min.css') !!}


<div class="container">
  <div class="row">
      <div class="col-sm-12" id="send_a_package_start_position">
      <h2 class="color-blue mainHeading">Send A Package </h2>
      <br>
      </div>

      <div class="col-sm-12">
        <div class="box-shadow">
          <h3 class="midHeading">Please complete the following fields</h3>
          <br>
          <hr>


          <div class="col-sm-12">
              <div class="step_box four_step clearfix">

                <div class="step first selected" id="step1-header">
                     <div>1</div>
                     <p class="text-center colol-black">Package Details</p>
                  </div>

                  <div class="step inner" id="step2-header">
                     <div>2</div>
                     <p class="text-center colol-black">Pickup Address</p>
                  </div>

                  <div class="step inner" id="step3-header ">
                     <div>3</div>
                     <p class="text-center colol-black">Drop Off Address</p>
                  </div>

                  <div class="step last " id="step4-header">
                     <div>4</div>
                     <p class="text-center colol-black">Payment</p>
                  </div>

              </div>
          </div>

          <form name="add_item" id="add_item">
          <div class="row" id="sec3">
              <input type="hidden" name="request_id" value="{{Request::segment(3)}}">
             <div class="col-md-10 col-sm-offset-1">

                <div class="col-sm-6 col-xs-6">
                  <div class="form-group">
                    <label class="control-label">Product Title</label>
                    <input class="form-control required  usename-#title#" placeholder="Product Title" name="title" maxlength="60">
                  </div>
                </div>

                <div class="col-sm-6 col-xs-6">
                  <div class="form-group">
                    <label class="control-label">Package Value</label>
                    <div class="input-group error-input">
                    <span class="input-group-addon">$</span>
                    <input class="form-control required float maxlength-7 " placeholder="Package Value" name="package_value" maxlength="9" >
                  </div>
                </div>

              </div>



                <div class="col-sm-12 col-xs-12">
                <br>
                   <div class="form-group">
                      <label class="control-label">Measurement Unit</label>
                      <div class="clearfix">
                      		<div class="radio">
                           <label class="">
                           	<input type="radio" name="measurement_unit"  value="cm_kg" onclick="return unit_show();">
                           </label>&nbsp;Metric (Cm/Kg)
                           <label class="">
                           	<input type="radio" name="measurement_unit" checked value="inches_lbs" onclick="return unit_show();">
                           </label>&nbsp;Imperial (Inches/Lbs)
                         	</div>

                      </div>
                   </div>
                </div>

                <div class="col-md-3 col-sm-3 col-xs-12">
                   <div class="form-group">
                      <label class="control-label">Length</label>
                      <div class="input-group error-cm-input">
                      <input class="form-control required  float maxlength-9" placeholder="Length" name="length" maxlength="9">
                      <span class="input-group-addon" id="length_unit"></span>
                      </div>
                   </div>
                </div>

                <div class="col-md-3 col-sm-3 col-xs-12">
                   <div class="form-group">
                      <label class="control-label">Width</label>
                      <div class="input-group error-cm-input">
                      <input class="form-control required float maxlength-9" placeholder="Width" name="width" maxlength="9">
                      <span class="input-group-addon" id="width_unit"></span>
                      </div>
                   </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                   <div class="form-group">
                      <label class="control-label">Height</label>
                      <div class="input-group error-cm-input">
                      <input class="form-control required float maxlength-9" placeholder="Height" name="height" maxlength="9">
                      <span class="input-group-addon" id="height_unit"></span>
                      </div>
                   </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                   <div class="form-group">
                      <label class="control-label">Weight</label>
                      <div class="input-group error-cm-input">
                      <input class="form-control required float maxlength-9" placeholder="Weight" name="weight" maxlength="9">
                      <span class="input-group-addon" id="weight_unit"></span>
                      </div>
                   </div>
                </div>
                <div class="col-sm-12 col-xs-12">
                       <div class="form-group">

                          <label class="control-label">Quantity</label>


                          	<div class="radio">
                              <label class="">
                               	<input type="radio" name="quantity_type" value="single" onclick="show_quantity();" checked="true">
                              </label>&nbsp;Single
                             	<label class="">
                             		<input type="radio" name="quantity_type" value="multiple" onclick="show_quantity();">
                             	</label>&nbsp;Multiple
                      			</div>


                       </div>
                    </div>
                <div class="col-sm-12 col-xs-12" id="quantity_input">
                   		<div class="form-group">
              		<input type="text" class="form-control required numeric" name="quantity" placeholder="Quantity" >
              		</div>
              	</div>


              	@if(count($transporter_data) > 0)
              		<div class="col-sm-12 col-xs-12">
                     	<div class="form-group">
                     	<input type="hidden" name="travel_mode" value="{{$transporter_data->TravelMode}}" >
                  		<label class="control-label">Shipping Mode: </label>&nbsp;&nbsp;{{ucfirst($transporter_data->TravelMode)}}
                  	</div>
                  </div>

                  @else
                  <div class="col-sm-12 col-xs-12">
                     <div class="form-group">
                        <label class="control-label">Shipping Mode</label> &nbsp;&nbsp;(Not all items can be shipped by Air. Items shipped by sea typically take longer to arrive.)
                        <div class="radio">
                           <label class="">
                           	<input type="radio" name="travel_mode" value="air" onclick="toggle_category('.travel-mode-air','.travel-mode-ship','#package_category')" checked="checked">
                           </label>&nbsp;By Air
                           <label class="">
                           	<input type="radio" name="travel_mode" value="ship" onclick="toggle_category('.travel-mode-ship','.travel-mode-air','#package_category')">
                           </label>&nbsp;By Sea
                        </div>
                     </div>
                  </div>
                  @endif

                  @if(count($transporter_data) > 0)
                  <div class="col-sm-12 col-xs-12">
                       <div class="form-group">
                          <label class="control-label">Select Package Category</label>
                          <select class="form-control required" id="package_category" name="category">
                             <option value="">Select Category</option>

                             <?php foreach ($category2 as $key) {?>
                             	<option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}"}' class="travel-mode-air">{{$key->Content}}</option>
                             <?php }?>


                          </select>
                       </div>
                    </div>

                @else
                	<div class="col-sm-12 col-xs-12">
                       <div class="form-group">
                          <label class="control-label">Select Package Category</label>
                          <select class="form-control required" id="package_category" name="category">
                             <option value="">Select Category</option>
                             <?php foreach ($category as $key) {?>
                             	<option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}"}' class="travel-mode-{{$key->TravelMode}}">{{$key->Content}}</option>
                             <?php }?>
                          </select>
                       </div>
                    </div>
                @endif

                <div class="col-sm-12 col-xs-12">
                   <div class="form-group">
                      <label class="control-label">Package Content/Description/Instructions</label>
                      <textarea rows="3" class="form-control" name="description"></textarea>
                   </div>
                </div>

                <div class="col-sm-12 col-xs-12">
                   	<div class="form-group">
                      <label class="control-label">Upload Default Package Image</label>
                      <input type="file" placeholder="Browse" name="default_image" class="required valid-filetype-jpg,png,gif,jpeg,JPEG,JPG,PNG,GIF">
                   </div>
                </div>
                <!-- <div class="col-sm-12 col-xs-12">
                   <div class="form-group">
                      <label class="control-label">Upload Extra Image(s)(Optional)</label>
                      <div class="row">
                        <div class="col-sm-3 col-xs-12">
                        	<input type="file" placeholder="Browse" name="package_image2" class="valid-filetype-jpg,png,gif,jpeg,JPEG,JPG,PNG,GIF">
                        </div>
                        <div class="col-sm-3 col-xs-12">
                        	<input type="file" placeholder="Browse" name="package_image3" class="valid-filetype-jpg,png,gif,jpeg,JPEG,JPG,PNG,GIF">
                        </div>
                        <div class="col-sm-3 col-xs-12">
                        	<input type="file" placeholder="Browse" name="package_image4" class="valid-filetype-jpg,png,gif,jpeg,JPEG,JPG,PNG,GIF">
                        </div>
                        <div class="col-sm-3 col-xs-12">
                        	<input type="file" placeholder="Browse" name="package_image5" class="valid-filetype-jpg,png,gif,jpeg,JPEG,JPG,PNG,GIF">
                        </div>
                    </div>
                   </div>
                </div> -->


                <div class="col-sm-12 col-xs-12">
                   <div class="form-group">
                      <label class="control-label">Insurance</label>
                      <div class="radio">
                         <label>
                         	<input type="radio" name="insurance" value="yes" checked>
                         </label>&nbsp; Yes
                         &nbsp; &nbsp;
                         <label>
                         	<input type="radio" name="insurance" value="no">
                         </label>&nbsp;No
                      </div>
                   </div>
                   <div class="form-group">
                      <div class="checkbox">
                         <label>
                         	<span class="pull-left"><input type="checkbox" name="need_package_material"></span> <span class="Insurance_check"> Need Package Material</span>
                         </label>
                      </div>
                   </div>

                   <div class="form-group">
                      <div class="checkbox">
                         <!-- <label>
                         	<span class="pull-left"><input type="checkbox"  id="agree" name="checkbox" value="check"></span> <span class="Insurance_check"> I Accept the  <a href="{{url('terms-and-conditions')}}" target="_blank"> Terms and Conditions </a> .</span>
                         </label> -->
                      </div>
                   </div>
                </div>
                <div class="col-md-12 col-xs-12">
                   <hr>
                </div>


                <div class="col-sm-8 col-xs-12">
                        <div class="">

                        <div class="form-group">  
                        <a class="custom-btn1 btn text-center" href="{{url('admin/edit-request')}}/{{Request::segment(3)}}">
                             Back
                            <div class="custom-btn-h"></div>
                          </a>
                        </div>

                        <div class="col-xs-6 col-xs-12">
                             <div class="form-group">
                           <button class="custom-btn1 btn" id="calculate_loader">
                             Next
                             <div class="custom-btn-h"></div>
                          </button><div class="error-msg"></div>
                        </div>
                        </div>


                        </div>
                      </div>
             </div>
          </div>
          </form>

        </div>
      </div>
  </div>
</div>


{!! Html::script('theme/admin/custome/js/utility.js') !!}
{!! Html::script('theme/admin/custome/js/validation1.js') !!}
{!! Html::script('theme/admin/custome/js/send_package.js') !!}
{!! Html::script('theme/web/js/validation.js') !!}

@include('Admin::layouts.footer')

<script type="text/javascript">


new Validate({
    FormName : 'add_item',
    ErrorLevel : 1,
    validateHidden: false,
    callback : function(){
      $("#calculate_loader").addClass('spinning');
      $.ajax({
          url: SITEURL+"admin/add-item-exist-request",
          method: 'post',
          data: new FormData(document.getElementById('add_item')),
          processData: false,
          contentType: false,
          dataType: "json",

          success: function(res){
            if(res.success == 1){
              alert(res.msg);
              document.location.href = SITEURL + 'admin/edit-request/'+ $(location).attr('href').split('/').reverse()[0];
            }else{
              alert(res.msg);
            }
            $("#calculate_loader").removeClass('spinning');
          }
      });
    }

});

function toggle_category(showid, hideid, id) {
  $(id).val('');
  $(showid).attr('disabled', false);
  $(showid).show();
  $(hideid).attr('disabled', true);
  $(hideid).hide();
}

toggle_category('.travel-mode-air', '.travel-mode-ship');

function show_quantity()
{
  var type =$('input[name=quantity_type]:checked').val();
  if(type == 'multiple'){
    $('#quantity_input').show();
  }else{
    $('#quantity_input').hide();
  }

}
show_quantity();

function unit_show()
{
  var unit =$('input[name=measurement_unit]:checked').val();

  if(unit == 'cm_kg'){
    $('#length_unit').html('Cm');
    $('#width_unit').html('Cm');
    $('#height_unit').html('Cm');
    $('#weight_unit').html('Kg');

  }else{
    $('#length_unit').html('Inches');
    $('#width_unit').html('Inches');
    $('#height_unit').html('Inches');
    $('#weight_unit').html('Lbs');
  }

}

unit_show();

</script>
@endsection