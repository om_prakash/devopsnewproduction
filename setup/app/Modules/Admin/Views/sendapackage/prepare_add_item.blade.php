@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')

@section('content')
{!! Html::style('theme/admin/custome/css/style.min.css') !!}

<div class="container">
   <div class="row">
      <div class="col-sm-12"  id="send_a_package_start_position">
      <h2 class="color-blue">SEND A PACKAGE</h2>
      <br>
      </div>
        <style type="text/css">
                  .Insurance_check{float: left; margin-top: 3px; margin-left: 5px;}

                </style>
      <div class="col-sm-12">
        <div class="box-shadow">
        <h3>Please complete the following fields</h3>
        <hr>
        <div class="row">
        <!-- Fillable -->
          <div class="col-sm-12">
             <div class="step_box four_step clearfix">
              <div class="step first selected" id="step0-header" style="width:20%;">
                   <div>1</div>
                   <p class="text-center colol-black">Select User</p>
                </div>
                <div class="step inner" id="step1-header" style="width:20%;">
                   <div>2</div>
                   <p class="text-center colol-black">Package Details</p>
                </div>
                <div class="step inner" id="step2-header" style="width:20%;">
                   <div>3</div>
                   <p class="text-center colol-black">Drop Off Address</p>
                </div>
                <div class="step inner" id="step3-header" style="width:20%;">
                   <div>4</div>
                   <p class="text-center colol-black">Package Details</p>
                </div>
                <div class="step last " id="step4-header" style="width:20%;">
                   <div>5</div>
                   <p class="text-center colol-black">Payment</p>
                </div>
             </div>
          </div>

             <!-- <div id="sec1" >  -->
        {!! Form::open(['class'=>'form-vertical','name'=>'prepare_request_form','id' => 'prepare_request_form']) !!}
        <input type="hidden" name="request_type" id="request_type">
        <input type="hidden" name="request_id" id="request_id" value="">
        <input type="hidden" name="user_id" id="user_id">
            <div class="col-md-10 col-sm-offset-1" id="sec0" >

              <h2 class="col-sm-12 text-primary row" >Customer Detail</h2>
              <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                       <label class="control-label">User</label>
                       <div class="radio">
                          <label class="">
                          <input type="radio" name="user_type" id="existing" value="old_user" checked >
                          </label>&nbsp;Existing
                          <label class="">
                          <input type="radio" name="user_type" id="newuser" value="new_user" >
                          </label>&nbsp;New
                       </div>
                    </div>
                 </div>
              </div>
              <div class="col-sm-12 row" id="Existing_user">
                  <div class="form-group">
                      <div class="col-sm-6">
                      <div class="form-group">
                         <label>Email</label>
                         <input type="text" placeholder="Email" name="existing_user_email" id="existing_user_email"  maxlength= "105" class="form-control required" value="">
                      </div>
                   </div>
                  </div>
               </div>
                <div class="row" id="name" style="display:none;">
                 <div class="col-sm-6">
                  <div class="form-group">
                     <label>First Name</label><span class="red-star"> *</span>
                     <input type="text" placeholder="First Name" name="first_name" id="first_name"  maxlength= "125" class="form-control required">
                  </div>
               </div>

                 <div class="col-sm-6">
                  <div class="form-group">
                     <label>Last Name</label><span class="red-star"> *</span>
                     <input type="text" placeholder="Last Name" name="last_name" id="last_name"  maxlength= "125" class="form-control required">
                  </div>
               </div>
               <div class="col-sm-6">
                  <div class="form-group">
                     <label>Email</label><span class="red-star"> *</span>
                     <input type="text" placeholder="Email" name="user_email" id="user_email"  maxlength= "125" class="form-control required">
                     <p class="help-block" id="er_email"> </p>
                  </div>
               </div>
               </div>

        <div class="">
           <hr>
        </div>
        <div class="">
           <div class="form-group">
              <button class="custom-btn1" id="select_user_btn">
                 Next
                 <div class="custom-btn-h"></div>
              </button>
           </div>
        </div>
      </div>
     </form>
              <!-- Start section1 -->
               <form name="add_item" id="add_item">
              <div class="row" id="sec1" style="display: none;">
                 <div class="col-md-10 col-sm-offset-1">
                    <!--  <form class="form-vertical">    -->
                    <div class="col-sm-6 col-xs-6">
                      <div class="form-group">
                            <label class="control-label">Package Title</label>
                            <input class="form-control required" placeholder="Package Title" name="package_title" >
                      </div>
                  </div>

                   <div class="col-sm-6 col-xs-6">
                      <div class="form-group">
                            <label class="control-label">Package Value</label>
                            <div class="input-group error-input">
                            <span class="input-group-addon">$</span>
                            <input class="form-control required float maxlength-7 " placeholder="Package Value" name="package_value" maxlength="9" >
                            </div>
                      </div>
                  </div>


                    <div class="col-sm-12">
                       <div class="form-group">
                          <label class="control-label">Measurement Unit</label>
                          <div class="clearfix">
                             <label class="radio-inline">
                              <input type="radio" name="measurement_unit"  value="cm_kg"  onclick="return unit_show();">Metric (Cm/Kg)
                             </label>
                             <label class="radio-inline">
                              <input type="radio" name="measurement_unit" value="inches_lbs" onclick="return unit_show();" checked >Imperial (Inches/lbs)
                             </label>
                          </div>
                       </div>
                    </div>

                    <div class="col-md-2 col-sm-3 col-xs-12">
                       <div class="form-group">
                          <label class="control-label">Length</label><span class="red-star"> *</span>
                          <div class="input-group error-cm-input">
                          <input class="form-control required  float maxlength-9" placeholder="Length" name="length" maxlength="9">
                          <span class="input-group-addon" id="length_unit"></span>
                          </div>
                       </div>
                    </div>

                    <div class="col-md-3 col-sm-3 col-xs-12">
                       <div class="form-group">
                          <label class="control-label">Width</label><span class="red-star"> *</span>
                          <div class="input-group error-cm-input">
                          <input class="form-control required float maxlength-9" placeholder="Width" name="width" maxlength="9">
                          <span class="input-group-addon" id="width_unit"></span>
                          </div>
                       </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-12">
                       <div class="form-group">
                          <label class="control-label">Height</label><span class="red-star"> *</span>
                          <div class="input-group error-cm-input">
                          <input class="form-control required float maxlength-9" placeholder="Height" name="height" maxlength="9">
                          <span class="input-group-addon" id="height_unit"></span>
                          </div>
                       </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-12">
                       <div class="form-group">
                          <label class="control-label">Weight</label><span class="red-star"> *</span>
                          <div class="input-group error-cm-input">
                          <input class="form-control required float maxlength-9" placeholder="Weight" name="weight" maxlength="9">
                          <span class="input-group-addon" id="weight_unit"></span>
                          </div>
                       </div>
                    </div>


                  <div class="col-sm-12 col-xs-12">
                        <div class="form-group">
                          <label class="control-label">Quantity</label>


                            <div class="radio">
                              <label class="">
                                <input type="radio" name="quantity_type" value="single" onclick="show_quantity();" checked="true">
                              </label>&nbsp;Single
                              <label class="">
                                <input type="radio" name="quantity_type" value="multiple" onclick="show_quantity();">
                              </label>&nbsp;Multiple
                        </div>


                        </div>
                </div>

                <div class="col-sm-12 col-xs-12" id="quantity_input">
                          <div class="form-group">
                      <input type="text" class="form-control required numeric" name="quantity" placeholder="Quantity" >
                      </div>
                </div>
                    <div class="col-sm-6">
                       <div class="form-group">
                          <label class="control-label">Shipping Mode</label>
                          <div class="clearfix">
                             <label class="radio-inline">
                              <span class="pull-left"><input type="radio" name="travel_mode" value="air" onclick="toggle_category('.travel-mode-air','.travel-mode-ship','#package_category')" checked="checked" ></span> <span class="Insurance_check">  By Air </span>
                             </label>
                             <label class="radio-inline">
                              <span class="pull-left"><input type="radio" name="travel_mode" value="ship" onclick="toggle_category('.travel-mode-ship','.travel-mode-air','#package_category')">
                                </span> <span class="Insurance_check"> By Sea  </span>
                             </label>
                          </div>
                       </div>
                    </div>
                    <div class="col-sm-6">
                       <div class="form-group">
                          <label class="control-label">Select Package Category</label><span class="red-star"> *</span>
                          <select class="form-control required" id="package_category" name="category">
                             <option value="">Select Category</option>

                             <?php foreach ($category as $key) {?>

                              <option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}"}'  class="travel-mode-{{$key->TravelMode}}">{{$key->Content}}
                              </option>

                             <?php }?>
                          </select>
                       </div>
                    </div>
                    <div class="col-sm-12">
                       <div class="form-group">
                          <label class="control-label ">Package Content/Description/Instructions</label>
                          <textarea rows="3" class="form-control" name="description"></textarea>
                       </div>
                    </div>

          <div class="col-sm-12">
                        <div class="form-group">
                          <label class="control-label">Upload Default Package Image</label><span class="red-star"> *</span>
                          <input type="file" placeholder="Browse" name="default_image" class="required valid-filetype-jpg,png,gif,jpeg,JPEG,JPG,PNG,GIF">
                        </div>
                    </div>


                      {{-- <div class="col-sm-12">
                       <div class="form-group">
                          <label class="control-label">Upload Extra Image(s)(Optional)</label>
                          <div class="row">
                            <div class="col-sm-3">
                              <input  type="file" placeholder="Browse" name="package_image2" class="valid-filetype-jpg,png,gif,jpeg,JPG,PNG,GIF,JPEG">

                            </div>
                            <div class="col-sm-3">
                              <input type="file" placeholder="Browse" name="package_image3" class="valid-filetype-jpg,png,gif,jpeg,JPG,PNG,GIF,JPEG">
                            </div>
                            <div class="col-sm-3">
                              <input type="file" placeholder="Browse" name="package_image4" class="valid-filetype-jpg,png,gif,jpeg,JPG,PNG,GIF,JPEG">
                            </div>
                            <div class="col-sm-3">
                              <input type="file" placeholder="Browse" name="package_image5" class="valid-filetype-jpg,png,gif,jpeg,JPG,PNG,GIF,JPEG">
                            </div>
                        </div>
                       </div>
                    </div> --}}

                    <div class="col-sm-6">
                       <div class="form-group">
                          <label class="control-label">Receiver's Phone number</label><span class="red-star"> *</span>
                          <div class="row">
                             <div class="col-xs-4">
                                <input type="text" class="form-control required numeric maxlength-3" placeholder="Country Code" name="country_code" maxlength="3" value="">
                             </div>
                             <div class="col-xs-8">
                                <input type="text" class="form-control required numeric between-8-12" name="phone_number" placeholder="Phone Number" value="" pattern=".{8,12}" title="8 to 12 numbers">
                             </div>
                          </div>
                       </div>
                    </div>



                    <div class="col-sm-12">
                       <div class="form-group">
                          <label class="control-label">Insurance</label>
                          <div class="checkbox">
                             <label>
                              <span class="pull-left row"><input type="radio" name="insurance" value="yes" checked></span> <span class="Insurance_check"> Yes </span>
                             </label>
                             &nbsp; &nbsp;
                             <label>
                              <span class="pull-left"><input type="radio" name="insurance" value="no"></span> <span class="Insurance_check"> No </span>
                             </label>
                          </div>
                       </div>
                       <div class="form-group">
                          <div class="checkbox">
                             <label>
                              <span class="pull-left"><input type="checkbox" value="yes" name="need_package_material" ></span> <span class="Insurance_check"> Need package material </span>
                             </label>
                          </div>
                       </div>

                        <div class="form-group">
                          <div class="checkbox">

                          </div>
                       </div>
                    </div>
                    <div class="col-md-12">
                       <hr>
                    </div>
                    <div class="">
                      <div class="">

                        <div class="col-xs-12">
                           <div class="form-group">
                              <button class="custom-btn1 btn"  id="calculate_loader">
                                 Next
                                 <div class="custom-btn-h"></div>
                              </button>
                               <a class="custom-btn1 btn  text-center" onclick="switch_request_header('#sec2','#sec3',2)" href="javascript:void(0)">
                                 Back
                                       <div class="custom-btn-h"></div>
                              </a>
                           </div>
                      </div>

                    </div>
                    </div>
                 </div>
              </div>
              <!-- End section 3 -->
              </form>

        </div>
     </div>
  </div>
</div>


{!! Form::close() !!}
<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=false&key=AIzaSyCT0uj-bffWWdOdwDp15LvrMko-OuoZUoY"></script>

{!! Html::script('theme/admin/custome/js/validation1.js') !!}
{!! Html::script('theme/admin/custome/js/utility.js') !!}
{!! Html::script('theme/admin/custome/js/request.js') !!}
{!! Html::style('theme/date-time-picker/jquery.datetimepicker.css') !!}
{!! Html::script('theme/date-time-picker/jquery.datetimepicker.js') !!}

@include('Admin::layouts.footer')


<script>

$('#pp_pickup_country').trigger('change');
$('#pp_dropoff_country').trigger('change');
$('#pp_nd_return_country').trigger('change');
$('#pp_return_country').trigger('change');




$(function() {
       var action;
       $(".number-spinner a").mousedown(function () {
           btn = $(this);
           input = btn.closest('.number-spinner').find('input');
           btn.closest('.number-spinner').find('a').prop("disabled", false);

           if (btn.attr('data-dir') == 'up') {
               action = setInterval(function(){
                   if ( input.attr('max') == undefined || parseInt(input.val()) < parseInt(input.attr('max')) ) {
                       input.val(parseInt(input.val())+1);
                   }else{
                       btn.prop("disabled", true);
                       clearInterval(action);
                   }
               }, 50);
           } else {
               action = setInterval(function(){
                   if ( input.attr('min') == undefined || parseInt(input.val()) > parseInt(input.attr('min')) ) {
                       input.val(parseInt(input.val())-1);
                   }else{
                       btn.prop("disabled", true);
                       clearInterval(action);
                   }
               }, 50);
           }
       }).mouseup(function(){
           clearInterval(action);
       });
   });


</script>

<script type="text/javascript">



function journey_show()
{
  if($('[name="journey_type"]:checked').val() == "return")
    {
      $("#return_journy").trigger("onclick");
    }
}

journey_show();

function image_preview(obj,previewid,evt)
{
    var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
    if ($.inArray($(obj).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
       $(obj).val('');
       $('#'+previewid).attr('src', '{{ImageUrl}}/user-no-image.jpg');
        alert("Only "+fileExtension.join(', ')+" formats are allowed.");
     } else {

       var file = evt.target.files[0];
       if (file)
       {
         var reader = new FileReader();

         reader.onload = function (e) {
             $('#'+previewid).attr('src', e.target.result)
         };
         reader.readAsDataURL(file);
       }
     }
}

function toggle_category(showid, hideid, id) {
  $(id).val('');
  $(showid).attr('disabled', false);
  $(showid).show();
  $(hideid).attr('disabled', true);
  $(hideid).hide();
}


toggle_category('.travel-mode-air','.travel-mode-ship','#package_category');


function show_quantity()
{
  var type =$('input[name=quantity_type]:checked').val();
  if(type == 'multiple'){
    $('#quantity_input').show();
  }else{
    $('#quantity_input').hide();
  }

}
show_quantity();


function unit_show()
{
  var unit =$('input[name=measurement_unit]:checked').val();

  if(unit == 'cm_kg'){
    $('#length_unit').html('Cm');
    $('#width_unit').html('Cm');
    $('#height_unit').html('Cm');
    $('#weight_unit').html('Kg');

  }else{
    $('#length_unit').html('Inches');
    $('#width_unit').html('Inches');
    $('#height_unit').html('Inches');
    $('#weight_unit').html('lbs');
  }

}
unit_show();


new Validate({
    FormName : 'add_item',
    ErrorLevel : 1,
    validateHidden: false,
    callback : function(){
      $("#calculate_loader").addClass('spinning');
      $.ajax({
          url: SITEURL+"admin/send-a-package-add-item",
          method: 'post',
          data: new FormData(document.getElementById('add_item')),
          processData: false,
          contentType: false,
          dataType: "json",

          success: function(res){
            if(res.success == 1){
              alert(res.msg);
              document.location.href = SITEURL + 'admin/add-send-a-package';
            }else{
              alert(res.msg);
            }
            $("#calculate_loader").removeClass('spinning');
          }
      });
    }

});

</script>

@endsection
