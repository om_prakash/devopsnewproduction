@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')

@section('content')

<?php

function permission($id, $info) {
    if (($info['UserPermission'] & $id) == $id) {
        echo 'checked="checked"';
    }
}

?>
<div class="container-fluid">
<div class="panel panel-default">
  <div class="panel-heading"><i class="fa fa-pencil-square-o"></i>&nbsp;Edit Admin User</div>
  <div class="panel-body">

  	<div class="row">
  		<div class="col-md-12">
		{!! Form::model($info, ['method' => 'POST', 'url' => ['admin/edit_admin_user',$info['_id']],'onsubmit'=>'return form_validation()']) !!}

     	  <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}"  id="er_container_name" >
		     {!! Form::label('name', 'Name',['class' => 'col-md-2 text-right']) !!}
		     <div class="col-md-6">
				{!! Form::text('name', $info['UserName'], ['class'=>'form-control', 'placeholder'=> 'Name','id' =>'name']) !!}
				<span id="er_name" class="help-block" >@if ($errors->has('name')) {{ $errors->first('name') }} @endif</span>

			</div>
		     <div class="clearfix"></div>
         </div>
         <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}"  id="er_container_email" >
		     {!! Form::label('email', 'Email',['class' => 'col-md-2 text-right']) !!}
		     <div class="col-md-6" >
				{!! Form::text('email', $info['UserEmail'], ['class'=>'form-control', 'placeholder'=> 'Email','id' =>'email']) !!}
				<p class="help-block" id="er_email" style="color:#a94442"> @if ($errors->has('email')) {{ $errors->first('email') }} @endif</p>
			</div>
		     <div class="clearfix"></div>
         </div>
         <div class="form-group col-xs-offset-2 " id="cur-sec-password" ><span onclick="show_hide('cur-sec-password','sec-password')">Change Password</span></div>
         <div  id="sec-password" style="display:none">
			 <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}"  id="er_container_password"  >
				 {!! Form::label('password', 'Password',['class' => 'col-md-2 text-right']) !!}
				 <div class="col-md-6">
					{!! Form::password('password', ['class'=>'form-control', 'placeholder'=> 'Password','id' =>'password']) !!}
					<p class="help-block" id="er_password"> @if ($errors->has('password')) {{ $errors->first('password') }} @endif</p>
				</div>
				 <div class="clearfix"></div>
			 </div>
			 <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}"  id="er_container_conf_password" >
				 {!! Form::label('conf_password', 'Confirm Password',['class' => 'col-md-2 text-right']) !!}
				 <div class="col-md-6">
					{!! Form::password('conf_password', ['class'=>'form-control', 'placeholder'=> 'Confirm Password','id' =>'conf_password']) !!}
					<p class="help-block" id="er_conf_password"> @if ($errors->has('conf_password')) {{ $errors->first('conf_password') }} @endif</p>
				</div>
				 <div class="clearfix"></div>
			 </div>
         </div>
         <div class="form-group"  >
		     {!! Form::label('Permission', 'Permission',['class' => 'col-md-2 text-right']) !!}
		     <div class="col-md-8">
				 <b><a href="javascript:void(0)" onclick="check_all()" id="check_control">Check All</a></b>
			</div><br />
		     <div class="col-md-2">
				<input type="checkbox" class="permission"  name="carrier" value="{{CARRIER}}" <?php permission(CARRIER, $info);?> > &nbsp;{!! Form::label('carrier', 'Requester') !!}
				<br />
				<input type="checkbox" class="permission"  name="sender" value="{{SENDER}}" <?php permission(SENDER, $info);?> > &nbsp;{!! Form::label('sender', 'Transporter') !!}
				<br />
				<input type="checkbox" class="permission" name="admin" value="{{ADMIN}}" <?php permission(ADMIN, $info);?> > &nbsp;{!! Form::label('admin', 'Admin') !!}
				<br />
				<input type="checkbox" class="permission" name="app_content" value="{{APP_CONTENT}}" <?php permission(APP_CONTENT, $info);?> > &nbsp;{!! Form::label('app_content', 'App Content') !!}
				<br />
				<input type="checkbox" class="permission"  name="email_template" value="{{EMAIL_TEMPLATE}}" <?php permission(EMAIL_TEMPLATE, $info);?> > &nbsp;{!! Form::label('email_template', 'Email Template') !!}
				<br />
				<input type="checkbox" class="permission" name="web_content" value="{{WEB_CONTENT}}" <?php permission(WEB_CONTENT, $info);?> > &nbsp;{!! Form::label('web_content', 'Web Content') !!}
				<br />
				<input type="checkbox" class="permission" name="app_tutorial" value="{{APP_TUTORIAL}}" <?php permission(APP_TUTORIAL, $info);?> > &nbsp;{!! Form::label('app_tutorial', 'App Tutorial') !!}
				<br />
				<input type="checkbox" class="permission" name="about_app_content" value="{{ABOUT_APP}}" <?php permission(ABOUT_APP, $info);?> > &nbsp;{!! Form::label('about_app_content', 'About App Content') !!}
				<br />
				<input type="checkbox" class="permission" name="faq" value="{{FAQ}}" <?php permission(FAQ, $info);?> > &nbsp;{!! Form::label('faq', 'Faq') !!}
			</div>
			<div class="col-md-2">

				<input type="checkbox" class="permission" name="country" value="{{COUNTRY}}" <?php permission(COUNTRY, $info);?> > &nbsp;{!! Form::label('country', 'Country') !!}
				<br />
				<input type="checkbox" class="permission" name="state" value="{{STATE}}" <?php permission(STATE, $info);?> > &nbsp;{!! Form::label('state', 'State') !!}
				<br />
				<input type="checkbox" class="permission" name="city" value="{{CITY}}" <?php permission(CITY, $info);?> > &nbsp;{!! Form::label('city', 'City') !!}
				<br />

				<input type="checkbox" class="permission" name="package" value="{{PACKAGE}}" <?php permission(PACKAGE, $info);?> > &nbsp;{!! Form::label('package', 'Package') !!}
				<br />
				<input type="checkbox" class="permission" name="feedback" value="{{FEEDBACK}}" <?php permission(FEEDBACK, $info);?> > &nbsp;{!! Form::label('feedback', 'Feedback') !!}
				<br />
				<input type="checkbox" class="permission" name="notification" value="{{NOTIFICATION}}" <?php permission(NOTIFICATION, $info);?> > &nbsp;{!! Form::label('notification', 'Notification') !!}
				<br />
				<input type="checkbox" class="permission" name="congiguration" value="{{CONFIGURATION}}" <?php permission(CONFIGURATION, $info);?> > &nbsp;{!! Form::label('congiguration', 'Configuration') !!}
				<br />

				<input type="checkbox" class="permission" name="setting" value="{{SETTING}}" <?php permission(SETTING, $info);?> > &nbsp;{!! Form::label('setting', 'Setting') !!}
				<br />

				<input type="checkbox" class="permission" name="individual_trip" value="{{INDIVIDUAL_TRIP}}" <?php permission(INDIVIDUAL_TRIP, $info);?> > &nbsp;{!! Form::label('individualtrip', 'Individual Trip') !!}
				<br />

				
				
			</div>
			<div class="col-md-4">
				

				<input type="checkbox" class="permission" name="bussiness_trip" value="{{BUSSINESS_TRIP}}" <?php permission(BUSSINESS_TRIP, $info);?> > &nbsp;{!! Form::label('bussinesstrip', 'Bussiness Trip') !!}
				<br />

				<input type="checkbox" class="permission" name="transaction2" value="{{TRANSACTION2}}" <?php permission(TRANSACTION2, $info);?>  > &nbsp;{!! Form::label('transaction', 'Requester Transaction') !!}
				<br />

				<input type="checkbox" class="permission" name="transaction" value="{{TRANSACTION}}" <?php permission(TRANSACTION, $info);?> > &nbsp;{!! Form::label('transaction', 'Transporter Transaction') !!}
				<br />

				

				<input type="checkbox" class="permission" name="item_category" value="{{ITEMCATEGORY}}" <?php permission(ITEMCATEGORY, $info);?> > &nbsp;{!! Form::label('itemcategory', 'Item Category') !!}
				<br />

				<input type="checkbox" class="permission" name="promocode" value="{{PROMOCODE}}" <?php permission(PROMOCODE, $info);?> > &nbsp;{!! Form::label('promocode', 'Promocode') !!}
				<br />
				<input type="checkbox" class="permission" name="aquantuo_address" value="{{AQUANTUO_ADDRESS}}" <?php permission(AQUANTUO_ADDRESS, $info);?> > &nbsp;{!! Form::label('aquantuo_address', 'Aquantuo Address') !!}
			</div>

		     <div class="clearfix"></div>
         </div>
          <div class="form-group">
			  <div class="col-md-10 col-md-offset-2">
				{!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
				<a href="{{ 'admin_user' }}" class='btn btn-danger'>Cancel</a>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
      <!-- Pagination Section-->
  </div> <!-- Panel Body -->
</div>
</div>
{!! Html::script('theme/admin/custome/js/validation.js') !!}
<script>
var status = 'true';
var validatePassword = 'false';
function check_all()
{
	if(status == 'true'){
		$('.permission').prop('checked',true);
		status = 'false';
		$('#check_control').html('Uncheck All');
	}else{
		$('.permission').prop('checked',false);
		$('#check_control').html('Check All');
		status = 'true';
	}
}

function show_hide(current,show)
{
	validatePassword = 'true';
	$('#'+current).hide();
	$('#'+show).show();
}
function form_validation()
{
	var flag = true;
	if(valid.required('name','name') == false){ flag = false; }
	if(valid.required('email','email') == false){ flag = false; }else{
		if(valid.email('email','email') == false){ flag = false; }
	}
	if(validatePassword == 'true'){
		if(valid.required('password','password') == false){ flag = false; }
		if(valid.required('conf_password','confirm password') == false){ flag = false; }else{
			if(valid.match('conf_password','password','The password and confirm password field should be match.') == false){ flag = false; }
		}
	}
	return flag;
}
</script>
@include('Admin::layouts.footer')
@stop
