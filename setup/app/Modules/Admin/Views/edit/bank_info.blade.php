@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')

@section('content')
<?php
function get_value($dbval, $old)
{
    return (!empty(trim($old))) ? $old : $dbval;
}?>
<div class="container-fluid">
<div class="panel panel-default">
  <div class="panel-heading"><i class="fa fa-pencil-square-o"></i>&nbsp;Edit Bank Information</div>
  <div class="panel-body">
  	<div class="row">
  		<div class="col-md-12">

			{!! Form::open(array('url' =>'admin/post-edit-bank/'.$user->_id,  'id'=>'edit_bank', 'files' => 'true','name' => 'edit_bank', 'class' => 'form-horizontal','method' => 'post' )) !!}

			<div class="form-group {{ $errors->has('bank_country') ? ' has-error' : '' }}"  >
		     {!! Form::label('bank_country', 'Bank A/C Country',['class' => 'col-md-2 text-right']) !!}
		    	<div class="col-md-4">
 <?php
$country_list = array('' => 'Select Country');

foreach ($country as $key) {
    $country_list[$key['Content']] = ucfirst($key['Content']);
}
?>
		{!! Form::select('bank_country',$country_list,Input::get('bank_country'), ['class'=>'form-control']); !!}

					@if ($errors->has('bank_country')) <p class="help-block">{{ $errors->first('bank_country') }}</p> @endif
				</div>
		     	<div class="clearfix"></div>
         	</div>

            <div class="form-group {{ $errors->has('bank_name') ? ' has-error' : '' }}"  >
		     {!! Form::label('bank_name', 'Bank Name',['class' => 'col-md-2 text-right']) !!}
		    	<div class="col-md-4">
					{!! Form::text('bank_name',get_value($user->BankName,Input::old('bank_name')), ['class'=>'form-control', 'placeholder'=> 'Bank Name','id' =>'bank_name']) !!}
					@if ($errors->has('bank_name')) <p class="help-block">{{ $errors->first('bank_name') }}</p> @endif
				</div>
		     	<div class="clearfix"></div>
         	</div>

         	<div class="form-group {{ $errors->has('account_holder_name') ? ' has-error' : '' }}"  >
		     {!! Form::label('account_holder_name', 'Account Holder Name',['class' => 'col-md-2 text-right']) !!}
		    	<div class="col-md-4">
					{!! Form::text('account_holder_name',get_value($user->AccountHolderName,Input::old('account_holder_name')), ['class'=>'form-control', 'placeholder'=> 'Account Holder Name','id' =>'bank_name']) !!}
					@if ($errors->has('account_holder_name')) <p class="help-block">{{ $errors->first('account_holder_name') }}</p> @endif
				</div>
		     	<div class="clearfix"></div>
         	</div>

         	<div class="form-group {{ $errors->has('account_no') ? ' has-error' : '' }}"  >
		    	 {!! Form::label('account_no', 'Account No.',['class' => 'col-md-2 text-right']) !!}
		     	<div class="col-md-4">
					{!! Form::text('account_no', get_value($user->AccountNo,Input::old('account_no')), ['class'=>'form-control', 'placeholder'=> 'Account No.','id' =>'inputError1']) !!}
					@if ($errors->has('account_no')) <p class="help-block">{{ $errors->first('account_no') }}</p> @endif
				</div>
		     	<div class="clearfix"></div>
         	</div>


     	  	<div class="form-group {{ $errors->has('ssn') ? ' has-error' : '' }}"  >
		     {!! Form::label('ssn', 'SSN',['class' => 'col-md-2 text-right']) !!}
		     	<div class="col-md-4">
					{!! Form::text('ssn',get_value($user->SSN,Input::old('ssn')), ['class'=>'form-control', 'placeholder'=> 'SSN','id' =>'ssn']) !!}
					@if ($errors->has('ssn')) <p class="help-block">{{ $errors->first('ssn') }}</p> @endif
				</div>
		     	<div class="clearfix"></div>
         	</div>
         	<div class="form-group {{ $errors->has('routing_number') ? ' has-error' : '' }}"  >
		    	 {!! Form::label('routing_number', 'Routing No',['class' => 'col-md-2 text-right']) !!}
		     	<div class="col-md-4">
					{!! Form::text('routing_number', get_value($user->RoutingNo,Input::old('routing_number')), ['class'=>'form-control', 'placeholder'=> 'Routing No','id' =>'inputError1']) !!}
					@if ($errors->has('routing_number')) <p class="help-block">{{ $errors->first('routing_number') }}</p> @endif
				</div>
		     	<div class="clearfix"></div>
         	</div>

         	<div class="clearfix"></div>
          	<div class="form-group">
			  	<div class="col-md-4 col-md-offset-2">
					{!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
					<a href = "{{url('admin/transporter')}}" class = 'btn btn-danger'>Cancel</a>
					{!! Form::close() !!}
				</div>
			</div>
	</div>
</div>

  </div> <!-- Panel Body -->
</div>
</div>

@include('Admin::layouts.footer')
@endsection
