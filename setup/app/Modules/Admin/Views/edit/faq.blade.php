@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')

@section('content')
{!! Html::style('theme/admin/custome/css/sumoselect.css') !!}

<div class="container-fluid">
<div class="panel panel-default">
  <div class="panel-heading"><i class="fa fa-pencil-square-o"></i>&nbsp;Faq</div>
  <div class="panel-body">

  	<div class="row">
  		<div class="col-md-12">
		{!! Form::model('', ['method' => 'POST', 'url' => ['admin/post-edit-faq/'.$faq->id]]) !!}

		  <input type="hidden" name="sectionId" value="{{$faq->section_id}}">
     	  <div class="form-group {{ $errors->has('section_name') ? ' has-error' : '' }}"  >
		     {!! Form::label('Select Section', '',['class' => 'col-md-2 text-right']) !!}
		     <div class="col-md-10">
              <select class="form-control" name="section_name">
              	@foreach($section as $value)
              	 <option value="{{$value->id}}" @if($value->id == $faq->section_id) selected @endif>{{ucfirst($value->section_name)}}</option>
              	 @endforeach
              </select>
			</div>
		     <div class="clearfix"></div>
         </div>

     	  <div class="form-group {{ $errors->has('Question') ? ' has-error' : '' }}"  >
		     {!! Form::label('Question', '',['class' => 'col-md-2 text-right']) !!}
		     <div class="col-md-10">
				{!! Form::textarea('Question', $faq->question, ['class'=>'form-control', 'placeholder'=> 'Question','id' =>'inputError1','rows'=>'2']) !!}
				@if ($errors->has('Question')) <p class="help-block">{{ $errors->first('Question') }}</p> @endif
			</div>
		     <div class="clearfix"></div>
         </div>

         <div class="form-group {{ $errors->has('Answer') ? ' has-error' : '' }}">
		     {!! Form::label('Answer', 'Email Body',['class' => 'col-md-2 text-right']) !!}
		     <div class="col-md-10">
				{!! Form::textarea('Answer', $faq->answer, ['class'=>'form-control', 'placeholder'=> 'Answer','id'=>'TBody']) !!}
				@if ($errors->has('Answer')) <p class="help-block">{{ $errors->first('Answer') }}</p> @endif
			</div>
			<div class="clearfix"></div>
         </div><br/>

        <div class="">
         <?php	$final = explode(',', $faq->related_question);?>
		     {!! Form::label('Related Question', 'Related Question',['class' => 'col-md-2 text-right']) !!}
		     <div class="col-md-10" style="color:#a94442">
				<select id="framework" class="form-control testselect3" name="related_question[]" multiple >


					@foreach($question as $value)
                      @if($value->id != $faq->id)
					<option value="{{$value->id}}" @if($value->id == in_array($value->id, $final)) selected @endif >{{$value->question}}</option>
                     @endif

					@endforeach
				</select>
			</div>
			<div class="clearfix"></div>
         </div>
         <br/>



          <div class="form-group">
			  <div class="col-md-10 col-md-offset-2">
				{!! Form::submit('update', ['class' => 'btn btn-primary']) !!}
				<a href = "{{ url('admin/faq/'.$faq->section_id)}}" class = 'btn btn-danger'>Cancel</a>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
      <!-- Pagination Section-->
  </div> <!-- Panel Body -->
</div>
</div>
{!! Html::script('theme/tinymce/tinymce.min.js') !!}
{!! Html::script('theme/admin/custome/js/jquery.sumoselect.min.js') !!}
<script>
tinymce.init({
	selector: "#TBody",

	plugins: [
		"advlist autolink lists link image charmap print preview anchor","searchreplace visualblocks code fullscreen",
		"insertdatetime media table contextmenu paste jbimages"	],

	toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | jbimages",

	relative_urls: false,
    remove_script_host: false,
    convert_urls : true,
});

 $('.testselect3').SumoSelect();

</script>
@include('Admin::layouts.footer')
@stop
