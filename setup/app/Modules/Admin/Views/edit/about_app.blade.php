@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')

@section('content')

<div class="container-fluid"> 
<div class="panel panel-default">
  <div class="panel-heading"><i class="fa fa-pencil-square-o"></i>&nbsp;About App Content</div>
  <div class="panel-body">
  	<div class="row">
  		<div class="col-md-12">
		{!! Form::model('', ['method' => 'POST', 'url' => ['admin/update_about_app',$aboutApp[0]->_id]]) !!}
		  <div class="form-group {{ $errors->has('Phone') ? ' has-error' : '' }}"  >
		     {!! Form::label('phone', 'Phone',['class' => 'col-md-2 text-right']) !!}
		     <div class="col-md-4">
				{!! Form::text('Phone', $aboutApp[0]->Content['Phone'], ['class'=>'form-control', 'placeholder'=> 'phone','id' =>'inputError1']) !!}
				@if ($errors->has('Phone')) <p class="help-block">{{ $errors->first('Phone') }}</p> @endif
			</div>   
		     <div class="clearfix"></div>
         </div>
         <div class="form-group {{ $errors->has('Website') ? ' has-error' : '' }}"  >
		     {!! Form::label('website', 'Website',['class' => 'col-md-2 text-right']) !!}
		     <div class="col-md-4">
				{!! Form::text('Website', $aboutApp[0]->Content['Website'], ['class'=>'form-control', 'placeholder'=> 'Website','id' =>'inputError1']) !!}
				@if ($errors->has('Website')) <p class="help-block">{{ $errors->first('Website') }}</p> @endif
			</div>   
		     <div class="clearfix"></div>
         </div>
         <div class="form-group {{ $errors->has('Email') ? ' has-error' : '' }}"  >
		     {!! Form::label('email', 'Email',['class' => 'col-md-2 text-right']) !!}
		     <div class="col-md-4">
				{!! Form::text('Email', $aboutApp[0]->Content['Email'], ['class'=>'form-control', 'placeholder'=> 'Email']) !!}
				@if ($errors->has('Email')) <p class="help-block">{{ $errors->first('Email') }}</p> @endif
			</div>   
		     <div class="clearfix"></div>
         </div>
         <div class="form-group {{ $errors->has('Copyright') ? ' has-error' : '' }}"  >
		     {!! Form::label('copyright', 'Copyright',['class' => 'col-md-2 text-right']) !!}
		     <div class="col-md-4">
				{!! Form::text('Copyright', $aboutApp[0]->Content['Copyright'], ['class'=>'form-control', 'placeholder'=> 'Copyright']) !!}
				@if ($errors->has('Copyright')) <p class="help-block">{{ $errors->first('Copyright') }}</p> @endif
			</div>   
		     <div class="clearfix"></div>
         </div>
         <div class="form-group {{ $errors->has('Address') ? ' has-error' : '' }}"  >
		     {!! Form::label('address', 'Address',['class' => 'col-md-2 text-right']) !!}
		     <div class="col-md-4">
				{!! Form::text('Address', $aboutApp[0]->Content['Address'], ['class'=>'form-control', 'placeholder'=> 'Address']) !!}
				@if ($errors->has('Address')) <p class="help-block">{{ $errors->first('Address') }}</p> @endif
			</div>   
		     <div class="clearfix"></div>
         </div>
          <div class="form-group">
			  <div class="col-md-4 col-md-offset-2">
				{!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
				{!! Form::close() !!}
			</div>       
		</div>
	</div>
</div>
  
  </div> <!-- Panel Body -->
</div>
</div>
@include('Admin::layouts.footer')
@stop
