@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')

@section('content')

<div class="container-fluid">
<div class="panel panel-default">
  <div class="panel-heading"><i class="fa fa-pencil-square-o"></i>&nbsp;<b>Edit City</b></div>
  <div class="panel-body">
  	<div class="row">
  		<div class="col-md-12">
	{!! Form::model('', ['name' => 'cityForm', 'id' => 'cityForm', 'method' => 'POST', 'url' => 'admin/update_city/'.$lists->_id.'?id='.Input::get('state')]) !!}
	<div class="form-group">
		{!! Form::label('Select State', 'Select State',['class' => 'col-md-2 control-label']) !!}
		<div class="col-md-4" >
		<select name ="state" class="form-control" >
			<?php foreach ($state as $val) {?>
			<option value='{"id":"<?php echo $val->_id; ?>","name":"<?php echo $val->Content; ?>"}'  <?php if ($val->Content == $lists->SuperName) {echo 'Selected';}?> > <?php echo $val->Content; ?></option>
			<?php }?>
		</select>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="form-group">
		{!! Form::label('City Name', 'City Name',['class' => 'col-md-2 control-label']) !!}

		<div class="col-md-4" >
		{!! Form::text('Content', $lists->Content, ['class'=>'form-control', 'placeholder'=> 'City Name','id' =>'Content']) !!}
			<p class="help-block" id="city_msg" style="display: none;">City field is required.</p>
		</div>
		<div class="clearfix"></div>
	</div>

	<div class="form-group">
		{!! Form::label('Status', 'Status',['class' => 'col-md-2 control-label']) !!}
		<div class="col-md-4 ">
		 <?php if ($lists->Status == 'Active') {?>
           <input type="radio" name="city_status" value="Active" checked> Active &nbsp;
           <input type="radio" name="city_status" value="Inactive"> Inactive
           <?php } else {?>
           <input type="radio" name="city_status" value="Active"> Active &nbsp;
           <input type="radio" name="city_status" value="Inactive" checked> Inactive
           <?php }?>
		</div>
		<div class="clearfix"></div>
	</div>

	<div class="form-group">
		{!! Form::label('Sinup Status', 'Sinup Status',['class' => 'col-md-2 control-label']) !!}
		<div class="col-md-4 ">
		   <?php if ($lists->Signup == true) {?>
	           <input type="radio" name="city_sinup_status" value="active" checked> Active &nbsp;
	           <input type="radio" name="city_sinup_status" value="inactive"> Inactive
	           <?php } else {?>
	           <input type="radio" name="city_sinup_status" value="active" > Active &nbsp;
	           <input type="radio" name="city_sinup_status" value="inactive" checked> Inactive
	           <?php }?>
		</div>
		<div class="clearfix"></div>
	</div>

	<div class="form-group">
	<div class="col-md-4 col-md-offset-2">
	{!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
	<a href = "{{url('admin/dashboard')}}" class = 'btn btn-danger'>Cancel</a>

	</div>
	{!! Form::close() !!}
	</div>
</div>
</div>

      <!-- Pagination Section-->

  </div> <!-- Panel Body -->
</div>


</div>
<div class="clearfix"></div>

@include('Admin::layouts.footer')
@stop
