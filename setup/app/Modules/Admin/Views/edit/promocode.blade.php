@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')

@section('content')

{!! Html::style('theme/date-time-picker/jquery.datetimepicker.css') !!}
{!! Html::script('theme/date-time-picker/jquery.datetimepicker.js') !!}


<div class="container-fluid">
<div class="panel panel-default">
  <div class="panel-heading"><i class="fa fa-pencil-square-o"></i> <b>Edit Promocode</b></div>
  <div class="panel-body">

  		<div class="row">
  		<div class="col-md-8">

		{!! Form::model($info, ['name'=>'validate_form','method' => 'POST', 'url' => ['admin/update-promocode',$info->_id]])!!}

     	  <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}" >
		     {!! Form::label('title', 'Title',['class' => 'col-md-4 text-right']) !!}
		     <div class="col-md-6 text-color">
				{!! Form::text('title',$info->Title, ['class'=>'form-control required ', 'placeholder'=> 'Title','id' =>'inputError1']) !!}
				@if ($errors->has('title')) <p class="help-block">{{ $errors->first('title') }}</p> @endif
			</div>
			 <div class="clearfix"></div>
			 </div>
			 <div class="form-group {{ $errors->has('code') ? ' has-error' : '' }}"  >
		     {!! Form::label('code', 'Code',['class' => 'col-md-4 text-right']) !!}
		     <div class="col-md-6 text-color">
				{!! Form::text('code', $info->Code, ['class'=>'form-control required', 'placeholder'=> 'Code','id' =>'inputError1']) !!}
				@if ($errors->has('code')) <p class="help-block">{{ $errors->first('code') }}</p> @endif
			</div>
			 <div class="clearfix"></div>
			 </div>
			 <div class="form-group {{ $errors->has('minimumorderprice') ? ' has-error' : '' }}"  >
		     {!! Form::label('minimumorderprice', 'Minimum Order Price',['class' => 'col-md-4 text-right']) !!}
		     <div class="col-md-6 text-color">
				{!! Form::text('minimumorderprice', $info->MinimumOrderPrice, ['class'=>'form-control required usename-#minimum order prize#', 'placeholder'=> 'Minimum Order Price','id' =>'inputError1']) !!}
				@if ($errors->has('minimumorderprice')) <p class="help-block">{{ $errors->first('minimumorderprice') }}</p> @endif
			</div>
			 <div class="clearfix"></div>
			 </div>
			<div class="form-group {{ $errors->has('maximumDiscount') ? ' has-error' : '' }}"  >
		     {!! Form::label('maximumDiscount', 'Maximum Discount',['class' => 'col-md-4 text-right']) !!}
		     <div class="col-md-6 text-color">
				{!! Form::text('maximumDiscount', $info->MaximumDiscount, ['class'=>'form-control required usename-#maximum discount#', 'placeholder'=> 'Maximum Discount','id' =>'inputError1']) !!}
				@if ($errors->has('maximumDiscount')) <p class="help-block">{{ $errors->first('maximumDiscount') }}</p> @endif
			</div>
			 <div class="clearfix"></div>
			 </div>

			 <div class="form-group {{ $errors->has('DiscountAmount') ? ' has-error' : '' }}">
			  {!! Form::label('DiscountAmount', 'Discount Type/Discount Amount',['class' => 'col-md-4 text-right']) !!}
			   <div class="col-md-2 text-color">
				   			{!! Form::select('DiscountType', ['percent'=>'%','fixed'=>'$'],$info->DiscountType, ['class'=>'form-control required usename-#discount type#', 'placeholder'=> 'Maximum Discount','id' =>'inputError1']) !!}
				   </div>
		     <div class="col-md-4 text-color">
				{!! Form::text('DiscountAmount',$info->DiscountAmount, ['class'=>'form-control required usename-#discount amount#', 'placeholder'=> 'Discount Amount','id' =>'inputError1']) !!}
				@if ($errors->has('DiscountAmount')) <p class="help-block">{{ $errors->first('DiscountAmount') }}</p> @endif
			</div>
			 <div class="clearfix"></div>
			 </div>
			 <div class="form-group {{ $errors->has('validfrom') ? ' has-error' : '' }}"  >
		     {!! Form::label('validfrom', 'Valid From',['class' => 'col-md-4 text-right']) !!}
		     <div class="col-md-6 text-color"> <?php
if (isset($info->ValidFrom->sec)) {
    $info->ValidFrom = date('d/m/Y', $info->ValidFrom->sec);
}?>
				{!! Form::text ('validfrom',$info->ValidFrom, ['class'=>'form-control required usename-#date#', 'placeholder'=> 'Valid From','id' =>'startDate']) !!}
				@if ($errors->has('validfrom')) <p class="help-block">{{ $errors->first('validfrom') }}</p> @endif
			</div>
			 <div class="clearfix"></div>
			 </div>
			  <div class="form-group {{ $errors->has('validtill') ? ' has-error' : '' }}"  >
		     {!! Form::label('validtill', 'Valid Till',['class' => 'col-md-4 text-right']) !!}
		     <div class="col-md-6 text-color"><?php
if (isset($info->ValidTill->sec)) {
    $info->ValidTill = date('d/m/Y', $info->ValidTill->sec);
}?>
				{!! Form::text ('validtill', $info->ValidTill, ['class'=>'form-control usename-#date#', 'placeholder'=> 'Valid Till','id' =>'endDate']) !!}
				@if ($errors->has('validtill')) <p class="help-block">{{ $errors->first('validtill') }}</p> @endif
			</div>
			 <div class="clearfix"></div>
			 </div>
			 <div class="form-group {{ $errors->has('MaxUses') ? ' has-error' : '' }}"  >
		     {!! Form::label('MaxUses', 'Max Uses',['class' => 'col-md-4 text-right']) !!}
		     <div class="col-md-6 text-color">
				{!! Form::text('MaxUses', $info->MaxUses, ['class'=>'form-control usename-#max uses#', 'placeholder'=> 'Max Uses','id' =>'inputError1']) !!}
				@if ($errors->has('MaxUses')) <p class="help-block">{{ $errors->first('MaxUses') }}</p> @endif
			</div>
			 <div class="clearfix"></div>
			 </div>
			 <div class="form-group {{ $errors->has('MaxUsesPerPerson') ? ' has-error' : '' }}"  >
		     {!! Form::label('MaxUsesPerPerson', 'Max Uses Per Person',['class' => 'col-md-4 text-right']) !!}
		     <div class="col-md-6 text-color">
				{!! Form::text('MaxUsesPerPerson', $info->MaxUsesPerPerson, ['class'=>'form-control usename-#max user per person#', 'placeholder'=> 'Max Uses Per Person','id' =>'inputError1']) !!}
				@if ($errors->has('MaxUsesPerPerson')) <p class="help-block">{{ $errors->first('MaxUsesPerPerson') }}</p> @endif
			</div>
			 <div class="clearfix"></div>
			 </div>

			  <div class="form-group">
			  <div class="col-md-6 col-md-offset-4">
				{!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
				<a href="{{url('admin/promocode')}}" class="btn btn-primary">Cancel</a>
		     </div>
        </div>

				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
 </div>

    <!-- Pagination Section-->
  </div> <!-- Panel Body -->
</div>
</div>
{!! Html::script('theme/admin/custome/js/validate.js') !!}




<style>
.text-color
{
	color:#a94442;
}


</style>

<script>


	new Validate({
	FormName : 'validate_form',
	ErrorLevel : 1,

});

		$(function(){
		$('#startDate').datetimepicker({
			format:'d/m/Y',
			onChangeDateTime:function( ct ){
				$('#endDate').datetimepicker({ minDate:ct	})
			},
			timepicker:false,
		});

		@if(isset($info->ValidFrom->sec))
			var minDate = new Date(parseInt('{{$info->ValidFrom->sec}}') * 1000);
		@else
			var minDate = new Date();
		@endif

		$('#endDate').datetimepicker({
			format:'d/m/Y',
			minDate : minDate,
			onChangeDateTime:function( ct ){
				$('#startDate').datetimepicker({ maxDate:ct	})
			},
			timepicker:false,
		});
	});

</script>

{!! HTML::script('theme/tinymce/tinymce.min.js') !!}
<script>
tinymce.init({
	selector: "#TBody",

	plugins: [
		"advlist autolink lists link image charmap print preview anchor","searchreplace visualblocks code fullscreen",
		"insertdatetime media table contextmenu paste jbimages"	],

	toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | jbimages",

	relative_urls: false
});
</script>

@include('Admin::layouts.footer')
@stop
