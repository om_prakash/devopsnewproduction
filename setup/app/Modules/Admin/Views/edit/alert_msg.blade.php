@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')

@section('content')

<div class="container-fluid">
	<div class="panel panel-default">
	  	<div class="panel-heading"><i class="fa fa-pencil-square-o"></i>&nbsp;Alert Message</div>
	  	<div class="panel-body">
		  	<div class="row">
		  		<div class="col-md-12">
				{!! Form::model('', ['method' => 'POST', 'url' => ['admin/update-alert-msg']]) !!}
				  	<div class="form-group {{ $errors->has('Status') ? ' has-error' : '' }}"  >
				    	{!! Form::label('Status','Status',['class' => 'col-md-2 text-right']) !!}
				     	<div class="col-md-4">
				     		<div class="col-md-4">
							<input type="radio" name="Status" value="on" id="inputError1" placeholder="On" <?php echo ($alertMsg->status == 'on') ? 'checked' : '' ?>> On </div>
							<div class="col-md-4">
							<input type="radio" name="Status" value="off" id="inputError1" placeholder="Off" <?php echo ($alertMsg->status == 'off') ? 'checked' : '' ?>> Off </div>
							@if ($errors->has('Status')) <p class="help-block">{{ $errors->first('Status') }}</p> @endif
						</div>
				    	<div class="clearfix"></div>
		         	</div>
		         	<div class="form-group {{ $errors->has('Content') ? ' has-error' : '' }}"  >
				     	{!! Form::label('content','Content',['class' => 'col-md-2 text-right']) !!}
						<div class="col-md-8">
							{!! Form::textarea('Content', $alertMsg->content, ['class'=>'form-control', 'placeholder'=> 'Content','id' =>'inputError1','rows'=>3,'columns'=>4]) !!}
							@if ($errors->has('Content')) <p class="help-block">{{ $errors->first('Content') }}</p> @endif
						</div>
				     	<div class="clearfix"></div>
		         	</div>

		          	<div class="form-group">
					  	<div class="col-md-4 col-md-offset-2">
							{!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
							{!! Form::close() !!}
						</div>
					</div>
				</div>
			</div>
	  	</div> <!-- Panel Body -->
	</div>
</div>
@include('Admin::layouts.footer')
@stop