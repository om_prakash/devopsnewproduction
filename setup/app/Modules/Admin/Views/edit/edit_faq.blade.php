@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')

@section('content')

<div class="container-fluid"> 
<div class="panel panel-default">
  <div class="panel-heading"><i class="fa fa-pencil-square-o"></i>&nbsp;{{$edit}}</div>
  <div class="panel-body">
 
  	<div class="row">
  		<div class="col-md-12">
		{!! Form::model($info, ['method' => 'POST', 'url' => ['admin/update_faq', $info->_id]]) !!}
		
     	  <div class="form-group {{ $errors->has('Question') ? ' has-error' : '' }}"  >
		     {!! Form::label('Question', '',['class' => 'col-md-2 text-right']) !!}
		     <div class="col-md-10">
				{!! Form::textarea('Question', $info->Question, ['class'=>'form-control', 'placeholder'=> 'Question','id' =>'inputError1','rows'=>'2']) !!}
				@if ($errors->has('Question')) <p class="help-block">{{ $errors->first('Question') }}</p> @endif
			</div>
		     
		     <div class="clearfix"></div>
         </div>
         <div class="form-group {{ $errors->has('Answer') ? ' has-error' : '' }}">
		     {!! Form::label('Answer', '',['class' => 'col-md-2 text-right']) !!}
		     <div class="col-md-10">
				{!! Form::textarea('Answer', $info->Answer, ['class'=>'form-control', 'placeholder'=> 'Answer','id'=>'TBody']) !!}
				@if ($errors->has('Answer')) <p class="help-block">{{ $errors->first('Answer') }}</p> @endif
			</div>
			<div class="clearfix"></div>
         </div>
          <div class="form-group">
			  <div class="col-md-10 col-md-offset-2">
				{!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
				<a href = "{{URL::previous()}}" class = 'btn btn-danger'>Cancel</a>
				{!! Form::close() !!}
			</div>       
		</div>
	</div>
</div>
      <!-- Pagination Section-->
  </div> <!-- Panel Body -->
</div>
</div>
{!! Html::script('theme/tinymce/tinymce.min.js') !!}
<script>
tinymce.init({
	selector: "#TBody",

	plugins: [
		"advlist autolink lists link image charmap print preview anchor","searchreplace visualblocks code fullscreen",
		"insertdatetime media table contextmenu paste jbimages"	],
		
	toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | jbimages",

	relative_urls: false
});
</script>

@include('Admin::layouts.footer')
@stop
