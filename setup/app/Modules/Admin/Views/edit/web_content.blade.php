@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')

@section('content')

<div class="container-fluid"> 
<div class="panel panel-default">
  <div class="panel-heading"><i class="fa fa-pencil-square-o"></i>&nbsp;Edit Web Content</div>
  <div class="panel-body">
 
  	<div class="row">
  		<div class="col-md-12">
		{!! Form::model($info, ['name'=>'edit_web_content','method' => 'POST', 'url' => ['admin/update_web_content', $info->_id]]) !!}
		  		  	
     	  <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}"  >
		     {!! Form::label('title', 'Title',['class' => 'col-md-2 text-right']) !!}
		     <div class="col-md-10">
				{!! Form::text('title', $info->Title, ['class'=>'form-control', 'placeholder'=> 'Title','id' =>'inputError1']) !!}
				@if ($errors->has('title')) <p class="help-block">{{ $errors->first('title') }}</p> @endif
			</div>
		     
		     <div class="clearfix"></div>
         </div>
         <div class="form-group {{ $errors->has('content') ? ' has-error' : '' }}">
		     {!! Form::label('content', 'Email Body',['class' => 'col-md-2 text-right']) !!}
		     <div class="col-md-10">
				{!! Form::textarea('content', $info->Content, ['class'=>'form-control', 'placeholder'=> 'Content','id'=>'TBody']) !!}
				@if ($errors->has('content')) <p class="help-block">{{ $errors->first('content') }}</p> @endif
			</div>
			<div class="clearfix"></div>
         </div>
          <div class="form-group">
			  <div class="col-md-10 col-md-offset-2">
				{!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
			<a href = "{{URL::previous()}}" class = 'btn btn-danger'>Cancel</a>
				{!! Form::close() !!}
			</div>       
		</div>
	</div>
</div>
 
      
      
      <!-- Pagination Section-->
    
  

  
  </div> <!-- Panel Body -->
</div>


</div>
{!! Html::script('theme/tinymce/tinymce.min.js') !!}

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&key=AIzaSyCT0uj-bffWWdOdwDp15LvrMko-OuoZUoY"></script>
{!! Html::script('theme/admin/custome/js/validate.js') !!}
{!! Html::script('theme/admin/custome/js/request.js') !!}
<script>
tinymce.init({
	selector: "#TBody",

	plugins: [
		"advlist autolink lists link image charmap print preview anchor","searchreplace visualblocks code fullscreen",
		"insertdatetime media table contextmenu paste jbimages"	],
		
	toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | jbimages",

	relative_urls: false
});


</script>
@include('Admin::layouts.footer')
@stop
