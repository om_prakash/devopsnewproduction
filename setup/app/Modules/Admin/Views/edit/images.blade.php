@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')

@section('content')
{!! Html::style('theme/admin/custome/css/sumoselect.css') !!}
<style>
#parsley-id-5{
	position: absolute;
	bottom: -34px;
	padding-left: 13px;
	list-style-type: none;
	left: 3px;
	color: RED;
	font-size: 12px;
}
.help-block {
	color:RED;
}
</style>
<div class="container-fluid">
<div class="panel panel-default">
  <div class="panel-heading"><i class="fa fa-plus"></i>&nbsp;Images</div>
  <div class="panel-body">

  	<div class="row">
  		<div class="col-md-12">
		{!! Form::model('', ['name' => 'itemForm','data-parsley-validate', 'id' => 'itemForm', 'method' => 'POST', 'url' => ['admin/post-images/'.$images->id], 'files' => true]) !!}

		 {!! Form::label('Background Image', 'Background Image',['class' => 'col-md-2 text-right']) !!}
            <div class="col-md-3" >
           	@if ($images->image != '')
                  <img class="" width="100px" height="100px" src="<?php echo ImageUrl . $images->image; ?>" tag="product image" />
                @else
                  <img class="" width="100px" height="100px" src="<?php echo ImageUrl . '/no-image.jpg'; ?>"  />
               @endif
              <p> </p>
             <input type="file" name="image" id="image" >
              <p>dimensions (1500x450)</p>
             @if ($errors->has('image')) <p class="help-block">Please upload valid dimensions</p> @endif

		</div>


		 {!! Form::label('Slide Image', 'Slide Image',['class' => 'col-md-2 text-right']) !!}
            <div class="col-md-3" >
           	@if ($images->slideImage != '')
                  <img class="" width="100px" height="100px" src="<?php echo ImageUrl . $images->slideImage; ?>" tag="product image" />
                @else
                  <img class="" width="100px" height="100px" src="<?php echo ImageUrl . '/no-image.jpg'; ?>"  />
               @endif
              <p> </p>
             <input type="file" name="slideImage" id="image" >
             @if ($errors->has('slideImage')) <p class="help-block">{{ $errors->first('slideImage') }}</p> @endif

		</div>
		 <div class="clearfix"> </div><br/>

         <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
		     {!! Form::label('description', 'Description',['class' => 'col-md-2 text-right']) !!}
		     <div class="col-md-10" style="color:#a94442">
				 {!! Form::textarea('description', $images->sliderContant, ['class'=>'form-control', 'placeholder'=> 'Title','id'=>'TBody']) !!}
				@if ($errors->has('description')) <p class="help-block">{{ $errors->first('description') }}</p> @endif
				</span>
			</div>
			<div class="clearfix"></div>
         </div>


		</div>
          <div class="form-group">
			  <div class="col-md-10 col-md-offset-2">
				{!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
				<a href = "{{url('admin/silder-image')}}" class = 'btn btn-danger'>Cancel</a>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
      <!-- Pagination Section-->
  </div> <!-- Panel Body -->
</div>
</div>
   {!! Html::script('theme/tinymce/tinymce.min.js') !!}
    {!! Html::script('theme/admin/custome/css/parsley.css') !!}
	{!! Html::script('theme/admin/custome/js/parsley.min.js')!!}

<script>

tinymce.init({
	selector: "#TBody",

	plugins: [
		"advlist autolink lists link image charmap print preview anchor","searchreplace visualblocks code fullscreen",
		"insertdatetime media table contextmenu paste jbimages"	],

	toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | jbimages",

	relative_urls: false,
    remove_script_host: false,
    convert_urls : true,
});

  $('.testselect3').SumoSelect();

</script>
@include('Admin::layouts.footer')
@stop
