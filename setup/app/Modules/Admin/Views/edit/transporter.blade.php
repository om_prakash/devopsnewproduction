@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')

@section('content')
{!! Html::script('theme/admin/custome/js/validation.js') !!}
<div class="container-fluid"> 
<div class="panel panel-default">
  <div class="panel-heading"><i class="fa fa-pencil-square-o"></i>&nbsp;Edit User</div>
  <div class="panel-body">
  	<div class="row">
  		<div class="col-md-12">
			{!! Form::model($info, ['method' => 'POST','onsubmit'=>'return form_validation()', 'url' => ['admin/edit-transporter', $info->_id]]) !!}
		  		  	
     	  	<div class="form-group {{ $errors->has('firstname') ? ' has-error' : '' }}"  >
		     {!! Form::label('firstname', 'First Name',['class' => 'col-md-2 text-right']) !!}
		     	<div class="col-md-4">
					{!! Form::text('firstname', $info->FirstName, ['class'=>'form-control', 'placeholder'=> 'First name','id' =>'firstname']) !!}
					@if ($errors->has('firstname')){{ $errors->first('firstname') }}@endif <p class="help-block" id="er_firstname" style="color:#a94442"></p> 
				</div>   
		     	<div class="clearfix"></div>
         	</div>
         	<div class="form-group {{ $errors->has('lastname') ? ' has-error' : '' }}"  >
		    	 {!! Form::label('lastname', 'Last Name',['class' => 'col-md-2 text-right']) !!}
		     	<div class="col-md-4">
					{!! Form::text('lastname', $info->LastName, ['class'=>'form-control', 'placeholder'=> 'Last Name','id' =>'lastname']) !!}
					@if ($errors->has('lastname')) {{ $errors->first('lastname') }} @endif
					<p class="help-block" id="er_lastname" style="color:#a94442"></p>
				</div>   
		     	<div class="clearfix"></div>
         	</div>
         	<div class="form-group {{ ($errors->has('phone') || $errors->has('countrycode')) ? ' has-error' : '' }}"  >
		    	 {!! Form::label('phone', 'Phone Number',['class' => 'col-md-2 text-right']) !!}
		     	<div class="col-md-2">
					{!! Form::text('countrycode', $info->CountryCode, ['class'=>'form-control', 'placeholder'=> 'Country Code','id' =>'countrycode']) !!}
				<p class="help-block" id="er_countrycode" style="color:#a94442"></p>	
				</div>
		     	<div class="col-md-2">
				{!! Form::text('phone', $info->PhoneNo, ['class'=>'form-control', 'placeholder'=> 'Phone Number','id' =>'phone']) !!}
				@if ($errors->has('phone')) {{ $errors->first('phone') }} @endif
				@if ($errors->has('countrycode')) {{ $errors->first('countrycode') }} @endif
				<p class="help-block" id="er_phone" style="color:#a94442"></p>
				</div>
				<div class="clearfix"></div>
			</div>   
		    <div class="clearfix"></div>
         	<div class="form-group {{ $errors->has('ssn') ? ' has-error' : '' }}"  >
		    	 {!! Form::label('ssn', 'SSN',['class' => 'col-md-2 text-right']) !!}
		     	<div class="col-md-4">
					{!! Form::text('ssn', $info->SSN, ['class'=>'form-control', 'placeholder'=> 'SSN','id' =>'ssn']) !!}
					@if ($errors->has('ssn')) {{ $errors->first('ssn') }} @endif
					<p class="help-block" id="er_ssn" style="color:#a94442"></p>
				</div>   
		     	<div class="clearfix"></div>
         	</div>
         	<div class="form-group {{ $errors->has('address1') ? ' has-error' : '' }}"  >
		     	{!! Form::label('address1', 'Address Line-1',['class' => 'col-md-2 text-right']) !!}
		     	<div class="col-md-4">
					{!! Form::textarea('address1', $info->Street1, ['class'=>'form-control', 'placeholder'=> 'Address Line-1','rows'=>'3','id' =>'address1' ,'maxlength' => 150]) !!}
					@if ($errors->has('address1')) {{ $errors->first('address1') }} @endif
					<p class="help-block" id="er_address1" style="color:#a94442"></p>
				</div>   
		     	<div class="clearfix"></div>
         	</div>
         	<div class="form-group {{ $errors->has('address2') ? ' has-error' : '' }}"  >
		    	 {!! Form::label('address2', 'Address Line-2',['class' => 'col-md-2 text-right']) !!}
		     	<div class="col-md-4">
					{!! Form::textarea('address2', $info->Street2, ['class'=>'form-control', 'placeholder'=> 'Address Line-2','rows'=>'3','id'=>'address2','maxlength' => 150]) !!}
					@if ($errors->has('address2')) {{ $errors->first('address2') }} @endif
					<p class="help-block" id="er_address2" style="color:#a94442"></p>
				</div>   
		     	<div class="clearfix"></div>
         	</div>
         		<div class="clearfix"></div>
		   	<div class="col-md-2"></div>
	         	<div class="col-md-4  form-group{{ $errors->has('country') ? ' has-error' : '' }}">
					<select name="country" class='form-control' onchange='getState("#country","#state","#city")' id='country' >
						<option value="" data-id="">Select Country</option><?php
						foreach($state  as $key){ 
							if($key->type=='Country'){
							?>
							<option value="{{$key->Content}}" data-id="{{$key->Content}}" <?php if($info->Country == $key->Content) { echo 'selected="selected"'; } ?>>{{$key->Content}}</option>
						<?php } }?>
					</select>
					@if ($errors->has('country')) {{ $errors->first('country') }} @endif
					<p class="help-block" id="er_country" style="color:#a94442"></p>
				</div>
         	<div class="clearfix"></div>
		   	<div class="col-md-2"></div>
				<div class="col-md-2  form-group{{ $errors->has('state') ? ' has-error' : '' }}">
					<select name="state" class='form-control' onchange='getCity("#state","#city")' id='state' >
						<option value="" data-id="">Select State</option><?php
						foreach($state  as $key){ ?>
							<option value="{{$key->Content}}" data-id="{{$key->Content}}" <?php if($info->State == $key->Content) { echo 'selected="selected"'; } ?>>{{$key->Content}}</option>
						<?php } ?>
					</select>
					@if ($errors->has('state')) {{ $errors->first('state') }}@endif
					<p class="help-block" id="er_state" style="color:#a94442"></p> 
				</div>
			<div class="col-md-2  form-group{{ $errors->has('city') ? ' has-error' : '' }}">
				{!! Form::select('city',['' => 'Select City'], '',['class'=>'form-control','id'=>'city']) !!}
				@if ($errors->has('city')) {{ $errors->first('city') }}@endif
				<p class="help-block" id="er_city" style="color:#a94442"></p> 
			</div>   
		    <div class="clearfix"></div>
         	<div class="form-group {{ $errors->has('zip') ? ' has-error' : '' }}"  >
		    	 {!! Form::label('zip', 'Zip Code',['class' => 'col-md-2 text-right']) !!}
		     	<div class="col-md-4">
					{!! Form::text('zip', $info->ZipCode, ['class'=>'form-control', 'placeholder'=> 'Zip Code','id'=>'zipcode']) !!}
					@if ($errors->has('zip')) {{ $errors->first('zip') }}@endif
					<p class="help-block" id="er_zipcode" style="color:#a94442"></p> 
				</div>   
		     	<div class="clearfix"></div>
         	</div>
          	<div class="form-group">
			  	<div class="col-md-4 col-md-offset-2">
					{!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
					
					<a href = "{{ url('admin/transporter') }}" class = 'btn btn-danger'>Cancel</a>
					{!! Form::close() !!}
				</div>       
			</div>
	</div>
</div>
  
  </div> <!-- Panel Body -->
</div>
</div>

<script>

function form_validation()
{
	var flag = true;
	if(valid.required('firstname','first name') == false){ flag = false; }
	if(valid.required('lastname','last name') == false){ flag = false; }
	if(valid.required('countrycode','country code') == false){ flag = false; }
	if(valid.required('phone','phone number') == false){ flag = false; }
	if(valid.required('ssn','ssn number') == false){ flag = false; }
	if(valid.required('address1','address') == false){ flag = false; }
	if(valid.required('address2','address') == false){ flag = false; }
	if(valid.required('country','country') == false){ flag = false; }
	if(valid.required('state','state') == false){ flag = false; }
	if(valid.required('city','city') == false){ flag = false; }
	if(valid.required('zipcode','zipcode') == false){ flag = false; }
	
	return flag;
}

function getState($country,$state)
{
	var Country 	= '{{$info->Country}}';
	var url 		= SITEURL+'ajax/getState';
	var countryid 	= $($country).find(':selected').attr('data-id');
	$.ajax
	({
		type	: "get",
		url     : url,
		data    : {superid:countryid},
		headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		success : function(res)
		{					
			var obj = JSON.parse(res);
			if(obj.success == 1){
				$($state).html(obj.html);
				$('#city').html('<option ="">Select City</option>');
			}

		}
	});
}
function getCity($state,$city)
{ 
	var city = '{{$info->City}}';
	var url = SITEURL+'ajax/getCity';
	var stateid = $($state).find(':selected').attr('data-id');
	$.ajax
	({ 
		type: "get",
		url: url,
		data: {superid:stateid,city:city},
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		success: function(res)
		{					
			var obj = JSON.parse(res);
			if(obj.success == 1){
				$($city).html(obj.html);
			}
		}
	});
}
getCity('#state','#city');
</script>
@include('Admin::layouts.footer')
@stop
