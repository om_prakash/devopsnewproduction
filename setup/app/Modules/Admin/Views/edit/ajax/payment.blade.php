@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')
@section('content')

{!! Html::script('theme/admin/custome/js/validation.js') !!}

<div class="container">
   <h3 class="text-primary">Transaction</h3>

     <div class="box-shadow" style="padding-top: 15px; padding-bottom: 10px">

         <div class="col-sm-4">
          <label>Package Id: </label>
           {{@$delivery->PackageNumber}}
         </div>

          <div class="col-sm-4">
          <label>Requester Name: </label>
          {{@$delivery->RequesterName}}
         </div>

          <div class="col-sm-4">
          <label>Product Title: </label>
           {{@$delivery->ProductTitle}}
         </div>

       </div>


   <div class="box-shadow">
   <br><br>
  {!! Form::model('', ['name' => 'payment', 'id' => 'payment', 'method' => 'POST']) !!}
    <input type="hidden" name="promocode" value="{{ Input::get('promocode') }}">
    <input type="hidden" name="request_type" value="{{ Input::get('request_type') }}">

  <div class="col-sm-8">
    @if($delivery->RequestType == 'local_delivery')
      <p>Please fill the transaction id and description if you have or <a href="{{ url('admin/local-delivery-detail/'.Request::segment(3)) }}">Skip</a>.</p>
    @else
      <p>Please fill the transaction id and description if you have or <a href="{{ url('admin/package/detail/'.Request::segment(3)) }}">Skip</a>.</p>
    @endif

    
  <div class="form-group">
    <label for="transaction_id">Transaction ID</label>
    <input type="text" class="form-control required" id="transaction_id" name="transaction_id" placeholder="Transaction Id">
    <p class="help-block red" style="color:red;"  >{{$errors->first('transaction_id')}} </p>
  </div>
  </div>
  <div class="col-sm-8">
  <div class="form-group">
    <label for="description">Description</label>
    <textarea row="5" class="form-control required" id="description" name="description" placeholder="Description"> </textarea>
    <p class="help-block red" style="color:red;"  >{{$errors->first('description')}} </p>
  </div>
  </div>
  <div class="col-sm-8">
  <input type="hidden" value="{{ Request::segment(4) }}"  name="url_id">
  <button type="submit" class="btn btn-primary">Submit</button>

  @if($delivery->RequestType == 'local_delivery')
      
      <a href="{{ url('admin/local-delivery-detail/'.Request::segment(3)) }}" class="btn btn-primary">Skip</a>
    @else
      <a href="{{ url('admin/package/detail/'.Request::segment(3)) }}" class="btn btn-primary">Skip</a>
    @endif

  

    <a href="{{ url('admin/request-card-list/'.Request::segment(3)) }}?promocode={{@Input::get('promocode')}}" class="btn btn-primary">pay via card</a>

  <a href="{{ url('admin/package-mark-paid/'.Request::segment(3)) }}" class="btn btn-primary" onclick="return confirm('Are you sure you want to mark as paid.');">Mark as paid</a>

  <a href="{{ url('admin/pay-mobile-money/'.Request::segment(3)) }}" class="btn btn-primary" >Pay by Mobile Money</a>
  </div>
  <div class="clearfix"></div>
 {!! Form::close() !!}
</div>
</div>
@include('Admin::layouts.footer')

<script>
new Validate({
   FormName :  'payment',
   ErrorLevel : 1,

    });
</script>
@endsection
