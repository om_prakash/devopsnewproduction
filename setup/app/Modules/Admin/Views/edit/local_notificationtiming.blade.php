@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')

@section('content')

<div class="container-fluid"> 
<div class="panel panel-default">
  <div class="panel-heading"><i class="fa fa-pencil-square-o"></i>&nbsp;Local Notification Timing Managment</div>
  <div class="panel-body">
  	<div class="row">
  		<div class="col-md-12">
		{!! Form::model('', ['method' => 'POST', 'url' => ['admin/update_about_app',$aboutApp[0]->_id]]) !!}
		  <div class="form-group {{ $errors->has('Phone') ? ' has-error' : '' }}"  >
		     {!! Form::label('phone', 'M1 Timing',['class' => 'col-md-2 text-right']) !!}
		     
			 <div class="col-md-4">
				{!! Form::text('Phone', $aboutApp[0]->Content['Phone'], ['class'=>'form-control', 'placeholder'=> 'phone','id' =>'inputError1']) !!}
				<span>(In minutes)</span>
				@if ($errors->has('Phone')) <p class="help-block">{{ $errors->first('Phone') }}</p> @endif
			</div>   
		     <div class="clearfix"></div>
         </div>
         <div class="form-group {{ $errors->has('Website') ? ' has-error' : '' }}"  >
		     {!! Form::label('website', 'M1 Message',['class' => 'col-md-2 text-right']) !!}
		     <div class="col-md-4">
				{!! Form::text('Website', $aboutApp[0]->Content['Website'], ['class'=>'form-control', 'placeholder'=> 'Website','id' =>'inputError1']) !!}
				
				@if ($errors->has('Website')) <p class="help-block">{{ $errors->first('Website') }}</p> @endif
			</div>   
		     <div class="clearfix"></div>
         </div>
         <div class="form-group {{ $errors->has('Phone') ? ' has-error' : '' }}"  >
		     {!! Form::label('phone', 'M2 Timing',['class' => 'col-md-2 text-right']) !!}
		     
			 <div class="col-md-4">
				{!! Form::text('Phone', $aboutApp[0]->Content['Phone'], ['class'=>'form-control', 'placeholder'=> 'phone','id' =>'inputError1']) !!}
				<span>(In minutes)</span>
				@if ($errors->has('Phone')) <p class="help-block">{{ $errors->first('Phone') }}</p> @endif
			</div>   
		     <div class="clearfix"></div>
         </div>
         <div class="form-group {{ $errors->has('Website') ? ' has-error' : '' }}"  >
		     {!! Form::label('website', 'M2 Message',['class' => 'col-md-2 text-right']) !!}
		     <div class="col-md-4">
				{!! Form::text('Website', $aboutApp[0]->Content['Website'], ['class'=>'form-control', 'placeholder'=> 'Website','id' =>'inputError1']) !!}
				
				@if ($errors->has('Website')) <p class="help-block">{{ $errors->first('Website') }}</p> @endif
			</div>   
		     <div class="clearfix"></div>
         </div>
         <div class="form-group {{ $errors->has('Phone') ? ' has-error' : '' }}"  >
		     {!! Form::label('phone', 'M3 Timing',['class' => 'col-md-2 text-right']) !!}
		     
			 <div class="col-md-4">
				{!! Form::text('Phone', $aboutApp[0]->Content['Phone'], ['class'=>'form-control', 'placeholder'=> 'phone','id' =>'inputError1']) !!}
				<span>(In minutes)</span>
				@if ($errors->has('Phone')) <p class="help-block">{{ $errors->first('Phone') }}</p> @endif
			</div>   
		     <div class="clearfix"></div>
         </div>
         <div class="form-group {{ $errors->has('Website') ? ' has-error' : '' }}"  >
		     {!! Form::label('website', 'M3 Message',['class' => 'col-md-2 text-right']) !!}
		     <div class="col-md-4">
				{!! Form::text('Website', $aboutApp[0]->Content['Website'], ['class'=>'form-control', 'placeholder'=> 'Website','id' =>'inputError1']) !!}
				
				@if ($errors->has('Website')) <p class="help-block">{{ $errors->first('Website') }}</p> @endif
			</div>   
		     <div class="clearfix"></div>
         </div>
         <div class="form-group {{ $errors->has('Phone') ? ' has-error' : '' }}"  >
		     {!! Form::label('phone', 'M4 Timing',['class' => 'col-md-2 text-right']) !!}
		     
			 <div class="col-md-4">
				{!! Form::text('Phone', $aboutApp[0]->Content['Phone'], ['class'=>'form-control', 'placeholder'=> 'phone','id' =>'inputError1']) !!}
				<span>(In minutes)</span>
				@if ($errors->has('Phone')) <p class="help-block">{{ $errors->first('Phone') }}</p> @endif
			</div>   
		     <div class="clearfix"></div>
         </div>
         <div class="form-group {{ $errors->has('Website') ? ' has-error' : '' }}"  >
		     {!! Form::label('website', 'M4 Message',['class' => 'col-md-2 text-right']) !!}
		     <div class="col-md-4">
				{!! Form::text('Website', $aboutApp[0]->Content['Website'], ['class'=>'form-control', 'placeholder'=> 'Website','id' =>'inputError1']) !!}
				
				@if ($errors->has('Website')) <p class="help-block">{{ $errors->first('Website') }}</p> @endif
			</div>   
		     <div class="clearfix"></div>
         </div>
          <div class="form-group">
			  <div class="col-md-4 col-md-offset-2">
				{!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
				
				{!! Form::close() !!}
			</div>       
		</div>
	</div>
</div>
  
  </div> <!-- Panel Body -->
</div>
</div>

@include('Admin::layouts.footer')
@stop
