@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')

@section('content')
{!! Html::style('theme/dropdown_checklist/jquery-ui-1.11.2.custom.css') !!}
{!! Html::script('theme/dropdown_checklist/jquery-ui-1.11.2.custom.min.js') !!}
{!! Html::script('theme/dropdown_checklist/ui.dropdownchecklist-1.5-min.js') !!}

<div class="container-fluid">
<div class="panel panel-default">
  <div class="panel-heading"><i class="fa fa-pencil-square-o"></i>&nbsp;Edit Individual Trip</div>
  <div class="panel-body">
  	<div class="row">
  		<div class="col-md-12">
		{!! Form::model($info, ['name'=>'edit_individual_trip','method' => 'POST', 'url' => ['admin/edit-individual-trip', $info->_id]]) !!}

     	  	<div class="form-group {{ $errors->has('TransporterName') ? ' has-error' : '' }}"  >
		     {!! Form::label('TransporterName', 'Transporter Name',['class' => 'col-md-2 text-right']) !!}
		     	<div class="col-md-4" style="color:#a94442">
				{!! Form::text('TransporterName', $info->TransporterName, ['class'=>'form-control required', 'placeholder'=> 'Transporter Name','id' =>'inputError1']) !!}
				@if ($errors->has('TransporterName')) <p class="help-block" >{{ $errors->first('TransporterName') }}</p> @endif
				</div>
		     	<div class="clearfix"></div>
         	</div>


	        <div class="form-group {{ $errors->has('SourceAddress') ? ' has-error' : '' }}"  >
			     {!! Form::label('SourceAddress', 'Source Address',['class' => 'col-md-2 text-right']) !!}
			     <div class="col-md-4" style="color:#a94442">
					{!! Form::text('SourceAddress', $info->SourceAddress, ['class'=>'form-control required', 'placeholder'=> 'Source Address']) !!}
					@if ($errors->has('SourceAddress')) <p class="help-block">{{ $errors->first('SourceAddress') }}</p> @endif
				</div>
			     <div class="clearfix"></div>
	        </div>


         <div class="form-group {{ $errors->has('SourceCountry') ? ' has-error' : '' }}"  >
		     {!! Form::label('SourceCountry', 'Source Country',['class' => 'col-md-2 text-right']) !!}
		     <div class="col-md-4" style="color:#a94442">


				<select name="SourceCountry" class="form-control required usename-#country#" id="pp_nd_return_country" onchange="get_state('pp_nd_return_country','SourceState','SourceCity','SourceState','{{$info->SourceState}}','{{$info->SourceCity}}')">

		                            <option value="">Select Country</option>
		                            @foreach($country as $key)
		                             <option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}","state_available":"{{$key->state_available}}"}' @if($info->SourceCountry == $key->Content) selected='selected' @endif>{{$key->Content}}</option>
		                            @endforeach
		             </select>
				@if ($errors->has('SourceCountry')) <p class="help-block">{{ $errors->first('SourceCountry') }}</p> @endif
			</div>
		     <div class="clearfix"></div>
		     </div>
		     <div class="col-md-2"></div>
				<div class="col-md-2  form-group{{ $errors->has('state') ? ' has-error' : '' }}">

					<select name="SourceState" class="form-control required usename-#state#" id="SourceState" onchange="get_city('SourceState','SourceCity','SourceCity','{{$info->SourceCity}}')">

	                            <option value="">Select State</option>
	                </select>
					@if ($errors->has('SourceState')) <p class="help-block">{{ $errors->first('SourceState') }}</p> @endif
				</div>
			<div class="col-md-2  form-group{{ $errors->has('city') ? ' has-error' : '' }}">


				<select  name="SourceCity" class="form-control required usename-#city#" id="SourceCity">
	                   <option value="{{$info->SourceState}}">Select City</option>
	            </select>
				@if ($errors->has('SourceCity')) <p class="help-block">{{ $errors->first('city') }}</p> @endif
			</div>
		    <div class="clearfix"></div>

         <div class="form-group {{ $errors->has('DestiAddress') ? ' has-error' : '' }}"  >
		     {!! Form::label('DestiAddress', 'Destination Address',['class' => 'col-md-2 text-right']) !!}
		     <div class="col-md-4" style="color:#a94442">
				{!! Form::text('DestiAddress', $info->DestiAddress, ['class'=>'form-control required', 'placeholder'=> 'DestiAddress']) !!}
				@if ($errors->has('DestiAddress')) <p class="help-block">{{ $errors->first('DestiAddress') }}</p> @endif
			</div>
		     <div class="clearfix"></div>
         </div>
         <div class="form-group {{ $errors->has('DestiCountry') ? ' has-error' : '' }}"  >
		     {!! Form::label('DestiCountry', 'Destination Country',['class' => 'col-md-2 text-right']) !!}
		     <div class="col-md-4" style="color:#a94442">

				<select name="nd_return_country" class="form-control required usename-#country#" id="pp_nd_return_country" onchange="get_state('pp_nd_return_country','pp_dropoff_state','pp_nd_return_city','pp_nd_return_state','{{$info->DestiState}}','{{$info->DestiCity}}')">



		            <option value="">Select Country</option>
                        @foreach($country as $key)

                         <option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}","state_available":"{{$key->state_available}}"}'
                         @if($info->DestiCountry == $key->Content) selected='selected' @endif
                         >{{$key->Content}} </option>
                        @endforeach
		        </select>
				@if ($errors->has('DestiCountry')) <p class="help-block">{{ $errors->first('DestiCountry') }}</p> @endif
			</div>
		     <div class="clearfix"></div>
		     </div>
		     <div class="col-md-2"></div>
				<div class="col-md-2  form-group{{ $errors->has('state') ? ' has-error' : '' }}">

				<select name="drop_off_state" class="form-control required usename-#state#" id="pp_dropoff_state" onchange="get_city('pp_dropoff_state','pp_dropoff_city','pp_dropoff_city','{{$info->DestiCity}}')">

	                            <option value="">Select State</option>
	            </select>
					@if ($errors->has('DestiState')) <p class="help-block">{{ $errors->first('DestiState') }}</p> @endif
				</div>
			<div class="col-md-2  form-group{{ $errors->has('city') ? ' has-error' : '' }}">
				<select  name="drop_off_city" class="form-control required usename-#city#" id="pp_dropoff_city">
	                            <option value="">Select State</option>
	                        </select>
				@if ($errors->has('DestiCity')) <p class="help-block">{{ $errors->first('DestiCity') }}</p> @endif
			</div>
		    <div class="clearfix"></div>

         <div class="form-group {{ $errors->has('Weight') ? ' has-error' : '' }}"  >
		     {!! Form::label('Weight', 'Weight',['class' => 'col-md-2 text-right']) !!}
		     <div class="col-md-4" style="color:#a94442">
				{!! Form::text('Weight', $info->Weight, ['class'=>'form-control required', 'placeholder'=> 'Weight']) !!}
				@if ($errors->has('Weight')) <p class="help-block">{{ $errors->first('Weight') }}</p> @endif
			</div>
		     <div class="clearfix"></div>
         </div>
          	<div class="form-group {{ $errors->has('Categories') ? ' has-error' : '' }}">
			{!! Form::label('Travelmode', 'Shipping Mode',['class' => 'col-md-2 text-right']) !!}
				<div class="col-md-6">
					<div class="col-md-2">
					   {!! Form::radio('TravelMode', 'air','',['onclick'=>'yescheck()']) !!}
					   {!! Form::label('Byair', 'By air') !!}
					</div>

					<div class="col-md-2">
				    {!! Form::radio('TravelMode', 'ship','',['onclick'=>'yescheck()']) !!}
				    {!! Form::label('Byship', 'By sea') !!}
	              	</div>
              	</div>
              	<div class="clearfix"></div>
            </div>

			<div class="form-group {{ $errors->has('Categories') ? ' has-error' : '' }}"  >
				{!! Form::label('Categories', 'Categories',['class' => 'col-md-2 text-right']) !!}

				<div class="col-md-2" id="list1" >
					<?php $info->SelectCategory = array_map('strtolower', array_values($info->SelectCategory));?>
					<select name="aircategory[]" multiple="multiple" id='aircategory'>

						<?php foreach ($category as $key) {
    if ($key['TravelMode'] == 'air') {
        ?>
						<option value="{{$key['Content']}}" <?php if (in_array(strtolower($key['Content']), $info->SelectCategory)) {echo 'selected="selected"';}?> >{{ucfirst($key['Content'])}}</option>
						<?php }}?>
					</select>

				</div>


				<div class="col-md-2" id='list2' >
					<select name="shipcategory[]" multiple="true" id='shipcategory'>
						<option value=""></option>
						<?php foreach ($category as $key) {

    if ($key['TravelMode'] == 'ship') {
        ?>
						<option value="{{$key['Content']}}" <?php if (in_array(strtolower($key['Content']), $info->SelectCategory)) {echo 'selected="selected"';}?> >{{ucfirst($key['Content'])}}</option>
						<?php }}?>
					</select>

				</div>

				<div class="clearfix"></div>
			</div>



			<div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}"  >
		     {!! Form::label('Description', 'Description',['class' => 'col-md-2 text-right']) !!}
		     	<div class="col-md-4" style="color:#a94442">
				<textarea name="description" class="form-control required" maxlength="500">{{$info->Description}}</textarea>

				@if ($errors->has('description')) <p class="help-block" >{{ $errors->first('description') }}</p> @endif
				</div>
		     	<div class="clearfix"></div>
         	</div>





          <div class="form-group">
			  <div class="col-md-4 col-md-offset-2">
				{!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
				<a href = "{{URL::previous()}}" class = 'btn btn-danger'>Cancel</a>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

  </div> <!-- Panel Body -->
</div>
</div>

{!! Html::script('theme/web/js/utility.js') !!}
 <script type="text/javascript">
$('#SourceCountry').trigger('change');
$('#pp_nd_return_country').trigger('change');

get_state('pp_nd_return_country','SourceState','SourceCity','SourceState','{{$info->SourceState}}','{{$info->SourceCity}}');
get_state('pp_nd_return_country','pp_dropoff_state','pp_nd_return_city','pp_nd_return_state','{{$info->DestiState}}','{{$info->DestiCity}}');

var air = 0;
var ship = 0;
function yescheck()
{
	if($('[name="TravelMode"]:checked').val() == "air")
	{
      document.getElementById("list1").style.display = "block";
      document.getElementById("list2").style.display = "none";
      if(air == 0){
      	air = 1;
      	$("#aircategory").dropdownchecklist( {icon: {}, width: 150 } );
      }
    }
 else{
      document.getElementById("list2").style.display = "block";
      document.getElementById("list1").style.display = "none";
      if(ship == 0){
      	ship = 1;
      	$("#shipcategory").dropdownchecklist( {icon: {}, width: 150 } );
      }
    }
}
yescheck();

</script>
<script>
function getState($country,$state,$city,state,cityname)
{
	var url 		= SITEURL+'ajax/getState';
	var countryid 	= $($country).find(':selected').attr('data-id');
	$.ajax
	({
		type	: "get",
		url     : url,
		data    : {superid:countryid,state:state},
		headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		success : function(res)
		{
			var obj = JSON.parse(res);
			if(obj.success == 1){
				$($state).html(obj.html);
				$($city).html('<option ="">Select City</option>');
				getCity($state,$city,cityname);
			}

		}
	});
}

function getCity($state,$city,cityname)
{
	var url = SITEURL+'ajax/getCity';
	var stateid = $($state).find(':selected').attr('data-id');
	if(stateid != '')
	{
		$.ajax
		({
			type: "get",
			url: url,
			data: {superid:stateid,city:cityname},
			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			success: function(res)
			{
				var obj = JSON.parse(res);
				if(obj.success == 1){
					$($city).html(obj.html);
				}
			}
		});
	}
}
getState("#country","#state","#city","{{$info->SourceState}}","{{$info->SourceCity}}");
getState("#desticountry","#destistate","#desticity","{{$info->DestiState}}","{{$info->DestiCity}}");


</script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&key=AIzaSyCT0uj-bffWWdOdwDp15LvrMko-OuoZUoY"></script>


{!! Html::script('theme/admin/custome/js/validate.js') !!}
{!! Html::script('theme/admin/custome/js/request.js') !!}

@include('Admin::layouts.footer')
@stop
