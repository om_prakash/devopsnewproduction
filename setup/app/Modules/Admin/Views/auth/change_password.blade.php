@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')
@section('content')

<div class="container-fluid"> 
<div class="panel panel-default">
  <div class="panel-heading"><i class="fa fa-pencil-square-o"></i>&nbsp;<b>Change password</b></div>
  <div class="panel-body">
 <?php
 
 foreach($errors->first as $key){
	 print_r($key);
 } ?>
  	<div class="row">
  		<div class="col-md-12">
	{!! Form::model('', ['class' => 'form-horizontal','method' => 'POST', 'url' => ['admin/change_password_post']]) !!}
	<div class="form-group">
		{!! Form::label('oldPassword', 'Old Password',['class' => 'col-md-2 control-label']) !!}
	
		<div class="col-md-4  @if ($errors->has('ConfirmPassword')) has-error @endif">
			{!! Form::password('oldPassword',  ['class'=>'form-control', 'placeholder'=> 'Old Password','id' =>'inputError1']) !!}
			@if ($errors->has('oldPassword')) <div><p class="help-block">{{ $errors->first('oldPassword') }}</p></div> @endif
		</div>
		
		<div class="clearfix"></div>
	</div>
	<div class="form-group">
		{!! Form::label('NewPassword', 'New Password',['class' => 'col-md-2 control-label']) !!}
	
		<div class="col-md-4  @if ($errors->has('ConfirmPassword')) has-error @endif">
			{!! Form::password('NewPassword',  ['class'=>'form-control', 'placeholder'=> 'New Password','id' =>'inputError1']) !!}
			@if ($errors->has('NewPassword')) <div><p class="help-block">{{ $errors->first('NewPassword') }}</p></div> @endif
		</div>
		
		<div class="clearfix"></div>
	</div>
	<div class="form-group">
		{!! Form::label('ConfirmPassword', 'Confirm Password',['class' => 'col-md-2 control-label']) !!}
	
		<div class="col-md-4  @if ($errors->has('ConfirmPassword')) has-error @endif">
			{!! Form::password('ConfirmPassword', ['class'=>'form-control', 'placeholder'=> 'Confirm Password','id' =>'inputError1']) !!}
			@if ($errors->has('ConfirmPassword')) <div><p class="help-block">{{ $errors->first('ConfirmPassword') }}</p></div> @endif
		</div>
		
		<div class="clearfix"></div>
	</div>
	<div class="form-group">
	<div class="col-md-4 col-md-offset-2">
	{!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
	<a href = "{{url('admin/dashboard')}}" class = 'btn btn-danger'>Cancel</a>
	
	</div>
	{!! Form::close() !!}
	</div>
</div>
</div>
    
      <!-- Pagination Section-->
  
  </div> <!-- Panel Body -->
</div>


</div>
<div class="clearfix"></div>
@include('Admin::layouts.footer')
@stop

