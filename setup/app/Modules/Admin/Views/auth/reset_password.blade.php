@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')
@section('content')

<div class="login-bg">
<div class="login-container col-md-6 col-sm-8 col-xs-12 col-md-offset-3 col-sm-offset-2">
<div class="row">
<div class="account-container">
<h1 class="custom-heading">Reset password</h1><br />
{!! Form::model('', ['class' => 'form-horizontal','method' => 'POST', 'url' => ['admin/reset_password',Request::segment(3),Request::segment(4)]]) !!}
<div class="form-group">
	{!! Form::label('password', 'New Password',['class' => 'col-sm-4 control-label']) !!}
	<div class="col-sm-7">
		{!! Form::password('password', ['class'=>'form-control', 'placeholder'=> 'New Password','id' =>'inputError1']) !!}
        @if ($errors->has('password')) <div><p class="help-block red">{{ $errors->first('password') }}</p></div> @endif
	</div>	
	<div class="clearfix"></div>
</div>
<div class="form-group">
	{!! Form::label('password', 'Confirm Password',['class' => 'col-sm-4 control-label']) !!}
	<div class="col-sm-7">
		{!! Form::password('conf_password', ['class'=>'form-control', 'placeholder'=> 'Confirm Password','id' =>'inputError2']) !!}
		@if ($errors->has('conf_password')) <div><p class="help-block red">{{ $errors->first('conf_password') }}</p></div> @endif
	</div>	
	<div class="clearfix"></div>
</div>

<div class="form-group">
<div class="col-sm-8 col-sm-offset-3">

{!! Form::submit('Submit', ['class' => 'btn btn-primary custom-btn']) !!}

<a href="{{url('admin/login')}}" class="btn btn-danger custom-btn">Cancel</a>

</div>
{!! Form::close() !!}

</div>
</div>
</div>
</div>
</div>


@stop
