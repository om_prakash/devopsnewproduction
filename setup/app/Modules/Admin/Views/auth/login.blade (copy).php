@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')
@section('content')
<script>
$(".navbar").removeClass("hidden-xs");
</script>

<div class="login-bg">
<div class="container-fluid" id='error_msg_section'>

{{--    @if(Session::has('loginfail'))
    <div class="alert alert-danger">
        <ul>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<strong>Fail!</strong> {{Session::get('loginfail')}}
        </ul>
    </div>
	@endif --}}
    @if(Session::has('loginsuccessmail'))
    <div class="alert alert-success">
        <ul>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong>Success!</strong> {{Session::get('loginsuccessmail')}}
        </ul>
    </div>
    @endif
</div>
    <div class="login-container col-md-6 col-sm-8 col-xs-12 col-md-offset-3 col-sm-offset-2">
        <div class="row">
            <div class="account-container">

                <h1 class="custom-heading" align="center" >Sign in</h1>
                <p align="center">Sign in using your registered account</p>
                <br />
                    @if(Session::has('loginfail'))
                     <div class="alert alert-danger">
                      <ul>
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                       <strong>Fail!</strong> {{Session::get('loginfail')}}
                         </ul>
                       </div>
                 @endif
                {!! Form::model('', ['class' => 'form-horizontal','method' => 'POST', 'url' => ['admin/login']]) !!}
                <?php
$result = '';
if (old('name') == "") {
    if (isset($_COOKIE['useremail'])) {
        $result = $_COOKIE['useremail'];
    }
} else {
    $result = old('name');
}
?>
			<?php
$resu = '';
if (old('password') == "") {
    if (isset($_COOKIE['password'])) {
        $resu = $_COOKIE['password'];
    }
} else {
    $resu = old('password');
}
?>
			<?php
$chk_result = "";
if (isset($_COOKIE['useremail']) && isset($_COOKIE['password'])) {
    $chk_result = 'checked="checked"';
}

?>

		<!--{!! Form::text('UserEmail', old('name'), ['class'=>'form-control', 'placeholder'=> 'Email']) !!}-->

                <div class="form-group">

                {!! Form::label('Email', 'Email',['class' => 'col-sm-3 control-label']) !!}

                <div class="col-sm-8">
                {!! Form::text('email', $result, ['class'=>'form-control', 'placeholder'=> 'Email','id' =>'inputError1']) !!}
                </div>
                </div>
                <div class="form-group">

                {!! Form::label('password', 'Password',['class' => 'col-sm-3 control-label']) !!}

                <div class="col-sm-8">
					{!! Form::input('password', 'password', $resu, ['class'=>'form-control', 'placeholder'=> 'Password','id' =>'inputError1']) !!}
                </div>
                </div>
                <div class="form-group">
                <div class="col-sm-3"></div>
                <div class="col-sm-8 checkbox">
                <label>
                    <input type="checkbox" tabindex="4" class="field login-checkbox margin-top" style="margin-top:1px;" name="remember" id="Field" {!! $chk_result !!} >
                    <span>Remember Me</span>
                </label>
                <p class="custom-padding"><a href="{{ url('admin/forgot_password') }}">Forgot Password?</a></p>
                </div>
                </div>
                <div class="form-group">
                <div class="col-sm-8 col-sm-offset-3">
                {!! Form::submit('Sign in', ['class' => 'btn btn-primary custom-btn']) !!}
                {!! Form::reset('Reset', ['class' => 'btn btn-primary custom-btn']) !!}
                </div>
            </div>
            {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@stop


