@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')
@section('content')



<div class="login-bg">
	<div class="container-fluid" id='error_msg_section'>

   @if(Session::has('forgotfail'))
    <div class="alert alert-danger">
        <ul>
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<strong>Fail!</strong> {{Session::get('forgotfail')}}
        </ul>
    </div>
	@endif
</div>
<div class="login-container col-md-6 col-sm-8 col-xs-12 col-md-offset-3 col-sm-offset-2">
<div class="row">
<div class="account-container">
<h1 class="custom-heading text-center">Forgot Password</h1><br />
{!! Form::model('', ['class' => 'form-horizontal','method' => 'POST', 'url' => ['admin/forgot_password']]) !!}
<div class="form-group">
	{!! Form::label('Email', 'Email',['class' => 'col-sm-3 control-label']) !!}

	<div class="col-sm-8 has-feedback {{ $errors->has('email') ? ' has-error' : '' }}">
		{!! Form::text('email', '', ['class'=>'form-control', 'placeholder'=> 'Email','id' =>'inputError1']) !!}
		<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        @if ($errors->has('email')) <div><p class="help-block red">{{ $errors->first('email') }}</p></div> @endif
	</div>

	<div class="clearfix"></div>
</div>

<div class="form-group">
<div class="col-sm-8 col-sm-offset-3">

{!! Form::submit('Submit', ['class' => 'btn btn-primary custom-btn']) !!}

<a href="{{url('admin/login')}}" class="btn btn-danger custom-btn">Back To Login</a>

</div>
{!! Form::close() !!}

</div>
</div>
</div>
</div>
</div>


@stop
