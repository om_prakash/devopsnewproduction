<?php

Route::group(array('module' => 'Admin', 'namespace' => 'App\Modules\Admin\Controllers'), function () {
    //Login
    Route::get('admin/set-datetime', 'PageController@set_datetime');
    Route::get('admin/login', 'AuthController@getLogin');
    Route::post('admin/login', 'AuthController@authenticate');
    Route::get('admin/logout', 'PageController@logout');

    //Dashboard
    Route::any('admin/dashboard', 'PageController@dashboard');
    Route::any('admin/update-address', 'PageController@update_address');

    //package section
    Route::any('admin/package', 'PageController@package');
    Route::post('pagination/package', 'PaginationController@package');
    Route::get('admin/package/detail/{id}', 'PageController@package_detail');
    Route::post('admin/accept-package', 'PageController@accept_product');
    Route::any('accept-all/{id}', 'PageController@accept_all_product');
    Route::any('cancel-request', 'PageController@cancel_request');
    Route::any('pickup', 'PageController@pickup_request');
    Route::any('request-delivery', 'PageController@delivery_request');
    Route::any('request-complete', 'PageController@complete_request');

    Route::any('assign-transporter', 'PageController@assign_transporter');
    Route::any('update-promocode/{id}', 'PageController@update_promocode');

    //online-package
    Route::any('admin/online-package', 'PageController@online_package');
    Route::post('pagination/online-package', 'PaginationController@online_package');
    Route::get('admin/online_package/detail/{id}', 'PageController@online_package_detail');

    // Buy for me
    Route::get('admin/buy-for-me', 'PageController@buy_for_me');
    Route::post('pagination/buy-for-me', 'PaginationController@buy_for_me');
    Route::get('admin/buy-for-me/detail/{id}', 'PageController@buy_for_me_detail');
    Route::post('admin/buy-for-me-process', 'PageController@buy_for_me_process');
    Route::post('admin/get_item_info', 'PageController@get_item_info');
    Route::post('admin/update_item_info', 'PageController@update_item_info');
    Route::post('admin/buy_for_me_assign_transporter', 'PageController@buy_for_me_assign_transporter');

    //User list
    Route::any('admin/requester', 'PageController@requester');
    Route::post('pagination/requester', 'PaginationController@requester');
    //Route::get('admin/edit-requester/{id}','PageController@edit_requester');
    //~ Route::post('admin/edit-requester/{id}','PageController@post_edit_requester');
    Route::get('admin/user/detail/{id}', 'PageController@users_detail');
    Route::get('admin/requester/edit/{id}', 'PageController@edit_requester');
    Route::post('admin/requester/edit/{id}', 'PageController@post_edit_requester');
    Route::post('ajax/verify_user', 'AjaxController@post_verified_user');

    // Utility
    Route::get('status_activity/{id}/{status}/{function}', 'PageController@status_activity');
    Route::get('delete_activity/{id}/{function}', 'PageController@delete_activity');
    //~ Route::get('delete_activity_minumcost/{id}/{function}/{subid}','PageController@delete_activity_minumcost');
    Route::any('ajax/getCity', 'AjaxController@city');
    Route::any('ajax/getState', 'AjaxController@state');

    //Carrier list
    Route::any('admin/transporter', 'PageController@transporter');
    Route::any('pagination/transporter-list', 'PaginationController@transporter');
    // Route::any('pagination/address-list','PaginationController@address');
    //~ Route::get('admin/carrier/detail/{id}','PageController@users_detail');
    Route::get('admin/edit-transporter/{id}', 'PageController@edit_transporter');
    Route::post('admin/edit-transporter/{id}', 'PageController@post_edit_transporter');
    Route::post('ajax/add_account_to_stripe', 'AjaxController@postadd_account_to_stripe');
    Route::get('ajax/transfer_fund_view', 'AjaxController@view_transfer_fund');
    Route::post('ajax/transfer_fund_to_carrier', 'AjaxController@transfer_fund_to_carrier');
    //~ Route::get('admin/transaction_history','PageController@transaction_history');
    //~ Route::post('pagination/transaction_history','PaginationController@transaction_history');
    Route::any('admin/requester-transaction-history', 'PageController@requester_transaction');
    Route::post('pagination/requester-transaction-history', 'PaginationController@requester_transaction');
    //~ Route::post('admin/export_carrier_transaction','PageController@export_carrier_transaction');

    //~ Route::get('ajax/get_package_information/{id}','AjaxController@get_package_information');
    //~ Route::get('ajax/get_refund_information/{id}/{refundamt}','AjaxController@get_refund_information');
    //~ Route::get('admin/cancel_package/{id}','PageController@cancel_package');
    //~ Route::post('admin/cancel_package/{id}','PageController@postcancel_package');

    //Admin user
    Route::get('admin/admin_user', 'PageController@admin_user');
    Route::post('pagination/admin_user', 'PaginationController@admin_user');
    Route::get('admin/add_admin_user', 'PageController@add_admin_user');
    Route::post('admin/add_admin_user', 'PageController@post_add_admin_user');
    Route::get('admin/edit_admin_user/{id}', 'PageController@edit_admin_user');
    Route::post('admin/edit_admin_user/{id}', 'PageController@post_edit_admin_user');
    //~ // Old or unused code keep for demo
    //~ //Route::get('admin/users', 'PageController@user_list'); // Admin Controller
    //~ //Route::post('admin', 'PageController@create');    //save New category to database
    //~ Route::get('/user_delete/{id}/{function}','PageController@user_delete');

    //Transaction history
    Route::get('admin/transaction-history', 'TransactionController@transaction_history');
    Route::post('pagination/transaction-history', 'TransactionController@pagination_transaction_history');
    Route::any('admin/export_csv', 'TransactionController@export_csv');

    //Notification section
    Route::any('admin/notification', 'NotificationController@notification');
    Route::post('pagination/notification', 'NotificationController@pagination_notification');
    Route::any('admin/send-notification', 'NotificationController@send_notification');
    Route::any('admin/post-promocode-detail', 'NotificationController@post_promo_details');
    Route::post('pagination/send-notification', 'NotificationController@pagination_send_notification');
    Route::post('pagination/send-promocode-details', 'NotificationController@pagination_send_promocode');
    Route::post('admin/post-notificatione', 'NotificationController@post_notificatione');
    Route::any('admin/view-user/{id}', 'NotificationController@view_user');
    Route::any('pagination/view-user/{id}', 'NotificationController@pagination_view_user');

    // Country Section
    Route::any('admin/add_country', 'PageController@add_country');
    Route::any('admin/country_list', 'PageController@country_list');
    Route::resource('pagination/country_list', 'PaginationController@country_list');
    Route::post('admin/update_country/{id}', 'PageController@update_country');

    //State section
    Route::any('admin/state_list', 'PageController@state_list');
    Route::any('admin/state_list/{id}', 'PageController@state_list');
    Route::any('pagination/state_list', 'PaginationController@state_list');
    Route::post('admin/add_state', 'PageController@add_state');
    Route::post('admin/update_state/{id}', 'PageController@update_state');

    //City section
    Route::any('admin/city_list', 'PageController@city_list');
    Route::resource('pagination/city_list', 'PaginationController@city_list');
    Route::post('admin/add_city', 'PageController@add_city');
    Route::post('admin/post_add_city', 'PageController@post_add_city');
    Route::post('admin/update_city/{id}', 'PageController@update_city');

    // Category Section
    Route::any('admin/add_category', 'PageController@add_category');
    Route::any('admin/category_list', 'PageController@category_list');
    Route::resource('pagination/category_list', 'PaginationController@category_list');
    Route::post('admin/update_category/{id}', 'PageController@update_category');
    Route::get('admin/add-shipping/{id}', 'PageController@shipping');
    Route::post('admin/add-shipping/{id}', 'PageController@postshipping');

    //App content
    Route::get('admin/app_content', 'PageController@app_content');
    Route::get('admin/edit_app_content/{id}', 'PageController@edit_app_content');
    Route::post('admin/edit_app_content/{id}', 'PageController@post_edit_app_content');
    Route::post('pagination/app_content', 'PaginationController@app_content');

    //Web content
    Route::get('admin/web_content', 'PageController@web_content');
    Route::resource('pagination/web_content', 'PaginationController@web_content');
    Route::get('admin/edit_web_content/{id}', 'PageController@edit_web_content');
    Route::post('admin/update_web_content/{id}', 'PageController@update_web_content');
    Route::get('admin/add_web_content', 'PageController@add_web_content');

    //App Tutorial
    Route::any('admin/app_tutorial', 'PageController@app_tutorial');
    Route::resource('pagination/app_tutorial', 'PaginationController@app_tutorial');
    Route::any('admin/add_app_tutorial', 'PageController@add_app_tutorial');
    Route::get('admin/edit_app_tutorial/{id}', 'PageController@edit_app_tutorial');
    Route::post('admin/update_app_tutorial/{id}', 'PageController@update_app_tutorial');
    Route::get('admin/view_app_tutorial/{id}', 'PageController@view_app_tutorial');
    Route::post('admin/post_app_tutorial', 'PageController@post_app_tutorial');

    // About App
    Route::get('admin/about_app', 'PageController@about_app');
    Route::post('admin/update_about_app/{id}', 'PageController@update_about_app');

    //Email template section
    Route::get('admin/email_template', 'PageController@email_template');
    Route::resource('pagination/email_template', 'PaginationController@email_template');
    Route::get('admin/edit_email_template/{id}', 'PageController@edit_email_template');
    Route::post('admin/edit_email_template/{id}', 'PageController@post_email_template');

    // Faq
    Route::any('admin/faq_list', 'PageController@faq_list');
    Route::resource('pagination/faq_list', 'PaginationController@faq_list');
    Route::get('admin/add_faq', 'PageController@add_faq');
    Route::post('admin/post_faq', 'PageController@post_faq');
    Route::get('admin/edit_faq/{id}', 'PageController@edit_faq');
    Route::post('admin/update_faq/{id}', 'PageController@update_faq');

    //support section
    Route::any('admin/support', 'PageController@support');
    Route::resource('pagination/support', 'PaginationController@support');
    Route::post('admin/update-support/{id}', 'PageController@update_support');
    Route::any('admin/support_view_more/{id}', 'PageController@support_view_more');
    Route::any('pagination/support_view_more/{id}', 'PaginationController@support_view_more');
    Route::post('admin/update_support_view_more/{id}', 'PageController@update_support_view_more');

    // Vehicles section
    Route::get('admin/configuration', 'PageController@configuration');
    Route::post('admin/configuration', 'PageController@postConfiguration');

    /*   Route::get('admin', 'AdminController@dash');

    Route::filter('auth', function()
    {
    if (Auth::guest()) return Redirect::guest('users/login');
    });

    // Forgot password
    Route::get('admin/forgot_password','AuthController@getforgot');
    Route::post('admin/forgot_password','AuthController@postforgot');
    Route::get('admin/reset_password/{id}/{token}','AjaxController@reset_password');
    Route::post('admin/reset_password/{id}/{token}','AjaxController@postreset_password');

    //Route::resource('admin/dashboard', 'PageController@charts');
    Route::get('admin', 'PageController@dashboard');

    //Route::controller('admin','add_account_to_stripe');
     */
    // Transaction section
    Route::any('admin/transporter-transaction-history', 'PageController@transporter_transaction');
    Route::resource('pagination/transporter-history', 'PaginationController@transporter_transaction');
    Route::get('admin/transaction_detail/{id}', 'PageController@transaction_detail');
    Route::any('admin/auto_search', 'PageController@auto_search');

/*//

//Import country section
Route::any('admin/import_country','PageController@import_country');
Route::post('admin/post_import_country','PageController@post_import_country');

//feedback section
Route::any('admin/feedback','PageController@feedback');
Route::resource('pagination/feedback','PaginationController@feedback');

Route::get('/delete/{id}','PageController@destroy');
Route::resource('edit','PageController@edit');
Route::resource('update','PageController@update');

Route::resource('category', 'CategoryController');
//Route::get('category', 'CategoryController');
//Route::resource('photos.comments', 'PhotoCommentController');
 */
    Route::get('admin/change_password', 'PageController@change_password');
    Route::post('admin/change_password_post', 'PageController@change_password_post');

    Route::any('admin/local_notificationtiming', 'PageController@local_notificationtiming');

    //Route::get('ajax/state','AjaxController@state');
    //Route::get('ajax/city','AjaxController@city');
    //Route::get('ajax/address_information','AjaxController@address_information');
    Route::get('ajax/badge', 'AjaxController@badge');
    Route::get('admin/setting', 'PageController@setting');
    Route::post('admin/setting', 'PageController@postsetting');
    Route::get('admin/admin_notification', 'PageController@admin_notification');
    Route::post('pagination/admin_notification', 'PaginationController@admin_notification');
    Route::post('admin/refund_amount/{id}', 'PageController@refund_amount');

    //Trip
    Route::get('admin/indiviual-trip', 'PageController@trip');
    Route::post('pagination/trip', 'PaginationController@trip');
    Route::get('admin/trips_request/{id}', 'PageController@trips_request');
    Route::any('pagination/trips_request/{id}', 'PaginationController@trips_request');
    Route::get('admin/business-trip', 'PageController@business_trip');
    Route::post('pagination/business-trip', 'PaginationController@business_trip');
    Route::get('admin/business_request/{id}', 'PageController@business_request');
    Route::any('pagination/business_request/{id}', 'PaginationController@business_request');
    Route::get('admin/edit-business-trip/{id}', 'PageController@edit_business_trip');
    Route::post('admin/edit-business-trip/{id}', 'PageController@post_business_trip');
    Route::get('admin/edit-individual-trip/{id}', 'PageController@edit_individual_trip');
    Route::post('admin/edit-individual-trip/{id}', 'PageController@post_individual_trip');
    Route::get('ajax/update_perpage', 'PaginationController@update_perpage');

    Route::get('admin/promocode', 'PageController@promocode');
    Route::post('pagination/promocode', 'PaginationController@promocode');
    Route::get('admin/add-promocode', 'PageController@add_promocode');
    Route::post('admin/post-promocode', 'PageController@post_promocode');
    Route::get('admin/edit-promocode/{id}', 'PageController@edit_promocode_page');
    Route::get('admin/promocode-detail/{id}', 'PageController@promocode_detail');
    Route::post('admin/promocode-detail/{id}', 'PageController@post_promocode_detail');
    Route::post('admin/update-promocode/{id}', 'PageController@update_promocode');
    Route::get('admin/address', 'PageController@address');

});
