@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')
@section('content')

{!! Html::script('theme/admin/fancybox/source/jquery.fancybox.js') !!}
{!! Html::style('theme/admin/fancybox/source/jquery.fancybox.css') !!}
{!! Html::style('theme/admin/custome/css/adminsumoselect.css') !!}
{!! Html::script('theme/admin/custome/js/validation.js')!!}
{!! Html::script('theme/admin/custome/js/jquery.sumoselect.min.js') !!}
{!! Html::script('theme/admin/custome/css/parsley.css') !!}
{!! Html::script('theme/admin/custome/js/parsley.min.js')!!}
<style>
#parsley-id-multiple-item_name\[\] {
	position: absolute;
	bottom: -16px;
	padding-left: 13px;
	list-style-type: none;
	left: 3px;
	color: RED;
	font-size: 12px;
}
#parsley-id-11{
	position: absolute;
	bottom: -16px;
	padding-left: 13px;
	list-style-type: none;
	left: 3px;
	color: RED;
	font-size: 12px;
}
#parsley-id-7{
	position: absolute;
	bottom: -16px;
	padding-left: 13px;
	list-style-type: none;
	left: 3px;
	color: RED;
	font-size: 12px;
}
.SumoSelect {
	width: 100%;
}
.SumoSelect.demo.open > .optWrapper {
  display: block;
  left: 0;
  margin: 0 auto;
  right: 0;
  top: 57px;
  width: 90%;
}
</style>

<?php

$total_item = 0;
foreach ($info->ProductList as $key => $value) {
	$total_item = $total_item + 1;
	$status[] = $value['status'];
}
?>

<div class="container-fluid">
	<div id="msg_div" class=""></div>
	<div class="panel panel-default">
		<div class="panel-heading">Local Delivery Detail
         <a style="margin-right:5px;" class="btn btn-primary margin-top-less-7 pull-right" href="{{url('admin/local-delivery')}}">Go Back</a>
      	</div>

      	<!-- Other detail -->
      	<div class="panel-body">
      		<div class="col-xs-12">
      			<h3>Other Detail</h3>
      			<div class="row">

	               	<div class="col-sm-4 custom-table1">
	                  <label>Package Id:</label>
	                     {{ucfirst($info->PackageNumber)}}
	               	</div>
	               	<div class="col-sm-4 custom-table1">
	                 <label>Status:</label>
		                <?php echo get_status_title($info->Status)['status']; ?>
	               	</div>

	               	<div class="col-sm-4 custom-table1">
	                  <label>Requester Name:</label>
	                  <a data-toggle="modal" href="#myModal" onclick="return get_user_info('{{$info->RequesterId}}')">{{ucfirst($info->RequesterName)}}</a>
	                </div>

	                <div class="col-sm-4 custom-table1">
					  <label>Receiver Mobile No:</label>
						{{$info->ReceiverCountrycode}}-{{$info->ReceiverMobileNo}}
			       	</div>
			       	<div class="col-sm-4 custom-table1">
					  <label>Device Type: </label>
						{{ ucfirst(@$info->device_type)}}
			       	</div>

			       	<div class="col-sm-4 custom-table1">
					  <label>@if(@$info->device_type == 'website')Browser: @else Device Version: @endif </label>
						{{ @$info->device_version}}
			       	</div>

			       	<div class="col-sm-4 custom-table1">
					  <label>@if(@$info->device_type == 'website')
	                           Version
	                        @else
	                           App Version
	                        @endif: </label>
						{{ @$info->app_version}}
			       	</div>


			       <div class="col-sm-4 custom-table1">
					  <label>Created Date: </label>
					  {{ show_date(@$info->EnterOn) }}
			       </div>

			       	<div class="col-sm-4 custom-table1">
					  <label>Item Total: </label>
						<?php echo $total_item; ?>
			       	</div>

			       	<div class="col-sm-12 custom-table1">
					  <label>Drop Address: </label>
					    {{ucfirst($info->DeliveryFullAddress)}}
	               	</div>

	               	<div class="col-sm-12 custom-table1">
					  <label>Pickup Address: </label>
					    {{ucfirst($info->PickupFullAddress)}}
	               	</div>

	               	<div class="col-sm-12 custom-table1">
					  <label>Return Address: </label>
					    @if($info->ReturnFullAddress != '')
					    	{{ucfirst($info->ReturnFullAddress)}}
					    @else
					    	N/A
					    @endif
	               	</div>

	               	<div class="col-sm-12 custom-table1">
					  <label>Return Address(If not delivered): </label>
					  @if($info->NotDelReturnFullAddress != '')
					    {{ucfirst($info->NotDelReturnFullAddress)}}
					  @else
					  		N/A
					  @endif
	               	</div>

	               @if($info->marked_description)
	               <div class="col-sm-12 custom-table1">
					 <label>Description</label>(Marked as Paid)<label>:</label>
						{{ucfirst($info->marked_description)}}
	               </div>
	               @endif

	            </div>
      		</div>
      	</div>
      	</div>
      	<!-- End other detail -->
      	<div class="clearfix"></div>
      	<!-- Product Detail -->
      	<div class="panel panel-default">
      		<div class="panel-heading">Products Detail </div>
      		<div class="panel-body">
      			@if(count($info->ProductList) > 0)
					@foreach($info->ProductList as $index)
						<div class="panel panel-default panel-body">
							<div class="col-sm-4 custom-table1">
								 <b>Item Name</b>:
								@if(isset($index['product_name']))
								 {{$index['product_name']}}
								@endif
							</div>
							<div class="col-sm-4 custom-table1">
								 <b>Item Number</b>:
								 {{$index['package_id']}}
							</div>
							<div class="col-sm-4 custom-table1">
								 <b>Shipping Cost</b>:
								 ${{number_format($index['shippingCost'],2)}}
							</div>
							<div class="col-sm-4 custom-table1">
								 <b>Insurance Cost</b>:
								 ${{number_format($index['InsuranceCost'],2)}}
							</div>
							<div class="col-sm-4 custom-table1">
								 <b>Need Package Material</b>:
								 {{ucfirst($index['PackageMaterial'])}}
							</div>
							<div class="col-sm-4 custom-table1">
								 <b>Status</b>:

								 {{ get_status_title($index['status'])['status'] }}
							</div>
							<div class="col-sm-4 custom-table1">
								 <b>Verification Code</b>:
								 {{ucfirst($index['DeliveryVerifyCode'])}}
							</div>
							<div class="col-md-12">
								<b>Description:&nbsp;</b>
								&nbsp;{{ucfirst(@$index['Description'])}}
							</div>

							<div class="col-md-12">
							<br/>
								<table class="custom-table table table-bordered " >
									<thead>
										<tr>
											<th>Product Image</th>
											<th>Category</th>
											<th >Weight</th>
											<th >Height</th>
											<th >Width</th>
											<th>Length</th>
											<th>Quantity</th>
											{{-- <th>Shipping Mode</th> --}}
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>
												<?php
$fileexists = false;
if (isset($index['ProductImage']) && $index['ProductImage'] != '') {
	if (file_exists(BASEURL_FILE . $index['ProductImage'])) {
		$fileexists = true;
		?>
																<a class="fancybox" rel="group" href="{{ ImageUrl.$index['ProductImage'] }}" >
										                           <img id="senior-preview" src="{{ ImageUrl.$index['ProductImage'] }}" class="margin-top" width="120px" height="100px" >
										                         </a><?php
}
}
if ($fileexists == false) {?>
														<img src="{{ImageUrl}}no-image.jpg" class="margin-top" width="120px" height="100px">
													<?php }?>
											</td>

											<td>
												{{ucfirst(@$index['productCategory'])}}
											</td>
											<td>
												{{ucfirst(@$index['productWeight'])}}&nbsp;{{$index['ProductWeightUnit']}}
											</td>
											<td>
												{{ucfirst(@$index['productHeight'])}}&nbsp;{{$index['productHeightUnit']}}
											</td>
											<td>
												{{ucfirst(@$index['productWidth'])}}&nbsp;{{$index['productHeightUnit']}}
											</td>
											<td>
												{{ucfirst(@$index['productLength'])}}&nbsp;{{$index['productHeightUnit']}}
											</td>
											<td>
												{{ucfirst(@$index['productQty'])}}
											</td>
											{{-- <td>
												{{ucfirst(@$index['travelMode'])}}
											</td> --}}
										</tr>
									</tbody>
								</table>
							</div>
							<!-- Button Process -->
							<div class="col-md-12 text-right">


								@if(@$index['status'] == 'pending')
									@if($index['inform_mail_sent'] != 'yes')
										<a  type="button" class="btn btn-primary" title="Inform Requester" data-toggle="modal"  style="display:inline-block" id="inform2-{{@$index['_id']}}" onclick="send_upddate_mail('{{@$index['_id']}}','{{$info['_id']}}','')" >Inform Requester</a>
									@endif

								@endif

								@if($index['status'] == 'purchased')
                                 <a  type="button" class="btn btn-primary" title="Item Received"    id="reviewButton{{@$index['_id']}}" onclick="return confirm('Are you sure the item has been received?')? delivery_process('{{@$index['_id']}}','received','receivedButton'):'';" >Received</a>

                                @endif


                               {{--   @if($index['status'] == 'received')
                                 <a  type="button" class="btn btn-primary" title="Assign Transporter" data-toggle="modal"   id="assign-{{$index['_id']}}" data-target="#assign_tp" onclick="$('#tp_itemid').val('{{$index['_id']}}')" >Assign Transporter</a>
                                @endif --}}



								<?php $btn_status = 'display:none';
if ($index['status'] == 'ready') {$btn_status = 'display:inline-block';}?>

								<a  type="button" class="btn btn-primary" title="Item Reviewed"   style="{{$btn_status}}" id="reviewButton{{@$index['_id']}}" onclick="return confirm('Are you sure the item has been reviewed?')? delivery_process('{{@$index['_id']}}','review','reviewButton'):'';" >Review</a>

								<?php $btn_status = 'display:none';
if ($index['status'] == 'reviewed') {$btn_status = 'display:inline-block;';}?>
								<a  type="button" class="btn btn-primary" title="Update Item" data-toggle="modal"  style="{{$btn_status}}" id="item_update-{{$index['_id']}}"
									data-target="#edit_item" onclick="getItemInfo('{{$index['_id']}}','{{$info['_id']}}',this)" >Update Item
								</a>

								<?php $btn_status = 'display:none';
$info_btn_status = 'display:none';

if ($index['status'] == 'not_purchased') {$btn_status = 'display:inline-block';}
if (isset($index['inform_mail_sent']) && $index['inform_mail_sent'] == 'yes') {
	$info_btn_status = 'display:none';
} else {
	$info_btn_status = 'display:inline-block';
}

?>


								<span id="waiting-for-payment-{{@$index['_id']}}" style="{{$btn_status}}" class="btn" >Waiting for a requester to review</span>
								@if(in_array(@$index['status'],['not_purchased']))
								<a  type="button" class="btn btn-primary" title="Inform Requester" data-toggle="modal"  style="{{$info_btn_status}}" id="inform2-{{@$index['_id']}}" onclick="send_upddate_mail('{{@$index['_id']}}','{{$info['_id']}}','')" >Inform Requester</a>
								@endif

								<?php $btn_status = 'display:none';
if ($index['status'] == 'item_received') {$btn_status = 'display:inline-block';}?>
								<a  type="button" class="btn btn-primary" title="Assign Transporter" data-toggle="modal"  style="{{$btn_status}}" id="assign-{{$index['_id']}}" data-target="#assign_tp" onclick="$('#tp_itemid').val('{{$index['_id']}}')" >Assign Transporter</a>

								@if(!in_array(@$index['status'],['cancel','cancel_by_admin','delivered','purchased','assign']))
								<a type="button" class="btn btn-danger" title="Cancel" onclick="return confirm('Are you sure! you want to cancel this item')? delivery_process('{{@$index['_id']}}','cancel','cancelButton2'):'';" id="cancelButton2{{@$index['_id']}}">Cancel
								</a>
								@endif
							</div>
							<!-- End Button Process -->
						</div>


					@endforeach
				@endif



				<div class="col-sm-12">
					<div class="col-sm-4">
						<h3><b>Payment Information</b></h3>
						<table class="table table-bordered">
  							<tbody bgcolor="#F1F1F1">
  								<tr>
				                   <td class="">
				                      <div class="col-xs-8 text-left">
				                        <label class="" for="exampleInputEmail1"><b>Shipping Cost:</b> </label>
				                      </div>

				                      <div class="col-xs-4 text-right">
				                        ${{number_format((float) $info->ShippingCost, 2)}}

				                      </div>
				                    </td>
				               	</tr>

				              	<tr>
				                   <td class="">
				                      <div class="col-xs-8 text-left">
				                        <label class="" for="exampleInputEmail1"><b>Processing Fees:</b> </label>
				                      </div>
				                      <div class="col-xs-4 text-right">
				                        ${{number_format((float) $info->InsuranceCost,2)}}
				                      </div>
				                    </td>
				              	</tr>

				              	<tr>
				                   <td class="">
				                      <div class="col-xs-8 text-left">
				                        <label class="" for="exampleInputEmail1"><b>Region Cost:</b> </label>
				                      </div>
				                      <div class="col-xs-4 text-right">
				                         ${{number_format((float) @$info->AreaCharges,2)}}
				                     {{--  ${{number_format((float) @$info->region_charge,2)}} --}}
				                      </div>
				                    </td>
				              	</tr>

				              	<tr>
				                   <td class="">
				                      <div class="col-xs-8 text-left">
				                        <label class="" for="exampleInputEmail1"><b>After Item Review:</b> </label>
				                      </div>
				                      <div class="col-xs-4 text-right">
				                        ${{number_format((float) $info->after_update_difference,2)}}
				                      </div>
				                    </td>
				              	</tr>

				               @if($info->discount > 0)
				                  	<tr>
					                    <td class="">
					                        <div class="col-xs-8 text-left">
					                          <label class="" for="exampleInputEmail1"><b>Aquantuo Discount:</b>
					                           </label>
					                                @if(!$info->PromoCode == '')
					                                 <small>(<b>Promocode:</b>{{$info->PromoCode }}) </small>
					                                 @endif
					                        </div>

					                        <div class="col-xs-4 text-right">
					                            (-)${{number_format((float) $info->discount, 2)}}
					                        </div>
					                    </td>
				                  	</tr>
				                @endif

				                <td class="">
									<div class="col-xs-8 text-left">
										<label class="" for="exampleInputEmail1"><b>Total Price:</b> </label>
									</div>
										<div class="col-xs-4 text-right">
			    							<?php $total_cost = $info->TotalCost + $info->after_update_difference;?>
			    							${{ number_format((float) $total_cost,2)}}
										</div>
								    </td>
								</tr>

  							</tbody>
  						</table>
					</div>
				</div>

     				<div class="col-sm-12 text-right" >
					<?php $btn_status = 'display:none';
if ($info->Status == 'not_purchased') {$btn_status = 'display:inline-block';}?>
					{{-- <span id="reminder_button" style="{{$btn_status}}"  >
     					<a class="btn btn-primary" title="Send payment reminder"
						onclick="delivery_process('payment_reminder','{{@$info->_id}}',this,'Are you sure, you want to send reminder.');" >Reminder for Payment</a>&nbsp;&nbsp;
					</span> --}}

					@if($info->Status == 'not_purchased')

						<a data-toggle="modal" data-target="#paid_mark"  title="Mark as Paid" class="btn btn-primary"><i class="fa fa-check-square-o"></i>&nbsp;Mark as Paid
					</a>
					@endif
					</div>

				@foreach($info->ProductList as $value)
                    @if('ready' == $value['status'])
                        <?php $check_review = "true";?>
                    @elseif('purchased' == $value['status'])
                      <?php $check_received = "true";?>
                    @endif
                @endforeach

				<div class="col-sm-12 text-right" >
				@if(isset($check_review))
               			<a  type="button" class="btn btn-primary assign-btn" title="Select Item" data-toggle="modal"  id="review-{{$index['_id']}}" data-target="#review_select" >Review All</a>
                @endif

                @if(isset($check_received))
               			<a  type="button" class="btn btn-primary assign-btn" title="Select Item" data-toggle="modal"  id="received-{{$index['_id']}}" data-target="#received_select" >Received All</a>
                @endif

               @if(in_array('item_received',$status))
                       <a  type="button" class="btn btn-primary assign-btn" title="Assign Transporter" data-toggle="modal"  id="assign-{{$index['_id']}}" data-target="#assign_tp_again" onclick="$('#tp_itemid').val('{{$index['_id']}}')" >Assign All</a>
     			@endif
               </div>

     			@if(in_array('paid',$status))
               		<div class="col-sm-12 text-right" >
                       <a  type="button" class="btn btn-primary assign-btn" title="Assign Transporter" data-toggle="modal"  id="assign-{{$index['_id']}}" data-target="#assign_tp_again" onclick="$('#tp_itemid').val('{{$index['_id']}}')" >Assign All</a>
                    </div>
     			@endif


      		</div>
      	</div>
      	<!-- End Product Detail -->
	</div>
</div>

<!-- Item Update Modal -->
<div id="edit_item" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" id="edit_item_close">&times;</button>
        <h4 class="modal-title">Edit Item</h4>
      </div>
      <form id="item_update_form" name="item_update_form">
      	<div class="modal-body">
	        <div class="row">
	        	<div class="col-md-6">
	        		<div class="form-group">
					    <label for="item_name">Item Name:</label>
					    <input type="text" class="form-control" id="item_name" name="product_name">
					    <div class="red" id="er_item_name"></div>
					</div>
	        	</div>

	        	<div class="col-md-6">
	        		<div class="form-group">
					    <label for="item_name">Item Price:</label>
					    <input type="text" class="form-control" id="productCost" name="productCost">
					    <div class="red" id="er_productCost"></div>
					</div>
	        	</div>
	        </div>

	        <div class="row">
	        	<div class="col-md-6">
	        		<h4>Measurement Units</h4>
	        		<div class="clearfix"></div>
	        		<input type="radio" name="measurement_units" id="measurement_units_cm_kg" value="cm_kg" checked>&nbsp;Metric(cm/kg)&nbsp;&nbsp;
	        		<input type="radio" name="measurement_units" id="measurement_units_inches_lbs" value="inches_lbs">&nbsp;Imperial(inches/lbs)
	        	</div>

	        	<div class="col-md-6">
	        		<h4>Quantity</h4>
	        		<input type="text" class="form-control" id="productQty" name="productQty">
	        		<div class="red" id="er_productQty"></div>
	        	</div>

	        	<div class="col-md-12"><h4>Item Specification</h4></div>
	        	<div class="col-md-6">
	        		<div class="clearfix"></div>
	        		<div class="form-group">
					    <label for="item_name">Length:</label>
					    <input type="text" class="form-control" id="productLength" name="productLength">
					    <div class="red" id="er_productLength"></div>
					</div>
	        	</div>

	        	<div class="col-md-6">
	        		<div class="form-group">
					    <label for="item_name">Width:</label>
					    <input type="text" class="form-control" id="productWidth" name="productWidth">
					    <div class="red" id="er_productWidth"></div>
					</div>
	        	</div>

	        </div>

	        <div class="row">
	        	<div class="col-md-6">
	        		<div class="form-group">
					    <label for="item_name">Height:</label>
					    <input type="text" class="form-control" id="productHeight" name="productHeight">
					    <div class="red" id="er_productHeight"></div>
					</div>
	        	</div>

	        	<div class="col-md-6">
	        		<div class="form-group">
					    <label for="item_name">Weight:</label>
					    <input type="text" class="form-control" id="productWeight" name="productWeight">
					    <div class="red" id="er_productWeight"></div>
					</div>
	        	</div>
	        </div>

	        <div class="row">
	        	<div class="col-md-6">
	        		<h4>Insurance</h4>
	        		<div class="clearfix"></div>
	        		<input type="radio" name="InsuranceStatus" id="InsuranceStatus_yes" value="yes" checked>&nbsp;Yes&nbsp;&nbsp;
	        		<input type="radio" name="InsuranceStatus" id="InsuranceStatus_no" value="no">&nbsp;No
	        	</div>
	        </div>


	    	<input type="hidden" name="itemid" id="itemid" >
	    	<input type="hidden" name="requestid" id="requestid" >
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" id="item_sub_button">Submit</button>
      </div>
      </form>
    </div>

  </div>
</div>
<!-- End item update modal -->

<!-- Assign Transport -->
<!-- Modal for assign transporter -->
<div id="assign_tp" class="modal fade" role="dialog">
  <div class="modal-dialog modal-sm">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" id="assign_tp_close">&times;</button>
        <h4 class="modal-title">Assign Transporter</h4>
      </div>
      <form id="assing_tp_form">
      	<div class="modal-body">
	        <div class="row">
	        	<div class="col-md-12">
	        		<div class="form-group">
					    <label for="item_name">Transporter:</label>
					    <select class="form-control" id="tp_name" name="tp_name">
					    	<option value="">Select Transporter</option>
					    	@foreach($tplist as $key)
					    		<option value='{"id":"{{$key['_id']}}","name":"{{$key['Name']}}"}' name="tp_name">{{$key['Name']}}</option>
					    	@endforeach
					    </select>
					    <div class="red" id="er_tp_name"></div>
					</div>
	        	</div>
	        </div>

	    	<input type="hidden" name="product_id" id="tp_itemid">
	    	<input type="hidden" name="request_id" id="tp_requestid" value="{{Request::segment(3)}}">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" id="assing_tp_btn" >Submit</button>
      </div>
      </form>
    </div>

  </div>
</div>
<!-- End Assign Transport -->



<!-- Modal for review select -->
<div id="review_select" class="modal fade" role="dialog">
  <div class="modal-dialog modal-sm">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" id="assign_tp_close">&times;</button>
        <h4 class="modal-title">Select Item for review</h4>
      </div>
      {!! Form::model('', ['name'=>'add-appcontent','id' => 'review_from', 'method' => 'POST', 'data-parsley-validate', 'url' => ['local-delivery-accept-all/'.Request::segment(3)]]) !!}
      	<div class="modal-body">
	        <div class="row">
	        	<input type="hidden" name="requestid" value="{{Request::segment(3)}}">
	        	<div class="col-md-12">
	        		<div class="form-group">
					    <label for="item_name">Item:</label>
					    <select class="form-control testselect3  required " data-parsley-required-message = 'Please select atlist one  item is required.' required multiple="" id="tp_name" name="item_name[]">
					   @foreach($info->ProductList as $index)
					   	@if($index['status'] == 'ready')
					    <option value='{{$index['_id']}}' >{{ucfirst($index['product_name'])}}</option>
					    @endif
					   @endforeach
					    </select>
					    @if($errors->has('item_name'))
                            <p class="controll-error help-block"><i class="fa fa-times-circle-o"></i>&nbsp;
                                {{ $errors->first('item_name') }}
                            </p>
                            @endif
					</div>

	        	</div>
	        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" id="review_id"  >Submit</button>
      </div>
      </form>
    </div>

  </div>
</div>
<!-- Model end -->



<!-- Modal for received select -->
<div id="received_select" class="modal fade" role="dialog">
  <div class="modal-dialog modal-sm">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" id="assign_tp_close">&times;</button>
        <h4 class="modal-title">Select Item for received</h4>
      </div>
      {!! Form::model('', ['name'=>'add-appcontent','id' => 'review_from', 'method' => 'POST', 'data-parsley-validate', 'url' => ['local-delivery-received-all/'.Request::segment(3)]]) !!}
      	<div class="modal-body">
	        <div class="row">
	        	<input type="hidden" name="requestid" value="{{Request::segment(3)}}">
	        	<div class="col-md-12">
	        		<div class="form-group">
					    <label for="item_name">Item:</label>
					    <select class="form-control testselect3  required " data-parsley-required-message = 'Please select atlist one  item is required.' required multiple="" id="tp_name" name="item_name[]">
					   @foreach($info->ProductList as $index)
					   	@if($index['status'] == 'purchased')
					    <option value='{{$index['_id']}}' >{{ucfirst($index['product_name'])}}</option>
					    @endif
					   @endforeach
					    </select>
					    @if($errors->has('item_name'))
                            <p class="controll-error help-block"><i class="fa fa-times-circle-o"></i>&nbsp;
                                {{ $errors->first('item_name') }}
                            </p>
                            @endif
					</div>

	        	</div>
	        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" id="review_id"  >Submit</button>
      </div>
      </form>
    </div>

  </div>
</div>
<!-- Model end -->



<!-- Modal for assign transporter -->
<div id="assign_tp_again" class="modal fade" role="dialog">
  <div class="modal-dialog modal-sm">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" id="assign_tp_close">&times;</button>
        <h4 class="modal-title">Assign Transporter</h4>
      </div>
      	 {!! Form::model('', ['name' => 'assignalltp','id' => 'assing_tp2_from', 'data-parsley-validate', 'method' => 'post', 'url'=> ['local-assign-all-tp/'.Request::segment(3)]]) !!}

      	<div class="modal-body">
	        <div class="row">

	        	<div class="col-md-12">
	        		<div class="form-group">
					    <label for="item_name">Item:</label>
					    <select class="form-control testselect3 required" data-parsley-required-message = 'Please select atlist one  item is required.' required multiple="" id="tp_name" name="item_name[]">
					   @foreach($info->ProductList as $index)
					   	@if(in_array($index['status'],['item_received','paid']))
					    <option value='{{$index['_id']}}' >{{ucfirst($index['product_name'])}}</option>
					    @endif
					   @endforeach
					    </select>
					    <div class="red" id="er_tp_name"></div>
					</div>
	        	</div>

                <div class="clearfix"> </div>
                <br/>
	        	<div class="col-md-12">
	        		<div class="form-group">
					    <label for="item_name">Transporter:</label>
					    <select class="form-control" id="tp_name" data-parsley-required-message = 'Please select atlist one Transporter is required.' required name="tp_name">
					    	<option value="">Select Transporter</option>
					    	@foreach($tplist as $key)
					    		<option value='{"id":"{{$key['_id']}}","name":"{{$key['Name']}}"}' >{{$key['Name']}}</option>
					    	@endforeach
					    </select>
					    <div class="red" id="er_tp_name"></div>
					</div>
	        	</div>
	        </div>

	    	<input type="hidden" name="product_id" id="12tp_itemid" >
	    	<input type="hidden" name="request_id" id="tp_requestid" value="{{Request::segment(3)}}">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" id="assing_tp2" >Submit</button>
      </div>
      {!! Form::close() !!}
    </div>

  </div>
</div>
<!-- End Transporter Modal -->



<!-- mark paid after item update -->
<div class="modal fade" id="paid_mark" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title" id="exampleModalLabel">Mark as Paid</h4>
	  </div>

	  {!! Form::model('', ['name' => 'paid_payment2', 'id' => 'paid_payment2', 'method' => 'POST', 'url' => ['admin/mark_paid_after_item_update']]) !!}
	  <input type="hidden" name="requestid" value="{{$info->_id}}" id="requestid">
	  <input type="hidden" name="request_type" value="delivery" id="request_type">

	  <div class="modal-body">
			<div class="form-group" id="">
			<label>Description/Reason</label>
				<textarea class="form-control" name="description" placeholder="Description/Reason"></textarea>
				<p class="help-block" id="description" style="display: none;"></p>
			</div>

	  </div>
	  <div class="modal-footer">
		{!! Form::button('Close', ['class' => 'btn btn-primary', 'data-dismiss'=>'modal']) !!}
		{!! Form::submit('Submit', ['class' => 'btn btn-primary','onclick' => "return confirm('Are you sure? You want to mark as paid to this request!');"]) !!}
	  </div>
		<div class="clearfix"></div>
		{!! Form::close() !!}

	</div>
  </div>
</div>
<!-- end mark paid -->


<script type="text/javascript">

$(document).ready(function() {
   	$(".fancybox").fancybox();
});

   $('#assing_tp2_from').parsley().subscribe('parsley:form:validated', function (parsleyForm) {
  if (true === parsleyForm.validationResult ){
      $('#assing_tp2').addClass('spinning');
    }
  else{
      $('#assing_tp2').removeClass('spinning');
  }
});

$('#assing_tp_form').submit(function(){
	var flag = true;
	if(valid.required('tp_name','transprter') == false) { flag = false; }
	if(flag)
	{
		$('#assing_tp_btn').addClass('spinning');
		$.ajax({
		url     : '{{url("admin/local-delivery-assign-transporter")}}',
		type    : 'post',
		data    : $('#assing_tp_form').serialize(),
		dataType: 'json',
		success : function(res)
		{
			$('#assing_tp_btn').removeClass('spinning');
			if(res.success == 1)
			{
				$('#assing_tp_form')[0].reset();
				$('#assign_tp_close').trigger('click');

				$('#assign-'+$('#tp_itemid').val()).hide();
				alert(res.msg);
				location.reload();
			}
			alert(res.msg);
		}
	});
	}
	return false;

});


function send_upddate_mail(itemid,requestid,item_name)
{

	$("#inform-"+itemid).addClass('spinning');
	$("#inform2-"+itemid).addClass('spinning');

		$.ajax({
		url     : '{{url("admin/send-upddate-mail")}}',
		type    : 'post',
		data    : "request_id="+requestid+"&product_id="+itemid+"&product_name="+item_name,
		success : function(res){

			$("#inform-"+itemid).removeClass('spinning');
			$("#inform2-"+itemid).removeClass('spinning');
			//$("#inform2-"+itemid).hide();
			//$("#inform-"+itemid).hide();
			if(res.success == 1)
			{
				alert(res.msg);
				location.reload();
			}
			else
			{
				alert(res.msg);
			}
		}
	});
}



var requestid = '{{Request::segment(3)}}';
function delivery_process(itemid,type,obj)
{
	$("#"+obj+itemid).addClass('spinning');
	$.ajax({
		url     : '{{url("admin/local-delivery-process")}}',
		type    : 'post',
		data    : {type:type,itemid:itemid,requestid:requestid},
		dataType: 'json',
		success : function(res)
		{
			$("#"+obj+itemid).removeClass('spinning');
			$("#accept_all_button").hide();
			alert(res.msg);
			if(res.success == 1)
			{
				location.reload();
			}
		}
	});
	return false;
}

function getItemInfo(itemid,requestid,obj)
{
	$("#item_update_form").trigger('reset');
	$('#itemid').val(itemid);
	$('#requestid').val(requestid);
	$(obj).addClass('spinning');
	$.ajax({
		url     : '{{url("admin/get_item_info")}}',
		type    : 'post',
		data    : {itemid:itemid,requestid:requestid},
		dataType: 'json',
		success : function(res)
		{
			$(obj).removeClass('spinning');
			if(res.success == 1)
			{
				$('#item_name').val(res.ProductList.product_name);
				$('#productCost').val(res.ProductList.productCost);
				$('#productQty').val(res.ProductList.productQty);
				$('#productLength').val(res.ProductList.productLength);
				$('#productWidth').val(res.ProductList.productWidth);
				$('#productHeight').val(res.ProductList.productHeight);
				$('#productWeight').val(res.ProductList.productWeight);
				if(res.ProductList.productHeightUnit == 'cm') {
					$('#measurement_units_cm_kg').attr('checked','checked');
				} else {
					$('#measurement_units_inches_lbs').attr('checked','checked');
				}

				if(res.ProductList.InsuranceStatus == 'yes') {
					$('#InsuranceStatus_yes').attr('checked','checked');
				} else {
					$('#InsuranceStatus_no').attr('checked','checked');
				}
			}
			else
			{
				alert(res.msg);
			}
		}
	});
}


$('#item_update_form').submit(function()
{
	var flag = true;
	if(valid.required('item_name','item name') == false) { flag = false; }
	if(valid.required('productCost','item price') == false) { flag = false; }
	if(valid.required('productQty','quantity') == false) { flag = false; }
	if(valid.required('productLength','length') == false) { flag = false; }
	if(valid.required('productWidth','width') == false) { flag = false; }
	if(valid.required('productHeight','height') == false) { flag = false; }
	if(valid.required('productWeight','weight') == false) { flag = false; }

	if(flag)
	{
		$('#item_sub_button').addClass('spinning');
		$.ajax({
		url     : '{{url("admin/local-delivery-item-update")}}',
		type    : 'post',
		data    : $('#item_update_form').serialize(),
		dataType: 'json',
		success : function(res)
		{
			console.log(res);
			$('#item_sub_button').removeClass('spinning');
			if(res.success == 1)
			{
				$('#item_update_form')[0].reset();
				$('#edit_item_close').trigger('click');
				alert(res.msg);
				location.reload();
			}
			alert(res.msg);
		}
	});
	}
	return false;
});
 $('.testselect3').SumoSelect();

  $('#review_from').parsley().subscribe('parsley:form:validated', function (parsleyForm) {
  if (true === parsleyForm.validationResult ){
      $('#review_id').addClass('spinning');
    }

  else{
      $('#review_id').removeClass('spinning');
  }
});


</script>
@endsection

<style>
   .modal-header .close {
   margin-top: -9px !important;
   }

   .custom-table.table th {
    border-bottom: medium none !important;
    vertical-align: middle;
    height: 60px!important;
    background: #F1F1F1;
    /* text-align:center;*/
}
.custom-table.table > tbody td {
    /*text-align:center !important;*/

    vertical-align: middle;
}

.table-bordered td,.table-bordered th{border:1px solid #ddd!important}

.ratingAdjustmentcss {
background-color: #8d887a;
float: left;
margin-top: 3px !important;
width: 65px;
}

.graph {
background-color: #ecb90d;
height: 14px;
}

</style>
