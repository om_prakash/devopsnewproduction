@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')

@section('content')


<div class="container-fluid">
    @if(Session::has('success'))
    <div class="alert alert-success">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong>Success!</strong> {{Session::get('success')}}
    </div>
    @endif
    
   @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="panel panel-default">
  <div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;Listing</div>
  <div class="panel-body">
  		
  	<div class="row">
  <div class="col-md-3 col-sm-6 col-xs-12 pad-20 pad-top-10">
    <div class="input-group">
     
      <input type="text" class="form-control" placeholder="Search for...">
       <span class="input-group-btn">
        <button class="btn btn-primary" type="button">Go!</button>
      </span>
    </div><!-- /input-group -->
  </div><!-- /.col-md-3 -->
   <div class="col-md-3 col-sm-6 col-xs-12 padding-20 pull-right text-right pad-top-10">
    <button type="button" class="btn btn-primary col-md-6 col-sm-5 col-xs-12 list-add-button pull-right text-right" aria-label="Left Align" data-toggle="modal" data-target="#addModal">
 <i class="fa fa-plus"></i> Add User
</button>
  </div><!-- /.col-md-3 -->
  	</div>
      
      <!-- Pagination Section-->
	<input type="hidden" name="urlvalue" id="urlvalue" value="{{$paginationurl}}" />
	<input type="hidden" name="postvalue" id="postvalue" value="{{$postvalue}}" />
	<input type="hidden" name="orderby" id="orderby" value="{{$orderby}}" />
	<input type="hidden" name="orderType" id="orderType" value="{{$orderType}}" />
	<div id="containerdata"></div>
	<div class="clearfix"></div>
	
  	
  </div> <!-- Panel Body -->
</div>
</div>
@extends('Admin::layouts.footer')

@stop
