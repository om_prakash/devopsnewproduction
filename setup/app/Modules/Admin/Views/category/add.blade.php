@section('AddNewContentData')
<!-- Add Modal -->
<div id="addModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="fa fa-pencil-square-o"></i> Add Category</h4>
      </div>
      <div class="modal-body">
     	     {!! Form::open() !!}
         <div class="form-group">
		     {!! Form::label('UserFirstName', 'First Name') !!}
		     {!! Form::text('UserFirstName', '', ['class'=>'form-control', 'placeholder'=> 'First Name']) !!}
		     @if ($errors->has('name')) <p class="help-block">{{ $errors->first('UserFirstName') }}</p> @endif
         </div>
         <div class="form-group">
		     {!! Form::label('UserLastName', 'Last Name') !!}
		     {!! Form::text('UserLastName', '', ['class'=>'form-control', 'placeholder'=> 'Last Name']) !!}
         </div>
         <div class="form-group">
		     {!! Form::label('email', 'Email Address') !!}
		     {!! Form::email('UserEmail', '', ['class'=>'form-control', 'placeholder'=> 'Email Address']) !!}
         </div>

      <div class="form-group">
                            {!! Form::label('UserStatus', 'Status') !!}
                            {!! Form::select('UserStatus', ['1'=>'Active','2'=>'Inactive'],null, ['class'=>'form-control'])  !!}
                        </div>
      </div>
      <div class="modal-footer">
			{!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
			{!! Form::submit('Cancel', ['class' => 'btn btn-danger', 'data-dismiss'=>'modal']) !!}
  			{!! Form::close() !!}
       
      </div>
    </div>

  </div>
</div>
