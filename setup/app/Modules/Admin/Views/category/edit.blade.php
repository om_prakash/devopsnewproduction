@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')
@section('content')

<div class="container-fluid"> 
<div class="panel panel-default">
  <div class="panel-heading"><i class="fa fa-pencil-square-o"></i>&nbsp;Edit User</div>
  <div class="panel-body">
  		<?php //print_r($userEdit)?>
  	<div class="row">
  		<div class="col-md-6">
  		  	{!! Form::model($userEdit, ['method' => 'PATCH', 'url' => ['update', $userEdit->UserId]]) !!}
  		  	
  		  	
     	  <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}"  >
		     {!! Form::label('UserFirstName', 'First Name') !!}
		     {!! Form::text('UserFirstName', '', ['class'=>'form-control ', 'placeholder'=> 'First Name','id' =>'inputError1']) !!}
		         
		     @if ($errors->has('UserFirstName')) <p class="help-block">{{ $errors->first('UserFirstName') }}</p> @endif
         </div>
         <div class="form-group">
		     {!! Form::label('UserLastName', 'Last Name') !!}
		     {!! Form::text('UserLastName', '', ['class'=>'form-control', 'placeholder'=> 'Last Name']) !!}
         </div>
         <div class="form-group">
		     {!! Form::label('email', 'Email Address') !!}
		     {!! Form::email('UserEmail', '', ['class'=>'form-control', 'placeholder'=> 'Email Address']) !!}
         </div>

      <div class="form-group">
             {!! Form::label('UserStatus', 'Status') !!}
             {!! Form::select('UserStatus', ['1'=>'Active','2'=>'Inactive'],null, ['class'=>'form-control'])  !!}
         </div>
          <div class="form-group pull-right">
			{!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
			{!! Form::submit('Cancel', ['class' => 'btn btn-danger']) !!}
  			{!! Form::close() !!}
       
      </div>
      </div>

  </div>
      <!-- Pagination Section-->
  </div> <!-- Panel Body -->
</div>
</div>
@stop
