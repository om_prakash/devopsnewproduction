@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')
@section('content')

<div class="container-fluid">



<div class="panel panel-default">
  <div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;Listing</div>
  <div class="panel-body">
  		
  	<div class="row">
  <div class="col-md-3 pad-20 pad-top-10">
    <div class="input-group">
     
      <input type="text" class="form-control" placeholder="Search for...">
       <span class="input-group-btn">
        <button class="btn btn-primary" type="button">Go!</button>
      </span>
    </div><!-- /input-group -->
  </div><!-- /.col-md-3 -->
   <div class="col-md-3 padding-20 pull-right text-right">
    <button type="button" class="btn btn-primary" aria-label="Left Align">
 <i class="fa fa-plus"></i> Add Category
</button>
  </div><!-- /.col-md-3 -->
  	</div>
  	
    <table class="table table-bordered table-striped table-highlight table-list">
        <thead>
          <tr>
            <th>#</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Username</th>
            <th>Status</th>

          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="row">1</th>
            <td>Mark</td>
            <td>Otto</td>
            <td>@mdo</td>
            <td class="width-action-20">
	           	<a data-placement="top" data-toggle="tooltip" title="Tooltip on left" title="Make Inactive" class="btn btn-small btn-warning btn-action" href="#"><i class="fa fa-lock"></i></a>
	           	<a data-placement="top" title="Make Active" class="btn btn-small btn-success btn-action" href="#"><i class="fa fa-unlock"></i></i></a>
	           	<a data-placement="top" title="View List" class="btn btn-small btn-primary btn-action" href="#"><i class="fa fa-th-list"></i></a>
	           	<a data-placement="top" title="View Details" class="btn btn-small btn-info btn-action" href="#"><i class="fa fa-info-circle"></i></a>
				<a data-placement="top" title="Edit Record" class="btn btn-small btn-primary" btn-action href="#"><i class="fa fa-pencil-square-o"></i></a>
	            <a data-placement="top" title="Delete Record" class="btn btn-small btn-danger btn-action" href="#"><i class="fa fa-trash"></i></a>
           	</td>
          </tr>
          <tr>
            <th scope="row">2</th>
            <td>Jacob</td>
            <td>Thornton</td>
            <td>@fat</td>
           <td class="width-action-20">
	           	<a data-placement="top" title="Make Inactive" class="btn btn-small btn-warning btn-action" href="#"><i class="fa fa-lock"></i></a>
	           	<a data-placement="top" title="Make Active" class="btn btn-small btn-success btn-action" href="#"><i class="fa fa-unlock"></i></i></a>
	           	<a data-placement="top" title="View List" class="btn btn-small btn-primary btn-action" href="#"><i class="fa fa-th-list"></i></a>
	           	<a data-placement="top" title="View Details" class="btn btn-small btn-info btn-action" href="#"><i class="fa fa-info-circle"></i></a>
				<a data-placement="top" title="Edit Record" class="btn btn-small btn-primary" btn-action href="#"><i class="fa fa-pencil-square-o"></i></a>
	            <a data-placement="top" title="Delete Record" class="btn btn-small btn-danger btn-action" href="#"><i class="fa fa-trash"></i></a>
           	</td>

          </tr>
          <tr>
            <th scope="row">3</th>
            <td>Larry</td>
            <td>the Bird</td>
            <td>@twitter</td>
           <td class="width-action-20">
	           	<a data-placement="top" title="Make Inactive" class="btn btn-small btn-warning btn-action" href="#"><i class="fa fa-lock"></i></a>
	           	<a data-placement="top" title="Make Active" class="btn btn-small btn-success btn-action" href="#"><i class="fa fa-unlock"></i></i></a>
	           	<a data-placement="top" title="View List" class="btn btn-small btn-primary btn-action" href="#"><i class="fa fa-th-list"></i></a>
	           	<a data-placement="top" title="View Details" class="btn btn-small btn-info btn-action" href="#"><i class="fa fa-info-circle"></i></a>
				<a data-placement="top" title="Edit Record" class="btn btn-small btn-primary" btn-action href="#"><i class="fa fa-pencil-square-o"></i></a>
	            <a data-placement="top" title="Delete Record" class="btn btn-small btn-danger btn-action" href="#"><i class="fa fa-trash"></i></a>
           	</td>

          </tr>
        </tbody>
      </table>
      
      
      <!-- Pagination Section-->
    
  	<form role="search" class="navbar-form navbar-left margin-0">
            <div class="form-group input-group-sm">
          Records per page &nbsp;   <select type="text" placeholder="Search" class="form-control btn-small">
              	<option > 5</option>
              	<option > 10</option>
              	<option > 20</option>
              	<option > 50</option>
              	<option > 100</option>
             
             
              </select>
            </div>
     </form>


  	
  <ul class="pagination margin-0 navbar-center">
  	  <li>
      <a href="#" aria-label="Previous">
        <span aria-hidden="true">&laquo; First</span>
      </a>
    </li>
    <li>
      <a href="#" aria-label="Previous">
        <span aria-hidden="true">&laquo; Previous</span>
      </a>
    </li>
    <li><a href="#">1</a></li>
    <li><a href="#">2</a></li>
    <li><a href="#">3</a></li>
    <li><a href="#">4</a></li>
    <li><a href="#">5</a></li>
    <li>
      <a href="#" aria-label="Next">
        <span aria-hidden="true">&raquo; Next</span>
      </a>
    </li>
 	<li>
      <a href="#" aria-label="Next">
        <span aria-hidden="true">&raquo; Last</span>
      </a>
    </li>
 

  </ul>
  </div> <!-- Panel Body -->
</div>


</div>

	

@stop
