<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Slip</title>
</head>
<body style="font-family:arial, helvetica,serif,;  color:#333; margin:0; padding:0;">
	<table style="width: 100%; margin: 0px auto;  padding: 0px 0px;">
		<tr >
			<td colspan="2">
				<center><h2><u>INVOICE</u></h2></center>
			</td>
			
		</tr>


		<tr style="font-size:12px;">
			<td>
				<b>Shipper name:</b> {{$shipper_name}}
			</td>
			
			<td>
				<b>Consignee name:</b> {{$consignee_name}}
			</td>
		</tr>

		<tr style="font-size:12px;">
			<td>
				<b>Shipper address:</b> {{$shipper_address}}

			</td>
			
			<td>
				<b>Consignee address:</b> {{$consignee_address}}
			</td>
		</tr>

		<tr >
			<td colspan="2">
				<h4 style="font-size:12px;">Required for customs and security</h4>
				<table class="table" cellspacing="0" border="0" cellpadding="0" style="width:100%; margin-top:30px; margin-bottom:10px; border:1px solid #ddd;">
				    <thead>
				      <tr>
				        <th style="padding:10px 8px; font-size:12px;  color:#333; border-right:1px solid #ddd;" align="left">Count</th>
				        <th style="padding:10px 8px; font-size:12px;  color:#333; border-right:1px solid #ddd;" align="left">Item #</th>
				        <th style="padding:10px 8px; font-size:12px;  color:#333; border-right:1px solid #ddd;" align="left">Description</th>
				        <th style="padding:10px 8px; font-size:12px;  color:#333; border-right:1px solid #ddd;" align="left">Units</th>
				        <th style="padding:10px 8px; font-size:12px;  color:#333; border-right:1px solid #ddd;" align="left">Unit Value</th>
				        <th style="padding:10px 8px; font-size:12px;  color:#333; border-right:1px solid #ddd;" align="left">Total Value</th>
				      </tr>
				    </thead>

				    <tbody>
				    	@foreach($receivedItems as $key)
					    <tr  @if(fmod($sn,2)!=0) style="background:#eee"; @endif>
					    	<td style="padding:10px 8px; font-size:12px;  color:#333; border-right:1px solid #ddd;" align="left">{{$sn++}}</td>
					        <td style="padding:10px 8px; font-size:12px;  color:#333; border-right:1px solid #ddd;" align="left">{{$key['package_id']}}</td>
					        <td style="padding:10px 8px; font-size:12px;  color:#333; border-right:1px solid #ddd;" align="left">{{@$key['description']}}</td>
					        <td style="padding:10px 8px; font-size:12px;  color:#333; border-right:1px solid #ddd;" align="left">{{$key['qty']}}</td>
					        <td style="padding:10px 8px; font-size:12px;  color:#333; border-right:1px solid #ddd;" align="left">{{$key['price']}}</td>
					        <td style="padding:10px 8px; font-size:12px;  color:#333; border-right:1px solid #ddd;" align="left">{{$key['value']}}</td>
					    </tr>
					    @endforeach
					    
					    
				    </tbody>
				</table>


			</td>

		</tr>

		<tr style="font-size:12px;">
			<td colspan="2"><h4>I certify that this shipment does not contain and dangerous goods as per IATA Transport Regulations such as Explosives, compressed gases oxidizers, corrosives, flammable liquids and solid, firearms, radioactive materials and poisons 	qy7 considered hazardous.</h4></td>
		</tr>
		<tr style="font-size:10px;">
			<td>
				<b>Signature:</b>
			</td>
			<td>
				<b>Date:</b>
			</td>
		</tr>
		
	</table>

</body>
</html>