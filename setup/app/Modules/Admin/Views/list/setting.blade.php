@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')
@section('content')
{!! Html::script('theme/admin/custome/js/validation.js') !!}
<div class="container-fluid">

<div class="panel panel-default">
  <div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;<b>Setting</b></div>
  <div class="panel-body">
  	<div class="col-md-12 row">
  		{!! Form::model(['method' => 'POST', 'url' => ['admin/setting']]) !!}
  			<div class="row">
				<div class="col-sm-4">
					<div class="form-group {{ $errors->has('support_email') ? 'has-error' : '' }}">
						{!! Form::label('UserFirstName', 'Support Email:') !!}
		                <div>
							{!! Form::text('support_email', $info->SupportEmail, ['class'=>'form-control', 'placeholder'=> 'Support Email','id' =>'inputError1']) !!}
							@if ($errors->has('support_email')) <p class="help-block">{{ $errors->first('support_email') }}</p> @endif
		                </div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4">
					<div class="form-group {{ $errors->has('financeemail') ? 'has-error' : '' }}">					
		        		{!! Form::label('financeemail', 'Finance Email:') !!}
		                <div >
						{!! Form::text('financeemail', $info->FinanceEmail, ['class'=>'form-control', 'placeholder'=> 'Finance Email']) !!}
						@if ($errors->has('financeemail')) <p class="help-block">{{ $errors->first('financeemail') }}</p> @endif
		                </div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4">
					<div class="form-group {{ $errors->has('privacyemail') ? 'has-error' : '' }}">					
		        		{!! Form::label('privacyemail', 'Privacy Email:') !!}
		                <div >
						{!! Form::text('privacyemail', $info->PrivacyEmail, ['class'=>'form-control', 'placeholder'=> 'Privacy Email']) !!}
						@if ($errors->has('privacyemail')) <p class="help-block">{{ $errors->first('privacyemail') }}</p> @endif
		                </div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4">
					<div class="form-group {{ $errors->has('PaginationLimit') ? 'has-error' : '' }}">					
		        		{!! Form::label(' PaginationLimit', 'Pagination Limit:') !!}
		                <div >
						{!! Form::text('PaginationLimit', Auth::user()->PaginationLimit, ['class'=>'form-control', 'placeholder'=> 'Pagination Limit']) !!}
						@if ($errors->has('PaginationLimit')) <p class="help-block">{{ $errors->first('PaginationLimit') }}</p> @endif
		                </div>
					</div>
				</div>
			</div>
				<div class="inline-form">
					<div class="form-group">
						{!! Form::label('', '') !!}
						{!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
					</div>
				</div>
        </form>
        {!! Form::close() !!}
    </div>
	<div class="clearfix"></div>
  	
  </div> <!-- Panel Body -->
</div>

</div> <!-- Panel Body -->
</div>
@include('Admin::layouts.footer')
@stop
