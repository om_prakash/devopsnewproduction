@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')


@section('content')
<script>

$(document).ready(function(){
	
        $("#cityForm1").submit(function(){
		var flag = true;
		
		 if($("#Content1").val() =="")
			{ 
				 flag = false;
				 $("#cityDiv1").addClass("has-error");
				 $("#city_msg1").show();
			}
			else
			{
				 $("#city_msg1").hide();
			}
		return flag;
	});


    $("#cityForm").submit(function(){
		var flag = true;
		
		 if($("#Content3").val() =="")
			{ 
				 flag = false;
				 $("#cityDiv3").addClass("has-error");
				 $("#city_msg3").show();
			}
			else
			{
				 $("#city_msg3").hide();
			}
		return flag;
	});
});


</script>
<div class="container-fluid">
<div class="panel panel-default">
  <div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;<b>{{$title}}</b></div>
  <div class="panel-body">		
		{!! Form::model('', ['method' => 'get', 'url' => ['admin/city_list']]) !!}			
        	<div class="inline-form">
				<div class="form-group">
					{!! Form::label('Content', 'City Name', ['class'=>'control-lable']) !!}
				                  
						{!! Form::text('search_value', Input::get('search_value'), ['class'=>'form-control', 'placeholder'=> 'Search by City','id' =>'inputError1']) !!}
								
                </div>
				 </div>
                 <div class="inline-form">
					<div class="form-group">
						{!! Form::label('StateName', 'State Name', ['class'=>'control-lable']) !!}         
						{!! Form::text('stateName', Input::get('stateName'), ['class'=>'form-control', 'placeholder'=> 'Search by State','id' =>'inputError1']) !!}
	                </div>  
                </div>
                <div class="inline-form">
					<div class="form-group">
						{!! Form::label('countryName', 'Country Name', ['class'=>'control-lable']) !!}         
						{!! Form::text('countryName', Input::get('countryName'), ['class'=>'form-control', 'placeholder'=> 'Search by Country']) !!}
	                </div>  
                </div>
                <div class="inline-form">
                <div class="form-group">

					{!! Form::submit('Search', ['class' => 'btn btn-primary']) !!}

					<a href="{{URL::to('admin/city_list?id='.Input::get('id'))}}" class="btn btn-primary">
						<span aria-hidden="true" class="glyphicon glyphicon-refresh">
						
						</span>&nbsp;&nbsp;Reset
                    </a>

	         @if(Input::get('id') != '58391e807ac6f63d108b4567')    
					 <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#cityModal" data-whatever="@mdo">Add City</button>
             @else  
					 <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#cityModal2" data-whatever="@mdo">Add City</button>		
			 @endif
				</div>
				</div>
		{!! Form::close() !!}		
	<div class="clearfix"></div>

 
    <!-- Pagination Section-->
	<input type="hidden" name="urlvalue" id="urlvalue" value="{{$paginationurl}}" />
	<input type="hidden" name="postvalue" id="postvalue" value="{{$postvalue}}" />
	<input type="hidden" name="orderby" id="orderby" value="{{$orderby}}" />
	<input type="hidden" name="orderType" id="orderType" value="{{$orderType}}" />
	<div id="containerdata"></div>
	<div class="clearfix"></div>
	
  	
  </div> <!-- Panel Body -->
</div>
    
    
   </div> 
  </div> <!-- Panel Body -->

<!-- Pop Up --> 
<div class="modal fade" id="cityModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title" id="exampleModalLabel">Add City</h4>
	  </div>
	  
	  {!! Form::model('', ['name' => 'cityForm1', 'id' => 'cityForm1', 'method' => 'POST', 'url' => 'admin/add_city?id='.Input::get('id')]) !!}
	  
	  <div class="modal-body clearfix">
	        
			<div class="form-group">
				{!! Form::label('State', 'Select State:',['class' => 'control-label']) !!}
				<select name= "state" class="form-control" >
					<option value="">Select State</option>
					<?php foreach($state as $val){ ?>
						<option  value='{"id":"<?php echo $val->_id; ?>","name":"<?php echo $val->Content;?>"}' <?php if($val->_id == Input::get('id')) { echo 'selected="selected"'; } ?> ><?php echo $val->Content;?></option>
					<?php } ?>
				</select>
			</div>
	       
			<div class="form-group" id="cityDiv1">
				{!! Form::label('Content', 'City Name:',['class' => 'control-label']) !!}
				{!! Form::text('Content', '', ['class'=>'form-control', 'placeholder'=> 'City Name','id' =>'Content1']) !!}
				<p class="help-block" id="city_msg1" style="display:none;">City field is required.</p>	
			</div>
            <div class="col-md-12">
						<div class="col-md-6">
						<div class="form-group" id="cityDiv1">
						<div class="form-group ">
                           {!! Form::label('Status', 'Status'),':' !!}  </br>
                           <input type="radio" name="city_status" value="Active" checked> Active &nbsp;
                           <input type="radio" name="city_status" value="Inactive"> Inactive
                        </div>
                        <div class="clearfix"></div>
                     
						</div>	
						</div>
						<div class="col-md-6">
						<div class="form-group" id="cityDiv1">
						<div class="form-group ">
                           {!! Form::label('Sinup_Status', 'Sinup Status'),':' !!}  </br>
                           <input type="radio" name="city_sinup_status" value="active" checked> Active &nbsp;
                           <input type="radio" name="city_sinup_status" value="inactive"> Inactive
                        </div>
                        <div class="clearfix"></div>
                        </div>
                    
						
				      </div>
						</div>
	  </div>
	  <div class="modal-footer">
		{!! Form::button('Close', ['class' => 'btn btn-primary', 'data-dismiss'=>'modal']) !!}	
		{!! Form::submit('Add', ['class' => 'btn btn-primary']) !!}
	  </div>
	
	{!! Form::close() !!}
	<div class="clearfix"></div>
	</div>
  </div>
</div>
<!--- PopUp End	-->	

<!-- Pop Up --> 
<div class="modal fade" id="cityModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title" id="exampleModalLabel">Add City</h4>
	  </div>
	  
	  {!! Form::model('', ['name' => 'cityForm', 'id' => 'cityForm', 'method' => 'POST', 'url' => 'admin/post_add_city?id='.Input::get('id')]) !!}
	  
	  <div class="modal-body clearfix">
	              
	       
			<div class="form-group" id="cityDiv">
				{!! Form::label('Content', 'City Name:',['class' => 'control-label']) !!}
				{!! Form::text('Content', '', ['class'=>'form-control', 'placeholder'=> 'City Name','id' =>'Content3']) !!}
				<p class="help-block" id="city_msg3" style="display:none;color:red;">City field is required.</p>	
			</div>
            <div class="col-md-12">
						<div class="col-md-6">
						<div class="form-group" id="cityDiv3">
						<div class="form-group ">
                           {!! Form::label('Status', 'Status'),':' !!}  </br>
                           <input type="radio" name="city_status" value="Active" checked> Active &nbsp;
                           <input type="radio" name="city_status" value="Inactive"> Inactive
                        </div>
                        <div class="clearfix"></div>
                     
						</div>	
						</div>
						<div class="col-md-6">
						<div class="form-group" id="cityDiv3">
						<div class="form-group ">
                           {!! Form::label('Sinup_Status', 'Sinup Status'),':' !!}  </br>
                           <input type="radio" name="city_sinup_status" value="active" checked> Active &nbsp;
                           <input type="radio" name="city_sinup_status" value="inactive"> Inactive
                        </div>
                        <div class="clearfix"></div>
                        </div>
                    
						
				      </div>
						</div>
	  </div>
	  <div class="modal-footer">
		{!! Form::button('Close', ['class' => 'btn btn-primary', 'data-dismiss'=>'modal']) !!}	
		{!! Form::submit('Add', ['class' => 'btn btn-primary']) !!}
	  </div>
	
	{!! Form::close() !!}
	<div class="clearfix"></div>
	</div>
  </div>
</div>
<!--- PopUp End	-->	

@include('Admin::layouts.footer')
@stop
