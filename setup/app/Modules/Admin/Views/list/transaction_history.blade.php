@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')
@section('content')
{!! Html::style('theme/date-time-picker/jquery.datetimepicker.css') !!}

{!! Html::script('theme/date-time-picker/jquery.datetimepicker.js') !!}

<div class="container-fluid">

<div class="panel panel-default">
  <div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;<b>{{$title}}</b></div>
  <div class="panel-body">
  	<div class="col-md-12 row">
  		<form role="form" >
			<div class="inline-form">
				<div class="form-group">
					{!! Form::model(['method' => 'PATCH', 'url' => ['admin/users']]) !!}
        			{!! Form::label('UserFirstName', 'Deposit By / Deposit To') !!}
                    <div>
						{!! Form::text('search_value', Input::get('search_value'), ['class'=>'form-control', 'placeholder'=> 'Deposit By / Deposit To']) !!}
                    </div>
				</div>
			</div>

			<div class="inline-form">
				<div class="form-group">
					{!! Form::label('status', 'Status') !!}
					<div>
						{!! Form::select("status", array("all" => "All", "completed" => "Completed", "pending" => "Pending"), "all", ["class" => "form-control"]) !!}
					</div>
				</div>
			</div>

			<div class="inline-form">
				<div class="form-group">
					{!! Form::label('StartDate', 'From', ['class'=>'control-lable']) !!}
					<div>
						{!! Form::text('StartDate', Input::get('StartDate'), ['class'=>'form-control', 'placeholder'=> 'From','id' => 'startDate','readonly'=> 'readonly']) !!}
					</div>
				</div>
			</div>
			<div class="inline-form">
				<div class="form-group">
					{!! Form::label('EndDate', 'To', ['class'=>'control-lable']) !!}
					<div>
						{!! Form::text('EndDate', Input::get('EndDate'), ['class'=>'form-control', 'placeholder'=> 'To','id' => 'endDate','readonly'=> 'readonly']) !!}
					</div>
				</div>
			</div>

			<div class="inline-form">
				<div class="form-group">
					{!! Form::label('', '') !!}
					{!! Form::submit('Search', ['class' => 'btn btn-primary']) !!}

					<a href="{{URL('admin/transporter-transaction-history')}}" class="btn btn-primary">
						<span aria-hidden="true" class="glyphicon glyphicon-refresh">
						</span>&nbsp;&nbsp;Reset
                    </a>
				</div>
			</div>
        </form>
        {!! Form::close() !!}
    </div>
	<div class="clearfix"></div>
      <!-- Pagination Section-->
	<input type="hidden" name="urlvalue" id="urlvalue" value="{{$paginationurl}}" />
	<input type="hidden" name="postvalue" id="postvalue" value="{{$postvalue}}" />
	<input type="hidden" name="orderby" id="orderby" value="{{$orderby}}" />
	<input type="hidden" name="orderType" id="orderType" value="{{$orderType}}" />
	<div id="containerdata"></div>
	<div class="clearfix"></div>


  </div> <!-- Panel Body -->
</div>

</div> <!-- Panel Body -->
</div>
<script>
	$(function(){
		$('#startDate').datetimepicker({
			format:'d/m/Y',
			onChangeDateTime:function( ct ){
				$('#endDate').datetimepicker({	minDate:ct	})
			},
			timepicker:false,
		});
		$('#endDate').datetimepicker({
			format:'d/m/Y',
			onChangeDateTime:function( ct ){
				$('#startDate').datetimepicker({	maxDate:ct	})
			},
			timepicker:false,
		});
	});
</script>
@include('Admin::layouts.footer')
@stop
