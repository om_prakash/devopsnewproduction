@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')

@section('content')
{!! Html::style('theme/date-time-picker/jquery.datetimepicker.css') !!}

{!! Html::script('theme/date-time-picker/jquery.datetimepicker.js') !!}

<div class="container-fluid">
<div class="panel panel-default">
  <div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;<b>{{$title}}</b></div>
  <div class="panel-body">
		{!! Form::model('', ['method' => 'post', 'url' => ['admin/packing-slip-list']]) !!}
				<div class="inline-form">
                <div class="form-group">
					{!! Form::label('Package Number', 'Search by Package Number or Requester Name', ['class'=>'control-lable']) !!}
					<div>
						{!! Form::text('search_value', Input::get('search_value'), ['class'=>'form-control', 'placeholder'=> 'Package Number / Requester Name','id' =>'inputError1']) !!}
					</div>
                </div>
                </div>
                <div class="inline-form">
                	<div class="form-group">
						{!! Form::label('from', 'Created Date', ['class'=>'control-lable']) !!}
						<div>
							{!! Form::text('StartDate', Input::get('StartDate'), ['class'=>'form-control', 'placeholder'=> 'Created Date','id' => 'startDate','readonly' => 'readonly','style' => 'width:240px;']) !!}
						</div>
	                </div>
	            </div>

                <div class="inline-form">
                <div class="form-group">
					{!! Form::submit('Search', ['class' => 'btn btn-primary']) !!}
					<a href="{{url('')}}/admin/packing-slip-list" class="btn btn-primary mt-10">
						<span aria-hidden="true" class="glyphicon glyphicon-refresh">
						</span>&nbsp;&nbsp;Reset
                	</a>
                    </div>
				</div>

		{!! Form::close() !!}

	<a href="{{url('')}}/admin/get-packing-slip" class="btn btn-primary" style="margin-top: 10px;">
      <span> Create Packing Slip </span>
	</a>
	<div class="clearfix"></div>


      <!-- Pagination Section-->
	<input type="hidden" name="urlvalue" id="urlvalue" value="{{$paginationurl}}" />
	<input type="hidden" name="postvalue" id="postvalue" value="{{$postvalue}}" />
	<input type="hidden" name="orderby" id="orderby" value="{{$orderby}}" />
	<input type="hidden" name="orderType" id="orderType" value="{{$orderType}}" />
	<div id="containerdata"></div>
	<div class="clearfix"></div>


  </div> <!-- Panel Body -->


</div>
</div>

<script type="text/javascript">
	$(function(){
		$('#startDate').datetimepicker({
			format:'M d,Y',
			datepicker:true,
			timepicker:false,

		});
	});
</script>

@include('Admin::layouts.footer')

@stop
