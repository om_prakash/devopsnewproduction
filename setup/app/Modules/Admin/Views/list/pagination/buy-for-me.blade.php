<table class="table table-bordered table-striped table-highlight table-list">
<thead>
	<tr>

		<th style="width:1%;">SNo.</th>
		<th style="width:1%;">Package ID</th>
		<th style="width:10%;">Package Name</th>
		<th style="width:12%;">Requester Name</th>
		<th style="width:15%;">Drop Off Address</th>
		<th style="width:6%;" class="text-center"> Shipping Mode </th>
		<th style="width:100px">Category</th>
		<th style="width:10%">Created Date</th>
		<th style="width:60px; text-align:center" >Status</th>
		<th style="width:15%;">Action</th>
	</tr>
	</thead>
	<tbody>
		<?php if (count($users) > 0) {
	?>

			@foreach ($users as $lists)
			<tr id="row-{{$lists->_id}}">
				<td scope="row">{{$sno++}}</td>

				<td><a href="{{ url('admin/buy-for-me/detail',$lists->_id) }}">{{$lists->PackageNumber}}</a></td>

				<td>{{ucfirst($lists->ProductTitle)}}</td>

				<td>
					<a data-toggle="modal" href="#myModal" onclick="return get_user_info('{{$lists->RequesterId}}')">{{ucfirst($lists->RequesterName)}}</a>
				</td>

				<td>{{ucfirst($lists->DeliveryFullAddress)}}</td>
				<td class="text-center">
					@if (@$lists->ProductList[0]['travelMode'] == 'ship')
						Sea
					@else
						{{ ucfirst(@$lists->ProductList[0]['travelMode']) }}
					@endif
				</td>
				<td>@foreach($lists->ProductList as $index)
				     	{{$index['category']}}</br>
				   	@endforeach
				</td>
				<td>{{ show_date(@$lists->EnterOn) }}</td>
				<td align="center">
					<span class="{{ get_status_title($lists->Status)['class'] }}">
						{{ get_status_title($lists->Status)['status'] }}
					</span>
				</td>
				<td class="width-action-20">

                    <div style="position:relative;" class="visible-ipad">
                    	<button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    	Action
                    <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu action-menu">

	                    <li>
							<a href="{{ url('admin/buy-for-me/detail',$lists->_id) }}" data-placement="top" title="View Detail" class="btn btn-small btn-primary btn-action"><i class="fa fa-info"></i>&nbsp;Detail</a>

	                    	<a data-placement="top" title="Delete Record" class="btn btn-small btn-danger btn-action" onclick="remove_record('delete_activity/{{$lists->_id}}/package','{{$lists->_id}}')" href="javascript:void(0)"><i class="fa fa-trash"></i>&nbsp;Delete</a>
	                    </li>
	                    <li>
							<a href="{{ url('admin/package/detail',$lists->_id) }}" data-placement="top" title="Refund" class="btn btn-small btn-primary btn-action"><i class="fa fa-credit-card"></i>&nbsp;Refund</a>
	                    </li>

	                    <li>
	                    	@if($lists->Status == 'pending')
							<a href="{{ url('admin/buy-for-me/edit',[$lists->_id]) }}" data-placement="top" title="Edit" class="btn btn-small btn-info btn-action"><i class="fa fa-edit"></i>&nbsp;Edit</a>
							@endif
	                    </li>
	                    <li>
	                    	@if($lists->Status == 'pending')
								<a data-toggle="modal" data-target="#check_mark{{$lists->_id}}"  title="Mark as Paid" class="btn btn-success"><i class="fa fa-check-square-o"></i>&nbsp;Mark as Paid
								</a>
							@endif
	                    </li>
	                    <li>
	                    	@if($lists->Status == 'not_purchased')
							<a data-toggle="modal" data-target="#paid_mark{{$lists->_id}}"  title="Mark as Paid" class="btn btn-success"><i class="fa fa-check-square-o"></i>&nbsp;Mark as Paid
							</a>
							@endif
	                    </li>
	                    <li>
	                    	@if(!in_array($lists->Status,['delivered','cancel','cancel_by_admin','deleted']))
							<a href="{{url('admin/complete-request',$lists->_id)}}"  title="Request Delivered" class="btn btn-success" onclick="return confirm('Are you sure? you want to complete this request.')"><span class="glyphicon glyphicon-saved" aria-hidden="true"
							></span>&nbsp;&nbsp;Request Delivered
							</a>
							@endif
	                    </li>
	                    <li>
	                    	<a href="{{url('admin/comment-list',$lists->_id)}}"  title="Add Comment" class="btn btn-success"><span class="glyphicon glyphicon-comment" aria-hidden="true"></span>&nbsp;&nbsp;Add Comment
							</a>
	                    </li>
	                    <li>
	                    	<a href="{{url('admin/activity-log?request_id='.$lists->_id)}}"  title="Activity Log" class="btn btn-success btn-info btn-action"><span class="fa fa-history" aria-hidden="true"></span>&nbsp;&nbsp;Activity Log
							</a>
	                    </li>
	                    <li>
	                    	@if($lists->Status == 'momo_initiated')
								<a href="javascript:void(0)"  title="Ipay Status" class="btn btn-success btn-info btn-action" onclick="return getIpayStatus('{{$lists->_id}}')"><i class="fa fa-delicious" aria-hidden="true"></i>
								</a>&nbsp;Ipay Status
							@endif
	                    </li>
	                    <li>
	                    	@if(!in_array($lists->Status,['delivered','cancel','deleted','cancel_by_admin']))
							<a href="{{url('admin/cancel-request',$lists->_id)}}"  title="Cancel Request" class="btn btn-small btn-danger btn-action" onclick="return confirm('Are you sure? you want to cancel this package.')"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span>&nbsp;&nbsp;Cancel Request
							</a>
							@endif
	                    </li>

                    </ul>
                    </div><?php
if ($lists->Status === "cancel") {?>
							<a data-toggle="modal" data-target="#refund-modal" onclick="get_package_information('<?php echo $lists->_id; ?>')" title="Refund" class="btn btn-small btn-primary btn-action"><i class="fa fa-credit-card"></i></a>

					<?php }?>
					<button type="button" title="Penalty Amount" class="btn btn-small btn-info btn-action" data-toggle="modal" data-target="#exampleModal{{$lists->_id}}"><i class="fa fa-money"></i></button>


					<a data-placement="top" title="Delete Record" class="btn btn-small btn-danger btn-action" onclick="remove_record('delete_activity/{{$lists->_id}}/package','{{$lists->_id}}')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>
					<a href="{{ url('admin/buy-for-me/detail',[$lists->_id]) }}" data-placement="top" title="View Detail" class="btn btn-warning"><i class="fa fa-info"></i></a>
					@if($lists->Status == 'pending')
					<a href="{{ url('admin/buy-for-me/edit',[$lists->_id]) }}" data-placement="top" title="Edit" class="btn btn-small btn-info btn-action"><i class="fa fa-edit"></i></a>
					@endif

					@if($lists->Status == 'pending')
					<a data-toggle="modal" data-target="#check_mark{{$lists->_id}}"  title="Mark as Paid" class="btn btn-success"><i class="fa fa-check-square-o"></i>
					</a>
					@endif

					@if($lists->Status == 'not_purchased')
					<a data-toggle="modal" data-target="#paid_mark{{$lists->_id}}"  title="Mark as Paid" class="btn btn-success"><i class="fa fa-check-square-o"></i>
					</a>
					@endif

					@if(!in_array($lists->Status,['delivered','cancel','cancel_by_admin','deleted']))
					<a href="{{url('admin/complete-request',$lists->_id)}}"  title="Request Delivered" class="btn btn-success" onclick="return confirm('Are you sure? you want to complete this request.')"><span class="glyphicon glyphicon-saved" aria-hidden="true"
					></span>
					</a>
					@endif

					<a href="{{url('admin/comment-list',$lists->_id)}}"  title="Add Comment" class="btn btn-success"><span class="glyphicon glyphicon-comment" aria-hidden="true"></span>
					</a>

					<a href="{{url('admin/activity-log?request_id='.$lists->_id)}}"  title="Activity Log" class="btn btn-success btn-info btn-action"><span class="fa fa-history" aria-hidden="true"></span>
					</a>

					@if($lists->Status == 'momo_initiated')
						<a href="javascript:void(0)"  title="Ipay Status" class="btn btn-success btn-info btn-action" onclick="return getIpayStatus('{{$lists->_id}}')"><i class="fa fa-delicious" aria-hidden="true"></i>
						</a>
					@endif

					@if(!in_array($lists->Status,['delivered','cancel','deleted','cancel_by_admin']))
					<a href="{{url('admin/cancel-request',$lists->_id)}}"  title="Cancel Request" class="btn btn-small btn-danger btn-action" onclick="return confirm('Are you sure? you want to cancel this package.')"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span>
					</a>
					@endif


				</td>
			</tr>
			@endforeach
		<?php } else {?>
			<tr>
				<td colspan="10" class="text-center" >Package not found</td>
			</tr>
		<?php }?>
	</tbody>
</table>
@extends('Admin::list.pagination.footer')
@foreach ($users as $lists)
<!----- Pop Up - -->
<div class="modal fade" id="exampleModal{{$lists->_id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title" id="exampleModalLabel">Penalty Amount</h4>
	  </div>

	  {!! Form::model('', ['name' => 'countryForm1', 'id' => 'countryForm1', 'method' => 'POST', 'url' => ['admin/refund_amount', $lists->_id]]) !!}

	  <div class="modal-body">
			<div class="form-group" id="cityDiv1">
				{!! Form::label('Penalty', 'Penalty Amount:',['class' => 'control-label']) !!}
				{!! Form::text('penalty', $lists->Penalty, ['class'=>'form-control', 'placeholder'=> 'Penalty Amount','id' =>'penalty']) !!}

				<p class="help-block" id="state_msg1" style="display: none;">Penalty field is required.</p>
				<p class="help-block" id="state__valid_msg1" style="display: none;">Only numbers are required.</p>
			</div>

	  </div>
	  <div class="modal-footer">
		{!! Form::button('Close', ['class' => 'btn btn-primary', 'data-dismiss'=>'modal']) !!}
		{!! Form::submit('Submit', ['class' => 'btn btn-primary','onclick' => "return confirm('Are you sure? You want to charge penalty from transporter!');"]) !!}
	  </div>
		<div class="clearfix"></div>
		{!! Form::close() !!}

	</div>
  </div>
</div>

<!-- mark paid after request create -->
<div class="modal fade" id="check_mark{{$lists->_id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title" id="exampleModalLabel">Mark as Paid</h4>
	  </div>

	  {!! Form::model('', ['name' => 'paid_payment', 'id' => 'paid_payment', 'method' => 'POST', 'url' => ['admin/mark_paid']]) !!}
	  <input type="hidden" name="requestid" value="{{$lists->_id}}" id="requestid">
	  <input type="hidden" name="request_type" value="buy_for_me" id="request_type">

	  <div class="modal-body">
			<div class="form-group" id="">
			<label>Description/Reason</label>
				<textarea class="form-control" name="description" placeholder="Description/Reason"></textarea>
				<p class="help-block" id="description" style="display: none;"></p>

			</div>

	  </div>
	  <div class="modal-footer">
		{!! Form::button('Close', ['class' => 'btn btn-primary', 'data-dismiss'=>'modal']) !!}
		{!! Form::submit('Submit', ['class' => 'btn btn-primary','onclick' => "return confirm('Are you sure? You want to mark as paid to this request!');"]) !!}
	  </div>
		<div class="clearfix"></div>
		{!! Form::close() !!}

	</div>
  </div>
</div>
<!-- end mark paid -->




<!-- mark paid after item update -->
<div class="modal fade" id="paid_mark{{$lists->_id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title" id="exampleModalLabel">Mark as Paid</h4>
	  </div>

	  {!! Form::model('', ['name' => 'paid_payment2', 'id' => 'paid_payment2', 'method' => 'POST', 'url' => ['admin/mark_paid_after_item_update']]) !!}
	  <input type="hidden" name="requestid" value="{{$lists->_id}}" id="requestid">
	  <input type="hidden" name="request_type" value="buy_for_me" id="request_type">

	  <div class="modal-body">
			<div class="form-group" id="">
			<label>Description/Reason</label>
				<textarea class="form-control" name="description" placeholder="Description/Reason"></textarea>
				<p class="help-block" id="description" style="display: none;"></p>
			</div>

	  </div>
	  <div class="modal-footer">
		{!! Form::button('Close', ['class' => 'btn btn-primary', 'data-dismiss'=>'modal']) !!}
		{!! Form::submit('Submit', ['class' => 'btn btn-primary','onclick' => "return confirm('Are you sure? You want to mark as paid to this request!');"]) !!}
	  </div>
		<div class="clearfix"></div>
		{!! Form::close() !!}

	</div>
  </div>
</div>
<!-- end mark paid -->
<script>
$(document).ready(function(){
        $("#countryForm1").submit(function(){
		var flag = true;
		var numbers = /^[0-9]+$/;
		 if($("#penalty").val() =="")
			{
				 flag = false;
				 $("#cityDiv1").addClass( "has-error" );
				 $("#state_msg1").show();
			}
			else
			{
				 $("#state_msg1").hide();
				 if($.isNumeric($("#penalty").val()))
			      {
				 	 $("#state__valid_msg1").hide();
			      }
			      else
			      {
			      	 flag = false;
			      	 $("#cityDiv1").addClass( "has-error");
				 	 $("#state__valid_msg1").show();
			      }
			}

		return flag;

	});

});


$('#TotalRecordFound').html('Showing Records: '+{{$count}});
</script>


<!--- PopUp End	-->
@endforeach