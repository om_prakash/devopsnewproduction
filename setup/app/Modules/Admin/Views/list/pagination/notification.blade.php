<table class="table table-bordered table-striped table-highlight table-list">
<thead>
  <tr>
    <th>S.No.</th>
    <th>Date</th>
    <th>Title</th>
    <th>Message</th>
    <th>Action</th>
  </tr>
  </thead>
  <tbody>
    <?php if (count($notification) > 0) {
    ?>
     @foreach ($notification as $key)
      <tr id="row-{{$key->id}}">
        <td scope="row">{{$sno++}}</td>
        <td>{{ show_date(@$key->Date) }} </td>
        <td>{{$key->NotificationTitle}}</td>
        <td>{{substr(strip_tags($key->NotificationMessage),0,250)}}</td>
        <td >
          <a>
            <button type="button" class="btn btn-small btn-info btn-action" data-toggle="modal" data-target="#myModal{{$key->id}}"><i class="fa fa-info"></i></button></a>
          <a href="{{ url('admin/view-user',$key->_id) }}" data-placement="top" title="View Detail" class="btn btn-small btn-primary btn-action"><i class="fa fa-th-list"></i></a>
        </td>
      </tr>
    @endforeach
    <?php } else {?>
      <tr>
        <td colspan="7" class="text-center" >Notification not found</td>
      </tr>
    <?php }?>
  </tbody>
</table>
@extends('Admin::list.pagination.footer')
@foreach ($notification as $key)
<!-- Small modal -->

<div class="modal fade" id="myModal{{$key->id}}" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Notification Details</h4>
        </div>
        <div class="modal-body my-modal-body">
        <div class="table-responsive">
          <table class="table table-hover table-striped my-table">
              <tbody>
                    <tr>
                        <td>Date:</td>
                        <td>{{ show_date(@$key->Date) }}</td>
             </tr>
                    <tr>
                        <td>Title:</td>
                        <td>{{$key->NotificationTitle}}</td>
                    </tr>

                    <tr>
                        <td>Message:</td>
                        <td>{{strip_tags($key->NotificationMessage)}}</td>
                    </tr>

                </tbody>

            </table>
        </div>

        </div>
      <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>
@endforeach


