{!! Html::script('theme/admin/fancybox/source/jquery.fancybox.js') !!}
 {!! Html::style('theme/admin/fancybox/source/jquery.fancybox.css') !!}
<table class="table table-bordered table-striped table-highlight table-list">
<thead>
	<tr>
		<th>S.No.</th>
		<th>Requester Name</th>
		<th>Email</th>
    <th>Unit#</th>
    <th>City</th>
    <th>State</th>
    <th>Country</th>
    <th>Street1</th>
    <th>Street2</th>
		<th>Status</th>
		<th width="14%">Action</th>
	</tr>
	</thead>
	<tbody>
		<?php if (count($users) > 0) {?>

			@foreach ($users as $lists)
			<tr id="row-{{$lists->_id}}">
				<td scope="row">{{$sno++}}</td>
				<td>{{$lists->Name}}</td>
				<td>{{$lists->Email}}</td>
        <td>{{$lists->UniqueNo}}</td>
        <td>
          @if($lists->City != '')
            {{$lists->City}}
          @else
            N/A
          @endif
        </td>
        <td>
          @if($lists->State != '')
            {{$lists->State}}
          @else
            N/A
          @endif
        </td>
        <td>
          @if($lists->Country != '')
            {{$lists->Country}}
          @else
            N/A
          @endif  
        </td>
        <td>
          @if($lists->Street1 != '')
            {{ucfirst($lists->Street1)}}
          @else
            N/A
          @endif
        </td>
        <td>
         @if($lists->Street2) 
          {{ucfirst($lists->Street2)}}
         @else
           N/A
         @endif   
        </td>
				<td class="text-center">
            @if ($lists->RequesterStatus == 'active')
              <span class="badge bg-green">Active</span>
            @else
              <span class="badge bg-yellow">Inactive</span>
            @endif
            @if($lists->delete_status == 'yes')
              <br />
              <span class="red">User deleted</span>
            @endif
        </td>
				<td class="text-center">
          <div style="position:relative;" class="visible-ipad">
            <button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Action <span class="caret"></span>
            </button>
          </div>
        <a href="#" data-toggle="modal" data-target="#myModal{{$lists->_id}}" data-placement="top" title="View Detail" class="btn btn-small btn-info btn-action"><i class="fa fa-info"></i></a>
        @if($lists->delete_status != 'yes')
					@if ($lists->RequesterStatus == 'active')
						<a href="{{ url('status_activity',[$lists->_id,'inactive','Requester']) }}?href=admin/requester" data-placement="top" title="Make Inactive" class="btn btn-small btn-success btn-action" onclick ="return confirm(' Are you sure? You want to inactivate your requester profile!');">
						<i class="fa fa-unlock"></i></a>
					@else
						<a href="{{ url('status_activity',[$lists->_id,'active','Requester']) }}?href=admin/requester" data-placement="top" title="Make Active" class="btn btn-small btn-warning btn-action" onclick ="return confirm(' Are you sure? You want to activate your requester profile!');">
						<i class="fa fa-lock"></i></a>
					@endif

					<a href="{{ url('admin/requester/edit',$lists->_id) }}" data-placement="top" title="Edit" class="btn btn-small btn-primary btn-action"><i class="fa fa-edit"></i></a>
          <a href="{{ url('admin/user-comment-list',$lists->_id) }}" data-placement="top" title="View Notes/Comments" class="btn btn-small btn-primary btn-action"><i class="fa fa-th-list"></i></a>
					<a data-placement="top" title="Delete Record" class="btn btn-small btn-danger btn-action" onclick="remove_record('delete_activity/{{$lists->_id}}/requester','{{$lists->_id}}')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>
        @endif
				</td>
			</tr>
			@endforeach
		<?php } else {?>
			<tr>
				<td colspan="7" class="text-center" >Requester not found</td>
			</tr>
		<?php }?>
	</tbody>
</table>
@extends('Admin::list.pagination.footer')
<!-- Button trigger modal -->

@foreach ($users as $lists)
<!-- Modal -->
<div class="modal fade" id="myModal{{$lists->_id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">User Detail</h4>
      </div>

      <div class="modal-body my-modal-body">
        <div class="row">
          <div class="col-sm-4">
            <p>Profile Image</p>
            <?php if ($lists->Image == "") {?>
            <img class="thumbnail" src="{{ImageUrl}}/profile/noImageAvailable.jpg" height="100px" width="100px" alt="Profile image">
            <?php } else {?>
            <a class="fancybox" href="{{ImageUrl}}{{ $lists->Image}}" title="{{$lists->Name}}">
              <img class="thumbnail" src="{{ImageUrl}}{{ $lists->Image }}" height="100px" width="100px" alt="Profile image">
            </a>
            <?php }?>
            <script type="text/javascript">
              $(document).ready(function() {
                $(".fancybox").fancybox();
              })
            </script>
          </div>
          <div class="col-sm-8" style="position: relative;text-align: center;vertical-align: middle;line-height: 90px;">
            <a href="{{ url('admin/user-comment-list',$lists->_id) }}" data-placement="top" title="View Notes/Comments" class="btn btn-small btn-primary btn-action"><i class="fa fa-th-list"></i></a>
            <a href="{{ url('admin/requester/edit',$lists->_id) }}" data-placement="top" title="Edit" class="btn btn-small btn-primary btn-action" style="margin-right: 10;"><i class="fa fa-edit"></i></a><br />
            <a href="#" data-toggle="modal" data-target="#myCommentModal{{$lists->_id}}" data-placement="top" title="Add Notes/Comments" class="btn btn-small btn-info"><i class="fa fa-plus"></i> Add Notes/Comments</a>
          </div>
        </div>


      	<div class="table-responsive">
      		<table class="table table-hover table-striped my-table">
            	<tr>
                	<td>Name:</td>
                    <td>{{$lists->Name}}</td>
                </tr>
            	<tr>
                	<td>Email:</td>
                    <td>{{$lists->Email}}</td>
                </tr>

                <tr>
                	<td>Phone Number:</td>
                    <td>{{$lists->PhoneNo}}</td>
                </tr>
                <tr>
                  <td>Alternate Phone Number:</td>
                    <td>{{$lists->AlternatePhoneNo}}</td>
                </tr>
                <tr>
                	<td>Address line-1:</td>
                    <td>{{$lists->Street1}}</td>
                </tr>
                <tr>
                	<td>Address line-2:</td>
                    <td>{{$lists->Street2}}</td>
                </tr>
                <tr>
                	<td>Country:</td>
                    <td>{{$lists->Country}}</td>
                </tr>
                <tr>
                	<td>State:</td>
                    <td>{{$lists->State}}</td>
                </tr>
                <tr>
                	<td>City:</td>
                    <td>{{$lists->City}}</td>
                </tr>
                 
				<tr>
					<td>Zip Code:</td>
					<td>
						@if(@$lists->ZipCode)
							{{$lists->ZipCode}}
						@else
							N/A
						@endif
					</td>
				</tr>
                
				<tr>
					<td> Unit#: </td>
					<td style="color: #f00;"> {{ $lists->UniqueNo }} </td>
				</tr>
                
                <tr>
                	<td>Registration Date:</td>
                  <td>{{ show_date(@$lists->EnterOn) }}</td>
                </tr>
              </table>
      </div>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
<div id="myCommentModal{{$lists->_id}}" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="exampleModalLabel">Add Comment</h4>
			</div>
			<div class="col-sm-12 ">
				{!! Form::model('', ['name' => 'commentForm', 'id' => 'commentForm', 'method' => 'POST', 'url' => ['admin/post-user-comment']]) !!}
			
					<div class="modal-body clearfix">
						<div class="form-group" id="cityDiv">
							{!! Form::label('Comment', 'Comment:',['class' => 'control-label']) !!}
							{!! Form::textarea('comment', '', ['class'=>'form-control', 'placeholder'=> 'Comment','id' =>'comment','rows'=>'3']) !!}
							<p class="help-block red" id="er_comment"></p>
						</div>
					</div>
					
					<input type="hidden" name="user_id" id="user_id" value="{{$lists->_id}} " />
					<input type="hidden" name="request_id" id="request_id" value="{{$lists->_id}}" />
					<input type="hidden" name="product_title" id="product_title" value="{{$lists->Name}}" />

					<div class="modal-footer">
						{!! Form::button('Close', ['class' => 'btn btn-primary', 'data-dismiss'=>'modal']) !!}
						{!! Form::submit('Add', ['class' => 'btn btn-primary']) !!}
					</div>

				{!! Form::close() !!}
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
@endforeach

