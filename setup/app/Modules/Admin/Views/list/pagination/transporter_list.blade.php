<table class="table table-bordered table-striped table-highlight table-list">
<thead>
	<tr>
		<th>S.No.</th>
		<th>Transporter Name</th>
		<th>Email</th>
		<th>Unit #</th>
		<th>Type</th>
		<th>Verified</th>
		<th>SSN No.</th>
		<th>Bank Account</th>
		<th>Status</th>
		<th>Action</th>
	</tr>
	</thead>
	<tbody>	<?php
if (count($users) > 0) {?>

			@foreach ($users as $lists)
			<tr id="row-{{$lists->_id}}">
				<td scope="row">{{$sno++}}</td>
				<td>{{$lists->Name}}</td>
				<td>{{$lists->Email}}</td>
                <td>{{$lists->UniqueNo}}</td>
				<td class="text-center" >
					@if(@$lists->TransporterType == 'individual')
					<span class="badge bg-green"><?php echo ucfirst($lists->TransporterType); ?></span>
					@elseif (@$lists->TransporterType == 'business')
					<span class="badge"><?php echo ucfirst($lists->TransporterType); ?></span>
					@else
					N/A
					@endif
				</td>
				<td class="text-center" id="verification_section_<?php echo $lists->_id; ?>">
					@if ($lists->TransporterStatus == "not_verify")
						<span class="verification" onclick='send_verification_code("<?php echo $lists->_id; ?>")' >Member Verified </span>
					@else
						<span  class="badge bg-green">Verified</span>
					@endif

				</td>
				<td><?php echo ($lists->SSN != '') ? 'XXXXX-' . substr($lists->SSN, -4) : ''; ?></td>
				<td id="bank_ac_section_<?php echo $lists->_id; ?>">
					@if (@$lists->StripeBankId == "")
						<!-- <span class="btn btn-primary" onclick='add_bank_account("<?php /* echo $lists->_id; */?>")'>Add To Stripe</span> -->
						<a href="{{ url('admin/edit-bank',$lists->_id) }}" class="btn btn-primary"> Add To Stripe </a>
					@else
						<span class="badge bg-green" >Added</span>
					@endif
				</td>

				<td class="text-center" >
					@if($lists->TransporterStatus == 'active')
						<span class="badge bg-green">Active</span>
					@else
						<span class="badge bg-yellow">Inactive</span>
					@endif
					@if($lists->delete_status == 'yes')
						<br /> <span class="red">Deleted</span>
					@endif
				</td>
				<td class="width-action-20">
                <div style="position:relative;" class="visible-ipad">
                 <button type="button"  class="btn btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      					Action<span class="caret"></span>
    			</button>
    			<ul class="dropdown-menu action-menu">
    				<li>
						<a href="{{ url('admin/user/detail',$lists->_id) }}" data-placement="top" title="View Detail" class="btn btn-small btn-info btn-action"><i class="fa fa-info"></i>&nbsp;Info</a>
					</li>

    			@if($lists->delete_status != 'yes')

      				<li>
                         @if ($lists->TransporterStatus == 'active')
						<a href="{{ url('status_activity',[$lists->_id,'inactive','Transporter']) }}?href=admin/transporter"  data-placement="top" title="Make Inactive" class="btn btn-small btn-warning btn-action">
							<i class="fa fa-lock"></i>&nbsp;Inactive</a>

					@else
						<a href="{{ url('status_activity',[$lists->_id,'active','Transporter']) }}?href=admin/transporter" data-placement="top" title="Make Active" class="btn btn-small btn-success btn-action">
							<i class="fa fa-unlock"></i>&nbsp;Active</a>
					@endif
					</li>
                    <li>
						<a href="{{ url('admin/edit-transporter',$lists->_id) }}" data-placement="top" title="Edit" class="btn btn-primary btn-action"><i class="fa fa-edit"></i>&nbsp;Edit</a>
					</li>

                    <li>
					<a data-placement="top" title="Delete Record" class="btn btn-small btn-danger btn-action" onclick="remove_record('delete_activity/{{$lists->_id}}/carrier','{{$lists->_id}}')" href="javascript:void(0)"><i class="fa fa-trash"></i>&nbsp;Delete</a>
                    </li>
                @endif

    </ul>
                </div>
                	<a href="{{ url('admin/user/detail',$lists->_id) }}" data-placement="top" title="View Detail" class="btn btn-small btn-info btn-action"><i class="fa fa-info"></i></a>

                	<a href="javascript:void(0)" data-toggle="modal" data-target="#transfer_fund_modal" title="Transfer Fund" onclick="transfer_fund('<?php echo $lists->id; ?>')" class="btn btn-small btn-info btn-action"><i class="fa fa-exchange"></i></a>

					<a href="{{ url('admin/transaction-history?userid='.$lists->_id) }}" title="Transaction History" class="btn btn-small btn-info btn-action"><i class="fa fa-money"></i></a>

					 <a href="javascript:void(0)"  data-toggle="modal" title="Payment details" data-target="#myModal_transfer"  onclick="transfer_history('<?php echo $lists->id; ?>')" class="btn btn-small btn-info btn-action"><i class="fa fa-list"></i></a>
                @if($lists->delete_status != 'yes')
					@if ($lists->TransporterStatus == 'active')
						<a href="{{ url('status_activity',[$lists->_id,'inactive','Transporter']) }}?href=admin/transporter" data-placement="top" title="Make Inactive" class="btn btn-small btn-success btn-action" onclick ="return confirm(' Are you sure? You want to inactivate your transporter profile!');">
							<i class="fa fa-unlock"></i></a>
					@else
						<a href="{{ url('status_activity',[$lists->_id,'active','Transporter']) }}?href=admin/transporter" data-placement="top" title="Make Active" class="btn btn-small btn-warning btn-action" onclick ="return confirm(' Are you sure? You want to activate your transporter profile!');">
							<i class="fa fa-lock"></i></a>
					@endif

					<a href="{{ url('admin/edit-transporter',$lists->_id) }}" data-placement="top" title="Edit" class="btn btn-primary btn-action"><i class="fa fa-edit"></i></a>

					<a data-placement="top" title="Delete Record" class="btn btn-small btn-danger btn-action" onclick="remove_record('delete_activity/{{$lists->_id}}/transporter','{{$lists->_id}}')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>

					@endif

				</td>
			</tr>
			@endforeach
		<?php } else {?>
			<tr>
				<td colspan="9" class="text-center" >Transporter not found</td>
			</tr>
		<?php }?>
	</tbody>
</table>
@extends('Admin::list.pagination.footer')

