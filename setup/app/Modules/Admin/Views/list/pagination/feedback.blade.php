<?php
$orderType = ($orderType == "Asc")? "Desc" : "Asc";
			
$first = "Asc";
$firstbydefault = "Twoside";

if($orderby == "Rating") { $first = $orderType; $firstbydefault="Oneside"; }

?>
<table class="table table-bordered table-striped table-highlight table-list">
<thead>
	<tr>
		<th>S.No.</th>
		<th>Date</th>
		<th>User Name</th>
		<th onclick="Changeorder('Rating','<?php echo $first; ?>')" >Rating <div class="{{$first}}{{$firstbydefault}}"> </div></th>
		<th>Message</th>
		<th>City</th>
		<th>State</th>
		<th>Group</th>
		<th>Action</th>
	</tr>
	</thead>
	<tbody>
		<?php
		 if(count($feedback) > 0) { ?>
			@foreach ($feedback as $lists)                        
			<tr id="row-{{$lists->_id}}">
				<td scope="row">{{$sno++}}</td>
				<td>
				<?php
					if(isset($lists->EnterOn->sec)){
						echo date('d-M-Y h:i A',$lists->EnterOn->sec);
					}
				?>
				</td>
				<td>{{$lists->UserName}}</td>
				<td>
					<div class='rating_bar'>
						<div  class='rating' style='width:{{$lists->Rating * 20}}%'></div>
					</div>
				</td>
				<td>{{substr($lists->Comment, 0,50)}}</td>				
				<td>{{$lists->UserCity}}</td>
				<td>{{$lists->UserState}}</td>
				<td>{{$lists->UserGroup}}</td>
				<td class="width-action-20">
                
                <div style="position:relative;" class="visible-ipad">
                            <button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Action
                        <span class="caret"></span>
                        </button>
                     <ul class="dropdown-menu action-menu">
                       
                        
                        <li>
                        <button type="button" class="btn btn-small btn-info btn-action" data-toggle="modal" data-target="#myModal{{$lists->_id}}"><i class="fa fa-info"></i>&nbsp;Detail</button>
                        </li>
                     </ul>
                  </div>
                
                
                
                
					<button type="button" class="btn btn-small btn-info btn-action" data-toggle="modal" data-target="#myModal{{$lists->_id}}"><i class="fa fa-info"></i></button>
				</td>
			</tr>
			@endforeach
		<?php } else { ?>
			<tr>
				<td colspan="7" class="text-center" >Feedback not found</td>
			</tr>
		<?php } ?>
	</tbody>
</table>
@extends('Admin::list.pagination.footer')
@foreach ($feedback as $lists)
<!-- Small modal -->

<div class="modal fade" id="myModal{{$lists->_id}}" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Feedback Details</h4>
			</div>
			<div class="modal-body my-modal-body">
      	<div class="table-responsive">
      		<table class="table table-hover table-striped my-table">
            	<tbody><tr>
                	<td>Date:</td>
                    <td><?php
						if(isset($lists->EnterOn->sec)){
							echo date('d-M-Y',$lists->EnterOn->sec);
						}
					?></td>
                </tr>
            	<tr>
                	<td>Message:</td>
                    <td>{{$lists->Comment}}</td>
                </tr>
                <tr>
                	<td>User Name:</td>
                    <td>{{$lists->UserName}}</td>
                </tr> 
                <tr>
                	<td>Rating:</td>
                    <td><div class='rating_bar'><div  class='rating' style='width:{{$lists->Rating * 20}}%'></div></div></td>
                </tr>
            </tbody></table> 
      </div>
      </div>
            
			<div class="modal-footer">
			  <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
			</div>
       </div>
    </div>
</div>
@endforeach
