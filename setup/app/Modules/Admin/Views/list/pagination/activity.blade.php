
<table class="table table-bordered table-striped table-highlight table-list">
<thead>
	<tr>
		<th>S.No.</th>
		<th>Package id</th>
		<th>Request type</th>
    	<th>Message</th>
    	<th>Requester Name</th>
    	<th>Action User Name</th>
    	<th class="text-center">Status</th>
		<th width="14%">Date</th>
	</tr>
	</thead>
	<tbody>
		<?php if (count($activity) > 0) {?>

			@foreach ($activity as $lists)
			<tr id="row-{{$lists->_id}}">
				<td scope="row">{{@$sno++}}</td>
				<td>
				   	@if($lists->request_type == 'buy_for_me')
		            	@if($lists->package_id != '')
							<a href="{{ url('admin/buy-for-me/detail/'.$lists->request_id) }}">{{$lists->package_id}} </a>
						@else
							N/A
						@endif
		          	@elseif($lists->request_type == 'online')
		            	@if($lists->package_id != '')
							<a href="{{ url('admin/online_package/detail/'.$lists->request_id) }}">{{$lists->package_id}} </a>
						@else
							N/A
						@endif
					@else
					     @if($lists->package_id != '')
                            <a href="{{ url('admin/package/detail/'.$lists->request_id) }}">{{ $lists->package_id }} </a>
					     @else
                            N/A
					     @endif
		          	@endif
				</td>
				<td>
          @if($lists->request_type == 'buy_for_me')
            Buy for me
          @elseif($lists->request_type == 'online')
            Online
          @elseif($lists->request_type == 'delivery')
             Send a Package
          @else
             Local Delivery
          @endif
        </td>
        <td>{{ucfirst($lists->message)}}</td>
        <td>
		@if(@$lists->RequesterName)
		{{ucfirst($lists->RequesterName)}}
		@else
            Admin
          @endif
		</td>
		<td>
			{{ucfirst(@$lists->action_user_info['Name'])}}
		</td>
		<td class="text-center">
            @if($lists->status == 'not_purchased')
            	Not Purchased
            @elseif($lists->status == 'reviewed')
				Reviewed
			@elseif($lists->status == 'modified')
			   Modified
			@elseif($lists->status == 'assign')
<!--
			   Assign
-->
				Shipment Departed
			@elseif($lists->status == 'item_received')
			   Item received
			@elseif($lists->status == 'purchased')
			   Purchased
			@elseif($lists->status == 'pending')
			   Pending
			@elseif($lists->status == 'cancel_by_admin')
			   Cancel
			@elseif($lists->status == 'out_for_pickup')
			   Out for pickup
			@elseif($lists->status == 'out_for_delivery')
				Out for delivery
			@elseif($lists->status == 'delivered')
				Delivered
			@elseif($lists->status == 'ready')
			    Ready
			@elseif($lists->status == 'paid')
			    Paid
			@elseif($lists->status == 'accepted')
				Accepted
			@elseif($lists->status == 'shipment_departed')
				Shipment Departed
			@endif
        </td>

		<td class="text-center">
          {{ show_date($lists->EnterOn) }}
         </td>
		</tr>
			@endforeach
		<?php } else {?>
			<tr>
				<td colspan="11" class="text-center" >No Record Found.</td>
			</tr>
		<?php }?>
	</tbody>
</table>
@extends('Admin::list.pagination.footer')
