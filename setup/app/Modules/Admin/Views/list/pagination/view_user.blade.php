<table class="table table-bordered table-striped table-highlight table-list">
<thead>
	<tr>
		<th>S.No.</th>
		<th>User Name</th>
		<th>Email</th>
		<th>Phone</th>
	</tr>
	</thead>
	<tbody>
		<?php if (count($users) > 0) {?>

			@foreach ($users as $lists)
			<tr id="row-{{$lists->_id}}">
				<td scope="row">{{$sno++}}</td>
				<td>{{ucfirst($lists->Name)}}</td>
				<td>{{$lists->Email}}</td>
				<td>{{$lists->PhoneNo}}</td>
			</tr>
			@endforeach
		<?php } else {?>
			<tr>
				<td colspan="5" class="text-center" >User not found</td>
			</tr>
		<?php }?>
	</tbody>
</table>

@extends('Admin::list.pagination.footer')
