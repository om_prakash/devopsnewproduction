
<table class="table table-bordered table-striped table-highlight table-list">
<thead>
	<tr>
		<th width="8%">S.No.</th>
		<th width="25%">Category</th>
		<th width="25%">Shipping Mode</th>
		<th width="15%">Charge Type</th>
		<th width="15%">Status</th>
		<th width="15%">Action</th>
	</tr>
	</thead>
	<tbody>
		<?php if (count($category) > 0) {?>
			@foreach ($category as $lists)
			<tr id="row-{{$lists->_id}}">
				<td scope="row">{{$sno++}}</td>
				<td>{{$lists->Content}}</td>
				<td><?php if ($lists->TravelMode == 'ship') {echo "By Sea";} elseif ($lists->TravelMode == 'air') {echo "By Air";}?></td>
				<td><?php if ($lists->ChargeType == 'distance') {echo "Distance";} elseif ($lists->ChargeType == 'fixed') {echo "Fixed";}?></td>
				<td class="text-center">
				    @if ($lists->Status == 'Active')
		            <span class="badge bg-green">Active</span>
		            @else
		            <span class="badge bg-yellow">Inactive</span>
		            @endif
				</td>
				<td class="text-center">
                	<div style="position:relative;" class="visible-ipad">
                            <button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Action
                        <span class="caret"></span>
                        </button>
                     <ul class="dropdown-menu action-menu">

                       <li>
                	@if ($lists->Status == 'Active')
						<a href="{{ url('status_activity',[$lists->_id,'Inactive','Category']) }}?href=admin/category_list" data-placement="top" title="Make Inactive" class="btn btn-small btn-warning btn-action">
							<i class="fa fa-lock"></i>&nbsp;Inactive</a>

					@else
						<a href="{{ url('status_activity',[$lists->_id,'Active','Category']) }}?href=admin/category_list" data-placement="top" title="Make Active" class="btn btn-small btn-success btn-action">
							<i class="fa fa-unlock"></i>&nbsp;Active</a>
					@endif
                    </li>
                    <li>
					<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal{{$lists->_id}}" data-whatever="@mdo"><i class="fa fa-edit"></i>&nbsp;Edit</button>
						</li>
                        <li>
					<a data-placement="top" title="Delete Record" class="btn btn-small btn-danger btn-action" onclick="remove_record('delete_activity/{{$lists->_id}}/category_list','{{$lists->_id}}')" href="javascript:void(0)"><i class="fa fa-trash"></i>&nbsp;Delete</a>
                    </li>

                </ul>
                </div>
                <a data-placement="top" title="Shipping Charge" class="btn btn-small btn-info btn-action" href="{{url('admin/add-shipping',$lists->_id)}}" data-target="{{$lists->Content}},{{$lists->TravelMode}}"><i class="fa fa-road"></i></a>
					@if ($lists->Status == 'Active')
						<a href="{{ url('status_activity',[$lists->_id,'Inactive','Category']) }}?href=admin/category_list" data-placement="top" title="Make Inactive" class="btn btn-small btn-warning btn-action" onclick ="return confirm('Are you sure? You want to inactivate this category!');">
							<i class="fa fa-lock"></i></a>
					@else
						<a href="{{ url('status_activity',[$lists->_id,'Active','Category']) }}?href=admin/category_list" data-placement="top" title="Make Active" class="btn btn-small btn-success btn-action" onclick ="return confirm('Are you sure? You want to activate this category');">
							<i class="fa fa-unlock"></i></a>
					@endif
					{{--<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal{{$lists->_id}}" data-whatever="@mdo"><i class="fa fa-edit"></i> </button>--}}

					<a data-placement="top" title="Delete Record" class="btn btn-small btn-danger btn-action" onclick="remove_record('delete_activity/{{$lists->_id}}/category_list','{{$lists->_id}}')" href="javascript:void(0)" onclick ="return confirm('Are you sure? You want to delete this category!')"><i class="fa fa-trash"></i></a>
				</td>
			</tr>

			@endforeach
		<?php } else {?>
			<tr>
				<td colspan="5" class="text-center" >Category not found</td>
			</tr>
		<?php }?>
	</tbody>
</table>
@foreach ($category as $lists)

<div class="modal fade" id="exampleModal{{$lists->_id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title" id="exampleModalLabel">Edit Category</h4>
	  </div>

	  {!! Form::model('', ['name' => 'categoryForm1', 'id' => $lists->_id, 'method' => 'POST', 'url' => ['admin/update_category', $lists->_id]]) !!}

	  <div class="modal-body">
		  <div class="col-md-12">
			 <div class="form-group" id="cityDiv1">
				{!! Form::label('Category', 'Category:',['class' => 'control-label']) !!}
				{!! Form::text('Content', $lists->Content, ['class'=>'form-control', 'placeholder'=> 'Category','id' =>'Content'.$lists->_id]) !!}
				<p class="help-block red" id="er_Content{{$lists->_id}}" style="color:##a94442"></p>
			 </div>
		 </div>
			<div class="clearfix"></div>
		<div class="col-md-12">
			 <div class="form-group" id="cityDiv1">
				{!! Form::label('TravelMode', 'Travel Mode:',['class' => 'control-label']) !!}
				{!! Form::select('TravelMode', [''=>'Select mode','air'=>'By Air','ship'=>'By Sea'], $lists->TravelMode, ['class'=>'form-control', 'placeholder'=> 'Travel Mode','id' =>'travelmode1'.$lists->_id]) !!}
				<p class="help-block red" id="er_travelmode1{{$lists->_id}}" style="color:##a94442"></p>
			</div>
		 </div>
			<div class="clearfix"></div>
		<div class="col-md-12">
		     <div class="form-group" id="cityDiv">
				{!! Form::label('ChargeType', 'Charge Type:',['class' => 'control-label']) !!}
				{!! Form::select('ChargeType', [''=>'Select Type','distance'=>'Distance','fixed'=>'Fixed'], $lists->ChargeType, ['class'=>'form-control', 'placeholder'=> 'Charge Type','id' =>'ChargeType1'.$lists->_id]) !!}
				<p class="help-block red" id="er_ChargeType1{{$lists->_id}}" style="color:##a94442"></p>
               </div>
       </div>

		<div class="clearfix"></div>
		</div>

	  <div class="modal-footer">
		{!! Form::button('Close', ['class' => 'btn btn-primary', 'data-dismiss'=>'modal']) !!}
		{!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
	  </div>

		{!! Form::close() !!}
	<div class="clearfix"></div>
	</div>
  </div>
</div>

<script>
$("#{{$lists->_id}}").submit(function()
{
	var flag = true;

	if(valid.required('Content{{$lists->_id}}','category name') == false) { flag = false; }
	if(valid.required('travelmode1{{$lists->_id}}','travel mode') == false) { flag = false; }
	if(valid.required('ChargeType1{{$lists->_id}}','charge type') == false) { flag = false; }

	return flag;
});
</script>
@endforeach

@extends('Admin::list.pagination.footer')
