<script>
$(document).ready(function(){

        $("#stateForm").submit(function(){
		var flag = true;

		 if($("#Content").val() =="")
			{
				 flag = false;
				 $("#cityDiv").addClass( "has-error" );
				 $("#state_msg").show();
			}
			else
			{
				 $("#state_msg").hide();
			}
		return flag;

	});
});

</script>
<table class="table table-bordered table-striped table-highlight table-list">
<thead>
	<tr>
		<th width="8%">S.No.</th>
		<th width="25%">State</th>
		<th width="25%">Country</th>
		<th width="12%">Status</th>
		<th width="12%">Signup Status</th>
		<th width="12%">Action</th>
	</tr>
	</thead>
	<tbody>
		<?php if (count($state) > 0) {?>
			@foreach ($state as $lists)
			<tr id="row-{{$lists->_id}}">
				<td scope="row" width="100px;">{{$sno++}}</td>
				<td>{{ucfirst($lists->Content)}}</td>
				<td>{{ucfirst(str_replace('-', ' ',$lists->SuperName))}}</td>
				<td width="100px;" style="text-align: center;">
					@if($lists->Status == 'Active')
					    <a href="{{ url('status_activity',[$lists->_id,'Inactive','City_State_Country']) }}?href=admin/state_list/{{$lists->countryid}}&pageNo={{ Input::get('page') }}&record={{ Input::get('Pnum') }}" data-placement="top" title="Make Inactive"  onclick ="return confirm('Are you sure? You want to inactivate this state!');">
						<span class="badge bg-green">Active</span></a>
					@else
					    <a href="{{ url('status_activity',[$lists->_id,'Active','City_State_Country']) }}?href=admin/state_list/{{$lists->countryid}}&pageNo={{ Input::get('page') }}&record={{ Input::get('Pnum') }}" data-placement="top" title="Make Active"  onclick ="return confirm('Are you sure? You want to activate this state!');">
						<span class="badge bg-yellow">Inactive</span>
					@endif
				</td>

				<td width="100px;" style="text-align: center;">
					@if($lists->Signup)
						<a href="{{ url('status_activity',[$lists->_id,'false','City_State_Country_Signup']) }}?href=admin/state_list" data-placement="top" title="Make Inactive" onclick ="return confirm('Are you sure? You want to inactivate this state for signup!');">
							<span class="badge bg-green">Active</span>
						</a>
					@else
						<a href="{{ url('status_activity',[$lists->_id,'true','City_State_Country_Signup']) }}?href=admin/state_list" data-placement="top" title="Make Active" onclick ="return confirm('Are you sure? You want to activate this state for signup!');">
							<span class="badge">Inactive</span>
						</a>
					@endif
				</td>

				<td class="text-center" width="190px;">

                <div style="position:relative;" class="visible-ipad">
      <button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      Action
      <span class="caret"></span>
    </button>
    <ul class="dropdown-menu action-menu">
    	<li>
        @if ($lists->Status == 'Active')
			<a href="{{ url('status_activity',[$lists->_id,'Inactive','City_State_Country']) }}?href=admin/state_list" data-placement="top" title="Make Inactive" class="btn btn-small btn-success btn-action">
				<i class="fa fa-lock"></i>&nbsp;Inactive</a>
		@else
			<a href="{{ url('status_activity',[$lists->_id,'Active','City_State_Country']) }}?href=admin/state_list" data-placement="top" title="Make Active" class="btn btn-small btn-warning btn-action">
				<i class="fa fa-unlock"></i>&nbsp;Active</a>
		@endif
   		</li>

               <li>

					<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal{{$lists->_id}}" data-whatever="@mdo"><i class="fa fa-edit"></i>&nbsp;Edit</button>

                    </li>
                    <li>
					<a data-placement="top" title="Delete Record" class="btn btn-small btn-danger btn-action" onclick="remove_record('delete_activity/{{$lists->_id}}/country_list','{{$lists->_id}}')" href="javascript:void(0)"><i class="fa fa-trash"></i>&nbsp;Delete</a>
                    </li>
    </ul>
</div>





					@if ($lists->Status == 'Active')
						<a href="{{ url('status_activity',[$lists->_id,'Inactive','City_State_Country']) }}?href=admin/state_list" data-placement="top" title="Make Inactive" class="btn btn-small btn-success btn-action" onclick ="return confirm('Are you sure? You want to inactivate this state!');">
							<i class="fa fa-lock"></i></a>
					@else
						<a href="{{ url('status_activity',[$lists->_id,'Active','City_State_Country']) }}?href=admin/state_list" data-placement="top" title="Make Active" class="btn btn-small btn-warning btn-action" onclick ="return confirm('Are you sure? You want to activate this state!');">
							<i class="fa fa-unlock"></i></a>
					@endif
					<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal{{$lists->_id}}" data-whatever="@mdo"><i class="fa fa-edit"></i> </button>
					<a title="City List" class="btn btn-small btn-success btn-action" href="{{ url('admin/city_list?id='.$lists->_id) }}"><i class="fa fa-list"></i></a>
					<a data-placement="top" title="Delete Record" class="btn btn-small btn-danger btn-action" onclick="remove_record('delete_activity/{{$lists->_id}}/state_list','{{$lists->_id}}')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>
				</td>
			</tr>

			@endforeach
		<?php } else {?>
			<tr>
				<td colspan="6" class="text-center" >State not found</td>
			</tr>
		<?php }?>
	</tbody>
</table>
@foreach ($state as $lists)
<!----- Pop Up -->

<div class="modal fade" id="exampleModal{{$lists->_id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title" id="exampleModalLabel">Edit State</h4>
	  </div>

	  {!! Form::model('', ['name' => 'stateForm', 'id' => 'stateForm', 'method' => 'POST', 'url' => 'admin/update_state/'.$lists->_id.'?id='.Input::get('country')]) !!}

	  <div class="modal-body clearfix">

			<div class="form-group">
				{!! Form::label('Country', 'Select Country:',['class' => 'control-label']) !!}
				<select name ="country" class="form-control" >
					<?php foreach ($country as $val) {?>
					<option value='{"id":"<?php echo $val->_id; ?>","name":"<?php echo $val->Content; ?>"}' <?php if ($val->Content == $lists->SuperName) {echo 'Selected';}?> > <?php echo $val->Content; ?></option>
					<?php }?>
				</select>

		    </div>
			<div class="form-group" id="cityDiv">
				{!! Form::label('Content', 'State:',['class' => 'control-label']) !!}
				{!! Form::text('Content', $lists->Content, ['class'=>'form-control', 'placeholder'=> 'State','id' =>'Content']) !!}
				<p class="help-block" id="state_msg" style="display: none;">State field is required.</p>
			</div>
			<div class="col-md-12">
						<div class="col-md-6">
						<div class="form-group" id="cityDiv">
						<div class="form-group ">
                           {!! Form::label('Status', 'Status'),':' !!}  </br>
                           <?php if ($lists->Status == 'Active') {?>
                           <input type="radio" name="state_status" value="Active" checked> Active &nbsp;
                           <input type="radio" name="state_status" value="Inactive"> Inactive
                           <?php } else {?>
                            <input type="radio" name="state_status" value="Active"> Active &nbsp;
                           <input type="radio" name="state_status" value="Inactive" checked> Inactive
                           <?php }?>
                        </div>
                        <div class="clearfix"></div>

						</div>
						</div>
						<div class="col-md-6">
						<div class="form-group" id="cityDiv">
						<div class="form-group ">
                           {!! Form::label('Sinup_Status', 'Sinup Status'),':' !!}  </br>
                           <?php if ($lists->Signup == true) {?>
                           <input type="radio" name="state_sinup_status" value="active" checked> Active &nbsp;
                           <input type="radio" name="state_sinup_status" value="inactive"> Inactive
                           <?php } else {?>
                           <input type="radio" name="state_sinup_status" value="active" > Active &nbsp;
                           <input type="radio" name="state_sinup_status" value="inactive" checked>Inactive
                          <?php }?>
                        </div>
                        <div class="clearfix"></div>
                        </div>


				      </div>
						</div>
	  </div>
	  <div class="modal-footer">
		{!! Form::button('Close', ['class' => 'btn btn-primary', 'data-dismiss'=>'modal']) !!}
		{!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
	  </div>
		<div class="clearfix"></div>
		{!! Form::close() !!}

	</div>
  </div>
</div>
<!--- PopUp End	-->
@endforeach

@extends('Admin::list.pagination.footer')
