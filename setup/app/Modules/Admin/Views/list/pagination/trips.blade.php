<table class="table table-bordered table-striped table-highlight table-list">
<thead>
	<tr>
		<th>S.No.</th>
		<th>Transporter Name</th>
		<th>Source Address</th>
		<th>Destination Address</th>
		<th>Weight</th>
		<th>Travel Mode</th>
		<th>Category</th>
		<th>Status</th>
		<th>Create Date</th>
		<th>Action</th>
	</tr>
	</thead>
	<tbody>
		<?php if (count($trips) > 0) {
    ?>

    	 @foreach ($trips as $key)
			<tr id="row-{{$key->id}}">
				<td scope="row">{{$sno++}}</td>
				<td>{{$key->TransporterName}}</td>
				<td>
				<?php
echo get_formatted_address(array($key['SourceAddress'], $key['SourceCountry'], $key['SourceState'], $key['SourceCity']), "");

    ?>
				</td>
				<td>
					<?php
echo get_formatted_address(array($key['DestiAddress'], $key['DestiCountry'], $key['DestiState'], $key['DestiCity']), "");
    ?>
					</td>
			     <td>{{$key->Weight}}{{$key->Unit}}</td>
			    <td><?php if ($key->TravelMode == 'ship') {echo "By Sea";} elseif ($key->TravelMode == 'air') {echo "By Air";}?></td>
				<td><?php
if (is_array($key['SelectCategory'])) {
        echo implode(", ", $key['SelectCategory']);
    } else {echo 'n/a';}
    ?></td>
                <td class="text-center">


		            <!-- @if($key->status == 'active')
		                <span class="badge bg-green">Active</span>
		            @else
		                <span class="badge bg-yellow">Inactive</span>
		            @endif -->

		            <?php
						$current_date = new MongoDate();
						    //$current_date->sec;
					?>

		            @if(@$key->SourceDate->sec < $current_date->sec)
						<span class="badge bg-yellow">Inactive</span>
					@else
						<span class="badge bg-green">Active</span>
					@endif
				</td>
				<td>{{ show_date(@$key->SourceDate) }}</td>
				<td>
					<a href="{{ url('admin/trips_request',$key->_id) }}" data-placement="top" title="Request List" class="btn btn-small btn-primary btn-action">
						<i class ="fa fa-th-list"></i>
					</a>
					<a href="{{ url('admin/edit-individual-trip',$key->_id) }}" data-placement="top" title="Edit" class="btn btn-primary btn-action"><i class="fa fa-edit"></i></a>
					</a>
					<a data-placement="top" title="Delete Record" class="btn btn-small btn-danger btn-action" onclick="remove_record('delete_activity/{{$key->_id}}/individual-trip','{{$key->_id}}')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>

					<!-- @if ($key->status == 'active')
		                <a href="{{ url('status_activity',[$key->id,'inactive','trips']) }}?href=admin/indiviual-trip&pageNo={{ Input::get('page') }}&record={{ Input::get('Pnum') }}" data-placement="top" title="Make Inactive" class="btn-flat btn btn-small btn-success btn-action" onclick = "return confirm('Are you sure? You want to Inactive this User')">
		                    <i class="fa fa-unlock"></i>
		                </a>
		            @else
		               <a href="{{ url('status_activity',[$key->id,'active','trips']) }}?href=admin/indiviual-trip&pageNo={{ Input::get('page') }}&record={{ Input::get('Pnum') }}" data-placement="top" title="Make Active" class="btn-flat btn btn-small btn-warning btn-action" onclick = "return confirm('Are you sure? You want to Active this User')">
		                    <i class="fa fa-lock"></i>
		                </a>
		            @endif -->
				</td>
			</tr>
		@endforeach
		<?php } else {?>
			<tr>
				<td colspan="4" class="text-center" >App Content not found</td>
			</tr>
		<?php }?>
	</tbody>
</table>
<?php
function get_formatted_address($array, $zipcode)
{
    $address = '';
    foreach ($array as $key) {
        if (!empty($key)) {
            $address .= (($address != '') ? ", $key" : $key);
        }
    }
    if (!empty($zipcode)) {$address .= " - $zipcode";}
    return $address;
}
?>
@extends('Admin::list.pagination.footer')
