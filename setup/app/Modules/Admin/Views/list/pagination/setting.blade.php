<table class="table table-bordered table-striped table-highlight table-list">
<thead>
	<tr>
		<th>S.No.</th>
		<th>Notification Title</th>
		<th>Notification Message</th>
		<th>Date</th>
		 
	</tr>
	</thead>
	<tbody>	<?php
		 if(count($users) > 0) { ?>
		
			@foreach ($users as $lists)                        
			<tr id="row-{{$lists->_id}}">
				<td scope="row">{{$sno++}}</td>
				<td>{{$lists->NotificationTitle}}</td>
				<td>{{$lists->NotificationMessage}}</td> 
				<td>{{date('d-m-Y',$lists->Date->sec)}}</td> 
			</tr>
			@endforeach
		<?php } else { ?>
			<tr>
				<td colspan="7" class="text-center" >Notification not found</td>
			</tr>
		<?php } ?>
	</tbody>
</table>
@extends('Admin::list.pagination.footer')
 
