
<?php if(count($support) > 0){?>


  @foreach($support as $lists) 
      <div class="supportbody col-md-12">
         <div class="media">
            <div class="media-body">
               <h4 class="media-heading"><b>{{ $lists->UserName }}</b></h4>
               <small>{{ date('m d, Y',$lists['EnterOn']->sec) }} at {{ date('h:i A',$lists['EnterOn']->sec)}} </small>
               <div>{{ $lists->Query }}</div>
            </div>
         </div>
      </div>
   @endforeach
    

<?php }?>
<div class="clearfix"> </div>

 
