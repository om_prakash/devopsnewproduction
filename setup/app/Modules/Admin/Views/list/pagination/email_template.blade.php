<table class="table table-bordered table-striped table-highlight table-list">
<thead>
	<tr>
		<th>S.No.</th>
		<th>Title</th>
		<th>Subject</th>
		<th>Action</th>
	</tr>
	</thead>
	<tbody>
		<?php if(count($email) > 0) { ?>
		
    	 @foreach ($email as $key)
			<tr id="row-{{$key->UserId}}">
				<td scope="row">{{$sno++}}</td>
				<td>{{$key->TTitle}}</td>
				<td>{{$key->TSubject}}</td>
				<td class="width-action-20">
					<a href="{{ url('admin/edit_email_template', $key->_id) }}" title="Edit Record" class="btn btn-small btn-primary btn-action" >
						<i class="fa fa-pencil-square-o"></i>
					</a>
					
				</td>
			</tr>
		@endforeach
		<?php }else{ ?>
			<tr>
				<td colspan="4" class="text-center" >Email template not found</td>
			</tr>
		<?php } ?>
	</tbody>
</table>
@extends('Admin::list.pagination.footer')
