
@if(Input::get('notification') == 'notification')
<table class="table table-bordered table-striped table-highlight table-list">
<thead>
	<tr>
		<th>S.No.</th>
		<th>Title</th>
		<th>Message</th>
    <th>Device Type</th>
    <th>status</th>
		<th width="14%">Action</th>
	</tr>
	</thead>
	<tbody>
		<?php if (count($cronMail) > 0) {?>

			@foreach ($cronMail as $lists)
			<tr id="row-{{$lists->_id}}">
				<td scope="row">{{$sno++}}</td>
				<td>{{ucfirst($lists->title)}}</td>
				<td>{{ucfirst($lists->message)}}</td>
        <td>{{ucfirst($lists->DeviceType)}}</td>
				<td class="text-center">
            @if ($lists->RequesterStatus == 'pending')
              <span class="badge bg-green">Pending</span>
            @else
              <span class="badge bg-yellow">Done</span>
            @endif
        </td>
				<td class="text-center">
          <div style="position:relative;" class="visible-ipad">
            <button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Action <span class="caret"></span>
            </button>
          </div>
  
					<a data-placement="top" title="Delete Record" class="btn btn-small btn-danger btn-action" onclick="remove_record('delete_activity/{{$lists->_id}}/cronmail','{{$lists->_id}}')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>

				</td>
			</tr>
			@endforeach
		<?php } else {?>
			<tr>
				<td colspan="11" class="text-center" >Recored not found</td>
			</tr>
		<?php }?>
	</tbody>
</table>
@else

<table class="table table-bordered table-striped table-highlight table-list">
<thead>
  <tr>
    <th>S.No.</th>
    <th>User Name</th>
    <th>Email</th>
    <th>Package Title</th>
    <th>Item Price</th>
    <th>Shipping cost</th>
    <th>Package Number</th>
    <th>Total Cost</th>
    <th>status</th>
    <th>Action</th>
  </tr>
  </thead>
  <tbody>
    <?php if (count($cronMail2) > 0) {?>

      @foreach ($cronMail2 as $lists)
      <tr id="row-{{$lists->_id}}">
        <td scope="row">{{$sno++}}</td>
        <td>{{ucfirst($lists->USERNAME)}}</td>
        <td>{{$lists->email}}</td>
        <td>{{ucfirst($lists->PACKAGETITLE)}}</td>
        <td>${{number_format($lists->ITEM_PRICE,2)}}</td>
        <td>${{number_format($lists->SHIPPINGCOST,2)}}</td>
        <td>{{$lists->PACKAGENUMBER}}</td>
        <td>${{number_format($lists->TOTALCOST,2)}}</td>
        <td class="text-center">
            @if ($lists->RequesterStatus == 'pending')
              <span class="badge bg-green">Pending</span>
            @else
              <span class="badge bg-yellow">Done</span>
            @endif
        </td>
        <td class="text-center">
          <div style="position:relative;" class="visible-ipad">
            <button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Action <span class="caret"></span>
            </button>
          </div>
  
          <a data-placement="top" title="Delete Record" class="btn btn-small btn-danger btn-action" onclick="remove_record('delete_activity/{{$lists->_id}}/cronmail','{{$lists->_id}}')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>
        
        </td>
      </tr>
      @endforeach
    <?php } else {?>
      <tr>
        <td colspan="11" class="text-center" >Recored not found</td>
      </tr>
    <?php }?>
  </tbody>
</table>
@endif

@extends('Admin::list.pagination.footer')



