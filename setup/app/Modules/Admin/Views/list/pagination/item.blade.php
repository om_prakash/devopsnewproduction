
<table class="table table-bordered table-striped table-highlight table-list">
<thead>
	<tr>
		<th width="8%">S.No.</th>
		<th width="50%">Item Name</th>
		<th width="10%">Weight</th>
		<th width="10%">Price</th>
		<th width="50px" style="text-align:center">Status</th>
		<th width="100px" style="text-align:center">Action</th>
	</tr>
	</thead>
	<tbody>
		<?php if (count($item) > 0) {?>
			@foreach ($item as $lists)
			<tr id="row-{{$lists->_id}}">
				<td scope="row">{{$sno++}}</td>
				<!--<td>
				  @if($lists->image != '')
					<img src="{{ImageUrl}}/{{$lists->image}}" width="100px" height="100px;">
				  @else
				     <img src="{{ImageUrl}}no-image.jpg" width="100px" height="100px;">
				  @endif
				</td>-->
				<td>{{ucfirst($lists->item_name)}}</td>
				<td>@if($lists->kgWeight != '')
						{{number_format($lists->kgWeight,2).' kg'}} <br>
					@endif
					@if($lists->lbsWeight != '')
						{{number_format($lists->lbsWeight,2). ' lb'}}
					@endif
					@if($lists->kgWeight == '' && $lists->lbsWeight == '')
						N/A
					@endif
				</td>
				<!--<td>{{$lists->weightunit}}</td>-->
				<td>{{$lists->price}}</td>
				<td class="text-center">
				    @if ($lists->Status == 'Active')
		            <span class="badge bg-green">Active</span>
		            @else
		            <span class="badge bg-yellow">Inactive</span>
		            @endif
				</td>
				<td class="text-center">
                	<div style="position:relative;" class="visible-ipad">
                            <button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Action
                        <span class="caret"></span>
                        </button>
                     <ul class="dropdown-menu action-menu">

                       <li>
                	@if ($lists->Status == 'Active')
						<a href="{{ url('status_activity',[$lists->_id,'Inactive','Category']) }}?href=admin/category_list" data-placement="top" title="Make Inactive" class="btn btn-small btn-warning btn-action">
							<i class="fa fa-lock"></i>&nbsp;Inactive</a>

					@else
						<a href="{{ url('status_activity',[$lists->_id,'Active','Category']) }}?href=admin/category_list" data-placement="top" title="Make Active" class="btn btn-small btn-success btn-action">
							<i class="fa fa-unlock"></i>&nbsp;Active</a>
					@endif
                    </li>
                    <li>
					<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal{{$lists->_id}}" data-whatever="@mdo"><i class="fa fa-edit"></i>&nbsp;Edit</button>
						</li>
                        <li>
					<a data-placement="top" title="Delete Record" class="btn btn-small btn-danger btn-action" onclick="remove_record('delete_activity/{{$lists->_id}}/category_list','{{$lists->_id}}')" href="javascript:void(0)"><i class="fa fa-trash"></i>&nbsp;Delete</a>
                    </li>
                </ul>
                </div>

					@if ($lists->Status == 'Active')
						<a href="{{ url('status_activity',[$lists->_id,'Inactive','item']) }}?href=admin/item" data-placement="top" title="Make Inactive" class="btn btn-small btn-warning btn-action" onclick ="return confirm('Are you sure? You want to inactivate this item!');">
							<i class="fa fa-lock"></i></a>
					@else
						<a href="{{ url('status_activity',[$lists->_id,'Active','item']) }}?href=admin/item" data-placement="top" title="Make Active" class="btn btn-small btn-success btn-action" onclick ="return confirm('Are you sure? You want to activate this item');">
							<i class="fa fa-unlock"></i></a>
					@endif

					<a data-placement="top" title="Delete Record" class="btn btn-small btn-danger btn-action" onclick="remove_record('delete_activity/{{$lists->_id}}/item','{{$lists->_id}}')" href="javascript:void(0)" onclick ="return confirm('Are you sure? You want to delete this category!')"><i class="fa fa-trash"></i></a>

						<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal{{$lists->_id}}" data-whatever="@mdo"><i class="fa fa-edit"></i></button>
				</td>
			</tr>

			@endforeach
		<?php } else {?>
			<tr>
				<td colspan="8" class="text-center" >Item not found</td>
			</tr>
		<?php }?>
	</tbody>
</table>
@foreach ($item as $lists)
<div class="modal fade" id="exampleModal{{$lists->_id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title" id="exampleModalLabel">Edit Item</h4>
	  </div>

	  {!! Form::model('', ['name' => 'categoryForm1', 'id' => $lists->_id, 'method' => 'POST', 'url' => ['admin/update-add-item', $lists->_id], 'files' => true]) !!}

	  <div class="modal-body">

		  <div class="col-md-12">
			 <div class="form-group" id="cityDiv1">
				{!! Form::label('Item Name', 'Item Name:',['class' => 'control-label']) !!}
				{!! Form::text('item',$lists->item_name, ['class'=>'form-control', 'placeholder'=> 'Item name','id' =>'item_name'.$lists->_id]) !!}
				<p class="help-block red" id="er_item_name{{$lists->_id}}" style="color:##a94442"></p>
			 </div>
		 </div>
			<div class="clearfix"></div>
		<div class="col-md-6">
			 <div class="form-group" id="cityDiv1">
				{!! Form::label('weightunit', 'weight unit:',['class' => 'control-label']) !!}
				<select class="form-control" name="weightunit">
					 <option value="kg" @if($lists->weightunit == 'kg') selected @endif>Kg</option>
					 <option value="lbs" @if($lists->weightunit == 'lbs') selected @endif>Lbs</option>
			    </select>
				<p class="help-block red" id="er_weightunit{{$lists->_id}}" style="color:##a94442"></p>
			</div>
		 </div>

			 <div class="col-md-6">
			 <div class="form-group" id="cityDiv1">
				{!! Form::label('weight', 'Weight:',['class' => 'control-label']) !!}
				@if($lists->weightunit == 'kg')
					<?php $weight = $lists->kgWeight;?>
				@elseif($lists->weightunit == 'lbs')
					<?php $weight = $lists->lbsWeight;?>
				@endif
				{!! Form::text('weight', $weight, ['class'=>'form-control', 'placeholder'=> 'Weight','id' =>'weight'.$lists->_id]) !!}
				<p class="help-block red" id="er_weight{{$lists->_id}}" style="color:##a94442"></p>
			 </div>
		 </div>
			<div class="clearfix"></div>

		<div class="col-md-6">
			<div class="form-group" id="cityDiv">
				{!! Form::label('Price', 'Price:',['class' => 'control-label']) !!}
				{!! Form::text('price', $lists->price, ['class'=>'form-control', 'placeholder'=> 'Price','id' =>'price'.$lists->_id]) !!}
				<p class="help-block red" id="er_price{{$lists->_id}}" style="color:##a94442"></p>
			</div>
		</div>

        <!--<div class="col-md-6">
			<div class="form-group" id="cityDiv">
				{!! Form::label('Image', 'Image:',['class' => 'control-label']) !!}
				<input type="file" name="image" id="image" >
				@if($lists->image != '')
					<br/><img src="{{ImageUrl}}/{{$lists->image}}" width="100px" height="100px;">
				@else
					<br/><img src="{{ImageUrl}}no-image.jpg" width="100px" height="100px;">
				@endif
			</div>
        </div>-->

		<div class="clearfix"></div>
		</div>

	  <div class="modal-footer">
		{!! Form::button('Close', ['class' => 'btn btn-primary', 'data-dismiss'=>'modal']) !!}
		{!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
	  </div>

		{!! Form::close() !!}
	<div class="clearfix"></div>
	</div>
  </div>
</div>

<script>
$("#{{$lists->_id}}").submit(function()
{
	var flag = true;

	if(valid.required('item_name{{$lists->_id}}','item name') == false) { flag = false; }
	if(valid.required('weightunit{{$lists->_id}}','weight unit') == false) { flag = false; }
	if(valid.required('unit{{$lists->_id}}','unit') == false) { flag = false; }
	if(valid.required('price{{$lists->_id}}','price') == false) { flag = false; }
	if(valid.required('weight{{$lists->_id}}','weight') == false) { flag = false; }

	return flag;
});
</script>
@endforeach

@extends('Admin::list.pagination.footer')
