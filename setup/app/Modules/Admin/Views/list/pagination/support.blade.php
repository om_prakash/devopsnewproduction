<table class="table table-bordered table-striped table-highlight table-list">
<thead>
	<tr>
		<th>S.No.</th>
		<th>Date</th>
		<th>Token</th>
		<th>User Name</th>
		<th>Email</th>
		<th>Query</th>
		<th>Action</th>
	</tr>
	</thead>
	<tbody>
		<?php
		 if(count($support) > 0) { ?>
			@foreach ($support as $lists)                        
			<tr id="row-{{$lists['Token']}}">
				<td scope="row">{{$sno++}}</td>
				<td>{{ show_date(@$lists['EnterOn']) }}</td>
				<td>{{$lists['Token']}}</td>
				<td>{{$lists['UserName']}}</td>
				<td>{{$lists['Email']}}</td>
				<td>{{$lists['Query']}}</td>
				<td>
					<button type="button" title="Reply Mail" class="btn btn-small btn-info btn-action" data-toggle="modal" data-target="#exampleModal{{$lists['Token']}}"><i class="fa fa-mail-reply"></i></button>
				</td>
			</tr>
			@endforeach
		<?php } else { ?>
			<tr>
				<td colspan="6" class="text-center" >No Record Found Yet.</td>
			</tr>
		<?php } ?>
	</tbody>
</table>


@foreach ($support as $lists)
<!-- Small modal -->

<!----- Pop Up - --> 
<div class="modal fade" id="exampleModal{{$lists['Token']}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel">Send Mail</h4>
      </div>
      
    {!! Form::model('',[  
                          'name'   => 'countryForm1', 
                          'id'     => 'countryForm1', 
                          'onsubmit' => 'return validateForm("'.$lists["Token"].'")',  
                          'method'   => 'POST', 
                          'url'      => ['admin/update-support', $lists['Token']]]) 
                      !!}
      
      <div class="modal-body">
            <div class="form-group" id="cityDiv1">
                {!! Form::label('Email', 'To:',['class' => 'control-label']) !!}
                {!! Form::text('Email', $lists['Email'], ['class'=>'form-control', 'placeholder'=> 'Email',
                'id' =>'Email'.$lists['Token'] ]) !!}
                <p class="help-block red" id="er_Email1{{$lists['Token']}}" > </p>
            </div>
            
            <div class="form-group" id="cityDiv1">
                {!! Form::label('Query', 'Query: ',['class' => 'control-label']) !!} {{ucfirst($lists['Query'])}}
            </div>
                        
            <div class="form-group" id="cityDiv1">
                {!! Form::label('ReplyQuery', 'Reply Query:',['class' => 'control-label']) !!}
                {!! Form::textarea('ReplyQuery', '', ['class'=>'form-control', 'placeholder'=> 'Reply Query','id' =>'ReplyQuery1'.$lists['Token'],'size' => '30x3']) !!}
                <p class="help-block red" id="er_ReplyQuery1{{$lists['Token']}}" > </p>
            </div>
      </div>      
      <div class="modal-footer">
        <div class="row">
            <div class="col-md-8 text-right">
                {!! Form::submit('Send', ['class' => 'btn btn-primary  pull-left']) !!}             
                {!! Form::button('Close', ['class' => 'btn btn-primary  pull-left', 'data-dismiss'=>'modal']) !!}   
            </div>
            <div class="col-md-4">
                <a href="{{ url('admin/support_view_more',$lists['Token'])}}"><div  class="btn btn-primary">View Full Conversation</div></a>
            </div>
        </div>
        
      </div>
        <div class="clearfix"></div>
        {!! Form::close() !!}
    
    </div>
  </div>
</div>
<!--- PopUp End --> 
@endforeach


<script>
function validateForm(id) {
    
    var flag = true;
    try
    {
        document.getElementById('er_Email1'+id).innerHTML = '';
        document.getElementById('er_ReplyQuery1'+id).innerHTML = '';

        if(document.getElementById('Email'+id).value == '') {
            document.getElementById('er_Email1'+id).innerHTML = 'The email field is required';
            flag = false;
        }else if (document.getElementById('ReplyQuery1'+id).value == '') {
             document.getElementById('er_ReplyQuery1'+id).innerHTML = 'The reply query field is required';
             flag = false;
  }
    }catch(E) {
        console.log(E);
    }
   return flag;
}
</script>


@extends('Admin::list.pagination.footer')