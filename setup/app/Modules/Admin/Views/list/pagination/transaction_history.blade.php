<table class="table table-bordered table-striped table-highlight table-list">
<thead>
	<tr>
		<th width="7%">S.No.</th>
		<th width="12%">Date</th>
		<th width="12%">Deposit By</th>
		<th width="12%">Deposit To</th>
		<th width="30%">Description</th>
		<th width="10%">Credit</th>
		<th width="10%">Debit</th>
		<th width="10%">Status</th>
		<th width="5%">Action</th>
	</tr>
	</thead>
	<tbody>
	<?php if (count($info) > 0) {
    ?>

			@foreach ($info as $lists)
			<tr id="row-{{$lists->_id}}">

				<td scope="row">{{$sno++}}</td>
				<td>{{ show_date(@$lists->EnterOn) }}</td>
				<td>{{$lists->SendByName}}</td>
				<td>{{$lists->RecieveByName}}</td>
				<td>{{$lists->Description}}</td>
				<td>
				<?php if ($lists->Credit > 0) {
			        	if($lists->momo_id){
							echo "GHS ";
						}else{
						  	echo "$";
						}
			        	if(isset($lists->CurrencyRate)){
			        		echo number_format($lists->Credit / $lists->CurrencyRate, 2);
			        	}else{
			        		echo number_format($lists->Credit , 2);
			        	}
			        	
			        }
			    ?>
        	
        		</td>
				<td><?php if ($lists->Debit > 0) {
        echo "$";
        echo number_format($lists->Debit, 2);}?></td>
				<td class="text-center">

					<?php
switch (strtolower($lists->Status)) {
        case 'pending':
            echo '<span class="badge bg-red">Pending</span>';
            break;
        case 'paid':
            echo '<span class="badge bg-green">Completed</span>';
            break;
        case 'completed':
            echo '<span class="badge bg-green">Completed</span>';
            break;
    }
    ?>

				</td>
				<td>
					@if (@$lists->request_detail)
						<button class="btn btn-info" title="Information" data-toggle="modal" data-target="#transDetail" onclick="paymentDetail('{{ @$lists->request_detail }}', )">
							<i class="fa fa-info-circle" aria-hidden="true"></i>
						</button>
					@else
						N.A
					@endif
				</td>
			</tr>
			@endforeach
		<?php } else {?>
			<tr>
				<td colspan="8" class="text-center" >No transaction found yet</td>
			</tr>
		<?php }?>
	</tbody>
</table>

<!-- Modal -->
<div id="transDetail" class="modal fade" role="dialog">
	<div class="modal-dialog biling_modal">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"> Payment Information </h4>
			</div>
			<div class="modal-body payment_bill">
				<div class="billing_list">
					<ul>
						<li>
							<label> Package ID: </label> <span id="packageId"></span>
						</li>
						<li>
							<label> Shipping cost: </label> <span id="shippingCost"></span>
						</li>
						<li>
							<label> Shipping cost by user: </label> <span id="shippingCostByUser"></span>
						</li>
						<li>
							<label> Item cost: </label> <span id="itemCost"></span>
						</li>
						<li>
							<label> Insurance: </label> <span id="insuranceCost"></span>
						</li>
						<li>
							<label> Duty/Customs Clearing: </label> <span id="dutyAndCustoms"></span>
						</li>
						<li>
							<label> Tax: </label> <span id="tax"></span>
						</li>
						<li>
							<label> Processing fees: </label> <span id="processingFees"></span>
						</li>
						<li>
							<label> Region cost: </label> <span id="regionCost"></span>
						</li>
						<li>
							<label> After item review: </label> <span id="afterUpdateDiff"></span>
						</li>
					</ul>					
				</div>
				<div class="billing_list">
					<ul>
						<li>
							<label> Total price: </label> <span id="totalCost"></span>
						</li>
					</ul>					
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>

	</div>
</div>

<script type="text/javascript">
	function paymentDetail(argument) {
		var reqData = JSON.parse(argument);
		console.log(reqData);
		var reqType = reqData.RequestType;
		var shippingCost = 0.0;
		var insurance = 0.0;
		var regionCost = 0.0;
		
		if (reqType == "delivery") {
			shippingCost = parseFloat(reqData.ShippingCost);
			insurance = parseFloat(reqData.InsuranceCost);
			regionCost = parseFloat(reqData.region_charge);
		} else {
			shippingCost = parseFloat(reqData.shippingCost);
			insurance = parseFloat(reqData.insurance);
			regionCost = parseFloat(reqData.AreaCharges);
		}

		$("#packageId").html(reqData.PackageNumber);
		$("#shippingCost").html("$" + shippingCost);
		$("#regionCost").html("$" + regionCost);
		$("#insuranceCost").html("$" + insurance);
		var shippingCostByUser = parseFloat(reqData.shipping_cost_by_user);
		$("#shippingCostByUser").html("$" + (shippingCostByUser ? shippingCostByUser : 0.0));
		var totalItemPrice = parseFloat(reqData.total_item_price);
		$("#itemCost").html("$" + (totalItemPrice ? totalItemPrice : 0.0));
		var customsOrDuty = parseFloat(reqData.DutyAndCustom);
		$("#dutyAndCustoms").html("$" + (customsOrDuty ? customsOrDuty : 0.0));
		var tax = parseFloat(reqData.Tax);
		$("#tax").html("$" + (tax ? tax : 0.0));
		var processingFees = parseFloat(reqData.ProcessingFees);
		$("#processingFees").html("$" + (processingFees ? processingFees : 0.0));
		var afterUpdateDiff = parseFloat(reqData.after_update_difference);
		$("#afterUpdateDiff").html("$" + (afterUpdateDiff ? afterUpdateDiff : 0.0));
		var totalCost = parseFloat(reqData.TotalCost);
		$("#totalCost").html("$" + totalCost);
	}
</script>

@extends('Admin::list.pagination.footer')
