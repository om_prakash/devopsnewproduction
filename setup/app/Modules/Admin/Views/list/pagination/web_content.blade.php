<table class="table table-bordered table-striped table-highlight table-list">
<thead>
	<tr>
		<th>S.No.</th>
		<th>Title</th>
		<th>Content</th>
		<th>Action</th>
	</tr>
	</thead>
	<tbody>
		<?php if (count($webContent) > 0) {?>

    	 @foreach ($webContent as $key)
			<tr id="row-{{$key->id}}">
				<td scope="row">{{$sno++}}</td>
				<td>{{$key->Title}}</td>
				<td><?php echo substr(strip_tags($key->Content), 0, 255) ?></td>
				<td class="width-action-20 text-center">
					<a href="{{ url('admin/edit_web_content',$key->_id) }}" data-placement="top" title="Edit Record" class="btn btn-small btn-primary btn-action" >
						<i class="fa fa-pencil-square-o" data-toggle="modal" data-target="#editModal"></i>
					</a>
				</td>
			</tr>
		@endforeach
		<?php } else {?>
			<tr>
				<td colspan="4" class="text-center" >Web content not found</td>
			</tr>
		<?php }?>
	</tbody>
</table>
@extends('Admin::list.pagination.footer')
