<table class="table table-bordered table-striped table-highlight table-list">
<thead>
	<tr>
		<th>S.No.</th>
		<th>Title</th>
		<th>Action</th>
	</tr>
	</thead>
	<tbody>
		<?php if (count($appcontent) > 0) {?>

    	 @foreach ($appcontent as $key)
			<tr id="row-{{$key->id}}">
				<td scope="row">{{$sno++}}</td>
				<td>{{$key->Title}}</td>
				<td class="width-action-20 text-center">
                <div style="position:relative;" class="visible-ipad">
                    <button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Action
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu action-menu">
                        <li>
                        <a href="{{ url('admin/edit_app_content',$key->_id) }}" data-placement="top" title="Edit Record" class="btn btn-small btn-primary btn-action" >
                            <i class="fa fa-pencil-square-o"></i>
                        &nbsp;Edit</a>
                        </li>
                    </ul>
               </div>


					<a href="{{ url('admin/edit_app_content',$key->_id) }}" data-placement="top" title="Edit Record" class="btn btn-small btn-primary btn-action" >
						<i class="fa fa-pencil-square-o"></i>
					</a>
				</td>
			</tr>
		@endforeach
		<?php } else {?>
			<tr>
				<td colspan="4" class="text-center" >App Content not found</td>
			</tr>
		<?php }?>
	</tbody>
</table>
@extends('Admin::list.pagination.footer')
