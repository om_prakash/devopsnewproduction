<table class="table table-bordered table-striped table-highlight table-list">
<thead>
	<tr>
		<th>S.No.</th>
		<th>Date</th>
		<th>Email</th>
		<th>Query</th>
		<th>User Name</th>	
		<th>Action</th>
	</tr>
	</thead>
	<tbody>
		<?php
		 if(count($support) > 0) { ?>
			@foreach ($support as $lists)                        
			<tr id="row-{{$lists->_id}}">
				<td scope="row">{{$sno++}}</td>
				<td>
				<?php
					if(isset($lists->EnterOn->sec)){
						echo date('d-M-Y',$lists->EnterOn->sec);
					}
				?>
				</td>
				<td>{{ $lists->Email }}</td>
				<td>{{substr($lists->Query, 0,50)}}</td>
				<td>{{$lists->UserName}}</td>
				
				<td class="width-action-20">
                
                <div style="position:relative;" class="visible-ipad">
                            <button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Action
                        <span class="caret"></span>
                        </button>
                     <ul class="dropdown-menu action-menu">
                        <li>
                        <button type="button" class="btn btn-small btn-info btn-action" data-toggle="modal" data-target="#exampleModal{{$lists->_id}}"><i class="fa fa-mail-reply"></i>&nbsp;Detail</button>
                        </li>
                     </ul>
                  </div>
					<button type="button" class="btn btn-small btn-info btn-action" data-toggle="modal" data-target="#exampleModal{{$lists->_id}}"><i class="fa fa-mail-reply"></i></button>
				</td>
			</tr>
			@endforeach
		<?php } else { ?>
			<tr>
				<td colspan="6" class="text-center" >No Record Found Yet.</td>
			</tr>
		<?php } ?>
	</tbody>
</table>
@extends('Admin::list.pagination.footer')
@foreach ($support as $lists)
<!-- Small modal -->
<?php //echo "<pre>"; print_r($lists['ReplyQuery'][0]['Message']);?>
<!----- Pop Up - --> 
<div class="modal fade" id="exampleModal{{$lists->_id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title" id="exampleModalLabel">Edit Support</h4>
	  </div>
	  
	  {!! Form::model('', ['name' => 'countryForm1', 'id' => 'countryForm1', 'method' => 'POST', 'url' => ['admin/update_support', $lists->_id]]) !!}
	  
	  <div class="modal-body">
			<div class="form-group" id="cityDiv1">
				{!! Form::label('Email', 'Email:',['class' => 'control-label']) !!}
				{!! Form::text('Email', $lists->Email, ['class'=>'form-control', 'placeholder'=> 'Email','id' =>'Email1']) !!}
				<p class="help-block" id="state_msg1" style="display: none;">Country field is required.</p>	
			</div>
			<div class="form-group" id="cityDiv1">
				{!! Form::label('UserQuery', 'User Query:',['class' => 'control-label']) !!}
				{{$lists->Query}}
			</div>
			<div class="form-group" id="cityDiv1">
				{!! Form::label('ReplyQuery', 'Reply Query:',['class' => 'control-label']) !!}
				<?php 
				//foreach ($lists['ReplyQuery'] as $key) {
					//echo $key['Message']."<br/>";
				// } 
				?>
				
			</div>
			<div class="form-group" id="cityDiv1">
				{!! Form::label('ReplyQuery', 'Reply Query:',['class' => 'control-label']) !!}
				{!! Form::textarea('ReplyQuery', '', ['class'=>'form-control', 'placeholder'=> 'Query','id' =>'ReplyQuery1','size' => '30x3']) !!}
				<p class="help-block" id="state_msg1" style="display: none;">Query field is required.</p>	
			</div>
			
	  </div>
	  <div class="modal-footer">
		{!! Form::button('Close', ['class' => 'btn btn-primary', 'data-dismiss'=>'modal']) !!}	
		{!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
	  </div>
		<div class="clearfix"></div>
		{!! Form::close() !!}
	
	</div>
  </div>
</div>
<!--- PopUp End	-->	
@endforeach
