<div class="col-md-12">
<table class="table table-bordered table-striped table-highlight table-list">
<thead>
	<tr>
		<th width="1%" >S.No.</th>
		<!-- <th>Request type</th> -->
		<th width="3%">Action</th>
    	<th>Item Unic No.</th>
    	<th>Shipping</th>
    	<th>Insurance</th>
    	<th>Region</th>

    	<th>Shipping <small>(Retailer to Aquantuo's facility)</small></th>
    	<th>Item Cost</th>

    	<th>Difference</th>
    	<th>Difference Before</th>
    	<th>Processing</th>
    	<th>Discount</th>
    	<th>Total</th>
		<th>Date</th>
	</tr>
	</thead>
	<tbody>
		<?php if (count($info) > 0) {?>
		
			@foreach ($info as $lists)
			<tr id="row-{{$lists->_id}}">
				<td scope="row">{{$sno++}}</td>
				
				<td>@if($lists->action == 'create')
			            Request Create
			        @elseif($lists->action == 'update_request')
			            Request Update
			        @elseif($lists->action == 'item_update')
			            Item Updated
					@else
						{{ $lists->action }}
					@endif
			    </td>
			    <td class="text-center">
			    
			    	@if($lists->item_unic_number != '')
			    		{{$lists->item_unic_number}}
			    	@else
			    	 -
			    	@endif
			    </td>
				
				<td>${{number_format($lists->shippingCost,2)}}</td>
				<td>${{number_format($lists->insurance,2)}}</td>
				<td>${{number_format($lists->AreaCharges,2)}}</td>

				<td>${{number_format($lists->shipping_cost_by_user,2)}}</td>
				<td>${{number_format($lists->item_cost,2)}}</td>

				<td>${{number_format($lists->difference,2)}}</td>
				<td>${{number_format($lists->difference_before,2)}}</td>
				<td>${{number_format($lists->ProcessingFees,2)}}</td>
				<td>${{number_format($lists->discount,2)}}&nbsp;<small><b>{{$lists->PromoCode}}</b></small></td>
				
				<td>${{number_format($lists->TotalCost + $lists->difference,2)}}</td>
				<td>{{ show_date($lists->date) }}</td>
		</tr>
			@endforeach
		<?php } else {?>
			<tr>
				<td colspan="11" class="text-center" >No Record Found.</td>
			</tr>
		<?php }?>
	</tbody>
</table>
</div>
@extends('Admin::list.pagination.footer')