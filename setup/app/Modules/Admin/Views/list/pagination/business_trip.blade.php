<table class="table table-bordered table-striped table-highlight table-list">
<thead>
	<tr>
		<th>S.No.</th>
		<th>Transporter Name</th>
		<th>Countries</th>
		<th>Travel Mode</th>
		<th>Categories</th>
		<th>Weight</th>
		<th>Description</th>
		<th>Create Date</th>
		<th>Update Date</th>
		<th>Status</th>
		<th>Action</th>
	</tr>
	</thead>
	<tbody>
		<?php if (count($trips) > 0) {
    ?>

    	 @foreach ($trips as $key)
			<tr id="row-{{$key->id}}">
				<td scope="row">{{$sno++}}</td>
				<td>{{$key->TransporterName}}</td>

				<td>
				<?php
if (is_array($key['Countries'])) {
        echo implode(", ", $key['Countries']);
    } else {
        echo 'n/a';
    }
    ?></td>
				<td><?php if ($key->TravelMode == 'ship') {echo "By Sea";} elseif ($key->TravelMode == 'air') {echo "By Air";}?></td>
				<td>
				<?php
if (is_array($key['SelectCategory'])) {
        echo implode(", ", $key['SelectCategory']);
    } else {
        echo 'n/a';
    }

    ?></td>

				<td>{{$key->Weight}}{{$key->Unit}}</td>
				<td>{{$key->Description}}</td>
				<td>{{  show_date(@$key->EnterOn) }}</td>
	            <td>{{  show_date(@$key->UpdateDate) }}</td>
				<td>
				<?php
$current_date = new MongoDate();
    //$current_date->sec;
    ?>

				@if(@$key->SourceDate->sec < $current_date->sec)
					<span class="badge bg-yellow">Inactive</span>
				@else
					<span class="badge bg-green">Active</span>
				@endif

				</td>
				<td>
					<a href="{{ url('admin/business_request',$key->_id) }}" data-placement="top" title="Request List" class="btn btn-small btn-primary btn-action">
						<i class ="fa fa-th-list"></i>
					</a>
					<a href="{{ url('admin/edit-business-trip',$key->_id) }}" data-placement="top" title="Edit" class="btn btn-primary btn-action"><i class="fa fa-edit"></i></a>
					</a>
					<a data-placement="top" title="Delete Record" class="btn btn-small btn-danger btn-action" onclick="remove_record('delete_activity/{{$key->_id}}/business-trip','{{$key->_id}}')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>
				</td>
			</tr>
		@endforeach
		<?php } else {?>
			<tr>
				<td colspan ="11" class="text-center" >No record found.</td>
			</tr>
		<?php }?>
	</tbody>
</table>
@extends('Admin::list.pagination.footer')
