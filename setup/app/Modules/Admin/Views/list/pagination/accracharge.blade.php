
<table class="table table-bordered table-striped table-highlight table-list">
<thead>
	<tr>
		<th >S.No.</th>
		<th>Country</th>
		<th>Region</th>
    	<th >Charges</th>
		<th class="text-center">Status</th>
		<th class="text-center">Action</th>
	</tr>
	</thead>
	<tbody>
		<?php if (count($accracharge) > 0) {?>

			@foreach ($accracharge as $lists)
			<tr id="row-{{$lists->_id}}">
				<td scope="row">{{$sno++}}</td>
				<td> {{ucfirst($lists->country) }} </td>
				<td> {{ ucfirst($lists->state) }}
                      @if($lists->country == 'UK')
                      (Postal Code)
                      @endif
				</td>
        		<td>${{ number_format($lists->amount,2)  }}</td>
				<td class="text-center">
					@if ($lists->status == 'Active')
		            <span class="badge bg-green">Active</span>
		            @else
		            <span class="badge bg-yellow">Inactive</span>
		            @endif
				</td>
				<td class="text-center">
				@if ($lists->status == 'Active')
						<a href="{{ url('status_activity',[$lists->_id,'Inactive','extraregion']) }}?href=admin/accra-mamagement" data-placement="top" title="Make Inactive" class="btn btn-small btn-warning btn-action" onclick ="return confirm('Are you sure? You want to inactivate this item!');">
							<i class="fa fa-lock"></i></a>
					@else
						<a href="{{ url('status_activity',[$lists->_id,'Active','extraregion']) }}?href=admin/accra-mamagement" data-placement="top" title="Make Active" class="btn btn-small btn-success btn-action" onclick ="return confirm('Are you sure? You want to activate this item');">
							<i class="fa fa-unlock"></i></a>
					@endif

                <a data-placement="top" title="Delete Record" class="btn btn-small btn-danger btn-action" onclick="remove_record('delete_activity/{{$lists->_id}}/accra','{{$lists->_id}}')" href="javascript:void(0)" onclick ="return confirm('Are you sure? You want to delete this category!')"><i class="fa fa-trash"></i></a>

             <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal{{$lists->_id}}" data-whatever="@mdo"><i class="fa fa-edit"></i></button>
			   </td>
		</tr>
			@endforeach
		<?php } else {?>
			<tr>
				<td colspan="6" class="text-center" >Recored not found</td>
			</tr>
		<?php }?>
	</tbody>
</table>



@foreach ($accracharge as $lists)
<div class="modal fade" id="exampleModal{{$lists->_id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title" id="exampleModalLabel">Edit Region</h4>
	  </div>

	  {!! Form::model('', ['name' => 'categoryForm1', 'id' => $lists->_id, 'method' => 'POST', 'url' => ['admin/update-region', $lists->_id]]) !!}

	  <div class="modal-body">

	  		<div class="col-md-6">
	  		<label>Country:</label>
	  		{{  ucfirst($lists->country)}}
			</div>

			<div class="col-md-6">
			 <label>Region:</label>
			{{  ucfirst($lists->state)}}
			</div>

			<div class="clearfix"></div>
		<div class="col-md-6">
			<div class="form-group" id="cityDiv">
				{!! Form::label('Charges', 'Charges',['class' => 'control-label']) !!}
				{!! Form::text('Charges', $lists->amount, ['class'=>'form-control', 'placeholder'=> 'Charges','id' =>'Charges'.$lists->_id]) !!}
				<p class="help-block red" id="er_Charges{{$lists->_id}}" style="color:#a94442"></p>
			</div>
		</div>

		<div class="clearfix"></div>
		</div>

	  <div class="modal-footer">
		{!! Form::button('Close', ['class' => 'btn btn-primary', 'data-dismiss'=>'modal']) !!}
		{!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
	  </div>

		{!! Form::close() !!}
	<div class="clearfix"></div>
	</div>
  </div>
</div>

<script>
$("#{{$lists->_id}}").submit(function()
{
	var flag = true;

	if(valid.required('Charges{{$lists->_id}}','charge') == false) { flag = false; }

	return flag;
});
</script>
@endforeach
@extends('Admin::list.pagination.footer')