<table class="table table-bordered table-striped table-highlight table-list">
<thead>
	<tr>
		<th>S.No.</th>
		<th>Transporter Name</th>
		<th>Package Title</th>
		<th>PackageId</th>
		<th>Amount</th>
		<th>City</th>
		<th>State</th>
		<th>Date</th>
		<th>Status</th>
		<!-- <th>Action</th>	 -->
	</tr>
	</thead>
	<tbody>
		<?php if (count($users) > 0) {
    ?>
			@foreach ($users as $lists)
			<tr id="row-{{$lists->_id}}">
				<td scope="row">{{$sno++}}</td>
				<td>{{$lists->TransporterName}}</td>
				<td>{{$lists->ProductTitle}}</td>
				<td>{{$lists->PackageId}}</td>
				<td class="text-right">${{number_format($lists->TotalCost, 2)}}</td>
				<td>{{$lists->PickupCity}}</td>
				<td>{{$lists->PickupState}}</td>
				<td>
					<?php
if (isset($lists->PaymentDate->sec)) {
        echo date('d-M-Y h:i A', $lists->PaymentDate->sec);
    }
    ?>
				</td>
				<td><?php if ($lists->PaymentStatus == 'done') {echo 'Done';} else {echo 'Pending';}?></td>
			</tr>
			@endforeach
		<?php } else {?>
			<tr>
				<td colspan="5" class="text-center" >Transation not found</td>
			</tr>
		<?php }?>
	</tbody>
</table>
@extends('Admin::list.pagination.footer')
