{!! Html::script('theme/admin/fancybox/source/jquery.fancybox.js') !!}
 {!! Html::style('theme/admin/fancybox/source/jquery.fancybox.css') !!}
 <style>
.truncate {
  max-width: 100%;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
}
</style>
<table class="table table-bordered table-striped table-highlight table-list">
<thead>
	<tr>
		<th>S.No.</th>
		<th>Description</th>
		<th>Image</th>
		<th>Status</th>
		<th width="180px">Action</th>
	</tr>
	</thead>
	<tbody>
		<?php if (count($users) > 0) {?>

			@foreach ($users as $lists)
			<tr id="row-{{$lists->_id}}">
				<td scope="row">{{$sno++}}</td>
				<td style="max-width:500px;">
          @if($lists->sliderContant != '')
          <p class="truncate"><?php echo strip_tags(ucfirst($lists->sliderContant)); ?></p>
			    @else
          <p class="truncate">N/A</p>
          @endif 
        </td>
      	<td>
                @if ($lists->image != '') 
                  <img class="" width="40px" src="<?php echo ImageUrl . $lists->image; ?>" tag="product image" />
                @else
                  <img class="" width="40px" src="<?php echo ImageUrl .'/no-image.jpg';  ?>"  />
               @endif    
        </td>
				<td class="text-center">
            @if ($lists->Status == 'active')
              <span class="badge bg-green">Active</span>
            @else
              <span class="badge bg-yellow">Inactive</span>
            @endif
            @if($lists->delete_status == 'yes')
              <br />
              <span class="red">Removed</span>
            @endif
        </td>
				<td class="text-center">
          <div style="position:relative;" class="visible-ipad">
            <button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Action <span class="caret"></span>
            </button>
          </div>
      
        @if($lists->delete_status != 'yes')
					@if ($lists->Status == 'active')
						<a href="{{ url('status_activity',[$lists->_id,'inactive','images']) }}?href=admin/silder-image" data-placement="top" title="Make Inactive" class="btn btn-small btn-success btn-action" onclick ="return confirm(' Are you sure? You want to inactivate this image!');">
						<i class="fa fa-unlock"></i></a>
					@else
						<a href="{{ url('status_activity',[$lists->_id,'active','images']) }}?href=admin/silder-image" data-placement="top" title="Make Active" class="btn btn-small btn-warning btn-action" onclick ="return confirm(' Are you sure? You want to activate this image!');">
						<i class="fa fa-lock"></i></a>
					@endif
            <!--     <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal{{$lists->_id}}" data-whatever="@mdo"><i class="fa fa-edit"></i></button> 
            -->

          <a type="button" class="btn btn-primary" data-toggle="modal" href="{{url('admin/view-image-detail/'.$lists->_id)}}" data-whatever="@mdo"><i class="fa fa-eye"></i></a>

           <a type="button" class="btn btn-primary" data-toggle="modal" href="{{url('admin/edit-image/'.$lists->_id)}}" data-whatever="@mdo"><i class="fa fa-pencil"></i></a>

					<a data-placement="top" title="Delete Record" class="btn btn-small btn-danger btn-action" onclick="remove_record('delete_activity/{{$lists->_id}}/image','{{$lists->_id}}')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>
        @endif
				</td>
			</tr>
			@endforeach
		<?php } else {?>
			<tr>
				<td colspan="11" class="text-center" >Images not found</td>
			</tr>
		<?php }?>
	</tbody>
</table>
@foreach ($users as $lists)
<div class="modal fade" id="exampleModal{{$lists->_id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="exampleModalLabel">Edit Item</h4>
    </div>

    {!! Form::model('', ['name' => 'categoryForm1', 'id' => $lists->_id, 'method' => 'POST', 'url' => ['admin/update-images', $lists->_id], 'files' => true]) !!}

    <div class="modal-body">

      <div class="col-md-12">
       <div class="form-group" id="cityDiv1">
        {!! Form::label('Title', 'Title:',['class' => 'control-label']) !!}
        {!! Form::text('title',$lists->title, ['class'=>'form-control', 'placeholder'=> 'Title','id' =>'title'.$lists->_id]) !!}
        <p class="help-block red" id="er_title{{$lists->_id}}" style="color:##a94442"></p>
       </div>
     </div>
      <div class="clearfix"></div>
   
       <div class="col-md-6">
         <div class="form-group" id="cityDiv">
        {!! Form::label('Image', 'Image:',['class' => 'control-label']) !!}
        <input type="file" name="image" id="image" >
          @if($lists->image != '')
          <br/><img src="{{ImageUrl}}/{{$lists->image}}" width="100px" height="100px;">
          @else
             <br/><img src="{{ImageUrl}}no-image.jpg" width="100px" height="100px;">
          @endif
               </div>
       </div>

    <div class="clearfix"></div>
    </div>

    <div class="modal-footer">
    {!! Form::button('Close', ['class' => 'btn btn-primary', 'data-dismiss'=>'modal']) !!}
    {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
    </div>
    {!! Form::close() !!}
    <div class="clearfix"></div>
  </div>
  </div>
</div>

<script>
$("#{{$lists->_id}}").submit(function()
{
    var flag = true;

  if(valid.required('title{{$lists->_id}}','title') == false) { flag = false; }
  return flag;
});
</script>
@endforeach
@extends('Admin::list.pagination.footer')
<!-- Button trigger modal -->