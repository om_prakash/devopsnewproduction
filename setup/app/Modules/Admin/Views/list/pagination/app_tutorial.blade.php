<table class="table table-bordered table-striped table-highlight table-list">
<thead>
	<tr>
		<th>S.No.</th>
		<th>Title</th>
		<th>Content</th>
		<th>Status</th>
		<th>Action</th>
	</tr>
	</thead>
	<tbody>
		<?php if (count($appTutorial) > 0) {?>

    	 @foreach ($appTutorial as $key)
			<tr id="row-{{$key->id}}">
				<td scope="row">{{$sno++}}</td>
				<td>{{$key->Title}}</td>
				<td>{{strip_tags($key->Content)}}</td>
				<td>
				   @if($key->Status == 'Active')
					<span class="badge bg-green">Active</span>
					@else
					<span class="badge bg-yellow">Inactive</span>
					@endif

				</td>
				<td class="width-action-20">


                	<div style="position:relative;" class="visible-ipad">
                    <button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Action
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu action-menu">

                        <li>
                        @if ($key->Status == 'Active')
						<a href="{{ url('status_activity',[$key->_id,'Inactive','app_tutorial']) }}?href=admin/app_tutorial" data-placement="top" title="Make Inactive" class="btn btn-small btn-warning btn-action" onclick ="return confirm('Are you sure? You want to inactivate App Tutorial!');">
							<i class="fa fa-lock"></i>&nbsp;Inactive</a>
					@else
						<a href="{{ url('status_activity',[$key->_id,'Active','app_tutorial']) }}?href=admin/app_tutorial" data-placement="top" title="Make Active" class="btn btn-small btn-success btn-action">
							<i class="fa fa-unlock"></i>&nbsp;Active</a>
					@endif
                        </li>
                        <li>
                        <a href="{{ url('admin/edit_app_tutorial',$key->_id) }}" data-placement="top" title="Edit" class="btn btn-small btn-primary btn-action"><i class="fa fa-edit"></i>&nbsp;Edit</a>
                        </li>
                        <li>
                        <a href="{{ url('admin/view_app_tutorial',$key->_id) }}" data-placement="top" title="View Detail" class="btn btn-small btn-info btn-action"><i class="fa fa-info"></i>&nbsp;Info</a>
                        </li>
                        <li>
                        <a data-placement="top" title="Delete Record" class="btn btn-small btn-danger btn-action" onclick="remove_record('delete_activity/{{$key->_id}}/app_tutorial','{{$key->_id}}')" href="javascript:void(0)"><i class="fa fa-trash"></i>&nbsp;Delete</a>
                        </li>
                    </ul>
               </div>

					@if ($key->Status == 'Active')
						<a href="{{ url('status_activity',[$key->_id,'Inactive','app_tutorial']) }}?href=admin/app_tutorial" data-placement="top" title="Make Inactive" class="btn btn-small btn-success btn-action" onclick ="return confirm('Are you sure? You want to inactivate App Tutorial!');">
							<i class="fa fa-unlock"></i></a>
					@else
						<a href="{{ url('status_activity',[$key->_id,'Active','app_tutorial']) }}?href=admin/app_tutorial" data-placement="top" title="Make Active" class="btn btn-small btn-warning btn-action" onclick ="return confirm('Are you sure? You want to activate App Tutorial!');">
							<i class="fa fa-lock"></i></a>
					@endif
					<a href="{{ url('admin/edit_app_tutorial',$key->_id) }}" data-placement="top" title="Edit" class="btn btn-small btn-primary btn-action"><i class="fa fa-edit"></i></a>
					<a href="{{ url('admin/view_app_tutorial',$key->_id) }}" data-placement="top" title="View Detail" class="btn btn-small btn-info btn-action"><i class="fa fa-info"></i></a>
					<a data-placement="top" title="Delete Record" class="btn btn-small btn-danger btn-action" onclick="remove_record('delete_activity/{{$key->_id}}/app_tutorial','{{$key->_id}}')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>
				</td>
			</tr>
		@endforeach
		<?php } else {?>
			<tr>
				<td colspan="4" class="text-center" >Tutorial not found</td>
			</tr>
		<?php }?>
	</tbody>
</table>
<script>
	$(document).foundation();
</script>
