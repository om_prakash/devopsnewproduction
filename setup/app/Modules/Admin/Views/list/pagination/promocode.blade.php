<table class="table table-bordered table-striped table-highlight table-list">
<thead>
	<tr>

		<th style="width:1%;">S.No.</th>
		<th style="width:">Title</th>
		<th style="width:%;">Promocode</th>
		<th style="width:%;">Minimum Order Prize</th>
		<th>Maximum Discount</th>
		<th style="width:%;">Discount Amount</th>
		<th style="width:%;">Valid From</th>
		<th style="width:%;">Valid Till</th>
		<th style="width:%;text-align:center">Max Uses</th>
		<th style="width:%;text-align:center">Max Uses Per Person</th>
		<th style="width:%;text-align:center">Uses Till Date</th>

		<th style="width:px; text-align:center" >Status</th>
		<th style="width:%;">Action</th>
	</tr>
	</thead>
	<tbody>
		<?php if (count($users) > 0) {?>

			@foreach ($users as $key)
			<tr id="row-{{$key->_id}}">
			<td scope="row">{{$sno++}}</td>
				<td>{{ucfirst($key->Title)}}</td>
				<td>{{ucfirst($key->Code)}}</td>
				<td>{{number_format($key->MinimumOrderPrice,2)}}</td>

				<td>{{number_format($key->MaximumDiscount,2)}}</td>

				<td>{{number_format($key->DiscountAmount,2)}}</td>
				<td>{{ show_date(@$key->ValidFrom,'M d, Y') }}</td>
				<!--<td>{{date('M d,Y',@$key->ValidFrom->sec)}}</td>-->
				<td>{{ show_date(@$key->ValidTill,'M d, Y') }}</td>
				<!--<td>{{date('M d,Y',@$key->ValidTill->sec)}}</td>-->
				<td style="text-align:center">{{$key->MaxUses}}</td>
				<td style="text-align:center">{{$key->MaxUsesPerPerson}}</td>
				<td style="text-align:center">{{$key->UsesCount}}</td>

				<td align="center">

				@if(isset($key->ValidTill->sec) && (new MongoDate())->sec > @$key->ValidTill->sec)
					<span class="badge bg-red">Expired</span>
				@elseif($key->Status == 'active')
					<span class="badge bg-green">Active</span>
				@elseif($key->Status == 'inactive')
					<span class="badge bg-yellow">Inactive</span>
				@endif

				</td>
				<td class="width-action-20">

                   <div style="position:relative;" class="visible-ipad">
                 <button type="button"  class="btn btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      					Action<span class="caret"></span>
    			</button>
    			<ul class="dropdown-menu action-menu">

      				<li>
                      @if(!isset($key->ValidTill->sec) || @$key->ValidTill->sec > (new MongoDate())->sec)

					@if ($key->Status == 'active')
						<a href="{{ url('status_activity',[$key->_id,'inactive','Promocode']) }}?href=admin/promocode" data-placement="top" title="Make Inactive" class="btn btn-small btn-success btn-action" onclick ="return confirm(' Are you sure? You want to inactivate this promocode!');">
								<i class="fa fa-unlock"></i>Active</a>
					@else
						<a href="{{ url('status_activity',[$key->_id,'active','Promocode']) }}?href=admin/promocode" data-placement="top" title="Make Active" class="btn btn-small btn-warning btn-action" onclick ="return confirm(' Are you sure? You want to activate this promocode!');">
								<i class="fa fa-lock"></i>Inactive</a>
					@endif
				@endif
					</li>

                    <li>
						<a href="{{ url('admin/edit-promocode',$key->_id) }}" data-placement="top" title="Edit" class="btn btn-primary btn-action"><i class="fa fa-edit"></i>&nbsp;Edit</a>
					</li>

                    <li>
					<a href="{{ url('admin/promocode-detail',$key->_id) }}" data-placement="top" title="View Detail" class="btn btn-small btn-info btn-action"><i class="fa fa-info"></i>&nbsp;Info</a>
					</li>

                    <li>
					<a data-placement="top" title="Delete Record" class="btn btn-small btn-danger btn-action" onclick="remove_record('delete_activity/{{$key->_id}}/promocode','{{$key->_id}}')" href="javascript:void(0)"><i class="fa fa-trash"></i>&nbsp;Delete</a>

                    </li>
    </ul>
                </div>
                @if(!isset($key->ValidTill->sec) || @$key->ValidTill->sec > (new MongoDate())->sec)

					@if ($key->Status == 'active')
						<a href="{{ url('status_activity',[$key->_id,'inactive','Promocode']) }}?href=admin/promocode" data-placement="top" title="Make Inactive" class="btn btn-small btn-success btn-action" onclick ="return confirm(' Are you sure? You want to inactivate this promocode!');">
								<i class="fa fa-unlock"></i></a>
					@else
						<a href="{{ url('status_activity',[$key->_id,'active','Promocode']) }}?href=admin/promocode" data-placement="top" title="Make Active" class="btn btn-small btn-warning btn-action" onclick ="return confirm(' Are you sure? You want to activate this promocode!');">
								<i class="fa fa-lock"></i></a>
					@endif
				@endif

					<a href="{{ url('admin/edit-promocode',$key->_id) }}" data-placement="top" title="Edit" class="btn btn-primary btn-action"><i class="fa fa-edit"></i></a>

					<a data-placement="top" title="Delete Record" class="btn btn-small btn-danger btn-action" onclick="remove_record('delete_activity/{{$key->_id}}/promocode','{{$key->_id}}')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>
					  <a href="{{ url('admin/promocode-detail',$key->_id) }}" data-placement="top" title="Assign user" class="btn btn-primary btn-action"><i class="fa fa-hand-o-up" aria-hidden="true"></i>
                    </a>
				</td>
			</tr>
			@endforeach
		<?php } else {?>
			<tr>
				<td colspan="8" class="text-center" >Promocode not found</td>
			</tr>
		<?php }?>
	</tbody>
</table>
@extends('Admin::list.pagination.footer')
@foreach ($users as $lists)
<!--- Pop Up - -->
<div class="modal fade" id="exampleModal{{$lists->_id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title" id="exampleModalLabel">Penalty Amount</h4>
	  </div>

	  {!! Form::model('', ['name' => 'countryForm1', 'id' => 'countryForm1', 'method' => 'POST', 'url' => ['admin/refund_amount', $lists->_id]]) !!}

	  <div class="modal-body">
			<div class="form-group" id="cityDiv1">
				{!! Form::label('Penalty', 'Penalty Amount:',['class' => 'control-label']) !!}
				{!! Form::text('penalty', $lists->Penalty, ['class'=>'form-control', 'placeholder'=> 'Penalty Amount','id' =>'penalty']) !!}

				<p class="help-block" id="state_msg1" style="display: none;">Penalty field is required.</p>
				<p class="help-block" id="state__valid_msg1" style="display: none;">Only numbers are required.</p>
			</div>

	  </div>
	  <div class="modal-footer">
		{!! Form::button('Close', ['class' => 'btn btn-primary', 'data-dismiss'=>'modal']) !!}
		{!! Form::submit('Submit', ['class' => 'btn btn-primary','onclick' => "return confirm('Are you sure? You want to charge penalty from transporter!');"]) !!}
	  </div>
		<div class="clearfix"></div>
		{!! Form::close() !!}

	</div>
  </div>
</div>
<script>
$(document).ready(function(){
        $("#countryForm1").submit(function(){
		var flag = true;
		var numbers = /^[0-9]+$/;
		 if($("#penalty").val() =="")
			{
				 flag = false;
				 $("#cityDiv1").addClass( "has-error" );
				 $("#state_msg1").show();
			}
			else
			{
				 $("#state_msg1").hide();
				 if($.isNumeric($("#penalty").val()))
			      {
				 	 $("#state__valid_msg1").hide();
			      }
			      else
			      {
			      	 flag = false;
			      	 $("#cityDiv1").addClass( "has-error");
				 	 $("#state__valid_msg1").show();
			      }
			}

		return flag;

	});

});


$('#TotalRecordFound').html('Showing Records: '+{{$count}});
</script>


<!--- PopUp End	-->
@endforeach
<?php

/*function get_status_title($status) {
switch ($status) {

case 'ready':
return 'Ready';
break;
case 'trip_pending':
return 'Pending';
break;
case 'trip_ready':
return 'Ready';
break;
case 'out_for_delivery':
return 'On Delivery';
break;
case 'out_for_pickup':
return 'Out for Pickup';
break;
case 'delivered':
return 'Delivered';
break;
case 'cancel':
return 'Cancel';
break;
case 'assigned':
return 'Assigned';
break;
case 'purchased':
return 'Purchased';
break;
case 'accepted':
return 'Accepted';
break;
case 'paid':
return 'Paid';
break;
default:
return 'Pending';

}
}*/
?>



