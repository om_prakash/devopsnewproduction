<table class="table table-bordered table-striped table-highlight table-list">
<thead>
  <tr>
    

    <th width="2%">S.No.</th>
    <th width="10%">Name</th>
    <th width="10%">Package Number</th>
    <th>Package Name</th>
    <th width="12%">Created date</th>
    <th width="8%">Action</th>
  </tr>
  </thead>
  <tbody>
    <?php if (count($slip) > 0) {
    ?>
     @foreach ($slip as $key)
      <tr id="row-{{$key->id}}">
        <td scope="row">{{$sno++}}</td>
        <td>{{$key->RequesterName}}</td>
        <td>{{$key->PackageNumber}}</td>
        <td>
        

        <?php $string = ''; ?>
        @if(count(@$key->ProductList) > 0)
          @foreach($key->ProductList as $val)
            
            @if($string == '')
            <?php $string .= $val['product_name']; ?>
            @else
            <?php $string .=", ". $val['product_name']; ?>
            @endif
          @endforeach
        @endif
        {{$string}}
        </td>
        <td>{{show_date($key->EnterOn)}}</td>
        <td >
          <a>
          <a href="{{ url('admin/get-packing-slip')}}?RequesterName={{$key->UserId}}&orderId={{$key->PackageNumber}}&slip_id={{$key->_id}}" data-placement="top" title="View Detail" class="btn btn-small btn-primary btn-action"><i class="fa fa-th-list"></i></a>

           <a href="{{ url('admin/delete-packing-slip')}}?slip_id={{$key->_id}}" data-placement="top" title="delete" class="btn btn-small btn-primary btn-danger" onclick="return confirm('Are you sure you want to delete.');"><i class="fa fa-trash"></i></a>
        </td>
      </tr>
    @endforeach
    <?php } else {?>
      <tr>
        <td colspan="7" class="text-center" >Slip list not found</td>
      </tr>
    <?php }?>
  </tbody>
</table>
@extends('Admin::list.pagination.footer')


