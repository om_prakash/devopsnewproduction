<table class="table table-bordered table-striped table-highlight table-list">
	<thead>
		<tr>
			<th>S.No.</th>
			<th>Title</th>
			<th>By Comment</th>
			<th>Comment</th>
			<th>Date</th>
			<!--<th>Action</th>-->
		</tr>
	</thead>
	<tbody>
		<?php if (count($info) > 0) {?>

    	@foreach ($info as $key)
			<tr id="row-{{$key->id}}">
				<td scope="row">{{$sno++}}</td>
				<td>{{$key->product_title}}</td>
				<td>{{$key->by_comment}}</td>
				<td>{{$key->comment}}</td>
				<td>{{ show_date($key->Date) }}</td>
			</tr>
		@endforeach
		<?php } else {?>
			<tr>
				<td colspan="5" class="text-center" >Comment not found</td>
			</tr>
		<?php }?>
	</tbody>
</table>
@extends('Admin::list.pagination.footer')