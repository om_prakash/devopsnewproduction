
<table class="table table-bordered table-striped table-highlight table-list">
<thead>
	<tr>
		<th>S.No.</th>
		<th>Package Name</th>
		<th>Package Id</th>
		<th>Requester Name</th>
		<th>Pickup Address</th>
		<th>Drop Address</th>
		<th>Pickup Date</th>
		<th>Drop Date</th>
		<th>Status</th>
		<th>Action</th>
	</tr>
	</thead>
	<tbody>
		<?php if(count($users) > 0) { ?>
		
			@foreach ($users as $lists)			
			<tr id="row-{{$lists->_id}}">
				<td scope="row">{{$sno++}}</td>
				<td>{{$lists->ProductTitle}}</td>
				<td>{{$lists->PackageNumber}}</td>
				<td>{{$lists->RequesterName}}</td>
				<td>{{$lists->PickupFullAddress}}</td>
				<td>{{$lists->DeliveryFullAddress}}</td>
				<td><?php
						if(isset($lists->PickupDate->sec)){
							echo date('M d, Y g:i A',$lists->PickupDate->sec);
						}
				?></td>
				<td><?php
						if(isset($lists->DeliveryDate->sec)){
							echo date('M d, Y g:i A',$lists->DeliveryDate->sec);
						}
				?></td>
				
				<td><?php echo get_status_title($lists->Status); ?></td>
				<td class="width-action-20">
                
                    <div style="position:relative;" class="visible-ipad">
                    	<button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    	Action
                    <span class="caret"></span>
                    </button>
                        <ul class="dropdown-menu action-menu">
                        	
                    <li>
					<a href="{{ url('admin/package/detail',$lists->_id) }}" data-placement="top" title="View Detail" class="btn btn-small btn-primary btn-action"><i class="fa fa-info"></i>&nbsp;Detail</a>
                    </li>
                    
                    <li>
					
                    </li>
                    <li>
					<a href="{{ url('admin/package/detail',$lists->_id) }}" data-placement="top" title="Refund" class="btn btn-small btn-primary btn-action"><i class="fa fa-credit-card"></i>&nbsp;Refund</a>
                    </li>
                   
                        </ul>
                    </div>
					<a data-placement="top" title="Delete Record" class="btn btn-small btn-danger btn-action" onclick="remove_record('delete_activity/{{$lists->_id}}/package','{{$lists->_id}}')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>
					<a href="{{ url('admin/package/detail',$lists->_id) }}" data-placement="top" title="View Detail" class="btn btn-small btn-info btn-action"><i class="fa fa-info"></i></a>
				</td>
			</tr>
			@endforeach
		<?php } else { ?>
			<tr>
				<td colspan="10" class="text-center" >No request found</td>
			</tr>
		<?php } ?>
	</tbody>
</table>
@extends('Admin::list.pagination.footer')
@foreach ($users as $lists)
<!----- Pop Up - --> 
<div class="modal fade" id="exampleModal{{$lists->_id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title" id="exampleModalLabel">Penalty Amount</h4>
	  </div>
	  
	  {!! Form::model('', ['name' => 'countryForm1', 'id' => 'countryForm1', 'method' => 'POST', 'url' => ['admin/refund_amount', $lists->_id]]) !!}
	  
	  <div class="modal-body">
			<div class="form-group" id="cityDiv1">
				{!! Form::label('Penalty', 'Penalty Amount:',['class' => 'control-label']) !!}
				{!! Form::text('penalty', $lists->Penalty, ['class'=>'form-control', 'placeholder'=> 'Penalty Amount','id' =>'penalty']) !!}
				
				<p class="help-block" id="state_msg1" style="display: none;">Penalty field is required.</p>	
				<p class="help-block" id="state__valid_msg1" style="display: none;">Only numbers are required.</p>	
			</div>
	
	  </div>
	  <div class="modal-footer">
		{!! Form::button('Close', ['class' => 'btn btn-primary', 'data-dismiss'=>'modal']) !!}	
		{!! Form::submit('Submit', ['class' => 'btn btn-primary','onclick' => "return confirm('Are you sure applied Penalty');"]) !!}
	  </div>
		<div class="clearfix"></div>
		{!! Form::close() !!}
	
	</div>
  </div>
</div>
<script>
$(document).ready(function(){
        $("#countryForm1").submit(function(){
		var flag = true;
		var numbers = /^[0-9]+$/; 
		 if($("#penalty").val() =="")
			{ 
				 flag = false;
				 $("#cityDiv1").addClass( "has-error" );
				 $("#state_msg1").show();
			}
			else
			{
				 $("#state_msg1").hide();
				 if($.isNumeric($("#penalty").val()))  
			      {  
				 	 $("#state__valid_msg1").hide(); 
			      }  
			      else  
			      {  
			      	 flag = false;
			      	 $("#cityDiv1").addClass( "has-error");
				 	 $("#state__valid_msg1").show();
			      } 
			}
		
		return flag;
	
	});

});


$('#TotalRecordFound').html('Showing Records: '+{{$count}});
</script>


<!--- PopUp End	-->	
@endforeach
<?php
function get_status_title($status)
{
	switch($status)
	{
		case 'ready':
			return 'Ready';
		break;
		case 'trip_pending':
			return 'Pending';
		break;
		case 'trip_ready':
			return 'Ready';
		break;
		case 'out_for_delivery':
			return 'On Delivery';
		break;		
		case 'out_for_pickup':
			return 'Out for Pickup';
		break;		 
		case 'delivered':
			return 'Delivered';
		break;
		case 'cancel':
			return 'Cancel';
		break;
		default:
			return 'Pending';
		
	}
}
?>
