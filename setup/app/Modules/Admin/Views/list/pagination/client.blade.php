<style>
.truncate {
  max-width: 100%;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
}
</style>
<table class="table table-bordered table-striped table-highlight table-list">
<thead>
	<tr>
		<th>S.No.</th>
		<th>Name</th>
		<th>Position</th>
    <th>Message</th>
    <th>Image</th>
    <th>status</th>
		<th width="14%">Action</th>
	</tr>
	</thead>
	<tbody>
		<?php if (count($client) > 0) {?>

			@foreach ($client as $lists)
			<tr id="row-{{$lists->_id}}">
				<td scope="row">{{$sno++}}</td>
				<td>{{ucfirst($lists->name)}}</td>
				<td>{{ucfirst($lists->position)}}</td>
        <td style="max-width:500px;">
          <p class="truncate">{{ucfirst($lists->description)}}</p></td>

        <td>
           @if ($lists->image != '') 
                <img class="" width="40px" src="<?php echo ImageUrl . $lists->image; ?>" tag="product image" />
           @else
                <img class="" width="40px" src="<?php echo ImageUrl .'/no-image.jpg';  ?>"  />
            @endif 
        </td>  
				<td class="text-center">
            @if ($lists->Status == 'active')
              <span class="badge bg-green">Active</span>
            @else
              <span class="badge bg-yellow">Inactive</span>
            @endif
        </td>
				<td class="text-center">
          <div style="position:relative;" class="visible-ipad">
            <button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Action <span class="caret"></span>
            </button>
          </div>

          @if ($lists->Status == 'active')
            <a href="{{ url('status_activity',[$lists->_id,'inactive','client']) }}?href=admin/client" data-placement="top" title="Make Inactive" class="btn btn-small btn-success btn-action" onclick ="return confirm(' Are you sure? You want to inactivate this image!');">
            <i class="fa fa-unlock"></i></a>
          @else
            <a href="{{ url('status_activity',[$lists->_id,'active','client']) }}?href=admin/client" data-placement="top" title="Make Active" class="btn btn-small btn-warning btn-action" onclick ="return confirm(' Are you sure? You want to activate this image!');">
            <i class="fa fa-lock"></i></a>
          @endif
  
					<a data-placement="top" title="Delete Record" class="btn btn-small btn-danger btn-action" onclick="remove_record('delete_activity/{{$lists->_id}}/client','{{$lists->_id}}')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>

          <a type="button" class="btn btn-primary" data-toggle="modal" href="{{url('admin/edit-client/'.$lists->_id)}}" data-whatever="@mdo"><i class="fa fa-pencil"></i></a>

				</td>
			</tr>
			@endforeach
		<?php } else {?>
			<tr>
				<td colspan="11" class="text-center" >Recored not found</td>
			</tr>
		<?php }?>
	</tbody>
</table>


@extends('Admin::list.pagination.footer')



