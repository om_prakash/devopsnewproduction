 <table class="table table-bordered table-striped table-highlight table-list">
<thead>
	<tr>
		<th width="1%">S.No.</th>
		<th>Title</th>
		<th>Message</th>		
		<th>Date</th>
		<th>Action</th>
	</tr>
	</thead>
	<tbody>
		<?php if(count($users) > 0) { ?>
			@foreach ($users as $lists) 
			<tr id="row-{{$lists->_id}}">
				<td scope="row">{{$sno++}}</td>
				<td>{{$lists->NotificationTitle}}</td>
				<td>{{$lists->NotificationMessage}}</td>
				<td>
					@if(isset($lists->Date->sec))
						{{date('d-m-Y h:i A',$lists->Date->sec)}}
					@endif
				</td>
				<td>
					@if($lists->location == 'request_detail')
						<a href="{{url('admin/package/detail',$lists->locationkey)}}" title="View Detail" class="btn btn-small btn-info btn-action"><i class="fa fa-info"></i></a>
					@elseif($lists->location == 'support_request')
						<a href="{{url('admin/support_view_more',$lists->locationkey)}}" title="View Detail" class="btn btn-small btn-info btn-action"><i class="fa fa-info"></i></a>
					@elseif($lists->location == 'user')
						<a href="{{url('admin/user/detail/',$lists->locationkey)}}" title="View Detail" class="btn btn-small btn-info btn-action"><i class="fa fa-info"></i></a>
					@endif
				
			</tr>
			@endforeach
		<?php } else { ?>
			<tr>
				<td colspan="5" class="text-center" >User not found yet</td>
			</tr>
		<?php } ?>
	</tbody>
</table>
@extends('Admin::list.pagination.footer')

