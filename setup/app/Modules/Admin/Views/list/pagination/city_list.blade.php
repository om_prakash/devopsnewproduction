<script>

$(document).ready(function(){

        $("#cityForm").submit(function(){
		var flag = true;

		 if($("#Content").val() =="")
			{
				 flag = false;
				 $("#cityDiv").addClass( "has-error" );
				 $("#city_msg").show();
			}
			else
			{
				 $("#city_msg").hide();
			}
		return flag;

	});
});

</script>
<table class="table table-bordered table-striped table-highlight table-list">
<thead>
	<tr>
		<th width="8%">S.No.</th>
		<th width="25%">City</th>
		<th width="30%"> State</th>
		<th width="50px" style="text-align:center">Status</th>
		<th width="12%" style="text-align:center">Signup Status</th>
		<th width="100px" style="text-align:center">Action</th>
	</tr>
	</thead>
	<tbody>

	 <?php /*  echo "<pre>";   print_r($city);  die; */?>



		<?php if (count($city) > 0) {?>
			@foreach ($city as $lists)
			<tr id="row-{{$lists->_id}}">
				<td scope="row" width="100px;">{{$sno++}}</td>
				<td>{{ucfirst($lists->Content)}}</td>

				<td>{{ucfirst(str_replace('-', ' ',$lists->SuperName))}}</td>

				<td width="100px;" style="text-align: center;">
					@if($lists->Status == 'Active')
					    <a href="{{ url('status_activity',[$lists->_id,'Inactive','City_State_Country']) }}?href=admin/city_list?id={{$lists->StateId}}" data-placement="top" title="Make Inactive"  onclick ="return confirm('Are you sure? You want to inactivate this city!');">
						<span class="badge bg-green">Active</span></a>
					@else
					    <a href="{{ url('status_activity',[$lists->_id,'Active','City_State_Country']) }}?href=admin/city_list?id={{$lists->StateId}}" data-placement="top" title="Make Active" onclick ="return confirm('Are you sure? You want to activate this city!');">
						<span class="badge bg-yellow">Inactive</span>
					@endif
				</td>
				<td width="100px;" style="text-align: center;">
					@if($lists->Signup)
						<a href="{{ url('status_activity',[$lists->_id,'false','City_State_Country_Signup']) }}?href=admin/city_list?id={{$lists->StateId}}" data-placement="top" title="Make Inactive" onclick ="return confirm('Are you sure? You want to inactivate this city for signup!');">
							<span class="badge bg-green">Active</span>
						</a>
					@else
						<a href="{{ url('status_activity',[$lists->_id,'true','City_State_Country_Signup']) }}?href=admin/city_list?id={{$lists->StateId}}" data-placement="top" title="Make Active" onclick ="return confirm('Are you sure? You want to activate this city for signup!');">
							<span class="badge">Inactive</span>
						</a>
					@endif
				</td>
				<td class="text-center" width="150px;">

                <div style="position:relative;" class="visible-ipad">
                            <button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Action
                        <span class="caret"></span>
                        </button>
                     <ul class="dropdown-menu action-menu">
                     	<li>

                      @if ($lists->Status == 'Active')
						<a href="{{ url('status_activity',[$lists->_id,'Inactive','City_State_Country']) }}?href=admin/city_list?id={{$lists->StateId}}" data-placement="top" title="Make Inactive" class="btn btn-small btn-success btn-action">
							<i class="fa fa-lock"></i>&nbsp;Inactive</a>
					@else
						<a href="{{ url('status_activity',[$lists->_id,'Active','City_State_Country']) }}?href=admin/city_list?id={{$lists->StateId}}" data-placement="top" title="Make Active" class="btn btn-small btn-warning btn-action">
							<i class="fa fa-unlock"></i>&nbsp;Active</a>
					@endif
                    </li>
                    <li>
					<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal{{$lists->_id}}" data-whatever="@mdo"><i class="fa fa-edit"></i>&nbsp;Edit</button>
					</li>
                    <li>
					<a data-placement="top" title="Delete Record" class="btn btn-small btn-danger btn-action" onclick="remove_record('delete_activity/{{$lists->_id}}/city_list','{{$lists->_id}}')" href="javascript:void(0)"><i class="fa fa-trash"></i>&nbsp;Delete</a>
                    </li>

                </ul>
                </div>
					@if ($lists->Status == 'Active')
						<a href="{{ url('status_activity',[$lists->_id,'Inactive','City_State_Country']) }}?href=admin/city_list?id={{$lists->StateId}}" data-placement="top" title="Make Inactive" class="btn btn-small btn-success btn-action" onclick ="return confirm('Are you sure? You want to inactivate this city!');">
							<i class="fa fa-lock"></i></a>
					@else
						<a href="{{ url('status_activity',[$lists->_id,'Active','City_State_Country']) }}?href=admin/city_list?id={{$lists->StateId}}" data-placement="top" title="Make Active" class="btn btn-small btn-warning btn-action" onclick ="return confirm('Are you sure? You want to activate this city!');">
							<i class="fa fa-unlock"></i></a>
					@endif
				{{-- 	<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal{{$lists->_id}}" data-whatever="@mdo"><i class="fa fa-edit"></i> </button> --}}

					<a class="btn btn-primary" href="{{url('admin/editcity/'.$lists->_id) }}"><i class="fa fa-edit"></i> </a>

					<a data-placement="top" title="Delete Record" class="btn btn-small btn-danger btn-action" onclick="remove_record('delete_activity/{{$lists->_id}}/city_list','{{$lists->_id}}')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>
				</td>
			</tr>
			@endforeach
		<?php } else {?>
			<tr>
				<td colspan="5" class="text-center" >City not found</td>
			</tr>
		<?php }?>
	</tbody>
</table>
@extends('Admin::list.pagination.footer')
