<table class="table table-bordered table-striped table-highlight table-list">
<thead>
	<tr>
		<th>S.No.</th>
		<th>Question</th>
		<th>Answer</th>		
		<th>Status</th>	
		<th>Action</th>	
	</tr>
	</thead>
	<tbody>
		<?php if(count($section) > 0) { ?>
			@foreach ($section as $lists) 
			<tr id="row-{{$lists->_id}}">
				<td scope="row">{{$sno++}}</td>
				<td>{{$lists->section_name}}...</td>
				<td>gffgfdgdgd</td>		
				<td>
				
					@if ($lists->Status == 'Active') 
					<span class="badge bg-green">Active</span>
					@else
					<span class="badge bg-yellow">Inactive</span>
					@endif
				</td>
				<td class="width-action-20">
                
                
                
                <div style="position:relative;" class="visible-ipad">
                            <button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Action
                        <span class="caret"></span>
                        </button>
                     <ul class="dropdown-menu action-menu">
                       
                        <li>
                        @if ($lists->Status == 'Active') 
						<a href="{{ url('status_activity',[$lists->_id,'Inactive','faq_list']) }}?href=admin/faq_list" data-placement="top" title="Make Inactive" class="btn btn-small btn-warning btn-action">
							<i class="fa fa-lock"></i>&nbsp;Inactive</a>
					@else
						<a href="{{ url('status_activity',[$lists->_id,'Active','faq_list']) }}?href=admin/faq_list" data-placement="top" title="Make Active" class="btn btn-small btn-success btn-action">
							<i class="fa fa-unlock"></i>&nbsp;Active</a>
					@endif
                    </li>
                    <li>
					<a href="{{ url('admin/edit_faq/'.$lists->_id)}}" data-placement="top" title="Edit" class="btn btn-small btn-primary btn-action">
						<i class="fa fa-pencil-square-o"></i>&nbsp;Edit</a>
					</li>
                    <li>
					<a data-placement="top" title="Delete Record" class="btn btn-small btn-danger btn-action" onclick="remove_record('delete_activity/{{$lists->_id}}/faq_list','{{$lists->_id}}')" href="javascript:void(0)"><i class="fa fa-trash"></i>&nbsp;Delete</a>
                    </li>
                     </ul>
                  </div>
					@if ($lists->Status == 'Active') 
						<a href="{{ url('status_activity',[$lists->_id,'Inactive','faq_list']) }}?href=admin/faq_list" data-placement="top" title="Make Inactive" class="btn btn-small btn-warning btn-action" onclick ="return confirm('Are you sure? You want to inactivate FAQ!');">
							<i class="fa fa-lock"></i></a>
					@else
						<a href="{{ url('status_activity',[$lists->_id,'Active','faq_list']) }}?href=admin/faq_list" data-placement="top" title="Make Active" class="btn btn-small btn-success btn-action" onclick ="return confirm('Are you sure? You want to activate FAQ!');">
							<i class="fa fa-unlock"></i></a>
					@endif
					<a href="{{ url('admin/edit_faq/'.$lists->_id)}}" data-placement="top" title="Edit" class="btn btn-small btn-primary btn-action">
						<i class="fa fa-pencil-square-o"></i></a>
					<a data-placement="top" title="Delete Record" class="btn btn-small btn-danger btn-action" onclick="remove_record('delete_activity/{{$lists->_id}}/faq_list','{{$lists->_id}}')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>
				</td>
			</tr>
			@endforeach
		<?php } else { ?>
			<tr>
				<td colspan="5" class="text-center" >FAQ not found</td>
			</tr>
		<?php } ?>
	</tbody>
</table>
@extends('Admin::list.pagination.footer')
