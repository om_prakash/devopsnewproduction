<table class="table table-bordered table-striped table-highlight table-list">
<thead>
  <tr>
    <th width="2%">S.No.</th>
    <th width="10%">Name</th>
    <th width="10%">Package Number</th>
    <th>Package Name</th>
    <th width="12%">Created date</th>
    <th width="8%">Action</th>
  </tr>
  </thead>
  <tbody>
    <?php if (count($slip) > 0) {?>
     @foreach ($slip as $val)
      <tr id="row-{{$val->id}}">
        <td scope="row">{{$sno++}}</td>
        <td>
        {{$val->RequesterName}}
        </td>
        <td>
        {{$val->PackageNumber}}
        </td>
        <td>
        <?php $string = ''; ?>
        @if(count(@$val->ProductList) > 0)
          @foreach($val->ProductList as $key)
            
            @if($string == '')
            <?php $string .= $key['product_name']; ?>
            @else
            <?php $string .=", ". $key['product_name']; ?>
            @endif
          @endforeach
        @endif
        {{$string}}
        </td>
        <td>{{show_date($val->EnterOn)}}</td>
        <td >
          <a>
          <a href="{{url('admin/get-slip')}}?RequesterName={{$val->UserId}}&orderId={{$val->PackageNumber}}&slip_id={{$val->_id}}" data-placement="top" title="View Detail" class="btn btn-small btn-primary btn-action"><i class="fa fa-th-list"></i></a>

           <a href="{{ url('admin/delete-packing-slip')}}?slip_id={{$val->_id}}" data-placement="top" title="delete" class="btn btn-small btn-primary btn-danger" onclick="return confirm('Are you sure you want to delete.');"><i class="fa fa-trash"></i></a>

        </td>
      </tr>
    @endforeach
    <?php } else {?>
      <tr>
        <td colspan="7" class="text-center" >Slip list not found</td>
      </tr>
    <?php }?>
  </tbody>
</table>
@extends('Admin::list.pagination.footer')


