<?php 
if($no_of_paginations!=0){
/* ---------------Calculating the starting and endign values for the loop----------------------------------- */
if ($cur_page >= 7) {
    $start_loop = $cur_page - 3;
    if ($no_of_paginations > $cur_page + 3)
        $end_loop = $cur_page + 3;
    else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
        $start_loop = $no_of_paginations - 6;
        $end_loop = $no_of_paginations;
    } else {
        $end_loop = $no_of_paginations;
    }
} else {
    $start_loop = 1;
    if ($no_of_paginations > 7)
        $end_loop = 7;
    else
        $end_loop = $no_of_paginations;
}
/* ----------------------------------------------------------------------------------------------------------- */
?>

<div class='pagination'>
	<form role="search" class="navbar-form navbar-left margin-0 pull-left">
		<div class='displaypag form-group input-group-md'>Display# 
			<select id='howpage' class='displaypagselect form-control btn-small' onChange='Changepagnum(this.value);'>
				<option value='10' <?php if($per_page == 10){ echo 'selected="selected"'; } ?> >10 </option>
				<option value='20' <?php if($per_page == 20){ echo 'selected="selected"'; } ?> >20 </option>
				<option value='50' <?php if($per_page == 50){ echo 'selected="selected"'; } ?> >50 </option>
				<option value='100' <?php if($per_page == 100){ echo 'selected="selected"'; } ?> >100 </option>
			</select>
		</div>
	</form>
<ul class="pagination margin-0 navbar-center add-page" > 

<li p='1' <?php if ($first_btn && $cur_page > 1) { echo  'class="active"'; } else if ($first_btn) { echo  'class="inactive"'; } ?> ><a href='javascript:void(0);'>First</a></li>


<?php if ($previous_btn && $cur_page > 1) {
    $pre = $cur_page - 1; ?>
    <li p='<?php echo $pre; ?>' class='active'><a href='javascript:void(0);'>Previous</a></li>
<?php } else if ($previous_btn) { ?>
    <li class='inactive'><a href='javascript:void(0);'>Previous</a></li>
<?php } 

for ($i = $start_loop; $i <= $end_loop; $i++) { ?>
        <li p='<?php echo $i; ?>' class='<?php echo ($cur_page == $i)? "current-active" : "active"; ?>' >
			<a href='javascript:void(0);'><?php echo $i; ?></a>
		</li> <?php        
}

// TO ENABLE THE NEXT BUTTON
if ($next_btn && $cur_page < $no_of_paginations) {
    $nex = $cur_page + 1; ?>
    <li p='<?php echo $nex; ?>' class='active'><a href='javascript:void(0);'>Next</a></li><?php
} else if ($next_btn) { ?>
    <li class='inactive'><a href='javascript:void(0);'>Next</a></li><?php
}

// TO ENABLE THE END BUTTON
if ($last_btn && $cur_page < $no_of_paginations) { ?>
    <li p="<?php echo $no_of_paginations; ?>" class='active'><a href='javascript:void(0);'>Last</a></li><?php
} else if ($last_btn) { ?>
    <li p="<?php echo $no_of_paginations; ?>" class='inactive'><a href='javascript:void(0);'>Last</a></li><?php
} ?>
<input type='hidden' id='CurPage' value="<?php echo $cur_page; ?>" > 
</ul>
<form role="search" class="navbar-form navbar-right margin-0">
<input type='text' class='goto form-control pagination-input1' placeholder='Enter page no.' size='12' />
	<span class='searcherror'></span>
	<input type='button' id='go_btn' class='btn btn-secondary' value='Go'/><?php
if($no_of_paginations=='0'){
	$cur_page = 0; ?>
	<span class='total' a="<?php echo $no_of_paginations; ?>" >Page <b>1</b> of <b>1</b></span> <?php
}else{ ?>
	<span class='total' a="<?php echo $no_of_paginations; ?>">Page <b><?php echo $cur_page; ?></b> of <b><?php echo $no_of_paginations; ?></b></span><?php
} ?>
</div><?php

}
else  
{ ?>
	<input type='hidden' id='CurPage' value="<?php echo $cur_page; ?>"><input type='hidden' id='howpage' value='10'> <?php
}
?>
</form>
<script>

</script>