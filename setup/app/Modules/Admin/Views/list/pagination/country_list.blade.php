<script>
$(document).ready(function(){
        $("#countryForm1").submit(function(){
		var flag = true;

		 if($("#Content1").val() =="")
			{
				 flag = false;
				 $("#cityDiv1").addClass( "has-error" );
				 $("#state_msg1").show();
			}
			else
			{
				 $("#state_msg1").hide();
			}
		return flag;

	});
});

</script>
<table class="table table-bordered table-striped table-highlight table-list">
<thead>
	<tr>
		<th>S.No.</th>
		<th>Country</th>
		<th width="15%">Status</th>
		<th width="15%">Signup Status</th>
		<th width="15%">Action</th>
	</tr>
	</thead>
	<tbody>
		<?php if (count($country) > 0) {?>
			@foreach ($country as $lists)
			<tr id="row-{{$lists->_id}}">
				<td scope="row" width="100px;">{{$sno++}}</td>
				<td>{{ucfirst($lists->Content)}}</td>
				<td width="100px;" style="text-align: center;">
					@if($lists->Status == 'Active')
						<a href="{{ url('status_activity',[$lists->_id,'Inactive','City_State_Country']) }}?href=admin/country_list" data-placement="top" title="Make Inactive"  onclick ="return confirm('Are you sure? You want to inactivate this country!');">
							</i>
							<span class="badge bg-green">Active</span></a>
					@else
						<a href="{{ url('status_activity',[$lists->_id,'Active','City_State_Country']) }}?href=admin/country_list" data-placement="top" title="Make Active"  onclick ="return confirm('Are you sure? You want to activate this country!');">
							<span class="badge bg-yellow">Inactive</span></a>
					@endif
				</td>
				<td width="100px;" style="text-align: center;">
					@if($lists->Signup)
						<a href="{{ url('status_activity',[$lists->_id,'false','City_State_Country_Signup']) }}?href=admin/country_list" data-placement="top" title="Make Inactive" onclick ="return confirm('Are you sure? You want to inactivate this country for signup!');">
							<span class="badge bg-green">Active</span>
						</a>
					@else
						<a href="{{ url('status_activity',[$lists->_id,'true','City_State_Country_Signup']) }}?href=admin/country_list" data-placement="top" title="Make Active" onclick ="return confirm('Are you sure? You want to activate this country for signup!');">
							<span class="badge">Inactive</span>
						</a>
					@endif
				</td>
				<td class="text-center" width="190px;">


                	<div style="position:relative;" class="visible-ipad">
                            <button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Action
                        <span class="caret"></span>
                        </button>
                     <ul class="dropdown-menu action-menu">
                       <li>
                	@if ($lists->Status == 'Active')
						<a href="{{ url('status_activity',[$lists->_id,'Inactive','City_State_Country']) }}?href=admin/country_list" data-placement="top" title="Make Inactive" class="btn btn-small btn-success btn-action">
							<i class="fa fa-lock"></i>&nbsp;Inactive</a>

					@else
						<a href="{{ url('status_activity',[$lists->_id,'Active','City_State_Country']) }}?href=admin/country_list" data-placement="top" title="Make Active" class="btn btn-small btn-warning btn-action">
							<i class="fa fa-unlock"></i>&nbsp;Active</a>
					@endif
                    </li>
                    <li>
					<button type="button" class="btn btn-primary" data-toggle="modal" title="Edit" data-target="#exampleModal{{$lists->_id}}" data-whatever="@mdo"><i class="fa fa-edit"></i>&nbsp;Edit</button>
						</li>
                        <li>
					<a data-placement="top" title="Delete Record" class="btn btn-small btn-danger btn-action" onclick="remove_record('delete_activity/{{$lists->_id}}/country_list','{{$lists->_id}}')" href="javascript:void(0)"><i class="fa fa-trash"></i>&nbsp;Delete</a>
                    </li>

                </ul>
                </div>

					@if ($lists->Status == 'Active')
						<a href="{{ url('status_activity',[$lists->_id,'Inactive','City_State_Country']) }}?href=admin/country_list" data-placement="top" title="Make Inactive" class="btn btn-small btn-success btn-action" onclick ="return confirm('Are you sure? You want to inactivate this country!');">
							<i class="fa fa-lock"></i></a>
					@else
						<a href="{{ url('status_activity',[$lists->_id,'Active','City_State_Country']) }}?href=admin/country_list" data-placement="top" title="Make Active" class="btn btn-small btn-warning btn-action" onclick ="return confirm('Are you sure? You want to activate this country!');">
							<i class="fa fa-unlock"></i></a>
					@endif
					<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal{{$lists->_id}}" data-whatever="@mdo"><i class="fa fa-edit"></i> </button>
					@if($lists->state_available == true)
                    	<a title="State List" class="btn btn-small btn-success btn-action" href="{{ url('admin/state_list?id='.$lists->_id) }}"><i class="fa fa-list"></i></a>
					@else
						<a title="State List" class="btn btn-small btn-success btn-action" href="{{ url('admin/city_list?id='.$lists->_id) }}"><i class="fa fa-list"></i></a>
                    @endif
					<a data-placement="top" title="Delete Record" class="btn btn-small btn-danger btn-action" onclick="remove_record('delete_activity/{{$lists->_id}}/country_list','{{$lists->_id}}')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>
				</td>
			</tr>

			@endforeach
		<?php } else {?>
			<tr>
				<td colspan="5" class="text-center" >Country not found</td>
			</tr>
		<?php }?>
	</tbody>
</table>
@foreach ($country as $lists)

<div class="modal fade" id="exampleModal{{$lists->_id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title" id="exampleModalLabel">Edit Country</h4>
	  </div>

	  {!! Form::model('', ['name' => 'countryForm1', 'id' => $lists->_id, 'method' => 'POST', 'url' => ['admin/update_country', $lists->_id]]) !!}

	  <div class="modal-body clearfix">
			<div class="form-group" id="cityDiv1">
				{!! Form::label('Country', 'Country:',['class' => 'control-label']) !!}
				{!! Form::text('Content', $lists->Content, ['class'=>'form-control', 'placeholder'=> 'Country','id' =>'Content'.$lists->_id]) !!}
				<p class="help-block red" id="er_Content{{$lists->_id}}"></p>
			</div>
			<div class="row">
			<div class="col-sm-6">
	       <div class="form-group" id="cityDiv1">
	           {!! Form::label('allow', 'Allow For Same Country:',['class' => 'control-label']) !!}
		       {!! Form::select('allow', [''=>'Select','allow'=>'Yes','not_allow'=>'No'], $lists->allow, ['class'=>'form-control','id' =>'allow'.$lists->_id]) !!}
		      <p class="help-block red" id="er_allow{{$lists->_id}}"></p>
		   </div>
           </div>
           <div class="col-sm-6">
	       <div class="form-group" id="cityDiv1">


	           {!! Form::label('state_available', 'Has State:',['class' => 'control-label']) !!}
		       <!-- {!! Form::select('state_available', [''=>'Select','true'=>'Yes','false'=>'No'], $lists->state_available1, ['class'=>'form-control','id' =>'state_available'.$lists->_id]) !!}
 -->
		       <select name="state_available" class="form-control" id="state_available{{$lists->_id}}">
		       	<option value="">Select</option>
		       	<option value="true" <?php if ($lists->state_available == true) {echo 'selected';}?>>Yes</option>
		       	<option value="false" <?php if ($lists->state_available == false) {echo 'selected';}?>>No</option>
		       </select>


		      <p class="help-block red" id="er_state_available{{$lists->_id}}"></p>
		   </div>
           </div>
           </div>
		   <div class="row">
		   <div class="col-sm-6">
          <div class="form-group" id="cityDiv1">
				{!! Form::label('Currency Code', 'Currency Code:',['class' => 'control-label']) !!}
				{!! Form::text('Currency_Code', $lists->CurrencyCode, ['class'=>'form-control', 'placeholder'=> 'Country Code','maxlength' => 7 ,'id' =>'CurrencyCode'.$lists->_id]) !!}
				<p class="help-block red" id="er_CurrencyCode{{$lists->_id}}"></p>
			</div>
			</div>
			<div class="col-sm-6">
             <div class="form-group" id="cityDiv1">
				{!! Form::label('Currency Rate', 'Currency Rate:',['class' => 'control-label']) !!}
				{!! Form::text('Currency_Rate', $lists->CurrencyRate, ['class'=>'form-control', 'placeholder'=> 'Country Rate', 'maxlength' => 7 ,'id' =>'CurrencyRate'.$lists->_id]) !!}
				<p class="help-block red" id="er_CurrencyRate{{$lists->_id}}"></p>
			</div>
			</div>
			</div>
			<div class="row">
		   <div class="col-sm-6">
			 <div class="form-group" id="cityDiv1">
				{!! Form::label('Currency Symbol', 'Currency Symbol:',['class' => 'control-label']) !!}
				{!! Form::text('Currency_Symbol', $lists->CurrencySymbol, ['class'=>'form-control', 'placeholder'=> 'Country Symbol', 'maxlength' => 7 ,'id' =>'CurrencySymbol'.$lists->_id]) !!}
				<p class="help-block red" id="er_CurrencySymbol{{$lists->_id}}"></p>
			</div>
			</div>
			<div class="col-sm-6">
			 <div class="form-group" id="cityDiv1">
				{!! Form::label('Formated Text', 'Formated Text:',['class' => 'control-label']) !!}
				{!! Form::text('Formated_Text', $lists->FormatedText, ['class'=>'form-control', 'placeholder'=> 'Formated Text','id' =>'FormatedText'.$lists->_id]) !!}
				<p class="help-block red" id="er_FormatedText{{$lists->_id}}"></p>
			</div>
            </div>
            </div>
		   <div class="">
						<div class="col-md-6">
						<div class="form-group" id="cityDiv">
						<div class="form-group ">
                           {!! Form::label('Status', 'Status'),':' !!}  </br>
                           <?php if ($lists->Status == 'Active') {?>
                           <input type="radio" name="status" value="Active" checked> Active &nbsp;
                           <input type="radio" name="status" value="Inactive"> Inactive
                           <?php } else {?>
                           <input type="radio" name="status" value="Active" > Active &nbsp;
                           <input type="radio" name="status" value="Inactive" checked> Inactive
                           <?php }?>
                        </div>
                        <div class="clearfix"></div>

						</div>
						</div>
						<div class="col-md-6">
						<div class="form-group" id="cityDiv">
						<div class="form-group ">
                           {!! Form::label('Sinup_Status', 'Signup Status'),':' !!}  </br>
                           <?php if ($lists->Signup == true) {?>
                           <input type="radio" name="sinup_status" value="active" checked> Active &nbsp;
                           <input type="radio" name="sinup_status" value="inactive"> Inactive
                           <?php } else {?>
                           <input type="radio" name="sinup_status" value="active"> Active &nbsp;
                           <input type="radio" name="sinup_status" value="inactive" checked> Inactive
                           <?php }?>
                        </div>
                        <div class="clearfix"></div>

						</div>
				      </div>
						</div>
						</div>

	  <div class="modal-footer">
		{!! Form::button('Close', ['class' => 'btn btn-primary', 'data-dismiss'=>'modal']) !!}
		{!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
	  </div>
		<div class="clearfix"></div>
		{!! Form::close() !!}
	   </div>

  </div>
</div>

<script>
$("#{{$lists->_id}}").submit(function()
{
	var flag = true;

	if(valid.required('Content{{$lists->_id}}','country name') == false) { flag = false; }
	if(valid.required('CurrencyCode{{$lists->_id}}','currency code') == false) { flag = false; }

	if(valid.required('CurrencyRate{{$lists->_id}}','currency rate') == false) { flag = false; }
	else
    {
       if(valid.maxlength('CurrencyRate{{$lists->_id}}','currency rate',5) == false)
        {  flag = false; }
       else
       {
       	  if(valid.float('CurrencyRate{{$lists->_id}}','currency rate') == false)
        {  flag = false; }
       }
	}
	if(valid.required('CurrencySymbol{{$lists->_id}}','currency symbol') == false) { flag = false; }
	else
    {
       if(valid.maxlength('CurrencySymbol{{$lists->_id}}','currency symbol',5) == false)
        {  flag = false; }
	}
	if(valid.required('FormatedText{{$lists->_id}}','formated text') == false) { flag = false; }
	if(valid.required('allow{{$lists->_id}}','allow shipping with in same country') == false) { flag = false; }
	if(valid.required('state_available{{$lists->_id}}','has state') == false) { flag = false; }



	return flag;
});
</script>

@endforeach

@extends('Admin::list.pagination.footer')
