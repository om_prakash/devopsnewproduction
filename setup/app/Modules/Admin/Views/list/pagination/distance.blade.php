
<table class="table table-bordered table-striped table-highlight table-list">
<thead>
	<tr>
		<th style="width:40px;">S.No.</th>
		<th>Country</th>
		<th style="width:120px;" >From (miles)</th>
		<th style="width:120px;">To (miles)</th>
		<th style="width:100px;">Price</th>
		<th class="text-center"style="width:100px;">Status</th>
		<th class="text-center"style="width:120px;">Action</th>
	</tr>
	</thead>
	<tbody>
		<?php if (count($distance) > 0) {?>
			@foreach ($distance as $lists)
			<tr id="row-{{$lists->_id}}">
				<td scope="row">{{$sno++}}</td>
				<td>
				 @if($lists->country_id == '58231521cf32073c2b710d67')
				   UK
				 @elseif($lists->country_id == '56bda94bcf3207510bee89c9')
				   USA
				 @elseif($lists->country_id == '56bdbfb4cf32079714ee89ca')
				  Ghana
				@elseif($lists->country_id == '582314e7cf32074f2c710d67')
				   Canada
				@else
				   {{ $lists->country_id }}
				@endif

				</td>
				<td>{{$lists->from}}</td>
				<td>{{$lists->to}}</td>
				<td>${{number_format($lists->price,2)}}</td>
				<td class="text-center">
				    @if ($lists->Status == 'Active')
		            <span class="badge bg-green">Active</span>
		            @else
		            <span class="badge bg-yellow">Inactive</span>
		            @endif
				</td>
				<td class="text-center">
                	<div style="position:relative;" class="visible-ipad">
                            <button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Action
                        <span class="caret"></span>
                        </button>
                </div>

					@if ($lists->Status == 'Active')
						<a href="{{ url('status_activity',[$lists->_id,'Inactive','distance']) }}?href=admin/distance" data-placement="top" title="Make Inactive" class="btn btn-small btn-warning btn-action" onclick ="return confirm('Are you sure? You want to inactivate this item!');">
							<i class="fa fa-lock"></i></a>
					@else
						<a href="{{ url('status_activity',[$lists->_id,'Active','distance']) }}?href=admin/distance" data-placement="top" title="Make Active" class="btn btn-small btn-success btn-action" onclick ="return confirm('Are you sure? You want to activate this item');">
							<i class="fa fa-unlock"></i></a>
					@endif

					<a data-placement="top" title="Delete Record" class="btn btn-small btn-danger btn-action" onclick="remove_record('delete_activity/{{$lists->_id}}/distance','{{$lists->_id}}')" href="javascript:void(0)" onclick ="return confirm('Are you sure? You want to delete this category!')"><i class="fa fa-trash"></i></a>

						<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal{{$lists->_id}}" data-whatever="@mdo"><i class="fa fa-edit"></i></button>
				</td>
			</tr>

			@endforeach
		<?php } else {?>
			<tr>
				<td colspan="7" class="text-center" >Distance not found</td>
			</tr>
		<?php }?>
	</tbody>
</table>

@foreach ($distance as $lists)
<div class="modal fade" id="exampleModal{{$lists->_id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title" id="exampleModalLabel">Edit Distance</h4>
	  </div>

	  {!! Form::model('', ['name' => 'categoryForm1', 'id' => $lists->_id, 'method' => 'POST', 'url' => ['admin/update-distance', $lists->_id]]) !!}

	  <div class="modal-body">

	  		<div class="col-md-6">
				<div class="form-group" id="cityDiv">
					{!! Form::label('Country', 'Country',['class' => 'control-label']) !!}
					<select class="form-control" name="country" id="country" >
					   <option value="">Select Country</option>
					   @foreach($country as $value)
					   <option value="{{ $value->_id }}" @if($value->_id == $lists->country_id ) selected @endif>{{ ucfirst($value->Content) }}</option>
					   @endforeach
					</select>
					<p class="help-block red" id="er_country{{$lists->_id}}" style="color:#a94442"></p>
				</div>
			</div>


		 <div class="col-md-6">
                      <div class="form-group">
                            <label class="control-label">Price</label>
                            <div class="input-group error-input">
                            <span class="input-group-addon">$</span>
                            <input class="form-control required" value="{{ $lists->price }}" placeholder="Price" name="price"  >
                            <p class="help-block red" id="er_price" style="color:#ab4442"></p>
                            </div>
                      </div>
                  </div>


			<div class="clearfix"></div>
		<div class="col-md-6">
			<div class="form-group" id="cityDiv">
				{!! Form::label('From (miles)', 'From (miles)',['class' => 'control-label']) !!}
				{!! Form::text('from', $lists->from, ['class'=>'form-control', 'placeholder'=> 'From','id' =>'from'.$lists->_id]) !!}
				<p class="help-block red" id="er_from{{$lists->_id}}" style="color:#a94442"></p>
			</div>
		</div>

		<div class="col-md-6">
			<div class="form-group" id="cityDiv">
				{!! Form::label('To (miles)', 'To (miles)',['class' => 'control-label']) !!}
				{!! Form::text('to', $lists->to, ['class'=>'form-control', 'placeholder'=> 'Price','id' =>'to'.$lists->_id]) !!}
				<p class="help-block red" id="er_to{{$lists->_id}}" style="color:#a94442"></p>
			</div>
		</div>




		<div class="clearfix"></div>
		</div>

	  <div class="modal-footer">
		{!! Form::button('Close', ['class' => 'btn btn-primary', 'data-dismiss'=>'modal']) !!}
		{!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
	  </div>

		{!! Form::close() !!}
	<div class="clearfix"></div>
	</div>
  </div>
</div>

<script>
$("#{{$lists->_id}}").submit(function()
{
	var flag = true;

	if(valid.required('price{{$lists->_id}}','price') == false) { flag = false; }
	if(valid.required('to{{$lists->_id}}','to') == false) { flag = false; }
	if(valid.required('from{{$lists->_id}}','from') == false) { flag = false; }
	if(valid.required('country{{$lists->_id}}','country') == false) { flag = false; }

	return flag;
});
</script>
@endforeach

@extends('Admin::list.pagination.footer')
