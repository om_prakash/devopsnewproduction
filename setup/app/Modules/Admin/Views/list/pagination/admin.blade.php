<table class="table table-bordered table-striped table-highlight table-list">
<thead>
	<tr>
		<th width="1%">S.No.</th>
		<th>User name</th>
		<th>Email</th>
		<th>Status</th>
		<th width="12%">Action</th>
	</tr>
	</thead>
	<tbody>
		<?php if (count($user) > 0) {?>
			@foreach ($user as $lists)
			<tr id="row-{{$lists->_id}}">
				<td scope="row">{{$sno++}}</td>
				<td>{{ucfirst($lists->UserName)}}</td>
				<td>{{$lists->UserEmail}}</td>
				<td class="text-center">
					@if ($lists->UserStatus == 'Active')
					<span class="badge bg-green">Active</span>
					@else
					<span class="badge bg-yellow">Inactive</span>
					@endif
				</td>

				<td class="width-action-20">
					<div style="position:relative;" class="visible-ipad">
						<button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Action<span class="caret"></span>
                        </button>
                  </div>

					@if ($lists->UserStatus == 'Active')
						<a href="{{ url('status_activity',[$lists->_id,'Inactive','admin_user']) }}?href=admin/admin_user" data-placement="top" title="Make Inactive" class="btn btn-small btn-success btn-action" onclick ="return confirm('Are you sure? You want to inactivate admin!');">
							<i class="fa fa-lock"></i></a>
					@else
						<a href="{{ url('status_activity',[$lists->_id,'Active','admin_user']) }}?href=admin/admin_user" data-placement="top" title="Make Active" class="btn btn-small btn-warning btn-action" onclick ="return confirm('Are you sure? You want to activate admin!');">
							<i class="fa fa-unlock"></i></a>
					@endif

					<a href="{{ url('admin/edit_admin_user/'.$lists->_id)}}" data-placement="top" title="Edit" class="btn btn-small btn-primary btn-action">
						<i class="fa fa-pencil-square-o"></i></a>
					<a data-placement="top" title="Delete Record" class="btn btn-small btn-danger btn-action" onclick="remove_record('delete_activity/{{$lists->_id}}/admin_user','{{$lists->_id}}')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>
				</td>
			</tr>
			@endforeach
		<?php } else {?>
			<tr>
				<td colspan="5" class="text-center" >User not found yet</td>
			</tr>
		<?php }?>
	</tbody>
</table>
@extends('Admin::list.pagination.footer')
