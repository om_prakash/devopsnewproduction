@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')


@section('content')

<div class="container-fluid">
<div class="panel panel-default">
  <div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;<b>{{$title}}</b></div>
  <div class="panel-body">
		{!! Form::model('', ['method' => 'PATCH', 'url' => ['admin/country_list']]) !!}
        	<div class="inline-form">
				<div class="form-group">
					{!! Form::label('Content', 'Country Name', ['class'=>'control-lable']) !!}
						{!! Form::text('search_value', Input::get('search_value'), ['class'=>'form-control', 'placeholder'=> 'Country Name','id' =>'inputError1']) !!}
					</div>
            </div>
            <div class="inline-form">
                <div class="form-group">
					<label></label>
					{!! Form::submit('Search', ['class' => 'btn btn-primary']) !!}

					<a href="{{URL::to('admin/country_list')}}" class="btn btn-primary">
						<span aria-hidden="true" class="glyphicon glyphicon-refresh">

						</span>&nbsp;&nbsp;Reset
                    </a>
					<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Add Country</button>
				</div>
			</div>


		{!! Form::close() !!}
	<div class="clearfix"></div>

      <!-- Pagination Section-->
	<input type="hidden" name="urlvalue" id="urlvalue" value="{{$paginationurl}}" />
	<input type="hidden" name="postvalue" id="postvalue" value="{{$postvalue}}" />
	<input type="hidden" name="orderby" id="orderby" value="{{$orderby}}" />
	<input type="hidden" name="orderType" id="orderType" value="{{$orderType}}" />
	<div id="containerdata"></div>
	<div class="clearfix"></div>


  </div> <!-- Panel Body -->
</div>

  </div> <!-- Panel Body -->
</div>

			<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
			  <div class="modal-dialog" role="document">
				<div class="modal-content">
				  <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="exampleModalLabel">Add Country</h4>
				  </div>
				  <div class="col-sm-12 ">
				  {!! Form::model('', ['name' => 'countryForm', 'id' => 'countryForm', 'method' => 'POST', 'url' => ['admin/add_country']]) !!}
				  <div class="modal-body clearfix">

						<div class="form-group" id="cityDiv">
							{!! Form::label('Country', 'Country Name:',['class' => 'control-label']) !!}
							{!! Form::text('Content', '', ['class'=>'form-control', 'placeholder'=> 'Country Name','id' =>'Content']) !!}
							<p class="help-block red" id="er_Content"</p>
						</div>

						<div class="row">
						<div class="col-sm-6">
						<div class="form-group" id="cityDiv">
							{!! Form::label('allow', 'Allow for same country:',['class' => 'control-label']) !!}
							 {!! Form::select('allow', [''=>'Select','allow'=>'Yes','not_allow'=>'No'], '', ['class'=>'form-control','id' =>'Allow']) !!}
							<p class="help-block red" id="er_Allow"</p>
						</div>
						</div>
						<div class="col-sm-6">
						<div class="form-group" id="cityDiv">
							{!! Form::label('state_available', 'Has State:',['class' => 'control-label']) !!}
		       {!! Form::select('state_available', [''=>'Select','true'=>'Yes','false'=>'No'], '', ['class'=>'form-control','id' =>'state_available']) !!}
		      <p class="help-block red" id="er_state_available"></p>
						</div>
						</div>
						</div>

						<div class="row">
						<div class="col-sm-6">
						<div class="form-group" id="cityDiv">
							{!! Form::label('Currency Code', 'Currency Code:',['class' => 'control-label']) !!}
							{!! Form::text('Currency_Code', '', ['class'=>'form-control', 'placeholder'=> 'Currency Code', 'maxlength' => 7 , 'id' =>'CurrencyCode']) !!}
							<p class="help-block red" id="er_CurrencyCode"</p>
						</div>
						 </div>
						 <div class="col-sm-6">
						<div class="form-group" id="cityDiv">
							{!! Form::label('Currency Rate', 'Currency Rate:',['class' => 'control-label']) !!}
							{!! Form::text('Currency_Rate', '', ['class'=>'form-control', 'placeholder'=> 'Currency Rate', 'maxlength' => 7 ,'id' =>'CurrencyRate']) !!}
							<p class="help-block red" id="er_CurrencyRate"</p>
						</div>
						</div>
						</div>
						<div class="row">
						<div class="col-sm-6">
						<div class="form-group" id="cityDiv">
							{!! Form::label('Currency Symbol', 'Currency Symbol:',['class' => 'control-label']) !!}
							{!! Form::text('Currency_Symbol', '', ['class'=>'form-control', 'placeholder'=> 'Currency Symbol', 'maxlength' => 7  ,'id' =>'CurrencySymbol']) !!}
							<p class="help-block red" id="er_CurrencySymbol"</p>
						</div>
						</div>
						<div class="col-sm-6">
						<div class="form-group" id="cityDiv">
							{!! Form::label('Formated Text', 'Formated Text:',['class' => 'control-label']) !!}
							{!! Form::text('Formated_Text', '', ['class'=>'form-control', 'placeholder'=> 'Formated Text','id' =>'FormatedText']) !!}
							<p class="help-block red" id="er_FormatedText"</p>
						</div>
                         </div>
                         </div>
						<div class="">
						<div class="col-md-6 row">
						<div class="form-group" id="cityDiv">
						<div class="form-group ">
                           {!! Form::label('Status', 'Status'),':' !!}  </br>
                           <input type="radio" name="status" value="Active" checked> Active &nbsp;
                           <input type="radio" name="status" value="Inactive"> Inactive
                        </div>
                        <div class="clearfix"></div>

						</div>
						</div>
						<div class="col-md-6">
						<div class="form-group" id="cityDiv">
						<div class="form-group ">
                           {!! Form::label('Sinup_Status', 'Singup Status'),':' !!}  </br>
                           <input type="radio" name="sinup_status" value="active" checked> Active &nbsp;
                           <input type="radio" name="sinup_status" value="inactive"> Inactive
                        </div>
                        <div class="clearfix"></div>

						</div>
				      </div>
						</div>


				  </div>
				  <div class="modal-footer">
					{!! Form::button('Close', ['class' => 'btn btn-primary', 'data-dismiss'=>'modal']) !!}
					{!! Form::submit('Add', ['class' => 'btn btn-primary']) !!}
				  </div>

				{!! Form::close() !!}
				</div>
				<div class="clearfix"></div>
				</div>
			  </div>
			</div>
			</div>

{!! Html::script('theme/admin/custome/js/validation.js') !!}


<script>

$("#countryForm").submit(function()
{
	var flag = true;

	if(valid.required('Content','country name') == false) { flag = false; }
	if(valid.required('CurrencyCode','currency code') == false) { flag = false; }

	if(valid.required('CurrencyRate','currency rate') == false) { flag = false; }
	else
    {
       if(valid.maxlength('CurrencyRate','currency rate',5) == false)
        {  flag = false; }
       else
       {
       	  if(valid.float('CurrencyRate','currency rate') == false)
        {  flag = false; }
       }
	}
	if(valid.required('CurrencySymbol','currency symbol') == false) { flag = false; }
	else
    {
       if(valid.maxlength('CurrencySymbol','currency symbol',5) == false)
        {  flag = false; }
	}
	if(valid.required('FormatedText','formated text') == false) { flag = false; }
	if(valid.required('Allow','allow shipping with in same country') == false) { flag = false; }
	return flag;
});
</script>
@include('Admin::layouts.footer')
@stop
