@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')

@section('content')

<div class="container-fluid">
	<div class="panel panel-default">
	  	<div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;<b>Client</b></div>
	  	<div class="panel-body">
        	<div class="col-md-12 row">

      {!! Form::model('', ['method' => 'GET', 'url' => ['admin/client']]) !!}

                <div class="inline-form">
                <div class="form-group">
          {!! Form::label('Search Name', 'Search Name', ['class'=>'control-lable']) !!}
          <div>
            <input type="text" name="search_value" placeholder="Name" class="form-control" >

          </div>
                </div>
                </div>
                <div class="inline-form">
                <div class="form-group">
          {!! Form::label('', '', ['class'=>'control-lable']) !!}
          {!! Form::submit('Search', ['class' => 'btn btn-primary']) !!}

          <a href="{{URL::to('admin/client')}}" class="btn btn-primary">
            <span aria-hidden="true" class="glyphicon glyphicon-refresh">
            </span>&nbsp;&nbsp;Reset
                    </a>

                     <a href="{{URL::to('admin/add-client')}}" class="btn btn-primary"><i class="fa fa-plus"></i>&nbsp;&nbsp;Add Client
                    </a>

                    </div>
        </div>

    {!! Form::close() !!}
         </div>
		<div class="clearfix"></div>
	      <!-- Pagination Section-->
		<input type="hidden" name="urlvalue" id="urlvalue" value="{{$paginationurl}}" />
		<input type="hidden" name="postvalue" id="postvalue" value="{{$postvalue}}" />
		<input type="hidden" name="orderby" id="orderby" value="{{$orderby}}" />
		<input type="hidden" name="orderType" id="orderType" value="{{$orderType}}" />
		<div id="containerdata"></div>
		<div class="clearfix"></div>
	  	</div> <!-- Panel Body -->
	</div>
</div>
{!! Html::script('theme/admin/custome/js/utility.js') !!}
@include('Admin::layouts.footer')

@stop
