@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')

@section('content')
<div class="container-fluid">
	<div class="panel panel-default">
		<div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;<b>{{$title}}</b></div>
		<div class="panel-body">
				{!! Form::model('', ['method' => 'PATCH', 'url' => ['admin/item']]) !!}
					<div class="inline-form">
						<div class="form-group">
							{!! Form::label('Item', 'Item Name', ['class'=>'control-lable']) !!}
							{!! Form::text('search_value', Input::get('search_value'), ['class'=>'form-control', 'placeholder'=> 'Item Name','id' =>'inputError1']) !!}
						</div>
					</div>

					<div class="inline-form">
						<div class="form-group">
							<label></label>
							{!! Form::submit('Search', ['class' => 'btn btn-primary']) !!}

							<a href="{{URL::to('admin/item')}}" class="btn btn-primary">
								<span aria-hidden="true" class="glyphicon glyphicon-refresh">
								</span>&nbsp;&nbsp;Reset
							</a>

							<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo"><i class="fa fa-plus"></i>&nbsp;&nbsp;Add Item</button>
						</div>
					</div>
				{!! Form::close() !!}
			<div class="clearfix"></div>

			<!-- Pagination Section-->
			<input type="hidden" name="urlvalue" id="urlvalue" value="{{$paginationurl}}" />
			<input type="hidden" name="postvalue" id="postvalue" value="{{$postvalue}}" />
			<input type="hidden" name="orderby" id="orderby" value="{{$orderby}}" />
			<input type="hidden" name="orderType" id="orderType" value="{{$orderType}}" />
			<div id="containerdata"></div>
			<div class="clearfix"></div>
		</div> <!-- Panel Body -->
	</div>

</div> <!-- Panel Body -->
</div>


	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="exampleModalLabel">Add Item</h4>
				</div>

				{!! Form::model('', ['name' => 'itemForm', 'id' => 'itemForm', 'method' => 'POST', 'url' => ['admin/add-item'], 'files' => true]) !!}
				<div class="modal-body">
				<div class="col-md-12">
					<div class="form-group" id="cityDiv">
						{!! Form::label('Item name', 'Item name:',['class' => 'control-label']) !!}
						{!! Form::text('item_name', '', ['class'=>'form-control', 'placeholder'=> 'Item Name','id' =>'item_name','maxlength'=>'60']) !!}
						<p class="help-block red" id="er_item_name" style="color:#a94442"></p>
					</div>
				</div>
				<div class ="clearfix"></div>

				<div class="col-md-6">
					<div class="form-group" id="cityDiv">
						{!! Form::label('weight', 'Weight:',['class' => 'control-label']) !!}
						{!! Form::text('weight', '', ['class'=>'form-control', 'placeholder'=> 'Weight','id' =>'weight']) !!}
						<p class="help-block red" id="er_weight" style="color:#a94442"></p>
					</div>
				</div>

				<div class="col-md-6">
					<div class="form-group" id="cityDiv">
						{!! Form::label('weightunit', 'Weight unit:',['class' => 'control-label']) !!}
						{!! Form::select('weightunit', [''=>'Select unit','kg'=>'Kg','lbs'=>'Lbs'], '', ['class'=>'form-control','id' =>'weightunit',"onchange"=>"return toggle_category()"]) !!}
						<p class="help-block red" id="er_weightunit" style="color:#a94442"></p>
					</div>
				</div>
				<div class ="clearfix"></div>

				<div class="col-md-6">
					<div class="form-group" id="cityDiv">
						{!! Form::label('Price', 'Price:',['class' => 'control-label']) !!}
						{!! Form::text('price', '', ['class'=>'form-control', 'placeholder'=> 'Price','id' =>'price']) !!}
						<p class="help-block red" id="er_price" style="color:#a94442"></p>
					</div>
				</div>

				<!--<div class="col-md-6">
					<div class="form-group" id="cityDiv">
						{!! Form::label('Image', 'Image:',['class' => 'control-label']) !!}
						<input type="file" name="image" id="image" >
						<p class="help-block red" id="er_price" style="color:#a94442"></p>
					</div>
				</div>-->
				<div class ="clearfix"></div>

				</div>

				<div class="modal-footer">
				{!! Form::button('Close', ['class' => 'btn btn-primary', 'data-dismiss'=>'modal']) !!}
				{!! Form::submit('Add', ['class' => 'btn btn-primary']) !!}
				</div>
			{!! Form::close() !!}
			<div class="clearfix"></div>
			</div>
		</div>
	</div>
	</div>

{!! Html::script('theme/admin/custome/js/validation.js') !!}


<script>

function toggle_category() {
	if($('#travelmode').val() == 'air'){
		$('#ChargeType').val('fixed');
	}else {
		$('#ChargeType').val('distance');
	}
}

$("#itemForm").submit(function()
{
	var flag = true;

	if(valid.required('item_name','item name') == false) { flag = false; }
	if(valid.required('weight','weight') == false) { flag = false; }
	if(valid.required('weightunit','weight unit') == false) { flag = false; }
	if(valid.required('price','price') == false) { flag = false; }

	return flag;
});

</script>
@include('Admin::layouts.footer')
@stop
