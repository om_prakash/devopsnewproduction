@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')


@section('content')
<style>
.truncate {
  max-width: 100%;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
}
.parsley-errors-list{
	color:red;
	padding-left: 10px;
}
</style>
{!! Html::script('theme/admin/custome/css/parsley.css') !!}
{!! Html::script('theme/admin/fancybox/source/jquery.fancybox.js') !!}
 {!! Html::style('theme/admin/fancybox/source/jquery.fancybox.css') !!}

<div class="container-fluid">
<div class="panel panel-default">
  <div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;<b>Users Feedback</b>
  </div>
  <div class="panel-body">

		   {!! Form::open(array('url' =>'admin/complains','method' => 'GET','class' => '','id' => 'search_form_id','role' => "search")) !!}
        		<div class="inline-form">
				<div class="form-group">
					{!! Form::label('Search Email', '', ['class'=>'control-lable']) !!}
					<div>
						{!! Form::text('search_value', Input::get('search_value'), ['class'=>'form-control', 'placeholder'=> 'Search by Email','id' =>'inputError1']) !!}
					</div>
                </div>
				</div>
                <div class="inline-form">
                <div class="form-group">
					{!! Form::submit('Search', ['class' => 'btn btn-primary']) !!}
					<a href="{{url('admin/complains')}}" class="btn btn-primary">
						<span aria-hidden="true" class="glyphicon glyphicon-refresh">

						</span>&nbsp;&nbsp;Reset
                    </a>
				</div>
                </div>
		{!! Form::close() !!}
	<div class="clearfix"></div>


    <table class="table table-bordered table-striped table-highlight table-list">
<thead>

	<tr>
		<th>S.No.</th>
		<th>Email</th>
		<th>Request Type</th>
		<th>Description</th>
		<th>Image</th>
	 	<th>Action</th>
	</tr>
	</thead>
			<?php
if (Input::get('page') != '') {
    $i = 10 * (Input::get('page') - 1) + 1;
} else {
    $i = 1;
}
?>
	<tbody id="sortable">
		<?php if (count($result) > 0) {
    ?>
			@foreach ($result as $lists)
			<tr id="row-{{$lists->id}}" class="item" >
				<td scope="row" id="seq-row-{{$lists->id}}"><?php echo $i++; ?></td>
				<td >{{$lists->email}}</td>
				<td >{{$lists->request_type}}</td>
				<td style="max-width: 500px;">
					<p class="truncate"> {{$lists->description}}</p></td>

				<td>


					@if($lists->attachments != '')

          <?php $path_parts = pathinfo("https://aquantuo.com/aquantuoapi/uploads/support/" . $lists->attachments);
    $path_parts['extension'];

    ?>
            @if($path_parts['extension'] == "jpg" || $path_parts['extension']=="jpeg" || $path_parts['extension'] == "png")
            <a class="fancybox" href="https://aquantuo.com/aquantuoapi/uploads/support/{{$lists->attachments}}" data-fancybox-group="gallery">

                <img src="https://aquantuo.com/aquantuoapi/uploads/support/{{$lists->attachments}}" width="75px" height="75px" >

            </a>
            @elseif($path_parts['extension'] == "pdf" || $path_parts['extension']=="docx" || $path_parts['extension'] == "xml" || $path_parts['extension'] == "wpd")
                <a target="_blank" href="https://aquantuo.com/aquantuoapi/uploads/support/{{$lists->attachments}}" width="75px" height="75px" > <img src="https://aquantuo.com/upload/pdf.png" width="75px" height="75px"></a>
            @else
             <img src="https://aquantuo.com/upload/no-image.jpg" width="75px" height="75px" />
            @endif
					@else
					   <img src="https://aquantuo.com/upload/no-image.jpg" width="75px" height="75px" >
					@endif
				</td>
				 <td class="width-action-20">
                <div style="position:relative;" class="visible-ipad">
                            <button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Action
                        <span class="caret"></span>
                        </button>
                  </div>


				<span data-toggle="modal" data-target="#myModal{{$lists->id}}">
                    <a href="#" data-toggle="tooltip" data-placement="bottom" class="btn btn-info btn-sm tooltip-bottom" title="Email"  style="height:28px;" onclick="this.form.reset();"><i class="fa fa-envelope">
                       </i></a>
                </span>

				</td>
			</tr>
			@endforeach
		<?php } else {?>
			<tr>
				<td colspan="7" class="text-center" >Recored not found</td>
			</tr>
		<?php }?>
	</tbody>
</table>
{!! $result->render() !!}

	<div class="clearfix"></div>


  </div> <!-- Panel Body -->
</div>
</div>

  @foreach ($result as $lists)
 <!-- Modal  Send Mail-->
  <div class="modal fade" id="myModal{{$lists->id}}" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">

          <h4 class="modal-title">Send Mail</h4>
        </div>
        <div class="modal-body">
           {!! Form::model('', ['id' => 'emailsend'.$lists->id,'method' => 'post', 'url'=> ['admin/send-email'],'files' => true, 'data-parsley-validate']) !!}

            <div class="col-md-12">
              <div class="form-group">
                     <input type="email", name="email" readonly class="form-control" value="{{$lists->email}}">
                     @if($errors->has('email'))
                     <p class="controll-error help-block"><i class="fa fa-times-circle-o"></i>&nbsp;
                        {{ $errors->first('email') }}
                     </p>
                     @endif
                  </div>
                  </div>
                  <br/>


                  <div class="col-md-12">
                    <div class="from-group">
                    <br/>
                       {!! Form::textarea('message','', ['class'=>'form-control editor', 'rows'=>'4', 'cols' =>'5',   'placeholder'=> 'Message','required', 'data-parsley-required-message' => 'Message is required.',]) !!}
                     @if($errors->has('message'))
                     <p class="controll-error help-block"><i class="fa fa-times-circle-o"></i>&nbsp;
                        {{ $errors->first('message') }}
                     </p>
                     @endif
                  </div>
                  </div>

                  <div class="clearfix"></div>
            <br>
        <div class="col-sm-12">
           <button type="submit" class="btn btn-primary" >Send</button>
        <button type="submit" class="btn btn-danger reset" data-dismiss="modal" onclick="this.form.reset();reserParsley(this.form.id);">Close</button>
        </div>
        <div class="clearfix"></div>
             {!! Form::close() !!}
        </div>
      </div>

    </div>
  </div>
  @endforeach
<script type="text/javascript">
  $(document).ready(function() {
    $(".fancybox").fancybox();
  })
</script>

@include('Admin::layouts.footer')

@stop
