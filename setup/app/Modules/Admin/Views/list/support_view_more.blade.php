@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')

@section('content')


<div class="container-fluid">
<div class="panel panel-default">
  <div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;<b>{{$title}}</b></div>
 <div class="panel-body">		
		{!! Form::model('', ['method' => 'PATCH', 'url' => ['admin/support']]) !!}

		{!! Form::close() !!}		
	<div class="clearfix"></div>

  	  
      <!-- Pagination Section-->
	<input type="hidden" name="urlvalue" id="urlvalue" value="{{$paginationurl}}" />
	<input type="hidden" name="postvalue" id="postvalue" value="{{$postvalue}}" />
	<input type="hidden" name="orderby" id="orderby" value="{{$orderby}}" />
	<input type="hidden" name="orderType" id="orderType" value="{{$orderType}}" />
	<div class="support_design">
   <div class="supportHeading col-md-12">
      <span>Ticket No: {{$support[0]['Token']}}</span> 
      <div class="pull-right">
         <a href="#replyDiv" class="btn btn-primary">Go to Reply</a>
      </div>
   </div>
   
   <div class="clearfix"></div>
   
   <div class="support_detail col-md-12">
      <b>Opened by </b> {{$support[0]['UserName']}} on
      	{{date('d F,Y ',$support[0]['EnterOn']->sec)}} at {{date('h:i A',$support[0]['EnterOn']->sec)}}
   </div>
   <div class="support_detail col-md-12">
   		<b>Query: </b> {{$support[0]['Query']}}
   </div>
   <br />
		<div id="containerdata"></div>
		<div class="clearfix"></div>
	</div>
    
		{!! Form::model('', ['name' => 'countryForm1', 'id' => 'countryForm1', 'method' => 'POST', 'url' => ['admin/update_support_view_more',$support[0]['Token']]]) !!}
		<div class="col-md-6 row">
		   <div class="form-group" id="replyDiv">
		    
		      {!! Form::textarea('ReplyQuery', '', ['class'=>'form-control', 'placeholder'=> 'Reply Query','id' =>'ReplyQuery1','size' => '30x3']) !!}
		   </div>
		    <p class="help-block red" id='er_ReplyQuery1'</p>
		   <div>	
		      {!! Form::submit('Send', ['class' => 'btn btn-primary']) !!}
		       </div>
		    </div>
		      {!! Form::close() !!}
       
  	
	</div> 
</div>
</div>


@endsection

<script>
function validateForm() {
    
    var flag = true;

        if(document.getElementById('ReplyQuery1').value == '') {
            document.getElementById('er_ReplyQuery1').innerHTML = 'The reply query field is required';
            flag = false;
        }
  return flag;
}
</script>