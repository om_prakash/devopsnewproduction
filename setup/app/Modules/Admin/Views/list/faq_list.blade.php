@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')


@section('content')

<div class="container-fluid">
<div class="panel panel-default">
  <div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;<b>{{$title}}</b>
  </div>
  <div class="panel-body">		
		{!! Form::model('', ['method' => 'PATCH', 'url' => ['admin/faq_list']]) !!}
        		<div class="inline-form">
				<div class="form-group">
					{!! Form::label('Question', 'Question Name', ['class'=>'control-lable']) !!}
					<div>                    
						{!! Form::text('search_value', Input::get('search_value'), ['class'=>'form-control', 'placeholder'=> 'Search by Question','id' =>'inputError1']) !!}
					</div>					
                </div>
				</div>
                <div class="inline-form">
                <div class="form-group">
					{!! Form::submit('Search', ['class' => 'btn btn-primary']) !!}
					<a href="{{ url('admin/add_faq') }}" data-placement="top" title="Add" class="btn btn-primary btn-action">Add Faq</a>
				</div>
                </div>

		{!! Form::close() !!}		
	<div class="clearfix"></div>

  	  
      <!-- Pagination Section-->
	<input type="hidden" name="urlvalue" id="urlvalue" value="{{$paginationurl}}" />
	<input type="hidden" name="postvalue" id="postvalue" value="{{$postvalue}}" />
	<input type="hidden" name="orderby" id="orderby" value="{{$orderby}}" />
	<input type="hidden" name="orderType" id="orderType" value="{{$orderType}}" />
	<div id="containerdata"></div>
	<div class="clearfix"></div>
	
  	
  </div> <!-- Panel Body -->
</div>
</div>


@include('Admin::layouts.footer')
@stop
