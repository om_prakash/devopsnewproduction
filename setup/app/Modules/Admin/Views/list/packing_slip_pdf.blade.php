<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Slip</title>

</head>
<body style="font-family:arial, helvetica,serif,; font-size:12px; color:#000; margin:0; padding:0;">

    <table style="width: 100%; margin: 0px auto;  padding: 0px 0px;">
        <tr>
            <td>
                <table style="width:100%; margin-bottom:10px;">
                    <tr>
                        <td style="width:50%" valign="top">
                            <div style="padding: 0; margin-top: 0px;">
                            <img src="{{url('')}}/theme/admin/custome/images/logo_receipt.png" width="240" style="margin-bottom:10px;">
                            </div>
                        </td>
                        <td width="10%">&nbsp;</td>
                        <td style="width:40%" valign="top" align="right">
                            <h3 style="font-size:54px; margin-bottom:10px; margin-top:-5px;"><b style="font-weight: 700; font-size:28px; color:#004269;">Packing Slip</b></h3>
                            <p style="margin:3px 0;">Date: {{show_date(@$info->PackingSlip)}}</p>
                        </td>
                    </tr>
                </table>
                <div style="clear:both;"></div>

                <table>
                    <tr>
                        <td  valign="top" width="550px;">
                            <p style="margin-bottom:-7px;"><span style="color:#000; font-weight:700;">Street Address:</span>&nbsp;&nbsp; {{@$info->address}} </p>
                          {{--   <p style="margin-bottom:-7px;"><span style="color:#000; font-weight:700;">City, ST ZIP:</span>&nbsp;&nbsp; 30 243 4505 </p> --}}
                          @if(@$info->address == '1685 South State Street Dover, DE 19901')
                            <p style="margin-bottom:-7px;"><span style="color:#000; font-weight:700;">Phone:</span>&nbsp;&nbsp; (+1) 888 652 2233 </p>
                          @else
                          <p style="margin-bottom:-7px;"><span style="color:#000; font-weight:700;">Phone:</span>&nbsp;&nbsp; (+233) 30 243 4505 or (+233) 55 496 7571 or (+233) 50 163 4195 </p>
                          @endif

                           {{--  <p style="margin-bottom:-7px;"><span style="color:#000; font-weight:700;">Fax:</span>&nbsp;&nbsp; (+1) 888 652 2233</p> --}}
                            <p style="margin-bottom:-7px;"><span style="color:#000; font-weight:700;">Website:</span>&nbsp;&nbsp; <a href="http://www.aquantuo.com">www.aquantuo.com </a></p>
                       </td>
                        <td align="right" valign="top" style="padding-top:10px;">
                            
                            <p style="margin-bottom:-7px;"><span style="color:#000; font-weight:700;">Status:</span>@if(@$info->paidStatus == 'yes') Paid @else Hold @endif </p>
                            <p style="margin-bottom:-7px;"><span style="color:#000; font-weight:700;">Package Status:</span>{{ get_status_title(@$info->Status)['status'] }} </p>
                        </td>
                    </tr>
                </table>
                <br><br>
                <table width="100%">
                    <tr>
                        {{-- <td width="35%" valign="top">
                            <h2 class="heading-bg" style="margin-bottom:10px;  font-size:16px;"><b>Bill To:</b></h2>
                            <p style="margin-bottom:-7px;"><span style="color:#000; font-weight:700;">Name:&nbsp;</span> {{@$info->RequesterName}} </p>
                             <p style="margin-bottom:-10px;"><span style="color:#000; font-weight:700;">Company Name:&nbsp;</span> Ideal IT Technologys</p>
                            <p style="margin-bottom:-7px;"><span style="color:#000; font-weight:700;">Street Address:&nbsp;</span>  {{@$info->DeliveryFullAddress}}</p>
                            <p style="margin-bottom:-7px;"><span style="color:#000; font-weight:700;">City, ST ZIP:&nbsp;</span> 452101</p>
                            <p style="margin-bottom:-7px;"><span style="color:#000; font-weight:700;">Phone:&nbsp;</span> {{@$info->ReceiverMobileNo}}</p>
                        </td> --}}
                        <td width="75%" style="valign="top">
                            <h2 class="heading-bg" style="margin-bottom:10px;  font-size:16px;"><b>Customer Details:</b></h2>
                            <p style="margin-bottom:-7px;"><span style="color:#000; font-weight:700;">Name:&nbsp;</span> {{@$info->RequesterName}}</p>
                          {{--   <p style="margin-bottom:-7px;"><span style="color:#000; font-weight:700;">Company Name:&nbsp;</span> Ideal IT Technologys</p> --}}
                            <p style="margin-bottom:-7px;"><span style="color:#000; font-weight:700;">Street Address:&nbsp;</span> {{@$info->DeliveryFullAddress}}</p>
                           {{--  <p style="margin-bottom:-7px;"><span style="color:#000; font-weight:700;">City, ST ZIP:&nbsp;</span> 452101</p> --}}
                            <p style="margin-bottom:-7px;"><span style="color:#000; font-weight:700;">Phone:&nbsp;</span> {{@$info->ReceiverMobileNo}}</p>
                        </td>
                        <td width="25%" valign="top" align="right" style="text-align:right;">
                            <h2 class="heading-bg" style="margin-bottom:10px; font-size:16px;"><b>Transporter:</b></h2>
                            <p style="margin-bottom:-7px;"><span style="color:#000; font-weight:700;">&nbsp;</span> {{@$info->TransporterName}}</p>
                        </td>
                    </tr>
                </table>
                <br><br><br>
                <div style="clear:both;"></div>

                           <?php
if (@$info['ProductList']) {
    $totalW = 0;

    foreach (@$info['ProductList'] as $product) {
        $totalW = $totalW+@$product['weight'];
        $unit = @$product['weight_unit'];

    }
}

?>

                <table class="custom-table  table-stripped" style="border:1px solid #ddd; width:100%; margin:25px 0 35px" cellspacing="0" cellpadding="0">
                    <thead>
                        <tr>
                           {{--  <th style=" padding:10px 10px; border-right:1px solid #ddd;" width="25%">Received Date</th> --}}
                            <th style="padding:8px 10px; border-right:1px solid #ddd;" width="33%">Shipping Date</th>
                            <th style="padding:8px 10px; border-right:1px solid #ddd;" width="33%">Arrival Date</th>
                            <th style="padding:8px 10px; " width="34%">Total Weight</th>
                            </tr>
                        </thead>
                    <tbody>
                        <tr>
                          {{--   <td style="padding:5px 10px; border-top:1px solid #ddd; border-right:1px solid #ddd;">{{show_date(@$info->ReceivedDate)}}</td> --}}
                            <td style="padding:5px 10px; border-top:1px solid #ddd; border-right:1px solid #ddd;">{{show_date(@$info->ShippingDate)}}</td>
                            <td style="padding:5px 10px; border-top:1px solid #ddd; border-right:1px solid #ddd;">{{show_date(@$info->ArrivalDate)}}</td>
                            <td style="padding:5px 10px; border-top:1px solid #ddd;">@if(@$totalW){{@$totalW}} {{@$unit}} @else 0 @endif</td>
                        </tr>
                    </tbody>

                </table>
         <?php

$quantity = 0;
$ShipQty = 0;

?>
                <table class="custom-table  table-stripped" style="border:1px solid #ddd; width:100%;page-break-before:auto;" cellspacing="0" cellpadding="0">
                <thead>
                    <tr>
                        <th style=" padding:10px 10px; border-right:1px solid #ddd;">Item Id</th>
                        <th style="padding:8px 10px; border-right:1px solid #ddd;">Received Date</th>
                        <th style="padding:8px 10px; border-right:1px solid #ddd;">Item Name</th>
                        <th style="padding:8px 10px; border-right:1px solid #ddd;" width="50">Item Weight</th>
                        <th style="padding:8px 10px;" width="80">Received Qty/Ship Qty</th>
                    </tr>
                </thead>
                <tbody>
          @if(@$info['ProductList'])
          <?php $count = 0;?>
             {{! $i = 2}}
        @foreach(@$info['ProductList'] as $product)
        <?php $count = $count +1;?>
                <tr @if(fmod($i,2)==0) style="background:#eee"; @else style="background:"; @endif >
                    <td style="padding:5px 10px; border-right:1px solid #ddd;"> {{@$product['package_id']}}</td>
                    <td style="padding:5px 10px; border-right:1px solid #ddd;"> @if(@$product['ReceivedDate'] !='') {{show_date(@$product['ReceivedDate'])}} @endif</td>
                    <td style="padding:5px 10px; border-right:1px solid #ddd;"> {{@$product['product_name']}} </td>
                    <td style="padding:5px 10px; border-right:1px solid #ddd;"> {{@$product['weight']}} {{@$product['weight_unit']}}</td>
                   {{--  <td style="padding:5px 10px; border-right:1px solid #ddd;">{{@$product['qty']}}</td> --}}
                    <td style="padding:5px 10px;">{{@$product['Delivered']}}</td>
                </tr>
                       {{--  {{! $quantity = $quantity+ @$product['qty']}} --}}
                        {{! $ShipQty = $ShipQty+ @$product['Delivered']}}
                         {{!$i = $i + 1}}
                    @endforeach
                    @endif
                </tbody>
                <tfoot>
                        <tr>
                            <td colspan="4" style="padding:5px 10px; border-bottom:1px solid #ddd;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="" style="padding:5px 10px; ">&nbsp;</td>
                            <td colspan="" style="padding:5px 10px; ">&nbsp;</td>
                            <td colspan="" style="padding:5px 10px; ">&nbsp;</td>
                            <td style="padding:5px 10px; border-right:1px solid #ddd;">TOTAL</td>
                           {{--  <td style="padding:5px 10px; border-right:1px solid #ddd;">{{$quantity}}</td> --}}
                            <td style="padding:5px 10px; border:1px solid #ddd;">{{$ShipQty}}</td>
                        </tr>
                    </tfoot>
                </table>

                


                <div style="clear:both;"></div>
                <div style="width:100%; height:1px; margin-top:10px; margin-bottom:30px;"></div>
                <div style="clear:both;"></div>
                 <table style="width:100%;page-break-before:auto;">
                    <tr>
                        <td style="width:100%">
                            <p style=" font-weight: 500; font-size:12px;"><span style="color:#000; font-weight:700;">Comments:</span> &nbsp;<span style="">
                            <input type="text" placeholder="" name="Comments" value="{{@$info->comments}}" style="padding:0px 0px; margin-top:0px; text-align:left; border:none; border-bottom:1px solid #ddd; width:85%;"></span></p>
                        </td>
                    </tr>

                </table>
                @if($count > 6)
                    <div style="page-break-after: always;"></div>
                @endif
                
                <table style="text-align:center; width:100%; padding-top:0px; padding-bottom:20px; position:absolute; bottom:80px; width:100%;page-break-before:auto !important;">
                    <tr>
                        <td>

            <h3 style="text-transform:uppercase; font-size:22px; margin-bottom:10px;"><b style="font-weight: 700; font-size:16px;">Thank you for your business!</b></h3>
            <p style="margin:2px 0;">Should you have any enquiries concerning this delivery, please contact us at:</p>
            <p style="margin:2px 0;">Email:<a style="color: #0e8ac3;" href="mailto:support@aquantuo.com" class="anchor-blue">support@aquantuo.com</a>
            or call us at: Ghana: <a style="color: #0e8ac3;" href="tel:0302434505" class="anchor-blue">030 243 4505</a>
            US: <a style="color: #0e8ac3;" href="tel:8886522233" class="anchor-blue">888 652 2233
.</a>
            .</p>

                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>

</html>
