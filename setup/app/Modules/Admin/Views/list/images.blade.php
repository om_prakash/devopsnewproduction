@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')

@section('content')

<div class="container-fluid">
	<div class="panel panel-default">
	  	<div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;<b>Slider Images</b></div>
	  	<div class="panel-body">
        	<div class="col-md-12 row">
			{!! Form::model('', ['method' => 'GET', 'url' => ['admin/silder-image']]) !!}

            	<div class="inline-form">
					<div class="form-group">
					{!! Form::label('Description', 'Description', ['class'=>'control-lable']) !!}
					{!! Form::text('search_value', Input::get('search_value'), ['class'=>'form-control', 'placeholder'=> 'Description']) !!}
	                 </div>
                 </div>

                
                <div class="inline-form">
	                <div class="form-group">
	                {!! Form::submit('Search', ['class' => 'btn btn-primary']) !!}
	                </div>

                 </div>

                <div class="inline-form">
	                <div class="form-group">
						<a href="{{URL::to('admin/silder-image')}}" class="btn btn-primary">
						<span aria-hidden="true" class="glyphicon glyphicon-refresh">

						</span>&nbsp;&nbsp;Reset
                    </a>
	                </div>
				</div>

				<div class="inline-form">
				<div class="form-group">
		   	
		   			<a type="button" class="btn btn-primary" href="{{url('admin/add-images')}}">
		   				<i class="fa fa-plus"></i>&nbsp;&nbsp;Add Images</a>
				</div>
				</div>

			{!! Form::close() !!}
         </div>
		<div class="clearfix"></div>
	      <!-- Pagination Section-->
		<input type="hidden" name="urlvalue" id="urlvalue" value="{{$paginationurl}}" />
		<input type="hidden" name="postvalue" id="postvalue" value="{{$postvalue}}" />
		<input type="hidden" name="orderby" id="orderby" value="{{$orderby}}" />
		<input type="hidden" name="orderType" id="orderType" value="{{$orderType}}" />
		<div id="containerdata"></div>
		<div class="clearfix"></div>
	  	</div> <!-- Panel Body -->
	</div>
</div>
@include('Admin::layouts.footer')

@stop
