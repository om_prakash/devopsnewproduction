@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')
@section('content')
{!! Html::style('theme/date-time-picker/jquery.datetimepicker.css') !!}

{!! Html::script('theme/date-time-picker/jquery.datetimepicker.js') !!}
<style>
input[type="radio"] {
    -ms-transform: scale(0.80.8); /* IE 9 */
    -webkit-transform: scale(0.80.8); /* Chrome, Safari, Opera */
    transform: scale(0.8);
}
input[type="checkbox"] {
    -ms-transform: scale(0.80.8); /* IE 9 */
    -webkit-transform: scale(0.80.8); /* Chrome, Safari, Opera */
    transform: scale(0.8);
}
</style>
<div style="background:#3F3F3F; margin-top:-20px; padding-top:20px;">
	<div class="container-fluid panel-default">
		<div class="panel-heading" style="height:55 !important"><i class="fa fa-th-list"></i>&nbsp;<b>Received Items Slip List</b></div>
	</div>
	
	<div class="panel-body">
		<div class="receipt_block" style="max-width: 950px !important;">
			<div id="containerdata">
				{!! Form::model('', ['method' => 'get', 'url' => ['admin/received-slip-list']]) !!}
				<div class="row">
					<div class="col-md-2">
						<div class="form-group">
							{!! Form::label('from', 'Created From', ['class'=>'control-lable']) !!}
							<div>
								{!! Form::text('StartDate', Input::get('StartDate'), ['class'=>'form-control', 'placeholder'=> 'From','id' => 'startDate','readonly' => 'readonly']) !!}
							</div>
						</div>
					</div>

	                <div class="col-md-2">
						<div class="form-group">
							{!! Form::label('to', 'Created To', ['class'=>'control-lable']) !!}
							<div>
								{!! Form::text('EndDate', Input::get('EndDate'), ['class'=>'form-control', 'placeholder'=> 'To','id' => 'endDate','readonly' => 'readonly']) !!}
							</div>
						</div>
	                </div>

					<div class="col-md-1">
						<div class="form-group" style="text-align: center;">
							{!! Form::label('air', 'Air', ['class'=>'control-lable']) !!}
							<div>
								{!! Form::radio('type', 'air', Input::get('type')=='air', ['class'=>'form-control', 'placeholder'=> 'To','id' => 'air']) !!}
							</div>
						</div>
					</div>

					<div class="col-md-1">
						<div class="form-group" style="text-align: center;">
							{!! Form::label('sea', 'Sea', ['class'=>'control-lable']) !!}
							<div>
								{!! Form::radio('type', 'sea', Input::get('type')=='sea', ['class'=>'form-control', 'placeholder'=> 'To','id' => 'sea']) !!}
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label>Country</label>
							<select name="country" class="form-control required" id="pp_pickup_country3" >
								<option value="" id="country">All Country</option>
								@foreach($countryList as $key)
								<option value={{$key->Content}}
								@if($selectedCountry == $key->Content) selected='selected' @endif
								>{{$key->Content}}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="form-group">
						{!! Form::label('from', 'Request Type', ['class'=>'control-lable']) !!}
					</div>
				</div>
				<div class="row">
					<div class="col-md-2">
						<div class="form-group" style="text-align: center;">
							{!! Form::label('op', '"Received" for OP', ['class'=>'control-lable']) !!}
							<div>
								{!! Form::checkbox('op', 'op', Input::get('op')=='op', ['class'=>'form-control', 'id' => 'op']) !!}
							</div>
						</div>
					</div>
					<div class="col-md-2">
						<div class="form-group" style="text-align: center;">
							{!! Form::label('bfm', '"Received" for BFM', ['class'=>'control-lable']) !!}
							<div>
								{!! Form::checkbox('bfm', 'bfm', Input::get('bfm')=='bfm', ['class'=>'form-control', 'id' => 'bfm']) !!}
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group" style="text-align: center;">
							{!! Form::label('sap', '"Out for Pick up" for SAP', ['class'=>'control-lable']) !!}
							<div>
								{!! Form::checkbox('sap', 'sap', Input::get('sap')=='sap', ['class'=>'form-control', 'id' => 'sap']) !!}
							</div>
						</div>
					</div>
					<div class="col-md-5">
						<div class="form-group" style="text-align: center;">
							{!! Form::label('pending', '"Pending" with "INFORM REQUESTER EMAIL SENT"', ['class'=>'control-lable']) !!}
							<div>
								{!! Form::checkbox('pending', 'pending', Input::get('pending')=='pending', ['class'=>'form-control', 'id' => 'pending']) !!}
							</div>
						</div>
					</div>
				</div>
				<div class="row">
				</div>
				<div class="row">
					<div class="col-sm-3 col-xs-12">
						<button type="submit" class="btn btn-primary btn-margin"> Search</button>
					</div>
				</div>
				{!! Form::close() !!}
				<div class="clearfix"></div>
				<hr>
				{!! Form::model('', ['method' => 'post', 'url' => ['admin/create-received-slip'], 'target'=>"_blank",'id'=>'form_s']) !!}

				<input type="hidden" name="StartDate" value="{{Input::get('StartDate')}}">
				<input type="hidden" name="EndDate" value="{{Input::get('EndDate')}}">
				<input type="hidden" name="type" value="{{Input::get('type')}}">
				<input type="hidden" name="op" value="{{Input::get('op')}}">
				<input type="hidden" name="bfm" value="{{Input::get('bfm')}}">
				<input type="hidden" name="sap" value="{{Input::get('sap')}}">
				<input type="hidden" name="pending" value="{{Input::get('pending')}}">
				<input type="hidden" name="country" value="{{$selectedCountry}}">
				<div class="row">
					<center><h2><u>COMMERCIAL INVOICE</u></h2></center>
					<div class="col-md-12 ">&nbsp;</div>
					<!-- Row 1 -->
					<div class="col-md-6 row">
						<div class="col-md-4">
							<p style=" margin-bottom:6px;"><span style="color:#000; font-weight:700; margin-bottom:10px;">Shipper name:&nbsp;</span> 
							</p>
						</div>
						<div class="col-md-8">
							<Input type="text" name="shipper_name" id="shipper_name" value="Aquantuo LLC">
							<span class="er_feild" style="color:red;" id="shipper_name_er"></span>
						</div>
					</div>

					<div class="col-md-6 row">
						<div class="col-md-4">
							<p style=" margin-bottom:6px;"><span style="color:#000; font-weight:700; margin-bottom:10px;">Consignee name:&nbsp;</span> 
							</p>
						</div>
						<div class="col-md-8">
							<Input type="text" name="consignee_name" id="consignee_name" value={{$consigneeName}}>
							<span class="er_feild" style="color:red;" id="consignee_name_er"></span>
						</div>
					</div>
					<!-- End row 1 -->
					<div class="col-md-12 ">&nbsp;</div>
					<!-- Row 2 -->
					<div class="col-md-6 row">
						<div class="col-md-4">
							<p style=" margin-bottom:6px;"><span style="color:#000; font-weight:700; margin-bottom:10px;">Shipper address:&nbsp;</span> 
							</p>
						</div>
						<div class="col-md-8">
							<Input type="text" name="shipper_address" id="shipper_address" value="5520 N Dupont Hwy Dover Delaware 19901,">
							<span class="er_feild" style="color:red;" id="shipper_address_er"></span>
						</div>
					</div>

					<div class="col-md-6 row">
						<div class="col-md-4">
							<p style=" margin-bottom:6px;"><span style="color:#000; font-weight:700; margin-bottom:10px;">Consignee address:&nbsp;</span> 
							</p>
						</div>
						<div class="col-md-8">
							<Input type="text" name="consignee_address" id="consignee_address" value="<?= $consigneeAddress;?>">
							<span class="er_feild" style="color:red;" id="consignee_address_er"></span>
						</div>
					</div>
					<?php $total = $totalWeight = $unit = $unitValue = 0;?>
					<!-- End row 2 -->
					<!-- <div class="col-md-12 "><h5>Required for customs and security</h5></div> -->
					<div class="col-md-12 ">
						<div class="table-responsiv">        
						  <table class="table">
						    <thead>
						      <tr>
						        <th>Count</th>
						        <th>Item #</th>
						        <th>Title</th>
						        <th>Units</th>
						        <th>Unit Value</th>
						        <th>Total Value</th>
						      </tr>
						    </thead>

						    <tbody>
						    @if(count($receivedItems) > 0)
						    	@foreach($receivedItems as $key)
						    		<tr>
						    			<td>{{$sn++}}</td>
						    			<td>{{$key['package_id']}}</td>
						    			<td>
						    			{{@$key['product_name']}}
						    			<!-- @if(isset($key['description']))
						    				{{@$key['description']}}
						    			@elseif(isset($key['Description']))
						    				{{@$key['Description']}}
						    			@endif -->
						    			</td>

						    			<td>
						    			<?php 
						    				$quantity = @$key['qty'];
						    				$price = @$key['price'];
						    				$Value = $quantity * $price;
											$total = $total + $price;
											$unit = $unit + $quantity;
											$unitValue = $unitValue + $Value;
											$totalWeight = $totalWeight + @$key['weight'] * @$key['qty'];
						    			?>
						    			<input value="{{$quantity}}" type="text" name="quantity_{{$key['_id']}}">
						    				
						    			</td>
						    			
						    			<td>
						    				<input value="{{$price}}" type="text" name="price_{{$key['_id']}}">
						    				
						    			</td>
						    			<td>
						    				<input value="{{$Value}}" type="text" name="value_{{$key['_id']}}">
						    			</td>
						    			
						    		</tr>
						    	@endforeach
								<tr>
									<td></td>
									<td><b>TOTAL</b></td>
									<td></td>
									<td class="text-center"><b><?= $unit; ?></b></td>
									<td class="text-center"><b><?= $unitValue; ?></b></td>
									<td class="text-center"><b>$<?= $total; ?></b></td>
								</tr>
						    @endif
						      
						      
						    </tbody>
						  </table>
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<b>TOTAL PACKAGE WEIGHT (Lbs): </b>
						</div>
						<div class="col-md-2">
							<input value="<?= $totalWeight;?>" type="text" name="total_weight" style="text-align: left" />
						</div>
					</div>
					<div class="col-md-12 ">
					<h5>I certify that this shipment does not contain and dangerous goods as per IATA Transport Regulations such as Explosives, compressed gases oxidizers, corrosives, flammable liquids and solid, firearms, radioactive materials and poisons 	qy7 considered hazardous.</h5>
					</div>
					<div class="col-md-12 ">&nbsp;</div>
					<div class="col-md-12 ">
						<div class="col-md-8 row">
							Signature:
						
						</div>

						<div class="col-md-4">
							Date:
						
						</div>
					</div>
					
					<div class="col-md-12 "><hr></div>
					<div class="col-md-12">
						<div class="col-md-6 row">
							
							<button id="resetBt" type="button" class="btn slip-btn"> Reset Slip</button>
							<button type="submit" class="btn slip-btn"> Create Packing Slip</button>
						</div>
						
					</div>

					
               </div>
		
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>

<script>
$(function(){
	$('#startDate').datetimepicker({
		format:'d/m/Y',
		onChangeDateTime:function( ct ){
			$('#endDate').datetimepicker({	minDate:ct	})
		},
		timepicker:false,
		scrollInput : false,
	});
	$('#endDate').datetimepicker({
		format:'d/m/Y',
		onChangeDateTime:function( ct ){
			$('#startDate').datetimepicker({	maxDate:ct	})
		},
		timepicker:false,
		scrollInput : false,
	});

	

});

$(document).ready(function(){
	$("#resetBt").click(function(){
		$('#form_s').trigger("reset");
	});
});

$(document).ready(function(){
	
	$("#form_s").submit(function(){
		var flage = true;
		$('.er_feild').html('');
		if($('#consignee_address').val() == ''){
			$('#consignee_address_er').html('This field is required');
			flage = false;
		}

		if($('#shipper_address').val() == ''){
			$('#shipper_address_er').html('This field is required');
			flage = false;
		}
		if($('#shipper_name').val() == ''){
			$('#shipper_name_er').html('This field is required');
			flage = false;
		}
		if($('#consignee_name').val() == ''){
			$('#consignee_name_er').html('This field is required');
			flage = false;
		}
		return flage ;
	});
});





</script>

@endsection