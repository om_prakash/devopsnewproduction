@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')

@section('content')

{!! Html::style('theme/date-time-picker/jquery.datetimepicker.css') !!}

{!! Html::script('theme/date-time-picker/jquery.datetimepicker.js') !!}

<div class="container-fluid">
<div class="panel panel-default">
  <div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;<b>{{$title}}</b></div>
  <div class="panel-body">

		{!! Form::model('', ['method' => 'GET']) !!}
			<div class="col-md-12">

				<div class="inline-form">
	                <div class="form-group">
	                {!! Form::label('code', 'Code', ['class'=>'control-lable']) !!}
					{!! Form::text('code', Input::get('code'), ['class'=>'form-control', 'placeholder'=> 'Code']) !!}

	                </div>
				</div>

        		<div class="inline-form">
	                <div class="form-group">
	                	{!! Form::label('validfrom', 'Valid From',['class' => 'control-lable']) !!}
					{!! Form::text('validfrom', Input::get('validfrom'), ['class'=>'form-control', 'placeholder'=> 'Valid From','id' =>'startDate']) !!}

	                </div>
				</div>

				<div class="inline-form">
	                <div class="form-group">
	                	{!! Form::label('validtill', 'Valid Till',['class' => 'control-lable']) !!}
					{!! Form::text('validtill', Input::get('validtill'), ['class'=>'form-control', 'placeholder'=> 'Valid Till','id' =>'endDate']) !!}

	                </div>
				</div>

                 <div class="inline-form">
	                <div class="form-group">
	                {!! Form::submit('Search', ['class' => 'btn btn-primary']) !!}
	                </div>
                 </div>

                <div class="inline-form">
	                <div class="form-group">
						<a href="{{URL::to('admin/promocode')}}" class="btn btn-primary">
						<span aria-hidden="true" class="glyphicon glyphicon-refresh">

						</span>&nbsp;&nbsp;Reset
                    </a>
	                </div>
				</div>

				<div class="inline-form">
	                <div class="form-group">
						<a href="{{URL::to('admin/add-promocode')}}" class="btn btn-primary">


						</span>&nbsp;&nbsp;Add Promocode
                    </a>
	                </div>
				</div>


            </div>
            <div class="col-md-12">









					<div class="inline-form pull-right">
					<br /><br />

					</div>
			</div>


	<div class="clearfix"></div>


      <!-- Pagination Section-->
	<input type="hidden" name="urlvalue" id="urlvalue" value="{{$paginationurl}}" />
	<input type="hidden" name="postvalue" id="postvalue" value="{{$postvalue}}" />
	<input type="hidden" name="orderby" id="orderby" value="{{$orderby}}" />
	<input type="hidden" name="orderType" id="orderType" value="{{$orderType}}" />
	<div id="containerdata"></div>
	<div class="clearfix"></div>
	{!! Form::close() !!}



</div> <!-- Panel Body -->
</div>

</div>
</div>
<script>
		$(function(){
		$('#startDate').datetimepicker({
			format:'M d, Y',
			onChangeDateTime:function( ct ){
				$('#endDate').datetimepicker({	minDate:ct	})
			},
			timepicker:false,
		});
		$('#endDate').datetimepicker({
			format:'M d, Y',
			onChangeDateTime:function( ct ){
				$('#startDate').datetimepicker({	maxDate:ct	})
			},
			timepicker:false,
		});
	});

</script>


<!-- Modal -->
<div class="modal fade" id="refund-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Refund</h4>
      </div>
      <div id="refund-data" class="msg_stripe"></div>
    </div>
  </div>
</div>
@include('Admin::layouts.footer')
@stop

<script>

function get_package_information(id)
{
	var url = SITEURL+'ajax/get_package_information/'+id;
	$('#loading').show();
	$.ajax({
		type:"GET",
		url:url,
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		success: function(msg)
		{
			$('#loading').hide();
			$("#refund-data").html(msg);
		}
	});
}
function make_refund(id)
{
	var refundAmount =$("#refund_amount").val();
	 if(refundAmount =="")
		{
			 flag = false;
			 $("#refund_amountdiv").addClass("has-error");
			 $("#refund_amountError").html('Refund amount field is required.');
		}
		else
		{

		  if(!$.isNumeric(refundAmount))  //if(Number(refundAmount)<0)
		  {
			$("#refund_amountdiv").addClass("has-error");
			$("#refund_amountError").html('Refund amount is only positive number.');
		  }
		   else
		   {
				$("#refund_amountError").html('');
				//var id = "5628e4886734c4a0637dd22c";
				var url = SITEURL+'ajax/get_refund_information/'+id+'/'+refundAmount;
				$('#loading').show();
				$.ajax({
					type:"GET",
					url:url,
					dataType: "json",
					headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
					success: function(msg)
					{
						var json1 = JSON.parse(msg);
						//console.log(json1);
						if(json1.error.type =="invalid_request_error")
						{
							var respons = '<p class="error_msg">'+json1.error.message+'</p>';
						}
						else if(json1.status =="succeeded")
						{
							var respons = '<p class="success_msg">'+"Your amount is refunded"+'</p>';
						}
						else
						{
							var respons = "Your stripe id valid";
						}
						$('#loading').hide();
						$("#refund-data").html(respons);
					}
				});
		   }
		}
}
</script>

