@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')

@section('content')
{!! Html::style('theme/date-time-picker/jquery.datetimepicker.css') !!}
{!! Html::script('theme/date-time-picker/jquery.datetimepicker.js') !!}

<div class="container-fluid">
<div class="panel panel-default">
  <div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;<b>{{$title}}</b></div>
  <div class="panel-body">
  			<div class="row">
		{!! Form::model('', ['method' => 'post', 'url' => ['admin/slip-list']]) !!}


                <div class="col-sm-2 col-xs-12">
					{!! Form::label('Name', 'Name', ['class'=>'control-lable']) !!}
					<div>
						{!! Form::text('search_name', Input::get('search_name'), ['class'=>'form-control', 'placeholder'=> 'Name','id' =>'inputError1']) !!}
					</div>
                </div>

				<div class="col-sm-2 col-xs-12">
					{!! Form::label('Package Number', 'Package Number', ['class'=>'control-lable']) !!}
					<div>
						{!! Form::text('search_value', Input::get('search_value'), ['class'=>'form-control', 'placeholder'=> 'Package Number','id' =>'inputError1']) !!}
					</div>
                </div>

              {{--  <div class="col-sm-2 col-xs-12">
						{!! Form::label('from', 'From', ['class'=>'control-lable']) !!}
						<div>
							{!! Form::text('StartDate', Input::get('StartDate'), ['class'=>'form-control', 'placeholder'=> 'From','id' => 'startDate','readonly' => 'readonly']) !!}
						</div>
		      </div>
			       <div class="col-sm-2 col-xs-12">
						{!! Form::label('to', 'To', ['class'=>'control-lable']) !!}
						<div>
							{!! Form::text('EndDate', Input::get('EndDate'), ['class'=>'form-control', 'placeholder'=> 'To','id' => 'endDate','readonly' => 'readonly']) !!}
						</div>
				   </div> --}}


                <div class="inline-form">
                <div class="form-group">
					{!! Form::submit('Search', ['class' => 'btn btn-primary']) !!}
					<a href="{{url('')}}/admin/slip-list" class="btn btn-primary mt-10">
						<span aria-hidden="true" class="glyphicon glyphicon-refresh">
						</span>&nbsp;&nbsp;Reset
                	</a>
                    </div>
				</div>
		{!! Form::close() !!}

	<a href="{{url('')}}/admin/slip-management" class="btn btn-primary" style="margin-top: 10px;">
      <span> Create Slip </span>
	</a>
	</div>

	<div class="clearfix"></div>


      <!-- Pagination Section-->
	<input type="hidden" name="urlvalue" id="urlvalue" value="{{$paginationurl}}" />
	<input type="hidden" name="postvalue" id="postvalue" value="{{$postvalue}}" />
	<input type="hidden" name="orderby" id="orderby" value="{{$orderby}}" />
	<input type="hidden" name="orderType" id="orderType" value="{{$orderType}}" />
	<div id="containerdata"></div>
	<div class="clearfix"></div>


  </div> <!-- Panel Body -->

<script>
	$(function(){
		$('#startDate').datetimepicker({
			format:'d/m/Y',
			onChangeDateTime:function( ct ){
				$('#endDate').datetimepicker({	minDate:ct	})
			},
			timepicker:false,
			scrollInput : false,
		});
		$('#endDate').datetimepicker({
			format:'d/m/Y',
			onChangeDateTime:function( ct ){
				$('#startDate').datetimepicker({	maxDate:ct	})
			},
			timepicker:false,
			scrollInput : false,
		});
	});
</script>


</div>
</div>

@include('Admin::layouts.footer')

@stop
