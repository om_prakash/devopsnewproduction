@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')

@section('content')
<div class="container-fluid">
<div class="panel panel-default">
  <div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;<b>{{$title}}</b></div>
  <div class="panel-body">
		{!! Form::model('', ['method' => 'PATCH', 'url' => ['admin/category_list']]) !!}
        	<div class="inline-form">
				<div class="form-group">
					{!! Form::label('Content', 'Category Name', ['class'=>'control-lable']) !!}
						{!! Form::text('search_value', Input::get('search_value'), ['class'=>'form-control', 'placeholder'=> 'Categoty Name','id' =>'inputError1']) !!}
					</div>

            </div>
            <div class="inline-form">
				<div class="form-group">
					{!! Form::label('Shipping Mode', 'Shipping Mode:',['class' => 'control-label']) !!}
				   {!! Form::select('TravelMode', ['air'=>'By Air','ship'=>'By Sea'], Input::get('TravelMode'), ['class'=>'form-control', 'placeholder'=> 'Shipping Mode','id' =>'travelmode1']) !!}


				</div>

            </div>
            <div class="inline-form">
                <div class="form-group">
					<label></label>
					{!! Form::submit('Search', ['class' => 'btn btn-primary']) !!}

					 <a href="{{URL::to('admin/category_list')}}" class="btn btn-primary">
						<span aria-hidden="true" class="glyphicon glyphicon-refresh">
						</span>&nbsp;&nbsp;Reset
                      </a>

					<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Add Category</button>
				</div>
			</div>
		{!! Form::close() !!}
	<div class="clearfix"></div>

      <!-- Pagination Section-->
	<input type="hidden" name="urlvalue" id="urlvalue" value="{{$paginationurl}}" />
	<input type="hidden" name="postvalue" id="postvalue" value="{{$postvalue}}" />
	<input type="hidden" name="orderby" id="orderby" value="{{$orderby}}" />
	<input type="hidden" name="orderType" id="orderType" value="{{$orderType}}" />
	<div id="containerdata"></div>
	<div class="clearfix"></div>


  </div> <!-- Panel Body -->
</div>

  </div> <!-- Panel Body -->
</div>


			<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
			  <div class="modal-dialog" role="document">
				<div class="modal-content">
				  <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="exampleModalLabel">Add Category</h4>
				  </div>

				  {!! Form::model('', ['name' => 'categoryForm', 'id' => 'categoryForm', 'method' => 'POST', 'url' => ['admin/add_category']]) !!}
				  <div class="modal-body">
					<div class="col-md-12">
						<div class="form-group" id="cityDiv">
							{!! Form::label('Category', 'Category Name:',['class' => 'control-label']) !!}
							{!! Form::text('Content', '', ['class'=>'form-control', 'placeholder'=> 'Category Name','id' =>'content']) !!}
							<p class="help-block red" id="er_content" style="color:#a94442"></p>
						</div>
					</div>
					<div class ="clearfix"></div>
					<div class="col-md-12">
						<div class="form-group" id="cityDiv">
							{!! Form::label('TravelMode', 'Shipping Mode:',['class' => 'control-label']) !!}
							{!! Form::select('TravelMode', [''=>'Select mode','air'=>'By Air','ship'=>'By Sea'], '', ['class'=>'form-control','id' =>'travelmode',"onchange"=>"return toggle_category()"]) !!}
							<p class="help-block red" id="er_travelmode" style="color:#a94442"></p>
                          </div>
                    </div>
					<div class ="clearfix"></div>
					<input type="hidden" name="ChargeType" id="ChargeType" value="">
					<!-- <div class="col-md-12">
						<div class="form-group charge_class" id="cityDiv">
							{!! Form::label('ChargeType', 'Charge Type:',['class' => 'control-label']) !!}

							<select name="ChargeType" class="form-control" id="chargetype">
								<option value="">Select Type</option>
								<option value="distance" class="dis">Distance</option>
								<option value="fixed" class="fix">Fixed</option>
							</select>

							<!-- {!! Form::select('ChargeType', [''=>'Select Type','distance'=>'Distance','fixed'=>'Fixed'], '', ['class'=>'form-control', 'placeholder'=> 'Charge Type','id' =>'chargetype']) !!}


							<p class="help-block red" id="er_chargetype" style="color:#a94442"></p>
                          </div>
                    </div> -->
                   <div class ="clearfix"></div>
                   </div>

				  <div class="modal-footer">
					{!! Form::button('Close', ['class' => 'btn btn-primary', 'data-dismiss'=>'modal']) !!}
					{!! Form::submit('Add', ['class' => 'btn btn-primary']) !!}

				 </div>
				{!! Form::close() !!}
				<div class="clearfix"></div>
				</div>
			  </div>
			</div>
	 </div>

{!! Html::script('theme/admin/custome/js/validation.js') !!}


<script>

function toggle_category() {
	if($('#travelmode').val() == 'air'){
		$('#ChargeType').val('fixed');
	}else {
		$('#ChargeType').val('distance');
	}

	//alert($('#travelmode').val());
	/*if($('#travelmode').val() == 'air'){
		$("div.charge_class select").val("fixed");
		$('.dis').attr('disabled', true);
		$('.fix').attr('disabled',false);
	}else if($('#travelmode').val() == 'ship'){
		$("div.charge_class select").val("distance");
		$('.fix').attr('disabled', true);
		$('.dis').attr('disabled',false);
	}else{
		$("div.charge_class select").val("");
	}*/


}

$("#categoryForm").submit(function()
{
	var flag = true;

	if(valid.required('content','category name') == false) { flag = false; }
	if(valid.required('travelmode','travel mode') == false) { flag = false; }
	if(valid.required('chargetype','charge type') == false) { flag = false; }


	return flag;
});

</script>
@include('Admin::layouts.footer')
@stop
