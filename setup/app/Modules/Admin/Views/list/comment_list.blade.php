@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')

@section('content')

<style>
	.link {
		display: none;
	}
</style>

<div class="container-fluid">
	<div class="panel panel-default">
	  	<div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;<b>{{$title}}</b><button style="margin-top:-5px;" type="button" class="btn btn-success pull-right" onclick="backButtonCall()">&nbsp;Back&nbsp;</button><button style="margin-top:-5px;" type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#myModal" data-whatever="@mdo"><i class="fa fa-plus"></i>&nbsp;Add Comment</button>&nbsp;<a style="margin-top:-5px;" class="btn btn-primary pull-right" href="{{url($detail_url)}}" >&nbsp;Go to Package Detail</a></div>
	  	<div class="panel-body">	
		  <!-- Pagination Section-->
			<input type="hidden" name="urlvalue" id="urlvalue" value="{{$paginationurl}}" />
			<input type="hidden" name="postvalue" id="postvalue" value="{{$postvalue}}" />
			<input type="hidden" name="orderby" id="orderby" value="{{$orderby}}" />
			<input type="hidden" name="orderType" id="orderType" value="{{$orderType}}" />
			<div id="containerdata"></div>
			<div class="clearfix"></div>
	  	</div><!-- Panel Body -->
	</div>
</div>


<div id="myModal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="exampleModalLabel">Add Comment</h4>
			</div>
			<div class="col-sm-12 ">
				{!! Form::model('', ['name' => 'commentForm', 'id' => 'commentForm', 'method' => 'POST', 'url' => ['admin/post-comment']]) !!}
			
					<div class="modal-body clearfix">
						<div class="form-group" id="cityDiv">
							{!! Form::label('Comment', 'Comment:',['class' => 'control-label']) !!}
							{!! Form::textarea('comment', '', ['class'=>'form-control', 'placeholder'=> 'Comment','id' =>'comment','rows'=>'3']) !!}
							<p class="help-block red" id="er_comment"</p>
						</div>
					</div>
					
					<input type="hidden" name="user_id" id="user_id" value="{{$comment['RequesterId']}} " />
					<input type="hidden" name="request_id" id="request_id" value="{{$comment['_id']}}" />
					<input type="hidden" name="product_title" id="product_title" value="{{$comment['ProductTitle']}}" />

					<div class="modal-footer">
						{!! Form::button('Close', ['class' => 'btn btn-primary', 'data-dismiss'=>'modal']) !!}
						{!! Form::submit('Add', ['class' => 'btn btn-primary']) !!}
					</div>

				{!! Form::close() !!}
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>

<!-- Small modal -->

	{!! Html::script('theme/admin/custome/js/validation.js') !!}

	<script>	
		$("#commentForm").submit(function() {
			var flag = true;
			if(valid.required('comment','Comment') == false) { flag = false; }
			return flag;
		});

		function backButtonCall() {
			window.history.back();
		}
	</script>

@include('Admin::layouts.footer')
@stop