@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')


@section('content')
<style>
.truncate {
  max-width: 100%;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
}
</style>

<div class="container-fluid">
<div class="panel panel-default">
  <div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;<b>Faq Question</b>
  </div>
  <div class="panel-body">

		   {!! Form::open(array('url' =>'admin/faq/'.Request::segment(3),'method' => 'GET','class' => '','id' => 'search_form_id','role' => "search")) !!}
        		<div class="inline-form">
				<div class="form-group">
					{!! Form::label('Search Question', '', ['class'=>'control-lable']) !!}
					<div>
						{!! Form::text('search_value', Input::get('search_value'), ['class'=>'form-control', 'placeholder'=> 'Search by question','id' =>'inputError1']) !!}
					</div>
                </div>
				</div>
                <div class="inline-form">
                <div class="form-group">
					{!! Form::submit('Search', ['class' => 'btn btn-primary']) !!}
					<a href="{{url('admin/faq/'.Request::segment(3))}}" class="btn btn-primary">
						<span aria-hidden="true" class="glyphicon glyphicon-refresh">

						</span>&nbsp;&nbsp;Reset
                    </a>
					<a href="{{ url('admin/add-faq/'.Request::segment(3)) }}" data-placement="top" title="Add" class="btn btn-primary btn-action">Add Faq</a>
				</div>
                </div>
		{!! Form::close() !!}
	<div class="clearfix"></div>
	<div class="col-sm-12 row">
		 <label class="text-info"><h3>Section Name - {{ucfirst($section->section_name)}}</h3></label>
    </div>

    <table class="table table-bordered table-striped table-highlight table-list">
<thead>

	<tr>
		<th>S.No.</th>
		<th>Question</th>
		<th style="max-width:300px;">Answer</th>
		<th>Action</th>
	</tr>
	</thead>
			<?php
$sno = 1;
?>
	<tbody id="sortable">
		<?php if (count($faq) > 0) {?>
			@foreach ($faq as $lists)
			<tr id="row-{{$lists->id}}" class="item" >
				<td scope="row" id="seq-row-{{$lists->id}}"><?php echo $sno++; ?></td>
				<td style="max-width:500px;">
					<p class="truncate">{{ucfirst($lists->question)}}</td>
				<td style="max-width:500px;" >
				<p class="truncate"><?php echo strip_tags(ucfirst($lists->answer)); ?>
				</p>
			</td>
				<td class="width-action-20">
                <div style="position:relative;" class="visible-ipad">
                            <button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Action
                        <span class="caret"></span>
                        </button>
                  </div>

					<a href="{{ url('admin/edit-faq/'.$lists->id)}}" data-placement="top" title="Edit" class="btn btn-small btn-primary btn-action">
						<i class="fa fa-pencil-square-o"></i></a>

					<a data-placement="top" title="Delete Record" class="btn btn-small btn-danger btn-action" onclick="remove_record('delete_activity/{{$lists->id}}/faqlist','{{$lists->id}}')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>

					<a data-placement="top" title="Faq Answer" class="btn btn- btn-primary btn-action"  href="{{url('admin/faq-answer/'.$lists->id)}}"><i class="fa fa-eye"></i></a>

					<a data-placement="top" title="Web Link" target="_blank" class="btn btn- btn-primary btn-action"  href="http://support.aquantuo.com/help-support/{{ $lists->id }}"><i class="fa fa-link"></i></a>
				</td>
			</tr>
			@endforeach
		<?php } else {?>
			<tr>
				<td colspan="5" class="text-center" >Question not found</td>
			</tr>
		<?php }?>
	</tbody>
</table>

	<div class="clearfix"></div>


  </div> <!-- Panel Body -->
</div>
</div>


@include('Admin::layouts.footer')
{!! Html::script('theme/admin/custome/js/shortable.js') !!}
<script>

/*

	$(function() {
		$("#sortable").sortable({
			beforeStop: function( event, ui ) {
				set_short();
			}
		});
	});

	var id = '';
	function set_short() {
		var i = 0;
		var newposition = {};
		$('.item').each(function() {

			i++;
			id = this.id.replace('row-','');

				newposition[id] = i;
			$('#seq-row-'+id).html(i);

		});

		if(true) {
			$.ajax
			({
				type: "POST",
				url: SITEURL+'admin/sequence',
				data: {newposition: newposition},
				headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
				success: function(res)
				{
				}
			});
		}
 	}
*/

	</script>
@stop
