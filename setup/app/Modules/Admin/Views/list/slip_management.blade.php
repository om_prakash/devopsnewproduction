@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')


@section('content')

<div style="background:#3F3F3F; margin-top:-20px; padding-top:20px;">
 <div class="container-fluid panel-default">
  <div class="panel-heading" style="height:55 !important"><i class="fa fa-th-list"></i>&nbsp;<b>{{$title}}</b><span class="pull-right"><a href="{{ url('admin/slip-list') }}" class="btn btn-primary">Back</a></span></div>
</div>
  <div class="panel-body">
  <div class="receipt_block">
	<div id="containerdata">
	<div class="row">
		{!! Form::model('', ['method' => 'get', 'url' => ['admin/get-slip']]) !!}
		<div class="col-sm-5 col-xs-12">
		{!! Form::label('Requester Name', 'Requester Name', ['class'=>'control-lable']) !!}
					<select name="RequesterName" id="RequesterName" class="form-control chosen-select" onchange="myFunction()">
					<option value=""> Select </option>
					@foreach($res as $val)
                   <option value="{{$val['_id']}}" @if(@$val['_id'] == @Input::get('RequesterName')) selected @endif>{{$val['Name']}}</option>
					@endforeach
					</select>
			</div>

			<div class="col-sm-4 col-xs-12">
		    {!! Form::label('Package Id', 'Package Id', ['class'=>'control-lable']) !!}
					<select name="orderId" class="form-control" id="orderId">
					@if(count(@$packageNum)>0)
					@foreach($packageNum as $val)
                   <option value="{{$val['PackageNumber']}}" @if(@$val['PackageNumber'] == @Input::get('orderId')) selected @endif >
                   {{$val['PackageNumber']}}
                   </option>
					@endforeach
					@endif

					</select>
			</div>
			<div class="col-sm-3 col-xs-12">
		    <button type="submit" class="btn btn-primary btn-margin"> Search Slip</button>
			</div>
				{!! Form::close() !!}
	 </div>
	</div>
 @if(count(@$info) > 0)
	<div>
	<hr>
	<div class="clearfix"></div>
	<div class="">
	<div class="row">
	{!! Form::model('', ['method' => 'post', 'url' => ['admin/create-slip'], 'target'=>"_blank"]) !!}

	 <div class="col-sm-7 col-xs-12">
	 <div class="logo-div">
	  <img src="{{url('')}}/theme/admin/custome/images/logo_receipt.png">

	  </div>
	  <p style="margin-top:20px;"><b>Ghana:</b> 131 Race Course Street Abeka Accra (Opposite Derby Royal Hotel)</p>
	  <p><b>Phone:</b> 030 243 4505 or 055 496 7571 or 050 163 4195 </p>
	 </div>
	  <div class="col-sm-5 col-xs-12 data text-right" >
	  <h3><b style="color:#004269; padding-right:0px; text-align:right; width:100%; display:block;">
	  Delivery Note</b></h3>
	   <p><b>Order Date:</b><span style="border:1px solid #ddd; padding:2px 8px; min-width:150px;display: inline-block;"> {{show_date(@$info->EnterOn)}}</span></p>
	   <p style="margin-top:-3px;"><b>Package ID #:</b> <span style="border:1px solid #ddd; padding:2px 8px; min-width:150px;display: inline-block;">{{ucfirst($info->PackageNumber)}}</span></p>
	    <p style="margin-top:-3px;"><b>Delivery Note:</b> <span style="border:1px solid #ddd; padding:2px 8px; min-width:150px;display: inline-block;">{{@$DeliveryNote}}</span></p>
	     <p style="margin-top:-3px; position:relative;"><b>Dispatch Date:</b>
	     {{-- {{show_date(@$info->DeliveryDate)}} --}}
	     <input type='text' class='DispatchDate' style="padding:0px 2px; padding-right:23px; width:150px;" name="dispatchDate" value="@if(@$info->dispatchDate){{show_date(@$info->dispatchDate)}} @else {{date('M d Y h:i a')}} @endif" style="width: 130px; border:1px solid #ddd; padding-top: 5px; height:30px;"/>
	     <i class="fa fa-calendar" style="position: absolute; right:5px; top:5px;" id=''></i>
	     </p>
	      <p style="margin-top:-3px;"><b>Delivery Method:</b>
          <select name="deliveryMethod" style="width: 150px; border:1px solid #ddd; background:#fff; padding-top: 0px; height:24px;">
          <option value="Drop Off" @if(@$info->deliveryMethod == 'Drop Off') selected @endif> Drop Off </option>
          <option value="Pickup" @if(@$info->deliveryMethod == 'Pickup') selected @endif> Pickup </option>
          </select></p>
		   </div>
		   <div class="clearfix"></div><br>
	       <div class="col-sm-5 col-xs-12" style="">
	       <h4 class="heading-bg"><b>Delivery Address</b></h4>
	       <p>{{@$info->RequesterName}}</p>
	        {{-- <p>[Company Name]</p> --}}
	        <p>{{@$info->DeliveryFullAddress}}</p>
	         <p>{{@$info->ReceiverMobileNo}}</p>
			 </div>
			 <div class="col-sm-2 col-xs-12"></div>
	          <div class="col-sm-5 col-xs-12 text-right" style="">
               <?php
if (@$info['ProductList']) {
    $totalW = 0;

    foreach (@$info['ProductList'] as $product) {
        $totalW = $totalW+@$product['weight'];
        $unit = @$product['weight_unit'];

    }
}

?>

	           <p style="margin-top:5px;">Total Weight: {{@$totalW}} {{@$unit}}</p>
			   </div> </div>

	           <div class="row">
	           <div class="col-sm-12 col-xs-12">
	           <div class="table-responsive table-stripped" style="margin:30px 0;">

	         	<input type="hidden" name="RequesterName" value="{{@Input::get('RequesterName')}}">
	         	<input type="hidden" name="orderId" value="{{@Input::get('orderId')}}">
	         	<input type="hidden" name="slip_id" value="{{@Input::get('slip_id')}}">

	           <table class="custom-table  table-stripped" style="border:1px solid #ddd; width:100%;">
	           <thead>
				   <tr>
					   <th style="padding:10px 10px; border-right:1px solid #ddd;">Item</th>
					   <th style="padding:8px 10px; border-right:1px solid #ddd;">Item Name</th>
					   <th style="padding:8px 10px; border-right:1px solid #ddd;" width="60">Ordered</th>
					   <th style="padding:8px 10px; border-right:1px solid #ddd;" width="60">Delivered</th>
					   <th style="padding:8px 10px;" width="60">Outstanding</th>
					</tr>
				</thead>
	           <tbody>

               @if(@$info['ProductList'])
	           @foreach(@$info['ProductList'] as $product)
	           <tr> <td style="padding:5px 10px; border-right:1px solid #ddd;"">
	           <input type="hidden" name="package_id[]" value="{{@$product['package_id']}}">
	           <input type="hidden" name="weight[]" value="{{@$product['weight']}}">
	            <input type="hidden" name="weight_unit[]" value="{{@$product['weight_unit']}}">
	           {{@$product['package_id']}}</td>
	           <td align="left" style="padding:5px 10px; border-right:1px solid #ddd;"">
	            <input type="hidden" name="product_name[]" value="{{@$product['product_name']}}">
	           {{@$product['product_name']}}
	           </td>
	           <td align="left" style="padding:5px 10px; border-right:1px solid #ddd;"">
	           <input type="text" name="Ordered[]" value="{{@$product['qty']}}" ></td>
	           <td align="left" style="padding:5px 10px; border-right:1px solid #ddd;"">
	           <input type="text" name="Delivered[]" value="@if(@$product['Delivered']){{@$product['Delivered']}} @else 0 @endif" ></td>
	           <td align="left" style="padding:5px 10px;">
	           <input type="text" name="Outstanding[]" value="@if(@$product['Outstanding']){{@$product['Outstanding']}} @else 0 @endif" ></td>
	            </tr>
	            @endforeach

                 @else
	           <tr>
	           <td>
	           <input type="hidden" name="package_id[]" value="{{@$info['PackageNumber']}}">
	           <input type="hidden" name="weight[]" value="N/A">
	           <input type="hidden" name="weight_unit[]" value="">
	           {{@$info['PackageNumber']}}
	           </td>
	           <td>
	            <input type="hidden" name="product_name[]" value="{{@$info['ProductTitle']}}">
	           {{@$info['ProductTitle']}}
	           </td>
	           <td>
	           <input type="text" name="Ordered[]" value="@if(@$info['qty']){{@$info['qty']}}@else 1 @endif" style="width: 60px;"></td>
	           <td><input type="text" name="Delivered[]" value="0" style="width: 60px;"></td>
	            <td><input type="text" name="Outstanding[]" value="0" style="width: 60px;"></td>
	            </tr>



                @endif
	           </tbody>
	           </table>
			   </div> </div>
			   <div class="clearfix"></div>
			   <div class="clearfix"></div>
	           <div class="col-sm-12">
	           <h5 style="margin-bottom:15px; margin-top:0; word-break: break-all;">PACKED BY:&nbsp;{{-- {{@Auth::user()->UserName}} --}} <input type="text" placeholder="" name="packedBy" value="{{@$info['packedBy']}}" style="padding:0px 0px; margin-top:-10px; text-align:left; height:17px; border:none; border-bottom:1px solid #ddd; width:65%;"></h5>

	           <h5 style="margin-bottom:20px; word-break: break-all;">DELIVERED BY:&nbsp;{{-- {{@$info->TransporterName}} --}}<input type="text" placeholder="" name="deliveredBy" value="{{@$info['deliveredBy']}}" style="padding:0px 0px; margin-top:-10px; height:17px; text-align:left; border:none; border-bottom:1px solid #ddd; width:65%;"></h5>
	            <h5 style="margin-bottom:10px;">RECEIVED IN GOOD CONDITION EXCEPT AS NOTED BY(NAME and SIGNATURE): <input type="text" placeholder="" name="signature" value="{{@$info['signature']}}" style="padding:0px 0px; margin-top:-10px; height:17px; text-align:left; border:none; border-bottom:1px solid #ddd; width:150px"></h5>
	            </div>
               <div class="col-sm-12 col-xs-12">
				   <br>
                {{-- <a href="{{url('/admin/slip-management')}}" class="btn slip-btn">Reset Slip</a> --}}
               <div class="col-sm-2">
               <input type="reset" class="btn slip-btn" value="Reset Slip" style="height:34px; border:none;">
               </div>
		       <div class="col-sm-2"><button type="submit" class="btn slip-btn"> Create Slip</button></div>
			</div>

           		{!! Form::close() !!}

	           <div class="col-sm-12 text-center"> <br></br><br><br></br><br>
	           <h4 class="text-upparcase"><b>Thank you for your business!</b></h4>
	            <p>Should you have any enquiries concerning this delivery, please contact us at:</p>
	            <p>Email: <a href="mailto:support@aquantuo.com" class="anchor-blue">support@aquantuo.com</a>
	            or call us at: Ghana: <a href="tel:0302434505" class="anchor-blue">030 243 4505</a>
	            US: <a href="tel:8886522233" class="anchor-blue">888 652 2233</a>.</p>

	             </div>
	             </div>
	             </div>
	             </div>
	             	@endif
	</div>
  	</div> <!-- Panel Body -->


	</div>
</div>

@include('Admin::layouts.footer')
@stop
		{!! Html::style('theme/admin/choose-multiple/bootstrap-chosen.css') !!}
		{!! Html::style('theme/date-time-picker/jquery.datetimepicker.css') !!}

 <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        {!! Html::script('theme/date-time-picker/jquery.datetimepicker.js') !!}
		{!! Html::script('theme/admin/choose-multiple/chosen.jquery.js') !!}


<script type="text/javascript">
jQuery(document).ready(function ($) {
$(".chosen-select").chosen({no_results_text: "Oops, nothing found!"})
});


jQuery(document).ready(function ($) {
		$('.DispatchDate').datetimepicker({
			format:'M d Y h:i a',
			timepicker:true,
			scrollInput : false,
		});
	});


function myFunction()
{
var name = $('#RequesterName').val();

  $.ajax({
        url: SITEURL+'admin/slip-orderId',
        data: { 'name' : name},
        type: "post",
        cache: false,
        success: function (res) {
        var obj = jQuery.parseJSON(res);

        var HtmalVal ='';
        $("#orderId").empty();

        for (var i=0; i<= obj.length; i++)
        {
         var HtmalVal ="<option value='"+obj[i].PackageNumber+"'>"+obj[i].PackageNumber+"</option>"
          $("#orderId").append(HtmalVal);
        }

     }

    });

}

</script>
