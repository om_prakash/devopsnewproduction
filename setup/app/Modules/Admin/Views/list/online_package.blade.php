@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')

@section('content')

{!! Html::style('theme/date-time-picker/jquery.datetimepicker.css') !!}

{!! Html::script('theme/date-time-picker/jquery.datetimepicker.js') !!}

<div class="container-fluid">
<div class="panel panel-default">

<?php
if (Request::segment(2) == 'buy-for-me') {
	//$val = 'Item Purchased';
	$st_title = "Pending Purchase";
	$val = 'Item Received / Purchased';
} else {
	$st_title = "Waiting for Item to be received";
	//$val = 'Item Received';
	$val = 'Item Received / Purchased';
}
?>
  <div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;<b>{{$title}}</b></div>
  <div class="panel-body">
		{!! Form::open(['method' => 'get']) !!}
			<div class="col-md-12 col-sm-12 col-xs-12">
        		<div class="form-inline">
					<div class="form-group">
						{!! Form::label('Status', 'Category', ['class'=>'control-lable']) !!}
						<div><?php
$category_list = array('' => 'Select Category');
foreach ($category as $key) {
	$category_list[$key['Content']] = $key['Content'];
}?>

							{!! Form::select('category',$category_list,Input::get('category'),	['class'=>'form-control','style' => 'width:240px;']); !!}
						</div>
	                </div>
					<div class="form-group">
						{!! Form::label('Status', 'Status', ['class'=>'control-lable']) !!}
						<div>
							{!! Form::select('status',array(
				             ''		    =>	'Select Status',
				             'accepted'		=> 'Accepted',
				             'assign'		=> 'Transporter Assigned',
				             'cancel'		=> 'Cancel',
				             'delivered'	=> 'Delivered',
				             'no_delivery'		=> 'No Delivery',
				             'out_for_pickup'   => 'Out for Pickup',
				             'out_for_delivery' => 'Out for Delivery',
				             'pending'  		=> 'Pending',
				             'momo_initiated' => 'MoMo Initiated',
				             'paid'		=> $st_title,
				             'purchased'	=> $val,
				             'ready' 			=> 'Ready',
				             'reviewed'	=> 'Reviewed',
				             ),
							    Input::get('status'),	['class'=>'form-control','style' => 'width:240px;']);
							!!}
						</div>
	                </div>
	                <div class="form-group">
						{!! Form::label('address', 'Package ID/Package Name/Drop off Address/Requester Name', ['class'=>'control-lable']) !!}
						<div>
							{!! Form::text('search_value', Input::get('search_value'), ['class'=>'form-control', 'placeholder'=> 'Package ID/Package Name/Drop off Address/Requester Name','id' => 'address1','style' => 'width:450px;']) !!}
						</div>
					</div>
					<div class="clearfix"></div>
					<br>
					<div class="form-group">
						{!! Form::label('from', 'Created Date', ['class'=>'control-lable']) !!}
						<div>
							{!! Form::text('StartDate', Input::get('StartDate'), ['class'=>'form-control', 'placeholder'=> 'Created Date','id' => 'startDate','readonly' => 'readonly','style' => 'width:240px;']) !!}
						</div>
	                </div>
	                <div class="form-group">
						{!! Form::label('', '', ['class'=>'control-lable']) !!}
						{!! Form::submit('Search', ['class' => 'btn btn-primary mt-10']) !!}
	                </div>
	                <div class="form-group">
						<a href="{{URL::to('admin/'.Request::segment(2))}}" class="btn btn-primary mt-10">
						<span aria-hidden="true" class="glyphicon glyphicon-refresh">

						</span>&nbsp;&nbsp;Reset
                    </a>
	                </div>
               <?php if (Request::segment(2) == 'online-package') {?>
	                <div class="form-group">
						<a href="{{url('admin/on-line-purchases')}}" class="btn btn-primary mt-10">
						<i class="fa fa-plus"></i>&nbsp;&nbsp;Add Online Purchase
                    </a>
	                </div>
				<?php }?>

				<?php if (Request::segment(2) == 'buy-for-me') {?>
	                <div class="form-group">
						<a href="{{url('admin/post-buy-for-me')}}" class="btn btn-primary mt-10">
						<i class="fa fa-plus"></i>&nbsp;&nbsp;Add Buy For Me
                    </a>
	                </div>
				<?php }?>
				<div class="clearfix"></div><br>
					<div class="inline-form pull-right">
						{!! Form::label('', '', ['class'=>'control-lable','id'=>'TotalRecordFound']) !!}
					</div>
			</div>


	<div class="clearfix"></div>


      <!-- Pagination Section-->
	<input type="hidden" name="urlvalue" id="urlvalue" value="{{$paginationurl}}" />
	<input type="hidden" name="postvalue" id="postvalue" value="{{$postvalue}}" />
	<input type="hidden" name="orderby" id="orderby" value="{{$orderby}}" />
	<input type="hidden" name="orderType" id="orderType" value="{{$orderType}}" />
	<div id="containerdata"></div>
	<div class="clearfix"></div>
	{!! Form::close() !!}



</div> <!-- Panel Body -->
</div>

</div>
</div>

<!--requester detail-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="close_modal"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">User Detail</h4>
      </div>

      <div class="modal-body my-modal-body">
	  	<div class="row">
          <div class="col-sm-4">
			<div id="profile_image">
				<p>Profile Image</p>
			</div>
            <script type="text/javascript">
              $(document).ready(function() {
                $(".fancybox").fancybox();
              })
            </script>
          </div>
          <div class="col-sm-8" id="actions" style="position: relative;text-align: center;vertical-align: middle;line-height: 90px;">
            
          </div>
        </div>



      	<div class="table-responsive">
      		<table class="table table-hover table-striped my-table">
            	<tr>
                	<td>Name:</td>
                    <td><p id="name"></p></td>
                </tr>
            	<tr>
                	<td>Email:</td>
                    <td><p id="email"></p></td>
                </tr>
                <tr>
                	<td>Phone Number:</td>
                    <td><p id="phone"></p></td>
                </tr>
                <tr>
                  <td>Alternate Phone Number:</td>
                    <td> <p id="phone2"></p></td>
                </tr>
                <tr>
                	<td>Address line-1:</td>
                    <td><p id="street1"></p></td>
                </tr>
                <tr>
                	<td>Address line-2:</td>
                    <td> <p id="street2"></p></td>
                </tr>
                <tr>
                	<td>Country:</td>
                    <td><p id="detail_country"></p></td>
                </tr>
                <tr>
                	<td>State:</td>
                    <td><p id="detail_state"></p></td>
                </tr>
                <tr>
                	<td>City:</td>
                    <td> <p id="detail_city"></p></td>
                </tr>
                 <tr>
                	<td>Zip Code:</td>
                    <td><p id="detail_zipcode"></p></td>
                </tr>
				<tr>
                	<td> Unit#: </td>
                	<td style="color: #f00;"> <p id="unitHash"></p> </td>
                </tr>
            </table>
      	</div>

      	<div id="no_user" style="display:none;"><center>User not found.</center></div>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
<!-- end -->

<div id="myCommentModal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="exampleModalLabel">Add Comment</h4>
			</div>
			<div class="col-sm-12 ">
				{!! Form::model('', ['name' => 'commentForm', 'id' => 'commentForm', 'method' => 'POST', 'url' => ['admin/post-user-comment']]) !!}
			
					<div class="modal-body clearfix">
						<div class="form-group" id="cityDiv">
							{!! Form::label('Comment', 'Comment:',['class' => 'control-label']) !!}
							{!! Form::textarea('comment', '', ['class'=>'form-control', 'placeholder'=> 'Comment','id' =>'comment','rows'=>'3']) !!}
							<p class="help-block red" id="er_comment"></p>
						</div>
					</div>
					
					<input type="hidden" name="user_id" id="user_id" value="" />
					<input type="hidden" name="request_id" id="request_id" value="" />
					<input type="hidden" name="product_title" id="product_title" value="" />

					<div class="modal-footer">
						{!! Form::button('Close', ['class' => 'btn btn-primary', 'data-dismiss'=>'modal']) !!}
						{!! Form::submit('Add', ['class' => 'btn btn-primary']) !!}
					</div>

				{!! Form::close() !!}
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<script>
function get_user_info(id)
{
	var url = SITEURL+'ajax/get_user_info/'+id;
	$('#loading').show();
	$.ajax({
		type:"GET",
		url:url,
		//headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		success: function(res)
		{	$('#loading').hide();
			if(res.success == 0){
				$(".table-responsive").hide();
				$("#profile_image").html('');
				$("#no_user").show();


			}else{
				$(".table-responsive").show();
				$("#no_user").hide();
				$("#name").html(res.result.Name);
				$("#user_id").val(res.result._id);
				$("#request_id").val(res.result._id);
				$("#product_title").val(res.result.Name);
				$("#email").html(res.result.Email);
				$("#unitHash").html(res.result.UniqueNo);
				$("#phone").html(res.result.PhoneNo);
				$("#phone2").html(res.result.AlternatePhoneNo);
				$("#street1").html(res.result.Street1);
				$("#street2").html(res.result.Street2);
				$("#detail_country").html(res.result.Country);
				$("#detail_state").html(res.result.State);
				$("#detail_city").html(res.result.City);
				$("#detail_zipcode").html(res.result.ZipCode);
				if(res.result.Image == ''){
					$("#profile_image").html('<img class="thumbnail" src="{{ImageUrl}}/profile/noImageAvailable.jpg" height="100px" width="100px" alt="Profile image">');
				}else{
					$("#profile_image").html('<img class="thumbnail" src="{{ImageUrl}}/'+res.result.Image+'" height="100px" width="100px" alt="Profile image">');
				}
				$("#actions").html('<a href="/admin/user-comment-list/'+res.result._id+'" data-placement="top" title="View Notes/Comments" class="btn btn-small btn-primary btn-action"><i class="fa fa-th-list"></i></a>\
				<a href="/admin/requester/edit/'+res.result._id+'" data-placement="top" title="Edit" class="btn btn-small btn-primary btn-action" style="margin-right: 10;"><i class="fa fa-edit"></i></a><br />\
				<a href="#" data-toggle="modal" data-target="#myCommentModal" data-placement="top" title="Add Notes/Comments" class="btn btn-small btn-info"><i class="fa fa-plus"></i> Add Notes/Comments</a>');
			}
		}
	});
}




	$(function(){
		$('#startDate').datetimepicker({
			format:'d/m/Y',
			datepicker:true,

			/*onChangeDateTime:function( ct ){
				$('#endDate').datetimepicker({	minDate:ct	})
			},*/
			timepicker:false,

		});

		/*$('#endDate').datetimepicker({
			format:'d/m/Y',
			onChangeDateTime:function( ct ){
				$('#startDate').datetimepicker({	maxDate:ct	})
			},
			timepicker:false,
		});*/
	});
</script>


<!-- Modal -->
<div class="modal fade" id="refund-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Refund</h4>
      </div>
      <div id="refund-data" class="msg_stripe"></div>
    </div>
  </div>
</div>


<!-- Modal -->
<div class="modal fade" id="ipay-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Ipay Payment Status</h4>
      </div>

      <div class="modal-body">
      	<div id="ipay_html" class=""></div>
      	<div id="ipay_bt" class=""></div>
      </div>
      
    </div>
  </div>
</div>


<button id="model_bt" data-toggle="modal" data-target="#ipay-modal" style="display: none;">&nbsp;</button>
@include('Admin::layouts.footer')
@stop

<script>

function getIpayStatus(id)
{	
	if(id != ''){
		$('#loading').show();
		var url = SITEURL+'admin/ajax/getIpayStatus';
		var url2 = SITEURL+'admin/ajax/changeIpayStatus/'+id;
		$.ajax({
			type:"POST",
			url:url,
			data:'id='+id,
			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			success: function(res)
			{
				$('#loading').hide();
				if(res.success == 1){
					//alert(res.show_popup);
					if(res.show_popup == 1){
						
						$("#ipay_html").html(res.msgres);
						$("#ipay_bt").html('<a class="btn btn-success btn-info btn-action" href="'+url2+'" >Change Status to Pending</a>');
						$("#model_bt").trigger('click');
					}else{
						alert(res.msgres);
					}
					
				}else{
					alert(res.msg);
				}
				
			}
		});
	}
	return false;
}





function get_package_information(id)
{
	var url = SITEURL+'ajax/get_package_information/'+id;
	$('#loading').show();
	$.ajax({
		type:"GET",
		url:url,
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		success: function(msg)
		{
			$('#loading').hide();
			$("#refund-data").html(msg);
		}
	});
}
function make_refund(id)
{
	var refundAmount =$("#refund_amount").val();
	 if(refundAmount =="")
		{
			 flag = false;
			 $("#refund_amountdiv").addClass("has-error");
			 $("#refund_amountError").html('Refund amount field is required.');
		}
		else
		{

		  if(!$.isNumeric(refundAmount))  //if(Number(refundAmount)<0)
		  {
			$("#refund_amountdiv").addClass("has-error");
			$("#refund_amountError").html('Refund amount is only positive number.');
		  }
		   else
		   {
				$("#refund_amountError").html('');
				//var id = "5628e4886734c4a0637dd22c";
				var url = SITEURL+'ajax/get_refund_information/'+id+'/'+refundAmount;
				$('#loading').show();
				$.ajax({
					type:"GET",
					url:url,
					dataType: "json",
					headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
					success: function(msg)
					{
						var json1 = JSON.parse(msg);
						//console.log(json1);
						if(json1.error.type =="invalid_request_error")
						{
							var respons = '<p class="error_msg">'+json1.error.message+'</p>';
						}
						else if(json1.status =="succeeded")
						{
							var respons = '<p class="success_msg">'+"Your amount is refunded"+'</p>';
						}
						else
						{
							var respons = "Your stripe id valid";
						}
						$('#loading').hide();
						$("#refund-data").html(respons);
					}
				});
		   }
		}
}
</script>

