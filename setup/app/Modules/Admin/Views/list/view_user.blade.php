@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')

@section('content')

<div class="container-fluid">
<div class="panel panel-default">
  <div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;<b>User List</b>
    <a style="margin-right:5px;" class="btn btn-primary margin-top-less-7 pull-right" href="{{url('admin/notification')}}">Go Back</a>
  </div>

  <div class="panel-body">
		{!! Form::model('', ['method' => 'GET']) !!}
        	<div class="inline-form">
				<div class="form-group">

				{!! Form::label('UserFirstName', 'User Name', ['class'=>'control-lable']) !!}

				{!! Form::text('search_value', Input::get('search_value'), ['class'=>'form-control', 'placeholder'=> 'First Name','id' =>'inputError1']) !!}

                 </div>
            </div>
            <div class="inline-form">
                <div class="form-group">
                {!! Form::submit('Search', ['class' => 'btn btn-primary']) !!}
                </div>
                </div>

		{!! Form::close() !!}

	<div class="clearfix"></div>


      <!-- Pagination Section-->
	<input type="hidden" name="urlvalue" id="urlvalue" value="{{$paginationurl}}" />
	<input type="hidden" name="postvalue" id="postvalue" value="{{$postvalue}}" />
	<input type="hidden" name="orderby" id="orderby" value="{{$orderby}}" />
	<input type="hidden" name="orderType" id="orderType" value="{{$orderType}}" />
	<div id="containerdata"></div>
	<div class="clearfix"></div>


  </div> <!-- Panel Body -->
</div>


</div>

@include('Admin::layouts.footer')
@stop
