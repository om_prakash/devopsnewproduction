@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')
@section('content')
{!! Html::script('theme/admin/custome/js/validation.js') !!}


<div class="container-fluid">

<div class="panel panel-default">
  <div class="panel-heading"><i class="fa fa-cog"></i>&nbsp;<b>Configuration</b></div>
  <div class="panel-body">
  	<div class="col-md-12">
  		{!! Form::model('', ['name' => 'configurationForm', 'id' => 'configurationForm','method' => 'POST','id'=>'configurationForm', 'url' => ['admin/configuration']]) !!}
  		<div class="col-md-12">
				<div class="col-md-3" style="text-align:right">
					 {!! Form::label('aquantuo_fees', 'Aquantuo Fees (%):') !!}
				</div>
				<div class="col-md-9">
				    <div class="col-md-3">
			            <div class="form-group {{ $errors->has('aquantuo_fees') ? 'has-error' : '' }}">
					        {!! Form::text('aquantuo_fees', $info->aquantuoFees, ['class'=>'form-control', 'placeholder'=> 'Aquantuo Fees','id' =>'inputError1']) !!}
					        @if ($errors->has('aquantuo_fees')) <p class="help-block">{{ $errors->first('aquantuo_fees') }}</p> @endif
			            </div>
			       </div>
		      </div>

          <div class="clearfix"></div>

          <div class="col-md-3" style="text-align:right">
              {!! Form::label('processing_fees', 'Processing fees (%):') !!}
          </div>
          <div class="col-md-9">
            <div class="col-md-3">
                  <div class="form-group {{ $errors->has('processing_fees') ? 'has-error' : '' }}">
                  {!! Form::text('processing_fees', $info->ProcessingFees, ['class'=>'form-control', 'placeholder'=> 'Aquantuo Fees','id' =>'inputError1']) !!}
                  @if ($errors->has('processing_fees')) <p class="help-block">{{ $errors->first('processing_fees') }}</p> @endif
                  </div>
             </div>
          </div>
          <div class="clearfix"></div>

          <div class="col-md-3" style="text-align:right">
            {!! Form::label("dutyCustoms", "Duty/Customs Clearing (%):") !!}
          </div>
          <div class="col-md-9">
            <div class="col-md-3">
              <div class="form-group {{ $errors->has('dutyCustoms') ? 'has-error' : '' }}">
                {!! Form::text("dutyCustoms", $info->duty_customs, ["class" => "form-control", "id" => "inputError1"]) !!}
                @if ($errors->has("dutyCustoms"))
                  <p class="help-block"> {{ $errors->first("dutyCustoms") }} </p>
                @endif
              </div>
            </div>
          </div>
          <div class="col-md-3" style="text-align:right">
            {!! Form::label("Insurance", "Insurance (%):") !!}
          </div>
          <div class="col-md-9">
            <div class="col-md-3">
              <div class="form-group {{ $errors->has('main_insurance') ? 'has-error' : '' }}">
                {!! Form::text("main_insurance", $info->main_insurance, ["class" => "form-control", "id" => "inputError1"]) !!}
                @if ($errors->has("main_insurance"))
                  <p class="help-block"> {{ $errors->first("main_insurance") }} </p>
                @endif
              </div>
            </div>
          </div>
          
          <div class="clearfix"></div>
          
           <div class="col-md-3" style="text-align:right">
            {!! Form::label("BFM Concierge", "BFM Concierge (%):") !!}
          </div>
          <div class="col-md-9">
            <div class="col-md-3">
              <div class="form-group {{ $errors->has('bfm_concierge') ? 'has-error' : '' }}">
                {!! Form::text("bfm_concierge", $info->bfm_concierge, ["class" => "form-control", "id" => "inputError1"]) !!}
                @if ($errors->has("bfm_concierge"))
                  <p class="help-block"> {{ $errors->first("bfm_concierge") }} </p>
                @endif
              </div>
            </div>
          </div>

          <div class="col-md-3" style="text-align:right">
            {!! Form::label("tax", "Tax (%):") !!}
          </div>
          <div class="col-md-9">
            <div class="col-md-3">
              <div class="form-group {{ $errors->has('tax') ? 'has-error' : '' }}">
                {!! Form::text("tax", $info->tax, ["class" => "form-control", "id" => "inputError1"]) !!}
                @if ($errors->has("tax"))
                  <p class="help-block"> {{ $errors->first("tax") }} </p>
                @endif
              </div>
            </div>
          </div>
          <div class="clearfix"></div>

          <div class="col-md-3" style="text-align:right">
              {!! Form::label('aquantuo_fees2', 'Aquantuo Local Delivery Fees (%):') !!}
          </div>
          <div class="col-md-9">
            <div class="col-md-3">
                  <div class="form-group {{ $errors->has('aquantuo_fees_ld') ? 'has-error' : '' }}">
                  {!! Form::text('aquantuo_fees_ld', $info->aquantuo_fees_ld, ['class'=>'form-control', 'placeholder'=> 'Aquantuo Local Delivery Fees','id' =>'inputError12']) !!}
                  @if ($errors->has('aquantuo_fees_ld')) <p class="help-block">{{ $errors->first('aquantuo_fees_ld') }}</p> @endif
                  </div>
             </div>
          </div>
          <div class="clearfix"></div>

          <div class="col-md-3" style="text-align:right">
            {!! Form::label("warehouse_transit_fee", "Warehouse Transit Processing Fee ($):") !!}
          </div>
          <div class="col-md-9">
            <div class="col-md-3">
              <div class="form-group {{ $errors->has('warehouse_transit_fee') ? 'has-error' : '' }}">
                {!! Form::text("warehouse_transit_fee", $info->warehouse_transit_fee, ["class" => "form-control", "id" => "inputError1"]) !!}
                @if ($errors->has("warehouse_transit_fee"))
                  <p class="help-block"> {{ $errors->first("warehouse_transit_fee") }} </p>
                @endif
              </div>
            </div>
          </div>
          <div class="clearfix"></div>

          <!-- Kenya fee configure -->

          <div class="col-md-3" style="text-align:right">
            {!! Form::label("kenya_rate_per_kilogram", "Kenya - rate per kilogram:") !!}
          </div>
          <div class="col-md-9">
            <div class="col-md-3">
              <div class="form-group {{ $errors->has('kenya_rate_per_kilogram') ? 'has-error' : '' }}">
                {!! Form::text("kenya_rate_per_kilogram", $info->kenya_rate_per_kilogram, ["class" => "form-control", "id" => "inputError1"]) !!}
                @if ($errors->has("kenya_rate_per_kilogram"))
                  <p class="help-block"> {{ $errors->first("kenya_rate_per_kilogram") }} </p>
                @endif
              </div>
            </div>
          </div>
          <div class="clearfix"></div>

          <div class="col-md-3" style="text-align:right">
            {!! Form::label("kenya_per_item_value", "Kenya - % of item value (valuable items):") !!}
          </div>
          <div class="col-md-9">
            <div class="col-md-3">
              <div class="form-group {{ $errors->has('kenya_per_item_value') ? 'has-error' : '' }}">
                {!! Form::text("kenya_per_item_value", $info->kenya_per_item_value, ["class" => "form-control", "id" => "inputError1"]) !!}
                @if ($errors->has("kenya_per_item_value"))
                  <p class="help-block"> {{ $errors->first("kenya_per_item_value") }} </p>
                @endif
              </div>
            </div>
          </div>
          <div class="clearfix"></div>

          <div class="col-md-3" style="text-align:right">
            {!! Form::label("kenya_minimum_item_value", "Kenya minimum valuable item value ($):") !!}
          </div>
          <div class="col-md-9">
            <div class="col-md-3">
              <div class="form-group {{ $errors->has('kenya_minimum_item_value') ? 'has-error' : '' }}">
                {!! Form::text("kenya_minimum_item_value", $info->kenya_minimum_item_value, ["class" => "form-control", "id" => "inputError1"]) !!}
                @if ($errors->has("kenya_minimum_item_value"))
                  <p class="help-block"> {{ $errors->first("kenya_minimum_item_value") }} </p>
                @endif
              </div>
            </div>
          </div>
          <div class="clearfix"></div>

          <!-- End Kenya fee configure -->

				  <div class="col-md-3" style="text-align:right">
				    {!! Form::label('aquantuo_fees', 'Map Area for New Requset: ') !!}
				  </div>
			    <div class="col-md-9">
			      <div class="col-md-3">
			          <div class="form-group {{ $errors->has('CoverageArea') ? 'has-error' : '' }}">

                           {!! Form::text('CoverageArea', $info->CoverageArea, ['class'=>'form-control', 'placeholder'=> 'Area','id' =>'inputError1']) !!}
                           @if ($errors->has('CoverageArea')) <p class="help-block">{{ $errors->first('CoverageArea') }}</p> @endif
                          <p>Note: Area in meter</p>
                </div>
		        </div>
		      </div>
		    <div class="clearfix"></div>

        <div class="col-md-3" style="text-align:right">
            {!! Form::label('msg', 'Message: ') !!}
          </div>
          <div class="col-md-9">
            <div class="col-md-8">
                <div class="form-group {{ $errors->has('msg') ? 'has-error' : '' }}">

                <textarea class="form-control" placeholder="Message" id='' name="msg" cols="4" rows="4">{{$info->message}}</textarea>

                           @if ($errors->has('msg')) <p class="help-block">{{ $errors->first('msg') }}</p> @endif

                </div>
            </div>
          </div>
        <div class="clearfix"></div>



     <div class="form-group">
        <div class="col-md-3" style="text-align:right">{!! Form::label('Insurance', 'Insurance: ') !!}</div>
        <div class="col-md-9">
          <div class="col-md-3">{!! Form::label('min_cost', 'Minimum Cost') !!}</div>
          <div class="col-md-3">{!! Form::label('max_cost', 'Maximum Cost') !!}</div>
          <div class="col-md-3">{!! Form::label('insurancecost_cost', 'Insurance Cost') !!}</div>
        </div>
        </div>
        <?php if (count($info['Insurance']) == 0) {?>
			 <div class="col-md-9 col-md-offset-2 ">
                <div class="col-md-3">
                  <div class="form-group"><input type ="text" name="MinimumCost[]" placeholder="Minimum Cost" class="form-control required validation" id = "MinimumCost"></div></div>
                  <div class="col-md-3"><div class="form-group"><input type="text" name="MaximumCost[]" placeholder="Maximum Cost" class="form-control required validation" id = "MaximumCost"/></div></div>
                  <div class="col-md-3"><div class="form-group"><input type="text" name="InsuranceCost[]" placeholder="Insurance Cost" class="form-control required validation" id = "InsuranceCost"/></div></div>
                  </div>
            <?php }?>
        <?php foreach ($info['Insurance'] as $key => $val) {?>
              <div class="col-md-9 col-md-offset-3 ">
                <div class="col-md-3">
                  <div class="form-group validation"><input type ="text" name="MinimumCost[]"  value="<?php echo $val['MinPrice']; ?>" placeholder="Minimum Cost" class="form-control required" id = "MinimumCost"></div></div>
                  <div class="col-md-3"><div class="form-group validation"><input type="text" name="MaximumCost[]" value="<?php echo $val['MaxPrice']; ?>" placeholder="Maximum Cost" class="form-control required" id = "MaximumCost"/></div></div>
                  <div class="col-md-3"><div class="form-group validation"><input type="text" name="InsuranceCost[]" value="<?php echo $val['Rate']; ?>" placeholder="Insurance Cost" class="form-control required" id = "InsuranceCost"/></div></div>
                   <a href="javascript:void(0);" class="remove_button1 btn btn-danger" title="Remove field"  ><i class="fa fa-trash-o"></i></a></div>
        <?php }?>

           <div class="add-more"></div>
           <div class="col-md-9 col-md-offset-3 ">
                 <div class="col-md-3">
                     <div class="form-group"><a href="javascript:void(0);" class="add_button btn btn-success" title="Add field">Add More</a></div></div></div>
           <div class="clearfix"></div>
           <div class="col-md-9 col-md-offset-3 ">
		       <div class="col-md-3">
			       <div class="form-group">
					 {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
				 </div>
			</div>
		</div>
       {!! Form::close() !!}
    </div>
</div>
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>
var float_expr = /^[0-9\.]+$/;

$("#configurationForm").submit(function()
{
  var flag = true;
  $('.validation-row').remove();
$('.required').each(function(){


		if ($(this).val() == '') // only add if not added
    {

    $(this).parent().after("<div class='validation validation-row' style='color:red;margin-bottom: 20px;'>This field is required</div>");
		 flag = false;
		}
	else if(float_expr.test($(this).val()) == false){
    $(this).parent().after("<div class='validation validation-row' style='color:red;margin-bottom: 20px;'>The field must have an integer value.</div>");
     flag = false;
   }

});
 return flag;
});
</script>

<script type="text/javascript">
$(document).ready(function(){
    //var maxField = 10; //Input fields increment limitation
    var addButton = $('.add_button'); //Add button selector
    var wrapper = $('.add-more'); //Input field wrapper
    var fieldHTML = '<div class="col-md-9 col-md-offset-3 "><div class="col-md-3"><div class="form-group"><input type="text" name="MaximumCost[]" value="" placeholder="Maximum Cost" class="form-control required"/></div></div><div class="col-md-3"><div class="form-group"><input type="text" name="MinimumCost[]" value="" placeholder="Minimum Cost" class="form-control required"/></div></div><div class="col-md-3"><div class="form-group"><input type="text" name="InsuranceCost[]" value="" placeholder="Insurance Cost" class="form-control required"/></div></div><a href="javascript:void(0);" class="remove_button btn btn-danger" title="Remove field" onclick ="return confirm("Are you sure? You want to remove!")"><i class="fa fa-trash-o"></i></a></div></div></div></div>'; //New input field html
    var x = 1; //Initial field counter is 1
    $(addButton).click(function(){ //Once add button is clicked
        //if(x < maxField){ //Check maximum number of input fields
            x++; //Increment field counter
            $(wrapper).append(fieldHTML); // Add field html

    });
    $(wrapper).on('click', '.remove_button', function(e){ //Once remove button is clicked

        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
    });
    $(".remove_button1").on('click',  function(e){ //Once remove button is clicked
       if(confirm('Are you sure? You want to Remove!'))
       {
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
	}
     });
});
</script>

@include('Admin::layouts.footer')
@stop
