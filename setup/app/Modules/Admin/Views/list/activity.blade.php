@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')

@section('content')

<div class="container-fluid">
	<div class="panel panel-default">
	  	<div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;<b>Activity Log</b></div>
	  	<div class="panel-body">
        	<div class="col-md-12 row">

      {!! Form::model('', ['method' => 'GET', 'url' => ['admin/activity-log']]) !!}
                <div class="inline-form">
                <div class="form-group">
          {!! Form::label('Package Id', 'Package Id', ['class'=>'control-lable']) !!}
          <div>
             <input type="text" value="{{Input::get('package_id')}}" class="form-control" placeholder="Package Id" name="package_id">
          </div>
                </div>
                </div>

                  <div class="inline-form">
                <div class="form-group">
               {!! Form::label('Request type', 'Request type', ['class'=>'control-lable']) !!}
              <div>
              <select class="form-control" name="request_type" >
                <option value="">Select type</option>
                <option value="buy_for_me" @if(Input::get('request_type') == 'buy_for_me') selected @endif>Buy For Me </option>
                <option value="online" @if(Input::get('request_type') == 'online') selected @endif>Online</option>
                <option value="delivery" @if(Input::get('request_type') == 'delivery') selected @endif >Send A Package</option>
                <option value="local_delivery" @if(Input::get('request_type') == 'local_delivery') selected @endif >Local Delivery</option>
              </select>
          </div>
                </div>
                </div>

                 <div class="inline-form">
                <div class="form-group">
               {!! Form::label('Status', 'Status', ['class'=>'control-lable']) !!}
              <div>
              <select class="form-control" name="status" >
                <option value="">Select status</option>
                <option value="pending" @if(Input::get('status') == 'pending') selected @endif>Pending</option>
                <option value="ready" @if(Input::get('status') == 'ready') selected @endif>Ready</option>
                <option value="reviewed" @if(Input::get('status') == 'reviewed') selected @endif>Reviewed</option>
                <option value="not_purchased" @if(Input::get('status') == 'not_purchased') selected @endif>Not Purchased</option>
                <option value="paid" @if(Input::get('status') == 'paid') selected @endif>Paid</option>
                <option value="purchased" @if(Input::get('status') == 'purchased') selected @endif>Purchased</option>
                <option value="item_received" @if(Input::get('status') == 'item_received') selected @endif>Item Received</option>
                <option value="assign" @if(Input::get('status') == 'assign') selected @endif>Assign</option>
                <option value="modified" @if(Input::get('status') == 'modified') selected @endif>Modified</option>
                <option value="out_for_pickup" @if(Input::get('status') == 'out_for_pickup') selected @endif>Out For Pickup</option>
                <option value="out_for_delivery" @if(Input::get('status') == 'out_for_delivery') selected @endif>Out For Delivery</option>
                <option value="delivered" @if(Input::get('status') == 'delivered') selected @endif>Delivered</option>
                <option value="cancel_by_admin" @if(Input::get('status') == 'cancel_by_admin') selected @endif>Cancel By Admin</option>
              </select>
          </div>
                </div>
                </div>

                  <div class="inline-form">
				<div class="form-group">
					{!! Form::label('StartDate', 'From', ['class'=>'control-lable']) !!}
					<div>
						{!! Form::text('StartDate', Input::get('StartDate'), ['class'=>'form-control', 'placeholder'=> 'From','id' => 'startDate','readonly'=> 'readonly']) !!}
					</div>
                    </div>
                </div>

                <div class="inline-form">
                <div class="form-group">
					{!! Form::label('EndDate', 'To', ['class'=>'control-lable']) !!}
					<div>
						{!! Form::text('EndDate', Input::get('EndDate'), ['class'=>'form-control', 'placeholder'=> 'To','id' => 'endDate','readonly'=> 'readonly']) !!}
					</div>
                </div>
                </div>


                <div class="inline-form">
                <div class="form-group">
          {!! Form::label('', '', ['class'=>'control-lable']) !!}
          {!! Form::submit('Search', ['class' => 'btn btn-primary']) !!}

          <a href="{{URL::to('admin/activity-log')}}" class="btn btn-primary">
            <span aria-hidden="true" class="glyphicon glyphicon-refresh">
            </span>&nbsp;&nbsp;Reset
                    </a>

                    </div>
        </div>

    {!! Form::close() !!}
         </div>
		<div class="clearfix"></div>
	      <!-- Pagination Section-->
		<input type="hidden" name="urlvalue" id="urlvalue" value="{{$paginationurl}}" />
		<input type="hidden" name="postvalue" id="postvalue" value="{{$postvalue}}" />
		<input type="hidden" name="orderby" id="orderby" value="{{$orderby}}" />
		<input type="hidden" name="orderType" id="orderType" value="{{$orderType}}" />
		<div id="containerdata"></div>
		<div class="clearfix"></div>
	  	</div> <!-- Panel Body -->
	</div>
</div>
{!! Html::style('theme/date-time-picker/jquery.datetimepicker.css') !!}
{!! Html::script('theme/date-time-picker/jquery.datetimepicker.js') !!}
<script>
	$(function(){
		$('#startDate').datetimepicker({
			format:'d/m/Y',
			onChangeDateTime:function( ct ){
				$('#endDate').datetimepicker({	minDate:ct	})
			},
			timepicker:false,
		});
		$('#endDate').datetimepicker({
			format:'d/m/Y',
			onChangeDateTime:function( ct ){
				$('#startDate').datetimepicker({	maxDate:ct	})
			},
			timepicker:false,
		});
	});
</script>
@include('Admin::layouts.footer')

@stop
