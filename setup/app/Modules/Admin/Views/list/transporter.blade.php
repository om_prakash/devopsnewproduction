@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')
@section('content')
{!! Html::script('theme/admin/custome/js/validation.js') !!}
<div class="container-fluid">

<div class="panel panel-default">
  <div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;<b>Transporter List</b></div>
  <div class="panel-body">
  	<div class="col-md-12 row">
  		<form role="form">
			<div class="inline-form">
				<div class="form-group">
        			{!! Form::label('UserFirstName', 'Transporter Name') !!}
                    <div>
					{!! Form::text('search_value', Input::get('search_value'), ['class'=>'form-control', 'placeholder'=> 'Transporter Name']) !!}
                    </div>
				</div>
			</div>
			<div class="inline-form">
				<div class="form-group">
        			{!! Form::label('UserEmail', 'Email') !!}
                    <div>
					{!! Form::text('search_email', Input::get('search_email'), ['class'=>'form-control', 'placeholder'=> 'Email']) !!}
                    </div>
				</div>
			</div>
			<div class="inline-form">
				<div class="form-group">
        			{!! Form::label('SSN', 'SSN') !!}
                    <div>
					{!! Form::text('search_ssn', Input::get('search_ssn'), ['class'=>'form-control', 'placeholder'=> 'SSN']) !!}
                    </div>
				</div>
			</div>
			<div class="inline-form">
				<div class="form-group">

					{!! Form::label('UserClass', 'Transporter Type') !!}

					{!! Form::select('transporter_type', array(''=>'Select Transporter Type','individual'=>'Individual','business'=>'Business'),Input::get('transporter_type'),['class'=>'form-control']); !!}
				</div>
			</div>
			<div class="inline-form">
				<div class="form-group">
					{!! Form::submit('Search', ['class' => 'btn btn-primary']) !!}
				</div>
			</div>
        </form>
        {!! Form::close() !!}
    </div>
	<div class="clearfix"></div>
      <!-- Pagination Section-->
	<input type="hidden" name="urlvalue" id="urlvalue" value="{{$paginationurl}}" />
	<input type="hidden" name="postvalue" id="postvalue" value="{{$postvalue}}" />
	<input type="hidden" name="orderby" id="orderby" value="{{$orderby}}" />
	<input type="hidden" name="orderType" id="orderType" value="{{$orderType}}" />
	<div id="containerdata"></div>
	<div class="clearfix"></div>


  </div> <!-- Panel Body -->
</div>

</div> <!-- Panel Body -->
</div>
@include('Admin::layouts.footer')
@stop
<script>
function send_verification_code(id){
	if (confirm("Are you sure all verification checks have been completed for the Transporter? Click OK to enable profile!")) {

	$('#verification_section_'+id+' >').addClass('spinning');
	var url = SITEURL+'ajax/verify_user';
	$.ajax
	({
		type: "POST",
		url: url,
		data: "id="+id,
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		success: function(res)
		{
			var obj = eval("("+res+")");
			if(obj.success == 1)
			{
				$('#verification_section_'+id).html('<span class="badge bg-green">Verified</span>');
			}
			alert(obj.msg);

			$('#verification_section_'+id+' >').removeClass('spinning');
		}
	});
	}

}



function add_bank_account(id){
  if (confirm("Are you sure? You want to attach transporter account to your stripe account.")) {
	var url = SITEURL+'ajax/add_account_to_stripe';
	$('#bank_ac_section_'+id+' > span').addClass('spinning');
	$.ajax
	({
		type: "POST",
		url: url,
		data: "id="+id,
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		success: function(res)
		{
		$('#bank_ac_section_'+id+' > span').removeClass('spinning');
			var obj = eval("("+res+")");
			if(obj.success == 1)
			{
				$('#bank_ac_section_'+id).html('<span class="badge bg-green">Added</span>');
			}

			alert(obj.msg);
		}

	});
}
}
function transfer_fund(id)
{
	var url = SITEURL+'ajax/transfer_fund_view';
	$.ajax
	({
		type: "GET",
		url: url,
		data: "id="+id,
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		success: function(res)
		{
			$('#transfer_fund_modal_body').html(res);
			$('#transfer_to').val(id);
		}
	});

}
function transfer_fund_modal_body()
{
	var flag = true;
	if(valid.required('transfer_amount','amount') == false){ flag = false; }else{
		if(valid.numeric('transfer_amount','amount') == false){ flag = false; }
	}
	if(flag == true)
	{
		 if (confirm(" Are you sure? You want to trasfer fund to transporter!")) {
		$.ajax
		({
			type: "POST",
			url: SITEURL+'ajax/transfer_fund_to_carrier',
			data: $('#transfer_fund_modal_body').serialize(),
			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			success: function(res)
			{
				var obj = eval('('+res+')');
				if(obj.success == 1)
				{
					$('#transfer_fund_modal_body')[0].reset();
					$('#fund-error').html('<div role="alert" class="alert alert-success alert-dismissible fade in">'
										+'<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>'+obj.msg+'</div>');

				}else{
					$('#fund-error').html('<div role="alert" class="alert alert-danger alert-dismissible fade in">'
	      				+'<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>'+obj.msg+'</div>');
				}

	}
		});
}
}
	return false;

}
</script>

<div class="modal fade" id="transfer_fund_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Transfer Fund</h4>
      </div>
      {!! Form::model('', ['class' => 'form-horizontal', 'method' => 'POST', 'url' => ['admin/post_app_tutorial'],'id' => 'transfer_fund_modal_body', 'onsubmit'=>'return transfer_fund_modal_body()']) !!}

      {!! Form::close() !!}
    </div>
  </div>
</div>
