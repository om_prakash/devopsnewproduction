@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')

@section('content')
{!! Html::style('theme/admin/custome/css/style.min.css') !!}
<div class="container-fluid">
	<div class="panel panel-default">
		<div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;<b>Item value</b></div>
		<div class="panel-body">
				{!! Form::model('', ['method' => 'get', 'url' => ['admin/item-value']]) !!}
					<div class="inline-form">
						<div class="form-group">
							{!! Form::label('Price', 'Price', ['class'=>'control-lable']) !!}
							{!! Form::text('search_value', Input::get('search_value'), ['class'=>'form-control', 'placeholder'=> 'Price','id' =>'inputError1']) !!}
						</div>
					</div>

					<div class="inline-form">
						<div class="form-group">
							{!! Form::label('Country', 'Country', ['class'=>'control-lable']) !!}
							<select name="country" id="country" class="form-control">
							   <option>Select Country</option>
							   @foreach($country as $value)
							   <option value="{{ $value->_id }}">{{ ucfirst($value->Content) }}</option>
							   @endforeach
							</select>
						</div>
					</div>

					<div class="inline-form">
						<div class="form-group">
							<label></label>
							{!! Form::submit('Search', ['class' => 'btn btn-primary']) !!}

							<a href="{{URL::to('admin/item-value')}}" class="btn btn-primary">
								<span aria-hidden="true" class="glyphicon glyphicon-refresh">
								</span>&nbsp;&nbsp;Reset
							</a>

							<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo"><i class="fa fa-plus"></i>&nbsp;&nbsp;Add item</button>
						</div>
					</div>
				{!! Form::close() !!}
			<div class="clearfix"></div>

			<!-- Pagination Section-->
			<input type="hidden" name="urlvalue" id="urlvalue" value="{{$paginationurl}}" />
			<input type="hidden" name="postvalue" id="postvalue" value="{{$postvalue}}" />
			<input type="hidden" name="orderby" id="orderby" value="{{$orderby}}" />
			<input type="hidden" name="orderType" id="orderType" value="{{$orderType}}" />
			<div id="containerdata"></div>
			<div class="clearfix"></div>
		</div> <!-- Panel Body -->
	</div>

</div> <!-- Panel Body -->
</div>


	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="exampleModalLabel">Add Item value</h4>
				</div>

				{!! Form::model('', ['name' => 'itemForm', 'id' => 'itemForm', 'method' => 'POST', 'url' => ['admin/add-distance']]) !!}
				<div class="modal-body">

                <div class="col-md-6">
					<div class="form-group" id="cityDiv">
						{!! Form::label('Country', 'Country',['class' => 'control-label']) !!}
						<select name="country" class="form-control required usename-#country#" id="pp_pickup_country" onchange="get_state('pp_pickup_country','pp_pickup_state','pp_pickup_city','pp_pickup_state','','')">

                              <option value="">Select Country</option>
                              @foreach($country as $key)
                               <option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}","state_available":"{{$key->state_available}}"}' >{{$key->Content}}</option>
                              @endforeach

                          </select>
						<p class="help-block red" id="er_country" style="color:#a94442"></p>
					</div>
				</div>

				<div class="col-md-6">
                  <div class="form-group">
                        <label class="control-label">Price</label>
                        <div class="input-group error-input">
                        <span class="input-group-addon">$</span>
                        <input class="form-control required float" placeholder="Price" name="price"  >
                        <p class="help-block red" id="er_price" style="color:#ab4442"></p>
                        </div>
                  </div>
              </div>
              <div class="clearfix"> </div>

				<div class="col-md-6">
                  <div class="form-group">
                        <label class="control-label">Item Amount From</label>
                        <div class="input-group error-input">
                        <span class="input-group-addon">$</span>
                        <input class="form-control required float" placeholder="From" name="from"  >
                        <p class="help-block red" id="er_price" style="color:#ab4442"></p>
                        </div>
                  </div>
              </div>

			<div class="col-md-6">
              <div class="form-group">
                    <label class="control-label">Item Amount To</label>
                    <div class="input-group error-input">
                    <span class="input-group-addon">$</span>
                    <input class="form-control required float" placeholder="To" name="to"  >
                    <p class="help-block red" id="er_price" style="color:#ab4442"></p>
                    </div>
              </div>
              </div>

                 <input type="hidden" name="type" value="item">
				<div class ="clearfix"></div>
				</div>

				<div class="modal-footer">
				<a class="btn btn-primary" data-dismiss="modal" href="{{ url('admin/item-value') }}">Close</a>
				{!! Form::submit('Add', ['class' => 'btn btn-primary']) !!}
				</div>
			{!! Form::close() !!}
			<div class="clearfix"></div>
			</div>
		</div>
	</div>
	</div>

{!! Html::script('theme/admin/custome/js/validation1.js') !!}


<script>

new Validate({
  FormName : 'itemForm',
  ErrorLevel : 1,

});

function toggle_category() {
	if($('#travelmode').val() == 'air'){
		$('#ChargeType').val('fixed');
	}else {
		$('#ChargeType').val('distance');
	}
}

</script>
@include('Admin::layouts.footer')
@stop
