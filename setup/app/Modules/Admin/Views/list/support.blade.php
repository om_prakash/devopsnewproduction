@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')

@section('content')


<div class="container-fluid">
	<div class="panel panel-default">
		<div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;<b>Support Management</b></div>

			<div class="panel-body">
				<div class="col-md-12 row">
		{!! Form::model('', ['method' => 'PATCH', 'url' => ['admin/support']]) !!}
        		<div class="inline-form">
				<div class="form-group">
					{!! Form::label('Support', 'User Name', ['class'=>'control-lable']) !!}
					<div>
						{!! Form::text('search_value', Input::get('search_value'), ['class'=>'form-control', 'placeholder'=> 'Search By User Name','id' =>'inputError1']) !!}
					</div>
                </div>
				</div>
                <div class="inline-form">
                <div class="form-group">
					{!! Form::submit('Search', ['class' => 'btn btn-primary']) !!}

					<a href="{{URL::to('admin/support')}}" class="btn btn-primary">
						<span aria-hidden="true" class="glyphicon glyphicon-refresh">
						</span>&nbsp;&nbsp;Reset
                    </a>

				</div>
                </div>

		{!! Form::close() !!}
	<div class="clearfix"></div>


      <!-- Pagination Section-->
	<input type="hidden" name="urlvalue" id="urlvalue" value="{{$paginationurl}}" />
	<input type="hidden" name="postvalue" id="postvalue" value="{{$postvalue}}" />
	<input type="hidden" name="orderby" id="orderby" value="{{$orderby}}" />
	<input type="hidden" name="orderType" id="orderType" value="{{$orderType}}" />
	<div id="containerdata"></div>
	<div class="clearfix"></div>


  </div> <!-- Panel Body -->


</div>
</div>


<script>
	$(function(){
		$('#startDate').datetimepicker({
			format:'d/m/Y',
			onChangeDateTime:function( ct ){
				$('#endDate').datetimepicker({	minDate:ct	})
			},
			timepicker:false,
		});
	});
</script>
@include('Admin::layouts.footer')
@stop
