@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')
@extends('Admin::layouts.ajax')

@section('content')

<div class="container-fluid">

<div class="panel panel-default">
  <div class="panel-heading">
  	<i class="fa fa-th-list"></i>&nbsp;{{$title}}
	<a href = "/admin/add_email_template" class = 'pull-right' style="color: #fff"><i class="fa fa-plus-circle"></i>&nbsp;ADD</a>
  </div>
  <div class="panel-body">
  	  
      <!-- Pagination Section-->
	<input type="hidden" name="urlvalue" id="urlvalue" value="{{$paginationurl}}" />
	<input type="hidden" name="postvalue" id="postvalue" value="{{$postvalue}}" />
	<input type="hidden" name="orderby" id="orderby" value="{{$orderby}}" />
	<input type="hidden" name="orderType" id="orderType" value="{{$orderType}}" />
	<div id="containerdata"></div>
	<div class="clearfix"></div>
	
  	
  </div> <!-- Panel Body -->
</div>


</div>
@include('Admin::layouts.footer')
@stop
