@extends('Admin::layouts.master')


@section('content')
@include('Admin::layouts.menu')
{!! Html::style('theme/date-time-picker/jquery.datetimepicker.css') !!}

{!! Html::script('theme/date-time-picker/jquery.datetimepicker.js') !!}

<div class="container-fluid">
<div class="panel panel-default">
  	<div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;<b>{{$title}}</b></div>
  	<div class="panel-body">
  		<div class="col-md-12">
          <div class="" id="temp-loader"> </div>
        </div>
		{!! Form::model('', ['method' => 'get', 'url' => ['admin/package?type='.Input::get('type')]]) !!}
			<div class="row">



                <div class="col-md-2">
					<div class="form-group">
						{!! Form::label('Status', 'Status', ['class'=>'control-lable']) !!}
						<div>
							{!! Form::select('status',array(
							 				''=>'Select status',
							 				'pending' => 'Pending',
							 				'ready' => 'Ready',
							 				'paid' => 'Paid',
							 				'accepted' => 'Accepted',
							 				'requester'=>'No Delivery',
							 				'out_for_pickup'=>'To Pickup',
							 				'out_for_delivery'=>'On Delivery',
							 				'delivered'=>'Delivered',
							 				'transporter'=>'Rejected',
							 				'cancel'=>'Canceled',
							 	),Input::get('status'),	['class'=>'form-control']); !!}
						</div>
	                </div>
                </div>

                <div class="col-md-4">
	                <div class="form-group">
						{!! Form::label('search_value', 'Package ID/Package Name/Drop off Address/Requester Name', ['class'=>'control-lable']) !!}
						<div>
							{!! Form::text('search_value', Input::get('search_value'), ['class'=>'form-control', 'placeholder'=> 'Package ID/Package Name/Drop off Address/Requester Name']) !!}
						</div>
	                </div>
                </div>


                <!-- <div class="col-md-2">
	                <div class="form-group">
						{!! Form::label('productid', 'Package ID', ['class'=>'control-lable']) !!}
						<div>
							{!! Form::text('Packageid', Input::get('Packageid'), ['class'=>'form-control', 'placeholder'=> 'Package ID']) !!}
						</div>
	                </div>
                </div>

                <div class="col-md-2">
	                <div class="form-group">
						{!! Form::label('ProductName', 'Package Name', ['class'=>'control-lable']) !!}
						<div>
							{!! Form::text('ProductName', Input::get('ProductName'), ['class'=>'form-control', 'placeholder'=> 'Package Name','id' => 'endDate1']) !!}
						</div>
	                </div>
                </div> -->


			</div>

			<div class="row">

				<div class="col-md-2">
					<div class="form-group">
						{!! Form::label('country', 'Country', ['class'=>'control-lable']) !!}
						<select name="country" class="form-control required" id="pp_pickup_country" onchange="get_state('pp_pickup_country','pp_pickup_state','pp_pickup_city','pp_pickup_state','')">
	                        <option value="" id="country">Select Country</option>
	                        @foreach($country as $key)
	                        <option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}","state_available":"{{$key->state_available}}"}' >{{$key->Content}}</option>
	                        @endforeach
	                    </select>
					</div>
				</div>

				<div class="col-md-2">
					<div class="form-group">
                     	<label>State</label><span class="red-star"> *</span>
                     	<div class="clearfix"></div>
                      	<select name="state" class="form-control required left-disabled" id="pp_pickup_state" onchange="get_city('pp_pickup_state','pp_pickup_city','pp_pickup_city','')">
                        	<option value="" >Select State</option>

                     	</select>
              		</div>
				</div>

				<div class="col-md-2">
					<div class="form-group">
	                    <label>City</label><span class="red-star"> *</span>
	                    <div class="clearfix"></div>
	                    <select  name="city" class="form-control required" id="pp_pickup_city">
	                        <option value="">Select City</option>

	                    </select>
	                </div>
				</div>

				<div class="col-md-2">
					<div class="form-group">
						{!! Form::label('from', 'From', ['class'=>'control-lable']) !!}
						<div>
							{!! Form::text('StartDate', Input::get('StartDate'), ['class'=>'form-control', 'placeholder'=> 'From','id' => 'startDate','readonly' => 'readonly']) !!}
						</div>
					</div>
				</div>

                <div class="col-md-2">
					<div class="form-group">
						{!! Form::label('to', 'To', ['class'=>'control-lable']) !!}
						<div>
							{!! Form::text('EndDate', Input::get('EndDate'), ['class'=>'form-control', 'placeholder'=> 'To','id' => 'endDate','readonly' => 'readonly']) !!}
						</div>
					</div>
                </div>

                <!-- <div class="col-md-2">
					<div class="form-group">
						{!! Form::label('requester', 'Requester Name', ['class'=>'control-lable']) !!}
						<div>
							{!! Form::text('requester',Input::get('requester'), ['class'=>'form-control', 'placeholder'=> 'Requester Name']) !!}
						</div>
					</div>
                </div> -->

				<div class="col-md-12 row">
	                <div class="form-group">
	                	&nbsp;&nbsp;
	                	{!! Form::label('', '', ['class'=>'control-lable']) !!}
						{!! Form::submit('Search', ['class' => 'btn btn-primary']) !!}

						<a href="{{URL::to('admin/package')}}" class="btn btn-primary">
							<span aria-hidden="true" class="glyphicon glyphicon-refresh">
							</span>&nbsp;&nbsp;Reset
						</a>

						<a href="{{URL::to('admin/add-send-a-package')}}" class="btn btn-primary">
							<span aria-hidden="true" class="glyphicon glyphicon-refresh">
							</span>&nbsp;&nbsp;Add Send a Package
						</a>
	                </div>
				</div>

				<div class="inline-form pull-right">
					<br/><br/>
					{!! Form::label('', '', ['class'=>'control-lable','id'=>'TotalRecordFound']) !!}
				</div>
			</div>

			<div class="clearfix"></div>

      		<!-- Pagination Section-->
			<input type="hidden" name="urlvalue" id="urlvalue" value="{{$paginationurl}}" />
			<input type="hidden" name="postvalue" id="postvalue" value="{{$postvalue}}" />
			<input type="hidden" name="orderby" id="orderby" value="{{$orderby}}" />
			<input type="hidden" name="orderType" id="orderType" value="{{$orderType}}" />
			<div id="containerdata"></div>
			<div class="clearfix"></div>
		{!! Form::close() !!}

	</div> <!-- Panel Body -->
</div>

</div>
</div>
<script>
	$(function(){
		$('#startDate').datetimepicker({
			format:'d/m/Y',
			onChangeDateTime:function( ct ){
				$('#endDate').datetimepicker({	minDate:ct	})
			},
			timepicker:false,
			scrollInput : false,
		});
		$('#endDate').datetimepicker({
			format:'d/m/Y',
			onChangeDateTime:function( ct ){
				$('#startDate').datetimepicker({	maxDate:ct	})
			},
			timepicker:false,
			scrollInput : false,
		});
	});

$(document).ready(function(){

  var country ='<?php echo Input::get('country') ?>';
  var state ='<?php echo Input::get('state') ?>';
  var json = JSON.parse(country);
  var json2 = JSON.parse(state);
  var country_id = json.id;

  if(json){
    $("#temp-loader").addClass('spinnerin');
    $("#pp_pickup_country").val('<?php echo Input::get('country') ?>');
    get_state('pp_pickup_country','pp_pickup_state','pp_pickup_city','pp_pickup_state','','','yes');
    setTimeout(select_state,2000);
    setTimeout(fetch_city,3000);
    setTimeout(select_city,4000);
  }
});

function select_state()
{
  $('#pp_pickup_state option').each(function() {

      if($(this).val() == '<?php echo Input::get('state') ?>') {
          $(this).prop("selected", true);
      }
  });
}
function fetch_city()
{
  get_city('pp_pickup_state','pp_pickup_city','pp_pickup_city','')
}

function select_city(){
  $('#pp_pickup_city option').each(function() {
      if($(this).val() == '<?php echo Input::get('city') ?>') {
          $(this).prop("selected", true);
      }
  });
  $("#temp-loader").removeClass('spinnerin');
}
</script>


<!-- Modal -->
<div class="modal fade" id="refund-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Refund</h4>
      </div>
      <div id="refund-data" class="msg_stripe"></div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="ipay-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Ipay Payment Status</h4>
      </div>

      <div class="modal-body">
      	<div id="ipay_html" class=""></div>
      	<div id="ipay_bt" class=""></div>
      </div>
      
    </div>
  </div>
</div>


<button id="model_bt" data-toggle="modal" data-target="#ipay-modal" style="display: none;">&nbsp;</button>




<!--requester detail-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
        	<button type="button" class="close" data-dismiss="modal" aria-label="Close" id="close_modal"><span aria-hidden="true">&times;</span></button>
        	<h4 class="modal-title" id="myModalLabel">User Detail</h4>
        </div>

      <div class="modal-body my-modal-body">

	  	<div class="row">
          <div class="col-sm-4">
			<div id="profile_image">
				<p>Profile Image</p>
			</div>
            <script type="text/javascript">
              $(document).ready(function() {
                $(".fancybox").fancybox();
              })
            </script>
          </div>
          <div class="col-sm-8" id="actions" style="position: relative;text-align: center;vertical-align: middle;line-height: 90px;">
            
          </div>
        </div>



      	<div class="table-responsive">
      		<table class="table table-hover table-striped my-table">
            	<tr>
                	<td>Name:</td>
                    <td><p id="name"></p></td>
                </tr>
            	<tr>
                	<td>Email:</td>
                    <td><p id="email"></p></td>
                </tr>
                <tr>
                	<td>Phone Number:</td>
                    <td><p id="phone"></p></td>
                </tr>
                <tr>
                  <td>Alternate Phone Number:</td>
                    <td> <p id="phone2"></p></td>
                </tr>
                <tr>
                	<td>Address line-1:</td>
                    <td><p id="street1"></p></td>
                </tr>
                <tr>
                	<td>Address line-2:</td>
                    <td> <p id="street2"></p></td>
                </tr>
                <tr>
                	<td>Country:</td>
                    <td><p id="detail_country"></p></td>
                </tr>
                <tr>
                	<td>State:</td>
                    <td><p id="detail_state"></p></td>
                </tr>
                <tr>
                	<td>City:</td>
                    <td> <p id="detail_city"></p></td>
                </tr>
                 <tr>
                	<td>Zip Code:</td>
                    <td><p id="detail_zipcode"></p></td>
                </tr>
				<tr>
					<td> Unit#: </td>
					<td style="color: #f00;"> <p id="unitHash"></p> </td>
				</tr>
            </table>
      </div>
      <div id="no_user" style="display:none;"><center>User not found.</center></div>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
<!-- end -->

<div id="myCommentModal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="exampleModalLabel">Add Comment</h4>
			</div>
			<div class="col-sm-12 ">
				{!! Form::model('', ['name' => 'commentForm', 'id' => 'commentForm', 'method' => 'POST', 'url' => ['admin/post-user-comment']]) !!}
			
					<div class="modal-body clearfix">
						<div class="form-group" id="cityDiv">
							{!! Form::label('Comment', 'Comment:',['class' => 'control-label']) !!}
							{!! Form::textarea('comment', '', ['class'=>'form-control', 'placeholder'=> 'Comment','id' =>'comment','rows'=>'3']) !!}
							<p class="help-block red" id="er_comment"></p>
						</div>
					</div>
					
					<input type="hidden" name="user_id" id="user_id" value="" />
					<input type="hidden" name="request_id" id="request_id" value="" />
					<input type="hidden" name="product_title" id="product_title" value="" />

					<div class="modal-footer">
						{!! Form::button('Close', ['class' => 'btn btn-primary', 'data-dismiss'=>'modal']) !!}
						{!! Form::submit('Add', ['class' => 'btn btn-primary']) !!}
					</div>

				{!! Form::close() !!}
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<style type="text/css">
  @keyframes spinnerin {
    to {transform: rotate(360deg);}
  }
  .spinnerin:before {
    content: '';
    box-sizing: border-box;
    position: absolute;
    top: 50%;
    left: 50%;
    width: 20px;
    height: 20px;
    margin-top: -10px;
    margin-left: -10px;
    border-radius: 50%;
    border: 2px solid #ccc;
    border-top-color: #333;
    animation: spinnerin .6s linear infinite;
  }
</style>


<script>

function getIpayStatus(id)
{	
	if(id != ''){
		$('#loading').show();
		var url = SITEURL+'admin/ajax/getIpayStatus';
		var url2 = SITEURL+'admin/ajax/changeIpayStatus/'+id;
		$.ajax({
			type:"POST",
			url:url,
			data:'id='+id,
			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			success: function(res)
			{
				$('#loading').hide();
				if(res.success == 1){
					//alert(res.show_popup);
					if(res.show_popup == 1){
						
						$("#ipay_html").html(res.msgres);
						$("#ipay_bt").html('<a class="btn btn-success btn-info btn-action" href="'+url2+'" >Change Status to Pending</a>');
						$("#model_bt").trigger('click');
					}else{
						alert(res.msgres);
					}
					
				}else{
					alert(res.msg);
				}
				
			}
		});
	}
	return false;
}



function cancel_request(id)
{

	if(confirm("Are you sure? you want to cancel this request.")){

		$("#cancel_bt_"+id).addClass('spinning');
		$.ajax({
			type:"GET",
			url:'{{url("admin/cancel-delivery-request")}}',
			data:"request_id="+id,
			success: function(res){
				$("#cancel_bt_"+id).removeClass('spinning');
				if(res.success == 0){
					alert(res.msg);
				}else if(res.success == 1){
					alert(res.msg);
					location.reload();
				}

			}
		});

	}
	return false;

}


function get_user_info(id)
{
	var url = SITEURL+'ajax/get_user_info/'+id;
	$('#loading').show();
	$.ajax({
		type:"GET",
		url:url,
		//headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		success: function(res)
		{	$('#loading').hide();
			if(res.success == 0){



				$(".table-responsive").hide();
				$("#profile_image").html('');
				$("#no_user").show();

			}else{
				$(".table-responsive").show();
				$("#no_user").hide();
				$("#name").html(res.result.Name);
				$("#user_id").val(res.result._id);
				$("#request_id").val(res.result._id);
				$("#product_title").val(res.result.Name);
				$("#email").html(res.result.Email);
				$("#unitHash").html(res.result.UniqueNo);
				$("#phone").html(res.result.PhoneNo);
				$("#phone2").html(res.result.AlternatePhoneNo);
				$("#street1").html(res.result.Street1);
				$("#street2").html(res.result.Street2);
				$("#detail_country").html(res.result.Country);
				$("#detail_state").html(res.result.State);
				$("#detail_city").html(res.result.City);
				$("#detail_zipcode").html(res.result.ZipCode);
				if(res.result.Image == ''){
					$("#profile_image").html('<img class="thumbnail" src="{{ImageUrl}}/profile/noImageAvailable.jpg" height="100px" width="100px" alt="Profile image">');
				}else{
					$("#profile_image").html('<img class="thumbnail" src="{{ImageUrl}}/'+res.result.Image+'" height="100px" width="100px" alt="Profile image">');
				}
				$("#actions").html('<a href="/admin/user-comment-list/'+res.result._id+'" data-placement="top" title="View Notes/Comments" class="btn btn-small btn-primary btn-action"><i class="fa fa-th-list"></i></a>\
				<a href="/admin/requester/edit/'+res.result._id+'" data-placement="top" title="Edit" class="btn btn-small btn-primary btn-action" style="margin-right: 10;"><i class="fa fa-edit"></i></a><br />\
				<a href="#" data-toggle="modal" data-target="#myCommentModal" data-placement="top" title="Add Notes/Comments" class="btn btn-small btn-info"><i class="fa fa-plus"></i> Add Notes/Comments</a>');
				//$("#detail_reg_date").html('{{ show_date('+res.result.EnterOn+') }}');
				//$('#detail_modal').trigger('click');


			}

			//$("#refund-data").html(msg);
		}
	});
}

function get_package_information(id)
{
	var url = SITEURL+'ajax/get_package_information/'+id;
	$('#loading').show();
	$.ajax({
		type:"GET",
		url:url,
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		success: function(msg)
		{
			$('#loading').hide();
			$("#refund-data").html(msg);
		}
	});
}
function make_refund(id)
{
	var refundAmount =$("#refund_amount").val();
	 if(refundAmount =="")
		{
			 flag = false;
			 $("#refund_amountdiv").addClass("has-error");
			 $("#refund_amountError").html('Refund amount field is required.');
		}
		else
		{

		  if(!$.isNumeric(refundAmount))  //if(Number(refundAmount)<0)
		  {
			$("#refund_amountdiv").addClass("has-error");
			$("#refund_amountError").html('Refund amount is only positive number.');
		  }
		   else
		   {
				$("#refund_amountError").html('');
				//var id = "5628e4886734c4a0637dd22c";
				var url = SITEURL+'ajax/get_refund_information/'+id+'/'+refundAmount;
				$('#loading').show();
				$.ajax({
					type:"GET",
					url:url,
					dataType: "json",
					headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
					success: function(msg)
					{
						var json1 = JSON.parse(msg);
						//console.log(json1);
						if(json1.error.type =="invalid_request_error")
						{
							var respons = '<p class="error_msg">'+json1.error.message+'</p>';
						}
						else if(json1.status =="succeeded")
						{
							var respons = '<p class="success_msg">'+"Your amount is refunded"+'</p>';
						}
						else
						{
							var respons = "Your stripe id valid";
						}
						$('#loading').hide();
						$("#refund-data").html(respons);
					}
				});
		   }
		}
}
</script>

@include('Admin::layouts.footer')
{!! Html::script('theme/admin/custome/js/utility.js') !!}
@stop

