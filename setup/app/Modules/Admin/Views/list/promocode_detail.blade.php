@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')

@section('content')
{!! Html::script('theme/admin/custome/js/jquery.bootstrap-duallistbox.js') !!}
{!! Html::script('theme/admin/custome/js/jquery.bootstrap-duallistbox.js') !!}
{!! Html::style('theme/admin/custome/css/bootstrap.min.css') !!}


   <div class="container-fluid">
<div class="panel panel-default">
  <div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;<b>Assign promocode</b></div>
    <div class="panel-body">  
 
          <div class="col-sm-12 row">
           <div class="col-sm-6">
          <label> All user list </label>
           </div>
           <div class="col-sm-6">
          <label> &nbsp;&nbsp;&nbsp;Selected user list </label>
           </div>
           </div>
           <p>  </p>
             {!! Form::model('', ['name' => 'promocode', 'id' =>'promocodeform', 'method' => 'post']) !!}
            <div class="col-sm-12">
                <select multiple="multiple" size="10" name="promocodelistbox[]">
                    <?php foreach($user as $key){ ?>
                        <option  value="{{$key->_id}}"
                        <?php 
                          if(is_array($old_user->assign_promocode_users)){
                            if (in_array(strtolower($key['_id']), $old_user->assign_promocode_users)) {echo 'selected="selected"';}
                          }
                          ?> 
                        >{{ucfirst($key->FirstName)}} {{$key->LastName}}</option>
                    <?php } ?>
                </select>
                <br>
                  <button type="submit" class="btn btn-primary">Submit</button>
            </div>
       {!! Form::close() !!}
	   </div>
     </div>
     </div>
     </div>


 <script>
    var demo1 = $('select[name="promocodelistbox[]"]').bootstrapDualListbox();
    $("#promocodeform").submit(function() {
       
   });

  </script>

@include('Admin::layouts.footer')

@stop


