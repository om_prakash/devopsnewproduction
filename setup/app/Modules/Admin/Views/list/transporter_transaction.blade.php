@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')

@section('content')
{!! Html::style('theme/date-time-picker/jquery.datetimepicker.css') !!}
{!! Html::script('theme/date-time-picker/jquery.datetimepicker.js') !!}

<div class="container-fluid">
<div class="panel panel-default">
  <div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;<b>{{$title}}</b></div>

  <div class="panel-body">

    <div class="col-md-12 row">
		{!! Form::model('', ['method' => 'PATCH', 'url' => ['admin/transporter_transaction']]) !!}
        		<div class="inline-form">
                	<div class="form-group">
                        {!! Form::label('TransporterName', 'Transporter Name', ['class'=>'control-lable']) !!}
                        {!! Form::text('search_value', Input::get('search_value'), ['class'=>'form-control', 'placeholder'=> 'Transporter Name','id' =>'inputError1']) !!}
                	</div>
                </div>
                 <div class="inline-form">
				<div class="form-group">
					{!! Form::label('StartDate', 'From', ['class'=>'control-lable']) !!}
					<div>
						{!! Form::text('StartDate', Input::get('StartDate'), ['class'=>'form-control', 'placeholder'=> 'From','id' => 'startDate','readonly'=> 'readonly']) !!}
					</div>
                    </div>
                </div>
                <div class="inline-form">
					<div class="form-group">
						{!! Form::label('EndDate', 'To', ['class'=>'control-lable']) !!}
						<div>
							{!! Form::text('EndDate', Input::get('EndDate'), ['class'=>'form-control', 'placeholder'=> 'To','id' => 'endDate','readonly'=> 'readonly']) !!}
						</div>
					</div>
                </div>
                 <div class="inline-form">
                <div class="form-group">
					{!! Form::label('Status', 'Status', ['class'=>'control-lable']) !!}
					<div>
						<select name="Status" class="form-control">
							<option value="">Select Status</option>
							<option value="done" <?php if (Input::get('Status') == 'done') {echo 'selected="selected"';}?> >Done</option>
							<option value="capture" <?php if (Input::get('Status') == 'capture') {echo 'selected="selected"';}?> >Pending</option>
						</select>
					</div>
                </div>
                </div>
                <div class="inline-form">
					<div class="form-group">
						{!! Form::label('state', 'State', ['class'=>'control-lable']) !!}
						<div>
							{!! Form::text('state', Input::get('state'), ['class'=>'form-control', 'placeholder'=> 'State']) !!}
						</div>
					</div>
                </div>
                <div class="inline-form">
					<div class="form-group">
						{!! Form::label('city', 'City', ['class'=>'control-lable']) !!}
						<div>
							{!! Form::text('city', Input::get('city'), ['class'=>'form-control', 'placeholder'=> 'City']) !!}
						</div>
					</div>
                </div>
				<div class="inline-form">
                <div class="form-group">
                    {!! Form::submit('Search', ['class' => 'btn btn-primary']) !!}
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Export to CSV</button>
                </div>
                </div>

             </div>

	  {!! Form::close() !!}
     <!-- Panel Body -->

	<div class="clearfix"></div>


      <!-- Pagination Section-->
	<input type="hidden" name="urlvalue" id="urlvalue" value="{{$paginationurl}}" />
	<input type="hidden" name="postvalue" id="postvalue" value="{{$postvalue}}" />
	<input type="hidden" name="orderby" id="orderby" value="{{$orderby}}" />
	<input type="hidden" name="orderType" id="orderType" value="{{$orderType}}" />
	<div id="containerdata"></div>
	<div class="clearfix"></div>

    </div> <!-- Panel Body -->
</div>




</div>
<script>
	$(function(){
		$('#startDate').datetimepicker({
			format:'d/m/Y',
			onChangeDateTime:function( ct ){
				$('#endDate').datetimepicker({	minDate:ct	})
			},
			timepicker:false,
		});
		$('#endDate').datetimepicker({
			format:'d/m/Y',
			onChangeDateTime:function( ct ){
				$('#startDate').datetimepicker({	maxDate:ct	})
			},
			timepicker:false,
		});
	});
</script>

<!----- Pop Up - -->
			<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
			  <div class="modal-dialog" role="document">
				<div class="modal-content">
				  <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="exampleModalLabel">Export Sender Payment Information</h4>
				  </div>

				  {!! Form::model('', ['name' => 'actionform', 'method' => 'POST', 'url' => ['admin/export_csv'], 'id' => 'actionform']) !!}
				  <div class="modal-body">
					  <div class="col-md-6">
							<div class="form-group">
								{!! Form::label('FromDate', 'From Date:',['class' => 'control-label']) !!}
								{!! Form::text('FromDate', '', ['class'=>'form-control', 'placeholder'=> 'From Date','id' =>'FromDate']) !!}
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								{!! Form::label('ToDate', 'To Date:',['class' => 'control-label']) !!}
								{!! Form::text('ToDate', '', ['class'=>'form-control', 'placeholder'=> 'To Date','id' =>'ToDate']) !!}
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="col-md-6">
							<div class="form-group">
								{!! Form::label('Name', 'Transporter Name:',['class' => 'control-label']) !!}
								{!! Form::text('search_value', '', ['class'=>'form-control', 'placeholder'=> 'Transporter Name','id' =>'tagss']) !!}

							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								{!! Form::label('UserName', 'Status:',['class' => 'control-label']) !!}
								<select name="status" class="form-control">
									<option value="">Select Status</option>
									<option value="done">Complete</option>
									<option value="capture">Pending</option>
								</select>

							</div>
						</div>
						<div class="clearfix"></div>
				  </div>

				  <div class="modal-footer">
					{!! Form::button('Close', ['class' => 'btn btn-primary', 'data-dismiss'=>'modal']) !!}
					{!! Form::submit('Export', ['class' => 'btn btn-primary']) !!}
				  </div>

				{!! Form::close() !!}
				<div class="clearfix"></div>
				</div>
			  </div>
			</div>
			<!--- PopUp End	-->

<script>
	$(function(){
		$('#FromDate').datetimepicker({
			format:'d/m/Y',
			timepicker:false,
			onChangeDateTime:function( ct ){
				$('#ToDate').datetimepicker({	minDate:ct	})
			}
		});
		$('#ToDate').datetimepicker({
			format:'d/m/Y',
			timepicker:false,
			onChangeDateTime:function( ct ){
				$('#FromDate').datetimepicker({	maxDate:ct	})
			}
		});
	});
</script>
@include('Admin::layouts.footer')
@stop
