@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')
@section('content')
{!! Html::script('theme/admin/custome/js/validation.js') !!}


<div class="container-fluid">

<div class="panel panel-default">
  <div class="panel-heading"><i class="fa fa-cog"></i>&nbsp;<b>Shipping Charge for {{ucfirst($info->Content)}} (By {{ucfirst($info->TravelMode)}})</b> 
  <a style="margin-right:5px;" class="btn btn-primary margin-top-less-7 pull-right" href="{{URL('admin/category_list')}}">Go Back</a>
  </div> 
  <div class="panel-body">
    <div class="col-md-12">
      {!! Form::model($info ,['name' => 'shippingForm', 'id' => 'shippingForm','method' => 'POST','id'=>'shippingForm', 'url' => ['admin/add-shipping',$info->id]]) !!}
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label class="control-label col-md-4 text-right" for="custom_duty"> Customs/Duty (%): </label>
            <div class="col-md-8">
              <input class="form-control" type="text" name="custom_duty" id="custom_duty" value="{{ $info->custom_duty }}" />
            </div>
          </div>
        </div>
        <div class="col-md-6"></div>
      </div>

     <div class="form-group">
        <div class="col-md-2" style="text-align:right">{!! Form::label('Shipping', 'Shipping: ') !!}</div>
    <?php if($info['ChargeType'] == 'distance'){?> 
        <div class="col-md-9">
          <div class="col-md-3">{!! Form::label('min_cost', 'Minimum Distance(in Miles)') !!}</div>
          <div class="col-md-3">{!! Form::label('max_cost', 'Maximum Distance(in Miles)') !!}</div>
          <div class="col-md-3">{!! Form::label('rate', 'UnConsolidated Rate(in US $)') !!}</div>
          <div class="col-md-3">{!! Form::label('rate', 'Consolidated Rate(in US $)') !!}</div>

          </div>
        </div>
        <?php } else {?>
       <div class="col-md-9">
          <div class="col-md-3">{!! Form::label('min_cost', 'Minimum Weight (in Lbs)') !!}</div>
          <div class="col-md-3">{!! Form::label('max_cost', 'Maximum Weight (in Lbs)') !!}</div>
          <div class="col-md-3">{!! Form::label('rate', 'UnConsolidated Rate(in US $)') !!}</div>
          <div class="col-md-3">{!! Form::label('rate', 'Consolidated Rate(in US $)') !!}</div>
          </div>
        </div>
            
        <?php } ?>






        <?php if(count($info['Shipping']) == 0){?>
       <div class="col-md-9 col-md-offset-2 ">
                <div class="col-md-3">
                  <div class="form-group"><input type="text" name="MinimumDistance[]" <?php if($info['ChargeType'] == 'distance'){?> placeholder="Minimum Distance"<?php } else {?>placeholder="Minimum Weight"<?php } ?> class="form-control required validation"></div></div>
                  <div class="col-md-3"><div class="form-group"><input type="text" name="MaximumDistance[]" <?php if($info['ChargeType'] == 'distance'){?> placeholder="Maximum Distance"<?php } else {?>placeholder="Maximum Weight"<?php } ?> class ="form-control required validation"></div></div>
                  <div class="col-md-3"><div class="form-group"><input type="text" name="Rate[]"  placeholder="Rate" class="form-control required validation"></div></div>

                  <div class="col-md-2"><div class="form-group"><input type="text" name="CONSOLIDATE_RATE[]" value="" placeholder="Rate" class="form-control required"/></div></div>
            </div>
            <?php } ?>
           
        
        <?php foreach($mul as $key){ if ($key['Shipping']['MaxDistance'] != '') { ?>
              <div class="col-md-9 col-md-offset-2 ">
                <div class="col-md-3">
                  <div class="form-group validation">
                    <input type="text" name="MinimumDistance[]" value="<?php echo  $key['Shipping']['MinDistance']; ?>" <?php if($info['ChargeType'] == 'distance'){?> placeholder="Minimum Distance"<?php } else {?>placeholder="Minimum Weight"<?php } ?> class="form-control required">

                  </div>
                </div>

                <div class="col-md-3">
                  <div class="form-group validation">
                    <input type="text" name="MaximumDistance[]" value="<?php echo $key['Shipping']['MaxDistance']; ?>"<?php if($info['ChargeType'] == 'distance'){?> placeholder="Maximum Distance"<?php } else {?>placeholder="Maximum Weight"<?php } ?> class="form-control required">
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group validation">
                    <input type="text" name="Rate[]" value="<?php echo  $key['Shipping']['Rate'];  ?>" placeholder="Rate" class="form-control required">
                  </div>
                </div>

                <div class="col-md-2">
                  <div class="form-group validation">
                    <input type="text" name="CONSOLIDATE_RATE[]" value="<?php echo  $key['Shipping']['CONSOLIDATE_RATE'];  ?>" placeholder="Rate" class="form-control required">
                  </div>
                </div>
                <a href="javascript:void(0);" class="remove_button1 btn btn-danger" title="Remove field"><i class="fa fa-trash-o"></i></a>
              </div>
        <?php }} ?>




         
           <div class="add-more"></div>
           <div class="col-md-9 col-md-offset-2 ">
                 <div class="col-md-3">
                     <div class="form-group"><a href="javascript:void(0);" class="add_button btn btn-success" title="Add field">Add More</a></div></div></div>    
           <div class="clearfix"></div>
           <div class="col-md-9 col-md-offset-2">
           <div class="col-md-3">
             <div class="form-group">
           {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
          <a href = "{{URL('admin/category_list')}}" class = 'btn btn-danger'>Cancel</a>
         </div>
      </div>
    </div>
       {!! Form::close() !!}
    </div>
</div>
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script> 
<script>
var float_expr = /^[0-9\.]+$/;

$("#shippingForm").submit(function()
{
  var flag = true;
  $('.validation-row').remove();
  $('.required').each(function(){
  if($(this).val() == false){
  if ($(this).parent().next(this).length == 0) // only add if not added
    {
       //$(this).parent().after("<div class='validation validation-row' style='color:red;margin-bottom: 20px;'>This field is required</div>");
    }   
      
  } 
  else if(float_expr.test($(this).val()) == false)
  {
       $(this).parent().after("<div class='validation validation-row' style='color:red;margin-bottom: 20px;'>The field must have an integer value.</div>");  
       flag = false;
  }
});
 return flag;
});
</script>
<script type="text/javascript">

var  placeholder = "Weight";
@if($info['ChargeType'] == 'distance')
  placeholder = 'Distance';
@endif;

$(document).ready(function(){
    //var maxField = 10; //Input fields increment limitation
    var addButton = $('.add_button'); //Add button selector
    var wrapper = $('.add-more'); //Input field wrapper
    var fieldHTML = '<div class="col-md-9 col-md-offset-2"><div class="col-md-3"><div class="form-group"><input type="text" name="MinimumDistance[]" value="" placeholder="Minimum '+placeholder+'" class="form-control required"/></div></div><div class="col-md-3"><div class="form-group"><input type="text" name="MaximumDistance[]" value="" placeholder="Maximum '+placeholder+'" class="form-control required"/></div></div>'+
      '<div class="col-md-3"><div class="form-group"><input type="text" name="Rate[]" value="" placeholder="Rate" class="form-control required"/></div></div>'+
      '<div class="col-md-2"><div class="form-group"><input type="text" name="CONSOLIDATE_RATE[]" value="" placeholder="Rate" class="form-control required"/></div></div>'+


      '<a href="javascript:void(0);" class="remove_button btn btn-danger" title="Remove field"><i class="fa fa-trash-o"></i></a></div></div></div></div>'; //New input field html 
    var x = 1; //Initial field counter is 1
    $(addButton).click(function(){ //Once add button is clicked
        //if(x < maxField){ //Check maximum number of input fields
            x++; //Increment field counter
            $(wrapper).append(fieldHTML); // Add field html
        
    });
    $(wrapper).on('click', '.remove_button', function(e){ //Once remove button is clicked
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
    });
    $(".remove_button1").on('click',  function(e){ //Once remove button is clicked
        if(confirm('Are you sure? You want to Remove!'))
       {
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
  }
     });
});
</script>
@include('Admin::layouts.footer')
@stop
