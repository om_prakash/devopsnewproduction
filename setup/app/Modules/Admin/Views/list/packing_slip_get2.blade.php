@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')

@section('content')
{!! Html::style('theme/admin/choose-multiple/bootstrap-chosen.css') !!}
{!! Html::script('theme/admin/choose-multiple/chosen.jquery.js') !!}
{!! Html::script('theme/web/js/utility.js') !!}
<div class="panel-body">
  	
		
			<div class="row">
			{!! Form::model('', ['method' => 'get', 'url' => ['admin/get-packing-slip']]) !!}

			<div class="col-sm-5 col-xs-12">

			{!! Form::label('Requester Name', 'Requester Name', ['class'=>'control-lable']) !!}
					<select name="RequesterName" id="RequesterName" class="form-control chosen-select" onchange="myFunction()">
					<option value=""> Select </option>
					@foreach($users as $val)
                   <option value="{{trim($val->RequesterName)}}" @if(@trim($val->RequesterName) == @Input::get('RequesterName')) selected @endif>
                   {{trim($val->RequesterName)}}
                   </option>
					@endforeach
					</select>
				
				<label class="control-label">Select user</label><span class="red-star"> *</span>
				

				<select class="form-control required chosen-select" id="user" name="user" >
                    <option value="" >Select User</option>
                    <?php foreach ($res as $key) {?>
                    <option  value='{{$key->Name}}' class="travel-mode-{{$key->TravelMode}}" >{{$key->Name}}</option>
                    <?php }?>
                 </select>
			</div>

			<div class="col-sm-3 col-xs-12">
		    <button type="submit" class="btn btn-primary btn-margin"> Search Slip</button>
			</div>
			{!! Form::close() !!}
			</div>
		
	
</div>

{!! Html::style('theme/admin/choose-multiple/bootstrap-chosen.css') !!}
{!! Html::script('theme/admin/choose-multiple/chosen.jquery.js') !!}
{!! Html::style('theme/admin/custome/css/style.min.css') !!}
{!! Html::script('theme/web/js/utility.js') !!}
<script type="text/javascript">
	

jQuery(document).ready(function ($) {
$(".chosen-select").chosen({no_results_text: "Oops, nothing found!"})
});
</script>
@endsection