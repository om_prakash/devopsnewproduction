@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')

@section('content')
{!! Html::style('theme/date-time-picker/jquery.datetimepicker.css') !!}

{!! Html::script('theme/date-time-picker/jquery.datetimepicker.js') !!}

<div class="container-fluid">
<div class="panel panel-default">
  <div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;<b>{{$title}}</b></div>
  <div class="panel-body">
		{!! Form::model('', ['method' => 'GET']) !!}
				<div class="inline-form">
                <div class="form-group">
					{!! Form::label('Title', 'Package Title', ['class'=>'control-lable']) !!}
					<div>
						{!! Form::text('search_value', Input::get('search_value'), ['class'=>'form-control', 'placeholder'=> 'Package Title','id' =>'inputError1']) !!}
					</div>
                </div>
                </div>
                <div class="inline-form">
				<div class="form-group">
					{!! Form::label('StartDate', 'From', ['class'=>'control-lable']) !!}
					<div>
						{!! Form::text('StartDate', Input::get('StartDate'), ['class'=>'form-control', 'placeholder'=> 'From','id' => 'startDate','readonly'=> 'readonly']) !!}
					</div>
                    </div>
                </div>
                <div class="inline-form">
                <div class="form-group">
					{!! Form::label('EndDate', 'To', ['class'=>'control-lable']) !!}
					<div>
						{!! Form::text('EndDate', Input::get('EndDate'), ['class'=>'form-control', 'placeholder'=> 'To','id' => 'endDate','readonly'=> 'readonly']) !!}
					</div>
                </div>
                </div>
                <div class="inline-form">
                <div class="form-group">
					{!! Form::label('', '', ['class'=>'control-lable']) !!}
					{!! Form::submit('Search', ['class' => 'btn btn-primary']) !!}

					<a href="{{URL::to('admin/notification')}}" class="btn btn-primary">
						<span aria-hidden="true" class="glyphicon glyphicon-refresh">
						</span>&nbsp;&nbsp;Reset
                    </a>

					<a href="{{ url('admin/send-notification') }}" data-placement="top" title="Edit" class="btn btn-primary btn-action">Send Notification</a>
                    </div>
				</div>

		{!! Form::close() !!}
	<div class="clearfix"></div>


      <!-- Pagination Section-->
	<input type="hidden" name="urlvalue" id="urlvalue" value="{{$paginationurl}}" />
	<input type="hidden" name="postvalue" id="postvalue" value="{{$postvalue}}" />
	<input type="hidden" name="orderby" id="orderby" value="{{$orderby}}" />
	<input type="hidden" name="orderType" id="orderType" value="{{$orderType}}" />
	<div id="containerdata"></div>
	<div class="clearfix"></div>


  </div> <!-- Panel Body -->


</div>
</div>
<script>
	$(function(){
		$('#startDate').datetimepicker({
			format:'d/m/Y',
			onChangeDateTime:function( ct ){
				$('#endDate').datetimepicker({	minDate:ct	})
			},
			timepicker:false,
		});
		$('#endDate').datetimepicker({
			format:'d/m/Y',
			onChangeDateTime:function( ct ){
				$('#startDate').datetimepicker({	maxDate:ct	})
			},
			timepicker:false,
		});
	});
</script>
@include('Admin::layouts.footer')

@stop
