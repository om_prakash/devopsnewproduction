@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')
@section('content')
{!! Html::script('theme/admin/custome/js/validation.js') !!}
<div class="container-fluid">

<div class="panel panel-default">
  <div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;<b>{{$title}}</b></div>
  <div class="panel-body">
  	<div class="col-md-12 row">
  		<form role="form">
			<div class="inline-form">
				<div class="form-group">
					{!! Form::model(['method' => 'PATCH', 'url' => ['admin/users']]) !!}
        			{!! Form::label('UserFirstName', 'User Name') !!}
                    <div>
					{!! Form::text('search_value', Input::get('search_value'), ['class'=>'form-control', 'placeholder'=> 'Notification Title','id' =>'inputError1']) !!}
                    </div>
				</div>
			</div>
			
			<div class="inline-form">
				<div class="form-group">
					{!! Form::label('', '') !!}
					{!! Form::submit('Search', ['class' => 'btn btn-primary']) !!}
				</div>
			</div>
        </form>
        {!! Form::close() !!}
    </div>
	<div class="clearfix"></div>
      <!-- Pagination Section-->
	<input type="hidden" name="urlvalue" id="urlvalue" value="{{$paginationurl}}" />
	<input type="hidden" name="postvalue" id="postvalue" value="{{$postvalue}}" />
	<input type="hidden" name="orderby" id="orderby" value="{{$orderby}}" />
	<input type="hidden" name="orderType" id="orderType" value="{{$orderType}}" />
	<div id="containerdata"></div>
	<div class="clearfix"></div>
	
  	
  </div> <!-- Panel Body -->
</div>

</div> <!-- Panel Body -->
</div>
@include('Admin::layouts.footer')
@stop
<script>
function send_verification_code(id){
	
	var url = SITEURL+'ajax/verify_user';
	$.ajax
	({
		type: "POST",
		url: url,
		data: "id="+id,
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		success: function(res)
		{				
			var obj = eval("("+res+")");
			if(obj.success == 1)
			{
				$('#verification_section_'+id).html('<span>Pending <span class="verification" onclick=send_verification_code("'+id+'")>Resend</span></span>');
				$('#virification_code_'+id).html(obj.code);
				alert('Code send successfully.');
			}else{
				alert('Unable to send code.');
			}
		}
	});
	
}
function add_bank_account(id){

	var url = SITEURL+'ajax/add_account_to_stripe';
	$('#bank_ac_section_'+id+' > span').addClass('spinning');
	$.ajax
	({
		type: "POST",
		url: url,
		data: "id="+id,
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		success: function(res)
		{
		$('#bank_ac_section_'+id+' > span').removeClass('spinning');				
			var obj = eval("("+res+")");
			if(obj.success == 1)
			{
				$('#bank_ac_section_'+id).html('<span>Added</span>');	
			}
			alert(obj.msg);
		}
	});
}
function transfer_fund(id)
{
	var url = SITEURL+'ajax/transfer_fund_view';
	$.ajax
	({
		type: "GET",
		url: url,
		data: "id="+id,
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		success: function(res)
		{
			$('#transfer_fund_modal_body').html(res);
			$('#transfer_to').val(id);
		}
	});
}
function transfer_fund_modal_body()
{
	var flag = true;
	if(valid.required('transfer_amount','amount') == false){ flag = false; }else{
		if(valid.numeric('transfer_amount','amount') == false){ flag = false; }
	}
	if(flag == true)
	{
		$.ajax
		({
			type: "POST",
			url: SITEURL+'ajax/transfer_fund_to_carrier',
			data: $('#transfer_fund_modal_body').serialize(),
			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			success: function(res)
			{
				var obj = eval('('+res+')');
				if(obj.success == 1)
				{
					$('#transfer_fund_modal_body')[0].reset();
					$('#fund-error').html('<div role="alert" class="alert alert-success alert-dismissible fade in">'
										+'<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>'+obj.msg+'</div>');

				}else{
					$('#fund-error').html('<div role="alert" class="alert alert-danger alert-dismissible fade in">'
	      				+'<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>'+obj.msg+'</div>');
				}
			}
		});
	}
	return false;
}
</script>

<div class="modal fade" id="transfer_fund_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Transfer Fund</h4>
      </div>
      {!! Form::model('', ['class' => 'form-horizontal', 'method' => 'POST', 'url' => ['admin/post_app_tutorial'],'id' => 'transfer_fund_modal_body', 'onsubmit'=>'return transfer_fund_modal_body()']) !!}
      
      {!! Form::close() !!}
    </div>
  </div>
</div>
