<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Slip</title>

  <style type="text/css">

.header,
.footer {
    width: 100%;
    text-align: center;
    position: fixed;
}
.header {
    top: 0px;
}
.footer {
    bottom: 0px;
}
.pagenum:before {
    content: counter(page);
}

  </style>
</head>
<body style="font-family:arial, helvetica,serif,; font-size:12px; color:#333; margin:0; padding:0;">

    <table style="width: 100%; margin: 0px auto;  padding: 0px 0px;">
        <tr>
            <td>
                <table style="width:100%; margin-bottom:10px;">
                    <tr>
                        <td style="width:50%" valign="top">
                            <div style="padding: 0; margin-top: 0px;">
                            <img src="{{url('')}}/theme/admin/custome/images/logo_receipt.png" width="240" style="margin-bottom:10px;">
                             <p style="line-height:15px;"><b>Ghana:</b> 131 Race Course Street Abeka Accra (Opposite Derby Royal Hotel)</p>
                              <p><b>Phone:</b> 030 243 4505 or 055 496 7571 or 050 163 4195 </p>
                            </div>
                        </td>
                        <td width="10%">&nbsp;</td>
                        <td style="width:40%" valign="top" align="right">

                            <h2 style="font-size:54px; margin-bottom:10px; margin-top:-5px;"><b style="font-weight: 700; font-size:28px; color:#004269;">Delivery Note</b></h2>
                            <table style="width:100%; float:right;" cellpadding="0" cellspacing="0">
                            <tr>
                            <td align="right">Order Date: &nbsp;&nbsp;</td>

                            <td width="110"><span style="border:1px solid #ddd; border-bottom:none; padding:2px 8px;display: inline-block; width:100%;">{{show_date(@$info->EnterOn)}}</span></td>
                            </tr>
                            <tr>
                            <td align="right">Package ID #: &nbsp;&nbsp; </td>

                            <td width="110"><span style="border:1px solid #ddd; border-bottom:none; padding:2px 8px;display: inline-block; width:100%;">{{ucfirst($info->PackageNumber)}}</span></td>
                            </tr>
                            <tr>
                            <td align="right">Delivery Note: &nbsp;&nbsp; </td>

                            <td width="110"><span style="border:1px solid #ddd; border-bottom:none; padding:2px 8px; width:100%;display: inline-block;">{{@$info->DeliveryNote}}</span></td>
                            </tr>
                            <tr>
                            <td align="right">Dispatch Date: &nbsp;&nbsp; </td>

                            <td width="110"><span style="border:1px solid #ddd;border-bottom:none; padding:2px 8px; width:100%;display: inline-block;">{{show_date(@$info->dispatchDate)}}</span></td>
                            </tr>
                            <tr>
                            <td align="right">Delivery Method: &nbsp;&nbsp; </td>

                            <td width="110"><span style="border:1px solid #ddd; padding:2px 8px; width:100%;display: inline-block;">{{$info->deliveryMethod}}</span></td>
                            </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <div style="clear:both;"></div>
                <div style="width:100%;  margin-top:40px; margin-bottom:10px;"></div>
                <div style="clear:both;"></div>

                <table style="width:100%" style="margin-bottom:10px; margin-top:15px">
                    <tr style="margin-top:20px;">
                        <td style="width:40%; margin-bottom:10px" valign="top">
                            <h4 style="padding:0px; margin-top:-5px; margin-bottom:5px;"><b style="font-size:16px;font-weight: 700; color:#333;">Delivery Address</b></h4>
                            <p style="margin:2px 0px;">{{@$info->RequesterName}}</p>
                            <p style="margin:2px 0px;">{{@$info->DeliveryFullAddress}}</p>
                            <p style="margin:2px 0px; margin-bottom:10px">{{@$info->ReceiverMobileNo}}</p>
                        </td>
                        <td width="20%">&nbsp;</td>
                        <td style="width:40%; margin-bottom:10px;"  valign="top">
                            <h4 style=" padding: 0px; margin-top:10px;  margin-bottom:5px;"><b style="font-weight: 700; font-size:16px;  color:#333;">{{-- Delivery Address --}}</b></h4>
                                           <?php
if (@$info['ProductList2']) {
    $totalW = 0;

    foreach (@$info['ProductList2'] as $product) {
        $totalW = $totalW + $product['weight'];
        $unit = $product['weight_unit'];

    }
}

?>

            <p style="margin-top:20px;" align="right">Total Weight: {{@$totalW}} {{@$unit}} </p>
                        </td>
                    </tr>

                </table>
                <div style="clear:both;"></div>
                <div style="width:100%; height:1px; margin-top:20px; margin-bottom:30px;"></div>
                <div style="clear:both;"></div>
            <table cellspacing="0" cellpadding="0" style="width:100%; min-height:400px; margin-top:30px; margin-bottom:10px; border:1px solid #ddd">
            <thead>
                <tr>
                    <th style="padding:8px 8px; font-size:12px;  color:#333; border-right:1px solid #ddd;" align="left">Item</th>
                    <th style="padding:8px 8px; color:#333; border-right:1px solid #ddd;" align="left">Item Name</th>
                    <th style="padding: 8px 8px;  color:#333; border-right:1px solid #ddd;" width="80" align="right">Ordered</th>
                    <th style="padding: 8px 8px;   color:#333; border-right:1px solid #ddd;" width="80" align="right">Delivered</th>
                    <th style="padding: 8px 8px; color:#333;" width="80" align="right">Outstanding</th>
                </tr>
            </thead>
            <tbody>

                @if(@$info['ProductList2'])
               {{$i = 2}}
               @foreach(@$info['ProductList2'] as $product)
               <tr @if(fmod($i,2)==0) style="background:#eee"; @else style="background:"; @endif >
               <td style="padding: 8px; border-right:1px solid #ddd;">{{@$product['package_id']}}</td>
               <td style="padding: 8px; border-right:1px solid #ddd;" >{{$product['product_name']}}</td>
               <td style="padding: 8px; border-right:1px solid #ddd;" width="60" align="right">
               {{$product['qty']}}</td>
               <td style="padding: 8px; border-right:1px solid #ddd;" width="60" align="right">
                {{$product['Delivered']}}</td>
               <td style="padding: 8px 8;" width="60" align="right"> {{$product['Outstanding']}} </td>
                </tr>
                {{$i = $i + 1}}
                @endforeach

              @endif

            </tbody>
        </table>
        <div style="clear:both;"></div>
        <div style="width:100%; height:1px; margin:20px 5px"></div>
        <div style="clear:both;"></div>
                <table style="width:100%">
                    <tr>
                        <td style="width:100%" valign="top">
                            <p style=" font-weight: 500; font-size:12px; margin-bottom:10px; margin-top:4px;">PACKED BY:&nbsp;{{-- {{@Auth::user()->UserName}} --}}<span style="font-size: 12px;"> <input type="text" placeholder="" name="packedBy" value="{{@$info->packedBy}}" style="padding:0px 0px; margin-top:0px; text-align:left; height:17px; border:none; border-bottom:1px solid #ddd; width:65%;">
                            </span></p>
                            <p style="font-weight: 500; font-size:12px; margin-bottom:10px; margin-top:4px;">DELIVERED BY:&nbsp; {{-- {{@$info->TransporterName}} --}}<span style="font-size: 12px;">
                            <input type="text" placeholder="" name="deliveredBy" value="{{@$info->deliveredBy}}" style="padding:0px 0px; margin-top:0px; height:17px; text-align:left; border:none; border-bottom:1px solid #ddd; width:65%;">
                            </span></p>
                            <p style="font-weight: 500; font-size:12px; margin-bottom:0px; margin-top:4px;">
                            RECEIVED IN GOOD CONDITION EXCEPT AS NOTED BY(NAME and SIGNATURE): <span style="font-size: 12px;">
                            <input type="text" placeholder="" value="{{@$info->signature}}" style="padding:0px 0px;  text-align:left; height:17px; border:none; border-bottom:1px solid #ddd; width:200px;"></span></p>
                                              
                            @if(count($info['ProductList2']) < 3)     
                             <p style="padding:0px 0px;  text-align:left; height:17px; border:none; border-bottom:1px solid #ddd; width:100%"></span></p>
                            <p style="padding:0px 0px;  text-align:left; height:17px; border:none; border-bottom:1px solid #ddd; width:100%"></span></p>
                            <p style="padding:0px 0px;  text-align:left; height:17px; border:none; border-bottom:1px solid #ddd; width:100%"></span></p>

                            <p style="padding:0px 0px;  text-align:left; height:17px; border:none; border-bottom:1px solid #ddd; width:100%"></span></p> 
                            @endif
                        </td>
                    </tr>

                </table>
                <table style="text-align:center; width:100%; padding-top:0px; padding-bottom:20px; position:absolute; bottom:80px; width:100%;">
                    <tr>
                        <td>

            <h3 style="text-transform:uppercase; font-size:22px; margin-bottom:10px;"><b style="font-weight: 700; font-size:16px;">Thank you for your business!</b></h3>
            <p style="margin:2px 0;">Should you have any enquiries concerning this delivery, please contact us at:</p>
            <p style="margin:2px 0;">Email:<a style="color: #0e8ac3;" href="mailto:support@aquantuo.com" class="anchor-blue">support@aquantuo.com</a>
            or call us at: Ghana: <a style="color: #0e8ac3;" href="tel:0302434505" class="anchor-blue">030 243 4505</a>
            US: <a style="color: #0e8ac3;" href="tel:8886522233" class="anchor-blue">888 652 2233</a>.</p>

                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

  {{--   <div class="footer">
    Page <span class="pagenum"></span>
</div> --}}

</body>

</html>