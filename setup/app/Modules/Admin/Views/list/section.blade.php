@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')


@section('content')
{!! Html::script('theme/admin/custome/css/parsley.css') !!}
{!! Html::script('theme/admin/custome/js/parsley.min.js') !!}
<style>
.link_circle {
    background: #1FA0D8;
    width: 80px;
    height: 80px;
    border-radius: 50%;
    -webkit-border-radius: 50%;
    -moz-border-radius: 50%;
    align-items: center;
    margin-bottom: 0px;
    display:flex;
    display:-webkit-flex;
    justify-content: center;
    align-items: center;
}
.controll-error{
  color:red;
}
</style>
<div class="container-fluid">
<div class="panel panel-default">
  <div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;<b>Help & support</b>
  </div>
  <div class="panel-body">
		{!! Form::model('', ['method' => 'GET', 'url' => ['admin/section']]) !!}
        		<div class="inline-form">
				<div class="form-group">
					{!! Form::label('Search Section', '', ['class'=>'control-lable']) !!}
					<div>
						{!! Form::text('search_value', Input::get('search_value'), ['class'=>'form-control', 'placeholder'=> 'Search by name','id' =>'inputError1']) !!}
					</div>
                </div>
				</div>
                <div class="inline-form">
                <div class="form-group">
					{!! Form::submit('Search', ['class' => 'btn btn-primary']) !!}
					<a href="{{url('admin/section')}}" class="btn btn-primary">
						<span aria-hidden="true" class="glyphicon glyphicon-refresh">
						</span>&nbsp;&nbsp;Reset
                    </a>

			{{-- 		<a href="{{ url('admin/add-section') }}" data-placement="top" title="Add" class="btn btn-primary btn-action"><i class="fa fa-plus"></i>&nbsp;Add Section</a> --}}

         <!--    <span data-toggle="modal" data-target="#addSection">
            <a href="#" data-toggle="tooltip" data-placement="top" class="btn btn-primary btn-action"  title="Add section"  onclick="this.form.reset();"><i class="fa fa-plus"></i>&nbsp;Add Section</a>
          </span> -->



				</div>
                </div>

		{!! Form::close() !!}
	<div class="clearfix"></div>

    <table class="table table-bordered table-striped table-highlight table-list">
<thead>
	<tr>
		<th width="20px">S.No.</th>
		<th  width="80px;">Image</th>
		<th>Section Name</th>
		<th style="min-width:130px;">Action</th>
	</tr>
	</thead>
			<?php
$sno = 1;
?>
	<tbody>
		<?php if (count($section) > 0) {?>
			@foreach ($section as $lists)

			<tr >
				<td scope="row"><?php echo $sno++; ?></td>
				<td>
          <div class="link_circle">
				    @if ($lists->image != '')
                  <img class="" width="40px" src="<?php echo ImageUrl . 'section/' . $lists->image; ?>" tag="product image" />
                @else
                  <img class="" width="40px" src="{{ImageUrl}}no-image.jpg"  />
               @endif
             </div>
				</td>
				<td>{{ucfirst($lists->section_name)}}</td>
				<td class="width-action-20">
                <div style="position:relative;" class="visible-ipad">
                            <button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Action
                        <span class="caret"></span>
                        </button>
                  </div>

	                 <span data-toggle="modal" data-target="#myModal{{$lists->id}}">
	                <a href="#" data-toggle="tooltip" data-placement="top" class="btn btn-small btn-primary btn-action" title="Edit section"  onclick="this.form.reset();"><i class="fa fa-pencil-square-o">
	                   </i></a>
	               </span>

					<a data-placement="top" title="Faq Question" style="width:38px"; class="btn btn- btn-primary btn-action"  href="{{url('admin/faq/'.$lists->id)}}"><i class="fa fa-question"></i></a>

					<a data-placement="top" title="Delete Record" class="btn btn-small btn-danger btn-action" onclick="remove_record('delete_activity/{{$lists->id}}/section','{{$lists->id}}')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>

				</td>
			</tr>
			@endforeach
		<?php } else {?>
			<tr>
				<td colspan="5" class="text-center" >Section not found</td>
			</tr>
		<?php }?>
	</tbody>
</table>
<div class="clearfix"></div>


  </div> <!-- Panel Body -->
</div>
</div>


 <!-- Modal  add section-->
  <div class="modal fade" id="addSection" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">

          <h4 class="modal-title">Add section</h4>
        </div>
        <div class="modal-body">
           {!! Form::model('', ['name'=>'validate_form','id' => 'addsection','method' => 'post',  'url'=> ['admin/post-add-section'],'files' => true, 'data-parsley-validate']) !!}

                  <div class="col-md-12">
                  <div class="from-group">
                     <label>Section Name</label><br/>
                     <input type="text", name="section_name required" placeholder="Section Name" class="form-control" data-parsley-required-message ='Section Name is required.', value="">
                     @if($errors->has('section_name'))
                     <p class="controll-error help-block"><i class="fa fa-times-circle-o"></i>&nbsp;
                        {{ $errors->first('section_name') }}
                     </p>
                     @endif
                  </div>
                  </div>

                    <div class="col-md-12">
                    <div class="from-group">
                       <br/>
                     <label>Image</label>
                     <input type="file" name="image" id="image">
                     <br/>
                  </div>
                  </div>
                  <br/>

                  <div class="clearfix"></div>
            <br>
        <div class="col-sm-12">
           <button type="submit" class="btn btn-primary" >Send</button>
        <button type="submit" class="btn btn-danger reset" data-dismiss="modal" onclick="this.form.reset();reserParsley(this.form.id);">Close</button>
        </div>
        <div class="clearfix"></div>
             {!! Form::close() !!}
        </div>
      </div>

    </div>
  </div>



	@foreach ($section as $lists)
 <!-- Modal  Send Mail-->
  <div class="modal fade" id="myModal{{$lists->id}}" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">

          <h4 class="modal-title">Edit section</h4>
        </div>
        <div class="modal-body">
           {!! Form::model('', ['id' => 'emailsend'.$lists->id,'method' => 'post', 'url'=> ['admin/post-edit-section'],'files' => true, 'data-parsley-validate']) !!}
                <input type="hidden" name="sectionid" value="{{$lists->id}}">
                  <div class="col-md-12">
                  <div class="from-group">
                     <label>Section Name</label><br/>
                     <input type="text", name="section_name" class="form-control" value="{{$lists->section_name}}">
                     @if($errors->has('section_name'))
                     <p class="controll-error help-block"><i class="fa fa-times-circle-o"></i>&nbsp;
                        {{ $errors->first('section_name') }}
                     </p>
                     @endif
                  </div>
                  </div>

                    <div class="col-md-12">
                    <div class="from-group">
                       <br/>
                     <label>Image</label>
                     <input type="file" name="image" id="image">
                     <br/>
                    <div class="link_circle">
                @if ($lists->image != '')
                  <img class="" width="40px" src="<?php echo ImageUrl . 'section/' . $lists->image; ?>" tag="product image" />
                @else
                  <img class="" width="40px" src="{{ImageUrl}}no-image.jpg"  />
               @endif
                </div>


                  </div>
                  </div>
                  <br/>

                  <div class="clearfix"></div>
            <br>
        <div class="col-sm-12">
           <button type="submit" class="btn btn-primary" >Send</button>
        <button type="submit" class="btn btn-danger reset" data-dismiss="modal" onclick="this.form.reset();reserParsley(this.form.id);">Close</button>
        </div>
        <div class="clearfix"></div>
             {!! Form::close() !!}
        </div>
      </div>

    </div>
  </div>
@endforeach

@include('Admin::layouts.footer')


@stop
