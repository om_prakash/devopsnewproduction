@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')

@section('content')



<div class="container-fluid"> 
<div class="panel panel-default">
  <div class="panel-heading"><i class="fa fa-pencil-square-o"><b>&nbsp;Edit Aquantuo Address</b></i>&nbsp;</div>
  <div class="panel-body">
 
  	<div class="row">
  		<div class="col-md-8">
		{!! Form::model('', ['name'=>'validate_form','method' => 'POST', 'url' => ['admin/update-address']]) !!}
		  		  	
     	  <div class="form-group {{ $errors->has('address') ? ' has-error' : '' }}"  >
		     {!! Form::label('title', 'Address',['class' => 'col-md-2 text-right']) !!}
		     <div class="col-md-6 text-color" >
				{!! Form::text('address', $setting->AqAddress, ['class'=>'form-control required ', 'placeholder'=> 'Address','id' =>'inputError1','maxlength' =>'25']) !!}
				@if ($errors->has('address')) <p class="help-block">{{ $errors->first('address') }}</p> @endif
			</div>
			 <div class="clearfix"></div>
			 </div>
			 <div class="form-group {{ $errors->has('city') ? ' has-error' : '' }}"  >
		     {!! Form::label('city', 'City',['class' => 'col-md-2 text-right']) !!}
		     <div class="col-md-6 text-color">
				{!! Form::text('city', $setting->AqCity, ['class'=>'form-control required', 'placeholder'=> 'City','id' =>'inputError1','maxlength' =>'25']) !!}
				@if ($errors->has('city')) <p class="help-block">{{ $errors->first('city') }}</p> @endif
			</div>
			 <div class="clearfix"></div>
			 </div>
			 <div class="form-group {{ $errors->has('state') ? ' has-error' : '' }}"  >
		     {!! Form::label('state', 'State',['class' => 'col-md-2 text-right']) !!}
		     <div class="col-md-6 text-color">
				{!! Form::text('state', $setting->AqState, ['class'=>'form-control required', 'placeholder'=> 'State','id' =>'inputError1','maxlength' =>'25']) !!}
				@if ($errors->has('state')) <p class="help-block">{{ $errors->first('state') }}</p> @endif
			</div>
			 <div class="clearfix"></div>
			 </div>
			  <div class="form-group {{ $errors->has('country') ? ' has-error' : '' }}"  >
		     {!! Form::label('country', 'Country',['class' => 'col-md-2 text-right']) !!}
		     <div class="col-md-6 text-color">
				{!! Form::text('country', $setting->AqCountry, ['class'=>'form-control required', 'placeholder'=> 'Country','id' =>'inputError1','maxlength' =>'25']) !!}
				@if ($errors->has('country')) <p class="help-block">{{ $errors->first('country') }}</p> @endif
			</div>
			 <div class="clearfix"></div>
			 </div>



			<div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}"  >
		     	{!! Form::label('phone', 'Phone Number',['class' => 'col-md-2 text-right']) !!}
		     	<div class="col-md-6 text-color">
		     		<div class="col-md-3 text-color row">
		     		{!! Form::text('cc', $setting->Aqcc, ['class'=>'form-control required', 'placeholder'=> 'CC','id' =>'inputError1','maxlength' =>'14']) !!}
		     		</div>

		     		<div class="col-md-9 text-color">
					{!! Form::text('phone', $setting->Aqphone, ['class'=>'form-control required', 'placeholder'=> 'Phone Number','id' =>'inputError1','maxlength' =>'14','width'=>'260px']) !!}
					</div>
					@if ($errors->has('phone')) <p class="help-block">{{ $errors->first('phone') }}</p>@endif

					@if ($errors->has('cc')) <p class="help-block">{{ $errors->first('cc') }}</p>@endif
				</div>
			 	<div class="clearfix"></div>
			</div>
			 
			
			 <div class="form-group {{ $errors->has('zipcode') ? ' has-error' : '' }}"  >
		     {!! Form::label('zipcode', 'Zipcode',['class' => 'col-md-2 text-right']) !!}
		     <div class="col-md-6 text-color">
				{!! Form::text('zipcode', $setting->AqZipcode, ['class'=>'form-control required', 'placeholder'=> 'Zipcode','id' =>'','maxlength' =>'5']) !!}
				@if ($errors->has('zipcode')) <p class="help-block">{{ $errors->first('zipcode') }}</p> @endif
			</div>
			 <div class="clearfix"></div>
			 </div>
			 
			 
			 
			 <div class="form-group">
			  <div class="col-md-6 col-md-offset-2">
				{!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
				
				</div>
				</div>
				{!! Form::close() !!}
			 </div>
		</div>
	</div>
</div>
  </div>
        
      
      
      <!-- Pagination Section-->
    
  

  
  </div> <!-- Panel Body -->
</div>
{!! Html::script('theme/admin/custome/js/validate.js') !!}
</div>


<style>
.text-color
{
	color:#a94442;
}	


</style>


<script>


	
new Validate({
	FormName : 'validate_form',
	ErrorLevel : 1,
	
});


</script>

@include('Admin::layouts.footer')
@stop
