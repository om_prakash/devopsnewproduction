@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')


@section('content')
	<div class="container-fluid">
	<div class="panel panel-default">
		<div class="panel-heading"><i class="fa fa-th-list"></i><b> Trip Detail </b> 
          
			 {!! link_to(URL('admin/indiviual-trip'), 'Go Back', ['class' => 'btn btn-primary margin-top-less-7 pull-right']) !!}
         </div>
		<!-- <div class="panel-body"> 
		<div class="col-md-6">
			<div class="row margin-bottom10">
				<div class="col-md-4"><b>Transporter Name</b>:</div>
	            <div class="col-md-8">{{ucfirst($info->TransporterName)}}</div>
	        </div>
	        <div class="row margin-bottom10">
				<div class="col-md-4"><b>Create Date</b>:</div>
	            <div class="col-md-8">{{ show_date(@$info->EnterOn) }}</div>
	        </div>
	        <div class="row margin-bottom10">
				<div class="col-md-4"><b>Source Address</b>:</div>
	            <div class="col-md-8">
	            <?php
				 echo get_formatted_address(array(ucfirst($info['SourceAddress']),$info['SourceCountry'],$info['SourceState'],$info['SourceCity']),"");
				?>
	            </div>
	        </div>
	        
	        <div class="row margin-bottom10">
				<div class="col-md-4"><b>Description</b>:</div>
	            <div class="col-md-8">{{ucfirst($info->Description)}}</div>
	        </div>
		</div>
		<div class="col-md-6">
			<div class="row margin-bottom10">
				<div class="col-md-4"><b>Category</b>:</div>
	            <div class="col-md-8"><?php echo implode(", " , $info['SelectCategory']);?></div>
	        </div>
	        <div class="row margin-bottom10">
				<div class="col-md-4"><b>Update Date</b>:</div>
	            <div class="col-md-8">{{ show_date(@$info->UpdateDate) }}</div>
	        </div>
	        <div class="row margin-bottom10">
				<div class="col-md-4"><b>Destination Address</b>:</div>
	            <div class="col-md-8">
	            <?php 
					echo get_formatted_address(array(ucfirst($info['DestiAddress']),$info['DestiCountry'],$info['DestiState'],$info['DestiCity']),"");
					?>
	            </div>
	        </div>
	    </div> -->
	    <div class="panel-body">
         	<div class="col-sm-4 custom-table1">
                  <div class="row">
                     <b>Transporter Name</b>:
                    

                     <a href="{{ url('admin/user/detail',$info->TransporterId) }}">{{ucfirst($info->TransporterName)}}</a>
                     
                  </div>
            </div>
            <div class="col-sm-4 custom-table1">
                  <div class="row">
                     <b>Shipping Mode</b>:
                     
                     	 @if(@$index['travelMode'] == 'ship') 
					 		Sea
					 	@else
					 		{{ucfirst($info->TravelMode)}}
					 	@endif
                  </div>
            </div>

            <div class="col-sm-4 custom-table1">
                  <div class="row">
                     <b>Trip Type</b>:
                     {{ucfirst($info->TripType)}}
                  </div>
            </div>

            <div class="col-sm-4 custom-table1">
                  <div class="row">
                     <b>Flight No</b>:
                     {{ucfirst($info->FlightNo)}}
                  </div>
            </div>

            <div class="col-sm-4 custom-table1">
                  <div class="row">
                     <b>Airline</b>:
                     {{ucfirst($info->Airline)}}
                  </div>
            </div>

            <div class="col-sm-4 custom-table1">
                  <div class="row">
                     <b>Weight</b>:
                     {{$info->Weight}} {{$info->Unit}}
                  </div>
            </div>
            

            <div class="col-sm-4 custom-table1">
                  <div class="row">
                     <b>Categories</b>:
                     <?php
						if (is_array($info['SelectCategory'])) {
						    echo implode(", ", $info['SelectCategory']);
						} else {
						    echo 'n/a';
						}

					?>
                  </div>
            </div>

            <div class="col-sm-4 custom-table1">
                  <div class="row">
                     <b>Created at</b>:
                     {{  show_date(@$info->EnterOn) }}

                  </div>
            </div>

            <div class="col-sm-4 custom-table1">
                  <div class="row">
                     <b>Update Date</b>:
                     {{  show_date(@$info->UpdateDate) }}
                  </div>
            </div>

            

            <div class="col-sm-4 custom-table1">
                  <div class="row">
                     <b>Status</b>:
                     {{$info->Status}} 
                  </div>
            </div>

            <div class="col-sm-4 custom-table1">
                  <div class="row">
                     <b>Source Date</b>:
                     {{  show_date(@$info->SourceDate) }}
                    
                    
                      
                  </div>
            </div>

            <div class="col-sm-4 custom-table1">
                  <div class="row">
                     <b>Destination Date</b>:
                    
                     {{  show_date(@$info->DestiDate) }}
                     
                  </div>
            </div>

            <div class="col-sm-12 custom-table1">
                  <div class="row">
                     <b>Source Address</b>:
                     {{$info->SourceFullAddress}} 
                  </div>
            </div>

            <div class="col-sm-12 custom-table1">
                  <div class="row">
                     <b>Destination Address</b>:
                     {{$info->DestFullAddress}} 
                  </div>
            </div>

            
            <div class="col-sm-12 custom-table1">
                  <div class="row">
                     <b>Description</b>:
                     {{ucfirst($info->Description)}} 
                  </div>
            </div>
            

            
        
 </div>
 </div>


	<div class="panel panel-default">
	  <div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;<b>{{$title}}</b>
	  </div>
	  <div class="panel-body">
		  
		  <!-- Pagination Section-->
		<input type="hidden" name="urlvalue" id="urlvalue" value="{{$paginationurl}}" />
		<input type="hidden" name="postvalue" id="postvalue" value="{{$postvalue}}" />
	<!-- 	<input type="hidden" name="orderby" id="orderby" value="{{$orderby}}" /> -->
		<input type="hidden" name="orderType" id="orderType" value="{{$orderType}}" />
		
		<div id="containerdata"></div>
		<div class="clearfix"></div>
	  </div> <!-- Panel Body -->
	</div>
</div>
<?php
function get_formatted_address($array,$zipcode)
{
	$address = '';
	foreach($array as $info){
		if(!empty($info)){			
			$address .= (($address != '')? ", $info" : $info);
		}
	}
	if(!empty($zipcode)){ $address .= " - $zipcode"; }
	return $address;
}
?>
@include('Admin::layouts.footer')
@stop
