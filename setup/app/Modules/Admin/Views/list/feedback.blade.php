@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')

@section('content')
{!! Html::style('theme/date-time-picker/jquery.datetimepicker.css') !!}
			
{!! Html::script('theme/date-time-picker/jquery.datetimepicker.js') !!}

<div class="container-fluid">
<div class="panel panel-default">
  <div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;<b>{{$title}}</b></div>
  </div>
 <div class="panel-body">	
 <div class="row">
	 <div class="col-md-7">
	{!! Form::model('', ['method' => 'PATCH', 'url' => ['admin/feedback']]) !!}
			<div class="inline-form">
				<div class="form-group">
					{!! Form::label('Feedback', 'User Name', ['class'=>'control-lable']) !!}
					<div>                    
						{!! Form::text('search_value', Input::get('search_value'), ['class'=>'form-control', 'placeholder'=> 'User Name']) !!}
					</div>					
				</div>
			</div>
			<div class="inline-form">
				<div class="form-group">
					{!! Form::label('Feedback', 'User Group', ['class'=>'control-lable']) !!}
					<select name="group_to" class="form-control">
						<option value="">Select Group</option>
						<option value="requester" <?php if(Input::get('group_to') == 'requester') { echo 'selected="selected"'; } ?> >Requester</option>
						<option value="transporter" <?php if(Input::get('group_to') == 'transporter') { echo 'selected="selected"'; } ?> >Transporter</option>
					</select>
				</div>
			</div>
			<div class="inline-form">
				<div class="form-group">
					{!! Form::label('Feedback', 'State', ['class'=>'control-lable']) !!}
					<div>                    
						{!! Form::text('search_state', Input::get('search_state'), ['class'=>'form-control', 'placeholder'=> 'State']) !!}
					</div>					
				</div>
			</div>
			<div class="inline-form">
				<div class="form-group">
					{!! Form::label('Feedback', 'City', ['class'=>'control-lable']) !!}
					<div>                    
						{!! Form::text('search_city', Input::get('search_city'), ['class'=>'form-control', 'placeholder'=> 'City']) !!}
					</div>					
				</div>
			</div>
			
			
			<div class="inline-form">
				<div class="form-group">
					{!! Form::submit('Search', ['class' => 'btn btn-primary']) !!}
				</div>
			</div>
	{!! Form::close() !!}	
	</div>	
	<div class="col-md-5">
		{!! Form::label('Feedback', 'Average Rating', ['class'=>'control-lable']) !!}
		<div class="clearfix"></div>
		<div class="rating_bar">
			<div style="width:{{($rating * 20) / $count }}px" class="rating"></div>
		</div>
	</div>
</div>	
	<div class="clearfix"></div>


  	  
      <!-- Pagination Section-->
	<input type="hidden" name="urlvalue" id="urlvalue" value="{{$paginationurl}}" />
	<input type="hidden" name="postvalue" id="postvalue" value="{{$postvalue}}" />
	<input type="hidden" name="orderby" id="orderby" value="{{$orderby}}" />
	<input type="hidden" name="orderType" id="orderType" value="{{$orderType}}" />
	<div id="containerdata"></div>
	<div class="clearfix"></div>
	
  	
  </div> <!-- Panel Body --> 
  

</div>
</div>


<script>
	$(function(){
		$('#startDate').datetimepicker({			
			format:'d/m/Y',
			onChangeDateTime:function( ct ){
				$('#endDate').datetimepicker({	minDate:ct	})
			},
			timepicker:false,
		});
	});
</script>
@include('Admin::layouts.footer')
@stop
