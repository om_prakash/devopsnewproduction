@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')

@section('content')

<div class="container-fluid">
	<div class="panel panel-default">
	  	<div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;<b>Payment Log</b>
        &nbsp;<a style="margin-top:-5px;" class="btn btn-primary pull-right" href="{{url($detail_url)}}" >&nbsp;Go to Package Detail</a>&nbsp;

      </div>
	  	<div class="panel-body">
        <div class="col-md-12 row">
          {!! Form::model('', ['method' => 'GET', 'url' => ['admin/payment-log/'.$request_info->_id]]) !!}

          <div class="inline-form">
				    <div class="form-group">
  					   {!! Form::label('StartDate', 'From', ['class'=>'control-lable']) !!}
  					   <div>
    						{!! Form::text('StartDate', Input::get('StartDate'), ['class'=>'form-control', 'placeholder'=> 'From','id' => 'startDate','readonly'=> 'readonly']) !!}
    					</div>
            </div>
          </div>

          <div class="inline-form">
            <div class="form-group">
					     {!! Form::label('EndDate', 'To', ['class'=>'control-lable']) !!}
    					 <div>
    						{!! Form::text('EndDate', Input::get('EndDate'), ['class'=>'form-control', 'placeholder'=> 'To','id' => 'endDate','readonly'=> 'readonly']) !!}
    					 </div>
            </div>
          </div>


          <div class="inline-form">
            <div class="form-group">
              {!! Form::label('', '', ['class'=>'control-lable']) !!}
              {!! Form::submit('Search', ['class' => 'btn btn-primary']) !!}
                <a href="{{URL::to('admin/payment-log',[$request_info->_id])}}" class="btn btn-primary">
                  <span aria-hidden="true" class="glyphicon glyphicon-refresh">
                  </span>&nbsp;&nbsp;Reset
                </a>
            </div>
          </div>

        {!! Form::close() !!}
        </div>
          <!--Other Info -->
          <div class="panel panel-default col-md-6">
              <div class="panel-body">
                <div class="col-md-6 row">
                  <p><label>Product Title:</label>&nbsp;{{ucfirst($request_info->ProductTitle)}}</p>
                  <p><label>Requester Name:</label>&nbsp;{{ucfirst($user_info->Name)}}</p>
                </div>
                <div class="col-md-6 row">
                  <p><label>Package Id:</label>&nbsp;
                  @if($request_info->RequestType == 'online')
                      <a href="{{ url('admin/online_package/detail/'.$request_info->_id) }}">{{$request_info->PackageNumber}}</a>
                    
                  @elseif($request_info->RequestType == 'buy_for_me')
                    <a href="{{ url('admin/buy-for-me/detail/'.$request_info->_id) }}">{{$request_info->PackageNumber}} </a>
                  
                  @else
                     <a href="{{ url('admin/package/detail/'.$request_info->_id) }}">{{$request_info->PackageNumber}}</a>
                  @endif
                  </p>
                </div>
              </div>
          </div>
          <!-- end -->


         </div>
		<div class="clearfix"></div>
	      <!-- Pagination Section-->
		<input type="hidden" name="urlvalue" id="urlvalue" value="{{$paginationurl}}" />
		<input type="hidden" name="postvalue" id="postvalue" value="{{$postvalue}}" />
		<input type="hidden" name="orderby" id="orderby" value="{{$orderby}}" />
		<input type="hidden" name="orderType" id="orderType" value="{{$orderType}}" />
		<div id="containerdata"></div>
		<div class="clearfix"></div>
	  	</div> <!-- Panel Body -->
	</div>
</div>
{!! Html::style('theme/date-time-picker/jquery.datetimepicker.css') !!}
{!! Html::script('theme/date-time-picker/jquery.datetimepicker.js') !!}
<script>
	$(function(){
		$('#startDate').datetimepicker({
			format:'d/m/Y',
			onChangeDateTime:function( ct ){
				$('#endDate').datetimepicker({	minDate:ct	})
			},
			timepicker:false,
		});
		$('#endDate').datetimepicker({
			format:'d/m/Y',
			onChangeDateTime:function( ct ){
				$('#startDate').datetimepicker({	maxDate:ct	})
			},
			timepicker:false,
		});
	});
</script>
@include('Admin::layouts.footer')

@stop
