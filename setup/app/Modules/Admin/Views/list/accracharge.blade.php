@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')

@section('content')
{!! Html::style('theme/admin/custome/css/style.min.css') !!}
<div class="container-fluid">
	<div class="panel panel-default">
	  	<div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;<b>Region</b></div>
	  	<div class="panel-body">
        	<div class="col-md-12 row">

      {!! Form::model('', ['method' => 'GET', 'url' => ['admin/accra-mamagement']]) !!}
                <div class="inline-form">
                <div class="form-group">
          {!! Form::label('Amount', 'Amount', ['class'=>'control-lable']) !!}
          <div>
             <input type="text" value="{{Input::get('amount')}}" class="form-control" placeholder="Amount" name="search_value">
          </div>
                </div>
                </div>


                  <div class="inline-form">
            <div class="form-group">
              {!! Form::label('Country', 'Country', ['class'=>'control-lable']) !!}
                <select name="country" class="form-control required usename-#country#" id="pp_pickup_country" onchange="get_state('pp_pickup_country','pp_pickup_state','pp_pickup_city','pp_pickup_state','','')">

                              <option value="">Select Country</option>
                              @foreach($country as $key)
                               <option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}","state_available":"{{$key->state_available}}"}' >{{$key->Content}}</option>
                              @endforeach

                          </select>
            </div>
          </div>

          <div class="col-sm-2">
                       <div class="form-group">
                          <label class="control-label">Region</label>
                          <select name="state" class="form-control required left-disabled" id="pp_pickup_state" onchange="get_city('pp_pickup_state','pp_pickup_city','pp_pickup_city','')" >
                              <option value="">Select State</option>
                          </select>
                       </div>
                    </div>



                <div class="inline-form">
                <div class="form-group">
          {!! Form::label('', '', ['class'=>'control-lable']) !!}
          {!! Form::submit('Search', ['class' => 'btn btn-primary']) !!}

          <a href="{{URL::to('admin/accra-mamagement')}}" class="btn btn-primary">
            <span aria-hidden="true" class="glyphicon glyphicon-refresh">
            </span>&nbsp;&nbsp;Reset
                    </a>

     {{--          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#accra" data-whatever="@mdo"><i class="fa fa-plus"></i>&nbsp;&nbsp;Add Region</button> --}}
               <a class="btn btn-primary" href="{{ url('admin/add-region') }}"><i class="fa fa-plus"></i>&nbsp;&nbsp;Add Region</a>
                    </div>
        </div>

    {!! Form::close() !!}
         </div>
		<div class="clearfix"></div>
	      <!-- Pagination Section-->
		<input type="hidden" name="urlvalue" id="urlvalue" value="{{$paginationurl}}" />
		<input type="hidden" name="postvalue" id="postvalue" value="{{$postvalue}}" />
		<input type="hidden" name="orderby" id="orderby" value="{{$orderby}}" />
		<input type="hidden" name="orderType" id="orderType" value="{{$orderType}}" />
		<div id="containerdata"></div>
		<div class="clearfix"></div>
	  	</div> <!-- Panel Body -->
	</div>
</div>


  <div class="modal fade" id="accra" tabindex="-1" role="dialog" aria-labelledby="accra">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="exampleModalLabel">Add Region</h4>
        </div>

        {!! Form::model('', ['name' => 'itemForm', 'id' => 'itemForm', 'method' => 'POST', 'url' => ['admin/add-accra']]) !!}
        <div class="modal-body">

          <div class="col-md-6">
             <div class="form-group" id="cityDiv">
            {!! Form::label('Country', 'Country',['class' => 'control-label']) !!}
               <select name="country" class="form-control required usename-#country#" id="pp_pickup_country" onchange="get_state('pp_pickup_country','pp_pickup_state','pp_pickup_city','pp_pickup_state','','')">

                              <option value="">Select Country</option>
                              @foreach($country as $key)
                               <option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}","state_available":"{{$key->state_available}}"}' >{{$key->Content}}</option>
                              @endforeach

                          </select>
            <p class="help-block red" id="er_country" style="color:#a94442"></p>
          </div>
        </div>

                    <div class="col-sm-6">
                       <div class="form-group">
                          <label class="control-label">Region</label><span class="red-star"> *</span>
                             <select name="state" class="form-control required left-disabled" id="pp_pickup_state2" onchange="get_city('pp_pickup_state','pp_pickup_city','pp_pickup_city','')" >
                              <option value="">Select State</option>
                          </select>
                       </div>
                    </div>

                <div class="col-md-12">
                      <div class="form-group">
                            <label class="control-label">Amount</label>
                            <div class="input-group error-input">
                            <span class="input-group-addon">$</span>
                            <input class="form-control required" placeholder="Amount" name="amount"  >
                            <p class="help-block red" id="er_amount" style="color:#ab4442"></p>
                            </div>
                      </div>
                  </div>

        <div class ="clearfix"></div>
        </div>
        <div class="col-sm-12">
        <div class="modal-footer">
        <a class="btn btn-primary" data-dismiss=modal  >Close</a>
        {!! Form::submit('Add', ['class' => 'btn btn-primary']) !!}
        </div>
        </div>
      {!! Form::close() !!}
      <div class="clearfix"></div>
      </div>
    </div>
  </div>
{!! Html::script('theme/admin/custome/js/utility.js') !!}
{!! Html::script('theme/admin/custome/js/validation1.js') !!}
<script>

new Validate({
  FormName : 'itemForm',
  ErrorLevel : 1,

});

</script>

@include('Admin::layouts.footer')

@stop
