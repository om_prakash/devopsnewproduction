@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')


@section('content')
<script>

$(document).ready(function(){
	
        $("#stateForm1").submit(function(){
		var flag = true;
		
		 if($("#Content1").val() =="")
			{ 
				 flag = false;
				 $("#stateDiv1").addClass("has-error");
				 $("#state_msg1").show();
			}
			else
			{
				 $("#city_msg1").hide();
			}
		return flag;
	});
});

</script>
<div class="container-fluid">
<div class="panel panel-default">
  <div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;<b>{{$title}}</b></div>
  <div class="panel-body">		
		{!! Form::model('', ['method' => 'get', 'url' => ['admin/state_list']]) !!}
        	<div class="inline-form">
				<div class="form-group">
					{!! Form::label('Content', 'State Name', ['class'=>'control-lable']) !!}
				                  
						{!! Form::text('search_value', Input::get('search_value'), ['class'=>'form-control', 'placeholder'=> 'Search by State','id' =>'inputError1']) !!}
					</div>					
                </div>
            <div class="inline-form">
				<div class="form-group">
					{!! Form::label('Country', 'Country Name', ['class'=>'control-lable']) !!}
					<?php 
						$country_list = array(''=>'Select Country');
						foreach($country as $key){
							$country_list[$key['_id']] = $key['Content'];

						} ?>
					{!! Form::select('id', $country_list,Input::get('id'),['class'=>'form-control']) !!}
					</div>					
                </div>
                <div class="inline-form">
                <div class="form-group ">
					
					
					{!! Form::submit('Search', ['class' => 'btn btn-primary']) !!}
					<a href="{{URL::to('admin/state_list')}}" class="btn btn-primary">
						<span aria-hidden="true" class="glyphicon glyphicon-refresh">
						
						</span>&nbsp;&nbsp;Reset
                    </a>
					<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#stateModal" data-whatever="@mdo">Add State</button>	
				</div>
                </div>

		{!! Form::close() !!}		
	<div class="clearfix"></div>

  	  
      <!-- Pagination Section-->
	<input type="hidden" name="urlvalue" id="urlvalue" value="{{$paginationurl}}" />
	<input type="hidden" name="postvalue" id="postvalue" value="{{$postvalue}}" />
	<input type="hidden" name="orderby" id="orderby" value="{{$orderby}}" />
	<input type="hidden" name="orderType" id="orderType" value="{{$orderType}}" />
	<div id="containerdata"></div>
	<div class="clearfix"></div>
	</div>   
  	</div> <!-- Panel Body -->
	</div>
</div> 
<!--- Pop Up --> 

<div class="modal fade" id="stateModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title" id="exampleModalLabel">Add State</h4>
	  </div>
	  
	  {!! Form::model('', ['name' => 'stateForm1', 'id' => 'stateForm1', 'method' => 'POST', 'url' => 'admin/add_state?id='.Input::get('id')]) !!}
	  
	  <div class="modal-body clearfix">
	  
			<div class="form-group">
				
				{!! Form::label('Country', 'Select Country:',['class' => 'control-label']) !!}
				<select name= "country" class="form-control">
					
					<option value>Select Country</option>
					<?php  foreach($country as $val){ ?>
					<option value='{"id":"<?php echo $val->_id; ?>","name":"<?php echo $val->Content;?>"}' <?php if($val->_id == Input::get('id')) { echo 'selected="selected"'; } ?> ><?php echo $val->Content;?></option>
					<?php } ?>
				</select>
			</div>
	  
			<div class="form-group" id="stateDiv1">
				{!! Form::label('Content', 'State Name:',['class' => 'control-label']) !!}
				{!! Form::text('Content', '', ['class'=>'form-control', 'placeholder'=> 'State Name','id' =>'Content1']) !!}
				<p class="help-block" id="state_msg1" style="display:none;">state field is required.</p>	
			</div>
			<div class="col-md-12">
						<div class="col-md-6">
						<div class="form-group" id="cityDiv">
						<div class="form-group ">
                           {!! Form::label('Status', 'Status'),':' !!}  </br>
                           <input type="radio" name="state_status" value="Active" checked> Active &nbsp;
                           <input type="radio" name="state_status" value="Inactive"> Inactive
                        </div>
                        <div class="clearfix"></div>
                     
						</div>	
						</div>
						<div class="col-md-6">
						<div class="form-group" id="cityDiv">
						<div class="form-group ">
                           {!! Form::label('Sinup_Status', 'Sinup Status'),':' !!}  </br>
                           <input type="radio" name="state_sinup_status" value="active" checked> Active &nbsp;
                           <input type="radio" name="state_sinup_status" value="inactive"> Inactive
                        </div>
                        <div class="clearfix"></div>
                        </div>
                    
						
				      </div>
						</div>
	  </div>
	  <div class="modal-footer">
		{!! Form::button('Close', ['class' => 'btn btn-primary', 'data-dismiss'=>'modal']) !!}	
		{!! Form::submit('Add', ['class' => 'btn btn-primary']) !!}
	  </div>
	
	{!! Form::close() !!}
	<div class="clearfix"></div>
	</div>
  </div>
</div>
<!--- PopUp End	-->	

@include('Admin::layouts.footer')
@stop
