@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')


@section('content')
{!! Html::style('theme/admin/choose-multiple/bootstrap-chosen.css') !!}
{!! Html::script('theme/admin/choose-multiple/chosen.jquery.js') !!}

<style>
input[type="text"]{height:30px !important;}
</style>

<div style="background:#3F3F3F; margin-top:-20px; padding-top:20px;">
 <div class="container-fluid panel-default">
  <div class="panel-heading" style="height:55 !important"><i class="fa fa-th-list"></i>&nbsp;<b>{{$title}}</b><span class="pull-right"><a href="{{ url('admin/packing-slip-list') }}" class="btn btn-primary" style="margin-bottom: 2px !important;margin-top: 0px !important">Back</a></span></div>
</div>
  <div class="panel-body">
  <div class="receipt_block">
	<div id="containerdata">
	<div class="row">
		{!! Form::model('', ['method' => 'get', 'url' => ['admin/get-packing-slip']]) !!}
		<div class="col-sm-5 col-xs-12">
					

					<label class="control-label">Requester Name</label>
				

					<select class="form-control required chosen-select" id="RequesterName" name="RequesterName" onchange="myFunction()">
	                    <option value="" >Select</option>
	                    <?php foreach ($users as $key) {?>
	                    <option  value='{{$key->_id}}' class="travel-mode-{{$key->TravelMode}}" @if(@trim($val->_id) == @Input::get('RequesterName')) selected @endif>{{$key->Name}}</option>
	                    <?php }?>
	                 </select>
			</div>

			<div class="col-sm-4 col-xs-12">
		    {!! Form::label('Package Id', 'Package Id', ['class'=>'control-lable']) !!}
					<select name="orderId" class="form-control" id="orderId">
					@if(count(@$packageNum)>0)
					@foreach($packageNum as $val)
                   <option value="{{$val['PackageNumber']}}" @if(@$val['PackageNumber'] == @Input::get('orderId')) selected @endif >
                   {{$val['PackageNumber']}}
                   </option>
					@endforeach
					@endif
					</select>
			</div>
			<div class="col-sm-3 col-xs-12">
		    <button type="submit" class="btn btn-primary btn-margin"> Search Slip</button>
			</div>
				{!! Form::close() !!}
	 </div>
	</div>
 @if(count(@$info) > 0)
	<div>
	<hr>
	<div class="clearfix"></div>
	<div class="">
	<div class="row">
	{!! Form::model('', ['method' => 'post', 'url' => ['admin/create-packing-slip'], 'target'=>"_blank"]) !!}

	 <div class="col-sm-7 col-xs-12">
		<div class="logo-div">
		<img src="{{url('')}}/theme/admin/custome/images/logo_receipt.png">
		</div>
	</div>
	<div class="col-sm-5 col-xs-12 data text-right" >
			<h2><b style="color:#004269; padding-right:0px; text-align:right; width:100%; display:block;">Packing Slip</b></h2>
			<p><b>Date:</b>
<input type='text' class='DispatchDate' style="padding:10px 2px; width:140px; height:32px !important" name="PackingSlip" value="@if(@$info->PackingSlip){{show_date(@$info->PackingSlip)}} @else {{date('M d Y h:i a')}} @endif" style="width: 130px; border:1px solid #ddd; padding-top: 5px; height:30px;"/>
	     <i class="fa fa-calendar" id=''></i>

			</p>
	</div>
	<div class="clearfix"></div>

	<br><br>
	<div class="col-sm-8 col-xs-12" style="">
    <p>
    <span style="color:#000; font-weight:700;">USA</span>&nbsp;&nbsp;
    <input type="radio" value="USA" name="address" @if(@$info->address != '131 Race Course St Abeka Accra' ) checked="true" @endif onchange="changeAdd(this.value)" style="width:20px; height:16px; margin:1px;">

    <span style="color:#000; font-weight:700;">Ghana</span>&nbsp;&nbsp;
     <input type="radio" value="Ghana" name="address" @if(@$info->address != '1685 South State Street Dover, DE 19901' && @$info->address) checked="true" @endif onchange="changeAdd(this.value)"  style="width:20px; height:16px; margin:1px;">
         </p>

             <span id="usa" @if(@$info->address != '1685 South State Street Dover, DE 19901') style="display: none" @endif>
             <p>
			<span style="color:#000; font-weight:700;">Street Address:</span>
			&nbsp;&nbsp; 1685 South State Street Dover, DE 19901
			</p>
			<p><span style="color:#000; font-weight:700;">Phone:</span>&nbsp;&nbsp; (+1) 888 652 2233
			</p>
			{{-- <p><span style="color:#000; font-weight:700;">Fax:</span>&nbsp;&nbsp; (+1) 888 652 2233</p> --}}
			</span>
			<input type="hidden" name="addressVal" id="addressVal" value="1685 South State Street Dover, DE 19901">

			<span id="ghana" @if(@$info->address == '1685 South State Street Dover, DE 19901') style="display: none" style="display: none" @endif>
			<p>
			<span style="color:#000; font-weight:700;">Street Address:</span>
			&nbsp;&nbsp; 131 Race Course St Abeka Accra</p>
			<p><span style="color:#000; font-weight:700;">Phone:</span>&nbsp;&nbsp;  (+233) 30 243 4505 or (+233) 55 496 7571 or (+233) 50 163 4195
			</p>
            </span>

			<p><span style="color:#000; font-weight:700;">Website:</span>&nbsp;&nbsp; <a href="http://www.aquantuo.com">www.aquantuo.com </a></p>
	</div>
	<div class="col-sm-4 col-xs-12 text-right" style="">
		<div style="width:141px; float:right">
		<p><span style="background:#; padding:5px 8px 5px 3px;  float:left; color:#000; margin:5px 0; display:inline-flex;">
		<input type="radio" value="yes" name="paidStatus" @if(@$info->paidStatus == 'yes')checked="true" @endif style="float:left; width:20px; height:16px; margin:1px;">  Paid?</span>
		<span style="background:#; padding:5px 8px 5px 3px;  float:left; color:#000; margin:5px 0; display:inline-flex; margin-left:10px;">
		<input type="radio" value="hold" name="paidStatus" @if(@$info->paidStatus != 'yes')checked="true" @endif style="float:left; width:20px; height:16px; margin:1px;">  Hold?</span></p>
		</div>
		<div class="clearfix"></div>
		<div>
		<span style="color:#000; font-weight:700;">Package Status:</span> {{ get_status_title(@$info->Status)['status'] }}
		</div>
	</div>

	<div class="clearfix"></div><br><br>
	{{-- <div class="col-sm-4 col-xs-12" style="">
		<h4 class="heading-bg"style="margin-bottom:10px;"><b>Bill To:</b></h4>
		<p style=" margin-bottom:6px;"><span style="color:#000; font-weight:700; margin-bottom:10px;">Name:&nbsp;</span> {{@$info->RequesterName}}</p>
		 <p style=" margin-bottom:6px;"><span style="color:#000; font-weight:700; margin-bottom:10px;">Company Name:&nbsp;</span> Ideal IT Technologys</p>
		<p style=" margin-bottom:6px;"><span style="color:#000; font-weight:700; margin-bottom:10px;">Street Address:&nbsp;</span> {{@$info->DeliveryFullAddress}}</p>
		 <p style=" margin-bottom:6px;"><span style="color:#000; font-weight:700; margin-bottom:10px;">City, ST ZIP:&nbsp;</span> 452101</p>
		<p style=" margin-bottom:6px;"><span style="color:#000; font-weight:700; margin-bottom:10px;">Phone:&nbsp;</span> {{@$info->ReceiverMobileNo}}</p>
	</div> --}}
	<div class="col-sm-8 col-xs-12" style="">
		<h4 class="heading-bg" style="margin-bottom:10px;"><b>Customer Detail:</b></h4>
		<p style=" margin-bottom:6px;"><span style="color:#000; font-weight:700; margin-bottom:10px;">Name:&nbsp;</span> {{@$info->RequesterName}}</p>
		{{-- <p style=" margin-bottom:6px;"><span style="color:#000; font-weight:700; margin-bottom:10px;">Company Name:&nbsp;</span> Ideal IT Technologys</p> --}}
		<p style=" margin-bottom:6px;" ><span style="color:#000; font-weight:700;">Street Address:&nbsp;</span>
		 {{@$info->DeliveryFullAddress}}</p>
		{{-- <p style=" margin-bottom:6px;"><span style="color:#000; font-weight:700; margin-bottom:10px;">City, ST ZIP:&nbsp;</span> 452101</p> --}}
		<p style=" margin-bottom:6px;"><span style="color:#000; font-weight:700; margin-bottom:10px;">Phone:&nbsp;</span> {{@$info->ReceiverMobileNo}}</p>
	</div>
	<div class="col-sm-4 col-xs-12 text-right">
		<h4 class="heading-bg" style="margin-bottom:10px;"><b>Transporter Detail:</b></h4>
		<p  style=" margin-bottom:6px;"><span style="color:#000; font-weight:700; margin-bottom:10px;">&nbsp;</span> {{@$info->TransporterName}}</p>
		{{-- <p  style=" margin-bottom:6px;"><span style="color:#000; font-weight:700; margin-bottom:10px;">Company Name:&nbsp;</span> Ideal IT Technologys</p>
		<p  style=" margin-bottom:6px;"><span style="color:#000; font-weight:700; margin-bottom:10px;">Street Address:&nbsp;</span> 131 Race Course Street Abeka Accra (Opposite Derby
Royal Hotel)</p>
		<p  style=" margin-bottom:6px;"><span style="color:#000; font-weight:700; margin-bottom:10px;">City, ST ZIP:&nbsp;</span> 452101</p>
		<p  style=" margin-bottom:6px;"><span style="color:#000; font-weight:700;">Phone:&nbsp;</span> 123-456-4154</p>
	</div> --}}

	</div>
	</div>

	               <?php
if (@$info['ProductList']) {
    $totalW = 0;

    foreach (@$info['ProductList'] as $product) {
        $totalW = $totalW+@$product['weight'];
        $unit = @$product['weight_unit'];

    }
}

?>

	<div class="row">
		<div class="col-sm-12 col-xs-12">
		<div class="table-responsive table-stripped" style="margin:20px 0;">

		<input type="hidden" name="RequesterName" value="{{@Input::get('RequesterName')}}">
		<input type="hidden" name="orderId" value="{{@Input::get('orderId')}}">
		<input type="hidden" name="slip_id" value="{{@Input::get('slip_id')}}">

		<table class="custom-table  table-stripped" style="border:1px solid #ddd; width:100%; margin:25px 0 35px">
			<thead>
				<tr>
					{{-- <th width="25%" style=" padding:10px 10px; border-right:1px solid #ddd;">Received Date</th> --}}
					<th  width="33%" style="padding:8px 10px; border-right:1px solid #ddd;">Shipping Date</th>
					<th width="33%" style="padding:8px 10px; border-right:1px solid #ddd;">Arrival Date</th>
					<th width="34%" style="padding:8px 10px; border-right:1px solid #ddd;">Total Weight</th>
					</tr>
				</thead>
			<tbody>
				<tr>
					{{-- <td style="padding:5px 10px; border-right:1px solid #ddd;">
                   <input type='text' class='DispatchDate' style="padding:7px 2px; width:140px;" name="ReceivedDate" value="@if(@$info->ReceivedDate){{show_date(@$info->ReceivedDate)}} @else {{date('M d Y h:i a')}} @endif" style="width: 130px; border:1px solid #ddd; padding-top: 5px; height:30px;"/>
	     <i class="fa fa-calendar" id=''></i>
					</td> --}}
					<td style="padding:5px 10px; border-right:1px solid #ddd;">
                      <input type='text' class='DispatchDate' style="padding:7px 2px; width:200px;" name="ShippingDate" value="@if(@$info->ShippingDate){{show_date(@$info->ShippingDate)}} @else {{date('M d Y h:i a')}} @endif" style="width: 200px; border:1px solid #ddd; padding-top: 5px; height:30px;"/>
	     <i class="fa fa-calendar" id=''></i>
					 </td>
					<td style="padding:5px 10px; border-right:1px solid #ddd;">
                <input type='text' class='DispatchDate' style="padding:7px 2px; width:200px;" name="ArrivalDate" value="@if(@$info->ArrivalDate){{show_date(@$info->ArrivalDate)}} @else {{date('M d Y h:i a')}} @endif" style="width: 200px; border:1px solid #ddd; padding-top: 5px; height:30px;"/>
	     <i class="fa fa-calendar" id=''></i>
					 </td>
					<td style="padding:5px 10px; border-right:1px solid #ddd;"> @if(@$totalW){{@$totalW}} {{@$unit}} @else 0 @endif</td>
				</tr>
			</tbody>

		</table>
		<table class="custom-table  table-stripped" style="border:1px solid #ddd; width:100%;">
		<thead>
			<tr>
				<th width="15" style=" padding:10px 10px; border-right:1px solid #ddd;">Item Id</th>
				<th width="35" style="padding:8px 10px; border-right:1px solid #ddd;">Received Date</th>
				<th width="30" style="padding:8px 10px; border-right:1px solid #ddd;">Item Name</th>
				<th width="10" style="padding:8px 10px; border-right:1px solid #ddd;" >Item Weight</th>
				<th width="10" style="padding:8px 10px; border-right:1px solid #ddd;" >Received/Shipped Qty</th>
			</tr>
		</thead>
		<tbody>

		 <?php $quantity = 0?>

		@if(@$info['ProductList'])
       <?php $i = 2?>
		@foreach(@$info['ProductList'] as $product)
		<tr>
		<td style="padding:5px 10px; border-right:1px solid #ddd;" @if(fmod($i,2)==0) style="background:#eee"; @else style="background:"; @endif >
		<input type="hidden" name="package_id[]" value="{{@$product['package_id']}}">
		<input type="hidden" name="weight[]" value="{{@$product['weight']}}">
		<input type="hidden" name="weight_unit[]" value="{{@$product['weight_unit']}}">
		{{@$product['package_id']}}</td>


		<td align="left" style="padding:5px 10px; border-right:1px solid #ddd;">
		<input type="text" name="ReceivedDate[]" class='DispatchDate' value="@if(@$product['ReceivedDate'] != '') {{show_date(@$product['ReceivedDate'])}} @endif" style="text-align: left; width: 80% !important">
		<i class="fa fa-calendar" id=''></i>
		</td>

		<td align="left" style="padding:5px 10px; border-right:1px solid #ddd;">
		<input type="hidden" name="product_name[]" value="{{@$product['product_name']}}">
		{{@$product['product_name']}}
		</td>
		<td align="left" style="padding:5px 10px; border-right:1px solid #ddd;">

		<input type="hidden" name="Ordered[]" value="{{@$product['qty']}}" >
		{{@$product['weight']}} {{@$product['weight_unit']}}
		</td>
		<td align="left" style="padding:5px 10px; border-right:1px solid #ddd;">
		<input type="text" name="Delivered[]" value="@if(@$product['Delivered']){{@$product['Delivered']}} @else {{@$product['qty']}} @endif" >

		</td>
        {{! $quantity = $quantity+ @$product['qty']}}
         {{!$i = $i + 1}}
		</tr>
		@endforeach

			@else
		<tr>

		<td style="padding:5px 10px; border-right:1px solid #ddd;">
		<input type="hidden" name="package_id[]" value="{{@$info['PackageNumber']}}">
		<input type="hidden" name="weight[]" value="{{@$info['ProductWeight']}}">
		<input type="hidden" name="weight_unit[]" value="{{@$info['ProductWeightUnit']}}">
		{{@$info['PackageNumber']}}
		</td>

        <td align="left" style="padding:5px 10px; border-right:1px solid #ddd;">
		<input type="text" name="ReceivedDate[]" class='DispatchDate' value="{{show_date(@$info['EnterOn'])}}" style="text-align: left;">
		</td>

		<td style="padding:5px 10px; border-right:1px solid #ddd;">
		<input type="hidden" name="product_name[]" value="{{@$info['ProductTitle']}}">
		{{@$info['ProductTitle']}}
		</td>

		<td style="padding:5px 10px; border-right:1px solid #ddd;" width="100px">
		<input type="hidden" name="Ordered[]" value="@if(@$info['qty']){{@$info['qty']}}@else 1 @endif" style="width:100px">
        {{@$info['ProductWeight']}} {{@$info['ProductWeightUnit']}}
		</td>

		<td style="padding:5px 10px;"  width="100px"><input type="text" name="Delivered[]" value="0" style="width: 100px">
		</td>

		</tr>

		 @if(@$info['qty']){{! $quantity = $quantity+@$info['qty']}}@else {{!$quantity = 1 }}@endif
		@endif
		</tbody>
		<tfoot>
		<tr>
		<td colspan="4" style="padding:5px 10px; border:1px solid #ddd;">&nbsp;</td>
		</tr>
				<tr>
					<th style="padding:5px 10px">&nbsp;</th> <th style="padding:5px 10px">&nbsp;</th>
					<th style="padding:5px 10px">&nbsp;</th>
					<th style="padding:5px 10px; border-right:1px solid #ddd; text-align:right;" align="right">TOTAL</th>
					{{-- <th style="padding:5px 10px; border-right:1px solid #ddd; text-align:center;" align="center">
					<input type="text" name="Ordered[]" value="{{@$quantity}}" style="100px"></td>
					</th> --}}
					<th style="padding:5px 10px; border:1px solid #ddd; text-align:center;" align="center" >	<input type="text" name="Ordered[]" value="{{$quantity }}" style="width:100px">
					</th>
				</tr>
			</tfoot>
		</table>
		</div> </div>
		<div class="clearfix"></div>
		<div class="clearfix"></div>
		<div class="col-sm-12">
		<h5 style="margin-bottom:15px; margin-top:0; word-break: break-all;">Comments:&nbsp;
		 <input type="text" placeholder="" name="Comments" value="{{$info['comments']}}" style="padding:0px 0px; margin-top:-10px; text-align:left; height:17px; border:none; border-bottom:1px solid #ddd; width:90%;"></h5>
		</div>
		<div class="col-sm-12 col-xs-12">
		{{-- <a href="{{url('/admin/get-packing-slip')}}" class="btn slip-btn">Reset Slip</a> --}}
		 <div class="col-sm-2">
               <input type="reset" class="btn slip-btn" value="Reset Slip" style="height:34px !important; border:none;">
               </div>
		<button type="submit" class="btn slip-btn"> Create Packing Slip</button>
	</div>

           		{!! Form::close() !!}
	             </div>
	             </div>
	             </div>
	             	@endif
	</div>
  	</div> <!-- Panel Body -->


</div>

<style type="text/css">
  #user_chosen{
   width: 100% !important;
  }
</style>

{!! Html::style('theme/admin/choose-multiple/bootstrap-chosen.css') !!}
{!! Html::script('theme/admin/choose-multiple/chosen.jquery.js') !!}
{!! Html::style('theme/date-time-picker/jquery.datetimepicker.css') !!}
{!! Html::script('theme/date-time-picker/jquery.datetimepicker.js') !!}
@include('Admin::layouts.footer')

		
<!-- 	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>  -->
 		

 		
        


<script type="text/javascript">

	function changeAdd(address)
        	{
        		if(address == 'USA')
        		{$("#ghana").hide();
        		  $("#usa").show();
        		  $('#addressVal').val('1685 South State Street Dover, DE 19901');
        		}else{$("#usa").hide();
        			 $("#ghana").show();
        			 $('#addressVal').val('131 Race Course St Abeka Accra');
        		}
        	}

jQuery(document).ready(function ($) {
$(".chosen-select").chosen({no_results_text: "Oops, nothing found!"})
});


jQuery(document).ready(function ($) {
		$('.DispatchDate').datetimepicker({
			format:'M d Y h:i a',
			timepicker:true,
			scrollInput : false,
		});
	});


function myFunction()
{
var name = $('#RequesterName').val();

  $.ajax({
        url: SITEURL+'admin/slip-orderId',
        data: { 'name' : name},
        type: "post",
        cache: false,
        success: function (res) {
        var obj = jQuery.parseJSON(res);

        var HtmalVal ='';
        $("#orderId").empty();

        for (var i=0; i<= obj.length; i++)
        {
         var HtmalVal ="<option value='"+obj[i].PackageNumber+"'>"+obj[i].PackageNumber+"</option>"
          $("#orderId").append(HtmalVal);
        }

     }

    });

}

</script>
@endsection