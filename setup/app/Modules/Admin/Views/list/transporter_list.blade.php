@extends('Admin::layouts.master')


@section('content')
@include('Admin::layouts.menu')
{!! Html::style('theme/date-time-picker/jquery.datetimepicker.css') !!}
{!! Html::script('theme/admin/custome/js/validation.js') !!}
{!! Html::script('theme/date-time-picker/jquery.datetimepicker.js') !!}

<div class="container-fluid">
<div class="panel panel-default">
  <div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;<b>Transporter List</b></div>
  <div class="panel-body">


            <div class="col-md-12">
                <form role="form">
			<div class="inline-form">
				<div class="form-group">
        			{!! Form::label('UserFirstName', 'Transporter Name') !!}
                    <div>
					{!! Form::text('search_value', Input::get('search_value'), ['class'=>'form-control', 'placeholder'=> 'Transporter Name']) !!}
                    </div>
				</div>
			</div>
			<div class="inline-form">
				<div class="form-group">
        			{!! Form::label('UserEmail', 'Email') !!}
                    <div>
					{!! Form::text('search_email', Input::get('search_email'), ['class'=>'form-control', 'placeholder'=> 'Email']) !!}
                    </div>
				</div>
			</div>
			<div class="inline-form">
				<div class="form-group">
        			{!! Form::label('SSN', 'SSN') !!}
                    <div>
					{!! Form::text('search_ssn', Input::get('search_ssn'), ['class'=>'form-control', 'placeholder'=> 'SSN']) !!}
                    </div>
				</div>
			</div>
			<div class="inline-form">
				<div class="form-group">

					{!! Form::label('UserClass', 'Transporter Type') !!}

					{!! Form::select('transporter_type', array(''=>'Select Transporter Type','individual'=>'Individual','business'=>'Business'),Input::get('transporter_type'),['class'=>'form-control']); !!}
				</div>
			</div>

			<div class="inline-form">
				<div class="form-group">

					{!! Form::label('Unit', 'Unit#') !!}
                	{!! Form::text('search_unit', Input::get('search_unit'), ['class'=>'form-control', 'placeholder'=> 'Unit#']) !!}

				</div>
			</div>

				<div class="inline-form">
				<div class="form-group">
					{!! Form::submit('Search', ['class' => 'btn btn-primary']) !!}
				</div>
			</div>

			<div class="inline-form">
                <div class="form-group">
					<a href="{{URL::to('admin/transporter')}}" class="btn btn-primary">
						<span aria-hidden="true" class="glyphicon glyphicon-refresh">
						</span>&nbsp;&nbsp;Reset
                	</a>
                </div>
			</div>

				<div class="form-group">
		   <a class="btn-flat btn btn-primary mt-10" href="{{url('admin/transporter/export-excel')}}?&search_value={{input::get('search_value')}}&City={{Input::get('City')}}&State={{Input::get('State')}}&Country={{Input::get('Country')}}"><span class="fa fa-file-excel-o"></span>&nbsp;Export csv</a>
		</div>

			</div>

			</form>
	<div class="clearfix"></div>


      <!-- Pagination Section-->

	<input type="hidden" name="urlvalue" id="urlvalue" value="{{$paginationurl}}" />
	<input type="hidden" name="postvalue" id="postvalue" value="{{$postvalue}}" />
	<input type="hidden" name="orderby" id="orderby" value="{{$orderby}}" />
	<input type="hidden" name="orderType" id="orderType" value="{{$orderType}}" />

	<div id="containerdata"></div>
	<div class="clearfix"></div>
	{!! Form::close() !!}



</div> <!-- Panel Body -->
</div>

</div>
</div>

<div class="modal fade" id="transfer_fund_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Transfer Fund</h4>
      </div>
      {!! Form::model('', ['class' => 'form-horizontal', 'method' => 'POST','id' => 'transfer_fund_modal_body', 'onsubmit'=>'return transfer_fund_modal_body()']) !!}

      {!! Form::close() !!}
    </div>
  </div>
</div>

<div id="myModal_transfer" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Payment Information</h4>
      </div>

        {!! Form::model('', ['method' => 'POST', 'id' => 'transfer_driver_form', 'onsubmit'=>'return transfer_driver_modal_body()']) !!}

      <div class="col-md-12">
      	<div id="driver_payment" class="col-md-12" style="margin-top: 10px;">
      	</div>
	  </div>
	  <div class="col-md-12">
      	<div id="history" ></div>
      	</div>
	<label class="control-label col-sm-6" for="email">Pay Amount: $<span  id="amountInfo"></span></label>

				<input type="hidden" value="0" class="form-control" name="amount" placeholder="amount" id="amount" readonly>


        <div class="modal-footer">
        	<button type="submit"  id="driver_payment_loader" class="btn btn-primary">Transfer</button>
        	<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
      {!! Form::close() !!}
    </div>

  </div>
</div>



<script>
	$(function(){
		$('#startDate').datetimepicker({
			format:'d/m/Y',
			onChangeDateTime:function( ct ){
				$('#endDate').datetimepicker({	minDate:ct	})
			},
			timepicker:false,
		});
		$('#endDate').datetimepicker({
			format:'d/m/Y',
			onChangeDateTime:function( ct ){
				$('#startDate').datetimepicker({	maxDate:ct	})
			},
			timepicker:false,
		});
	});
</script>


<!-- Modal -->
<div class="modal fade" id="refund-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Refund</h4>
      </div>
      <div id="refund-data" class="msg_stripe"></div>
    </div>
  </div>
</div>
@include('Admin::layouts.footer')
@stop

<script>
function send_verification_code(id){
	if (confirm("Are you sure all verification checks have been completed for the Transporter? Click OK to enable profile!")) {

	$('#verification_section_'+id+' >').addClass('spinning');
	var url = SITEURL+'ajax/verify_user';
	$.ajax
	({
		type: "POST",
		url: url,
		data: "id="+id,
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		success: function(res)
		{
			var obj = eval("("+res+")");
			if(obj.success == 1)
			{
				$('#verification_section_'+id).html('<span class="badge bg-green">Verified</span>');
			}
			alert(obj.msg);

			$('#verification_section_'+id+' >').removeClass('spinning');
		}
	});
	}

}

function add_bank_account(id){
  if (confirm("Are you sure? You want to attach transporter account to your stripe account.")) {
	var url = SITEURL+'ajax/add_account_to_stripe';
	$('#bank_ac_section_'+id+' > span').addClass('spinning');
	$.ajax
	({
		type: "POST",
		url: url,
		data: "id="+id,
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		success: function(res)
		{
		$('#bank_ac_section_'+id+' > span').removeClass('spinning');
			var obj = eval("("+res+")");
			if(obj.success == 1)
			{
				$('#bank_ac_section_'+id).html('<span class="badge bg-green">Added</span>');
			}

			alert(obj.msg);
		}

	});
}
}

function transfer_fund(id)
{
	var url = SITEURL+'ajax/transfer_fund_view';
	$.ajax
	({
		type: "GET",
		url: url,
		data: "id="+id,
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		success: function(res)
		{
			$('#transfer_fund_modal_body').html(res);
			$('#transfer_to').val(id);
		}
	});

}

function transfer_fund_modal_body()
{
	var flag = true;

	if(valid.required('transfer_amount','amount') == false){ flag = false; }else{
		if(valid.numeric('transfer_amount','amount') == false){ flag = false; }
	}
	if(flag == true)
	{
		 if (confirm(" Are you sure? You want to trasfer fund to transporter!")) {
		$.ajax
		({
			type: "POST",
			url: SITEURL+'ajax/transfer_fund_to_carrier',
			data: $('#transfer_fund_modal_body').serialize(),
			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			success: function(res)
			{
				var obj = eval('('+res+')');
				if(obj.success == 1)
				{
					$('#transfer_fund_modal_body')[0].reset();
					$('#fund-error').html('<div role="alert" class="alert alert-success alert-dismissible fade in">'
										+'<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>'+obj.msg+'</div>');

				}else{
					$('#fund-error').html('<div role="alert" class="alert alert-danger alert-dismissible fade in">'
	      				+'<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>'+obj.msg+'</div>');
				}

			}
		});
	}
}
	return false;

}


function get_package_information(id)
{
	var url = SITEURL+'ajax/get_package_information/'+id;
	$('#loading').show();
	$.ajax({
		type:"GET",
		url:url,
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		success: function(msg)
		{
			$('#loading').hide();
			$("#refund-data").html(msg);
		}
	});
}
function make_refund(id)
{
	var refundAmount =$("#refund_amount").val();
	 if(refundAmount =="")
		{
			 flag = false;
			 $("#refund_amountdiv").addClass("has-error");
			 $("#refund_amountError").html('Refund amount field is required.');
		}
		else
		{

		  if(!$.isNumeric(refundAmount))  //if(Number(refundAmount)<0)
		  {
			$("#refund_amountdiv").addClass("has-error");
			$("#refund_amountError").html('Refund amount is only positive number.');
		  }
		   else
		   {
				$("#refund_amountError").html('');
				//var id = "5628e4886734c4a0637dd22c";
				var url = SITEURL+'ajax/get_refund_information/'+id+'/'+refundAmount;
				$('#loading').show();
				$.ajax({
					type:"GET",
					url:url,
					dataType: "json",
					headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
					success: function(msg)
					{
						var json1 = JSON.parse(msg);
						//console.log(json1);
						if(json1.error.type =="invalid_request_error")
						{
							var respons = '<p class="error_msg">'+json1.error.message+'</p>';
						}
						else if(json1.status =="succeeded")
						{
							var respons = '<p class="success_msg">'+"Your amount is refunded"+'</p>';
						}
						else
						{
							var respons = "Your stripe id valid";
						}
						$('#loading').hide();
						$("#refund-data").html(respons);
					}
				});
		   }
		}
}

function transfer_history(deliveryboyId){
		$.ajax({
			url  : SITEURL+'ajax/transaction-history-driver',
			type : 'POST',
			data : 'deliveryboyId='+deliveryboyId,
			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			success:function(res){
				console.log(res);
				$("#history").html(res);
				add_total(1);
			}
		});
	}


 	function add_total(OrderId){
 		var driverValue = $("#driverValue_"+OrderId).val();
 		var amount      = $("#amount").val();
 		var total =  0;

 		$(".addTotal").each(function(){
 			if($(this).is(":checked")) {
		        total = total + parseFloat($('#driverValue_'+$(this).val()).val());
 			}
 		});
 		$("#amount").val(parseFloat(total));
 		$("#amountInfo").html(parseFloat(total));

 	}


function transfer_driver_modal_body(){
 	var flag = true;
 	var deliveryBoyId = $("#deliveryBoyId").val();
 	if ($("#amount").val() == 0 || $("#amount").val() == "") {
 		alert('Sorry $0 amount can not transfer.');
 		flag = false;
 	}

 	if (flag == true) {
 		$("#driver_payment_loader").addClass('spinning');
	 	$.ajax({
	 		url  : SITEURL+'ajax/transaction_to_driver',
	 		type :'POST',
	 		data :$("#transfer_driver_form").serialize(),
	 		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
	 		success:function(res){

	 			$("#driver_payment_loader").removeClass('spinning');
	 			var obj = eval("("+res+")");

	 			if (obj.success == 1) {
	 			    window.setTimeout(function() {
   					 $(".alert-success").fadeTo(3000, 0).slideUp(100, function(){
   						 });
						}, 1000);
	 				$("#driver_payment").html('<div role="alert" class="alert alert-success alert-dismissible fade in">'
										+'<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>'+obj.msg+'</div>');
	 				/* $("#driver_payment").addClass('alert alert-success'); */
	 			}
	 			else{
	 			    window.setTimeout(function() {
   					 $(".alert-danger").fadeTo(3000, 0).slideUp(100, function(){
   						 });
						}, 1000);

	 				$("#driver_payment").html('<div role="alert" class="alert alert-danger alert-dismissible fade in">'
	      				+'<button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button>'+obj.msg+'</div>');
	 				<!-- $("#driver_payment").addClass('alert alert-danger'); -->
	 			}
	 		}
	 	});
 	}
 	return false;

 }


</script>
