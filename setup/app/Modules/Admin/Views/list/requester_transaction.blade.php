@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')

@section('content')
{!! Html::style('theme/date-time-picker/jquery.datetimepicker.css') !!}		
{!! Html::script('theme/date-time-picker/jquery.datetimepicker.js') !!}

<div class="container-fluid">
<div class="panel panel-default">
  <div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;<b>Requester Transaction List</b></div>

  <div class="panel-body">	
  	
    <div class="col-md-10 row">  
		{!! Form::model('', ['method' => 'PATCH', 'url' => ['admin/requester_transaction']]) !!}
        		<div class="inline-form">
                	<div class="form-group">
                        {!! Form::label('FirstName', 'Transfer By / Requester Name', ['class'=>'control-lable']) !!}
                        {!! Form::text('search_value', Input::get('search_value'), ['class'=>'form-control', 'placeholder'=> 'Transfer By / Requester Name','id' =>'inputError1']) !!}
                	</div>
                </div>
               
                
               
        
				<div class="inline-form">
                <div class="form-group">
                    {!! Form::submit('Search', ['class' => 'btn btn-primary']) !!}
                    
                    <a href="{{URL('admin/requester-transaction-history')}}" class="btn btn-primary">
						<span aria-hidden="true" class="glyphicon glyphicon-refresh">
						
						</span>&nbsp;&nbsp;Reset
                    </a>
                </div>
                </div>

             </div>

	  {!! Form::close() !!}
     <!-- Panel Body -->
        	
	<div class="clearfix"></div>
    
 
      <!-- Pagination Section-->
	<input type="hidden" name="urlvalue" id="urlvalue" value="{{$paginationurl}}" />
	<input type="hidden" name="postvalue" id="postvalue" value="{{$postvalue}}" />
	<input type="hidden" name="orderby" id="orderby" value="{{$orderby}}" />
	<input type="hidden" name="orderType" id="orderType" value="{{$orderType}}" />
	<div id="containerdata"></div>
	<div class="clearfix"></div>
    
    </div> <!-- Panel Body -->
</div>
    
	


</div>
<script>
	$(function(){
		$('#startDate').datetimepicker({			
			format:'d/m/Y',
			onChangeDateTime:function( ct ){
				$('#endDate').datetimepicker({	minDate:ct	})
			},
			timepicker:false,
		});
		$('#endDate').datetimepicker({
			format:'d/m/Y',
			onChangeDateTime:function( ct ){
				$('#startDate').datetimepicker({	maxDate:ct	})
			},
			timepicker:false,
		});
	});
</script>
  
@include('Admin::layouts.footer')
@stop
