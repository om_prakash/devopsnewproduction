@extends('Admin::layouts.master')
@extends('Admin::layouts.menu')

@section('content')

<div class="container-fluid">
	<div class="panel panel-default">
	  	<div class="panel-heading"><i class="fa fa-th-list"></i>&nbsp;<b>Requester List</b></div>
	  	<div class="panel-body">	
        	<div class="col-md-12 row">	
			{!! Form::model('', ['method' => 'GET', 'url' => ['admin/requester']]) !!}			
            	<div class="inline-form">
					<div class="form-group">
					{!! Form::label('UserFirstName', 'Requester Name', ['class'=>'control-lable']) !!}                   
					{!! Form::text('search_value', Input::get('search_value'), ['class'=>'form-control', 'placeholder'=> 'Requester Name']) !!}
	                 </div>
                 </div>
                 <div class="inline-form">
					<div class="form-group">
					{!! Form::label('Email', 'Email', ['class'=>'control-lable']) !!}                   
					{!! Form::text('search_email', Input::get('search_email'), ['class'=>'form-control', 'placeholder'=> 'Email']) !!}
	                 </div>
                 </div>

                 <div class="inline-form">
					<div class="form-group">
					{!! Form::label('Unit#', 'Unit#', ['class'=>'control-lable']) !!}                   
					{!! Form::text('search_unit', Input::get('search_unit'), ['class'=>'form-control', 'placeholder'=> 'Unit#']) !!}
	                 </div>
                 </div>

                 <div class="inline-form">
					<div class="form-group">
					{!! Form::label('Country', 'Country', ['class'=>'control-lable']) !!}                   
					
					<select name="Country" class="form-control required" id="pp_pickup_country" onchange="get_state('pp_pickup_country','pp_pickup_state','pp_pickup_city','pp_pickup_state','')">
                        <option value="" id="country">Select Country</option>
                        @foreach($country as $key)
                        <option value='{"id":"{{$key->_id}}","name":"{{$key->Content}}","state_available":"{{$key->state_available}}"}' >{{$key->Content}}</option>
                        @endforeach
                     </select>
	                </div>
                 </div>

                 <div class="inline-form">
                 	<div class="form-group">
	                    <label>State</label><span class="red-star"> *</span>
	                    <div class="clearfix"></div>
	                    <select name="State" class="form-control required left-disabled" id="pp_pickup_state" onchange="get_city('pp_pickup_state','pp_pickup_city','pp_pickup_city','')">
	                        <option value="" >Select State</option>

	                    </select>
	              	</div>
                 </div>

                 <div class="inline-form">
                 	<label>City</label><span class="red-star"> *</span>
                     <div class="clearfix"></div>
                     <select  name="City" class="form-control required" id="pp_pickup_city">
                        <option value="">Select City</option>

                     </select>
                 </div>



                <div class="inline-form">	
	                <div class="form-group">
	                {!! Form::submit('Search', ['class' => 'btn btn-primary']) !!}
	                </div>
                 </div>
                 <div class="inline-form">
	                <div class="form-group">
						<a href="{{URL::to('admin/requester')}}" class="btn btn-primary">
						<span aria-hidden="true" class="glyphicon glyphicon-refresh">
						
						</span>&nbsp;&nbsp;Reset
                    </a>
	                </div>
				</div>


	            <div class="form-group">
		   			<a class="btn-flat btn btn-primary mt-10" href="{{url('admin/requester/export-excel')}}?&search_value={{Input::get('search_value')}}&streat={{Input::get('streat')}}&City={{Input::get('City')}}&State={{Input::get('State')}}&Country={{Input::get('Country')}}"><span class="fa fa-file-excel-o"></span>&nbsp;Export csv</a>
				</div>

			{!! Form::close() !!}
         </div>
		<div class="clearfix"></div>
	      <!-- Pagination Section-->
		<input type="hidden" name="urlvalue" id="urlvalue" value="{{$paginationurl}}" />
		<input type="hidden" name="postvalue" id="postvalue" value="{{$postvalue}}" />
		<input type="hidden" name="orderby" id="orderby" value="{{$orderby}}" />
		<input type="hidden" name="orderType" id="orderType" value="{{$orderType}}" />
		<div id="containerdata"></div>
		<div class="clearfix"></div>
	  	</div> <!-- Panel Body -->
	</div>
</div>

<script>


$(document).ready(function(){
  var country ='<?php echo Input::get('Country') ?>';
  var state ='<?php echo Input::get('State') ?>';
  var json = JSON.parse(country);
  var json2 = JSON.parse(state);
  var country_id = json.id;
  if(json){
    $("#temp-loader").addClass('spinnerin');
    $("#pp_pickup_country").val('<?php echo Input::get('Country') ?>');
    get_state('pp_pickup_country','pp_pickup_state','pp_pickup_city','pp_pickup_state','','','yes');
    setTimeout(select_state,2000);
    setTimeout(fetch_city,3000);
    setTimeout(select_city,4000);
  }
});

function select_state()
{
  $('#pp_pickup_state option').each(function() {

      if($(this).val() == '<?php echo Input::get('State') ?>') {
          $(this).prop("selected", true);
      }
  });
}
function fetch_city()
{
  get_city('pp_pickup_state','pp_pickup_city','pp_pickup_city','')
}

function select_city(){
  $('#pp_pickup_city option').each(function() {
      if($(this).val() == '<?php echo Input::get('City') ?>') {
          $(this).prop("selected", true);
      }
  });
  $("#temp-loader").removeClass('spinnerin');
}

  </script>
@include('Admin::layouts.footer')
{!! Html::script('theme/admin/custome/js/utility.js') !!}
@stop
