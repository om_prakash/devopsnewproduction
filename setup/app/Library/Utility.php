<?php
namespace App\Library;

use App\Http\Models\Configuration;
use App\Http\Models\Sequence;

class Utility
{

    public static function sequence($tbl)
    {
        $seq = 1;
        $sequence = Sequence::find($tbl);
        if (count($sequence) > 0) {
            $seq = $sequence->seq = $sequence->seq + 1;
            $sequence->save();
        } else {
            Sequence::insert(['_id' => $tbl, 'seq' => 1]);
        }
        return $seq;
    }
    public function change_unit($weight, $currentunit, $changeto)
    {

        //  echo $weight." ".$currentunit." ".$changeto; //die;

        //  echo "<br>";

        $newweight = '';
        $newchangeto = $changeto;
        switch ($currentunit) {
            case 'kg':
                $newweight = $weight * 1000;
                break;
            case 'lbs':
                $newweight = $weight * 0.00220462;
                break;
            case 'inches':
                $newweight = $weight;
                break;
            case 'cm':
                $newweight = $weight * 0.393701;
                break;
        }

        switch ($changeto) {
            case 'kg':
                $newweight = $newweight / 1000;
                break;
            case 'lbs':
                $newweight = $newweight * 0.00220462;
                break;
            case 'cm':
                $newweight = $newweight / 0.393701;
                break;
            case 'inches':
                $newweight = $newweight;
                break;
        }

        return $newweight;

    }

    public static function random($length)
    {
        $token = '';
        $array = array_merge(array_merge(range(0, 9), range('A', 'Z')), range('a', 'z'));

        for ($i = 0; $i <= $length; $i++) {
            $token .= $array[rand(0, 61)];
        }

        return $token;
    }
    public static function getDistanceBetweenPointsNew($latitude1, $longitude1, $latitude2, $longitude2, $unit = 'Meter')
    {
        $latitude1 = floatval($latitude1);
        $longitude1 = floatval($longitude1);
        $latitude2 = floatval($latitude2);
        $longitude2 = floatval($longitude2);

        $theta = $longitude1 - $longitude2;
        $distance = (sin(deg2rad($latitude1)) * sin(deg2rad($latitude2))) + (cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * cos(deg2rad($theta)));
        $distance = acos($distance);
        $distance = rad2deg($distance);
        $distance = $distance * 60 * 1.1515;switch ($unit) {
            case 'Mi':break;case 'Km':$distance = $distance * 1.609344;
                break;case 'Meter':$distance = $distance * 1609.34;
                break;
        }
        return (round($distance, 2));
    }
    public static function getdate($dt)
    {
        return date('M  d, Y', strtotime($dt));

    }

    public static function getdate_time($dt)
    {
        return date('M  d, Y H:i A', strtotime($dt));

    }

    public function status($status)
    {
        switch ($status) {
            case 'assign':
                return 'Assign';
                break;
            case 'out_for_pickup':
                return 'Out for Pickup';
                break;
            case 'purchased':
                return 'Purchased';
                break;
            case 'paid':
                return 'Paid';
                break;
            case 'delivered':
                return 'Delivered';
                break;
            case 'cancel':
                return 'Canceled';
                break;
            case 'ready':
                return 'Ready';
                break;
            case 'out_for_delivery':
                return 'Out for Delivery';
                break;
            case 'accepted':
                return 'Accepted';
                break;
            case 'pending':
                return 'Pending';
                break;

            case 'not_purchased':
                return 'Paid';
                break;

            default:
                return 'cancel';
                break;
        }

    }

    public static function getLatLong($address)
    {

        try
        {
            if (!empty($address)) {
                //Formatted address
                $formattedAddr = str_replace(' ', '+', $address);
                //Send request and receive json data by address
                $geocodeFromAddr = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address=' . $formattedAddr . '&sensor=false&key=AIzaSyCTCDs2nzufixfLdPKP2ethJHwebhMoiX0');
                $output = json_decode($geocodeFromAddr);
                //Get latitude and longitute from json data
                if (isset($output->results[0]->geometry->location->lat)) {
                    $data['lat'] = $output->results[0]->geometry->location->lat;
                    $data['lng'] = $output->results[0]->geometry->location->lng;
                }
                //Return latitude and longitude of the given address
                if (isset($data)) {
                    return $data;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (exception $e) {
            return false;
        }
    }
    public static function GetDrivingDistance($lat1, $long1, $lat2, $long2)
    {
        $dist = 0;
        $time = 0;

        $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" . $lat1 . "," . $long1 . "&destinations=" . $lat2 . "," . $long2 . "&mode=driving&language=pl-PL&key=AIzaSyCTCDs2nzufixfLdPKP2ethJHwebhMoiX0";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);

        $response_a = json_decode($response, true);
        if (isset($response_a['rows'][0]['elements'][0]['distance']['value'])) {

            $dist = $response_a['rows'][0]['elements'][0]['distance']['value'];
            $time = $response_a['rows'][0]['elements'][0]['duration']['value'];
        }
        if ($dist <= 0) {
            $dist = Self::getDistanceBetweenPointsNew($lat1, $long1, $lat2, $long2);
            return (Object) array('distance' => $dist, 'time' => $time, 'google_status' => false);
        }
        return (Object) array('distance' => $dist, 'time' => $time, 'google_status' => true);
    }

    public static function formated_address($array, $zip = '')
    {
        $address = '';
        foreach ($array as $ky => $val) {
            if (trim($val) != '') {
                if (trim($address) != '') {$address = "$address, ";}
                $address .= $val;
            }
        }
        if (trim($zip) != '') {$address .= "- $zip";}

        return $address;
    }
    public static function get_processing_fee($itemCost)
    {
        $config = Configuration::find('5673e33e6734c4f874685c84');
        if (count($config) > 0) {
            return ($config->ProcessingFees * $itemCost) / 100;
        } else {
            return ($itemCost * 2) / 100;
        }

    }
	/*
	 * TODO get Volumetric Weight Calculator
	 * http://wap.dhl.com/serv/volweight.html
	 */ 
	 public static function calculate_volumetric_weight($info) {
			if($info['type']=='lbs'){
				$volumetric_weight = ceil(($info['length'] *$info['width']* $info['height']) / 139);
			}else{
				$volumetric_weight = ceil(($info['length'] * $info['width'] * $info['height']) / 5000);
			}
			
		return $volumetric_weight;
	 }

    /*
     * TODO shpping cost from UPS API
     */ 
    public static function calculate_ups($params) {
        // dd($params);
        //~ $rate = new Ups\Rate(
            //~ $accessKey,
            //~ $userId,
            //~ $password
        //~ );
        $rate = new \Ups\Rate(
            "1D736EB369B97195",
            "ship@aquantuo.com",
            "Aqua987!"
        );

        try {
            $shipment = new \Ups\Entity\Shipment();

            $shipperAddress = $shipment->getShipper()->getAddress();
            $shipperAddress->setPostalCode($params["pickupPostalCode"]); // Pickup

            $address = new \Ups\Entity\Address();
            $address->setCity($params["pickupCity"]);
            $address->setStateProvinceCode($params["pickupStateProvinceCode"]);
            $address->setCountryCode($params["pickupCountryCode"]);
            $address->setPostalCode($params["pickupPostalCode"]);    // Pickup
            $shipFrom = new \Ups\Entity\ShipFrom();
            $shipFrom->setAddress($address);

            $shipment->setShipFrom($shipFrom);

            $shipTo = $shipment->getShipTo();
            // $shipTo->setCompanyName('Test Ship To');
            $shipToAddress = $shipTo->getAddress();
            $shipToAddress->setPostalCode($params["destinationPostalCode"]);  // drop or destination.

            $package = new \Ups\Entity\Package();
            $package->getPackagingType()->setCode(\Ups\Entity\PackagingType::PT_PACKAGE);
            $package->getPackageWeight()->setWeight($params["weight"]);

            // if you need this (depends of the shipper country)
            //$weightUnit = new \Ups\Entity\UnitOfMeasurement;
            //$weightUnit->setCode(\Ups\Entity\UnitOfMeasurement::UOM_KGS);
            //$package->getPackageWeight()->setUnitOfMeasurement($weightUnit);

            $dimensions = new \Ups\Entity\Dimensions();
            $dimensions->setHeight($params["height"]);
            $dimensions->setWidth($params["width"]);
            $dimensions->setLength($params["length"]);

            $unit = new \Ups\Entity\UnitOfMeasurement;
            $unit->setCode(\Ups\Entity\UnitOfMeasurement::UOM_IN);

            $dimensions->setUnitOfMeasurement($unit);
            $package->setDimensions($dimensions);

            $shipment->addPackage($package);
            return $rate->getRate($shipment);
            /*echo "<pre>";
            var_dump($rate->getRate($shipment));*/
        } catch (\Ups\Exception\InvalidResponseException $e) {
            echo "code: ".$e->getCode();
            echo "message: ".$e->getMessage();
            die();
            return 0;
            // echo "error";
            // die;
        }
    }

	 public static function address_validation() { 
		 
		$accessKey = "1D736EB369B97195";
		$userId = "ship@aquantuo.com";
		$password = "Aqua987!";
		$address = new \Ups\Entity\Address();
		$address->setAttentionName('Test Test');
		$address->setBuildingName('Test');
		$address->setAddressLine1('Address Line 1');
		$address->setAddressLine2('Address Line 2');
		$address->setAddressLine3('Address Line 3');
		$address->setStateProvinceCode('NY');
		$address->setCity('New York');
		$address->setCountryCode('US');
		$address->setPostalCode('10000');

		$xav = new \Ups\AddressValidation($accessKey, $userId, $password);
		$xav->activateReturnObjectOnValidate(); //This is optional
		try {
			$response = $xav->validate($address, $requestOption = \Ups\AddressValidation::REQUEST_OPTION_ADDRESS_VALIDATION, $maxSuggestion = 15);
		} catch (Exception $e) {
			var_dump($e);
		}
	}
}
