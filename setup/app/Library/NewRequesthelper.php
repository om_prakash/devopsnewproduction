<?php

namespace App\Library;

use App\Http\Models\Category;
use App\Http\Models\CityStateCountry;
use App\Http\Models\Configuration;

class NewRequesthelper
{
    private $category;
    private $setting;
    private $currency;
    private $information;
    private $insurencestatus;
    private $raw;

    public function __construct($array = array())
    {
        $this->information = (Object) array(
            "shippingcost" => 0,
            "costInCurrency" => 0,
            "distance" => 0,
            "insurance" => 0,
            "volume" => 0,
            "weight" => 0,
            "weightUnit" => '',
            "showVolume" => 0,
            "DutyAndCustom"=>0,
            "Tax"=>0,
            "formated_currency" => "",
            "warehouse_transit_fee"=>0,
        );
        $this->raw = (Object) array(
            "needInsurance" => false,
            "productQty" => 0,
            "productCost" => 0,
            "productWidth" => 0,
            "productWidthUnit" => "",
            "productHeight" => 0,
            "productHeightUnit" => "",
            "productLength" => 0,
            "productLengthUnit" => "",
            "productWeight" => 0,
            "productWeightUnit" => "",
            "travelMode" => "",
            "productCategory" => "",
            "productCategoryId" => "",
            "distance" => 0,
            "currency" => 'USD',
            'consolidate_check'=>'off',
        );
        foreach ($array as $key => $val) {
            $this->raw->$key = $val;
        }
        $this->information->weightUnit = $this->raw->productWeightUnit;
        $this->information->widthUnit = $this->raw->productWidthUnit;

        $this->raw->needInsurance = ($this->raw->needInsurance == 'yes') ? true : false;
        $this->get_setting();

    }
    public function get_information()
    {
        if (empty(trim($this->raw->currency))) {
            $this->raw->currency = 'GHS';
        }

        if ($this->raw->currency == 'USD') {
            $this->raw->currency = 'GHS';
        }

        $currency = CityStateCountry::where(['CurrencyCode' => $this->raw->currency])
            ->first();
        $this->information->warehouse_transit_fee = $this->setting->warehouse_transit_fee;

        if (count($currency) > 0) {
            $this->information->costInCurrency = (($this->information->shippingcost + $this->information->insurance) * $currency->CurrencyRate);
            $this->information->formated_currency = str_replace('[AMT]', number_format($this->information->costInCurrency, 2), $currency->FormatedText);
        }

        return $this->information;

    }
    public function calculate()
    {
        if (!$this->get_category_data()) {
            return;
        }

        // print_r($this->raw->productWeightUnit)

        $this->information->weight = $this->get_weight($this->raw->productWeight, $this->raw->productWeightUnit);
        $this->information->showVolume = number_format(($this->raw->productWidth * $this->raw->productHeight * $this->raw->productLength), 2);
        $this->get_volume(array(
            "width" => $this->raw->productWidth,
            "widthunit" => $this->raw->productWidthUnit,
            "height" => $this->raw->productHeight,
            "heightunit" => $this->raw->productHeightUnit,
            "length" => $this->raw->productLength,
            "lengthunit" => $this->raw->productLengthUnit,
        ));
        

        if ($this->get_cost() > 0) {

            $this->get_distance();

            $this->information->shippingcost = $this->get_cost();

            if ($this->information->shippingcost > 0) {
                $this->get_insurance();
                $this->get_tax();
                $this->get_DutyAndCustom();
                
                
            } else {

                $this->information->error = "Package info you entered does not match with our shipping conditions, Please check package info and try again.";
            }
        } else {
            if (!isset($this->information->error)) {
                $this->information->error = "Package info you entered does not match with our shipping conditions, Please check package info and try again.";
            }

        }
    }
    private function get_cost()
    {
        
        $intch = $this->get_size_in_inch($this->raw->productHeight, $this->raw->productHeightUnit) + $this->get_size_in_inch($this->raw->productLength, $this->raw->productLengthUnit) + $this->get_size_in_inch($this->raw->productWidth, $this->raw->productWidthUnit);

        
        

        if (isset($this->category->Shipping)) {
            if (is_array($this->category->Shipping)) {
                $distance = $this->get_distance();

                /*if (in_array(strtolower(trim($this->raw->productCategory)), array('suitcase', 'electronics', 'document', 'other')) && $this->raw->travelMode == 'air') {*/
                if($this->raw->travelMode == 'air'){
                    if ($this->information->weight > 500) {
                        $this->information->error = "The weight you entered is outside the limits allowed by the airlines. If possible, split your package into smaller packages or choose to ship your item by sea.";
                        return;
                    } else if ($intch > 620) {

                        $this->information->error = "The dimensions you entered are outside the limits allowed by the airlines. If possible, split your package into smaller packages or choose to ship your item by sea.";
                        return; // 50 * (int)$this->raw->productQty;
                    }

                }
		
                
				$last_price = 0;// is used for onlt other categry in air mode
				$is_macth = false;
				$weight_increase_by = $this->category->weight_increase_by;
				if(isset($this->category->weightArray)){
					foreach ($this->category->weightArray as $key3) {
						if ($key3['MinWeight'] < floatval($this->information->weight)) {
							$weight_increase_by = $key3['Rate'];
						}
					}
				}

                foreach ($this->category->Shipping as $key) {
					
                    if ($this->category->ChargeType == 'distance') {
						
                        if ($distance >= $key['MinDistance'] && $distance <= $key['MaxDistance']) {
                            if ($this->raw->consolidate_check == 'on') {
                                return $key['CONSOLIDATE_RATE'] * (int) $this->raw->productQty;
                            } else {
                                return $key['Rate'] * (int) $this->raw->productQty;
                            }
                        }
                    }elseif ($this->category->ChargeType == 'fixed' && ($this->raw->productCategoryId == OtherCategory || $this->raw->productCategoryId == ElectronicsCategory) ) {

                        if ($this->information->weight >= $key['MinDistance'] && $this->information->weight <= $key['MaxDistance']) {
							$is_macth = true;
                            $rate = $key['Rate'];
                            
                            if($this->raw->productCost  > 1){
                                $p = $this->raw->productCost - 1;
                                $rate = $rate + ($p * $this->category->price_increase_by);
                            }
                           
                            return $rate * (int) $this->raw->productQty;
                        }
                        $last_price = $key['Rate'];

                       
                    }else if ($this->category->ChargeType == 'fixed') {
						
                        if ($this->information->weight >= $key['MinDistance'] && $this->information->weight <= $key['MaxDistance']) {
                            
                            if($this->raw->consolidate_check == 'on'){
                                return $key['CONSOLIDATE_RATE'] * (int) $this->raw->productQty;
                            }else{
                                return $key['Rate'] * (int) $this->raw->productQty;
                            }
                        }
                    }
                }
                
                // calculate new price for other in air mode
                if(!$is_macth && ($this->raw->productCategoryId == OtherCategory || $this->raw->productCategoryId == ElectronicsCategory)){
					
					$itemWeight_1 = $this->information->weight-2;
					$itemWeight_2 = intval($itemWeight_1/2);
					
					$rate_1 =  $itemWeight_2 * $weight_increase_by;
					$rate_2 = $rate_1 + $last_price;
					$rate = $rate_2;
					if($this->raw->productCost  > 1){
						$p = $this->raw->productCost - 1;
						$rate = $rate + ($p * $this->category->price_increase_by);
                    }
										
					return $rate * (int) $this->raw->productQty;
					
				}
                
            }
        }
    }

    private function get_category_data()
    {
        $data = Category::where(array('_id' => $this->raw->productCategoryId, 'TravelMode' => $this->raw->travelMode, 'Status' => 'Active'))->first();

        //echo $this->raw->productCategoryId; echo '----';

        if (count($data) > 0) {
            $this->category = $data;
            return 1;
        } else {
            $this->information->error = "The category you have selected is not found.";
            return;
        }}
    private function get_setting()
    {

        $data = Configuration::where(array('_id' => '5673e33e6734c4f874685c84'))
            ->first();
        if (count($data) > 0) {
            $this->setting = $data;
        }
        return 1;
    }
    private function get_distance()
    {
        return $this->information->distance = floatval($this->raw->distance) * 0.000621371;
    }

    private function get_weight($weight, $type)
    {
        $type = strtolower($type);
        if ($type == 'kg') {
            return (((float) $weight) * 2.20462);
        } else if ($type == 'gram') {
            return (((float) $weight) * 0.00220462);
        } else {
            return $weight;
        }
    }
    private function get_volume($array)
    {
        $this->information->volume = ($this->get_size_in_feet($array['length'], $array['lengthunit']) * $this->get_size_in_feet($array['width'], $array['widthunit']) * $this->get_size_in_feet($array['height'], $array['heightunit']));
    }
    public function get_size_in_feet($height, $unit)
    {
        $InFeet = (float) $height;
        $unit = strtolower($unit);
        if ($unit == 'inches') {
            $InFeet = $InFeet * 0.0833333;
        } else if ($unit == 'meter') {
            $InFeet = $InFeet * 3.28084;
        } else if ($unit == 'cm') {
            $InFeet = $InFeet * 0.0328084;
        }
        return $InFeet;
    }
    private function get_size_in_inch($length, $unit)
    {
        $unit = strtolower($unit);
        switch ($unit) {
            case 'inches':
                return floatval($length);
                break;
            case 'cm':
                return floatval($length) * 0.393701;
                break;
        }
    }
    public function get_insurance()
    {

        if ($this->raw->needInsurance) {

            $totalPrice = $this->raw->productCost * (int) $this->raw->productQty;
            $insurance =  $totalPrice * $this->setting->main_insurance;
            $this->information->insurance =  $insurance / 100;


            if ($this->information->insurance < 0) {
                $this->information->error = "Sorry! We are not able to provide insurence.";
            }
        }
    }

    public function get_tax(){
		
        $totalPrice = $this->raw->productCost * (int) $this->raw->productQty;
        $Tax = $totalPrice * $this->setting->tax;
        $this->information->Tax =  $Tax / 100;
    }

    public function get_DutyAndCustom(){
        $totalPrice = $this->raw->productCost * (int) $this->raw->productQty;
        //$DutyAndCustom = $totalPrice * $this->setting->duty_customs; // main custome duty
        if(isset($this->category->custom_duty)){
			$DutyAndCustom =  $totalPrice * $this->category->custom_duty; //categoty wise insurance
			$this->information->DutyAndCustom =  $DutyAndCustom / 100;
			
		}
    }
    
}
