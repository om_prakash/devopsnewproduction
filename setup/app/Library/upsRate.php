<?php

namespace App\Library;



class upsRate
{

    private $AccessLicenseNumber;
	private $UserId;
	private $Password;
	private $shipperNumber;
	private $credentials;

    public function __construct($array = array())
    {
        $this->AccessLicenseNumber = "5D753DBD0B3F6592"; //;"1D736EB369B97195";
		$this->UserID = "ship@aquantuo.com";
		$this->Password = "Aqua987!";
		$this->shipperNumber = "6AF664";
		$this->credentials = 0;
		
    }
    public function setCredentials()
    {
       $this->credentials = 1;

    }
    // Define the function getRate() - no parameters
	public function getRate($params) {
		
		
		if ($this->credentials != 1) {
			return 0;
		}
		$PostalCode =  $params["pickupPostalCode"];
		$dest_zip = $params["destinationPostalCode"];
		$service =  $params["service"];
		$length =  $params["length"];
		$width =  $params["width"];
		$height =  $params["height"];
		$weight =  $params["weight"];
		
		$data ="<?xml version=\"1.0\"?>
		<AccessRequest xml:lang=\"en-US\">
		<AccessLicenseNumber>$this->AccessLicenseNumber</AccessLicenseNumber>
		<UserId>$this->UserID</UserId>
		<Password>$this->Password</Password>
		</AccessRequest>
		<?xml version=\"1.0\"?>
		<RatingServiceSelectionRequest xml:lang=\"en-US\">
		<Request>
		<TransactionReference>
		<CustomerContext>Bare Bones Rate Request</CustomerContext>
		<XpciVersion>1.0001</XpciVersion>
		</TransactionReference>
		<RequestAction>Rate</RequestAction>
		<RequestOption>Rate</RequestOption>
		</Request>
		<PickupType>
		<Code>03</Code>
		</PickupType>
		<Shipment>
		<Shipper>
		<Address>
		<PostalCode>$PostalCode</PostalCode>
		<CountryCode>US</CountryCode>
		</Address>
		<ShipperNumber>$this->shipperNumber</ShipperNumber>
		</Shipper>
		<ShipTo>
		<Address>
		<PostalCode>$dest_zip</PostalCode>
		<CountryCode>US</CountryCode>
		<ResidentialAddressIndicator/>
		</Address>
		</ShipTo>
		<ShipFrom>
		<Address>
		<PostalCode>$PostalCode</PostalCode>
		<CountryCode>US</CountryCode>
		</Address>
		</ShipFrom>
		<Service>
		<Code>$service</Code>
		</Service>
		<Package>
		<PackagingType>
		<Code>02</Code>
		</PackagingType>
		<Dimensions>
		<UnitOfMeasurement>
		<Code>IN</Code>
		</UnitOfMeasurement>
		<Length>$length</Length>
		<Width>$width</Width>
		<Height>$height</Height>
		</Dimensions>
		<PackageWeight>
		<UnitOfMeasurement>
		<Code>LBS</Code>
		</UnitOfMeasurement>
		<Weight>$weight</Weight>
		</PackageWeight>
		</Package>
		</Shipment>
		</RatingServiceSelectionRequest>";
		$ch = curl_init("https://www.ups.com/ups.app/xml/Rate");
		curl_setopt($ch, CURLOPT_HEADER, 1);
		curl_setopt($ch,CURLOPT_POST,1);
		curl_setopt($ch,CURLOPT_TIMEOUT, 60);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch,CURLOPT_POSTFIELDS,$data);
		$result=curl_exec ($ch);
		//  echo '<!-- '. $result. ' -->'; // THIS LINE IS FOR DEBUG PURPOSES ONLY-IT WILL SHOW IN HTML COMMENTS
		$data = strstr($result, '<?');
		$xml_parser = xml_parser_create();
		xml_parse_into_struct($xml_parser, $data, $vals, $index);
		xml_parser_free($xml_parser);
		$params = array();
		$level = array();
		foreach ($vals as $xml_elem) {
			if ($xml_elem['type'] == 'open') {
				if (array_key_exists('attributes',$xml_elem)) {
					list($level[$xml_elem['level']],$extra) = array_values($xml_elem['attributes']);
					} else {
					$level[$xml_elem['level']] = $xml_elem['tag'];
				}
			}
			if ($xml_elem['type'] == 'complete') {
				$start_level = 1;
				$php_stmt = '$params';
				while($start_level < $xml_elem['level']) {
					$php_stmt .= '[$level['.$start_level.']]';
					$start_level++;
				}
				if(isset($xml_elem['value'])){
					$php_stmt .= '[$xml_elem[\'tag\']] = $xml_elem[\'value\'];';
					eval($php_stmt);
				}
			}
		}
		curl_close($ch);
		if(isset($params['RATINGSERVICESELECTIONRESPONSE']['RATEDSHIPMENT']['TOTALCHARGES']['MONETARYVALUE'])){
			$lastPrice =  $params['RATINGSERVICESELECTIONRESPONSE']['RATEDSHIPMENT']['TOTALCHARGES']['MONETARYVALUE'];
			return $lastPrice;
		}else{
			/*
			 * According to client if weight more then 160 lbs price will be fixed $300
			 *
			*/
			
			if ($weight >= 160) {
				return 300;
			}
			
			return 0;
		}
	}
	 // Define the function getRate() - no parameters
	public function getRateNew($params) {
		
		$wsdl ='/var/www/html/setup/app/Library/FreightShip.wsdl';
		 $mode = array
		(
         'soap_version' => 'SOAP_1_1',  // use soap 1.1 client
         'trace' => 1
		);

    // initialize soap client
  	$client = new \SoapClient($wsdl , $mode);
  	$endpointurl = 'https://onlinetools.ups.com/webservices/FreightRate';
  	
  	//set endpoint url
  	$client->__setLocation($endpointurl);
	$operation = "ProcessShipment";

    //create soap header
    $usernameToken['Username'] = $this->UserID;
    $usernameToken['Password'] = $this->Password;
    $serviceAccessLicense['AccessLicenseNumber'] = $this->AccessLicenseNumber;
    $upss['UsernameToken'] = $usernameToken;
    $upss['ServiceAccessToken'] = $serviceAccessLicense;

    $header = new \SoapHeader('http://www.ups.com/XMLSchema/XOLTWS/UPSS/v1.0','UPSSecurity',$upss);
    $client->__setSoapHeaders($header);
    
    //get response
  	@$resp = $client->__soapCall($operation ,array($this->processShipment()));
    
    
  	echo "<pre>";
  	print_r($resp);die;
  	
		if ($this->credentials != 1) {
			return 0;
		}
		$PostalCode =  $params["pickupPostalCode"];
		$dest_zip = $params["destinationPostalCode"];
		$service =  $params["service"];
		$length =  $params["length"];
		$width =  $params["width"];
		$height =  $params["height"];
		$weight =  $params["weight"];
		
		$data ="<?xml version=\"1.0\"?>
		<AccessRequest xml:lang=\"en-US\">
		<AccessLicenseNumber>$this->AccessLicenseNumber</AccessLicenseNumber>
		<UserId>$this->UserID</UserId>
		<Password>$this->Password</Password>
		</AccessRequest>
		<?xml version=\"1.0\"?>
		<RatingServiceSelectionRequest xml:lang=\"en-US\">
		<Request>
		<TransactionReference>
		<CustomerContext>Bare Bones Rate Request</CustomerContext>
		<XpciVersion>1.0001</XpciVersion>
		</TransactionReference>
		<RequestAction>Rate</RequestAction>
		<RequestOption>Rate</RequestOption>
		</Request>
		<PickupType>
		<Code>03</Code>
		</PickupType>
		<Shipment>
		<Shipper>
		<Address>
		<PostalCode>$PostalCode</PostalCode>
		<CountryCode>US</CountryCode>
		</Address>
		<ShipperNumber>$this->shipperNumber</ShipperNumber>
		</Shipper>
		<ShipTo>
		<Address>
		<PostalCode>$dest_zip</PostalCode>
		<CountryCode>US</CountryCode>
		<ResidentialAddressIndicator/>
		</Address>
		</ShipTo>
		<ShipFrom>
		<Address>
		<PostalCode>$PostalCode</PostalCode>
		<CountryCode>US</CountryCode>
		</Address>
		</ShipFrom>
		<Service>
		<Code>$service</Code>
		</Service>
		<Package>
		<PackagingType>
		<Code>02</Code>
		</PackagingType>
		<Dimensions>
		<UnitOfMeasurement>
		<Code>IN</Code>
		</UnitOfMeasurement>
		<Length>$length</Length>
		<Width>$width</Width>
		<Height>$height</Height>
		</Dimensions>
		<PackageWeight>
		<UnitOfMeasurement>
		<Code>LBS</Code>
		</UnitOfMeasurement>
		<Weight>$weight</Weight>
		</PackageWeight>
		</Package>
		</Shipment>
		</RatingServiceSelectionRequest>";
	//	$ch = curl_init("https://www.ups.com/ups.app/xml/Rate"); // below used latest link
		$ch = curl_init("https://onlinetools.ups.com/ups.app/xml/Rate");
	
		curl_setopt($ch, CURLOPT_HEADER, 1);
		curl_setopt($ch,CURLOPT_POST,1);
		curl_setopt($ch,CURLOPT_TIMEOUT, 60);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch,CURLOPT_POSTFIELDS,$data);
		$result=curl_exec ($ch);
		//  echo '<!-- '. $result. ' -->'; // THIS LINE IS FOR DEBUG PURPOSES ONLY-IT WILL SHOW IN HTML COMMENTS
		$data = strstr($result, '<?');
		$xml_parser = xml_parser_create();
		xml_parse_into_struct($xml_parser, $data, $vals, $index);
		xml_parser_free($xml_parser);
		$params = array();
		$level = array();
		foreach ($vals as $xml_elem) {
			if ($xml_elem['type'] == 'open') {
				if (array_key_exists('attributes',$xml_elem)) {
					list($level[$xml_elem['level']],$extra) = array_values($xml_elem['attributes']);
					} else {
					$level[$xml_elem['level']] = $xml_elem['tag'];
				}
			}
			if ($xml_elem['type'] == 'complete') {
				$start_level = 1;
				$php_stmt = '$params';
				while($start_level < $xml_elem['level']) {
					$php_stmt .= '[$level['.$start_level.']]';
					$start_level++;
				}
				if(isset($xml_elem['value'])){
					$php_stmt .= '[$xml_elem[\'tag\']] = $xml_elem[\'value\'];';
					eval($php_stmt);
				}
			}
		}
		curl_close($ch);
		if(isset($params['RATINGSERVICESELECTIONRESPONSE']['RATEDSHIPMENT']['TOTALCHARGES']['MONETARYVALUE'])){
			$lastPrice =  $params['RATINGSERVICESELECTIONRESPONSE']['RATEDSHIPMENT']['TOTALCHARGES']['MONETARYVALUE'];
			return $lastPrice;
		}else{
			/*echo "error";
			print_r($params);die;*/
			return 0;
		}
	}
	public function processShipment(){
		
		
		//create soap request
      $request['Request'] = array
      (
         'RequestOption' => array ('1','Shipping')
      );
      $shipfrom['Name'] = 'Pat Stewart';
      $shipfrom['TaxIdentification'] = '1234567890';
      $shipfrom['Address'] = array
      (
          'AddressLine' => '2311 York Road',
          'City' => 'Timonium',
          'StateProvinceCode' => 'MD',
          'PostalCode' => '21093',
          'CountryCode' => 'US'
      );
      $shipfrom['AttentionName'] = 'String';
      $shipfrom['Phone'] = array
      (
          'Number' => '6785851000',
          'Extension' => '123'
      );
      $shipment['ShipFrom'] = $shipfrom;
      $shipment['ShipperNumber'] = 'Your Shipper Number';

      $shipto['Name'] = 'Superman';
      $shipto['Address'] = array
      (
           'AddressLine' => '2010 Warsaw Road',
           'StateProvinceCode' => 'ON',
           'PostalCode' => 'M4C2M6',
           'CountryCode' => 'CA',

           'City' => 'Roswell',
           'StateProvinceCode' => 'GA',
           'PostalCode' => '30076',
           'CountryCode' => 'US'
       );
       $shipto['AttentionName'] = 'String';
       $shipto['Phone'] = array
       (
           'Number' => '6785851000',
           'Extention' => '111'
       );
       $shipment['ShipTo'] = $shipto;

       $payer['Name'] = 'Superman';
       $payer['Address'] = array
       (
           'AddressLine' => '2010 Warsaw Road',
           'City' => 'Roswell',
           'StateProvinceCode' => 'GA',
           'PostalCode' => '30075',
           'CountryCode' => 'US'
       );
       $payer['ShipperNumber'] = 'Your Shipper Number';
       $payer['AttentionName'] = 'String';
       $payer['Phone'] = array
       (
           'Number' => '6785851000'
       );
       $paymentinformation['Payer'] = $payer;
       $paymentinformation['ShipmentBillingOption'] = array
       (
           'Code' => '10',
           'Description' => 'String'
       );
       $shipment['PaymentInformation'] = $paymentinformation;
       $shipment['Service'] = array
       (
           'Code' => '308',
           'Description' => 'String'
       );

       $shipment['HandlingUnitOne'] = array
       (
           'Quantity' => '16',
           'Type' => array
           (
               'Code' => 'PLT',
               'Description' => 'String'
           )
       );
       $shipment['Commodity'] = array
       (
           'CommodityID' => '22',
           'Description' => 'BUGS',
           'Weight' => array
           (
               'UnitOfMeasurement' => array
               (
                    'Code' => 'LBS',
                    'Description' => 'String'
               ),
               'Value' => '511.25'
           ),
           'Dimensions' => array
           (
               'UnitOfMeasurement' => array
               (
                    'Code' => 'IN',
                    'Description' => 'String'
               ),
               'Length' => '1.25',
               'Width' => '1.2',
               'Height' => '5'
           ),
           'NumberOfPieces' => '1',
           'PackagingType' => array
           (
               'Code' => 'PLT',
               'Description' => 'String'
           ),
           'CommodityValue' => array
           (
               'CurrencyCode' => 'USD',
               'MonetaryValue' => '265.2'
           ),
           'FreightClass' => '60',
           'NMFCCommodityCode' => '566'
       );

       $shipment['Reference'] = array
       (
           'Number' => array
           (
                'Code' => 'PM',
                'Value' => '1651651616'
           ),

           'BarCodeIndicator' => array
           (
                'NumberOfCartons' => '5',
                'Weight' => array
                (
                    'UnitOfMeasurement' => array
                    (
                        'Code' => 'LBS',
                        'Description' => 'String'
                    ),
                    'Value' => '2'
                )
           )
       );
      $request['Shipment'] = $shipment;

      echo "Request.......\n";
      print_r($request);
      echo "\n\n";
      return $request;
	}
}
