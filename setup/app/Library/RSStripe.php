<?php namespace App\Library;

use App;
use Cartalyst\Stripe\Exception\BadRequestException;
use Cartalyst\Stripe\Exception\CardErrorException;

//Libraries

use Cartalyst\Stripe\Exception\InvalidRequestError;
use Cartalyst\Stripe\Exception\InvalidRequestException;
use Cartalyst\Stripe\Exception\MissingParameterException;
use Cartalyst\Stripe\Exception\NotFoundException;
use Cartalyst\Stripe\Exception\ServerErrorException;
use Cartalyst\Stripe\Exception\StripeException;
use Cartalyst\Stripe\Exception\UnauthorizedException;
use Stripe;

class RSStripe
{

    public static function capture($stripeid, $amount, $cardid, $capture = true)
    {
        return RSStripe::run('capture', ['stripeid' => $stripeid, 'amount' => $amount, 'cardid' => $cardid, 'capture' => $capture]);
    }
    public static function transfer($amount, $accountid)
    {

        return RSStripe::run('transfer', ['amount' => $amount, 'accountid' => $accountid]);

    }
    public static function refund($stripeid, $amount = 0)
    {
        return RSStripe::run('refund', ['stripeid' => $stripeid, 'amount' => $amount]);
    }
    public static function charge_detail($stripeid)
    {
        return RSStripe::run('capture_detail', ['stripeid' => $stripeid]);
    }
    public static function retrive_amount($captureid, $amount = '')
    {

        return RSStripe::run('retrive_amount', ['captureid' => $captureid, 'amount' => $amount]);

    }
    public static function create_account($options, $country, $bankid = '')
    {
        return RSStripe::run('account', ['country' => $country, 'bankid' => $bankid, 'options' => $options]);
    }

    public static function run($action, $options)
    {
        $options = (Object) $options;
        $stripe = Stripe::make();
        try
        {
            switch ($action) {
                case 'capture':

                    return $stripe->charges()->create([
                        'customer' => $options->stripeid,
                        'card' => $options->cardid,
                        'currency' => 'USD',
                        'amount' => $options->amount,
                        'capture' => isset($options->capture) ? $options->capture : false,
                    ]);

                    break;
                case 'capture_detail':
                    return $stripe->charges()->find($options->stripeid);
                    break;
                case 'retrive_amount':
                    return $stripe->charges()->capture($options->captureid, $options->amount);
                    break;
                case 'transfer':
                    return $stripe->transfers()->create([
                        'amount' => $options->amount,
                        'currency' => 'USD',
                        'destination' => $options->accountid,
                    ]);
                    break;
                case 'refund':
                    return $stripe->refunds()->create($options->stripeid, $options->amount);
                    break;

                case 'account':
                    if (empty(trim($options->bankid))) {
                        $options->options["managed"] = true;
                        $options->options["country"] = $options->country;
                        $options->options["tos_acceptance"] = [
                            "date" => time(),
                            "ip" => @$_SERVER['REMOTE_ADDR'],
                        ];
                        return $stripe->account()->create($options->options);
                    } else {
                        return $stripe->account()->update(trim($options->bankid), $options->options);
                    }
                    break;
                default:
                    return 'Invalid parameter';
                    break;
            }
        } catch (BadRequestException $e) {
            return $e->getMessage();
        } catch (UnauthorizedException $e) {
            return $e->getMessage();
        } catch (InvalidRequestException $e) {
            return $e->getMessage();
        } catch (NotFoundException $e) {
            return $e->getMessage();
        } catch (ServerErrorException $e) {
            return $e->getMessage();
        } catch (InvalidRequestError $e) {
            return $e->getMessage();
        } catch (MissingParameterException $e) {
            return $e->getMessage();
        } catch (CardErrorException $e) {
            return $e->getMessage();
        } catch (StripeException $e) {
            return $e->getMessage();
        }
    }

    public static function find_charge($stripeid)
    {

        $stripe = Stripe::make();
        try
        {
            if (!empty(trim($stripeid))) {
                return $stripe->charges()->find($stripeid);
            } else {
                return 'Stripe id not found';
            }
        } catch (BadRequestException $e) {
            return $e->getMessage();
        } catch (UnauthorizedException $e) {
            return $e->getMessage();
        } catch (InvalidRequestException $e) {
            return $e->getMessage();
        } catch (NotFoundException $e) {
            return $e->getMessage();
        } catch (ServerErrorException $e) {
            return $e->getMessage();
        } catch (InvalidRequestError $e) {
            return $e->getMessage();
        } catch (MissingParameterException $e) {
            return $e->getMessage();
        } catch (CardErrorException $e) {
            return $e->getMessage();
        }
    }
}
