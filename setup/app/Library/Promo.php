<?php
namespace App\Library;

use App\Http\Models\CityStateCountry;
use App\Http\Models\Promocode;
use App\Http\Models\Deliveryrequest;
use MongoDate;

Class Promo
{
 
    
    public static function get_validate($PromoCode,$Shipping,$TotalAmount,$userid,$default_currency = 'USD')
    {
        $response = array("success" => 0, "msg"=> 'The promo code you have entered is not valid.');
        

        $discountdata = Promocode::where(['Code'=>strtoupper($PromoCode),'Status'=>'active'])
                                    ->where('ValidFrom', '<=', new MongoDate())->first();

        if(count($discountdata) > 0)
        {

            if($discountdata->ValidTill > new MongoDate())
            {
                if($Shipping >= $discountdata->MinimumOrderPrice)
                {
                    if($discountdata->UsesCount >= $discountdata->MaxUses) {
                        $response['msg'] = 'This promo code has expired.';
                    }else{

                        $usescount = Deliveryrequest::where(['RequesterId'=>$userid,'PromoCode'=>strtoupper($PromoCode)])->first();
                        
                        if(count($usescount) >=  $discountdata->MaxUsesPerPerson)
                        {
                            $response['msg'] = 'You have already used this promo code.';
                        }else{
                            if($discountdata->DiscountType == 'percent') {
                                $discount = ($Shipping * $discountdata->DiscountAmount) / 100;
                            } else {
                                $discount = $discountdata->DiscountAmount;
                            }

                            if($discount > $discountdata->MaximumDiscount) {
                                $discount = $discountdata->MaximumDiscount;
                            }
                            if($discount > $Shipping) {
                                $response['msg'] = 'The promo code you have entered is not valid for this delivery request.';
                            } else{

                                $ghanacurrency = 0;
                                $FormatedText = '';
                                $currencydata = CityStateCountry::where(['CurrencyCode'=> $default_currency])
                                                            ->first();
                                if(count($currencydata) > 0) {


                                    $ghanacurrency = $currencydata->CurrencyRate;
                                    $FormatedText = $currencydata->FormatedText;
                                }

                                $GhanaTotalCost = number_format((($TotalAmount-$discount) * $ghanacurrency),2);
                                $discount = number_format($discount, 2);
                                $response = array('success'=> 1,'discount' => $discount);
                                $response['msg'] = "Success! You get a discount of $".$discount;
                                $response['GhanaTotalCost'] = str_replace('[AMT]',$GhanaTotalCost,$FormatedText);
                                $response['TotalCost_Ghana'] = $GhanaTotalCost;
                            }   
                        }
                    }           
                } else {
                    $response['msg'] = "Oops! Minimum order amount should be greater than $".$discountdata->MinimumOrderPrice.".";
                }
            } else {
                $response['msg'] = 'This promo code has expired.';
            }   
        }
        return $response;
    }
    public static function mark_promocde_as_use($code)
    {
        if(!empty(trim($code))) {
            Promocode::where('Code',strtoupper($code))->increment('UsesCount',1);
        }
    }
}
