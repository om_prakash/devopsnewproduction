<?php
namespace App\Library;

use App\Http\Models\Setting;
use App\Http\Models\User;
use MongoDate;

class UserReg
{
    public function __construct($input)
    {
        $this->get_field();
        if (is_array($input)) {
            foreach ($input as $key => $val) {
                $this->insert[$key] = $val;
            }
        }

    }
    public function _reg()
    {

        $user = User::where(['Email' => strtolower(trim($this->insert['Email']))])
            ->first();
        if (count($user) > 0) {
            return ['success' => 0, 'msg' => "The email already been taken."];
        }

        $this->insert['Name'] = ucfirst($this->insert['FirstName']) . ' ' . ucfirst($this->insert['LastName']);

        $UniqueNo = strtoupper($this->insert['FirstName'][0] . $this->insert['LastName'][0]);
        $this->insert['UniqueNo'] = $UniqueNo . Utility::sequence('user_unique');
        // Get aquantuo addres
        $supportemail = Setting::find('563b0e31e4b03271a097e1ca');

        if (count($supportemail) > 0) {
            $this->insert['AqAddress'] = $supportemail->AqAddress;
            $this->insert['AqCity'] = $supportemail->AqCity;
            $this->insert['AqState'] = $supportemail->AqState;
            $this->insert['AqZipcode'] = $supportemail->AqZipcode;
            $this->insert['AqCountry'] = $supportemail->AqCountry;
            $this->insert['AqLatLong'] = $supportemail->AqLatlong;
        }

        $this->insert['_id'] = (string) User::insertGetId($this->insert);

        if ($this->insert['_id']) {

            // email to user
            send_mail('58e4f6f77ac6f6e92d8b4567', [
                'to' => $this->insert['Email'],
                'replace' => [
                    '[USERNAME]' => $this->insert['Name'],
                    '[PASSWORD]' => $this->insert['temprary'],
                ],
            ]);

            // email to admin
            if (count($supportemail) > 0) {
                send_mail('565fd3e3e4b076e7a2a176f5', [
                    'to' => $supportemail->RegEmail,
                    'replace' => [
                        '[USERNAME]' => $this->insert['Name'],
                        '[EMAIL]' => $this->insert['Email'],
                        '[USERTYPE]' => (($this->insert['UserType'] == 'both') ? "Transporter({Input::get('transportertype')})" : 'Requester'),

                    ],
                ]);
            }

            return ['success' => 1, 'result' => $this->insert];

        }

    }
    public function get_field()
    {
        $this->insert = [
            'FirstName' => '',
            'LastName' => '',
            'Name' => '',
            'UniqueNo' => '',
            'Email' => '',
            'Password' => '',
            'TransporterType' => '',
            'UserType' => 'requester',
            'ChatName' => '',
            'CountryCode' => "",
            'PhoneNo' => '',
            'AlternateCCode' => '',
            'AlternatePhoneNo' => '',
            'RequesterStatus' => 'active',
            'Age' => '',
            'SSN' => '',
            'Street1' => '',
            'Street2' => '',
            'Country' => '',
            'delete_status' => 'no',
            'State' => '',
            'City' => '',
            'ZipCode' => '',
            'BankName' => '',
            'AccountHolderName' => '',
            'BankAccountNo' => '',
            'RoutingNo' => '',
            'StripeId' => '',

            'VatTaxNo' => '',
            'TransporterStatus' => 'not_registred',
            'UserStatus' => 'active',
            'Image' => '',
            'IDProof' => '',

            'LicenceId' => '',
            'type_of_id' => '',
            'TPTrackLocation' => 'on',
            'EmailStatus' => 'on',
            'consolidate_item' => 'on',
            'NoficationStatus' => "on",
            'TPSetting' => "on",
            'SoundStatus' => "on",
            'VibrationStatus' => "on",
            'EnterOn' => new MongoDate(),
            'UpdateOn' => new MongoDate(),
            'RatingCount' => 0,
            'RatingByCount' => 0,
            'CurrentLocation' => array(),
            'DeliveryAreaCountry' => array(),
            'DeliveryAreaState' => array(),
            'DeliveryAreaCities' => array(),
            'ProfileStatus' => 'step-one',
            'StripeBankId' => "",
            'NotificationId' => '',
            'DeviceId' => '',
            'DeviceType' => 'web',
            'bank_info' => [],
            'AqAddress' => '',
            'AqCity' => '',
            'AqState' => '',
            'AqCountry' => '',
            'AqZipcode' => '',
            'AqLatLong' => [],
            'Default_Currency' => 'GHS',
        ];

    }
}
