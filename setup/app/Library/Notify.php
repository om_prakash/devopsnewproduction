<?php
namespace App\Library;

use ApnsPHP_Abstract;
use ApnsPHP_Message;
use ApnsPHP_Message_Exception;
use ApnsPHP_Push;

class Notify
{
    private $iphoneBundal;
    private $iphoneNotificationId;
    private $androidBundal;
    private $androidNotificationId;

    public function __construct()
    {
        $this->iphoneNotificationId = array();
        $this->androidNotificationId = array();
        $this->webNotificationId = array();
        $this->iphoneBundal = array('title' => '', 'badge' => 0, 'locationkey' => '', 'location' => 'no_action', 'TransporterImage' => '', 'TransporterName' => '', 'message' => '','request_version'=> 'new','type' => '','itemId' => '');
        $this->androidBundal = array('title' => '', 'badge' => 0, 'locationkey' => '', 'location' => 'no_action', 'TransporterImage' => '', 'TransporterName' => '', 'message' => '','request_version'=> 'new','type' => '','itemId' => '');
        $this->webBundal = array('title' => '', 'badge' => 0, 'locationkey' => '', 'location' => 'no_action', 'TransporterImage' => '', 'TransporterName' => '', 'message' => '', 'type' => '', 'chat-data' => '', 'itemId' => '','request_version'=> 'new','itemId' => '');

        $this->load_iphone();
    }
    /*
     * Add notification id that we want ot send notification
     *
     */

    public function add_user($userNotificationid = '', $type = '')
    {

        if (!empty($userNotificationid) && $userNotificationid != null) {
            if ($type == 'android') {
                array_push($this->androidNotificationId, $userNotificationid);
            } else if ($type == 'iphone') {
                array_push($this->iphoneNotificationId, $userNotificationid);
            } else if ($type == 'web') {
                array_push($this->webNotificationId, $userNotificationid);
            }

        }
    }
    /*
     * Add a custom field to bundal
     *
     */
    public function setCustomField($field, $value, $type = 'both')
    {
        if ($type == 'both') {
            $this->iphoneBundal[$field] = $value;
            $this->androidBundal[$field] = $value;
        } else if ($type == 'iphone') {
            $this->iphoneBundal[$field] = $value;
        } else if ($type == 'android') {
            $this->androidBundal[$field] = $value;
        } else if ($type == 'web') {
            $this->webNotificationId[$field] = $value;
        }

    }
    /*
     * Set value
     *
     */
    public function setValue($field, $value, $type = 'both')
    {
        if ($type == 'both') {
            $this->iphoneBundal[$field] = $value;
            $this->androidBundal[$field] = $value;
            $this->webBundal[$field] = $value;
        } else if ($type == 'iphone') {
            $this->iphoneBundal[$field] = $value;
        } else if ($type == 'android') {
            $this->androidBundal[$field] = $value;
        } else if ($type == 'web') {
            $this->webBundal[$field] = $value;
        }
    }
    /*
     * Send Notification to both android and iphone
     */
    public function fire()
    {
        try {
            $this->fire_android();
            $this->fire_iphone();
            $this->fire_web();
        } catch (ApnsPHP_Message_Exception $e) {}

    }
    /*
     * Send Notification to iphone
     */
    private function fire_iphone()
    {
        if (count($this->iphoneNotificationId) < 1) {
            return;
        }

        //if(!$token || !$msg) return;

        // Instantiate a new ApnsPHP_Push object
        $push = new ApnsPHP_Push(
            ApnsPHP_Abstract::ENVIRONMENT_SANDBOX, // ENVIRONMENT_PRODUCTION   ENVIRONMENT_SANDBOX
            //$this->apnsDir . 'SSL/Aquantuo_Production.pem'
            $this->apnsDir . 'SSL/Auantuo_Dev_10_12_2019.pem'
            
        );

        // Set the Provider Certificate passphrase
        // $push->setProviderCertificatePassphrase('tablecan29');

        // Set the Root Certificate Autority to verify the Apple remote peer
        $push->setRootCertificationAuthority($this->apnsDir . 'SSL/entrust_root_certification_authority.pem');

        // Connect to the Apple Push Notification Service
        $push->connect();

        foreach ($this->iphoneNotificationId as $key => $regId) {

            $message = new ApnsPHP_Message($regId);

            $message->setCustomIdentifier("Message-Badge-" . $regId);
            $message->setSound();
            $message->setbadge($this->iphoneBundal['badge']);
            $message->setTitle($this->iphoneBundal['title']);
            $message->setText(array(
                'title' => $this->iphoneBundal['title'],
                'message' => $this->iphoneBundal['message'],
                'action-loc-key' => $this->iphoneBundal['location'],
                'loc-key' => $this->iphoneBundal['message'],
                'itemId' => $this->iphoneBundal['itemId'],
                
                'actionkey' => $this->iphoneBundal['locationkey'],
                'TransporterName' => $this->iphoneBundal['TransporterName'],
                'TransporterImage' => $this->iphoneBundal['TransporterImage'],
                'request_version'=> 'new',
                'type' => $this->iphoneBundal['type'],
                
            )
            );

            $push->add($message);

        }
        $this->iphoneNotificationId = array();
        // Send all messages in the message queue
        try {
            $d = $push->send();

        } catch (Exception $e) {
            //echo "Send Push Failed".$e->getMessage();
            $push->send();
        }
        // Disconnect from the Apple Push Notification Service
        $push->disconnect();
        // Examine the error message container
        $aErrorQueue = $push->getErrors();
        if (!empty($aErrorQueue)) {
            //return $aErrorQueue;
        }

    }
    private function load_iphone()
    {
        $this->apnsDir = __dir__ . '/Notification/ApnsPHP/';
        $this->_apns_req();
    }
    private function _apns_req()
    {

        require_once $this->apnsDir . 'Abstract.php';
        require_once $this->apnsDir . 'Exception.php';
        require_once $this->apnsDir . 'Feedback.php';
        require_once $this->apnsDir . 'Message.php';
        require_once $this->apnsDir . 'Log/Interface.php';
        require_once $this->apnsDir . 'Log/Embedded.php';
        require_once $this->apnsDir . 'Message/Custom.php';
        require_once $this->apnsDir . 'Message/Exception.php';
        require_once $this->apnsDir . 'Push.php';
        require_once $this->apnsDir . 'Push/Exception.php';
        require_once $this->apnsDir . 'Push/Server.php';
        require_once $this->apnsDir . 'Push/Server/Exception.php';

        return;

    }

    /** 
    * TODO send notifcaiton with FCM
    */
    private function fire_android() {
        if (count($this->androidNotificationId) < 1) {
            return;
        }

        $url = 'https://fcm.googleapis.com/fcm/send';
        $result = array_chunk($this->androidNotificationId, 1000);

        if (is_array($result) && count($result) > 0) {
            foreach ($result as $key => $regId) {
                $data = array('registration_ids' => $regId, 'data' => $this->androidBundal);
                $response = $this->CreateHandle($url, $data);
            }
        }

        $this->androidNotificationId = array();
    }

    /*
     * Send Notification to both android
     */
    private function fire_android_old()
    {
        // $url = "https://android.googleapis.com/gcm/send";
        $url = 'https://fcm.googleapis.com/fcm/send';

        $result = array_chunk($this->androidNotificationId, 1000);
        if (is_array($result) && count($result) > 0) {
            foreach ($result as $key => $regId) {
                $urls[] = $url;
                $data[] = array(
                    "data" => $this->androidBundal,
                    "dry_run" => false,
                    "delay_while_idle" => true,
                    'registration_ids' => $regId,
                );
                // $data = array('data' => $this->androidBundal, 'registration_ids' => $regId);
            }
            $response = $this->MultiRequests($urls, $data);
            // print_r($response); die;
        }
        $this->androidNotificationId = array();

    }
    private function MultiRequests($urls, $data)
    {
        $curlMultiHandle = curl_multi_init();
        $curlHandles = array();
        $responses = array();
        foreach ($urls as $id => $url) {
            $curlHandles[$id] = $this->CreateHandle($url, $data[$id]);

            curl_multi_add_handle($curlMultiHandle, $curlHandles[$id]);
        }

        $running = null;
        do {
            curl_multi_exec($curlMultiHandle, $running);
        } while ($running > 0);

        foreach ($curlHandles as $id => $handle) {
            $responses[$id] = curl_multi_getcontent($handle);
            curl_multi_remove_handle($curlMultiHandle, $handle);
        }
        curl_multi_close($curlMultiHandle);

        return $responses;
    }
    private function CreateHandle_old($url, $data)
    {

        $curlHandle = curl_init($url);
        /// --- CHANGES BY ANOOP ----
        /// old AIzaSyDLfrFFYjApPUkSkup2YNoAOwLLwa3ILgg
        // $headers = array("Content-Type:" . "application/json", "Authorization:" . "key=AIzaSyCTCDs2nzufixfLdPKP2ethJHwebhMoiX0");
    
        $headers = array(
            'Authorization: key=' . env("FCM_SERVER_KEY"),
            'Content-Type: application/json',
        );

        $defaultOptions = array(
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_ENCODING => "gzip",
            CURLOPT_FOLLOWLOCATION => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => json_encode($data),
        );

        curl_setopt_array($curlHandle, $defaultOptions);

        return $curlHandle;
    }

    private function CreateHandle($url, $data) {
        // API access key from Google API's Console
        define( 'API_ACCESS_KEY', env("FCM_SERVER_KEY") );

        $headers = array (
            'Authorization: key=' . env("FCM_SERVER_KEY"),
            'Content-Type: application/json'
        );
         
        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, $url );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $data ) );
        $result = curl_exec($ch );
        curl_close( $ch );

        /*echo $result;
        die();*/
    }

    private function fire_web()
    {
        if (count($this->webNotificationId) < 1) {
            return;
        }

        $url = 'https://fcm.googleapis.com/fcm/send';

        $fields = array(
            'registration_ids' => $this->webNotificationId,
            'data' => array(
                'badge' => 1,
                'sound' => 'example.aiff',
                'title' => $this->webBundal['title'],
                'message' => $this->webBundal['message'],
                'action-loc-key' => $this->webBundal['type'],
                'loc-key' => $this->webBundal['message'],
                'actionkey' => $this->webBundal['locationkey'],
                'userName' => $this->webBundal['TransporterName'],
                'userImage' => $this->webBundal['TransporterImage'],
                'chat-data' => $this->webBundal['chat-data'],
                'itemId' => $this->webBundal['itemId'],
                'notification' => [
                    'title' => $this->webBundal['title'],
                    'body' => $this->webBundal['message'],
                ],
                'request_version'=> 'new',
            ),
        );
        $fields = json_encode($fields);

        $headers = array(
            'Authorization: key=' . env("FCM_SERVER_KEY"),
            'Content-Type: application/json',
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

        $result = curl_exec($ch);

        curl_close($ch);
        return $result;

    }
}
