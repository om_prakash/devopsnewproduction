<?php
class excel_reader
{
	function getData($filename)
	{
		if(trim($filename) != '')
		{
			require_once 'excel_reader2.php';
			$data = new Spreadsheet_Excel_Reader("$filename",true,"UTF-16");
			return $data->dumptoarray();
		}
	}
	function remove($filename)
	{
		if(is_dir($filename) == FALSE)
		{
			if(trim($filename) != '')
			{
				if(file_exists($filename))
				{
					unlink($filename);
				}
			}
		}
	}
}
