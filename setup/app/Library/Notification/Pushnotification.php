<?php 
namespace App\Library\Notification;
use ApnsPHP_Push;
use ApnsPHP_Abstract;
use ApnsPHP_Message;

Class Pushnotification
{
	private $iphoneBundal;
	private $iphoneNotificationId;
	private $androidBundal;
	private $androidNotificationId;
	
	public function __construct()
	{
		$this->iphoneNotificationId = array();
		$this->androidNotificationId = array();
		$this->iphoneBundal = array('title' => '', 'badge' => 0, 'locationkey' => '', 'location' => 'no_action', 'TransporterImage' => '', 'TransporterName' => '', 'message' => '', 'request_version' => 'new', 'type' => '', 'chat-data' => array(), 'itemId' => '');
		$this->androidBundal = array('title' => '', 'badge' => 0, 'locationkey' => '', 'location' => 'no_action', 'TransporterImage' => '', 'TransporterName' => '', 'message' => '', 'request_version' => 'new', 'type' => '', 'chat-data' => array(), 'itemId' => '');
		$this->load_iphone();
	}
	/*
	 * Add notification id that we want ot send notification
	 *
	 */

	public function add_user($userNotificationid='',$type='')
	{
			$type = strtolower($type);
		if(!empty($userNotificationid) && $userNotificationid != null)
		{
			if($type == 'android'){
				array_push($this->androidNotificationId,$userNotificationid);
			}else if($type == 'iphone') {
				array_push($this->iphoneNotificationId,$userNotificationid);
			}
		}
		

	}	
	/*
	 * Add a custom field to bundal
	 *
	 */
	public function setCustomField($field,$value,$type='both')
	{
		if($type == 'both'){
			$this->iphoneBundal[$field] = $value;
			$this->androidBundal[$field] = $value;
		}else if($type == 'iphone'){
			$this->iphoneBundal[$field] = $value;
		}else if($type == 'android'){
			$this->androidBundal[$field] = $value;
		}
		
	}
	/*
	 * Set value
	 *
	 */
	public function setValue($field,$value,$type='both')
	{
		if($type == 'both'){
			$this->iphoneBundal[$field] = $value;
			$this->androidBundal[$field] = $value;
		}else if($type == 'iphone'){
			$this->iphoneBundal[$field] = $value;
		}else if($type == 'android'){
			$this->androidBundal[$field] = $value;
		}

	}
	/*
	 * Send Notification to both android and iphone
	 */
	public function fire()
	{

		try{

			$this->fire_android();
			$this->fire_iphone();
		}
		catch(exception $e){}
		
	}
	/*
	 * Send Notification to iphone
	 */
	private function fire_iphone()
	{	
		if(count($this->iphoneNotificationId) < 1){
			return;
		}

		 //if(!$token || !$msg) return;

        // Instantiate a new ApnsPHP_Push object
        /*$push = new ApnsPHP_Push(
            ApnsPHP_Abstract::ENVIRONMENT_PRODUCTION, 
            $this->apnsDir.'SSL/iHelper_production.pem'	
        );*/
        $push = new ApnsPHP_Push(
            ApnsPHP_Abstract::ENVIRONMENT_SANDBOX, // ENVIRONMENT_PRODUCTION
           	//$this->apnsDir.'SSL/Aquantuo_Developement.pem'
            //$this->apnsDir.'SSL/Aqunato_Dev.pem'	
            $this->apnsDir.'SSL/Auantuo_Dev_10_12_2019.pem'
            
        );
       
        // Set the Provider Certificate passphrase
        // $push->setProviderCertificatePassphrase('tablecan29');

        // Set the Root Certificate Autority to verify the Apple remote peer
        $push->setRootCertificationAuthority($this->apnsDir.'SSL/entrust_root_certification_authority.pem');

        // Connect to the Apple Push Notification Service
        $push->connect();

        foreach($this->iphoneNotificationId as $key => $regId)
		{
 
			$message = new ApnsPHP_Message($regId);
			
			$message->setCustomIdentifier("Message-Badge-".$regId);
			$message->setSound();
			$message->setbadge($this->iphoneBundal['badge']);
			$message->setTitle($this->iphoneBundal['title']);
			$message->setText(array(
							'title' 	=> $this->iphoneBundal['title'],
							'message' 	=> $this->iphoneBundal['message'],
							'action-loc-key' => $this->iphoneBundal['type'],
							'loc-key'	=> $this->iphoneBundal['message'],
							'actionkey'	=> $this->iphoneBundal['locationkey'],
							'userName' => $this->iphoneBundal['TransporterName'],
							'userImage' => $this->iphoneBundal['TransporterImage'],
							'chat-data'	=> $this->iphoneBundal['chat-data'],
							'itemId'	=> $this->iphoneBundal['itemId'],
						)
					);	/*
			$message->setText(array(
							'title' 	=> $this->iphoneBundal['title'],
							'message' 	=> $this->iphoneBundal['message'],
							'action-loc-key' => $this->iphoneBundal['location'],
							'loc-key'	=> $this->iphoneBundal['message'],
							'actionkey' => $this->iphoneBundal['locationkey'],							
							'TransporterName' => $this->iphoneBundal['TransporterName'],
							'TransporterImage' => $this->iphoneBundal['TransporterImage']
						)
					);	*/
			
			$push->add($message);
			
		}
		 
		$this->iphoneNotificationId  = array();
		// Send all messages in the message queue
		try{
			$d = $push->send();
			//echo "Send Push success";
			//print_r($d); die;
			
		}catch(Exception $e){
			 
			$push->send();
		}
		// Disconnect from the Apple Push Notification Service
		$push->disconnect();
		// Examine the error message container
		$aErrorQueue = $push->getErrors();
		if (!empty($aErrorQueue)) 
		{
			return $aErrorQueue;
		}

	}
	private function load_iphone()
	{
		$this->apnsDir = __dir__.'/ApnsPHP/';
        $this->_apns_req();
    }   
    private function _apns_req() {		
		
        require_once $this->apnsDir.'Abstract.php';        
        require_once $this->apnsDir.'Exception.php';
        require_once $this->apnsDir.'Feedback.php';
        require_once $this->apnsDir.'Message.php';
        require_once $this->apnsDir.'Log/Interface.php';
        require_once $this->apnsDir.'Log/Embedded.php';
        require_once $this->apnsDir.'Message/Custom.php';
        require_once $this->apnsDir.'Message/Exception.php';
        require_once $this->apnsDir.'Push.php';
        require_once $this->apnsDir.'Push/Exception.php';
        require_once $this->apnsDir.'Push/Server.php';
        require_once $this->apnsDir.'Push/Server/Exception.php';

        return;

    }
	/*
	 * Send Notification to both android
	 */
	private function fire_android()
	{
		//$url="https://android.googleapis.com/gcm/send";
		$url = 'https://fcm.googleapis.com/fcm/send';
      
		$result = array_chunk($this->androidNotificationId, 1000);
       
		if(is_array($result) && count($result) > 0)
		{ 
			foreach($result as $key => $regId)
			{
				$urls[]=$url;
				$data[]=array(
						"data" 		=> $this->androidBundal,
						"dry_run"	=> false,
						"delay_while_idle" => true,
						'registration_ids' => $regId
				);
			}
			$response = $this->MultiRequests($urls,$data);
             
		}
		$this->androidNotificationId = array();
         
	}
	private function MultiRequests($urls , $data)
	{
		$curlMultiHandle = curl_multi_init();
		$curlHandles = array();
		$responses = array();
		foreach($urls as $id => $url) {
			$curlHandles[$id] = $this->CreateHandle($url , $data[$id]);

			curl_multi_add_handle($curlMultiHandle, $curlHandles[$id]);
		}

		$running = null;
		do {
			curl_multi_exec($curlMultiHandle, $running);
		} while($running > 0);

		foreach($curlHandles as $id => $handle) {
			$responses[$id] = curl_multi_getcontent($handle);
			curl_multi_remove_handle($curlMultiHandle, $handle);
		}
		curl_multi_close($curlMultiHandle);

		return $responses;
	}
	private function CreateHandle($url , $data) {
		
		$curlHandle = curl_init($url);
		/// --- CHANGES BY ANOOP ----
		$headers = array("Content-Type:" . "application/json", "Authorization:" . "key=AIzaSyDLfrFFYjApPUkSkup2YNoAOwLLwa3ILgg");
		$defaultOptions = array (
			CURLOPT_HTTPHEADER =>$headers,		
			CURLOPT_ENCODING => "gzip" ,
			CURLOPT_FOLLOWLOCATION => false,
			CURLOPT_RETURNTRANSFER => true ,
			CURLOPT_POST => 1,
			CURLOPT_POSTFIELDS => json_encode($data)
		);

		curl_setopt_array($curlHandle , $defaultOptions);

		return $curlHandle;
	}
}
