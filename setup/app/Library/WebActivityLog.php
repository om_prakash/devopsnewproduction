<?php
namespace App\Library;
use App\Http\Models\Activitylog;
use App\Http\Models\Deliveryrequest;
use MongoDate;

class WebActivityLog {

	public function ActivityLog($inputArray) {

		if (count($inputArray) > 0) {
			$message = '';
			if ($inputArray['ProductList'][0]['status'] == 'out_for_pickup') {
				$message = 'Pick up Scheduled/Package enroute to Aquantuo’s facility.';//'Item is in destination country going through customs and sorting.';
				if ($inputArray['RequestType'] === 'local_delivery') {
					$message = 'Request has been picked up.';
				}
			} else if ($inputArray['ProductList'][0]['status'] == 'out_for_delivery') {
				$message = 'Your package is en route to be delivered.';
				if ($inputArray['RequestType'] === 'delivery') {
					$message = 'Package is in destination country and is out for delivery.';
				}
			} else if ($inputArray['ProductList'][0]['status'] == 'purchased') {
				$message = 'Product has been cancel.';
			} else if ($inputArray['ProductList'][0]['status'] == 'delivered') {
				$message = 'Package has been successfully delivered.';
			} else if ($inputArray['ProductList'][0]['status'] == 'ready') {
				$message = 'Product has been cancel.';
			} else if ($inputArray['ProductList'][0]['status'] == 'shipment_departed') {
				$message = 'Shipment Departed originating country.';
			}

			if (isset($inputArray['ProductList'])) {
				$activityCollection = [
					'request_id' => (string) $inputArray['_id'],
					'request_type' => $inputArray['RequestType'],
					'PackageNumber' => $inputArray['PackageNumber'],
					'log_type' => 'request',
					'status' => $inputArray['ProductList'][0]['status'],
					'message' => $message,
					'package_id' => $inputArray['ProductList'][0]['package_id'],
					'item_name' => $inputArray['ProductList'][0]['product_name'],
					'action_user_id'=> @$inputArray['action_user_id'],
					'EnterOn' => new MongoDate,
				];
				Activitylog::Insert($activityCollection);

			}
		}
	}

	public function buyActivity($id) {
		$ActivityLog = DeliveryRequest::where('_id', '=', $id)->select('_id', 'PackageNumber', 'ProductList')->first();
		$array = $ActivityLog->ProductList;
		$array = $ActivityLog->ProductList;
		foreach ($array as $key) {
			/* Activity Log */
			echo "<pre>";
			print_r($key);
			$insertactivity = [
				'request_id' => $id,
				'request_type' => 'buy_for_me',
				'PackageNumber' => $ActivityLog->PackageNumber,
				'item_id' => $key['_id'],
				'package_id' => $key['package_id'],
				'item_name' => $key['product_name'],
				'log_type' => 'request',
				'message' => 'Package has been created.',
				'status' => 'pending',
				'EnterOn' => new MongoDate(),

			];
			Activitylog::insert($insertactivity);
		}
	}
}
