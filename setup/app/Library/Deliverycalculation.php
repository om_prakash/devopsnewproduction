<?php
namespace App\Library;

use App\Modules\Admin\Models\Calculation\Calculation;
use App\Modules\Admin\Models\Counters\Counters;

class Deliverycalculation {
    
    public static function get_feet($value,$unit)
	{
		$InFeet = (float)$value;
		$unit = strtolower($unit);
		if($unit == 'inch'){ 
			 $InFeet = $InFeet * 0.0833333;
		}else if($unit == 'meter'){ 
			$InFeet = $InFeet * 3.28084;
		}else if($unit == 'centimetre' || $unit == 'centimeter'){ 
			$InFeet = $InFeet * 0.0328084;
		}
		return $InFeet;
	}
	public static function get_cost($deliveryType='Regular',$Weight=0,$distance=0,$CostPer=0,$UrgentCostPer=0,$productSize=0)
	{
		
		
		$where = array();
		if($productSize>0.5){
			$where['size'] = 'Max';
		}
		else{
			$where['size'] = 'Min';
		}
		if($distance>500){
			$where['distance'] = 'Max';
		}
		else{
			$where['distance'] = 'Min';
		}
		$field = array('distance','size','FixedInsuranceValue','FixedInsuranceCost','InsuranceRate','MiminumCost','ShippingFactor');	
		$setting = Calculation::where($where)->get($field);
		
		if(count($setting) > 0)
		{	
			
		$deliveryType = $deliveryType; // Delivery type
		// Calculation for Shipping cost 
		$NCPM = $WeightFactorial = $SizeFactorial = 0;

		$ProductWeightInPound = $Weight; //Weight in pound
		if($Weight==0){ $ProductWeightInPound = 0.01;}
		$urgentcost_per_km = $UrgentCostPer; // minmimum Urgent cost
		$distanceInMiles = $distance; // distance in miles 
		$cost_per_km = $CostPer ; // minmium Cost
	 
		$Nextminimumcost = $Nexturgentcost = $FinalMinimumCost = $FinalUrgentCost= $minimumcost =  $urgentcost = 0;
		//==============================================Get first time ==============================================
		
		foreach($setting[0]->MiminumCost as $key) {		
			if($key['StartWeight'] < $ProductWeightInPound AND $key['EndWeight'] >= $ProductWeightInPound) {
				$NCPM = $key['NCPM'];
				$WeightFactorial = $key['WeightFactorial'];		
				$SizeFactorial = $key['SizeFactorial']; 
				//break;
			}
			$MaxWeight[] = $key['EndWeight'];
		}
		
		
		
		  $Actualdivided = ($ProductWeightInPound / max($MaxWeight)); 
		
		$divided = (int)($ProductWeightInPound / max($MaxWeight)); 
		$modulo = $ProductWeightInPound % max($MaxWeight);
		//=====================if pacakge weight more then maimum wait  then we divide into sub pacakge
		if(max($MaxWeight)<$ProductWeightInPound)
		{
			foreach($setting[0]->MiminumCost as $key) {		
				if($key['StartWeight'] <= $modulo AND $key['EndWeight'] >= $modulo) {
					$NextNCPM = $key['NCPM'];
					 $NextWeightFactorial = $key['WeightFactorial'];		
					 $NextSizeFactorial = $key['SizeFactorial'];
					break;
				}
				
			}
			foreach($setting[0]->MiminumCost as $key) {
				if($key['EndWeight'] == max($MaxWeight)) {
					$NCPM = $key['NCPM'];
					$WeightFactorial = $key['WeightFactorial'];		
					$SizeFactorial = $key['SizeFactorial'];
					
				}
				$MaxWeight[] = $key['StartWeight'];
			}
		}
		if($distance<=500)
		{
			//==================================Calulate one package value=========================== 
			$minimumcost = ($cost_per_km + ($NCPM * $WeightFactorial * $SizeFactorial * $distanceInMiles));	
			if($deliveryType == 'Urgent') {
			 $urgentcost  = ($urgentcost_per_km + $NCPM * $WeightFactorial * $SizeFactorial * $distanceInMiles);
			}
			 
			 //=======================================If request is grater then one package===========================
			
			 if($Actualdivided>1 && $modulo>0) {
				 $Nextminimumcost = ($cost_per_km + ($NextNCPM * $NextWeightFactorial * $NextSizeFactorial * $distanceInMiles));	
				if($deliveryType == 'Urgent') {
					 $Nexturgentcost  = ($urgentcost_per_km + $NextNCPM * $NextWeightFactorial * $NextSizeFactorial * $distanceInMiles);
				}
				
			 } 
		 }
		 else{
			//==================================Calulate one package value=========================== 
			$minimumcost = $cost_per_km + $NCPM * $WeightFactorial * $SizeFactorial *500*(1+(1/$setting['ShippingFactor'])*min($distance/500,$setting['ShippingFactor']));	
							
			if($deliveryType == 'Urgent') {
			 $urgentcost  = $urgentcost_per_km + $NCPM * $WeightFactorial * $SizeFactorial *500*(1+(1/$setting['ShippingFactor'])*min($distance/500,$setting['ShippingFactor']));
			}
			 
			 //=================================If request is grater then one package========================
			
			 if($Actualdivided>1 && $modulo>0) {
				$Nextminimumcost = ($cost_per_km + ($NextNCPM * $NextWeightFactorial * $NextSizeFactorial * $distanceInMiles));	
				$Nextminimumcost = $cost_per_km + $NextNCPM * $NextWeightFactorial * $NextSizeFactorial *500*(1+(1/20)*min($distance/500,20));	

				if($deliveryType == 'Urgent') {
					 $Nexturgentcost  = $urgentcost_per_km + $NextNCPM * $NextWeightFactorial * $NextSizeFactorial *500*(1+(1/20)*min($distance/500,20));	
				}
				
			 } 
		 }
		
		if(($modulo<=0 || $Actualdivided>1) && $ProductWeightInPound>=1) {

		   $minimumcost = 	$minimumcost*$divided; 
			if($deliveryType == 'Urgent') {			
				$urgentcost  = $urgentcost*$divided;
			}
		}
		 //=======================================End if  request is grater then one package===========================
		 
		 $FinalMinimumCost = $minimumcost + $Nextminimumcost;
		 if($deliveryType == 'Urgent') {		 
				$FinalUrgentCost  = $urgentcost+ $FinalUrgentCost;
			}
		
		$Cost= array('MinmunCost'=>$FinalMinimumCost,'UrgentCost'=>$FinalUrgentCost,'success'=>1);
		}
		else{
			$Cost= array('success'=>0);
		}
		return ($Cost);
	}
	public static function getSequence($name)
	{
		$seq = 1;
		if(Counters::where('_id', $name)->increment('seq') == 0){
			Counters::insert(array('_id'=>$name,'seq'=>2));			
		}
		$d = Counters::where('_id', $name)->get();
		foreach($d as $k)
		{
			$seq = $k['seq'];
		}
		return $seq;
	}
}
