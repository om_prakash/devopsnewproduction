<?php

namespace App\Library;

use App\Http\Models\Configuration;
use App\Http\Models\Deliveryrequest;
use App\Http\Models\Notification;
use App\Http\Models\User;
use App\Library\Notify;
use App\Library\Promo;
use App\Library\Requesthelper;
use App\Library\Utility;
use DateTime;
use Input;
use MongoDate;
use MongoId;
use Session;

class Reqhelper {

	private static function set($input, $field) {
		return isset($input[$field]) ? $input[$field] : '';
	}
	private static function setint($input, $field) {
		$input[$field] = isset($input[$field]) ? $input[$field] : '';
		return (floatval($input[$field]));
	}
	private static function get_measurement_unit($input, $field) {
		$unit = (Object) ['weight' => 'kg', 'volume' => 'cm'];
		$input[$field] = isset($input[$field]) ? $input[$field] : '';
		if ($input[$field] == 'inches_lbs') {
			$unit = (Object) ['weight' => 'lbs', 'volume' => 'inches'];
		}
		return $unit;
	}

	public function outsideOfAccraCharge($address) {

		if (isset($address->lat) && isset($address->lng)) {
			$lat = $address->lat;
			$lng = $address->lng;
		} else {
			$lat = $address['lat'];
			$lng = $address['lng'];
		}

		$distance = Utility::getDistanceBetweenPointsNew(5.5187188, -0.2046139, $lat, $lng);
		$charge = false;
		if ($distance != false && $distance > 0) {
			$distance_km = $distance / 1000;
			$range = 30;
			//print_r($address->lat); die;
			if ($distance_km > 30) {
				$charge = true;
			}
		}
		return $charge;
	}

	public function request_create($user) {

		$response = ['success' => 0, 'msg' => 'Oops something is worng'];
		$input = Input::get();

		$input['RequesterId'] = new MongoId($user->_id);
		$input['RequesterName'] = $user->Name;

		$unit = self::get_measurement_unit($input, 'measurement_unit');
		$input['unit_volume'] = $unit->volume;
		$input['unit_weight'] = $unit->weight;

		//print_r($input); die;

		$calculationinfo = $this->get_shipping_charge($input);
		if (isset($calculationinfo->error)) {
			$response['msg'] = $calculationinfo->error;
		}
		$input['discount'] = 0;
		$input['insurance_cost'] = $calculationinfo->insurance;
		$input['shipping_cost'] = $calculationinfo->shippingcost;
		$input['total_cost'] = $calculationinfo->insurance + $calculationinfo->shippingcost;
		$input['aq_fee'] = self::get_aquantuo_fees($input['total_cost']);
		$input['outside_accra_charge'] = 0;
		$address = ['lat' => Input::get('DeliveryLat'), 'lng' => Input::get('DeliveryLong')];

		$extraa_charge = $this->outsideOfAccraCharge($address);
		if ($extraa_charge != false) {
			$rate = Configuration::where(['_id' => '5673e33e6734c4f874685c84'])->select('accra_charge', 'accra_charge_type')->first();

			if ($rate) {
				if ($rate->accra_charge_type == 'Percentage') {
					$input['outside_accra_charge'] = ($input['shipping_cost'] * $rate->accra_charge) / 100;
				} else {
					$input['outside_accra_charge'] = $rate->accra_charge;
				}

			}
			$input['total_cost'] = $input['total_cost'] + $input['outside_accra_charge'];
			$input['shipping_cost'] = $input['shipping_cost'] + $input['outside_accra_charge'];
		}

		$response['success'] = 1;

		$discount = Promo::get_validate($input['promocode'], $input['shipping_cost'], Session::get('UserId'), $user->Default_Currency);
		if ($discount['success'] == 1) {
			$input['discount'] = $discount['discount'];
		}

		if (trim(Input::get('request_id')) == '') {
			$input['StripeChargeId'] = '';
			$input = $this->pr_field($input);
			$response['type'] = 'create';
			$response['reqid'] = (String) Deliveryrequest::insertGetId($input);

			/*Notification::insert([
				                "NotificationTitle" => "New request of Send a package",
				                "NotificationMessage" => sprintf('The send a package request titled:"%s", ID:%s has been created.',
				                    ucfirst($input['ProductTitle']), $input['PackageNumber']),
				                "NotificationUserId" => [],
				                "NotificationReadStatus" => 0,
				                "location" => "online",
				                "locationkey" => (String) @$input['_id'],
				                "Date" => new MongoDate(),
				                "GroupTo" => "Admin",
			*/

		} else {
			$delinfo = Deliveryrequest::where(array('_id' => Input::get('request_id')))
				->whereIn('Status', ['ready', 'pending'])->first();
			if (count($delinfo) > 0) {
				$input['old_ProductImage'] = $delinfo->ProductImage;
				$input['old_OtherImage'] = $delinfo->OtherImage;
				$input['StripeChargeId'] = $delinfo->StripeChargeId;
				$input = $this->pr_field($input);
				$update = (String) Deliveryrequest::where(array('_id' => Input::get('request_id')))->update($input);
				if ($update) {
					$response['reqid'] = Input::get('request_id');
					$response['type'] = 'update';

					Notification::insert([
						"NotificationTitle" => "Update request of Send a package",
						"NotificationMessage" => sprintf('The send a package request titled:"%s", ID:%s has been updated.', ucfirst($input['ProductTitle']), $input['PackageNumber']),
						"NotificationUserId" => [],
						"NotificationReadStatus" => 0,
						"location" => "online",
						"locationkey" => (String) @$input['_id'],
						"Date" => new MongoDate(),
						"GroupTo" => "Admin",
					]);

				}
			} else {
				$response['success'] = 0;
				$response['reqid'] = Input::get('request_id');
				$response['type'] = 'update';
				$response['msg'] = 'You can only update ready or pending request.';
			}
		}

		return $response;
	}
	private function get_shipping_charge($input) {
		$requesthelper = new Requesthelper(
			[
				"needInsurance" => (self::set($input, 'insurance') == 'yes') ? 'yes' : 'no',
				"productQty" => (self::setint($input, 'quantity') > 1) ? (int) $input['quantity'] : 1,
				"productCost" => self::setint($input, 'package_value'),
				"productWidth" => self::set($input, 'width'),
				"productWidthUnit" => self::set($input, 'unit_volume'),
				"productHeight" => self::set($input, 'height'),
				"productHeightUnit" => self::set($input, 'unit_volume'),
				"productLength" => self::set($input, 'length'),
				"productLengthUnit" => self::set($input, 'unit_volume'),
				"productWeight" => self::set($input, 'weight'),
				"productWeightUnit" => self::set($input, 'unit_weight'),
				"productCategory" => $this->get_value_from_json(self::set($input, 'category'), 'name'),
				"productCategoryId" => $this->get_value_from_json(self::set($input, 'category'), 'id'),
				"distance" => self::setint($input, 'distance'),
				"travelMode" => self::set($input, 'travel_mode'),
			]
		);

		$requesthelper->calculate();
		return $requesthelper->get_information();

	}
	private function pr_field($input, $id = '') {
		// Prepare request field
		$PackageId = $this->get_packageno();
		$unit = self::get_measurement_unit($input, 'measurement_unit');

		// calculate distance if api did not give
		$calculated_distance = floatval(self::set($input, 'calculated_distance'));

		if ($calculated_distance > 0) {
			$calculated_distance = $calculated_distance * 0.000621371;
		} else {
			$calculated_distance = $this->getDistanceBetweenPointsNew(
				floatval(self::set($input, 'PickupLat')),
				floatval(self::set($input, 'PickupLong')),
				floatval(self::set($input, 'DeliveryLat')),
				floatval(self::set($input, 'DeliveryLong'))
			);
		}
		// end calculate distance if api did not give

		if (isset($input['transporter_id'])) {
			if (strlen($input['transporter_id']) == 24) {
				$input['transporter_id'] = new MongoId($input['transporter_id']);
			}
		}
		if (isset($input['tripid']) && strlen(@$input['tripid']) == 24) {
			$input['tripid'] = new MongoId($input['tripid']);
		}
		// Default image section
		$ProductImage = self::set($input, 'old_ProductImage');

		if (Input::hasFile('default_image')) {
			if (Input::file('default_image')->isValid()) {
				$ext = Input::file('default_image')->getClientOriginalExtension();
				$ProductImage = time() . rand(100, 9999) . ".$ext";

				if (Input::file('default_image')->move(BASEURL_FILE . 'package/', $ProductImage)) {
					$ProductImage = "package/$ProductImage";

					// Start remove old image
					$oldimg = self::set($input, 'old_ProductImage');
					if (!empty(trim($oldimg))) {
						if (file_exists(BASEURL_FILE . $oldimg)) {
							unlink(BASEURL_FILE . $oldimg);
						}
					}
					// End remove old image
				} else {
					$ProductImage = '';
				}
			}
		}
		// End default image section

		// Upload all other images
		$otherImages = [];
		$oldimg = self::set($input, 'old_OtherImage');
		if (is_array($oldimg)) {
			$otherImages = $oldimg;
		}

		$imageArray = array('package_image2', 'package_image3', 'package_image4', 'package_image5');
		foreach ($imageArray as $key => $val) {
			if (Input::hasFile($val)) {
				if (Input::file($val)->isValid()) {
					$ext = Input::file($val)->getClientOriginalExtension();
					$img = time() . rand(100, 9999) . ".$ext";

					if (Input::file($val)->move(BASEURL_FILE . 'package/', $img)) {
						$img = "package/$img";
						if (isset($otherImages[$key])) {

							// Start remove old image
							if (!empty(trim($otherImages[$key]))) {
								if (file_exists(BASEURL_FILE . $otherImages[$key])) {
									unlink(BASEURL_FILE . $otherImages[$key]);
								}
							}
							// End remove old image

							$otherImages[$key] = $img;
						} else {
							$otherImages[] = $img;
						}
					}
				}
			}
		}

		return [
			"RequesterId" => self::set($input, 'RequesterId'),
			"RequesterName" => self::set($input, 'RequesterName'),
			"TransporterName" => isset($input['TransporterName']) ? $input['TransporterName'] : '',
			"TransporterId" => isset($input['transporter_id']) ? $input['transporter_id'] : '',
			"RequestType" => "delivery",
			'device_version' => isset($input['browser']) ? $input['browser'] : '',
			'app_version' => isset($input['version']) ? $input['version'] : '',
			'device_type' => isset($input['device_type']) ? $input['device_type'] : '',
			"PackageId" => $PackageId,
			"TripId" => isset($input['tripid']) ? $input['tripid'] : '',
			"PackageNumber" => $PackageId . time(),
			"ProductTitle" => isset($input['title']) ? $input['title'] : '',
			"PickupFullAddress" => $this->formated_address([
				isset($input['address_line_1']) ? $input['address_line_1'] : '',
				isset($input['address_line_2']) ? $input['address_line_2'] : '',
				$this->get_value_from_json(self::set($input, 'city'), 'name'),
				$this->get_value_from_json(self::set($input, 'state'), 'name'),
				$this->get_value_from_json(self::set($input, 'country'), 'name'),
			], self::set($input, 'zipcode')),
			"PickupAddress" => isset($input['address_line_1']) ? $input['address_line_1'] : '',
			"PickupAddress2" => isset($input['address_line_2']) ? $input['address_line_2'] : '',
			"PickupCity" => $this->get_value_from_json(self::set($input, 'city'), 'name'),
			"PickupState" => $this->get_value_from_json(self::set($input, 'state'), 'name'),
			"PickupCountry" => $this->get_value_from_json(self::set($input, 'country'), 'name'),
			"PickupPinCode" => isset($input['zipcode']) ? $input['zipcode'] : '',
			"PickupLatLong" => [floatval(self::set($input, 'PickupLong')), floatval(self::set($input, 'PickupLat'))],

			"PickupDate" => get_utc_time(self::set($input, 'pickup_date')),
			"DeliveryFullAddress" => $this->formated_address([
				isset($input['drop_off_address_line_1']) ? $input['drop_off_address_line_1'] : '',
				isset($input['drop_off_address_line_2']) ? $input['drop_off_address_line_2'] : '',
				$this->get_value_from_json(self::set($input, 'drop_off_city'), 'name'),
				$this->get_value_from_json(self::set($input, 'drop_off_state'), 'name'),
				$this->get_value_from_json(self::set($input, 'drop_off_country'), 'name'),
			], self::set($input, 'drop_off_zipcode')),
			"DeliveryAddress" => self::set($input, 'drop_off_address_line_1'),
			"DeliveryAddress2" => self::set($input, 'drop_off_address_line_2'),
			"DeliveryCity" => $this->get_value_from_json(self::set($input, 'drop_off_city'), 'name'),
			"DeliveryState" => $this->get_value_from_json(self::set($input, 'drop_off_state'), 'name'),
			"DeliveryCountry" => $this->get_value_from_json(self::set($input, 'drop_off_country'), 'name'),
			"DeliveryPincode" => self::set($input, 'drop_off_zipcode'),
			"DeliveryLatLong" => [floatval(self::set($input, 'DeliveryLong')), floatval(self::set($input, 'DeliveryLat'))],
			"DeliveryDate" => get_utc_time(self::set($input, 'drop_off_date')),
			"ReturnFullAddress" => $this->formated_address([
				self::set($input, 'return_address_line_1'),
				self::set($input, 'return_address_line_2'),
				$this->get_value_from_json(self::set($input, 'return_city'), 'name'),
				$this->get_value_from_json(self::set($input, 'return_state'), 'name'),
				$this->get_value_from_json(self::set($input, 'return_country'), 'name'),
			], self::set($input, 'return_zipcode')),

			"ReturnAddress" => self::set($input, 'return_address_line_1'),
			"ReturnAddress2" => self::set($input, 'return_address_line_2'),
			"ReturnCityTitle" => $this->get_value_from_json(self::set($input, 'return_city'), 'name'),
			"ReturnStateTitle" => $this->get_value_from_json(self::set($input, 'return_state'), 'name'),
			"ReturnCountry" => $this->get_value_from_json(self::set($input, 'return_country'), 'name'),
			"ReturnPincode" => self::set($input, 'return_zipcode'),
			"FlexibleDeliveryDate" => (self::set($input, 'is_delivery_date_flexible') == 'yes') ? 'yes' : 'no',
			"JournyType" => (self::set($input, 'journey_type') == 'return') ? 'return' : 'one_way',
			"NotDelReturnFullAddress" => $this->formated_address([
				self::set($input, 'nd_return_address_line_1'),
				self::set($input, 'nd_return_address_line_2'),
				$this->get_value_from_json(self::set($input, 'nd_return_city'), 'name'),
				$this->get_value_from_json(self::set($input, 'nd_return_state'), 'name'),
				$this->get_value_from_json(self::set($input, 'nd_return_country'), 'name'),
			], self::set($input, 'nd_return_zipcode')),
			"InCaseNotDelReturnAddress" => self::set($input, 'nd_return_address_line_1'),
			"InCaseNotDelReturnAddress2" => self::set($input, 'nd_return_address_line_2'),
			"InCaseNotDelReturnCity" => $this->get_value_from_json(self::set($input, 'nd_return_city'), 'name'),
			"InCaseNotDelReturnState" => $this->get_value_from_json(self::set($input, 'nd_return_state'), 'name'),
			"InCaseNotDelReturnCountry" => $this->get_value_from_json(self::set($input, 'nd_return_country'), 'name'),
			"InCaseNotDelReturnPincode" => self::set($input, 'nd_return_zipcode'),
			"ProductCost" => self::setint($input, 'package_value'),
			"ProductWeight" => self::set($input, 'weight'),
			"ProductWeightUnit" => $unit->weight,
			"ProductHeight" => self::set($input, 'height'),
			"ProductHeightUnit" => $unit->volume,
			"ProductLength" => self::set($input, 'length'),
			"ProductLengthUnit" => $unit->volume,
			"ProductWidth" => self::set($input, 'width'),
			"ProductWidthUnit" => $unit->volume,
			"ProductImage" => $ProductImage,
			"OtherImage" => $otherImages,
			"ReceiverCountrycode" => self::set($input, 'country_code'),
			"ReceiverMobileNo" => self::set($input, 'phone_number'),
			"QuantityStatus" => (self::set($input, 'quantity') > 1) ? 'multiple' : 'single',
			//"BoxQuantity" => (int) self::set($input, 'quantity'),
			"BoxQuantity" => (self::set($input, 'quantity') == 0) ? 1 : (int) (self::set($input, 'quantity') == 0),
			"Category" => $this->get_value_from_json(self::set($input, 'category'), 'name'),
			"CategoryId" => $this->get_value_from_json(self::set($input, 'category'), 'id'),
			"Distance" => $calculated_distance,
			"Description" => self::set($input, 'description'),
			"Status" => ((self::setint($input, 'total_cost') - $input['discount']) <= 0.50) ? 'ready' : 'pending',
			"InsuranceStatus" => (self::set($input, 'insurance') == 'yes') ? 'yes' : 'no',
			"PackageMaterial" => (self::set($input, 'need_package_material') == 'yes') ? 'yes' : 'no',
			"PackageMaterialShipped" => '',
			"StripeChargeId" => $input['StripeChargeId'],
			"PaymentDate" => '',
			"PaymentStatus" => '',
			"InsuranceCost" => self::setint($input, 'insurance_cost'),
			"ShippingCost" => self::setint($input, 'shipping_cost'),
			"outside_accra_charge" => self::setint($input, 'outside_accra_charge'),
			"TotalCost" => self::setint($input, 'total_cost') - $input['discount'],
			"Discount" => floatval($input['discount']),
			"AquantuoFees" => self::get_aquantuo_fees(self::setint($input, 'total_cost')),
			"RejectBy" => '',
			"ReturnType" => '',
			"RejectTime" => '',
			"TrackLocation" => '',
			"ReceiptImage" => '',
			"TrackingNumber" => '',
			"ReturnDate" => '',
			"CarrierMessage" => '',
			"DeliveredStartTime" => '',
			"DeliveredTime" => '',
			"PackageCareNote" => self::set($input, 'care_note'),
			"DeliveryVerifyCode" => rand(1000, 9999),
			"TransporterFeedbcak" => '',
			"TransporterRating" => '',
			"RequesterFeedbcak" => '',
			"RequesterRating" => '',
			"TPLatLong" => [],
			"PublicPlace" => (self::set($input, 'public_palce') == 'yes') ? 'yes' : 'no',
			"TravelMode" => (self::set($input, 'travel_mode') == 'air') ? 'air' : 'ship',
			"EnterOn" => new MongoDate(),
			"UpdateOn" => new MongoDate(),
		];
	}

	private function get_packageno() {
		return Utility::sequence('Request');
	}
	private function formated_address($array, $zip) {
		$address = '';
		foreach ($array as $ky => $val) {
			if (trim($val) != '') {
				if (trim($address) != '') {$address = "$address, ";}
				$address .= $val;
			}
		}
		if (trim($zip) != '') {$address .= "- $zip";}

		return $address;
	}
	private function get_value_from_json($value, $field) {
		$value = (array) json_decode($value);
		return isset($value[$field]) ? $value[$field] : '';
	}
	private function get_mongo_date($date, $formate = 'd/m/Y h:i A') {
		$date = DateTime::createFromFormat($formate, $date);
		if (is_object($date)) {
			return new MongoDate(strtotime($date->format('d-m-Y H:i:s')));
		}
		return;
	}
	public static function get_aquantuo_fees($totalcost) {
		$fees = 0;

		$config = Configuration::find('5673e33e6734c4f874685c84');
		if (count($config) > 0) {
			$fees = ((floatval($totalcost) * $config['aquantuoFees']) / 100);
		}
		return $fees;
	}
	public function getDistanceBetweenPointsNew($latitude1, $longitude1, $latitude2, $longitude2, $unit = 'Mi') {
		$theta = $longitude1 - $longitude2;
		$distance = (sin(deg2rad($latitude1)) * sin(deg2rad($latitude2))) + (cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * cos(deg2rad($theta)));
		$distance = acos($distance);
		$distance = rad2deg($distance);
		$distance = $distance * 60 * 1.1515;switch ($unit) {
		case 'Mi':break;case 'Km':$distance = $distance * 1.609344;
		}
		return (round($distance, 2));
	}

	public static function update_status($requestid) {
		$status = '';
		$info = Deliveryrequest::where('_id', $requestid)->first();
		if (count($info) > 0) {
			if (in_array(@$info->RequestType, ['online', 'buy_for_me'])) {
				foreach ($info->ProductList as $key) {
					if (in_array($key['status'], ['cancel', 'cancel_by_admin']) &&
						!in_array($status, ['pending', 'ready', 'reviewed', 'not_purchased', 'paid', 'purchased','item_received','assign','out_for_pickup','out_for_delivery','accepted'])) {

						$status = $info->Status = $key['status'];
					} else if ($key['status'] == 'delivered' &&
						!in_array($status, ['pending', 'ready', 'reviewed', 'not_purchased', 'paid', 'purchased','item_received','assign','out_for_pickup','out_for_delivery','accepted'])) {

						$status = $info->Status = 'delivered';
					}else if ($key['status'] == 'out_for_delivery' &&
						!in_array($status, ['pending', 'ready', 'reviewed', 'not_purchased', 'paid', 'purchased','item_received','assign','out_for_pickup','accepted'])) {

						$status = $info->Status = 'out_for_delivery';
					} else if ($key['status'] == 'out_for_pickup'
						&& !in_array($status, ['pending', 'ready', 'reviewed', 'not_purchased', 'paid', 'purchased','item_received','assign','accepted'])) {
						$status = $info->Status = 'out_for_pickup';
					} else if ($key['status'] == 'accepted'
						&& !in_array($status, ['pending', 'ready', 'reviewed', 'not_purchased', 'paid', 'purchased','item_received','assign'])) {
						$status = $info->Status = 'accepted';
					} else if ($key['status'] == 'assign' &&
						!in_array($status, ['pending', 'ready', 'reviewed', 'not_purchased', 'paid', 'purchased','item_received'])) {
						$status = $info->Status = 'assign';
					}else if ($key['status'] == 'item_received' &&
						!in_array($status, ['pending', 'ready','reviewed', 'not_purchased', 'paid','purchased'])) {
						$status = $info->Status = 'item_received';
					}else if ($key['status'] == 'purchased' &&
						!in_array($status, ['pending', 'ready','reviewed', 'not_purchased', 'paid'])) {
						$status = $info->Status = 'purchased';
					}else if ($key['status'] == 'paid' &&
						!in_array($status, ['pending', 'ready', 'reviewed', 'not_purchased'])) {
						$status = $info->Status = 'paid';
					}else if ($key['status'] == 'not_purchased' &&
						!in_array($status, ['pending', 'ready','reviewed'])) {

						$status = $info->Status = 'not_purchased';
					}else if ($key['status'] == 'reviewed' &&
						!in_array($status, ['pending', 'ready'])) {
						$status = $info->Status = 'reviewed';
					}else if ($key['status'] == 'ready' && !in_array($status, ['pending'])) {
						$status = $info->Status = 'ready';
					} else if ($key['status'] == 'pending') {
						$status = $info->Status = 'pending';
					}
					$info->save();
				}

			}
		}
		return $status;
	}

	public static function update_status_old($requestid) {
		$status = '';
		$info = Deliveryrequest::where('_id', $requestid)->first();
		if (count($info) > 0) {
			if (in_array(@$info->RequestType, ['online', 'buy_for_me'])) {
				foreach ($info->ProductList as $key) {
					if (in_array($key['status'], ['cancel', 'cancel_by_admin']) &&
						!in_array($status, ['pending', 'ready', 'accepted', 'assign', 'out_for_pickup', 'out_for_delivery', 'delivered', 'purchased', 'not_purchased'])) {

						$status = $info->Status = $key['status'];
					} else if ($key['status'] == 'delivered' &&
						!in_array($status, ['pending', 'ready', 'accepted', 'assign', 'out_for_pickup', 'out_for_delivery','purchased'])) {

						$status = $info->Status = 'delivered';
					} else if ($key['status'] == 'out_for_delivery' &&
						!in_array($status, ['pending', 'ready', 'accepted', 'assign', 'out_for_pickup','purchased'])) {

						$status = $info->Status = 'out_for_delivery';
					} else if ($key['status'] == 'out_for_pickup'
						&& !in_array($status, ['accepted', 'assign', 'pending', 'ready'])) {

						$status = $info->Status = 'out_for_pickup';
					} else if ($key['status'] == 'assign' &&
						!in_array($status, ['pending', 'ready', 'accepted', 'reviewed', 'not_purchased', 'paid', 'purchased'])) {
						$status = $info->Status = 'assign';
					} else if ($key['status'] == 'purchased' &&
						!in_array($status, ['pending', 'ready', 'accepted', 'reviewed', 'not_purchased', 'paid'])) {
						$status = $info->Status = 'purchased';
					} else if ($key['status'] == 'item_received' &&
						!in_array($status, ['pending', 'ready', 'accepted', 'reviewed', 'not_purchased', 'paid'])) {
						$status = $info->Status = 'item_received';
					} else if ($key['status'] == 'paid' &&
						!in_array($status, ['pending', 'ready', 'accepted', 'reviewed', 'not_purchased'])) {
						$status = $info->Status = 'paid';
					} else if ($key['status'] == 'not_purchased' &&
						!in_array($status, ['pending', 'ready', 'accepted', 'reviewed'])) {

						$status = $info->Status = 'not_purchased';
					} else if ($key['status'] == 'reviewed' &&
						!in_array($status, ['pending', 'ready', 'accepted'])) {

						$status = $info->Status = 'reviewed';
					} else if ($key['status'] == 'accepted' && !in_array($status, ['pending', 'ready'])) {

						$status = $info->Status = 'accepted';
					} else if ($key['status'] == 'ready' && !in_array($status, ['pending'])) {

						$status = $info->Status = 'ready';
					} else if ($key['status'] == 'pending') {
						$status = $info->Status = 'pending';
					}

				}
				$info->save();
			}
		}
		return $status;
	}

	public static function update_status2($requestid) {
		$status = '';
		$info = Deliveryrequest::where('_id', $requestid)->first();
		if (count($info) > 0) {
			if (in_array(@$info->RequestType, ['delivery', 'local_delivery'])) {
				foreach ($info->ProductList as $key) {
					if (in_array($key['status'], ['cancel', 'cancel_by_admin']) &&
						!in_array($status, ['pending', 'ready', 'assign', 'accepted', 'out_for_pickup', 'out_for_delivery', 'delivered', 'purchased', 'not_purchased'])) {
						$status = $info->Status = $key['status'];
					} else if ($key['status'] == 'delivered' &&
						!in_array($status, ['pending', 'ready', 'reviewed', 'not_purchased', 'purchased', 'item_received', 'assign', 'accepted', 'out_for_pickup', 'out_for_delivery'])) {
						$status = $info->Status = $key['status'];
					} else if ($key['status'] == 'out_for_delivery' &&
						!in_array($status, ['pending', 'ready', 'reviewed', 'not_purchased', 'purchased', 'item_received', 'assign', 'accepted', 'out_for_pickup', 'shipment_departed'])) {

						$status = $info->Status = 'out_for_delivery';
					} else if ($key['status'] == 'out_for_pickup' &&
						!in_array($status, ['pending', 'ready', 'reviewed', 'not_purchased', 'purchased', 'item_received', 'assign', 'accepted'])) {

						$status = $info->Status = 'out_for_pickup';
					} else if ($key['status'] == 'shipment_departed' && !in_array($status, ['pending', 'ready', 'reviewed', 'not_purchased', 'purchased', 'item_received', 'assign', 'accepted', 'out_for_pickup'])) {
						$status = $info->Status = $key['status'];
					} else if ($key['status'] == 'accepted' &&
						!in_array($status, ['pending', 'ready', 'reviewed', 'not_purchased', 'purchased', 'item_received', 'assign'])) {

						$status = $info->Status = 'accepted';
					} else if ($key['status'] == 'assign' &&
						!in_array($status, ['pending', 'ready', 'reviewed', 'not_purchased', 'purchased', 'item_received'])) {

						$status = $info->Status = 'assign';
					} else if ($key['status'] == 'item_received' &&
						!in_array($status, ['pending', 'ready', 'reviewed', 'not_purchased', 'purchased'])) {

						$status = $info->Status = 'item_received';
					} else if ($key['status'] == 'purchased' &&
						!in_array($status, ['pending', 'ready', 'reviewed', 'not_purchased'])) {

						$status = $info->Status = 'purchased';
					} else if ($key['status'] == 'not_purchased' &&
						!in_array($status, ['pending', 'ready', 'reviewed'])) {

						$status = $info->Status = 'not_purchased';
					} else if ($key['status'] == 'reviewed' &&
						!in_array($status, ['pending', 'ready'])) {

						$status = $info->Status = 'reviewed';
					} else if ($key['status'] == 'ready' &&
						!in_array($status, ['pending'])) {

						$status = $info->Status = 'ready';
					} else if ($key['status'] == 'pending') {
						$status = $info->Status = 'pending';
					}
				}
				$info->save();
			}

		}
		return $status;
	}

	public static function send_notification_to_tp($option) {
		$option = (Object) $option;
		$info = Deliveryrequest::find($option->id);
		if (count($info) > 0) {
			$notification = [
				"NotificationTitle" => trans('lang.NEW_REQUEST'),
				"NotificationShortMessage" => "Request for package \"{$info->ProductTitle}\" has been ready to deliver.",
				"NotificationMessage" => sprintf(trans('lang.MSG_NEW_REQUEST'), $info->ProductTitle, $info->PickupFullAddress, $info->DeliveryFullAddress),
				"NotificationType" => "",
				"NotificationUserId" => [],
				"Date" => new MongoDate(),
				"GroupTo" => "User",
			];
			if (@$option->request_type == 'update') {
				$notification['NotificationTitle'] = 'Package is available for delivery';
			}

			$user = User::query();
			//$user->where(['TransporterStatus'=>'active','TPSetting'=>'on'])->where('_id','!=',)
			$user->select('NotificationId', 'DeviceType', 'NoficationStatus', 'TPSetting');

			if (empty(trim($info->TripId))) {
				$user->where('_id', '!=', $info->RequesterId)
					->where(['TransporterStatus'=>'active'])
					->whereIn('UserType', ['both', 'transporter'])
					->whereIn('TransporterType', ['individual', 'business'])
					->where(['TPSetting' => 'on']);
			} else {
				$notification['NotificationTitle'] = 'Trip Request';
				$notification['NotificationMessage'] = sprintf('"%s" is intrested to send their package "%s" from %s to $s', $info->RequesterName, $info->ProductTitle, $info->PickupFullAddress, $info->DeliveryFullAddress);

				if (@$option->request_type == 'update') {
					$notification['NotificationTitle'] = 'Trip request updated';
				}

				$user->where([
					'_id' => $info->TransporterId,
					'TPSetting' => 'on',
				]);
			}

			$userdata = $user->get();
			if (count($userdata) > 0) {
				$userids = [];
				$notify = new Notify();

				$notify->setValue('title', $notification['NotificationTitle']);
				$notify->setValue('message', $notification['NotificationMessage']);
				$notify->setValue('location', 'transporter_delivery_detail');
				$notify->setValue('locationkey', (String) $info->_id);

				foreach ($userdata as $tp) {
					$notification['NotificationUserId'][] = new MongoId($tp->_id);
					if ($tp['NoficationStatus'] == 'on') {
						$notify->add_user($tp->NotificationId, $tp->DeviceType);
					}
				}
				$notify->fire();

				Notification::insert($notification);

			}

		}
	}
	
}
