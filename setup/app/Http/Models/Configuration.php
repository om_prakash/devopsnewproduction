<?php 
namespace App\Http\Models;

use Jenssegers\Mongodb\Model as Eloquent;

class Configuration extends Eloquent {

    protected $collection = 'configuration';

}

