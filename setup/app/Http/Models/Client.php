<?php 
namespace App\Http\Models;


use Jenssegers\Mongodb\Model as Eloquent;

class Client extends Eloquent {

    protected $collection = 'client';

}

