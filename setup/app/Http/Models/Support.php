<?php 
namespace App\Http\Models;

use Jenssegers\Mongodb\Model as Eloquent;

class Support extends Eloquent {

    protected $collection = 'support';

}

