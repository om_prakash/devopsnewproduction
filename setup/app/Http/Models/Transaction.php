<?php namespace App\Http\Models;

use Jenssegers\Mongodb\Model as Eloquent;

class Transaction extends Eloquent {

    protected $collection = 'transaction';

}

