<?php namespace App\http\models;

use Jenssegers\Mongodb\Model as Eloquent;

class SlipManagement extends Eloquent
{

    protected $collection = 'slip_management';

}
