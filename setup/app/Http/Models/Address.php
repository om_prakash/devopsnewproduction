<?php 
namespace App\Http\Models;

use Jenssegers\Mongodb\Model as Eloquent;

class Address extends Eloquent {

    protected $collection = 'address';

}

