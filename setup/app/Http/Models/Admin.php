<?php namespace App\Http\Models;


use Jenssegers\Mongodb\Model as Eloquent;

class Admin extends Eloquent {

    protected $collection = 'admin';	
		
}
