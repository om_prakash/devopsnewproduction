<?php 
namespace App\Http\Models;

use Jenssegers\Mongodb\Model as Eloquent;

class Appcontent extends Eloquent {

    protected $collection = 'app_content';

}

