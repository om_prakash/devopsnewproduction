<?php 
namespace App\Http\Models;

use Jenssegers\Mongodb\Model as Eloquent;

class Notification extends Eloquent {

    protected $collection = 'notification';

}

