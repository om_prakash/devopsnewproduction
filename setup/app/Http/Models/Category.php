<?php 
namespace App\Http\Models;

use Jenssegers\Mongodb\Model as Eloquent;

class Category extends Eloquent {

    protected $collection = 'category';

}

