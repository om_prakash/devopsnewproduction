<?php 
namespace App\Modules\Admin\Models\Calculation;

use Jenssegers\Mongodb\Model as Eloquent;

class Calculation extends Eloquent {

    protected $collection = 'calculation';

}

