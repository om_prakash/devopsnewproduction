<?php 
namespace App\Http\Models;

use Jenssegers\Mongodb\Model as Eloquent;

class Item extends Eloquent {

    protected $collection = 'item';

}

