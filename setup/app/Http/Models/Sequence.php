<?php namespace App\Http\Models;

use Jenssegers\Mongodb\Model as Eloquent;

class Sequence extends Eloquent {

    protected $collection = 'counters';	
		
}