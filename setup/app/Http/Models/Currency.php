<?php 
namespace App\Http\Models;

use Jenssegers\Mongodb\Model as Eloquent;

class Currency extends Eloquent {

    protected $collection = 'currency';

}

