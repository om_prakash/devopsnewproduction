<?php 
namespace App\Http\Models;

use Jenssegers\Mongodb\Model as Eloquent;

class SendMail extends Eloquent {

    protected $collection = 'sendmail';

}