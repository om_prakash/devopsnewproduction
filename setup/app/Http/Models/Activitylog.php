<?php 
namespace App\Http\Models;

use Jenssegers\Mongodb\Model as Eloquent;

class Activitylog extends Eloquent {

    protected $collection = 'activitylog';

}

