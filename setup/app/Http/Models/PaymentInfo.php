<?php 
namespace App\Http\Models;

use Jenssegers\Mongodb\Model as Eloquent;

class PaymentInfo extends Eloquent {

    protected $collection = 'payment_info';

}