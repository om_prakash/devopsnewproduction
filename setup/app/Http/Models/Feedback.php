<?php namespace App\Modules\Admin\Models\Feedback;

use Jenssegers\Mongodb\Model as Eloquent;

class Feedback extends Eloquent {

    protected $collection = 'feedback';

}

