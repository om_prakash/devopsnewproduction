<?php 
namespace App\Http\Models;

use Jenssegers\Mongodb\Model as Eloquent;

class Comment extends Eloquent {

    protected $collection = 'comment';

}

