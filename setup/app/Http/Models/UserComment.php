<?php 
namespace App\Http\Models;

use Jenssegers\Mongodb\Model as Eloquent;

class UserComment extends Eloquent {

    protected $collection = 'user_comment';

}

