<?php 
namespace App\Modules\Admin\Models\LocalNotificationTime;

use Jenssegers\Mongodb\Model as Eloquent;

class LocalNotificationTime extends Eloquent {

    protected $collection = 'local_notification_time';

}

