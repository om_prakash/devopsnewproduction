<?php namespace App\Http\Models;

use Jenssegers\Mongodb\Model as Eloquent;

class RequestCard extends Eloquent {

	protected $collection = 'request_cards';

}