<?php 
namespace App\Http\Models;

use Jenssegers\Mongodb\Model as Eloquent;

class AppTutorial extends Eloquent {

    protected $collection = 'app_tutorial';

}

