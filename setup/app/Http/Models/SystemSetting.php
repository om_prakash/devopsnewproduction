<?php 
namespace App\Modules\Admin\Models\SystemSetting;

use Jenssegers\Mongodb\Model as Eloquent;

class SystemSetting extends Eloquent {

    protected $collection = 'system_setting';

}

