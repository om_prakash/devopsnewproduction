<?php 
namespace App\Http\Models;

use Jenssegers\Mongodb\Model as Eloquent;

class Webcontent extends Eloquent {

    protected $collection = 'web_content';

}

