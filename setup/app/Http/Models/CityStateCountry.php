<?php 
namespace App\Http\Models;

use Jenssegers\Mongodb\Model as Eloquent;

class CityStateCountry extends Eloquent {

    protected $collection = 'city_state_country';

}

