<?php
namespace App\Traits;

use App\Http\Models\Setting;

trait AdminTrait
{
    public function getAqLatLong()
    {
        $result = Setting::where('_id', '563b0e31e4b03271a097e1ca')
            ->select('AqLatlong')->first();

        return (Object) [
            'aqlat' => isset($result['AqLatlong'][1]) ? $result['AqLatlong'][1] : '',
            'aqlong' => isset($result['AqLatlong'][0]) ? $result['AqLatlong'][0] : '',
        ];
    }
}
