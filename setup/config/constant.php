<?php

$baseURL = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http");
if (isset($_SERVER['HTTP_HOST'])) {
    $host = $_SERVER['HTTP_HOST'];
    $baseURL .= "://" . $host;
}
$baseURL .= str_replace(basename($_SERVER['SCRIPT_NAME']), "", $_SERVER['SCRIPT_NAME']);

define('ImageUrl', $baseURL . 'upload/');
define('BASEURL_FILE', '/var/www/html/upload/');
define('THEME', $baseURL . 'theme/');
define('NEWPATH', $baseURL . 'upload/');

// //file : app/config/constants.php
// define('CARRIER', 2);
// define('SENDER', 4);
// define('ADMIN', 8);
// define('APP_CONTENT', 16);
// define('EMAIL_TEMPLATE', 32);
// define('WEB_CONTENT', 64);
// define('APP_TUTORIAL', 128);
// define('ABOUT_APP', 256);
// define('FAQ', 512);
// define('TRANSACTION', 1024);
// define('COUNTRY', 2048);
// define('STATE', 4096);
// define('CITY', 8192);

// define('VEHICLE', 16384);
// define('PACKAGE', 32768);
// define('FEEDBACK', 65536);
// define('SMALL_PACKAGE_CONF', 131072);
// define('BIG_PACKAGE_CONF', 262144);
// define('PLANE_SMALL_PACKAGE_CONF', 524288);
// define('PLANE_BIG_PACKAGE_CONF', 1048576);
// define('CONFIGURATION', 2097152);
// define('SETTING', 4194304);
// define('PROMOCODE', 8388608);
// define('INDIVIDUAL_TRIP', 16777216);
// define('BUSSINESS_TRIP', 33554432);
// define('TRANSACTION2', 67108864);
// define('ITEMCATEGORY', 134217728);
// define('NOTIFICATION', 268435456);
// define('AQUANTUO_ADDRESS', 536870912);
// define('SUPPORT', 1073741824);
// define('ONLINEPACKAGE', 2147483648);
// define('BUYFORME', 4294967296);
// define('SECTION', 8589935592);
// define('SLIPMANAGEMENT',17179871184);

// $baseURL = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http");
// //$baseURL .= "://" . $_SERVER['HTTP_HOST'];
// $baseURL .= "://aquantuo.com";

// $baseURL .= str_replace(basename($_SERVER['SCRIPT_NAME']), "", $_SERVER['SCRIPT_NAME']);

// define('ImageUrl', $baseURL . 'upload/');
//define('BASEURL_FILE', BASE_PATH . 'upload/');
// define('BASEURL_FILE','/var/www/html/upload/');


// define('THEME', $baseURL . 'theme/');
// define('NEWPATH', $baseURL . 'upload/');

//file : app/config/constants.php
define('CARRIER', 2);
define('SENDER', 4);
define('ADMIN', 8);
define('APP_CONTENT', 16);
define('EMAIL_TEMPLATE', 32);
define('WEB_CONTENT', 64);
define('APP_TUTORIAL', 128);
define('ABOUT_APP', 256);
define('FAQ', 512);
define('TRANSACTION', 1024);
define('COUNTRY', 2048);
define('STATE', 4096);
define('CITY', 8192);

define('VEHICLE', 16384);
define('PACKAGE', 32768);
define('FEEDBACK', 65536);
define('SMALL_PACKAGE_CONF', 131072);
define('BIG_PACKAGE_CONF', 262144);
define('PLANE_SMALL_PACKAGE_CONF', 524288);
define('PLANE_BIG_PACKAGE_CONF', 1048576);
define('CONFIGURATION', 2097152);
define('SETTING', 4194304);
define('PROMOCODE', 8388608);
define('INDIVIDUAL_TRIP', 16777216);
define('BUSSINESS_TRIP', 33554432);
define('TRANSACTION2', 67108864);
define('ITEMCATEGORY', 134217728);
define('NOTIFICATION', 268435456);
define('AQUANTUO_ADDRESS', 536870912);
define('SUPPORT', 1073741824);
define('ONLINEPACKAGE', 2147483648);
define('BUYFORME', 4294967296);
define('SECTION', 8589935592);
define('SLIPMANAGEMENT',17179871184);


define('OtherCategory','568e2f7ccf3207975ae0d8be');
define('ElectronicsCategory','5650140b6734c4af698b4567');
define('CongigId','5673e33e6734c4f874685c84');
define('ConfigId','5673e33e6734c4f874685c84');

define('AUTO_FULLSIZE_SUV', '56d8fdffcf3207063869c999');
define('AUTO_INTERMEDIATE_SUV', '56d8fe2acf3207673069c999');
define('AUTO_SEDAN_ECONOMY', '56ee584ccf32071c0d52561f');
define('AUTO_SEDAN_FULLSIZE', '56ee58a2cf32071c0d525620');
define('AUTO_SEDAN_INTERMEDIATE', '56ee5869cf3207d471525620');
define('AUTO_STANDARD_SUV', '56d8fe15cf3207bb2b69c999');
define('AUTOMOBILE_VAN', '568e2e12cf3207d25ae0d8bd');