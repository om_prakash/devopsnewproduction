<?php

return array(

    'appNameIOS'     => array(
        'environment' =>'production',
        'certificate' => app_path().'/Notification/Aquantuo_Production.pem',
        'passPhrase'  =>'123456',
        'service'     =>'apns'
    ),
    'appNameAndroid' => array(
        'environment' =>'production',
        'apiKey'      =>'AIzaSyDLfrFFYjApPUkSkup2YNoAOwLLwa3ILgg',
        'service'     =>'gcm'
    )

);