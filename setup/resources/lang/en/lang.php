<?php
/*
Start with
N = Notification
Second
 */

return [
	'NEW_REQUEST' => 'New request for delivery',
	'MSG_NEW_REQUEST' => 'A request for package "%s" is ready from %s to %s.',
	'ADMIN_NEW_REQUEST' => 'New request of Send a package',
	'ADMIN_MSG_NEW_REQUEST' => 'The send a package request titled:"%s", ID:%s has created.',
	'REQUEST_DELIVERED' => 'Package has been successfully delivered.',
	'MSG_REQUEST_DELIVERED' => 'Transporter "%s" has delivered your package "%s".',
	'ACCEPT_REQUEST' => 'Your request was accepted',
	'MSG_ACCEPT_REQUEST' => 'Transporter "%s" has accepted your delivery request "%s".',
	//Buy for me
	"BUYFORME_REQUEST_CREATED_TITLE" => 'New Buy for me request created',
	"BUYFORME_REQUEST_CREATED" => 'The buy for me request titled:"%s", ID:%s has been created.',
	//Buy for me -out for pickup
	"BUY_OUTFORPICKUP_TITLE" => 'Item is in destination country going through customs and sorting.',
	"BUY_OUTFORPICKUP_MSG" => 'Unless you shipped your item to Aquantuo or the Transporter, please have it ready. The Transporter is out to pick up your package or to meet at the agreed upon location.',
	"BUY_OUTFORPICKUP_MSG_ADMIN" => 'The transporter is out to pickup the request titled: %s, ID: %s',
	//Buy for me -out for delivery
	"BUY_OUTFORDELIVERY_TITLE" => 'Your package is en route to be delivered.',
	"BUY_OUTFORDELIVERY_MSG" => 'Transporter is en route to deliver your item titled:%s, ID: %s.',
	"BUY_OUTFORDELIVERY_MSG_ADMIN" => 'Transporter has picked up the package and is out for delivery. Item titled: %s, ID: %s to %s',
	"BUY_UPDATE_TITLE" => 'Item Updated',

	"BUY_UPDATE_MSG" => 'Your all item Item has been purchased. Please process to pay for next process.',
	//Buy for me - request cancel
	"BUYFORME_CANCELED_TITLE" => 'Your request has been canceled',
	"BUYFORME_CANCELED_MSG_IPAY" => 'We regret to inform you, your package delivery, %s was not delivered. Aquantuo is continuing to look into the issue. Please contact us at %s should you have any questions',
	"BUYFORME_CANCELED_MSG_CRE" => 'Your Package "%s" failed delivery at the specified address. Your package would be returned by to the address specified at the time of the post. Additional fees may apply. Carrier Message: %s',
	"BUYFORME_CANCELED_MSG_OSCRE" => 'Your Package "%s" has been canceled by "%s". Carrier Message: %s',
	"BUYFORME_COMPLETED_TITLE" => 'Package has been successfully delivered',
	"BUYFORME_COMPLETED_MSG" => 'You have delivered package successfully..',
	"BUYFORME_COMPLETED_MSG_REQ" => 'Transporter "%s" has delivered your package "%s".',

	"BUYFORME_COMPLETED_TITLE_ADMIN" => 'Your package has been delivered.',
	"BUYFORME_COMPLETED_MSG_ADMIN" => 'Transporter "%s" has dropped off PackageID: "%s" at drop off location.',

	//Online request
	"ONLINE_REQUEST_CREATED_TITLE" => 'Online purchase request',
	"ONLINE_REQUEST_CREATED_MSG" => 'The online request titled:"%s", ID:%s has been created.',
	//online request -out for pickup
	"ONLINE_OUTFORPICKUP_TITLE" => 'Transporter is out for pickup',
	"ONLINE_OUTFORPICKUP_MSG" => 'Unless you shipped your item to Aquantuo or the Transporter, please have it ready. The Transporter is out to pick up your package or to meet at the agreed upon location.',
	"ONLINE_OUTFORPICKUP_ADMIN" => 'The Transporter is out to pickup package to be delivered.Package: %s, ID: %s',
	//Online out for delivery
	"ONLINE_OUTFORDELIVERY_TITLE" => 'Your package is en route to be delivered.',
	"ONLINE_OUTFORDELIVERY_MSG" => 'Transporter is en route to deliver your item titled:%s, ID: %s.',
	"ONLINE_OUTFORDELIVERY_ADMIN" => 'Transporter has picked up the package and is out to deliver the item titled: %s, ID: %s to the %s',
	//onlie delivered
	"ONLINE_DELIVERED_USER_TITLE" => 'Package has been successfully delivered',
	"ONLINE_DELIVERED_ADMIN_TITLE" => 'Package delivered',
	"ONLINE_DELIVERED_USER_MSG" => 'Transporter "%s" has delivered your package "%s"',
	"ONLINE_DELIVERED_USER_ADMIN" => 'Transporter "%s" has dropped off PackageID: "%s" at the drop off location.',
	"ONLINE_CANCELED" => 'Package canceled',
	"ONLINE_CANCELED_MSG" => 'Your package "%s" has been canceled by "%s"',
	//Send package
	"SEND_CREATE_TITLE" => 'New request of Send a package',
	"SEND_CREATE_MSG" => 'The send a package request titled:"%s", ID:%s has been created.',
	"SEND_ACCEPT_TITLE" => 'Your request was accepted',
	"SEND_ACCEPT_MSG" => 'Transporter "%s" has accepted your delivery request "%s"',
	"SEND_ACCEPT_MSG_ADMIN" => 'Transporter "%s" has accepted request to deliver from %s to %s.',
	"SEND_OUTFORPICKUP_TITLE" => 'Item is in destination country going through customs and sorting',
	"SEND_OUTFORPICKUP_MSG" => 'Unless you shipped your item to Aquantuo or the Transporter, please have it ready. The Transporter is out to pick up your package or to meet at the agreed upon location.',
	"SEND_OUTFORPICKUP_ADMIN" => 'Transporter "%s" is out to pickup the package for delivery from "%s" to "%s".',
	"SEND_OUTFORDELIVERY_TITLE" => 'Your package is en route to be delivered',
	"SEND_OUTFORDELIVERY_MSG" => 'Transporter is en route to deliver your item titled:%s, ID: %s',
	"SEND_OUTFORDELIVERY_ADMIN" => 'Transporter has picked up the package and is out to deliver the item titled: %s, ID: %s to the delivery address.',
	//Send package
	"SEND_DELIVERED_TITLE_ADMIN" => 'Package has been successfully delivered.',
	"SEND_DELIVERED_ADMIN" => 'Transporter "%s" has dropped off PackageID: "%s" at the drop off location.',
	"SEND_CANCELED" => 'Package canceled',
	"SEND_CANCELED_MSG" => 'Your package "%s" has been canceled by "%s"',
	//Account
	"ACCOUNT_VERIFY_TITLE" => 'Account verify',
	"ACCOUNT_VERIFY" => 'Your transporter account has been verified, please logout and login again to access transporter features.',
	"ACCOUNT_DEACTIVATE_TITLE" => 'Profile deactivated',
	"ACCOUNT_DEACTIVATE" => 'Your transporter profile has been deactivated, please contact us at support@aquantuo.com',

	//Admin message
	//Buy for me
	"ITEM_REVIEW_TITLE" => 'Listing Reviewed',
	"ITEM_REVIEW_MEG_BUY" => 'Aquantuo has reviewed your Buy for me request for "%s"',
	"ITEM_REVIEW_MEG_ONLINE" => 'Aquantuo has reviewed your online request for "%s"',
	"ITEM_PURCHASED_TITLE" => 'Item Purchased',
	"ITEM_PURCHASED_MSG" => 'Aquantuo has purchased your requested item: "%s".',
	"ITEM_PURCHASED_MSG2" => 'Aquantuo has purchased your requested item(s): "%s".',
	"ITEM_PURCHASED" => 'Your item, %s has been purchased. Please proceed for the next steps.',
	"ITEM_REMINDER_TITLE" => 'Pending payment',
	"ITEM_REMINDER_MSG" => 'Additional payment is required on your item prior to purchase.',
	//"ITEM_ASSIGN_TP_USER"=>'Transporter has been assigned to your request titled: "%s", ID: %s.',

	"ITEM_ASSIGN_TP_USER" => 'Transporter has been assigned to your request titled: "%s".',

	"ITEM_ASSIGN_TP_TITLE" => 'Transporter Assigned.',
	// "ITEM_ASSIGN_TP_TRP"=>'You have been assigned as a Transporter for: %s, ID: %s.',
	"ITEM_ASSIGN_TP_TRP" => 'You have been assigned as a Transporter for: %s.',

	"ITEM_RECEIVED" => 'Item Received',
	"ITEM_RECEIVED_MSG2" => 'Aquantuo has received your requested item(s): "%s".',
	"ITEM_RECEIVED_MSG" => 'Aquantuo has received your requested item: "%s".',
	//Online
	"ONLINE_REVIEWED_TITLE" => 'Request reviewed',
	"ONLINE_REVIEWED_MSG" => 'Aquantuo has reviewed your online request for "%s"',
	"ONLINE_RECEIVED_TITLE" => 'Request received',
	"ONLINE_RECEIVED_MSG" => 'Aquantuo has received your online request for package %s.',
	"ONLINE_ASSIGN_TP_TITLE" => 'Transporter Assigned.',
	//"ONLINE_ASSIGN_TP_USER"=>'A Transporter has been assigned to your request titled: %s, ID: %s.',
	"ONLINE_ASSIGN_TP_USER" => 'A Transporter has been assigned to your request titled: "%s".',

	//"ONLINE_ASSIGN_TP_TRP"=>'You have been assigned as transporter for: %s, ID: %s.',
	"ONLINE_ASSIGN_TP_TRP" => 'You have been assigned as transporter for: %s.',
	"ONLINE_ASSIGN_TP_ADMIN" => 'The package for %s, has been assigned to Transporter. Package is %s.',
	'SHIPMENT_DEPARTED_TITLE' => 'Shipment Departed',
	'SHIPMENT_DEPARTED_MSG' => 'Shipment departed originating country.',
];
