//ation for prepare request
//prf= buy for me form
//document.getElementsByName('buy_for_me_form')[0].reset();
var prf_step = 0,
	pickup_lat = 0,
	pickup_long = 0,
	dropoff_lat = 0,
	dropoff_long = 0,
	total_amount = 0,
	ghana_total_amount = 0,
	request_has_error = 0,
	distance = 0,
	olp_discount = 0,
	shipping_cost = 0,
	olp_formated_currency = '';

new Validate({
	FormName: 'buy_for_me_form',
	ErrorLevel: 1,
	validateHidden: false,
	callback: function() {

		try {
			if (prf_step == 0) {
				$("#calculate_loader").addClass("spinning");

				$.ajax({
					url: SITEURL + "online-purchase-calculation",
					data: $('#buy_for_me_form').serialize(),
					method: 'post',
					dataType: "json",
					success: function(res) {

						prf_step = 1;
						$('#step2-header').addClass('selected');
						$("#calculate_loader").removeClass("spinning");
						request_has_error = res.success;
						$('#shipping_detail').html(res.html);
						prf_step = 3;
						total_amount = res.total_amount;
						shipping_cost = res.shipping_cost;
						ghana_total_amount = res.ghana_total_amount;
						distance = res.distance;
						$('#sec2').hide();
						$('#sec3').show();
						$('#step3-header').addClass('selected');
						$('#total_amount_div').html(parseFloat(res.total_amount).toFixed(2));
						$('#ghana_total_amount').html(parseFloat(res.ghana_total_amount).toFixed(2));
						scroll_to('online_start_position');
					}
				});



			} else if (prf_step == 3) {
				$("#creating_req_btn").addClass("spinning");
				$('#step2-header').addClass('selected');
				$('#distance').val(distance);
				$('#promo_code_olp_promo').val($('#promocode').val());
				$('#olp_discount').val(olp_discount);
				$.ajax({
					url: SITEURL + "post-online-purchase",
					method: 'post',
					data: $('#buy_for_me_form').serialize(),
					dataType: "json",
					success: function(res) {
						$("#creating_req_btn").removeClass("spinning");
						alert(res.msg);
						if (res.success == 1) {
							document.location.href = res.redirect_to;
						}
					}
				});

			}
		} catch (e) {
			console.log(e);
		}
	}
});

function switch_html(showid, hideid) {
	$(showid).show();
	$(hideid).hide();
}

function switch_request_header(showid, hideid, section) {
	prf_step = section;
	for (var i = 1; i <= 3; i++) {
		if (i <= section) {
			$('#step' + i + '-header').addClass('selected');
		} else {
			$('#step' + i + '-header').removeClass('selected');
		}
	}
	switch_html(showid, hideid);
	scroll_to('online_start_position');

}

function parse_json($string) {
	return eval('(' + $string + ')');
}

function toggle_html(showid, hideid) {
	$('#package_category').val('');
	switch_html(showid, hideid)
}

function toggle_category(showid, hideid, id) {
	$(id).val('');
	$(showid).attr('disabled', false);
	$(showid).show();
	$(hideid).attr('disabled', true);
	$(hideid).hide();
}
toggle_category('.travel-mode-air', '.travel-mode-ship');

function check_promocode($inputfield, shipping_cost, success_div, total_amount) {
	$('#er_' + $inputfield).html('');
	if ($('#' + $inputfield).val() == '') {
		$('#er_' + $inputfield).html('Please enter promocode.');
		return false;
	}

	$('#' + success_div + '_input').addClass('spinning');
	$.ajax({
		url: SITEURL + "validate_promocode",
		data: {
			promocode: $('#' + $inputfield).val(),
			shipping_cost: shipping_cost,
			total_amount: total_amount
		},
		method: 'post',
		dataType: "json",
		success: function(res) {

			$('#' + success_div + '_input').removeClass('spinning');
			if (res.success == 1) {
				olp_discount = res.result.discount;
				$('#' + success_div).show();
				$('#' + success_div + '_input').hide();
				$('#' + success_div + '_msg').html(res.msg);
				$('#' + success_div + '_payable').html('$' + (total_amount - res.result.discount).toFixed(2));
				$('#' + success_div + '_payable_ghana').html(res.result.GhanaTotalCost);


			} else {
				$('#er_' + $inputfield).html(res.msg);
				$('#' + success_div + '_input').show();
			}

		}
	});

}


function remove_promocode(success_div, inputfield) {
	olp_discount = 0;
	$('#' + inputfield).val('');
	$('#' + success_div).hide();
	$('#' + success_div + '_input').show();
	$('#' + success_div + '_payable').html('$' + total_amount.toFixed(2));
	$('#' + success_div + '_payable_ghana').html($('#' + success_div + '_ghana').html());
}

function scroll_to(id) {
	$('html, body').animate({
		scrollTop: $('#' + id).offset().top
	}, 'slow');
}

function image_preview(obj, previewid, evt) {
	var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
	if ($.inArray($(obj).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
		$(obj).val('');
		$('#' + previewid).attr('src', 'user-no-image.jpg');
		alert("Only " + fileExtension.join(', ') + " formats are allowed.");
	} else {

		var file = evt.target.files[0];

		if (file) {
			var reader = new FileReader();

			reader.onload = function(e) {
				$('#' + previewid).attr('src', e.target.result)
			};
			reader.readAsDataURL(file);
		}
	}
}