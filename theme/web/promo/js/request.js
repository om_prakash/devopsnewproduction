//validation for prepare request
//prf= prepare request form
//document.getElementsByName('prepare_request_form')[0].reset();
var prf_step = 0,
	pickup_lat = 0,
	pickup_long = 0,
	dropoff_lat = 0,
	dropoff_long = 0,
	total_amount = 0,
	ghana_total_amount = 0,
	request_has_error = 0,
	distance = 0,
	shipping_cost = 0,
	pr_formated_currency = '';

new Validate({
	FormName: 'prepare_request_form',
	ErrorLevel: 1,
	validateHidden: false,
	callback: function() {

		try {
			if (prf_step == 0) {
				prf_step = 1;
				$('#sec1').hide();
				$('#sec2').show();
				$('#step2-header').addClass('selected');
				scroll_to('send_a_package_start_position');
			} else if (prf_step == 1) {
				// getting pickup addres lat long


				$('#step2-next-btn').addClass('spinning');
				var geocoder = new google.maps.Geocoder();
				var pickup_address = '';
				var dropoff_address = '';

				var pickupstate = parse_json($('#pp_pickup_state').val());
				if (typeof pickupstate !== 'object') {
					pickupstate = {
						"id": "",
						"name": ""
					};
				}


				var pickupcountry = parse_json($('#pp_pickup_country').val());
				var pickupcity = parse_json($('#pp_pickup_city').val());

				var dropoffstate = parse_json($('#pp_dropoff_state').val());
				if (typeof dropoffstate !== 'object') {
					dropoffstate = {
						"id": "",
						"name": ""
					};
				}

				var dropoffcountry = parse_json($('#pp_dropoff_country').val());
				var dropoffcity = parse_json($('#pp_dropoff_city').val());

				if (dropoffcountry.name == pickupcountry.name) {
					$('#step2-next-btn').removeClass('spinning');
					alert("Aquantuo currently doesn’t support transfers within the same country.");
					return;
				}
				if (pickupcity.name == dropoffcity.name && pickupcountry.name == dropoffcountry.name) {
					pickup_address = $('#address_line_1').val() + ', ' + $('#address_line_2').val() + ', ' + pickupcity.name + ', ' +
						pp_pickup_state.name + ', ' + pickupcountry.name;
					dropoff_address = $('#drop_off_address_line_1').val() + ', ' + $('#drop_off_address_line_2').val() + ', ' +
						dropoffcity.name + ', ' + dropoffstate.name + ', ' + dropoffcountry.name;
				} else {
					pickup_address = pickupcity.name + ', ' + pickupstate.name + ', ' + pickupcountry.name;
					dropoff_address = dropoffcity.name + ', ' + dropoffstate.name + ', ' + dropoffcountry.name;
				}

				geocoder.geocode({
					'address': pickup_address
				}, function(pickup, status) {

					if (status == google.maps.GeocoderStatus.OK) {

						$('#PickupLat').val(pickup[0].geometry.location.lat());
						$('#PickupLong').val(pickup[0].geometry.location.lng());

						pickup_lat = pickup[0].geometry.location.lat();
						pickup_long = pickup[0].geometry.location.lng();

						if (pickup[0].geometry.location.lat() != 0) {
							geocoder.geocode({
								'address': dropoff_address
							}, function(dropoff, status) {

								if (status == google.maps.GeocoderStatus.OK) {
									$('#DeliveryLat').val(dropoff[0].geometry.location.lat());
									$('#DeliveryLong').val(dropoff[0].geometry.location.lng());

									dropoff_lat = dropoff[0].geometry.location.lat();
									dropoff_long = dropoff[0].geometry.location.lng();
									// Calculation distance

									var service = new google.maps.DistanceMatrixService();
									service.getDistanceMatrix({
										origins: [new google.maps.LatLng(pickup_lat, pickup_long)],
										destinations: [new google.maps.LatLng(dropoff_lat, dropoff_long)],
										travelMode: 'DRIVING',

									}, function(response, status) {
										if (status == 'OK') {

											try {
												distance = response.rows[0].elements[0].distance.value;
											} catch (e) {}

											$('#calculated_distance').val(distance);

											prf_step = 2;
											$('#step2-next-btn').removeClass('spinning');
											$('#sec2').hide();
											$('#sec3').show();
											$('#step3-header').addClass('selected');
											scroll_to('send_a_package_start_position');
										}
									});


									// End distance calculation

								} else {
									// Error message on incorrect address
									$('#step2-next-btn').removeClass('spinning');
									alert('Oops! We are unable to find your drop off location. Please correct it');
								}
							});
						}
					} else {
						// Open pickup form due to incorrect address
						$('#step2-next-btn').removeClass('spinning');
						prf_step = 0;
						$('#sec1').show();
						$('#sec2').hide();
						$('#step2-header').addClass('selected');
						alert('Oops! We are unable to find your pickup location. Please correct it');
						scroll_to('send_a_package_start_position');
					}
				});
			} else if (prf_step == 2) {
				if (document.getElementById('agree').checked != true) {
					alert('Please accept agreement.');
				} else {
					$("#calculate_loader").addClass("spinning");
					$.ajax({
						url: SITEURL + "prepare-request-calculation",
						data: $('#prepare_request_form').serialize(),
						dataType: "json",
						success: function(res) {
							$("#calculate_loader").removeClass("spinning");
							if (res.success == 1) {
								request_has_error = res.success;
								$('#shipping_detail').html(res.html);
								prf_step = 3;
								total_amount = res.total_amount;
								ghana_total_amount = res.ghana_total_amount;
								shipping_cost = res.shipping_cost;
								pr_formated_currency = res.formated_currency;
								$('#sec3').hide();
								$('#sec4').show();
								$('#step4-header').addClass('selected');
								$('#total_amount_div').html(parseFloat(res.total_amount).toFixed(2));
								$('#ghana_total_amount').html(parseFloat(res.ghana_total_amount).toFixed(2));
								scroll_to('send_a_package_start_position');
							} else {
								alert(res.msg);
							}
						}
					});
				}

			} else if (prf_step == 3) {
				if (request_has_error == 0) {
					alert(
						'The weight you entered is outside the limits allowed by the airlines. If possible, split your package into smaller packages or choose to ship your item by sea....!'
					);
				} else {

					$("#item_loader").addClass("spinning");
					if (document.getElementById('same_as_pickup').checked == true) {
						fill_nd_return_address();
					}

					$.ajax({
						url: SITEURL + "prepare-request",
						method: 'post',
						data: new FormData(document.getElementById('prepare_request_form')),
						processData: false,
						contentType: false,
						dataType: "json",
						success: function(res) {
							/* update 20/1/17   */

							if (res.success == 1) {
								document.location.href = SITEURL + 'process-card-list/' + res.reqid + '?promocode=' + $('#promocode').val() +
									'&request_type=' + res.type;
							} else {
								$("#item_loader").removeClass("spinning");
								alert(res.msg);
							}

						}
					});
				}
			}

		} catch (e) {
			console.log(e);
		}
	}
});

function switch_html(showid, hideid) {
	$(showid).show();
	$(hideid).hide();
}

function switch_request_header(showid, hideid, section) {
	prf_step = section - 1;
	for (var i = 1; i <= 4; i++) {
		if (i <= section) {
			$('#step' + i + '-header').addClass('selected');
		} else {
			$('#step' + i + '-header').removeClass('selected');
		}
	}
	switch_html(showid, hideid);
	scroll_to('send_a_package_start_position');
}


function parse_json($string) {
	if ($string.trim() != '') {
		return eval('(' + $string + ')');
	}
}

function toggle_html(showid, hideid) {
	$('#package_category').val('');
	switch_html(showid, hideid)
}

function check_promocode($inputfield, shipping_cost, success_div, total_amount) {
	$('#er_' + $inputfield).html('');
	if ($('#' + $inputfield).val() == '') {
		$('#er_' + $inputfield).html('Please enter promocode.');
		return false;
	}

	$('#' + success_div + '_input').addClass('spinning');
	$.ajax({
		url: SITEURL + "validate_promocode",
		data: {
			promocode: $('#' + $inputfield).val(),
			shipping_cost: shipping_cost,
			total_amount: total_amount,
		},
		method: 'post',
		dataType: "json",
		success: function(res) {

			$('#' + success_div + '_input').removeClass('spinning');
			if (res.success == 1) {
				$('#' + success_div).show();
				$('#' + success_div + '_input').hide();
				$('#' + success_div + '_msg').html(res.msg);
				$('#' + success_div + '_payable').html('$' + (total_amount - res.result.discount).toFixed(2));
				$('#' + success_div + '_payable_ghana').html(res.result.GhanaTotalCost);

			} else {
				$('#er_' + $inputfield).html(res.msg);
				$('#' + success_div + '_input').show();
			}

		}
	});

}

function remove_promocode(success_div, inputfield) {
	$('#' + inputfield).val('');
	$('#' + success_div).hide();
	$('#' + success_div + '_input').show();
	$('#' + success_div + '_payable').html('$' + total_amount.toFixed(2));
	//$('#'+success_div+'_payable_ghana').html($('#olp_promo_ghana').html());
	$('#' + success_div + '_payable_ghana').html(pr_formated_currency);

}


$('#return_same_as_pickup').click(function() {
	if (document.getElementById('return_same_as_pickup').checked == true) {

		$('input[name="return_address_line_1"]').val($('#address_line_1').val());
		$('input[name="return_address_line_2"]').val($('input[name="address_line_2"]').val());
		$('#pp_return_country').html($('#pp_pickup_country').html());
		$('#pp_return_country').val($('#pp_pickup_country').val());
		$('#pp_return_state').html($('#pp_pickup_state').html());
		$('#pp_return_state').val($('#pp_pickup_state').val());
		$('#pp_return_city').html($('#pp_pickup_city').html());
		$('#pp_return_city').val($('#pp_pickup_city').val());
		$('input[name="return_zipcode"]').val($('input[name="zipcode"]').val());

		$('#return_jurney_address').hide();
	} else {
		$('#return_jurney_address').show();
	}
});
$('#same_as_pickup').click(function() {
	if (document.getElementById('same_as_pickup').checked == true) {
		fill_nd_return_address();
	} else {
		$('#return_address_action').show();
	}
});

function fill_nd_return_address() {
	$('#return_address_action').hide();
	$('input[name="nd_return_address_line_1"]').val($('#address_line_1').val());
	$('input[name="nd_return_address_line_2"]').val($('input[name="nd_return_address_line_2"]').val());
	$('#pp_nd_return_country').html($('#pp_pickup_country').html());
	$('#pp_nd_return_country').val($('#pp_pickup_country').val());
	$('#pp_nd_return_state').html($('#pp_pickup_state').html());
	$('#pp_nd_return_state').val($('#pp_pickup_state').val());
	$('#pp_nd_return_city').html($('#pp_pickup_city').html());
	$('#pp_nd_return_city').val($('#pp_pickup_city').val());
	$('input[name="nd_return_zipcode"]').val($('input[name="zipcode"]').val());
}



function toggle_category(showid, hideid, id) {
	$(id).val('');
	$(showid).attr('disabled', false);
	$(showid).show();
	$(hideid).attr('disabled', true);
	$(hideid).hide();
}

toggle_category('.travel-mode-air', '.travel-mode-ship');

function callback(response, status) {
	if (status == 'OK') {
		distance = response.rows[0].elements[0].distance.value;
	}
}



function scroll_to(id) {
	$('html, body').animate({
		scrollTop: $('#' + id).offset().top
	}, 'slow');
}