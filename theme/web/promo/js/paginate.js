(function() {
	this.paginate = function(option) {

		var options = {
            container: '',
            url: "",
            postvalue: "",
            pageno: 1,
            limit: 10,
            unique: new_guid()
        }
        var _this = this;
        _this.defaults = __extent(options,option);

        $('<div  id="loading_'+options.container+'" class="web_pagination_loading"></div>').insertBefore('#'+options.container);
        
		$("#"+options.container).on('click',  '.'+options.unique+' li.current-active', function()
		{
			_this.defaults.pageno = $(this).attr('p');
			load_data(_this.defaults);
		});

		$("#"+options.container).on('change',  '.changepagnum', function()
		{
			_this.defaults.limit = $(this).val();
			_this.defaults.pageno = 1;
			load_data(_this.defaults);
		});

		load_data(options);
		$("#"+options.container).on('click',  '.goto-btn', function()
		{
			var page = parseInt($("#"+options.container+" .goto-textbox").val());
			
			var no_of_pages = parseInt($("#"+options.container+" .total").attr('a'));			
			if(page != 0 && page <= no_of_pages)
			{
				_this.defaults.pageno = page;
				load_data(_this.defaults);
			}
			else
			{
				if(no_of_pages=='0'){
					$("#"+options.container+" .searcherror").html('There is no result so you can not search any more');
				}else{
					$("#"+options.container+" .searcherror").html('Enter a Page between 1 and '+no_of_pages);
				}
				$('.goto').val("").focus();
				return false;
			}
		});
		   
		

	},
    __extent = function(option, useroption) {
        for(var i in useroption) {
            option[i] = useroption[i];
        }
        return option;
    },
    new_guid = function() {
		var sGuid="";
		for (var i=0; i<32; i++)
		{
		  sGuid+=Math.floor(Math.random()*0xF).toString(0xF);
		}
		return sGuid;
	},
	load_data = function(defaults) {

		var postval = defaults.postvalue;
	        postval.page = defaults.pageno;
	        postval.Pnum = defaults.limit;
	        postval.unique = defaults.unique;

	        $.ajax
			({
				type: "POST",
				url: SITEURL+defaults.url,
				data: postval,
				//headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
				success: function(msg)
				{		
					$('#loading_'+defaults.container).hide();
					defaults.pageno++;
					$("#"+defaults.container).html(msg);				
					console.log('#loading_'+defaults.container);
				}
			});
	}

}());

function remove_record(url,rowid,confirm_msg)
{
	if (typeof confirm_msg === "undefined" || confirm_msg === null) { 
    	confirm_msg = 'Are you sure, you want to delete this record?';	
  	}
	
	if(confirm(confirm_msg) == true)
	{
		$('#row-'+rowid).addClass('relative-pos spinning');
		
		url = SITEURL+url;
		$.ajax
			({
				url: url,
				headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
				success: function(res)
				{					
					var obj = JSON.parse(res);
					$('#row-'+rowid).removeClass('relative-pos spinning');
					if(obj.success == 1){
						
						$('#row-'+rowid).css({'background-color':'red'});
						$('#row-'+rowid).fadeOut('slow');
						$('#error_msg_section').html('<div class="alert alert-success">'+ obj.msg + '</div>');
					}else{
						$('#row-'+rowid).css({'background-color':'white'});
						$('#error_msg_section').html('<div class="alert alert-danger">'+ obj.msg + '</div>');
						alert(obj.msg);
					}
				}
			});
	}
	return false;
} 
