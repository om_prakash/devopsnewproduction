//validation for prepare request
//prf= prepare request form
//document.getElementsByName('prepare_request_form')[0].reset();
var prf_step = 0,
	pickup_lat = 0,
	pickup_long = 0,
	dropoff_lat = 0,
	dropoff_long = 0,
	total_amount = 0,
	ghana_total_amount = 0,
	request_has_error = 0,
	distance = 0;

new Validate({
	FormName : 'prepare_request_form',
	ErrorLevel : 1,
	validateHidden: false,
	callback : function() {

		try
		{
		if(prf_step == 0) {
		    prf_step = 1;
			$('#sec1').hide();
			$('#sec2').show();
			$('#step2-header').addClass('selected');
		}
		else if(prf_step == 1) {
			// getting pickup addres lat long

			var geocoder = new google.maps.Geocoder();
			var pickup_address = '';
			var dropoff_address = '';

			var pickupstate = parse_json($('#pp_pickup_state').val());
			var pickupcountry = parse_json($('#pp_pickup_country').val());

			var dropoffstate = parse_json($('#pp_dropoff_state').val());
			var dropoffcountry = parse_json($('#pp_dropoff_country').val());


			if($('#pp_pickup_city').val() == $('#pp_dropoff_city').val() && pickupcountry.name == dropoffcountry.name) {
				pickup_address = $('#address_line_1').val()+', '+$('#address_line_2').val()+', '+$('#pp_pickup_city').val()+', '+pickupstate.name+', '+pickupcountry.name;
				dropoff_address = $('#drop_off_address_line_1').val()+', '+$('#drop_off_address_line_2').val()+', '+$('#pp_dropoff_city').val()+', '+dropoffstate.name+', '+dropoffcountry.name;
			} else {
				pickup_address = $('#pp_pickup_city').val()+', '+pickupstate.name+', '+pickupcountry.name;
				dropoff_address = $('#pp_dropoff_city').val()+', '+dropoffstate.name+', '+dropoffcountry.name;
			}

				geocoder.geocode( { 'address': pickup_address}, function(pickup, status) {

					if (status == google.maps.GeocoderStatus.OK)
					{
						
						$('#PickupLat').val(pickup[0].geometry.location.lat());
						$('#PickupLong').val(pickup[0].geometry.location.lng());
						
						console.log(pickup[0].geometry.location.lat());
						pickup_lat = pickup[0].geometry.location.lat();
						pickup_long = pickup[0].geometry.location.lng();
					    
					    if(pickup[0].geometry.location.lat() != 0)
					    {
					    	geocoder.geocode( { 'address': dropoff_address}, function(dropoff, status) {

								if (status == google.maps.GeocoderStatus.OK)
								{
									$('#DeliveryLat').val(pickup[0].geometry.location.lat());
						            $('#DeliveryLong').val(pickup[0].geometry.location.lng());
						            
									dropoff_lat = dropoff[0].geometry.location.lat();
									dropoff_long = dropoff[0].geometry.location.lng();
                                    // Calculation distance

									var service = new google.maps.DistanceMatrixService();
									service.getDistanceMatrix(
									  {
									    origins: [new google.maps.LatLng(pickup_lat, pickup_long)],
									    destinations: [new google.maps.LatLng(dropoff_lat, dropoff_long)],
									    travelMode: 'DRIVING',
									    
									  }, function(response, status) {
										  if(status == 'OK') {
										  	console.log(response.rows[0]);
										  		//distance = response.rows[0].elements[0].distance.value;

										  		$('#calculated_distance').val(distance);
										  		
										  		prf_step = 2;
												$('#sec2').hide();
												$('#sec3').show();
												$('#step3-header').addClass('selected');
										  }
										});

									
                                    // End distance calculation
									
								} else {
									// Error message on incorrect address
									alert('Oops! We are unable to find your drop off location. Please correct it');
								}
							});
						}
					} else {
						// Open pickup form due to incorrect address
						prf_step = 0;
						$('#sec1').show();
						$('#sec2').hide();
						$('#step2-header').addClass('selected');
						alert('Oops! We are unable to find your pickup location. Please correct it');
					}
				});
		}
		else if(prf_step == 2)
		{
			$.ajax({
				url: SITEURL+"prepare-request-calculation",
				data: $('#prepare_request_form').serialize(),
				dataType: "json",
				success:  function (res) {
					request_has_error = res.success;
					$('#shipping_detail').html(res.html);
					prf_step = 3;
					total_amount = res.total_amount;
					ghana_total_amount = res.ghana_total_amount;
					$('#sec3').hide();
					$('#sec4').show();
					$('#step4-header').addClass('selected');
					$('#total_amount_div').html(parseFloat(res.total_amount).toFixed(2));
					$('#ghana_total_amount').html(parseFloat(res.ghana_total_amount).toFixed(2));
				}
			});
			
		}
		else if(prf_step == 3) 
		{
			if(request_has_error == 0) {
				alert('The weight you entered is outside the limits allowed by the airlines. If possible, split your package into smaller packages or choose to ship your item by sea....!');
			} else
			 {
				$.ajax({
					url: SITEURL+"edit_prepare_request",
					method: 'post',
					data: $('#prepare_request_form').serialize(),
					dataType: "json",
					success:  function (res) {
						alert(res.msg);
						if(res.success == 1) {
							document.location.href = SITEURL+'process-card-list/'+res.reqid;
						} 

					}
				});
			}
		}
		
	}catch(e){
		console.log(e);
	}
	}
});
 
function switch_html(showid,hideid) {
	$(showid).show();
	$(hideid).hide();
}
function switch_request_header(showid,hideid,section) {
	prf_step = section - 1;
	for(var i=1;i<=4;i++) {
		if(i<=section){
			$('#step'+i+'-header').addClass('selected');
		} else {
			$('#step'+i+'-header').removeClass('selected');
		}
	}
	switch_html(showid,hideid);
	
}


function parse_json($string) {
	return eval('('+$string+')');
}
function toggle_html(showid,hideid) {
	$('#package_category').val('');
	switch_html(showid,hideid)
}

function check_promocode($input,total_amount,sectionid)
{
	var code = $('#'+$input).val();
	$('#er_'+$input).html('');
	if(code.trim() == '') {
		$('#er_'+$input).html('The promocode field is required.');
	} else {

		$.ajax({
			url: SITEURL+"validate-promocode",
			data: {code:code,total_amount:total_amount},
			dataType: "json",
			success:  function (res) {

				$('#'+sectionid+'_input').hide();
				$('#'+sectionid).show();
				$('#'+sectionid+'_msg').html();
				
			}
		});
	}
}

$('#return_same_as_pickup').click(function(){
	if(document.getElementById('return_same_as_pickup').checked == true) {
		
		$('input[name="return_address_line_1"]').val($('#address_line_1').val());
		$('input[name="return_address_line_2"]').val($('input[name="address_line_2"]').val());
		$('#pp_return_country').html($('#pp_pickup_country').html());
		$('#pp_return_country').val($('#pp_pickup_country').val());
		$('#pp_return_state').html($('#pp_pickup_state').html());
		$('#pp_return_state').val($('#pp_pickup_state').val());
		$('#pp_return_city').html($('#pp_pickup_city').html());
		$('#pp_return_city').val($('#pp_pickup_city').val());
		$('input[name="return_zipcode"]').val($('input[name="zipcode"]').val());

		$('#return_jurney_address').hide();
	} else {
		$('#return_jurney_address').show();
	}
});
$('#same_as_pickup').click(function(){
	if(document.getElementById('same_as_pickup').checked == true) {
		$('#return_address_action').hide();
		$('input[name="nd_return_address_line_1"]').val($('#address_line_1').val());
		$('input[name="nd_return_address_line_2"]').val($('input[name="nd_return_address_line_2"]').val());
		$('#pp_nd_return_country').html($('#pp_pickup_country').html());
		$('#pp_nd_return_country').val($('#pp_pickup_country').val());
		$('#pp_nd_return_state').html($('#pp_pickup_state').html());
		$('#pp_nd_return_state').val($('#pp_pickup_state').val());
		$('#pp_nd_return_city').html($('#pp_pickup_city').html());
		$('#pp_nd_return_city').val($('#pp_pickup_city').val());
		$('input[name="nd_return_zipcode"]').val($('input[name="zipcode"]').val());
	} else {
		$('#return_address_action').show();
	}
});



function toggle_category(showid,hideid,id){
	$(id).val('');
	$(showid).attr('disabled',false);
	$(showid).show();
	$(hideid).attr('disabled',true);
	$(hideid).hide();
}

toggle_category('.travel-mode-air','.travel-mode-ship');

function callback(response, status) {
  if (status == 'OK') {
  		distance = response.rows[0].elements[0].distance.value;
  }
}


