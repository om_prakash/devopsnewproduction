//var socket = io.connect('https://apis.aquantuo.com:8080/');
//var socket = io.connect('https://apis.aquantuo.com:8080',{secure: true});

//node js login
$(document).ready(function() {
  socket.emit("test",function() {
    console.log('okkk');
  });

  var user_id = USERID;
    //alert(user_id);
    var data = JSON.stringify({
        "user_id":user_id,
    });
    socket.emit("login", data, function(response) {
        console.log(response);
    });
});
//end of login

function open_chat_box(id,name){
    $(".purechat-collapsed purechat-collapsed-default purechat-display-block").hide();
    $("#msg").addClass('spinning');
    $("#to_id").val('');
    $("#customer_name").html('');
    $("#to_id").val(id);
    $("#customer_name").html(' '+name);
	$("#chat_box").show();
    chatHistory();
}

function close_box ()
{
    $("#to_id").val('');
    $("#customer_name").html('');
	$("#chat_box").hide();
    $("#msg").removeClass('spinning');
}

function keyupi()
{
    var data = JSON.stringify({
        "from_id": USERID,
        "to_id": $("#to_id").val(),
        
    });

    socket.emit("StopPrivateTyping", data, function(response) {
        console.log(response);
    });
	
}

function myFunction() {
    var data = JSON.stringify({
        "from_id": USERID,
        "to_id": $("#to_id").val(),
        'type':'web',
        
    });
    socket.emit("StartPrivateTyping", data, function(response) {
        setTimeout( keyupi() , 3000 );
    });
}

socket.on("StartPrivateTyping", function (data) {
	var json = JSON.parse(data);
	$("#typing").show().delay(3000).fadeOut();
});

function mes_send()
{
    var from_id =USERID;
    //var time = moment.utc().valueOf()
    //var currentTime = moment(time).format(' hh:mm A');
    var msg =$("#msg-input").val();
    var from_name =USERNAME;
    
    var data = JSON.stringify({
        "from_id":from_id,
        "from_name":from_name,
        "to_id":$("#to_id").val(),
        "to_name":$("#to_name").val(),
        "message":$("#msg-input").val(),
        "user_from":"",
       
    });
    var msg =$("#msg-input").val();
    $("#msg-input").val('');
    var str="";
    

    socket.emit("PrivateMessage", data, function(response) {
        var time = moment.utc().valueOf()
        var currentTime = moment(time).format(' hh:mm A');
        var res = response.result;
        
        str ='<li style="width:100%;">'+
            '<div class="msj-rta macro"><div class="text text-r">'+
                '<p>'+res.message+'</p>'+

                '<p><small>'+currentTime+'</small></p></div>'+
                
            '</div>'+
        '</li>';

            
        $("#msg").append(str);
        $('#msg').animate({scrollTop: $('#msg').prop("scrollHeight")}, 1000); 
        
    });
}

socket.on("PrivateMessage", function (res) {

    var time = moment.utc().valueOf()
    var currentTime = moment(time).format(' hh:mm A');
    var user_data = res;
    
    var str ='<li style="width:100%">'+
        '<div class="msj macro"><div class="text text-l">'+
            '<p>'+user_data.message+'</p>'+

            '<p><small>'+currentTime+'</small></p></div>'+
            
        '</div>'+
    '</li>';               

    
    $("#msg").append(str);
    //$("#msg").animate({ scrollTop: $(document).height() });
    $('#msg').animate({scrollTop: $('#msg').prop("scrollHeight")}, 1000); 
});

socket.on("changeUserOnlineStatus", function (res) {
    if(res.user_id == $("#to_id").val()){
        $("#user_status").html('');
        $("#user_status").html('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+res.msg);
    }
})


function chatHistory()
{
    var from_id =USERID;
    //var time = moment.utc().valueOf()
    //var currentTime = moment(time).format(' hh:mm A');
    var from_name =USERNAME;
    var data = JSON.stringify({
        "to_id":USERID,
        "from_id":$("#to_id").val(),
    });
    $("#msg").html('');
    $("#user_status").html('');
    socket.emit("ChatHistory", data, function(res) {
        
        var result = res.result;
        var str ='';
        
        for (var int = 0; int < result.length; int++) {

                var current_date = new Date();
                var current_format = (current_date.getMonth() + 1) + '/' + current_date.getDate() + '/' +  current_date.getFullYear();
                var olddate = new Date(result[int].date_time);
                var olddateformat = (olddate.getMonth() + 1) + '/' + olddate.getDate() + '/' +  olddate.getFullYear();

                var d3 = new Date(current_format);
                var d4 = new Date(olddateformat);

                var timeDiff = Math.abs(d3.getTime() - d4.getTime());
                var diffDays123 = Math.ceil(timeDiff / (1000 * 3600 * 24));

                if(diffDays123 > 0){
                    var show = olddateformat;
                }else{
                    var date = new Date(result[int].date_time);
                    var h = date.getHours();
                    var m = date.getMinutes();
                    if(h >= 12){
                        var format = "PM";
                    }else{ format = "AM"; }
                        show =h+':'+m+' '+format;
                        
                }

                if(result[int].from_id == USERID){
                    str +=  '<li style="width:100%;">'+
                        '<div class="msj-rta macro"><div class="text text-r">'+
                            '<p>'+result[int].message+'</p>'+

                            '<p><small>'+show+'</small></p></div>'+
                            
                        '</div>'+
                    '</li>';
                }else{

                   str += '<li style="width:100%">'+
                        '<div class="msj macro"><div class="text text-l">'+
                            '<p>'+result[int].message+'</p>'+

                            '<p><small>'+show+'</small></p></div>'+
                            
                        '</div>'+
                    '</li>';

                }
        }
        $("#user_status").html('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+res.online_status);
        $("#msg").append(str);
        $("#msg").removeClass('spinning');
        $('#msg').animate({scrollTop: $('#msg').prop("scrollHeight")}, 1000); 
    });
}

function socket_disconnect()
{
    socket.emit("socket_disconnect",function() {
        console.log('hhhhhhhh');
    });
    window.location.href = SITEURL+"logout";
}

//

function chat_test()
{
    socket.emit("test",function(res) {
        alert(res);
    });
}

$("#9").click(function(e){
    e.preventDefault();//this will prevent the link trying to navigate to another page
    var href = $(this).attr("href");//get the href so we can navigate later
    socket.emit("socket_disconnect",function() {
        console.log('hhhhhhhh');
    });
});