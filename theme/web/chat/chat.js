var _desc = "Have items delivered to your home or your business in Ghana or Kenya from the US. Send packages to the US from Ghana. Shop international stores and receive them at your door. Get help shopping international store";
var _keyword = "shipping ghana, shipping to ghana, shipping to kenya, shipping to ghana from usa, shipping to kenya from usa, shipping from ghana to usa, ghana shipping companies, kenya shipping companies, air cargo companies in ghana, air cargo companies in kenya, air freight to ghana, air freight to kenya, freight logistics companies";

document.querySelector('meta[name="description"]').setAttribute("content", _desc);
document.querySelector('meta[name="keywords"]').setAttribute("content", _keyword);