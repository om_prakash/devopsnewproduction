var myVar = setInterval(function(){ check_system(); }, 1000);
var notifyresult = {};
var msg = '';
var notify_support_check = false;
function check_system()
{
  clearInterval(myVar);
  
  $.ajax(
  {
    url: SITEURL+'check-system',
    method: 'get',
    
    success: function(obj) {
      
      if(obj.notify.length > 0) {

        for(var i in obj.notify) {

          notifyMe(obj.notify[i].NotificationTitle,obj.notify[i].NotificationMessage);
        }
      }      
      myVar = setInterval(function(){ check_system(); }, 50000); //300000
    }
  }); 
}

function notifyMe(msg,body) {
  var options = {
    body  : body,
    sound : '',
    tag   : 'soManyNotification',
    icon  : SITEURL+'theme/web/promo/images/preloader.gif',
  }

  // Let's check if the browser supports notifications

  if (!("Notification" in window) && notify_support_check == false) {
    notify_support_check = true;
    alert("This browser does not support system notifications");
  }

  


  // Let's check whether notification permissions have already been granted
  else if (Notification.permission === "granted") {
    // If it's okay let's create a notification
    return notification = new Notification(msg,options);
  }

  // Otherwise, we need to ask the user for permission
  else if (Notification.permission !== 'denied') {
    Notification.requestPermission(function (permission) {
      // If the user accepts, let's create a notification
      if (permission === "granted") {
        return notification = new Notification(msg,options);
      }
    });
  }

  // Finally, if the user has denied notifications and you 
  // want to be respectful there is no need to bother them any more.
}
