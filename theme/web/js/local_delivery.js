//validation for prepare request
//prf= prepare request form
//document.getElementsByName('local_prepare_request_form')[0].reset();

var prf_step = 0,
	pickup_lat = 0,
	pickup_long = 0,
	dropoff_lat = 0,
	dropoff_long = 0,
	total_amount = 0,
	ghana_total_amount = 0,
	request_has_error = 0,
	distance = 0,
	shipping_cost = 0,
	pr_formated_currency = '';

new Validate({
	FormName: 'local_prepare_request_form',
	ErrorLevel: 1,
	validateHidden: false,
	callback: function() {


		try {
			if (prf_step == 0) {
				prf_step = 1;
				$('#sec1').hide();
				$('#sec2').show();
				$('#step2-header').addClass('selected');
				scroll_to('send_a_package_start_position');
			} else if (prf_step == 1) {

				prf_step = 2;
				$('#sec2').hide();
				$('#sec3').show();
				$('#step3-header').addClass('selected');
				scroll_to('send_a_package_start_position');
			} else if (prf_step == 2) {
				$('#step3-next-btn').addClass('spinning');
				var geocoder = new google.maps.Geocoder();
				var pickup_address = '';
				var dropoff_address = '';

				var pickupstate = parse_json($('#pp_pickup_state').val());
				if (typeof pickupstate !== 'object') {
					pickupstate = {
						"id": "",
						"name": ""
					};
				}


				var pickupcountry = parse_json($('#pp_pickup_country').val());
				var pickupcity = parse_json($('#pp_pickup_city').val());

				var dropoffstate = parse_json($('#pp_dropoff_state').val());
				if (typeof dropoffstate !== 'object') {
					dropoffstate = {
						"id": "",
						"name": ""
					};
				}

				var dropoffcountry = parse_json($('#pp_dropoff_country').val());
				var dropoffcity = parse_json($('#pp_dropoff_city').val());

				/*if (dropoffcountry.name == pickupcountry.name) {
					$('#step2-next-btn').removeClass('spinning');
					alert("Aquantuo currently doesn’t support transfers within the same country.");
					return;
				}*/
				if (pickupcity.name == dropoffcity.name && pickupcountry.name == dropoffcountry.name) {
					pickup_address = $('#address_line_1').val() + ', ' + $('#address_line_2').val() + ', ' + pickupcity.name + ', ' +
						pp_pickup_state.name + ', ' + pickupcountry.name;
					dropoff_address = $('#drop_off_address_line_1').val() + ', ' + $('#drop_off_address_line_2').val() + ', ' +
						dropoffcity.name + ', ' + dropoffstate.name + ', ' + dropoffcountry.name;
				} else {
					pickup_address = pickupcity.name + ', ' + pickupstate.name + ', ' + pickupcountry.name;
					dropoff_address = dropoffcity.name + ', ' + dropoffstate.name + ', ' + dropoffcountry.name;
				}

				geocoder.geocode({
					'address': pickup_address
				}, function(pickup, status) {


					if (status == google.maps.GeocoderStatus.OK) {
						
						$('#PickupLat').val(pickup[0].geometry.location.lat());
						$('#PickupLong').val(pickup[0].geometry.location.lng());

						pickup_lat = pickup[0].geometry.location.lat();
						pickup_long = pickup[0].geometry.location.lng();

						if (pickup[0].geometry.location.lat() != 0) {
							geocoder.geocode({
								'address': dropoff_address
							}, function(dropoff, status) {

								if (status == google.maps.GeocoderStatus.OK) {
									$('#DeliveryLat').val(dropoff[0].geometry.location.lat());
									$('#DeliveryLong').val(dropoff[0].geometry.location.lng());

									dropoff_lat = dropoff[0].geometry.location.lat();
									dropoff_long = dropoff[0].geometry.location.lng();
									// Calculation distance

									var service = new google.maps.DistanceMatrixService();
									service.getDistanceMatrix({
										origins: [new google.maps.LatLng(pickup_lat, pickup_long)],
										destinations: [new google.maps.LatLng(dropoff_lat, dropoff_long)],
										travelMode: 'DRIVING',

									}, function(response, status) {
										if (status == 'OK') {

											try {
												distance = response.rows[0].elements[0].distance.value;
											} catch (e) {}

											//alert(distance);

											$('#calculated_distance').val(distance);

											//calculation
											var action_url = SITEURL + "local_prepare_request_calculation";
											/*if ($("#request_id").val()) {
												var action_url = SITEURL + "local_edit_prepare_request_calculation";
											}*/

											$.ajax({
												url: action_url,
												method: 'post',
												data: new FormData(document.getElementById('local_prepare_request_form')),
												processData: false,
												contentType: false,
												dataType: "json",

												success: function(res) {
													$("#calculate_loader").removeClass("spinning");
													if (res.success == 1) {
														prf_step = 3;
														$('#step2-next-btn').removeClass('spinning');
															
														var showDis = res.result.distance * 1.60934;
														$('#distance').html(showDis.toFixed(2) + " Km");


														//$('#shipping_cost').html("$" + res.result.shipping_cost);
														$('#shipping_cost').html("GHS " + res.result.shipping_cost_in_ghs);	// +" / $"+res.result.shipping_cost

														var showWei = res.result.total_weight * 0.453592;
														$('#total_weight').html(showWei.toFixed(2) + " kg");
														$('#total_volume').html(res.result.volume.toFixed(2) + " Cu.Cm");
														//$('#user_currency').html(res.result.user_currency);

														$('#user_currency').html("GHS " + res.result.total_amount_in_ghs);	// +" / $"+res.result.total_amount

														$('#total_amount').html("$" + res.result.total_amount);
														$("#totalItemCost").html("GHS " + res.result.totalItemCost_in_ghs);	//  + " / " + "$ " + (res.result.totalItemCost).toFixed(2)

														//$('#insurance_cost').html("$" + res.result.insurance_price);

														$('#insurance_cost').html("GHS " + res.result.insurance_price_in_ghs);	// +" / $"+res.result.insurance_price
														
														if(res.result.insurance_price=='0'){
															$('.insurance_cost_show').hide();
														}

														//$('#region_cost').html("$" + res.result.region_price);
														$('#region_cost').html("GHS " + res.result.region_price_in_ghs);	// +" / $"+res.result.region_price
														if(res.result.region_price=='0'){
															$('.region_cost_show').hide();
														}
														
														
														$('#total_item').html(res.result.item_count);
														
														//$('#item_price').html("$" + res.item_price);

														$('#shipping_cost_input').val(res.result.shipping_cost);
														$('#total_amount_input').val(res.result.total_amount);

														$('#olp_promo_payable_ghana').html(res.result.user_currency);
														$('#olp_promo_payable').html("$" + res.result.total_amount);



														$('#sec3').hide();
														$('#sec4').show();
														$('#step3-next-btn').removeClass('spinning');
														$('#step4-header').addClass('selected');


													} else {
														$('#step3-next-btn').removeClass('spinning');
														alert(res.msg);
													}
												}

											});
											//end calculation

											//prf_step = 3;
											//$('#step2-next-btn').removeClass('spinning');
											//$('#sec2').hide();
											//$('#sec3').show();
											//$('#step3-header').addClass('selected');
											//scroll_to('send_a_package_start_position');
										}
									});


									// End distance calculation

								} else {
									// Error message on incorrect address
									$('#step2-next-btn').removeClass('spinning');
									alert('Oops! We are unable to find your drop off location. Please correct it');
								}
							});
						}
					} else {
						// Open pickup form due to incorrect address
						$('#step3-next-btn').removeClass('spinning');
						prf_step = 1;
						$('#sec2').show();
						$('#sec3').hide();
						$('#step2-header').addClass('selected');
						alert('Oops! We are unable to find your pickup location. Please correct it');
						scroll_to('send_a_package_start_position');
					}
				});
			} else if (prf_step == 3) {
				var action_url = SITEURL + "local_prepare_request_create";
				$('#last-stage').addClass('spinning');

				$.ajax({
					url: action_url,
					method: 'post',
					data: new FormData(document.getElementById('local_prepare_request_form')),
					processData: false,
					contentType: false,
					dataType: "json",

					success: function(res) {
						$("#calculate_loader").removeClass("spinning");
						$('#last-stage').removeClass('spinning');
						if (res.success == 1) {
							$('#step2-next-btn').removeClass('spinning');
							alert(res.msg);
							//document.location.href = SITEURL+'local-delivery-detail/'+res.reqid;
							document.location.href = SITEURL + 'local-delivery-process-card-list/' + res.reqid + '?promocode=' + $(
									'#promocode').val() +
								'&request_type=' + res.type;
						} else {
							alert(res.msg);
							$('#last-stage').removeClass('spinning');
						}
					}
				});
			}

		} catch (e) {
			console.log(e);
		}
	}
});


function request_calculation() {
	var action_url = SITEURL + "local_prepare_request_calculation";
	$.ajax({
		url: action_url,
		method: 'post',
		data: new FormData(document.getElementById('local_prepare_request_form')),
		processData: false,
		contentType: false,
		dataType: "json",

		success: function(res) {
			$("#calculate_loader").removeClass("spinning");
			if (res.success == 1) {
				$('#step2-next-btn').removeClass('spinning');
				//document.location.href = SITEURL + 'create-request';
			} else {
				alert(res.msg);
			}
		}

	});
}



function switch_html(showid, hideid) {
	$(showid).show();
	$(hideid).hide();
}

function switch_request_header(showid, hideid, section) {
	prf_step = section - 1;


	for (var i = 1; i <= 4; i++) {
		if (i <= section) {
			$('#step' + i + '-header').addClass('selected');
		} else {
			$('#step' + i + '-header').removeClass('selected');
		}
	}
	//alert("last "+prf_step); 
	$(showid).show();
	$(hideid).hide();
	//switch_html(showid, hideid);
	scroll_to('send_a_package_start_position');
}


function parse_json($string) {
	if ($string.trim() != '') {
		return eval('(' + $string + ')');
	}
}

function toggle_html(showid, hideid) {
	$('#package_category').val('');
	switch_html(showid, hideid)
}

function check_promocode($inputfield, shipping_cost, success_div, total_amount) {
	$('#er_' + $inputfield).html('');
	if ($('#' + $inputfield).val() == '') {
		$('#er_' + $inputfield).html('Please enter promocode.');
		return false;
	}

	var shipping_cost = $('#' + shipping_cost).val();
	var total_amount = $('#' + total_amount).val();

	$('#' + success_div + '_input').addClass('spinning');
	$.ajax({
		url: SITEURL + "validate_promocode",
		data: {
			promocode: $('#' + $inputfield).val(),
			shipping_cost: shipping_cost,
			total_amount: total_amount,
		},
		method: 'post',
		dataType: "json",
		success: function(res) {

			$('#' + success_div + '_input').removeClass('spinning');
			if (res.success == 1) {
				$('#' + success_div).show();
				$('#' + success_div + '_input').hide();
				$('#' + success_div + '_msg').html(res.msg);
				$('#' + success_div + '_payable').html('$' + (total_amount - res.result.discount).toFixed(2));
				//$('#' + success_div + '_payable_ghana').html(res.result.GhanaTotalCost);
				$('#' + success_div + '_payable_ghana').html(res.result.TotalCost_Ghana);
				

			} else {
				$('#er_' + $inputfield).html(res.msg);
				$('#' + success_div + '_input').show();
			}

		}
	});

}

function remove_promocode(success_div, inputfield) {
	$('#' + inputfield).val('');
	$('#' + success_div).hide();
	$('#' + success_div + '_input').show();
	$('#' + success_div + '_payable').html('$' + total_amount.toFixed(2));
	//$('#'+success_div+'_payable_ghana').html($('#olp_promo_ghana').html());
	$('#' + success_div + '_payable_ghana').html(pr_formated_currency);

}


$('#return_same_as_pickup').click(function() {
	if (document.getElementById('return_same_as_pickup').checked == true) {

		$('input[name="return_address_line_1"]').val($('#address_line_1').val());
		$('input[name="return_address_line_2"]').val($('input[name="address_line_2"]').val());
		$('#pp_return_country').html($('#pp_pickup_country').html());
		$('#pp_return_country').val($('#pp_pickup_country').val());
		$('#pp_return_state').html($('#pp_pickup_state').html());
		$('#pp_return_state').val($('#pp_pickup_state').val());
		$('#pp_return_city').html($('#pp_pickup_city').html());
		$('#pp_return_city').val($('#pp_pickup_city').val());
		$('input[name="return_zipcode"]').val($('input[name="zipcode"]').val());

		$('#return_jurney_address').hide();
	} else {
		$('#return_jurney_address').show();
	}
});
$('#same_as_pickup').click(function() {
	if (document.getElementById('same_as_pickup').checked == true) {
		fill_nd_return_address();
	} else {
		$('#return_address_action').show();
	}
});

function fill_nd_return_address() {
	$('#return_address_action').hide();
	$('input[name="nd_return_address_line_1"]').val($('#address_line_1').val());
	$('input[name="nd_return_address_line_2"]').val($('input[name="nd_return_address_line_2"]').val());
	$('#pp_nd_return_country').html($('#pp_pickup_country').html());
	$('#pp_nd_return_country').val($('#pp_pickup_country').val());
	$('#pp_nd_return_state').html($('#pp_pickup_state').html());
	$('#pp_nd_return_state').val($('#pp_pickup_state').val());
	$('#pp_nd_return_city').html($('#pp_pickup_city').html());
	$('#pp_nd_return_city').val($('#pp_pickup_city').val());
	$('input[name="nd_return_zipcode"]').val($('input[name="zipcode"]').val());
}



function toggle_category(showid, hideid, id) {
	$(id).val('');
	$(showid).attr('disabled', false);
	$(showid).show();
	$(hideid).attr('disabled', true);
	$(hideid).hide();
}

toggle_category('.travel-mode-air', '.travel-mode-ship');

function callback(response, status) {
	if (status == 'OK') {
		distance = response.rows[0].elements[0].distance.value;
	}
}



function scroll_to(id) {
	$('html, body').animate({
		scrollTop: $('#' + id).offset().top
	}, 'slow');
}
