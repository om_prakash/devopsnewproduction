function get_state22(country, stateid, cityid, loader, oldtext, city_old_text, isghana ,append_id,search) {
	if ($("#" + country).val() != "") {
		var country_json = eval('(' + $("#" + country).val() + ')');
		//console.log(country_json);
		alert(country_json.state_available);

		if (country_json.state_available == 0) {
			$('#' + stateid).prop('disabled', true);
			$('#' + stateid).val('');
			get_city(country, cityid, loader, city_old_text);
		} else {
			$("#" + loader).addClass('spinning');
			$('#pp_pickup_state'+append_id+"_chosen").addClass('spinning');			

			var input_data = 'data=' + $("#" + country).val() + '&oldtext=' + oldtext + '&isghana =' + isghana+'&append_id='+append_id;
			if (isghana != '') {
				var action_url = SITEURL + 'ghana-state-list';
			} else {
				var action_url = SITEURL + 'state-list2';
			}
			$.ajax({
				url: action_url,
				type: 'post',
				data: 'data=' + $("#" + country).val() + '&oldtext=' + oldtext+'&append_id='+append_id+'&search='+search, //
				success: function(obj) {
					$('#' + stateid).prop('disabled', false);
					$("#" + loader).removeClass('spinning');
					//$("#pp_pickup_state10_chosen").removeClass('spinning');
					$('#pp_pickup_state'+append_id+"_chosen").removeClass('spinning');
					//$("#" + stateid).html(obj);
					$("#ap_id"+append_id).html(obj);
					if(search != ''){
						$('.chosen-select').trigger('chosen:open');
					}
					
					
					$("#" + cityid).html('<option value="" style="color:#555">Select City</option>');
					if (oldtext != '') {

						$('#' + stateid).trigger('onchange');
					}

				}
			});
	
		}
	}
}
function get_city(state,cityid,loader,oldtext)
{
	alert('p');
	if($("#"+state).val() !="")
	{
		$("#"+loader).addClass('spinning');
		$.ajax({
			url    : SITEURL+'city-list',
			type   : 'post',
			data   : 'data='+$("#"+state).val()+'&oldtext='+oldtext,
			success: function(obj)
			{
				$("#"+loader).removeClass('spinning');
				$("#"+cityid).html(obj);
				$("#"+cityid).trigger("chosen:updated");				
			}
		});
	}
}