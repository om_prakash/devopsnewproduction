// Public function for state and city

function get_state(country,stateid,cityid,loader,oldtext,city_old_text)
{

	if($("#"+country).val() !="")
	{
		var country_json = eval('('+$("#"+country).val()+')');
console.log(country_json.state_available);
		if(country_json.state_available == 0)
		{
			$('#'+stateid).prop('disabled', true);
			$('#'+stateid).val('');
			get_city(country,cityid,loader,city_old_text);
		}
		else
		{
			$("#"+loader).addClass('spinning');
			$.ajax({
				url    : SITEURL+'state-list',
				type   : 'post',
				data   : 'data='+$("#"+country).val()+'&oldtext='+oldtext,
				success: function(obj)
				{
					$('#'+stateid).prop('disabled', false);
					$("#"+loader).removeClass('spinning');
					$("#"+stateid).html(obj);
					$("#"+cityid).html('<option value="">Select State</option>');
					if(oldtext != '') {
						$('#'+stateid).trigger('onchange');
					}
				}
			});
		}		
	}
}


function get_city(state,cityid,loader,oldtext)
{
	if($("#"+state).val() !="")
	{
		$("#"+loader).addClass('spinning');
		$.ajax({
			url    : SITEURL+'city-list',
			type   : 'post',
			data   : 'data='+$("#"+state).val()+'&oldtext='+oldtext,
			success: function(obj)
			{
				$("#"+loader).removeClass('spinning');
				$("#"+cityid).html(obj);				
			}
		});
	}
}
