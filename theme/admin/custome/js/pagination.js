function loadData(page,Howpage)
{
		
	var urlvalue = $("#urlvalue").val();
	urlvalue = SITEURL+urlvalue;
	var postvalue=$("#postvalue").val();
	var urisegmnt=$("#urisegmnt").val();
	var orderby=$("#orderby").val();
	var orderType=$("#orderType").val();
	
		if(typeof orderby!="undefined"){
			$('#loading').show();
		} 
		
		$.ajax
		({
			type: "POST",
			url: urlvalue,
			data: "page="+page+postvalue+"&uri="+urisegmnt+"&orderby="+orderby+"&orderType="+orderType+"&Pnum="+Howpage,
			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			success: function(msg)
			{	
				
				$('#loading').hide();				
				$("#containerdata").html(msg);
				$('#actionform').unbind('submit');		
				$(window).scrollTop($('.navbar-header').offset().top);		//move to header 
			}
		});
}

$('#containerdata').on('click',  '.pagination li.active', function()
{
	var page = $(this).attr('p');
	var howpage=$("#howpage").val();
	loadData(page,howpage);
});           

$('#containerdata').on('click',  '#go_btn', function()
{
	var page = parseInt($('.goto').val());
	var no_of_pages = parseInt($('.total').attr('a'));
		var howpage=$("#howpage").val();
	if(page != 0 && page <= no_of_pages)
		{
			loadData(page,howpage);
		}
	else
	{
		if(no_of_pages=='0'){
			$(".searcherror").html('There is no result so you can not search any more');
		}else{
			$(".searcherror").html('Enter a PAGE between 1 and '+no_of_pages);
		}
		$('.goto').val("").focus();
		return false;
	}
});
loadData(1,20);  // For first time page load default results

function Changeorder(orderby,orderType)
{
	$("#orderby").val(orderby);
	$("#orderType").val(orderType);
	var howpage=$("#howpage").val();
	
	if(howpage!="")
	{	
		loadData(1,howpage);
	}
	else 
	{
		loadData(1,10);
	}
	
} 
function Changepagnum()
{
	var CurPage=$("#CurPage").val();
	var howpage=$("#howpage").val();
	var urlvalue = SITEURL+'ajax/update_perpage';
	$.ajax
		({
			type: "GET",
			url: urlvalue,
			data: "Pnum="+howpage,
			success: function(msg1)
			{	
				loadData(1,howpage);		
			}
		});
}
function remove_record(url,rowid)
{	
	if(confirm('Are you sure you want to delete this record?') == true)
	{
		$('#row-'+rowid).addClass('relative-pos spinning');
		
		url = SITEURL+url;
		
		$.ajax
			({
				url: url,
				headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
				success: function(res)
				{					
					var obj = JSON.parse(res);
					if(obj.success == 1){
						
						$('#row-'+rowid).css({'background-color':'red'});
						$('#row-'+rowid).fadeOut('slow');
						$('#error_msg_section').html('<div class="alert alert-success">'+ obj.msg + '</div>');
					}else{
						$('#row-'+rowid).css({'background-color':'white'});
						$('#error_msg_section').html('<div class="alert alert-danger">'+ obj.msg + '</div>');
					}
				}
			});
	}
	return false;
}
function change_record_status(url,rowid)
{		
	url = SITEURL+url;
	$.ajax
		({
			url: url,
			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			success: function(res)
			{					
				var obj = JSON.parse(res);
				if(obj.success == 1){
					$('#row-'+rowid).remove();
					$('#error_msg_section').html('<div class="alert alert-success">'+ obj.msg + '</div>');
				}else{
					$('#error_msg_section').html('<div class="alert alert-danger">'+ obj.msg + '</div>');
				}
			}
		});
	return false;
}
 

$(document).ready(function(e) {
	$('.side-menu > .nav.navbar-nav.nav-left > li').mouseenter(function(){
		$(this).stop().addClass('open');
	});
		$('.side-menu > .nav.navbar-nav.nav-left > li').mouseleave(function(){
		$(this).stop().removeClass('open');
	});
	
	
	
	if($(window).width() <= 980){

	
}else{
		$('#menu1').mouseenter(function(){
		$('#menu1').stop().css({'display':'block'});
	});
		$('#menu1').mouseleave(function(){
		$('#menu1').stop().css({'display':'none'});
	});
	
	
		$('#menu2').mouseenter(function(){
		$('#menu2').stop().css({'display':'block'});
	});
		$('#menu2').mouseleave(function(){
		$('#menu2').stop().css({'display':'none'});
	});
	
	
		$('#menu3').mouseenter(function(){
		$('#menu3').stop().css({'display':'block'});
	});
		$('#menu3').mouseleave(function(){
		$('#menu3').stop().css({'display':'none'});
	});
	
	
		$('#menu4').mouseenter(function(){
		$('#menu4').stop().css({'display':'block'});
	});
		$('#menu4').mouseleave(function(){
		$('#menu4').stop().css({'display':'none'});
	});
	
	
		$('#menu5').mouseenter(function(){
		$('#menu5').stop().css({'display':'block'});
	});
		$('#menu5').mouseleave(function(){
		$('#menu5').stop().css({'display':'none'});
	});
	
	
		$('#menu6').mouseenter(function(){
		$('#menu6').stop().css({'display':'block'});
	});
		$('#menu6').mouseleave(function(){
		$('#menu6').stop().css({'display':'none'});
	});
	
	
		$('#menu7').mouseenter(function(){
		$('#menu7').stop().css({'display':'block'});
	});
		$('#menu7').mouseleave(function(){
		$('#menu7').stop().css({'display':'none'});
	});
	
	
		$('#menu8').mouseenter(function(){
		$('#menu8').stop().css({'display':'block'});
	});
		$('#menu8').mouseleave(function(){
		$('#menu8').stop().css({'display':'none'});
	});
	
	
	
		$('#menu9').mouseenter(function(){
		$('#menu9').stop().css({'display':'block'});
	});
		$('#menu9').mouseleave(function(){
		$('#menu9').stop().css({'display':'none'});
	});
	
	
		$('#menu10').mouseenter(function(){
		$('#menu10').stop().css({'display':'block'});
	});
		$('#menu10').mouseleave(function(){
		$('#menu10').stop().css({'display':'none'});
	});
 
 
 
 
		$('#dropdown-toggle1').mouseenter(function(){	
		$('#menu1').slideDown(300);
	});
		$('#dropdown-toggle1').mouseleave(function(){
		$('#menu1').slideUp(300);
	});


    	$('#dropdown-toggle2').mouseenter(function(){
		$('#menu2').slideDown(300);
	});
		$('#dropdown-toggle2').mouseleave(function(){
		$('#menu2').slideUp(300);
	});


    	$('#dropdown-toggle3').mouseenter(function(){
		$('#menu3').slideDown(300);
	});
		$('#dropdown-toggle3').mouseleave(function(){
		$('#menu3').slideUp(300);
	});


   		$('#dropdown-toggle4').mouseenter(function(){
		$('#menu4').slideDown(300);
	});
		$('#dropdown-toggle4').mouseleave(function(){
		$('#menu4').slideUp(300);
	});
	
	
		$('#dropdown-toggle5').mouseenter(function(){
		$('#menu5').slideDown(300);
	});
		$('#dropdown-toggle5').mouseleave(function(){
		$('#menu5').slideUp(300);
	});
	
	
	$('#dropdown-toggle6').mouseenter(function(){
		$('#menu6').slideDown(300);
	});
		$('#dropdown-toggle6').mouseleave(function(){
		$('#menu6').slideUp(300);
	});
	
	
	$('#dropdown-toggle7').mouseenter(function(){
		$('#menu7').slideDown(300);
	});
		$('#dropdown-toggle7').mouseleave(function(){
		$('#menu7').slideUp(300);
	});
	
	
	$('#dropdown-toggle8').mouseenter(function(){
		$('#menu8').slideDown(300);
	});
		$('#dropdown-toggle8').mouseleave(function(){
		$('#menu8').slideUp(300);
	});
	
	
	$('#dropdown-toggle9').mouseenter(function(){
		$('#menu9').slideDown(300);
	});
		$('#dropdown-toggle9').mouseleave(function(){
		$('#menu9').slideUp(300);
	});
	
	
	$('#dropdown-toggle10').mouseenter(function(){
		$('#menu10').slideDown(300);
	});
		$('#dropdown-toggle10,#menu10').mouseleave(function(){
		$('#menu10').slideUp(300);
	});
}});



$(window).resize(function() {
	if($(window).width() <= 768){
		$('.pagination-ul').addClass('pagination-sm');
		$('.displaypagselect').addClass('input-sm');
		$('.pagination-input1').addClass('input-sm');
		}else{
			$('.pagination-ul' ).removeClass('pagination-sm');
		};
});
