$(document).on('click change','input[name="chk_all_del1"]',function() { 
    var checkboxes = $('.chk_del');
    if($(this).is(':checked')) {
        //checkboxes.attr("checked" , true);
        checkboxes.not(this).prop('checked', this.checked);
        $("#actionbtn" ).prop( "disabled", false );
        for(i=0;i<$("#totalnotiid").val();i++)
        {
			$("#NoficationBox"+i).attr('style', 'background-color: #00ACD6;color: #ffffff');
		}
    } else {
        checkboxes.attr ( "checked" , false );
        $("#actionbtn" ).prop( "disabled", true );
        for(i=0;i<$("#totalnotiid").val();i++)
        {
			$("#NoficationBox"+i).attr('style', 'background-color: #ffffff; color: #333333');
		}
    }
});

function check_action()
{ 
	if($('input[type="checkbox"]').is(':checked') )
	{
       //$('#deleted').removeClass("btn btn-danger disabled").addClass("btn btn-danger"); 
		  $("#actionbtn" ).prop( "disabled", false );
    }
   else
   {
    	//$('#deleted').removeClass("btn btn-danger").addClass("btn btn-danger disabled"); 
    	$("#actionbtn" ).prop( "disabled", true );
   }         
}

function check_action_notification(ivalue)
{

	 document.getElementById("chk_all_del1").checked = false;
	
	if($("#chk_del_"+ivalue).is(':checked') )
	{
		  $("#NoficationBox"+ivalue).attr('style', 'background-color: #00ACD6;color: #ffffff');
	}
   else
   {
    	$("#NoficationBox"+ivalue).attr('style', 'background-color: #ffffff; color: #333333');
   }        		
}


function validate_noti()
 {	
  	flag=true;
	if($('input[type="checkbox"]').is(':checked') )
	{
		flag=true;
		$("#user_error").hide();
	}
	else
	{
		$("#user_error").show();
		flag=false;
	}
  	
  	return flag
 }
