var email_expr = /^[A-Z0-9\._%-]+@[A-Z0-9\.-]+\.[A-Z]{2,4}(?:(?:[,;][A-Z0-9\._%-]+@[A-Z0-9\.-]+))?$/i;
var numeric_expr = /^[0-9]+$/;
var float_expr = /^[0-9\.]+$/;
var numericWithDash = /^[0-9\-]+$/;
var latlong_expr = /^[0-9\-.]+$/;
var webExpr =/(http(s)?:\\)?([\w-]+\.)+[\w-]+[.com|.in|.org]+(\[\?%&=]*)?/;
var valid = {
required : function(f, m){
    var v = $('#'+f).val();
        if(v == '')
        {
            $('#er_'+f).html('<p>The '+m+' is required.</p>');
            $('#er_container_'+f).addClass('has-error');
            return false;
        }
        else
        {
            $('#er_'+f).html('');
            $('#er_container_'+f).removeClass('has-error');
            return true;
        }
    },
email : function(f,m){
var v = $('#'+f).val();
if(email_expr.test(v) == false){
$('#er_'+f).html('<p>The '+ m +' field must contain a valid email address.</p>');
return false;
}else{
$('#er_'+f).html('');
return true;
}
},
numeric : function(f,m){
    var v = $('#'+f).val();
    $('#er_'+f).html('');
    if(v != ''){
            if(numeric_expr.test(v) == false){
                $('#er_'+f).html('<p>The '+m+' must contain only numeric value.</p>');
                $('#er_container_'+f).addClass('has-error');
                return false;
            }else{
                $('#er_'+f).html('');
                $('#er_container_'+f).removeClass('has-error');
                return true;
            }
        }
    },
latlong : function(f,m){
    var v = $('#'+f).val();
    $('#er_'+f).html('');
    if(v != ''){
            if(latlong_expr.test(v) == false){
                $('#er_'+f).html('<p>The '+m+' must contain only numeric value.</p>');
                return false;
            }else{
                $('#er_'+f).html('');
                return true;
            }
        }
    },
    
float : function(f,m){
    var v = $('#'+f).val();
    $('#er_'+f).html('');
    if(v != ''){
            if(float_expr.test(v) == false){
                $('#er_'+f).html('<p>The '+m+' must contain only numeric value.</p>');
                return false;
            }else{
                $('#er_'+f).html('');
                return true;
            }
        }
    },
numericWithDash : function(f,m){
    var v = $('#'+f).val();
    $('#er_'+f).html('');
    if(v != ''){
            if(numericWithDash.test(v) == false){
                $('#er_'+f).html('<p>The '+m+' must contain only numeric value.</p>');
                return false;
            }else{
                $('#er_'+f).html('');
                return true;
            }
        }
    },
length : function(f,m,l){
var v = $('#'+f).val();
if(v.length != l) {
$('#er_'+f).html(m);
return false;
}else{
$('#er_'+f).html('');
return true;
}
},
match : function(f,f2,m){
var v = $('#'+f).val();
var v2 = $('#'+f2).val();
if(v != v2){
$('#er_'+f).html(m);
$('#er_container_'+f).addClass('has-error');
return false;
}else{
$('#er_'+f).html();
$('#er_container_'+f).removeClass('has-error');
return true;
}
},
minlength : function(f,m,l){
var v = $('#'+f).val();
if(v.length < l) {
$('#er_'+f).html(m);
return false;
}else{
$('#er_'+f).html('');
return true;
}
},
    maxlength : function(f,m,l) {
        var v = $('#'+f).val();
        if(v.length > l) {
            $('#er_'+f).html('<p>The '+m+' can not exceed '+l+' characters in length.');
            return false;
        }else{
            $('#er_'+f).html('');
            return true;
        }
    },
web : function(f,m){
var v = $('#'+f).val();
if(v != ''){
if(webExpr.test(v) == false){
$('#er_'+f).html('<p>The web url is not valid.</p>');
return false;
}else{
$('#er_'+f).html('');
return true;
}
}
}
};   
