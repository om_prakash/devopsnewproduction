webpackJsonp(["main"],{

/***/ "../../../../../src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../../../../../src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "../../../../../src/app/_directives/alert.component.html":
/***/ (function(module, exports) {

module.exports = "<style>\n    .successpop {\n        width: 350px;\n        height: auto;\n        top: 100px;\n        position: fixed;\n        z-index: 2050;\n        margin: 0 auto;\n        right: -150%;\n        border-radius: 10px;\n        -webkit-border-radius: 10px;\n        text-align: center;\n        padding: 20px;\n        box-shadow: 0 0 40px rgba(0, 0, 0, .2);\n        -webkit-box-shadow: 0 0 40px rgba(0, 0, 0, .2);\n        display: -webkit-flex;\n        align-content: center;\n        align-items: center;\n        -webkit-transition: all 0.3s ease-in-out 0s;\n        -moz-transition: all 0.3s ease-in-out 0s;\n        transition: all 0.3s ease-in-out 0s;\n    }\n    \n    .successpop.active {\n        right: 5%;\n    }\n    \n    .success_message {\n        background: #ffffff;\n        background: -moz-linear-gradient(-45deg, #ffffff 50%, #f3ffe1 50%);\n        background: -webkit-linear-gradient(-45deg, #ffffff 50%, #f3ffe1 50%);\n        background: linear-gradient(135deg, #ffffff 50%, #f3ffe1 50%);\n        filter: progid: DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#f3ffe1', GradientType=1);\n    }\n    \n    .faild_message {\n        background: #ffffff;\n        background: -moz-linear-gradient(-45deg, #ffffff 50%, #ffe9e9 50%);\n        background: -webkit-linear-gradient(-45deg, #ffffff 50%, #ffe9e9 50%);\n        background: linear-gradient(135deg, #ffffff 50%, #ffe9e9 50%);\n        filter: progid: DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#ffe9e9', GradientType=1);\n    }\n</style>\n\n<div *ngIf=\"alert_status == 'success'\">\n    <div class=\"successpop {{alert_css}}\" id=\"success_message\">\n        <div class=\"media\">\n            <img src=\"assets/images/smiely.svg\" width=\"60\" alt=\"\">\n            <div class=\"media-body\">\n                <h4 style=\"color:rgb(73, 184, 106);\">YAY!!</h4>\n                <p>{{alert_message}}</p>\n            </div>\n        </div>\n    </div>\n</div>\n\n<!-- faild alert popups -->\n<div *ngIf=\"alert_status == 'error'\">\n    <div class=\"successpop {{alert_css}}\" id=\"faild_message\">\n        <div class=\"media\">\n            <img class=\"mr-3\" src=\"assets/images/sad.svg\" width=\"60\" alt=\"\">\n            <div class=\"media-body\">\n                <h4 style=\"color:#f77070\">Ooops!!</h4>\n                <p>{{alert_message}}</p>\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/_directives/alert.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AlertComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models_index__ = __webpack_require__("../../../../../src/app/_models/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_index__ = __webpack_require__("../../../../../src/app/_services/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/*declare var $: any;*/
var AlertComponent = /** @class */ (function () {
    function AlertComponent(alertService) {
        this.alertService = alertService;
        this.alerts = [];
    }
    AlertComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.alertService.getAlert().subscribe(function (alert) {
            if (!alert) {
                // clear alerts when an empty alert is received
                _this.alerts = [];
                return;
            }
            if (__WEBPACK_IMPORTED_MODULE_1__models_index__["a" /* AlertType */].Success === alert.type) {
                _this.alert_status = 'success';
                _this.alert_message = alert.message;
                _this.alert_css = 'success_message active';
            }
            else {
                _this.alert_status = 'error';
                _this.alert_message = alert.message;
                _this.alert_css = 'faild_message active';
            }
            setTimeout(function () {
                _this.alert_css = '';
            }, 3000);
        });
    };
    AlertComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            moduleId: module.i,
            selector: 'alert',
            template: __webpack_require__("../../../../../src/app/_directives/alert.component.html")
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__services_index__["a" /* AlertService */]])
    ], AlertComponent);
    return AlertComponent;
}());



/***/ }),

/***/ "../../../../../src/app/_directives/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__alert_component__ = __webpack_require__("../../../../../src/app/_directives/alert.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__alert_component__["a"]; });



/***/ }),

/***/ "../../../../../src/app/_directives/number.directive.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NumberOnlyDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var NumberOnlyDirective = /** @class */ (function () {
    function NumberOnlyDirective(el) {
        this.el = el;
        // Allow decimal numbers and negative values
        this.regex = new RegExp(/^-?[0-9]+(\.[0-9]*){0,1}$/g);
        // Allow key codes for special events. Reflect :
        // Backspace, tab, end, home
        this.specialKeys = ['Backspace', 'Tab', 'End', 'Home', '-'];
    }
    NumberOnlyDirective.prototype.onKeyDown = function (event) {
        // Allow Backspace, tab, end, and home keys
        if (this.specialKeys.indexOf(event.key) !== -1) {
            return;
        }
        var current = this.el.nativeElement.value;
        var next = current.concat(event.key);
        if (next && !String(next).match(this.regex)) {
            event.preventDefault();
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["HostListener"])('keydown', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [KeyboardEvent]),
        __metadata("design:returntype", void 0)
    ], NumberOnlyDirective.prototype, "onKeyDown", null);
    NumberOnlyDirective = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"])({
            selector: '[myNumberOnly]'
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"]])
    ], NumberOnlyDirective);
    return NumberOnlyDirective;
}());



/***/ }),

/***/ "../../../../../src/app/_models/alert.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export Alert */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AlertType; });
/* unused harmony export Loader */
var Alert = /** @class */ (function () {
    function Alert() {
    }
    return Alert;
}());

var AlertType;
(function (AlertType) {
    AlertType[AlertType["Success"] = 0] = "Success";
    AlertType[AlertType["Error"] = 1] = "Error";
    AlertType[AlertType["Info"] = 2] = "Info";
    AlertType[AlertType["Warning"] = 3] = "Warning";
})(AlertType || (AlertType = {}));
var Loader = /** @class */ (function () {
    function Loader() {
    }
    return Loader;
}());



/***/ }),

/***/ "../../../../../src/app/_models/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__alert__ = __webpack_require__("../../../../../src/app/_models/alert.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__alert__["a"]; });



/***/ }),

/***/ "../../../../../src/app/_services/alert.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AlertService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Subject__ = __webpack_require__("../../../../rxjs/_esm5/Subject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__models_alert__ = __webpack_require__("../../../../../src/app/_models/alert.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AlertService = /** @class */ (function () {
    function AlertService(router) {
        var _this = this;
        this.router = router;
        this.subject = new __WEBPACK_IMPORTED_MODULE_2_rxjs_Subject__["Subject"]();
        this.keepAfterRouteChange = false;
        // clear alert messages on route change unless 'keepAfterRouteChange' flag is true
        router.events.subscribe(function (event) {
            if (event instanceof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* NavigationStart */]) {
                if (_this.keepAfterRouteChange) {
                    // only keep for a single route change
                    _this.keepAfterRouteChange = false;
                }
                else {
                    // clear alert messages
                    _this.clear();
                }
            }
        });
    }
    AlertService.prototype.getAlert = function () {
        return this.subject.asObservable();
    };
    AlertService.prototype.success = function (message, keepAfterRouteChange) {
        if (keepAfterRouteChange === void 0) { keepAfterRouteChange = false; }
        this.alert(__WEBPACK_IMPORTED_MODULE_3__models_alert__["a" /* AlertType */].Success, message, keepAfterRouteChange);
    };
    AlertService.prototype.error = function (message, keepAfterRouteChange) {
        if (keepAfterRouteChange === void 0) { keepAfterRouteChange = false; }
        this.alert(__WEBPACK_IMPORTED_MODULE_3__models_alert__["a" /* AlertType */].Error, message, keepAfterRouteChange);
    };
    AlertService.prototype.info = function (message, keepAfterRouteChange) {
        if (keepAfterRouteChange === void 0) { keepAfterRouteChange = false; }
        this.alert(__WEBPACK_IMPORTED_MODULE_3__models_alert__["a" /* AlertType */].Info, message, keepAfterRouteChange);
    };
    AlertService.prototype.warn = function (message, keepAfterRouteChange) {
        if (keepAfterRouteChange === void 0) { keepAfterRouteChange = false; }
        this.alert(__WEBPACK_IMPORTED_MODULE_3__models_alert__["a" /* AlertType */].Warning, message, keepAfterRouteChange);
    };
    AlertService.prototype.alert = function (type, message, keepAfterRouteChange) {
        if (keepAfterRouteChange === void 0) { keepAfterRouteChange = false; }
        this.keepAfterRouteChange = keepAfterRouteChange;
        this.subject.next({ type: type, message: message });
    };
    AlertService.prototype.clear = function () {
        // clear alerts
        this.subject.next();
    };
    AlertService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]])
    ], AlertService);
    return AlertService;
}());



/***/ }),

/***/ "../../../../../src/app/_services/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__alert_service__ = __webpack_require__("../../../../../src/app/_services/alert.service.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__alert_service__["a"]; });



/***/ }),

/***/ "../../../../../src/app/app-routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__index_index_component__ = __webpack_require__("../../../../../src/app/index/index.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__help_support_help_support_component__ = __webpack_require__("../../../../../src/app/help-support/help-support.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__help_support_detail_help_support_detail_component__ = __webpack_require__("../../../../../src/app/help-support-detail/help-support-detail.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__contact_contact_component__ = __webpack_require__("../../../../../src/app/contact/contact.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__section_detail_section_detail_component__ = __webpack_require__("../../../../../src/app/section-detail/section-detail.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var routes = [
    { path: '', component: __WEBPACK_IMPORTED_MODULE_2__index_index_component__["a" /* IndexComponent */] },
    { path: 'help-support', component: __WEBPACK_IMPORTED_MODULE_3__help_support_help_support_component__["a" /* HelpSupportComponent */] },
    { path: 'help-support/:section_faq_id', component: __WEBPACK_IMPORTED_MODULE_4__help_support_detail_help_support_detail_component__["a" /* HelpSupportDetailComponent */] },
    { path: 'contact', component: __WEBPACK_IMPORTED_MODULE_5__contact_contact_component__["a" /* ContactComponent */] },
    { path: 'section-detail/:section_id', component: __WEBPACK_IMPORTED_MODULE_6__section_detail_section_detail_component__["a" /* SectionDetailComponent */] },
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* RouterModule */].forRoot(routes)],
            exports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* RouterModule */]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "../../../../../src/app/app.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<!--The content below is only a placeholder and can be replaced.-->\n<alert></alert>\n<ng4-loading-spinner> </ng4-loading-spinner>\n<router-outlet></router-outlet>"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AppComponent = /** @class */ (function () {
    function AppComponent(router) {
        this.router = router;
        this.title = 'app';
    }
    AppComponent.prototype.ngOnInit = function () {
        var location = window.location.href;
        var location_check = location.split(":");
        if (location_check[0] == 'https') {
            this.router.navigate(['/']);
        }
    };
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-root',
            template: __webpack_require__("../../../../../src/app/app.component.html"),
            styles: [__webpack_require__("../../../../../src/app/app.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* Router */]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_videogular2_core__ = __webpack_require__("../../../../videogular2/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_videogular2_core___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_videogular2_core__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_videogular2_controls__ = __webpack_require__("../../../../videogular2/controls.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_videogular2_controls___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_videogular2_controls__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ng4_loading_spinner__ = __webpack_require__("../../../../ng4-loading-spinner/ng4-loading-spinner.umd.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ng4_loading_spinner___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_ng4_loading_spinner__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_ngx_embed_video__ = __webpack_require__("../../../../ngx-embed-video/dist/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_ngx_embed_video___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_ngx_embed_video__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__app_routing_module__ = __webpack_require__("../../../../../src/app/app-routing.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__index_index_component__ = __webpack_require__("../../../../../src/app/index/index.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__header_header_component__ = __webpack_require__("../../../../../src/app/header/header.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__footer_footer_component__ = __webpack_require__("../../../../../src/app/footer/footer.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__help_support_help_support_component__ = __webpack_require__("../../../../../src/app/help-support/help-support.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__aquantuo_service__ = __webpack_require__("../../../../../src/app/aquantuo.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__help_support_detail_help_support_detail_component__ = __webpack_require__("../../../../../src/app/help-support-detail/help-support-detail.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__contact_contact_component__ = __webpack_require__("../../../../../src/app/contact/contact.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__services_index__ = __webpack_require__("../../../../../src/app/_services/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__directives_index__ = __webpack_require__("../../../../../src/app/_directives/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__directives_number_directive__ = __webpack_require__("../../../../../src/app/_directives/number.directive.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__section_detail_section_detail_component__ = __webpack_require__("../../../../../src/app/section-detail/section-detail.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_9__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_19__directives_index__["a" /* AlertComponent */],
                __WEBPACK_IMPORTED_MODULE_11__index_index_component__["a" /* IndexComponent */],
                __WEBPACK_IMPORTED_MODULE_12__header_header_component__["a" /* HeaderComponent */],
                __WEBPACK_IMPORTED_MODULE_13__footer_footer_component__["a" /* FooterComponent */],
                __WEBPACK_IMPORTED_MODULE_14__help_support_help_support_component__["a" /* HelpSupportComponent */],
                __WEBPACK_IMPORTED_MODULE_16__help_support_detail_help_support_detail_component__["a" /* HelpSupportDetailComponent */],
                __WEBPACK_IMPORTED_MODULE_17__contact_contact_component__["a" /* ContactComponent */],
                __WEBPACK_IMPORTED_MODULE_20__directives_number_directive__["a" /* NumberOnlyDirective */],
                __WEBPACK_IMPORTED_MODULE_21__section_detail_section_detail_component__["a" /* SectionDetailComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["BrowserModule"],
                __WEBPACK_IMPORTED_MODULE_7_ng4_loading_spinner__["Ng4LoadingSpinnerModule"],
                __WEBPACK_IMPORTED_MODULE_4__angular_http__["HttpModule"],
                __WEBPACK_IMPORTED_MODULE_1__angular_router__["d" /* RouterModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_10__app_routing_module__["a" /* AppRoutingModule */],
                __WEBPACK_IMPORTED_MODULE_5_videogular2_core__["VgCoreModule"],
                __WEBPACK_IMPORTED_MODULE_6_videogular2_controls__["VgControlsModule"],
                __WEBPACK_IMPORTED_MODULE_8_ngx_embed_video__["EmbedVideo"].forRoot()
            ],
            providers: [__WEBPACK_IMPORTED_MODULE_15__aquantuo_service__["a" /* AquantuoService */], __WEBPACK_IMPORTED_MODULE_18__services_index__["a" /* AlertService */]],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_9__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "../../../../../src/app/aquantuo.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AquantuoService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AquantuoService = /** @class */ (function () {
    function AquantuoService(http) {
        this.http = http;
        this.fire = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    AquantuoService.prototype.show_loader = function () {
    };
    AquantuoService.prototype.hide_loader = function () {
    };
    //  --------------- SECTION LIST AND FAQ LIST  ----------------
    // search_faq(formdata: any){
    //   var headers = new Headers();
    //   headers.append('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
    //    let options = new RequestOptions({ headers: headers });
    //    return  this.http.get(environment.api_url+'faq-search?search_value='+formdata.search_value, options).map(response => response.json());
    // }
    //  --------------- SECTION LIST AND FAQ LIST  ----------------
    AquantuoService.prototype.home_page = function (search_value) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        headers.append('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["RequestOptions"]({ headers: headers });
        return this.http.get(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].api_url + 'home?search_value=' + search_value, options).map(function (response) { return response.json(); });
    };
    //  --------------- SECTION LIST AND FAQ LIST  ----------------
    AquantuoService.prototype.section_list = function () {
        var section_id = "";
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        headers.append('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["RequestOptions"]({ headers: headers });
        return this.http.get(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].api_url + 'section/list?section_id=' + section_id, options).map(function (response) { return response.json(); });
    };
    //  --------------- CLICK EVENT FAQ LIST  ----------------
    AquantuoService.prototype.faq_section_list = function (section_id) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        headers.append('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["RequestOptions"]({ headers: headers });
        return this.http.get(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].api_url + 'section/list?section_id=' + section_id, options).map(function (response) { return response.json(); });
    };
    //  --------------- SECTION FAQ DETAIL  ----------------
    AquantuoService.prototype.section_faq_detail = function (faq_id, ip_address, recently_view) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        headers.append('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["RequestOptions"]({ headers: headers });
        return this.http.get(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].api_url + 'section/detail?faq_id=' + faq_id + '&remote_address=' + ip_address + '&recently_view_question=' + recently_view, options).map(function (response) { return response.json(); });
    };
    //  --------------- ADD SUPPORT  ----------------
    AquantuoService.prototype.add_support = function (formdata, image, image_type) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        headers.append('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["RequestOptions"]({ headers: headers });
        var field = "name=" + formdata.name + "&email=" + formdata.email + "&request_type=" + formdata.request_type + "&phone_number=" + formdata.phone_number + "&description=" + formdata.description + "&user_profile_link="
            + formdata.user_profile_link + "&aquantuo_link=" + formdata.aquantuo_link + "&image=" + image + "&image_extention=" + image_type;
        return this.http.post(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].api_url + 'support', field, options)
            .map(function (response) { return response.json(); });
    };
    //  --------------- ADDRESS  ----------------
    AquantuoService.prototype.address = function () {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        headers.append('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["RequestOptions"]({ headers: headers });
        return this.http.get(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].api_url + 'address', options).map(function (response) { return response.json(); });
    };
    AquantuoService.prototype.faq_like = function () {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        headers.append('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["RequestOptions"]({ headers: headers });
        return this.http.get("https://api.ipify.org/?format=json", options).map(function (response) { return response.json(); });
    };
    AquantuoService.prototype.insert_faq_like = function (faq_id, remote_address, status) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        headers.append('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["RequestOptions"]({ headers: headers });
        return this.http.get(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].api_url + 'section/faqlike?faq_id=' + faq_id + '&remote_address=' + remote_address + '&status=' + status, options).map(function (response) { return response.json(); });
    };
    AquantuoService.prototype.search_faq = function (search_value) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["Headers"]();
        headers.append('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["RequestOptions"]({ headers: headers });
        return this.http.get(__WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].api_url + 'faq-search?search_value=' + search_value, options).map(function (response) { return response.json(); });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"])
    ], AquantuoService.prototype, "fire", void 0);
    AquantuoService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["Http"]])
    ], AquantuoService);
    return AquantuoService;
}());



/***/ }),

/***/ "../../../../../src/app/contact/contact.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/contact/contact.component.html":
/***/ (function(module, exports) {

module.exports = "<app-header>\n</app-header>\n\n<section class=\"banner_section faq_bg detail_bg\">\n    <div class=\"container\">\n        <div class=\"row justify-content-center pb-5\">\n            <div class=\"col-12 col-sm-8\">\n                <!-- <div class=\"input-group\">\n                    <div class=\"input-group input-group-lg mb-3\">\n                        <!-- <input type=\"text\" class=\"form-control\" placeholder=\"Search for help\" aria-label=\"Recipient's username\" aria-describedby=\"basic-addon2\">\n                        <div class=\"input-group-append\">\n                            <button class=\"btn btn-info\" type=\"button\">Search <img width=\"20\" src=\"assets/images/search.svg\" alt=\"...\"/></button>\n                        </div>\n                    </div>\n                </div> -->\n                <h2>Contact Us</h2>\n            </div>\n        </div>\n    </div>\n</section>\n<section class=\"page_section inner shade_bg\">\n    <div class=\"container\">\n        <div class=\"row\">\n            <div class=\"col-12 col-sm-6 white_bg\">\n\n                <div class=\"media\">\n                    <div class=\"cont_icon\">\n                        <img src=\"assets/images/location.svg\" width=\"20\" alt=\"...\" />\n                    </div>\n                    <div>\n                        <p><strong>Ghana:</strong>\n                            <!-- {{result?.address}} -->131 Race Course street, Abeka Accra ( opposite Derby Royal Hotel ).</p>\n                    </div>\n                </div>\n\n                <div class=\"media\">\n                    <div class=\"cont_icon\">\n                        <img src=\"assets/images/call.svg\" width=\"20\" alt=\"...\" />\n                    </div>\n                    <div>\n\n\n                        <!-- <p><strong>Ghana:</strong> <a target=\"_blank\" class=\"link\" href=\"tel:302434505\">(+233) 30 243 4505</a> or <a target=\"_blank\" class=\"link\" href=\"tel:554967571\">(+233) 55 496 7571</a> or <a target=\"_blank\" class=\"link\" href=\"tel:501634195\"> (+233) 50 163 4195</a></p> -->\n\n                        <p><strong>Ghana:</strong> <a target=\"_blank\" class=\"link\" href=\"tel:302434505\">(+233) 30 243 4505</a> or <a target=\"_blank\" class=\"link\" href=\"tel:501634195\">(+233) 50 163 4195</a> or <a target=\"_blank\" class=\"link\" href=\"tel:501634196\"> (+233) 50 163 4196</a></p>\n\n                        <!-- (+233) 30 243 4505 or (+233) 50 163 4195 or (+233) 50 163 4196 -->\n\n\n\n                        <!-- +{{result?.country_code}}-{{result?.phone_number}}-->\n\n                    </div>\n                </div>\n\n                <div class=\"media\">\n                    <div class=\"cont_icon\">\n                        <img src=\"assets/images/call.svg\" width=\"20\" alt=\"...\" />\n                    </div>\n                    <div>\n                        <p><strong>USA:</strong> <a target=\"_blank\" class=\"link\" href=\"tel:8886522233\"> (+1) 888 652 2233</a></p>\n\n                        <!-- +{{result?.country_code}}-{{result?.phone_number}}-->\n\n                    </div>\n                </div>\n                <div class=\"media\">\n                    <div class=\"cont_icon\">\n                        <img src=\"assets/images/clock.svg\" width=\"20\" alt=\"...\" />\n                    </div>\n                    <div>\n                        <p><strong>Office Hours:</strong>\n                           <!-- {{result?.address}} -->Mon to Fri : 8am to 5pm, Closed on weekends and public holidays.</p>\n                    </div>\n                </div>\n                <div class=\"media\">\n                    <div class=\"cont_icon\">\n                        <img src=\"assets/images/envelope.svg\" width=\"20\" alt=\"...\" />\n                    </div>\n                    <div>\n                        <p><strong>Support:</strong>\n                            <a target=\"_blank\" class=\"link\" href=\"mailto:{{result?.SupportEmail}}\">{{result?.SupportEmail}}</a></p>\n                    </div>\n                </div>\n\n                <div class=\"contact-social mt-4 \">\n                    <a href=\"https://www.facebook.com/Aquantuo\" target=\"_blank\" class=\"\"><i class=\"fa fa-facebook\"></i></a>\n                    <a href=\"https://twitter.com/Aquantuo\" target=\"_blank\" class=\"\"><i class=\"fa fa-twitter\"></i></a>\n                    <a href=\"https://www.instagram.com/Aquantuo/\" target=\"_blank\" class=\"\"><i class=\"fa fa-instagram\"></i></a>\n                </div>\n            </div>\n            <div class=\"col-12 col-sm-6 contact-field\">\n                <form (ngSubmit)=\"addSupport(loginForm.valid)\" #loginForm=\"ngForm\">\n                    <div class=\"form-group\">\n                        <input id=\"name\" required [(ngModel)]=\"model.name\" name=\"name\" type=\"text\" class=\"form-control\" placeholder=\"Your Name\" #name=\"ngModel\">\n                        <div style=\"color:red\" *ngIf=\"name.errors && (name.dirty || name.touched || loginForm.submitted)\">\n                            <p *ngIf=\"name.errors.required\">\n                                Name is required.\n                            </p>\n\n                        </div>\n                    </div>\n\n                    <div class=\"form-group\">\n                        <input id=\"email\" pattern=\"[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,8}$\" required [(ngModel)]=\"model.email\" name=\"email\" type=\"text\" class=\"form-control\" placeholder=\"Your Email Address\" #email=\"ngModel\">\n                        <div style=\"color:red\" *ngIf=\"email.errors && (email.dirty || email.touched || loginForm.submitted)\">\n                            <p *ngIf=\"email.errors.required\">\n                                Email is required.\n                            </p>\n                            <p *ngIf=\"email.errors.pattern\">\n                                Please enter valid email.\n                            </p>\n                        </div>\n                    </div>\n                    <div class=\"form-group\">\n                        <input id=\"phone_number\" pattern=\"[0-9]+\" minlength=\"6\" maxlength=\"15\" [(ngModel)]=\"model.phone_number\" required name=\"phone_number\" type=\"text\" class=\"form-control\" placeholder=\"Your Phone Number\" #phone_number=\"ngModel\">\n                        <div style=\"color:red\" *ngIf=\"phone_number.errors && (phone_number.dirty || phone_number.touched || loginForm.submitted)\">\n                            <p *ngIf=\"phone_number.errors.required\">\n                                Phone number is required.\n                            </p>\n                            <p *ngIf=\"phone_number.errors.pattern\">\n                                Please enter valid Phone number.\n                            </p>\n                            <p *ngIf=\"phone_number.errors.maxlength\">\n                                Please enter maximum 15 digits.\n                            </p>\n                            <p *ngIf=\"phone_number.errors.minlength\">\n                                Phone number should be between 6 to 15 digits.\n                            </p>\n\n\n                        </div>\n                    </div>\n\n\n\n                    <div class=\"form-group\">\n\n                        <select class=\"form-control\" required [(ngModel)]=\"model.request_type\" name=\"request_type\" id=\"request_type\" #request_type=\"ngModel\">\n                            \n                            <option class=\"select-item\" value=\"Question about aquantuo\">Question about Aquantuo</option>\n                            <option class=\"select-item\" value=\"Delivery issue\">Delivery issue</option>\n                            <option class=\"select-item\" value=\"Payment issue\">Payment issue</option>\n                            <option class=\"select-item\" value=\"Order delayed\">Order delayed</option>\n                            <option class=\"select-item\" value=\"Item not found\">Item not received</option>\n                            <option class=\"select-item\" value=\"Report user\"> Report user</option>\n                            <option class=\"select-item\" value=\"Driver not responding\">Transporter not responding</option>\n                            <option class=\"select-item\" value=\"Order not delivered\">Order not delivered</option>\n                            <option class=\"select-item\" value=\"Other\">Other</option>\n                        </select>\n                        <div style=\"color:red\" *ngIf=\"request_type.errors && (request_type.dirty || request_type.touched || loginForm.submitted)\">\n                            <p *ngIf=\"request_type.errors.required\">\n                                Request type is required.\n                            </p>\n                        </div>\n                    </div>\n\n                    <div class=\"form-group\">\n                        <textarea type=\"text\" name=\"description\" required class=\"form-control\" placeholder=\"Your message\" id=\"description\" [(ngModel)]=\"model.description\" #description=\"ngModel\">\n                            </textarea>\n                        <div style=\"color:red\" *ngIf=\"description.errors && (description.dirty || description.touched || loginForm.submitted)\">\n                            <p *ngIf=\"description.errors.required\">\n                                Description is required.\n                            </p>\n                        </div>\n                    </div>\n\n                    <!-- <div class=\"form-group\">\n                        <input type=\"text\" name=\"user_profile_link\" required class=\"form-control\" placeholder=\"Link to user profile\" [(ngModel)]=\"model.user_profile_link\" #user_profile_link=\"ngModel\">\n\n                    </div>\n\n                    <div class=\"form-group\">\n                        <input type=\"text\" name=\"aquantuo_link\" required class=\"form-control\" placeholder=\"Link to aquantuo\" [(ngModel)]=\"model.aquantuo_link\" #aquantuo_link=\"ngModel\">\n\n                    </div> -->\n\n                    <div class=\"form-group\">\n                        <div class=\"uploaded_img\">\n                            <img *ngIf=\"imgstatus == false\" [src]=\"'data:image/jpg;base64,'+base64textString\" alt=\"...\" height=\"80px\" width=\"80px\" onError=\"this.src='assets/images/pdf.png'\">\n                        </div>\n                        <span class=\"browse_btn\">\n                            \n                            <input id=\"file\" (change)=\"handleFileSelect($event)\" type=\"file\" class=\"\">\n                        </span>\n                    </div>\n\n                    <input type=\"submit\" class=\"btn btn-info\" value=\"Submit\">\n                </form>\n\n                <!-- {{loginForm.value | json}} -->\n            </div>\n        </div>\n    </div>\n</section>\n\n<app-footer></app-footer>"

/***/ }),

/***/ "../../../../../src/app/contact/contact.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__aquantuo_service__ = __webpack_require__("../../../../../src/app/aquantuo.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_index__ = __webpack_require__("../../../../../src/app/_services/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ng4_loading_spinner__ = __webpack_require__("../../../../ng4-loading-spinner/ng4-loading-spinner.umd.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ng4_loading_spinner___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_ng4_loading_spinner__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ContactComponent = /** @class */ (function () {
    function ContactComponent(aquantuoService, router, alertService, spinnerService) {
        this.aquantuoService = aquantuoService;
        this.router = router;
        this.alertService = alertService;
        this.spinnerService = spinnerService;
        // public phone_number: string;
        // public password: string;
        this.model = {};
        this.base64textString = '';
        this.submitted = false;
    }
    ContactComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.model.request_type = "Question about aquantuo";
        this.app_constant = __WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */];
        this.imgstatus = true;
        this.aquantuoService.address().subscribe(function (data) {
            if (data.status === 1) {
                // this.alertService.success('data.message');
                _this.aquantuoService.hide_loader();
                _this.result = data.result;
            }
            else {
                console.log(data.message);
            }
        });
    };
    ContactComponent.prototype.handleFileSelect = function (evt) {
        var files = evt.target.files;
        var file = files[0];
        var type = file.type;
        var image_type = type.split("/");
        this.image_type = image_type[1];
        if (files && file) {
            var reader = new FileReader();
            reader.onload = this._handleReaderLoaded.bind(this);
            reader.readAsBinaryString(file);
        }
    };
    ContactComponent.prototype._handleReaderLoaded = function (readerEvt) {
        var binaryString = readerEvt.target.result;
        this.base64textString = btoa(binaryString);
        this.imgstatus = false;
    };
    ContactComponent.prototype.addSupport = function (form) {
        var _this = this;
        //this.aquantuoService.show_loader();
        if (form == true) {
            this.aquantuoService.add_support(this.model, this.base64textString, this.image_type).subscribe(function (data) {
                if (data.status == 1) {
                    // window.location.href='http://support.aquantuo.com/contact';
                    // this.aquantuoService.hide_loader();
                    _this.alertService.success(data.message);
                    setTimeout(function () {
                        location.reload();
                    }, 2000);
                    // this.router.navigate(['/help-support']);
                }
                else {
                    _this.alertService.error(data.message);
                    //this.router.navigate(['/contact']);
                    console.log(data.message);
                }
            });
        }
    };
    ContactComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-contact',
            template: __webpack_require__("../../../../../src/app/contact/contact.component.html"),
            styles: [__webpack_require__("../../../../../src/app/contact/contact.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__aquantuo_service__["a" /* AquantuoService */], __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* Router */], __WEBPACK_IMPORTED_MODULE_5__services_index__["a" /* AlertService */], __WEBPACK_IMPORTED_MODULE_6_ng4_loading_spinner__["Ng4LoadingSpinnerService"]])
    ], ContactComponent);
    return ContactComponent;
}());



/***/ }),

/***/ "../../../../../src/app/footer/footer.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/footer/footer.component.html":
/***/ (function(module, exports) {

module.exports = "<footer>\n    <div class=\"container\">\n        <div class=\"row\">\n            <div class=\"col-12 col-sm-6\">\n                <div class=\"media\">\n                    <img class=\"mr-4\" width=\"100\" src=\"assets/images/footer-logo.png\" alt=\"...\" />\n                    <div class=\"media-body\">\n                        <p>We provide customized end to end air freight and sea freight services from the US to Ghana and from Ghana to the US for individuals, small and large businesses.</p>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-12 col-sm-3\">\n                <ul class=\"footer-list\">\n                    <li><a routerLink=''>- Home</a></li>\n                    <li><a routerLink='/help-support'>- Help & Support</a></li>\n                    <!-- <li><a href=\"#features\">- Features</a></li>\n                    <li><a href=\"#download\">- Download</a></li> -->\n                    <li><a routerLink='/contact'>- Contact</a></li>\n                </ul>\n            </div>\n            <div class=\"col-12 col-sm-3\">\n                <div class=\"social-media clearfix\">\n                    <a href=\"https://www.facebook.com/Aquantuo\" target=\"_blank\">\n                        <div class=\"social-icon-facebook\">\n                            <i class=\"fa fa-facebook\"></i>\n                        </div>\n                    </a>\n                    <a href=\"https://twitter.com/Aquantuo\" target=\"_blank\">\n                        <div class=\"social-icon-twitter\">\n                            <i class=\"fa fa-twitter\"></i>\n                        </div>\n                    </a>\n                    <a href=\"https://www.instagram.com/Aquantuo/\" target=\"_blank\">\n                        <div class=\"social-icon-instagram\">\n                            <i class=\"fa fa-instagram\"></i>\n                        </div>\n                    </a>\n                </div>\n            </div>\n        </div>\n    </div>\n    <div class=\"second-f \">\n        <div class=\"container\">\n            <div class=\"row clearfix\">\n                <div class=\"col-12 col-sm-6 \">\n                    Copyright © 2019 Aquantuo\n                </div>\n                <div class=\"col-12 col-sm-6 text-right\">\n                    <a href=\"https://aquantuo.com/terms-and-conditions\">Terms and Conditions</a>&nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;\n                    <a href=\"https://aquantuo.com/privacy-policy\">Privacy Policy</a>\n                </div>\n            </div>\n        </div>\n    </div>\n    <!--Start of Zendesk Chat Script-->\n\n</footer>"

/***/ }),

/***/ "../../../../../src/app/footer/footer.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FooterComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FooterComponent = /** @class */ (function () {
    function FooterComponent() {
    }
    FooterComponent.prototype.ngOnInit = function () {
    };
    FooterComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-footer',
            template: __webpack_require__("../../../../../src/app/footer/footer.component.html"),
            styles: [__webpack_require__("../../../../../src/app/footer/footer.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], FooterComponent);
    return FooterComponent;
}());



/***/ }),

/***/ "../../../../../src/app/header/header.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/header/header.component.html":
/***/ (function(module, exports) {

module.exports = "<header class=\"fixed-top\">\n    <div class=\"container\">\n        <div class=\"row\">\n            <div class=\"col\">\n                <nav class=\"navbar navbar-expand-lg\">\n                    <a class=\"navbar-brand\" routerLink=''><img class=\"w-logo\" width=\"300\" src=\"assets/images/logo.png\" alt=\"...\"> <img width=\"60\" class=\"m-logo\" src=\"assets/images/mobile-logo.png\" alt=\"...\"></a>\n                    <div class=\"navbar-nav  ml-auto float-right my-nav\">\n                        <div class=\"header_search\">\n\n                            <!-- <input class=\"serch-input\" (keyup)=\"searchSuggestaionFaq(search_value)\" [(ngModel)]=\"search_value\" id=\"header_search\" name=\"search_value\" type=\"text\" class=\"form-control\" placeholder=\"Search..\" required>\n                            <div class=\"suggestion_search_box\" id=\"suggestion_search\">\n                                <ul class=\"suggestion_list\">\n                                    <li *ngFor=\"let search_suggestaions of search_suggestaion; let i = index\">\n                                        <a (click)=\"setSearchValue(search_suggestaions.question)\">{{search_suggestaions.question  | slice:0:150}}</a>\n                                    </li>\n                                </ul>\n                            </div>\n                            <button (click)=\"searchFaq(search_value)\" type=\"button\"><i class=\"fa fa-search\"></i> </button> -->\n\n                        </div>\n\n                        <div class=\"dropdown\" id=\"dropdownMenuButton\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n                            <div class=\"menu-icon\">\n                                <style type=\"text/css\">\n                                    .st0 {\n                                        fill: none;\n                                        stroke: #fff;\n                                        stroke-linecap: round;\n                                        stroke-miterlimit: 10;\n                                        stroke-width: 2;\n                                    }\n                                    \n                                    .sidebar__toggle[rotate-icon=\"true\"] {\n                                        transform: rotate(90deg);\n                                    }\n                                </style>\n                                <span class=\"st0\"></span>\n                                <span class=\"st0\"></span>\n                                <span class=\"st0\"></span>\n                            </div>\n                            <div class=\"dropdown-menu dropdown-menu-right menu-drop\" aria-labelledby=\"dropdownMenuButton\">\n                                <!-- <a class=\"dropdown-item\" (click)=\"faqSectionList(service_lists.id)\" *ngFor=\"let service_lists of service_list\">\n                                    <div class=\"float-left\">{{service_lists.section_name}}</div>\n                                    <img src=\"{{app_constant.service_image_url}}{{service_lists.image}}\" width=\"40\" alt=\"...\" />\n                                </a> -->\n                                <a class=\"dropdown-item\" (click)=\"goCNN()\">\n                                    <div class=\"float-left\">Go to Aquantuo</div> <img src=\"assets/images/home.svg\" alt=\"...\" width=\"20\"></a>\n                                <a class=\"dropdown-item\" routerLink=\"/section-detail/1\">\n                                    <div class=\"float-left\">Popular Questions</div> <img src=\"assets/images/p_question_black.svg\" alt=\"...\" width=\"25\"></a>\n                                <a class=\"dropdown-item\" routerLink=\"/section-detail/2\">\n                                    <div class=\"float-left\">Get Started</div> <img src=\"assets/images/flag-black.svg\" alt=\"...\" width=\"20\"></a>\n                                <a class=\"dropdown-item\" routerLink='/section-detail/3'>\n                                    <div class=\"float-left\">General Info</div> <img src=\"assets/images/info-black.svg\" alt=\"...\" width=\"20\"></a>\n                                <a class=\"dropdown-item\" routerLink='/section-detail/4'>\n                                    <div class=\"float-left\">For Shoppers/Requesters</div> <img src=\"assets/images/shopping-bag-black.svg\" alt=\"...\" width=\"20\"></a>\n                                <a class=\"dropdown-item\" routerLink='/section-detail/5'>\n                                    <div class=\"float-left\">For Travelers</div> <img src=\"assets/images/departures-black.svg\" alt=\"...\" width=\"25\"></a>\n                                <a class=\"dropdown-item\" routerLink='/section-detail/6'>\n                                    <div class=\"float-left\">Payment</div> <img src=\"assets/images/credit-card-black.svg\" alt=\"...\" width=\"20\"></a>\n                                <a class=\"dropdown-item\" routerLink=\"/contact\">\n                                    <div class=\"float-left\">Contact Us</div> <img src=\"assets/images/phone-call-black.svg\" alt=\"...\" width=\"20\">\n                                </a>\n                            </div>\n                        </div>\n                    </div>\n                </nav>\n            </div>\n        </div>\n    </div>\n</header>\n\n<section class=\"banner_section faq_bg\" *ngIf=\"faq_list.length > 0\"></section>\n\n<section class=\"page_section inner\" *ngIf=\"faq_list.length > 0\">\n    <div class=\"container\">\n        <div class=\"row mt-5 justify-content-center\">\n            <div class=\"col-12 col-sm-10 col-lg-9 mt-4\">\n                <ul class=\"questions_list mt-4\">\n\n                    <li *ngFor=\"let faq_lists of faq_list\">\n                        <a routerLink='/help-support/{{faq_lists.id}}'>{{faq_lists.question | slice:0:200}}</a>\n                    </li>\n                </ul>\n            </div>\n\n        </div>\n    </div>\n</section>"

/***/ }),

/***/ "../../../../../src/app/header/header.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HeaderComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__aquantuo_service__ = __webpack_require__("../../../../../src/app/aquantuo.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_index__ = __webpack_require__("../../../../../src/app/_services/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_jquery_dist_jquery_min_js__ = __webpack_require__("../../../../jquery/dist/jquery.min.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_jquery_dist_jquery_min_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_jquery_dist_jquery_min_js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ng4_loading_spinner__ = __webpack_require__("../../../../ng4-loading-spinner/ng4-loading-spinner.umd.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ng4_loading_spinner___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_ng4_loading_spinner__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var HeaderComponent = /** @class */ (function () {
    function HeaderComponent(aquantuoService, alertService, spinnerService) {
        this.aquantuoService = aquantuoService;
        this.alertService = alertService;
        this.spinnerService = spinnerService;
        this.outputPath = 'https://aquantuo.com/';
        this.service_list = [];
        this.faq_list = [];
    }
    HeaderComponent.prototype.ngOnInit = function () {
        var _this = this;
        __WEBPACK_IMPORTED_MODULE_5_jquery_dist_jquery_min_js__('#suggestion_search').hide();
        this.app_constant = __WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */];
        this.aquantuoService.section_list().subscribe(function (data) {
            if (data.status === 1) {
                _this.loader = 1;
                _this.service_list = data.section_result;
                console.log(data.message);
            }
            else {
                _this.spinnerService.hide();
                console.log(data.message);
            }
        });
    };
    HeaderComponent.prototype.searchSuggestaionFaq = function (search_value) {
        var _this = this;
        if (search_value != "") {
            __WEBPACK_IMPORTED_MODULE_5_jquery_dist_jquery_min_js__('#suggestion_search').show();
            this.aquantuoService.search_faq(search_value).subscribe(function (data) {
                if (data.status === 1) {
                    _this.spinnerService.hide();
                    _this.loader = 1;
                    _this.search_suggestaion = data.faq_result;
                    // console.log(data.message);
                }
                else {
                    _this.spinnerService.hide();
                    _this.faq_list = [{ "question": "Check your spelling, or try a different search." }];
                    console.log(data.message);
                }
            });
        }
        else {
            __WEBPACK_IMPORTED_MODULE_5_jquery_dist_jquery_min_js__('#suggestion_search').hide();
        }
    };
    HeaderComponent.prototype.setSearchValue = function (set_search_value) {
        __WEBPACK_IMPORTED_MODULE_5_jquery_dist_jquery_min_js__('#header_search').val(set_search_value);
        __WEBPACK_IMPORTED_MODULE_5_jquery_dist_jquery_min_js__("#suggestion_search").hide();
    };
    HeaderComponent.prototype.searchFaq = function (search_value) {
        var _this = this;
        this.aquantuoService.search_faq(search_value).subscribe(function (data) {
            if (data.status === 1) {
                _this.aquantuoService.hide_loader();
                _this.loader = 1;
                _this.faq_list = data.faq_result;
                // console.log(data.message);
            }
            else {
                _this.faq_list = [{ "question": "Check your spelling, or try a different search." }];
                console.log(data.message);
            }
        });
    };
    HeaderComponent.prototype.goCNN = function () {
        window.location.href = 'http://aquantuo.com/';
    };
    HeaderComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-header',
            template: __webpack_require__("../../../../../src/app/header/header.component.html"),
            styles: [__webpack_require__("../../../../../src/app/header/header.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__aquantuo_service__["a" /* AquantuoService */], __WEBPACK_IMPORTED_MODULE_4__services_index__["a" /* AlertService */], __WEBPACK_IMPORTED_MODULE_6_ng4_loading_spinner__["Ng4LoadingSpinnerService"]])
    ], HeaderComponent);
    return HeaderComponent;
}());



/***/ }),

/***/ "../../../../../src/app/help-support-detail/help-support-detail.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/help-support-detail/help-support-detail.component.html":
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\n\n<section class=\"banner_section faq_bg detail_bg\">\n    <div class=\"container\">\n        <div class=\"row justify-content-center pb-5\">\n            <div class=\"col-12 col-sm-6\">\n                <div id=\"sticky-anchor\"></div>\n                <div class=\"input-group  search_box\" id=\"search-box\">\n                    <div class=\"input-group input-group-lg mb-3\">\n                        <input name=\"search_value\" autocomplete=\"off\" (keyup)=\"searchSuggestaionFaq()\" [(ngModel)]=\"search_value\" class=\"form-control sidebar fixedsticky\" type=\"text\" class=\"form-control\" placeholder=\"Search..\" id=\"search_value\">\n                        <div class=\"suggestion_search_box\">\n                            <ul class=\"suggestion_list \">\n                                <li *ngFor=\"let search_suggestaions of search_suggestaion; let i = index\">\n                                    <a (click)=\"setSearchValue(search_suggestaions.question)\">{{search_suggestaions.question  | slice:0:150}}</a>\n                                    <strong class=\"text-center\">{{search_suggestaions?.record_not_found}} </strong>\n                                </li>\n                            </ul>\n                        </div>\n                        <div class=\"input-group-append\">\n                            <button class=\"btn btn-info\" (click)=\"searchFaq()\" type=\"button\"><img width=\"20\" src=\"assets/images/search.svg\" alt=\"...\"/></button>\n                        </div>\n                    </div>\n                </div>\n                <!-- <input name=\"search_value\" [(ngModel)]=\"search_value\" class=\"form-control\" type=\"text\" class=\"form-control\" placeholder=\"Search..\">\n                        <div class=\"input-group-append\">\n                            <button class=\"btn btn-info\" (click)=\"searchFaq(search_value)\" type=\"button\">Search <img width=\"20\" src=\"assets/images/search.svg\" alt=\"...\"/></button>\n                        </div> -->\n                <h2> {{result?.section_name}} </h2>\n            </div>\n        </div>\n    </div>\n</section>\n<section class=\"page_section inner gray_bg\">\n    <div class=\"container\">\n        <div class=\"row\">\n\n\n            <div class=\"col-12 col-sm-7 col-lg-8\">\n                <div class=\"list-group-item mb-3 section-padding\">\n                    <h5 class=\"\" [innerHTML]=\"result?.question\"></h5>\n                    <p class=\"like_count\"><span class=\"text-gray\">{{result?.section_name}}</span>&nbsp; | &nbsp; <span class=\"text-gray\"><i class=\"fa fa-thumbs-up\"></i> {{result?.like_faq}}</span></p>\n                    <div class=\"clearfix\"></div>\n                    <p class=\"like_count\"> <span class=\"e2e-inner-html-bound\" [innerHTML]=\"answer\"></span></p>\n                    <div class=\"text-center mt-4 mb-4\">\n                        <div *ngIf=\"faq_like?.status == undefined \">\n                            <span class=\"like_box\" (click)=\"faqLike(result?.id, 'like')\"><i class=\"fa fa-thumbs-up\"></i></span>\n                            <span class=\"like_box\" (click)=\"faqLike(result?.id, 'unlike')\"><i class=\"fa fa-thumbs-down\"></i></span>\n                        </div>\n                        <div *ngIf=\"faq_like?.status == 'like'\">\n                            <span class=\"like_box active\" (click)=\"faqLike(result.id, 'like')\"><i class=\"fa fa-thumbs-up\"></i></span>\n                            <span class=\"like_box\" (click)=\"faqLike(result.id, 'unlike')\"><i class=\"fa fa-thumbs-down\"></i></span>\n                        </div>\n                        <div *ngIf=\"faq_like?.status == 'unlike'\">\n                            <span class=\"like_box \" (click)=\"faqLike(result.id, 'like') \"><i class=\"fa fa-thumbs-up \"></i></span>\n                            <span class=\"like_box active \" (click)=\"faqLike(result.id, 'unlike') \"><i class=\"fa fa-thumbs-down \"></i></span>\n                        </div>\n                    </div>\n                    <ul class=\"questions_list mt-4\">\n                        <li *ngFor=\"let faq_lists of faq_list\">\n                            <a routerLink='/help-support/{{faq_lists.id}}' (click)=\"ngOnInit()\"><strong>Q. </strong>{{faq_lists.question | slice:0:100}}\n                            <br/>\n                            <strong>Ans. </strong><span class=\"capitalFirstText\" [innerHTML]=\"faq_lists?.answer | slice:0:200\">...</span>\n                            </a>\n                        </li>\n                    </ul>\n                    <h5>{{faq_list[0]?.record_not_found}} </h5>\n                </div>\n                <div class=\"col-12 col-sm-10 col-lg-9 mt-4\">\n\n\n                </div>\n                <div class=\"search_suggest\"></div>\n            </div>\n            <div class=\"col-12 col-sm-5 col-lg-4\">\n                <div class=\"\">\n                    <ul class=\"white-box list-group mb-3 related_question\">\n                        <h5 class=\" text-transform section-padding question-heading\">Related Questions</h5>\n                        <li class=\"\" *ngFor=\"let related_questions of related_question\">\n                            <a routerLink='/help-support/{{related_questions.id}}'>{{related_questions.question | slice:0:150}}</a>\n                        </li>\n                    </ul>\n                </div>\n                <ul class=\"white-box list-group mt-3\">\n                    <h5 class=\"text-transform section-padding question-heading\">Recently Viewed Questions</h5>\n                    <li class=\"\" *ngFor=\"let recently_viewes of recently_viewed\">\n                        <a routerLink='/help-support/{{recently_viewes.id}}'>{{recently_viewes.question | slice:0:150}}</a>\n                    </li>\n                    <br/>\n                    <strong class=\"\">{{record_not_found[0]?.record_not_found}} </strong>\n                </ul>\n\n                <!-- <div id=\"frame_1\" [innerHtml]=\"iframe_html\"></div> -->\n                <!-- <iframe src=\"https://www.youtube.com/embed/v=iHhcHTlGtRs\" width=\"425\" height=\"350\"></iframe> -->\n\n            </div>\n        </div>\n    </div>\n</section>\n\n<app-footer></app-footer>"

/***/ }),

/***/ "../../../../../src/app/help-support-detail/help-support-detail.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HelpSupportDetailComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__aquantuo_service__ = __webpack_require__("../../../../../src/app/aquantuo.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__ = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_index__ = __webpack_require__("../../../../../src/app/_services/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ng4_loading_spinner__ = __webpack_require__("../../../../ng4-loading-spinner/ng4-loading-spinner.umd.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ng4_loading_spinner___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_ng4_loading_spinner__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_ngx_embed_video__ = __webpack_require__("../../../../ngx-embed-video/dist/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_ngx_embed_video___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_ngx_embed_video__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_jquery_dist_jquery_min_js__ = __webpack_require__("../../../../jquery/dist/jquery.min.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_jquery_dist_jquery_min_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_jquery_dist_jquery_min_js__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var HelpSupportDetailComponent = /** @class */ (function () {
    function HelpSupportDetailComponent(router, sanitizer, aquantuoService, embedService, route, http, alertService, spinnerService) {
        this.router = router;
        this.sanitizer = sanitizer;
        this.aquantuoService = aquantuoService;
        this.embedService = embedService;
        this.route = route;
        this.http = http;
        this.alertService = alertService;
        this.spinnerService = spinnerService;
        this.array = [];
        this.youtubeUrl = "https://www.youtube.com/watch?v=iHhcHTlGtRs";
    }
    HelpSupportDetailComponent.prototype.ngOnInit = function () {
        var _this = this;
        __WEBPACK_IMPORTED_MODULE_9_jquery_dist_jquery_min_js__('.search_box_list').hide();
        __WEBPACK_IMPORTED_MODULE_9_jquery_dist_jquery_min_js__('.suggestion_search_box').hide();
        __WEBPACK_IMPORTED_MODULE_9_jquery_dist_jquery_min_js__('body').click(function () {
            __WEBPACK_IMPORTED_MODULE_9_jquery_dist_jquery_min_js__('.suggestion_search_box').hide();
        });
        var local_storage = [];
        var a = [];
        this.sub = this.route.params.subscribe(function (params) {
            _this.section_faq_id = params['section_faq_id'];
            // GET IP ADDRESS 
            _this.aquantuoService.faq_like().subscribe(function (ip) {
                //  CALL FAQ DETAIL API
                var recently_view_question_id = localStorage.getItem('local_storage');
                if (recently_view_question_id != "") {
                    _this.spinnerService.hide();
                    var aa = _this.section_faq_id + "," + recently_view_question_id;
                    localStorage.setItem('local_storage', aa);
                }
                else {
                    _this.spinnerService.hide();
                    localStorage.setItem('local_storage', _this.section_faq_id);
                }
                _this.aquantuoService.section_faq_detail(_this.section_faq_id, ip.ip, recently_view_question_id).subscribe(function (data) {
                    _this.spinnerService.show();
                    if (data.status === 1) {
                        __WEBPACK_IMPORTED_MODULE_9_jquery_dist_jquery_min_js__(".questions_list").hide();
                        __WEBPACK_IMPORTED_MODULE_9_jquery_dist_jquery_min_js__(".like_count").show();
                        __WEBPACK_IMPORTED_MODULE_9_jquery_dist_jquery_min_js__(".like_box").show();
                        _this.record_not_found = [];
                        //---- JS Search Box
                        _this.spinnerService.hide();
                        __WEBPACK_IMPORTED_MODULE_9_jquery_dist_jquery_min_js__('.search_box_list').hide();
                        __WEBPACK_IMPORTED_MODULE_9_jquery_dist_jquery_min_js__("#search_value").val("");
                        // ---- Result For Html Page
                        _this.result = data.result;
                        _this.answer = _this.sanitizer.bypassSecurityTrustHtml(data.result.answer);
                        _this.iframe_html = _this.embedService.embed(_this.youtubeUrl);
                        _this.faq_list = [];
                        //---- Related question
                        if (data.related_question != "") {
                            __WEBPACK_IMPORTED_MODULE_9_jquery_dist_jquery_min_js__(".related_question").show();
                            _this.related_question = data.related_question;
                        }
                        else {
                            __WEBPACK_IMPORTED_MODULE_9_jquery_dist_jquery_min_js__(".related_question").hide();
                            _this.related_question = [{ "question": "Related question not found" }];
                        }
                        //----  Faq like or Unlike
                        if (data.faq_like != "") {
                            _this.faq_like = data.faq_like;
                        }
                        else {
                            _this.faq_like = [{ 'status': "" }];
                        }
                        //------  Recently Viewed Question
                        if (data.recently_viewed != "") {
                            _this.recently_viewed = data.recently_viewed;
                        }
                        else {
                            _this.record_not_found = [{ 'record_not_found': "Recent Question Not Found." }];
                        }
                    }
                    else {
                        _this.spinnerService.hide();
                        _this.alertService.error(data.message);
                        console.log(data.message);
                    }
                });
            });
        });
    };
    HelpSupportDetailComponent.prototype.faqLike = function (faq_id, status) {
        var _this = this;
        this.aquantuoService.faq_like().subscribe(function (data) {
            _this.spinnerService.show();
            if (data.ip != "") {
                _this.spinnerService.hide();
                _this.aquantuoService.insert_faq_like(faq_id, data.ip, status).subscribe(function (data) {
                    if (data.faq_like != "" || data.faq_like != undefined) {
                        if (data.status == 1) {
                            _this.alertService.success(data.message);
                            _this.faq_like = data.faq_like;
                            _this.result.like_faq = data.result.like_faq;
                        }
                        else {
                            // this.alertService.error(data.message);
                            _this.faq_like = [{ 'status': undefined }];
                        }
                    }
                    else {
                        _this.spinnerService.hide();
                        _this.alertService.error(data.message);
                        _this.faq_like = [{ 'status': undefined }];
                    }
                });
            }
        });
    };
    HelpSupportDetailComponent.prototype.searchSuggestaionFaq = function () {
        var _this = this;
        var search_value = __WEBPACK_IMPORTED_MODULE_9_jquery_dist_jquery_min_js__('#search_value').val();
        if (search_value != "") {
            __WEBPACK_IMPORTED_MODULE_9_jquery_dist_jquery_min_js__('.suggestion_search_box').show();
            this.aquantuoService.search_faq(search_value).subscribe(function (data) {
                if (data.status === 1) {
                    _this.loader = 1;
                    _this.search_suggestaion = data.faq_result;
                    // console.log(data.message);
                }
                else {
                    _this.search_suggestaion = [{ "record_not_found": "No search results" }];
                    console.log(data.message);
                }
            });
        }
        else {
            __WEBPACK_IMPORTED_MODULE_9_jquery_dist_jquery_min_js__('.suggestion_search_box').hide();
        }
    };
    HelpSupportDetailComponent.prototype.setSearchValue = function (search_value) {
        __WEBPACK_IMPORTED_MODULE_9_jquery_dist_jquery_min_js__('#search_value').val(search_value);
        __WEBPACK_IMPORTED_MODULE_9_jquery_dist_jquery_min_js__(".suggestion_search_box").hide();
        this.searchFaq();
    };
    HelpSupportDetailComponent.prototype.searchFaq = function () {
        //   $('html, body').animate({
        //     scrollTop: $(".page_section.inner").offset().top
        // }, 1000);
        var _this = this;
        var search_value = __WEBPACK_IMPORTED_MODULE_9_jquery_dist_jquery_min_js__('#search_value').val();
        if (search_value != "") {
            this.spinnerService.show();
            this.aquantuoService.search_faq(search_value).subscribe(function (data) {
                if (data.status === 1) {
                    _this.spinnerService.hide();
                    __WEBPACK_IMPORTED_MODULE_9_jquery_dist_jquery_min_js__(".questions_list").show();
                    _this.faq_list = data.faq_result;
                    _this.result = [];
                    __WEBPACK_IMPORTED_MODULE_9_jquery_dist_jquery_min_js__(".like_box").hide();
                    __WEBPACK_IMPORTED_MODULE_9_jquery_dist_jquery_min_js__(".like_count").hide();
                    __WEBPACK_IMPORTED_MODULE_9_jquery_dist_jquery_min_js__('.search_box_list').show();
                    // console.log(data.message);
                }
                else {
                    __WEBPACK_IMPORTED_MODULE_9_jquery_dist_jquery_min_js__(".questions_list").hide();
                    __WEBPACK_IMPORTED_MODULE_9_jquery_dist_jquery_min_js__(".like_box").hide();
                    __WEBPACK_IMPORTED_MODULE_9_jquery_dist_jquery_min_js__(".like_count").hide();
                    __WEBPACK_IMPORTED_MODULE_9_jquery_dist_jquery_min_js__('.search_box_list').show();
                    _this.result = [];
                    _this.answer = [];
                    _this.spinnerService.hide();
                    _this.faq_list = [{ "record_not_found": " We couldn't find anything to match your search for: " + search_value }];
                    console.log(data.message);
                }
            });
        }
        else {
            this.alertService.error("Please enter search keyword to perform search");
            this.ngOnInit();
        }
    };
    HelpSupportDetailComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-help-support-detail',
            template: __webpack_require__("../../../../../src/app/help-support-detail/help-support-detail.component.html"),
            styles: [__webpack_require__("../../../../../src/app/help-support-detail/help-support-detail.component.css")],
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__angular_router__["c" /* Router */], __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__["DomSanitizer"], __WEBPACK_IMPORTED_MODULE_1__aquantuo_service__["a" /* AquantuoService */], __WEBPACK_IMPORTED_MODULE_8_ngx_embed_video__["EmbedVideoService"], __WEBPACK_IMPORTED_MODULE_4__angular_router__["a" /* ActivatedRoute */], __WEBPACK_IMPORTED_MODULE_3__angular_http__["Http"], __WEBPACK_IMPORTED_MODULE_6__services_index__["a" /* AlertService */], __WEBPACK_IMPORTED_MODULE_7_ng4_loading_spinner__["Ng4LoadingSpinnerService"]])
    ], HelpSupportDetailComponent);
    return HelpSupportDetailComponent;
}());



/***/ }),

/***/ "../../../../../src/app/help-support/help-support.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/help-support/help-support.component.html":
/***/ (function(module, exports) {

module.exports = "<app-header>\n</app-header>\n<style>\n\n</style>\n<section class=\"banner_section faq_bg\">\n    <div class=\"container\">\n        <div class=\"row justify-content-center pb-5\">\n            <div class=\"col-12 col-sm-6\">\n                <div id=\"sticky-anchor\"></div>\n                <div class=\"input-group  search_box\" id=\"search-box\">\n                    <div class=\"input-group input-group-lg mb-3\">\n                        <input name=\"search_value\" autocomplete=\"off\" (keyup)=\"searchSuggestaionFaq()\" [(ngModel)]=\"search_value\" class=\"form-control sidebar fixedsticky\" type=\"text\" class=\"form-control\" placeholder=\"Search..\" id=\"search_value\">\n                        <div class=\"suggestion_search_box\">\n                            <ul class=\"suggestion_list \">\n                                <li *ngFor=\"let search_suggestaions of search_suggestaion; let i = index\">\n                                    <strong class=\"text-center\">{{search_suggestaions?.record_not_found}} </strong>\n                                    <a (click)=\"setSearchValue(search_suggestaions.question)\">{{search_suggestaions.question  | slice:0:150}}</a>\n\n                                </li>\n                            </ul>\n                        </div>\n                        <div class=\"input-group-append\">\n                            <button class=\"btn btn-info\" (click)=\"searchFaq()\" type=\"button\"> <img width=\"20\" src=\"assets/images/search.svg\" alt=\"...\"/></button>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</section>\n\n<section class=\"page_section inner\">\n    <div class=\"container\">\n        <div class=\"row mt-5 justify-content-center\">\n            <div class=\"m-col col-sm-2 text-center\" *ngFor=\"let service_lists of service_list\">\n                <div class=\"link_circle\" id=\"link_circle_{{service_lists.id}}\">\n                    <a id=\"link_help_{{service_lists.id}}\" (click)=\"faqSectionList(service_lists.id)\">\n                        <img src=\"{{app_constant.service_image_url}}{{service_lists.image}}\" width=\"40\" alt=\"...\" />\n                    </a>\n                </div>\n                <p> {{service_lists?.section_name | uppercase}}</p>\n\n            </div>\n        </div>\n\n        <div class=\"row mt-5 justify-content-center\">\n            <div class=\"col-12 col-sm-10 col-lg-9 mt-4\">\n\n                <h2 class=\"text-uppercase text-center pb-3 m-h1\"> {{faq_list[0]?.section_name | uppercase}}</h2>\n                <h4 class=\"text-uppercase text-center pb-3 m-h1 search_result\"> {{search_result}}</h4>\n                <ul class=\"questions_list mt-4\">\n\n                    <li *ngFor=\"let faq_lists of faq_list\" class=\"index_question\">\n                        <a routerLink='/help-support/{{faq_lists.id}}' id=\"question_{{faq_lists.id}}\" (mouseenter)=\"collapseAnswer(faq_lists.id)\"><strong>Q. </strong>{{faq_lists.question | slice:0:100}} </a>\n                    </li>\n                    <li *ngFor=\"let faq_lists of search_faq_list; let i = index\" class=\"search_index_question\">\n                        <a routerLink='/help-support/{{faq_lists.id}}'><strong>Q. </strong><span class=\"capitalFirstText\" [innerHTML] = \"faq_lists?.question | slice:0:100\"></span> \n                        <br/>\n                        <strong>Ans. </strong><span class=\"capitalFirstText\" [innerHTML]=\"faq_lists?.answer | slice:0:200\">...</span>\n                    </a>\n                    </li>\n                </ul>\n                <h5>{{record_not_found[0]?.record_not_found}} </h5>\n            </div>\n\n        </div>\n    </div>\n</section>\n<app-footer></app-footer>"

/***/ }),

/***/ "../../../../../src/app/help-support/help-support.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HelpSupportComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__aquantuo_service__ = __webpack_require__("../../../../../src/app/aquantuo.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_jquery_dist_jquery_min_js__ = __webpack_require__("../../../../jquery/dist/jquery.min.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_jquery_dist_jquery_min_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_jquery_dist_jquery_min_js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng4_loading_spinner__ = __webpack_require__("../../../../ng4-loading-spinner/ng4-loading-spinner.umd.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng4_loading_spinner___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_ng4_loading_spinner__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_index__ = __webpack_require__("../../../../../src/app/_services/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var HelpSupportComponent = /** @class */ (function () {
    function HelpSupportComponent(aquantuoService, alertService, spinnerService) {
        this.aquantuoService = aquantuoService;
        this.alertService = alertService;
        this.spinnerService = spinnerService;
        this.service_list = [];
        this.faq_list = [];
    }
    HelpSupportComponent.prototype.ngOnInit = function () {
        var _this = this;
        __WEBPACK_IMPORTED_MODULE_4_jquery_dist_jquery_min_js__(".questions_list").show();
        __WEBPACK_IMPORTED_MODULE_4_jquery_dist_jquery_min_js__(".index_question").show();
        __WEBPACK_IMPORTED_MODULE_4_jquery_dist_jquery_min_js__('.suggestion_search_box').hide();
        __WEBPACK_IMPORTED_MODULE_4_jquery_dist_jquery_min_js__('body').click(function () {
            __WEBPACK_IMPORTED_MODULE_4_jquery_dist_jquery_min_js__('.suggestion_search_box').hide();
        });
        this.app_constant = __WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */];
        this.aquantuoService.section_list().subscribe(function (data) {
            _this.spinnerService.show();
            if (data.status == 1) {
                _this.spinnerService.hide();
                _this.loader = 1;
                _this.service_list = data.section_result;
                _this.faq_list = data.faq_result;
                _this.search_faq_list = [];
                _this.record_not_found = [];
                console.log(data.message);
            }
            else {
                __WEBPACK_IMPORTED_MODULE_4_jquery_dist_jquery_min_js__(".questions_list").hide();
                _this.spinnerService.hide();
                console.log(data.message);
            }
        });
    };
    HelpSupportComponent.prototype.faqSectionList = function (section_id) {
        var _this = this;
        __WEBPACK_IMPORTED_MODULE_4_jquery_dist_jquery_min_js__(".questions_list").show();
        __WEBPACK_IMPORTED_MODULE_4_jquery_dist_jquery_min_js__('html, body').animate({
            scrollTop: __WEBPACK_IMPORTED_MODULE_4_jquery_dist_jquery_min_js__(".page_section.inner").offset().top
        }, 1000);
        __WEBPACK_IMPORTED_MODULE_4_jquery_dist_jquery_min_js__('.link_circle').removeClass('active');
        this.aquantuoService.faq_section_list(section_id).subscribe(function (data) {
            _this.spinnerService.show();
            __WEBPACK_IMPORTED_MODULE_4_jquery_dist_jquery_min_js__('#link_circle_' + section_id).addClass('active');
            if (data.status === 1) {
                __WEBPACK_IMPORTED_MODULE_4_jquery_dist_jquery_min_js__(".search_result").hide();
                _this.spinnerService.hide();
                _this.loader = 1;
                // this.service_list = data.section_result;
                if (data.faq_result.length > 0) {
                    _this.faq_list = data.faq_result;
                }
                else {
                    __WEBPACK_IMPORTED_MODULE_4_jquery_dist_jquery_min_js__(".questions_list").hide();
                    _this.faq_list = [{ "record_not_found": "Record Not Found" }];
                }
            }
            else {
                _this.spinnerService.hide();
                __WEBPACK_IMPORTED_MODULE_4_jquery_dist_jquery_min_js__(".questions_list").hide();
                _this.faq_list = [{ "record_not_found": "Record Not Found" }];
                console.log(data.message);
            }
        });
    };
    HelpSupportComponent.prototype.searchFaq = function () {
        var _this = this;
        __WEBPACK_IMPORTED_MODULE_4_jquery_dist_jquery_min_js__('html, body').animate({
            scrollTop: __WEBPACK_IMPORTED_MODULE_4_jquery_dist_jquery_min_js__(".page_section.inner").offset().top
        }, 1000);
        var search_value = __WEBPACK_IMPORTED_MODULE_4_jquery_dist_jquery_min_js__('#search_value').val();
        if (search_value != "") {
            this.aquantuoService.search_faq(search_value).subscribe(function (data) {
                _this.spinnerService.show();
                if (data.status == 1) {
                    __WEBPACK_IMPORTED_MODULE_4_jquery_dist_jquery_min_js__(".questions_list").show();
                    __WEBPACK_IMPORTED_MODULE_4_jquery_dist_jquery_min_js__(".search_result").hide();
                    __WEBPACK_IMPORTED_MODULE_4_jquery_dist_jquery_min_js__(".index_question").hide();
                    _this.spinnerService.hide();
                    _this.search_faq_list = data.faq_result;
                    _this.faq_list = [];
                    _this.record_not_found = [];
                    _this.search_result = "Search Result";
                    console.log(data.message);
                }
                else {
                    __WEBPACK_IMPORTED_MODULE_4_jquery_dist_jquery_min_js__(".questions_list").hide();
                    _this.spinnerService.hide();
                    _this.record_not_found = [{ "section_name": "Search Result", "record_not_found": " We couldn't find anything to match your search for: " + search_value }];
                    // this.search_result = [{"search_result":"Search Result"}];
                    console.log(data.message);
                }
            });
        }
        else {
            this.alertService.error("Please enter search keyword to perform search");
            this.ngOnInit();
        }
    };
    HelpSupportComponent.prototype.searchSuggestaionFaq = function () {
        var _this = this;
        var search_value = __WEBPACK_IMPORTED_MODULE_4_jquery_dist_jquery_min_js__('#search_value').val();
        if (search_value != "") {
            __WEBPACK_IMPORTED_MODULE_4_jquery_dist_jquery_min_js__('.suggestion_search_box').show();
            this.aquantuoService.search_faq(search_value).subscribe(function (data) {
                if (data.status === 1) {
                    _this.loader = 1;
                    _this.search_suggestaion = data.faq_result;
                    // console.log(data.message);
                }
                else {
                    _this.search_suggestaion = [{ "record_not_found": "No search results" }];
                    console.log(data.message);
                }
            });
        }
        else {
            __WEBPACK_IMPORTED_MODULE_4_jquery_dist_jquery_min_js__('.suggestion_search_box').hide();
        }
    };
    HelpSupportComponent.prototype.setSearchValue = function (set_search_value) {
        __WEBPACK_IMPORTED_MODULE_4_jquery_dist_jquery_min_js__('#search_value').val(set_search_value);
        __WEBPACK_IMPORTED_MODULE_4_jquery_dist_jquery_min_js__(".suggestion_search_box").hide();
        this.searchFaq();
    };
    HelpSupportComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-help-support',
            template: __webpack_require__("../../../../../src/app/help-support/help-support.component.html"),
            styles: [__webpack_require__("../../../../../src/app/help-support/help-support.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__aquantuo_service__["a" /* AquantuoService */], __WEBPACK_IMPORTED_MODULE_6__services_index__["a" /* AlertService */], __WEBPACK_IMPORTED_MODULE_5_ng4_loading_spinner__["Ng4LoadingSpinnerService"]])
    ], HelpSupportComponent);
    return HelpSupportComponent;
}());



/***/ }),

/***/ "../../../../../src/app/index/index.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/index/index.component.html":
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\n\n<section class=\"banner_section home_slide\">\n    <div class=\"container animate-box\">\n        <div class=\"row justify-content-center\">\n            <div class=\"m-col col-12 col-sm-6 col-lg-6  animated fadeInDown\">\n                <div class=\"white_box\">\n                    <a routerLink='/help-support'>\n                        <h5>Frequently Asked Questions</h5>\n                        <img src=\"assets/images/faq.svg\" alt=\"...\" width=\"100\" />\n                        <p id=\"Header\">Answers to Frequently Asked Questions </p>\n\n                    </a>\n                </div>\n            </div>\n            <!-- <div class=\"m-col col-12 col-sm-4 col-lg-3 animated fadeInDown\">\n                <div class=\"white_box\">\n                    <a href=\"#\">\n                        <h4>QA</h4>\n                        <img src=\"assets/images/qa.svg\" alt=\"...\" width=\"100\" />\n                        <p>{{faq_list[0].question | slice:0:50}}... </p>\n                    </a>\n                </div>\n            </div>\n            <div class=\"m-col col-12 col-sm-4 col-lg-3 animated fadeInDown\">\n                <div class=\"white_box\">\n                    <a href=\"#\">\n                        <h4>Documents</h4>\n                        <img src=\"assets/images/document.svg\" alt=\"...\" width=\"100\" />\n                        <p>{{faq_list[0].question | slice:0:50}}... </p>\n                    </a>\n                </div>\n            </div> -->\n        </div>\n    </div>\n    <div class=\"trans_bg\">\n        <div class=\"container\">\n            <div class=\"row justify-content-center\">\n                <div class=\"col-12  col-sm-4 col-lg-3 \">\n                    <div class=\"bottom_box animated fadeInDown\">\n                        <img src=\"assets/images/mail.svg\" alt=\"...\" width=\"40\" />\n                        <h4>Email Us</h4>\n                        <a routerLink='/contact'>support@aquantuo.com</a>\n\n                    </div>\n                </div>\n                <div class=\"col-12  col-sm-4 col-lg-3 \">\n                    <div class=\"bottom_box animated fadeInDown\">\n                        <img src=\"assets/images/phone-call.svg\" alt=\"...\" width=\"35\" />\n                        <h4>Call Us</h4>\n                        <p>+<a target=\"_blank\" class=\"button\" href=\"tel:233302434505\"> 233 30 243 4505 </a></p>\n                    </div>\n                </div>\n                <div class=\"col-12  col-sm-4 col-lg-3 \">\n                    <div class=\"bottom_box animated fadeInDown\">\n                        <img src=\"assets/images/chat.svg\" alt=\"...\" width=\"40\" />\n                        <h4>Online Chat</h4>\n                        <p><a routerLink=\"/help-support\" class=\"purechat-widget-title-link\" data-trigger=\"collapsedClick\">Your Questions Answered</a></p>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</section>\n\n<section class=\"page_section\">\n    <div class=\"container\">\n        <div class=\"row justify-content-center\">\n            <div class=\"col-12 col-sm-8\">\n                <div id=\"sticky-anchor\"></div>\n                <div class=\"input-group search_box\" id=\"search-box\">\n                    <div class=\"input-group input-group-lg mb-3\">\n                        <input name=\"search_value\" autocomplete=\"off\" (keyup)=\"searchSuggestaionFaq()\" [(ngModel)]=\"search_value\" class=\"form-control\" type=\"text\" class=\"form-control\" placeholder=\"Search..\" id=\"search_value\">\n\n                        <div class=\"suggestion_search_box\" id=\"search_box\">\n                            <ul class=\"suggestion_list\">\n                                <li *ngFor=\"let search_suggestaions of search_suggestaion; let i = index\">\n                                    <a (click)=\"setSearchValue(search_suggestaions.question)\">{{search_suggestaions.question  | slice:0:150}}</a>\n                                    <!-- <strong class=\"text-center\">{{search_suggestaions?.record_not_found}} </strong> -->\n                                </li>\n                            </ul>\n\n                        </div>\n                        <div class=\"input-group-append\">\n                            <button class=\"btn btn-info\" (click)=\"searchFaq()\" type=\"button\"><img width=\"20\" src=\"assets/images/search.svg\" alt=\"...\"/></button>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n        <div class=\"row mt-5\">\n            <div class=\"col-12 col-sm-12\">\n                <div class=\"row\">\n                    <div class=\"col-12 col-sm-12 col-lg-7\">\n                        <h2 class=\"large-heading m-h1 capitalFirstText\">{{comman_question}}</h2>\n                        <ul class=\"co_questions mt-3\">\n                            <li *ngFor=\"let faq_lists of faq_list\" class=\"index_question\">\n                                <a routerLink='/help-support/{{faq_lists.id}}' id=\"question_{{faq_lists.id}}\" (mouseenter)=\"collapseAnswer(faq_lists.id)\"><strong>Q. </strong>{{faq_lists.question | slice:0:100}} </a>\n                                <!-- <strong>Ans.  </strong><span class=\"capitalFirstText\" id=\"answer_{{faq_lists.id}}\" [innerHTML]=\"faq_lists?.answer | slice:0:100\">...</span> -->\n\n                                <!-- <a class=\"accordion-toggle\" data-toggle=\"collapse\" href=\"#answer_{{faq_lists.id}}\" id=\"question_{{faq_lists.id}}\"> {{faq_lists.question | slice:0:100}}</a>\n                                <div class=\"panel-collapse collapse\" id=\"answer_{{faq_lists.id}}\">\n                                    <a routerLink='/help-support/{{faq_lists.id}}'>\n                                        <span class=\"capitalFirstText collapse-answer\" id=\"answer_{{faq_lists.id}}\" [innerHTML]=\"faq_lists?.answer | slice:0:500\">...</span></a>\n                                </div> -->\n                            </li>\n                            <li *ngFor=\"let faq_lists of search_faq_list; let i = index\" class=\"search_index_question\">\n                                <a routerLink='/help-support/{{faq_lists.id}}'><strong>Q. </strong><span class=\"capitalFirstText\" [innerHTML] = \"faq_lists?.question | slice:0:100\"></span> \n                                <br/>\n                                <strong>Ans. </strong><span class=\"capitalFirstText\" [innerHTML]=\"faq_lists?.answer | slice:0:200\">...</span>\n                            </a>\n\n                            </li>\n                            <!-- <h5>{{record_not_found[0]?.record_not_found}} </h5> -->\n                        </ul>\n\n\n                    </div>\n                    <div class=\"col-12 col-sm-12 col-lg-5 graphic-area\">\n                        <img src=\"assets/images/graphic.svg\" alt=\"...\" width=\"300\" />\n                        <h4 class=\"mt-5 mb-4\">Still can't find your answer?</h4>\n                        <a href=\"#\" class=\"btn btn-outline-default\" routerLink='/help-support'>MORE FAQ</a>\n                        <!-- <a href=\"#\" class=\"btn btn-outline-default\" routerLink=''>SUPPORT DESK</a> -->\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</section>\n\n<app-footer></app-footer>"

/***/ }),

/***/ "../../../../../src/app/index/index.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IndexComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__aquantuo_service__ = __webpack_require__("../../../../../src/app/aquantuo.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__ = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_jquery_dist_jquery_min_js__ = __webpack_require__("../../../../jquery/dist/jquery.min.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_jquery_dist_jquery_min_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_jquery_dist_jquery_min_js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng4_loading_spinner__ = __webpack_require__("../../../../ng4-loading-spinner/ng4-loading-spinner.umd.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng4_loading_spinner___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_ng4_loading_spinner__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_index__ = __webpack_require__("../../../../../src/app/_services/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var IndexComponent = /** @class */ (function () {
    function IndexComponent(aquantuoService, sanitizer, alertService, router, spinnerService) {
        this.aquantuoService = aquantuoService;
        this.sanitizer = sanitizer;
        this.alertService = alertService;
        this.router = router;
        this.spinnerService = spinnerService;
    }
    IndexComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.comman_question = "Common Questions";
        __WEBPACK_IMPORTED_MODULE_4_jquery_dist_jquery_min_js__('#search_box').hide();
        __WEBPACK_IMPORTED_MODULE_4_jquery_dist_jquery_min_js__('body').click(function () {
            __WEBPACK_IMPORTED_MODULE_4_jquery_dist_jquery_min_js__('#search_box').hide();
        });
        __WEBPACK_IMPORTED_MODULE_4_jquery_dist_jquery_min_js__(".co_questions").show();
        __WEBPACK_IMPORTED_MODULE_4_jquery_dist_jquery_min_js__(".index_question").show();
        var search_value = "";
        this.aquantuoService.home_page(search_value).subscribe(function (data) {
            _this.spinnerService.show();
            if (data.status === 1) {
                _this.spinnerService.hide();
                _this.aquantuoService.hide_loader();
                _this.faq_list = data.result;
                _this.search_faq_list = [];
                _this.record_not_found = [];
                //return this.faq_list = this.sanitizer.bypassSecurityTrustUrl(data.result);
                //this.faq_list = this.sanitizer.bypassSecurityTrustHtml(data.result);
                // console.log(data.message);
            }
            else {
                _this.spinnerService.hide();
                _this.faq_list = [{ "record_not_found": "Record not found." }];
                console.log(data.message);
            }
        });
    };
    IndexComponent.prototype.searchFaq = function () {
        var _this = this;
        __WEBPACK_IMPORTED_MODULE_4_jquery_dist_jquery_min_js__('html, body').animate({
            scrollTop: __WEBPACK_IMPORTED_MODULE_4_jquery_dist_jquery_min_js__(".page_section").offset().top
        }, 300);
        var search_value = __WEBPACK_IMPORTED_MODULE_4_jquery_dist_jquery_min_js__('#search_value').val();
        if (search_value != "") {
            this.comman_question = "Search Result";
            this.aquantuoService.home_page(search_value).subscribe(function (data) {
                if (data.status === 1) {
                    __WEBPACK_IMPORTED_MODULE_4_jquery_dist_jquery_min_js__(".co_questions").show();
                    __WEBPACK_IMPORTED_MODULE_4_jquery_dist_jquery_min_js__(".index_question").hide();
                    _this.aquantuoService.hide_loader();
                    _this.search_faq_list = data.result;
                    _this.faq_list = [];
                    _this.record_not_found = [];
                    // console.log(data.message);
                }
                else {
                    _this.search_faq_list = [];
                    __WEBPACK_IMPORTED_MODULE_4_jquery_dist_jquery_min_js__(".index_question").hide();
                    __WEBPACK_IMPORTED_MODULE_4_jquery_dist_jquery_min_js__(".search_index_question").hide();
                    _this.record_not_found = [{ "record_not_found": " We couldn't find anything to match your search for: " + search_value }];
                    // console.log(data.message);
                }
            });
        }
        else {
            this.alertService.error("Please enter search keyword to perform search");
            this.ngOnInit();
        }
    };
    IndexComponent.prototype.searchSuggestaionFaq = function () {
        var _this = this;
        var search_value = __WEBPACK_IMPORTED_MODULE_4_jquery_dist_jquery_min_js__('#search_value').val();
        if (search_value != "") {
            __WEBPACK_IMPORTED_MODULE_4_jquery_dist_jquery_min_js__('#search_box').show();
            this.aquantuoService.search_faq(search_value).subscribe(function (data) {
                if (data.status === 1) {
                    _this.aquantuoService.hide_loader();
                    _this.loader = 1;
                    _this.search_suggestaion = data.faq_result;
                    // console.log(data.message);
                }
                else {
                    _this.search_suggestaion = [{ "record_not_found": "No search results" }];
                    console.log(data.message);
                }
            });
        }
        else {
            __WEBPACK_IMPORTED_MODULE_4_jquery_dist_jquery_min_js__('#search_box').hide();
        }
    };
    IndexComponent.prototype.setSearchValue = function (set_search_value) {
        __WEBPACK_IMPORTED_MODULE_4_jquery_dist_jquery_min_js__('#search_value').val(set_search_value);
        __WEBPACK_IMPORTED_MODULE_4_jquery_dist_jquery_min_js__(".suggestion_search_box").hide();
        this.searchFaq();
    };
    IndexComponent.prototype.collapseAnswer = function (id) {
        __WEBPACK_IMPORTED_MODULE_4_jquery_dist_jquery_min_js__("#answer_" + id).collapse('toggle');
    };
    IndexComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-index',
            template: __webpack_require__("../../../../../src/app/index/index.component.html"),
            styles: [__webpack_require__("../../../../../src/app/index/index.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__aquantuo_service__["a" /* AquantuoService */], __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__["DomSanitizer"], __WEBPACK_IMPORTED_MODULE_7__services_index__["a" /* AlertService */], __WEBPACK_IMPORTED_MODULE_6__angular_router__["a" /* ActivatedRoute */], __WEBPACK_IMPORTED_MODULE_5_ng4_loading_spinner__["Ng4LoadingSpinnerService"]])
    ], IndexComponent);
    return IndexComponent;
}());



/***/ }),

/***/ "../../../../../src/app/section-detail/section-detail.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/section-detail/section-detail.component.html":
/***/ (function(module, exports) {

module.exports = "<app-header>\n</app-header>\n\n<section class=\"banner_section faq_bg\">\n    <div class=\"container\">\n        <div class=\"row justify-content-center pb-5\">\n            <div class=\"col-12 col-sm-6\">\n                <div id=\"sticky-anchor\"></div>\n                <div class=\"input-group  search_box\" id=\"search-box\">\n                    <div class=\"input-group input-group-lg mb-3\">\n                        <input name=\"search_value\" autocomplete=\"off\" (keyup)=\"searchSuggestaionFaq()\" [(ngModel)]=\"search_value\" class=\"form-control sidebar fixedsticky\" type=\"text\" class=\"form-control\" placeholder=\"Search..\" id=\"search_value\">\n                        <div class=\"suggestion_search_box\">\n                            <ul class=\"suggestion_list \">\n                                <li *ngFor=\"let search_suggestaions of search_suggestaion; let i = index\">\n                                    <strong class=\"text-center\">{{search_suggestaions?.record_not_found}} </strong>\n                                    <a (click)=\"setSearchValue(search_suggestaions.question)\">{{search_suggestaions.question  | slice:0:150}}</a>\n                                </li>\n                            </ul>\n                        </div>\n                        <div class=\"input-group-append\">\n                            <button class=\"btn btn-info\" (click)=\"searchFaq()\" type=\"button\"><img width=\"20\" src=\"assets/images/search.svg\" alt=\"...\"/></button>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</section>\n\n<section class=\"page_section inner\">\n    <div class=\"container\">\n        <div class=\"row mt-5 justify-content-center\">\n            <div class=\"m-col col-sm-2 text-center\" *ngFor=\"let service_lists of service_list\">\n                <span *ngIf=\"section_id == service_lists.id\">\n                <div class=\"link_circle active\" id=\"link_circle_{{service_lists.id}}\">\n                    <a id=\"link_help_{{service_lists.id}}\" (click)=\"faqSectionList(service_lists.id)\">\n                        <img src=\"{{app_constant.service_image_url}}{{service_lists.image}}\" width=\"40\" alt=\"...\" />\n                    </a>\n                </div>\n                </span>\n                <span *ngIf=\"section_id != service_lists.id\">\n                        <div class=\"link_circle\" id=\"link_circle_{{service_lists.id}}\">\n                            <a id=\"link_help_{{service_lists.id}}\" (click)=\"faqSectionList(service_lists.id)\">\n                                <img src=\"{{app_constant.service_image_url}}{{service_lists.image}}\" width=\"40\" alt=\"...\" />\n                            </a>\n                        </div>\n                        </span>\n                <p> {{service_lists?.section_name | uppercase}} </p>\n\n            </div>\n        </div>\n\n        <div class=\"row mt-5 justify-content-center\">\n            <div class=\"col-12 col-sm-10 col-lg-9 mt-4\">\n                <h2 class=\"text-uppercase text-center pb-3 m-h1\"> {{faq_list[0]?.section_name | uppercase}}</h2>\n                <h4 class=\"text-uppercase text-center pb-3 m-h1 search_result\"> {{search_result}}</h4>\n                <ul class=\"questions_list mt-4\">\n\n                    <li *ngFor=\"let faq_lists of faq_list\" class=\"index_question\">\n                        <a routerLink='/help-support/{{faq_lists.id}}' id=\"question_{{faq_lists.id}}\" (mouseenter)=\"collapseAnswer(faq_lists.id)\"><strong>Q. </strong>{{faq_lists.question | slice:0:100}} </a>\n                    </li>\n                    <li *ngFor=\"let faq_lists of search_faq_list; let i = index\" class=\"search_index_question\">\n                        <a routerLink='/help-support/{{faq_lists.id}}'><strong>Q. </strong><span class=\"capitalFirstText\" [innerHTML] = \"faq_lists?.question | slice:0:100\"></span> \n                        <br/>\n                        <strong>Ans. </strong><span class=\"capitalFirstText\" [innerHTML]=\"faq_lists?.answer | slice:0:200\">...</span>\n                    </a>\n                    </li>\n                </ul>\n                <h5>{{faq_list[0]?.record_not_found}} </h5>\n\n            </div>\n\n        </div>\n    </div>\n</section>\n<app-footer></app-footer>"

/***/ }),

/***/ "../../../../../src/app/section-detail/section-detail.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SectionDetailComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__aquantuo_service__ = __webpack_require__("../../../../../src/app/aquantuo.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_index__ = __webpack_require__("../../../../../src/app/_services/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_jquery_dist_jquery_min_js__ = __webpack_require__("../../../../jquery/dist/jquery.min.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_jquery_dist_jquery_min_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_jquery_dist_jquery_min_js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_ng4_loading_spinner__ = __webpack_require__("../../../../ng4-loading-spinner/ng4-loading-spinner.umd.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_ng4_loading_spinner___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_ng4_loading_spinner__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var SectionDetailComponent = /** @class */ (function () {
    function SectionDetailComponent(aquantuoService, route, http, alertService, spinnerService) {
        this.aquantuoService = aquantuoService;
        this.route = route;
        this.http = http;
        this.alertService = alertService;
        this.spinnerService = spinnerService;
        this.faq_list = [];
    }
    SectionDetailComponent.prototype.ngOnInit = function () {
        var _this = this;
        __WEBPACK_IMPORTED_MODULE_7_jquery_dist_jquery_min_js__(".questions_list").show();
        __WEBPACK_IMPORTED_MODULE_7_jquery_dist_jquery_min_js__('.suggestion_search_box').hide();
        __WEBPACK_IMPORTED_MODULE_7_jquery_dist_jquery_min_js__('body').click(function () {
            __WEBPACK_IMPORTED_MODULE_7_jquery_dist_jquery_min_js__('.suggestion_search_box').hide();
        });
        this.app_constant = __WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */];
        //alert(this.app_constant);
        this.sub = this.route.params.subscribe(function (params) {
            _this.section_id = params['section_id'];
            _this.aquantuoService.faq_section_list(_this.section_id).subscribe(function (data) {
                _this.spinnerService.show();
                if (data.status === 1) {
                    __WEBPACK_IMPORTED_MODULE_7_jquery_dist_jquery_min_js__(".search_result").hide();
                    _this.search_faq_list = [];
                    _this.spinnerService.hide();
                    _this.aquantuoService.hide_loader();
                    _this.loader = 1;
                    _this.service_list = data.section_result;
                    __WEBPACK_IMPORTED_MODULE_7_jquery_dist_jquery_min_js__('#link_circle_' + _this.section_id).addClass('active');
                    __WEBPACK_IMPORTED_MODULE_7_jquery_dist_jquery_min_js__('html, body').animate({
                        scrollTop: __WEBPACK_IMPORTED_MODULE_7_jquery_dist_jquery_min_js__(".page_section.inner").offset().top
                    }, 1000);
                    if (data.faq_result.length > 0) {
                        _this.faq_list = data.faq_result;
                    }
                    else {
                        __WEBPACK_IMPORTED_MODULE_7_jquery_dist_jquery_min_js__(".questions_list").hide();
                        _this.faq_list = [{ "record_not_found": "Record Not Found" }];
                    }
                }
                else {
                    _this.spinnerService.hide();
                    __WEBPACK_IMPORTED_MODULE_7_jquery_dist_jquery_min_js__(".questions_list").hide();
                    _this.faq_list = [{ "record_not_found": "Record Not Found" }];
                    console.log(data.message);
                }
            });
        });
    };
    SectionDetailComponent.prototype.faqSectionList = function (section_id) {
        var _this = this;
        this.spinnerService.show();
        __WEBPACK_IMPORTED_MODULE_7_jquery_dist_jquery_min_js__(".questions_list").show();
        __WEBPACK_IMPORTED_MODULE_7_jquery_dist_jquery_min_js__('.link_circle').removeClass('active');
        this.faq_list = [{ "record_not_found": "Record Not Found" }];
        this.aquantuoService.faq_section_list(section_id).subscribe(function (data) {
            __WEBPACK_IMPORTED_MODULE_7_jquery_dist_jquery_min_js__('#link_circle_' + section_id).addClass('active');
            __WEBPACK_IMPORTED_MODULE_7_jquery_dist_jquery_min_js__('html, body').animate({
                scrollTop: __WEBPACK_IMPORTED_MODULE_7_jquery_dist_jquery_min_js__(".page_section.inner").offset().top
            }, 1000);
            if (data.status === 1) {
                __WEBPACK_IMPORTED_MODULE_7_jquery_dist_jquery_min_js__(".search_result").hide();
                _this.spinnerService.hide();
                _this.loader = 1;
                // this.service_list = data.section_result;
                if (data.faq_result.length > 0) {
                    _this.faq_list = data.faq_result;
                }
                else {
                    __WEBPACK_IMPORTED_MODULE_7_jquery_dist_jquery_min_js__(".questions_list").hide();
                    _this.faq_list = [{ "record_not_found": "Record Not Found" }];
                }
            }
            else {
                _this.spinnerService.hide();
                __WEBPACK_IMPORTED_MODULE_7_jquery_dist_jquery_min_js__(".questions_list").hide();
                _this.faq_list = [{ "record_not_found": "Record Not Found" }];
                console.log(data.message);
            }
        });
    };
    SectionDetailComponent.prototype.searchSuggestaionFaq = function () {
        var _this = this;
        var search_value = __WEBPACK_IMPORTED_MODULE_7_jquery_dist_jquery_min_js__('#search_value').val();
        if (search_value != "") {
            __WEBPACK_IMPORTED_MODULE_7_jquery_dist_jquery_min_js__('.suggestion_search_box').show();
            this.aquantuoService.search_faq(search_value).subscribe(function (data) {
                _this.spinnerService.show();
                if (data.status === 1) {
                    _this.spinnerService.hide();
                    _this.loader = 1;
                    _this.search_suggestaion = data.faq_result;
                    // console.log(data.message);
                }
                else {
                    _this.spinnerService.hide();
                    _this.search_suggestaion = [{ "record_not_found": "No search results" }];
                    console.log(data.message);
                }
            });
        }
        else {
            __WEBPACK_IMPORTED_MODULE_7_jquery_dist_jquery_min_js__('.suggestion_search_box').hide();
        }
    };
    SectionDetailComponent.prototype.setSearchValue = function (set_search_value) {
        __WEBPACK_IMPORTED_MODULE_7_jquery_dist_jquery_min_js__('#search_value').val(set_search_value);
        __WEBPACK_IMPORTED_MODULE_7_jquery_dist_jquery_min_js__(".suggestion_search_box").hide();
        this.searchFaq();
    };
    SectionDetailComponent.prototype.searchFaq = function () {
        var _this = this;
        __WEBPACK_IMPORTED_MODULE_7_jquery_dist_jquery_min_js__('html, body').animate({
            scrollTop: __WEBPACK_IMPORTED_MODULE_7_jquery_dist_jquery_min_js__(".page_section.inner").offset().top
        }, 1000);
        var search_value = __WEBPACK_IMPORTED_MODULE_7_jquery_dist_jquery_min_js__('#search_value').val();
        if (search_value != "") {
            this.spinnerService.show();
            this.aquantuoService.search_faq(search_value).subscribe(function (data) {
                if (data.status == 1) {
                    __WEBPACK_IMPORTED_MODULE_7_jquery_dist_jquery_min_js__(".questions_list").show();
                    _this.spinnerService.hide();
                    __WEBPACK_IMPORTED_MODULE_7_jquery_dist_jquery_min_js__(".search_result").hide();
                    _this.search_result = "Search Result";
                    _this.search_faq_list = data.faq_result;
                    _this.faq_list = [];
                    // console.log(data.message);
                }
                else {
                    __WEBPACK_IMPORTED_MODULE_7_jquery_dist_jquery_min_js__(".questions_list").hide();
                    _this.spinnerService.hide();
                    _this.faq_list = [{ "section_name": "Search Result", "record_not_found": " We couldn't find anything to match your search for: " + search_value }];
                    // this.search_result = [{"search_result":"Search Result"}];
                    console.log(data.message);
                }
            });
        }
        else {
            this.search_faq_list = [];
            //$("#search_value").css("color", "red");
            this.alertService.error("Please enter search keyword to perform search");
            this.ngOnInit();
        }
    };
    SectionDetailComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-section-detail',
            template: __webpack_require__("../../../../../src/app/section-detail/section-detail.component.html"),
            styles: [__webpack_require__("../../../../../src/app/section-detail/section-detail.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__aquantuo_service__["a" /* AquantuoService */], __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* ActivatedRoute */], __WEBPACK_IMPORTED_MODULE_2__angular_http__["Http"], __WEBPACK_IMPORTED_MODULE_6__services_index__["a" /* AlertService */], __WEBPACK_IMPORTED_MODULE_8_ng4_loading_spinner__["Ng4LoadingSpinnerService"]])
    ], SectionDetailComponent);
    return SectionDetailComponent;
}());



/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false,
    api_url: 'http://192.168.1.35:8443/',
    service_image_url: "http://192.168.1.101/aq/dev3/upload/section/",
};


/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["enableProdMode"])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map