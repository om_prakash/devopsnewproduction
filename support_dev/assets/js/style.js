$(".link_circle").click(function() {

    $('html, body').animate({
        scrollTop: $(".page_section.inner").offset().top
    }, 2000);
});

//----------------------- fixed header -------------------//
$(window).scroll(function() {

    var scroll = $(window).scrollTop();
    if (scroll >= 60) {
        //clearHeader, not clearheader - caps H
        $(".fixed-top").addClass("darkHeader");
        $(".animate-box").addClass("transYanimate");

    } else {
        $(".fixed-top").removeClass("darkHeader");
        $(".animate-box").removeClass("transYanimate");
    }

}); //missing );

$('#link').click(function() {

    $("html, body").animate({
        scrollTop: 0
    }, 600);
    alert();
    return false;
});



//----------------------- Stick searchbox -------------------//

function sticky_relocate() {
    var window_top = $(window).scrollTop();
    var div_top = $('#sticky-anchor').offset().top - 20;
    if (window_top > div_top) {
        $('#search-box').addClass('stick');
        $('#sticky-anchor').height($('#search-box').outerHeight());
    } else {
        $('#search-box').removeClass('stick');
        $('#sticky-anchor').height(0);
    }
}

$(function() {
    $(window).scroll(sticky_relocate);
    sticky_relocate();
});

var dir = 1;
var MIN_TOP = 200;
var MAX_TOP = 350;

function autoscroll() {
    var window_top = $(window).scrollTop() + dir;
    if (window_top >= MAX_TOP) {
        window_top = MAX_TOP;
        dir = -1;
    } else if (window_top <= MIN_TOP) {
        window_top = MIN_TOP;
        dir = 1;
    }
    $(window).scrollTop(window_top);
    window.setTimeout(autoscroll, 100);
}