var express = require('express')
var app = express();
var parser = require("body-parser");
var port = 7000;
var fs = require('fs'),
    http = require('http'),
    https = require('https');
var constants = require('./config/constant');

var options = {
    key: fs.readFileSync('key.pem'),
    cert: fs.readFileSync('cert.pem')
};


app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    res.header("Access-Control-Allow-Headers", "Content-Type");
    res.header("Access-Control-Allow-Methods", "PUT, GET, POST, DELETE, OPTIONS");
    next();
});

app.use(parser.json());

app.use(parser.urlencoded({
    extended: true,
    limit: '50mb'
}));


var server = https.createServer(options, app).listen(port, function() {
    console.log("Express server listening on port " + port);
});

app.listen(port, function() {
    console.log('Your server running at ' + port);
});

app.use(require('./route'));

/*var express = require('express');
var app = express();
var server = require('http');
var parser = require("body-parser");
var port = 7000;
var constants = require('./config/constant');


app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    res.header("Access-Control-Allow-Headers", "Content-Type");
    res.header("Access-Control-Allow-Methods", "PUT, GET, POST, DELETE, OPTIONS");
    next();
});

app.use(parser.json());

app.use(parser.urlencoded({
    extended: true,
    limit: '50mb'
}));


app.listen(port, function() {
    console.log('Your server running at ' + port);
});


app.use(require('./route'));*/