/*
 * Database connection 
 */
const Sequelize = require('sequelize');

require('dotenv').config();

const sequelize = new Sequelize('sequelize', null, null, {
	port: 3306,
	dialect: 'mysql',

	replication: {
		read: [{
				host: process.env.DB_HOST_MASTER,
				username: process.env.DB_USERNAME_MASTER,
				password: process.env.DB_PASSWORD_MASTER,
				database: process.env.DB_DATABASE_MASTER
			},
			/*{
			           host: process.env.DB_HOST_SLAVE_2,
			           username: process.env.DB_USERNAME_SLAVE_2,
			           password: process.env.DB_PASSWORD_SLAVE_2,
			           database: process.env.DB_DATABASE_SLAVE_2
			       }*/
		],
		write: {
			host: process.env.DB_HOST_MASTER,
			username: process.env.DB_USERNAME_MASTER,
			password: process.env.DB_PASSWORD_MASTER,
			database: process.env.DB_DATABASE_MASTER
		}
	},
	pool: { // If you want to override the options used for the read pool you can do so here
		max: 20,
		idle: 30000
	},

});

// export module pool to be used in other files
module.exports = sequelize;