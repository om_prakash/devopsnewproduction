var express = require('express'),
	router = express.Router();


var sectionList = require('./models/sectionList');
router.get('/section/list', sectionList);

var sectionDetail = require('./models/sectionDetail');
router.get('/section/detail', sectionDetail);

var faqLike = require('./models/faqLike');
router.get('/section/faqlike', faqLike);

var support = require('./models/support');
router.post('/support', support);

var address = require('./models/address');
router.get('/address', address);

var homePage = require('./models/homePage');
router.get('/home', homePage);

var faqSearch = require('./models/faq_search');
router.get('/faq-search', faqSearch);

module.exports = router;