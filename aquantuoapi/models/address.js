var constants = require('./../config/constant');
var dbConnection = require('./../config/connection');

var address = function(req, res) {

    var address = "SELECT * FROM address WHERE id = 1";

    dbConnection.query(address, {
        type: dbConnection.QueryTypes.SELECT
    }).then(function(address_data) {
        if (address_data.length > 0) {
            var response = {
                "status": 1,
                "result": address_data[0],
            };
            res.send(response);
        } else {
            var response = {
                "status": 0,
                "message": 'Record not found.'
            };
            res.send(response);
        }
    })
}
module.exports = address;