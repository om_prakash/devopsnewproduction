var constants = require('./../config/constant');
var dbConnection = require('./../config/connection');

var sectionDetail = function(req, res) {

    var faq_id = req.param('faq_id') ? req.param('faq_id') : "";
    var remote_address = req.param('remote_address') ? req.param('remote_address') : "";
    var response = {};
    var faq_like_data = "0";

    if (faq_id === "") {
        var response = {
            "status": 0,
            "message": 'Faq id can not be blank'
        };
        res.json(response);
    } else {
        var faq_detail =
            "SELECT faq.id, COUNT(faq_like.id) as like_faq,  faq.status, question, related_question, section_id, section_name, answer FROM faq LEFT JOIN section ON faq.section_id = section.id LEFT JOIN faq_like ON faq.id = faq_like.faq_id  WHERE faq.id = " +
            faq_id + "";

        var faq_like = "SELECT * from faq_like WHERE faq_id=" + faq_id +
            " AND remote_address='" +
            remote_address + "'";

        dbConnection.query(faq_like, {
            type: dbConnection.QueryTypes.SELECT
        }).then(function(faq_like_data) {
            //console.log(faq_like_data);
            faq_like_data_result = faq_like_data[0];
        })

        dbConnection.query(faq_detail, {
            type: dbConnection.QueryTypes.SELECT
        }).then(function(faq_detail_data) {
            if (faq_detail_data.length > 0) {

                if (faq_detail_data[0]['related_question'] != "") {

                    var related_question = "SELECT id, question, section_id, related_question, status FROM faq WHERE id IN(" +
                        faq_detail_data[0]['related_question'] + ")";
                    dbConnection.query(related_question, {
                        type: dbConnection.QueryTypes.SELECT
                    }).then(function(related_question_data) {

                        if (related_question_data.length > 0) {
                            var response = {
                                "status": 1,
                                "result": faq_detail_data[0],
                                "related_question": related_question_data,
                                "faq_like": faq_like_data_result
                            };
                            res.json(response);
                        } else {
                            var response = {
                                "status": 1,
                                "result": faq_detail_data[0],
                                "related_question": [],
                                "faq_like": faq_like_data_result
                            };
                            res.json(response);
                        }
                    });
                    var response = {
                        "status": 1,
                        "result": faq_detail_data[0],
                        "related_question": related_question_data,
                        "faq_like": faq_like_data_result
                    };
                    res.json(response);
                } else {
                    var response = {
                        "status": 1,
                        "result": faq_detail_data[0],
                        "related_question": [],
                        "faq_like": faq_like_data_result
                    };
                    res.json(response);
                }
                console.log(response);
            } else {
                var response = {
                    "status": 0,
                    "message": "Record Not Found."
                };
                res.json(response);
            }
        }).catch(function(err) {
            console.log("Your SECTION FAQ DETAIl query in error please check...");
        });
    }
}
module.exports = sectionDetail;