var constants = require('./../config/constant');
var dbConnection = require('./../config/connection');

var faqLike = function(req, res) {

	var faq_id = req.param('faq_id') ? req.param('faq_id') : "";
	var status = req.param('status') ? req.param('status') : "";
	var remote_address = req.param('remote_address') ? req.param('remote_address') : "";
	if (faq_id === "") {
		var response = {
			"status": 0,
			"message": "faq id can't be blank"
		};
		res.json(response);
	} else if (remote_address === "") {
		var response = {
			"status": 0,
			"message": "ip_address can't be blank"
		};
		res.json(response);
	} else if (status === "") {
		var response = {
			"status": 0,
			"message": "status can't be blank"
		};
		res.json(response);
	} else {


		var faq_check = "SELECT * FROM faq_like WHERE faq_id = " + faq_id + " AND remote_address = '" +
			remote_address + "'";

		dbConnection.query(faq_check, {
			type: dbConnection.QueryTypes.SELECT
		}).then(function(faq_like_data) {
			if (faq_like_data.length > 0) {

				if (faq_like_data[0].status != status) {

					var faq_update = "UPDATE faq_like SET " +
						"`status`   = '" + status + "'" +
						" WHERE faq_id = " + faq_id + " AND remote_address = '" + remote_address + "'";

					dbConnection.query(faq_update, {
						type: dbConnection.QueryTypes.UPDATE
					}).then(function(faq_update) {

						if (faq_update.length > 0) {

							var response = {
								"status": 1,
								"message": "You are " + status + " this faq",
								"faq_like": {
									"status": status
								},
							};
							res.json(response);

						}
					});
				} else {

					var faq_delete = "DELETE FROM faq_like WHERE faq_id = " + faq_id + " AND remote_address = '" +
						remote_address +
						"' AND status = '" + status + "'";
					dbConnection.query(faq_delete, {
						type: dbConnection.QueryTypes.DELETE
					}).then(function(faq_delete) {
						var faq_detail =
							"SELECT COUNT(faq_like.id) as like_faq FROM faq_like WHERE faq_id = " + faq_id + " AND status= 'like'";
						dbConnection.query(faq_detail, {
							type: dbConnection.QueryTypes.SELECT
						}).then(function(faq_detail_data) {
							faq_like = faq_detail_data[0];
							var response = {
								"status": 0,
								"message": "Removed to " + status + " faq",
								"faq_like": {},
								"result": faq_like

							};
							res.json(response);

						});

					});

				}
			} else {

				var faq_like_insert = "INSERT INTO faq_like SET " +
					"`faq_id`      = " + faq_id + "," +
					"`remote_address`      = '" + remote_address + "'," +
					"`status`      = '" + status + "'" +
					"";

				dbConnection.query(faq_like_insert, {
					type: dbConnection.QueryTypes.INSERT
				}).then(function(faq_insert_data) {

					if (faq_insert_data.length > 0) {

						var faq_detail =
							"SELECT COUNT(faq_like.id) as like_faq FROM faq_like WHERE faq_id = " + faq_id + " AND status= 'like'";
						dbConnection.query(faq_detail, {
							type: dbConnection.QueryTypes.SELECT
						}).then(function(faq_detail_data) {
							faq_like = faq_detail_data[0];
							var response = {
								"status": 1,
								"message": "You are " + status + " this faq",
								"faq_like": {
									"status": status
								},
								"result": faq_like
							};
							res.json(response);

						});
					} else {
						var response = {
							"status": 0,
							"message": "Problem in " + status + " this faq",
							"result": faq_like
						};
						res.json(response);
					}
				}).catch(function(err) {
					console.log("Your SECTION FAQ DETAIl query in error please check...");
				});
			}

		}).catch(function(err) {
			console.log("Your SECTION FAQ DETAIl query in error please check...");
		})
	}

}

module.exports = faqLike;