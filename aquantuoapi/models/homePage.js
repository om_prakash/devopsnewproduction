var constants = require('./../config/constant');
var dbConnection = require('./../config/connection');

var homePage = function(req, res) {
	var search_value = req.param('search_value') ? req.param('search_value') : "";
	if (search_value != "") {
		var faq_list =
			'SELECT * FROM faq WHERE question LIKE "%' + search_value + '%" LIMIT 0,9';
	} else {
		var faq_list =
			"SELECT faq.id, section_id, question, answer, status, created_at FROM faq WHERE section_id IN (1,2) LIMIT 0,9";
	}
	dbConnection.query(faq_list, {
		type: dbConnection.QueryTypes.SELECT
	}).then(function(faq_list_data) {
		if (faq_list_data.length > 0) {
			var response = {
				"status": 1,
				"result": faq_list_data,
			}
			res.json(response);
		} else {
			var response = {
				"status": 0,
				"message": "Record not found"
			}
			res.json(response);
		}
	}).catch(function(err) {

		console.log(err);
		console.log("Your HOME PAGE QUERY in error please check...");
	});



}
module.exports = homePage;