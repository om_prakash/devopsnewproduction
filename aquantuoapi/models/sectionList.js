var constants = require('./../config/constant');
var dbConnection = require('./../config/connection');

var sectionList = function(req, res) {

	var section_id = req.param('section_id') ? req.param('section_id') : "";
	var search_value = req.param('search_value') ? req.param('search_value') : "";

	var response = {};

	if (search_value != "") {
		var faq_list =
			"SELECT * FROM faq WHERE question LIKE '%" + search_value + "%'";
	} else {
		var sections_list = "SELECT * FROM section WHERE NOT id = 1";

		dbConnection.query(sections_list, {
			type: dbConnection.QueryTypes.SELECT
		}).then(function(section_list_data) {
			if (section_list_data.length > 0) {

				if (section_id != "undefined" && section_id != "") {
					var faq_list =
						"SELECT faq.id, status, question, section_id, section_name FROM faq LEFT JOIN section ON section.id = faq.section_id WHERE section_id = " +
						section_id + " AND status = 0";
				} else {
					var faq_list =
						"SELECT faq.id, status, question, section_id, section_name FROM faq LEFT JOIN section ON section.id = faq.section_id WHERE section_id = 1  AND status = 0";
				}

				//  var faq_list = "SELECT * FROM faq WHERE section_id = " + section_list_data[0]['id'];

				dbConnection.query(faq_list, {
					type: dbConnection.QueryTypes.SELECT
				}).then(function(faq_list_data) {
					if (faq_list_data.length > 0) {
						var response = {
							"status": 1,
							"section_result": section_list_data,
							"faq_result": faq_list_data,
						}
						res.json(response);
					}
					var response = {
						"status": 1,
						"section_result": section_list_data,
						"faq_result": faq_list_data,
					}
					res.json(response);
				});

				var response = {
					"status": 1,
					"section_result": section_list_data,
					"faq_result": faq_list_data,
				}
				res.json(response);
			} else {
				var response = {
					"status": 0,
					"message": "Record not found."
				}
				res.json(response);
			}
		}).catch(function(err) {

			console.log(err);
			console.log("Your SECTION LIST in error please check...")
		});
	}



}
module.exports = sectionList;