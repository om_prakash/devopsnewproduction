var constants = require('./../config/constant');
var dbConnection = require('./../config/connection');
var fs = require('fs');
var _ = require('lodash');
var uuidv4 = require('uuid/v4');


var contactUs = function(req, res) {


	var email = req.body.email ? req.body.email : "";
	var request_type = req.body.request_type ? req.body.request_type : "";
	var description = req.body.description ? req.body.description : "";
	var user_profile_link = req.body.user_profile_link ? req.body.user_profile_link : "";
	var aquantuo_link = req.body.aquantuo_link ? req.body.aquantuo_link : "";
	var image = req.body.image ? req.body.image : "";
	var image_extention = req.body.image_extention ? req.body.image_extention : "";


	if (email === "" || email != undefined) {
		var response = {
			"status": 0,
			"message": 'Email can not be blank'
		};
		res.send(response);
	} else if (request_type == "" || request_type != undefined) {
		var response = {
			"status": 0,
			"message": 'Support type can not be blank'
		};
		res.send(response);
	} else if (description == "" || description != undefined) {
		var response = {
			"status": 0,
			"message": 'Description can not be blank'
		};
		res.send(response);
	} else if (user_profile_link === "" || user_profile_link != undefined) {
		var response = {
			"status": 0,
			"message": 'User profile link can not be blank'
		};
		res.send(response);
	} else if (aquantuo_link === "" || aquantuo_link != undefined) {
		var response = {
			"status": 0,
			"message": 'Aquantuo link can not be blank'
		};
		res.send(response);
	} else {

		var file_name = "";
		var base64Data = image.replace(/^data:image\/png;base64,/, "");
		var file_name = uuidv4() + '.' + image_extention;
		fs.writeFile("/var/www/html/aquantuoapi/uploads/support/" + file_name, base64Data, 'base64');

		// var insert_record = "INSERT INTO support SET " +
		// 	"`email`        = '" + email + "'," +
		// 	"`request_type`        = '" + request_type + "'," +
		// 	"`description`        = '" + description + "'," +
		// 	"`user_profile_link`        = '" + user_profile_link + "'," +
		// 	"`aquantuo_link`        = '" + aquantuo_link + "'," +
		// 	"`attachment`        = '" + file_name + "";

		var insert_record = "INSERT INTO support SET " +
			"`email`   = '" + email + "'," +
			"`description`   = '" + description + "'," +
			"`request_type`   = '" + request_type + "'," +
			"`user_profile_link`   = '" + user_profile_link + "'," +
			"`aquantuo_link`   = '" + aquantuo_link + "'," +
			"`attachments`           = '" + file_name + "'" +
			"";


		console.log(insert_record)
		dbConnection.query(insert_record, {
			type: dbConnection.QueryTypes.INSERT
		}).then(function(exist_email_data) {
			if (exist_email_data.length > 0) {
				var response = {
					"status": 1,
					"message": 'Record insert successfully.'
				};
				res.send(response);
			} else {
				var response = {
					"status": 0,
					"message": 'Oops something went wrong'
				};
				res.send(response);
			}
		}).catch(function(err) {
			console.log(' -- check Add Support query failed err.message: ' + err.SOMETHING_WENT_WRONG);
			// response  to send

		});
	}
}

module.exports = contactUs;