<?php

/**
 * Step 1: Require the Slim Framework
 *
 * If you are not using Composer, you need to require the
 * Slim Framework and register its PSR-0 autoloader.
 *
 * If you are using Composer, you can skip this step.
 *
 * Filename Name : authentication.php
 * File Path : services/authentication.php
 * Description : This file contains method related to userinformation.
 * Author: Ravi shukla
 * Created Date : 22-08-2015
 * Library : Email,DbConnect
 *
 * */

require 'Slim/Slim.php';
require 'Slim/config/DbConnect.php';
require 'Slim/library/Common.php';

\Slim\Slim::registerAutoloader();

/**
 * Step 2: Instantiate a Slim application
 *
 * This example instantiates a Slim application using
 * its default settings. However, you will usually configure
 * your Slim application now by passing an associative array
 * of setting names and values into the application constructor.
 */
$app = new \Slim\Slim();

$userId = '';
$userType = '';

$app->post('/calculate_request_cost', function () use ($app) {
	global $db;
	global $userId;
	$collection = $db->category;
	$configuration = $db->configuration;
	$configuration = $configuration->find(['_id' => new MongoId('5673e33e6734c4f874685c84')], ['Insurance', 'aquantuoFees','main_insurance','warehouse_transit_fee','tax']);
	$Insurance_array = [];
	if ($configuration->count() > 0) {
		$configuration = $configuration->getNext();
		$Insurance_array = $configuration['Insurance'];

	}

	$response = array('success' => 0, 'msg' => 'Oops! Something went wrong.');
	    
	verifyRequiredParams(array('products_list', 'distance', 'Default_Currency','pickup_country','drop_off_country','pickup_city','drop_off_city'));
	$products_list = json_decode($app->request->post('products_list'), 1);
	$weight = 0;
	$insurance = 0;
	$rate = 0;
	$volume = 0;
	$ups_rate = 0;
	$warehouse_transit_fee = 0;
	$is_ups_rate_applied = false;
	$total_ups_rate = 0;
	$total_custom_duty_rate = 0;
	$is_customs_duty_applied =false;
	$totalItemCost = 0;
	// $distance =10000;
	$distance = $app->request->post('distance');
	$distance = floatval($distance) * 0.000621371;
	$return_array = $products_list;
	$total_weight = 0;
	$inputData['consolidate_check'] = $app->request->post('consolidate_item');
	$res['air_rate'] = 0;

	$set_air_category = set_air_category($products_list, $inputData);
	//print_r($set_air_category); die;
	include_once ('Slim/library/upsRate.php');
	foreach ($products_list as $key => $value) {
		// UPS charge
		// Calculate ups rate of each item.
		$last_price = 0;// is used for only other categry in air mode
		$is_macth = false;
		$value['productUpsRate'] = 0;
		$return_array[$key]['productUpsRate'] =  0;

		// Get Volumetric Weight of product.
		$productVolumetricWeightInLbs = GetVolumetricWeightInLbs($value['productWidth'], $value['productHeight'], $value['productLength'], $value['productWeight'], $value['productWeightUnit']);

		if ($app->request->post('pickup_country') != $app->request->post('drop_off_country')) {
			$is_customs_duty_applied  = true;
			$weight = $value['productWeight'];

			if ($value['productWeightUnit'] == "kg") {
				$weight = kgToLb($weight);
			}

			if ($productVolumetricWeightInLbs > $weight) {
				$weight = $productVolumetricWeightInLbs;
			}

			if ($app->request->post('pickup_country') == "USA") {
				$params = array(
					"width" => $value['productWidth'],
					"height" => $value['productHeight'],
					"length" => $value['productLength'], 
					"weight" => $weight,
					"pickupPostalCode" => $app->request->post('pickup_zipcode'),
					"pickupStateProvinceCode" => $app->request->post('pickup_state_short_name'),
					"pickupCity" => $app->request->post('pickup_city'),
					"pickupCountryCode" => "US",
					"destinationPostalCode" => "19977",
					"service"=> "03"
				); 
				
		

				$ups = new upsRate();
				$ups->setCredentials();
				$ups_rate = $ups->getRate($params);
				$value['productUpsRate'] = $ups_rate;
				$return_array[$key]['productUpsRate'] =  $ups_rate;
				$total_ups_rate += $ups_rate;
				$is_ups_rate_applied = true;
			} else if ($app->request->post('drop_off_country') == "USA") {	// destination country.
				if ($value['productCost'] <= 2000) {
					$is_customs_duty_applied = false;
				}

				$params = array(
					"width" => $value['productWidth'],
					"height" => $value['productHeight'],
					"length" => $value['productLength'],
					"weight" => $weight,
					"pickupPostalCode" => "19977",
					"pickupStateProvinceCode" => "DE",
					"pickupCity" => "Smyrna",
					"pickupCountryCode" => "US",
					"destinationPostalCode" => $app->request->post('drop_off_zipcode'),
					"service"=> "03"
				);

				$ups = new upsRate();
				$ups->setCredentials();
				$ups_rate = $ups->getRate($params);
				$value['productUpsRate'] = $ups_rate;
				$return_array[$key]['productUpsRate'] =  $ups_rate;
				$total_ups_rate += $ups_rate;
				$is_ups_rate_applied = true;
			}
		}
		

		// UPS charge also applied in USA if state is diffrent.
		if ($app->request->post('pickup_country') == "USA" && $app->request->post('drop_off_country') == "USA") {
			if($app->request->post('pickup_state') != $app->request->post('drop_off_state')){
				$weight = $value['productWeight'];

				if ($value['productWeightUnit'] == "kg") {
					$weight = kgToLb($weight);
				}

				if ($productVolumetricWeightInLbs > $weight) {
					$weight = $productVolumetricWeightInLbs;
				}

				$params = array(
					"width" => $value['productWidth'],
					"height" => $value['productHeight'],
					"length" => $value['productLength'],
					"weight" => $weight,
					"pickupPostalCode" => "19977",
					"pickupStateProvinceCode" => "DE",
					"pickupCity" => "Smyrna",
					"pickupCountryCode" => "US",
					"destinationPostalCode" => $app->request->post('drop_off_zipcode'),
					"service"=> "03"
				);

				$ups = new upsRate();
				$ups->setCredentials();
				$ups_rate = $ups->getRate($params);
				$value['productUpsRate'] = $ups_rate;
				$return_array[$key]['productUpsRate'] =  $ups_rate;
				$total_ups_rate += $ups_rate;
				$is_ups_rate_applied = true;
			}
		}
		
		if ($weight > 160 && $is_ups_rate_applied) {
			$response['result']['note_html1'] = "Aquantuo will review and provide you with an updated cost based on the weight/dimensions provided.";
		}
		
		if ($value['productCategoryId'] == '') {
			$response = array('success' => 0, 'msg' => 'Category not found.');
			echoRespnse(200, $response);die;
		}
		$weight2 = get_weight($value['productWeight'], $value['productWeightUnit']);
		
		$total_weight += $weight2;
		
		$return_array[$key]['InsuranceCost'] = 0;
		if ($value['InsuranceStatus'] == 'yes') {
			
			//$return_array[$key]['InsuranceCost'] = get_insurance($value['productCost'], $value['productQty'], $Insurance_array);
			$in1 = $value['productCost'] * (int) $value['productQty'];
            $in2 =  $in1 * $configuration['main_insurance'];
            $return_array[$key]['InsuranceCost'] =  $in2 / 100;
			/*if ($return_array[$key]['InsuranceCost'] == 0) {
				$response['msg'] = "Sorry! We are not able to provide insurance for product '" . $value['item_name'] . "'";
				echoRespnse(200, $response);die;
			}*/
			$insurance += $return_array[$key]['InsuranceCost'];
		}
		
		$volume += get_volume(array(
			"width" => $value['productWidth'],
			"widthunit" => $value['productHeightUnit'],
			"height" => $value['productHeight'],
			"heightunit" => $value['productHeightUnit'],
			"length" => $value['productLength'],
			"lengthunit" => $value['productHeightUnit'],
		));
		
		$category = $collection->find(['_id' => new MongoId($value['productCategoryId']), 'TravelMode' => $value['travelMode'], 'Status' => 'Active']);
		if($category->count() == 0) {
				$response['success'] = 0;
				$response['msg'] = "Category not found";
				echoRespnse(200, $response);die;
		}
		$category = $category->getNext();
		$return_array[$key]['customDuty'] = 0;
		//get custom duty 
		if(isset($category['custom_duty']) && $is_customs_duty_applied){
			$total_custom_duty_rate +=  (($value['productCost'] * (int) $value['productQty']) * $category['custom_duty'])/100;
			$return_array[$key]['customDuty'] = (($value['productCost'] * (int) $value['productQty']) * $category['custom_duty'])/100;
		}
		if ($category['ChargeType'] == 'distance') {
			$categoryConstantArray = array(AUTO_FULLSIZE_SUV, AUTO_INTERMEDIATE_SUV, AUTO_SEDAN_ECONOMY, AUTO_SEDAN_FULLSIZE, AUTO_SEDAN_INTERMEDIATE, AUTO_STANDARD_SUV, AUTOMOBILE_VAN);

			if ($value['travelMode'] == 'ship' && in_array($value['productCategoryId'], $categoryConstantArray)) {
				foreach ($category['Shipping'] as $key2) {
					if ($distance >= $key2['MinDistance'] && $distance <= $key2['MaxDistance']) {
						if ($inputData['consolidate_check'] == 'on') {
							$return_array[$key]['shippingCost'] = $key2['CONSOLIDATE_RATE'] * (int) $value['productQty'];
							$rate += $key2['CONSOLIDATE_RATE'] * (int) $value['productQty'];
						} else {
							$return_array[$key]['shippingCost'] = $key2['Rate'] * (int) $value['productQty'];
							$rate += $key2['Rate'] * (int) $value['productQty'];
						}
					}		
				}
			} else {
				$cat_count = category_count($products_list, $value['productCategoryId']);

				foreach ($category['Shipping'] as $key2) {
					if ($distance >= $key2['MinDistance'] && $distance <= $key2['MaxDistance']) {
						if ($inputData['consolidate_check'] == 'on') {
							$return_array[$key]['shippingCost'] = $key2['CONSOLIDATE_RATE'] / $cat_count['count'];
							$rate += $key2['CONSOLIDATE_RATE'] / $cat_count['count'];
						} else {
							$return_array[$key]['shippingCost'] = $key2['Rate'] / $cat_count['count'];
							$rate += $key2['Rate'] / $cat_count['count'];
						}
					}		
				}
			}
		} else if ($value['travelMode'] == 'air' && ($value['productCategoryId'] != OtherCategory && $value['productCategoryId'] != ElectronicsCategory)) {
				$rate2 = get_category_price($set_air_category,$value['productCategoryId']);
				$cat_count = category_count($products_list,$value['productCategoryId']);
				$return_array[$key]['shippingCost'] = $rate2 / $cat_count['count'];
				$rate += $rate2 / $cat_count['count'];
				$res['air_rate'] += $rate2 / $cat_count['count'];
		} else if ($value['travelMode'] == 'air' && ($value['productCategoryId'] == OtherCategory || $value['productCategoryId'] != ElectronicsCategory)) {
			foreach ($category['Shipping'] as $key2) {
				if ($distance >= $key2['MinDistance'] && $distance <= $key2['MaxDistance']) {
					$is_macth = true;
					$return_array[$key]['shippingCost'] = $key2['Rate'] * (int) $value['productQty'];
					$rate += $key2['Rate'] * (int) $value['productQty'];
				}

				$last_price = $key2['Rate'];
			}

			if (!$is_macth && ($value['productCategoryId'] == OtherCategory || $value['productCategoryId'] == ElectronicsCategory)) {
				$itemWeight_1 = $weight2-2;
				$itemWeight_2 = intval($itemWeight_1/2);
				
				$rate_1 =  $itemWeight_2 * $category['weight_increase_by'];
				$rate_2 = $rate_1 + $last_price;
				if ($value['productCost'] > 1) {
					$p = $value['productCost'] - 1;
					$return_array[$key]['shippingCost'] = $rate_2 + ($p * $category['price_increase_by']) * (int) $value['productQty'];
					$rate += $rate_2 + ($p * $category['price_increase_by']) * (int) $value['productQty'];
												
				} else {
					$rate += $rate_2 * (int) $value['productQty'];
					$return_array[$key]['shippingCost'] = $rate_2 * (int) $value['productQty'];
				}
			}		
		} else {
			if (isset($category['Shipping']) && is_array($category['Shipping'])) {
				foreach ($category['Shipping'] as $key2) {
					if ($distance >= $key2['MinDistance'] && $distance <= $key2['MaxDistance']) {
						if($inputData['consolidate_check']=='on'){
							$return_array[$key]['shippingCost'] = $key2['CONSOLIDATE_RATE'] * (int) $value['productQty'];
							$rate += $key2['CONSOLIDATE_RATE'] * (int) $value['productQty'];
						}else{
							$return_array[$key]['shippingCost'] = $key2['Rate'] * (int) $value['productQty'];
							$rate += $key2['Rate'] * (int) $value['productQty'];
						}
					}
					
				}
			}
					
		}
		$totalItemCost += $value['productCost'] * (int) $value['productQty'];
	}
	
		

	if ($rate > 0) {
		
		// custome duty and tax 
		$DutyAndCustom = 0;
		if ($is_customs_duty_applied) {
			$DutyAndCustom = $total_custom_duty_rate;
		}

		$Tax =  $totalItemCost * $configuration['tax'] / 100;
		
		if ($is_ups_rate_applied) {
			$warehouse_transit_fee = $configuration['warehouse_transit_fee'];
		}
		
		$response['success'] = 1;
		$response['msg'] = "Calculation Data";
		$response['result']['total_no_of_item'] = count($products_list);
		$response['result']['shippingCost'] = $rate;
		// $response['result']['shippingCost'] = $rate + $total_ups_rate + $warehouse_transit_fee;
		$response['result']['insurance'] = $insurance;
		$response['result']['totalCost'] = $response['result']['shippingCost'] + $insurance + $DutyAndCustom + $Tax;

		$response['result']['distanceShow'] = number_format($distance, 2) . " Miles";
		$response['result']['distance'] = $distance;
		$response['result']['volume'] = number_format($volume, 2) . ' Cu.Cm';
		$response['result']['total_weight'] = number_format($total_weight, 2) . ' Lbs ';

		$response['result']['AreaCharges'] = resionCharges(['name' => $app->request->post('drop_off_state'), 'id' => $app->request->post('drop_off_state_id')]);

		$response['result']['AquantuoFees'] = ((floatval($response['result']['shippingCost']) * $configuration['aquantuoFees']) / 100);

		$response['result']['totalCost'] = $response['result']['AreaCharges'] + $response['result']['totalCost'];

		$response['result']['weight'] = $total_weight;
		$response['result']['volume'] = $volume;
		$response['result']['custom_duty'] = $DutyAndCustom;
		$response['result']['tax'] = $Tax;
		$response['result']['ups_rate'] = $total_ups_rate;
		$response['result']['item_total_price'] = $totalItemCost;
		$response['result']['ProcessingFees'] = 0;
		$response['result']['warehouse_transit_fee'] = $warehouse_transit_fee;
		

		$currency_conversion = currency_conversion($response['result']['totalCost']);
		if ($app->request->post('Default_Currency') == 'GHS') {
			$response['result']['GhanaTotalCost'] = $currency_conversion['ghanian_cost'];
		} else if ($app->request->post('Default_Currency') == 'CAD') {
			$response['result']['GhanaTotalCost'] = $currency_conversion['canadian_cost'];
		} else if ($app->request->post('Default_Currency') == 'PHP') {
			$response['result']['GhanaTotalCost'] = $currency_conversion['philipins_cost'];
		} else if ($app->request->post('Default_Currency') == 'GBP') {
			$response['result']['GhanaTotalCost'] = $currency_conversion['uk_cost'];
		} else if ($app->request->post('Default_Currency') == 'KES') {
			$response['result']['GhanaTotalCost'] = $currency_conversion['kenya_cost'];
		} else {
			$response['result']['GhanaTotalCost'] = $currency_conversion['ghanian_cost'];
		}
		$USDRate = 1;
		$GBPRate = $GHSRate = $CADRate = 1;
		if(isset($currency_conversion['CADRate'])){
			$CADRate  = $currency_conversion['CADRate'];
		}
		if(isset($currency_conversion['GHSRate'])){
			$GHSRate  = $currency_conversion['GHSRate'];
		}
		if(isset($currency_conversion['GBPRate'])){
			$GBPRate  = $currency_conversion['GBPRate'];
		}
		if(isset($currency_conversion['KESRate'])){
			$KESRate  = $currency_conversion['KESRate'];
		}
		$currencies = array(
						array('label'=>'USA - $','symbol'=>'$','value'=>1),
						array('label'=>'UK - £','symbol'=>'£','value'=>$GBPRate),
						array('label'=>'Ghana - GHS','symbol'=>'GHS','value'=>$GHSRate),
						array('label'=>'Canada - C$','symbol'=>'Can$','value'=>$CADRate),
						array('label'=>'Kenya - KES','symbol'=>'Ksh','value'=>$KESRate)
					);
		
		$response['result']['products_list'] = $return_array;
		$response['result']['currencies'] = $currencies;
		$response['result']['note_html'] = "If actual weight wasn’t provided, you may be billed or refunded any differences in price or shipping upon review.";

	} else {
		$response['msg'] = "Package info you entered does not match with our shipping conditions, Please check pacakge info and try again.";
	}
	echoRespnse(200, $response);

});

function calculate_volumetric_weight($info) {
	if ($info['type'] == 'lbs') {
		$volumetric_weight = ceil(($info['length'] * $info['width'] * $info['height']) / 139);
	} else {
		$volumetric_weight = ceil(($info['length'] * $info['width'] * $info['height']) / 5000);
	}

	return $volumetric_weight;
}

function GetVolumetricWeightInLbs($width, $height, $length, $weight, $type) {
	$VolumetricInfo = ["length" => $length, "height" => $height, "width" => $width, "type" => $type];
	$VolumetricWeight = calculate_volumetric_weight($VolumetricInfo);

	if ($weight > $VolumetricWeight) {
		$VolumetricWeight  = $weight;
	}

	$type = strtolower($type);

	if ($type == 'kg') {
		$VolumetricWeightLbs = (((float) $VolumetricWeight) * 2.20462);
	} else if ($type == 'gram') {
		$VolumetricWeightLbs = (((float) $VolumetricWeight) * 0.00220462);
	} else {
		$VolumetricWeightLbs = $VolumetricWeight;
	}
	return $VolumetricWeightLbs;
}

function kgToLb ($val) {
	return $val * 2.20462;
}

function get_category_price($category_set, $id) {
	$price = 0;
	foreach ($category_set as $key) {
		if ($key['cat_id'] == $id) {
			$price = $key['rate'];
		}
	}
	return $price;
}

function category_count($array, $cat_id) {
	$res['count'] = 0;
	$res['weight'] = 0;
	foreach ($array as $key => $value) {
		if ($cat_id == $value['productCategoryId']) {
			$res['count'] += 1;
			$res['weight'] += get_weight($value['productWeight'] * (int) $value['productQty'], $value['productWeightUnit']);
		}
	}
	return $res;
}

function set_air_category($array, $inputData) {
	global $db;
	$categoryColl = $db->category;
	$already_calculated_category = [];
	$category_set = [];
	foreach ($array as $key => $val) {
		if ($val['travelMode'] == 'air') {
			if (!in_array($val['productCategoryId'], $already_calculated_category)) {
			
				$cat_weight = category_count($array, $val['productCategoryId']);
				$weight = $cat_weight['weight'];
				$already_calculated_category[] = $val['productCategoryId'];

				$data = $categoryColl->find([
					'_id' => new MongoId($val['productCategoryId']),
					'Status' => 'Active',
					'TravelMode' => 'air',
					'ChargeType' => 'fixed',
				], ['Shipping','price_increase_by']);
				// print_r(json_decode(json_encode($data), true)); die();
				if (count($data) > 0) {
					$data = $data->getNext();
					$rate = 0;
					// ajay code here
						$is_other_category = 'no';
						// if category is other then new code works
						if ($data['_id'] == OtherCategory) {
							foreach ($data['Shipping'] as $key1) {
								if ($key1['MinDistance'] <= floatval($weight) && $key1['MaxDistance'] >= floatval($weight)) {
									$rate = $key1['Rate'];
									
									if($val['productCost']  > 1){
											$p = $val['productCost'] - 1;
											$rate = $rate + ($p * $data['price_increase_by']);
									}
									
								}
							}
							
						}else{
							foreach ($data['Shipping'] as $key1) {
								if ($key1['MinDistance'] <= floatval($weight) && $key1['MaxDistance'] >= floatval($weight)) {
									if($inputData['consolidate_check'] == 'on'){
										$rate = @$key1['CONSOLIDATE_RATE'];
									}else{
										$rate = $key1['Rate'];
									}
								}
							}
						}

						if($rate > 0){
							$category_set[]=[
								'cat_id'=>$val['productCategoryId'],
								'rate'=> $rate,
							];
						}
					

				}
			}
		}
	}

	return $category_set;
}

function currency_conversion($shipcost) {
	$data = ['cost' => 0, 'ghanian_cost' => 0, 'philipins_cost' => 0, 'uk_cost' => 0, 'canadian_cost' => 0];
	global $db;
	$collection = $db->currency;
	$currency = $collection->find(['Status' => 'Active'], ['CurrencyRate', 'FormatedText', 'CurrencyCode']);

	if ($currency->count() > 0) {
		//$currency = $currency->getNext();
		foreach ($currency as $key) {
			if ($key['CurrencyCode'] == 'GHS') {
				$data['GHSRate'] = $key['CurrencyRate'];
				$data['ghanian_cost'] = str_replace('[AMT]', number_format($shipcost * $key['CurrencyRate'], 2), $key['FormatedText']);
			} else if ($key['CurrencyCode'] == 'CAD') {
				$data['CADRate'] = $key['CurrencyRate'];
				$data['canadian_cost'] = str_replace('[AMT]', number_format($shipcost * $key['CurrencyRate'], 2), $key['FormatedText']);
			} else if ($key['CurrencyCode'] == 'PHP') {
				$data['PHPRate'] = $key['CurrencyRate'];
				$data['philipins_cost'] = str_replace('[AMT]', number_format($shipcost * $key['CurrencyRate'], 2), $key['FormatedText']);
			} else if ($key['CurrencyCode'] == 'GBP') {
				$data['GBPRate'] = $key['CurrencyRate'];
				$data['uk_cost'] = str_replace('[AMT]', number_format($shipcost * $key['CurrencyRate'], 2), $key['FormatedText']);
			} else if ($key['CurrencyCode'] == 'KES') {
				$data['KESRate'] = $key['CurrencyRate'];
				$data['kenya_cost'] = str_replace('[AMT]', number_format($shipcost * $key['CurrencyRate'], 2), $key['FormatedText']);
			}
		}
	}
	return $data;
}

function get_insurance($cost, $quantity, $data) {
	
	$configuration = $configuration->find(['_id' => new MongoId('5673e33e6734c4f874685c84')], ['Insurance', 'aquantuoFees','main_insurance']);
	$value = 0;
	if ($configuration->count() > 0) {
		$configuration = $configuration->getNext();
		$totalPrice = $cost * (int) $quantity;
        $insurance =  $totalPrice * $configuration['main_insurance'];
        $value =  $insurance / 100;

	}
	
	return $value;
	
}

function get_weight($weight, $type) {
	$type = strtolower($type);
	if ($type == 'kg') {
		return (((float) $weight) * 2.20462);
	} else if ($type == 'gram') {
		return (((float) $weight) * 0.00220462);
	} else {
		return $weight;
	}
}

function get_volume($array) {
	
	if($array['lengthunit']=="inches"){
		return (getSizeInInches($array['length'], $array['lengthunit']) * getSizeInInches($array['width'], $array['widthunit']) * getSizeInInches($array['height'], $array['heightunit']));
	}else{
		return (get_size_in_cm($array['length'], $array['lengthunit']) * get_size_in_cm($array['width'], $array['widthunit']) * get_size_in_cm($array['height'], $array['heightunit']));
	}
}

function get_size_in_cm($height, $unit) {
	$in_cm = (float) $height;
	$unit = strtolower($unit);
	if ($unit == 'inches') {
		$in_cm = $in_cm * 2.54;
	}
	return $in_cm;
}

function getSizeInInches($whl, $unit) {
		$in_inches = (float) $whl;
		$unit = strtolower($unit);
		if ($unit == "cm") {
			$in_inches = $in_inches / 2.54;
		}
		return $in_inches;
}

function getDistanceBetweenPointsNew($latitude1, $longitude1, $latitude2, $longitude2, $unit = 'Meter') {
	$latitude1 = floatval($latitude1);
	$longitude1 = floatval($longitude1);
	$latitude2 = floatval($latitude2);
	$longitude2 = floatval($longitude2);

	$theta = $longitude1 - $longitude2;
	$distance = (sin(deg2rad($latitude1)) * sin(deg2rad($latitude2))) + (cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * cos(deg2rad($theta)));
	$distance = acos($distance);
	$distance = rad2deg($distance);
	$distance = $distance * 60 * 1.1515;switch ($unit) {
	case 'Mi':break;case 'Km':$distance = $distance * 1.609344;
		break;case 'Meter':$distance = $distance * 1609.34;
		break;
	}
	return (round($distance, 2));
}

$app->post('/create_send_package', 'requester_auth', function () use ($app) {
	global $db;
	global $userId;
	global $userInformation;

	$collection = $db->delivery_request;
	$settingColl = $db->system_setting;
	$usercoll = $db->users;

	$sendmail_col = $db->sendmail;

	$configuration_coll = $db->configuration;
	$configurationdata = $configuration_coll->find(array('_id' => new MongoId('5673e33e6734c4f874685c84')));
	$configurationdata = $configurationdata->getNext();

	require 'Slim/library/Sequence.php';
	verifyRequiredParams(array('products_list'));
	$response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];

	if ($app->request->post('RequestId') != '') {
		$req_data = $collection->find(['_id' => new MongoId($app->request->post('RequestId')), 'Status' => 'pending'], ['PackageNumber', 'PackageId']);
		//echo 'sd'; die;
		if ($req_data->count() == 0) {
			$response = ['success' => 0, 'msg' => 'Oops! Something went wrong.'];
			echoRespnse(200, $response);die;
		}

		$req_data = $req_data->getNext();
		$PackageId = $req_data['PackageId'];
		$pn = $req_data['PackageNumber'];

	} else {
		$PackageId = getSequence('Request');
		$pn = $PackageId . time();
	}

	$ProductImage = $pickUpDate = $deliveryDate = '';
	if ($app->request->post('pickUpDate') != '') {
		$pickUpDate = new MongoDate(strtotime($app->request->post('pickUpDate')));
	}

	if ($app->request->post('deliveryDate') != '') {
		$deliveryDate = new MongoDate(strtotime($app->request->post('deliveryDate')));
	}

	/*Trip Section*/
	$requestStatus = 'pending';
	$tripDetail = '';
	$tripid = $transporterName = $transporterId = '';
	if ($app->request->post('transporterId') != '' && $app->request->post('tripId') != '') {
		if (strlen($app->request->post('tripId')) == 24 && strlen($app->request->post('transporterId')) == 24) {

			$tripColl = $db->trips;
			$tripid = new MongoId($app->request->post('tripId'));
			$transporterId = new MongoId($app->request->post('transporterId'));
			$tripObj = $tripColl->find(array('_id' => $tripid, 'TransporterId' => $transporterId));

			if ($tripObj->count() > 0) {
				$tripObj = $tripObj->getNext();
				if ($tripObj['TripType'] == 'individual') {
					if ($tripObj['SourceDate'] < new MongoDate()) {
						echoRespnse(200, array('success' => 0, 'msg' => 'Trip has been started. You are not able to create request.'));
						$app->stop();
					}
				}
				$requestStatus = 'pending_trip';
				$transporterName = @$tripObj['TransporterName'];
				$transporterinfo = $usercoll->find(array('_id' => $transporterId), array('Name', 'Email', 'TPSetting', 'EmailStatus', 'NoficationStatus'));
				if ($transporterinfo->count() > 0) {
					$transporterinfo = (Object) $transporterinfo->getNext();
				} else {
					$transporterinfo = '';
				}

			} else {
				echoRespnse(200, array('success' => 0, 'msg' => 'Invalid trip or transporter.'));
				$app->stop();
			}

		} else {
			echoRespnse(200, array('success' => 0, 'msg' => 'Invalid trip or transporter.'));
			$app->stop();
		}
	}
	/*End Trip*/

	$user_info = $usercoll->find(['_id' => $userId]);
	$user_info = $user_info->getNext();

	$insertData = [
		'RequesterId' => new MongoId($userId),
		'RequesterName' => ucfirst($user_info['Name']),
		'ProductTitle' => '',
		'RequestType' => "delivery",
		'api'=>'8',
		'Status' => 'pending',
		'request_version' => 'new',
		'ReceiverIsDifferent' => $app->request->post('ReceiverIsDifferent'),
		'ReceiverName' => ucfirst($app->request->post('ReceiverName')),
		/*for trip*/
		"TransporterName" => $transporterName,
		"TransporterId" => $transporterId,
		"TripId" => trim($app->request->post('tripId')),
		/*end*/
		'PackageId' => $PackageId,
		'device_version' => $app->request->post('browser'),
		'app_version' => $app->request->post('version'),
		'device_type' => $app->request->post('device_type'),
		"PackageNumber" => $pn,
		"PickupFullAddress" => get_formatted_address(array($app->request->post('pickupAddress'),
			$app->request->post('pickupAddress2'),
			$app->request->post('pickupCity'),
			$app->request->post('pickupState'),
			$app->request->post('pickupCountry'),
		), $app->request->post('pickupPinCode')
		),
		"PickupAddress" => $app->request->post('pickupAddress'),
		"PickupAddress2" => $app->request->post('pickupAddress2'),
		"PickupCity" => $app->request->post('pickupCity'),
		"PickupState" => $app->request->post('pickupState'),
		"PickupCountry" => $app->request->post('pickupCountry'),
		"PickupPinCode" => $app->request->post('pickupPinCode'),
		"PickupLatLong" => array(floatval($app->request->post('pickuplong')), floatval($app->request->post('pickuplat'))),
		"PickupDate" => $pickUpDate,
		"DeliveryFullAddress" => get_formatted_address(array($app->request->post('deliveryAddress'),
			$app->request->post('deliveryAddress2'),
			$app->request->post('deliveryCity'),
			$app->request->post('deliveryState'),
			$app->request->post('deliveryCountry'),
		), $app->request->post('deliveryPincode')
		),
		"DeliveryAddress" => $app->request->post('deliveryAddress'),
		"DeliveryAddress2" => $app->request->post('deliveryAddress2'),
		"DeliveryCity" => $app->request->post('deliveryCity'),
		"DeliveryState" => $app->request->post('deliveryState'),
		"DeliveryCountry" => $app->request->post('deliveryCountry'),
		"DeliveryPincode" => $app->request->post('deliveryPincode'),
		"DeliveryLatLong" => array(floatval($app->request->post('deliverylong')), floatval($app->request->post('deliverylat'))),
		"DeliveryDate" => $deliveryDate,

		"DeliveryStateId" => $app->request->post('state_id'),
		"DeliveryCountryId" => $app->request->post('country_id'),

		"ReturnFullAddress" => get_formatted_address(array($app->request->post('returnAddress'),
			$app->request->post('returnAddress2'),
			$app->request->post('returnCity'),
			$app->request->post('returnState'),
			$app->request->post('returnCountry'),
		), $app->request->post('returnPincode')
		),
		"ReturnAddress" => $app->request->post('returnAddress'),
		"ReturnAddress2" => $app->request->post('returnAddress2'),
		"ReturnCityTitle" => $app->request->post('returnCity'),
		"ReturnStateTitle" => $app->request->post('returnState'),
		"ReturnCountry" => $app->request->post('returnCountry'),
		"ReturnPincode" => $app->request->post('returnPincode'),
		"FlexibleDeliveryDate" => strtolower($app->request->post('flexibleDeliveryDate')),
		"consolidate_item" => ($app->request->post('consolidate_item') == 'on') ? 'on' : 'off',
		"JournyType" => ($app->request->post('journyType') == 'return') ? 'return' : 'one_way',
		"NotDelReturnFullAddress" => get_formatted_address(array($app->request->post('inCaseNotDelReturnAddress'),
			$app->request->post('inCaseNotDelReturnAddress2'),
			$app->request->post('inCaseNotDelReturnCity'),
			$app->request->post('inCaseNotDelReturnState'),
			$app->request->post('inCaseNotDelReturnCountry'),
		), $app->request->post('inCaseNotDelReturnPincode')
		),
		"ReturnAddressSamePickup" => $app->request->post('ReturnAddressSamePickup'),
		"InCaseNotDelReturnAddress" => $app->request->post('inCaseNotDelReturnAddress'),
		"InCaseNotDelReturnAddress2" => $app->request->post('inCaseNotDelReturnAddress2'),
		"InCaseNotDelReturnCity" => $app->request->post('inCaseNotDelReturnCity'),
		"InCaseNotDelReturnState" => $app->request->post('inCaseNotDelReturnState'),
		"InCaseNotDelReturnCountry" => $app->request->post('inCaseNotDelReturnCountry'),
		"InCaseNotDelReturnPincode" => $app->request->post('inCaseNotDelReturnPincode'),
		"InCaseNotDelReturnSamePickup" => $app->request->post('InCaseNotDelReturnSamePickup'),
		"PackageMaterialShipped" => '',
		"PublicPlace" => ($app->request->post('public_palce') == 'yes') ? 'yes' : 'no',
		'ReceiverCountrycode' => $app->request->post('countryCode'),
		'ReceiverMobileNo' => $app->request->post('receiverMobileNo'),
		"Distance" => $app->request->post('distance'),
		'ShippingCost' => $app->request->post('shippingCost'),
		"totalUpsRate" => 0,
		"WareHouseTransitFee"=> 0,
		'InsuranceCost' => 0,
		'Discount' => floatval($app->request->post('discount')),
		'DutyAndCustom'=>0,
		'Tax'=>0,
		'AquantuoFees' => 0,
		'region_charge' => 0,
		'TotalCost' => 0,
		"StripeChargeId" => '',
		"PaymentDate" => "",
		"PaymentStatus" => 'pending',
		'promocode' => $app->request->post('promocode'),
		'ProductList' => [],
		'after_update_difference' => 0,
		'itemCount' => 0,
		"EnterOn" => new MongoDate(),
		"UpdateOn" => new MongoDate(),
		'ProductList' => [],
	];

	$products_list = json_decode($app->request->post('products_list'), 1);

	$k = 0;
	
	foreach ($products_list as $key) {
		$k = $k + 1;
		// $insertData['ShippingCost'] += $key['shippingCost']+ $key['productUpsRate'];
		$insertData['InsuranceCost'] += $key['InsuranceCost'];
		$insertData['TotalCost'] += $key['shippingCost'] + $key['InsuranceCost']+ $key['productUpsRate'];
		$insertData['DutyAndCustom'] += $key['customDuty'];
		$fees = ((floatval($key['shippingCost']) * $configurationdata['aquantuoFees']) / 100);
		$insertData['AquantuoFees'] += $fees;
		$insertData['totalUpsRate'] += $key['productUpsRate'];

		if (!empty(trim($insertData["ProductTitle"]))) {
			$insertData["ProductTitle"] = "{$insertData['ProductTitle']},";
		}

		$insertData["ProductTitle"] .= @$key['item_name'];
		$insertData['ProductList'][] = [
			'_id' => (string) new MongoId(),
			"product_name" => @$key['item_name'],
			'package_id' => (@$key['package_id'] != '') ? $key['package_id'] : $pn . $k,
			'status' => 'pending',
			"productUpsRate" => $key['productUpsRate'],
			"productWidth" => $key['productWidth'],
			"productHeight" => $key['productHeight'],
			"productLength" => $key['productLength'],
			"productCost" => $key['productCost'],
			"productWeight" => $key['productWeight'],
			"productHeightUnit" => $key['productHeightUnit'],
			"ProductWeightUnit" => $key['productWeightUnit'],
			"ProductLengthUnit" => $key['productHeightUnit'],
			"productCategory" => $key['productCategory'],
			"productCategoryId" => $key['productCategoryId'],
			"travelMode" => $key['travelMode'],
			"InsuranceStatus" => $key['InsuranceStatus'],
			"productQty" => $key['productQty'],
			"ProductImage" => $key['ProductImage'],
			"QuantityStatus" => $key['QuantityStatus'],
			"Description" => $key['description'],
			"PackageMaterial" => (@$key['need_package_material'] == 'yes') ? 'yes' : 'no',
			"PackageMaterialShipped" => "",
			"shippingCost" => floatval($key['shippingCost']),
			'InsuranceCost' => floatval($key['InsuranceCost']),
			"DeliveryVerifyCode" => rand(1000, 9999),
			'tpid' => (String) $transporterId,
			'tpName' => $transporterName,
			'inform_mail_sent' => 'no',
			'PaymentStatus' => 'no',
			'aq_fees' => floatval($fees),
			'total_cost' => $key['shippingCost'] + $key['InsuranceCost'],
			'after_update' => $key['shippingCost'] + $key['InsuranceCost'],
			'TransporterFeedbcak' => '',
			'TransporterRating' => '',
			'RequesterFeedbcak' => '',
			'RequesterRating' => '',
			'TransporterMessage' => '',
			'ExpectedDate' => '',
			'DeliveredTime' => '',
		];
	}
	$insertData['itemCount'] = $k;
	$insertData['TotalCost'] = $insertData['TotalCost'] - $app->request->post('discount');
	//$insertData['AquantuoFees'] = get_aquantuo_fees($insertData['TotalCost']);
	$insertData['TotalCost'] = $insertData['TotalCost'];
	// $insertData['ShippingCost'] += $app->request->post('warehouse_transit_fee');

	//Resion Charges
	$insertData['region_charge'] = resionCharges(['name' => $app->request->post('state_name'), 'id' => $app->request->post('state_id')]);
	$insertData['TotalCost'] = $insertData['TotalCost'] + $insertData['region_charge'] + $insertData['DutyAndCustom']+$app->request->post('tax')+$app->request->post('warehouse_transit_fee');
	$insertData['WareHouseTransitFee'] = $app->request->post('warehouse_transit_fee');
	$insertData['Tax'] = $app->request->post('tax');
	//end

	if ($app->request->post('RequestId') == '') {
		$res = $collection->insert($insertData);
		if ($res['ok'] == 1) {
			$response = array(
				'success' => 1,
				'msg' => 'Request has been created successfully. Please proceed to payment page to make payment',
				"packageId" => (string) $insertData['_id'],
				"totalcost" => $insertData['TotalCost'], 
				'request_id'=> $PackageId,
				"Status" => $insertData['Status'],
				"PackageNumber" => $insertData['PackageNumber'],
			);
		}

		$cron_mail = [
			"USERNAME" => ucfirst($user_info['Name']),
			"PACKAGETITLE" => ucfirst($insertData['ProductTitle']),
			"PACKAGEID" => $insertData['PackageNumber'],
			"SOURCE" => $insertData['PickupFullAddress'],
			"DESTINATION" => $insertData['DeliveryFullAddress'],
			'email_id' => '58773d2d7ac6f61c198b4567',
			'email' => $user_info['Email'],
			'status' => 'ready',
		];
		//$sendmail_col->insert($cron_mail);

	} else {

		$res = $collection->update(array('_id' => new MongoId($app->request->post('RequestId'))), array('$set' => $insertData));

		if ($res['ok'] == 1) {

			$msg = 'The send a package request titled:"'.$insertData['ProductTitle'].'" ID:'.$insertData['PackageNumber'].'has been updated.';

			require_once 'vendor/Notification/Notification.php';
			$pushnoti = new Notification();
			$pushnoti->setValue('title', 'Update request of Send a package');
			$pushnoti->setValue('message', $msg);
			$pushnoti->setValue('location', 'delivery');
			$pushnoti->setValue('locationkey', $app->request->post('RequestId'));
			$pushnoti->add_user($user_info['NotificationId'] ,$user_info['DeviceType']);
			//$pushnoti->fire();

			
			$response = array(
				'success' => 1,
				'msg' => 'Your request has been updated Successfully. Please proceed to payment method.',
				"packageId" => $app->request->post('RequestId'),
				"totalcost" => $insertData['TotalCost'], 
				'request_id'=> $PackageId,
				"Status" => $insertData['Status'],
				"PackageNumber" => $insertData['PackageNumber'],
			);
		}
	}
	require_once 'notify.php';
	$inputArray = [
		'requestid' => @$insertData['_id'],
		'_id' => @$insertData['_id'],
		'log_type' => "request",
		'message' => "Request Created.",
		'action_user_id' => (string) $userId,
	];
	activityLog($inputArray);

	echoRespnse(200, $response);

});

function resionCharges($state) {
	global $db;
	$collection = $db->extraregion;

	$charges = $collection->find(['state_id' => $state['id'], 'status' => 'Active'], ['amount']);
	if ($charges->count() > 0) {
		$charges = $charges->getNext();
		return $charges['amount'];
	} else {
		return 0.00;
	}
}

function get_formatted_address($array, $zipcode = '') {
	$address = '';
	foreach ($array as $key) {
		if (!empty($key)) {
			$address .= (($address != '') ? ", $key" : $key);
		}
	}
	if (!empty($zipcode)) {$address .= " - $zipcode";}
	return $address;
}

$app->get('/delivery_detail', 'requester_auth', function () use ($app) {
	global $db;
	global $userId;
	verifyRequiredParams(array('requestId'));
	$response = array('success' => 0, 'msg' => 'No Records Found');
	$requestCollection = $db->delivery_request;
	$usercollection = $db->users;

	$requestData = $requestCollection->find(array('_id' => new MongoId($app->request->get('requestId')), 'RequesterId' => $userId));
	$itemhistory = $db->itemhistory;
	$itemhistory = $itemhistory->find(['request_id' => $app->request->get('requestId')]);
	if ($requestData->count() > 0) {
		$card = $db->request_cards->find(['request_id' => $app->request->get('requestId')]);
		$requestData = $requestData->getNext();

		$requester_info = $usercollection->find(['_id' => $userId], ['Default_Currency']);
		if ($requester_info->count() > 0) {
			$requester_info = $requester_info->getNext();

			$currency_res = currency_conversion($requestData['TotalCost']);
			$currency_res2 = currency_conversion(@$requestData['need_to_pay']);

			$total_cost_currency = $currency_res['ghanian_cost'];
			$need_to_pay_currency = $currency_res2['ghanian_cost'];
			if ($requester_info['Default_Currency'] != '') {
				if ($requester_info['Default_Currency'] == 'CAD') {
					$total_cost_currency = $currency_res['canadian_cost'];
					$need_to_pay_currency = $currency_res2['canadian_cost'];

				} else if ($requester_info['Default_Currency'] == 'PHP') {
					$total_cost_currency = $currency_res['philipins_cost'];
					$need_to_pay_currency = $currency_res2['philipins_cost'];

				} else if ($requester_info['Default_Currency'] == 'GBP') {
					$total_cost_currency = $currency_res['uk_cost'];
					$need_to_pay_currency = $currency_res2['uk_cost'];

				}
			}
		}

		$requestData['PackageId'] = (String) $requestData['_id'];
		$requestData['requestId'] = (String) (String) $requestData['_id'];
		unset($requestData['_id']);
		$requestData['RequesterId'] = (String) @$requestData['RequesterId'];

		$requestData['EnterOn'] = (String) @$requestData['EnterOn']->sec;
		$requestData['UpdateOn'] = (String) @$requestData['UpdateOn']->sec;
		$requestData['PickupDate'] = (String) @$requestData['PickupDate']->sec;
		$requestData['DeliveryDate'] = (String) @$requestData['DeliveryDate']->sec;
		$return_array = $requestData['ProductList'];
		$requestData['ShippingCost'] = $requestData['ShippingCost'];

		foreach ($return_array as $key => $val) {
			//$return_array[$key]['show_status'] = get_status_title($val['status'], $requestData['RequestType']);

			if ($val['status'] == 'cancel' && @$val['RejectBy'] != '') {
				$return_array[$key]['show_status'] = "Not Delivered";
			} else {
				$return_array[$key]['show_status'] = get_status_title($val['status'], $requestData['RequestType']);
			}
			
			if ($val['tpid'] != '' && strlen($val['tpid']) == 24) {

				$usercollection2 = $usercollection->find(['_id' => new MongoId($val['tpid'])], ['Name', 'Image', 'ChatName', 'RatingByCount', 'RatingCount']);

				if ($usercollection2->count() > 0) {
					$usercollection2 = (Object) $usercollection2->getNext();

					$return_array[$key]["ChatUserName"] = $usercollection2->Name;
					$return_array[$key]["ChatUserImage"] = $usercollection2->Image;
					$return_array[$key]["ChatUserId"] = (string) $usercollection2->_id;
					$return_array[$key]['UserRating'] = 0;
					$return_array[$key]['feedback'] = '';

					$return_array[$key]['PickupLat'] = @$requestData['PickupLatLong'][1];
					$return_array[$key]['PickupLong'] = @$requestData['PickupLatLong'][0];
					$return_array[$key]['DeliveryLat'] = @$requestData['DeliveryLatLong'][1];
					$return_array[$key]['DeliveryLong'] = @$requestData['DeliveryLatLong'][0];

					if ($usercollection2->RatingCount > 0 && $usercollection2->RatingByCount > 0) {
						$return_array[$key]['UserRating'] = $usercollection2->RatingCount / $usercollection2->RatingByCount;
					}
				}

				$return_array[$key]['tpid'] = (string) $val['tpid'];
				
				
			}
			$return_array[$key]['tpid'] = ($val['tpid'] != '') ? $val['tpid'] : '';
		}

		$response['result'] = array(

			'Title' => $requestData['ProductTitle'],
			'requestId'=> $requestData['requestId'],
			
			'PackageId' => $requestData['PackageNumber'],
			'PackageNumber' => $requestData['PackageNumber'],
			'RequestType' => $requestData['RequestType'],
			'RequesterName' => $requestData['RequesterName'],
			'RequestDate' => $requestData['EnterOn'],
			'PickupDate' => $requestData['PickupDate'],
			'PickupAddress' => $requestData['PickupFullAddress'],
			'PickupCountry' => $requestData['PickupCountry'],
			'PickupPinCode' => $requestData['PickupPinCode'],
			'DeliveryAddress' => $requestData['DeliveryFullAddress'],
			'DeliveryDate' => $requestData['DeliveryDate'],
			'TransporterName' => @$requestData['TransporterName'],
			'TransporterId' => (string) @$requestData['TransporterId'],
			'PublicPlace' => ucfirst($requestData['PublicPlace']),
			'FlexibleDeliveryDate' => ucfirst($requestData['FlexibleDeliveryDate']),
			'DeliveryCountry' => $requestData['DeliveryCountry'],
			'ReturnAddress' => $requestData['ReturnFullAddress'],
			'ReturnCountry' => $requestData['ReturnCountry'],
			'ReturnPincode' => $requestData['ReturnPincode'],
			'Status' => $requestData['Status'],
			"DeliveryStatus" => $requestData['Status'],
			"DeliveryStatusToShow" => get_status_title($requestData['Status'], $requestData['RequestType']),
			"RequesterPaid" => number_format($requestData['TotalCost'], 2),
			"AquantuoFess" => @$requestData['AquantuoFees'],
			"TransporterEarnings" => number_format(($requestData['TotalCost']-@$requestData['AquantuoFees']), 2),
			'NotDeliveredReturnAddress' => $requestData['NotDelReturnFullAddress'],
			"DeliveryVerifyCode" => @$requestData["DeliveryVerifyCode"],
			"JournyType" => ucwords(str_replace('_', ' ', $requestData["JournyType"])),
			"TrackingNumber" => @$requestData['TrackingNumber'],
			"RejectBy" => ucfirst(@$requestData['RejectBy']),
			"ReturnType" => ucfirst(@$requestData['ReturnType']),
			"ReceiptImage" => @$requestData['ReceiptImage'],
			"PackageMaterialShipped" => ucfirst(@$requestData['PackageMaterialShipped']),
			"ChatUserId" => (String) @$requestData['TransporterId'],
			"ChatUserName" => "",
			"ChatUsrImage" => "",
			"ChatName" => "",
			"TravelMode" => @$requestData['TravelMode'],
			"TransporterFeedbcak" => @$requestData['TransporterFeedbcak'],
			"TransporterRating" => @$requestData['TransporterRating'],
			"RequesterRating" => @$requestData['RequesterRating'],
			"RequesterFeedbcak" => @$requestData['RequesterFeedbcak'],
			"InsuranceCost" => @$requestData['InsuranceCost'],
			"TotalCost" => @$requestData['TotalCost'] + $requestData['after_update_difference'],
			"ShippingCost" => @$requestData['ShippingCost'],
			"Discount" => $requestData['Discount'],
			
			"promocode" => $requestData['promocode'],
			"UserRating" => 0,
			"TransporterMessage" => @$requestData['TransporterMessage'],
			"DeliveredTime" => @$requestData['DeliveredTime']->sec,
			'ReceiverMobileNo' => @$requestData['ReceiverMobileNo'],
			'ReceiverCountrycode' => @$requestData['ReceiverCountrycode'],
			'after_update_difference' => @$requestData['after_update_difference'],
			'region_charge' => @$requestData['region_charge'],
			'ReturnAddressSamePickup' => @$requestData['ReturnAddressSamePickup'],
			'InCaseNotDelReturnSamePickup' => @$requestData['InCaseNotDelReturnSamePickup'],
			'consolidate_item' => @$requestData['consolidate_item'],
			'ReceiverIsDifferent' => @$requestData['ReceiverIsDifferent'],
			'ReceiverName' => @$requestData['ReceiverName'],
			'ProductList' => $return_array,
			'need_to_pay' => @$requestData['need_to_pay'],
			'total_cost_currency' => @$total_cost_currency,
			'need_to_pay_currency'=> @$need_to_pay_currency,
		);
		$response['result']['DutyAndCustom'] = @$requestData['DutyAndCustom'];
		$response['result']['Tax'] = @$requestData['Tax'];
		$response['result']['ups_rate'] = @$requestData['totalUpsRate'];

		$requestData['ChatUserName'] = '';
		$requestData['ChatUsrImage'] = '';
		$requestData['ChatName'] = '';
		$requestData['UserRating'] = '';

		if (@$requestData['TransporterId'] != '') {
			$transporterinfo = $usercollection->find(array('_id' => $requestData['TransporterId']), array('ChatName', 'Name', 'Image', 'RatingCount', 'RatingByCount'));
			if ($transporterinfo->count() > 0) {
				$transporterinfo = (Object) $transporterinfo->getNext();

				$response['result']['ChatUserName'] = $transporterinfo->Name;
				$requestData['ChatUserName'] = $transporterinfo->Name;
				$response['result']['ChatUsrImage'] = $transporterinfo->Image;
				$requestData['ChatUsrImage'] = $transporterinfo->Image;
				$response['result']['ChatName'] = $transporterinfo->ChatName;
				$requestData['ChatName'] = $transporterinfo->ChatName;
				if (@$transporterinfo->RatingCount > 0 && @$transporterinfo->RatingByCount > 0) {
					$response['result']['UserRating'] = (@$transporterinfo->RatingCount / @$transporterinfo->RatingByCount);
					$requestData['UserRating'] = (@$transporterinfo->RatingCount / @$transporterinfo->RatingByCount);
				}
			}
			$requestData['TransporterId'] = (string) $requestData['TransporterId'];

		}

		$response['success'] = 1;
		$response['msg'] = "Records found";

		$requestData['PickupLat'] = $requestData['PickupLatLong'][1];
		$requestData['PickupLong'] = $requestData['PickupLatLong'][0];
		$requestData['DeliveryLat'] = $requestData['DeliveryLatLong'][1];
		$requestData['DeliveryLong'] = $requestData['DeliveryLatLong'][0];
		$requestData['TripType'] = "";

		if (@$requestData['TripId'] != '') {
			$tripColl = $db->trips;
			$tripid = new MongoId($requestData['TripId']);
			$tripObj = $tripColl->find(array('_id' => $tripid));
			if ($tripObj->count() > 0) {
				$tripObj = $tripObj->getNext();
				$requestData['TripType'] = $tripObj['TripType'];
			}
		}

		$response['result']['cards'] = [];
		//print_r($card->count()); die;
		if ($card->count() > 0) {
			///$card = $card->getNext();
			foreach ($card as $key) {
				$response['result']['cards'][] = [
					'last4' => $key['last4'],
					'brand' => $key['brand'],
					'amount' => $key['payment'],
				];
			}
		}
		$response['result']['updated_data'] = [];
		if ($itemhistory->count() > 0 && isset($requestData['need_to_pay'])) {
			foreach ($itemhistory as $key) {
				$response['result']['updated_data'][] = ['msg' => "After reviewing your listing, '" . $key['product_name'] . "', the cost was increased by $" . number_format(@$key['update_difference'], 2) . "."];

			}
		}
		$response['result']['total_cost_currency'] = $total_cost_currency;
		$response['editResponse'] = $requestData;
	}
	echoRespnse(200, $response);
});

function get_aquantuo_fees($totalcost) {
	global $db;
	$fees = 0;
	$configuration_coll = $db->configuration;
	$configurationdata = $configuration_coll->find(array('_id' => new MongoId('5673e33e6734c4f874685c84')));
	if ($configurationdata->count() > 0) {
		$configurationdata = $configurationdata->getNext();
		$fees = ((floatval($totalcost) * $configurationdata['aquantuoFees']) / 100);
	}
	return $fees;
}

function get_status_title($status, $requesttype = '') {
	switch ($status) {
	case 'ready':
		return 'Ready';
		break;
	case 'trip_pending':
		return 'Pending';
		break;
	case 'trip_ready':
		return 'Ready';
		break;
	case 'out_for_delivery':
		return 'Out for Delivery';
		break;
	case 'out_for_pickup':
		return 'Out for Pickup';
		break;
	case 'accepted':
		return 'Accepted';
		break;
	case 'delivered':
		return 'Delivered';
		break;
	case 'cancel':
		return 'Canceled';
		break;
	case 'cancel_by_admin':
		return 'Cancelled by Admin';
		break;
	case 'purchased':
		return ($requesttype == 'online') ? 'Item Received' : 'Item Purchased';
		break;
	case 'assign':
		return 'Transporter Assigned';
		break;
	case 'paid':
		return ($requesttype == 'online') ? 'Waiting for Item to be Received' : 'Pending Purchase';
		break;
		break;
	case 'reviewed':
		return 'Reviewed';
		break;
	case 'not_purchased':
		return 'Waiting for your payment';
		break;
	case 'item_received':
		return 'Item Received';
		break;
	case 'momo_initiated':
		return 'MoMo Initiated';
		break;
	case 'shipment_departed':
		return 'Shipment Departed';
		break;
	default:
		return 'Pending';

	}
}
$app->run();
