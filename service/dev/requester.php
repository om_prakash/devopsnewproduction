<?php
/**
 * Step 1: Require the Slim Framework
 *
 * If you are not using Composer, you need to require the
 * Slim Framework and register its PSR-0 autoloader.
 *
 * If you are using Composer, you can skip this step.
 *
 * Filename Name : authentication.php
 * File Path : services/authentication.php
 * Description : This file contains method related to userinformation.
 * Author: Ravi shukla
 * Created Date : 22-08-2015
 * Library : Email,DbConnect
 *
 * */

require 'Slim/Slim.php';
require 'Slim/config/DbConnect.php';
require 'Slim/library/Common.php';

\Slim\Slim::registerAutoloader();

/**
 * Step 2: Instantiate a Slim application
 *
 * This example instantiates a Slim application using
 * its default settings. However, you will usually configure
 * your Slim application now by passing an associative array
 * of setting names and values into the application constructor.
 */
$app = new \Slim\Slim();

$userId = '';
$userType = '';

/*
 * Function Name :  prepare_request_calculation
 * Description : prepare Calulation
 * Url : http://192.168.11.101/aq/services/requester.php/calculate_request_cost
 * Method : POST
 * Parameter : distance=1000&productHeight=1&productHeightUnit=meter&productWeightUnit=kg&productLength=1&productLengthUnit=meter&productWidthUnit=meter&productWidth=meter&productCost=50
 * Library : Sequence
 * Created By : Ravi shukla
 * Create Date : 24-08-2015
 *
 * */

$app->post('/calculate_request_cost', function () use ($app) {
	global $db;
	global $userId;

	include_once 'Slim/library/Requesthelper.php';

	$collection = $db->delivery_request;
	$settingColl = $db->system_setting;
	$currencycoll = $db->city_state_country;

	$response = array('success' => 0, 'msg' => '');

	verifyRequiredParams(array('travelMode', 'productWeightUnit', 'distance', 'productCost', 'productCategory', 'insuranceStatus'));

	$productSize = $ProductWeightInPound = $urgentcost = $distanceInMiles = $urgentcost_per_km = $insurance = $cost_per_km = 0;
	$distance = (float) $app->request->post('distance');

	$requesthelper = new Requesthelper(array(
		"needInsurance" => $app->request->post('insuranceStatus'),
		"productQty" => ($app->request->post('productQty') < 1) ? 1 : $app->request->post('productQty'),
		"productCost" => $app->request->post('productCost'),
		"productWidth" => $app->request->post('productWidth'),
		"productWidthUnit" => $app->request->post('productWidthUnit'),
		"productHeight" => $app->request->post('productHeight'),
		"productHeightUnit" => $app->request->post('productHeightUnit'),
		"productLength" => $app->request->post('productLength'),
		"productLengthUnit" => $app->request->post('productLengthUnit'),
		"productCategory" => $app->request->post('productCategory'),
		"distance" => $app->request->post('distance'),
		"productWeight" => $app->request->post('productWeight'),
		"productWeightUnit" => $app->request->post('productWeightUnit'),
		"travelMode" => $app->request->post('travelMode'),
	));
	$requesthelper->calculate();
	$calculationinfo = $requesthelper->get_information();

	if (!isset($calculationinfo->error)) {

		$default_currency = $app->request->post('currency');

		if (in_array(strtoupper(trim($app->request->post('currency'))),
			['USD', ''])) {
			$default_currency = 'GHS';
		}

		$currencydata = $currencycoll->find(array('CurrencyCode' => $default_currency));
		$ghanacurrency = 0;
		$FormatedText = '';
		if ($currencydata->count() > 0) {

			$currencydata = $currencydata->getNext();

			$ghanacurrency = (float) @$currencydata['CurrencyRate'];
			$FormatedText = @$currencydata['FormatedText'];
		}
		$response = array(
			'success' => 1,
			'msg' => 'Request calculation',
			'distanceShow' => number_format($calculationinfo->distance, 2) . " Miles",
			'distance' => $calculationinfo->distance,
			'shippingCost' => $calculationinfo->shippingcost,
			'insurance' => number_format($calculationinfo->insurance, 2),
			'weight' => $calculationinfo->weight,
			'showWeight' => number_format($calculationinfo->weight, 2) . ' Lbs ',
			'volume' => $calculationinfo->volume,
			//'showVolume'    => number_format($calculationinfo->volume,2).' Cu. Ft',
			'showVolume' => number_format($calculationinfo->showVolume, 2) . ' Cu. ' . ucfirst($app->request->post('productWidthUnit')),
			'showCurrency' => '$',
			'totalCost' => $calculationinfo->shippingcost + $calculationinfo->insurance,
			'GhanaTotalCost' => number_format((($calculationinfo->shippingcost + $calculationinfo->insurance) * $ghanacurrency), 2),
		);

		$response['GhanaTotalCost'] = str_replace('[AMT]', $response['GhanaTotalCost'], $FormatedText);

	} else {
		$response = array('success' => 0, 'msg' => $calculationinfo->error);
	}
	echoRespnse(200, $response);

});

/*
 * Function Name :  prepare_request
 * Description : prepare Calulation
 * Url : http://192.168.11.101/aq/services/requester.php/prepare_request
 * Method : POST
 * Parameter : distance=1000&productHeight=5&productHeightUnit=inch&productWeightUnit=kg&productLength=6&productLengthUnit=meter&
 * productWidthUnit=meter&productWidth=5&productCost=50&userName=Ravi%20shukla&productTitle=productTitle&pickupAddress=bhawarkua&pickupAddress2=bhawarkua2
 * &pickupCity=indore&pickupState=Madhya%20Pradesh&pickupCountry=India&pickupPinCode=452001&pickuplat=22.7000&deliveryAddress2=bhopal&deliveryCity=bhopal&
 * deliveryState=Madhya%20Pradesh&deliveryCountry=Indore&deliveryPincode=452001&deliverylong=77.4167&deliverylat=23.2500&pickUpDate=10-05-2015&
 * deliveryDate=15-05-2015&returnAddress=bhawar%20kua&returnAddress2=bhawar%20kua&returnCity=indore&returnState=Madhya%20pradesh&returnCountry=india&
 * returnPincode=452001&flexibleDeliveryDate=0&journyType=one_side&productWeight=5&productWeightUnitId=kg&productHeightUnitId=inch&
 * productLengthUnitId=meter&productWidthUnitId=meter&boxQuantity=1&category=electronic&description=5%20km&insuranceStatus=0&packageMaterial=0&
 * insurance=10&shippingCost=10&totalCost=20&pickUpTime=00%3A00%3A00&deliveryTime=00%3A00%3A00&pickuplong=00&deliveryAddress=bhawar%20kua
 * Library : Sequence
 * Created By : Ravi shukla
 * Create Date : 24-08-2015
 *
 * */

$app->post('/upload_image_for_request', 'requester_auth', function () use ($app) {
	$response = array('success' => 0, 'msg' => 'Opps! Something went wrong.');

	if (strlen($app->request->post('packageId')) == 24) {
		global $db;
		global $userId;

		$collection = $db->delivery_request;

		$requestinfo = $collection->find(array('_id' => new MongoId($app->request->post('packageId'))), array('OtherImage', 'ProductImage'));

		if ($requestinfo->count() > 0) {
			$requestinfo = $requestinfo->getnext();
			$insertData = array();
			$insertData['OtherImage'] = $requestinfo['OtherImage'];

			// Default image
			if (isset($_FILES['ProductImage1']['name']) && $_FILES['ProductImage1']['name'] != '') {
				$exts = explode('.', $_FILES['ProductImage1']['name']);
				$ext = $exts[count($exts) - 1];
				$filename = "productimage" . rand(1000, 9999) . time() . ".$ext";
				$newfile = FILE_URL . "package/$filename";
				if (move_uploaded_file($_FILES['ProductImage1']['tmp_name'], $newfile)) {
					$insertData['ProductImage'] = "package/$filename";
					if ($requestinfo['ProductImage'] != '') {
						if (file_exists(FILE_URL . $requestinfo['ProductImage'])) {
							unlink(FILE_URL . $requestinfo['ProductImage']);
						}
					}
				}
			}
			$imageArray = array('ProductImage2', 'ProductImage3', 'ProductImage4', 'ProductImage5', 'ProductImage6');
			$arrayofimage = array();
			// Other Images
			foreach ($imageArray as $key => $val) {
				if (isset($_FILES[$val]['name']) && $_FILES[$val]['name'] != '') {
					$exts = explode('.', $_FILES[$val]['name']);
					$ext = $exts[count($exts) - 1];
					$filename = "productimage" . rand(1000, 9999) . time() . "$key.$ext";
					$newfile = FILE_URL . "package/$filename";
					if (move_uploaded_file($_FILES[$val]['tmp_name'], $newfile)) {
						$insertData['OtherImage'][] = "package/$filename";
					}
				}
			}
			///Remove old image
			$oldimage = explode(',', @$app->request->post('oldImage'));
			if (is_array($oldimage)) {
				foreach ($oldimage as $key) {
					if (!empty($key)) {
						$oldfile = FILE_URL . $key;
						if (file_exists($oldfile)) {
							unlink($oldfile);
						}
						$insertData['OtherImage'] = array_values(array_diff($insertData['OtherImage'], array($key)));
					}
				}
			}
			if (count($insertData) > 0) {
				//$insertData['sfdsdf'] = $app->request->post('oldImage');
				$collection->update(
					array('_id' => new MongoId($app->request->post('packageId'))),
					array('$set' => $insertData)
				);

				$response = array('success' => 1, 'msg' => 'Package image has been uploaded successfully.');

			}
		}
	}
	echo echoRespnse(200, $response);
});

/*
 * Function Name :  prepare_request
 * Description : prepare Calulation
 * Url : http://192.168.11.101/aq/services/requester.php/prepare_request
 * Method : POST
 * Parameter : distance=1000&productHeight=5&productHeightUnit=inch&productWeightUnit=kg&productLength=6&productLengthUnit=meter&
 * productWidthUnit=meter&productWidth=5&productCost=50&userName=Ravi%20shukla&productTitle=productTitle&pickupAddress=bhawarkua&pickupAddress2=bhawarkua2
 * &pickupCity=indore&pickupState=Madhya%20Pradesh&pickupCountry=India&pickupPinCode=452001&pickuplat=22.7000&deliveryAddress2=bhopal&deliveryCity=bhopal&
 * deliveryState=Madhya%20Pradesh&deliveryCountry=Indore&deliveryPincode=452001&deliverylong=77.4167&deliverylat=23.2500&pickUpDate=10-05-2015&
 * deliveryDate=15-05-2015&returnAddress=bhawar%20kua&returnAddress2=bhawar%20kua&returnCity=indore&returnState=Madhya%20pradesh&returnCountry=india&
 * returnPincode=452001&flexibleDeliveryDate=0&journyType=one_side&productWeight=5&productWeightUnitId=kg&productHeightUnitId=inch&
 * productLengthUnitId=meter&productWidthUnitId=meter&boxQuantity=1&category=electronic&description=5%20km&insuranceStatus=0&packageMaterial=0&
 * insurance=10&shippingCost=10&totalCost=20&pickUpTime=00%3A00%3A00&deliveryTime=00%3A00%3A00&pickuplong=00&deliveryAddress=bhawar%20kua
 * Library : Sequence
 * Created By : Ravi shukla
 * Create Date : 24-08-2015
 *
 * */

$app->post('/prepare_request', 'requester_auth', function () use ($app) {
	verifyRequiredParams(array('pickUpDate', 'deliveryDate', 'pickUpTime', 'deliveryTime', 'productTitle', 'pickupAddress', 'pickupCity', 'pickupCountry', 'pickuplat', 'pickuplong', 'deliveryAddress', 'deliveryCity', 'deliveryCountry', 'deliverylat', 'deliverylong', 'inCaseNotDelReturnAddress', 'inCaseNotDelReturnCity', 'inCaseNotDelReturnCountry', 'productCost', 'productWeight', 'productWeightUnitId', 'productWeightUnit', 'productHeight', 'productHeightUnit', 'productHeightUnitId', 'productLength', 'productLengthUnit', 'productLengthUnitId', 'productWidth', 'productWidthUnit', 'productWidthUnitId', 'distance', 'description', 'insurance', 'shippingCost', 'totalCost'));

	global $db;
	global $userId;
	global $userInformation;

	require 'Slim/library/Sequence.php';

	$collection = $db->delivery_request;
	$settingColl = $db->system_setting;
	$usercoll = $db->users;
	$response = array('success' => 0, 'msg' => 'Opps! Something went wrong.');

	$ProductImage = $pickUpDate = $deliveryDate = '';
	if ($app->request->post('pickUpDate') != '') {
		//$pickUpDate = new MongoDate(strtotime($app->request->post('pickUpDate')." ".$app->request->post('pickUpTime')));
		$pickUpDate = new MongoDate(strtotime($app->request->post('pickUpDate')));
	}
	if ($app->request->post('deliveryDate') != '') {
		$deliveryDate = new MongoDate(strtotime($app->request->post('deliveryDate')));
	}
	$requestStatus = 'pending';
	$tripDetail = '';
	$tripid = $transporterName = $transporterId = '';

	///  Trip section start
	if ($app->request->post('transporterId') != '' && $app->request->post('tripId') != '') {
		if (strlen($app->request->post('tripId')) == 24 && strlen($app->request->post('transporterId')) == 24) {

			$tripColl = $db->trips;
			$tripid = new MongoId($app->request->post('tripId'));
			$transporterId = new MongoId($app->request->post('transporterId'));
			$tripObj = $tripColl->find(array('_id' => $tripid, 'TransporterId' => $transporterId));

			if ($tripObj->count() > 0) {
				$tripObj = $tripObj->getNext();
				if ($tripObj['TripType'] == 'individual') {
					if ($tripObj['SourceDate'] < new MongoDate()) {
						echoRespnse(200, array('success' => 0, 'msg' => 'Trip has been started. You are not able to create request.'));
						$app->stop();
					}
				}
				$requestStatus = 'pending_trip';
				$transporterName = @$tripObj['TransporterName'];
				$transporterinfo = $usercoll->find(array('_id' => $transporterId), array('Name', 'Email', 'TPSetting', 'EmailStatus', 'NoficationStatus'));
				if ($transporterinfo->count() > 0) {
					$transporterinfo = (Object) $transporterinfo->getNext();
				} else {
					$transporterinfo = '';
				}

			} else {
				echoRespnse(200, array('success' => 0, 'msg' => 'Invalid trip or transporter.'));
				$app->stop();
			}

		} else {
			echoRespnse(200, array('success' => 0, 'msg' => 'Invalid trip or transporter.'));
			$app->stop();
		}
	}

	$PackageId = getSequence('Request');
	$verificationCode = rand(1000, 9999);
	$insertData = array(
		"RequesterId" => $userId,
		"RequesterName" => $userInformation->Name,
		"TransporterName" => $transporterName,
		"TransporterId" => $transporterId,
		"PackageId" => $PackageId,
		//Added 8-11-2017
		"device_version" => $app->request->post('device_version'),
		"app_version" => $app->request->post('app_version'),
		"device_type" => $app->request->post('device_type'),
		//end
		"TripId" => $tripid,
		"PackageNumber" => $PackageId . time(),
		"ProductTitle" => ucfirst($app->request->post('productTitle')),
		"PickupFullAddress" => get_formatted_address(array($app->request->post('pickupAddress'),
			$app->request->post('pickupAddress2'),
			$app->request->post('pickupCity'),
			$app->request->post('pickupState'),
			$app->request->post('pickupCountry'),
		), $app->request->post('pickupPinCode')
		),
		"PickupAddress" => $app->request->post('pickupAddress'),
		"PickupAddress2" => $app->request->post('pickupAddress2'),
		"PickupCity" => $app->request->post('pickupCity'),
		"PickupState" => $app->request->post('pickupState'),
		"PickupCountry" => $app->request->post('pickupCountry'),
		"PickupPinCode" => $app->request->post('pickupPinCode'),
		"PickupLatLong" => array(floatval($app->request->post('pickuplong')), floatval($app->request->post('pickuplat'))),
		"PickupDate" => $pickUpDate,
		"DeliveryFullAddress" => get_formatted_address(array($app->request->post('deliveryAddress'),
			$app->request->post('deliveryAddress2'),
			$app->request->post('deliveryCity'),
			$app->request->post('deliveryState'),
			$app->request->post('deliveryCountry'),
		), $app->request->post('deliveryPincode')
		),
		"DeliveryAddress" => $app->request->post('deliveryAddress'),
		"DeliveryAddress2" => $app->request->post('deliveryAddress2'),
		"DeliveryCity" => $app->request->post('deliveryCity'),
		"DeliveryState" => $app->request->post('deliveryState'),
		"DeliveryCountry" => $app->request->post('deliveryCountry'),
		"DeliveryPincode" => $app->request->post('deliveryPincode'),
		"DeliveryLatLong" => array(floatval($app->request->post('deliverylong')), floatval($app->request->post('deliverylat'))),
		"DeliveryDate" => $deliveryDate,
		"ReturnFullAddress" => get_formatted_address(array($app->request->post('returnAddress'),
			$app->request->post('returnAddress2'),
			$app->request->post('returnCity'),
			$app->request->post('returnState'),
			$app->request->post('returnCountry'),
		), $app->request->post('returnPincode')
		),
		"ReturnAddress" => $app->request->post('returnAddress'),
		"ReturnAddress2" => $app->request->post('returnAddress2'),
		"ReturnCityTitle" => $app->request->post('returnCity'),
		"ReturnStateTitle" => $app->request->post('returnState'),
		"ReturnCountry" => $app->request->post('returnCountry'),
		"ReturnPincode" => $app->request->post('returnPincode'),
		"FlexibleDeliveryDate" => strtolower($app->request->post('flexibleDeliveryDate')),
		"JournyType" => ($app->request->post('journyType') == 'return') ? 'return' : 'one_way',
		"NotDelReturnFullAddress" => get_formatted_address(array($app->request->post('inCaseNotDelReturnAddress'),
			$app->request->post('inCaseNotDelReturnAddress2'),
			$app->request->post('inCaseNotDelReturnCity'),
			$app->request->post('inCaseNotDelReturnState'),
			$app->request->post('inCaseNotDelReturnCountry'),
		), $app->request->post('inCaseNotDelReturnPincode')
		),
		"InCaseNotDelReturnAddress" => $app->request->post('inCaseNotDelReturnAddress'),
		"InCaseNotDelReturnAddress2" => $app->request->post('inCaseNotDelReturnAddress2'),
		"InCaseNotDelReturnCity" => $app->request->post('inCaseNotDelReturnCity'),
		"InCaseNotDelReturnState" => $app->request->post('inCaseNotDelReturnState'),
		"InCaseNotDelReturnCountry" => $app->request->post('inCaseNotDelReturnCountry'),
		"InCaseNotDelReturnPincode" => $app->request->post('inCaseNotDelReturnPincode'),
		"ProductCost" => floatval($app->request->post('productCost')),
		"ProductWeight" => $app->request->post('productWeight'),
		"ProductWeightUnit" => $app->request->post('productWeightUnit'),
		"ProductHeight" => $app->request->post('productHeight'),
		"ProductHeightUnit" => $app->request->post('productHeightUnit'),
		"ProductLength" => $app->request->post('productLength'),
		"ProductLengthUnit" => $app->request->post('productLengthUnit'),
		"ProductWidth" => $app->request->post('productWidth'),
		"ProductWidthUnit" => $app->request->post('productWidthUnit'),
		"ProductImage" => "",
		"OtherImage" => array(),
		"ReceiverCountrycode" => $app->request->post("countryCode"),
		"ReceiverMobileNo" => $app->request->post('receiverMobileNo'),
		"QuantityStatus" => strtolower($app->request->post('quantityStatus')),
		"BoxQuantity" => (((int) $app->request->post('boxQuantity') < 2) ? 1 : (int) $app->request->post('boxQuantity')),
		"Category" => $app->request->post('category'),
		"Distance" => (float) $app->request->post('distance'),
		"Description" => $app->request->post('description'),
		"Status" => 'pending', //$requestStatus,
		"InsuranceStatus" => ((strtolower($app->request->post('insuranceStatus')) == 'yes') ? 'yes' : 'no'),
		"PackageMaterial" => ((strtolower($app->request->post('packageMaterial')) == 'yes') ? 'yes' : 'no'),
		"PackageMaterialShipped" => "",
		"StripeChargeId" => '',
		"PaymentDate" => "",
		"PaymentStatus" => 'pending',
		"InsuranceCost" => floatval($app->request->post('insurance')),
		"ShippingCost" => floatval($app->request->post('shippingCost')),
		"TotalCost" => floatval($app->request->post('totalCost')) - floatval($app->request->post('discount')),
		'AquantuoFees' => get_aquantuo_fees($app->request->post('totalCost')),
		'discount' => floatval($app->request->post('discount')),
		'promocode' => $app->request->post('promocode'),
		"RejectBy" => "",
		"ReturnType" => "",
		"RejectTime" => "",
		"TrackLocation" => "on",
		"ReceiptImage" => "",
		"TrackingNumber" => "",
		"ReturnDate" => '',
		"CarrierMessage" => "",
		"DeliveredStartTime" => '',
		"DeliveredTime" => '',
		"PackageCareNote" => ucfirst($app->request->post('packagecareNote')),
		"DeliveryVerifyCode" => $verificationCode,
		"TransporterFeedbcak" => "",
		"TransporterRating" => "",
		"RequesterFeedbcak" => "",
		"RequesterRating" => "",
		"TPLatLong" => array(0, 0),
		'PublicPlace' => (strtolower($app->request->post('publicPlace')) == 'yes') ? 'yes' : 'no',
		"TravelMode" => $app->request->post('travelMode'),
		'RequestType' => 'delivery',
		"EnterOn" => new MongoDate(),
		"UpdateOn" => new MongoDate(),
	);
	if ($insertData['TotalCost'] < 0.50) {
		$insertData['Status'] = 'ready';
	}

	// Default image
	if (isset($_FILES['ProductImage1']['name']) && $_FILES['ProductImage1']['name'] != '') {
		$exts = explode('.', $_FILES['ProductImage1']['name']);
		$ext = $exts[count($exts) - 1];
		$filename = "productimage" . rand(1000, 9999) . time() . ".$ext";
		$newfile = FILE_URL . "package/$filename";
		if (move_uploaded_file($_FILES['ProductImage1']['tmp_name'], $newfile)) {
			$insertData['ProductImage'] = "package/$filename";
		}
	}
	//OtherImage
	$imageArray = array('ProductImage2', 'ProductImage3', 'ProductImage4', 'ProductImage5', 'ProductImage6');
	$arrayofimage = array();
	foreach ($imageArray as $key => $val) {
		if (isset($_FILES[$val]['name']) && $_FILES[$val]['name'] != '') {
			$exts = explode('.', $_FILES[$val]['name']);
			$ext = $exts[count($exts) - 1];
			$filename = "productimage" . rand(1000, 9999) . time() . "$key.$ext";
			$newfile = FILE_URL . "package/$filename";
			if (move_uploaded_file($_FILES[$val]['tmp_name'], $newfile)) {
				$insertData['OtherImage'][] = "package/$filename";
			}
		}
	}

	try {
		$res = $collection->insert($insertData);
		if ($res['ok'] == 1) {
			mark_promocde_as_use($app->request->post('promocode'));

			if ($insertData['Status'] == 'ready') {
				$response = array('success' => 1, 'msg' => 'Your request has been created successfully.', "packageId" => (string) $insertData['_id']);

				if ($insertData['TotalCost'] < 0.50) {

					require_once 'Slim/library/Requestnotification.php';
					try {
						if (empty(trim($insertData['TripId']))) {
							carriernotication($app->request->post('packageId'));
						} else {
							notify_on_trip($insertData);
						}
					} catch (exception $e) {}
				}

			} else {

				$response = array(
					'success' => 1,
					'msg' => 'Your request has been created. Please proceed to payment method.',
					"packageId" => (string) $insertData['_id'],
					"Status" => $insertData['Status'],

				);
			}
			$response['Status'] = $insertData['Status'];
		}
	} catch (Exception $e) {
		$response['error'] = $e->getMessage();
	}
	echo echoRespnse(200, $response);
});

function get_aquantuo_fees($totalcost) {
	global $db;
	$fees = 0;
	$configuration_coll = $db->configuration;
	$configurationdata = $configuration_coll->find(array('_id' => new MongoId('5673e33e6734c4f874685c84')));
	if ($configurationdata->count() > 0) {
		$configurationdata = $configurationdata->getNext();
		$fees = ((floatval($totalcost) * $configurationdata['aquantuoFees']) / 100);
	}
	return $fees;
}

/*
 * Function Name :  edit_prepare_request
 * Description : prepare Calulation
 * Url : http://192.168.11.101/aq/services/requester.php/edit_prepare_request
 * Method : POST
 * Parameter : requestid=123456789&distance=1000&productHeight=5&productHeightUnit=inch&productWeightUnit=kg&productLength=6&productLengthUnit=meter&
 * productWidthUnit=meter&productWidth=5&productCost=50&userName=Ravi%20shukla&productTitle=productTitle&pickupAddress=bhawarkua&pickupAddress2=bhawarkua2
 * &pickupCity=indore&pickupState=Madhya%20Pradesh&pickupCountry=India&pickupPinCode=452001&pickuplat=22.7000&deliveryAddress2=bhopal&deliveryCity=bhopal&
 * deliveryState=Madhya%20Pradesh&deliveryCountry=Indore&deliveryPincode=452001&deliverylong=77.4167&deliverylat=23.2500&pickUpDate=10-05-2015&
 * deliveryDate=15-05-2015&returnAddress=bhawar%20kua&returnAddress2=bhawar%20kua&returnCity=indore&returnState=Madhya%20pradesh&returnCountry=india&
 * returnPincode=452001&flexibleDeliveryDate=0&journyType=one_side&productWeight=5&productWeightUnitId=kg&productHeightUnitId=inch&
 * productLengthUnitId=meter&productWidthUnitId=meter&boxQuantity=1&category=electronic&description=5%20km&insuranceStatus=0&packageMaterial=0&
 * insurance=10&shippingCost=10&totalCost=20&pickUpTime=00%3A00%3A00&deliveryTime=00%3A00%3A00&pickuplong=00&deliveryAddress=bhawar%20kua
 * Library : Sequence
 * Created By : Ravi shukla
 * Create Date : 24-08-2015
 *
 * */

$app->post('/edit_prepare_request', 'requester_auth', function () use ($app) {
	//verifyRequiredParams(array('requestid','pickUpDate','deliveryDate','userName','productTitle','pickupAddress','pickupCity','pickupState','pickupCountry','pickuplat','pickuplong','deliveryAddress','deliveryCity','deliveryState','deliveryCountry','deliverylat','deliverylong','inCaseNotDelReturnAddress','inCaseNotDelReturnCity','inCaseNotDelReturnState','inCaseNotDelReturnCountry','productCost','productWeight','productWeightUnitId','productWeightUnit','productHeight','productHeightUnit','productHeightUnitId','productLength','productLengthUnit','productLengthUnitId','productWidth','productWidthUnit','productWidthUnitId','distance','description','insurance','shippingCost','totalCost'));

	global $db;
	global $userId;
	global $userType;

	require 'Slim/library/Sequence.php';

	$collection = $db->delivery_request;
	$settingColl = $db->system_setting;
	$response = array('success' => 0, 'msg' => 'Invalid request');
	if (strlen($app->request->post('requestid')) == 24) {
		$requestid = new MongoId($app->request->post('requestid'));
		$requestinfo = $collection->find(array('_id' => $requestid, 'RequesterId' => $userId));
		if ($requestinfo->count() > 0) {
			$requestinfo = $requestinfo->getNext();

			$ProductImage = $pickUpDate = $deliveryDate = '';
			if ($app->request->post('pickUpDate') != '') {
				$pickUpDate = new MongoDate(strtotime($app->request->post('pickUpDate')));
			}
			if ($app->request->post('deliveryDate') != '') {
				$deliveryDate = new MongoDate(strtotime($app->request->post('deliveryDate')));
			}
			$verificationCode = rand(1000, 9999);

			$insertData = array(
				"ProductTitle" => ucfirst($app->request->post('productTitle')),
				"PickupFullAddress" => get_formatted_address(array($app->request->post('pickupAddress'),
					$app->request->post('pickupAddress2'),
					$app->request->post('pickupCity'),
					$app->request->post('pickupState'),
					$app->request->post('pickupCountry'),
				), $app->request->post('pickupPinCode')
				),
				"PickupAddress" => $app->request->post('pickupAddress'),
				//Added 8-11-2017
				"device_version" => $app->request->post('device_version'),
				"app_version" => $app->request->post('app_version'),
				"device_type" => $app->request->post('device_type'),
				//end
				"PickupAddress2" => $app->request->post('pickupAddress2'),
				"PickupCity" => $app->request->post('pickupCity'),
				"PickupState" => $app->request->post('pickupState'),
				"PickupCountry" => $app->request->post('pickupCountry'),
				"PickupPinCode" => $app->request->post('pickupPinCode'),
				"PickupLatLong" => array(floatval($app->request->post('pickuplong')), floatval($app->request->post('pickuplat'))),
				"PickupDate" => $pickUpDate,
				"DeliveryFullAddress" => get_formatted_address(array($app->request->post('deliveryAddress'),
					$app->request->post('deliveryAddress2'),
					$app->request->post('deliveryCity'),
					$app->request->post('deliveryState'),
					$app->request->post('deliveryCountry'),
				), $app->request->post('deliveryPincode')
				),
				"DeliveryAddress" => $app->request->post('deliveryAddress'),
				"DeliveryAddress2" => $app->request->post('deliveryAddress2'),
				"DeliveryCity" => $app->request->post('deliveryCity'),
				"DeliveryState" => $app->request->post('deliveryState'),
				"DeliveryCountry" => $app->request->post('deliveryCountry'),
				"DeliveryPincode" => $app->request->post('deliveryPincode'),
				"DeliveryLatLong" => array(floatval($app->request->post('deliverylong')), floatval($app->request->post('deliverylat'))),
				"DeliveryDate" => $deliveryDate,
				"ReturnFullAddress" => get_formatted_address(array($app->request->post('returnAddress'),
					$app->request->post('returnAddress2'),
					$app->request->post('returnCity'),
					$app->request->post('returnState'),
					$app->request->post('returnCountry'),
				), $app->request->post('returnPincode')
				),
				"ReturnAddress" => $app->request->post('returnAddress'),
				"ReturnAddress2" => $app->request->post('returnAddress2'),
				"ReturnCityTitle" => $app->request->post('returnCity'),
				"ReturnStateTitle" => $app->request->post('returnState'),
				"ReturnCountry" => $app->request->post('returnCountry'),
				"ReturnPincode" => $app->request->post('returnPincode'),
				"FlexibleDeliveryDate" => strtolower($app->request->post('flexibleDeliveryDate')),
				"JournyType" => ($app->request->post('journyType') == 'return') ? 'return' : 'one_way',
				"NotDelReturnFullAddress" => get_formatted_address(array($app->request->post('inCaseNotDelReturnAddress'),
					$app->request->post('inCaseNotDelReturnAddress2'),
					$app->request->post('inCaseNotDelReturnCity'),
					$app->request->post('inCaseNotDelReturnState'),
					$app->request->post('inCaseNotDelReturnCountry'),
				), $app->request->post('inCaseNotDelReturnPincode')
				),
				"InCaseNotDelReturnAddress" => $app->request->post('inCaseNotDelReturnAddress'),
				"InCaseNotDelReturnAddress2" => $app->request->post('inCaseNotDelReturnAddress2'),
				"InCaseNotDelReturnCity" => $app->request->post('inCaseNotDelReturnCity'),
				"InCaseNotDelReturnState" => $app->request->post('inCaseNotDelReturnState'),
				"InCaseNotDelReturnCountry" => $app->request->post('inCaseNotDelReturnCountry'),
				"InCaseNotDelReturnPincode" => $app->request->post('inCaseNotDelReturnPincode'),
				"ProductCost" => floatval($app->request->post('productCost')),
				"ProductWeight" => $app->request->post('productWeight'),
				"ProductWeightUnit" => $app->request->post('productWeightUnit'),
				"ProductHeight" => $app->request->post('productHeight'),
				"ProductHeightUnit" => $app->request->post('productHeightUnit'),
				"ProductLength" => $app->request->post('productLength'),
				"ProductLengthUnit" => $app->request->post('productLengthUnit'),
				"ProductWidth" => $app->request->post('productWidth'),
				"ProductWidthUnit" => $app->request->post('productWidthUnit'),
				"ReceiverCountrycode" => $app->request->post("countryCode"),
				"ReceiverMobileNo" => $app->request->post('receiverMobileNo'),
				"QuantityStatus" => strtolower($app->request->post('quantityStatus')),
				"BoxQuantity" => (((int) $app->request->post('boxQuantity') < 2) ? 1 : (int) $app->request->post('boxQuantity')),
				"Category" => strtolower($app->request->post('category')),
				"Distance" => (float) $app->request->post('distance'),
				"Description" => $app->request->post('description'),
				"Status" => 'pending',
				"InsuranceStatus" => ((strtolower($app->request->post('insuranceStatus')) == 'yes') ? 'yes' : 'no'),
				"PackageMaterial" => ((strtolower($app->request->post('packageMaterial')) == 'yes') ? 'yes' : 'no'),
				"PackageMaterialShipped" => "",
				"PaymentDate" => "",
				"PaymentStatus" => 'pending',
				"InsuranceCost" => floatval($app->request->post('insurance')),
				"ShippingCost" => floatval($app->request->post('shippingCost')),
				"TotalCost" => floatval($app->request->post('totalCost')) - floatval($app->request->post('discount')),
				'AquantuoFees' => get_aquantuo_fees($app->request->post('totalCost')),
				'discount' => floatval($app->request->post('discount')),
				'promocode' => $app->request->post('promocode'),
				"PackageCareNote" => ucfirst($app->request->post('packagecareNote')),
				"DeliveryVerifyCode" => $verificationCode,
				'PublicPlace' => (strtolower($app->request->post('publicPlace')) == 'yes') ? 'yes' : 'no',
				"TravelMode" => $app->request->post('travelMode'),
				"UpdateOn" => new MongoDate(),
			);

			if ($insertData['TotalCost'] <= 0.50) {
				$insertData['Status'] = 'ready';
			}

			try {
				$requestid = new MongoId($app->request->post('requestid'));
				$res = $collection->update(array('_id' => $requestid), array('$set' => $insertData));
				if ($res['ok'] == 1) {

					if ($insertData['Status'] == 'ready') {
						$response = array('success' => 1,
							'msg' => 'Your request has been updated.',
							"packageId" => $app->request->post('requestid'),
							'Status' => $insertData['Status'],
						);
					} else {

						$response = array('success' => 1, 'msg' => 'Your request has been updated Please proceed to payment method.', "packageId" => $app->request->post('requestid'), 'Status' => $insertData['Status']);
					}
				}
			} catch (Exception $e) {
				$response['error'] = $e->getMessage();
			}
		}
	}
	echo echoRespnse(200, $response);
});

/*
 * Function Name    : my_request
 * Description      : getting list of new_request, current_delivery and history of different Status.(requester).
 * Url              : http://192.168.11.101/aq/services/requester.php/my_request
 * Method           : GET
 * Parameter        : pageno,type
 * New Request      : Status : 'pickup', 'out_for_delivery', 'pending'.
 * Current delivery : Status : 'on_delivery', 'out_for_delivery', 'pending'.
 * History          : Status : 'delivered', 'cancel'.
 * Created By       : Pankaj Gawande.
 * Create Date      : 23-11-2015.
 *
 */

$app->get('/my_request', 'requester_auth', function () use ($app) {
	global $db;
	global $userId;
	$limit = 20;
	$start = (((int) $app->request->get('pageno')) - 1) * $limit;
	$where = array('RequesterId' => $userId);

	$response = array('success' => 0, 'msg' => '', 'result' => array(), 'pagination' => $limit);
	$response['msg'] = 'You have no new requests. You can prepare a new delivery request by tapping on the plus (+) icon at the top right corner.';
	$collection = $db->delivery_request;
	$usercollection = $db->users;

	$where['Status'] = array('$in' => array('pending','momo_initiated'));

	if ($app->request->get('type') == 'current_delivery') {
		$where['Status'] = array('$in' => array('ready', 'accepted', 'paid', 'assign', 'out_for_pickup', 'item_received', 'out_for_delivery', 'purchased', 'reviewed', 'not_purchased','shipment_departed'));
		$response['msg'] = 'No active deliveries found.';
	} else if ($app->request->get('type') == 'history') {
		$where['Status'] = array('$in' => array('delivered', 'cancel', 'cancel_by_admin'));
		$response['msg'] = 'No completed/canceled deliveries found.';
	}

	$field = array('_id', 'ProductTitle', 'ProductImage', 'TransporterName', 'Status', 'UpdateOn','EnterOn', 'DeliveryLatLong', 'PickupLatLong',
		'PickupFullAddress', 'DeliveryFullAddress', 'ReturnFullAddress', 'TransporterId', 'TripId', 'RequestType', 'ProductList', 'RequestType', 'request_version');
	$requestData = $collection->find($where, $field)->sort(array('_id' => -1))->skip($start)->limit($limit);

	if ($requestData->count() > 0) {
		// get transporter data by

		$transporterids = array();
		$transporterdata = array();
		foreach ($requestData as $val) {
			$transporterids[] = @$val['TransporterId'];
		}

		$transporterinfo = $usercollection->find(array('_id' => array('$in' => $transporterids)), array('ChatName', 'Name', 'Image'));

		if ($transporterinfo->count() > 0) {
			foreach ($transporterinfo as $key) {
				$transporterdata[(string) $key['_id']] = $key;
			}
		}

		// End get transporter data
		foreach ($requestData as $val) {
			$response['success'] = 1;
			$response['msg'] = 'Records found';
			$description = '';
			if (in_array($val['Status'], array('pending_trip', 'ready_trip'))) {
				$description = "Requested to transporter: {$val['TransporterName']}";
			}
			$ChatName = '';
			$ChatUserName = '';
			$ChatUsrImage = '';
			if (isset($transporterdata[(String) @$val['TransporterId']])) {
				$ChatName = $transporterdata[(String) $val['TransporterId']]['ChatName'];
				$ChatUserName = $transporterdata[(String) $val['TransporterId']]['Name'];
				$ChatUsrImage = $transporterdata[(String) $val['TransporterId']]['Image'];
			}
			$complete = $process = $pending = 0;

			if (isset($val['RequestType'])) {
				if (in_array($val['RequestType'], ['online', 'buy_for_me'])) {
					if (isset($val['ProductList'][0]['image'])) {
						$val['ProductImage'] = $val['ProductList'][0]['image'];
					}

					foreach ($val['ProductList'] as $plk => $pkv) {
						if (in_array(@$pkv['status'], ['completed', 'canceled', 'cancel_by_admin'])) {
							$complete++;
						} else if (in_array(@$pkv['status'], ['accepted', 'assigned', 'out_for_pickup', 'out_for_delivery', 'purchased', 'shipment_departed'])) {
							$process++;
						} else {
							$pending++;
						}
					}
				}

				if (in_array($val['RequestType'], ['local_delivery'])) {
					$val['ProductImage'] = $val['ProductList'][0]['ProductImage'];
				}

				if (in_array($val['RequestType'], ['delivery']) && isset($val['ProductList'])) {
					$val['ProductImage'] = $val['ProductList'][0]['ProductImage'];
				}
			} else {
				$val['RequestType'] = 'delivery';
			}

			$response['result'][] = array(
				"id" => (string) $val['_id'],
				"TripId" => (@$val['TripId'] != '') ? (String) @$val['TripId'] : '',
				'request_version' => @$val['request_version'],
				"ProductTitle" => @$val['ProductTitle'],
				"ProductImage" => @$val['ProductImage'],
				"Status" => $val['Status'],
				"StatusTitle" => get_status_title($val['Status'], $val['RequestType']),
				"Date" => (isset($val['EnterOn']->sec)) ? $val['EnterOn']->sec : '',
				"Description" => $description,
				"TrackLocation" => "",
				"TPCurrentLat" => @$val['TPLatLong'][1],
				"TPCurrentLong" => @$val['TPLatLong'][0],
				"DeliveryLat" => $val['DeliveryLatLong'][1],
				"DeliveryLong" => isset($val['DeliveryLatLong'][0]) ? $val['DeliveryLatLong'][0] : '',
				"PickupLat" => isset($val['PickupLatLong'][1]) ? $val['PickupLatLong'][1] : '',
				"PickupLong" => isset($val['PickupLatLong'][0]) ? $val['PickupLatLong'][0] : '',
				"PickupAddress" => isset($val['PickupFullAddress']) ? $val['PickupFullAddress'] : '',
				"DeliveryAddress" => $val['DeliveryFullAddress'],
				"ReturnAddress" => isset($val['ReturnFullAddress']) ? $val['ReturnFullAddress'] : '',
				"ChatUserId" => (String) @$val['TransporterId'],
				"ChatUserName" => $ChatUserName,
				"ChatUsrImage" => $ChatUsrImage,
				"ChatName" => $ChatName,
				"RequestType" => $val['RequestType'],
				"Pending" => $pending,
				"Process" => $process,
				"Complete" => $complete,
				"ItemStatus" => "(" . ($process + $complete) . "/" . ($process + $complete + $pending) . ")",
				'request_version' => (@$val['request_version'] == null) ? 'old' : $val['request_version'],
			);
		}

	}
	echo json_encode($response);

});

/*
 * Function Name :  cancel_delivery
 * Description : cancel request by sender
 * Url : http://192.168.11.101/aq/services/requester.php/cancel_delivery
 * Header : Apikey, Userype
 * Method : POST
 * Parameter : requestid,type
 * Created By : Ravi shukla
 * Create Date : 27-11-2015
 * */

$app->post('/cancel_delivery', 'requester_auth', function () use ($app) {

	global $db;
	global $userId;
	verifyRequiredParams(array('requestid'));
	$response = array('success' => 0, 'msg' => 'Fail to finish delivery.');

	if (strlen($app->request->post('requestid')) == 24) {
		$collection = $db->delivery_request;
		$PackageId = new MongoId($app->request->post('requestid'));

		if ($app->request->post('type') == "pending") {
			$res = $collection->remove(array('_id' => $PackageId, 'RequesterId' => $userId, 'Status' => 'pending'));

			if ($res['ok'] == 1) {
				$response['success'] = 1;
				$response['msg'] = "Package deleted successfully.";
			} else {
				$response['msg'] = "Fail to deleted package.";
			}
		} else {
			$updData = array("Status" => 'pending');
			$res = $collection->update(array('_id' => $PackageId, 'RequesterId' => $userId, 'Status' => 'ready'), array('$set' => $updData));

			if ($res['ok'] == 1) {
				$response['success'] = 1;
				$response['msg'] = "Package canceled successfully.";
			} else {
				$response['msg'] = "Fail to canceled package.";
			}
		}
	}

	echo echoRespnse(200, $response);
});

/*
 * Function Name     : post_by_transporters.
 * Description         : It shows the list of post by transporters.
 * Url                 : http://192.168.11.101/aq/services/requester.php/post_by_transporters
 * Method             : GET
 * Header           : Apikey
 * Param            : type
 * Created By         : RAvi shukla
 * Create Date         : 02-01-2015
 *
 */

$app->get('/post_by_transporters', 'requester_auth', function () use ($app) {
	global $db;
	global $userId;

	verifyRequiredParams(array('pageno'));

	$response = array('success' => 0, 'msg' => 'No Records Found');
	$tripsCollection = $db->trips;
	$usercollection = $db->users;
	$limit = 30;
	$start = (int) ((((int) $app->request->get('pageno')) - 1) * $limit);

	$searchKey = $app->request->get('searchKey');
	$where = array(
		//'SourceFullAddress' => array('$regex' => new MongoRegex("/" . trim($app->request->get('SearchSourceKey')) . "/i")),
		//'DestFullAddress' => array('$regex' => new MongoRegex("/" . trim($app->request->get('SearchDestinationKey')) . "/i")),
		'TransporterId' => array('$ne' => $userId),
		'TripType' => $app->request->get('type'),
		'Status' => 'active',
		"SourceDate" => array('$gt' => new MongoDate()),
	);

	if($app->request->get('SearchSourceKey') != ''){
		$where['SourceFullAddress'] = array('$regex' => new MongoRegex("/" . trim($app->request->get('SearchSourceKey')) . "/i"));
	}

	if($app->request->get('SearchDestinationKey') != ''){
		$where['DestFullAddress'] = array('$regex' => new MongoRegex("/" . trim($app->request->get('SearchDestinationKey')) . "/i"));
	}



	if (strlen($app->request->get('TransporterId')) == 24) {
		$where['TransporterId'] = new MongoId($app->request->get('TransporterId'));
	}
	if ($app->request->get('type') == 'both') {
		unset($where['TripType']);
	}

	$tripsDetails = $tripsCollection->aggregate(
		array('$match' => $where),
		array('$sort' => array('SourceDate' => -1)),
		array('$limit' => $limit),
		array('$skip' => $start),
		array('$group' => array(
			'_id' => array(
				'year' => array('$year' => '$SourceDate'),
				'month' => array('$month' => '$SourceDate'),
				'day' => array('$dayOfMonth' => '$SourceDate'),
			),
			'tripsDetails' => array('$push' => array(
				'TripId' => '$_id',
				'TransporterId' => '$TransporterId',
				'SourceCountry' => '$SourceCountry',
				'SourceState' => '$SourceState',
				'SourceCity' => '$SourceCity',
				'DestiCountry' => '$DestiCountry',
				'DestiState' => '$DestiState',
				'DestiCity' => '$DestiCity',
				'EnterOn' => '$SourceDate',
				'DestiDate' => '$DestiDate',
				'Category' => '$SelectCategory',
				'Weight' => '$Weight',
				'Unit' => '$Unit',
				'Description' => '$Description',
				'ChargeRangeMin' => '$ChargeRangeMin',
				'ChargeRangeMax' => '$ChargeRangeMax',
				'TransporterName' => '$TransporterName',
				'TransporterImage' => '$TransporterId',
			),
			),
		),
		)
	);

	if (isset($tripsDetails['result']) && count($tripsDetails['result']) > 0) {
		$response = array('success' => 1, 'msg' => 'Records found', 'result' => array());
		$transporterid = array();

		foreach ($tripsDetails['result'] as $key) {

			foreach ($key['tripsDetails'] as $k => $v) {
				$transporterid[] = $v['TransporterId'];
			}
		}

		$transporterinfo = array();
		$transporter = $usercollection->find(array('_id' => array('$in' => $transporterid)), array('Image', 'RatingCount', 'RatingByCount', 'FirstName', 'Name'));
		if ($transporter->count() > 0) {
			foreach ($transporter as $key) {
				$transporterinfo[(String) $key['_id']] = $key;
			}
		}
		foreach ($tripsDetails['result'] as $key) {
			$tripbatch = array();
			foreach ($key['tripsDetails'] as $k => $v) {
				$tpimage = '';
				$rating = 0;

				if (isset($transporterinfo[(String) $v['TransporterId']])) {
					$tpimage = $transporterinfo[(String) $v['TransporterId']]['Image'];
					@$v['TransporterName'] = $transporterinfo[(String) $v['TransporterId']]['FirstName'];

					if ((int) @$transporterinfo[(String) $v['TransporterId']]['RatingByCount'] > 0 && (int) (@$transporterinfo[(String) $v['TransporterId']]['RatingCount']) > 0) {
						$rating = (int) (@$transporterinfo[(String) $v['TransporterId']]['RatingCount']) / (int) (@$transporterinfo[(String) $v['TransporterId']]['RatingByCount']);
					}
				}

				$tripbatch[] = array(
					'TripId' => (String) $v['TripId'],
					'SourceCountry' => @$v['SourceCountry'],
					'SourceState' => @$v['SourceState'],
					'SourceCity' => @$v['SourceCity'],
					'DestiCountry' => @$v['DestiCountry'],
					'DestiState' => @$v['DestiState'],
					'DestiCity' => @$v['DestiCity'],
					'tripDate' => @$v['EnterOn']->sec,
					'DropoffDate' => @$v['DestiDate']->sec,
					'Category' => @$v['Category'],
					'Weight' => @$v['Weight'],
					'Unit' => @$v['Unit'],
					'Description' => @$v['Description'],
					'ChargeRangeMin' => @$v['ChargeRangeMin'],
					'ChargeRangeMax' => @$v['ChargeRangeMax'],
					'TransporterName' => @$v['TransporterName'],
					'TransporterImage' => $tpimage,
					'transporterId' => (String) @$v['TransporterId'],
					'TransporterId' => (String) @$v['TransporterId'],
					'TransporterRating' => $rating,
				);
			}
			if (count($tripbatch) > 0) {
				$response['result'][] = array(
					"Date" => $v['EnterOn']->sec,
					"tripsDetails" => $tripbatch,
				);
			}

		}
	}
	echoRespnse(200, $response);
});

/*
 * Function Name     : post_by_individula_transporters.
 * Description         : It shows the trip list post by business transporters.
 * Url                 : http://192.168.11.101/aq/services/requester.php/post_by_individula_transporters
 * Method             : GET
 * Header           : Apikey
 * Param            : type
 * Created By         : Ravi shukla
 * Create Date         : 08-01-2016
 *
 */

$app->get('/individula_trip_detail', 'requester_auth', function () use ($app) {

	global $db;
	global $userId;
	$response = array('success' => 0, 'msg' => 'Trip not found');
	$tripsCollection = $db->trips;
	$usercollection = $db->users;
	$requestCollection = $db->delivery_request;

	if (strlen($app->request->get('tripid')) == 24) {
		$tripinfo = $tripsCollection->find(array('_id' => new MongoId($app->request->get('tripid')), 'Status' => 'active'));
		if ($tripinfo->count() > 0) {
			$tripinfo = $tripinfo->getNext();

			$response = array('success' => 1, 'msg' => 'Trip detail');
			$response['result'] = array(
				'TripId' => (String) $tripinfo['_id'],
				'SourceCountry' => @$tripinfo['SourceCountry'],
				'SourceState' => @$tripinfo['SourceState'],
				'SourceCity' => @$tripinfo['SourceCity'],
				'DestiCountry' => @$tripinfo['DestiCountry'],
				'DestiState' => @$tripinfo['DestiState'],
				'DestiCity' => @$tripinfo['DestiCity'],
				'tripDate' => @$tripinfo['SourceDate']->sec,
				'DropoffDate' => @$tripinfo['DestiDate']->sec,
				'Category' => @$tripinfo['SelectCategory'],
				'Weight' => @$tripinfo['Weight'],
				'Unit' => @$tripinfo['Unit'],
				'Description' => @$tripinfo['Description'],
				'ChargeRangeMin' => @$tripinfo['ChargeRangeMin'],
				'ChargeRangeMax' => @$tripinfo['ChargeRangeMax'],
				'TransporterId' => (String) @$tripinfo['TransporterId'],
				'TransporterName' => @$tripinfo['TransporterName'],
				'TransporterImage' => '',
				'TransporterRating' => 0,
				'TravelMode' => @$tripinfo['TravelMode'],
			);

			// Transporter information
			$userinfo = $usercollection->find(array('_id' => $tripinfo['TransporterId']), array('Image', 'RatingCount', 'RatingByCount', 'FirstName', 'Name'));
			if ($userinfo->count() > 0) {
				$userinfo = $userinfo->getNext();
				$response['result']["TransporterImage"] = $userinfo['Image'];
				if (@$userinfo['RatingCount'] > 0 && @$userinfo['RatingByCount'] > 0) {
					$response['result']["TransporterRating"] = (@$userinfo['RatingCount'] / @$userinfo['RatingByCount']);
				}

				// Check requester has created any request for this transporter
				$requestinfo = $requestCollection->find(array('RequesterId' => $userId, 'TransporterId' => $tripinfo['TransporterId']), array('_id'));

				if ($requestinfo->count() > 0) {
					$response['result']['TransporterName'] = $userinfo['Name'];
				} else {
					$response['result']['TransporterName'] = $userinfo['FirstName'];
				}
			}
		}
	}

	echoRespnse(200, $response);

});

/*
 * Function Name     : post_by_business_transporters.
 * Description         : It shows the trip list post by business transporters.
 * Url                 : http://192.168.11.101/aq/services/requester.php/post_by_business_transporters
 * Method             : GET
 * Header           : Apikey
 * Param            : type
 * Created By         : Ravi shukla
 * Create Date         : 08-01-2016
 *
 */

$app->get('/post_by_business_transporters', 'requester_auth', function () use ($app) {
	global $db;
	global $userId;
	$response = array('success' => 0, 'msg' => 'No Records Found');
	$tripsCollection = $db->trips;

	$limit = 30;
	$start = (int) ((((int) $app->request->get('pageno')) - 1) * $limit);

	$where = array(
		'Countries' => array('$in' => array(new MongoRegex("/" . $app->request->get('SearchSourceKey') . "/i"))),
		'TransporterId' => array('$ne' => $userId),
		'TripType' => 'business',
		'Status' => 'active',
	);
	if (strlen($app->request->get('TransporterId')) == 24) {
		$where['TransporterId'] = new MongoId($app->request->get('TransporterId'));
	}
	$tripsDetails = $tripsCollection->aggregate(
		array('$match' => $where),
		array('$sort' => array('UpdateDate' => 1)),
		array('$limit' => $limit),
		array('$skip' => $start),
		array('$group' => array(
			'_id' => array(
				'year' => array('$year' => '$UpdateDate'),
				'month' => array('$month' => '$UpdateDate'),
				'day' => array('$dayOfMonth' => '$UpdateDate'),
			),
			'tripsDetails' => array('$push' => array(
				'TripId' => '$_id',
				'TransporterId' => '$TransporterId',
				'SourceCountries' => '$Countries',
				'Category' => '$Categories',
				'Weight' => '$Weight',
				'Unit' => '$Unit',
				'EnterOn' => '$UpdateDate',
			),
			),
		),
		)
	);
	if (isset($tripsDetails['result']) && count($tripsDetails['result']) > 0) {
		$response = array('success' => 1, 'msg' => 'Records found', 'result' => array());
		foreach ($tripsDetails['result'] as $key) {
			$tripbatch = array();
			foreach ($key['tripsDetails'] as $k => $v) {
				$tripbatch[] = array(
					'TripId' => (String) $v['TripId'],
					'TransporterId' => (String) @$v['TransporterId'],
					'SourceCountries' => @$v['SourceCountries'],
					'Category' => @$v['Category'],
					'Weight' => @$v['Weight'],
					'Unit' => @$v['Unit'],
				);
			}
			if (count($tripbatch) > 0) {
				$response['result'][] = array(
					"Date" => $v['EnterOn']->sec,
					"tripsDetails" => $tripbatch,
				);
			}

		}
	}
	echoRespnse(200, $response);
});

/*
 * Function Name     : post_by_business_transporters.
 * Description         : It shows the trip list post by business transporters.
 * Url                 : http://192.168.11.101/aq/services/requester.php/trip_detail_for_business
 * Method             : GET
 * Header           : Apikey
 * Param            : type
 * Created By         : Ravi shukla
 * Create Date         : 08-01-2016
 *
 */

$app->get('/trip_detail_for_business', 'requester_auth', function () use ($app) {
	global $db;
	global $userId;
	$response = array('success' => 0, 'msg' => 'Trip not found');
	$tripsCollection = $db->trips;
	$usercollection = $db->users;
	$requestCollection = $db->delivery_request;

	if (strlen($app->request->get('tripid')) == 24) {
		$tripinfo = $tripsCollection->find(array('_id' => new MongoId($app->request->get('tripid')), "TripType" => "business", 'Status' => 'active'));
		if ($tripinfo->count() > 0) {
			$tripinfo = $tripinfo->getNext();

			$response = array('success' => 1, 'msg' => 'Trip detail');
			$response['result'] = array(
				"TripId" => (String) $tripinfo['_id'],
				"TransporterId" => (String) $tripinfo['TransporterId'],
				"TransporterName" => $tripinfo['TransporterName'],
				"TransporterImage" => '',
				"TransporterRating" => 0,
				"Countries" => $tripinfo['Countries'],
				"Category" => $tripinfo['Categories'],
				"Weight" => $tripinfo['Weight'],
				"Unit" => $tripinfo['Unit'],
				"Description" => $tripinfo['Description'],
			);

			// Transporter information
			$userinfo = $usercollection->find(array('_id' => $tripinfo['TransporterId']), array('Image', 'RatingCount', 'RatingByCount', 'FirstName', 'Name'));
			if ($userinfo->count() > 0) {
				$userinfo = $userinfo->getNext();
				$response['result']["TransporterImage"] = $userinfo['Image'];
				if (@$userinfo['RatingCount'] > 0 && @$userinfo['RatingByCount'] > 0) {
					$response['result']["TransporterRating"] = (@$userinfo['RatingCount'] / @$userinfo['RatingByCount']);
				}

				// Check requester has created any request for this transporter
				$requestinfo = $requestCollection->find(array('RequesterId' => $userId, 'TransporterId' => $tripinfo['TransporterId']), array('_id'));

				if ($requestinfo->count() > 0) {
					$response['result']['TransporterName'] = $userinfo['Name'];
				} else {
					$response['result']['TransporterName'] = $userinfo['FirstName'];
				}
			}
		}
	}

	echoRespnse(200, $response);

});

/*
 * Function Name     : delivery_details.
 * Description       : It returns the details of requester.
 * Url               : http://192.168.11.101/aq/services/requester.php/delivery_details
 * Method            : GET
 * Header            : Apikey
 * Param             : requestid
 * Created By        : Pankaj Gawande
 * Create Date       : 14-12-2015
 *
 */
$app->get('/delivery_details', 'requester_auth', function () use ($app) {
	global $db;
	global $userId;
	verifyRequiredParams(array('requestId'));
	$response = array('success' => 0, 'msg' => 'No Records Found');
	$requestCollection = $db->delivery_request;
	$usercollection = $db->users;
	$requestData = $requestCollection->find(array('_id' => new MongoId($app->request->get('requestId')), 'RequesterId' => $userId));

	if ($requestData->count() > 0) {
		$requestData = $requestData->getNext();

		$requester_info = $usercollection->find(['_id' => $userId], ['Default_Currency']);
		if ($requester_info->count() > 0) {
			$requester_info = $requester_info->getNext();
			$currency_res = currency_conversion($requestData['TotalCost']);

			$total_cost_currency = $currency_res['ghanian_cost'];

			if ($requester_info['Default_Currency'] != '') {
				if ($requester_info['Default_Currency'] == 'CAD') {
					$total_cost_currency = $currency_res['canadian_cost'];

				} else if ($requester_info['Default_Currency'] == 'PHP') {
					$total_cost_currency = $currency_res['philipins_cost'];

				} else if ($requester_info['Default_Currency'] == 'GBP') {
					$total_cost_currency = $currency_res['uk_cost'];

				}
			}
		}

		$requestData['PackageId'] = (String) $requestData['_id'];
		unset($requestData['_id']);
		$requestData['RequesterId'] = (String) @$requestData['RequesterId'];
		$requestData['EnterOn'] = (String) @$requestData['EnterOn']->sec;
		$requestData['UpdateOn'] = (String) @$requestData['UpdateOn']->sec;
		$requestData['PickupDate'] = (String) @$requestData['PickupDate']->sec;
		$requestData['DeliveryDate'] = (String) @$requestData['DeliveryDate']->sec;

		$response['result'] = array(
			'Title' => $requestData['ProductTitle'],
			'PackageId' => $requestData['PackageNumber'],
			'RequesterName' => $requestData['RequesterName'],
			'RequestDate' => $requestData['EnterOn'],
			'PickupDate' => $requestData['PickupDate'],
			'PickupAddress' => $requestData['PickupFullAddress'],
			'PickupCountry' => $requestData['PickupCountry'],
			'PickupPinCode' => $requestData['PickupPinCode'],
			'DeliveryAddress' => $requestData['DeliveryFullAddress'],
			'DeliveryDate' => $requestData['DeliveryDate'],
			'TransporterName' => @$requestData['TransporterName'],
			'TransporterId' => (string) @$requestData['TransporterId'],
			'PublicPlace' => ucfirst($requestData['PublicPlace']),
			'FlexibleDeliveryDate' => ucfirst($requestData['FlexibleDeliveryDate']),
			'DeliveryCountry' => $requestData['DeliveryCountry'],
			'ReturnAddress' => $requestData['ReturnFullAddress'],
			'ReturnCountry' => $requestData['ReturnCountry'],
			'ReturnPincode' => $requestData['ReturnPincode'],
			//'Weight' => number_format(change_unit($requestData['ProductWeight'], $requestData['ProductWeightUnit'], 'kg'), 2) . ' Kg',
			'Weightlbs' => number_format(change_unit($requestData['ProductWeight'], $requestData['ProductWeightUnit'], 'lbs'), 2) . ' Lbs',
			'Status' => $requestData['Status'],
			'ProductCost' => number_format($requestData['ProductCost'], 2),
			'PackageDimesion' => "L-" . number_format(change_unit($requestData['ProductLength'], $requestData['ProductLengthUnit'], 'inch'), 2) . 'Inch' . ", H-" . number_format(change_unit($requestData['ProductHeight'], $requestData['ProductHeightUnit'], 'inch'), 2) . 'Inch' . ", W-" . number_format(change_unit($requestData['ProductWidth'], $requestData['ProductWidthUnit'], 'inch'), 2) . 'Inch',
			'PackageDimesion2' => "L-" . number_format(change_unit($requestData['ProductLength'], $requestData['ProductLengthUnit'], 'cm'), 0) . 'Cm' . ", H-" . number_format(change_unit($requestData['ProductHeight'], $requestData['ProductHeightUnit'], 'cm'), 0) . 'Cm' . ", W-" . number_format(change_unit($requestData['ProductWidth'], $requestData['ProductWidthUnit'], 'cm'), 0) . 'Cm',
			'Quantity' => @$requestData['BoxQuantity'],
			'InsuranceStatus' => ucfirst($requestData['InsuranceStatus']),
			'Description' => $requestData['Description'],
			'PackageCareNote' => $requestData['PackageCareNote'],
			'ShippingCost' => number_format($requestData['ShippingCost'], 2),
			'PackageMaterial' => ($requestData['PackageMaterial'] == 'yes') ? 'Yes' : 'N/A',
			"ProductImage" => $requestData['ProductImage'],
			"OtherImage" => $requestData['OtherImage'],
			"DeliveryStatus" => $requestData['Status'],
			"DeliveryStatusToShow" => get_status_title($requestData['Status'], $requestData['RequestType']),
			"RequesterPaid" => number_format($requestData['TotalCost'], 2),
			"AquantuoFess" => @$requestData['AquantuoFees'],
			"TransporterEarnings" => number_format(($requestData['TotalCost']-@$requestData['AquantuoFees']), 2),
			'NotDeliveredReturnAddress' => $requestData['NotDelReturnFullAddress'],
			"DeliveryVerifyCode" => @$requestData["DeliveryVerifyCode"],
			"JournyType" => ucwords(str_replace('_', ' ', $requestData["JournyType"])),
			"TrackingNumber" => @$requestData['TrackingNumber'],
			"RejectBy" => ucfirst(@$requestData['RejectBy']),
			"ReturnType" => ucfirst(@$requestData['ReturnType']),
			"ReceiptImage" => @$requestData['ReceiptImage'],
			"PackageMaterialShipped" => ucfirst(@$requestData['PackageMaterialShipped']),
			"ChatUserId" => (String) @$requestData['TransporterId'],
			"ChatUserName" => "",
			"ChatUsrImage" => "",
			"ChatName" => "",
			"TravelMode" => @$requestData['TravelMode'],
			"TransporterFeedbcak" => @$requestData['TransporterFeedbcak'],
			"TransporterRating" => @$requestData['TransporterRating'],
			"RequesterRating" => @$requestData['RequesterRating'],
			"RequesterFeedbcak" => @$requestData['RequesterFeedbcak'],
			"InsuranceCost" => $requestData['InsuranceCost'],
			"TotalCost" => $requestData['TotalCost'],
			"UserRating" => 0,
			"TransporterMessage" => @$requestData['TransporterMessage'],
			"DeliveredTime" => @$requestData['DeliveredTime']->sec,
			'ReceiverMobileNo' => @$requestData['ReceiverMobileNo'],
			'ReceiverCountrycode' => @$requestData['ReceiverCountrycode'],
			'ProductList' => @$requestData['ProductList'],
		);
		if (@$requestData['TransporterId'] != '') {
			$transporterinfo = $usercollection->find(array('_id' => $requestData['TransporterId']), array('ChatName', 'Name', 'Image', 'RatingCount', 'RatingByCount'));
			if ($transporterinfo->count() > 0) {
				$transporterinfo = (Object) $transporterinfo->getNext();

				$response['result']['ChatUserName'] = $transporterinfo->Name;
				$response['result']['ChatUsrImage'] = $transporterinfo->Image;
				$response['result']['ChatName'] = $transporterinfo->ChatName;
				if (@$transporterinfo->RatingCount > 0 && @$transporterinfo->RatingByCount > 0) {
					$response['result']['UserRating'] = (@$transporterinfo->RatingCount / @$transporterinfo->RatingByCount);
				}
			}
		}

		//array_unshift(@$response['result']["OtherImage"], @$requestData['ProductImage']);
		$response['success'] = 1;
		$response['msg'] = "Records found";

		$requestData['PickupLat'] = $requestData['PickupLatLong'][1];
		$requestData['PickupLong'] = $requestData['PickupLatLong'][0];
		$requestData['DeliveryLat'] = $requestData['DeliveryLatLong'][1];
		$requestData['DeliveryLong'] = $requestData['DeliveryLatLong'][0];

		$requestData['total_cost_currency'] = $total_cost_currency;

		$response['editResponse'] = $requestData;

	}
	echoRespnse(200, $response);
});

function change_unit($weight, $currentunit, $changeto) {

//    echo $weight." ".$currentunit." ".$changeto; //die;

//    echo "<br>";

	$newweight = '';
	$newchangeto = $changeto;
	switch ($currentunit) {
	case 'kg':
		$newweight = $weight * 1000;
		break;
	case 'lbs':
		$newweight = $weight * 453.592;
		break;
	case 'inches':
		$newweight = $weight;
		break;
	case 'cm':
		$newweight = $weight * 0.393701;
		break;
	}

	switch ($changeto) {
	case 'kg':
		$newweight = $newweight / 1000;
		break;
	case 'lbs':
		$newweight = $newweight / 453.592;
		break;
	case 'cm':
		$newweight = $newweight / 0.393701;
		break;
	case 'inches':
		$newweight = $newweight;
		break;
	}

	return floatval($newweight);

}

/*
 * Function Name   : completed_deliveries
 * Description     : This function returns completed deliveries of requester.
 * Url             : http://192.168.11.101/aq/services/requester.php/completed_deliveries
 * Method          : GET
 * Header          : apiKey, userType
 * Parameter       : NA
 * Created By      : Pankaj Gawande
 * Create Date     : 11-12-2015
 *
 */

$app->get('/completed_deliveries', 'requester_auth', function () use ($app) {
	global $db;
	global $userId;
	$response = array('success' => 0, 'msg' => 'No Records Found');

	$limit = 30;
	$start = (((int) $app->request->get('pageno')) - 1) * $limit;

	$requestCollection = $db->delivery_request;
	$requestData = $requestCollection->find(array('RequesterId' => $userId, 'Status' => 'delivered'), array('DeliveryDate', 'ProductTitle', 'TotalCost', 'ProductImage', 'RequestType', 'ProductList','request_version'))
		->skip($start)->limit($limit)->sort(array('_id' => -1));

	if ($requestData->count() > 0) {
		$response['success'] = 1;
		$response['msg'] = "Records found";
		foreach ($requestData as $key => $value) {
			if (isset($value['RequestType']) && in_array(@$value['RequestType'], ['buy_for_me', 'online'])) {
				$value['ProductImage'] = @$value['ProductList'][0]['image'];
			}

			$response['result'][] = array(
				'PackageId' => (String) $value['_id'],
				'ProductTitle' => @$value['ProductTitle'],
				'Date' => isset($value['DeliveryDate']->sec) ? $value['DeliveryDate']->sec : "",
				'TotalCost' => $value['TotalCost'],
				'Status' => 'Completed',
				'ProductImage' => @$value['ProductImage'],
				'RequestType' => isset($value['RequestType']) ? $value['RequestType'] : '',
				'request_version'=> (@$value['request_version'] == null) ? 'old' : $value['request_version'],
				'id'=> (String) $value['_id'],
			);

		}
	}

	echoRespnse(200, $response);

});

/*
 * Function Name   : not_completed_deliveries
 * Description     : This function returns completed deliveries of requester.
 * Url             : http://192.168.11.101/aq/services/requester.php/not_completed_deliveries
 * Method          : GET
 * Header          : apiKey, userType
 * Parameter       : NA
 * Created By      : Pankaj Gawande
 * Create Date     : 11-12-2015
 *
 */

$app->get('/not_completed_deliveries', 'requester_auth', function () use ($app) {
	global $db;
	global $userId;
	$response = array('success' => 0, 'msg' => 'No Records Found');

	$limit = 30;
	$start = (((int) $app->request->get('pageno')) - 1) * $limit;

	$requestCollection = $db->delivery_request;
	$requestData = $requestCollection->find(array('RequesterId' => $userId, 'Status' => 'cancel'), array('RejectTime', 'ProductTitle', 'TotalCost', 'ProductImage', 'Status', 'RequestType', 'ProductList', 'RequestType','request_version'))
		->skip($start)->limit($limit)->sort(array('_id' => -1));

	if ($requestData->count() > 0) {
		$response['success'] = 1;
		$response['msg'] = "Records found";
		foreach ($requestData as $key => $value) {

			if (isset($value['RequestType']) && in_array(@$value['RequestType'], ['buy_for_me', 'online'])) {
				$value['ProductImage'] = @$value['ProductList'][0]['image'];
			}

			if (@$value['ProductList']) {
				foreach ($value['ProductList'] as $plk => $plv) {
					$response['result'][] = array(
						'Requestid' => (String) $value['_id'],
						'RequestType' => $value['RequestType'],
						'PackageId' => (string) $plv['_id'],
						'ProductTitle' => @$plv['product_name'],
						'Date' => @$plv['CancelDate']->sec ? @$plv['CancelDate']->sec : '',
						'TotalCost' => number_format(@$plv['total_cost'], 2),
						'Status' => @$plv['status'],
						'ProductImage' => @$plv['image'],
						'request_version'=> (@$value['request_version'] == null) ? 'old' : $value['request_version'],
					);
				}
			} else {
				$response['result'][] = array(
					'PackageId' => (String) $value['_id'],
					'ProductTitle' => $value['ProductTitle'],
					'Date' => isset($value['EnterOn']->sec) ? (String) $value['EnterOn']->sec : "",
					'TotalCost' => number_format($value['TotalCost'], 2),
					'Status' => get_status_title($value['Status'], $value['RequestType']),
					'ProductImage' => @$value['ProductImage'],
					'RequestType' => isset($value['RequestType']) ? $value['RequestType'] : '',
					'request_version'=> (@$value['request_version'] == null) ? 'old' : $value['request_version'],
					'id'=> (String) $value['_id'],
				);
			}
		}
	}

	echoRespnse(200, $response);

});

/*
 * Function Name :  finish_delivery
 * Description : finish delivery
 * Url : http://192.168.11.101/aq/services/requester.php/feedback_on_package_delivered
 * Method : POST
 * Header : Apikey, Usertype
 * Parameter : requestId,message,rating
 * Created By : Ravi shukla
 * Create Date : 01-09-2015
 * */
$app->post('/feedback_on_package_delivered', 'requester_auth', function () use ($app) {

	global $db;
	global $userId;

	verifyRequiredParams(array('requestId', 'rating', 'message', 'itemId', 'tpid'));
	$response = array('success' => 0, 'msg' => 'Fail to save feedbcak.');

	$requestCollection = $db->delivery_request;
	$usercoll = $db->users;

	if (strlen($app->request->post('requestId')) == 24) {

		$updData = array(
			"RequesterFeedbcak" => $app->request->post('message'),
			"RequesterRating" => (int) $app->request->post('rating'),
		);
		$res = $requestCollection->findAndModify(array('_id' => new MongoId($app->request->post('requestId')), 'RequesterId' => $userId), array('$set' => $updData));
		if (count($res) > 0) {
			
			if(isset($res['TransporterId'])){
			
				$usercoll->update(array('_id' => $res['TransporterId']), array('$inc' => array('RatingByCount' => 1, 'RatingCount' => (	float) $app->request->post('rating'))));
			}

			$response = array('success' => 1, 'msg' => 'Thanks for your feedback.');
		}

	}

	echo echoRespnse(200, $response);
});

$app->post('/feedback_by_requester', 'requester_auth', function () use ($app) {

	global $db;
	global $userId;

	verifyRequiredParams(array('requestId', 'rating', 'message', 'itemId', 'tpid'));
	$response = array('success' => 0, 'msg' => 'Oops! Something went wrong.');

	$requestCollection = $db->delivery_request;
	$usercoll = $db->users;
	if (strlen($app->request->post('requestId')) == 24) {
	
		$where = [
			'ProductList.tpid' => $app->request->post('tpid'),
			'ProductList._id' => $app->request->post('itemId'),
			'ProductList.status' => 'delivered',
			'RequesterId' => $userId,
			'_id' => new MongoId($app->request->post('requestId')),
		];

		$req_data = $requestCollection->find($where, ['ProductList']);

		if ($req_data->count() > 0) {
			$req_data = $req_data->getNext();
			$update_array = $req_data['ProductList'];
			foreach ($req_data['ProductList'] as $key => $value) {
				if ($app->request->post('itemId') == $value['_id'] && $app->request->post('tpid') == $value['tpid']) {
					
					$update_array[$key]['RequesterFeedbcak'] = $app->request->post('message');
					$update_array[$key]['RequesterRating'] = floatval($app->request->post('rating'));

				}
			}
			

			$res = $requestCollection->findAndModify($where, array('$set' => ['ProductList' => $update_array]));

			if (count($res) > 0 && strlen($app->request->post('tpid')) == 24) {
				$usercoll->update(array('_id' => new MongoId($app->request->post('tpid'))), array('$inc' => array('RatingByCount' => 1, 'RatingCount' => (float) $app->request->post('rating'))));
				$response = array('success' => 1, 'msg' => 'Thanks for your feedback.');
			}else{
				$response = array('success' => 1, 'msg' => 'Thanks for your feedback.');
			}
		}

		

	}

	echo echoRespnse(200, $response);
});

/*
 * Function Name :  finish_delivery
 * Description : finish delivery
 * Url : http://192.168.11.101/aq/services/requester.php/remove_package
 * Method : POST
 * Header : Apikey, Usertype
 * Parameter : requestId
 * Created By : Ravi shukla
 * Create Date : 01-09-2015
 * */

$app->post('/remove_package', 'requester_auth', function () use ($app) {

	global $db;
	global $userId;
	global $userInformation;

	$response = array('success' => 0, 'msg' => 'OOops! Something went wrong.');

	$requestCollection = $db->delivery_request;
	$settingcollection = $db->setting;

	if (strlen($app->request->post('requestId')) == 24) {

		$requestinfo = $requestCollection->find(array('_id' => new MongoId($app->request->post('requestId')), 'Status' => array('$in' => array('pending', 'ready')), 'RequesterId' => $userId));

		if ($requestinfo->count() > 0) {
			$requestinfo = (Object) $requestinfo->getNext();

			$res = $requestCollection->remove(array('_id' => new MongoId($app->request->post('requestId')), 'Status' => array('$in' => array('pending', 'ready')), 'RequesterId' => $userId));
			if ($res['n'] > 0) {

				$setting = $settingcollection->find(array('_id' => new MongoId('563b0e31e4b03271a097e1ca')));

				if ($setting->count() > 0) {
					$setting = $setting->getNext();
					require_once 'vendor/mailer/Email.php';
					$ETemplate = array(
						"to" => $setting['FinanceEmail'],
						"replace" => array(
							"[SENDERNAME]" => $requestinfo->RequesterName,
							"[EMAIL]" => $userInformation->Email,
							"[PACKAGEID]" => $requestinfo->PackageNumber,
							"[PACKAGETITLE]" => $requestinfo->ProductTitle,
							"[SOURCE]" => $requestinfo->PickupFullAddress,
							"[DESTINATION]" => $requestinfo->DeliveryFullAddress,
						),
					);
					$response = array('success' => 1, 'msg' => $setting['FinanceEmail']);
					send_mail('56c86a935509251cd677740a', $ETemplate);
				}
				$response = array('success' => 1, 'msg' => 'Package has been removed successfully.');
			} else {
				$response = array('success' => 0, 'msg' => 'Oops! You can delete only ready or pending request.');
			}
		} else {
			$response = array('success' => 0, 'msg' => 'Oops! Something went wrong.');
		}
	}
	echo echoRespnse(200, $response);
});

function get_status_title($status, $requesttype = '') {
	switch ($status) {
	case 'ready':
		return 'Ready';
		break;
	case 'trip_pending':
		return 'Pending';
		break;
	case 'trip_ready':
		return 'Ready';
		break;
	case 'out_for_delivery':
		return 'Out for Delivery';
		break;
	case 'out_for_pickup':
		return ($requesttype == 'online' || $requesttype == 'buy_for_me') ? 'Item has arrived in destination country' : 'Out for Pickup';
		break;
	case 'accepted':
		return 'Accepted';
		break;
	case 'delivered':
		return 'Delivered';
		break;
	case 'cancel':
		return 'Canceled';
		break;
	case 'cancel_by_admin':
		return 'Cancelled by Admin';
		break;
	case 'purchased':
		return ($requesttype == 'online') ? 'Item Received' : 'Item Purchased';
		break;
	case 'assign':
		return ($requesttype != 'local_delivery') ? 'Shipment Departed' : 'Transporter Assigned';
		break;
	case 'paid':
		return ($requesttype == 'online') ? 'Waiting for Item to be received' : 'Pending Purchase';
		break;
		break;
	case 'reviewed':
		return 'Reviewed';
		break;
	case 'not_purchased':
		return 'Waiting for your payment';
		break;
	case 'item_received':
		return 'Item Received';
		break;
	case 'momo_initiated':
		return 'MoMo Initiated';
		break;
	case 'shipment_departed':
		return 'Shipment Departed';
		break;
	default:
		return 'Pending';

	}
}

function get_unit_title($status) {
	switch (strtolower($status)) {
	case 'inch':
		return 'In';
		break;
	case 'meter':
		return 'M';
		break;
	case 'pound':
		return 'Lbs';
		break;
	case 'feet':
		return 'Ft';
		break;
	case 'gram':
		return 'Gm';
		break;
	case 'centimetre':
		return 'Cm';
		break;
	default:
		return ucfirst($status);
		break;
	}
}

function get_formatted_address($array, $zipcode = '') {
	$address = '';
	foreach ($array as $key) {
		if (!empty($key)) {
			$address .= (($address != '') ? ", $key" : $key);
		}
	}
	if (!empty($zipcode)) {$address .= " - $zipcode";}
	return $address;
}

/*
 * Function Name :  online_purchase_shipping_calculation
 * Description : prepare Calulation
 * Url : http://192.168.11.101/aq/services/requester.php/online_purchase_shipping_calculation
 * Method : POST
 * Parameter : distance=1000&productHeight=1&productHeightUnit=meter&productWeightUnit=kg&productLength=1&productLengthUnit=meter&productWidthUnit=meter&productWidth=meter&productCost=50
 * Library : Sequence
 * Created By : Ravi shukla
 * Create Date : 24-08-2015
 *
 * */

function resionCharges($state) {
	global $db;
	$collection = $db->extraregion;

	$charges = $collection->find(['state_id' => $state['id'], 'status' => 'Active'], ['amount']);
	if ($charges->count() > 0) {
		$charges = $charges->getNext();
		return $charges['amount'];
	} else {
		return 0.00;
	}
}

$app->post('/online_purchase_shipping_calculation', 'requester_auth', function () use ($app) {

	global $db;
	global $userId;
	global $userInformation;

	include_once 'Slim/library/Requesthelper.php';

	$collection = $db->delivery_request;
	$settingColl = $db->system_setting;
	$currencycoll = $db->city_state_country;
	$collection = $db->users;

	$response = array('success' => 0, 'msg' => 'Oops! Something went wrong.');

	verifyRequiredParams(['products_list', 'distance', 'consolidate_item','drop_off_state_id','drop_off_country_id','drop_off_country']);

	
	$configuration_coll = $db->configuration;
	$configurationdata = $configuration_coll->find(array('_id' => new MongoId('5673e33e6734c4f874685c84')));
	$configurationdata = $configurationdata->getNext();

	$userinfo = $collection->find(array('_id' => $userId), array());
	if ($userinfo->count() > 0) {
		$userinfo = $userinfo->getNext();

		$aqlat = isset($userinfo['AqLatLong'][1]) ? $userinfo['AqLatLong'][1] : '';
		$aqlong = isset($userinfo['AqLatLong'][0]) ? $userinfo['AqLatLong'][0] : '';

		$data = [
			'shippingcost' => 0,
			'insurance' => 0,
			'InsuranceCost' => 0,
			'ghana_total_amount' => 0,
			'total_amount' => 0,
			'error' => [],
			'formated_text' => '',
			'distance_in_mile' => 0,
			'product_count' => 0,
			'ProcessingFees' => 0,
			'item_cost' => 0,
			'shipping_cost_by_user' => 0,
			'distance' => $app->request->post('distance'),
			'type' => 'online',
			'consolidate_check' => $app->request->post('consolidate_item'),
			'pakage_type'=>'online'
		];

		$data['is_customs_duty_applied'] = false;

		if ($app->request->post('drop_off_country') != "USA" && $app->request->post('drop_off_country') != "Kenya") {
			$data['is_customs_duty_applied'] = true;
		}

		$products_list = json_decode($app->request->post('products_list'), 1);

		if (is_array($products_list)) {
			include_once ('Slim/library/ReqHelper.php');
			$reqHelper = new reqHelper();
			$reqResponse = $reqHelper->getRate($products_list, $data, $userinfo, $app->request->post('drop_off_country'));

			if ($reqResponse['success'] == 0) {
				$response = array('success' => 0, 'msg' => $reqResponse['msg']);
				echoRespnse(200, $response);die;
			}

			$array = $reqResponse['data']['product'];
			$Itemprice = 0;

			$response['msg'] = $reqResponse['msg'];

			if ($reqResponse['success'] == 1) {
				$region_charge = resionCharges(['name' => $app->request->post('drop_off_state'), 'id' => $app->request->post('drop_off_state_id')]);

				$reqResponse['shipping_cost'] = $reqResponse['data']['shipping_cost'];
				$reqResponse['total_amount'] = $reqResponse['data']['total_amount'];
				
				$reqResponse['total_amount'] = $reqResponse['total_amount'] + $region_charge;
				$currency_conversion = currency_conversion($reqResponse['total_amount']);
				if ($userinfo['Default_Currency'] == 'GHS') {
					$user_currency = $currency_conversion['ghanian_cost'];
				} else if ($userinfo['Default_Currency'] == 'CAD') {
					$user_currency = $currency_conversion['canadian_cost'];
				} else if ($userinfo['Default_Currency'] == 'PHP') {
					$user_currency = $currency_conversion['philipins_cost'];
				} else if ($userinfo['Default_Currency'] == 'GBP') {
					$user_currency = $currency_conversion['uk_cost'];
				} else if ($userinfo['Default_Currency'] == 'KES') {
					$user_currency = $currency_conversion['kenya_cost'];
				} else {
					$user_currency = $currency_conversion['ghanian_cost'];
				}

				$array = $reqResponse['data']['product'];
				foreach ($reqResponse['data']['product'] as $key => $value) {
					//$array[$key]['shippingCost'] = $value['shippingCost'] + $value['ProcessingFees'];

				}
				$USDRate = 1;
				$GBPRate = $GHSRate = $CADRate = 1;
				if(isset($currency_conversion['CADRate'])){
					$CADRate  = $currency_conversion['CADRate'];
				}
				if(isset($currency_conversion['GHSRate'])){
					$GHSRate  = $currency_conversion['GHSRate'];
				}
				if(isset($currency_conversion['GBPRate'])){
					$GBPRate  = $currency_conversion['GBPRate'];
				}
				if(isset($currency_conversion['KESRate'])){
					$KESRate  = $currency_conversion['KESRate'];
				}
				$currencies = array(
								array('label'=>'USA - $','symbol'=>'$','value'=>1),
								array('label'=>'UK - £','symbol'=>'£','value'=>$GBPRate),
								array('label'=>'Ghana - GHS','symbol'=>'GHS','value'=>$GHSRate),
								array('label'=>'Canada - C$','symbol'=>'Can$','value'=>$CADRate),
								array('label'=>'Kenya - KES','symbol'=>'Ksh','value'=>$KESRate)
							);

				$response = array(
					'success' => 1,
					'msg' => 'Request calculation ',
					'AreaCharges' => number_format(floatval($region_charge), 2),
					'total_no_of_item' => count($products_list),
					'distanceShow' => number_format($reqHelper->get_distance($app->request->post('distance')), 2) . " Miles",
					'distance' => $app->request->post('distance'),
					'shippingCost' => $reqResponse['shipping_cost'],
					'insurance' => $reqResponse['data']['insurance'],
					'showCurrency' => '$',
					'item_total_price' => $reqResponse['data']['item_cost'],
					'ProcessingFees' => $reqResponse['data']['ProcessingFees'],
					'totalCost' => $reqResponse['total_amount'],
					'shipping_cost_by_user' => $reqResponse['data']['shipping_cost_by_user'],
					'products_list' => $array,
					'GhanaTotalCost' => $user_currency,
					'weight'=> $reqResponse['data']['total_weight'],
					'volume'=> $reqResponse['data']['total_volume'],
					'custom_duty'=> $reqResponse['data']['DutyAndCustom'],
					'tax'=> $reqResponse['data']['Tax'],
					'ups_rate'=>0,
					'note_html' => "If actual weight wasn’t provided, you may be billed or refunded any differences in price or shipping upon review.",
					'currencies'=>$currencies,
				);

			}

		}
	}

	echoRespnse(200, $response);

});

function currency_conversion($shipcost) {
	$data = ['cost' => 0, 'ghanian_cost' => 0, 'philipins_cost' => 0, 'uk_cost' => 0, 'canadian_cost' => 0, 'kenya_cost' => 0];
	global $db;
	$collection = $db->currency;
	$currency = $collection->find(['Status' => 'Active'], ['CurrencyRate', 'FormatedText', 'CurrencyCode']);

	if ($currency->count() > 0) {
		//$currency = $currency->getNext();
		foreach ($currency as $key) {
			if ($key['CurrencyCode'] == 'GHS') {
				$data['GHSRate'] = $key['CurrencyRate'];
				$data['ghanian_cost'] = str_replace('[AMT]', number_format($shipcost * $key['CurrencyRate'], 2), $key['FormatedText']);
			} else if ($key['CurrencyCode'] == 'CAD') {
				$data['CADRate'] = $key['CurrencyRate'];
				$data['canadian_cost'] = str_replace('[AMT]', number_format($shipcost * $key['CurrencyRate'], 2), $key['FormatedText']);
			} else if ($key['CurrencyCode'] == 'PHP') {
				$data['PHPRate'] = $key['CurrencyRate'];
				$data['philipins_cost'] = str_replace('[AMT]', number_format($shipcost * $key['CurrencyRate'], 2), $key['FormatedText']);
			} else if ($key['CurrencyCode'] == 'GBP') {
				$data['GBPRate'] = $key['CurrencyRate'];
				$data['uk_cost'] = str_replace('[AMT]', number_format($shipcost * $key['CurrencyRate'], 2), $key['FormatedText']);
			} else if ($key['CurrencyCode'] == 'KES') {
				$data['KESRate'] = $key['CurrencyRate'];
				$data['kenya_cost'] = str_replace('[AMT]', number_format($shipcost * $key['CurrencyRate'], 2), $key['FormatedText']);
			}
		}
	}
	return $data;
}

/*
 * Online Purchase
 * Para: products_list: [{"marketid" :"ebay","marketname":"eBay","product_name":"Shirt","url" : "http://ebay.com","price":"20","weight":"20","weight_unit" :"lbs","qty" :"5","description":"description"},{"marketid" :"ebay","marketname":"eBay","product_name":"Shirt","url" : "http://ebay.com","price":"20","weight":"20","weight_unit" :"lbs","qty" :"5","description":"description"}]
address1, address2, country,state,city,zipcode,date,time,phone_number,lat,long,time,date,promocode
 */

$app->post('/online_purchase', 'requester_auth', function () use ($app) {

	global $db;
	global $userId;
	global $userInformation;

	verifyRequiredParams(array('products_list', 'address1', 'drop_off_country', 'drop_off_city', 'phone_number', 'drop_off_state_id','drop_off_country_id','drop_off_state'));
	
	include_once 'Slim/library/Sequence.php';

	$requestCollection = $db->delivery_request;
	$userobj = $db->users;
	$paymentInfo = $db->payment_info;

	$configuration_coll = $db->configuration;
	$configurationdata = $configuration_coll->find(array('_id' => new MongoId('5673e33e6734c4f874685c84')));
	$configurationdata = $configurationdata->getNext();

	$response = array('success' => 0, 'msg' => 'Oops! Something went wrong.');

	$userinfo = $userobj->find(array('_id' => $userId), array());
	if ($userinfo->count() > 0) {
		$userinfo = $userinfo->getNext();

		$data = [
			'shippingcost' => 0,
			'insurance' => 0,
			'ghana_total_amount' => 0,
			'total_amount' => 0,
			'error' => [],
			'formated_text' => '',
			'distance_in_mile' => 0,
			'product_count' => 0,
			'ProcessingFees' => 0,
			'item_cost' => 0,
			'shipping_cost_by_user' => 0,
			'distance' => $app->request->post('distance'),
			'type' => 'online',
			'consolidate_check' => $app->request->post('consolidate_item'),
			'pakage_type'=>'online'
		];
		$data['is_customs_duty_applied'] = false;
		if($app->request->post('drop_off_country')!="USA"){
			$data['is_customs_duty_applied'] = true;
		}
		$products_list = json_decode($app->request->post('products_list'), 1);

		if (is_array($products_list)) {

			include_once ('Slim/library/ReqHelper.php');

			$reqHelper = new reqHelper();
			$reqResponse = $reqHelper->getRate($products_list, $data, $userinfo, $app->request->post('drop_off_country'));

			$response['msg'] = $reqResponse['msg'];

			if ($reqResponse['success'] == 1) {
				$products_list = $reqResponse['data']['product'];
				$PackageNumber = getSequence('Request') . time();

				$insert = array(
					"RequesterId" => $userInformation->_id,
					"RequesterName" => $userInformation->Name,
					"TransporterName" => '',
					'api' => 'v8',
					"TransporterId" => '',
					"ProductTitle" => '',
					'request_version' => 'new',
					'ReceiverIsDifferent' => $app->request->post('ReceiverIsDifferent'),
					'ReceiverName' => ucfirst($app->request->post('ReceiverName')),

					//Added 8-11-2017
					"device_version" => $app->request->post('device_version'),
					"app_version" => $app->request->post('app_version'),
					"device_type" => $app->request->post('device_type'),
					//end
					"ProductList" => $products_list,
					"DeliveryFullAddress" => get_formatted_address(array($app->request->post('address1'),
						$app->request->post('address2'),
						$app->request->post('drop_off_city'),
						$app->request->post('drop_off_state'),
						$app->request->post('drop_off_country'),
					), $app->request->post('drop_off_zipcode')
					),
					"DeliveryAddress" => $app->request->post('address1'),
					"DeliveryAddress2" => $app->request->post('address2'),
					"DeliveryCity" => $app->request->post('drop_off_city'),
					"DeliveryState" => $app->request->post('drop_off_state'),
					"DeliveryStateId" => $app->request->post('drop_off_state_id'),
					"DeliveryCountryId" => $app->request->post('drop_off_country_id'),

					"DeliveryCountry" => $app->request->post('drop_off_country'),
					"DeliveryPincode" => $app->request->post('drop_off_zipcode'),
					"DeliveryLatLong" => array(floatval($app->request->post('drop_off_long')), floatval($app->request->post('drop_off_lat'))),
					"DeliveryDate" => (!trim($app->request->post('date'))) ? new MongoDate(strtotime(str_replace('/', '-', $app->request->post('date')))) : '',
					"ReturnToAquantuo" => (floatval($app->request->post('return_to') == 'true')) ? true : false,
					"ReturnAddress" => get_formatted_address([$userinfo['AqAddress'],
						$userinfo['UniqueNo'],
						$userinfo['AqCity'],
						$userinfo['AqState'],
						$userinfo['AqZipcode'],
						$userinfo['AqCountry'],
					]),
					"PickupAddress" => get_formatted_address([$userinfo['AqAddress'],
						$userinfo['UniqueNo'],
						$userinfo['AqCity'],
						$userinfo['AqState'],
						$userinfo['AqZipcode'],
						$userinfo['AqCountry'],
					]
					),
					"ReceiverCountrycode" => $app->request->post('country_code'),
					"ReceiverMobileNo" => $app->request->post('phone_number'),
					"Status" => 'pending',
					"log_type" => 'request',
					"RequestType" => "online",
					"UpdateOn" => new MongoDate(),
					"distance" => $reqHelper->get_distance($app->request->post('distance')),
					'distanceType' => " Miles",
					"PromoCode" => $app->request->post('promocode'),
					"discount" => 0,
					"shippingCost" => $app->request->post('shippingCost'),
					'insurance' => 0,
					'DutyAndCustom' => 0,
					'Tax' => 0,
					"TotalCost" => $app->request->post('totalCost'),
					'AquantuoFees' => 0,
					'ProcessingFees'=>0,
					"after_update_difference" => 0,
					'region_charge' => 0,
					"BeforePurchaseTotalCost" => floatval($reqResponse['total_amount']),
					"GhanaTotalCost" => floatval($reqResponse['ghana_total_amount']),
					"DeliveredDate" => "",
					"PickupLatLong" => $userinfo['AqLatLong'],
					"consolidate_item" => $app->request->post('consolidate_item'),
					'need_to_pay' => 0,

				);
			//	print_r($reqResponse['data']['product']); die;
				foreach ($reqResponse['data']['product'] as $key => $productVal) {

					// $insert['shippingCost'] += $productVal['shippingCost'];
					$insert['insurance'] += $productVal['insurance'];
					
					$insert['ProcessingFees'] += $productVal['ProcessingFees'];
					$insert['AquantuoFees'] += $productVal['aq_fee'];
					
					$insert["TotalCost"] += $productVal['price'];
					
					/*print_r($productVal);die;*/
					$products_list[$key]['_id'] = (String) new MongoId();
					$products_list[$key]['status'] = 'pending';
					$products_list[$key]['verify_code'] = rand(1000, 9999);
					$products_list[$key]['tpid'] = '';
					$products_list[$key]['tpName'] = '';
					$products_list[$key]['shippingCost'] = $productVal['shippingCost'];
					$products_list[$key]['insurance'] = $productVal['insurance'];
					$products_list[$key]['package_id'] = $PackageNumber . $key;
					$products_list[$key]['txnid'] = '';
					$products_list[$key]['aq_fee'] = floatval($productVal['aq_fee']);
					$products_list[$key]['discount'] = 0;
					$products_list[$key]['TransporterFeedbcak'] = '';
					$products_list[$key]['TransporterRating'] = 0;
					$products_list[$key]['RequesterFeedbcak'] = '';
					$products_list[$key]['RequesterRating'] = 0;
					$products_list[$key]['RejectBy'] = '';
					$products_list[$key]['ReturnType'] = '';
					$products_list[$key]['ReceiptImage'] = '';
					$products_list[$key]['RejectTime'] = '';
					$products_list[$key]['TrackingNumber'] = '';
					$products_list[$key]['TransporterMessage'] = '';
					$products_list[$key]['CancelDate'] = '';
					$products_list[$key]['StripeChargeId'] = '';
					$products_list[$key]['DeliveredDate'] = '';
					$products_list[$key]['tracking_number'] = @$productVal['tracking_number'];
					$products_list[$key]['EnterOn'] = new MongoDate;
					$products_list[$key]['total_cost'] = $productVal['shippingCost'] + $productVal['insurance'];
					$products_list[$key]['after_update'] = $productVal['shippingCost'] + $productVal['insurance'];
					$products_list[$key]['inform_mail_sent'] = 'no';
					$products_list[$key]['PaymentStatus'] = 'no';

					if (!isset($products_list[$key]['image'])) {
						$products_list[$key]['image'] = '';
					}

					if (!empty($insert['ProductTitle'])) {
						$insert['ProductTitle'] .= ", ";
					}
					$insert['ProductTitle'] .= $productVal['product_name'];
					if (!empty($productVal['image'])) {
						if (file_exists(FILE_URL . 'temp/' . $productVal['image'])) {
							rename(FILE_URL . 'temp/' . $productVal['image'], FILE_URL . 'package/' . $productVal['image']);
							$products_list[$key]['image'] = "package/{$productVal['image']}";
						}
					}

				}

				$insert['discount'] = $app->request->post('discount');

				$insert['AreaCharges'] = resionCharges(['name' => $app->request->post('drop_off_state'), 'id' => $app->request->post('drop_off_state_id')]);

				$insert["TotalCost"] = ($insert["TotalCost"] + $insert['AreaCharges'] + $reqResponse['data']['DutyAndCustom']+$reqResponse['data']['Tax'] + $insert['ProcessingFees']) - $insert["discount"];
				
				$insert["DutyAndCustom"]  = $reqResponse['data']['DutyAndCustom'];
				$insert["Tax"]  = $reqResponse['data']['Tax'];
				

				if ($insert['TotalCost'] < 0.50) {
					$insert['Status'] = 'ready';
				}
				$insert["ProductList"] = $products_list;
				if (strlen($app->request->post('request_id')) == 24) {
					$res = $requestCollection->update(['_id' => new MongoId($app->request->post('request_id'))], ['$set' => $insert]);

					$req_info = $requestCollection->find(array('_id' => new MongoId($app->request->post('request_id'))), array('PackageNumber'));

					if (@$res['updatedExisting'] > 0) {
						$req_info = $req_info->getNext();
						//$response = ['success' => 1, 'msg' => 'Your request has been updated successfully.', "packageId" => $app->request->post('request_id'), 'PackageNumber' => $req_info['PackageNumber']];
						$response = ['success' => 1, 'msg' => 'Your request has been updated successfully.',
							"packageId" => $app->request->post('request_id'),
							"TotalCost" => $insert['TotalCost'],
							"PackageNumber" => $req_info['PackageNumber'],
						];
					}

					$paymentInfo->insert([
						'request_id' => $app->request->post('request_id'),
						'user_id' => (string) $userId,
						'action_user_id' => (string) $userId,
						'RequestType' => 'online',
						'action' => 'update_request',
						'item_id' => '',
						'item_unic_number' => '',
						'TotalCost' => floatval($insert['TotalCost']),
						'shippingCost' => floatval($insert['shippingCost']),
						'insurance' => floatval($insert['insurance']),
						'AreaCharges' => floatval($insert['AreaCharges']),
						'ProcessingFees' => 0.0,
						'discount' => floatval($insert['discount']),
						'PromoCode' => $insert['PromoCode'],
						'shipping_cost_by_user' => 0.0,
						'item_cost' => 0.0,
						'difference' => floatval($insert['after_update_difference']),
						'difference_before' => floatval($insert['after_update_difference']),
						'date' => New MongoDate(),
					]);
				} else {
					$insert["EnterOn"] = new MongoDate();
					$insert["PackageId"] = getSequence('Request');
					$insert["PackageNumber"] = $PackageNumber;

					$res = $requestCollection->insert($insert);
					if ($res['ok'] == 1) {

						/* Activity Log update by aakash  23-2-2018
							online package request created
						*/
						$insert['action_user_id'] = (string) $userInformation->_id;
						include_once ('notify.php');
						activityLog($insert);

						$response = [
							'success' => 1,
							'msg' => 'Your listing has been successfully created, please proceed to the payment page.',
							"packageId" => (string) $insert['_id'],
							"TotalCost" => $insert['TotalCost'],
							"PackageNumber" => $insert['PackageNumber'],
							"Status" => $insert['Status'],
						];

						$paymentInfo->insert([
							'request_id' => (string) $insert['_id'],
							'user_id' => (string) $userId,
							'action_user_id' => (string) $userId,
							'RequestType' => 'online',
							'action' => 'create',
							'item_id' => '',
							'item_unic_number' => '',
							'TotalCost' => floatval($insert['TotalCost']),
							'shippingCost' => floatval($insert['shippingCost']),
							'insurance' => floatval($insert['insurance']),
							'AreaCharges' => floatval($insert['AreaCharges']),
							'ProcessingFees' => 0.0,
							'discount' => floatval($insert['discount']),
							'PromoCode' => $insert['PromoCode'],
							'shipping_cost_by_user' => 0.0,
							'item_cost' => 0.0,
							'difference' => floatval($insert['after_update_difference']),
							'difference_before' => floatval($insert['after_update_difference']),
							'date' => New MongoDate(),
						]);

						if ($insert['Status'] == 'ready') {
							// Email and notifcation section
							//require_once 'notify.php';
							//online_send_notification_to_admin($insert);
							//online_send_email_to_requester($insert);
							//online_send_email_to_admin($insert, $userinfo);
						}
					}
				}

			}

		}
	}
	/*$response['note_html'] = "You will be billed or refunded any differences in price or shipping at the time of purchase.";*/
	$response['note_html'] = "I understand that customs duty may be accessed on a value of my item other than what I provided.";
	echoRespnse(200, $response);

});

/*
 * Function Name     : delivery_details.
 * Description         : It returns the details of requester.
 * Url                 : http://192.168.11.101/aq/services/requester.php/delivery_details
 * Method             : GET
 * Header           : Apikey
 * Param            : requestid
 * Created By         : Pankaj Gawande
 * Create Date         : 14-12-2015
 *
 */

$app->get('/online-delivery-details', 'requester_auth', function () use ($app) {

	global $db;
	global $userId;
	verifyRequiredParams(array('requestId'));
	$response = array('success' => 0, 'msg' => 'No Records Found');
	$requestCollection = $db->delivery_request;
	$userobj = $db->users;

	$itemhistory = $db->itemhistory;
	$itemhistory = $itemhistory->find(['request_id' => $app->request->get('requestId')]);
	$requestData = $requestCollection->find(array('_id' => new MongoId($app->request->get('requestId')), 'RequesterId' => $userId));

	if ($requestData->count() > 0) {
		$card = $db->request_cards->find(['request_id' => $app->request->get('requestId')]);
		$requestData = $requestData->getNext();

		$requester_info = $userobj->find(['_id' => $userId], ['Default_Currency']);
		if ($requester_info->count() > 0) {
			$requester_info = $requester_info->getNext();
			$currency_res = currency_conversion($requestData['TotalCost']);
			$currency_res2 = currency_conversion(@$requestData['need_to_pay']);
			$total_cost_currency = $currency_res['ghanian_cost'];
			$need_to_pay_currency = $currency_res2['ghanian_cost'];
			if ($requester_info['Default_Currency'] != '') {
				if ($requester_info['Default_Currency'] == 'CAD') {
					$total_cost_currency = $currency_res['canadian_cost'];
					$need_to_pay_currency = $currency_res2['canadian_cost'];
				} else if ($requester_info['Default_Currency'] == 'PHP') {
					$total_cost_currency = $currency_res['philipins_cost'];
					$need_to_pay_currency = $currency_res2['philipins_cost'];
				} else if ($requester_info['Default_Currency'] == 'GBP') {
					$total_cost_currency = $currency_res['uk_cost'];
					$need_to_pay_currency = $currency_res2['uk_cost'];
				}
			}
		}

		$requestData['PackageId'] = (String) $requestData['_id'];
		$requestData['requestData'] = $requestData;
		unset($requestData['_id']);
		$requestData['RequesterId'] = (String) @$requestData['RequesterId'];
		$requestData['EnterOn'] = (String) @$requestData['EnterOn']->sec;
		$requestData['UpdateOn'] = (String) @$requestData['UpdateOn']->sec;
		$requestData['DeliveryDate'] = (String) @$requestData['DeliveryDate']->sec;
		$requestData['ItemPrice'] = 0;
		$requestData['region_charge'] = @$requestData['AreaCharges'];
		$requestData['shippingCost'] = @$requestData['shippingCost'];
		$requestData['ReceiverName'] = @$requestData['ReceiverName'];
		if($requestData['Status'] == 'pending'){
			$requestData['mobilemoney'] = @$requestData['mobilemoney'];
		}else{
			$requestData['mobilemoney'] = @$requestData['mobilemoney'];
		}

		$response['editResponse'] = $requestData;

		foreach ($requestData['ProductList'] as $key => $val) {

			$requestData['ProductList'][$key]["shippingCost"] = $val['shippingCost'];
			$requestData['ItemPrice'] += @$val['price'];

			$requestData['ProductList'][$key]["DeliveredDate"] = isset($val['DeliveredDate']->sec) ? $val['DeliveredDate']->sec : '';
			$requestData['ProductList'][$key]["ChatUserName"] = '';
			$requestData['ProductList'][$key]["ChatUsrImage"] = '';
			$requestData['ProductList'][$key]["ChatName"] = '';
			$requestData['ProductList'][$key]["ChatUserId"] = $val['tpid'];
			$requestData['ProductList'][$key]['UserRating'] = 0;

			if ($val['status'] == 'cancel' && @$val['RejectBy'] != '') {
				$requestData['ProductList'][$key]['StatusTitle'] = "Not Delivered";
			} else {
				$requestData['ProductList'][$key]['StatusTitle'] = get_status_title(
					$val['status'], $requestData['RequestType']);
			}
			//$requestData['ProductList'][$key]['TotalCost'] = $val['insurance'] + $val['shippingCost'];
			$requestData['ProductList'][$key]['DeliveredDate'] = isset($val['DeliveredDate']->sec) ? $val['DeliveredDate']->sec : '';

			$requestData['ProductList'][$key]['PickupLat'] = @$requestData['PickupLatLong'][1];
			$requestData['ProductList'][$key]['PickupLong'] = @$requestData['PickupLatLong'][0];
			$requestData['ProductList'][$key]['DeliveryLat'] = @$requestData['DeliveryLatLong'][1];
			$requestData['ProductList'][$key]['DeliveryLong'] = @$requestData['DeliveryLatLong'][0];

			if (strlen($val['tpid']) == 24) {
				$userinfo = $userobj->find(['_id' => new MongoId($val['tpid'])], ['Name', 'Image', 'ChatName', 'RatingByCount', 'RatingCount']);
				if ($userinfo->count() > 0) {
					$userinfo = (Object) $userinfo->getNext();

					$requestData['ProductList'][$key]["ChatUserName"] = $userinfo->Name;
					$requestData['ProductList'][$key]["ChatUsrImage"] = $userinfo->Image;
					$requestData['ProductList'][$key]["ChatName"] = $userinfo->ChatName;
					if ($userinfo->RatingCount > 0 && $userinfo->RatingByCount > 0) {
						$requestData['ProductList'][$key]['UserRating'] = $userinfo->RatingCount / $userinfo->RatingByCount;
					}

				}
			}

		}
		$requestData['distanceShow'] = (@$requestData['distance'] > 0) ? number_format($requestData['distance'], 2) . " Miles" : '0 Miles';
		$requestData['total_no_of_item'] = count($requestData['ProductList']);

		$requestData['GhanaTotalCost'] = '(In Ghanaian cedi GHS ' . @$requestData['GhanaTotalCost'] . ')';
		/*$requestData['note_html'] = 'You will be billed or refunded any differences in price or shipping at the time of purchase.';*/
		$response['note_html'] = "If actual weight wasn’t provided, you may be billed or refunded any differences in price or shipping upon review";

		$response['result'] = $requestData;
		$response['result']['cards'] = [];
		$response['result']['updated_data'] = [];
		if ($itemhistory->count() > 0 && isset($requestData['need_to_pay'])) {
			foreach ($itemhistory as $key) {
				$response['result']['updated_data'][] = ['msg' => "After reviewing your listing, '" . $key['product_name'] . "', the cost was increased by $" . number_format(@$key['update_difference'], 2) . "."];

			}
		}

		if ($card->count() > 0) {
			///$card = $card->getNext();
			foreach ($card as $key) {
				$response['result']['cards'][] = [
					'last4' => $key['last4'],
					'brand' => $key['brand'],
					'amount' => $key['payment'],
				];
			}
		}
		$response['result']['total_cost_currency'] = $total_cost_currency;
		$response['result']['need_to_pay_currency'] = $need_to_pay_currency;

		$response['success'] = 1;
		$response['msg'] = "Records found";
	}
	//$requestData['note_html']='You will be billed or refunded any differences in price or shipping at the time of purchase.';
	//$response['note_html'] = 'You will be billed or refunded any differences in price or shipping at the time of purchase.';
	echoRespnse(200, $response);
});

/*
 * Function Name     : delivery_details.
 * Description         : It returns the details of requester.
 * Url                 : http://192.168.11.101/aq/services/requester.php/delivery_details
 * Method             : GET
 * Header           : Apikey
 * Param            : requestid
 * Created By         : Pankaj Gawande
 * Create Date         : 14-12-2015
 *
 */

$app->get('/buy-for-me-details', 'requester_auth', function () use ($app) {
	global $db;
	global $userId;
	global $userInformation;

	verifyRequiredParams(array('requestId'));
	$response = array('success' => 0, 'msg' => 'No Records Found');
	$requestCollection = $db->delivery_request;
	$userobj = $db->users;
	$itemhistory = $db->itemhistory;
	$itemhistory = $itemhistory->find(['request_id' => $app->request->get('requestId')]);
	$requestData = $requestCollection->find(array('_id' => new MongoId($app->request->get('requestId')), 'RequesterId' => $userId));

	if ($requestData->count() > 0) {
		$requestData = $requestData->getNext();
		$card = $db->request_cards->find(['request_id' => $app->request->get('requestId')]);

		$requester_info = $userobj->find(['_id' => $userId], ['Default_Currency']);
		if ($requester_info->count() > 0) {
			$requester_info = $requester_info->getNext();
			$currency_res = currency_conversion($requestData['TotalCost']);
			$currency_res2 = currency_conversion(@$requestData['need_to_pay']);
			$total_cost_currency = $currency_res['ghanian_cost'];
			$need_to_pay_currency = $currency_res2['ghanian_cost'];
			if ($requester_info['Default_Currency'] != '') {
				if ($requester_info['Default_Currency'] == 'CAD') {
					$total_cost_currency = $currency_res['canadian_cost'];
					$need_to_pay_currency = $currency_res2['canadian_cost'];
				} else if ($requester_info['Default_Currency'] == 'PHP') {
					$total_cost_currency = $currency_res['philipins_cost'];
					$need_to_pay_currency = $currency_res2['philipins_cost'];
				} else if ($requester_info['Default_Currency'] == 'GBP') {
					$total_cost_currency = $currency_res['uk_cost'];
					$need_to_pay_currency = $currency_res2['uk_cost'];
				}
			}
		}

		$requestData['PackageId'] = (String) $requestData['_id'];
		$requestData['_id'] = (string) $requestData['_id'];
		$requestData['RequesterId'] = (String) @$requestData['RequesterId'];
		$requestData['EnterOn'] = (String) @$requestData['EnterOn']->sec;
		$requestData['UpdateOn'] = (String) @$requestData['UpdateOn']->sec;
		$requestData['DeliveryDate'] = (String) @$requestData['DeliveryDate']->sec;
		$requestData['ItemPrice'] = 0;
		$requestData['region_charge'] = @$requestData['AreaCharges'];
		$requestData['shippingCost'] = $requestData['shippingCost'];

		$response['editResponse'] = $requestData;

		foreach ($requestData['ProductList'] as $key => $val) {
			$requestData['ProductList'][$key]['shippingCost'] = $val['shippingCost'];

			$requestData['ItemPrice'] += $val['price'] * $val['qty'];
			$requestData['ProductList'][$key]['price'] = number_format($val['price'], 2);
			$requestData['ProductList'][$key]['product_name'] = ucfirst($val['product_name']);
			$requestData['ProductList'][$key]['travelMode'] = ucfirst($val['travelMode']);
			if ($val['status'] == 'cancel' && @$val['RejectBy'] != '') {
				$requestData['ProductList'][$key]['StatusTitle'] = "Not Delivered";
			} else {
				$requestData['ProductList'][$key]['StatusTitle'] = get_status_title(
					$val['status'], $requestData['RequestType']);
			}
			$requestData['ProductList'][$key]['TransporterName'] = '';
			$requestData['ProductList'][$key]['TransporterImage'] = '';
			$requestData['ProductList'][$key]['ChatUserName'] = '';
			$requestData['ProductList'][$key]['ChatUsrImage'] = '';
			$requestData['ProductList'][$key]['ChatName'] = '';
			$requestData['ProductList'][$key]['UserRating'] = '';

			$requestData['ProductList'][$key]['PickupLat'] = @$requestData['PickupLatLong'][1];
			$requestData['ProductList'][$key]['PickupLong'] = @$requestData['PickupLatLong'][0];
			$requestData['ProductList'][$key]['DeliveryLat'] = @$requestData['DeliveryLatLong'][1];
			$requestData['ProductList'][$key]['DeliveryLong'] = @$requestData['DeliveryLatLong'][0];

			$requestData['ProductList'][$key]["DeliveredDate"] = isset($val['DeliveredDate']->sec) ? $val['DeliveredDate']->sec : '';

			if (strlen($val['tpid']) == 24) {
				$userinfo = $userobj->find(['_id' => new MongoId($val['tpid'])], ['Name', 'Image', 'ChatName', 'RatingByCount', 'RatingCount']);
				if ($userinfo->count() > 0) {
					$userinfo = (Object) $userinfo->getNext();

					$requestData['ProductList'][$key]['TransporterName'] = $userinfo->Name;
					$requestData['ProductList'][$key]['TransporterImage'] = $userinfo->Image;
					$requestData['ProductList'][$key]["ChatUserName"] = $userinfo->Name;
					$requestData['ProductList'][$key]["ChatUsrImage"] = $userinfo->Image;
					$requestData['ProductList'][$key]["ChatName"] = $userinfo->ChatName;
					$requestData['ProductList'][$key]["ChatUserId"] = $val['tpid'];

					if ($userinfo->RatingCount > 0 && $userinfo->RatingByCount > 0) {
						$requestData['ProductList'][$key]['UserRating'] = $userinfo->RatingCount / $userinfo->RatingByCount;
					}
				}
			}
		}

		$requestData['distanceShow'] = ($requestData['distance'] > 0) ? number_format($requestData['distance'], 2) . " Miles" : '0 Miles';
		$requestData['StatusTitle'] = get_status_title($requestData['Status'], $requestData['RequestType']);
		$requestData['total_no_of_item'] = count($requestData['ProductList']);

		// Currency conversation
		if ($requestData['GhanaTotalCost'] <= 0) {

			$currencycoll = $db->city_state_country;

			$currencydata = $currencycoll->find(array('CurrencyCode' => $userInformation->Default_Currency));

			if ($currencydata->count() > 0) {

				$currencydata = $currencydata->getNext();

				$FormatedText = @$currencydata['FormatedText'];

				$GhanaTotalCost = $requestData['TotalCost'] * (float) @$currencydata['CurrencyRate'];

				$requestData['GhanaTotalCost'] = str_replace('[AMT]', $GhanaTotalCost, $FormatedText);

				if (in_array($requestData['Status'], ['paid', 'out_for_pickup', 'out_for_delivery', 'delivered'])) {
					$requestCollection->update(['_id' => $requestData['_id']], ['$set' => ['GhanaTotalCost' => $GhanaTotalCost]]);
				}
			}
		}
		// Currency conversation
		//	echo "<pre>";
		//	print_r($requestData);die;
		$requestData['showCurrency'] = '$';
		/*$requestData['note_html'] = 'You will be billed or refunded any differences in price or shipping at the time of purchase.';*/
		$response['note_html'] = "If actual weight wasn’t provided, you may be billed or refunded any differences in price or shipping upon review";

		if (isset($requestData['request_version']) && $requestData['request_version'] == 'new') {
			$totalCost = ($requestData['TotalCost'] + $requestData['after_update_difference']);
		} else {
			$totalCost = $requestData['TotalCost'];
		}

		$response['result'] = $requestData;

		$response['result']['cards'] = [];
		$response['result']['updated_data'] = [];
		if ($itemhistory->count() > 0 && isset($requestData['need_to_pay'])) {
			foreach ($itemhistory as $key) {
				$response['result']['updated_data'][] = ['msg' => "After reviewing your listing, '" . $key['product_name'] . "', the cost was increased by $" . number_format($key['update_difference'], 2) . "."];

			}
		}
		if ($card->count() > 0) {
			///$card = $card->getNext();
			foreach ($card as $key) {
				$response['result']['cards'][] = [
					'last4' => $key['last4'],
					'brand' => $key['brand'],
					'amount' => $key['payment'],
				];
			}
		}

		$response['result']['total_cost_currency'] = $total_cost_currency;
		$response['result']['need_to_pay_currency'] = $need_to_pay_currency;

		$response['success'] = 1;
		$response['msg'] = "Records found";

	}
	echoRespnse(200, $response);
});

/*
 * Function Name : get_discount
 * Description      :
 * Url              : http://192.168.11.101/aq/dev/services/dev/customer.php/get_discount
 * Method          : POST
 * Created By    : Ravi shukla
 * Create Date   : 24-08-2016
 * */
$app->post('/get_discount', 'requester_auth', function () use ($app) {
	global $db;
	global $userInformation;
	verifyRequiredParams(array('PromoCode', 'TotalAmount'));
	require 'Slim/library/GetDiscount.php';
	$response = array("success" => 0, "msg" => 'The promo code you have entered is not valid.');

	$res = get_discount_amt($app->request->post('PromoCode'),
		$app->request->post('ShippingCost'),
		$app->request->post('TotalAmount'),
		$userInformation->Default_Currency
	);
	$response['msg'] = $res['msg'];
	if ($res['success'] == 1) {
		$response['success'] = 1;
		$response['discount'] = $res['discount'];
		$response['GhanaTotalCost'] = $res['GhanaTotalCost'];
	}
	echoRespnse(200, $response);

});

function mark_promocde_as_use($code) {
	if (!empty(trim($code))) {
		global $db;
		$promocode = $db->promocode;

		$promocode->update(
			array('Code' => strtoupper($code)),
			array('$inc' => array('UsesCount' => 1))
		);
	}
}

$app->post('/upload_image', function () use ($app) {
	$response = ['success' => 0, 'msg' => 'Something went wrong.'];

	if (isset($_FILES['image']['name'])) {
		if (!empty(trim($_FILES['image']['name']))) {

			$exts = pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);

			$filename = rand(1000, 9999) . time() . ".$exts";
			$newfile = FILE_URL . "temp/$filename";
			if (move_uploaded_file($_FILES['image']['tmp_name'], $newfile)) {
				$response = [
					'success' => 1,
					'msg' => 'File has been uploaded successfully.',
					'filename' => $filename,
					'show_file' => "temp/$filename",
				];
			}
		}
	}
	$response['test'] = $_FILES;
	echoRespnse(200, $response);
});

/*
 * Function Name :  online_purchase_shipping_calculation
 * Description : prepare Calulation
 * Url : http://192.168.11.101/aq/services/requester.php/online_purchase_shipping_calculation
 * Method : POST
 * Parameter : distance=1000&productHeight=1&productHeightUnit=meter&productWeightUnit=kg&productLength=1&productLengthUnit=meter&productWidthUnit=meter&productWidth=meter&productCost=50
 * Library : Sequence
 * Created By : Ravi shukla
 * Create Date : 24-08-2015
 *
 * */

$app->post('/buy_for_me_shipping_cost', 'requester_auth', function () use ($app) {

	global $db;
	global $userId;
	global $userInformation;

	include_once 'Slim/library/Requesthelper.php';

	$collection = $db->delivery_request;
	$settingColl = $db->system_setting;
	$currencycoll = $db->city_state_country;
	$collection = $db->users;

	$response = array('success' => 0, 'msg' => 'Oops! Something went wrong.');
	verifyRequiredParams(['products_list', 'distance', 'consolidate_item','drop_off_state_id','drop_off_country_id','drop_off_country']);
	
	$configuration_coll = $db->configuration;
	$configurationdata = $configuration_coll->find(array('_id' => new MongoId('5673e33e6734c4f874685c84')));
	$configurationdata = $configurationdata->getNext();

	$userinfo = $collection->find(array('_id' => $userId), array());
	if ($userinfo->count() > 0) {
		$userinfo = $userinfo->getNext();

		$aqlat = isset($userinfo['AqLatLong'][1]) ? $userinfo['AqLatLong'][1] : '';
		$aqlong = isset($userinfo['AqLatLong'][0]) ? $userinfo['AqLatLong'][0] : '';

		$data = [
			'shippingcost' => 0,
			'insurance' => 0,
			'ghana_total_amount' => 0,
			'total_amount' => 0,
			'error' => [],
			'formated_text' => '',
			'distance_in_mile' => 0,
			'product_count' => 0,
			'ProcessingFees' => 0,
			'item_cost' => 0,
			'shipping_cost_by_user' => 0,
			'distance' => $app->request->post('distance'),
			'type' => 'buyforme',
			'consolidate_check' => $app->request->post('consolidate_item'),
			'pakage_type'=>'buyforme'
		];
		$data['is_customs_duty_applied'] = false;
		if($app->request->post('drop_off_country')!="USA" || $app->request->post('drop_off_country')!="Kenya"){
			$data['is_customs_duty_applied'] = true;
		}
		$products_list = json_decode($app->request->post('products_list'), 1);
		if (is_array($products_list)) {

			include_once ('Slim/library/ReqHelper.php');

			$reqHelper = new reqHelper();
			$reqResponse = $reqHelper->getRate($products_list, $data, $userinfo, $app->request->post('drop_off_country'));
			// print_r($reqResponse);
			$response['msg'] = $reqResponse['msg'];
			
			if ($reqResponse['success'] == 1) {

				$region_charge = resionCharges(['name' => $app->request->post('drop_off_state'), 'id' => $app->request->post('drop_off_state_id')]);

				$reqResponse['shipping_cost'] = $reqResponse['data']['shipping_cost'];
				$reqResponse['total_amount'] = $reqResponse['data']['total_amount'];
				
				
				

				$reqResponse['total_amount'] = $reqResponse['total_amount'] + $region_charge;
				$currency_conversion = currency_conversion($reqResponse['total_amount']);
				if ($userinfo['Default_Currency'] == 'GHS') {
					$user_currency = $currency_conversion['ghanian_cost'];
				} else if ($userinfo['Default_Currency'] == 'CAD') {
					$user_currency = $currency_conversion['canadian_cost'];
				} else if ($userinfo['Default_Currency'] == 'PHP') {
					$user_currency = $currency_conversion['philipins_cost'];
				} else if ($userinfo['Default_Currency'] == 'GBP') {
					$user_currency = $currency_conversion['uk_cost'];
				} else if ($userinfo['Default_Currency'] == 'KES') {
					$user_currency = $currency_conversion['kenya_cost'];
				} else {
					$user_currency = $currency_conversion['ghanian_cost'];
				}

				$array = $reqResponse['data']['product'];
				foreach ($reqResponse['data']['product'] as $key => $value) {
					//$array[$key]['shippingCost'] = $value['shippingCost'] + $value['ProcessingFees'];

				}
				$USDRate = 1;
				$GBPRate = $GHSRate = $CADRate = $KESRate = 1;
				if(isset($currency_conversion['CADRate'])){
					$CADRate  = $currency_conversion['CADRate'];
				}
				if(isset($currency_conversion['GHSRate'])){
					$GHSRate  = $currency_conversion['GHSRate'];
				}
				if(isset($currency_conversion['GBPRate'])){
					$GBPRate  = $currency_conversion['GBPRate'];
				}
				if(isset($currency_conversion['KESRate'])){
					$KESRate  = $currency_conversion['KESRate'];
				}
				$currencies = array(
								array('label'=>'USA - $','symbol'=>'$','value'=>1),
								array('label'=>'UK - £','symbol'=>'£','value'=>$GBPRate),
								array('label'=>'Ghana - GHS','symbol'=>'GHS','value'=>$GHSRate),
								array('label'=>'Canada - C$','symbol'=>'Can$','value'=>$CADRate),
								array('label'=>'Kenya - KES','symbol'=>'Ksh','value'=>$KESRate)
							);

				$response = array(
					'success' => 1,
					'msg' => 'Request calculation ',
					'AreaCharges' => number_format(floatval($region_charge), 2),
					'total_no_of_item' => count($products_list),
					'distanceShow' => number_format($reqHelper->get_distance($app->request->post('distance')), 2) . " Miles",
					'distance' => $app->request->post('distance'),
					'shippingCost' => $reqResponse['shipping_cost'],
					'insurance' => $reqResponse['data']['insurance'],
					'showCurrency' => '$',
					'item_total_price' => $reqResponse['data']['item_cost'],
					'ProcessingFees' => $reqResponse['data']['ProcessingFees'],
					'totalCost' => $reqResponse['total_amount'],
					'shipping_cost_by_user' => $reqResponse['data']['shipping_cost_by_user'],
					'products_list' => $array,
					'GhanaTotalCost' => $user_currency,
					'weight'=> $reqResponse['data']['total_weight'],
					'volume'=> $reqResponse['data']['total_volume'],
					'custom_duty'=> number_format($reqResponse['data']['DutyAndCustom'], 2),
					'tax'=> $reqResponse['data']['Tax'],
					'ups_rate'=>0,
					'note_html' => "If actual weight wasn’t provided, you may be billed or refunded any differences in price or shipping upon review.",
					'currencies'=>$currencies,
				);

			}

		}
	}

	echoRespnse(200, $response);

});

function get_processing_fee($itemPrice) {
	global $db;
	$ConfigColl = $db->configuration;

	$Config = $ConfigColl->find(['_id' => new MongoId('5673e33e6734c4f874685c84')], ['ProcessingFees']);
	if ($Config->count() > 0) {
		$Config = $Config->getNext();
		return ($Config['ProcessingFees'] * $itemPrice) / 100;
	}
	return 0;
}

/*
 * Online Purchase
 * Para: products_list: [{"marketid" :"ebay","marketname":"eBay","product_name":"Shirt","url" : "http://ebay.com","price":"20","weight":"20","weight_unit" :"lbs","qty" :"5","description":"description"},{"marketid" :"ebay","marketname":"eBay","product_name":"Shirt","url" : "http://ebay.com","price":"20","weight":"20","weight_unit" :"lbs","qty" :"5","description":"description"}]
address1, address2, country,state,city,zipcode,date,time,phone_number,lat,long,time,date,promocode
 */
$app->post('/buy_for_me', 'requester_auth', function () use ($app) {

	global $db;
	global $userInformation;
	global $userId;
	require 'Slim/library/Sequence.php';
	include_once 'Slim/library/Requesthelper.php';
	include_once 'Slim/library/ReqStatus.php';
	require 'Slim/library/GetDiscount.php';
	require_once 'vendor/mailer/Email.php';

	verifyRequiredParams(array('products_list', 'address1', 'drop_off_country', 'drop_off_city', 'phone_number', 'drop_off_state_id','drop_off_country_id','drop_off_state'));
	$response = array('success' => 0, 'msg' => 'Oops! Something went wrong.');

	$userobj = $db->users;
	$notification = $db->notification;
	$requestCollection = $db->delivery_request;
	$paymentInfo = $db->payment_info;

	$configuration_coll = $db->configuration;
	$configurationdata = $configuration_coll->find(array('_id' => new MongoId('5673e33e6734c4f874685c84')));
	$configurationdata = $configurationdata->getNext();

	$userinfo = $userobj->find(array('_id' => $userInformation->_id), array());
	if ($userinfo->count() > 0) {
		$userinfo = $userinfo->getNext();

		$aqlat = isset($userinfo['AqLatLong'][1]) ? $userinfo['AqLatLong'][1] : '';
		$aqlong = isset($userinfo['AqLatLong'][0]) ? $userinfo['AqLatLong'][0] : '';

		$data = [
			'shippingcost' => 0,
			'insurance' => 0,
			'ghana_total_amount' => 0,
			'total_amount' => 0,
			'error' => [],
			'formated_text' => '',
			'distance_in_mile' => 0,
			'product_count' => 0,
			'ProcessingFees' => 0,
			'item_cost' => 0,
			'shipping_cost_by_user' => 0,
			'distance' => $app->request->post('distance'),
			'type' => 'buyforme',
			'consolidate_check' => $app->request->post('consolidate_item'),
			'pakage_type'=>'buyforme'
		];
		$data['is_customs_duty_applied'] = false;
		if($app->request->post('drop_off_country')!="USA" || $app->request->post('drop_off_country')!="Kenya"){
			$data['is_customs_duty_applied'] = true;
		}

		$products_list = json_decode($app->request->post('products_list'), 1);

		if (is_array($products_list)) {

			include_once ('Slim/library/ReqHelper.php');

			$reqHelper = new reqHelper();
			$reqResponse = $reqHelper->getRate($products_list, $data, $userinfo, $app->request->post('drop_off_country'));

			$response['msg'] = $reqResponse['msg'];
			if ($reqResponse['success'] == 1) {

				$distance = $app->request->post('distance');

				$PackageNumber = getSequence('Request') . time();
				$insert = array(
					"RequesterId" => $userInformation->_id,
					"RequesterName" => $userInformation->Name,
					'api' => 'v8',
					"TransporterName" => '',
					"TransporterId" => '',
					'request_version' => 'new',
					'ReceiverIsDifferent' => $app->request->post('ReceiverIsDifferent'),
					'ReceiverName' => ucfirst($app->request->post('ReceiverName')),
					//Added 8-11-2017
					"device_version" => $app->request->post('device_version'),
					"app_version" => $app->request->post('app_version'),
					"device_type" => $app->request->post('device_type'),
					//end
					"PackageNumber" => $PackageNumber,
					"ProductTitle" => '',
					"ProductList" => $products_list,
					"DeliveryFullAddress" => get_formatted_address(array($app->request->post('address1'),
						$app->request->post('address2'),
						$app->request->post('drop_off_city'),
						$app->request->post('drop_off_state'),
						$app->request->post('drop_off_country'),
					), $app->request->post('drop_off_zipcode')
					),
					"DeliveryAddress" => $app->request->post('address1'),
					"DeliveryAddress2" => $app->request->post('address2'),
					"DeliveryCity" => $app->request->post('drop_off_city'),
					"DeliveryState" => $app->request->post('drop_off_state'),
					"DeliveryCountry" => $app->request->post('drop_off_country'),
					"DeliveryStateId" => $app->request->post('drop_off_state_id'),
					"DeliveryCountryId" => $app->request->post('drop_off_country_id'),

					"DeliveryPincode" => $app->request->post('drop_off_zipcode'),
					"DeliveryLatLong" => array(floatval($app->request->post('drop_off_long')), floatval($app->request->post('drop_off_lat'))),
					"DeliveryDate" => new MongoDate(strtotime(str_replace('/', '-', $app->request->post('date') . " " . $app->request->post('time')))),
					"ReturnToAquantuo" => (floatval($app->request->post('return_to') == 'true')) ? true : false,
					"ReturnAddress" => get_formatted_address([$userinfo['AqAddress'],
						$userinfo['UniqueNo'],
						$userinfo['AqCity'],
						$userinfo['AqState'],
						$userinfo['AqZipcode'],
						$userinfo['AqCountry'],
					]),
					"PickupAddress" => get_formatted_address([$userinfo['AqAddress'],
						$userinfo['UniqueNo'],
						$userinfo['AqCity'],
						$userinfo['AqState'],
						$userinfo['AqZipcode'],
						$userinfo['AqCountry'],
					]
					),
					"PickupLatLong" => $userinfo['AqLatLong'],
					"ReceiverCountrycode" => $app->request->post('country_code'),

					"ReceiverMobileNo" => $app->request->post('phone_number'),
					"Status" => 'pending',
					"log_type" => 'request',
					"PaymentStatus" => 'paid',
					"RequestType" => "buy_for_me",
					"distance" => $reqHelper->get_distance($distance),
					'distanceType' => " Miles",
					"PromoCode" => $app->request->post('promocode'),
					"discount" => 0,
					"shippingCost" => $app->request->post('shippingCost'),
					'insurance' => 0,
					'DutyAndCustom' => 0,
					'Tax' => 0,
					'AquantuoFees' => 0,
					"ProcessingFees" => 0,
					"TotalCost" => 0,
					'total_item_price' => 0,
					'after_update_difference' => 0,
					"BeforePurchaseTotalCost" => $reqResponse['total_amount'],
					"shipping_cost_by_user" => $reqResponse['data']['shipping_cost_by_user'],
					"DeliveredDate" => '',
					"GhanaTotalCost" => $app->request->post('GhanaTotalCost'),
					"consolidate_item" => $app->request->post('consolidate_item'),
					'need_to_pay' => 0,
					"UpdateOn" => new MongoDate(),
					"EnterOn" => new MongoDate(),
				);
				//print_r($reqResponse); die;
				$total_item_price = 0;
				foreach ($reqResponse['data']['product'] as $key => $productVal) {

					if (!empty($insert['ProductTitle'])) {
						$insert['ProductTitle'] .= ", ";
					}
					$insert['ProductTitle'] .= $productVal['product_name'];
					if (!empty($productVal['image'])) {

						if (file_exists(FILE_URL . 'temp/' . $productVal['image'])) {
							rename(FILE_URL . 'temp/' . $productVal['image'], FILE_URL . 'package/' . $productVal['image']);
							$insert['ProductList'][$key]['image'] = "package/{$productVal['image']}";
						}
					}

					$insert['ProcessingFees'] += $productVal['ProcessingFees'];
					$insert['AquantuoFees'] += $productVal['aq_fee'];

					$total_item_price += floatval(@$productVal['price']) * $productVal['qty'];
					// $insert['shippingCost'] += $productVal['shippingCost'];
					$insert['insurance'] += $productVal['insurance'];

					$insert["TotalCost"] += (floatval(@$productVal['price']) * $productVal['qty']) + $productVal['shipping_cost_by_user'] + $productVal['shippingCost'] + $productVal['insurance'];

					$insert['ProductList'][$key]['_id'] = (String) new MongoId();
					$insert['ProductList'][$key]['status'] = 'pending';
					$insert['ProductList'][$key]['verify_code'] = rand(1000, 9999);
					$insert['ProductList'][$key]['tpid'] = '';
					$insert['ProductList'][$key]['tpName'] = '';
					$insert['ProductList'][$key]['shippingCost'] = $productVal['shippingCost'];
					$insert['ProductList'][$key]['insurance'] = $productVal['insurance'];
					$insert['ProductList'][$key]['shipping_cost_by_user'] = floatval(@$productVal['shipping_cost_by_user']);
					$insert['ProductList'][$key]['aq_fee'] = @$productVal['aq_fee'];
					$insert['ProductList'][$key]['ProcessingFees'] = @$productVal['ProcessingFees'];
					$insert['ProductList'][$key]['total_cost'] = (floatval(@$productVal['price']) * $productVal['qty']) + $productVal['shipping_cost_by_user'] + $productVal['shippingCost'] + $productVal['insurance'];
					$insert['ProductList'][$key]['after_update'] = (floatval(@$productVal['price']) * $productVal['qty']) + $productVal['shipping_cost_by_user'] + $productVal['shippingCost'] + $productVal['insurance'];

					$insert['ProductList'][$key]['PaymentStatus'] = 'no';

					$insert['ProductList'][$key]['ExpectedDate'] = '';
					$insert['ProductList'][$key]['EnterOn'] = new MongoDate;

					//	$insert['ProductList'][$key]'total_cost'] = $productVal['shippingCost'] + $key['InsuranceCost'] + $fees,
					$insert['ProductList'][$key]['discount'] = 0;
					$insert['ProductList'][$key]['package_id'] = $PackageNumber . $key;
					$insert['ProductList'][$key]['txnid'] = '';
					$insert['ProductList'][$key]['TransporterFeedbcak'] = '';
					$insert['ProductList'][$key]['TransporterRating'] = 0;
					$insert['ProductList'][$key]['RequesterFeedbcak'] = '';
					$insert['ProductList'][$key]['RequesterRating'] = 0;
					$insert['ProductList'][$key]['RejectBy'] = '';
					$insert['ProductList'][$key]['ReturnType'] = '';
					$insert['ProductList'][$key]['ReceiptImage'] = '';
					$insert['ProductList'][$key]['RejectTime'] = '';
					$insert['ProductList'][$key]['TrackingNumber'] = '';
					$insert['ProductList'][$key]['TransporterMessage'] = '';
					$insert['ProductList'][$key]['CancelDate'] = '';
					$insert['ProductList'][$key]['StripeChargeId'] = '';
					$insert['ProductList'][$key]['DeliveredDate'] = '';
					$insert['ProductList'][$key]['inform_mail_sent'] = 'no';
				}

				$region_charge = resionCharges(['name' => $app->request->post('drop_off_state'), 'id' => $app->request->post('drop_off_state_id')]);

				$insert["discount"] = floatval($app->request->post('discount'));
				$insert["TotalCost"] = ($insert["TotalCost"] + $region_charge +$reqResponse['data']['DutyAndCustom']+$reqResponse['data']['Tax'] + $insert['ProcessingFees']) - $insert["discount"];

				$insert['AreaCharges'] = floatval($region_charge);
				
				$insert['DutyAndCustom'] = $reqResponse['data']['DutyAndCustom'];
				$insert['Tax']  = $reqResponse['data']['Tax'];
			
				$insert['BeforePurchaseTotalCost'] = $insert['TotalCost'];
				
				
				$insert['total_item_price'] = floatval($total_item_price);

				if (strlen($app->request->post('request_id')) == 24) {
					$res = $requestCollection->update(['_id' => new MongoId($app->request->post('request_id'))], $insert);
					$req_info = $requestCollection->find(array('_id' => new MongoId($app->request->post('request_id'))), array('PackageNumber'));
					if (@$res['updatedExisting'] > 0) {
						$req_info = $req_info->getNext();
						$response = [
							'success' => 1,
							'msg' => 'Your request has been updated successfully.',
							"packageId" => $app->request->post('request_id'),
							"TotalCost" => $insert["TotalCost"],
							"PackageNumber" => $req_info["PackageNumber"],
						];
					}

					$paymentInfo->insert([
						'request_id' => $app->request->post('request_id'),
						'user_id' => (string) $userId,
						'action_user_id' => (string) $userId,
						'RequestType' => 'buy_for_me',
						'action' => 'update_request',
						'item_id' => '',
						'item_unic_number' => '',
						'TotalCost' => floatval($insert['TotalCost']),
						'shippingCost' => floatval($insert['shippingCost']),
						'insurance' => floatval($insert['insurance']),
						'AreaCharges' => floatval($insert['AreaCharges']),
						'ProcessingFees' => floatval($insert['ProcessingFees']),
						'shipping_cost_by_user' => floatval($insert['shipping_cost_by_user']),
						'item_cost' => floatval($total_item_price),
						'discount' => floatval($insert['discount']),
						'PromoCode' => $insert['PromoCode'],
						'difference' => floatval($insert['after_update_difference']),
						'difference_before' => floatval($insert['after_update_difference']),
						'date' => New MongoDate(),
					]);
				} else {
					$insert["EnterOn"] = new MongoDate();
					$res = $requestCollection->insert($insert);
					if ($res['ok'] == 1) {

						/* Activity Log update by aakash  23-2-2018*/
						$insert['action_user_id'] = (string) $insert['RequesterId'];
						include_once ('notify.php');
						activityLog($insert);

						$response = [
							'success' => 1,
							'msg' => 'Your listing has been successfully created, please proceed to the payment page.',
							"packageId" => (string) $insert['_id'],
							'Status' => $insert['Status'],
							"TotalCost" => $insert["TotalCost"],
							"PackageNumber" => $insert["PackageNumber"],
							"note_html" => 'I understand that customs duty may be accessed on a value of my item other than what I provided.'
						];

						$paymentInfo->insert([
							'request_id' => (string) $insert['_id'],
							'user_id' => (string) $userId,
							'action_user_id' => (string) $userId,
							'RequestType' => 'buy_for_me',
							'action' => 'create',
							'item_id' => '',
							'item_unic_number' => '',
							'TotalCost' => floatval($insert['TotalCost']),
							'shippingCost' => floatval($insert['shippingCost']),
							'insurance' => floatval($insert['insurance']),
							'AreaCharges' => floatval($insert['AreaCharges']),
							'ProcessingFees' => floatval($insert['ProcessingFees']),
							'shipping_cost_by_user' => floatval($insert['shipping_cost_by_user']),
							'item_cost' => floatval($total_item_price),
							'discount' => floatval($insert['discount']),
							'PromoCode' => $insert['PromoCode'],
							'difference' => floatval($insert['after_update_difference']),
							'difference_before' => floatval($insert['after_update_difference']),
							'date' => New MongoDate(),
						]);

						//if ($insert['Status'] == 'ready') {
							// Email and notifcation section
							//require_once 'notify.php';
							//bfm_send_notification_to_admin($insert);
							//bfm_send_email_to_requester($insert, $userinfo);
							//bfm_send_email_to_admin($insert);
						//}

					}
				}

			}

		}
	}

	echo echoRespnse(200, $response);

});

function get_in_miles($meter) {
	return $meter * 0.000621371;
}

$app->get('/near-by-transporter', function () use ($app) {
	global $db;
	global $userId;
	$collection = $db->trips;
	$usercollection = $db->users;

	$where = array(
		'SourceDate', '>=', new MongoDate,
	);

	$field = array('_id', 'TransporterId');

	//$tripData = $collection->find($where, $field)->groupBy('TransporterId')->distinct();
	$tripData = $collection->find($where, $field)->get();
	print_r($tripData);

});

$app->run();
