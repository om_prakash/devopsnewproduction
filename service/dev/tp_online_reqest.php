<?php
/**
 * Step 1: Require the Slim Framework
 *
 * If you are not using Composer, you need to require the
 * Slim Framework and register its PSR-0 autoloader.
 *
 * If you are using Composer, you can skip this step.
 * 
 * Filename Name : tp_online_requet.php
 * File Path : services/authentication.php
 * Description : This file contains method related to userinformation. 
 * Author: Ravi shukla
 * Created Date : 19-09-2016
 * Library : Email,DbConnect
 * 
 * */
 
require 'Slim/Slim.php';
require 'Slim/config/DbConnect.php';
require 'Slim/library/Common.php';

\Slim\Slim::registerAutoloader();

/**
 * Step 2: Instantiate a Slim application
 *
 * This example instantiates a Slim application using
 * its default settings. However, you will usually configure
 * your Slim application now by passing an associative array
 * of setting names and values into the application constructor.
 */
$app = new \Slim\Slim();

$userId = '';
$userType = '';
  
/* 
 * Function Name : my_delivery_area
 * Description 	 : This is used to insert my area(country, state, cities) of individual transporter.
 * Url 			 : http://192.168.11.101/aq/services/transporter.php/my_delivery_area
 * Method 		 : POST
 * Header 		 : apiKey, userType
 * Parameter     : deliveryAreaCountrydeliveryAreaState,deliveryAreaCities.
 * Created By    : Pankaj Gawande
 * Create Date 	 : 1-12-2015 
 * 
 */

$app->post('/my_delivery_area','transporter_auth', function() use($app){
	global $db;
	global $userId;

	$req = new Request();
	if($req->update_online_request())
	{

	}

	verifyRequiredParams(array('deliveryAreaCountry'));
	$response 		= array('success'=>0, "msg"=>'Records not Updated');
	$collection 	= $db->users;
	 
	$requestUpdate 	= $collection->update(array('_id'=>$userId), array('$set'=>$updateData));
	 if($requestUpdate['updatedExisting'] > 0){
	 	$response = array('success'=>1, "msg"=>'Records Updated');
	 }
	 echoRespnse(200, $response);
});
 

$app->run();

