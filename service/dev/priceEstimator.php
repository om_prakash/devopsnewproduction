<?php
require 'Slim/Slim.php';
require 'Slim/config/DbConnect.php';
require 'vendor/mailer/Email.php';
require 'Slim/library/Common.php';

\Slim\Slim::registerAutoloader();

/**
 * Step 2: Instantiate a Slim application
 *
 * This example instantiates a Slim application using
 * its default settings. However, you will usually configure
 * your Slim application now by passing an associative array
 * of setting names and values into the application constructor.
 */
$app = new \Slim\Slim();

$userId = '';
$userType = '';

    $app->get('/suggested_weights', function () use ($app) {
        $response = array('success' => 0, 'msg' => '');
        verifyRequiredParams(array('type'));
        global $db;

        $collection = $db->item;

        /*if($app->request->get('type') == 'list'){
            $info = $collection->find(array('Status' => 'Active'),['Status','item_name','kgWeight','lbsWeight'])->limit(5);
        }else{

        }*/

        $info = $collection->find(array('Status' => 'Active'),['Status','item_name','kgWeight','lbsWeight']);
        
        foreach ($info as $key ) {
            $array[]=[
            'Status'=>@$key['Status'],
            'item_name'=>@$key['item_name'],
            'kgWeight'=> number_format( @$key['kgWeight'],2),
            'lbsWeight'=> number_format( @$key['lbsWeight'],2),
            ];
        }
        $response = array('success' => 1, 'result' => $array);
        echoRespnse(200, $response);
    });



    $app->post('/price_estimator_calculation', function () use ($app) {
    	$response = array('success' => 0, 'msg' => '','result'=>[]);
        global $db;
        $collection = $db->configuration;
        $outSsideAccraRate = $collection->find(array('_id'=> new MongoId('5673e33e6734c4f874685c84')),['accra_charge','accra_charge_type']);
        $outSsideAccraRate = $outSsideAccraRate->getNext();
        $category_collection = $db->category;
        $distance_collection = $db->distance;



        verifyRequiredParams(array('travelMode','request_type','drop_lat','drop_long','category_id','category_name','state_id'));
        $destination_address=['lat'=>$app->request->post('drop_lat'),'lng'=>$app->request->post('drop_long')];
        
        $aqlat = 39.2625832;
		$aqlong = -75.60483959999999;

        if(in_array($app->request->post('request_type'), ['online','buyforme'])){
        	$distance = getDistanceBetweenPointsNew($aqlat, $aqlong, $app->request->post('drop_lat'), $app->request->post('drop_long'));
        	include_once ('Slim/library/ReqHelper.php');
            $weight = $app->request->post('weight');
            $weight_unit = $app->request->post('weight_unit'); 
            //print_r($distance); die;
            $city = json_decode($app->request->post('city')) ; $state = json_decode($app->request->post('state')); $country = json_decode(  $app->request->post('country'));
            $data = [
                'shippingcost' => 0,
                'insurance' => 0,
                'ghana_total_amount' => 0,
                'total_amount' => 0,
                'error' => [],
                'formated_text' => '',
                'distance_in_mile' => 0,
                'product_count' => 0,
                'ProcessingFees' => 0,
                'item_cost' => 0,
                'shipping_cost_by_user' => 0,
                'distance' => $distance,
                'type' => $app->request->post('request_type'),
                'pakage_type' => $app->request->post('request_type'),
                'consolidate_check'=> ($app->request->post('consolidate_item') != '')? $app->request->post('consolidate_item'):'off', 
            ];

            if ($app->request->post('request_type') == 'buyforme') {
                $data['item_cost'] = $app->request->post('product_cost');
                $data['type'] = 'buyforme';
                $data['pakage_type'] = 'buyforme';
            }



            $product[] = [
                'request_type'=>$app->request->post('request_type'),
                'insurance_status'=>"no",
                'travelMode'=> $app->request->post('travelMode'),
                'qty'=>1,
                'categoryid'=> $app->request->post('category_id'),
                'category'=> $app->request->post('category_name'),
                "weight_unit" => $weight_unit, 
                "length"=>1, 
                "width"=>$weight, 
                "height" =>1, 
                "weight" =>  ($weight == '')? 0:$weight, 
                'shipping_cost_by_user'=> 0,
                'price'=>($app->request->post('request_type') == 'buyforme')? $app->request->post('price'):1,


            ];
            $userinfo=['Default_Currency' => 'USD'];
            $reqHelper = new reqHelper();
            $reqResponse = $reqHelper->getRate($product, $data, $userinfo);
            
            if ($reqResponse['success'] == 1) {

                //Resion Charges
                $response['resionCharges'] = resionCharges(['name' =>'','id'=>$app->request->post('state_id')]);
                $reqResponse['total_amount'] = $response['resionCharges'] + $reqResponse['total_amount'];

                // if($app->request->post('request_type') == 'buyforme'){
                //     $reqResponse['total_amount'] = $reqResponse['total_amount'] + $reqResponse['data']['ProcessingFees'];
                // }

                

                $currency_conversion = currency_conversion($reqResponse['total_amount']);
                $response['success'] = 1;
                $response['msg'] = "Calculation";

                $response['result']['total_amount'] = floatval($reqResponse['total_amount']) ;
                $response['result']['ghana_total_amount'] = $currency_conversion['ghanian_cost'];
                $response['result']['canadian_cost'] = $currency_conversion['canadian_cost'];
                $response['result']['philipins_cost'] = $currency_conversion['philipins_cost'];
                $response['result']['uk_cost'] = $currency_conversion['uk_cost'];
                $response['result']['kenya_cost'] = $currency_conversion['kenya_cost'];
                $response['result']['total_weight'] = floatval($reqResponse['total_weight']) ;

                $response['result']['item_cost'] = floatval($data['item_cost']);
                $response['result']['fees'] = $response['result']['total_amount'] - $data['item_cost'];
                $response['result']['fees'] = floatval($response['result']['ProcessingFees']);

                if($app->request->post('travelMode') == 'ship'){
                    $response['result']['total_weight'] = floatval(get_weight($app->request->post('weight') ,$weight_unit));
                }
                $response['result']['total_distance'] = ($distance > 0)? $distance /1000:$distance;
                $response['result']['total_distance']=floatval($response['result']['total_distance']);
            }else{
                $response['msg'] = $reqResponse['msg'];
            }
        }else if(in_array($app->request->post('request_type'), ['delivery'])){

        	if($app->request->post('pick_lat') == '' | $app->request->post('pick_long') == ''){
        		$response['msg']="pick_lat and pick_long is required.";
        		echoRespnse(200, $response); die;
        	}

        	$distance = getDistanceBetweenPointsNew($app->request->post('pick_lat'), $app->request->post('pick_long'), $app->request->post('drop_lat'), $app->request->post('drop_long'));


            $lhwunit = 'cm';
            $weightunit = 'kg';
            if ($app->request->post('measurement_unit') == 'inches_lbs') {
                $lhwunit = 'inches';
                $weightunit = 'lbs';
            }
            $category = json_decode($app->request->post('category'));
            //$distance = (float) $app->request->post('distance');
            include_once 'Slim/library/Requesthelper.php';
            $requesthelper = new Requesthelper(
                [
                    "needInsurance" => ($app->request->post('pe_insurance') == 'yes') ? true : false,
                    "productQty" => ($app->request->post('productQty') < 1) ? 1 : Input::get('productQty'),
                    "productCost" => $app->request->post('product_cost'),
                    "productWidth" => $app->request->post('width') ,
                    "productWidthUnit" => $lhwunit,
                    "productHeight" => $app->request->post('height'),
                    "productHeightUnit" => $lhwunit,
                    "productLength" => $app->request->post('length'),
                    "productLengthUnit" => $lhwunit,
                    "productWeight" => $app->request->post('weight'),
                    "productWeightUnit" => $weightunit,
                    "productCategory" => $app->request->post('category_name'),
                    "productCategoryId" => $app->request->post('category_id'),
                    "distance" => $distance,
                    "travelMode" =>  ($app->request->post('request_type') == 'local_ghana')? "ship": $app->request->post('travelMode'),
                    "currency" => 'GHS',
                    'consolidate_check'=> ($app->request->post('consolidate_item') != '')? $app->request->post('consolidate_item'):'off',
                ]
            );

            $requesthelper->calculate();
            $calculationinfo = $requesthelper->get_information();
           

            if (!isset($calculationinfo->error)) {
                $response['success'] = 1;
                $response['total_amount'] = $calculationinfo->shippingcost + $calculationinfo->insurance;

                
                
                //Resion Charges
                $response['resionCharges'] = resionCharges(['name' =>'','id'=>$app->request->post('state_id')]);
                $response['total_amount'] = $response['resionCharges'] + $response['total_amount'];

                $currency_conversion = currency_conversion($response['total_amount']);
                $response['msg'] = "Calculation";
                $response['result']['total_amount']= floatval($response['total_amount']);
                $response['result']['total_weight'] = floatval(get_weight($app->request->post('weight') ,$weightunit));
                $response['result']['total_distance'] = floatval(($distance > 0)? $distance /1000:$distance);
                $response['result']['ghana_total_amount'] = $currency_conversion['ghanian_cost'];
                $response['result']['canadian_cost'] = $currency_conversion['canadian_cost'];
                $response['result']['philipins_cost'] = $currency_conversion['philipins_cost'];
                $response['result']['uk_cost'] = $currency_conversion['uk_cost'];
                $response['result']['kenya_cost'] = $currency_conversion['kenya_cost'];
            }else{
                $response['msg'] = $calculationinfo->error;
            }
        }else if(in_array($app->request->post('request_type'), ['local_ghana'])){

            if($app->request->post('pick_lat') == '' | $app->request->post('pick_long') == ''){
                $response['msg']="pick_lat and pick_long is required.";
                echoRespnse(200, $response); die;
            }

            $distance = getDistanceBetweenPointsNew($app->request->post('pick_lat'), $app->request->post('pick_long'), $app->request->post('drop_lat'), $app->request->post('drop_long'));



            $lhwunit = 'cm';
            $weightunit = 'kg';
            if ($app->request->post('measurement_unit') == 'inches_lbs') {
                $lhwunit = 'inches';
                $weightunit = 'lbs';
            }

            if($app->request->post('weight_unit') != 'kg'){
                $weightunit = $app->request->post('weight_unit');
            }
            

            if($app->request->post('category_id')){
                $match_category = $category_collection->find(
                    [
                        '_id'=> new MongoId($app->request->post('category_id')),
                        'Status'=> 'Active',
                        'type'=>'localCategory',
                        'country_id'=> $app->request->post('country_id')
                    ],
                    ['price']
                );
                

                if($match_category->count() > 0){
                    $match_category = $match_category->getNext();
                    $response['success']=1;
                    $response['category_price'] = $match_category['price'];
                }else{
                    $response = ['success' => 0, 'msg' => 'The shipping category you have specified is unsupported.'];
                    echoRespnse(200, $response); die;
                    
                }
            }


            //Item value calculation
            if($app->request->post('product_cost') != '' && $response['success'] == 1){

                $match_value = $distance_collection->find(
                    array(  
                        'from'=>['$lte'=> (float) $app->request->post('product_cost')],
                        'to'=>['$gte'=> (float) $app->request->post('product_cost')],
                        'Status'=> 'Active',
                        'type'=>'item-value',
                        'country_id'=> $app->request->post('country_id')
                    ),
                    array( 'price')
                );

                
                if($match_value->count() > 0){
                    $match_value = $match_value->getNext();
                    $response['success']=1;
                    $response['item_price']= $match_value['price'];
                    $response['item_price']= $match_value['price'];

                }else{
                    $response = ['success' => 0, 'msg' => 'The item value you have specified is unsupported.'];
                    echoRespnse(200, $response); die;

                }
            }

            if($distance > 0 != '' && $response['success'] == 1){
                //Distance in to miles
                $distance = $distance * 0.000621371;
                //print_r($distance); die;
                $match_distance = $distance_collection->find(
                    array(  
                        'from'=>['$lte'=> (float) $distance],
                        'to'=>['$gte'=> (float) $distance],
                        'Status'=> 'Active',
                        'type'=>'distance',
                        'country_id'=> $app->request->post('country_id')
                    ),
                    array( 'price')
                );

                if($match_distance->count() > 0){
                    $match_distance = $match_distance->getNext();
                    $response['success']=1;
                    $response['distance_price'] = $match_distance['price'];
                }else{
                    $response = ['success' => 0, 'msg' => 'The shipping address/distance you have specified is unsupported.'];
                    echoRespnse(200, $response); die;
                } 
            }

            if($response['success'] == 1){
                $response['total_amount'] = $response['distance_price'] + $response['category_price']+$response['item_price'];
                //Resion Charges
                $response['resionCharges'] = resionCharges(['name' =>'','id'=>$app->request->post('state_id')]);
                //print_r($app->request->post('state_id')); die;
                $response['total_amount'] = $response['resionCharges'] + $response['total_amount'];

                //print_r($app->request->post('state_id')); die;
                //
                $currency_conversion = currency_conversion($response['total_amount']);
                $response['msg'] = "Calculation";
                $response['result']['total_amount']= floatval($response['total_amount']);
                $response['result']['total_weight'] = floatval(get_weight($app->request->post('weight') ,$weightunit));
                //print_r($weightunit); die;
                $response['result']['total_distance'] = floatval(($distance > 0)? $distance /1000:$distance);
                $response['result']['ghana_total_amount'] = $currency_conversion['ghanian_cost'];
                $response['result']['canadian_cost'] = $currency_conversion['canadian_cost'];
                $response['result']['philipins_cost'] = $currency_conversion['philipins_cost'];
                $response['result']['uk_cost'] = $currency_conversion['uk_cost'];
                $response['result']['kenya_cost'] = $currency_conversion['kenya_cost'];
            }

        }
        echoRespnse(200, $response); die;

    });

    function get_weight($weight, $type)
    {
        $type = strtolower($type);
        if ($type == 'kg') {
            return (((float) $weight) * 2.20462);
        }else {
            return $weight;
        }
    }

    function resionCharges($state) {
        global $db;
        $collection = $db->extraregion;
        $charges = $collection->find(array('state_id'=> $state['id'] ,'status'=>'Active'),['amount']);
        
        if($charges->count() > 0){
            $charges = $charges->getNext();
            return $charges['amount'];
        }else{
            return 0.00;
        }
    }

    function currency_conversion($shipcost){
        $data = ['cost'=>0,'ghanian_cost'=>0,'philipins_cost'=>0,'uk_cost'=>0,'canadian_cost'=>0,'kenya_cost'=>0];
        global $db;
        $collection = $db->currency;
        $currency = $collection->find(array('Status' => 'Active'),['CurrencyRate','FormatedText','CurrencyCode']);

        if($currency){
            foreach ($currency as $key) {
                if($key['CurrencyCode'] == 'GHS'){
                    $data['ghanian_cost'] = str_replace('[AMT]', number_format($shipcost * $key['CurrencyRate'], 2),$key['FormatedText']);
                }else if($key['CurrencyCode'] == 'CAD'){
                    $data['canadian_cost'] = str_replace('[AMT]', number_format($shipcost * $key['CurrencyRate'], 2), $key['FormatedText']);
                }else if($key['CurrencyCode'] == 'PHP'){
                    $data['philipins_cost'] = str_replace('[AMT]', number_format($shipcost * $key['CurrencyRate'], 2), $key['FormatedText']);
                }else if($key['CurrencyCode'] == 'GBP'){
                    $data['uk_cost'] = str_replace('[AMT]', number_format($shipcost * $key['CurrencyRate'], 2), $key['FormatedText']);
                }else if($key['CurrencyCode'] == 'KES'){
                    $data['kenya_cost'] = str_replace('[AMT]', number_format($shipcost * $key['CurrencyRate'], 2), $key['FormatedText']);
                }
            }

        }
        return $data;
    }

    function getDistanceBetweenPointsNew($latitude1, $longitude1, $latitude2, $longitude2, $unit = 'Meter')
    {
        $latitude1 = floatval($latitude1);
        $longitude1 = floatval($longitude1);
        $latitude2 = floatval($latitude2);
        $longitude2 = floatval($longitude2);

        $theta = $longitude1 - $longitude2;
        $distance = (sin(deg2rad($latitude1)) * sin(deg2rad($latitude2))) + (cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * cos(deg2rad($theta)));
        $distance = acos($distance);
        $distance = rad2deg($distance);
        $distance = $distance * 60 * 1.1515;switch ($unit) {
            case 'Mi':break;case 'Km':$distance = $distance * 1.609344;
                break;case 'Meter':$distance = $distance * 1609.34;
                break;
        }
        return (round($distance, 2));
    }

    function outsideOfAccraCharge($address)
    {
        $distance = getDistanceBetweenPointsNew(5.5913754, -0.2499413, $address['lat'], $address['lng']);
        $charge = false;
        if ($distance != false && $distance > 0) {
            $distance_km = $distance/1000;
            $range = 30;
            if($distance_km > 30){
                $charge = true;
            }
        }
        return $charge;
    }

    

$app->run();