<?php
require 'Slim/config/DbConnect.php';

$baseURL = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "https");
$host = $baseURL .= "://" . $_SERVER['HTTP_HOST'];
$baseURL .= str_replace(basename($_SERVER['SCRIPT_NAME']), "", $_SERVER['SCRIPT_NAME']);

$currencycoll = $db->city_state_country;

$currencyobj = $currencycoll->find(['type' => 'Country'], ['Content', 'CurrencyCode', 'CurrencySymbol','CurrencyRate']);
$msg = '';
$configuration = $db->configuration;
if ($configuration->count() > 0) {

    $admin_msg = $configuration->find(['_id' => new MongoId('5673e33e6734c4f874685c84')]);
    $admin_msg = (Object) $admin_msg->getNext();
    $msg = $admin_msg->message;
}

if($host == 'https://192.168.1.101'){
    $newurl=$host.'/aq/dev3';
}else{
    $newurl=$host;
}
$settingColl = $db->setting;
$admin_msg2 = $settingColl->find(['_id' => new MongoId('5c57fefccab743651e132659')]);
$admin_msg2 = (Object) $admin_msg2->getNext();


$array = array(
    "success" => 1,
    "msg" => "this is base url.",
    "baseurl" => "https://aquantuo.com/service/dev/",
    "ipayurl" => "https://aquantuo.com/api/retrievePayment",
    "test" => $baseURL,
    "currency" => '$',
    "nodeBaseurl" => "https://aquantuo.com:8080/",
    "ImageUrl" => "$host/upload/",
    "ProductUrl" => "$host/upload/",
    //"ProductUrl" => "http://192.168.1.101/aq/dev3/upload/",
    //'laravel_api'=>"$newurl/api/v1/",
    'laravel_api'=>"https://aquantuo.com/api/v9/",

    "UpdateApp" => "No",
    "UpdateAppMessage" => "A new version of Aquantuo App is availabe. Please upgrade to new version of app. We no longer support old apps so please upgrade it now.",
    "UpdateAppOptional" => "No",
    "UpdateAppMessageOptional" => "A new version of Aquantuo App is availabe please upgrade to new version to enjoy new features of application.",
    "PlayStoreUrl" => "https://play.google.com/store/apps/details?id=com.it.aquantuo&hl=en",
    "IosUrl" => "https://itunes.apple.com/us/app/aquantuo/id1067797285?ls=1&mt=8",
    "Subject" => "Invitation to Aquantuo",
    "InviteText" => "Hi,\n\nI came across Aquantuo and thought you might be interested.\n\nIt’s a peer-to-peer delivery platform where travelers get paid for using the spare space in their luggage to deliver an item from the community of users when they travel internationally.\n\nWith this app, you can also list items you want to send to family and friends and someone will help get it there.\n\nUsers can also shop online from their favorite online stores and a verified Transporter will help deliver it.\n\nIf you are a business that ships packages across the globe, this app will help you find clients for your business as well.\n\nHope you check it out!",

    'online_market' => [
        [
            'id' => 'amazon',
            'name' => 'Amazon',
            'url' => 'https://www.amazon.com/',
        ],
        [
            'id' => 'ebay',
            'name' => 'eBay',
            'url' => 'http://www.ebay.com/',
        ],
        [
            'id' => 'walmart',
            'name' => 'Walmart',
            'url' => 'https://www.walmart.com/',
        ],
        [
            'id' => 'asos',
            'name' => 'Asos',
            'url' => 'https://my.asos.com',
        ],
        [
            'id' => 'bestbuy',
            'name' => 'Bestbuy',
            'url' => 'http://www.bestbuy.com/',
        ],
        [
            'id' => 'macys',
            'name' => 'macys',
            'url' => 'http://www1.macys.com/',
        ],
    ],
    "currency_list" => [],
    "admin_msg" => $msg,

    "pay_now_msg"=>'If actual weight wasn’t provided, you may be billed or refunded any differences in price or shipping upon review.',
    "package_type"=>[
        "online"=>"Online Purchase",
        "buy_for_me"=>"Buy For Me",
        "send_package"=>"Send A Package",
    ],
    //"showPopup"=>'yes',
    //"popupContent"=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut dapibus dolor libero, a consequat massa vulputate at. Fusce arcu felis, gravida eget venenatis nec, interdum quis orci. Donec arcu mauris, scelerisque eget venenatis vitae, iaculis eget orci. Cras auctor iaculis felis in sagittis. Integer felis ante, ullamcorper sit amet neque id, suscipit faucibus quam. Donec accumsan, metus nec dignissim fringilla, orci mi molestie urna, sed ornare lectus purus id orci. Ut laoreet, sapien ullamcorper venenatis cursus, nisl turpis auctor lorem, id condimentum nisi tortor vitae nulla. Ut at felis varius, condimentum nibh vitae, varius nibh. Nunc nec orci quis ipsum tempus placerat vel euismod diam. Ut et suscipit nulla. Suspendisse ornare finibus molestie. Sed eu sem sed lectus scelerisque auctor. Duis eleifend sodales elit, a molestie turpis luctus vel. Maecenas ac faucibus sem. Morbi lacinia pharetra arcu ac luctus.",
    'showPopup'=>($admin_msg2->status == 'on')?'yes':'no',
    'popupContent'=> $admin_msg2->content,
);
if ($currencyobj->count() > 0) {
    foreach ($currencyobj as $key) {

        $array['currency_list'][] = [
            "id" => $key['CurrencyCode'],
            'name' => $key['Content'] . " - " . $key['CurrencySymbol'],
            'CurrencyRate' => $key['CurrencyRate'],
        ];
    }
}

header('Content-type: application/json');
echo json_encode($array);
