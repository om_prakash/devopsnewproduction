<?php
/**
 * Step 1: Require the Slim Framework
 *
 * If you are not using Composer, you need to require the
 * Slim Framework and register its PSR-0 autoloader.
 *
 * If you are using Composer, you can skip this step.
 *
 * Filename Name : authentication.php
 * File Path : services/authentication.php
 * Description : This file contains method related to userinformation.
 * Author: Ravi shukla
 * Created Date : 22-08-2015
 * Library : Email,DbConnect
 *
 * */

require 'Slim/Slim.php';
require 'Slim/config/DbConnect.php';
require_once 'vendor/stripe/init.php';
require 'Slim/library/Common.php';

\Slim\Slim::registerAutoloader();

/**
 * Step 2: Instantiate a Slim application
 *
 * This example instantiates a Slim application using
 * its default settings. However, you will usually configure
 * your Slim application now by passing an associative array
 * of setting names and values into the application constructor.
 */
$app = new \Slim\Slim();

$userId = '';
$userType = '';

/*
 * Function Name :  made_payment
 * Description : made_payment
 * Url : http://192.168.11.101/aq/services/stripe.php/made_payment
 * Method : POST
 * Created By : Ravi shukla
 * Create Date : 25-08-2015
 *
 * */

$app->post('/made_payment', 'authenticate', function () use ($app) {

	require_once 'vendor/mailer/Email.php';

	verifyRequiredParams(array('creditCardId', 'packageId'));

	global $userId;
	global $db;
	global $userInformation;

	$localarray = array();

	$response = array('success' => 0, 'msg' => 'Your Request for delivery is not successfully placed.');
	$collection = $db->delivery_request;
	$UCollection = $db->users;
	$sysCollection = $db->system_setting;
	$settingcoll = $db->setting;
	$collTransaction = $db->transaction;
	$coll = $db->notification;
	if (strlen($app->request->post('packageId')) == 24) {
		$auth = $collection->find(array('_id' => new MongoId($app->request->post('packageId'))));
		$uInfo = $UCollection->find(array('_id' => $userId), array('StripeId', 'Email', 'Name', 'UserImage', 'NotificationId', 'DeviceType', 'Default_Currency'));
		if ($auth->count() > 0 && $uInfo->count() > 0) {
			$auth = $auth->getNext();
			$uInfo = $uInfo->getNext();

			if (!empty(trim($app->request->post('promocode')))) {
				$res = get_discount_amt($app->request->post('promocode'),
					$auth['ShippingCost'],
					$auth['ShippingCost'],
					$uInfo->Default_Currency
				);
				$auth['discount'] = 0;
				if ($res['success'] == 1) {
					$auth['discount'] = $res['discount'];
					$auth['TotalCost'] = $auth['TotalCost'] - $res['discount'];
				}
			}

			try {
				if (!empty(trim($auth['StripeChargeId']))) {
					\Stripe\Refund::create(array(
						"charge" => $auth['StripeChargeId'],
					));
				}
				$StripeResponse = \Stripe\Charge::create(array(
					"amount" => (int) ($auth['TotalCost'] * 100),
					"card" => $app->request->post('creditCardId'),
					"customer" => $uInfo['StripeId'],
					"currency" => "usd",
					"capture" => false,
					'metadata'=>[
						'customer_name'=> @$uInfo['Name'],
						'customer_email'=> @$uInfo['Email'],
						'action'=>'user'
					],
					'description'=> @$uInfo['Name'],
				));

				if (isset($StripeResponse->status) && @$StripeResponse->status == 'succeeded') {
					$collection->update(array('_id' => new MongoId($app->request->post('packageId'))), array('$set' => array('PaymentStatus' => 'capture', 'Status' => 'ready', 'StripeChargeId' => $StripeResponse->id, 'PaymentDate' => new MongoDate())));

					$response['success'] = 1;
					$response['msg'] = 'Your item "' . $auth["ProductTitle"] . '" has been successfully posted. Once a transporter reviews and accepts it, you will be notified. You may also check the list of available transporters close to your current location.';

					// Insert Transaction information
					$collTransaction->insert(array(
						"SendById" => $auth["_id"],
						"SendToId" => "aquantuo",
						"SendByName" => @$auth['RequesterName'],
						"RecieveByName" => "Aquantuo",
						"Description" => "Amount deposited against delivery request for {$auth['ProductTitle']}, PackageId: {$auth['PackageNumber']} from {$auth['PickupFullAddress']} to {$auth['DeliveryFullAddress']}",
						"Credit" => floatval($auth['TotalCost']),
						"Debit" => "",
						"Status" => "pending",
						"TransactionType" => "delivery_request",
						'request_id' => $app->request->post('packageId'),
						'item_id' => '',
						"EnterOn" => new MongoDate(),
					));

					// Email to requester for information
					$ETemplate = array(
						"to" => $userInformation->Email,
						"replace" => array(
							"[USERNAME]" => $userInformation->Name,
							"[PACKAGETITLE]" => $auth['ProductTitle'],
							"[SOURCE]" => $auth['PickupFullAddress'],
							"[DESTINATION]" => $auth['DeliveryFullAddress'],
						),
					);
					if ($app->request->post('requesttype') == 'old') {
						send_mail('5877454f7ac6f6da198b456a', $ETemplate);
					} else {
						send_mail('5694a10f5509251cd67773eb', $ETemplate);

						// Notification to admin
						$coll->insert(array(
							'NotificationTitle' => "New request of Send a package",
							'NotificationMessage' => sprintf('The send a package request titled:"%s", ID:%s has been created.',
								$auth['ProductTitle'], $auth['PackageNumber']),
							'NotificationType' => 1,
							'NotificationReadStatus' => 0,
							'Date' => new MongoDate(),
							'GroupTo' => 'Admin',
						));

					}

					if ($auth['TripId'] != '') {
						$transporterinfomration = $UCollection->find(array('_id' => $auth['TransporterId']), array('EmailStatus', 'Email', 'Name', 'NotificationId', 'DeviceType', 'NoficationStatus', 'TPSetting'));

						if ($transporterinfomration->count() > 0) {
							$transporterinfomration = (Object) $transporterinfomration->getNext();

							// End of email

							if ($transporterinfomration->TPSetting == 'on' && $transporterinfomration->EmailStatus == 'on') {
								$ETemplate = array(
									"to" => $transporterinfomration->Email,
									"replace" => array(
										"[USERNAME]" => $transporterinfomration->Name,
										"[PACKAGEID]" => $auth['PackageNumber'],
										"[PACKAGETITLE]" => $auth['ProductTitle'],
										"[SOURCE]" => $auth['PickupFullAddress'],
										"[DESTINATION]" => $auth['DeliveryFullAddress'],
									),
								);

								if ($app->request->post('requesttype') == 'old') {
									send_mail('569f82bc5509251cd67773f2', $ETemplate);

								} else {

									send_mail('569e16b65509251cd67773f1', $ETemplate);
								}

							}
							if ($transporterinfomration->NoficationStatus == 'on') {
								require_once 'Slim/library/Requestnotification.php';

								$pushnoti = new Notification();
								if ($app->request->post('requesttype') == 'old') {
									$pushnoti->setValue('title', "Trip Request Updated");
									$pushnoti->setValue('message', "\"{$auth['ProductTitle']}\" is updated by user.");

								} else {
									$pushnoti->setValue('title', "One Trip Request");
									$pushnoti->setValue('message', "Requester:\"{$auth['RequesterName']}\" is create a request for trip.");
								}

								$pushnoti->setValue('location', 'transporter_delivery_detail');
								$pushnoti->setValue('locationkey', $app->request->post('packageId'));
								$pushnoti->add_user($transporterinfomration->NotificationId, $transporterinfomration->DeviceType);

								$pushnoti->fire();
							}

							$insNotification = array(
								'NotificationType' => "Prepared new request",
								'NotificationUserId' => array($transporterinfomration->_id),
								'Date' => new MongoDate(),
								'GroupTo' => 'User',
							);
							if ($app->request->post('requesttype') == 'old') {
								$insNotification['NotificationTitle'] = "Trip Request Updated";
								$insNotification['NotificationMessage'] = "Requester:\"{$auth['RequesterName']}\" is updated their request. Which is posted for you.";
							} else {
								$insNotification['NotificationTitle'] = "One Trip Request";
								$insNotification['NotificationMessage'] = "Requester:\"{$auth['RequesterName']}\" is create a request for trip.";
							}

							// Notification Section
							if (isset($insNotification)) {
								$coll->insert($insNotification);
							}

						}

					}
					// Email to admin

					$setting = $settingcoll->find(array('_id' => new MongoId('563b0e31e4b03271a097e1ca')));
					if ($setting->count() > 0) {
						$setting = (Object) $setting->getNext();
						$adminETemplate = array(
							"to" => $setting->SupportEmail,
							"replace" => array(
								"[PACKAGEID]" => $auth['PackageNumber'],
								"[PACKAGETITLE]" => $auth['ProductTitle'],
								"[SOURCE]" => $auth['PickupFullAddress'],
								"[DESTINATION]" => $auth['DeliveryFullAddress'],
								"[USERNAME]" => @$auth['RequesterName'],
							),
						);
						if ($app->request->post('requesttype') == 'old') {
							send_mail('58773d8f7ac6f6d5188b4567', $adminETemplate);
						} else {
							send_mail('56cd3e1f5509251cd677740c', $adminETemplate);
						}
					}
					// End email to admin

					if ($app->request->post('requesttype') != 'old') {
						// Send notification to all transporter
						require_once 'Slim/library/Requestnotification.php';
						try {
							carriernotication($app->request->post('packageId'));
						} catch (exception $e) {}
					}

				}
			} catch (Exception $e) {
				$response['msg'] = $e->getMessage();
			}
		}
	}
	echoRespnse(200, $response);

});

$app->post('/made_payment_new_send_package', 'authenticate', function () use ($app) {

	require_once 'vendor/mailer/Email.php';

	verifyRequiredParams(array('creditCardId', 'packageId'));

	global $userId;
	global $db;
	global $userInformation;

	$localarray = array();

	$response = array('success' => 0, 'msg' => 'Your Request for delivery is not successfully placed.');
	$collection = $db->delivery_request;
	$UCollection = $db->users;
	$sysCollection = $db->system_setting;
	$settingcoll = $db->setting;
	$collTransaction = $db->transaction;
	$coll = $db->notification;
	$cardcoll = $db->request_cards;
	$sendmail_col = $db->sendmail;

	if (strlen($app->request->post('packageId')) == 24) {
		$auth = $collection->find(array('_id' => new MongoId($app->request->post('packageId'))));
		$uInfo = $UCollection->find(array('_id' => $userId), array('StripeId', 'Email', 'Name', 'UserImage', 'NotificationId', 'DeviceType', 'Default_Currency', 'ProductList'));
		if ($auth->count() > 0 && $uInfo->count() > 0) {

			$auth = $auth->getNext();
			$uInfo = $uInfo->getNext();
			$status = 'ready';
			$tp = $tpname = '';
			if ($auth['TripId'] != '') {
				$status = 'accepted';
				$tp = (String) $auth['TransporterId'];
				$tpname = $auth['TransporterName'];
			}

			try {
				if (!empty(trim($auth['StripeChargeId']))) {
					\Stripe\Refund::create(array(
						"charge" => $auth['StripeChargeId'],
					));
				}

				$payable = $auth['TotalCost'];
				$StripeResponse = \Stripe\Charge::create(array(
					"amount" => (int) ($payable * 100),
					"card" => $app->request->post('creditCardId'),
					"customer" => $uInfo['StripeId'],
					"currency" => "usd",
					"capture" => true,
					'metadata'=>[
						'customer_name'=> @$uInfo['Name'],
						'customer_email'=> @$uInfo['Email'],
						'action'=>'user'
					],
					'description'=> @$uInfo['Name'],
				));

				if (isset($StripeResponse->status) && @$StripeResponse->status == 'succeeded') {

					if (isset($auth['ProductList'])) {
						$p_array = $auth['ProductList'];
						foreach ($p_array as $key => $value) {
							$p_array[$key]['status'] = $status;
							$p_array[$key]['PaymentStatus'] = 'yes';
							$p_array[$key]['tpid'] = $tp;
							$p_array[$key]['tpName'] = $tpname;

						}
						//$update['ProductList'] = $p_array;
					}
					$collection->update(array('_id' => new MongoId($app->request->post('packageId'))), array('$set' => array('PaymentStatus' => 'capture', 'Status' => $status, 'StripeChargeId' => $StripeResponse->id, 'PaymentDate' => new MongoDate(),
						'ProductList' => $p_array,
					)));

					$response['success'] = 1;
					$response['msg'] = 'Your item "' . $auth["ProductTitle"] . '" has been successfully posted. Once a transporter reviews and accepts it, you will be notified. You may also check the list of available transporters close to your current location.';

					// Insert Transaction information
					$collTransaction->insert(array(
						"SendById" => $auth["_id"],
						"SendToId" => "aquantuo",
						"SendByName" => @$auth['RequesterName'],
						"RecieveByName" => "Aquantuo",
						"Description" => "Amount deposited against delivery request for {$auth['ProductTitle']}, PackageId: {$auth['PackageNumber']} from {$auth['PickupFullAddress']} to {$auth['DeliveryFullAddress']}",
						"Credit" => floatval($payable),
						"Debit" => "",
						"Status" => "completed",
						"TransactionType" => "delivery_request",
						'request_id' => $app->request->post('packageId'),
						'transaction_id' => @$StripeResponse->id,
						'item_id' => '',
						"EnterOn" => new MongoDate(),
					));

					$cardcoll->insert([
						'request_id' => $app->request->post('packageId'),
						'item_id' => "",
						'card_id' => $app->request->post('creditCardId'),
						'type' => 'request',
						'payment' => $payable,
						'brand' => @$StripeResponse['source']['brand'],
						'last4' => @$StripeResponse['source']['last4'],
						'description' => "Payment done for request Title: '" . $auth['ProductTitle'] . "' Package Id:" . $auth['PackageNumber'] . ".",
						"EnterOn" => new MongoDate(),
					]);

					require_once 'notify.php';
					$inputArray = [
						'requestid' => $app->request->post('packageId'),
						'_id' => $app->request->post('packageId'),
						'log_type' => "request",
						'message' => "Request Created.",
						'action_user_id' => (string) $userId,
					];
					activityLog($inputArray);

					// Email to requester for information
					$ETemplate = array(
						"to" => $userInformation->Email,
						"replace" => array(
							"[USERNAME]" => $userInformation->Name,
							"[PACKAGETITLE]" => $auth['ProductTitle'],
							"[SOURCE]" => $auth['PickupFullAddress'],
							"[DESTINATION]" => $auth['DeliveryFullAddress'],
						),
					);

					$cron_mail = [
						"USERNAME" => ucfirst($userInformation->Name),
						"PACKAGETITLE" => ucfirst($auth['ProductTitle']),
						"SOURCE" => $auth['PickupFullAddress'],
						"DESTINATION" => $auth['DeliveryFullAddress'],
						'email_id' => '',
						'email' => $userInformation->Email,
						'status' => 'ready',
					];

					if ($app->request->post('requesttype') == 'old') {

						//send_mail('5877454f7ac6f6da198b456a', $ETemplate);
						$cron_mail['email_id'] = '5877454f7ac6f6da198b456a';
						$sendmail_col->insert($cron_mail);
					} else {
						//send_mail('5694a10f5509251cd67773eb', $ETemplate);
						$cron_mail['email_id'] = '5694a10f5509251cd67773eb';
						$sendmail_col->insert($cron_mail);

						// Notification to admin
						$coll->insert(array(
							'NotificationTitle' => "New request of Send a package",
							'NotificationMessage' => sprintf('The send a package request titled:"%s", ID:%s has been created.',
								$auth['ProductTitle'], $auth['PackageNumber']),
							'NotificationType' => 1,
							'NotificationReadStatus' => 0,
							'Date' => new MongoDate(),
							'GroupTo' => 'Admin',
						));

					}

					if ($auth['TripId'] != '') {
						$transporterinfomration = $UCollection->find(array('_id' => $auth['TransporterId']), array('EmailStatus', 'Email', 'Name', 'NotificationId', 'DeviceType', 'NoficationStatus', 'TPSetting'));

						if ($transporterinfomration->count() > 0) {
							$transporterinfomration = (Object) $transporterinfomration->getNext();
							// End of email
							if ($transporterinfomration->TPSetting == 'on' && $transporterinfomration->EmailStatus == 'on') {
								$ETemplate = array(
									"to" => $transporterinfomration->Email,
									"replace" => array(
										"[USERNAME]" => $transporterinfomration->Name,
										"[PACKAGEID]" => $auth['PackageNumber'],
										"[PACKAGETITLE]" => $auth['ProductTitle'],
										"[SOURCE]" => $auth['PickupFullAddress'],
										"[DESTINATION]" => $auth['DeliveryFullAddress'],
									),
								);
								if ($app->request->post('requesttype') == 'old') {
									send_mail('569f82bc5509251cd67773f2', $ETemplate);
								} else {
									send_mail('569e16b65509251cd67773f1', $ETemplate);
								}
							}
							if ($transporterinfomration->NoficationStatus == 'on') {
								require_once 'Slim/library/Requestnotification.php';

								$pushnoti = new Notification();
								if ($app->request->post('requesttype') == 'old') {
									$pushnoti->setValue('title', "Trip Request Updated");
									$pushnoti->setValue('message', "\"{$auth['ProductTitle']}\" is updated by user.");

								} else {
									$pushnoti->setValue('title', "One Trip Request");
									$pushnoti->setValue('message', "Requester:\"{$auth['RequesterName']}\" is create a request for trip.");
								}

								$pushnoti->setValue('location', 'transporter_delivery_detail');
								$pushnoti->setValue('locationkey', $app->request->post('packageId'));
								$pushnoti->add_user($transporterinfomration->NotificationId, $transporterinfomration->DeviceType);

								$pushnoti->fire();
							}

							$insNotification = array(
								'NotificationType' => "Prepared new request",
								'NotificationUserId' => array($transporterinfomration->_id),
								'Date' => new MongoDate(),
								'GroupTo' => 'User',
							);
							if ($app->request->post('requesttype') == 'old') {
								$insNotification['NotificationTitle'] = "Trip Request Updated";
								$insNotification['NotificationMessage'] = "Requester:\"{$auth['RequesterName']}\" is updated their request. Which is posted for you.";
							} else {
								$insNotification['NotificationTitle'] = "One Trip Request";
								$insNotification['NotificationMessage'] = "Requester:\"{$auth['RequesterName']}\" is create a request for trip.";
							}

							// Notification Section
							if (isset($insNotification)) {
								$coll->insert($insNotification);
							}

						}

					}
					// Email to admin

					$setting = $settingcoll->find(array('_id' => new MongoId('563b0e31e4b03271a097e1ca')));
					if ($setting->count() > 0) {
						$setting = (Object) $setting->getNext();
						$adminETemplate = array(
							"to" => $setting->SupportEmail,
							"replace" => array(
								"[PACKAGEID]" => $auth['PackageNumber'],
								"[PACKAGETITLE]" => $auth['ProductTitle'],
								"[SOURCE]" => $auth['PickupFullAddress'],
								"[DESTINATION]" => $auth['DeliveryFullAddress'],
								"[USERNAME]" => @$auth['RequesterName'],
							),
						);

						$cron_mail2 = [
							"USERNAME" => ucfirst(@$auth['RequesterName']),
							"PACKAGETITLE" => ucfirst($auth['ProductTitle']),
							"SOURCE" => $auth['PickupFullAddress'],
							"DESTINATION" => $auth['DeliveryFullAddress'],
							"PACKAGEID" => $auth['PackageNumber'],
							'email_id' => '',
							'email' => $setting->SupportEmail,
							'status' => 'ready',
						];

						if ($app->request->post('requesttype') == 'old') {
							$cron_mail2['email_id'] = '58773d8f7ac6f6d5188b4567';
							$sendmail_col->insert($cron_mail2);
							//send_mail('58773d8f7ac6f6d5188b4567', $adminETemplate);
						} else {
							//send_mail('56cd3e1f5509251cd677740c', $adminETemplate);
							$cron_mail2['email_id'] = '56cd3e1f5509251cd677740c';
							$sendmail_col->insert($cron_mail2);
						}
					}
					// End email to admin

					if ($app->request->post('requesttype') != 'old') {
						// Send notification to all transporter
						require_once 'Slim/library/Requestnotification.php';
						try {
							carriernotication($app->request->post('packageId'));
						} catch (exception $e) {}
					}

				}
			} catch (Exception $e) {
				$response['msg'] = $e->getMessage();
			}
		}
	}
	echoRespnse(200, $response);

});

$app->post('/online_request_payment', 'authenticate', function () use ($app) {

	verifyRequiredParams(array('creditCardId', 'packageId'));

	global $userId;
	global $db;
	global $userInformation;

	require 'Slim/library/GetDiscount.php';

	$response = array('success' => 0, 'msg' => 'Your Request for delivery is not successfully placed.');
	$collection = $db->delivery_request;
	$UCollection = $db->users;
	$coll = $db->notification;
	$cardcoll = $db->request_cards;
	$trcoll = $db->transaction;

	if (strlen($app->request->post('packageId')) == 24) {
		$request_info = $collection->find(array('_id' => new MongoId($app->request->post('packageId'))));
		$uInfo = $UCollection->find(array('_id' => $userId), array('StripeId', 'Email', 'Name', 'UserImage', 'NotificationId', 'DeviceType', 'EmailStatus', 'Email'));

		if ($request_info->count() > 0 && $uInfo->count() > 0) {
			$request_info = $request_info->getNext();
			$uInfo = $uInfo->getNext();
			$discountPerItem = 0;

			/*if (!empty(trim($app->request->post('promocode')))) {
				$res = get_discount_amt($app->request->post('promocode'),
					$request_info['shippingCost'],
					$request_info['shippingCost'],
					$userInformation->Default_Currency
				);
				$request_info['discount'] = 0;
				if ($res['success'] == 1) {
					$request_info['discount'] = $res['discount'];
				}
			}*/

			try
			{
				if ($request_info['discount'] > 0) {
					$discountPerItem = $request_info['discount'] / count($request_info['ProductList']);
				}

				foreach ($request_info['ProductList'] as $key => $val) {

					$request_info['ProductList'][$key]['discount'] = $discountPerItem;
					$request_info['ProductList'][$key]['status'] = 'ready';
					$request_info['ProductList'][$key]['PaymentStatus'] = 'yes';

				}
				
				$request_info['TotalCost'] = $request_info['TotalCost'];
				

				$StripeResponse = \Stripe\Charge::create(array(
					"amount" => (int) ($request_info['TotalCost'] * 100),
					"card" => $app->request->post('creditCardId'),
					"customer" => $uInfo['StripeId'],
					"currency" => "usd",
					"capture" => true,
					'metadata'=>[
						'customer_name'=> @$uInfo['Name'],
						'customer_email'=> @$uInfo['Email'],
						'action'=>'user'
					],
					'description'=> @$uInfo['Name'],
				));
				if (isset($StripeResponse->status) && @$StripeResponse->status == 'succeeded') {
					$collection->update(array('_id' => new MongoId($app->request->post('packageId'))), array('$set' => array(
						'PaymentStatus' => 'capture',
						'Status' => 'ready',
						'PaymentDate' => new MongoDate(),
						"PromoCode" => $app->request->post('promocode'),
						'ProductList' => $request_info['ProductList'],
						"TotalCost" => $request_info['TotalCost'],
						"BeforePurchaseTotalCost" => $request_info['TotalCost'],
						"discount" => $request_info['discount'],
						"StripeChargeId" => $StripeResponse->id,
					),
					)
					);

					$cardcoll->insert([
						'request_id' => $app->request->post('packageId'),
						'item_id' => "",
						'card_id' => $app->request->post('creditCardId'),
						'type' => 'online',
						'payment' => $request_info['TotalCost'],
						'brand' => @$StripeResponse['source']['brand'],
						'last4' => @$StripeResponse['source']['last4'],
						'description' => "Payment done for request Title: '" . $request_info['ProductTitle'] . "' Package Id:" . $request_info['PackageNumber'] . ".",
						"EnterOn" => new MongoDate(),
					]);

					$trcoll->insert([
						"SendById" => $uInfo['_id'],
						"SendByName" => $uInfo['Name'],
						"SendToId" => "aquantuo",
						"RecieveByName" => "Aquantuo",
						"Description" => "Amount deposited against delivery request for " . $request_info['ProductTitle'] . ",PackageId: " . $request_info['PackageNumber'] . ".",
						"Credit" => $request_info['TotalCost'],
						"Debit" => "",
						"Status" => "completed",
						"TransactionType" => "online",
						'request_id' => $app->request->post('packageId'),
						'item_id' => '',
						'transaction_id' => @$StripeResponse->id,
						"EnterOn" => new MongoDate(),
					]);

					$response['success'] = 1;

					/* Activity Log update by aakash  23-2-2018
						online package Payment
					*/
					$request_info['action_user_id'] = (string) $uInfo['_id'];
					include_once ('notify.php');
					activityLog($request_info);

					// $response['msg'] = 'Your listing has been successfully submitted. Aquantuo will review and proceed with next steps immediately';
					$response['msg'] = 'We will contact you upon review to arrange pick up and drop off';
					require_once 'notify.php';
					online_send_notification_to_admin($request_info);
					online_send_email_to_requester($request_info);
					online_send_email_to_admin($request_info, $uInfo);

				}

			} catch (Exception $e) {
				$response['msg'] = $e->getMessage();
			}
		}
	}
	echoRespnse(200, $response);

});

$app->post('/online_payment_after_item_update', 'authenticate', function () use ($app) {

	verifyRequiredParams(array('creditCardId', 'packageId'));

	global $userId;
	global $db;
	global $userInformation;

	$notification = $db->notification;
	$response = array('success' => 0, 'msg' => 'Oops! Something went wrong.');
	$collection = $db->delivery_request;
	$UCollection = $db->users;
	$coll = $db->notification;

	if (strlen($app->request->post('packageId')) == 24) {
		$request_info = $collection->find(array('_id' => new MongoId($app->request->post('packageId')), "RequestType" => "online"));
		$uInfo = $UCollection->find(array('_id' => $userId), array('StripeId', 'Email', 'Name', 'UserImage', 'NotificationId', 'DeviceType'));

		if ($request_info->count() > 0 && $uInfo->count() > 0) {
			$request_info = $request_info->getNext();
			$uInfo = $uInfo->getNext();
			try
			{
				$StripeResponse = \Stripe\Charge::create(array(
					"amount" => (int) (($request_info['TotalCost'] - $request_info['BeforePurchaseTotalCost']) * 100),
					"card" => $app->request->post('creditCardId'),
					"customer" => $uInfo['StripeId'],
					"currency" => "usd",
					"capture" => false,
					'metadata'=>[
						'customer_name'=> @$uInfo['Name'],
						'customer_email'=> @$uInfo['Email'],
						'action'=>'user'
					],
					'description'=> @$uInfo['Name'],
				));
				if (isset($StripeResponse->status) && @$StripeResponse->status == 'succeeded') {

					foreach ($request_info['ProductList'] as $key => $val) {

						$ship_cost = ($val['shippingCost'] + $val['insurance'] + ($val['price'] * $val['qty']));

						$collection->update(['_id' => new MongoId($app->request->post('packageId')),
							'ProductList._id' => $val['_id']],
							array('$set' => array(
								'ProductList.$.status' => 'paid',
							)));

					}

					$collection->update(array('_id' => new MongoId($app->request->post('packageId'))), array('$set' => array(
						'PaymentStatus' => 'capture',
						'Status' => 'paid',
						'PaymentDate' => new MongoDate(),
						"StripeChargeId2" => $StripeResponse->id,
					),
					));

					// Notification Section
					$notification->insert([
						'NotificationTitle' => "Online payment complete",
						'NotificationMessage' => sprintf('The requester has made payment of buy for me request titled:"%s", ID:%s.', ucfirst($request_info['ProductTitle']), $request_info['PackageNumber']),
						"NotificationReadStatus" => 0,
						"location" => "online",
						'Date' => new MongoDate(),
						'GroupTo' => 'Admin',
					]);

					$response['success'] = 1;
					$response['msg'] = 'Your payment has been completed successfully.';
				}

			} catch (Exception $e) {
				$response['msg'] = $e->getMessage();
			}
		}
	}
	echoRespnse(200, $response);

});
$app->post('/buy_for_me_payment_on_create', 'authenticate', function () use ($app) {

	verifyRequiredParams(array('creditCardId', 'packageId'));
	require_once 'Slim/library/GetDiscount.php';

	global $userId;
	global $db;
	global $userInformation;
	$response = array('success' => 0, 'msg' => 'Your Request for delivery is not successfully placed.');
	$collection = $db->delivery_request;
	$UCollection = $db->users;
	$coll = $db->notification;
	$cardcoll = $db->request_cards;
	$trcoll = $db->transaction;

	if (strlen($app->request->post('packageId')) == 24) {

		$request_info = $collection->find(array('_id' => new MongoId($app->request->post('packageId')), "RequestType" => "buy_for_me"));
		$uInfo = $UCollection->find(array('_id' => $userId), array('StripeId', 'Email', 'Name', 'UserImage', 'NotificationId', 'DeviceType'));

		if ($request_info->count() > 0 && $uInfo->count() > 0) {
			$request_info = $request_info->getNext();
			$uInfo = $uInfo->getNext();
			$discountPerItem = 0;

			/*if (!empty(trim($app->request->post('promocode')))) {

				$res = get_discount_amt($app->request->post('promocode'),
					$request_info['shippingCost']);

				if ($res['success'] == 1) {
					$request_info['discount'] = $res['discount'];
					if (count($request_info['ProductList']) > 0) {
						$discountPerItem = $request_info['discount'] / count($request_info['ProductList']);
					}
				}
			}*/

			try
			{
				foreach ($request_info['ProductList'] as $key => $val) {
					$request_info['ProductList'][$key]['discount'] = $discountPerItem;
					$request_info['ProductList'][$key]['status'] = 'ready';
					$request_info['ProductList'][$key]['PaymentStatus'] = 'yes';

				}

				$StripeResponse = \Stripe\Charge::create(array(
					"amount" => (int) ($request_info['TotalCost'] * 100),
					"card" => $app->request->post('creditCardId'),
					"customer" => $uInfo['StripeId'],
					"currency" => "usd",
					"capture" => true,
					'metadata'=>[
						'customer_name'=> @$uInfo['Name'],
						'customer_email'=> @$uInfo['Email'],
						'action'=>'user'
					],
					'description'=> @$uInfo['Name'],
				));
				if (isset($StripeResponse->status) && @$StripeResponse->status == 'succeeded') {

					$collection->update(array('_id' => new MongoId($app->request->post('packageId'))), array('$set' => array(
						'StripeCard' => $app->request->post('creditCardId'),
						'Status' => 'ready',
						'ProductList' => $request_info['ProductList'],
						'TotalCost' => $request_info['TotalCost'],
						'BeforePurchaseTotalCost' => $request_info['TotalCost'],
						'discount' => $request_info['discount'],
						'StripeChargeId' => $StripeResponse->id,
					),
					));

					$cardcoll->insert([
						'request_id' => $app->request->post('packageId'),
						'item_id' => "",
						'card_id' => $app->request->post('creditCardId'),
						'type' => 'buy_for_me',
						'payment' => $request_info['TotalCost'],
						'brand' => @$StripeResponse['source']['brand'],
						'last4' => @$StripeResponse['source']['last4'],
						'description' => "Payment done for request Title: '" . $request_info['ProductTitle'] . "' Package Id:" . $request_info['PackageNumber'] . ".",
						"EnterOn" => new MongoDate(),
					]);

					$trcoll->insert([
						"SendById" => $uInfo['_id'],
						"SendByName" => $uInfo['Name'],
						"SendToId" => "aquantuo",
						"RecieveByName" => "Aquantuo",
						"Description" => "Amount deposited against delivery request for " . $request_info['ProductTitle'] . ",PackageId: " . $request_info['PackageNumber'] . ".",
						"Credit" => $request_info['TotalCost'],
						"Debit" => "",
						"Status" => "completed",
						"TransactionType" => "buy_for_me",
						'request_id' => $app->request->post('packageId'),
						'item_id' => '',
						'transaction_id' => @$StripeResponse->id,
						"EnterOn" => new MongoDate(),
					]);

					/* 	Activity Log update by aakash  23-2-2018
						buy for me payment Process
					*/
					include_once ('notify.php');
					activityLog($request_info);

					// Email and notifcation section
					require_once 'notify.php';
					bfm_send_notification_to_admin($request_info);
					bfm_send_email_to_requester($request_info, $uInfo);
					bfm_send_email_to_admin($request_info);

					$response['success'] = 1;
					$response['msg'] = 'Your listing has been successfully submitted. Aquantuo will review and proceed with next steps immediately.';
				}

			} catch (Exception $e) {
				$response['msg'] = $e->getMessage();
			}
		}
	}
	echoRespnse(200, $response);

});

$app->post('/buy_for_me_payment', 'authenticate', function () use ($app) {

	verifyRequiredParams(array('creditCardId', 'packageId'));

	global $userId;
	global $db;
	global $userInformation;

	$notification = $db->notification;
	$response = array('success' => 0, 'msg' => 'Your Request for delivery is not successfully placed.');
	$collection = $db->delivery_request;
	$UCollection = $db->users;
	$coll = $db->notification;

	if (strlen($app->request->post('packageId')) == 24) {
		$request_info = $collection->find(array('_id' => new MongoId($app->request->post('packageId')), "RequestType" => "buy_for_me"));
		$uInfo = $UCollection->find(array('_id' => $userId), array('StripeId', 'Email', 'Name', 'UserImage', 'NotificationId', 'DeviceType'));

		if ($request_info->count() > 0 && $uInfo->count() > 0) {
			$request_info = $request_info->getNext();
			$uInfo = $uInfo->getNext();

			try
			{
				$StripeResponse = \Stripe\Charge::create(array(
					"amount" => (int) (($request_info['TotalCost'] + $request_info['after_update_difference'] - $request_info['BeforePurchaseTotalCost']) * 100),
					"card" => $app->request->post('creditCardId'),
					"customer" => $uInfo['StripeId'],
					"currency" => "usd",
					"capture" => false,
					'metadata'=>[
						'customer_name'=> @$uInfo['Name'],
						'customer_email'=> @$uInfo['Email'],
						'action'=>'user'
					],
					'description'=> @$uInfo['Name'],
				));
				if (isset($StripeResponse->status) && @$StripeResponse->status == 'succeeded') {

					$collection->update(array('_id' => new MongoId($app->request->post('packageId'))), array('$set' => array(
						'PaymentStatus' => 'capture',
						'Status' => 'paid',
						'TotalCost' => $request_info['TotalCost'],
						'StripeChargeId2' => $StripeResponse->id,
						'PaymentDate' => new MongoDate(),
					),
					)
					);

					// Notification Section
					$notification->insert([
						'NotificationTitle' => "Buy for me payment complete",
						'NotificationMessage' => sprintf('The requester has made payment of buy for me request titled:"%s", ID:%s.', ucfirst($request_info['ProductTitle']), $request_info['PackageNumber']),
						"NotificationReadStatus" => 0,
						"location" => "buy_for_me",
						'Date' => new MongoDate(),
						'GroupTo' => 'Admin',
					]);

					$response['success'] = 1;
					$response['msg'] = 'Your payment has been completed successfully.';

					foreach ($request_info['ProductList'] as $key => $val) {
						//$alreadypaid = $val['amt_before_calculation'];
						$ship_cost = ($val['shippingCost'] + $val['insurance'] + ($val['price'] * $val['qty']));

						$collection->update(['_id' => new MongoId($app->request->post('packageId')),
							'ProductList._id' => $val['_id']],
							array('$set' => array(
								'ProductList.$.status' => 'paid',
								'ProductList.$.StripeChargeId2' => '',
							)));

					}
				}

			} catch (Exception $e) {
				$response['msg'] = $e->getMessage();
			}
		}
	}
	echoRespnse(200, $response);

});

function mark_promocde_as_use($code) {
	if (!empty(trim($code))) {
		global $db;
		$promocode = $db->promocode;

		$promocode->update(
			array('Code' => strtoupper($code)),
			array('$inc' => array('UsesCount' => 1))
		);
	}
}

/*
 * Function Name :  add_credit_card
 * Description : add credit card
 * Url : http://192.168.11.101/aq/services/stripe.php/add_credit_card
 * Method : POST
 * Para : name,cardNumber,expMonth,expYear,cvv
 * Created By : Ravi shukla
 * Create Date : 25-08-2015
 *
 * */

$app->post('/add_credit_card', 'authenticate', function () use ($app) {

	verifyRequiredParams(array('name', 'cardNumber', 'expMonth', 'expYear', 'cvv'));
	global $userId;
	global $db;

	$response = array('success' => 0, 'msg' => 'Fail to add credit card');
	$collection = $db->users;

	$auth = $collection->find(array('_id' => $userId), array('StripeId'));

	if ($auth->count() > 0) {
		$auth = $auth->getNext();
		$StripeResponse = '';
		if (@$auth['StripeId'] == '') {
			try {
				$StripeResponse = \Stripe\Customer::create(array(
					"description" => "",
					"card" => array(
						"name" => $app->request->post('name'),
						"number" => $app->request->post('cardNumber'),
						"exp_month" => $app->request->post('expMonth'),
						"exp_year" => $app->request->post('expYear'),
						"cvc" => $app->request->post('cvv'),
					),
				));
				if (isset($StripeResponse->id)) {
					$collection->update(array('_id' => $userId), array('$set' => array('StripeId' => $StripeResponse->id)));
					$response['success'] = 1;
					$response['msg'] = "Credit card added successfully.";
				}
			} catch (Exception $e) {
				$response['msg'] = $e->getMessage();
			}
		} else {
			try {
				$cu = \Stripe\Customer::retrieve($auth['StripeId']);
				$StripeResponse = $cu->sources->create(array("card" => array(
					"name" => $app->request->post('name'),
					"number" => $app->request->post('cardNumber'),
					"exp_month" => $app->request->post('expMonth'),
					"exp_year" => $app->request->post('expYear'),
					"cvc" => $app->request->post('cvv'),
				)));
				$response['success'] = 1;
				$response['msg'] = "Credit card added successfully.";

			} catch (Exception $e) {
				$response['msg'] = $e->getMessage();
			}
		}

	}
	echoRespnse(200, $response);

});

/*
 * Function Name :  credit_card_list
 * Description : credit card list
 * Url : http://192.168.11.101/newlara/services/stripe.php/credit_card_list
 * Method : get
 * Created By : Ravi shukla
 * Create Date : 25-08-2015
 *
 * */

$app->get('/credit_card_list', 'authenticate', function () use ($app) {

	global $userId;
	global $db;

	$response = array('success' => 0, 'msg' => 'No credit card found.', 'result' => array());
	$collection = $db->users;
	$collection2 = $db->mobilemoney;

	$auth = $collection->find(array('_id' => $userId), array('StripeId'));
	$mobiledata = $collection2->find(array('user_id' => (string) $userId));
	
	if ($auth->count() > 0) {
		$auth = $auth->getNext();
		if ($auth['StripeId'] != '') {
			try {

				$StripeResponse = \Stripe\Customer::retrieve($auth['StripeId'])->sources->all(array('limit' => 50, 'object' => 'card'));
				// echoRespnse(200, $response);die();
				if (isset($StripeResponse['data'])) {
					foreach ($StripeResponse['data'] as $key => $val) {
						$response['result'][] = array(
							"creditCardid" => $val->id,
							"creditCardNumber" => $val->last4,
							"creditCardBrand" => $val->brand,
							"expMonth" => $val->exp_month,
							"expYear" => $val->exp_year,
							"name" => $val->name,
						);
					}
				}

				
			} catch (Exception $e) {
				$response['msg'] = $e->getMessage();
			}
		}

		if ($mobiledata->count() > 0) {
			foreach ($mobiledata as $value) {
				$response['mobilenumber'][] = array(
					"_id"=> (string) $value['_id'],
					"mobile_number" => $value['mobile_number'],
					"country_code" => $value['country_code'],
					"alternet_moblie" => $value['alternet_moblie'],
					"mobile_type" => $value['mobile_type'],
				);
			}
		} else {
			$response['mobilenumber'] = [];
		}

		if (count($response['result']) > 0 | count($response['mobilenumber']) > 0) {

			$response['success'] = 1;
			$response['msg'] = "Credit card list.";
			if(empty($response['result']) && empty($response['mobilenumber'])){
				$response['success'] = 0;
			}
		}
	}
	echoRespnse(200, $response);

});

/*
 * Function Name :  edit_credit_card_information
 * Description : edit credit card information
 * Url : http://192.168.11.101/aq/services/stripe.php/edit_credit_card
 * Method : POST
 * Para : creditCardId,name,expMonth,expYear
 * Created By : Ravi shukla
 * Create Date : 02-09-2015
 *
 * */

$app->post('/edit_credit_card', 'authenticate', function () use ($app) {

	verifyRequiredParams(array('creditCardId', 'name', 'expMonth', 'expYear'));

	global $userId;
	global $db;

	$response = array('success' => 0, 'msg' => 'Invalid creditcard information.');
	$UCollection = $db->users;

	$uInfo = $UCollection->find(array('_id' => $userId), array('StripeId'));
	if ($uInfo->count() > 0) {
		$uInfo = $uInfo->getNext();

		try {
			$cu = \Stripe\Customer::retrieve($uInfo['StripeId']);

			$card = $cu->sources->retrieve($app->request->post('creditCardId'));
			$card->name = $app->request->post('name');
			$card->exp_month = $app->request->post('expMonth');
			$card->exp_year = $app->request->post('expYear');

			$StripeResponse = $card->save();

			if (isset($StripeResponse->id)) {
				$response['success'] = 1;
				$response['msg'] = "Credit card updated successfully.";
			}
		} catch (Exception $e) {
			$response['msg'] = $e->getMessage();
		}
	}
	echoRespnse(200, $response);

});

/*
 * Function Name :  remove_credit_card
 * Description : remove card list
 * Url : http://192.168.11.101/newlara/services/stripe.php/remove_credit_card
 * Method : post
 * Created By : Ravi shukla
 * Create Date : 25-08-2015
 *
 * */

$app->post('/remove_credit_card', 'authenticate', function () use ($app) {

	verifyRequiredParams(array('creditCardId'));
	global $userId;
	global $db;

	$response = array('success' => 0, 'msg' => 'Oops! Something went wrong.', 'result' => array());
	$collection = $db->users;

	$auth = $collection->find(array('_id' => $userId), array('StripeId'));

	if ($auth->count() > 0) {
		$auth = $auth->getNext();
		if ($auth['StripeId'] != '') {
			try {

				$cu = \Stripe\Customer::retrieve($auth['StripeId']);
				$StripeResponse = $cu->sources->retrieve($app->request->post('creditCardId'))->delete();

				if (isset($StripeResponse->deleted) && @$StripeResponse->deleted == 1) {
					$response['success'] = 1;
					$response['msg'] = "Credit card deleted successfully.";
				}

			} catch (Exception $e) {
				$response['msg'] = $e->getMessage();
			}
		}
	}
	echoRespnse(200, $response);

});

/*
 * Function Name :  made_payment_popup
 * Description : made_payment after interval
 * Url : http://192.168.11.101/newlara/services/stripe.php/made_payment_popup
 * Method : POST
 * Parameter :'creditCardId','packageId','number'(number 1,2,3)
 * Created By : ajay chaudhary
 * Create Date : 29-10-2015
 *
 * */
$app->post('/made_payment_popup', 'authenticate', function () use ($app) {

	verifyRequiredParams(array('creditCardId', 'packageId', 'number'));

	global $userId;
	global $db;

	$localarray = array();

	$response = array('success' => 0, 'msg' => 'Invalid package information.');
	$collection = $db->AQ_DeliveryRequest;
	$UCollection = $db->AQ_Users;
	$sysCollection = $db->AQ_SystemSetting;
	$inverlNumber = $app->request->post('number');
	if (strlen($app->request->post('packageId')) == 24 && $inverlNumber < 4) {
		$auth = $collection->find(array('_id' => new MongoId($app->request->post('packageId')), 'Status' => 'ready'), array('DeliveryCost', 'InitialUrgentCost', 'TotalCost', 'ProductTitle'));
		$uInfo = $UCollection->find(array('_id' => $userId), array('UserStripeId'));
		if ($auth->count() > 0) {
			$auth = $auth->getNext();
			$uInfo = $uInfo->getNext();
			//======================================calculate nect total price===============================
			$id = new MongoId('5625d4a2e4b0fe6c03689667');
			$SystemInfo = $sysCollection->find(array('_id' => $id));
			$SystemInfoArray = array('WP' => '30', 'LWP' => '60');
			$m1 = 0; $m2 = 0; $m3 = 0;
			if ($SystemInfo->count() > 0) {
				$SystemInfo = $SystemInfo->getNext();
				$m1 = $SystemInfo['m1']; $m2 = $SystemInfo['m2']; $m3 = $SystemInfo['m3'];
				if ($inverlNumber == 1 || $inverlNumber == 0) {
					$UrgentCost = $auth['InitialUrgentCost'] * $m1;
					$TotalCost = $UrgentCost + ($auth['TotalCost'] - $auth['InitialUrgentCost']);
				}
				if ($inverlNumber == 2) {
					$UrgentCost = $auth['InitialUrgentCost'] * $m2;
					$TotalCost = $UrgentCost + ($auth['TotalCost'] - $auth['InitialUrgentCost']);
				}
				if ($inverlNumber == 3) {
					$UrgentCost = $auth['InitialUrgentCost'] * $m3;
					$TotalCost = $UrgentCost + ($auth['TotalCost'] - $auth['InitialUrgentCost']);
				}
				$message = @$SystemInfo['messageWP'];
			}

			try {
				$StripeResponse = \Stripe\Charge::create(array(
					"amount" => $auth['DeliveryCost'],
					"card" => $app->request->post('creditCardId'),
					"customer" => $uInfo['UserStripeId'],
					"currency" => "usd",
					"capture" => false,
				));

				if (isset($StripeResponse->status) && @$StripeResponse->status == 'succeeded') {
					$collection->update(array('_id' => new MongoId($app->request->post('packageId'))), array('$set' => array('PaymentStatus' => 'capture', 'TotalCost' => $TotalCost, 'UrgentCost' => $UrgentCost, 'Status' => 'ready', 'StripeChargeId' => $StripeResponse->id)));

					//==================End Send message for local timing=================================
					$response['packageId'] = (string) $app->request->post('packageId');

					$response['success'] = 1;
					$response['msg'] = "Payment done successfully.";
				}
			} catch (Exception $e) {
				$response['msg'] = $e->getMessage();
			}
		}
	}
	echoRespnse(200, $response);

});

$app->run();
