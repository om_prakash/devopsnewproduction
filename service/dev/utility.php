<?php

require 'Slim/Slim.php';
require 'Slim/config/DbConnect.php';
require 'Slim/library/Common.php';
require 'vendor/mailer/Email.php';

\Slim\Slim::registerAutoloader();

/**
 * Step 2: Instantiate a Slim application
 *
 * This example instantiates a Slim application using
 * its default settings. However, you will usually configure
 * your Slim application now by passing an associative array
 * of setting names and values into the application constructor.
 */
$app = new \Slim\Slim();

$userId = '';
$userType = '';

/*
 * Function Name :  get_registration_information
 * Description : get registration information
 * Url : http://192.168.11.101/aq/services/utility.php/get_agreement
 * Method : Get
 * Created By : Ravi shukla
 * Create Date : 22-08-2015
 * */

$app->get('/get_agreement', function () use ($app) {

    global $db;
    $collection = $db->app_content;
    $response = array('success' => 0, 'msg' => 'Agreement.');

    $data = $collection->find(array('ContentType' => $app->request->get('type')));

    if ($data->count() > 0) {
        $data = $data->getNext();
        if (isset($data['ContentType'])) {
            $response['success'] = 1;
            $response['result'] = $data['Content'];
        }
    }
    echoRespnse(200, $response);
});

/*
 * Function Name  : statistic
 * Description    : statistic
 * Url            : http://192.168.11.101/aq/services/utility.php/statistic
 * Method         : GET
 * Header         : Apikey, userType
 * Created By     : Pankaj Gawande
 * Create Date    : 23-11-2015
 *
 */

$app->get('/statistic', 'authenticate', function () use ($app) {

    global $db;
    global $userId;

    $response = array('success' => 0, 'msg' => 'Statistics', 'TransporterCompleteDeliveries' => 0, 'TransporterNotCompleteDeliveries' => 0, 'RequesterCompleteDeliveries' => 0, 'RequesterNotCompleteDeliveries' => 0);
    $collection = $db->delivery_request;

    $requesterInfo = $collection->aggregate(array(
        array('$match' => array('RequesterId' => $userId)),
        array('$group' => array('_id' => array('Status' => '$Status'), 'count' => array('$sum' => 1))),
    ));

    $transporterInfo = $collection->aggregate(array(
        array('$match' => array('TransporterId' => $userId)),
        array('$group' => array('_id' => array('Status' => '$Status'), 'count' => array('$sum' => 1))),
    ));

    foreach ($transporterInfo['result'] as $key) {
        if (isset($key['_id']['Status'])) {
            if ($key['_id']['Status'] == 'delivered') {
                $response['TransporterCompleteDeliveries'] = $key['count'] + $response['TransporterCompleteDeliveries'];
            }
            if ($key['_id']['Status'] == 'cancel') {
                $response['TransporterNotCompleteDeliveries'] = $key['count'] + $response['TransporterNotCompleteDeliveries'];
            }
        }
    }
    foreach ($requesterInfo['result'] as $key) {
        if (isset($key['_id']['Status'])) {
            if ($key['_id']['Status'] == 'delivered') {
                $response['RequesterCompleteDeliveries'] = $key['count'] + $response['RequesterCompleteDeliveries'];
            }
            if ($key['_id']['Status'] == 'cancel') {
                $response['RequesterNotCompleteDeliveries'] = $key['count'] + $response['RequesterNotCompleteDeliveries'];
            }
        }
    }
    $response['success'] = 1;
    $response['TotalTransporterDeliveries'] = $response['TransporterCompleteDeliveries'] + $response['TransporterNotCompleteDeliveries'];
    $response['TotalRequesterDeliveries'] = $response['RequesterCompleteDeliveries'] + $response['RequesterNotCompleteDeliveries'];
    echoRespnse(200, $response);
});

/*
 * Function Name  : get_country_list
 * Description    : getting country list.
 * Url            : http://192.168.11.101/aq/services/utility.php/get_country_list
 * Method         : GET
 * Created By     : Pankaj Gawande
 * Create Date    : 23-11-2015
 *
 */

$app->get('/get_country_list', function () use ($app) {
    $response = array('success' => 0, 'msg' => 'Country not found.', 'result' => array());
    global $db;
    $utility = $db->city_state_country;

    $where = array('Status' => 'Active', 'type' => 'Country');
    if ($app->request->get('operation') == 'signup') {
        $where = array('Signup' => true, 'type' => 'Country');
    } else if ($app->request->get('operation') == 'bank') {
        $where = array('Signup' => true, 'type' => 'Country', 'ShortCode' => [
            '$in' => ['US', 'CA', 'AU', 'UK', 'GB', 'BR', 'JP', 'SG', 'HK', 'NZ'],
        ]);
    }

    $city = $utility->find($where, array('Content', 'allow', 'state_available', 'CurrencyCode', 'ShortCode'))
        ->sort(array('Content' => 1));

    if ($city->count() > 0) {
        foreach ($city as $key => $val) {
            $response['result'][] = array(
                "id" => (String) $val['_id'],
                "title" => $val['Content'],
                "AllowSameCountry" => @$val['allow'],
                "state_available" => $val['state_available'],
                "ShortCode" => @$val['ShortCode'],
                "CurrencyCode" => @$val['CurrencyCode'],
            );
        }

        $getState = $utility->find(array('type' => 'State', 'SuperName' => 'India'), array('Content'))->sort(array('Content' => 1));

        $i = 0;
        $j = 0;

        $state['country'] = 'India';
        foreach ($getState as $stateData) {
            $state['State'][$i]['id'] = $stateData['Content'];
            $state['State'][$i]['title'] = $stateData['Content'];

            $getCity = $utility->find(array('Status' => 'Active', 'type' => 'city', 'SuperName' => $stateData['Content']), array('Content'))->sort(array('Content' => 1));

            $city = array();
            $j = 0;
            foreach ($getCity as $cities) {
                $city[$j]['id'] = $cities['Content'];
                $city[$j]['title'] = $cities['Content'];
                $j++;
            }

            $state['State'][$i]['City'] = $city;

            $i++;
        }

        $response['success'] = 1;
        $response['defaultCountry'] = $state;
        $response['msg'] = 'Country list';
    }

    echo json_encode($response);
});

$app->get('/get_state_list', function () use ($app) {

    $response = array('success' => 0, 'msg' => 'State not found.', 'result' => array());
    verifyRequiredParams(array('countryId'));
    global $db;
    $utility = $db->city_state_country;
    $where = array('Status' => 'Active', 'CountryId' => $app->request->get('countryId'));
    if ($app->request->get('operation') == 'signup') {
        $where = array('Signup' => true, 'CountryId' => $app->request->get('countryId'));
    }
    $city = $utility->find($where, array('Content'))->sort(array('Content' => 1));
    if ($city->count() > 0) {
        foreach ($city as $key => $val) {
            $response['result'][] = array(
                "id" => (String) $val['_id'],
                "title" => $val['Content'],
            );
        }
        $response['success'] = 1;
        $response['msg'] = 'State list';
    }
    echoRespnse(200, $response);
});

/*
 * Function Name :  get_city_list
 * Description : get city list
 * Url : http://192.168.11.101/aq/services/utility.php/get_city_list
 * Param : stateName
 * Method : Get
 * Created By : Ravi shukla
 * Create Date : 22-08-2015
 *
 * */

$app->get('/get_city_list', function () use ($app) {

    $response = array('success' => 0, 'msg' => 'City not found.', 'result' => array());
    verifyRequiredParams(array('stateId'));
    global $db;
    $utility = $db->city_state_country;
    $where = array('Status' => 'Active', 'StateId' => $app->request->get('stateId'));
    if ($app->request->get('operation') == 'signup') {
        $where = array('Signup' => true, 'StateId' => $app->request->get('stateId'));
    }
    $city = $utility->find($where, array('Content'))->sort(array('Content' => 1));

    if ($city->count() > 0) {
        foreach ($city as $key => $val) {
            $response['result'][] = array(
                "id" => (String) $val['_id'],
                "title" => $val['Content'],
            );
        }
        $response['success'] = 1;
        $response['msg'] = 'City list';
    }
    echoRespnse(200, $response);
});

/*
 * Function Name : get_notification_list
 * Description   : get_notification_list
 * Url           : http://192.168.11.101/aq/services/utility.php/get_notification_list
 * Method        : GET
 * header        : Apikey
 * Param         : pageno
 * Created By    : Ravi shukla
 * Create Date   : 24-11-2015
 *
 * */

$app->get('/get_notification_list', 'authenticate', function () use ($app) {

    $response = array('success' => 0, 'msg' => 'None at this time.');
    verifyRequiredParams(array('pageno'));
    global $db;
    global $userId;
    global $userType;

    $limit = 40;
    $start = ($app->request->get('pageno') - 1) * $limit;
    $utility = $db->notification;

    $where = array("NotificationUserId" => array('$elemMatch' => array('$in' => array($userId))));
    $notification = $utility->find($where, array('NotificationTitle', 'NotificationMessage', 'Date', 'NotificationShortMessage'))->skip($start)->limit($limit)->sort(array('_id' => -1));;
    if ($notification->count() > 0) {
        foreach ($notification as $key => $val) {
            $response['result'][] = array(
                "id" => (string) $val['_id'],
                "title" => @$val['NotificationTitle'],
                "status" => @$val['Status'],
                "message" => str_replace('/', '', $val['NotificationMessage']),
                "EnterOn" => $val['Date']->sec,
            );
        }
        if (count($response['result']) > 0) {
            $response['success'] = 1;
            $response['msg'] = 'Notification list';
        }
    }
    $response['userId'] = (string) $userId;
    echoRespnse(200, $response);
});

/*
 * Function Name : remove_notification
 * Description   : remove notification
 * Url           : http://192.168.11.101/newlara/services/utility.php/remove_notification
 * Method        : GET
 * Param         : notificationid
 * Header        : Apikey
 * Library       : Email
 * Created By    : Ravi shukla
 * Create Date   : 24-11-2015
 *
 * */

$app->get('/remove_notification', 'authenticate', function () use ($app) {

    $response = array('success' => 0, 'msg' => 'Notification not found.');
    verifyRequiredParams(array('notificationid'));
    global $db;
    global $userId;

    if (strlen($app->request->get('notificationid')) == 24) {
        $notificationid = new MongoId($app->request->get('notificationid'));
        $utility = $db->notification;

        $notification = $utility->update(array('_id' => $notificationid, 'NotificationType' => 'Admin'), array('$pull' => array('NotificationUserId' => $userId)));

        $remove = array('n' => 0);
        if (!$notification['updatedExisting'] > 0) {
            $remove = $utility->remove(array('_id' => $notificationid, 'NotificationType' => array('$ne' => 'Admin'), "NotificationUserId" => array('$elemMatch' => array('$in' => array($userId)))));
        }
        if ($notification['updatedExisting'] > 0 || $remove['n'] > 0) {
            $response['success'] = 1;
            $response['msg'] = 'Notification removed successfully.';
        }
    }
    echoRespnse(200, $response);
});

/*
 * Function Name : aboutapp
 * Description      : This will return about the app
 * Url           : http://192.168.11.101/aq/services/utility.php/aboutapp
 * Method        : GET
 * Created By    : Ravi shukla
 * Create Date   : 24-11-2015
 *
 * */

$app->get('/aboutapp', function () use ($app) {

    $response = array('success' => 0, 'msg' => 'About app not found.');
    global $db;
    $collection = $db->app_content;

    $info = $collection->find(array('_id' => new MongoId('5667d037780ed01e85586c11'), 'Type' => 'AboutApp'), array('Content'));
    if ($info->count() > 0) {
        $info = $info->getNext();
        $response = array('success' => 1, 'msg' => 'About app.');
        foreach ($info['Content'] as $key => $val) {
            $response['result'][$key] = $val;
        }
        $response['result']['ghana_phone'] = '030-243-4505';
    }

    echoRespnse(200, $response);
});

/*
 * Function Name : faq
 * Description   : This shows FAQs
 * Url           : http://192.168.11.101/aq/services/utility.php/faq
 * Method        : GET
 * Created By    : Ravi shukla
 * Create Date   : 22-09-2015
 *
 * */

$app->get('/faq', function () use ($app) {

    $response = array('success' => 0, 'msg' => 'Faq not found.', 'result' => array());
    global $db;

    verifyRequiredParams(array('pageno'));
    $coll = $db->faq;
    $limit = 10;
    $start = ($app->request->get('pageno') - 1) * $limit;
    $search_value = $app->request->get('searchKey');
    $where = array('Status' => 'Active');
    if ($search_value != '') {

        $where['Question'] = array('$regex' => new MongoRegex("/$search_value/i"));

    }
    $Result = $coll->find($where, array('Question', 'Answer'))->skip($start)->limit($limit)->sort(array('_id' => -1));

    foreach ($Result as $key) {
        $response['result'][] = array(
            "Question" => $key['Question'],
            "Answer" => $key['Answer'],
        );
    }
    if (count($response['result']) > 0) {
        $response['success'] = 1;
        $response['msg'] = 'Faq list.';
    }

    echoRespnse(200, $response);
});

/*
 * Function Name : get_unit_list
 * Description   : This function return unit list.
 * Url           : http://192.168.11.101/aq/services/utility.php/get_unit_list
 * Method        : GET
 * Created By    : Ravi shukla
 * Create Date   : 2-12-2015
 *
 * */
$app->get('/get_unit_list', function () use ($app) {

    $response = array('success' => 0, 'msg' => 'Unit list.', 'Weight' => array(), 'Length' => array());

    global $db;
    $utility = $db->utility;
    $unit = $utility->find(array('Type' => 'Unit'), array('DistanceUnit', 'WeightUnit', 'VolumeUnit', 'Length'));

    if ($unit->count() > 0) {
        $unit = $unit->getNext();
        foreach ($unit['WeightUnit'] as $WKey) {
            $response['Weight'][] = array('id' => (string) $WKey['_id'], 'title' => $WKey['value']);
        }
        foreach ($unit['Length'] as $WKey) {
            $response['Length'][] = array('id' => (string) $WKey['_id'], 'title' => $WKey['value']);
        }
        foreach ($unit['Length'] as $WKey) {
            $response['Height'][] = array('id' => (string) $WKey['_id'], 'title' => $WKey['value']);
        }
        foreach ($unit['Length'] as $WKey) {
            $response['Width'][] = array('id' => (string) $WKey['_id'], 'title' => $WKey['value']);
        }
        $response['success'] = 1;
    }

    echoRespnse(200, $response);
});

/*
 * Function Name : notification
 * Description   : This function send notification.
 * Url           : http://192.168.11.101/aq/services/utility.php/get_category_list
 * Method        : GET
 * Created By    : Ravi shukla
 * Create Date   : 3-12-2015
 *
 * */
$app->get('/get_category_list', function () use ($app) {
    global $db;
    $response = array('success' => 0, 'msg' => 'No any category found', 'result' => array());
    $collection = $db->category;

    $where = array('type' => 'Category', 'Status' => 'Active');
    if ($app->request->get('travelMode') == 'ship') {
        $where['TravelMode'] = 'ship';
    } else if ($app->request->get('travelMode') == 'air') {
        $where['TravelMode'] = 'air';
    }

    $data = $collection->find($where, array('Content', 'TravelMode'))->sort(array('Content' => 1));
    if ($data->count() > 0) {
        $response = array('success' => 1, 'msg' => 'Records found');
        $Other = true;
        foreach ($data as $key => $value) {
            if($value['Content'] == "Other") {
                if($Other == true) {
                    $response['result'][] = array(
                        'id' => (String) $value['_id'],
                        'categoryName' => $value['Content'],
                        'TravelMode'=>$value['TravelMode'],
                    );
                    $Other = false;
                }
            } else {
                $response['result'][] = array(
                    'id' => (String) $value['_id'],
                    'categoryName' => $value['Content'],
                    'TravelMode'=>$value['TravelMode'],
                );
            }
            // $response['result'][] = array(
            //     'id' => (String) $value['_id'],
            //     'categoryName' => ucfirst($value['Content']),
            //     'TravelMode'=>$value['TravelMode'],
            // );
        }

        if ($key = array_search('Other', array_column($response['result'], 'categoryName'))) {

            // array_push($response['result'], array('id' => $response['result'][$key]['id'], 'categoryName' => 'Other')); // inserts element at the end of the array
            // unset($response['result'][$key]); // Delete an item from the array
            $response['result'] = array_values($response['result']);
        }
    }

    echoRespnse(200, $response);
});

$app->get('/get_trip_category', function () use ($app) {
    global $db;
    $response = array('success' => 0, 'msg' => 'No any category found', 'result' => array());
    $collection = $db->trips;
    $ct_collection = $db->category;
    $where = array('_id' => new MongoId($app->request->get('TripId')));
    $data = $collection->find($where, array('SelectCategory', 'TravelMode'));
    if($data->count() > 0){
        
        $data = $data->getNext();
        $where2 = array('TravelMode'=>$data['TravelMode'],'Content'=>['$in'=> $data['SelectCategory']],'type' => 'Category', 'Status' => 'Active');
        $cat = $ct_collection->find($where2, array('Content', 'TravelMode'))->sort(array('Content' => 1));
        if($cat->count() > 0){
            $response = array('success' => 1, 'msg' => 'Records found');
            foreach ($cat as $key => $value) {
                $response['result'][] = array(
                    'id' => (String) $value['_id'],
                    'categoryName' => ucfirst($value['Content']),
                    'TravelMode'=>$value['TravelMode'],
                );
            }
            
        }
    }
    echoRespnse(200, $response);

});



$app->get('/get_local_category_list', function () use ($app) {
    global $db;
    $response = array('success' => 0, 'msg' => 'No any category found', 'result' => array());
    $collection = $db->category;

    $where = array('type' => 'localCategory', 'Status' => 'Active');
    if ($app->request->get('travelMode') == 'ship') {
        $where['TravelMode'] = 'ship';
    } else if ($app->request->get('travelMode') == 'air') {
        $where['TravelMode'] = 'air';
    }

    $data = $collection->find($where, array('Content', 'TravelMode'))->sort(array('Content' => 1));
    if ($data->count() > 0) {
        $response = array('success' => 1, 'msg' => 'Records found');
        foreach ($data as $key => $value) {
            $response['result'][] = array(
                'id' => (String) $value['_id'],
                'categoryName' => $value['Content'],
                'TravelMode'=>$value['TravelMode'],
            );
        }

        if ($key = array_search('Other', array_column($response['result'], 'categoryName'))) {

            array_push($response['result'], array('id' => $response['result'][$key]['id'], 'categoryName' => 'Other')); // inserts element at the end of the array
            unset($response['result'][$key]); // Delete an item from the array
            $response['result'] = array_values($response['result']);
        }
    }

    echoRespnse(200, $response);
});

/*
 * Function Name :  support
 * Description : support
 * Url : http://192.168.11.101/aq/services/utility.php/support
 * Method : Post
 * Parameter : title,query
 * Library : Email
 * Created By : Ravi shukla
 * Create Date : 01-09-2015
 *
 * */

$app->post('/support', 'authenticate', function () use ($app) {
    global $db;
    global $userId;
    verifyRequiredParams(array('title', 'query'));

    require 'Slim/library/Sequence.php';
    $response = array('success' => 0, 'msg' => 'Failed to send email.');
    $coll = $db->support;
    $syscoll = $db->setting;
    $users = $db->users;
    $notification = $db->notification;
    $sendmail_col = $db->sendmail;

    
    $userinfo = $users->find(array('_id' => $userId), array('Email', 'Name', 'UserType', 'DeviceType', 'TransporterType'));
    if ($userinfo->count() > 0) {

        $userinfo = $userinfo->getNext();
        $usertype = ucfirst(@$userinfo['UserType']);
        if (@$userinfo['UserType'] == 'both') {
            $usertype = "Requester / Transporter (" . ucfirst($userinfo['TransporterType']) . ")";
        } else if (@$userinfo['UserType'] == 'transporter') {
            $usertype = "Transporter (" . ucfirst($userinfo['TransporterType']) . ")";
        }
        $sys = $syscoll->find(array('_id' => new MongoId('563b0e31e4b03271a097e1ca')), array('SupportEmail'));
        $sys = $sys->getNext();
        $userinfo['TransporterType'] = ucfirst($userinfo['TransporterType']);

        $ETemplate = array(
            "to" => $sys['SupportEmail'],
            "replace" => array(
                "[USERNAME]" => @$userinfo['Name'],
                "[TITLE]" => ucfirst($app->request->post('title')),
                "[QUERY]" => ucfirst($app->request->post('query')),
                "[EMAIL]" => @$userinfo['Email'],
                "[USERTYPE]" => ucfirst($usertype),
                "[DEVICETYPE]" => ucfirst(@$userinfo['DeviceType']),
            ),
        );
        send_mail('563b5dffe4b03271a097e1ce', $ETemplate);

        /*$cron_mail = [
            "USERNAME" => @$userinfo['Name'],
            "TITLE" => ucfirst($app->request->post('title')),
            "QUERY" => ucfirst($app->request->post('query')),
            "EMAIL" => @$userinfo['Email'],
            "USERTYPE"=> ucfirst($usertype),
            "DEVICETYPE" => ucfirst(@$userinfo['DeviceType']),
            'email_id' =>'563b5dffe4b03271a097e1ce',
            'email' => $sys['SupportEmail'],
            'status' => 'ready',
        ];
        $sendmail_col->insert($cron_mail);*/

        $ETemplate = array(
            "to" => $userinfo['Email'],
            "subject" => array('[SUBJECT]' => ucfirst($app->request->post('title'))),
            "replace" => array(
                "[USERNAME]" => $userinfo['Name'],
                "[QUERY]" => ucfirst($app->request->post('query')),
            ),
        );
        send_mail('566550b813308b84d5997e89', $ETemplate);

        $userCollection = $db->users;
        $userInfo = $userCollection->find(array('_id' => $userId), array('Name', 'Email'));
        foreach ($userInfo as $key => $value) {
            $username = $value['Name'];
            $email = $value['Email'];
        }

        $insData = array(
            'Email' => $email,
            'UserName' => ucfirst($username),
            'Query' => ucfirst($app->request->post('query')),
            'title' => $app->request->post('title'),
            'Token' => getSequence('support'),
            'EnterOn' => new MongoDate(),
        );
        $coll->insert($insData);

        $insNoti = array(
            "NotificationTitle" => sprintf('"%s" has sent a message.',ucfirst($username)),
            "NotificationMessage" => ucfirst($app->request->post('query')),
            "NotificationReadStatus" => 0,
            "NotificationUserId" => array($userId),
            "location" => "support_request",
            "locationkey" => $insData['Token'],
            "Date" => new MongoDate(),
            "GroupTo" => "Admin",
        );
        
        $notification->insert($insNoti);
        $response['success'] = 1;
        $response['msg'] = "Thank you. We will review and get back to you shortly.";
    }
    echoRespnse(200, $response);
});

/* Function Name :  update_setting
 * Description : update setting
 * Method : Post
 * Url : http://192.168.11.101/aq/services/utility.php/update_setting
 * Para : status,type
 * Created By : Ravi shukla
 * Create Date : 19-10-2015
 * */

$app->post('/update_setting', 'authenticate', function () use ($app) {

    verifyRequiredParams(array('status', 'type'));
    global $db;
    global $userId;
    global $userType;
    $response = array('success' => 0, 'msg' => 'Fail to update.');
    $collection = $db->users;

    $update = array('updated_at' => new MongoDate());
    if ($app->request->post('type') == 'track' && $userType == 'Carriers') {
        $update['TPTrackLocation'] = 'on';
        if ($app->request->post('status') == 'off') {
            $update['TPTrackLocation'] = 'Off';
        }
    } else if ($app->request->post('type') == 'email') {
        $update['EmailStatus'] = 'on';
        if ($app->request->post('status') == 'off') {
            $update['EmailStatus'] = 'off';
        }
    } else if ($app->request->post('type') == 'notification') {
        $update['NoficationStatus'] = 'on';
        if ($app->request->post('status') == 'off') {
            $update['NoficationStatus'] = 'off';
        }
    } else if ($app->request->post('type') == 'transporter_setting') {
        $update['TPSetting'] = 'on';
        if ($app->request->post('status') == 'off') {
            $update['TPSetting'] = 'off';
        }
    } else if ($app->request->post('type') == 'sound') {
        $update['SoundStatus'] = 'on';
        if ($app->request->post('status') == 'off') {
            $update['SoundStatus'] = 'off';
        }
    } else if ($app->request->post('type') == 'vibration') {
        $update['VibrationStatus'] = 'on';
        if ($app->request->post('status') == 'off') {
            $update['VibrationStatus'] = 'off';
        }
    } else if ($app->request->post('type') == 'consolidate_item') {
        $update['consolidate_item'] = 'on';
        if ($app->request->post('status') == 'off') {
            $update['consolidate_item'] = 'off';
        }
    }
    $res = $collection->update(array('_id' => $userId), array('$set' => $update));

    if (@$res['updatedExisting'] > 0) {
        $response = array('success' => 1, 'msg' => 'Updated successfully.');
    }

    echoRespnse(200, $response);
});

/*
 * Function Name :  feedback
 * Description : feedback
 * Url : http://192.168.11.101/aq/services/utillity.php/feedback
 * Method : Post
 * Header : Apikey
 * Parameter : rating,comment
 * Created By : Ravi shukla
 * Create Date : 12-10-2015
 * */

$app->post('/feedback', 'authenticate', function () use ($app) {

    verifyRequiredParams(array('rating', 'comment'));

    global $db;
    global $userId;
    global $userType;
    $collection = $db->users;
    $Auth = $collection->find(array('_id' => $userId), array('Name', 'City', 'State', 'RequesterStatus', 'TransporterStatus'));
    $Auth = $Auth->getNext();

    $response = array('success' => 0, 'msg' => 'Oops! Something went wrong.');

    $feedback = $db->feedback;
    $insertData = array(
        'UserGroup' => ($Auth['RequesterStatus'] == 'active' && $Auth['TransporterStatus'] == 'active') ? 'Requester / Transporter' : 'Requester',
        'UserId' => $userId,
        'UserName' => $Auth['Name'],
        'UserCity' => @$Auth['City'],
        'UserState' => @$Auth['State'],
        'Comment' => $app->request->post('comment'),
        'Rating' => (float) $app->request->post('rating'),
        'EnterOn' => new MongoDate());

    $res = $feedback->insert($insertData);

    $response['success'] = 1;
    $response['msg'] = "Feedback saved successfully.";

    echoRespnse(200, $response);
});

$app->get('/test', function () use ($app) {

    global $db;
    $arry = json_decode(file_get_contents('vendor/city.json'));

    $city = array();
    foreach ($arry as $key) {
        if ($key->State == $app->request->get('state')) {

            $city[] = array(
                "Content" => ucwords(strtolower($key->City)),
                "StateId" => $app->request->get('stateId'),
                "SuperName" => $app->request->get('SuperName'),
                "type" => "city",
                "Status" => "Active",
            );

        }
    }
    $collection = $db->city_state_country;
    $info = $collection->batchInsert($city);
    print_r($info);

});

$app->run();
