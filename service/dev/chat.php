<?php

require 'Slim/Slim.php';
require 'Slim/config/DbConnect.php';
require 'Slim/library/Common.php';
require_once 'vendor/Notification/Notification.php';

\Slim\Slim::registerAutoloader();

/**
 * Step 2: Instantiate a Slim application
 *
 * This example instantiates a Slim application using
 * its default settings. However, you will usually configure
 * your Slim application now by passing an associative array
 * of setting names and values into the application constructor.
 */
$app = new \Slim\Slim();


 
$app->post('/chat_notification', function ()use ($app){
	
	global $db;

	$response = array('success'=>0,'msg'=>'Oops! Something went wrong.');
	if(strlen($app->request->post('ChatUserId')) == 24)
	{
		$usercollection = $db->users;

		$user = $usercollection->find(array('_id'=> new MongoId($app->request->post('ChatUserId'))),array('ChatName','NotificationId','DeviceType'));

		if($user->count() > 0)
		{
			$user = (Object)$user->getNext();

			$response = array('success'=>1,'msg'=>'Notification send successfully.');
			$array = json_decode($app->request->post('Chatdata'));

			$msg = 'You have new message(s) from '. ucwords($app->request->post('UserName'));

			//Send push notification	
			$pushnoti = new Notification();
			$pushnoti->setValue('title','Aquantuo');
			$pushnoti->setValue('message','You have new message');
			$pushnoti->setValue('location', 'chat_notification');
			$pushnoti->setValue('locationkey',$msg);
			$pushnoti->setValue('chat-data',$array);

			$pushnoti->add_user($user->NotificationId,$user->DeviceType);
			$pushnoti->fire(); 
			// End send push notification
		}
	}

	echoRespnse(200, $response);
});
  



$app->run();

 
