<?php

// Notification Section
require_once 'vendor/mailer/Email.php';

function online_send_notification_to_admin($request) {
	global $db;
	$coll = $db->notification;
	$coll->insert([
		'NotificationTitle' => "Online purchase request",
		'Color' => 'Green',
		'NotificationMessage' => sprintf('The online request titled:"%s", ID:%s has been created.', ucfirst($request['ProductTitle']), $request['PackageNumber']),
		'NotificationType' => 1,
		'NotificationReadStatus' => 0,
		'Date' => new MongoDate(),
		'GroupTo' => 'Admin',
	]);
}

// End notification

function online_send_email_to_requester($request) {
	global $db;
	$sendmail_col = $db->sendmail;
	$settingcollection = $db->setting;
	$setting = $settingcollection->find(array('_id' => new MongoId('563b0e31e4b03271a097e1ca')));

	if ($setting->count() > 0) {
		$setting = $setting->getNext();
		require_once 'vendor/mailer/Email.php';
		$ETemplate = array(
			"to" => $setting['SupportEmail'],
			"replace" => array(
				"[USERNAME]" => $request['RequesterName'],
				"[PACKAGETITLE]" => ucfirst($request['ProductTitle']),
				"[DESTINATION]" => ucfirst($request['DeliveryFullAddress']),
				"[PACKAGEID]" => $request["PackageNumber"],
				"[DELIVERY_TYPE]" => ucfirst($request["RequestType"]),
				"[DISTANCE]" => number_format($request['distance'], 2) . " Miles",
			),
		);

		$cron_mail = [
			"USERNAME" => $request['RequesterName'],
			"PACKAGETITLE" => ucfirst($request['ProductTitle']),
			"SOURCE" => "",
			"DESTINATION" => ucfirst($request['DeliveryFullAddress']),
			"PACKAGEID"=> $request["PackageNumber"],
			"DELIVERY_TYPE" => ucfirst($request["RequestType"]),
			"DISTANCE" => number_format($request['distance'], 2) . " Miles",
			'email_id' => '587751d47ac6f6e61c8b4568',
			'email' => $setting['SupportEmail'],
			'status' => 'ready',
		];
		$sendmail_col->insert($cron_mail);
		//send_mail('587751d47ac6f6e61c8b4568', $ETemplate);
	}

}

//email to admin for online request
function online_send_email_to_admin($request, $userinfo) {
	if (is_object($userinfo)) {
		$userinfo = (Array) $userinfo;
	}
	global $db;
	$sendmail_col = $db->sendmail;
	if ($userinfo['EmailStatus'] == 'on') {
		/*send_mail('587750587ac6f6e61c8b4567', [
			"to" => $userinfo['Email'],
			"replace" => [
				"[USERNAME]" => $request['RequesterName'],
				"[PACKAGETITLE]" => ucfirst($request['ProductTitle']),
				"[DESTINATION]" => ucfirst($request['DeliveryFullAddress']),
				"[PACKAGEID]" => $request["PackageNumber"],
				"[DELIVERY_TYPE]" => ucfirst($request["RequestType"]),
				"[DISTANCE]" => number_format($request['distance'], 2) . " Miles",
				"[SHIPPINGCOST]" => $request['shippingCost'],
				"[TOTALCOST]" => $request['TotalCost'],
			],
		]);*/

		$cron_mail = [
			"USERNAME" => $request['RequesterName'],
			"PACKAGETITLE" => ucfirst($request['ProductTitle']),
			//"SOURCE" => $auth['PickupFullAddress'],
			"DESTINATION" => ucfirst($request['DeliveryFullAddress']),
			"PACKAGEID"=> $request["PackageNumber"],
			"DELIVERY_TYPE" => ucfirst($request["RequestType"]),
			"DISTANCE" => number_format($request['distance'], 2) . " Miles",
			"SHIPPINGCOST" => $request['shippingCost'],
			"TOTALCOST" => $request['TotalCost'],
			'email_id' => '587750587ac6f6e61c8b4567',
			'email' => $userinfo['Email'],
			'status' => 'ready',
		];
		$sendmail_col->insert($cron_mail);
	}

}

function bfm_send_notification_to_admin($request, $activity = 'create') {
	global $db;
	$notification = $db->notification;
	// Notification Section
	if ($activity == 'create') {
		$notification->insert([
			'NotificationTitle' => "New Buy for me request created",
			'NotificationMessage' => sprintf('The buy for me request titled:"%s", ID:%s has been created.', ucfirst($request['ProductTitle']), $request['PackageNumber']),
			"NotificationReadStatus" => 0,
			"location" => "buy_for_me",
			'Date' => new MongoDate(),
			'GroupTo' => 'Admin',
		]);
	} else {
		$notification->insert([
			'NotificationTitle' => "Buy for me request updated",
			'NotificationMessage' => sprintf('The buy for me request titled:"%s", ID:%s has updated.', ucfirst($request['ProductTitle']), $request['PackageNumber']),
			"NotificationReadStatus" => 0,
			"location" => "buy_for_me",
			'Date' => new MongoDate(),
			'GroupTo' => 'Admin',
		]);
	}
}

function bfm_send_email_to_requester($request, $userinfo, $activity = 'create') {
	if (is_object($userinfo)) {
		$userinfo = (array) $userinfo;
	}

	global $db;
	$sendmail_col = $db->sendmail;
	// Email to requester
	if ($activity == 'create') {

		/*send_mail('587748397ac6f63c1a8b456d', [
			"to" => $userinfo['Email'],
			"replace" => [
				"[USERNAME]" => $userinfo['Name'],
				"[PACKAGETITLE]" => ucfirst($request['ProductTitle']),
				"[PACKAGEID]" => $request['PackageNumber'],
				"[DESTINATION]" => $request['DeliveryFullAddress'],
				"[SHIPPINGCOST]" => $request['shippingCost'],
				"[TOTALCOST]" => $request['TotalCost'],
			],
		]);*/

		$cron_mail = [
			"USERNAME" => $userinfo['Name'],
			"PACKAGETITLE" => ucfirst($request['ProductTitle']),
			"DESTINATION" => $request['DeliveryFullAddress'],
			"PACKAGEID"=> $request['PackageNumber'],
			"SHIPPINGCOST" => $request['shippingCost'],
			"TOTALCOST" => $request['TotalCost'],
			'email_id' => '587748397ac6f63c1a8b456d',
			'email' => $userinfo['Email'],
			'status' => 'ready',
			'PRFEE' => @$request['ProcessingFees'],
			'by_mean'=>'email_2'
		];
	} else {
		/*send_mail('58941acc6befd970002fa5ca', [
			"to" => $userinfo['Email'],
			"replace" => [
				"[USERNAME]" => $userinfo['Name'],
				"[PACKAGETITLE]" => ucfirst($request['ProductTitle']),
				"[PACKAGEID]" => $request['PackageNumber'],
				"[DESTINATION]" => $request['DeliveryFullAddress'],
				"[SHIPPINGCOST]" => $request['shippingCost'],
				"[TOTALCOST]" => $request['TotalCost'],
			],
		]);*/

		$cron_mail = [
			"USERNAME" => $userinfo['Name'],
			"PACKAGETITLE" => ucfirst($request['ProductTitle']),
			"DESTINATION" => $request['DeliveryFullAddress'],
			"PACKAGEID"=> $request['PackageNumber'],
			"SHIPPINGCOST" => $request['shippingCost'],
			"TOTALCOST" => $request['TotalCost'],
			//'email_id' => '58941acc6befd970002fa5ca',
			'email_id' => '587748397ac6f63c1a8b456d',
			
			'email' => $userinfo['Email'],
			'status' => 'ready',
		];
	}
	$sendmail_col->insert($cron_mail);

}

function bfm_send_email_to_admin($request) {
	global $db;
	//email to admin
	$settingcollection = $db->setting;
	$sendmail_col = $db->sendmail;
	$setting = $settingcollection->find(array('_id' => new MongoId('563b0e31e4b03271a097e1ca')));

	if ($setting->count() > 0) {
		$setting = $setting->getNext();
		$ETemplate = array(
			"to" => @$setting['SupportEmail'],
			"replace" => array(
				"[USERNAME]" => $request['RequesterName'],
				"[PACKAGETITLE]" => ucfirst($request['ProductTitle']),
				"[DESTINATION]" => ucfirst($request['DeliveryFullAddress']),
				"[PACKAGEID]" => $request["PackageNumber"],
				"[DELIVERY_TYPE]" => "Buy for Me",
				"[DISTANCE]" => number_format($request['distance'], 2) . " Miles",
			),
		);

		$cron_mail = [
			"USERNAME" => $request['RequesterName'],
			"PACKAGETITLE" => ucfirst($request['ProductTitle']),
			"DESTINATION" => ucfirst($request['DeliveryFullAddress']),
			"PACKAGEID" => $request["PackageNumber"],
			"DELIVERY_TYPE" => "Buy for Me",
			"DISTANCE" => number_format($request['distance'], 2) . " Miles",
			'email_id' => '583967f17ac6f6b21a8b4567',
			'email' => @$setting['SupportEmail'],
			'status' => 'ready',
		];
		//send_mail('583967f17ac6f6b21a8b4567', $ETemplate);
		$sendmail_col->insert($cron_mail);

	}
}

function activityLog($inputArray) {

	global $db;
	$requestCollection = $db->delivery_request;
	$activityCollection = $db->activitylog;

	$req_data = $requestCollection->find(["_id" => $inputArray['_id']], ['PackageNumber', 'RequestType', 'Status', 'ProductList', 'ProductTitle']);
	if ($req_data->count() > 0) {
		$req_data = $req_data->getNext();
		if ($req_data['Status'] == 'pending') {
			$message = 'Package has been created.';
		} elseif ($req_data['Status'] == 'ready') {
			$message = 'Payment successful.';
		} elseif ($req_data['Status'] == 'out_for_pickup') {
			$message = 'Item is in destination country going through customs and sorting.';
		} elseif ($req_data['Status'] == 'out_for_delivery') {
			$message = 'Your package is en route to be delivered.';
		} elseif ($req_data['Status'] == 'delivered') {
			$message = 'Package has been successfully delivered.';
		} elseif ($req_data['Status'] == 'cancel') {
			$message = 'Product has been cancel.';
		}

		$insert2 = [];
		if (isset($req_data['ProductList'])) {
			foreach ($req_data['ProductList'] as $key) {
				$activityCollection->insert(['request_id' => (string) $inputArray['_id'],
					'request_type' => $req_data['RequestType'],
					'PackageNumber' => $req_data['PackageNumber'],
					'log_type' => 'request',
					'status' => $req_data['Status'],
					'message' => $message,
					'package_id' => $key['package_id'],
					'item_name' => (isset($key['product_name'])) ? $key['product_name'] : @$key['item_name'],
					'action_user_id'=> @$inputArray['action_user_id'],
					'EnterOn' => new MongoDate]
				);
			}
		}
	}
}

function item_activity_log($inputArray){
	global $db;
	$requestCollection = $db->delivery_request;
	$activityCollection = $db->activitylog;


	
	if($inputArray['status'] == 'out_for_pickup'){
		$message = 'Item is in destination country going through customs and sorting.';
	}else if($inputArray['status'] == 'out_for_delivery'){
		$message = 'Your package is en route to be delivered.';
	}else if($inputArray['status'] == 'delivered'){
		$message = 'Package has been successfully delivered.';
	}else if($inputArray['status'] == 'cancel'){
		$message = 'Item delivery has been canceled.';
	}  

	$activityCollection->insert(['request_id' => $inputArray['_id'],
		'request_type' => $inputArray['RequestType'],
		'PackageNumber' => $inputArray['PackageNumber'],
		'log_type' => 'request',
		'status' => $inputArray['status'],
		'message' => $message,
		'package_id' => $inputArray['package_id'],
		'item_name' => $inputArray['product_name'],
		'action_user_id'=> @$inputArray['action_user_id'],
		'EnterOn' => new MongoDate]
	);
}
