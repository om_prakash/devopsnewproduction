<?php

/**
 * Step 1: Require the Slim Framework
 *
 * If you are not using Composer, you need to require the
 * Slim Framework and register its PSR-0 autoloader.
 *
 * If you are using Composer, you can skip this step.
 *
 * Filename Name : authentication.php
 * File Path : services/authentication.php
 * Description : This file contains method related to userinformation.
 * Author: Ravi shukla
 * Created Date : 22-08-2015
 * Library : Email,DbConnect
 *
 * */

require 'Slim/Slim.php';
require 'Slim/config/DbConnect.php';
require 'Slim/library/Common.php';

\Slim\Slim::registerAutoloader();

/**
 * Step 2: Instantiate a Slim application
 *
 * This example instantiates a Slim application using
 * its default settings. However, you will usually configure
 * your Slim application now by passing an associative array
 * of setting names and values into the application constructor.
 */
$app = new \Slim\Slim();
/*
 * Function Name    : trackinh_list
 * Description      : getting list of tracking item.
 * Url              : http://192.168.11.101/aq/services/tracking.php/trackinh_list
 * Method           : GET
 * Parameter        : pageno
 * Created By       : Aakash tejwal.
 * Create Date      : 13-02-2018.
 *
*/
$app->get('/tracking_list', 'requester_auth', function () use ($app) {
    global $db;
    global $userId;
    $collection = $db->activitylog;
    $collection2 = $db->delivery_request;
    $limit = 20;
    $start = (((int) $app->request->get('pageno')) - 1) * $limit;
    $response = array('success' => 0, 'msg' => 'Record not found.', 'result' => array(), 'pagination' => $limit);
    verifyRequiredParams(array('type','request_id'));
    if($app->request->get('type') == 'package_id'){
        $where = array('PackageNumber' => $app->request->get('request_id'));
        $where2 = array('PackageNumber' => $app->request->get('request_id'));
    }else if($app->request->get('type') == 'item_id'){
        $where = array('package_id' => $app->request->get('request_id'));
        $where2 = array("ProductList" => array('$elemMatch' => array('package_id' => $app->request->get('request_id'))));
    }
    $info = $collection->find($where)->sort(array('EnterOn' => -1));
    
    $delivery_info = $collection2->find($where2,['RequesterName','PackageNumber','ProductTitle','RequestType','Status','DeliveryFullAddress','ReceiverMobileNo','PaymentStatus','country_code','TotalCost','ProductList','ExpectedDate']);

    if($info->count() > 0 && $delivery_info->count() > 0){
        $delivery_info = $delivery_info->getNext();
        foreach ($info as $key => $value) {
            $result[]=[
                "request_id" => $value['request_id'],
                'message'=> $value['message'],
                'status'=> get_status_title($value['status'],$delivery_info['RequestType']),
                'EnterOn'=> (isset($value['EnterOn']->sec))?  $value['EnterOn']->sec :"" ,
                //'EnterOn'=> $value['EnterOn'] ,
            ];
        }

        
        $delivery_info['expected_date'] = '';

        
        $delivery_info['Status'] = get_status_title($delivery_info['Status'],$delivery_info['RequestType']);
        
        if($delivery_info['RequestType'] == 'online'  && $delivery_info['Status'] == 'purchased'){
            $delivery_info['Status'] = 'reviewed';
        }
        

        $detail = $delivery_info;
        if($app->request->get('type') == 'item_id'){
            foreach ($delivery_info['ProductList'] as $key => $value) {
                if($value['package_id'] == $app->request->get('request_id')){
                   $detail=[
                    "_id"=>$value['_id'],
                    "package_id"=> $value['package_id'],
                    "PackageNumber"=> $value['package_id'],
                    "tpid"=> $value['tpid'],
                    /*"Status"=> ($delivery_info['RequestType'] == 'online' && $value['status'] == 'purchased')? 'reviewed' :get_status_title($value['status'],$delivery_info['RequestType']),*/

                    "Status"=> ($value['status'] == 'cancel' && @$value['RejectBy'] != '')? "Not Delivered":get_status_title($value['status'],$delivery_info['RequestType']),

                    "tpName"=> $value['tpName'],
                    "marketid"=> @$value['marketid'],
                    "marketname"=> @$value['marketname'],
                    "product_name"=> $value['product_name'],
                    "url"=> @$value['url'],
                    "price"=> @$value['price'],
                    "qty"=> @$value['qty'],
                    "description"=> @$value['description'],
                    "travelMode"=> @$value['travelMode'],
                    "weight"=> @$value['weight'],
                    "weight_unit"=> @$value['weight_unit'],
                    "height"=> @$value['height'],
                    "heightUnit"=> @$value['heightUnit'],
                    "length"=> @$value['length'],
                    "lengthUnit"=> @$value['lengthUnit'],
                    "width"=> @$value['width'],
                    "widthUnit"=> @$value['widthUnit'],
                    "category"=> @$value['category'],
                    "categoryid"=> @$value['categoryid'],
                    "insurance_status"=> @$value['insurance_status'],
                    "image"=> @$value['image'],
                    "TrackingNumber"=> @$value['TrackingNumber'],
                    'expected_date'=>(@$value['ExpectedDate']->sec != '')?$value['ExpectedDate']->sec:'',
                   ] ;
                }
            }
        }
        //$detail['expected_date'] = @$delivery_info['ExpectedDate']->sec;
        $response = array('success' => 1, 'msg' => 'Record', 'result' => $result,'detail'=> $detail,'pagination' => $limit);
    }

    echoRespnse(200, $response);
    $app->stop();
});

function get_status_title($status, $requesttype = '')
{
    switch ($status) {
        case 'ready':
            return 'Ready';
            break;
        case 'trip_pending':
            return 'Pending';
            break;
        case 'trip_ready':
            return 'Ready';
            break;
        case 'out_for_delivery':
            return 'Out for Delivery';
            break;
        case 'out_for_pickup':
            return ($requesttype == 'online' || $requesttype == 'buy_for_me') ? 'Item has arrived in destination country' : 'Out for Pickup';
            break;
        case 'accepted':
            return 'Accepted';
            break;
        case 'delivered':
            return 'Delivered';
            break;
        case 'cancel':
            return 'Canceled';
            break;
        case 'cancel_by_admin':
            return 'Cancelled by Admin';
            break;
        case 'purchased':
            return ($requesttype == 'online') ? 'Item Reviewed' : 'Item Purchased';
            break;
        case 'assign':
            return ($requesttype != 'local_delivery') ? 'Shipment Departed' : 'Transporter Assigned';
            break;
        case 'paid':
            return ($requesttype == 'online') ? 'Waiting for Item to be Received' : 'Pending Purchase';
            break;
        case 'reviewed':
            return 'Reviewed';
            break;
        case 'not_purchased':
            return 'Waiting for your payment';
            break;
        case 'item_received':
            return 'Item Received';
            break;
        case 'shipment_departed':
			return 'Shipment Departed';
			break;
        default:
            return 'Pending';
    }
}

$app->run();
