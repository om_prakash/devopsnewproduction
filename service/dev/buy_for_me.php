<?php
/**
 * Step 1: Require the Slim Framework
 *
 * If you are not using Composer, you need to require the
 * Slim Framework and register its PSR-0 autoloader.
 *
 * If you are using Composer, you can skip this step.
 *
 * Filename Name : tp_online_requet.php
 * File Path : services/authentication.php
 * Description : This file contains method related to userinformation.
 * Author: Ravi shukla
 * Created Date : 19-09-2016
 * Library : Email,DbConnect
 *
 * */

require 'Slim/Slim.php';
require 'Slim/config/DbConnect.php';
require 'Slim/library/Common.php';

\Slim\Slim::registerAutoloader();

/**
 * Step 2: Instantiate a Slim application
 *
 * This example instantiates a Slim application using
 * its default settings. However, you will usually configure
 * your Slim application now by passing an associative array
 * of setting names and values into the application constructor.
 */
$app = new \Slim\Slim();

$userId = '';
$userType = '';

/*
 * Function Name : my_delivery_area
 * Description      : This is used to insert my area(country, state, cities) of individual transporter.
 * Url              : http://192.168.11.101/aq/service/buy_for_me.php/devliery_process
 * Method          : POST
 * Header          : apiKey, userType
 * Parameter     : deliveryAreaCountrydeliveryAreaState,deliveryAreaCities.
 * Created By    : Pankaj Gawande
 * Create Date      : 1-12-2015
 *
 */

$app->post('/devliery_process', 'transporter_auth', function () use ($app) {
	global $db;
	global $userId;
	global $userInformation;

	require 'Slim/library/ReqStatus.php';

	verifyRequiredParams(array('requestId', 'itemId', 'status'));

	$response = array('success' => 0, "msg" => 'Oops! Something went wrong.');
	if (in_array($app->request->post('status'), ['out_for_pickup', 'out_for_delivery', 'delivered', 'cancel'])) {
		$user_coll = $db->users;
		$delivery_coll = $db->delivery_request;
		$notify_coll = $db->notification;
		$sendmail_col = $db->sendmail;

		/*$where = ['_id' => new MongoId($app->request->post('requestId')), 'ProductList._id' => $app->request->post('itemId')];

		$where['ProductList.tpid'] = (String) $userId;*/

		$where = [
			'_id' => new MongoId($app->request->post('requestId')),
			'ProductList' => [
				'$elemMatch' => [
					'_id' => $app->request->post('itemId'),
					'tpid' => (String) $userId
				]
			]
		];

		$updatedata = ['ProductList.$.status' => strtolower($app->request->post('status'))];

		if ($app->request->post('status') == 'out_for_pickup') {
			$where['ProductList.status'] = 'assign';
		} else if ($app->request->post('status') == 'out_for_delivery') {
			$where['ProductList.status'] = 'out_for_pickup';
		} else if ($app->request->post('status') == 'delivered') {
			$where['ProductList.status'] = 'out_for_delivery';
		} else if ($app->request->post('status') == 'cancel') {
			$where['ProductList.status'] = ['$in' => ['assign', 'accepted', 'out_for_pickup']];
		}

		$deliveryinfo = $delivery_coll->find($where, ['ProductList', 'RequesterName', 'DeliveryFullAddress', 'ReturnAddress', 'ReturnAddress', 'verify_code', 'RequesterId', 'RequestType', 'PackageNumber', 'Status','ReceiverCountrycode','ReceiverMobileNo']);

		if ($deliveryinfo->count() > 0) {
			$deliveryinfo = (Object) $deliveryinfo->getNext();
			
			$updatedata = $deliveryinfo->ProductList;
			$inputArray = [
				'status'=>strtolower($app->request->post('status')),
				'RequestType'=>'buy_for_me',
				'package_id' =>'',
				'product_name'=>'',
				'PackageNumber'=>'',
				'action_user_id' => (string) $userId,
				'_id'=>$app->request->post('requestId')
			];
			
			foreach ($deliveryinfo->ProductList as $key => $value) {
				if ($app->request->post('itemId') == $value['_id']) {

					if ($app->request->post('status') == 'delivered') {
						if ($value['verify_code'] != (int) $app->request->post("verificationCode")) {

							$response['msg'] = "Oops! You have entered invalid verification code.";
							echoRespnse(200, $response);
							die;
						} else {
							$updatedata[$key]['DeliveredDate'] = new MongoDate();
						}
					}
					$updatedata[$key]['status'] = $app->request->post('status');
					if ($app->request->post('status') == 'cancel') {
						$updatedata[$key]['CancelDate'] = new MongoDate();
					}
					$updatedata[$key]['status'] = $app->request->post('status');
					$inputArray['package_id'] = $value['package_id'];
					$inputArray['PackageNumber'] = $value['package_id'];
					$inputArray['product_name'] = $value['product_name'];

				}
			}
			
			$requestUpdate = $delivery_coll->update($where, array('$set' => ['ProductList' => $updatedata]));
			/* 	Activity Log:-  update by aakash  24-2-2018
					Transporter case (out_for_pickup,out_for_delivery,delivered,cancel)
			*/
			

			include_once ('notify.php');
			item_activity_log($inputArray);

			if ($requestUpdate['updatedExisting'] > 0) {
				$response = ['success' => 1, "msg" => 'Your delivery has been processed for next step.'];

				// Manage outer status
				require_once ('Slim/library/ReqStatus.php');

				update_status($app->request->post('requestId'));

				$userinfo = $user_coll->find(array('_id' => $deliveryinfo->RequesterId), array('_id', 'NotificationId', 'DeviceType', 'Name', 'Email', 'EmailStatus'));

				if ($userinfo->count() > 0) {
					$userinfo = (Object) $userinfo->getNext();

					if (strtolower($app->request->post('status')) == 'out_for_pickup') {
						$templateid = '57d7d6757ac6f6b5158b4567';
						$templateidforTp = '5876272a7ac6f607128b4569';

						//$title = "Transporter is out for pickup.";
						$title = "Item is in destination country going through customs and sorting.";
						$msg = "Unless you shipped your item to Aquantuo or the Transporter, please have it ready. The Transporter is out to pick up your package or to meet at the agreed upon location.";
						//$msg = 'The assigned Transporter is out to accept/pick up your item(s) for delivery.';

						$adminMsg = sprintf('The transporter is out to pickup the request titled: %s, ID: %s', $deliveryinfo->ProductList[0]['product_name'], $deliveryinfo->ProductList[0]['package_id']);

						$response['msg'] = "Item is in destination country going through customs and sorting.";

					} else if (strtolower($app->request->post('status')) == 'out_for_delivery') {
						$templateid = '587863927ac6f6130c8b4568';
						$templateidforTp = '588f0b706befd90b2e270ce5';
						$title = "Your package is en route to be delivered.";

						$msg = sprintf('Transporter is en route to deliver your item titled:%s, ID: %s.',
							$deliveryinfo->ProductList[0]['product_name'], $deliveryinfo->ProductList[0]['package_id']);

						$adminMsg = sprintf('Transporter has picked up the package and is out for delivery. Item titled: %s, ID: %s to %s',
							$deliveryinfo->ProductList[0]['product_name'],
							$deliveryinfo->ProductList[0]['package_id'],
							$deliveryinfo->DeliveryFullAddress);

						$response['msg'] = "Your package is en route to be delivered.";

					} else if (strtolower($app->request->post('status')) == 'delivered') {
						$templateid = '57d7d8107ac6f6e0148b4567';
						$templateidforTp = '587629a97ac6f66e128b4567';

						$title = "Package has been successfully delivered.";
						$msg = "Your request has been completed";

						$adminMsg = "Transporter \"{$userInformation->Name}\" has dropoff PackageID: \"{$deliveryinfo->ProductList[0]['package_id']}\" to pickup location.";

						$response['msg'] = "Package has been successfully delivered.";

					} else if (strtolower($app->request->post('status')) == 'cancel') {
						$templateid = '588b56736befd90b9d7ea797';
						$templateidforTp = '58762b057ac6f6f2128b4567';
						$templateidforadmin = '57e8c3f5e4b01d01c916d24e';

						$title = "Your package canceled.";
						$msg = "Transporter \"{$userInformation->Name}\" has been canceled your package \"{$deliveryinfo->ProductList[0]['package_id']}\". ";
						$adminMsg = "Transporter \"{$userInformation->Name}\" has been canceled package PackageID: \"{$deliveryinfo->ProductList[0]['package_id']}\".";

						$response['msg'] = "Package has been canceled successfully.";

					}
					if (isset($msg)) {

						$notify_coll->batchInsert(array(
							array(
								"NotificationTitle" => $title,
								"NotificationShortMessage" => $msg,
								"NotificationMessage" => $msg,
								"NotificationType" => "request",
								"NotificationUserId" => array($userinfo->_id),
								"Date" => new MongoDate(),
								"GroupTo" => "User",
							),
							array(
								"NotificationTitle" => $title,
								"NotificationMessage" => $adminMsg,
								"NotificationUserId" => array(),
								"NotificationReadStatus" => 0,
								"location" => "request_detail",
								"locationkey" => $app->request->post('requestId'),
								"Date" => new MongoDate(),
								"GroupTo" => "Admin",
							),
						));

						if ($userinfo->EmailStatus == 'on') {
							$cron_mail = [
								"USERNAME" => @$deliveryinfo->RequesterName,
								"PACKAGETITLE" => @$deliveryinfo->ProductList[0]['product_name'],
								"SOURCE" => "",
								"DESTINATION" => @$deliveryinfo->DeliveryFullAddress,
								"PACKAGEID"=> @$deliveryinfo->ProductList[0]['package_id'],
								'email_id' => @$templateid,
								'email' => @$userinfo->Email,
								'status' => 'ready',
								'CC' => @$deliveryinfo->ReceiverCountrycode,
								'MNO' => @$deliveryinfo->ReceiverMobileNo,
								'by_mean'=> 'email_2'
							];
							$sendmail_col->insert($cron_mail);
						}

						if (isset($templateidforTp)) {
							if ($userInformation->TPSetting == 'on' && $userInformation->EmailStatus == 'on') {
								$cron_mail = [
									"USERNAME" => @$userInformation->Name,
									"PACKAGETITLE" => @$deliveryinfo->ProductList[0]['product_name'],
									"SOURCE" => "",
									"DESTINATION" => @$deliveryinfo->DeliveryFullAddress,
									"PACKAGEID"=> @$deliveryinfo->ProductList[0]['package_id'],
									'email_id' => @$templateidforTp,
									'email' => @$userInformation->Email,
									'status' => 'ready',
								];
								$sendmail_col->insert($cron_mail);
							}

							
						}

						

						/*require 'vendor/mailer/Email.php';

							if (isset($templateid)) {
								if ($userinfo->EmailStatus == 'on') {
									// Email for Requester
									$ETemplate = array(
										"to" => $userinfo->Email,
										"replace" => array(
											"[USERNAME]" => $deliveryinfo->RequesterName,
											"[PACKAGETITLE]" => $deliveryinfo->ProductList[0]['product_name'],
											"[PACKAGEID]" => $deliveryinfo->ProductList[0]['package_id'],
											"[SOURCE]" => "Aquantuo",
											"[DESTINATION]" => $deliveryinfo->DeliveryFullAddress,
										),
									);

									send_mail('588b56736befd90b9d7ea797', $ETemplate);
								}
							}

							// Email for Transporter
							if (isset($templateidforTp)) {
								if ($userInformation->TPSetting == 'on' && $userInformation->EmailStatus == 'on') {
									$ETemplate = array(
										"to" => @$userInformation->Email,
										"replace" => array(
											"[USERNAME]" => @$userInformation->Name,
											"[PACKAGETITLE]" => $deliveryinfo->ProductList[0]['product_name'],
											"[PACKAGEID]" => $deliveryinfo->ProductList[0]['package_id'],
											"[DESTINATION]" => $deliveryinfo->DeliveryFullAddress,
										),
									);
									send_mail('58762b057ac6f6f2128b4567', $ETemplate);
								}
						*/

						//Send push notification
						require_once 'vendor/Notification/Notification.php';
						$pushnoti = new Notification();
						$pushnoti->setValue('title', $title);
						$pushnoti->setValue('message', $msg);
						$pushnoti->setValue('location', 'BUY_FOR_ME_DELIVERY_DETAIL_FOR_REQUESTER');
						$pushnoti->setValue('locationkey', $app->request->post('requestId'));

						$pushnoti->add_user($userinfo->NotificationId, $userinfo->DeviceType);
						$pushnoti->fire();
						// End send push notification
					}

				}

			}

		}
	}

	echoRespnse(200, $response);
});

/*
 * Function Name :  finish_delivery
 * Description : finish delivery
 * Url : http://192.168.11.101/aq/services/transporter.php/feedback_on_package_delivered
 * Method : POST
 * Header : Apikey, Usertype
 * Parameter : requestId,message,rating
 * Created By : Ravi shukla
 * Create Date : 01-09-2015
 * */
$app->post('/feedback_by_tp', 'transporter_auth', function () use ($app) {

	global $db;
	global $userId;

	verifyRequiredParams(array('requestId', 'rating', 'message', 'itemId'));
	$response = array('success' => 0, 'msg' => 'Oops! Something went wrong.');

	$requestCollection = $db->delivery_request;
	$usercoll = $db->users;

	if (strlen($app->request->post('requestId')) == 24) {

		$where = [
			'ProductList.tpid' => (String) $userId,
			'ProductList._id' => $app->request->post('itemId'),
			'ProductList.status' => 'delivered',
			'_id' => new MongoId($app->request->post('requestId')),
		];

		$updData = array(
			"ProductList.$.TransporterFeedbcak" => $app->request->post('message'),
			"ProductList.$.TransporterRating" => (int) $app->request->post('rating'),
		);

		$res = $requestCollection->findAndModify($where, array('$set' => $updData));
		if (count($res) > 0) {

			$usercoll->update(array('_id' => $res['RequesterId']), array('$inc' => array('RatingByCount' => 1, 'RatingCount' => (float) $app->request->post('rating'))));

			$response = array('success' => 1, 'msg' => 'Thanks for your feedback.');
		}

	}

	echo echoRespnse(200, $response);
});

/*
 * Function Name :  finish_delivery
 * Description : finish delivery
 * Url : http://192.168.11.101/aq/services/transporter.php/feedback_on_package_delivered
 * Method : POST
 * Header : Apikey, Usertype
 * Parameter : requestId,message,rating
 * Created By : Ravi shukla
 * Create Date : 01-09-2015
 * */
$app->post('/feedback_by_requester', 'requester_auth', function () use ($app) {

	global $db;
	global $userId;

	verifyRequiredParams(array('requestId', 'rating', 'message', 'itemId', 'tpid'));
	$response = array('success' => 0, 'msg' => 'Oops! Something went wrong.');

	$requestCollection = $db->delivery_request;
	$usercoll = $db->users;

	if (strlen($app->request->post('requestId')) == 24 && strlen($app->request->post('tpid')) == 24) {

		$where = [
			'ProductList.tpid' => $app->request->post('tpid'),
			'ProductList._id' => $app->request->post('itemId'),
			'ProductList.status' => 'delivered',
			'RequesterId' => $userId,
			'_id' => new MongoId($app->request->post('requestId')),
		];

		$updData = array(
			"ProductList.$.RequesterFeedbcak" => $app->request->post('message'),
			"ProductList.$.RequesterRating" => (int) $app->request->post('rating'),
		);

		$res = $requestCollection->findAndModify($where, array('$set' => $updData));
		if (count($res) > 0) {

			$usercoll->update(array('_id' => new MongoId($app->request->post('tpid'))), array('$inc' => array('RatingByCount' => 1, 'RatingCount' => (float) $app->request->post('rating'))));

			$response = array('success' => 1, 'msg' => 'Thanks for your feedback.');
		}

	}

	echo echoRespnse(200, $response);
});

/*
 * Function Name :  cancel_delivery
 * Description : finish delivery
 * Url : http://192.168.11.101/newlara/services/online_request.php/cancel_delivery
 * Method : POST
 * Header : apiKey, userType
 * Parameter : requestId
 * rejectBy (transporter,requester),
 * returnType (osreturn,ipayment)
 * Created By : Ravi shukla
 * Create Date : 22-09-2016
 * */
$app->post('/cancel_delivery', 'transporter_auth', function () use ($app) {

	global $db;
	global $userId;
	global $userName;
	require_once 'vendor/mailer/Email.php';

	verifyRequiredParams(array('requestId', 'rejectBy', 'returnType', 'itemId'));
	$response = array('success' => 0, 'msg' => 'Invalid request.');
	$noticoll = $db->notification;
	$Usercoll = $db->users;
	$collection = $db->delivery_request;

	if (strlen($app->request->post('requestId')) == 24) {

		$PackageId = new MongoId($app->request->post('requestId'));
		$newReturnaddress = '';
		$info = $collection->find(['_id' => $PackageId, 'ProductList._id' => $app->request->post('itemId')],
			['ProductList.$', 'RequesterId']);
		if ($info->count() > 0) {

			$info = $info->getNext();
			$tpid = (strlen($info['ProductList'][0]['tpid']) == 24) ? new MongoId($info['ProductList'][0]['tpid']) : '';
			$field = array('NumberId', 'Email', 'Name', 'Image', 'NotificationId', 'DeviceType', 'EmailStatus', 'SenderNoficationStatus', 'TPSetting');
			$user = $Usercoll->find(array('_id' => array('$in' => array($info['RequesterId'], $tpid))), $field);
			if ($user->count() == 2) {
				$transporter = array();
				$requester = $user->getNext();
				if ($requester['_id'] == $tpid) {
					$transporter = $requester;
					$requester = $user->getNext();
				} else {
					$transporter = $user->getNext();
				}

				$NotificationMessage = 'Your package "' . $info['ProductList'][0]['product_name'] . '" has been canceled by ' . $requester['Name'] . '';
				$emailReasonforCancaletion = "Rejectby : " . ucfirst($app->request->post('rejectBy')) . ", Retrun type: " . $app->request->post('returnType');

				$updData = array(
					"ProductList.$.status" => 'cancel',
					"ProductList.$.RejectBy" => strtolower($app->request->post('rejectBy')),
					"ProductList.$.ReturnType" => $app->request->post('returnType'),
					"ProductList.$.ReceiptImage" => "",
					"ProductList.$.RejectTime" => new MongoDate(),
					"ProductList.$.TrackingNumber" => $app->request->post('trackingNumber'),
					"ProductList.$.TransporterMessage" => $app->request->post('message'),
					"ProductList.$.CancelDate" => new MongoDate(),
				);

				if (isset($_FILES['receipt']['name']) && @$_FILES['receipt']['name'] != '') {
					$exts = explode('.', $_FILES['receipt']['name']);
					$ext = $exts[count($exts) - 1];
					$profileImage = "receipt" . rand(2154, 45454) . time() . ".$ext";
					if ($ext == 'gif' || $ext == 'GIF' || $ext == 'png' || $ext == 'PNG' || $ext == 'jpg' || $ext == 'JPG' || $ext == 'JPEG' || $ext == 'jpeg') {
						$newpath = FILE_URL . "package/{$profileImage}";
						if (move_uploaded_file($_FILES['receipt']['tmp_name'], $newpath)) {
							$updData['ProductList.$.ReceiptImage'] = "package/{$profileImage}";
						}
					}
				}
				if ($app->request->post('returnType') == 'osreturn') {

					verifyRequiredParams(array('trackingNumber'));

				} else if ($app->request->post('returnType') == 'ipayment') {

					$NotificationMessage = sprintf('We regret to inform you, your package delivery, %s was not delivered. Aquantuo is continuing to look into the issue. Please contact us at %s should you have any questions',
						$info['ProductList'][0]['product_name'],
						'support@aquantuo.com'
					);

				} else if (strtolower($app->request->post('returnType')) == 'creturn' && strtolower($app->request->post('rejectBy')) == 'requester') {
					verifyRequiredParams(array('returnDate', 'returnTime'));
					if ($app->request->post('ReturnDate') != '') {
						$updData['ProductList.$.CancelReturnDate'] = new MongoDate(strtotime($app->request->post('returnDate') . " " . $app->request->post('returnTime')));
					}
					$NotificationMessage = sprintf('Your Package "%s" failed delivery at the specified address. Your package would be returned by to the address specified at the time of the post. Additional fees may apply. Carrier Message: "%s"',
						$info['ProductTitle'],
						$updData['TransporterMessage']
					);

				} else if ($app->request->post('returnType') == 'osreturn' && $app->request->post('rejectBy') == 'requester') {
					verifyRequiredParams(array('message', 'trackingNumber'));

					$NotificationMessage = sprintf('Your Package "%s" has been canceled by "%s". Carrier Message: %s',
						$info['ProductList'][0]['product_name'],
						$info['ProductList'][0]['tpName'],
						$updData['ProductList.$.TransporterMessage']
					);
				}
				//,'TransporterId'=>$userId,'Status'=>'on_delivery'

				$res = $collection->update(array(
					'_id' => $PackageId,
					'ProductList.tpid' => (string) $userId,
					'ProductList._id' => $app->request->post('itemId'),
				), array('$set' => $updData));

				if ($res['updatedExisting'] > 0) {

					// Manage outer status
					require_once ('Slim/library/ReqStatus.php');

					update_status($app->request->post('requestId'));

					$NotiData = array(
						'NotificationTitle' => "Package Canceled",
						'NotificationMessage' => $NotificationMessage,
						'NotificationShortMessage' => 'Your package "' . $info['ProductList'][0]['product_name'] . '" has been canceled.',
						'NotificationType' => "BUY_FOR_ME_CANCEL_DELIVERY",
						'NotificationRequestNumber' => $info['ProductList'][0]['package_id'],
						'Date' => new MongoDate(),
						'GroupTo' => "requester",
					);
					$noticoll->insert($NotiData);

					// Email for transporter
					if (@$transporter['EmailStatus'] == 'on' && @$transporter['TPSetting'] == 'on') {
						$ETemplate = array(
							"to" => $transporter['Email'],
							"replace" => array(
								"[PACKAGETITLE]" => $info['ProductList'][0]['product_name'],
								"[REASON]" => $NotificationMessage,
							),
						);
						send_mail('56224b45e4b0d4bc235582c0', $ETemplate);
						// End email for transporter
					}

					if (@$requester['EmailStatus'] == 'on') {
						$eArray = array(
							'to' => $requester['Email'],
							'replace' => array(
								'[USERNAME]' => $requester['Name'],
								'[PACKAGETITLE]' => $info['ProductList'][0]['product_name'],
								"[REASON]" => $emailReasonforCancaletion,
							),
						);
						send_mail('56224b45e4b0d4bc235582c0', $eArray);
					}

					if (@$requester['EmailStatus'] == 'on') {
						//Send push notification
						require_once 'vendor/Notification/Notification.php';
						$pushnoti = new Notification();
						$pushnoti->setValue('title', 'Your request has been cancled.');
						$pushnoti->setValue('message', $NotificationMessage);
						$pushnoti->setValue('location', 'BUY_FOR_ME_CANCEL_DELIVERY');
						$pushnoti->setValue('locationkey', (string) $PackageId);

						$pushnoti->add_user($requester['NotificationId'], $requester['DeviceType']);
						$pushnoti->fire();
						// End send push notification
					}

					$response['success'] = 1;
					$response['msg'] = "Package canceled successfully.";
				} else {
					$response['msg'] = "Failed to cancel package.";
				}
			} else {
				$response['msg'] = "Failed to cancel package.";
			}
		} else {
			$response['msg'] = "Invalid package.";
		}
	}
	echo echoRespnse(200, $response);
});

/*
 * Function Name :  cancel_delivery
 * Description : cancel request by sender
 * Url : http://192.168.11.101/aq/services/requester.php/cancel_delivery
 * Header : Apikey, Userype
 * Method : POST
 * Parameter : requestid,type
 * Created By : Ravi shukla
 * Create Date : 27-11-2015
 * */

$app->post('/cancel_delivery_for_requester', 'requester_auth', function () use ($app) {

	global $db;
	global $userId;
	verifyRequiredParams(array('requestId'));
	$response = array('success' => 0, 'msg' => 'Fail to cancel delivery.');

	if (strlen($app->request->post('requestId')) == 24) {
		$collection = $db->delivery_request;
		$PackageId = new MongoId($app->request->post('requestId'));
		$res = $collection->find(array('_id' => $PackageId, 'RequesterId' => $userId));
		if ($res->count() > 0) {
			$result = (Object) $res->getNext();

			$updData = array("Status" => 'deleted');
			$res = $collection->update(['_id' => $PackageId], ['$set' => ['Status' => 'deleted']]);

			if ($res['ok'] == 1) {
				$response['success'] = 1;
				$response['msg'] = "Package removed successfully.";
			} else {
				$response['msg'] = "Fail to canceled package.";
			}
		}
	}

	echo echoRespnse(200, $response);
});

$app->run();
