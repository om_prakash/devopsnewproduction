<?php
require 'Slim/Slim.php';
require 'Slim/config/DbConnect.php';
require 'Slim/library/Common.php';

\Slim\Slim::registerAutoloader();
$app = new \Slim\Slim();

$userId = '';
$userType = '';

$app->get('/suggested_weights', function () use ($app) {
    $response = array('success' => 0, 'msg' => '');
    verifyRequiredParams(array('type'));
    global $db;

    $collection = $db->item;

    if($app->request->get('type') == 'list'){
        $info = $collection->find(array('Status' => 'Active'),['Status','item_name','kgWeight','lbsWeight'])->limit(5);
    }else{
        $info = $collection->find(array('Status' => 'Active'),['Status','item_name','kgWeight','lbsWeight']);
    }

    
    
    foreach ($info as $key ) {
        $array[]=[
        'Status'=>@$key['Status'],
        'item_name'=>@$key['item_name'],
        'kgWeight'=> number_format( @$key['kgWeight'],2),
        'lbsWeight'=> number_format( @$key['lbsWeight'],2),
        ];
    }
    $response = array('success' => 1, 'result' => $array);
    //print_r($array); die;
    echoRespnse(200, $response);
});

$app->get('/nearby_transporter', function () use ($app) {
    $response = array('success' => 0, 'msg' => '');
    verifyRequiredParams(array('lat', 'long', 'userId'));

	global $db;
    global $userId;
    $collection = $db->trips;
    $usercollection = $db->users;

    $where = array(
    	"SourceDate" => array('$gt' => new MongoDate())
    );

    $field = array('_id','TransporterId');
    $tp_ids= array();

    //$tripData = $collection->find($where, $field)->groupBy('TransporterId')->distinct();
    //$tripData = $collection->find($where, $field);
    $tripData = $collection->distinct('TransporterId', $where);
    foreach ($tripData as $key ) {
    	$tp_ids[]=  $key ;
    }
    

    $where2 = array(
    	"_id" => array('$in' => $tp_ids,'$ne'=> $app->request->get('userId')),
    	"TransporterStatus"=> "active",
    	"CurrentLocation.0"=> ['$nin' => [0, '']],
    	"CurrentLocation"=> [
    		'$near'=>[
    			'$geometry'=>[
    				'type' => 'Point',
    				'coordinates' => [floatval($app->request->get('long')) ,floatval($app->request->get('lat'))],
    			],
    			'$maxDistance'=> 100000000,
    			'$uniqueDocs'=> 1,
    		]
    	]
    );
    $transporter = $usercollection->find($where2,array('TransporterType','DeliveryArea','Image','FirstName','RatingCount','RatingByCount','CurrentLocation'));
    $result = array();

    if($transporter->count() > 0){

    	foreach ($transporter as $key => $value) {
    		$result[]=[
    			"Lat" => $value['CurrentLocation'][1],
                "Lng"=> $value['CurrentLocation'][0],
                "UserId"=> (string) $value['_id'],
                "UserImage"=>$value['Image'] ,
                "TransporterType"=> $value['TransporterType'],
                "Name"=> $value['FirstName'],
                "Rating"=> ($value['RatingByCount'] > 0)? (($value['RatingCount'] * 20) / ($value['RatingByCount'])) :0,
    		];
    	}

    	$response = array('success' => 1, 'result' => $result,'msg'=>'Listing');
    }else{
    	$response = array('success' => 0,'msg'=>'No Transporter near you as of now. You can still list your item and we will help get it to you.','result'=>$result);
    }

   	echoRespnse(200, $response);
   	$app->stop();
});


$app->get('/new_request_for_transporter_map', function () use ($app) {
    $response = array('success' => 0, 'msg' => '');
    verifyRequiredParams(array('lat', 'long', 'userId'));
	global $db;
    global $userId;
    $collection = $db->configuration;
    $request = $db->delivery_request;
    

    
    $result =array();
    $area = $collection->find(array('_id' => new MongoId('5673e33e6734c4f874685c84') ),['CoverageArea']);


    if($area->count() > 0){
    	$area = $area->getNext();
    	$where = array(
	    	"RequesterId"=> [
	            '$ne'=> new MongoId($app->request->get('userId')) 
	        ],
	        "Status"=> "ready",
	        "TransporterId"=> "",
	        "PickupLatLong.0"=> [
	            '$nin'=> [0, '']
	        ],
	        "RequestType"=> [
	            '$in'=> ['delivery']
	        ],
	        /*"PickupLatLong"=> [
	            '$near'=> [
	            	'$geometry'=>[
	            		"type"=>"Point",
	            		"coordinates"=>[floatval($app->request->get('long')) ,floatval($app->request->get('lat'))]
	            	],
	            	'$maxDistance' =>floatval($area['CoverageArea']) ,
	            	'$uniqueDocs' => 1

	            ]
	        ],*/
            //"ProductList" => array('$elemMatch' => array('tpid' => $app->request->get('userId')))

	    );

       

    	$requests = $request->find($where,['DeliveryFullAddress','PickupFullAddress','DeliveryLatLong','PackageId',
                    'PickupDate','ProductTitle','PickupCity','DeliveryCity','EnterOn','ProductImage','PickupLatLong',
                    'TransporterName','Status','TotalCost','ProductList','request_version','RequestType'])->sort(array('_id' => -1));

    	if($requests->count() > 0){
    		foreach ($requests as $key => $value) {

    			$result[]=[
    				"Requestid" => (string) $value['_id'],
                    "RequestType"=> $value['RequestType'], 
    				"PackageNumber" => $value['PackageId'],
    				"RequesterName" => @$value['TransporterName'],
    				"ProductTitle" => $value['ProductTitle'],
    				//"Date" => (($value['PickupDate']->sec)/1000),
                    "Date" => @$value['PickupDate']->sec,
    				"RequestLocation" => $value['PickupCity'] .' to '.$value['DeliveryCity'],
    				"PickupLat" => $value['PickupLatLong'][1],
    				"PickupLong" => $value['PickupLatLong'][0],
    				"DeliveryLat" => $value['DeliveryLatLong'][1],
    				"DeliveryLong" => $value['DeliveryLatLong'][0],
    				"ProductImage" => @$value['ProductImage'],
    				"Amount" => $value['TotalCost'],
    				"Status" => $value['Status'],
    				"StatusTitle" =>  get_status_title($value['Status']),
    				"PickupAddress" =>  $value['PickupFullAddress'],
    				"DeliveryAddress" =>  $value['DeliveryFullAddress'],
                    'ProductList'=> @$value['ProductList'],
                    'request_version'=>@$value['request_version'],
    			];              
    		}

    		$response = array('success' => 1, 'result' => $result,'msg'=>'New delivery request');


    	}else{
    		$response = array('success' => 0, 'result' => $result,'msg'=>'No package found for delivery');
    	}           
    }else{
    		$response = array('success' => 0, 'result' => $result,'msg'=>'No package found for delivery');
    }

    echoRespnse(200, $response);
   	$app->stop();
    
});



$app->get('/new_request_for_transporter', function () use ($app) {
    $response = array('success' => 0, 'msg' => '');
    verifyRequiredParams(array('lat', 'long', 'pageno', 'userId'));
    global $db;
    global $userId;
    $request    =   $db->delivery_request;
    $pageno     =
    $limit      =   20;
    $start      =   (($app->request->get('pageno') -1) * $limit);
    $where      =   array(
        "RequesterId" => [
                '$ne' => $app->request->get('userId')
        ],
        "Status"=>"ready",
        "RequestType"=> "delivery", 
        "PickupLatLong"=> [
            '$near'=> [
                '$geometry'=> [
                    "type"=> "Point",
                    "coordinates"=>[floatval($app->request->get('long')) ,floatval($app->request->get('lat'))]
                ],
                '$maxDistance'=> 100000000,
                '$uniqueDocs'=> 1
            ]
        ]
    );

    $requests = $request->find($where,array('TransporterType','RequesterName','ProductTitle','PickupCity','DeliveryCity','EnterOn','ProductImage','PickupLatLong'));
    $result = array();

    if($requests->count() > 0){

        foreach ($requests as $key => $value) {
            $result[]=[
                "Requestid"=> (string) $value['_id'],
                "ProductTitle"=> $value['ProductTitle'],
                "RequestLocation"=> @$value['PickupCity']." to ".$value['DeliveryCity'],
                "ProductImage"=> @$value['ProductImage'],
                "PickupLat"=> $value['PickupLatLong'][1],
                "PickupLong"=> $value['PickupLatLong'][0],

            ];
        }

        $response = array('success' => 1, 'result' => $result,'msg'=>'Listing');
    }else{
        $response = array('success' => 1, 'result' => $result,'msg'=>'No package found for delivery');
    }

    echoRespnse(200, $response);

});

$app->get('/new', function () use ($app) {
    global $db;
    global $userId;
    $collection = $db->trips;

    //$tripData = $collection->distinct('TransporterId', $where);
    $data = $collection->group([
        /*[
            '$key'=> ["SourceDate" => array('$gt' => new MongoDate())]
        ],*/
        [
            '$key' => ['TransporterId'=>1]
        ],
        [
            '$cond'=>["SourceDate" =>array('$gt' => new MongoDate())]
        ],
        [
            '$initial'=> []
        ]
        
        /*[
            '$project'=>[
                'TransporterName'=>1,
                'TransporterId'=>1,
            ]
        ]*/
    ]);
    $re =array();
    foreach ($data as $key => $value) {
        $re[]=$value;
    }

    print_r($re); die;

});

function get_status_title($status, $requesttype = '')
{
    switch ($status) {
        case 'ready':
            return 'Ready';
            break;
        case 'trip_pending':
            return 'Pending';
            break;
        case 'trip_ready':
            return 'Ready';
            break;
        case 'out_for_delivery':
            return 'Out for Delivery';
            break;
        case 'out_for_pickup':
            return 'Out for Pickup';
            break;
        case 'accepted':
            return 'Accepted';
            break;
        case 'delivered':
            return 'Delivered';
            break;
        case 'cancel':
            return 'Canceled';
            break;
        case 'cancel_by_admin':
            return 'Cancelled by Admin';
            break;
        case 'purchased':
            return ($requesttype == 'online') ? 'Item Received' : 'Item Purchased';
            break;
        case 'assign':
            return 'Transporter Assigned';
            break;
        case 'paid':
            return ($requesttype == 'online') ? 'Waiting for Item to be Received' : 'Pending Purchase';
            break;
            break;
        case 'reviewed':
            return 'Reviewed';
            break;
        case 'not_purchased':
            return 'Waiting for your payment';
            break;
        case 'item_received':
            return 'Item Received';
            break;
        default:
            return 'Pending';

    }
}



$app->run();