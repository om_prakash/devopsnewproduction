<?php
/**
 * Step 1: Require the Slim Framework
 *
 * If you are not using Composer, you need to require the
 * Slim Framework and register its PSR-0 autoloader.
 *
 * If you are using Composer, you can skip this step.
 *
 * Filename Name : authentication.php
 * File Path : services/authentication.php
 * Description : This file contains method related to userinformation.
 * Author: Ravi shukla
 * Created Date : 22-08-2015
 * Library : Email,DbConnect
 *
 * */

require 'Slim/Slim.php';
require 'Slim/config/DbConnect.php';
require 'Slim/library/Common.php';
require_once 'Slim/config/lang.php';

\Slim\Slim::registerAutoloader();

/**
 * Step 2: Instantiate a Slim application
 *
 * This example instantiates a Slim application using
 * its default settings. However, you will usually configure
 * your Slim application now by passing an associative array
 * of setting names and values into the application constructor.
 */
$app = new \Slim\Slim();

$userId = '';
$userType = '';

/*
 * Function Name    : my_deliveries
 * Description      : getting list of current_delivery and history of different Status.(transporter).
 * Url              : http://192.168.11.101/aq/services/transporter.php/my_deliveries?pageno=1&type=current_delivery
 * Method           : GET
 * Parameter        : pageno,type
 * Current delivery : Status : 'accepted', 'out_for_pickup', 'out_for_delivery'.
 * History          : Status : 'accepted', 'cancel'.
 * Created By       : Pankaj Gawande.
 * Create Date      : 23-11-2015.
 *
 */

$app->get('/my_deliveries', 'transporter_auth', function () use ($app) {
    global $db;
    global $userId;
    verifyRequiredParams(array('pageno'));
    $limit = 25;
    $start = (((Int) $app->request->get('pageno')) - 1) * $limit;
    $where = array('TransporterId' => $userId);
    $orWhere = ['ProductList.tpid' => (String) $userId];
    $response = array('success' => 0, 'msg' => 'No request(s) found', 'result' => array(), 'pagination' => $limit);
    $collection = $db->delivery_request;
    $usercollection = $db->users;

    if ($app->request->get('type') == 'current_delivery') {
        $where['Status'] = array('$in' => array('accepted', 'out_for_pickup', 'out_for_delivery', 'ready'));
        $orWhere['ProductList.status'] = array('$in' => array('accepted', 'out_for_pickup', 'out_for_delivery', 'ready', 'purchased', 'paid', 'assign'));
    } else if ($app->request->get('type') == 'history') {
        $where['Status'] = array('$in' => array('delivered', 'cancel'));
        $orWhere['ProductList.status'] = ['$in' => ['delivered', 'cancel']];
    } else {
        $where['Status'] = "no_record";
    }

    $field = array('RequesterName', 'RequesterId', 'TripId', 'DeliveryCity', 'PickupLatLong', '_id', 'PickupCity', 'ProductTitle', 'ProductImage', 'Amount', 'Status', 'PackageNumber', 'CarrierName', 'DeliveryLatLong', 'EnterOn', 'PickupDate', 'PickupFullAddress', 'DeliveryFullAddress', 'ReturnFullAddress', 'RequestType', 'ProductList', 'DeliveryCity', 'PickupAddress');
    $requestData = $collection->find(['$or' => [$where, $orWhere]], $field)->sort(array('UpdateOn' => -1))->skip($start)->limit($limit);
    if ($requestData->count() > 0) {
        // get transporter data by

        $RequesterId = array();
        $Requesterdata = array();
        foreach ($requestData as $val) {
            $RequesterId[] = $val['RequesterId'];
        }

        $Requesterinfo = $usercollection->find(array('_id' => array('$in' => $RequesterId)), array('ChatName', 'Name', 'Image'));

        if ($Requesterinfo->count() > 0) {
            foreach ($Requesterinfo as $key) {
                $Requesterdata[(string) $key['_id']] = (Object) $key;
            }
        }
        // End get transporter data

        foreach ($requestData as $val) {
            $response['success'] = 1;
            $response['msg'] = 'Records found';

            $ChatName = '';
            $ChatUserName = '';
            $ChatUsrImage = '';
            if (isset($Requesterdata[(String) $val['RequesterId']])) {
                $ChatName = $Requesterdata[(String) $val['RequesterId']]->ChatName;
                $ChatUserName = $Requesterdata[(String) $val['RequesterId']]->Name;
                $ChatUsrImage = $Requesterdata[(String) $val['RequesterId']]->Image;
            }

            if (isset($val['RequestType']) && in_array(@$val['RequestType'], ['online', 'buy_for_me'])) {
                foreach ($val['ProductList'] as $plk => $plv) {
                    if ($plv['tpid'] == (String) $userId) {
                        $response['result'][] = [
                            "Requestid" => (string) $val['_id'],
                            "Itemid" => (string) $plv['_id'],
                            "ProductTitle" => @$plv['product_name'],
                            "RequestLocation" => "Aquantuo to {$val['DeliveryCity']}",
                            "ProductImage" => $plv['image'],
                            "Date" => isset($val['EnterOn']->sec) ? $val['EnterOn']->sec : '',
                            "PackageNumber" => @$plv['package_id'],
                            "RequesterName" => @$val['RequesterName'],
                            "Amount" => $plv['shippingCost'] + $plv['insurance'],
                            "Status" => $plv['status'],
                            "StatusTitle" => get_status_title($plv['status']),
                            "PickupAddress" => @$val['PickupAddress'],
                            "PickupLat" => @$val['PickupLatLong'][1],
                            "PickupLong" => @$val['PickupLatLong'][0],
                            "DeliveryLat" => @$val['DeliveryLatLong'][1],
                            "DeliveryLong" => @$val['DeliveryLatLong'][0],
                            "DeliveryAddress" => $val['DeliveryFullAddress'],
                            "ReturnAddress" => @$val['PickupAddress'],
                            "TripId" => "",
                            "ChatUserId" => (String) $val['RequesterId'],
                            "ChatUserName" => $ChatUserName,
                            "ChatUsrImage" => $ChatUsrImage,
                            "ChatName" => $ChatName,
                            "RequestType" => $val['RequestType'],
                        ];
                    }
                }

            } else {
                $response['result'][] = array(
                    "Requestid" => (string) $val['_id'],
                    "ProductTitle" => @$val['ProductTitle'],
                    "RequestLocation" => $val['PickupCity'] . " to " . $val['DeliveryCity'],
                    "PickupLat" => @$val['PickupLatLong'][1],
                    "PickupLong" => @$val['PickupLatLong'][0],
                    "ProductImage" => $val['ProductImage'],
                    "PickupDate" => $val['PickupDate']->sec,
                    "Date" => $val['EnterOn']->sec,
                    "PackageNumber" => @$val['PackageNumber'],
                    "RequesterName" => @$val['CarrierName'],
                    "Amount" => @$val['TotalCost'],
                    "Status" => $val['Status'],
                    "StatusTitle" => get_status_title($val['Status']),
                    "DeliveryLat" => isset($val['DeliveryLatLong'][1]) ? $val['DeliveryLatLong'][1] : '',
                    "DeliveryLong" => isset($val['DeliveryLatLong'][0]) ? $val['DeliveryLatLong'][0] : '',
                    "PickupAddress" => $val['PickupFullAddress'],
                    "DeliveryAddress" => $val['DeliveryFullAddress'],
                    "ReturnAddress" => $val['ReturnFullAddress'],
                    "TripId" => (String) @$val['TripId'],
                    "ChatUserId" => (String) $val['RequesterId'],
                    "ChatUserName" => $ChatUserName,
                    "ChatUsrImage" => $ChatUsrImage,
                    "ChatName" => $ChatName,
                    "RequestType" => isset($val['RequestType']) ? $val['RequestType'] : 'delivery',
                );
            }
        }
    }
    echo json_encode($response);
});

/*
 * Function Name     : delivery_details
 * Description         : getting delivery details
 * Url                 : http://192.168.11.101/aq/services/transporter.php/delivery_details
 * Method             : GET
 * Parameter         : Apikey, requestId
 * Created By         : Pankaj Gawande
 * Create Date         : 23-11-2015
 *
 */

$app->get('/delivery_details', 'transporter_auth', function () use ($app) {

    global $db;
    global $userId;
    $response = array('success' => 0, 'msg' => 'Records Not Found');

    if (strlen($app->request->get('requestId')) == 24) {
        $collection = $db->delivery_request;
        $user_coll = $db->users;
        $requestId = new MongoId($app->request->get('requestId'));
        $requestData = $collection->find(array('$or' => array(
            array('_id' => $requestId, 'TransporterId' => $userId),
            array('_id' => $requestId, 'Status' => array('$in' => array('ready'))),
        )));
        //'TransporterId' => '',
        if ($requestData->count() > 0) {
            $requestData = $requestData->getNext();
            if (!empty(trim($requestData['TransporterId'])) && $requestData['TransporterId'] != (String) $userId) {
                $response['msg'] = 'This request has already been accepted by other transporter.';
                echo echoRespnse(200, $response);
                die;
            }
            $response = array('success' => 1, 'msg' => 'Records Found');
            $requestData['id'] = (String) $requestData['_id'];

            $response['result'] = array(
                'Title' => $requestData['ProductTitle'],
                'RequestId' => (String) $requestData['_id'],
                'PackageId' => $requestData['PackageNumber'],
                'RequesterName' => $requestData['RequesterName'],
                'RequesterImage' => "",
                'RequestDate' => $requestData['EnterOn']->sec,
                'PickupDate' => $requestData['PickupDate']->sec,
                'PickupAddress' => $requestData['PickupFullAddress'],
                'PickupCountry' => $requestData['PickupCountry'],
                'PickupPinCode' => $requestData['PickupPinCode'],
                'DeliveryAddress' => $requestData['DeliveryFullAddress'],
                'DeliveryDate' => $requestData['DeliveryDate']->sec,
                'TransporterName' => $requestData['TransporterName'],
                'TransporterId' => (string) $requestData['TransporterId'],
                'PublicPlace' => ucfirst($requestData['PublicPlace']),
                'FlexibleDeliveryDate' => ucfirst($requestData['FlexibleDeliveryDate']),
                'DeliveryCountry' => $requestData['DeliveryCountry'],
                'ReturnAddress' => $requestData['ReturnFullAddress'],
                'ReturnCountry' => $requestData['ReturnCountry'],
                'ReturnPincode' => $requestData['ReturnPincode'],
                'NotDeliveredReturnAddress' => $requestData['NotDelReturnFullAddress'],
                'Weight' => number_format(change_unit($requestData['ProductWeight'], $requestData['ProductWeightUnit'], 'kg'), 2) . ' Kg',
                'Weightlbs' => number_format(change_unit($requestData['ProductWeight'], $requestData['ProductWeightUnit'], 'lbs'), 2) . ' Lbs',
                'Status' => $requestData['Status'],
                'ProductCost' => number_format($requestData['ProductCost'], 2),
                'PackageDimesion' => "L-" . number_format(change_unit($requestData['ProductLength'], $requestData['ProductLengthUnit'], 'inch'), 2) . 'Inch' . ", H-" . number_format(change_unit($requestData['ProductHeight'], $requestData['ProductHeightUnit'], 'inch'), 2) . 'Inch' . ", W-" . number_format(change_unit($requestData['ProductWidth'], $requestData['ProductWidthUnit'], 'inch'), 2) . 'Inch',
                'PackageDimesion2' => "L-" . number_format(change_unit($requestData['ProductLength'], $requestData['ProductLengthUnit'], 'cm'), 0) . 'Cm' . ", H-" . number_format(change_unit($requestData['ProductHeight'], $requestData['ProductHeightUnit'], 'cm'), 0) . 'Cm' . ", W-" . number_format(change_unit($requestData['ProductWidth'], $requestData['ProductWidthUnit'], 'cm'), 0) . 'Cm',
                'Quantity' => $requestData['BoxQuantity'],
                'InsuranceStatus' => ucfirst($requestData['InsuranceStatus']),
                'Description' => $requestData['Description'],
                'PackageCareNote' => $requestData['PackageCareNote'],
                'ShippingCost' => number_format($requestData['ShippingCost'], 2),
                'PackageMaterial' => ($requestData['PackageMaterial'] == 'yes') ? 'Yes' : 'N/A',
                "ProductImage" => $requestData['ProductImage'],
                "OtherImage" => $requestData['OtherImage'],
                "DeliveryStatus" => $requestData['Status'],
                "DeliveryStatusToShow" => get_status_title($requestData['Status']),
                "RequesterPaid" => $requestData['ShippingCost'],
                "AquantuoFess" => $requestData['AquantuoFees'],
                "TransporterEarnings" => ($requestData['ShippingCost'] - $requestData['AquantuoFees']),
                "JournyType" => ucwords(str_replace('_', ' ', @$requestData["JournyType"])),
                "TrackingNumber" => $requestData['TrackingNumber'],
                "RejectBy" => ucfirst($requestData['RejectBy']),
                "ReturnType" => ucfirst($requestData['ReturnType']),
                "ReceiptImage" => $requestData['ReceiptImage'],
                "PackageMaterialShipped" => ucwords(str_replace('_', ' ', @$requestData['PackageMaterialShipped'])),
                "ChatUserId" => (String) $requestData['RequesterId'],
                "ChatUserName" => "",
                "ChatUsrImage" => "",
                "ChatName" => "",
                "TransporterFeedbcak" => @$requestData['TransporterFeedbcak'],
                "TransporterRating" => @$requestData['TransporterRating'],
                "RequesterRating" => @$requestData['RequesterRating'],
                "RequesterFeedbcak" => @$requestData['RequesterFeedbcak'],
                "UserRating" => 0,
                "TransporterMessage" => @$requestData['TransporterMessage'],
                "DeliveredTime" => @$requestData['DeliveredTime']->sec,
                "ReceiverCountrycode" => @$requestData['ReceiverCountrycode'],
                "ReceiverMobileNo" => @$requestData['ReceiverMobileNo'],
            );

            // Get Requester Image
            $requesterdata = $user_coll->find(array('_id' => $requestData['RequesterId']), array('Image', 'ChatName', 'Name', 'Image', 'RatingByCount', 'RatingCount'));
            if ($requesterdata->count() > 0) {
                $requesterdata = $requesterdata->getNext();
                try {
                    $response['result']['RequesterImage'] = @$requesterdata['Image'];
                    $response['result']['ChatUserName'] = @$requesterdata['Name'];
                    $response['result']['ChatUsrImage'] = @$requesterdata['Image'];
                    $response['result']['ChatName'] = @$requesterdata['ChatName'];
                    if (@$requesterdata['RatingCount'] > 0 && @$requesterdata['RatingByCount'] > 0) {
                        $response['result']['UserRating'] = (@$requesterdata['RatingCount'] / @$requesterdata['RatingByCount']);
                    }
                } catch (exxception $e) {}
            }

            array_unshift($response['result']["OtherImage"], $requestData['ProductImage']);
        }
    }
    echo echoRespnse(200, $response);

});

/*
 * Function Name     : delivery_details.
 * Description         : It returns the details of requester.
 * Url                 : http://192.168.11.101/aq/services/requester.php/delivery_details
 * Method             : GET
 * Header           : Apikey
 * Param            : requestid
 * Created By         : Pankaj Gawande
 * Create Date         : 14-12-2015
 *
 */

$app->get('/online-delivery-details', 'transporter_auth', function () use ($app) {
    global $db;
    global $userId;
    verifyRequiredParams(array('requestId', 'itemId'));
    $response = array('success' => 0, 'msg' => 'Records not found');
    $requestCollection = $db->delivery_request;
    $usercollection = $db->users;

    $field = ['ProductList.$', 'RequesterId', 'EnterOn', 'DeliveryDate', 'UpdateOn', 'distance', 'DeliveryFullAddress', 'ReturnAddress', 'ReceiverMobileNo', 'ReceiverCountrycode'];
    $requestData = $requestCollection->find([
        '_id' => new MongoId($app->request->get('requestId')),
        "ProductList" => ['$elemMatch' => [
            '_id' => $app->request->get('itemId'),
            'tpid' => (String) $userId,
        ]],
    ], $field);

    if ($requestData->count() > 0) {
        $requestData = $requestData->getNext();

        // get Transporter information

        $userobj = $db->users;
        $chatdata = (Object) ['Name' => '', 'Image' => '', 'ChatName' => '', 'RatingByCount' => '', 'RatingCount' => ''];
        $userinfo = $userobj->find(['_id' => $requestData['RequesterId']], ['Name', 'Image', 'ChatName', 'RatingCount', 'RatingByCount']);
        if ($userinfo->count() > 0) {
            $chatdata = (Object) $userinfo->getNext();
        }

        $requestData['PackageId'] = (String) $requestData['_id'];
        unset($requestData['_id']);
        $requestData['RequesterId'] = (String) @$requestData['RequesterId'];
        $requestData['EnterOn'] = (String) @$requestData['EnterOn']->sec;
        $requestData['UpdateOn'] = (String) @$requestData['UpdateOn']->sec;
        $requestData['DeliveryDate'] = (String) @$requestData['DeliveryDate']->sec;
        $requestData['ItemPrice'] = 0;
        $requestData['aq_fee'] = 0;
        $requestData['UserRating'] = 0;
        $requestData['StatusTitle'] = 'Pending';

        if ($chatdata->RatingCount > 0 && $chatdata->RatingByCount > 0) {
            $requestData['UserRating'] = $chatdata->RatingCount / $chatdata->RatingByCount;
        }

        foreach ($requestData['ProductList'] as $key => $val) {
            $requestData['ItemPrice'] += $val['price'];
            $requestData['aq_fee'] += $val['aq_fee'];
            $requestData['ProductList'][$key]["ChatUserId"] = $requestData['RequesterId'];
            $requestData['ProductList'][$key]["ChatUserName"] = '';
            $requestData['ProductList'][$key]["ChatUsrImage"] = '';
            $requestData['ProductList'][$key]["ChatName"] = '';
            $requestData['ProductList'][$key]["RequesterName"] = '';
            $requestData['ProductList'][$key]["RequesterImage"] = '';
            $requestData['StatusTitle'] = get_status_title($val['status']);
            $requestData['ProductList'][$key]["UserRating"] = $requestData['UserRating'];
            $requestData['ProductList'][$key]["DeliveredDate"] = isset($val['DeliveredDate']->sec) ? $val['DeliveredDate']->sec : '';

            if (strlen($val['tpid']) == 24) {
                $userinfo = $userobj->find(['_id' => new MongoId($val['tpid'])], ['Name', 'Image', 'ChatName']);
                if ($userinfo->count() > 0) {
                    $userinfo = (Object) $userinfo->getNext();

                    $requestData['ProductList'][$key]["ChatUserName"] = $chatdata->Name;
                    $requestData['ProductList'][$key]["ChatUsrImage"] = $chatdata->Image;
                    $requestData['ProductList'][$key]["ChatName"] = $chatdata->ChatName;

                    $requestData['ProductList'][$key]["RequesterName"] = $chatdata->Name;
                    $requestData['ProductList'][$key]["RequesterImage"] = $chatdata->Image;
                }
            }

        }
        $requestData['distanceShow'] = ($requestData['distance'] > 0) ? number_format($requestData['distance'], 2) . " Miles" : '0 Miles';

        $requestData['insurance'] = isset($requestData['ProductList'][0]['insurance']) ? $requestData['ProductList'][0]['insurance'] : 0;
        $requestData['shippingCost'] = isset($requestData['ProductList'][0]['shippingCost']) ? $requestData['ProductList'][0]['shippingCost'] : 0;
        $requestData['tp_earning'] = isset($requestData['ProductList'][0]['aq_fee']) ? $requestData['shippingCost'] - $requestData['ProductList'][0]['aq_fee'] : 0;

        $requestData['Status'] = isset($requestData['ProductList'][0]['status']) ? $requestData['ProductList'][0]['status'] : 'pending';
        $requestData["StatusTitle"] = get_status_title($requestData['Status']);

        $response['result'] = $requestData;

        $response['success'] = 1;
        $response['msg'] = "Records found";

    }
    echoRespnse(200, $response);
});

$app->get('/buy-for-me-delivery-details', 'transporter_auth', function () use ($app) {
    global $db;
    global $userId;
    verifyRequiredParams(array('requestId', 'itemId'));
    $response = array('success' => 0, 'msg' => 'Records not found');
    $requestCollection = $db->delivery_request;
    $usercollection = $db->users;

    $field = ['ProductList.$', 'RequesterId', 'EnterOn', 'DeliveryDate', 'UpdateOn', 'distance', 'DeliveryFullAddress', 'ReturnAddress', 'ReceiverMobileNo', 'ReceiverCountrycode'];
    $requestData = $requestCollection->find([
        '_id' => new MongoId($app->request->get('requestId')),
        "ProductList._id" => $app->request->get('itemId'),
        //'ProductList.tpid' => (String)$userId,
    ], $field);

    if ($requestData->count() > 0) {
        $requestData = $requestData->getNext();

        if ($requestData['ProductList'][0]['tpid'] == (String) $userId) {
            // get Transporter information

            $userobj = $db->users;
            $chatdata = (Object) ['Name' => '', 'Image' => '', 'ChatName' => '', 'RatingByCount' => '', 'RatingCount' => ''];
            $userinfo = $userobj->find(['_id' => $requestData['RequesterId']], ['Name', 'Image', 'ChatName', 'RatingCount', 'RatingByCount']);
            if ($userinfo->count() > 0) {
                $chatdata = (Object) $userinfo->getNext();
            }

            $requestData['PackageId'] = (String) $requestData['_id'];
            unset($requestData['_id']);
            $requestData['RequesterId'] = (String) @$requestData['RequesterId'];
            $requestData['EnterOn'] = (String) @$requestData['EnterOn']->sec;
            $requestData['UpdateOn'] = (String) @$requestData['UpdateOn']->sec;
            $requestData['DeliveryDate'] = (String) @$requestData['DeliveryDate']->sec;
            $requestData['ItemPrice'] = 0;
            $requestData['aq_fee'] = 0;
            $requestData['UserRating'] = 0;
            $requestData['StatusTitle'] = 'Pending';

            if ($chatdata->RatingCount > 0 && $chatdata->RatingByCount > 0) {
                $requestData['UserRating'] = $chatdata->RatingCount / $chatdata->RatingByCount;
            }

            foreach ($requestData['ProductList'] as $key => $val) {
                $requestData['ItemPrice'] += $val['price'];
                $requestData['aq_fee'] += $val['aq_fee'];
                $requestData['ProductList'][$key]['price'] = number_format($val['price'], 2);
                $requestData['ProductList'][$key]['travelMode'] = ucfirst($val['travelMode']);
                $requestData['ProductList'][$key]["ChatUserId"] = $requestData['RequesterId'];
                $requestData['ProductList'][$key]["ChatUserName"] = '';
                $requestData['ProductList'][$key]["ChatUsrImage"] = '';
                $requestData['ProductList'][$key]["ChatName"] = '';
                $requestData['StatusTitle'] = get_status_title($val['status']);
                $requestData['ProductList'][$key]["UserRating"] = $requestData['UserRating'];

                $requestData['ProductList'][$key]['RequesterName'] = '';
                $requestData['ProductList'][$key]['RequesterImage'] = '';
                $requestData['ProductList'][$key]['ChatUserName'] = '';
                $requestData['ProductList'][$key]['ChatUsrImage'] = '';
                $requestData['ProductList'][$key]['ChatName'] = '';
                $requestData['ProductList'][$key]['UserRating'] = '';

                $requestData['ProductList'][$key]["DeliveredDate"] = isset($val['DeliveredDate']->sec) ? $val['DeliveredDate']->sec : '';

                if (strlen($val['tpid']) == 24) {
                    $userinfo = $userobj->find(['_id' => new MongoId($val['tpid'])], ['Name', 'Image', 'ChatName']);
                    if ($userinfo->count() > 0) {
                        $userinfo = (Object) $userinfo->getNext();

                        $requestData['ProductList'][$key]['RequesterName'] = $chatdata->Name;
                        $requestData['ProductList'][$key]['RequesterImage'] = $chatdata->Image;

                        $requestData['ProductList'][$key]["ChatUserName"] = $chatdata->Name;
                        $requestData['ProductList'][$key]["ChatUsrImage"] = $chatdata->Image;
                        $requestData['ProductList'][$key]["ChatName"] = $chatdata->ChatName;
                    }
                }

            }
            $requestData['distanceShow'] = ($requestData['distance'] > 0) ? number_format($requestData['distance'], 2) . " Miles" : '0 Miles';

            $requestData['insurance'] = isset($requestData['ProductList'][0]['insurance']) ? $requestData['ProductList'][0]['insurance'] : 0;
            $requestData['shippingCost'] = isset($requestData['ProductList'][0]['shippingCost']) ? $requestData['ProductList'][0]['shippingCost'] : 0;
            $requestData['tp_earning'] = isset($requestData['ProductList'][0]['aq_fee']) ? $requestData['shippingCost'] - $requestData['ProductList'][0]['aq_fee'] : 0;

            $requestData['Status'] = isset($requestData['ProductList'][0]['status']) ? $requestData['ProductList'][0]['status'] : 'pending';
            $requestData["StatusTitle"] = get_status_title($requestData['Status']);

            $response['result'] = $requestData;

            $response['success'] = 1;
            $response['msg'] = "Records found";
        }

    }
    echoRespnse(200, $response);
});

/*
 * Function Name     : create_trip_individual
 * Description         : This function insert source, destination address and categories details of individual(TR-type).
 * Url                 : http://192.168.11.101/aq/services/transporter.php/create_trip_individual.
 * Method             : POST
 * header(string)    : Apikey
 * Parameter         : transporterName,sourceAddress,sourceCountry,sourceState,sourceCity,sourceDate,destiAddress,destiCountry,dstiState,destiCity,destiDate,selectCategory,weight,unit,description
 * Created By         : Pankaj Gawande
 * Create Date         : 24-11-2015
 *
 */

$app->post('/create_trip_individual', 'transporter_auth', function () use ($app) {
    global $db;
    global $userId;
    verifyRequiredParams(array('transporterName', 'sourceAddress', 'sourceCountry', 'sourceCity', 'destiAddress', 'destiCountry', 'destiCity', 'sourceDate', 'destiDate', 'selectCategory'));

    $userColl = $db->users;
    $userObj = $userColl->find(['_id' => $userId], ["RatingCount", "RatingByCount", "Image"]);

    if ($userObj->count() > 0) {
        $userObj = (Object) $userObj->getNext();

        $response = array('success' => 0, 'msg' => 'Records not inserted');
        $tripsCollection = $db->trips;

        $insertData = array(
            "TransporterId" => $userId,
            "TransporterName" => $app->request->post('transporterName'),
            "TPImage" => $userObj->Image,
            "TPRating" => 0,
            "SourceFullAddress" => get_formatted_address([
                $app->request->post('sourceAddress'),
                $app->request->post('sourceCity'),
                $app->request->post('sourceState'),
                $app->request->post('sourceCountry'),
            ]),
            "SourceAddress" => $app->request->post('sourceAddress'),
            "SourceCity" => $app->request->post('sourceCity'),
            "SourceState" => $app->request->post('sourceState'),
            "SourceCountry" => $app->request->post('sourceCountry'),
            "DestFullAddress" => get_formatted_address([
                $app->request->post('destiAddress'),
                $app->request->post('destiCity'),
                $app->request->post('dstiState'),
                $app->request->post('destiCountry'),
            ]),

            "DestiAddress" => $app->request->post('destiAddress'),
            "DestiCity" => $app->request->post('destiCity'),
            "DestiState" => $app->request->post('dstiState'),
            "DestiCountry" => $app->request->post('destiCountry'),
            "EnterOn" => new MongoDate(),
            "UpdateDate" => new MongoDate(),
            "SourceDate" => ($app->request->post('sourceDate') != '') ? new MongoDate(strtotime(str_replace('/', '-', $app->request->post('sourceDate')))) : '',
            "DestiDate" => ($app->request->post('destiDate') != '') ? new MongoDate(strtotime(str_replace('/', '-', $app->request->post('destiDate')))) : '',
            "SelectCategory" => array_map('trim', explode(',', $app->request->post('selectCategory'))),
            "Weight" => $app->request->post('weight'),
            "Unit" => $app->request->post('unit'),
            "Description" => $app->request->post('description'),
            "ChargeRangeMin" => floatval($app->request->post('chargeRangeMin')),
            "ChargeRangeMax" => floatval($app->request->post('chargeRangeMax')),
            "TravelMode" => $app->request->post('travelMode'),
            "FlightNo" => $app->request->post('flightNo'),
            "Airline" => $app->request->post('airline'),
            "TripType" => ($app->request->post('transporter_type') == 'individual') ? 'individual' : 'business',
            "Status" => 'active',
        );

        if ($userObj->RatingCount > 0 && $userObj->RatingByCount > 0) {
            $insertData['TPRating'] = $userObj->RatingCount / $userObj->RatingByCount;
        }

        $tripsCollection->insert($insertData);
        // Email to admin
        $settingcoll = $db->setting;
        $setting = $settingcoll->find(array('_id' => new MongoId('563b0e31e4b03271a097e1ca')));
        if ($setting->count() > 0) {
            require_once 'vendor/mailer/Email.php';
            $setting = (Object) $setting->getNext();
            $adminETemplate = array(
                "to" => $setting->SupportEmail,
                "replace" => array(
                    "[TPNAME]" => $insertData['TransporterName'],
                    "[SOURCE]" => get_formatted_address(array($insertData['SourceAddress'], $insertData['SourceCity'],
                        $insertData['SourceState'], $insertData['SourceCountry'])),
                    "[SOURCEDATE]" => isset($insertData['SourceDate']->sec) ? date('d M,Y h:i A', $insertData['SourceDate']->sec) : '',
                    "[DESTINATION]" => get_formatted_address(array($insertData['DestiAddress'], $insertData['DestiCity'],
                        $insertData['DestiState'], $insertData['DestiCountry'])),
                    "[DESTINATIONDATE]" => isset($insertData['DestiDate']->sec) ? date('d M,Y h:i A', $insertData['DestiDate']->sec) : '',
                    "[TRAVELMODE]" => $insertData['TravelMode'],
                    "[CATEGORY]" => $app->request->post('selectCategory'),
                    "[FLIGHTNO]"    => isset($insertData['FlightNo'])? $insertData['FlightNo']:"Not Applicable",
                    "[AIRLINE]"    => isset($insertData['Airline'])? $insertData['Airline']:"Not Applicable",
                    "[DESCRIPTION]"    => isset($insertData['Description'])? $insertData['Description']:"Not Applicable",
                ),
            );
            send_mail('56cd43825509251cd677740e', $adminETemplate);

        }
    }
    // End email to admin

    $response = array('success' => 1, 'msg' => 'Trip has been created.');
    echoRespnse(200, $response);
});

/*
 * Function Name     : create_trip_business
 * Description         : it insert countries,categories and other details. (TR-type:business).
 * Url                 : http://192.168.11.101/aq/services/transporter.php/create_trip_business.
 * Method             : POST
 * header(string)    : Apikey
 * Parameter         : transporterName,countriesOperateIn,categories,weight,unit,description.
 * Created By         : Pankaj Gawande
 * Create Date         : 25-11-2015
 *
 */
/*

$app->post('/create_trip_business', 'transporter_auth', function() use($app){
global $db;
global $userId;
verifyRequiredParams(array('transporterName','countriesOperateIn','categories', 'weight', 'unit'));
$tripsCollection = $db->trips;
$response = array("success"=>0, "msg"=>"Records not inserted");

$insertData = array(
"TransporterId"        => $userId,
"TransporterName"    => $app->request->post('transporterName'),
"Countries"         => array_map('trim',explode(',', $app->request->post('countriesOperateIn'))),
"Categories"         => array_map('trim',explode(',', $app->request->post('categories'))),
"TravelMode"        => $app->request->post('travelMode'),
"Weight"             => $app->request->post('weight'),
"Unit"                 => $app->request->post('unit'),
"EnterOn"            => new MongoDate(),
"UpdateDate"        => new MongoDate(),
"Description"         => $app->request->post('description'),
"TripType"            => 'business',
"Status"            => 'active'
);
$tripsCollection->insert($insertData);
// Email to admin
$settingcoll = $db->setting;
$setting = $settingcoll->find(array('_id' => new MongoId('563b0e31e4b03271a097e1ca')));
if($setting->count() > 0)
{
require_once 'vendor/mailer/Email.php';
$setting = (Object)$setting->getNext();
$adminETemplate = array(
"to"         => $setting->SupportEmail,
"replace"    => array(
"[TPNAME]" => $insertData['TransporterName'],
"[COUNTRIES]" => $app->request->post('countriesOperateIn'),
"[CATEGORY]" => $app->request->post('categories'),
"[TRAVELMODE]" => $insertData['travelMode'],
'[WEIGHT]' => "{$insertData['Weight']} {$insertData['Unit']}",
"[DESCRIPTION]" => ucfirst($insertData['Description'])
)
);
send_mail('56cd8d3c5509251cd677740f',$adminETemplate);

}
// End email to admin

$response = array("success"=>1, "msg"=>"Trip has been created.");
echoRespnse(200, $response);

});
 */
/*
 * Function Name     : my_trips_business
 * Description         : it returns countries,categories and other details. (TR-type:business).
 * Url                 : http://192.168.11.101/aq/services/transporter.php/my_trips_business.
 * Method             : GET
 * header(string)    : Apikey
 * Parameter         : pageno
 * Created By         : Pankaj Gawande
 * Create Date         : 25-11-2015
 *

 */
/*
$app->get('/my_trips_business', 'transporter_auth', function() use($app){
global $db;
global $userId;
verifyRequiredParams(array('pageno'));
$limit              = 30;
$start              = (((Int)$app->request->get('pageno')) - 1) * $limit;
$response          = array('success'=>0, "msg"=>"No trip available yet.", "result"=>array());
$tripsCollection = $db->trips;
$where              = array(
'TripType'=>'business',
'TransporterId' => $userId
);
$tripsDetails      = $tripsCollection->find($where, array('TravelMode','Countries','Categories','Weight','Unit','Description','TripDate','Status'))->sort(array('_id'=>-1))->skip($start)->limit($limit);
if($tripsDetails->count() > 0){
$response = array('success'=>1, "msg"=>"Trip list");
foreach ($tripsDetails as $key) {
$response["result"][] = array(
"tripId"        => (String)$key['_id'],
"Countries"     => $key['Countries'],
"Category"         => $key['Categories'],
"Weight"         => $key['Weight'],
"Unit"             => $key['Unit'],
'tripDate'        => @$key['TripDate']->sec,
"TravelMode"    => @$key['TravelMode'],
"Description"     => $key['Description'],
"status"        => ucfirst(@$key['Status'])
);

}
}
echoRespnse(200, $response);
});

 */
/*
 * Function Name     : edit_posting_business
 * Description         : it updates countries,categories and other details. (TR-type:business).
 * Url                 : http://192.168.11.101/aq/services/transporter.php/edit_posting_business.
 * Method             : POST
 * header(string)    : Apikey
 * Parameter         : id,countriesOperateIn,categories,weight,unit,description.
 * Created By         : Pankaj Gawande
 * Create Date         : 25-11-2015
 *
 */

/*$app->post('/edit_posting_business', 'transporter_auth', function() use($app){
global $db;
global $userId;
$response             = array("success"=>0, "msg"=>"Records not Updated");
$tripsCollection     = $db->trips;
$where = array('_id'=> new MongoId($app->request->post('id')));
$updateData = array(
"Countries"     => explode(',', $app->request->post('countriesOperateIn')),
"Categories"     => explode(',', $app->request->post('categories')),
"TravelMode"        => $app->request->post('travelMode'),
"Weight"         => $app->request->post('weight'),
"Unit"             => $app->request->post('unit'),
"UpdateDate"    => new MongoDate(),
"Description"     => $app->request->post('description'),
"TripType"        => 'business'
);

$trips = $tripsCollection->update($where, array('$set'=>$updateData));
if($trips['updatedExisting'] > 0){
$response     = array("success"=>1, "msg"=>"Records Updated");
}
echoRespnse(200, $response);
});*/

/*
 * Function Name     : trip_update_individual
 * Description         : This function Update source, destination address and categories details of individual(TR-type).
 * Url                 : http://192.168.11.101/aq/services/transporter.php/trip_update_individual
 * Method             : POST
 * header(string)    : Apikey
 * Parameter         : sourceCountry,sourceState,sourceCity,destiCountry,dstiState,destiCity,updateDate,selectCategory,weight,unit,                             description
 * Created By         : Pankaj Gawande
 * Create Date         : 24-11-2015
 *
 */

$app->post('/trip_update_individual', 'transporter_auth', function () use ($app) {
    global $db;
    global $userId;
    $response = array('success' => 0, 'msg' => 'Records not updated');
    $userCollection = $db->users;
    $tripsCollection = $db->trips;
    $where = array('_id' => new MongoId($userId));
    $userInfo = $userCollection->find($where, array('TransporterType'));
    $userInfo = $userInfo->getNext();

    $updateData = array(
        "SourceCountry" => $app->request->post('sourceCountry'),
        "SourceState" => $app->request->post('sourceState'),
        "SourceCity" => $app->request->post('sourceCity'),
        "TravelMode" => $app->request->post('travelMode'),
        "DestiCountry" => $app->request->post('destiCountry'),
        "DestiState" => $app->request->post('destiState'),
        "DestiCity" => $app->request->post('destiCity'),
        "UpdateDate" => new MongoDate(),
        "SelectCategory" => $app->request->post('selectCategory'),
        "Weight" => $app->request->post('weight'),
        "Unit" => $app->request->post('unit'),
        "Description" => $app->request->post('description'),
        "TravelMode" => $app->request->post('travelMode'),
        "FlightNo" => $app->request->post('flightNo'),
        "Airline" => $app->request->post('airline'),
    );
    $tripsCollection->update(array('TransporterId' => new MongoId($userId)), array('$set' => $updateData));
    $response = array('success' => 1, 'msg' => 'Records Updated');

    echoRespnse(200, $response);
});

/*
 * Function Name     : trips_details
 * Description         : It returns the trips details.
 * Url                 : http://192.168.11.101/aq/services/transporter.php/trips_list_for_individual
 * Method             : get
 * header(string)    : Apikey
 * Created By         : Pankaj Gawande
 * Create Date         : 24-11-2015
 *
 */

$app->get('/trips_list_for_individual', 'transporter_auth', function () use ($app) {
    global $db;
    global $userId;

    verifyRequiredParams(array('pageno'));

    $response = array('success' => 0, 'msg' => 'Records not found', 'result' => array());
    $collection = $db->trips;

    $limit = 10;
    $start = ((((int) $app->request->get('pageno')) - 1) * $limit);
    $where = array(
        'TransporterId' => $userId,
        'SourceDate' => array('$gt' => new MongoDate()),
    );
    if ($app->request->get('type') == 'history') {
        $where['SourceDate'] = array('$lt' => new MongoDate());
    }
    $tripsDetails = $collection->find($where)->skip($start)->limit($limit)->sort(array('_id' => -1));

    if ($tripsDetails->count() > 0) {
        $response = array('success' => 1, 'msg' => 'Records found', 'result' => array());

        foreach ($tripsDetails as $key) {

            $response['result'][] = array(
                'TripId' => (String) $key['_id'],
                'SourceCountry' => $key['SourceCountry'],
                'SourceState' => $key['SourceState'],
                'SourceCity' => $key['SourceCity'],
                'DestiCountry' => $key['DestiCountry'],
                'DestiState' => $key['DestiState'],
                'DestiCity' => $key['DestiCity'],
                'SourceDate' => $key['SourceDate']->sec,
                'DestiDate' => $key['DestiDate']->sec,
                'SourceTime' => $key['SourceDate']->sec,
                'DestiTime' => $key['DestiDate']->sec,
                'EnterOn' => @$key['SourceDate']->sec,
                'Category' => $key['SelectCategory'],
                'Weight' => $key['Weight'],
                'Unit' => $key['Unit'],
                'ChargeRangeMin' => @$key['ChargeRangeMin'],
                'ChargeRangeMax' => @$key['ChargeRangeMax'],
                'Description' => $key['Description'],
                'SourceAddress' => $key['SourceAddress'],
                'DestiAddress' => $key['DestiAddress'],
                "TravelMode" => @$key['TravelMode'],
                "TripType" => @$key['TripType'],
                "FlightNo" => @$key['FlightNo'],
                "Airline" => @$key['Airline'],
                "status" => ucfirst(@$key['Status']),
            );
        }
    }
    echoRespnse(200, $response);
});

/*
 * Function Name     : trip_detail
 * Description         : It update the trip detail(TransporterType:individual).
 * Url                 : http://192.168.11.101/aq/services/transporter.php/trip_detail
 * Method             : GET
 * Header           : Apikey
 * Created By         : Pankaj Gawande
 * Create Date         : 25-11-2015
 */

$app->get('/trip_detail', 'transporter_auth', function () use ($app) {
    global $db;
    global $userId;
    $response = array('success' => 0, 'msg' => 'Records not found', 'result' => array());
    $collection = $db->trips;
    $usersCollection = $db->users;
    $type = $app->request->get('type');
    $userId = new MongoId($userId);
    $where = array('TransporterId' => $userId);

    $tripsDetails = $collection->find($where);

    foreach ($tripsDetails as $key) {
        $response = array('success' => 1, 'msg' => 'Records found', 'result' => array());
        $response['result'] = array(
            'SourceCountry' => $key['SourceCountry'],
            'SourceState' => $key['SourceState'],
            'SourceCity' => $key['SourceCity'],
            'DestiCountry' => $key['DestiCountry'],
            'DestiState' => $key['DestiState'],
            'DestiCity' => $key['DestiCity'],
            'TripDate' => $key['TripDate']->sec,
            'SelectCategory' => $key['SelectCategory'],
            'Weight' => $key['Weight'],
            'Unit' => $key['Unit'],
            'Description' => $key['Description'],
            "TravelMode" => @$key['TravelMode'],
            "status" => ucfirst(@$key['Status']),
        );
        $tripsDetails = $tripsDetails->getNext();
        $id = $tripsDetails['TransporterId'];
        $userInfo = $usersCollection->find(array('_id' => $id));
        foreach ($userInfo as $key) {
            $response['userInfo'] = array(
                "Name" => $key['Name'],
                "Image" => $key['Image'],
            );
        }
    }
    echoRespnse(200, $response);
});

/*
 * Function Name     : trip_detail_edit.
 * Description         : It update the trip detail(TransporterType:individual).
 * Url                 : http://192.168.11.101/aq/services/transporter.php/trip_detail_edit
 * Method             : POST
 * Header           : Apikey
 * Parameter         : sourceCountry,sourceState,sourceCity,destiCountry,dstiState,destiCity,updateDate,selectCategory,weight,unit,description.
chargeRangeMin,chargeRangeMax
 * Created By         : Pankaj Gawande
 * Create Date         : 24-11-2015
 *
 */

$app->post('/trip_detail_edit', 'transporter_auth', function () use ($app) {
    global $db;
    global $userId;

    $response = array('success' => 0, 'msg' => 'Oops! Something went wrong.');

    if (strlen($app->request->post('tripid')) == 24) {
        $tripsCollection = $db->trips;

        $updateData = array(
            "SourceAddress" => $app->request->post('sourceAddress'),
            "SourceCountry" => $app->request->post('sourceCountry'),
            "SourceState" => $app->request->post('sourceState'),
            "SourceCity" => $app->request->post('sourceCity'),
            "DestiAddress" => $app->request->post('destiAddress'),
            "DestiCountry" => $app->request->post('destiCountry'),
            "DestiState" => $app->request->post('destiState'),
            "DestiCity" => $app->request->post('destiCity'),
            "UpdateDate" => new MongoDate(),
            "SourceDate" => ($app->request->post('sourceDate') != '') ? new MongoDate(strtotime(str_replace('/', '-', $app->request->post('sourceDate')))) : '',
            "DestiDate" => ($app->request->post('destiDate') != '') ? new MongoDate(strtotime(str_replace('/', '-', $app->request->post('destiDate')))) : '',
            "SelectCategory" => array_map('trim', explode(',', $app->request->post('selectCategory'))),
            "ChargeRangeMin" => floatval($app->request->post('chargeRangeMin')),
            "ChargeRangeMax" => floatval($app->request->post('chargeRangeMax')),
            "Weight" => $app->request->post('weight'),
            "Unit" => $app->request->post('unit'),
            "Description" => $app->request->post('description'),
            "TravelMode" => $app->request->post('travelMode'),
            "FlightNo" => $app->request->post('flightNo'),
            "Airline" => $app->request->post('airline'),

        );
        $tripsCollection->update(
            array('_id' => new MongoId($app->request->post('tripid'))),
            array('$set' => $updateData)
        );
        $response = array('success' => 1, 'msg' => 'Trip updated successfully.');
    }
    echoRespnse(200, $response);
});

/*
 * Function Name :  track_location_on_off
 * Description   : On/Off track location
 * Url           : http://192.168.11.101/newlara/services/carrier.php/track_location
 * Method        : POST
 * Header        : apiKey, userType
 * Parameter     : ChangeType,RequestId
 * Created By    : Ajay chaudhary
 * Create Date   : 30-11-2015
 * */
$app->post('/track_location_on_off', 'transporter_auth', function () use ($app) {

    global $db;
    global $userId;
    verifyRequiredParams(array('RequestId', 'ChangeType'));
    $response = array('success' => 0, 'msg' => 'Invalid input.');

    if (strlen($app->request->post('RequestId')) == 24) {
        $collection = $db->delivery_request;

        $PackageId = new MongoId($app->request->post('RequestId'));

        $updData = array(
            "TrackLocation" => ($app->request->post('ChangeType') == 'on') ? 'on' : 'off',
        );
        $res = $collection->update(array('_id' => $PackageId, 'RequesterId' => $userId), array('$set' => $updData));

        if ($res['ok'] == 1) {
            $response['success'] = 1;
            $response['msg'] = "Track location update successfully.";
        } else {
            $response['msg'] = "Fail to update track location.";
        }
    }

    echo echoRespnse(200, $response);
});

/*
 * Function Name : change_package_status
 * Description      : This is used for change status like ready to accepted etc.
 * Url              : http://192.168.11.101/aq/services/transporter.php/change_package_status
 * library          : Notification.php('vendor/Notification/Notification.php')
 * Method          : POST
 * Header          : apiKey, userType
 * Parameter     : requestId,status
 * Created By    : Pankaj Gawande
 * Create Date      : 30-11-2015
 *
 */

$app->post("/change_package_status", 'transporter_auth', function () use ($app) {
    global $db;
    global $userId;
    global $userInformation;

    verifyRequiredParams(array('requestId', 'status'));
    $response = array('success' => 0, "msg" => 'Oops! Someting went wrong.');
    if (in_array(strtolower($app->request->post('status')), array('accepted', 'out_for_pickup', 'out_for_delivery'))) {
        $requestId = new MongoId($app->request->post('requestId'));
        $user_coll = $db->users;
        $settingcoll = $db->setting;
        $collection = $db->delivery_request;
        $notificationColl = $db->notification;
        $collTransaction = $db->transaction;

        $where = array('_id' => $requestId);

        // Verify it is a valid package or not
        $deliveryinfo = $collection->find(array('_id' => $requestId), array('_id', 'RequesterId', 'TransporterId', 'PickupFullAddress', 'DeliveryFullAddress', 'RequesterName', 'ProductTitle', 'PackageId', 'StripeChargeId', 'PackageNumber'));

        if ($deliveryinfo->count() > 0) {
            $deliveryinfo = $deliveryinfo->getNext();

            // Get User information
            $userinfo = $user_coll->find(array('_id' => $deliveryinfo['RequesterId']), array('_id', 'NotificationId', 'DeviceType', 'Name', 'Email', 'EmailStatus'));

            if ($userinfo->count() > 0) {
                $userinfo = (Object) $userinfo->getNext();

                // Verify that package is accepted or not . If accepted then transporter is correct or not
                if ($deliveryinfo['TransporterId'] == '' || $deliveryinfo['TransporterId'] == $userId) {
                    $updateData = array('Status' => strtolower($app->request->post('status')));

                    if (strtolower($app->request->post('status')) == 'accepted') {
                        $updateData['TransporterId'] = $userId;
                        $updateData['TransporterName'] = $userInformation->Name;

                    } else {
                        $where['TransporterId'] = $userId;
                    }
                    // Capture payment

                    if (strtolower($app->request->post('status')) == 'out_for_delivery') {
                        if (!empty(trim($deliveryinfo['StripeChargeId']))) {
                            try {
                                require_once ('vendor/stripe/init.php');

                                $ch = \Stripe\Charge::retrieve($deliveryinfo['StripeChargeId']);
                                $stripeRes = $ch->capture();
                                if ($stripeRes->status == 'succeeded') {
                                    $updateData['PaymentStatus'] = 'Success';
                                }
                            } catch (exception $e) {
                                if (isset($ch->captured) && @$ch->captured == true) {
                                    $updateData['PaymentStatus'] = 'Success';
                                } else {
                                    $response['msg'] = $e->getMessage();
                                    echoRespnse(200, $response);
                                    die;
                                }
                            }
                        }

                    }

                    $updateData['RequesterName'] = @$userinfo->Name;
                    $updateData['UpdateOn'] = new MongoDate();
                    $response['success'] = 0;

                    $requestUpdate = $collection->update($where, array('$set' => $updateData));
                    if ($requestUpdate['updatedExisting'] > 0) {

                        $response = array('success' => 1, "msg" => 'Records Updated');

                        if (strtolower($app->request->post('status')) == 'accepted') {
                            $emailTemplateid = '562226c0e4b0252ad07d079f';
                            $emailidforTP = '5694ce6e5509251cd67773ed';
                            $emailidforadmin = '56ab46f95509251cd67773f3';

                            $title = SEND_ACCEPT_TITLE;
                            
                            $msg =sprintf(SEND_ACCEPT_MSG,$userInformation->Name,$deliveryinfo['ProductTitle']);
                            $adminMsg = sprintf(SEND_ACCEPT_MSG_ADMIN,$userInformation->Name,$deliveryinfo['PickupFullAddress'],$deliveryinfo['DeliveryFullAddress']);

                            $collTransaction->update(array('SuperId' => $requestId), array('$set' => array('Status' => 'done')));
                            $response['msg'] = "You have accepted request successfully.";

                        } else if (strtolower($app->request->post('status')) == 'out_for_pickup') {
                            $emailTemplateid = '5694cb805509251cd67773ec';
                            $emailidforTP = '5694cf3c5509251cd67773ee';

                            $title = SEND_OUTFORPICKUP_TITLE;
                            $msg = SEND_OUTFORPICKUP_MSG;

                            
                            $adminMsg = sprintf(SEND_OUTFORPICKUP_ADMIN,$userInformation->Name,$deliveryinfo['PickupFullAddress'],$deliveryinfo['DeliveryFullAddress']);

                            $response['msg'] = "";

                        } else if (strtolower($app->request->post('status')) == 'out_for_delivery') {
                            $emailTemplateid = '562228e2e4b0252ad07d07a2';
                            $emailidforTP = '5694cffb5509251cd67773ef';
                            $title = SEND_OUTFORDELIVERY_TITLE;

                            $msg = sprintf(SEND_OUTFORDELIVERY_MSG, $deliveryinfo['ProductTitle'], $deliveryinfo['PackageNumber']);

                            $adminMsg = sprintf(SEND_OUTFORDELIVERY_ADMIN, $deliveryinfo['ProductTitle'], $deliveryinfo['PackageNumber']);

                            $response['msg'] = "";

                        }

                        if (isset($msg)) {

                            $insNotification = array(
                                array(
                                    "NotificationTitle" => $title,
                                    "NotificationShortMessage" => $msg,
                                    "NotificationMessage" => $msg,
                                    "NotificationType" => "request",
                                    "NotificationUserId" => array($userinfo->_id),
                                    "Date" => new MongoDate(),
                                    "GroupTo" => "User",
                                ),
                                array(
                                    "NotificationTitle" => "Your package delivered",
                                    "NotificationMessage" => $adminMsg,
                                    "NotificationUserId" => array(),
                                    "NotificationReadStatus" => 0,
                                    "location" => "request_detail",
                                    "locationkey" => (string) $requestId,
                                    "Date" => new MongoDate(),
                                    "GroupTo" => "Admin",
                                ),
                            );

                            $notificationColl->batchInsert($insNotification);

                            require 'vendor/mailer/Email.php';

                            if (isset($emailTemplateid)) {
                                if ($userinfo->EmailStatus == 'on') {
                                    // Email for Requester
                                    $ETemplate = array(
                                        "to" => $userinfo->Email,
                                        "replace" => array(
                                            "[USERNAME]" => $deliveryinfo['RequesterName'],
                                            "[PACKAGETITLE]" => $deliveryinfo['ProductTitle'],
                                            "[ACCEPT_BY]" => @$userInformation->Name,
                                            "[SOURCE]" => $deliveryinfo['PickupFullAddress'],
                                            "[DESTINATION]" => $deliveryinfo['DeliveryFullAddress'],
                                        ),
                                    );
                                    send_mail($emailTemplateid, $ETemplate);
                                }
                            }

                            // Email for Transporter
                            if (isset($emailidforTP)) {
                                if ($userInformation->TPSetting == 'on' && $userInformation->EmailStatus == 'on') {
                                    $ETemplate = array(
                                        "to" => @$userInformation->Email,
                                        "replace" => array(
                                            "[USERNAME]" => @$userInformation->Name,
                                            "[PACKAGETITLE]" => $deliveryinfo['ProductTitle'],
                                            "[PACKAGEID]" => $deliveryinfo['PackageId'],
                                            "[SOURCE]" => $deliveryinfo['PickupFullAddress'],
                                            "[DESTINATION]" => $deliveryinfo['DeliveryFullAddress'],
                                        ),
                                    );
                                    send_mail($emailidforTP, $ETemplate);
                                }
                            }

                            // Email for Admin
                            if (isset($emailidforadmin)) {
                                $financeemail = $settingcoll->find(array('_id' => new MongoId('563b0e31e4b03271a097e1ca')), array('FinanceEmail'));

                                if ($financeemail->count() > 0) {
                                    $financeemail = $financeemail->getNext();
                                    $ETemplate = array(
                                        "to" => $financeemail['FinanceEmail'],
                                        "replace" => array(
                                            "[TRANSPORTERNAME]" => @$userInformation->Name,
                                            "[PACKAGETITLE]" => $deliveryinfo['ProductTitle'],
                                            "[PACKAGEID]" => $deliveryinfo['PackageId'],
                                            "[SOURCE]" => $deliveryinfo['PickupFullAddress'],
                                            "[DESTINATION]" => $deliveryinfo['DeliveryFullAddress'],
                                        ),
                                    );
                                    send_mail($emailidforadmin, $ETemplate);
                                }
                            }

                            //Send push notification
                            require_once 'vendor/Notification/Notification.php';
                            $pushnoti = new Notification();
                            $pushnoti->setValue('title', $title);
                            $pushnoti->setValue('message', $msg);
                            $pushnoti->setValue('location', 'requester_delivery_detail');
                            $pushnoti->setValue('locationkey', (string) $requestId);

                            $pushnoti->add_user($userinfo->NotificationId, $userinfo->DeviceType);
                            $pushnoti->fire();
                            // End send push notification
                        }
                    }

                } else {

                    $response['msg'] = "Request already accepted by other transporter or You are not authorized for this package.";
                }
            }
        }

    }
    echoRespnse(200, $response);
});

/*
 * Function Name : my_delivery_area
 * Description      : This is used to insert my area(country, state, cities) of individual transporter.
 * Url              : http://192.168.11.101/aq/services/transporter.php/my_delivery_area
 * Method          : POST
 * Header          : apiKey, userType
 * Parameter     : deliveryAreaCountrydeliveryAreaState,deliveryAreaCities.
 * Created By    : Pankaj Gawande
 * Create Date      : 1-12-2015
 *
 */

$app->post('/my_delivery_area', 'transporter_auth', function () use ($app) {
    global $db;
    global $userId;

    verifyRequiredParams(array('deliveryAreaCountry'));
    $response = array('success' => 0, "msg" => 'Records not Updated');
    $collection = $db->users;
    $updateData = array(
        'DeliveryAreaCountry' => array_map('trim', explode(',', strtolower($app->request->post('deliveryAreaCountry')))),
        'DeliveryAreaState' => array_map('trim', explode(',', strtolower($app->request->post('deliveryAreaState')))),
        'DeliveryAreaCities' => array_map('trim', explode(',', strtolower($app->request->post('deliveryAreaCities')))),
    );
    $requestUpdate = $collection->update(array('_id' => $userId), array('$set' => $updateData));
    if ($requestUpdate['updatedExisting'] > 0) {
        $response = array('success' => 1, "msg" => 'Records Updated');
    }
    echoRespnse(200, $response);
});
/*
 * Function Name : package_material_status
 * Description      : package_material_status.
 * Url              : http://192.168.11.101/aq/services/transporter.php/package_material_status
 * Method          : POST
 * Header          : apiKey, userType
 * Parameter     : requestId,packageMaterialStatus.
 * Created By    : Pankaj Gawande
 * Create Date      : 1-12-2015
 *
 */
$app->post('/package_material_status', 'transporter_auth', function () use ($app) {
    global $db;
    global $userId;
    verifyRequiredParams(array('requestId', 'packageMaterialStatus'));
    $response = array('success' => 0, "msg" => 'Records not Updated');
    $collection = $db->delivery_request;
    $where = array('_id' => new MongoId($app->request->post('requestId')), 'TransporterId' => $userId);

    $updateData = array(
        'PackageMaterialShipped' => ($app->request->post('packageMaterialStatus') == 'yes') ? 'yes' : 'not_applicable',
    );
    $requestUpdate = $collection->update($where, array('$set' => $updateData));

    if ($requestUpdate['updatedExisting'] > 0) {
        $response = array('success' => 1, "msg" => 'Records Updated');
    }
    echoRespnse(200, $response);
});

/*
 * Function Name :  finish_delivery
 * Description : finish delivery
 * Url : http://192.168.11.101/aq/services/carrier.php/finish_delivery
 * Method : POST
 * Header : Apikey, Usertype
 * Parameter : requestId, verificationCode,message
 * Created By : Ravi shukla
 * Create Date : 01-09-2015
 * */
$app->post('/finish_delivery', 'transporter_auth', function () use ($app) {

    require_once 'vendor/Notification/Notification.php';

    require 'vendor/mailer/Email.php';

    global $db;
    global $userId;
    global $userInformation;

    verifyRequiredParams(array('requestId', 'verificationCode'));
    $response = array('success' => 0, 'msg' => 'Fail to finish delivery.');
    $noticoll = $db->notification;
    $Usercoll = $db->users;

    if (strlen($app->request->post('requestId')) == 24) {
        $collection = $db->delivery_request;
        $requestId = new MongoId($app->request->post('requestId'));

        $deliveryreq = $collection->find(array('_id' => $requestId, 'TransporterId' => $userId));

        if ($deliveryreq->count() > 0) {
            $deliveryreq = $deliveryreq->getNext();
            if (@$deliveryreq['DeliveryVerifyCode'] == $app->request->post('verificationCode')) {
                $updData = array(
                    'TransporterMessage' => $app->request->post('message'),
                    'Status' => 'delivered',
                    'DeliveredTime' => new MongoDate(),
                    'UpdateOn' => new MongoDate(),
                );

                $res = $collection->update(array('_id' => $requestId, 'TransporterId' => $userId), array('$set' => $updData));

                //Start notification section
                $userinfo = $Usercoll->find(array('_id' => array('$in' => array($deliveryreq['RequesterId'], $userId))), array('_id', 'Email', 'Name', 'NotificationId', 'DeviceType', 'NoficationStatus', 'EmailStatus', 'Image', 'TPSetting'));
                if ($userinfo->count() == 2) {
                    $requesterinfo = (Object) $userinfo->getNext();
                    if ($requesterinfo->_id == $userId) {
                        // if first record of transporter
                        $transporterinfo = $requesterinfo;
                        $requesterinfo = (Object) $userinfo->getNext();
                    } else {
                        $transporterinfo = (Object) $userinfo->getNext();
                    }

                    $title = REQUEST_DELIVERED;
                    $msg = sprintf(MSG_REQUEST_DELIVERED, $userInformation->Name, $deliveryreq['ProductTitle']);

                    //$adminMsg = sprintf('Transporter "%s" has dropoff PackageID: %s to pickup location.', $userInformation->Name, $deliveryreq['PackageNumber']);
                    $adminMsg = sprintf(SEND_DELIVERED_ADMIN, $userInformation->Name, $deliveryreq['PackageNumber']);

                    $insNotification = array(
                        array(
                            "NotificationTitle" => $title,
                            "NotificationMessage" => $msg,
                            "NotificationType" => "request",
                            "NotificationUserId" => array($requesterinfo->_id),
                            "Date" => new MongoDate(),
                            "GroupTo" => "User",
                        ),
                        array(
                            "NotificationTitle" => 'Your package delivered',
                            "NotificationMessage" => $adminMsg,
                            "NotificationUserId" => array(),
                            "NotificationReadStatus" => 0,
                            "location" => "request_detail",
                            "locationkey" => (string) $requestId,
                            "Date" => new MongoDate(),
                            "GroupTo" => "Admin",
                        ),
                    );

                    $noticoll->batchInsert($insNotification);

                    if ($requesterinfo->NoficationStatus == 'on') {
                        //Send push notification
                        $pushnoti = new Notification();
                        $pushnoti->setValue('title', $title);
                        $pushnoti->setValue('message', $msg);
                        $pushnoti->setValue('location', 'requester_rating_for_package');
                        $pushnoti->setValue('locationkey', (string) $requestId);
                        $pushnoti->setValue('TransporterImage', $transporterinfo->Image);
                        $pushnoti->setValue('TransporterName', $userInformation->Name);

                        $pushnoti->add_user($requesterinfo->NotificationId, $requesterinfo->DeviceType);
                        $pushnoti->fire();
                        // End send push notification
                    }
                    if ($requesterinfo->EmailStatus == 'on') {
                        //Send email to sender
                        $ETemplate = array(
                            "to" => $requesterinfo->Email,
                            "replace" => array(
                                "[USERNAME]" => $requesterinfo->Name,
                                "[PACKAGETITLE]" => $deliveryreq['ProductTitle'],
                            ),
                        );
                        send_mail('5622492ce4b0d4bc235582bd', $ETemplate);
                    }
                    if ($transporterinfo->EmailStatus == 'on' && $transporterinfo->TPSetting == 'on') {
                        //Send email to transporter
                        $ETemplate = array(
                            "to" => $transporterinfo->Email,
                            "replace" => array(
                                "[USERNAME]" => $transporterinfo->Name,
                                "[PACKAGETITLE]" => $deliveryreq['ProductTitle'],
                            ),
                        );
                        send_mail('5694d88a5509251cd67773f0', $ETemplate);
                    }
                }

                $response['msg'] = "Package delivered successfully.";
                $response['success'] = 1;

            } else {
                $response['msg'] = "Invalid verification code.";
            }
        } else {
            $response['msg'] = "Package not found.";
        }
    }
    echo echoRespnse(200, $response);
});

/*
 * Function Name :  finish_delivery
 * Description : finish delivery
 * Url : http://192.168.11.101/aq/services/transporter.php/feedback_on_package_delivered
 * Method : POST
 * Header : Apikey, Usertype
 * Parameter : requestId,message,rating
 * Created By : Ravi shukla
 * Create Date : 01-09-2015
 * */
$app->post('/feedback_on_package_delivered', 'transporter_auth', function () use ($app) {

    global $db;
    global $userId;

    verifyRequiredParams(array('requestId', 'rating', 'message'));
    $response = array('success' => 0, 'msg' => 'Fail to save feedbcak.');

    $requestCollection = $db->delivery_request;
    $usercoll = $db->users;

    if (strlen($app->request->post('requestId')) == 24) {

        $updData = array(
            "TransporterFeedbcak" => $app->request->post('message'),
            "TransporterRating" => (int) $app->request->post('rating'),
        );
        $res = $requestCollection->findAndModify(array('_id' => new MongoId($app->request->post('requestId')), 'TransporterId' => $userId), array('$set' => $updData));
        if (count($res) > 0) {

            $usercoll->update(array('_id' => $res['RequesterId']), array('$inc' => array('RatingByCount' => 1, 'RatingCount' => (float) $app->request->post('rating'))));

            $response = array('success' => 1, 'msg' => 'Thanks for your feedback.');
        }

    }

    echo echoRespnse(200, $response);
});
/*
 * Function Name :  finish_delivery
 * Description : finish delivery
 * Url : http://192.168.11.101/newlara/services/carrier.php/cancel_delivery
 * Method : POST
 * Header : apiKey, userType
 * Parameter : requestId
 * rejectBy (transporter,requester),
 * returnType (osreturn,ipayment)
 * Created By : Ravi shukla
 * Create Date : 01-09-2015
 * */
$app->post('/cancel_delivery', 'transporter_auth', function () use ($app) {

    global $db;
    global $userId;
    global $userName;
    require_once 'vendor/mailer/Email.php';

    verifyRequiredParams(array('requestId', 'rejectBy', 'returnType'));
    $response = array('success' => 0, 'msg' => 'Invalid request.');
    $noticoll = $db->notification;
    $Usercoll = $db->users;
    $collection = $db->delivery_request;

    if (strlen($app->request->post('requestId')) == 24) {
        $PackageId = new MongoId($app->request->post('requestId'));
        $newReturnaddress = '';
        $info = $collection->find(array('_id' => $PackageId));
        if ($info->count() > 0) {
            $info = $info->getNext();

            $field = array('NumberId', 'Email', 'Name', 'Image', 'NotificationId', 'DeviceType', 'EmailStatus', 'SenderNoficationStatus', 'TPSetting');
            $user = $Usercoll->find(array('_id' => array('$in' => array($info['RequesterId'], $info['TransporterId']))), $field);
            if ($user->count() == 2) {
                $transporter = array();
                $requester = $user->getNext();
                if ($requester['_id'] == $info['TransporterId']) {
                    $transporter = $requester;
                    $requester = $user->getNext();
                } else {
                    $transporter = $user->getNext();
                }

                $NotificationMessage = 'Your package "' . $info['ProductTitle'] . '" has been canceled by ' . $requester['Name'] . '';
                $emailReasonforCancaletion = "Rejectby : " . ucfirst($app->request->post('rejectBy')) . ", Retrun type: " . $app->request->post('returnType');

                $updData = array(
                    "Status" => 'cancel',
                    "RejectBy" => strtolower($app->request->post('rejectBy')),
                    "ReturnType" => $app->request->post('returnType'),
                    "ReceiptImage" => "",
                    "RejectTime" => new MongoDate(),
                    "TrackingNumber" => $app->request->post('trackingNumber'),
                    "TransporterMessage" => $app->request->post('message'),
                    "UpdateOn" => new MongoDate(),
                );

                if (isset($_FILES['receipt']['name']) && @$_FILES['receipt']['name'] != '') {
                    $exts = explode('.', $_FILES['receipt']['name']);
                    $ext = $exts[count($exts) - 1];
                    $profileImage = "receipt" . rand(2154, 45454) . time() . ".$ext";
                    if ($ext == 'gif' || $ext == 'GIF' || $ext == 'png' || $ext == 'PNG' || $ext == 'jpg' || $ext == 'JPG' || $ext == 'JPEG' || $ext == 'jpeg') {
                        $newpath = FILE_URL . "package/{$profileImage}";
                        if (move_uploaded_file($_FILES['receipt']['tmp_name'], $newpath)) {
                            $updData['ReceiptImage'] = "package/{$profileImage}";
                        }
                    }
                }
                if ($app->request->post('returnType') == 'osreturn') {

                    verifyRequiredParams(array('trackingNumber'));

                } else if ($app->request->post('returnType') == 'ipayment') {

                    $NotificationMessage = 'We regret to inform you, Your package delivery "' . $info['ProductTitle'] . '" was not delivered. Aquantuo is continuing to look into the issue. Please contact us at support@aquantuo.com should you have any questions';

                } else if (strtolower($app->request->post('returnType')) == 'creturn' && strtolower($app->request->post('rejectBy')) == 'requester') {
                    verifyRequiredParams(array('returnDate', 'returnTime'));
                    if ($app->request->post('ReturnDate') != '') {
                        $updData['CancelReturnDate'] = new MongoDate(strtotime($app->request->post('returnDate') . " " . $app->request->post('returnTime')));
                    }

                    $notifymsg = 'Your Package "' . $info['ProductTitle'] . '" failed delivery at the specified address. Your package would be returned by to the address specified at the time of the post. Additional fees may apply. Carrier Message: "' . $updData['TransporterMessage'] . '"';

                } else if ($app->request->post('returnType') == 'osreturn') {
                    verifyRequiredParams(array('message', 'trackingNumber'));

                    $NotificationMessage = 'Your Package "' . $info['ProductTitle'] . '" is canceled by requester.<br/><br/><b style="color:#2AABD2"> Carrier Message:</b> ' . $updData['TransporterMessage'];
                }
                //,'TransporterId'=>$userId,'Status'=>'on_delivery'

                $res = $collection->update(array('_id' => $PackageId, 'TransporterId' => $userId), array('$set' => $updData));

                if ($res['updatedExisting'] > 0) {

                    $NotiData = array(
                        'NotificationTitle' => "Package Canceled",
                        'NotificationMessage' => $NotificationMessage,

                        'NotificationShortMessage' => 'Your package "' . $info['ProductTitle'] . '" has been canceled by "' . $info['TransporterName'] . '".',

                        'NotificationType' => "canceled_delivery",
                        'NotificationRequestNumber' => $info['PackageId'],
                        'Date' => new MongoDate(),
                        'GroupTo' => "requester",
                    );

                    $noticoll->insert($NotiData);

                    // Email for transporter
                    if (@$transporter['EmailStatus'] == 'on' && @$transporter['TPSetting'] == 'on') {
                        $ETemplate = array(
                            "to" => $transporter['Email'],
                            "replace" => array(
                                "[PACKAGETITLE]" => $info['ProductTitle'],
                                "[REASON]" => $NotificationMessage,
                            ),
                        );
                        send_mail('56224b45e4b0d4bc235582c0', $ETemplate);
                        // End email for transporter
                    }

                    if (@$requester['EmailStatus'] == 'on') {
                        $eArray = array(
                            'to' => $requester['Email'],
                            'replace' => array(
                                '[USERNAME]' => $requester['Name'],
                                '[PACKAGETITLE]' => $info['ProductTitle'],
                                "[REASON]" => $emailReasonforCancaletion,
                            ),
                        );
                        send_mail('588ad8316befd90b9d7ea793', $eArray);
                    }

                    if (@$requester['EmailStatus'] == 'on') {
                        //Send push notification
                        require_once 'vendor/Notification/Notification.php';
                        $pushnoti = new Notification();
                        $pushnoti->setValue('title', 'Your request has been cancled');
                        $pushnoti->setValue('message', $NotificationMessage);
                        $pushnoti->setValue('location', 'delivery_detail');
                        $pushnoti->setValue('locationkey', (string) $PackageId);

                        $pushnoti->add_user($requester['NotificationId'], $requester['DeviceType']);
                        $pushnoti->fire();
                        // End send push notification
                    }

                    $response['success'] = 1;
                    $response['msg'] = "Package canceled successfully.";
                } else {
                    $response['msg'] = "Failed to cancel package.";
                }
            } else {
                $response['msg'] = "Failed to cancel package.";
            }
        } else {
            $response['msg'] = "Invalid package.";
        }
    }
    echo echoRespnse(200, $response);
});

/*
 * Function Name :  cancel_after_accepted
 * Description : Cancel request after accept any request
 * Url : http://192.168.11.101/aq/services/transporter.php/cancel_after_accepted
 * Method : POST
 * Header : Apikey, Usertype
 * Parameter : PackageId
 * Created By : Ravi shukla
 * Create Date : 14-10-2015
 * */
$app->post('/cancel_after_accepted', 'transporter_auth', function () use ($app) {

    global $db;
    global $userId;
    $usercollection = $db->users;

    require_once 'vendor/mailer/Email.php';
    $response = array('success' => 0, 'msg' => 'oops! Something went wrong.');

    if (strlen($app->request->post('packageId')) == 24) {
        $collection = $db->delivery_request;
        $packageId = new MongoId($app->request->post('packageId'));

        $info = $collection->find(['_id' => $packageId, 'TransporterId' => $userId]);
        if ($info->count() > 0) {
            $info = $info->getNext();

            $res = $collection->update(array('_id' => $packageId, 'TransporterId' => $userId, 'Status' => array('$in' => array('out_for_pickup', 'accepted'))),
                array('$set' => array('Status' => 'ready', 'TransporterId' => '', 'TransporterName' => '')));

            if (@$res['updatedExisting'] > 0) {

                //Start email to requester
                $requester = $usercollection->find(['_id' => $info['RequesterId']],
                    ['Name', 'Email', 'EmailStatus']);
                if ($requester->count() > 0) {
                    $requester = (Object) $requester->getNext();

                    if ($requester->EmailStatus == "on") {
                        send_mail('57e8c3dae4b01d01c916d24c', [
                            "to" => $requester->Email,
                            "replace" => [
                                "[USERNAME]" => $requester->Name,
                                "[PACKAGETITLE]" => ucfirst($info['ProductTitle']),
                                "[PACKAGEID]" => $info['PackageId'],
                                "[SOURCE]" => $info['PickupFullAddress'],
                                "[DESTINATION]" => $info['DeliveryFullAddress'],
                                "[PACKAGENUMBER]" => $info['PackageNumber'],
                                "[TOTALCOST]" => $info['TotalCost'],
                            ],
                        ]);
                    }
                }
                //End email to requester
                //Start email to transporter
                $tp = $usercollection->find(['_id' => $info['TransporterId']],
                    ['Name', 'Email', 'EmailStatus']);
                if ($tp->count() > 0) {
                    $tp = (Object) $tp->getNext();
                    if ($tp->EmailStatus == "on") {
                        send_mail('57e8c112e4b01d01c916d24a', [
                            "to" => $tp->Email,
                            "replace" => [
                                "[USERNAME]" => $tp->Name,
                                "[PACKAGETITLE]" => ucfirst($info['ProductTitle']),
                                "[PACKAGEID]" => $info['PackageId'],
                                "[SOURCE]" => $info['PickupFullAddress'],
                                "[DESTINATION]" => $info['DeliveryFullAddress'],
                                "[PACKAGENUMBER]" => $info['PackageNumber'],
                                "[TOTALCOST]" => $info['TotalCost'],
                            ],
                        ]);
                    }
                }
                //End email to tp
                //Start email to admin
                $settingcoll = $db->setting;
                $setting = $settingcoll->find(array('_id' => new MongoId('563b0e31e4b03271a097e1ca')));
                if ($setting->count() > 0) {

                    $setting = (Object) $setting->getNext();
                    $adminETemplate = array(
                        "to" => $setting->SupportEmail,
                        "replace" => array(
                            "[USERNAME]" => $info['TransporterName'],
                            "[PACKAGETITLE]" => $info['ProductTitle'],
                            "[PACKAGEID]" => $info['PackageNumber'],
                            "[TRANSPORTERNAME]" => $info['TransporterName'],
                            "[DESTINATION]" => $info['DeliveryFullAddress'],
                        ),
                    );
                    send_mail('57e8c3f5e4b01d01c916d24e', $adminETemplate);
                }
                //End email to admin

                // Insert notification
                $noticoll = $db->notification;

                $noticoll->batchInsert([
                    array(
                        "NotificationTitle" => 'Request canceled',
                        "NotificationMessage" => sprintf('The request of send a package title: "%s", ID: %s has been canceled by transporter.', $info['ProductTitle'], $info['PackageNumber']),
                        "NotificationType" => "request_detail",
                        "NotificationUserId" => array($info['RequesterId']),
                        "Date" => new MongoDate(),
                        "GroupTo" => "User",
                    ),
                    array(
                        "NotificationTitle" => 'Request canceled',
                        "NotificationMessage" => sprintf('The request of send a package title: "%s", ID: %s has been canceled by transporter.', $info['ProductTitle'], $info['PackageNumber']),
                        "NotificationUserId" => array(),
                        "NotificationReadStatus" => 0,
                        "location" => "request_detail",
                        "locationkey" => $app->request->post('packageId'),
                        "Date" => new MongoDate(),
                        "GroupTo" => "Admin",
                    ),
                ]);

                $response['success'] = 1;
                $response['msg'] = "Package has been canceled successfully.";
            } else {
                $response['msg'] = "Fail to canceled package.";
            }
        }
    }

    echo echoRespnse(200, $response);
});

/*
 * Function Name   : view_request
 * Description     : This function is used to view list of request created by requester.
 * Url             : http://192.168.11.101/aq/services/transporter.php/view_request
 * Method          : GET
 * Header          : apiKey, userType
 * Parameter       : requestId
 * Created By      : Pankaj Gawande
 * Create Date     : 11-12-2015
 *
 */

$app->get('/view_request', 'transporter_auth', function () use ($app) {
    global $db;
    global $userId;
    $response = array('success' => 0, 'msg' => 'Oops! Something went wrong');
    verifyRequiredParams(array('pageno', 'tripId'));
    if (strlen($app->request->get('tripId')) == 24) {
        $limit = 30;
        $start = (((Int) $app->request->get('pageno')) - 1) * $limit;
        $collection = $db->delivery_request;
        $where = array(
            "TransporterId" => $userId,
            "TripId" => new MongoId($app->request->get('tripId')),
        );
        $field = array('ProductTitle', 'Status', 'PickupCity', 'DeliveryCity', 'PickupLatLong', 'PickupDate', 'ProductImage', 'DeliveryLatLong');
        $requestInfo = $collection->find($where, $field)->sort(array('_id' => -1))->skip($start)->limit($limit);
        if ($requestInfo->count() > 0) {
            $response = array('success' => 1, 'msg' => 'Records found');
            foreach ($requestInfo as $key => $value) {
                $response['result'][] = array(
                    'PackageId' => (String) $value['_id'],
                    'Requestid' => (String) $value['_id'],
                    'ProductTitle' => $value['ProductTitle'],
                    'Status' => $value['Status'],
                    'PickupCity' => $value['PickupCity'],
                    'DeliveryCity' => $value['DeliveryCity'],
                    'PickupLat' => $value['PickupLatLong'][1],
                    'PickupLong' => $value['PickupLatLong'][0],
                    "PickupDate" => $value['PickupDate']->sec,
                    "ProductImage" => $value['ProductImage'],
                    "DeliveryLat" => $value['DeliveryLatLong'][1],
                    "DeliveryLong" => $value['DeliveryLatLong'][0],
                );
            }
        } else {
            $response['msg'] = " No request found yet.";
        }
    }

    echoRespnse(200, $response);
});

/*
 * Function Name   : completed_deliveries
 * Description     : This function returns completed deliveries of transporter.
 * Url             : http://192.168.11.101/aq/services/transporter.php/completed_deliveries
 * Method          : GET
 * Header          : apiKey, userType
 * Parameter       : NA
 * Created By      : Pankaj Gawande
 * Create Date     : 11-12-2015
 * Updated on         :
 *
 */

$app->get('/completed_deliveries', 'transporter_auth', function () use ($app) {
    global $db;
    global $userId;
    $response = array('success' => 0, 'msg' => 'Records not found');
    $limit = 30;
    $start = (((int) $app->request->get('pageno')) - 1) * $limit;

    $requestCollection = $db->delivery_request;
    $field = array('DeliveryDate', 'ProductTitle', 'TotalCost', 'ProductImage', 'RequestType');
    $requestData = $requestCollection->find(array('TransporterId' => $userId, 'Status' => 'delivered'), $field)
        ->skip($start)->limit($limit)->sort(array('_id' => -1));

    if ($requestData->count() > 0) {
        $response['success'] = 1;
        $response['msg'] = "Records found";
        foreach ($requestData as $key => $value) {
            $response['result'][] = array(
                'PackageId' => (String) $value['_id'],
                'ProductTitle' => $value['ProductTitle'],
                'Date' => $value['DeliveryDate']->sec,
                'TotalCost' => number_format($value['TotalCost'], 2),
                'Status' => 'Completed',
                'ProductImage' => isset($value['ProductImage'][0]) ? $value['ProductImage'][0] : "",
                'RequestType' => isset($value['RequestType']) ? $value['RequestType'] : 'delivery',
            );
        }
    }

    echoRespnse(200, $response);

});

/*
 * Function Name   : not_completed_deliveries
 * Description     : This function returns completed deliveries of transporter.
 * Url             : http://192.168.11.101/aq/services/transporter.php/not_completed_deliveries
 * Method          : GET
 * Header          : apiKey, userType
 * Parameter       : NA
 * Created By      : Pankaj Gawande
 * Create Date     : 11-12-2015
 *
 */

$app->get('/not_completed_deliveries', 'transporter_auth', function () use ($app) {
    global $db;
    global $userId;
    $response = array('success' => 0, 'msg' => 'Records not found');
    $limit = 30;
    $start = (((int) $app->request->get('pageno')) - 1) * $limit;

    $requestCollection = $db->delivery_request;
    $requestData = $requestCollection->find(array('TransporterId' => $userId, 'Status' => 'cancel'), array('RejectTime', 'ProductTitle', 'TotalCost', 'ProductImage', 'Status', 'RequestType'))
        ->skip($start)->limit($limit)->sort(array('_id' => -1));

    if ($requestData->count() > 0) {
        $response['success'] = 1;
        $response['msg'] = "Records found";
        foreach ($requestData as $key => $value) {
            $response['result'][] = array(
                'PackageId' => (String) $value['_id'],
                'ProductTitle' => $value['ProductTitle'],
                'Date' => $value['RejectTime']->sec,
                'TotalCost' => number_format($value['TotalCost'], 2),
                'Status' => get_status_title($value['Status']),
                'ProductImage' => $value['ProductImage'],
                'RequestType' => isset($value['RequestType']) ? $value['RequestType'] : 'delivery',
            );
        }
    }

    echoRespnse(200, $response);

});

/*
 * Function Name   : not_completed_deliveries
 * Description     : This function returns completed deliveries of transporter.
 * Url             : http://192.168.11.101/aq/services/transporter.php/update_transporter_location
 * Method          : POST
 * Header          : apiKey, userType
 * Parameter       : lat,lng
 * Created By      : Ravi shukla
 * Create Date     : 8-01-2015
 *
 */

$app->post('/update_transporter_location', 'transporter_auth', function () use ($app) {
    global $db;
    global $userId;

    if ($app->request->post('lat') != '' && $app->request->post('lng') != '') {
        $usercollection = $db->users;

        $usercollection->update(
            array('_id' => $userId),
            array('$set' => array('CurrentLocation' => array(floatval($app->request->post('lng')), floatval($app->request->post('lat')))))
        );
    }
    echoRespnse(200, array('success' => 1, 'msg' => 'Updated'));
});

/*
 * Function Name   : not_completed_deliveries
 * Description     : This function returns completed deliveries of transporter.
 * Url             : http://192.168.11.101/aq/services/transporter.php/my_delivery_area
 * Method          : GET
 * Header          : apiKey, userType
 * Parameter       : lat,lng
 * Created By      : Ravi shukla
 * Create Date     : 8-01-2015
 *
 */

$app->get('/my_delivery_area', 'transporter_auth', function () use ($app) {
    global $db;
    global $userId;
    $usercollection = $db->users;

    $usrinformation = $usercollection->find(array('_id' => $userId), array('DeliveryAreaCountry', 'DeliveryAreaState', 'DeliveryAreaCities'));

    $usrinformation = $usrinformation->getNext();
    unset($usrinformation['_id']);
    $usrinformation['DeliveryAreaCountry'] = array_map('ucfirst', $usrinformation['DeliveryAreaCountry']);
    $usrinformation['DeliveryAreaCities'] = array_map('ucfirst', $usrinformation['DeliveryAreaCities']);
    $response = array('success' => 1, 'msg' => 'My delivery area', 'result' => $usrinformation);
    echoRespnse(200, $response);
});

/*
 * Function Name   : not_completed_deliveries
 * Description     : This function returns completed deliveries of transporter.
 * Url             : http://192.168.11.101/aq/services/transporter.php/trip_status_activity
 * Method          : POST
 * Header          : Apikey
 * Parameter       : tripId
 * Created By      : Ravi shukla
 * Create Date     : 22-02-2016
 *
 */

$app->post('/trip_status_activity', 'transporter_auth', function () use ($app) {
    global $db;
    global $userId;

    $response = array('success' => 0, 'msg' => 'Oops! Invalid trip.');
    if (strlen($app->request->post('tripId')) == 24 && in_array($app->request->post('operation'), array('active', 'inactive'))) {
        $tripid = new MongoId($app->request->post('tripId'));

        $tripscollection = $db->trips;

        $result = $tripscollection->update(array('_id' => $tripid, 'TransporterId' => $userId),
            array('$set' => array('Status' => strtolower($app->request->post('operation')))));

        if ($result['n'] > 0) {
            $response['success'] = 1;
            $response['msg'] = ($app->request->post('operation') == 'active') ? 'Trip has been activated successfully.' : 'Trip has been deactivated successfully.';
        }
    }
    echoRespnse(200, $response);
});

function get_userinfo($id, $field = '')
{
    global $db;
    $coll = $db->users;
    $field = ($field != '') ? explode(',', $field) : array();
    $info = $coll->find(array('_id' => new MongoId((String) $id)), $field);

    if ($info->count() > 0) {
        return (Object) $info->getNext();
    } else {
        return (Object) array();
    }

}

function get_unit_title($status)
{
    switch (strtolower($status)) {
        case 'inch':
            return 'In';
            break;
        case 'meter':
            return 'M';
            break;
        case 'pound':
            return 'Lbs';
            break;
        case 'feet':
            return 'Ft';
            break;
        case 'gram':
            return 'Gm';
            break;
        case 'centimetre':
            return 'Cm';
            break;
        default:
            return ucfirst($status);
            break;
    }
}

function get_status_title($status)
{
    switch ($status) {

        case 'accepted':
            return 'Accepted';
            break;
        case 'out_for_delivery':
            return 'Out for Delivery';
            break;
        case 'ready':
            return 'Ready';
            break;
        case 'out_for_pickup':
            return 'Out for Pickup';
            break;
        case 'delivered':
            return 'Delivered';
            break;
        case 'cancel':
            return 'Canceled';
            break;
        case 'assign':
            return 'Assigned';
            break;
        default:
            return 'Pending';

    }
}

function get_formatted_address($array, $zipcode = '')
{
    $address = '';
    foreach ($array as $key) {
        if (!empty($key)) {
            $address .= (($address != '') ? ", $key" : $key);
        }
    }
    if (!empty($zipcode)) {$address .= " - $zipcode";}
    return $address;
}

function change_unit($weight, $currentunit, $changeto)
{

//    echo $weight." ".$currentunit." ".$changeto; //die;

//    echo "<br>";

    $newweight = '';
    $newchangeto = $changeto;
    switch ($currentunit) {
        case 'kg':
            $newweight = $weight * 1000;
            break;
        case 'lbs':
            $newweight = $weight * 453.592;
            break;
        case 'inches':
            $newweight = $weight;
            break;
        case 'cm':
            $newweight = $weight * 0.393701;
            break;
    }

    switch ($changeto) {
        case 'kg':
            $newweight = $newweight / 1000;
            break;
        case 'lbs':
            $newweight = $newweight / 453.592;
            break;
        case 'cm':
            $newweight = $newweight / 0.393701;
            break;
        case 'inches':
            $newweight = $newweight;
            break;
    }

    return $newweight;

}

$app->run();
