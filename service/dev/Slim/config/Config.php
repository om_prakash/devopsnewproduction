<?php
/**
 * Database configuration
 */
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
date_default_timezone_set('UTC');


define('DB_USERNAME', '');
define('DB_PASSWORD', '');
define('DB_HOST', '127.0.0.1:27017');
define('DB_NAME', 'aquantuo_live');
define('FILE_URL', '/var/www/html/upload/');
define('SITE_URL', 'https://aquantuo.com/');
define('OtherCategory','568e2f7ccf3207975ae0d8be');
define('ElectronicsCategory','5650140b6734c4af698b4567');
define('ConfigId','5673e33e6734c4f874685c84');

define('AUTO_FULLSIZE_SUV', '56d8fdffcf3207063869c999');
define('AUTO_INTERMEDIATE_SUV', '56d8fe2acf3207673069c999');
define('AUTO_SEDAN_ECONOMY', '56ee584ccf32071c0d52561f');
define('AUTO_SEDAN_FULLSIZE', '56ee58a2cf32071c0d525620');
define('AUTO_SEDAN_INTERMEDIATE', '56ee5869cf3207d471525620');
define('AUTO_STANDARD_SUV', '56d8fe15cf3207bb2b69c999');
define('AUTOMOBILE_VAN', '568e2e12cf3207d25ae0d8bd');
?>
