<?php

define('NEW_REQUEST','New request for delivery');
define('MSG_NEW_REQUEST','A request for package "%s" is ready from %s to %s.');
define('ADMIN_NEW_REQUEST','New request of Send a package');
define('ADMIN_MSG_NEW_REQUEST','The send a package request titled:"%s", ID:%s has created.');
define('REQUEST_DELIVERED','Your package was delivered');
define('MSG_REQUEST_DELIVERED','Transporter "%s" has delivered your package "%s".');
define('ACCEPT_REQUEST','Your request was accepted');
define('MSG_ACCEPT_REQUEST','Transporter "%s" has accepted your delivery request "%s".');
define('ACCEPT_REQUEST','Your request was accepted');
//Buy for me
define('BUYFORME_REQUEST_CREATED_TITLE','New Buy for me request created');
define('BUYFORME_REQUEST_CREATED','The buy for me request titled:"%s", ID:%s has been created.');
//Buy for me -out for pickup
define('BUY_OUTFORPICKUP_TITLE','The Transporter is now in possession of your package.');
define('BUY_OUTFORPICKUP_MSG','Unless you shipped your item to the transporter.Please have it ready.The transporter is out to pick up your package or to meet you at the agreed location.');
define('BUY_OUTFORPICKUP_MSG_ADMIN','The transporter is out to pickup the request titled: %s, ID: %s');
//Buy for me -out for delivery
define('BUY_OUTFORDELIVERY_TITLE','Package is en route to be delivered');
define('BUY_OUTFORDELIVERY_MSG','Transporter is en route to deliver your item titled:%s, ID: %s.');
define('BUY_OUTFORDELIVERY_MSG_ADMIN','Transporter has picked up the package and is out for delivery. Item titled: %s, ID: %s to %s');
define('BUY_UPDATE_TITLE','Item Updated');
define('BUY_UPDATE_MSG','Your all item Item has been purchased. Please process to pay for next process.');
//Buy for me - request cancel
define('BUYFORME_CANCELED_TITLE','Your request has been canceled');
define('BUYFORME_CANCELED_MSG_IPAY','We regret to inform you, your package delivery, %s was not delivered. Aquantuo is continuing to look into the issue. Please contact us at %s should you have any questions');
define('BUYFORME_CANCELED_MSG_CRE','Your Package "%s" failed delivery at the specified address. Your package would be returned by to the address specified at the time of the post. Additional fees may apply. Carrier Message: %s');
define('BUYFORME_CANCELED_MSG_OSCRE','Your Package "%s" has been canceled by "%s". Carrier Message: %s');
define('BUYFORME_COMPLETED_TITLE','Your package was delivered');
define('BUYFORME_COMPLETED_MSG','Your request has been completed.');
define('BUYFORME_COMPLETED_MSG_REQ','Transporter "%s" has delivered your package "%s".');
define('BUYFORME_COMPLETED_TITLE_ADMIN','Package delivered.');
define('BUYFORME_COMPLETED_MSG_ADMIN','Transporter "%s" has dropped off PackageID: "%s" at drop off location.');
define('BUYFORME_COMPLETED_TITLE_ADMIN','Package delivered.');
define('BUYFORME_CANCELLED_BY_TP_TITLE','Package canceled.');
define('BUYFORME_CANCELLED_BY_TP_MSG','Your package "%s" has been canceled by %s');
//Online request
define('ONLINE_REQUEST_CREATED_TITLE','Online purchase request');
define('ONLINE_REQUEST_CREATED_MSG','The online request titled:"%s", ID:%s has created.');
//online request -out for pickup
define('ONLINE_REQUEST_CREATED_TITLE','Online purchase request');
define('ONLINE_OUTFORPICKUP_TITLE','Transporter is out for pickup');
define('ONLINE_OUTFORPICKUP_MSG','Unless you shipped your item to the Transporter, please have it ready. The Transporter is out to pick up your package or to meet you at the agreed location.');
define('ONLINE_OUTFORPICKUP_ADMIN','The transporter has out for pickup package to delivered titled: %s, ID: %s');
 //Online out for delivery
define('ONLINE_OUTFORDELIVERY_TITLE','Package is en route to be delivered');
define('ONLINE_OUTFORDELIVERY_MSG','Transporter is en route to deliver your item titled:%s, ID: %s.');
/*define('ONLINE_OUTFORDELIVERY_ADMIN','Transporter has picked up the package and is out to deliver the item titled: %s, ID: %s to the %s');*/
define('ONLINE_OUTFORDELIVERY_ADMIN','Transporter has pickedup package and out for delivery item titled: %s, ID: %s to %s');

define('ONLINE_DELIVERED_USER_TITLE','Your package was delivered');
define('ONLINE_DELIVERED_ADMIN_TITLE','Package delivered');
define('ONLINE_DELIVERED_USER_MSG','Transporter "%s" has delivered your package "%s"');
define('ONLINE_DELIVERED_USER_ADMIN','Transporter "%s" has dropped off PackageID: "%s" at the drop off location.');
define('ONLINE_CANCELED','Package canceled');
define('ONLINE_CANCELED_MSG','Your package "%s" has been canceled by "%s"');
define('ONLINE_CANCELED_MSG_ADMIN','Transporter "%s" has been canceled package PackageID: %s');
//Send package
define('SEND_CREATE_MSG','The send a package request titled:"%s", ID: %s has created.');
define('SEND_ACCEPT_TITLE','Your request was accepted');
define('SEND_ACCEPT_MSG','Transporter "%s" has accepted your delivery request "%s"');
define('SEND_ACCEPT_MSG_ADMIN','Transporter "%s" has accepted request to delivered from %s to %s.');
define('SEND_OUTFORPICKUP_TITLE','Transporter is out for pickup');
define('SEND_OUTFORPICKUP_MSG','Unless you shipped your item to the Transporter, please have it ready. The Transporter is out to pick up your package or to meet you at the agreed location');
define('SEND_OUTFORPICKUP_ADMIN','Transporter "%s" is out to pickup the package for delivery from "%s" to "%s".');
define('SEND_OUTFORDELIVERY_TITLE','Package is en route to be delivered');
define('SEND_OUTFORDELIVERY_MSG','Transporter is en route to deliver your item titled:%s, ID: %s');
define('SEND_OUTFORDELIVERY_ADMIN','Transporter has picked up the package and is out to deliver the item titled: %s, ID: %s to the delivery address.');
define('SEND_DELIVERED_TITLE_ADMIN','Package has delivered');
define('SEND_DELIVERED_ADMIN','Transporter "%s" has droped off PackageID: "%s" at the drop off location.');
define('SEND_CANCELED','Package canceled');
define('SEND_CANCELED_MSG','Your package "%s" has been canceled by "%s"');
 //Account
define('ACCOUNT_VERIFY_TITLE','Account verify');
define('ACCOUNT_VERIFY','Your transporter account has been verified, please logout and login again to access transporter features.');
define('ACCOUNT_DEACTIVATE_TITLE','Profile deactivated');
define('ACCOUNT_DEACTIVATE','Your transporter profile has been deactivated, please contact us at support@aquantuo.com');

//Admin message
define('ITEM_REVIEW_TITLE','Request reviewed');
define('ITEM_REVIEW_MEG_BUY','Aquantuo has reviewed your Buy for me request for "%s"');
define('ITEM_REVIEW_MEG_ONLINE','Aquantuo has reviewed your online request for "%s"');
define('ITEM_PURCHASED_TITLE','Item Purchased');
define('ITEM_PURCHASED','Your item, %s has been purchased. Please proceed for the next steps.');
define('ITEM_REMINDER_TITLE','Pending payment');
define('ITEM_REMINDER_MSG','Additional payment is required on your item prior to purchase.');
define('ITEM_ASSIGN_TP_USER','Transporter has been assigned for your request titled: "%s", ID: %s.');
define('ITEM_ASSIGN_TP_TITLE','Assign transporter');
define('ITEM_ASSIGN_TP_TRP','You have been assigned as a Transporter for: %s, ID: %s.');
//Online
define('ONLINE_RECEIVED_TITLE','Request received');
define('ONLINE_RECEIVED_MSG','Aquantuo has received your online request for package %s.');
define('ONLINE_ASSIGN_TP_TITLE','Transporter Assign.');
define('ONLINE_ASSIGN_TP_USER','A Transporter has been assigned to your request titled: %s, ID: %s.');
define('ONLINE_ASSIGN_TP_TRP','You have been assigned as transporter for: %s, ID: %s.');
define('ONLINE_ASSIGN_TP_ADMIN','The package for %s, has been assigned to Transporter. Package is %s.');

