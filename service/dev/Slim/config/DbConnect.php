<?php

/**
 * Handling database connection
 *
 * @author Ravi Tamada
 * @link URL Tutorial link
 */
class DbConnect {

    private $conn;

    function __construct() {        
    }

    /**
     * Establishing database connection
     * @return database connection handler
     */
    function connect() {
        include_once dirname(__FILE__) . '/Config.php';
		try{
			
			$mb = new MongoClient('mongodb://'.DB_HOST);
			$this->conn = $mb->selectDB(DB_NAME);
		}
		catch (MongoConnectionException $exception)
		{
			show_error('Unable to connect to Database', 500);
		}
        return $this->conn;
    }

}

$db = (new DbConnect)->connect();
?>
