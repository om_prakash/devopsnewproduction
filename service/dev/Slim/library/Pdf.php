<?php 
/**
 * PDF Library
 *
 * @package			Dayoum
 * @category		Libraries
 * @author			Ravi shukla
 * @link			Slim/library
 */
 

require_once(dirname(__FILE__) . '/../../vendor/dompdf/dompdf_config.inc.php');


class Pdf extends DOMPDF
{

	/**
	 * @access	public
	 * @param	string	$html The view to load
	 * @param	array	$path The path
	 * @return	boolean
	 */
	public static function create($html, $path='')
	{
		$this->load_html($html);
		$this->render();
		$pdfdata  = $this->output();
		$res = fopen($path,'w');
		fwrite($res, $pdfdata);
		return true;
	}
}
