<?php

function update_status($requestid)
{
    global $db;
    $delcoll = $db->delivery_request;
    $status = '';
    $info = $delcoll->find(['_id' => new MongoId($requestid)]);
    if ($info->count() > 0) {
        $info = (Object) $info->getNext();
        foreach ($info->ProductList as $key) {
            if (in_array($key['status'], ['cancel', 'cancel_by_admin']) &&
                !in_array($status, ['pending', 'ready', 'accepted', 'assign', 'out_for_pickup', 'out_for_delivery', 'delivered', 'purchased', 'not_purchased'])) {

                $status = $info->Status = $key['status'];
            } else if ($key['status'] == 'delivered' &&
                !in_array($status, ['pending', 'ready', 'accepted', 'assign', 'out_for_pickup', 'out_for_delivery','reviewed', 'not_purchased', 'paid', 'purchased'])) {

                $status = $info->Status = 'delivered';
            } else if ($key['status'] == 'out_for_delivery' &&
                !in_array($status, ['pending', 'ready', 'accepted', 'assign', 'out_for_pickup','reviewed', 'not_purchased', 'paid', 'purchased'])) {

                $status = $info->Status = 'out_for_delivery';
            } else if ($key['status'] == 'out_for_pickup'
                && !in_array($status, ['accepted', 'assign', 'pending', 'ready','reviewed', 'not_purchased', 'paid', 'purchased'])) {

                $status = $info->Status = 'out_for_pickup';
            } else if ($key['status'] == 'assign' &&
                !in_array($status, ['pending', 'ready', 'accepted', 'reviewed', 'not_purchased', 'paid', 'purchased'])) {
                $status = $info->Status = 'assign';
            } else if ($key['status'] == 'purchased' &&
                !in_array($status, ['pending', 'ready', 'accepted', 'reviewed', 'not_purchased', 'paid'])) {
                $status = $info->Status = 'purchased';
            } else if ($key['status'] == 'item_received' &&
                !in_array($status, ['pending', 'ready', 'accepted', 'reviewed', 'not_purchased', 'paid'])) {
                $status = $info->Status = 'item_received';
            } else if ($key['status'] == 'paid' &&
                !in_array($status, ['pending', 'ready', 'accepted', 'reviewed', 'not_purchased'])) {
                $status = $info->Status = 'paid';
            } else if ($key['status'] == 'not_purchased' &&
                !in_array($status, ['pending', 'ready', 'accepted', 'reviewed'])) {

                $status = $info->Status = 'not_purchased';
            } else if ($key['status'] == 'reviewed' &&
                !in_array($status, ['pending', 'ready', 'accepted'])) {

                $status = $info->Status = 'reviewed';
            } else if ($key['status'] == 'accepted' && !in_array($status, ['pending', 'ready'])) {

                $status = $info->Status = 'accepted';
            } else if ($key['status'] == 'ready' && !in_array($status, ['pending'])) {

                $status = $info->Status = 'ready';
            } else if ($key['status'] == 'pending') {

                $status = $info->Status = 'pending';
            }

        }
        if (!empty(trim($status))) {
            $delcoll->update(['_id' => new MongoId($requestid)], ['$set' => [
                'Status' => $status,
            ]]);
        }

    }
}
