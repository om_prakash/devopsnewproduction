<?php

class Requesthelper
{
    private $category;
    private $setting;
    private $information;
    private $insurencestatus;
    private $raw;

    public function __construct($array = array())
    {
        $this->information = (Object) array(
            "shippingcost" => 0,
            "distance" => 0,
            "insurance" => 0,
            "volume" => 0,
            "weight" => 0,
            "showVolume" => 0,
            //"error" => '',
        );
        $this->raw = (Object) array(
            "needInsurance" => false,
            "productQty" => 0,
            "productCost" => 0,
            "productWidth" => 0,
            "productWidthUnit" => "",
            "productHeight" => 0,
            "productHeightUnit" => "",
            "productLength" => 0,
            "productLengthUnit" => "",
            "productWeight" => 0,
            "productWeightUnit" => "",
            "travelMode" => "",
            "productCategory" => "",
            "distance" => 0,
        );
        foreach ($array as $key => $val) {
            $this->raw->$key = $val;
        }
        $this->raw->needInsurance = ($this->raw->needInsurance == 'yes') ? true : false;
        $this->get_distance();
        $this->get_setting();

    }
    public function get_information()
    {
        return $this->information;
    }
    public function calculate()
    {
        $this->get_insurance();
        if (!$this->get_category_data()) {
            return;
        }

        $this->information->weight = $this->get_weight($this->raw->productWeight, $this->raw->productWeightUnit);
        $this->information->showVolume = $this->raw->productWidth * $this->raw->productHeight * $this->raw->productLength;
        $this->get_volume(array(
            "width" => $this->raw->productWidth,
            "widthunit" => $this->raw->productWidthUnit,
            "height" => $this->raw->productHeight,
            "heightunit" => $this->raw->productHeightUnit,
            "length" => $this->raw->productLength,
            "lengthunit" => $this->raw->productLengthUnit,
        ));
	
        if ($this->get_cost() > 0) {
            $this->information->shippingcost = $this->get_cost();
            if ($this->information->shippingcost > 0) {} else {

                $this->information->error = "Package info you entered does not match with our shipping conditions, Please check pacakge info and try again.";
            }
        } else {
            if ($this->information->error == '') {
                $this->information->error = "Package info you entered does not match with our shipping conditions, Please check pacakge info and try again.";
            }

        }
    }
    private function get_cost()
    {
        $intch = $this->get_size_in_inch($this->raw->productHeight, $this->raw->productHeightUnit) + $this->get_size_in_inch($this->raw->productLength, $this->raw->productLengthUnit) + $this->get_size_in_inch($this->raw->productWidth, $this->raw->productWidthUnit);
        if (isset($this->category->Shipping)) {
            if (is_array($this->category->Shipping)) {
                $distance = $this->get_distance();

                if (in_array(strtolower(trim($this->raw->productCategory)), array('suitcase', 'electronics', 'document', 'other')) && $this->raw->travelMode == 'air') {
                    if ($this->information->weight > 500) {
                        $this->information->error = "The weight you entered is outside the limits allowed by the airlines. If possible, split your package into smaller packages or choose to ship your item by sea.";
                        return;
                    } else if ($intch > 62) {

                        $this->information->error = "The dimensions you entered are outside the limits allowed by the airlines. If possible, split your package into smaller packages or choose to ship your item by sea.";
                        return; // 50 * (int)$this->raw->productQty;
                    }

                }

                foreach ($this->category->Shipping as $key) {
                    if ($this->category->ChargeType == 'distance') {
                        if ($distance >= $key['MinDistance'] && $distance <= $key['MaxDistance']) {
                            return $key['Rate'] * (int) $this->raw->productQty;
                        }
                    } else if ($this->category->ChargeType == 'fixed') {
                        if ($this->information->weight >= $key['MinDistance'] && $this->information->weight <= $key['MaxDistance']) {
                            return $key['Rate'] * (int) $this->raw->productQty;
                        }
                    }
                }
            }
        }
    }

    private function get_category_data()
    {
        global $db;
        $categorycoll = $db->category;

        $data = $categorycoll->find(array('Content' => $this->raw->productCategory, 'TravelMode' => $this->raw->travelMode, 'Status' => 'Active'));

        if ($data->count() > 0) {
            $this->category = (Object) $data->getNext();
            return 1;
        } else {
            $this->information->error = "The category you have selected is not found.";
            return;
        }

    }
    private function get_setting()
    {
        global $db;
        $categorycoll = $db->configuration;

        $data = $categorycoll->find(array('_id' => new MongoId('5673e33e6734c4f874685c84')), array('Insurance'));
        if ($data->count() > 0) {
            $this->setting = (Object) $data->getNext();
        }
        return 1;
    }
    private function get_distance()
    {
        return $this->information->distance = floatval($this->raw->distance) * 0.000621371;
    }

    private function get_weight($weight, $type)
    {
        $type = strtolower($type);
        if ($type == 'kg') {
            return (((float) $weight) * 2.20462);
        } else if ($type == 'gram') {
            return (((float) $weight) * 0.00220462);
        } else {
            return floatval($weight);
        }
    }
    private function get_volume($array)
    {
        $this->information->volume = ($this->get_size_in_feet($array['length'], $array['lengthunit']) * $this->get_size_in_feet($array['width'], $array['widthunit']) * $this->get_size_in_feet($array['height'], $array['heightunit']));
    }
    public function get_size_in_feet($height, $unit)
    {
        $InFeet = (float) $height;
        $unit = strtolower($unit);
        if ($unit == 'inches') {
            $InFeet = $InFeet * 0.0833333;
        } else if ($unit == 'meter') {
            $InFeet = $InFeet * 3.28084;
        } else if ($unit == 'cm') {
            $InFeet = $InFeet * 0.0328084;
        }
        return $InFeet;
    }
    private function get_size_in_inch($length, $unit)
    {
        $unit = strtolower($unit);
        switch ($unit) {
            case 'inches':
                return floatval($length);
                break;
            case 'cm':
                return floatval($length) * 0.393701;
                break;
        }
    }
    public function get_insurance()
    {
        if ($this->raw->needInsurance) {
            if (count($this->setting) > 0) {
                if (is_array($this->setting->Insurance)) {
                    foreach ($this->setting->Insurance as $key) {
                        if ($key['MinPrice'] <= (float) $this->raw->productCost && $key['MaxPrice'] >= (float) $this->raw->productCost) {
                            $this->information->insurance = $key['Rate'] * (int) $this->raw->productQty;
                            return;
                        }
                    }
                }
            }
            if ($this->information->insurance < 1) {
                $this->information->error = "Sorry! We are not able to provide insurence.";
            }
        }

    }
}
