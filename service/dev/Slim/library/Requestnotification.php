<?php

require_once 'vendor/Notification/Notification.php';

function carriernotication($requestId)
{
    global $db;
    global $app;
    $notificationColl = $db->notification;
    $requestId = new MongoId($requestId);
    $configColl = $db->system_setting;
    $collection = $db->delivery_request;
    $noticoll = $db->notification;
    $userCollection = $db->users;

    $requestData = $collection->find(array('_id' => $requestId));
    $IphoneId = $AndroidId = $SendcarrierId = $carrierId = array();
    if ($requestData->count() > 0) {
        $requestData = $requestData->getNext();
        $requesterid = (string) $requestData['RequesterId'];
        $packageTitle = $requestData['ProductTitle'];

        $field = array('NotificationId', 'DeviceType', 'NoficationStatus', 'TPSetting');

        $where = array('$or' => array(
            array('_id' => array('$ne' => $requestData['RequesterId']), 'UserType' => array('$in' => array('both', 'transporter')), 'TransporterType' => 'business', 'NoficationStatus' => 'on', 'TPSetting' => 'on','TransporterStatus'=>'active'),
            array('_id' => array('$ne' => $requestData['RequesterId']), 'UserType' => array('$in' => array('both', 'transporter')), 'TransporterType' => 'individual', 'NoficationStatus' => 'on', 'TPSetting' => 'on','TransporterStatus'=>'active'),
        ));
        if (@$requestData['TripId'] != '') // if request is a trip request
        {
            $where = array('_id' => $requestData['TransporterId'], 'NoficationStatus' => 'on', 'TPSetting' => 'on');
        }

        $userdata = $userCollection->find($where, $field);

        if ($userdata->count() > 0) {

            $msg = "A request for package \"{$requestData['ProductTitle']}\" is ready from {$requestData['PickupAddress']}, {$requestData['PickupCity']}, {$requestData['PickupCountry']} to {$requestData['DeliveryAddress']}, {$requestData['DeliveryCity']}, {$requestData['DeliveryCountry']}";

            $insNotification = array(
                "NotificationTitle" => "New request for delivery",
                "NotificationShortMessage" => "Request for package \"{$requestData['ProductTitle']}\" has been ready to deliver.",
                "NotificationMessage" => $msg,
                "NotificationType" => "",
                "NotificationUserId" => array(),
                "Date" => new MongoDate(),
                "GroupTo" => "User",
            );
            if (@$requestData['TripId'] != '') {
                // if request is a trip request

                $msg = "\"{$requestData['RequesterName']}\" is intrested to send their package \"{$requestData['ProductTitle']}\"  from {$requestData['PickupAddress']}, {$requestData['PickupCity']}, {$requestData['PickupCountry']} to {$requestData['DeliveryAddress']}, {$requestData['DeliveryCity']}, {$requestData['DeliveryCountry']}";
            }
            $pushnoti = new Notification();
            $pushnoti->setValue('title', "New request to delivery");
            $pushnoti->setValue('message', $msg);
            $pushnoti->setValue('location', 'transporter_delivery_detail');
            $pushnoti->setValue('locationkey', (string) $requestId);
            foreach ($userdata as $key) {
                if ($key['TPSetting'] == 'on' && $key['NoficationStatus'] == 'on') {
                    $pushnoti->add_user($key['NotificationId'], $key['DeviceType']);
                }
                $insNotification['NotificationUserId'][] = $key['_id'];
            }
            if (count($insNotification['NotificationUserId']) > 0) {
                $pushnoti->fire();
                $notificationColl->insert($insNotification);
            }
        }

    }
}

function notify_on_trip($auth)
{
    global $db;
    global $app;
    $UCollection = $db->users;
    $coll = $db->notification;

    $transporterinfomration = $UCollection->find(array('_id' => $auth['TransporterId']), array('EmailStatus', 'Email', 'Name', 'NotificationId', 'DeviceType', 'NoficationStatus', 'TPSetting'));

    if ($transporterinfomration->count() > 0) {
        $transporterinfomration = (Object) $transporterinfomration->getNext();

        // End of email

        if ($transporterinfomration->TPSetting == 'on' && $transporterinfomration->EmailStatus == 'on') {
            $ETemplate = array(
                "to" => $transporterinfomration->Email,
                "replace" => array(
                    "[USERNAME]" => $transporterinfomration->Name,
                    "[PACKAGEID]" => $auth['PackageNumber'],
                    "[PACKAGETITLE]" => $auth['ProductTitle'],
                    "[SOURCE]" => $auth['PickupFullAddress'],
                    "[DESTINATION]" => $auth['DeliveryFullAddress'],
                ),
            );

            if ($app->request->post('requesttype') == 'old') {
                send_mail('569f82bc5509251cd67773f2', $ETemplate);

            } else {

                send_mail('569e16b65509251cd67773f1', $ETemplate);
            }

        }
        if ($transporterinfomration->NoficationStatus == 'on') {
            require_once 'Slim/library/Requestnotification.php';

            $pushnoti = new Notification();
            if ($app->request->post('requesttype') == 'old') {
                $pushnoti->setValue('title', "Trip Request Updated");
                $pushnoti->setValue('message', "\"{$auth['ProductTitle']}\" is updated by user.");

            } else {
                $pushnoti->setValue('title', "One Trip Request");
                $pushnoti->setValue('message', "Requester:\"{$auth['RequesterName']}\" is create a request for trip.");
            }

            $pushnoti->setValue('location', 'transporter_delivery_detail');
            $pushnoti->setValue('locationkey', $app->request->post('packageId'));
            $pushnoti->add_user($transporterinfomration->NotificationId, $transporterinfomration->DeviceType);

            $pushnoti->fire();
        }

        $insNotification = array(
            'NotificationType' => "Prepared new request",
            'NotificationUserId' => array($transporterinfomration->_id),
            'Date' => new MongoDate(),
            'GroupTo' => 'User',
        );
        if ($app->request->post('requesttype') == 'old') {
            $insNotification['NotificationTitle'] = "Trip Request Updated";
            $insNotification['NotificationMessage'] = "Requester:\"{$auth['RequesterName']}\" is updated their request. Which is posted for you.";
        } else {
            $insNotification['NotificationTitle'] = "One Trip Request";
            $insNotification['NotificationMessage'] = "Requester:\"{$auth['RequesterName']}\" is create a request for trip.";
        }

        // Notification Section
        if (isset($insNotification)) {
            $coll->insert($insNotification);
        }

    }
}
