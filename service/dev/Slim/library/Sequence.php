<?php

function getSequence($name)
{
	global $db;
	$val = $db->command(
		  array(
			"findandmodify" => "counters",
			"query" => array("_id"=> $name),
			"update" => array('$inc'=> array("seq"=> 1)),
		  )
		);
	if(!isset($val['value']['seq'])){
		$collection = $db->counters;
		$collection->insert(array('_id'=>$name,'seq'=>2));
		$val['value']['seq'] = 1;	
	}
	return @$val['value']['seq'];
}
