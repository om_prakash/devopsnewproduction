<?php

$userId = '';
$userType = '';
$userInformation = (Object) array('NotificationId' => '');
if( !function_exists('apache_request_headers') ) {
    function apache_request_headers() {
      $arh = array();
      $rx_http = '/\AHTTP_/';
      foreach($_SERVER as $key => $val) {
        if( preg_match($rx_http, $key) ) {
          $arh_key = preg_replace($rx_http, '', $key);
          $rx_matches = array();
          // do some nasty string manipulations to restore the original letter case
          // this should work in most cases
          $rx_matches = explode('_', $arh_key);
          if( count($rx_matches) > 0 and strlen($arh_key) > 2 ) {
            foreach($rx_matches as $ak_key => $ak_val) $rx_matches[$ak_key] = ucfirst($ak_val);
            $arh_key = implode('-', $rx_matches);
          }
          $arh[$arh_key] = $val;
        }
      }
      return( $arh );
    }
}
if (!function_exists('getallheaders'))  
{
    function getallheaders() {
        foreach($_SERVER as $K=>$V){
            $a=explode('_' ,$K);
            if(array_shift($a)=='HTTP'){
                array_walk($a,function(&$v){$v=ucfirst(strtolower($v));});
                $retval[join('-',$a)]=$V;
            }
        }
        return $retval;
    }
}

function isValidUser()
{
    // Getting request headers
    global $db;
    global $userId;
    global $userType;
    global $userInformation;
    $headers = $_SERVER; //apache_request_headers();
    $app = \Slim\Slim::getInstance();
    $response = array();

    // Verifying Authorization Header
    if (isset($headers['HTTP_APIKEY'])) {
        $api_key = $headers['HTTP_APIKEY'];

        if (strlen($api_key) == 24) {
            $api_key = new MongoId($api_key);

            $collection = $db->users;
            $Auth = $collection->find(array('_id' => $api_key), array('TPSetting', 'NoficationStatus', 'EmailStatus', '_id', 'UserType', 'TransporterStatus', 'RequesterStatus', 'Name', 'Email', 'Default_Currency', 'FirstName','NotificationId'));
            if ($Auth->count() > 0) {
                $Auth = $Auth->getNext();
                if ($Auth['RequesterStatus'] == 'active' or $Auth['TransporterStatus'] == 'active' or ($Auth['RequesterStatus'] == 'active' and $Auth['TransporterStatus'] == 'active')) {
                    $userId = $api_key;
                    $userType = $Auth['UserType'];
                    $userInformation = (Object) array(
                        '_id' => $Auth['_id'],
                        'Name' => $Auth['Name'],
                        'FirstName' => $Auth['FirstName'],
                        'RequesterStatus' => $Auth['RequesterStatus'],
                        'TransporterStatus' => $Auth['TransporterStatus'],
                        'Email' => @$Auth['Email'],
                        'EmailStatus' => @$Auth['EmailStatus'],
                        'NoficationStatus' => @$Auth['NoficationStatus'],
                        'TPSetting' => @$Auth['TPSetting'],
                        'NotificationId' => @$Auth['NotificationId'],
                        'Default_Currency' => @$Auth['Default_Currency'],
                    );
                } else {
                    echo json_encode(array("error" => true, "success" => 0, "msg" => "Your account is inactive, please contact support."));
                    $app->stop();
                }

            } else {
                echo json_encode(array("error" => true, "success" => 0, "msg" => "Access Denied. Invalid Api key"));
                $app->stop();
            }

        } else {
            echo json_encode(array("error" => true, "success" => 0, "msg" => "Access Denied. Invalid Api key"));
            $app->stop();
        }
    } else {
        echo json_encode(array("error" => true, "success" => 0, "msg" => "Api key is misssing", "headers" => getallheaders()));
        $app->stop();
    }
}

function authenticate(\Slim\Route $route)
{
   // echo "string";die;
    isValidUser();

}

function requester_auth(\Slim\Route $route)
{
    isValidUser();
    global $userInformation;
    if (@$userInformation->RequesterStatus != 'active') {
        $app = \Slim\Slim::getInstance();
        echo json_encode(array("success" => 0, "msg" => "Your account is inactive, please contact support."));
        $app->stop();
    }
}

function transporter_auth(\Slim\Route $route)
{
    isValidUser();
    global $userInformation;
    if (@$userInformation->TransporterStatus != 'active') {
        $app = \Slim\Slim::getInstance();
        echo json_encode(array("success" => 0, "msg" => "Your account is inactive, please contact support."));
        $app->stop();
    }
}

function echoRespnse($status_code, $response)
{
    $app = \Slim\Slim::getInstance();
    // Http response code
    $app->status($status_code);

    // setting response content type to json
    $app->contentType('application/json');

    echo json_encode($response);
}

function verifyRequiredParams($required_fields)
{
    $error = false;
    $error_fields = "";
    $request_params = array();
    $request_params = $_REQUEST;
    // Handling PUT request params
    if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
        $app = \Slim\Slim::getInstance();
        parse_str($app->request()->getBody(), $request_params);
    }
    foreach ($required_fields as $field) {
        if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
            $error = true;
            $error_fields .= $field . ', ';
        }
    }

    if ($error) {
        // Required field(s) are missing or empty
        // echo error json and stop the app
        $response = array();
        $app = \Slim\Slim::getInstance();
        $response["error"] = true;
        $response["success"] = 0;
        $response["msg"] = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';

        echo json_encode($response);
        $app->stop();
    }
}
