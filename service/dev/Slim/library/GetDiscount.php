<?php
function get_discount_amt($PromoCode,$TotalAmount,$convertAmount=0,$currency = '')
{
	global $db;
	global $userInformation;
	$response = array("success" => 0, "msg"=> 'The promo code you have entered is not valid.');
	$promocodecollection 	= $db->promocode;
	$deliveryrequestcoll	= $db->delivery_request;
	
	$discountdata = $promocodecollection->find(array('Code'=>strtoupper($PromoCode),'ValidFrom' => array('$lt' => new MongoDate()),'Status'=>'active'));
		


	if($discountdata->count() > 0)
	{
		$discountdata = (Object)$discountdata->getNext();

		if($discountdata->ValidTill > new MongoDate())
		{

			if($TotalAmount >= $discountdata->MinimumOrderPrice)
			{
				if($discountdata->UsesCount >= $discountdata->MaxUses) {
					$response['msg'] = 'This promo code has expired.';
				}else{

					$usescount = $deliveryrequestcoll->find(array('RequesterId'=>$userInformation->_id,'PromoCode'=>strtoupper($PromoCode)));
					
					if($usescount->count() >=  $discountdata->MaxUsesPerPerson)
					{
						$response['msg'] = 'You have already used this promo code.';
					}else{
						if($discountdata->DiscountType == 'percent') {
							$discount = ($TotalAmount * $discountdata->DiscountAmount) / 100;
						} else {
							$discount = $discountdata->DiscountAmount;
						}

						if($discount > $discountdata->MaximumDiscount) {
							$discount = $discountdata->MaximumDiscount;
						}
						if($discount > $TotalAmount) {
							$response['msg'] = 'The promo code you have entered is not valid for this delivery request.';
						} else{

							$promocodecollection->update(array('Code' =>strtoupper($PromoCode)), array('$inc' => ['UsesCount'=>1]));
							
							$response = array('success'=> 1,'discount'=>$discount);
							$response['msg'] = "Success! You get a discount of $".$discount;
							$response['GhanaTotalCost'] = '';
							
							$currencycoll = $db->city_state_country;
							$currencydata = $currencycoll->find(array('CurrencyCode' => $currency ));
							$ghanacurrency = 0;
							if($currencydata->count() > 0) {

								$currencydata = $currencydata->getNext();
								$ghanacurrency = number_format((($convertAmount - $discount) * (float)@$currencydata['CurrencyRate']),2);
								$response['GhanaTotalCost'] = str_replace('[AMT]', $ghanacurrency, @$currencydata['FormatedText']);
							}

						}	
					}
				}			
			} else {
				$response['msg'] = "Oops! Minimum order amount should be greater than $discountdata->MinimumOrderPrice.";
			}
		} else {
			$response['msg'] = 'This promo code has expired.';
		}	
	}
	return $response;
}