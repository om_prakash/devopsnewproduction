<?php

class ReqHelper {
	public $db;
	public function __construct() {
		global $db;
		$this->db = $db;
	}

	

	public function get_aquantuo_fees($totalcost,$fees) {
		$fees = ((floatval($totalcost) * $fees) / 100);
		return $fees;
	}

	public function getRate($products, $data, $userinfo, $country=''){
		$data['airItemWithWeightCount'] = 0;
		$data['air_item_count'] = 0;
		$response = [
			'success' => 0,
			'msg' => 'Oops! Something went wrong.',
			'total_amount' => 0,
			'ghana_total_amount' => 0,
			'shipping_cost' => 0,
			'total_weight' => 0,
			
		];
		$total_weight = 0;
		$item_cost = 0;
		$shipping_cost_by_user = 0;
		

		$categoryColl = $this->db->configuration;
		$config = $categoryColl->find(['_id' => new MongoId('5673e33e6734c4f874685c84')]);

		$aq_fees = 0;
		if ($config->count() > 0) {
			$config = $config->getNext();
			$aq_fees = $config['aquantuoFees'];
			$processing_fees = $config['ProcessingFees'];
		}

		$already_calculated_category = [];
		$category_set = [];

		foreach ($products as $key => $val) {
			$item_cost += @$val['price'] * @$val['qty'];
			if ($val['travelMode'] == 'air' && $val['weight'] !== '' && $val['weight'] !== 0) {
				//$total_weight += $this->GetWeight($val['weight'], $val['weight_unit']) * (int) $val['qty'];
				$total_weight += $this->GetVolumetricWeightInLbs($val['length'],$val['height'],$val['width'],$val['weight'],$val['weight_unit']) * (int) $val['qty'];
			}
			if(isset($val['shipping_cost_by_user'])){
				$shipping_cost_by_user += $val['shipping_cost_by_user'];
			}
		}
		$response['total_weight'] = $total_weight;
		// comment the code no restrction 
		//$dimention = $this->calculation_inch_volume($products);
		$dimention = 1;
		if ($total_weight > 500) {
			$response['msg'] = "The weight you entered is outside the limits allowed by the airlines. If possible, split your package into smaller packages or choose to ship your item by sea.";
			return $response;
		} else if ($dimention > 1000) {
			$response['msg'] = "The dimensions you entered are outside the limits allowed by the airlines. If possible, split your package into smaller packages or choose to ship your item by sea.";
			return $response;
		} else if ($data['distance'] <= 0) {
			$response['msg'] = "Unable to calculate distance.";
			return $response;
		} else {
			// get All  Category 
			$categoryColl = $this->db->category;
			$Category = $categoryColl->find(array('Status' => 'Active'));
			
			$mappedCategory = [];
			foreach ($Category as $key => $value) {
				$mappedCategory[(string)$value['_id']] = [
					'Shipping'=> $value['Shipping'],
					'price_increase_by'=> @$value['price_increase_by'],
					'weight_increase_by'=> @$value['weight_increase_by'],
					'weightArray' => @$value['weightArray'],
					'custom_duty' => @$value['custom_duty']
				];
			}
			$information = ['Category' => $mappedCategory,'consolidate_check'=> @$data['consolidate_check']];
			$res = $this->separateByAirCategory($products,$information);
			
			if (!isset($data['distance_calculated'])) {
				$data['distance_calculated'] = false;
			}
			
			$information = ['Category' => $mappedCategory,'distance_calculated'=> $data['distance_calculated'],'distance'=> $data['distance'],'separateByAirCategory'=>$res,'is_customs_duty_applied'=>@$data['is_customs_duty_applied'],'pakage_type'=>$data['pakage_type'],'consolidate_check'=> @$data['consolidate_check']];

			$res = $this->SetRates($products,$information);
			$prd = $products[0];
			
			$prd['weight'] = $total_weight;
			$prd['weight_unit'] = 'lbs';
			$res1 = $this->SetRates([$prd],$information);
		
			if($res['success'] == 1){
				$response['success'] = 1;
				$response['msg'] = 'Shipping calculation.';
				$data['insurance'] = $res['insurance'];
				$data['insurance_cost'] = $res['insurance'];
				$data['shipping_cost'] = $res1['shippingCost'];
				//Duty and custom cost calculation
				$DutyAndCustom = $res['total_custom_duty'];
					
				if($res['total_custom_duty']>0 && @$data['is_customs_duty_applied']){
					$data['DutyAndCustom'] =  $DutyAndCustom;
				}else{
					$data['DutyAndCustom'] =  0.0;
				}
				//End Duty and custom cost calculation
				//Tax Fee Calculation
				$Tax = $res['total_item_cost'] * $res['config_data']['tax'];
				$data['Tax'] =  $Tax / 100;
				//End Tax Fee Calculation 
				$data['ProcessingFees'] = $res['ProcessingFees'];
				$data['total_item_cost'] = $res['total_item_cost'];
				// $config = Configuration::find('5673e33e6734c4f874685c84');
				// print_r($config);die();
				if ($country === 'Kenya') {
					$extra = 0;
					$data['DutyAndCustom'] =  0.0;
					if ($res['total_item_cost'] > 100) {
						$extra = ($res['total_item_cost'] * $config['kenya_per_item_value'])/100;
						$extra = number_format($extra, 2, '.', '');
					}
					// if ($data['weight_unit'] === 'kg') {
					// 	$ratePerKg = number_format(($data['weight'] * $config['kenya_rate_per_kilogram']), 2, '.', '');
					// } else {
						// if ($response['total_weight'] > 0) {
							$ratePerKg = number_format(($total_weight * 0.453592 * $config['kenya_rate_per_kilogram']), 2, '.', '');
						// } else {
						// 	$ratePerKg = $config['kenya_rate_per_kilogram'];
						// }
					// }
					$data['shipping_cost'] = $ratePerKg < $config['kenya_rate_per_kilogram'] ? 17 : $ratePerKg;
					$data['total_amount'] = $data['shipping_cost'] + $data['insurance_cost'] + $extra;
					$data['total_amount_txt'] = 'ratePerKg - ' .  $ratePerKg . 'extra - ' .  $extra;
					if ($data['total_amount'] < $config['kenya_rate_per_kilogram']) {
						$data['total_amount'] = $config['kenya_rate_per_kilogram'];
					}
					// if ($data['type'] == 'buyforme') {
					// 	$data['total_amount'] += $res['ProcessingFees'];
					// }
				} else {
					$data['total_amount'] = $data['shipping_cost'] + $res['insurance'] + $data['DutyAndCustom'] + $data['Tax']+ $res['ProcessingFees'] + $shipping_cost_by_user;
				}

				if($data['type'] == 'buyforme'){
					$data['total_amount'] += $res['total_item_cost'];
					$data['total_amount'] += $res['ProcessingFees'];
				}
				$data['total_weight'] = $total_weight;
				$data['total_volume'] = $res['total_volume'];
				
				$data['item_cost'] = $item_cost;
				$data['shipping_cost_by_user'] = $shipping_cost_by_user;
				$data['aq_fee'] = $res['aq_fee'];
				
				$De_currency = '';
				if (isset($userinfo->Default_Currency)) {
					if (in_array($userinfo->Default_Currency, ['', 'USD'])) {
						$userinfo->Default_Currency = 'GHS';
						$De_currency = 'GHS';
					}
				} else {
					$De_currency = $userinfo['Default_Currency'];
				}
				$city_state_country = $this->db->city_state_country;
				$currency = $city_state_country->find(['CurrencyCode' => $userinfo['Default_Currency']]);
				if ($currency->count() > 0) {
					$currency = $currency->getNext();
					$data['ghana_total_amount'] = $data['total_amount'] * $currency['CurrencyRate'];
				}
				$data['product'] = $res['product'];
			}
			$response['data'] = $data;
			$response['total_amount'] = $data['total_amount'];
			
			return $response;
		}

	}

	public function category_count($array,$cat_id){
		$res['count'] = 0;
		$res['weight'] = 0;
		foreach ($array as $key => $value) {
			if($cat_id == $value['categoryid']){
				$res['count'] += 1;
				//$res['weight'] += $this->GetWeight($value['weight'], $value['weight_unit']) * (int) $value['qty'];
				$res['weight'] += $this->GetVolumetricWeightInLbs($value['length'],$value['height'],$value['width'],$value['weight'],$value['weight_unit']);
			}
		}
		return $res;
	}

	public function set_air_category($array,$inputData){
		$categoryColl = $this->db->category;
		$already_calculated_category = [];
		$category_set = [];
		foreach ($array as $key => $val) {
			if ($val['travelMode'] == 'air') {
				if(!in_array($val['categoryid'], $already_calculated_category)){
					$cat_weight = $this->category_count($array,$val['categoryid']);
					$weight =$cat_weight['weight'];
					$already_calculated_category[]=$val['categoryid'];
					
					$data = $categoryColl->find([
							'_id' => new MongoId($val['categoryid']),
							'Status' => 'Active',
							'TravelMode' => 'air',
							'ChargeType' => 'fixed',
						], ['Shipping']);

					if (count($data) > 0) {
						$data = $data->getNext();
						$rate = 0;

						foreach ($data['Shipping'] as $key2) {
							if ($key2['MinDistance'] <= floatval($weight)  && $key2['MaxDistance'] >= floatval($weight)) {
								if($inputData['consolidate_check'] == 'on'){
									$rate = $key2['CONSOLIDATE_RATE'];
								}else{
									$rate = $key2['Rate'];
								}
							}
						}
						
						if($rate > 0){
							$category_set[]=[
								'cat_id'=>$val['categoryid'],
								'rate'=> $rate,
							];
						}

					}
				}
			}
		}

		return $category_set;
	}

	//Close
	/*
	     *Developer Name :Kapil Pancholi
	     *Function :This function is used to calculate shiping price of product which         travel mode is air
	     *Update at:25-5-2017
*/

	public function get_category_price($category_set,$id){
		$price = 0;

		foreach ($category_set as $key) {
			if($key['cat_id'] == $id){
				$price = $key['rate'];
			}
		}
		return $price;

	}


	public function get_shipping_rate($array,$weight,$inputData,$air_category) {
		$res['item'] = 0;
		$res['rate'] = 0;
		$res['existance'] = false;
		$res['success'] = 0;

		$res['air_rate'] = 0;
		$res['air_item_count'] = 0;


		$categoryColl = $this->db->category;
		$distance = $this->get_distance($inputData['distance']);
		foreach ($array as $key => $val) {

			if ($val['travelMode'] == 'air') {

				$rate = $this->get_category_price($air_category,$val['categoryid']);
				$cat_count = $this->category_count($array,$val['categoryid']);
				$array[$key]['shippingCost'] = $rate / $cat_count['count'];
				$res['air_rate'] += $rate / $cat_count['count'];
				$res['success'] = 1;
				if ($array[$key]['shippingCost'] == 0) {
					$res['success'] = 0;
					$res['msg'] = "Shipping conditions does not match with product '" . $val['product_name'] . "'";
				}

			} else if ($val['travelMode'] == 'ship') {
				$categoryInfo = $categoryColl->find([
					'_id' => new MongoId($val['categoryid']),
					'Status' => 'Active',
					'TravelMode' => 'ship',
					'ChargeType' => 'distance',

				], ['Shipping']);
				if ($categoryInfo->count() > 0) {
					$categoryInfo = $categoryInfo->getNext();
					foreach ($categoryInfo['Shipping'] as $key2) {
						if ($key2['MinDistance'] <= $distance && $key2['MaxDistance'] >= $distance) {
							$res['rate'] += $key2['Rate'];
							$array[$key]['shippingCost'] = $key2['Rate'];
							$res['success'] = 1;
						}
					}

					if ($array[$key]['shippingCost'] == 0) {
						$res['success'] = 0;
						$res['msg'] = "Shipping conditions does not match with product '" . @$val['product_name'] . "'";
						return $res;
					}
				} else {
					$res['success'] = 0;
					$res['msg'] = "Shipping conditions does not match with product '" . @$val['product_name'] . "'";
					return $res;
				}

			}
		}
		$res['rate'] += $res['air_rate'];
		$res['product'] = $array;
		return $res;

	}

	
	/*
	     *Developer Name :Kapil Pancholi
	     *Function :This function is used to calculate shiping price of product which         travel mode is ship
	     *Update at:25-5-2017
	*/
	//close
	/*
	     *Developer Name :Kapil Pancholi
	     *Function :This function is used to calculate volume in inch
	     *Update at:24-5-2017
*/
	public function calculation_inch_volume($array) {
		$intch = 0;
		foreach ($array as $key) {
			if ($key['travelMode'] == 'air') {
				$intch += $this->get_size_in_inch(@$key['height'], 'inches') + $this->get_size_in_inch(@$key['length'], 'inches') + $this->get_size_in_inch(@$key['width'], 'inches');
				$intch = $intch * (int) $key['qty'];
			}
		}
		return $intch;

	} //close
	/*
	     *Developer Name :Kapil Pancholi
	     *Function :This function is used to convert convert dimention unit
	     *Update at:24-5-2017
*/
	public function get_size_in_inch($length, $unit) {
		$unit = strtolower($unit);
		switch ($unit) {
		case 'inches':
			return floatval($length);
			break;
		case 'cm':
			return floatval($length) * 0.393701;
			break;
		}
	} //close
	/*
	     *Developer Name :Kapil Pancholi
	     *Function :This function is used to calculate insurance
	     *Update at:24-5-2017
*/
	public function GetInsurance($product) {
		$res = [
			'insurance' => 0,
			'existance' => false,
			'price' => 0,
			'product' => [],
		];

		foreach ($product as $productKey => $item) {

			$product[$productKey]['insurance'] = 0;
			if ($item['insurance_status'] == 'yes') {

				//$res['existance'] = true;
				$res['price'] += $item['price'] * $item['qty'];
				$config = $this->db->configuration;
				$configdata = $config->find(
					['_id' => new MongoId('5673e33e6734c4f874685c84')],
					['Insurance']
				);

				if ($configdata->count() > 0) {
					$configdata = $configdata->getNext();
					if (is_array($configdata['Insurance'])) {
						foreach ($configdata['Insurance'] as $key) {
							if ($key['MinPrice'] <= (float) $item['price'] && $key['MaxPrice'] >= (float) $item['price']) {
								$res['insurance'] += $key['Rate'] * $item['qty'];
								$product[$productKey]['insurance'] = $key['Rate'] * $item['qty'];
								$product[$productKey]['InsuranceCost'] = $key['Rate'] * $item['qty'];
								//  update in 22 march
								break;
							}
						}
					}
				}
			}
		}
		$res['product'] = $product;
		return $res;
	} //close
	/*
	     *Developer Name :Kapil Pancholi
	     *Function :This function is used to convert weight in to lbs
	     *Update at:24-5-2017
*/
	public function GetWeight($weight, $type) {
		$type = strtolower($type);
		if ($type == 'kg') {
			return (((float) $weight) * 2.20462);
		} else if ($type == 'gram') {
			return (((float) $weight) * 0.00220462);
		} else {
			return $weight;
		}
	}
	/*
		     *Developer Name :Kapil Pancholi
		     *Function :This function is used to convert distance in to miles
		     *Update at:25-5-2017
	*/
	public function get_distance($distance) {
		return $distance = floatval($distance) * 0.000621371;
	} //close
	/*
	 * TODO return Volumetric Weight in lbs
	 * 1 lbs = 2.20462 kg
	 * 1lbs = 0.00220462 gram
	 *
	 */
	public function GetVolumetricWeightInLbs($length,$height,$width,$weight,$type) {
		if($weight === '' || $weight === 0){
			$weight = 0;
		}
		$VolumetricInfo = ["length"=>$length,"height"=>$height,"width"=>$width,"type"=>$type];
		
		if($type=='lbs'){
				$VolumetricWeight = ceil(($length *$width* $height) / 139);
		}else{
				$VolumetricWeight = ceil(($length *$width* $height) / 5000);
		}
			
		
       
		if($weight>0){
			$VolumetricWeight  = $weight;
		}
        $type = strtolower($type);
		if ($type == 'kg') {
			$VolumetricWeightLbs = (((float) $VolumetricWeight) * 2.20462);
		} else if ($type == 'gram') {
			$VolumetricWeightLbs = (((float) $VolumetricWeight) * 0.00220462);
		} else {
			$VolumetricWeightLbs = $VolumetricWeight;
		}
		return $VolumetricWeightLbs;
	}
	/*
	 * TODO return Volumetric Weight in lbs
	 * 1 lbs = 2.20462 kg
	 * 1lbs = 0.00220462 gram
	 *
	 */
	public function GetVolumeWeightInLbs($length,$height,$width,$weight,$type) {
		$VolumetricInfo = ["length"=>$length,"height"=>$height,"width"=>$width,"type"=>$type];
		
		
		$VolumetricWeight = ceil(($length *$width* $height));
			
        $type = strtolower($type);
		if ($type == 'kg') {
			$VolumetricWeightLbs = (((float) $VolumetricWeight) * 2.20462);
		} else if ($type == 'gram') {
			$VolumetricWeightLbs = (((float) $VolumetricWeight) * 0.00220462);
		} else {
			$VolumetricWeightLbs = $VolumetricWeight;
		}
		return $VolumetricWeightLbs;
	}
	/*
	 * 
	 */
	 public function separateByAirCategory($products,$information){
		$already_calculated_category = [];
		$category_set = [];
		foreach ($products as $key => $val) {
			
			if(!in_array($val['categoryid'], $already_calculated_category) && $val['categoryid'] != OtherCategory && $val['travelMode'] == 'air' && $val['categoryid'] !=ElectronicsCategory){
				$cat_weight = $this->category_count($products,$val['categoryid']);
				$weight = $cat_weight['weight'];
				$weight = number_format($weight,1); 
				$already_calculated_category[]=$val['categoryid'];
				$data = $information['Category'][$val['categoryid']];
				$rate = 0;
				$custom_duty = @$data['custom_duty'];
				foreach ($data['Shipping'] as $key1) {
					if ($key1['MinDistance'] <= floatval($weight) && $key1['MaxDistance'] >= floatval($weight)) {
                           //changes to day 
							
                           	if(isset($information['consolidate_check'])){
                                if($information['consolidate_check'] == 'on'){
									$rate = $key1['CONSOLIDATE_RATE'];
								}else{
									$rate = $key1['Rate'];
								}
                           	}else{
                           		$rate = $key1['Rate'];
                           	}
                      
					}
				}

				if($rate > 0){
					$category_set[$val['categoryid']]=[
						'cat_id'=>$val['categoryid'],
						'rate'=> $rate,
						'custom_duty'=>$custom_duty
					];
				}
			}
		}
		return $category_set;

	}
	/*
	 * Put this code 
	 */  
	
	public function SetRates($products,$information){
		$returnProducts = $products;
		$returnData = [
			'success'=> 0,
			'msg' => 'Oops! Something went wrong.',
			'product'=>[],
			'insurance' => 0,
			'price' => 0,
			'DutyAndCustom' => 0,
			'Tax'=> 0,
			'ProcessingFees'=> 0,
			'total_weight'=> 0.0,
			'shippingCost'=> 0.0,
			'total_item_cost'=> 0.0,
			'total_custom_duty'=> 0.0,
			'aq_fee'=>0,
			'total_volume'=>0,
		];
		
		if(!$information['distance_calculated']){
			$distance = $this->get_distance($information['distance']);
		}else{
			// $distance = $inputData['distance']->distance;
			$distance = $information['distance'];
		}
		$categoryColl = $this->db->configuration;
		$config = $categoryColl->find(['_id' => new MongoId('5673e33e6734c4f874685c84')]);
		$config = $config->getNext();
		$aq_fees = 0;
		
		foreach ($products as $key => $val) {
			$returnData['total_item_cost'] += $val['price'] * $val['qty'];
			$returnData['total_volume'] += 	$this->GetVolumeWeightInLbs($val['length'],$val['height'],$val['width'],$val['weight'],$val['weight_unit']) * (int) $val['qty'];
			$returnData['total_weight'] += $this->GetVolumetricWeightInLbs($val['length'], $val['height'], $val['width'], $val['weight'], $val['weight_unit']);
			
			if ($val['travelMode'] == 'air' && $val['categoryid'] != OtherCategory && $val['categoryid'] != ElectronicsCategory) {
				$rate = $information['separateByAirCategory'][$val['categoryid']]['rate'];
				$custom_duty = $information['separateByAirCategory'][$val['categoryid']]['custom_duty'];
				$cat_count = $this->category_count($products,$val['categoryid']);
				$returnProducts[$key]['shippingCost'] = $rate / $cat_count['count'];
				$returnData['shippingCost'] += $rate / $cat_count['count'];

				//$itemWeight = $this->GetWeight($val['weight'], $val['weight_unit']) * (int) $val['qty'];
				$itemWeight = $this->GetVolumetricWeightInLbs($val['length'],$val['height'],$val['width'],$val['weight'],$val['weight_unit']) * (int) $val['qty'];
				$returnData['total_weight'] += $itemWeight;
				$returnProducts[$key]['custom_duty'] = 0.0;

				if ($custom_duty > 0) {
					$returnProducts[$key]['custom_duty'] = (($val['price'] * $val['qty'])*$custom_duty)/100;
					$returnData['total_custom_duty'] += (($val['price'] * $val['qty'])*$custom_duty)/100;
				}
			} else if ($val['travelMode'] == 'air' && ($val['categoryid'] == OtherCategory || $val['categoryid'] == ElectronicsCategory)) {
				$data = $information['Category'][$val['categoryid']];
				
				if (!isset($data['Shipping'])) {
					return $returnData;
				}

				//$itemWeight = $this->GetWeight($val['weight'], $val['weight_unit']) * (int) $val['qty'];
				$itemWeight = $this->GetVolumetricWeightInLbs($val['length'],$val['height'],$val['width'],$val['weight'],$val['weight_unit']) * (int) $val['qty'];
				$returnData['total_weight'] += $itemWeight;
				$is_macth = false;
				$last_price = 0;
				$weight_increase_by = $data['weight_increase_by'];
				
				foreach ($data['weightArray'] as $key3) {
					if ($key3['MinWeight'] < floatval($itemWeight)) {
						$weight_increase_by = $key3['Rate'];
					}
				}

				foreach ($data['Shipping'] as $key1) {
					if ($key1['MinDistance'] <= floatval($itemWeight) && $key1['MaxDistance'] >= floatval($itemWeight)) {
						$is_macth = true;
                   		$rate = $key1['Rate'];

                   		if ($val['price'] > 1) {
                   			$p = $val['price'] - 1;
                   			$rate = $rate + ($p * $data['price_increase_by']);
                   		}

                   		$returnProducts[$key]['shippingCost'] = $rate;
                   		//$returnData['shippingCost'] +=  $rate;
                   		$returnData['shippingCost'] += $rate;
	                }

	                $last_price = $key1['Rate'];
				}

				// if weight not range then cacluate from model doc
				if (!$is_macth) {
					$itemWeight_1 = $itemWeight-2;
					$itemWeight_2 = intval($itemWeight_1/2);
					
					$rate_1 =  $itemWeight_2 * $weight_increase_by;
					$rate_2 = $rate_1 + $last_price;
					//calculate price increment 
					$p = $val['price'] - 1;
	                $rate_4 = $rate_2 + ($p * $data['price_increase_by']);
					$rate = $rate_4;
										
					$returnProducts[$key]['shippingCost'] = $rate;
	                $returnData['shippingCost'] += $rate;
				}

				$returnProducts[$key]['custom_duty'] = 0.0;
				
				if ($data['custom_duty'] > 0) {
					$returnProducts[$key]['custom_duty'] = (($val['price'] * $val['qty'])*$data['custom_duty'])/100;
					$returnData['total_custom_duty'] += (($val['price'] * $val['qty'])*$data['custom_duty'])/100;
				}
			} else {
				$data = $information['Category'][$val['categoryid']];

				if (!isset($data['Shipping'])) {
					return $returnData;
				}

				$categoryConstantArray = array(AUTO_FULLSIZE_SUV, AUTO_INTERMEDIATE_SUV, AUTO_SEDAN_ECONOMY, AUTO_SEDAN_FULLSIZE, AUTO_SEDAN_INTERMEDIATE, AUTO_STANDARD_SUV, AUTOMOBILE_VAN);

				if ($val['travelMode'] == 'ship' && in_array($val['categoryid'], $categoryConstantArray)) {
					foreach ($data['Shipping'] as $key1) {
						if ($key1['MinDistance'] <= $distance && $key1['MaxDistance'] >= $distance) {
							if (@$information['consolidate_check'] == 'on') {
								$returnProducts[$key]['shippingCost'] = $key1['CONSOLIDATE_RATE'] * (int) $val['qty'];
								$returnData['shippingCost'] += $key1['CONSOLIDATE_RATE'] * (int) $val['qty'];
							} else {
								$returnProducts[$key]['shippingCost'] = $key1['Rate'] * (int) $val['qty'];
								$returnData['shippingCost'] += $key1['Rate'] * (int) $val['qty'];
							}
						}
					}
				} else {
					$cat_count = $this->category_count($products, $val['categoryid']);

					foreach ($data['Shipping'] as $key1) {
						if ($key1['MinDistance'] <= $distance && $key1['MaxDistance'] >= $distance) {
							if (@$information['consolidate_check'] == 'on') {
								$returnProducts[$key]['shippingCost'] = $key1['CONSOLIDATE_RATE'] / $cat_count['count'];
								$returnData['shippingCost'] += $key1['CONSOLIDATE_RATE'] / $cat_count['count'];
							} else {
								$returnProducts[$key]['shippingCost'] = $key1['Rate'] / $cat_count['count'];
								$returnData['shippingCost'] += $key1['Rate'] / $cat_count['count'];
							}
						}
					}
				}
				
				$returnProducts[$key]['custom_duty'] = 0.0;

				if ($data['custom_duty'] > 0) {
					$returnProducts[$key]['custom_duty'] = (($val['price'] * $val['qty'])*$data['custom_duty'])/100;
					$returnData['total_custom_duty'] += (($val['price'] * $val['qty'])*$data['custom_duty'])/100;
				}
			}
			
			if ($returnProducts[$key]['shippingCost'] == 0) {
				$returnData['msg'] = 'Item info you entered does not match with our shipping conditions, Please check package info and try again.';
				return $returnData;
			}//End shipping cost calculation

			//Insurance cost calculation
			$returnProducts[$key]['insurance'] = 0;
			$returnProducts[$key]['insurance_cost'] = 0;
			
			if ($val['insurance_status'] == 'yes') {
				$returnData['price'] += $val['price'] * $val['qty'];
				$insurance = ($val['price'] * $val['qty']) * $config['main_insurance'];
				$insurance =  $insurance / 100;
				$returnData['insurance'] +=  $insurance;
				$returnProducts[$key]['insurance_cost'] = $insurance;
				$returnProducts[$key]['insurance'] = $insurance;
			}//End Insurance shipping cost calculation
			

			//Processing Fee Calculation BFM Concierge 
			
			if(@$information['pakage_type'] == 'buy_for_me' || @$information['pakage_type'] == 'buyforme'){
				$returnProducts[$key]['ProcessingFees'] = 0;
				$ProcessingFees = ($val['price'] * $val['qty']) * $config['bfm_concierge'];
				$ProcessingFees =  $ProcessingFees / 100;
				$returnData['ProcessingFees'] +=  $ProcessingFees;
				$returnProducts[$key]['ProcessingFees'] = $ProcessingFees;
			}//End Processing Fee Calculation
			else{
				$returnProducts[$key]['ProcessingFees'] = 0;
			}
			
			
			// get Aquantuo fee 
			if (count($config) > 0) {
				$aq_fees = $config['aquantuoFees'];
			}
			$returnProducts[$key]['aq_fee'] = $this->get_aquantuo_fees(($val['price'] * $val['qty']),$aq_fees);
			$returnData['aq_fee'] +=  $returnProducts[$key]['aq_fee'];
			
		}
		
		
		
		$returnData['config_data'] = $config;
		$returnData['success'] = 1;
		$returnData['product'] = $returnProducts;
		return $returnData;
	}
	
}
