<?php
/**
 * Step 1: Require the Slim Framework
 *
 * If you are not using Composer, you need to require the
 * Slim Framework and register its PSR-0 autoloader.
 *
 * If you are using Composer, you can skip this step.
 *
 * Filename Name : authentication.php
 * File Path      : services/authentication.php
 * Description   : This file contains method related to userinformation.
 * Author        : Ravi shukla
 * Created Date  : 22-08-2015
 * Library       : Email,DbConnect
 *
 *
 */

require 'Slim/Slim.php';
require 'Slim/config/DbConnect.php';
require 'vendor/mailer/Email.php';
require 'Slim/library/Common.php';

\Slim\Slim::registerAutoloader();

/**
 * Step 2: Instantiate a Slim application
 *
 * This example instantiates a Slim application using
 * its default settings. However, you will usually configure
 * your Slim application now by passing an associative array
 * of setting names and values into the application constructor.
 */
$app = new \Slim\Slim();

$userId = '';
$userType = '';

/* Function Name : login
 * Description : Registration for Requester and Transporter.
 * url: http://192.168.11.101/aq/services/authentication.php/login
 * Method : Post
 * Param : Email,Password,NotificationId,DeviceId,DeviceType,AppVersion
 * Created By : Ajay chaudhary
 * Create Date : 19-11-2015
 * */

$app->post('/login', function () use ($app) {
    $response = array('success' => 0, 'msg' => '');

    global $db;

    verifyRequiredParams(array('email', 'password', 'deviceType', 'appVersion'));
    $collection = $db->users;

    $info = $collection->find(array(
        'Email' => $app->request->post('email'),
        'Password' => md5($app->request->post('password')),
        'delete_status' => ['$ne' => 'yes'],
    ));
    //$info = $collection->find(array('Email' => $app->request->post('email')));
    if ($info->count() > 0) {
        $Status = '';
        $userInfo = $info->getNext();
        if (@$userInfo['RequesterStatus'] == 'active' or @$userInfo['TransporterStatus'] == 'active' or (@$userInfo['RequesterStatus'] == 'active' and @$userInfo['TransporterStatus'] == 'active')) {

            if ($userInfo['DeviceId'] != $app->request->post('deviceId')) {
                //Send push notification
                require_once 'vendor/Notification/Notification.php';
                $pushnoti = new Notification();
                $pushnoti->setValue('title', 'Logout');
                $pushnoti->setValue('message', 'You are trying to login from another device.');
                $pushnoti->setValue('location', 'login_other_place');
                $pushnoti->setValue('locationkey', '');

                $pushnoti->add_user($userInfo['NotificationId'], $userInfo['DeviceType']);
                $pushnoti->fire();
                // End send push notification
            }

            $response['success'] = 1;
            $response['msg'] = 'Login successful.';
            $response['result'] = get_login_data($userInfo);
            $updateData = array("NotificationId" => "", "UserDeviceId" => "");
            $collection->update(array('NotificationId' => $app->request->post('notificationId')), array('$set' => $updateData), array('multiple' => true));
            $updateData = array(
                "NotificationId" => $app->request->post('notificationId'),
                "DeviceId" => $app->request->post('deviceId'),
                "DeviceType" => strtolower($app->request->post('deviceType')),
                "AppVersion" => $app->request->post('appVersion'),
                "UpdateOn" => new MongoDate(),
            );
            $collection->update(array('_id' => $userInfo['_id']), array('$set' => $updateData));
            if (isset($userInfo['UpdateOn']->sec) and @$userInfo['UpdateOn']->sec > 1) {
                $response['mydate'] = date("Y-m-d H:i:s", @$userInfo['UpdateOn']->sec);
            }
        } else {
            $response = array('success' => 0, 'msg' => 'Your account is inactive, please contact support.');
        }
    } else {
        $response['msg'] = "Your email or password is invalid.";
    }

    echoRespnse(200, $response);
});

/* Function Name : update_notification_info
 * Description : Update notification information if udid problem occur.
 * url: http://192.168.11.101/aq/services/authentication.php/update_notification_info
 * Method : Post
 * Param : notificationId,deviceId
 * Created By : Ravi shukla
 * Create Date : 19-11-2015
 * */

$app->post('/update_notification_info', 'authenticate', function () use ($app) {

    verifyRequiredParams(array('notificationId', 'deviceId'));
    $response = array('success' => 0, 'msg' => 'Oops! Something went wrong.');

    global $db;
    global $userId;

    $collecttion = $db->users;

    $status = $collecttion->update(array('_id' => $userId),
        array('$set' => array("NotificationId" => $app->request->post('notificationId'), 'DeviceId' => $app->request->post('deviceId'))));
    if ($status['updatedExisting'] > 0) {
        $response['success'] = 1;
        $response['msg'] = "Record updated.";
    }

    echoRespnse(200, $response);
});

/* Function Name : registration
 * Description : Registration for Requester and Transporter.
 * url: http://192.168.11.101/aq/services/authentication.php/registration
 * Method : Post
 * Param : firstName,lastName,email,password,notificationId,deviceId,deviceType,appVersion,userType
 * Created By : Ravi shukla
 * Create Date : 19-11-2015
 * */

$app->post('/registration', function () use ($app) {

    $response = array('success' => 0, 'msg' => 'User already signed up with this device.');

    global $db;
    require 'Slim/library/Sequence.php';

    verifyRequiredParams(array('firstName', 'email', 'password', 'deviceType', 'userType', 'appVersion'));
    $collecttion = $db->users;
    $where = array('DeviceId' => $app->request->post('deviceId'));
    $alreadyExist = $collecttion->find($where, array('Email', 'DeviceId'));

    //if($alreadyExist->count() < 1)
    if (true) {
        $EmailAlreadyExist = $collecttion->find(array(
            'Email' => $app->request->post('email'),
            'delete_status' => ['$ne' => 'yes'],
        ), array('Email'));

        if ($EmailAlreadyExist->count() == 0) {
            require 'Slim/library/Token.php';

            $UniqueNo = $app->request->post('firstName')[0] . $app->request->post('lastName')[0];

            $insData = array(
                "Name" => ucfirst($app->request->post('firstName')) . " " . ucfirst($app->request->post('lastName')),
                "FirstName" => ucfirst($app->request->post('firstName')),
                "LastName" => ucfirst($app->request->post('lastName')),
                "Email" => strtolower(trim($app->request->post('email'))),
                "Password" => md5($app->request->post('password')),
                "ChatName" => "aquantuo" . getSequence('user'),
                "TransporterType" => "",
                "CountryCode" => "",
                "PhoneNo" => '',
                "AlternateCCode" => '',
                "AlternatePhoneNo" => '',
                "BusinessName" => "",
                "Age" => "",
                "id_type" => "",
                "SSN" => "",
                "UserType" => "requester",
                "Street1" => '',
                "Street2" => '',
                "Country" => '',
                "State" => '',
                "City" => '',
                "ZipCode" => '',
                "BankName" => '',
                "AccountHolderName" => '',
                "BankAccountNo" => '',
                "RoutingNo" => '',
                "StripeId" => '',
                "VatTaxNo" => '',
                "NotificationId" => $app->request->post('notificationId'),
                "DeviceId" => $app->request->post('deviceId'),
                "DeviceType" => strtolower($app->request->post('deviceType')),
                "AppVersion" => $app->request->post('appVersion'),
                "TransporterStatus" => "not_registred",
                "RequesterStatus" => "active",
                "Image" => "",
                "IDProof" => '',
                "LicenceId" => '',
                "TPTrackLocation" => "on",
                "EmailStatus" => "on",
                "NoficationStatus" => "on",
                "TPSetting" => "on",
                "SoundStatus" => "on",
                "VibrationStatus" => "on",
                "consolidate_item" => "on",
                "EnterOn" => new MongoDate(),
                "UpdateOn" => new MongoDate(),
                "RatingCount" => 0,
                "RatingByCount" => 0,
                "CurrentLocation" => array(0, 0),
                "DeliveryAreaCountry" => array(),
                "DeliveryAreaState" => array(),
                "DeliveryAreaCities" => array(),
                "ProfileStatus" => 'step-one',
                "StripeBankId" => "",
                "UniqueNo" => "",
                "bank_info" => [],
                "AqAddress" => "",
                "AqCity" => "",
                "AqState" => "",
                "AqCountry" => "",
                "AqZipcode" => "",
                'AqLatLong' => [],
                'delete_status'=>'no',
                "Default_Currency" => $app->request->post("currency"),
                'UniqueNo' => strtoupper($UniqueNo) . getSequence('user_unique'),
            );

            // Get aquantuo addres
            $setting = $db->setting;
            $supportemail = $setting->find(['_id' => new MongoId('563b0e31e4b03271a097e1ca')]);
            if ($supportemail->count() > 0) {
                $supportemail = (Object) $supportemail->getNext();
                $insData['AqAddress'] = $supportemail->AqAddress;
                $insData['AqLatLong'] = $supportemail->AqLatlong;
                $insData["AqCity"] = $supportemail->AqCity;
                $insData["AqState"] = $supportemail->AqState;
                $insData["AqCountry"] = $supportemail->AqCountry;
                $insData["AqZipcode"] = $supportemail->AqZipcode;
                $insData["Aqcc"] = $supportemail->Aqcc;
                $insData["Aqphone"] = $supportemail->Aqphone;
            }

            // Generate unique no.
            /*for($i =0;$i<=50;$i++) {
            $insData['UniqueNo'] = strtoupper(get_token(4));
            $rewardcount = $collecttion->find(['UniqueNo' => $insData['UniqueNo']],['_id']);
            if($rewardcount->count() <= 0) {
            $i = 5555;
            }
            }*/
            if ($app->request->post('userType') == 'transporter') {
                $insData['UserType'] = 'both';
                $insData["TransporterType"] = "individual";
                $insData["TransporterStatus"] = "not_verify";
            }
            $auth = $collecttion->insert($insData);
            if ($auth['ok'] == 1) {
                // message for user
                $ETemplate = array(
                    "to" => $app->request->post('email'),
                    "replace" => array(
                        "[USERNAME]" => @$insData['Name'],
                        //'[ADDRESS]' => @$insData['UniqueNo'].', '. @$insData['AqAddress'].', '.@$insData['AqCity'].' '.@$insData['AqState'].' '.@$insData['AqZipcode'].', '.@$insData['AqCountry'],

                        '[ADDRESS]' => @$insData['AqAddress'].', Unit # '.@$insData['UniqueNo'].', '.@$insData['AqCity'].' '.@$insData['AqState'].' '.@$insData['AqZipcode'].', '.@$insData['AqCountry'],
                    ),
                );
                send_mail('55d5a0be6734c4fb378b4567', $ETemplate);

                // End of user email

                // Admin email section
                $ETemplate = array(
                    "to" => 'ravi@idealtechnologys.com',
                    "replace" => array(
                        "[USERNAME]" => @$insData['Name'],
                        "[EMAIL]" => $app->request->post('email'),
                        "[USERTYPE]" => ($insData['UserType'] == 'both') ? 'Transporter' : 'Requester',
                    ),
                );
                send_mail('565fd3e3e4b076e7a2a176f5', $ETemplate);

                // end of admin email

                $response['success'] = 1;
                $response['msg'] = "Registered successfully.";
                $response['instruction'] = "We can write instruction for transporter registration here.";
                $response['result'] = get_login_data($insData);
                /*$response['result'] = array(
                'UserId' => (string)$insData['_id'],
                'UserFirstName'     => $insData['FirstName'],
                'UserLastName'         => $insData['LastName'],
                'UserType'             => $insData['UserType'],
                'UserImage'         => $insData['Image'],
                'RequesterStatus'    => $insData['RequesterStatus'],
                'TransporterStatus'    => $insData['TransporterStatus'],
                'TransporterType'    => $insData['TransporterType'],
                'UserChatName'      => $insData['ChatName'],
                "TPTrackLocation"    => "on",
                "EmailStatus"        => "on",
                "NoficationStatus"    => "on",
                "TPSetting"            => "on",
                "SoundStatus"        => "on",
                "VibrationStatus"    => "on",
                "ProfileStatus"        => $insData['ProfileStatus'],
                "AqAddress"            => $insData['AqAddress'],
                "AqLatLong"            => $insData['AqLatLong']
                );        */
                // Notification Section

                $insNotification = array(
                    'NotificationTitle' => "New User Registered.",
                    'NotificationMessage' => "{$insData['Name']} has registered.",
                    'NotificationType' => "user",
                    'NotificationReadStatus' => 0,
                    'Date' => new MongoDate(),
                    'GroupTo' => 'Admin',
                );
                $coll = $db->notification;
                $coll->insert($insNotification);

                // End notification section
            }

        } else {
            $response['msg'] = 'This email is already registered.';
        }

    }

    echoRespnse(200, $response);

});

/*
 * Function Name : complete_transporter_profile
 * Description   : update requster user information
 * Url           : http://192.168.11.101/aq/services/authentication.php/complete_transporter_profile
 * Method        : Post
 * Header        : Apikey,Usertype
 * Parameter     : phoneNo,alternatePhoneNo,businessName,ssn,street1,street2,zipCode,routingNo,image,userId,licenceId
 * Created By    : Ravi shukla
 * Create Date   : 19-11-2015
 *
 * */

$app->post('/complete_transporter_profile', 'authenticate', function () use ($app) {

    $response = array('success' => 0, 'msg' => 'Failed to update user profile.');
    global $db;
    global $userId;
    global $userType;
    $collection = $db->users;

    $updateData = array(
        "TransporterType" => (strtolower($app->request->post('transporterType')) == 'business') ? 'business' : 'individual',
        "TransporterStatus" => 'not_verify',
        "CountryCode" => $app->request->post('CountryCode'),
        "PhoneNo" => $app->request->post('phoneNo'),
        "AlternateCCode" => $app->request->post('AlternateCCode'),
        "AlternatePhoneNo" => $app->request->post('alternatePhoneNo'),
        "BusinessName" => $app->request->post('businessName'),
        "Street1" => $app->request->post('street1'),
        "Street2" => $app->request->post('street2'),
        "Country" => $app->request->post('country'),
        "State" => $app->request->post('state'),
        "City" => $app->request->post('city'),
        "ZipCode" => $app->request->post('zipCode'),
        "VatTaxNo" => $app->request->post('vatTaxNo'),
        "UserType" => 'both',
        "ProfileStatus" => "step-two",
        "Default_Currency" => $app->request->post("currency"),
    );

    $upload = array('image' => 'Image', 'businessId' => 'IDProof', 'licenceId' => 'LicenceId');

    foreach ($upload as $key => $val) {
        if (isset($_FILES[$key]['name']) and @$_FILES[$key]['name'] != "") {
            $exts = explode('.', $_FILES[$key]['name']);
            $ext = $exts[count($exts) - 1];
            $profileImage = $val . '_' . rand(2154, 45454) . time() . ".$ext";
            if (in_array($ext, array('gif', 'GIF', 'png', 'PNG', 'jpg', 'JPG', 'JPEG', 'jpeg'))) {
                $newpath = FILE_URL . "profile/{$profileImage}";
                if (move_uploaded_file($_FILES[$key]['tmp_name'], $newpath)) {
                    $updateData[$val] = "profile/{$profileImage}";

                }
            }
        }
    }

    $userUpdateInfo = $collection->findAndModify(array('_id' => $userId), array('$set' => $updateData));

    if (count($userUpdateInfo) > 0) {
        if (strtolower($app->request->post('transporterType')) == 'business') {
            $userUpdateInfo['FirstName'] = $userUpdateInfo['BusinessName'];
            $userUpdateInfo['LastName'] = '';
        }
        $response['success'] = 1;
        $response['msg'] = "User profile updated successfully";
        $response['result'] = get_login_data($userUpdateInfo);
        $response['result']['UserImage'] = ((isset($updateData)) ? @$updateData['Image'] : @$updateData['Image']);
        $response['result']['UserType'] = $updateData['UserType'];
        $response['result']['TransporterStatus'] = $updateData['TransporterStatus'];
        $response['result']['TransporterType'] = $updateData['TransporterType'];
        $response['result']['ProfileStatus'] = $updateData['ProfileStatus'];

    }
    echoRespnse(200, $response);
});

/*
 * Function Name : profile_step_third
 * Description   : This function updates transporter(individual & business) profile info.
 * Method        : POST
 * Header        : Apikey,Usertype
 * Parameter     : ssn,backgroundCheckReport.
 * Url           : http://192.168.11.101/aq/services/authentication.php/profile_step_third
 * Created By      : Pankaj Gawande
 * Create Date   : 1-12-2015
 *
 * */

$app->post('/profile_step_third', 'authenticate', function () use ($app) {
    global $db;
    global $userId;
    verifyRequiredParams(array('ssn', 'backgroundCheckReport'));
    $response = array('success' => 0, "msg" => "Records not Updated", "result" => array());
    $usersCollection = $db->users;
    $updateData = array(
        'SSN' => $app->request->post('ssn'),
        'id_type' => $app->request->post('id_type'),
        'backgroundCheckReport' => $app->request->post('backgroundCheckReport'),
        "ProfileStatus" => "step-third",
    );

    if (isset($_FILES['licenceId']['name']) and @$_FILES['licenceId']['name'] != "") {
        $exts = explode('.', $_FILES['licenceId']['name']);
        $ext = $exts[count($exts) - 1];
        $profileImage = rand(2154, 45454) . time() . ".$ext";
        if ($ext == 'gif' || $ext == 'GIF' || $ext == 'png' || $ext == 'PNG' || $ext == 'jpg' || $ext == 'JPG' || $ext == 'JPEG' || $ext == 'jpeg') {
            $newpath = FILE_URL . "profile/{$profileImage}";
            if (move_uploaded_file($_FILES['licenceId']['tmp_name'], $newpath)) {
                $updateData['LicenceId'] = "profile/{$profileImage}";
            }
        }
    }
    $usersInfo = $usersCollection->findAndModify(array('_id' => $userId), array('$set' => $updateData));
    if (count($usersInfo) > 0) {

        // Admin email section
        $ETemplate = array(
            "to" => 'admin@aquantuo.com',
            "attachment" => array(
                "idproof" => SITE_URL . 'upload/' . $usersInfo['IDProof'],
                "Licenceid" => SITE_URL . 'upload/' . $usersInfo['LicenceId'],
                "ProfileImage" => SITE_URL . 'upload/' . $usersInfo['Image'],
            ),
            "replace" => array(
                "[USERNAME]" => @$usersInfo['Name'],
                "[EMAIL]" => $usersInfo['Email'],
                "[PHONENO]" => $usersInfo['PhoneNo'],
                "[ALTERNATEPHONENO]" => $usersInfo['AlternatePhoneNo'],
                "[BUSINESSNAME]" => $usersInfo['BusinessName'],
                "[AGE]" => $usersInfo['Age'],
                "[STREET1]" => $usersInfo['Street1'],
                "[STREET2]" => $usersInfo['Street2'],
                "[COUNTRY]" => $usersInfo['Country'],
                "[STATE]" => $usersInfo['State'],
                "[CITY]" => $usersInfo['City'],
                "[ZIPCODE]" => $usersInfo['ZipCode'],
            ),
        );
        send_mail('56ab567c5509251cd67773f4', $ETemplate);

        $response = array('success' => 1, "msg" => "Records Updated");
        $response['result'] = get_login_data($usersInfo);
        $response['SSN'] = $updateData['SSN'];
        $response['backgroundCheckReport'] = $updateData['backgroundCheckReport'];

    }
    echoRespnse(200, $response);
});

/*
 * Function Name : requester_update
 * Description   : update requster user information
 * Url           : http://192.168.11.101/aq/services/authentication.php/complete_requester_profile
 * Method        : Post
 * Header        : Apikey,Usertype
 * Parameter     : phoneNo,alternatePhoneNo,street1,street2,country,state,city,zipCode,image,cardName,cardNumber,expMonth,expYear,cvv
cardNumber,cardName,expMonth,expYear,cvv,image
 * Created By    : Ravi shukla
 * Create Date   : 19-11-2015
 *
 * */

$app->post('/complete_requester_profile', 'authenticate', function () use ($app) {

    $response = array('success' => 0, 'msg' => 'Fail to update user profile.', 'carderror' => '');
    global $db;
    global $userId;
    global $userType;
    $collection = $db->users;

    $updateData = array(
        "CountryCode" => $app->request->post('CountryCode'),
        "PhoneNo" => $app->request->post('phoneNo'),
        "AlternateCCode" => $app->request->post('AlternateCCode'),
        "AlternatePhoneNo" => $app->request->post('alternatePhoneNo'),
        "Street1" => $app->request->post('street1'),
        "Street2" => $app->request->post('street2'),
        "Country" => $app->request->post('country'),
        "State" => $app->request->post('state'),
        "City" => $app->request->post('city'),
        "ZipCode" => $app->request->post('zipCode'),
        "Default_Currency" => $app->request->post("currency"),
    );

    if (isset($_FILES['image']['name']) and @$_FILES['image']['name'] != "") {
        $exts = explode('.', $_FILES['image']['name']);
        $ext = $exts[count($exts) - 1];
        $profileImage = rand(2154, 45454) . time() . ".$ext";
        if ($ext == 'gif' || $ext == 'GIF' || $ext == 'png' || $ext == 'PNG' || $ext == 'jpg' || $ext == 'JPG' || $ext == 'JPEG' || $ext == 'jpeg') {
            $newpath = FILE_URL . "profile/{$profileImage}";
            if (move_uploaded_file($_FILES['image']['tmp_name'], $newpath)) {
                $updateData['Image'] = "profile/{$profileImage}";
            }
        }
    }

    $userUpdateInfo = $collection->findAndModify(array('_id' => $userId), array('$set' => $updateData));

    if (count($userUpdateInfo) > 0) {
        $response['success'] = 1;
        $response['msg'] = "You have successfully registered on Aquantuo.";
        $response['result'] = get_login_data($userUpdateInfo);
        $response['result']['UserImage'] = (isset($updateData['Image'])) ? $updateData['Image'] : $userUpdateInfo['Image'];

        $response['thanks'] = "You have successfully registered on Aquantuo.";

    }
    echoRespnse(200, $response);
});

/* Function Name :  ForgetPassword
 * Description     : Forget Password
 * Method : Post
 * Param : Email
 * Url : 192.168.11.101/aq/services/authentication.php/forget_password
 * Created By : Ravi shukla
 * Create Date : 21-08-2015
 * */

$app->post('/forget_password', function () use ($app) {

    global $db;
    $response = array('success' => 0, 'msg' => '');
    verifyRequiredParams(array('email'));
    $collection = $db->users;
    $UserInfo = $collection->find(array('Email' => strtolower($app->request->post('email'))), array('Name'));
    if ($UserInfo->count() > 0) {
        $UserInfo = $UserInfo->getNext();
        $token = md5(rand(215799, 9999999));

        $collection->update(array('Email' => strtolower($app->request->post('email'))),
            array('$set' => array('Token' => $token, 'TokenCreateAt' => new MongoDate())));

        $link = SITE_URL . "page/reset_password/" . $UserInfo['_id'] . "/$token";
        $ETemplate = array(
            "to" => $app->request->post('email'),
            "replace" => array(
                "[USERNAME]" => $UserInfo['Name'],
                "[LINK]" => "<a href='$link'>$link</a>",
            ),
        );
        send_mail('55d5a0be6734c4fb378b4568', $ETemplate);

        $response['success'] = 1;
        $response['msg'] = "An email has been sent to your registered email address. Please check.";
    } else {
        $response['msg'] = "This email is not registered with us.";
    }
    echoRespnse(200, $response);
});

/* Function Name :  change_password
 * Description      : Change Password
 * Method        : Post
 * Param : UserId,OldPassword,NewPassword
 * Url : http://192.168.11.101/aq/services/authentication.php/change_password
 * Created By : Ravi shukla
 * Create Date : 22-08-2015
 * */
$app->post('/change_password', 'authenticate', function () use ($app) {

    global $db;
    global $userId;
    $response = array('success' => 0, 'msg' => 'Failed to change password.');
    verifyRequiredParams(array('oldPassword', 'newPassword'));
    $collection = $db->users;
    $UserInfo = $collection->find(array('_id' => $userId), array('Password'));
    if ($UserInfo->count() > 0) {
        $UserInfo = $UserInfo->getNext();
        if ($UserInfo['Password'] == md5($app->request->post('oldPassword'))) {
            if ($UserInfo['Password'] != md5($app->request->post('newPassword'))) {
                $collection->update(array('_id' => $userId), array('$set' => array('Password' => md5($app->request->post('newPassword')))));
                $response['success'] = 1;
                $response['msg'] = "Password changed successfully.";
            } else {
                $response['msg'] = "Your old password and new password should not be same.";

            }
        } else {
            $response['msg'] = "Your current password is incorrect.";
        }
    }
    echoRespnse(200, $response);
});

/*
 * Function Name : get_user_profile
 * Description   : This function returns users information.
 * Method        : GET
 * Header        : Apikey
 * Url           : http://192.168.11.101/aq/services/authentication.php/get_user_profile
 * Created By      : Pankaj Gawande
 * Create Date   : 26-11-2015
 *
 * */

$app->get('/get_user_profile', 'authenticate', function () use ($app) {
    global $db;
    global $userId;
    $response = array("success" => 0, "msg" => "Records Not found", "result" => array());
    $usersCollection = $db->users;
    $userInfo = $usersCollection->find(array('_id' => new MongoId($userId)));
    if ($userInfo->count() > 0) {
        $response = array("success" => 1, "msg" => "Records found");
        $userInfo = $userInfo->getNext();
        unset($response['result']['_id']);

        $response['result'] = array(
            'UserFirstName' => $userInfo['FirstName'],
            'UserLastName' => $userInfo['LastName'],
            'UserEmail' => $userInfo['Email'],
            'CountryCode' => @$userInfo['CountryCode'],
            'UserPhoneNo' => $userInfo['PhoneNo'],
            'AlternateCCode' => @$userInfo['AlternateCCode'],
            'AlternatePhoneNo' => $userInfo['AlternatePhoneNo'],
            'UserImage' => $userInfo['Image'],

            'UserSSN' => @$userInfo['SSN'],
            'UserStreet1' => @$userInfo['Street1'],
            'UserStreet2' => @$userInfo['Street2'],
            'UserCountry' => @$userInfo['Country'],
            'State' => @$userInfo['State'],
            'UserCity' => @$userInfo['City'],
            'UserZipCode' => @$userInfo['ZipCode'],
            'BusinessName' => @$userInfo['BusinessName'] ? @$userInfo['BusinessName'] : "",
            'BankName' => @$userInfo['BankName'],
            'AccountHolderName' => @$userInfo['AccountHolderName'],
            'BankAccountNo' => @$userInfo['BankAccountNo'],
            'RoutingNo' => @$userInfo['RoutingNo'],
            'VatTaxNo' => @$userInfo['VatTaxNo'],
            'LincenceId' => @$userInfo['LicenceId'],
            'BusinessId' => @$userInfo['IDProof'],
            'PaypalID' => @$userInfo['PaypalID'],
        );
        $response['result'] = array_merge($response['result'], get_login_data($userInfo));
    }
    echoRespnse(200, $response);
});

/*
 * Function Name : edit_user_profile
 * Description   : This function update users information(Requester & Transporter).
 * Method        : POST
 * Header        : Apikey
 * Parameter     : firstName,lastName,phoneNo,alternatePhoneNo,ssn,street1,street2,country,
state,city,zipCode
 * Url           : http://192.168.11.101/aq/services/authentication.php/edit_user_profile
 * Created By      : Pankaj Gawande
 * Create Date   : 26-11-2015
 *
 * */

$app->post('/edit_user_profile', 'authenticate', function () use ($app) {
    global $db;
    global $userId;
    $response = array("success" => 0, "msg" => "Records not Updated");
    $usersCollection = $db->users;
    $where = array('_id' => $userId);
    $userInfo = $usersCollection->find($where, array('Image', 'IDProof', 'LicenceId'));
    $userInfo = $userInfo->getNext();

    $updateData = array(
        'FirstName' => ucfirst($app->request->post('firstName')),
        'LastName' => ucfirst($app->request->post('lastName')),
        'Name' => ucfirst($app->request->post('firstName')) . " " . ucfirst($app->request->post('lastName')),
        'CountryCode' => $app->request->post('CountryCode'),
        'PhoneNo' => $app->request->post('phoneNo'),
        'AlternateCCode' => $app->request->post('AlternateCCode'),
        'AlternatePhoneNo' => $app->request->post('alternatePhoneNo'),
        'BusinessName' => $app->request->post('businessName'),
        'VatTaxNo' => $app->request->post('vatTaxNo'),
        'SSN' => $app->request->post('ssn'),
        'Street1' => $app->request->post('street1'),
        'Street2' => $app->request->post('street2'),
        'Country' => $app->request->post('country'),
        'State' => $app->request->post('state'),
        'City' => $app->request->post('city'),
        'ZipCode' => $app->request->post('zipCode'),
        'id_type' => $app->request->post('id_type'),
    );

    $upload = array('image' => 'Image', 'businessId' => 'IDProof', 'licenceId' => 'LicenceId');
    foreach ($upload as $key => $val) {
        if (isset($_FILES[$key]['name']) and @$_FILES[$key]['name'] != "") {
            $exts = explode('.', $_FILES[$key]['name']);
            $ext = $exts[count($exts) - 1];
            $profileImage = $val . rand(2154, 45454) . time() . ".$ext";
            if (in_array($ext, array('gif', 'GIF', 'png', 'PNG', 'jpg', 'JPG', 'JPEG', 'jpeg'))) {
                $newpath = FILE_URL . "profile/{$profileImage}";
                if (move_uploaded_file($_FILES[$key]['tmp_name'], $newpath)) {
                    $updateData[$val] = "profile/{$profileImage}";
 
                    if (isset($userInfo[$val])) {
                        if (!$userInfo[$val] == "") {
                            $oldimage = $userInfo[$val];
                            if (file_exists(FILE_URL . "/" . $oldimage)) {
                                unlink(FILE_URL . "/" . $oldimage);
                            }
                        }

                    }
                }
            }
        }
    }
    $userUpdateInfo = $usersCollection->findAndModify($where, array('$set' => $updateData));

    //$userUpdateInfo = $usersCollection->find($where);

    $info = $usersCollection->find($where);
    $info = $info->getNext();

    $response = array("success" => 1, "msg" => "Profile updated successfully");
    //print_r($info); die;
   // echo $updateData['Image'];
    $response['result'] = get_login_data($info);

    echoRespnse(200, $response);

}); //edit_user_profile

/*
 * Function Name : edit_bank_information
 * Description   : This function update users bank information.
 * Method        : POST
 * Header        : Apikey
 * Parameter     : bankName,accountHolderName,bankAccountNo,routingNo
 * Url           : http://192.168.11.101/aq/services/authentication.php/edit_bank_information
 * Created By      : Pankaj Gawande
 * Create Date   : 26-11-2015
 *
 * */

$app->post('/edit_bank_information', 'authenticate', function () use ($app) {
    global $db;
    global $userId;
    require_once ('vendor/stripe/init.php');

    $response = array("success" => 0, "msg" => "Oops! Something went wrong.");
    $usersCollection = $db->users;
    verifyRequiredParams(array('CurrencyCode', 'BankCountryCode', 'bankName', 'accountHolderName', 'bankAccountNo', 'routingNo'));
    $userinformation = $usersCollection->find(array('_id' => $userId));
    if ($userinformation->count() > 0) {
        $usersInfo = $userinformation->getNext();

        try
        {
            $updateData = array(
                "CountryName" => $app->request->post('CountryName'),
                "BankCountryCode" => $app->request->post('BankCountryCode'),
                "CurrencyCode" => $app->request->post('CurrencyCode'),
                "BankName" => $app->request->post('bankName'),
                "AccountHolderName" => $app->request->post('accountHolderName'),
                "BankAccountNo" => $app->request->post('bankAccountNo'),
                "RoutingNo" => $app->request->post('routingNo'),
                'ProfileStatus' => 'complete',
                "PaypalID" => $app->request->post('paypalid'),
            );

            $account = [
                "legal_entity" => [
                    'type' => 'individual',
                    'first_name' => $usersInfo['FirstName'],
                    'last_name' => $usersInfo['LastName'],
                    //'personal_id_number'    => $data->SSN
                ],
                "external_account" => [
                    'object' => 'bank_account',
                    'account_number' => trim($app->request->post('bankAccountNo')),
                    'routing_number' => trim($app->request->post('routingNo')),
                    'country' => $app->request->post('BankCountryCode'),
                    'currency' => $app->request->post('CurrencyCode'),
                    'AccountHolderName' => trim($app->request->post('accountHolderName')),
                ],
            ];

            if (empty(trim($usersInfo['StripeBankId']))) {
                $account["managed"] = true;
                $account["country"] = trim($app->request->post('BankCountryCode'));
                $account["tos_acceptance"] = [
                    "date" => time(),
                    "ip" => @$_SERVER['REMOTE_ADDR'],
                ];

                $StripeResponse = \Stripe\Account::create($account);
                if (isset($StripeResponse->id)) {
                    $updateData['StripeChargeId'] = $StripeResponse->id;
                } else {
                    $response['msg'] = 'Fail to create account.';
                    echoRespnse(200, $response);
                    die;
                }
            } else {
                $StripeResponse = \Stripe\Account::create($account);
            }

            $res = $usersCollection->update(array('_id' => $userId), array('$set' => $updateData));
            if ($res['updatedExisting'] > 0) {
                $response = array("success" => 1, "msg" => "Bank information updated successfully.");
                $usersInfo['ProfileStatus'] = $updateData['ProfileStatus'];
                $response['result'] = get_login_data($usersInfo);
            }

        } catch (exception $e) {
            $response['msg'] = $e->getMessage();
        }
    }

    echoRespnse(200, $response);

});

$app->post('/add_bank_information', 'authenticate', function () use ($app) {
    global $db;
    global $userId;
    $response = array("success" => 0, "msg" => "Oops! Something went wrong.");
    $usersCollection = $db->users;
    verifyRequiredParams(array('bankName', 'accountHolderName', 'bankAccountNo', 'routingNo'));
    $userinformation = $usersCollection->find(array('_id' => $userId));
    if ($userinformation->count() > 0) {

        $usersInfo = $userinformation->getNext();
        $updateData = array(
            "_id" => new MongoId(),
            "BankName" => $app->request->post('bankName'),
            "AccountHolderName" => $app->request->post('accountHolderName'),
            "BankAccountNo" => $app->request->post('bankAccountNo'),
            "RoutingNo" => $app->request->post('routingNo'),
            "PaypalID" => $app->request->post('paypalid'),
        );

        $res = $usersCollection->update(array('_id' => $userId), array('$push' => ['bank_info' => $updateData]));
        if ($res['updatedExisting'] > 0) {
            $response = array("success" => 1, "msg" => "Success! Bank has been added successfully.");
            $usersInfo['bank_info'][] = $updateData;
            $response['result'] = get_login_data($usersInfo);
        }
    }

    echoRespnse(200, $response);

});

//edit_user_profile

/* Function Name : registration
 * Description : Test Registration for Requester and Transporter.
 * url: http://192.168.11.101/aq/services/authentication.php/test_registration
 * Method : Post
 * Param : firstName,lastName,email,password,notificationId,deviceId,deviceType,appVersion,userType
 * Created By : Ravi shukla
 * Create Date : 19-11-2015
 * */

$app->post('/test_registration', function () use ($app) {

    $response = array('success' => 0, 'msg' => 'User already registered with this device.');

    global $db;
    require 'Slim/library/Sequence.php';

    verifyRequiredParams(array('firstName', 'email', 'password', 'deviceType', 'deviceId', 'userType', 'appVersion', 'notificationId'));
    $collecttion = $db->test_users;
    $where = array('DeviceId' => $app->request->post('deviceId'));
    $alreadyExist = $collecttion->find($where, array('Email', 'DeviceId'));

    //if($alreadyExist->count() < 1)
    if (true) {
        $EmailAlreadyExist = $collecttion->find(array('Email' => $app->request->post('email')), array('Email'));

        $insData = array(
            "Name" => ucfirst($app->request->post('firstName')) . " " . ucfirst($app->request->post('lastName')),
            "FirstName" => ucfirst($app->request->post('firstName')),
            "LastName" => ucfirst($app->request->post('lastName')),
            "Email" => $app->request->post('email'),
            "Password" => md5($app->request->post('password')),
            "ChatName" => "aquantuo" . getSequence('user'),
            "TransporterType" => "",
            "PhoneNo" => '',
            "AlternatePhoneNo" => '',
            "BusinessName" => "",
            "Age" => "",
            "SSN" => "",
            "UserType" => "requester",
            "Street1" => '',
            "Street2" => '',
            "Country" => '',
            "State" => '',
            "City" => '',
            "ZipCode" => '',
            "BankName" => '',
            "AccountHolderName" => '',
            "BankAccountNo" => '',
            "RoutingNo" => '',
            "StripeId" => '',
            "VatTaxNo" => '',
            "NotificationId" => $app->request->post('notificationId'),
            "DeviceId" => $app->request->post('deviceId'),
            "DeviceType" => strtolower($app->request->post('deviceType')),
            "AppVersion" => $app->request->post('appVersion'),
            "TransporterStatus" => "not_registred",
            "RequesterStatus" => "active",
            "Image" => "",
            "IDProof" => '',
            "LicenceId" => '',
            "UserId" => "",
            "TPTrackLocation" => "on",
            "EmailStatus" => "on",
            "NoficationStatus" => "on",
            "EnterOn" => new MongoDate(),
        );
        if ($app->request->post('userType') == 'transporter') {
            $insData['UserType'] = 'both';
            $insData["TransporterType"] = "individual";
            $insData["TransporterStatus"] = "active";
        }
        $auth = $collecttion->insert($insData);
        if ($auth['ok'] == 1) {
            // message for user
            $ETemplate = array(
                "to" => $app->request->post('email'),
                "replace" => array(
                    "[USERNAME]" => $insData['Name'],
                ),
            );
            send_mail('55d5a0be6734c4fb378b4567', $ETemplate);

            // End of user email

            // Admin email section
            $ETemplate = array(
                "to" => 'admin@aquantuo.com',
                "replace" => array(
                    "[USERNAME]" => $insData['Name'],
                    "[EMAIL]" => $app->request->post('email'),
                ),
            );
            send_mail('565fd3e3e4b076e7a2a176f5', $ETemplate);

            // end of admin email

            $response['success'] = 1;
            $response['msg'] = "Registered successfully.";
            $response['instruction'] = "We can write instruction for transporter registration here.";
            $response['result'] = array(
                'UserId' => (string) $insData['_id'],
                'UserFirstName' => $insData['FirstName'],
                'UserLastName' => $insData['LastName'],
                'UserType' => $insData['UserType'],
                'UserImage' => $insData['Image'],
                'RequesterStatus' => $insData['RequesterStatus'],
                'TransporterStatus' => $insData['TransporterStatus'],
                'TransporterType' => $insData['TransporterType'],
                'UserChatName' => $insData['ChatName'],
            );
            // Notification Section

            $insNotification = array(
                'NotificationTitle' => "New User Registered.",
                'NotificationMessage' => "{$insData['Name']} has registered.",
                'NotificationType' => 1,
                'NotificationReadStatus' => 0,
                'Date' => new MongoDate(),
                'GroupTo' => 'Admin',
            );
            $coll = $db->notification;
            $coll->insert($insNotification);

            // End notification section
        }
    }

    echoRespnse(200, $response);

});

/* Function Name : delete_account
 * Description      : It will delete users account.
 * url           : http://192.168.11.101/aq/services/authentication.php/delete_account
 * Method        : GET
 * Param         : NA
 * Header        : Apikey,Usertype
 * Created By    : Pankaj Gawande
 * Create Date   : 10-12-2015
 * */
$app->get('/delete_account', 'authenticate', function () use ($app) {
    global $db;
    global $userId;
    $response = array("success" => 0, "msg" => "Invalid user", "result" => array());
    $usersCollection = $db->users;
    $removed_usersCollection = $db->removed_users;

    $userInfo = $usersCollection->find(array('_id' => $userId));
    if ($userInfo->count() > 0) {
        $userInfo = $userInfo->getNext();
        $insertData = $removed_usersCollection->insert($userInfo);

        if (count($insertData) > 0) {
            $usersCollection->update(
                ['_id' => $userId],
                ['delete_status' => 'yes']
            );
            $deliveryRequest = $db->delivery_request;

            $deliveryReques->remove([
                'RequesterId' => $userId,
                'Status' => ['$in' => ['ready', 'pending']],
            ]);

            //$usersCollection->remove(array('_id' => $userId));
            $response = array("success" => 1, "msg" => "User has been deleted.");
        }
    }
    echoRespnse(200, $response);

});

/* Function Name : registration
 * Description : Test Registration for Requester and Transporter.
 * url: http://192.168.11.101/aq/services/authentication.php/remove_transporter_account
 * Method : Post
 * Created By : Ravi shukla
 * Create Date : 19-11-2015
 * */

$app->post('/manage_transporter_account', 'authenticate', function () use ($app) {

    global $db;
    global $userId;
    $response = array("success" => 0, "msg" => "Invalid user");
    $usersCollection = $db->users;
    $removed_usersCollection = $db->removed_users;
    $update = array();
    if ($app->request->post('operation') == 'deactivate') {
        $update['TPSetting'] = 'off';
        $response["msg"] = "Your transporter account is disabled.";
    } else {
        $update['TPSetting'] = 'on';
        $response["msg"] = "Your transporter account is enabled.";
    }
    $userInfo = $usersCollection->findAndModify(array('_id' => $userId), array('$set' => $update));

    if (count($userInfo) > 0) {
        $response["success"] = 1;
        $userInfo['TPSetting'] = $update['TPSetting'];
        $response['result'] = get_login_data($userInfo);
    } else {
        $response["msg"] = "Invalid user";
    }
    echoRespnse(200, $response);

});

function get_login_data($userInfo)
{
    global $db;

    $currencycoll = $db->city_state_country;
    $currencyobj = $currencycoll->find(['CurrencyCode' => $userInfo['Default_Currency']],
        ['Content', 'CurrencySymbol']);

    if (is_object($userInfo)) {
        $userInfo = $userInfo->getNext();
    }
    /*  if ($userInfo['TransporterType'] == 'business') {
    $userInfo['LastName'] = '';
    $userInfo['FirstName'] = $userInfo['BusinessName'];
    }*/
    $userInfo['ship_allow_on_user_address'] = false;
    // check shipping available on city or not
    $csc_coll = $db->city_state_country;
    $where = array('Status' => 'Active', 'Content' => @$userInfo['City']);
    $city = $csc_coll->find($where, array('Content'))->sort(array('Content' => 1));
    if ($city->count() > 0) {
        $userInfo['ship_allow_on_user_address'] = true;
    }
    // end check shipping available on city or not
    $Show_Currency_code = 'USA - $';
    if ($currencyobj->count() > 0) {
        $currencyobj = (Object) $currencyobj->getNext();
        $Show_Currency_code = $currencyobj->Content . ' - ' . $currencyobj->CurrencySymbol;
    }

    /* if ($userInfo['TransporterType'] == 'business') {
    $userInfo['LastName'] = '';
    $userInfo['FirstName'] = $userInfo['BusinessName'];
    }*/

    return array(
        'UserId' => (string) @$userInfo['_id'],
        'UserFirstName' => @$userInfo['FirstName'],
        'UserLastName' => $userInfo['LastName'],
        'UserType' => $userInfo['UserType'],
        'UserImage' => $userInfo['Image'],
        'RequesterStatus' => $userInfo['RequesterStatus'],
        'TransporterStatus' => $userInfo['TransporterStatus'],
        'TransporterType' => $userInfo['TransporterType'],
        'UserChatName' => @$userInfo['ChatName'],
        "TPTrackLocation" => @$userInfo['TPTrackLocation'],
        "EmailStatus" => @$userInfo['EmailStatus'],
        "NoficationStatus" => @$userInfo['NoficationStatus'],
        "TPSetting" => @$userInfo['TPSetting'],
        "SoundStatus" => @$userInfo['SoundStatus'],
        "VibrationStatus" => @$userInfo['VibrationStatus'],
        "DeliveryAreaCountry" => @$userInfo['DeliveryAreaCountry'],
        "DeliveryAreaCities" => @$userInfo['DeliveryAreaCities'],
        "ProfileStatus" => @$userInfo['ProfileStatus'],
        "NotificationId" => @$userInfo['NotificationId'],
        "RewardCode" => @$userInfo['RewardCode'],
        "UniqueNo" => isset($userInfo['UniqueNo']) ? $userInfo['UniqueNo'] : '',
        "AqAddress" => isset($userInfo['AqAddress']) ? $userInfo['AqAddress'] : '',
        "AqCity" => isset($userInfo['AqCity']) ? $userInfo['AqCity'] : '',
        "AqCountry" => isset($userInfo['AqCountry']) ? $userInfo['AqCountry'] : '',
        "AqZipcode" => isset($userInfo['AqZipcode']) ? $userInfo['AqZipcode'] : '',
        "AqState" => isset($userInfo['AqState']) ? $userInfo['AqState'] : '',
        "AqLat" => isset($userInfo['AqLatLong'][1]) ? $userInfo['AqLatLong'][1] : '',
        "AqLong" => isset($userInfo['AqLatLong'][0]) ? $userInfo['AqLatLong'][0] : '',
        "Street1" => isset($userInfo['Street1']) ? $userInfo['Street1'] : '',
        "Street2" => isset($userInfo['Street2']) ? $userInfo['Street2'] : '',
        "City" => isset($userInfo['City']) ? $userInfo['City'] : '',
        "State" => isset($userInfo['State']) ? $userInfo['State'] : '',
        "Country" => isset($userInfo['Country']) ? $userInfo['Country'] : '',
        "ZipCode" => isset($userInfo['ZipCode']) ? $userInfo['ZipCode'] : '',
        "ship_allow_on_user_address" => @$userInfo['ship_allow_on_user_address'],
        "Default_Currency" => get_currency($userInfo['Default_Currency']),
        "Currency_Code" => $userInfo['Default_Currency'],
        "Show_Currency_code" => $Show_Currency_code,
        "id_type" => isset($userInfo['id_type']) ? $userInfo['id_type'] : '',
        "CountryName" => @$userInfo['CountryName'],
        "BankCountryCode" => @$userInfo['BankCountryCode'],
        "CurrencyCode" => @$userInfo['CurrencyCode'],
        "consolidate_item" => @$userInfo['consolidate_item'],
        "CountryCode" => @$userInfo['CountryCode'],
        "PhoneNo" => @$userInfo['PhoneNo'],
        "Aqphone" => @$userInfo['Aqphone'] ? @$userInfo['Aqphone'] : "888 652 2233",
        "Aqcc" => @$userInfo['Aqcc'] ? @$userInfo['Aqcc'] : "1",

    );
}

function get_currency($currency_code)
{
    switch ($currency_code) {
        case 'GHS':
            return 'GH₵';
            break;

        default:
            return '$';
            break;
    }
}

/* Function Name : check_notification_id
 * Description : Check notification id
 * url: http://192.168.11.101/aq/services/authentication.php/check_notification_id
 * Method : Post
 * Created By : Ravi shukla
 * Create Date : 19-11-2015
 * */

$app->get('/check_notification_id', 'authenticate', function () use ($app) {
   // echo "string";die;

    global $db;
    global $userInformation;

    $response = array("success" => 0, "msg" => "Notification id not found");
    if (!empty($userInformation->NotificationId)) {
        $response = array("success" => 1, "msg" => "Notification id found");
    }

    echoRespnse(200, $response);

});

/* Function Name : update_curreny
 * Description : Check notification id
 * url: http://192.168.11.101/aq/services/authentication.php/update_curreny
 * Method : Post
 * Created By : Ravi shukla
 * Create Date : 19-11-2015
 * */

$app->post('/update_curreny', 'authenticate', function () use ($app) {

    global $db;
    global $userInformation;

    $usersobj = $db->users;
    $response = array("success" => 0, "msg" => "Notification id not found");

    $res = $usersobj->update(['_id' => $userInformation->_id], ['$set' => [
        "Default_Currency" => $app->request->post("currency"),
    ],
    ]);

    if ($res) {
        $response = array("success" => 1, "msg" => "Currency has been updated successfully.");
    }

    echoRespnse(200, $response);

});

/* Function Name : update_curreny
 * Description : Check notification id
 * url: http://192.168.11.101/aq/services/authentication.php/get_reward_code
 * Method : Post
 * Created By : Ravi shukla
 * Create Date : 19-11-2015
 * */

$app->get('/get_reward_code', 'authenticate', function () use ($app) {
    global $db;
    global $userInformation;

    $promocodeobj = $db->promocode;
    $response = array("success" => 0, "msg" => "No record found.");

    $promocode = $promocodeobj->find(['assign_promocode_users' => [
        '$elemMatch' => ['$eq' => (String) $userInformation->_id],
    ],
    ]);

    if ($promocode->count() > 0) {
        $promocode = (Object) $promocode->getNext();

        $response = ["success" => 1, "msg" => "Reward code", 'code' => $promocode->Code];
    }

    echoRespnse(200, $response);

});

$app->run();
