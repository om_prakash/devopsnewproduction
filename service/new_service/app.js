var express = require('express');
var app = express();
//var app = require('express')();



//var server = require('http').createServer();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var mongodb = require('mongodb');
var appDB = require('./config/appDB.js');
var ls = require('local-storage');
var moment = require('moment');
var mainJs = require('./config/chat.js');

var cert_and_key = require('fs').readFileSync('./config/pushcert.pem');

var notifier = require('node_apns').services.Notifier({ cert: cert_and_key, key: cert_and_key }, true /* development = true, production = false */);
var Notification = require('node_apns').Notification;

server.on('error', function(e) {
    //console.log("Express server listening on port %d", server.address())
    if (e.code === 'EADDRINUSE') {
        console.log('Failed to bind to port - address already in use ');
        process.exit(1);
    }
    new Errornotification(e);
});



server.listen(8080, function() {
    console.log('connection...');
    console.log("listen ....");
    server.timeout = 2000000;
    mainJs.init([io]);
    //util.log('socket.io server listening at %s : 8000');
});



app.get('/dd', function(req, res) {
console.log('ppp'); res.send('pp');
   /* try {
            //console.log("-------"+apnOption.notification_id);
            notifier.notify(Notification('2a7f57ecb3219f62ac6163c901c4849188f9c87cccc9836a0a98b55d5f78e501', 
                    { aps: 
                        { 
                            'alert': "Hello from service", 
                            'sound': "default" ,
                            'body': "Message from anil",
                            'type': "NOTIFY_TO_OFFLINE_USER",
                            'title':"Message from anil",
                            'message':"Testomg message...",
                            'from_id':"5893119ecf32077f70dcf8cc",
                        }
                    }
                ), 
                function (err) { 
                 if (!err) console.log("Sent", this); 
                 else console.log('Error', err, 'on', this);
                }
            ); 

        } catch (e) {
            console.log("iphone notification error-----", e);
    }*/
   
});




io.on('connection', function(socket){
    console.log('----------------------------77');
    socket.on('appTest', function (data,callBack) {
		console.log('ttttttttttttttt');
		console.log('user login data----', data);
    	/*var json = JSON.stringify({
            'message': "typing...",
            'to_id': "123",
            'from_id': "879879",
        });*/
        var json={
            'message': "typing...",
            'to_id': "123",
            'from_id': "879879",
        };
        callBack(json);
    	
  	});




    socket.on('login', function(data, callBack) {

        console.log('user login data----', data);
        var authData = JSON.parse(data);
        if (authData.hasOwnProperty("user_id")) {
            if (authData.user_id.length == 24) {
                var user_id = mongodb.ObjectID(authData.user_id);
                var login_id =authData.user_id;
                authData.user_id = mongodb.ObjectID(authData.user_id);
                var where = {
                    _id: mongodb.ObjectID(authData.user_id)
                };
                appDB.users.find(where).toArray(function(err, result) {
                        if (!err) {
                            if (result.length > 0) {
                                var uniquId = 'user_' + authData.user_id;
                                if (ls.get(uniquId) != null) {
                                    ls.remove(ls.get(uniquId).socket_ids);
                                }
                                ls.set(uniquId, {
                                    'socket_ids': socket.id,
                                    'user_id': authData.user_id,
                                    'name': result[0].name,
                                });
                                ls.set(socket.id, {
                                    'socket_ids': socket.id,
                                    'user_id': authData.user_id,
                                    'name': result[0].name,

                                });
                                socket.join(uniquId);


                                var current_time = moment.utc().valueOf();
                                console.log('current_time ', current_time);
                                /*DB.users.update({
                                    "_id": user_id
                                }, {
                                    $set: {
                                        "last_seen": moment.utc().format()
                                    }
                                });*/

                                var current_time = moment.utc().valueOf();
                                io.emit("changeUserOnlineStatus", {
                                    "user_id": authData.user_id,
                                    "online_status": "online",
                                    "presence_status": 'online',
                                    "msg": "Online",
                                    "last_seen": moment.utc().format()
                                }); //offline*/

                                mainJs.SendPendingMessage(socket,login_id);
                                
                                
                                callBack({
                                    "success": 1,
                                    "msg": "login successfully.",
                                    "result": result,
                                    
                                });
                                console.log('login successfully.--'+result[0].FirstName);
                            } else {
                                console.log('No record found');
                                callBack({
                                    "success": 0,
                                    "msg": "No record found."
                                });
                            }


                        } else {
                            console.log('Something went wrong');
                            callBack({
                                "success": 0,
                                "msg": "Something went wrong.",
                                "error": err
                            });
                        }

                });
                //end

            } else {
                callBack({
                    "success": 0,
                    "msg": "Invalid user id length."
                });
            }

        } else {
            console.log('User not found.');
            callBack({
                "success": 0,
                "msg": "User not found."
            });
        }
    }); //login

    //start typing
    socket.on('StartPrivateTyping', function(data,callBack) {
        var authData = JSON.parse(data);
        console.log('1start private typing.....',authData.to_id);
        console.log('2start private typing.....',authData.from_id);
        
        if (authData.hasOwnProperty("from_id") === true) {
            var uniqueId = 'user_' + authData.to_id;
            if (ls.get(uniqueId) != null) {
                   
                    var json = JSON.stringify({
                        'message': "typing...",
                        'to_id': authData.to_id,
                        'from_id': authData.from_id
                    });

                    if (ls.get(uniqueId).socket_ids != undefined) {
                         console.log('start private typing emit from server.....');
                        io.sockets.in(ls.get(uniqueId).socket_ids).emit('StartPrivateTyping',
                            json);
                        if(authData.hasOwnProperty("type") && authData.type == 'web'){
                            callBack(json);
                        }
                        

                    }
            }
        }
    });
    //end typing

    //stop
    socket.on('StopPrivateTyping', function(data, callback) {
        console.log('stop typing');
        var authData = JSON.parse(data);
        var uniqueId = 'user_' + authData.to_id;
         if (ls.get(uniqueId) != null) {
            
            var json = JSON.stringify({
                'message': "Online",
                'to_id': authData.to_id,
                'from_id': authData.from_id
            });
            
            if (ls.get(uniqueId).socket_ids != undefined) {
                io.sockets.in(ls.get(uniqueId).socket_ids).emit('StopPrivateTyping',json);
            }
        }
    });
    //end


    //Private massage
    socket.on('PrivateMessage', function(data,callBack) {
        var data = JSON.parse(data);
        var message_data = {
            from_id: data.from_id,
            from_name: data.from_name,
            to_id: data.to_id,
            to_name: data.to_name,
            message: data.message,
            status:"", 
            date_time: moment.utc().format(),
            created_at: new Date(),
            'msg_type':1,
            
        };

        var ins_data = {
            from_id: data.from_id,
            from_name: data.from_name,
            to_id: data.to_id,
            to_name: data.to_name,
            message: data.message,
            status:0, 
            date_time: moment.utc().format(),
            created_at: new Date(),
        };

        var socket_user_id = 'user_' + data.to_id;
       
        if (ls.get(socket_user_id) === null) { //user is offline
            var online_status = 0;
            ins_data.status = 0;
            appDB.ChatHistory.insert(ins_data, function(err, records) {
                if(err){
                    console.log(err);
                }
            });

            var apnOption ={
                "from_name":data.from_name,
                "to_id":data.to_id,
                'msg':data.message,
                'from_id': data.from_id,
            };

            mainJs.Notification(apnOption);
            //mainJs.InsertNotification(apnOption);
            
            callBack({
                "success": 1,
                "msg": "message data.",
                "result": message_data,
            });
        } else {
            ins_data.status = 1;
            appDB.ChatHistory.insert(ins_data, function(err, records) {
                if(err){
                    console.log(err);
                }
            });
            
            if(ls.get(socket_user_id).socket_ids != undefined){
                console.log("online msg send");
                io.sockets.in(ls.get(socket_user_id).socket_ids).emit('PrivateMessage', message_data);
                console.log('here-------------1');
                callBack({
                    "success": 1,
                    "msg": "login successfully.",
                    "result": message_data,
                                        
                });
            }
        }

    });
    //end private message

    //chat history
    socket.on('ChatHistory', function(data,callBack) {
        console.log('chat history---');
        var data = JSON.parse(data);
        var message_data = {
            from_id: data.from_id,
            to_id: data.to_id,
        };
        console.log(' to_id chat history---'+data.to_id);
        console.log(' from_id chat history---'+data.from_id);
        var socket_user_id = 'user_' + data.from_id;
        var online_status="Offline";
        

        if (ls.get(socket_user_id) != null) {
            online_status="Online";
            console.log("user online");
        }


        var where={};
        where.$or=[];
        where.$or.push({
                from_id: data.from_id,
                to_id: data.to_id,   
            },
            {
                from_id: data.to_id,
                to_id: data.from_id, 
            }
        );


        appDB.ChatHistory.find(where).toArray(function(err, result) {
            if (!err) {
                console.log('history  found..'+result.length);
                if (result.length > 0) {

                    callBack({
                        'status':1,
                        'result':result,
                        'online_status':online_status,
                    });

                }else{
                    callBack({
                        'status':0,
                        'result':[],
                        'online_status':online_status,
                    });
                    console.log('history not found..');
                }
                
            }
        });


    });
    //end history

    socket.on('GetUserInfo', function(data,callBack) {
        var data = JSON.parse(data);
        console.log('get user info----'+data.to_id);
        var where = {
            _id:mongodb.ObjectID(data.to_id)
        };

        var uniquId = 'user_' +data.to_id;
        var online_status ="offline";

        if (ls.get(uniquId) != null) {
            online_status ="online";
        }

        console.log("----------------------"+online_status);
        appDB.users.find(where).toArray(function(err, result) {
            if(!err){
                if(result.length > 0){
                	var res ={
                		"_id":result[0]._id,
                		"full_name":result[0].Name,
                		"user_image":result[0].Image,
                		"online_status":online_status,
                        "ImageUrl":'http://34.231.75.89/upload/',

                	};
                    callBack({
                        "success": 1,
                        "online_status":online_status,
                        "result": res,
                                    
                    });
                }else{
                    callBack({
                        "success": 0,
                        "msg": "No record found."
                    });
                }
            }else{
                console.log(err);
                callBack({
                        "success": 0,
                        "msg": "No record found."
                });
            }

        });
    });
    
    //disconnect for app user
    socket.on('disconnect', function() {

        console.log('disconnect calling---------');
        
        var store_data = ls.get(socket.id);

        if(store_data != null){
             if (store_data.user_id != undefined) {
                io.emit("offline_status",store_data.user_id);
                console.log('disconnected user---' + (store_data.user_id));
                var user_id = store_data.user_id;
                var uniquId = 'user_' + user_id;

               
                
                console.log("========" + user_id);
                //var dt = dateTime.create();
                //var formatted = dt.format('Y-m-d H:M');

                

                ls.remove(store_data.user_id);
                ls.remove(socket.id);
                ls.remove(uniquId);

                //update users last seen time

                //offline
                
                var current_time = moment.utc().valueOf(); //mainJs.getCurrentTime();
                io.emit("changeUserOnlineStatus",{
                    "user_id": user_id,
                    "online_status": "offline",
                    "msg": "Offline",
                    "last_seen": current_time
                });//offline
                return 1;
            } else {
               return 1;
            }

        }else{
            console.log("here3-----------------");
        }


        
    }); //disconnect


    //disconnect for website user
    socket.on('socket_disconnect', function() {

        console.log('disconnect calling---------');
        
        var store_data = ls.get(socket.id);
        if(store_data === null){
            console.log("here3-----------------");
        }else{



            if (store_data.user_id != undefined) {
                io.emit("offline_status",store_data.user_id);
                console.log('disconnected user---' + (store_data.user_id));
                var user_id = store_data.user_id;
                var uniquId = 'user_' + user_id;

               
                
                console.log("========" + user_id);
                //var dt = dateTime.create();
                //var formatted = dt.format('Y-m-d H:M');

                

                ls.remove(store_data.user_id);
                ls.remove(socket.id);
                ls.remove(uniquId);

                //update users last seen time

                //offline
                
                var current_time = moment.utc().valueOf(); //mainJs.getCurrentTime();
                io.emit("changeUserOnlineStatus",{
                    "user_id": user_id,
                    "online_status": "offline",
                    "msg": "Offline",
                    "last_seen": current_time
                });//offline
                return 1;
            } else {
               return 1;
            }
        } 
    }); 
    //end disconnect
    //socket.on('disconnect', function(){});
});







app.get('/nearby_transporter/:reqObj',function(req, res) {

    var str = JSON.parse(req.params.reqObj);

    if (str.hasOwnProperty('lat') == true && str.hasOwnProperty('long') == true && str.hasOwnProperty('userId') == true &&
        (str.userId).length == 24) {
        var maxDistance = 100000000;
        str.lat = parseFloat(str.lat);
        str.long = parseFloat(str.long);
        str.userId = mongodb.ObjectID(str.userId);
        var available_tr = [];

        appDB.TRIPS.distinct('TransporterId',
                {'SourceDate': {
                    '$gt': new Date(),
                },
            }
            ,function(err, items) {
                if(!err){
                    for (value in items) {
                        console.log("===yyyyy==="+items[value]);
                        available_tr.push(new ObjectID(items[value]));
                        console.log("====arrrrrr=="+available_tr);
                    }

            appDB.users.find({
            "_id": {
                '$ne': str.userId,
                '$in': available_tr,
            },
            "TransporterStatus": "active",
            "CurrentLocation.0": {
                "$nin": [0, '']
            },
            "CurrentLocation": {
                "$near": {
                    "$geometry": {
                        "type": "Point",
                        "coordinates": [str.long, str.lat]
                    },
                    "$maxDistance": maxDistance,
                    "$uniqueDocs": 1
                }
            }
        }, {
            TransporterType: 1,
            DeliveryArea: 1,
            Image: 1,
            FirstName: 1,
            RatingCount: 1,
            RatingByCount: 1,
            CurrentLocation: 1
        }).toArray(function(err, docs) {
            if (!err & docs.length > 0) {
                var result = [];

                for (var i in docs) {
                    result.push({
                        "Lat": docs[i].CurrentLocation[1],
                        "Lng": docs[i].CurrentLocation[0],
                        "UserId": docs[i]._id,
                        "UserImage": docs[i].Image,
                        "TransporterType": docs[i].TransporterType,
                        "Name": docs[i].FirstName,
                        "Rating": ((docs[i].RatingCount * 20) / docs[i].RatingByCount)
                    });

                }

                res.send({
                    "success": 1,
                    "msg": "Transporter List",
                    "result": result
                });

            } else {
                res.send({
                    "success": 0,
                    "msg": "No Any Transporter Near You"
                });
            }

        });

                }
                    
        });
        
        
    } else {
        res.send({
            "success": 0,
            "msg": "Invalid input"
        });
    }

});


app.get('/new_request_for_transporter/:reqObj', function(req, res) {
     console.log('--------------new_request_for_transporter');
    var str = JSON.parse(req.params.reqObj);

    if (str.hasOwnProperty('lat') == true && str.hasOwnProperty('long') == true && str.hasOwnProperty('pageno') == true &&
        str.hasOwnProperty('userId') == true && (str.userId).length == 24) {
        var maxDistance = 100000000;
        var limit = 20;
        var start = ((parseInt(str.pageno) - 1) * limit);
        str.lat = parseFloat(str.lat);
        str.long = parseFloat(str.long);
        str.userId = mongodb.ObjectID(str.userId);

        appDB.DELIVERY_REQUEST.find({
            "RequesterId": {
                '$ne': str.userId
            },
            "Status": "ready",
            "PickupLatLong": {
                "$near": {
                    "$geometry": {
                        "type": "Point",
                        "coordinates": [str.long, str.lat]
                    },
                    "$maxDistance": maxDistance,
                    "$uniqueDocs": 1
                }
            }
        }, {
            TransporterType: 1,
            RequesterName: 1,
            ProductTitle: 1,
            PickupCity: 1,
            DeliveryCity: 1,
            'EnterOn': 1,
            "ProductImage": 1
        }).skip(start).limit(limit).toArray(function(err, docs) {
            if (!err & docs.length > 0) {
                var result = [];

                for (var i in docs) {
                    result.push({
                        "Requestid": String(docs[i]._id),
                        "ProductTitle": docs[i].ProductTitle,
                        "RequestLocation": docs[i].PickupCity + " to " + docs[i].DeliveryCity,
                        "ProductImage": docs[i].ProductImage,
                        "TransporterType": docs[i].TransporterType,
                        "PickupLat": docs[i].PickupLatLong[1],
                        "PickupLong": docs[i].PickupLatLong[0],
                    });

                }

                res.send({
                    "success": 1,
                    "msg": "No package found for delivery",
                    "result": result
                });

            } else {
                res.send({
                    "success": 0,
                    "msg": "No package found for delivery"
                });
            }

        });

    } else {
        res.send({
            "success": 0,
            "msg": "Invalid input"
        });
    }

});


app.get('/new_request_for_transporter_map/:reqObj', function(req, res) {
    console.log('--------------new_request_for_transporter_map');
    var str = JSON.parse(req.params.reqObj);

    if (str.hasOwnProperty('lat') == true && str.hasOwnProperty('long') == true && str.hasOwnProperty('userId') == true &&
        (str.userId).length == 24) {
        str.lat = parseFloat(str.lat);
        str.long = parseFloat(str.long);
        str.userId = mongodb.ObjectID(str.userId);
        appDB.CONFIGURATION.find({
            '_id': mongodb.ObjectID('5673e33e6734c4f874685c84')
        }, {
            CoverageArea: 1
        }).toArray(function(err, configres) {

            if (!err && configres.length > 0) {
                var maxDistance = parseFloat(configres[0].CoverageArea);
                appDB.DELIVERY_REQUEST.find({
                    "RequesterId": {
                        '$ne': str.userId
                    },
                    "Status": "ready",
                    "TransporterId": "",
                    "PickupLatLong.0": {
                        "$nin": [0, '']
                    },
                    "RequestType": {
                        '$in': [null, 'delivery']
                    },
                    "PickupLatLong": {
                        "$near": {
                            "$geometry": {
                                "type": "Point",
                                "coordinates": [str.long, str.lat]
                            },
                            "$maxDistance": maxDistance,
                            "$uniqueDocs": 1
                        }
                    }
                }, {
                    DeliveryFullAddress: 1,
                    PickupFullAddress: 1,
                    DeliveryLatLong: 1,
                    PackageId: 1,
                    PickupDate: 1,
                    'ProductTitle': 1,
                    'PickupCity': 1,
                    'DeliveryCity': 1,
                    'EnterOn': 1,
                    "ProductImage": 1,
                    "PickupLatLong": 1,
                    TransporterName: 1,
                    Status: 1,
                    TotalCost: 1
                }).sort({
                    _id: -1
                }).toArray(function(err, docs) {
                    if (!err & docs.length > 0) {
                        var result = [];

                        for (var i in docs) {
                            result.push({
                                "Requestid": String(docs[i]._id),
                                "PackageNumber": docs[i].PackageId,
                               // "RequesterName": (docs[i].TransporterName).ucfirst(),
                                "RequesterName": docs[i].TransporterName,
                                //"ProductTitle": (docs[i].ProductTitle).ucfirst(),
                                "ProductTitle": docs[i].ProductTitle,
                                "Date": (docs[i].PickupDate / 1000),
                                //"RequestLocation": (docs[i].PickupCity).ucfirst() + " to " + (docs[i].DeliveryCity).ucfirst(),
                                "RequestLocation": docs[i].PickupCity+ " to " + docs[i].DeliveryCity,
                                "PickupLat": docs[i].PickupLatLong[1],
                                "PickupLong": docs[i].PickupLatLong[0],
                                "DeliveryLat": docs[i].DeliveryLatLong[1],
                                "DeliveryLong": docs[i].DeliveryLatLong[0],
                                "ProductImage": docs[i].ProductImage,
                                "Amount": docs[i].TotalCost,
                                "Status": docs[i].Status,
                                "StatusTitle": get_status_title(docs[i].Status),
                                "PickupAddress": docs[i].PickupFullAddress,
                                "DeliveryAddress": docs[i].DeliveryFullAddress,
                            });
                        }

                        res.send({
                            "success": 1,
                            "msg": "New delivery request",
                            "result": result
                        });

                    } else {
                        res.send({
                            "success": 0,
                            "msg": "No package found for delivery"
                        });
                    }

                });

            } else {
                res.send({
                    "success": 0,
                    "msg": "No package found for delivery"
                });
            }

        });

    } else {
        res.send({
            "success": 0,
            "msg": "Invalid input"
        });
    }

});

function get_status_title(status) {
    switch (status) {
        case 'ready':
            return 'Ready';
            break;
        case 'trip_pending':
            return 'Pending';
            break;
        case 'trip_ready':
            return 'Ready';
            break;
        case 'out_for_delivery':
            return 'On Delivery';
            break;
        case 'out_for_pickup':
            return 'Out for Pickup';
            break;
        case 'delivered':
            return 'Delivered';
            break;
        case 'cancel':
            return 'Cancel';
            break;
        default:
            return 'Pending';

    }
}



