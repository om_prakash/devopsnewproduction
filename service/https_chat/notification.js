"use strict";
var gcm = require('node-gcm');
//iphone
var cert_and_key = require('fs').readFileSync('./config/pushcert.pem');
//var cert_and_key = require('fs').readFileSync('./config/Aquantuo_Production.pem');
var notifier = require('node_apns').services.Notifier({ cert: cert_and_key, key: cert_and_key }, true /* development = true, production = false */);
var Notification1 = require('node_apns').Notification;



var Notification = function() {}
Notification.prototype.sendFcmNotification = function(apnOption, deviceType) {

        var serverKey = 'AIzaSyDLfrFFYjApPUkSkup2YNoAOwLLwa3ILgg';
        try {
            var sender = new gcm.Sender(serverKey);
            if(deviceType == 'iphone'){
                var message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
                    priority: 'high',
                    to: apnOption.notification_id,
                    collapse_key: apnOption.type,

                    notification: {
                        body: apnOption.title,
                        type: apnOption.type,
                        message:apnOption.chat_msg,
                    },
                    
                };


            }else{

                var message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
                    priority: 'high',
                    to: apnOption.notification_id,
                    collapse_key: apnOption.type,
                    data: { //you can send only notification or only data(or include both)
                        title: apnOption.title,
                        msg: apnOption.chat_msg,
                        type: apnOption.type,
                        location: apnOption.type,
                        locationkey:apnOption.from_id
                        
                    }
                };
            }

            var message = new gcm.Message(message);
            var regTokens = [apnOption.notification_id];
            sender.send(message, { registrationTokens: regTokens }, function (err,response) {
                if (err) console.error(err);
                else console.log(response);
            });

        } catch (e) {
            console.log("iphone notification error-----", e);
        }

} //sendApnNotification


Notification.prototype.IphoneNotification = function(apnOption, deviceType) {
    try {
            //console.log("-------"+apnOption.notification_id);
            notifier.notify(Notification1(apnOption.notification_id, 
                    { aps: 
                        { 
                            alert: apnOption.title, 
                            sound: "default",
                            body: apnOption.title,
                            type: "NOTIFY_TO_OFFLINE_USER",
                            'title':apnOption.title,
                            'message':apnOption.chat_msg,
                            'from_id':apnOption.from_id,
                            "content-available" : 1,
                        }
                    }
                ), 
                function (err) { 
                 if (!err) console.log("Sent", this); 
                 else console.log('Error', err, 'on', this);
                }
            ); 

        } catch (e) {
            console.log("iphone notification error-----", e);
    }
}


module.exports = Notification;


/*if (deviceType == 'iphone') {
                var message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
                    priority: 'high',
                    to: apnOption.device,
                    collapse_key: apnOption.userMsgType,
                    

                    notification: {
                        title:apnOption.title,
                        body: apnOption.title,
                        type: apnOption.type,
                        msg_from: "",
                        message:rating_msg,
                    },
                    
                };

            } else {
                var message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
                    priority: 'high',
                    to: apnOption.device,
                    collapse_key: apnOption.userMsgType,
                    data: { //you can send only notification or only data(or include both)
                        title: apnOption.title,
                        msg: apnOption.userMsg,
                        type: apnOption.type
                    }
                };
            }
            
            fcm.send(message, function(err, response) {
                console.log(err);
                if (err) {
                    console.log("Something has gone wrong!", err);
                } else {
                    console.log("Successfully sent with response: ", response);
                }
            });*/