var app = require("express")();
var fs = require('fs'); 
var https  = require('https');
var mongodb = require('mongodb');
var appDB = require('./config/appDB.js');
var ls = require('local-storage');
var moment = require('moment');
var mainJs = require('./config/chat.js');

var server = https.createServer({ 
    key: fs.readFileSync('./privkey.pem'),
    cert: fs.readFileSync('./cert.pem') 
},app);

var io = require('socket.io').listen(server);


app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    res.header("Access-Control-Allow-Headers", "Content-Type");
    res.header("Access-Control-Allow-Methods", "PUT, GET, POST, DELETE, OPTIONS");
    next();
});


server.listen(8080, function() {
    console.log('listening on *:8080');
    mainJs.init([io]);
});

app.get('/dev',function(req, res) {
    console.log('okk53454354354');
    res.send('klk');
    //res.writeHead(200, {'Content-Type': 'text/plain'});
  //res.write('Hello World!');
  //res.end();
});

io.on('connection', function(socket) {
    console.log('------------------------------------------');
    socket.on("error", function(err) {
        console.log("Caught flash policy server socket error: ");
        console.log(err.stack);
    });

    socket.on('login', function(data, callBack) {
        console.log('user login data----', data);
        var authData = JSON.parse(data);
        if (authData.hasOwnProperty("user_id")) {
            if (authData.user_id.length == 24) {
                var user_id = mongodb.ObjectID(authData.user_id);
                var login_id =authData.user_id;
                authData.user_id = mongodb.ObjectID(authData.user_id);
                var where = {
                    _id: mongodb.ObjectID(authData.user_id)
                };
                appDB.users.find(where).toArray(function(err, result) {
                    if (!err) {
                        if (result.length > 0) {
                            var uniquId = 'user_' + authData.user_id;
                            if (ls.get(uniquId) != null) {
                                ls.remove(ls.get(uniquId).socket_ids);
                            }
                            ls.set(uniquId, {
                                'socket_ids': socket.id,
                                'user_id': authData.user_id,
                                'name': result[0].name,
                            });
                            ls.set(socket.id, {
                                'socket_ids': socket.id,
                                'user_id': authData.user_id,
                                'name': result[0].name,

                            });
                            socket.join(uniquId);


                            var current_time = moment.utc().valueOf();
                            console.log('current_time ', current_time);
                            /*DB.users.update({
                                "_id": user_id
                            }, {
                                $set: {
                                    "last_seen": moment.utc().format()
                                }
                            });*/

                            var current_time = moment.utc().valueOf();
                            io.emit("changeUserOnlineStatus", {
                                "user_id": authData.user_id,
                                "online_status": "online",
                                "presence_status": 'online',
                                "msg": "Online",
                                "last_seen": moment.utc().format()
                            }); //offline*/

                            mainJs.SendPendingMessage(socket,login_id);
                            
                            
                            callBack({
                                "success": 1,
                                "msg": "login successfully.",
                                "result": result,
                                
                            });
                            console.log('login successfully.--'+result[0].FirstName);
                        } else {
                            console.log('No record found');
                            callBack({
                                "success": 0,
                                "msg": "No record found."
                            });
                        }


                    } else {
                        console.log('Something went wrong');
                        callBack({
                            "success": 0,
                            "msg": "Something went wrong.",
                            "error": err
                        });
                    }

                });
                //end

            } else {
                callBack({
                    "success": 0,
                    "msg": "Invalid user id length."
                });
            }

        } else {
            console.log('User not found.');
            callBack({
                "success": 0,
                "msg": "User not found."
            });
        }
    }); //login

    //chat history
    socket.on('ChatHistory', function(data,callBack) {
        console.log('chat history---');
        var data = JSON.parse(data);
        var message_data = {
            from_id: data.from_id,
            to_id: data.to_id,
        };
        console.log(' to_id chat history---'+data.to_id);
        console.log(' from_id chat history---'+data.from_id);
        var socket_user_id = 'user_' + data.from_id;
        var online_status="Offline";
        

        if (ls.get(socket_user_id) != null) {
            online_status="Online";
            console.log("user online");
        }


        var where={};
        where.$or=[];
        where.$or.push({
                from_id: data.from_id,
                to_id: data.to_id,   
            },
            {
                from_id: data.to_id,
                to_id: data.from_id, 
            }
        );


        appDB.ChatHistory.find(where).toArray(function(err, result) {
            if (!err) {
                console.log('history  found..'+result.length);
                if (result.length > 0) {

                    callBack({
                        'status':1,
                        'result':result,
                        'online_status':online_status,
                    });

                }else{
                    callBack({
                        'status':0,
                        'result':[],
                        'online_status':online_status,
                    });
                    console.log('history not found..');
                }
                
            }
        });
    });
    //end history

    //start typing
    socket.on('StartPrivateTyping', function(data,callBack) {
        var authData = JSON.parse(data);
        //console.log('1start private typing.....',authData.to_id);
        //console.log('2start private typing.....',authData.from_id);
        
        if (authData.hasOwnProperty("from_id") === true) {
            var uniqueId = 'user_' + authData.to_id;
            if (ls.get(uniqueId) != null) {
                   
                    var json = JSON.stringify({
                        'message': "typing...",
                        'to_id': authData.to_id,
                        'from_id': authData.from_id
                    });

                    if (ls.get(uniqueId).socket_ids != undefined) {
                         //console.log('start private typing emit from server.....');
                        io.sockets.in(ls.get(uniqueId).socket_ids).emit('StartPrivateTyping',
                            json);
                        if(authData.hasOwnProperty("type") && authData.type == 'web'){
                            callBack(json);
                        }
                        

                    }
            }
        }
    });
    //end typing

    //stop
    socket.on('StopPrivateTyping', function(data, callback) {
       // console.log('stop typing');
        var authData = JSON.parse(data);
        var uniqueId = 'user_' + authData.to_id;
         if (ls.get(uniqueId) != null) {
            
            var json = JSON.stringify({
                'message': "Online",
                'to_id': authData.to_id,
                'from_id': authData.from_id
            });
            
            if (ls.get(uniqueId).socket_ids != undefined) {
                io.sockets.in(ls.get(uniqueId).socket_ids).emit('StopPrivateTyping',json);
            }
        }
    });
    //end

    //Private massage
    socket.on('PrivateMessage', function(data,callBack) {
        var data = JSON.parse(data);
        var message_data = {
            from_id: data.from_id,
            from_name: data.from_name,
            to_id: data.to_id,
            to_name: data.to_name,
            message: data.message,
            status:"", 
            date_time: moment.utc().format(),
            created_at: new Date(),
            'msg_type':1,
            
        };

        var ins_data = {
            from_id: data.from_id,
            from_name: data.from_name,
            to_id: data.to_id,
            to_name: data.to_name,
            message: data.message,
            status:0, 
            date_time: moment.utc().format(),
            created_at: new Date(),
        };

        var socket_user_id = 'user_' + data.to_id;
       
        if (ls.get(socket_user_id) === null) { //user is offline
            var online_status = 0;
            ins_data.status = 0;
            appDB.ChatHistory.insert(ins_data, function(err, records) {
                if(err){
                    console.log(err);
                }
            });

            var apnOption ={
                "from_name":data.from_name,
                "to_id":data.to_id,
                'msg':data.message,
                'from_id': data.from_id,
            };

            mainJs.Notification(apnOption);
            //mainJs.InsertNotification(apnOption);
            
            callBack({
                "success": 1,
                "msg": "message data.",
                "result": message_data,
            });
        } else {
            ins_data.status = 1;
            appDB.ChatHistory.insert(ins_data, function(err, records) {
                if(err){
                    console.log(err);
                }
            });
            
            if(ls.get(socket_user_id).socket_ids != undefined){
                console.log("online msg send");
                io.sockets.in(ls.get(socket_user_id).socket_ids).emit('PrivateMessage', message_data);
                console.log('here-------------1');
                callBack({
                    "success": 1,
                    "msg": "login successfully.",
                    "result": message_data,
                                        
                });
            }
        }

    });
    //end private message

    socket.on('GetUserInfo', function(data,callBack) {
        var data = JSON.parse(data);
        console.log('get user info----'+data.to_id);
        var where = {
            _id:mongodb.ObjectID(data.to_id)
        };

        var uniquId = 'user_' +data.to_id;
        var online_status ="offline";

        if (ls.get(uniquId) != null) {
            online_status ="online";
        }

        console.log("----------------------"+online_status);
        appDB.users.find(where).toArray(function(err, result) {
            if(!err){
                if(result.length > 0){
                    var res ={
                        "_id":result[0]._id,
                        "full_name":result[0].Name,
                        "user_image":result[0].Image,
                        "online_status":online_status,
                        "ImageUrl":'http://34.231.75.89/upload/',

                    };
                    callBack({
                        "success": 1,
                        "online_status":online_status,
                        "result": res,
                                    
                    });
                }else{
                    callBack({
                        "success": 0,
                        "msg": "No record found."
                    });
                }
            }else{
                console.log(err);
                callBack({
                        "success": 0,
                        "msg": "No record found."
                });
            }

        });
    });
    //disconnect for app user
    socket.on('disconnect', function() {
        console.log('disconnect calling---------');
        var store_data = ls.get(socket.id);
        if(store_data != null){
             if (store_data.user_id != undefined) {
                io.emit("offline_status",store_data.user_id);
                console.log('disconnected user---' + (store_data.user_id));
                var user_id = store_data.user_id;
                var uniquId = 'user_' + user_id;
                console.log("========" + user_id);
                ls.remove(store_data.user_id);
                ls.remove(socket.id);
                ls.remove(uniquId);
                //update users last seen time

                //offline
                
                var current_time = moment.utc().valueOf(); //mainJs.getCurrentTime();
                io.emit("changeUserOnlineStatus",{
                    "user_id": user_id,
                    "online_status": "offline",
                    "msg": "Offline",
                    "last_seen": current_time
                });//offline
                return 1;
            } else {
               return 1;
            }

        }else{
            console.log("here3-----------------");
        }
        
    }); //disconnect
    //disconnect for website user
    socket.on('socket_disconnect', function() {

        console.log('disconnect calling---------');
        
        var store_data = ls.get(socket.id);
        if(store_data === null){
            console.log("here3-----------------");
        }else{



            if (store_data.user_id != undefined) {
                io.emit("offline_status",store_data.user_id);
                console.log('disconnected user---' + (store_data.user_id));
                var user_id = store_data.user_id;
                var uniquId = 'user_' + user_id;

               
                
                console.log("========" + user_id);
                //var dt = dateTime.create();
                //var formatted = dt.format('Y-m-d H:M');

                

                ls.remove(store_data.user_id);
                ls.remove(socket.id);
                ls.remove(uniquId);

                //update users last seen time

                //offline
                
                var current_time = moment.utc().valueOf(); //mainJs.getCurrentTime();
                io.emit("changeUserOnlineStatus",{
                    "user_id": user_id,
                    "online_status": "offline",
                    "msg": "Offline",
                    "last_seen": current_time
                });//offline
                return 1;
            } else {
               return 1;
            }
        } 
    }); 
    //end disconnect
});