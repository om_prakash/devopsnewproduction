

"use strict";
var mongodb = require('mongodb'),
    //assert = require('assert'),
    config = require('./config');
var url = 'mongodb://' + config.DB_HOST + ':' + config.DB_PORT + '/' + config.DB_DATABASE;

var appDataBase = function() {
        var that = this;
        /*mongodb*/
        mongodb.MongoClient.connect("mongodb://localhost:27017/development", function(err, database) {
            if (!err) {
                var db = database;
                appDB.users = db.collection('users');
                appDB.ChatHistory = db.collection('chat_history');
                appDB.TRIPS = db.collection('trips');
                appDB.DELIVERY_REQUEST = db.collection('delivery_request');
                appDB.SYS_SETTING = db.collection('system_setting');
                appDB.CONFIGURATION = db.collection('configuration');
                appDB.NOTIFICATION = db.collection('notification');
                console.log("Connected to development MongoDB...");
            } else {
                console.log(err);
            }
        });
        return this;
    } //appDataBase

var appDB = new appDataBase();
module.exports = appDB;
