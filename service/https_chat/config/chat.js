"use strict";

var ls = require('local-storage');
var appDB = require('./appDB.js');
var Notification = require('../notification');
var mongodb = require('mongodb');
var io;
var App = function() {
        this.io = null;
        return this;
    } //app

App.prototype = {
        
        ReturnResponse: function(socket, data, id) {
        console.log('here-------------2');
            var socket_user_id = 'user_' +id;
            if(ls.get(socket_user_id).socket_ids != undefined){
                console.log('here-------------3');
                io.sockets.in(ls.get(socket_user_id).socket_ids).emit('SocketResponse',data);
            }

        },

        HistoryResponse: function(socket, data, id) {
        console.log('here-------------2');
            var socket_user_id = 'user_' +id;
            
            if(ls.get(socket_user_id).socket_ids != undefined){
                console.log('here-------------3');
                io.sockets.in(ls.get(socket_user_id).socket_ids).emit('HistoryResponse',data);
            }

        },

        Notification:function(data){
            var where ={
                "_id":mongodb.ObjectID(data.to_id),

            };
            appDB.users.find(where).toArray(function(err, result) {
                if(!err){
                    if(result.length > 0){

                        var apnOption ={
                            "type": "NOTIFY_TO_OFFLINE_USER",
                            "title": "Message from "+data.from_name,
                            'notification_id':result[0].NotificationId,
                            'chat_msg':data.msg,
                            'to_id':data.to_id,
                            'from_id':data.from_id,
                        };
                        
                        try {
                            console.log('-=--=-=---=--=--=notitest- to =-==-==');
                            console.log('-=--=-=---=--=--=notitest- to---'+data.to_id);
                            console.log(result[0].NotificationId);
                            console.log(result[0].DeviceType, 'apns option----------', apnOption);
                            var notification = new Notification();
                            if(result[0].DeviceType == 'iphone'){
                                notification.IphoneNotification(apnOption,result[0].DeviceType);
                            }else if(result[0].DeviceType == 'android'){
                                notification.sendFcmNotification(apnOption,result[0].DeviceType);
                            }else{
                                console.log('device type neither iphone nor android');
                            }
                            
                        } catch (e) {
                            //console.log('Error!!!!!!!!!!!!!!==>'+e);
                        }
                    }
                }

            });
        },


        SendPendingMessage:function(socket,to_id){

            var where={
                'to_id':to_id,
                'status':0
            };
            appDB.ChatHistory.find(where).toArray(function(err, result) {
                if(!err){
                    if(result.length > 0){
                       for(var int in result) {
                            var message_data = {
                                from_id: result[int].from_id,
                                from_name: result[int].from_name,
                                to_id: result[int].to_id,
                                to_name: result[int].to_name,
                                message: result[int].message,
                                date_time: result[int].date_time,
                                'msg_type':1,
                            };

                            io.sockets.in(socket.id).emit('PrivateMessage', message_data);
                            console.log('PendingMessage--------', message_data);

                            appDB.ChatHistory.update({
                            "_id":mongodb.ObjectID(result[int]._id) 
                                }, {
                                    $set: {
                                        "status":1
                                }
                            });
                        }
                    }
                }

            });
        },

        InsertNotification:function(data){
            var ins_data ={
                'NotificationTitle':'Message from '+data.from_name,
                'NotificationShortMessage':'Message from '+data.from_name,
                'NotificationMessage':'Message from '+data.from_name,
                'NotificationType':"CHAT",
                'NotificationUserId': [mongodb.ObjectID(data.to_id)],
                'Date':new Date(),
                'GroupTo':"User"

            };
            appDB.NOTIFICATION.insert(ins_data, function(err, records) {
                if(err){
                    console.log(err);
                }else{
                    console.log('notification inser....');
                }
            });

        },

       
        init: function(args) {
            io = args[0];
        }, 
} //App.prototype


var mainJs = new App();
module.exports = mainJs;