var options = {
  auto_reconnect: true,
  db: {
    w: 1
  },
  server: {
    socketOptions: {
      keepAlive: 1
    }
  }
};
module.exports = function(MongoClient, mongodb, format, ObjectID) {
  "use strict";
  var appDB = {
    initializeMongoDb: function() {
      //================= Initialize connection once=====================================
      MongoClient.connect("mongodb://localhost:27017/development", options, function(err, database) {
        if (!err) {
          var db = database;
          appDB.USERS = db.collection('users');
          appDB.TRIPS = db.collection('trips');
          appDB.DELIVERY_REQUEST = db.collection('delivery_request');
          appDB.SYS_SETTING = db.collection('system_setting');
          appDB.CONFIGURATION = db.collection('configuration');
          appDB.ChatHistory = db.collection('chat_history');
        } else {
          console.log(err);
        }
      });
    }

  }
  return appDB;
}