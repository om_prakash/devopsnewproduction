"use strict";

var ls = require('local-storage');
var io;
var App = function() {
        this.io = null;
        return this;
    } //app

App.prototype = {
        
        ReturnResponse: function(socket, data, id) {
        console.log('here-------------2');
            var socket_user_id = 'user_' +id;
            if(ls.get(socket_user_id).socket_ids != undefined){
                console.log('here-------------3');
                io.sockets.in(ls.get(socket_user_id).socket_ids).emit('SocketResponse',data);
            }

        },

        HistoryResponse: function(socket, data, id) {
        console.log('here-------------2');
            var socket_user_id = 'user_' +id;
            
            if(ls.get(socket_user_id).socket_ids != undefined){
                console.log('here-------------3');
                io.sockets.in(ls.get(socket_user_id).socket_ids).emit('HistoryResponse',data);
            }

        },

       
        init: function(args) {
            io = args[0];
        }, 
} //App.prototype


var mainJs = new App();
module.exports = mainJs;