var express = require('express'),
	app = express(),
	server = require('http').Server(app),
	io = require('socket.io')(server),
	fs = require('fs'),
	mongodb = require('mongodb'),
	MongoClient = require('mongodb').MongoClient,
	path = require('path'),
	format = require('util').format,
	Busboy = require('busboy'),
	dateFormat = require('dateformat'),
	ls = require('local-storage'),
	moment = require('moment'),
	ObjectID = require('mongodb').ObjectID;
	var Promise = require('promise');
var mainJs = require('./public/chat.js');
//var Callback = require('node-callback');



server.listen(8081, function() {
	console.log('port listen.............');
	server.timeout = 2000000;
	mainJs.init([io]);
	//util.log('socket.io server listening at %s : 8000');
});





"use strict";

//process.env.TZ = 'Etc/UTC';
String.prototype.ucfirst = function() {
	return this.charAt(0).toUpperCase() + this.slice(1);
}

var appD = require('./public/appDB.js');
var appDB = appD(MongoClient, mongodb, format, ObjectID);
appDB.initializeMongoDb();


app.get('/', function (req, res) {
   	fs.readFile('/var/www/html/service/nodeservices/chat.html',
	function (err, data) {
	    if (err) {
	      res.writeHead(500);
	      return res.end('Error loading index.html');
	    }
	    res.writeHead(200);
	    res.end(data);
	});
});



//=================here is info fucntion======================        
app.get('/info', function(req, res) {
	console.log(__dirname + '/html/info.html');
	res.sendfile(__dirname + '/html/info.html');
});

io.on('connection', function(socket) {

	console.log('connection stablish............');
	socket.on('appTest', function (data,callBack) {
		console.log('ttttttttttttttt');
		console.log('user login data----', data);
    	/*var json = JSON.stringify({
            'message': "typing...",
            'to_id': "123",
            'from_id': "879879",
        });*/
        var json={
            'message': "typing...",
            'to_id': "123",
            'from_id': "879879",
        };
        callBack(json);
    	
  	});

  	socket.on('login', function(data, callBack) {
		console.log('user login data----', data);
		var authData = JSON.parse(data);
		if (authData.hasOwnProperty("user_id")) {
			if (authData.user_id.length == 24) {
				//console.log('user_id found----', authData.user_id.length)
				var user_id = mongodb.ObjectID(authData.user_id);
				authData.user_id = mongodb.ObjectID(authData.user_id);
				var where = {
					_id: authData.user_id
				};

				//console.log(user_id);

				appDB.USERS.find(where).toArray(function(err, result) {

						//console.log('result.length----', result.length)
						if (!err) {
							if (result.length > 0) {
								var uniquId = 'user_' + authData.user_id;
								if (ls.get(uniquId) != null) {
									ls.remove(ls.get(uniquId).socket_ids);
								}
								ls.set(uniquId, {
									'socket_ids': socket.id,
									'user_id': authData.user_id,
									'name': result[0].name,
								});
								ls.set(socket.id, {
									'socket_ids': socket.id,
									'user_id': authData.user_id,
									'name': result[0].name,

								});
								socket.join(uniquId);


								var current_time = moment.utc().valueOf();
								console.log('current_time ', current_time);
								/*DB.users.update({
									"_id": user_id
								}, {
									$set: {
										"last_seen": moment.utc().format()
									}
								});*/

								var current_time = moment.utc().valueOf();
								/*io.emit("changeUserOnlineStatus", {
									//"user_id": authData.user_id,
									"online_status": 1,
									"presence_status": 'online',
									"msg": "Online",
									"last_seen": moment.utc().format()
								}); //offline*/
								var docs =[];
								for (var i in docs) {
									docs.push({
										"FirstName": result[i].FirstName,
										"LastName": result[i].LastName,
										"UserId": result[i]._id,
										"UserImage": result[i].Image,
										"TransporterType": result[i].TransporterType,
										"Name": result[i].FirstName,
									});

								}
								console.log('login successfully.--'+result[0].FirstName);
								
								callBack({
									"success": 1,
									"msg": "login successfully.",
									"result": docs,
									
								});
							} else {
								console.log('No record found');
								callBack({
									"success": 0,
									"msg": "No record found."
								});
							}


						} else {
							console.log('Something went wrong');
							callBack({
								"success": 0,
								"msg": "Something went wrong.",
								"error": err
							});
						}

				});
				//end

			} else {
				callBack({
					"success": 0,
					"msg": "Invalid user id length."
				});
			}

		} else {
			console.log('User not found.');
			callBack({
				"success": 0,
				"msg": "User not found."
			});
		}


	}); //login

	//start typing
	socket.on('StartPrivateTyping', function(data, callBack) {
		var authData = JSON.parse(data);
		console.log('1start private typing.....',authData.to_id);
		console.log('2start private typing.....',authData.from_id);
		
        if (authData.hasOwnProperty("from_id") === true) {
        	var uniqueId = 'user_' + authData.to_id;
        	if (ls.get(uniqueId) != null) {
                    console.log('start private typing.....', ls.get(uniqueId));
                    var json = JSON.stringify({
                        'message': "typing...",
                        'to_id': authData.to_id,
                        'from_id': authData.from_id
                    });

                    if (ls.get(uniqueId).socket_ids != undefined) {
                        io.sockets.in(ls.get(uniqueId).socket_ids).emit('StartPrivateTyping',
                            json);
                    }
                    
                }

        }
	});
	//end typing

	//Private massage
	socket.on('privateMessage', function(data,callBack) {
		var data = JSON.parse(data);
		var message_data = {
            from_id: data.from_id,
            from_name: data.from_name,
            to_id: data.to_id,
            to_name: data.to_name,
            message: data.message,
            status:"", 
            date_time: moment.utc().format(),
            created_at: new Date(),
            res_type:"privatemsg",
        };

        var ins_data = {
            from_id: data.from_id,
            from_name: data.from_name,
            to_id: data.to_id,
            to_name: data.to_name,
            message: data.message,
            status:"", 
            date_time: moment.utc().format(),
            created_at: new Date(),
        };

        var socket_user_id = 'user_' + data.to_id;
       
        if (ls.get(socket_user_id) === null) { //user is offline
            var online_status = 0;
            ins_data.status = 0;
            appDB.ChatHistory.insert(ins_data, function(err, records) {
        		if(err){
        			console.log(err);
        		}
        	});
        	mainJs.ReturnResponse(socket,message_data,data.from_id);
        } else {
        	ins_data.status = 1;
        	appDB.ChatHistory.insert(ins_data, function(err, records) {
        		if(err){
        			console.log(err);
        		}
        	});
        	
        	if(ls.get(socket_user_id).socket_ids != undefined){
        		console.log("online msg send");
        		io.sockets.in(ls.get(socket_user_id).socket_ids).emit('privateMessage', message_data);
        		console.log('here-------------1');
        		
        		mainJs.ReturnResponse(socket,message_data,data.from_id);
        	}

            
        }

	});
	//end private message

	socket.on('ChatHistory', function(data,callBack) {
		console.log('chat history---');
		var data = JSON.parse(data);
		var message_data = {
            from_id: data.from_id,
            to_id: data.to_id,
        };
        console.log('chat history---'+data.to_id);

        var where={
        	from_id: data.from_id,
            to_id: data.to_id,

            //from_id: data.to_id,
            //to_id: data.to_id,	
        	//status:0,
        }

        appDB.ChatHistory.find(where).toArray(function(err, result) {
        	if (!err) {
                if (result.length > 0) {

                	var docs =[];
		            for (var i in result) {
		                docs.push({
		                        "from_id": result[i].from_id,
		                        "to_id": result[i].to_id,
		                        "message": result[i].message,
		                        "date_time": result[i].date_time,
		                        //"TransporterType": result[i].TransporterType,
		                        //"Name": result[i].FirstName,
		                });

		            }

                	callBack({
                		'status':1,
                		'result':docs
                	});

                	mainJs.HistoryResponse(socket,docs,data.to_id);
                }else{
                	callBack({
                		'status':0,
                		'result':[]
                	});
                }
                console.log('history not found..');
            }
        });


	});

	//disconnect
	socket.on('socket_disconnect', function() {

	 	console.log('disconnect calling---------');
		
	 	var store_data = ls.get(socket.id);
	 	if(store_data === null){
	 		console.log("here3-----------------");
	 	}else{



            if (store_data.user_id != undefined) {
            	io.emit("offline_status",store_data.user_id);
                console.log('disconnected user---' + (store_data.user_id));
               	var user_id = store_data.user_id;
               	var uniquId = 'user_' + user_id;

               
               	
                console.log("========" + user_id);
                //var dt = dateTime.create();
				//var formatted = dt.format('Y-m-d H:M');

                

                ls.remove(store_data.user_id);
                ls.remove(socket.id);
                ls.remove(uniquId);

                //update users last seen time

                //offline
                
                var current_time = moment.utc().valueOf(); //mainJs.getCurrentTime();
              	io.emit("changeUserOnlineStatus",{
                    "user_id": user_id,
                    "online_status": 0,
                    "msg": "Offline",
                    "last_seen": current_time
            	});//offline
				return 1;
            } else {
               return 1;
            }
        } 
    }); //disconnect
	//end disconnect

	socket.on('disconnect', function() {

	 	console.log('disconnect calling---------');
		
	 	var store_data = ls.get(socket.id);
	 	if(store_data === null){
	 		console.log("here3-----------------");
	 	}else{

            if (store_data.user_id != undefined) {
            	io.emit("offline_status",store_data.user_id);
                console.log('disconnected user---' + (store_data.user_id));
               	var user_id = store_data.user_id;
               	var uniquId = 'user_' + user_id;

               
               	
                console.log("========" + user_id);
                //var dt = dateTime.create();
				//var formatted = dt.format('Y-m-d H:M');

                

                ls.remove(store_data.user_id);
                ls.remove(socket.id);
                ls.remove(uniquId);

                //update users last seen time

                //offline
                
                var current_time = moment.utc().valueOf(); //mainJs.getCurrentTime();
              	io.emit("changeUserOnlineStatus",{
                    "user_id": user_id,
                    "online_status": 0,
                    "msg": "Offline",
                    "last_seen": current_time
            	});//offline
				return 1;
            } else {
               return 1;
            }
        } 
    }); 




});








// ================================ Near Transporter ===================================
app.get('/nearby_transporter/:reqObj',function(req, res) {

	var str = JSON.parse(req.params.reqObj);

	if (str.hasOwnProperty('lat') == true && str.hasOwnProperty('long') == true && str.hasOwnProperty('userId') == true &&
		(str.userId).length == 24) {
		var maxDistance = 100000000;
		str.lat = parseFloat(str.lat);
		str.long = parseFloat(str.long);
		str.userId = mongodb.ObjectID(str.userId);
		var available_tr = [];
/*
		appDB.TRIPS.distinct('TransporterId', {
			'SourceDate': {
				'$gt': new Date(),
			}
		},{}).toArray(function(triperr, tripdocs){
			if (!triperr & tripdocs.length > 0) {
				for (var index in tripdocs) {
					console.log(tripdocs[index]);
				}
				for (value in tripdocs) {
					available_tr.push(new ObjectID(tripdocs[value].TransporterId));
				}
			}
		});*/

		appDB.TRIPS.distinct('TransporterId',
				{'SourceDate': {
					'$gt': new Date(),
				},
			}
			,function(err, items) {
				if(!err){
					for (value in items) {
						console.log("===yyyyy==="+items[value]);
						available_tr.push(new ObjectID(items[value]));
						console.log("====arrrrrr=="+available_tr);
					}

					appDB.USERS.find({
			"_id": {
				'$ne': str.userId,
				'$in': available_tr,
			},
			"TransporterStatus": "active",
			"CurrentLocation.0": {
				"$nin": [0, '']
			},
			"CurrentLocation": {
				"$near": {
					"$geometry": {
						"type": "Point",
						"coordinates": [str.long, str.lat]
					},
					"$maxDistance": maxDistance,
					"$uniqueDocs": 1
				}
			}
		}, {
			TransporterType: 1,
			DeliveryArea: 1,
			Image: 1,
			FirstName: 1,
			RatingCount: 1,
			RatingByCount: 1,
			CurrentLocation: 1
		}).toArray(function(err, docs) {
			if (!err & docs.length > 0) {
				var result = [];

				for (var i in docs) {
					result.push({
						"Lat": docs[i].CurrentLocation[1],
						"Lng": docs[i].CurrentLocation[0],
						"UserId": docs[i]._id,
						"UserImage": docs[i].Image,
						"TransporterType": docs[i].TransporterType,
						"Name": docs[i].FirstName,
						"Rating": ((docs[i].RatingCount * 20) / docs[i].RatingByCount)
					});

				}

				res.send({
					"success": 1,
					"msg": "Transporter List",
					"result": result
				});

			} else {
				res.send({
					"success": 0,
					"msg": "No Any Transporter Near You"
				});
			}

		});

				}
					
		});
		
		
	} else {
		res.send({
			"success": 0,
			"msg": "Invalid input"
		});
	}

});

// ================================ Near Transporter ===================================
app.get('/new_request_for_transporter/:reqObj', function(req, res) {

	var str = JSON.parse(req.params.reqObj);

	if (str.hasOwnProperty('lat') == true && str.hasOwnProperty('long') == true && str.hasOwnProperty('pageno') == true &&
		str.hasOwnProperty('userId') == true && (str.userId).length == 24) {
		var maxDistance = 100000000;
		var limit = 20;
		var start = ((parseInt(str.pageno) - 1) * limit);
		str.lat = parseFloat(str.lat);
		str.long = parseFloat(str.long);
		str.userId = mongodb.ObjectID(str.userId);
		appDB.DELIVERY_REQUEST.find({
			"RequesterId": {
				'$ne': str.userId
			},
			"Status": "ready",
			"PickupLatLong": {
				"$near": {
					"$geometry": {
						"type": "Point",
						"coordinates": [str.long, str.lat]
					},
					"$maxDistance": maxDistance,
					"$uniqueDocs": 1
				}
			}
		}, {
			TransporterType: 1,
			RequesterName: 1,
			ProductTitle: 1,
			PickupCity: 1,
			DeliveryCity: 1,
			'EnterOn': 1,
			"ProductImage": 1
		}).skip(start).limit(limit).toArray(function(err, docs) {
			if (!err & docs.length > 0) {
				var result = [];

				for (var i in docs) {
					result.push({
						"Requestid": String(docs[i]._id),
						"ProductTitle": docs[i].ProductTitle,
						"RequestLocation": docs[i].PickupCity + " to " + docs[i].DeliveryCity,
						"ProductImage": docs[i].ProductImage,
						"TransporterType": docs[i].TransporterType,
						"PickupLat": docs[i].PickupLatLong[1],
						"PickupLong": docs[i].PickupLatLong[0],
					});

				}

				res.send({
					"success": 1,
					"msg": "No package found for delivery",
					"result": result
				});

			} else {
				res.send({
					"success": 0,
					"msg": "No package found for delivery"
				});
			}

		});

	} else {
		res.send({
			"success": 0,
			"msg": "Invalid input"
		});
	}

});

// ================================ Near Transporter ===================================
app.get('/new_request_for_transporter_map/:reqObj', function(req, res) {

	var str = JSON.parse(req.params.reqObj);

	if (str.hasOwnProperty('lat') == true && str.hasOwnProperty('long') == true && str.hasOwnProperty('userId') == true &&
		(str.userId).length == 24) {
		str.lat = parseFloat(str.lat);
		str.long = parseFloat(str.long);
		str.userId = mongodb.ObjectID(str.userId);
		appDB.CONFIGURATION.find({
			'_id': mongodb.ObjectID('5673e33e6734c4f874685c84')
		}, {
			CoverageArea: 1
		}).toArray(function(err, configres) {

			if (!err && configres.length > 0) {
				var maxDistance = parseFloat(configres[0].CoverageArea);
				appDB.DELIVERY_REQUEST.find({
					"RequesterId": {
						'$ne': str.userId
					},
					"Status": "ready",
					"TransporterId": "",
					"PickupLatLong.0": {
						"$nin": [0, '']
					},
					"RequestType": {
						'$in': [null, 'delivery']
					},
					"PickupLatLong": {
						"$near": {
							"$geometry": {
								"type": "Point",
								"coordinates": [str.long, str.lat]
							},
							"$maxDistance": maxDistance,
							"$uniqueDocs": 1
						}
					}
				}, {
					DeliveryFullAddress: 1,
					PickupFullAddress: 1,
					DeliveryLatLong: 1,
					PackageId: 1,
					PickupDate: 1,
					'ProductTitle': 1,
					'PickupCity': 1,
					'DeliveryCity': 1,
					'EnterOn': 1,
					"ProductImage": 1,
					"PickupLatLong": 1,
					TransporterName: 1,
					Status: 1,
					TotalCost: 1
				}).sort({
					_id: -1
				}).toArray(function(err, docs) {
					if (!err & docs.length > 0) {
						var result = [];

						for (var i in docs) {
							result.push({
								"Requestid": String(docs[i]._id),
								"PackageNumber": docs[i].PackageId,
								"RequesterName": (docs[i].TransporterName).ucfirst(),
								"ProductTitle": (docs[i].ProductTitle).ucfirst(),
								"Date": (docs[i].PickupDate / 1000),
								"RequestLocation": (docs[i].PickupCity).ucfirst() + " to " + (docs[i].DeliveryCity).ucfirst(),
								"PickupLat": docs[i].PickupLatLong[1],
								"PickupLong": docs[i].PickupLatLong[0],
								"DeliveryLat": docs[i].DeliveryLatLong[1],
								"DeliveryLong": docs[i].DeliveryLatLong[0],
								"ProductImage": docs[i].ProductImage,
								"Amount": docs[i].TotalCost,
								"Status": docs[i].Status,
								"StatusTitle": get_status_title(docs[i].Status),
								"PickupAddress": docs[i].PickupFullAddress,
								"DeliveryAddress": docs[i].DeliveryFullAddress,
							});
						}

						res.send({
							"success": 1,
							"msg": "New delivery request",
							"result": result
						});

					} else {
						res.send({
							"success": 0,
							"msg": "No package found for delivery"
						});
					}

				});

			} else {
				res.send({
					"success": 0,
					"msg": "No package found for delivery"
				});
			}

		});

	} else {
		res.send({
			"success": 0,
			"msg": "Invalid input"
		});
	}

});


function get_status_title(status) {
	switch (status) {
		case 'ready':
			return 'Ready';
			break;
		case 'trip_pending':
			return 'Pending';
			break;
		case 'trip_ready':
			return 'Ready';
			break;
		case 'out_for_delivery':
			return 'On Delivery';
			break;
		case 'out_for_pickup':
			return 'Out for Pickup';
			break;
		case 'delivered':
			return 'Delivered';
			break;
		case 'cancel':
			return 'Cancel';
			break;
		default:
			return 'Pending';

	}
}
